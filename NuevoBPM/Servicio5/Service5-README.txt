Service5 - IronMountain entrante

Este servicio expone a IMA un WebService con dos operaciones, que se usan como parte del workflow de los documentos.

En la nueva implementación el BPM escribirá directamente en la MQ y llamará al store procedure.

Copio el WSDL como referencia para que quede más claro el formato de los parámetros y otros aspectos.


Las operaciones son:
- bpmsIncomingMessage
    Mete un mensaje en una cola para AIS, espera la respuesta y se la devuelve a IMA


El mensaje que se envía a AIS es:

    		String value = requestParam.getBusDataSeg().getCod() + requestParam.getBusDataSeg().getId() + requestParam.getBusDataSeg().getMsg();


- issuanceStatusUpdate
    Llama a una función en la base de datos SNCV


El StoreProcedure que invoca lleva como parámetro:

    		String idoe = requestParam.getBusDataSeg().getIdoe();
    		String loadUser = requestParam.getBusDataSeg().getUser();
    		String reason = requestParam.getBusDataSeg().getReason();
    		String status = requestParam.getBusDataSeg().getStatus();

    		int u64idoe = Integer.parseInt(idoe);
    		String u64usuario_carga = loadUser;
    		String u64motivo = reason;
    		int u64estado = Integer.parseInt(status);

    		SNCVFacade dbFacade = new SNCVFacade();
    		String dbResp = dbFacade.P_Oe_Marcar_EstadosU591BG( u64idoe, u64usuario_carga, u64motivo, u64estado);


--------

Para acceder a la base de desarrollo, usar driver para MS SQL 2000

Credenciales en desarrollo:
Servidor ARD808VWNCDB 
Base:sncv 
Usuario: SIS_SNCVDBO 
Clave:78qpoerx 



