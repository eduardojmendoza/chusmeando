package lbawA_OfVirtualLBA;

import com.snoopconsulting.qbe.VBObjectClass;

public class lbaw_GetParamGral extends VBObjectClass {

	@Override
	public int Execute(String wvarRequest, Variant wvarResponse, String extra) {
		String resp = "<response>" + "<PARAMNUM>42</PARAMNUM>" + "</response>";
		wvarResponse.set(resp);
		return 0;
	}
	
	// PARAMNUM
}
