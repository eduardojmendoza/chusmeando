package lbawA_OfVirtualLBA;

import com.snoopconsulting.qbe.VBObjectClass;

import diamondedge.util.Variant;

public class lbaw_GetNroCot extends VBObjectClass {

	@Override
	public int Execute(String wvarRequest, Variant wvarResponse, String extra) {
		String resp = "<response>" + "<NROCOT>42</NROCOT>"
				+ "<FECHA_DIA>13</FECHA_DIA>"
				+ "<FECHA_SIGUIENTE>14</FECHA_SIGUIENTE>" + "</response>";
		wvarResponse.set(resp);
		return 0;
	}
}
