package lbawA_OVLBAMQ;

import com.snoopconsulting.qbe.VBObjectClass;

import diamondedge.util.Variant;

public class lbaw_GetSumAseg extends VBObjectClass {

	@Override
	public int Execute(String wvarRequest, Variant wvarResponse, String extra) {
		String resp = "<response>" + "<COMBO>" +
				"<OPTION value=\"1000\" sumaseg=\"1000\">1000 pesos</OPTION>" +
				"</COMBO>"
				+ "</response>";
		wvarResponse.set(resp);
		return 0;
	}
	
	// //COMBO/OPTION[@value=1000]/@sumaseg
}
