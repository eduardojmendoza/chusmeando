package com.qbe.lbawA_OfVUtilsFiles;

import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;

import com.snoopconsulting.qbe.VBObjectClass;

import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVGenerarZ_FTP extends VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OfVUtilsFiles.lbaw_OVGenerarZ_FTP";
  static final String mcteParam_SERVER = "//SERVER";
  static final String mcteParam_FileName = "//FILENAME";
  static final String mcteParam_BORRARORIGEN = "//BORRARORIGEN";
  static final String mcteParam_BORRARDESTINO = "//BORRARDESTINO";
  static final String mcteParam_FILEDESTINO = "//FILEDESTINO";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo ) throws Exception
  {
    int IAction_Execute = 0;
    int wvarStep = 0;
    String wvarResult = "";
    boolean wvarMarcaMantenerArch = false;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    org.w3c.dom.Node wobjEle = null;
    diamondedge.util.XmlDom wobjXMLServer = null;
    String wvarFilePath = "";
    String wvarFileName = "";
    Variant wvarZipPath = new Variant();
    boolean wvarBorrarOrigen = false;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //On Error GoTo errorHandler
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //
    wvarStep = 10;

    //Levanto los par�metros que llegan desde la p�gina
    wobjXMLRequest = new diamondedge.util.XmlDom();
    wobjXMLResponse = new diamondedge.util.XmlDom();
    wobjXMLServer = new diamondedge.util.XmlDom();
    wobjXMLServer.load( System.getProperty("user.dir") + "\\LBAVirtualServers.xml" );
    //unsup wobjXMLRequest.async = false;
    wobjXMLRequest.loadXML( Request );
    wvarBorrarOrigen = ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_BORRARORIGEN ) */ == (org.w3c.dom.Node) null);
    wvarFilePath = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLServer.selectSingleNode( "//" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( mcteParam_SERVER ) *) ) ) */ );
    //
    if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FILEDESTINO ) */ == (org.w3c.dom.Node) null) )
    {
      wvarZipPath.set( wvarFilePath + "\\" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FILEDESTINO ) */ ) );
    }
    //
    for( int nwobjEle = 0; nwobjEle < null /*unsup wobjXMLRequest.selectNodes( mcteParam_FileName ) */.getLength(); nwobjEle++ )
    {
      wobjEle = null /*unsup wobjXMLRequest.selectNodes( mcteParam_FileName ) */.item( nwobjEle );
      wvarFileName = diamondedge.util.XmlDom.getText( wobjEle );
      Response.set( Response + "<ZIP>" + invoke( "AppendToZip", new Variant[] { new Variant(wvarFilePath), new Variant(wvarFileName), new Variant(wvarZipPath), new Variant(wvarBorrarOrigen) } ) + "</ZIP>" );
    }
    //
    //
    Response.set( "<Response><Estado resultado='true' mensaje='' />" + Response + "</Response>" );

    wobjXMLServer = (diamondedge.util.XmlDom) null;
    wobjEle = (org.w3c.dom.Node) null;
    wobjXMLRequest = (diamondedge.util.XmlDom) null;
    wobjXMLResponse = (diamondedge.util.XmlDom) null;
    //
    wvarStep = 100;
    /*unsup mobjCOM_Context.SetComplete() */;
    IAction_Execute = 0;
    return IAction_Execute;
  }

  private String AppendToZip( String pvarPath, String pvarFileName, Variant pvarZipPath, boolean pvarBorrarArchivoOrigen ) throws Exception
  {
    String AppendToZip = "";
    String wvarFile = "";
    VbCollection wobjColFiles = null;

    if( pvarZipPath.toString().equals( "" ) )
    {
      pvarZipPath.set( pvarPath + "\\" + pvarFileName + ".zip" );
    }

    if( !"" /*unsup this.Dir( (pvarPath + "\\" + pvarFileName + ".zip"), 0 ) */.equals( "" ) )
    {
      //Bueno, una lastima
      if( !"" /*unsup this.Dir( (pvarPath + "\\" + pvarFileName + "*.txt"), 0 ) */.equals( "" ) )
      {
        FileSystem.kill( pvarPath + "\\" + pvarFileName + ".zip" );
      }
      else
      {
        //Si existe el .ZIP uso directamente ese
        AppendToZip = pvarPath + "\\" + pvarFileName + ".zip";
        return AppendToZip;
      }
    }
    if( !"" /*unsup this.Dir( (pvarPath + "\\" + pvarFileName), 0 ) */.equals( "" ) )
    {
      //Existe el archivo, se agrega y se devuelve el ZIP generado
      invoke( "GrabarDataEnZip", new Variant[] { new Variant(new Variant(new Variant( wvarFile )).toString()), new Variant(pvarZipPath.toString()), new Variant(pvarBorrarArchivoOrigen) } );
      AppendToZip = pvarZipPath.toString();
    }
    else if( !"" /*unsup this.Dir( (pvarPath + "\\" + pvarFileName + "*.*"), 0 ) */.equals( "" ) )
    {
      wobjColFiles = new VbCollection();
      wvarFile = "" /*unsup this.Dir( pvarPath + "\\" + pvarFileName + "*.*", 0 ) */;
      while( !wvarFile.equals( "" ) )
      {
        wobjColFiles.add( new Variant( wvarFile ) );
        wvarFile = "" /*unsup this.Dir() */;
      }
      for( int nwvarFile = 1; nwvarFile <= wobjColFiles.getCount(); nwvarFile++ )
      {
        wvarFile = wobjColFiles.getItem(nwvarFile).toString();
        //Agregamos al ZIP Todos los archivos Similares al enviado
        invoke( "GrabarDataEnZip", new Variant[] { new Variant(pvarPath + "\\" + wvarFile), new Variant(pvarZipPath.toString()), new Variant(pvarBorrarArchivoOrigen) } );
      }

      AppendToZip = pvarZipPath.toString();
    }

    ClearObjects: 
    wobjColFiles = (VbCollection) null;
    return AppendToZip;
  }

  private boolean GrabarDataEnZip( String pvarFile, String pvarZip, boolean pvarBorrarArchivoOrigen ) throws Exception
  {
    boolean GrabarDataEnZip = false;
    String wvarAction = "";
    String wvarRequest = "";
    String[] wvarArr = null;
    String wvarFileName = "";
    String wvarResponse = "";
    WD.NTFileManager wobjNTFM = new WD.NTFileManager();

    wvarAction = (!"" /*unsup this.Dir( pvarZip, 0 ) */.equals( "" ) ? "addtozip" : "zip");
    wvarArr = Strings.split( pvarFile, "\\", -1 );
    wvarFileName = wvarArr[(wvarArr.length-1)];
    wvarRequest = "<Request>" + "<Command>" + wvarAction + "</Command>" + "<FileName>" + pvarFile + "</FileName>" + "<NewFileName>" + pvarZip + "</NewFileName>" + "<ZipName>" + wvarFileName + "</ZipName>" + "</Request>";

    wobjNTFM = new WD.NTFileManager();
    wobjNTFM.Execute( wvarRequest, wvarResponse, "" );
    //
    if( pvarBorrarArchivoOrigen )
    {
      FileSystem.kill( pvarFile );
    }
    //
    ClearObjects: 
    wobjNTFM = (WD.NTFileManager) null;
    return GrabarDataEnZip;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
