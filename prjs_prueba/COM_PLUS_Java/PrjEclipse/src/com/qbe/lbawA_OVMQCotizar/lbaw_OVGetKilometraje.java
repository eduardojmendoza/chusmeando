package com.qbe.lbawA_OVMQCotizar;

import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

/**
 * Objetos del FrameWork
 */

public class lbaw_OVGetKilometraje implements Variant, ObjectControl,
		HSBCInterfaces.IAction {
	/**
	 * Implementacion de los objetos Datos de la accion
	 */
	static final String mcteClassName = "lbawA_OVMQCotizar.lbaw_OVGetKilometraje";
	static final String mcteOpID = "0058";
	/**
	 * Parametros XML de Entrada
	 */
	static final String mcteParam_Ramopcod = "//RAMOPCOD";
	static final String mcteParam_Campacod = "//CAMPACOD";
	static final String mcteParam_Agentcod = "//AGENTCOD";
	static final String mcteParam_AgentCla = "//AGENTCLA";
	static final String mcteParam_Codizona = "//CODIZONA";
	static final String mcteParam_Fecinici = "//FECINICI";
	static final String mcteParam_Fecinvig = "//FECINVIG";
	private Object mobjCOM_Context = null;
	private Object mobjEventLog = null;
	/**
	 * static variable for method: IAction_Execute
	 */
	private final String wcteFnName = "IAction_Execute";

	private int IAction_Execute(String Request, Variant Response,
			String ContextInfo) {
		int IAction_Execute = 0;
		Variant vbLogEventTypeError = new Variant();
		Variant gcteClassMQConnection = new Variant();
		Variant gcteRAMOPCOD = new Variant();
		Variant gcteNodosAutoScoring = new Variant();
		Variant gcteParamFileName = new Variant();
		Variant gcteConfFileName = new Variant();
		diamondedge.util.XmlDom wobjXMLRequest = null;
		diamondedge.util.XmlDom wobjXMLConfig = null;
		diamondedge.util.XmlDom wobjXMLParametros = null;
		int wvarMQError = 0;
		String wvarArea = "";
		Object wobjFrame2MQ = null;
		int wvarStep = 0;
		String wvarResult = "";
		String wvarRamopcod = "";
		String wvarCampacod = "";
		String wvarAgentcod = "";
		String wvarAgentCla = "";
		String wvarCodiZona = "";
		String wvarFecInici = "";
		String wvarFecInVig = "";
		String strParseString = "";
		String wvarError = "";
		//
		//
		//
		//
		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//
			wvarStep = 10;
			// Levanto los parámetros que llegan desde la página
			wobjXMLRequest = new diamondedge.util.XmlDom();
			// unsup wobjXMLRequest.async = false;
			wobjXMLRequest.loadXML(Request);
			// Deberá venir desde la página
			wvarCampacod = Strings
					.left(diamondedge.util.XmlDom.getText(null /*
																 * unsup
																 * wobjXMLRequest
																 * .
																 * selectSingleNode
																 * (
																 * mcteParam_Campacod
																 * )
																 */)
							+ Strings.space(4), 4);
			wvarAgentcod = Strings.right(Strings.fill(4, "0")
					+ diamondedge.util.XmlDom.getText(null /*
															 * unsup
															 * wobjXMLRequest
															 * .selectSingleNode
															 * (
															 * mcteParam_Agentcod
															 * )
															 */), 4);
			wvarAgentCla = Strings
					.left(diamondedge.util.XmlDom.getText(null /*
																 * unsup
																 * wobjXMLRequest
																 * .
																 * selectSingleNode
																 * (
																 * mcteParam_AgentCla
																 * )
																 */)
							+ Strings.space(2), 2);
			wvarCodiZona = Strings.right(Strings.fill(4, "0")
					+ diamondedge.util.XmlDom.getText(null /*
															 * unsup
															 * wobjXMLRequest
															 * .selectSingleNode
															 * (
															 * mcteParam_Codizona
															 * )
															 */), 4);
			wvarFecInici = Strings.right(Strings.fill(8, "0")
					+ diamondedge.util.XmlDom.getText(null /*
															 * unsup
															 * wobjXMLRequest
															 * .selectSingleNode
															 * (
															 * mcteParam_Fecinici
															 * )
															 */), 8);
			wvarFecInVig = Strings.right(Strings.fill(8, "0")
					+ diamondedge.util.XmlDom.getText(null /*
															 * unsup
															 * wobjXMLRequest
															 * .selectSingleNode
															 * (
															 * mcteParam_Fecinvig
															 * )
															 */), 8);
			//
			//
			wvarStep = 20;
			wobjXMLRequest = (diamondedge.util.XmlDom) null;
			//
			wvarStep = 30;
			// Levanto los datos de la cola de MQ del archivo de configuración
			wobjXMLConfig = new diamondedge.util.XmlDom();
			// unsup wobjXMLConfig.async = false;
			wobjXMLConfig.load(System.getProperty("user.dir") + "\\"
					+ gcteConfFileName);
			//
			// Levanto los parámetros generales (ParametrosMQ.xml)
			wvarStep = 60;
			wobjXMLParametros = new diamondedge.util.XmlDom();
			// unsup wobjXMLParametros.async = false;
			wobjXMLParametros.load(System.getProperty("user.dir") + "\\"
					+ gcteParamFileName);
			wvarRamopcod = diamondedge.util.XmlDom.getText(null /*
																 * unsup
																 * wobjXMLParametros
																 * .
																 * selectSingleNode
																 * (
																 * String.valueOf
																 * (
																 * gcteNodosAutoScoring
																 * ) +
																 * gcteRAMOPCOD
																 * )
																 */);
			//
			wvarStep = 70;
			wobjXMLParametros = (diamondedge.util.XmlDom) null;
			//
			wvarStep = 80;
			wvarArea = mcteOpID + wvarRamopcod + wvarCampacod + wvarAgentcod
					+ wvarAgentCla + wvarCodiZona + wvarFecInici + wvarFecInVig;
			diamondedge.util.XmlDom.setText(null /*
												 * unsup
												 * wobjXMLConfig.selectSingleNode
												 * (
												 * "//MQCONFIG/GMO_WAITINTERVAL"
												 * )
												 */, String.valueOf(VB
					.val(diamondedge.util.XmlDom.getText(null /*
															 * unsup
															 * wobjXMLConfig
															 * .selectSingleNode
															 * (
															 * "//MQCONFIG/GMO_WAITINTERVAL"
															 * )
															 */)) * 40));
			wobjFrame2MQ = new gcteClassMQConnection();
			// error: function 'Execute' was not found.
			// unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea,
			// strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
			wobjFrame2MQ = null;
			//
			wvarStep = 150;
			if (wvarMQError != 0) {
				Response.set("<Response><Estado resultado="
						+ String.valueOf((char) (34)) + "false"
						+ String.valueOf((char) (34)) + " mensaje="
						+ String.valueOf((char) (34))
						+ "El servicio de consulta no se encuentra disponible"
						+ String.valueOf((char) (34)) + " />" + "Codigo Error:"
						+ wvarMQError + "</Response>");
				mobjEventLog
						.Log(this
								.getValue("EventLog_Category.evtLog_Category_Unexpected"),
								mcteClassName + "--" + mcteClassName,
								wcteFnName, wvarStep, Err.getError()
										.getNumber(), "Error= [" + wvarMQError
										+ "] - " + strParseString + " Area:"
										+ wvarArea + " Hora:" + DateTime.now(),
								vbLogEventTypeError);
				// unsup GoTo ClearObjects
			}
			//
			//
			wvarStep = 180;

			if (!Strings.trim(strParseString).equals("")) {
				wvarStep = 190;
				//
				wvarResult = invoke("ParseoMensaje", new Variant[] {
						new Variant(strParseString), new Variant(35) });
				//
				wvarStep = 200;
				Response.set("<Response><Estado resultado="
						+ String.valueOf((char) (34)) + "true"
						+ String.valueOf((char) (34)) + " mensaje="
						+ String.valueOf((char) (34))
						+ String.valueOf((char) (34)) + " />" + wvarResult
						+ "</Response>");

			} else {
				Response.set("<Response><Estado resultado="
						+ String.valueOf((char) (34)) + "false"
						+ String.valueOf((char) (34)) + " mensaje="
						+ String.valueOf((char) (34))
						+ "NO SE ENCONTRARON DATOS."
						+ String.valueOf((char) (34)) + " /></Response>");
			}
			//
			wvarStep = 220;
			//
			wvarStep = 230;
			/* unsup mobjCOM_Context.SetComplete() */;
			IAction_Execute = 0;
			//
			// ~~~~~~~~~~~~~~~
			ClearObjects:
			// ~~~~~~~~~~~~~~~
			// LIBERO LOS OBJETOS
			wobjXMLConfig = (diamondedge.util.XmlDom) null;
			return IAction_Execute;
			//
			// ~~~~~~~~~~~~~~~
		} catch (Exception _e_) {
			Err.set(_e_);
			try {
				// ~~~~~~~~~~~~~~~
				//
				mobjEventLog
						.Log(this
								.getValue("EventLog_Category.evtLog_Category_Unexpected"),
								mcteClassName, wcteFnName, wvarStep, Err
										.getError().getNumber(), "Error= ["
										+ Err.getError().getNumber() + "] - "
										+ Err.getError().getDescription()
										+ " Mensaje:" + mcteOpID + wvarRamopcod
										+ wvarCampacod + wvarAgentcod
										+ wvarAgentCla + wvarCodiZona
										+ wvarFecInici + wvarFecInVig
										+ " Hora:" + DateTime.now(),
								vbLogEventTypeError);
				IAction_Execute = 1;
				/* unsup mobjCOM_Context.SetAbort() */;
				// unsup Resume ClearObjects
				Err.clear();
			} catch (Exception _e2_) {
			}
		}
		return IAction_Execute;
	}

	private void ObjectControl_Activate() throws Exception {
	}

	private boolean ObjectControl_CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		return ObjectControl_CanBePooled;
	}

	private void ObjectControl_Deactivate() throws Exception {
	}

	private String ParseoMensaje(String strParseString, int plPos)
			throws Exception {
		String ParseoMensaje = "";
		String wvarResult = "";
		int i = 0;
		String wvarDesde = "";
		String wvarHasta = "";

		//
		if (!Strings.trim(strParseString).equals("")) {
			for (i = 1; i <= 50; i++) {
				wvarDesde = Strings
						.right(Strings.space(7)
								+ String.valueOf(Obj.toInt("0"
										+ Strings.mid(strParseString, plPos, 7))),
								7);
				if (!Strings.trim(wvarDesde).equals("0")) {
					wvarHasta = Strings.right(
							Strings.space(7)
									+ String.valueOf(Obj.toInt("0"
											+ Strings.mid(strParseString,
													plPos + 7, 7))), 7);
					wvarResult = wvarResult + "<OPTION value='"
							+ Strings.trim(wvarHasta) + "' >" + wvarDesde
							+ " - " + wvarHasta + "</OPTION>";
				}
				plPos = plPos + 14;
			}

		}

		//
		ParseoMensaje = wvarResult;
		//
		return ParseoMensaje;
	}

	public void Activate() throws Exception {
		//
		mobjCOM_Context = null /* unsup this.GetObjectContext() */;
		mobjEventLog = new HSBC.EventLog();
		//
	}

	public boolean CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		//
		ObjectControl_CanBePooled = true;
		//
		return ObjectControl_CanBePooled;
	}

	public void Deactivate() throws Exception {
		//
		mobjCOM_Context = (Object) null;
		mobjEventLog = null;
		//
	}
}
