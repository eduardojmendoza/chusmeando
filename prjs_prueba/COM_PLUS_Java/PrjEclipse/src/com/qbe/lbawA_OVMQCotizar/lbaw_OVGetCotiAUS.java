package com.qbe.lbawA_OVMQCotizar;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import java.io.File;

import javax.swing.*;

import com.snoopconsulting.qbe.EventLog;
import com.snoopconsulting.qbe.MQConnectionConnector;
import com.snoopconsulting.qbe.StringHolder;
import com.snoopconsulting.qbe.VBObjectClass;
import com.snoopconsulting.qbe.XmlDomExtended;

import diamondedge.swing.*;
/**
 * ******************************************************************************
 * Fecha de Modificaci�n: 30/09/2011
 * PPCR: 2011-00390
 * Desarrollador: Leonardo Ruiz
 * Descripci�n: Anexo I (Nueva Respuesta, Planes y Coberturas Variables).
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2011. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : lbaw_OVGetCotiAUS
 *  File Name : lbaw_OVGetCotiAUS.cls
 *  Creation Date: 30/09/2011
 *  Programmer : Desconocido
 *  Abstract :   Cotizacion de Autoscoring
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class lbaw_OVGetCotiAUS extends VBObjectClass implements  ObjectControl, HSBCInterfaces_IAction 
{
  /**
   * 
   * Implementacion de los objetos
   * 
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQCotizar.lbaw_OVGetCotiAUS";
  static final String mcteOpID = "0047";
  /**
   * 
   * Parametros XML de Entrada
   */
  static final String mcteParam_Cliensec = "//CLIENSEC";
  static final String mcteParam_NacimAnn = "//NACIMANN";
  static final String mcteParam_NacimMes = "//NACIMMES";
  static final String mcteParam_NacimDia = "//NACIMDIA";
  static final String mcteParam_Sexo = "//SEXO";
  static final String mcteParam_Estado = "//ESTADO";
  static final String mcteParam_IVA = "//IVA";
  static final String mcteParam_IBB = "//IBB";
  /**
   * Fjo 2009-02-13
   */
  static final String mcteParam_CLIENTIP = "//CLIENTIP";
  static final String mcteParam_CobroTip = "//COBROTIP";
  static final String mcteParam_CobroCod = "//COBROCOD";
  static final String mcteParam_AgeCod = "//AGECOD";
  /**
   * MMC 2012-11-28
   */
  static final String mcteParam_AgeCla = "//AGECLA";
  static final String mcteParam_DatosPlan = "//DATOSPLAN";
  static final String mcteParam_ModeAutCod = "//MODEAUTCOD";
  static final String mcteParam_KMsrngCod = "//KMSRNGCOD";
  static final String mcteParam_EfectAnn = "//EFECTANN";
  static final String mcteParam_SiGarage = "//SIGARAGE";
  static final String mcteParam_Siniestros = "//SINIESTROS";
  static final String mcteParam_Gas = "//GAS";
  static final String mcteParam_ClubLBA = "//CLUBLBA";
  static final String mcteParam_Luneta = "//LUNETA";
  static final String mcteParam_Green = "//CLUBECO";
  static final String mcteParam_Granizo = "//GRANIZO";
  static final String mcteParam_RoboCont = "//ROBOCONT";
  static final String mcteParam_Provi = "//PROVI";
  static final String mcteParam_LocalidadCod = "//LOCALIDADCOD";
  static final String mcteParam_Campacod = "//CAMPACOD";
  static final String mcteParam_EsCero = "//ESCERO";
  static final String mcteParam_Portal = "//PORTAL";
  /**
   * 23/01/2006
   */
  static final String mcteParam_DEST80 = "//DESTRUCCION_80";
  /**
   * Datos de los Hijos
   */
  static final String mcteNodos_Hijos = "//Request/HIJOS/HIJO";
  static final String mcteParam_NacimHijo = "NACIMHIJO";
  static final String mcteParam_SexoHijo = "SEXOHIJO";
  static final String mcteParam_EstadoHijo = "ESTADOHIJO";
  /**
   * Datos de los Accesorios
   */
  static final String mcteNodos_Accesorios = "//Request/ACCESORIOS/ACCESORIO";
  static final String mcteParam_PrecioAcc = "PRECIOACC";
  static final String mcteParam_DescripcionAcc = "DESCRIPCIONACC";
  static final String mcteParam_CodigoAcc = "CODIGOACC";
  /**
   * Para Mercado Abierto
   */
  static final String mcteParam_CampaTel = "//TELEFONO";
  /**
   * Incios y Tama�os
   */
  static final int mcteInicioCHijos = 3 - 1;
  static final int mcteInicioSHijos = 2323 - 1;
  static final int mcteInicioOtrosD = 6689 - 1;
  static final int mcteTamanoCHijos = 116;
  static final int mcteTamanoSHijos = 165;
  static final int mcteTamanoOtrosD = 129;
  static final int mcteInicioTbDECA = 5633 - 1;
  static final int mcteTamanoTbDECA = 35;
  static final int mcteInicioMxDECA = 6686 - 1;
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParams = null;
    XmlDomExtended wobjXMLConfig = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    VBObjectClass wobjClass = new lbawA_OfVirtualLBA.lbaw_GetPortalComerc();
    String wvarRequest = "";
    String wvarResponse = "";
    int wvarMQError = 0;
    String wvarArea = "";
    MQConnectionConnector wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCliensec = "";
    String wvarMensaje = "";
    String wvarNacimAnn = "";
    String wvarNacimMes = "";
    String wvarNacimDia = "";
    String wvarSexo = "";
    String wvarEstado = "";
    String wvarIVA = "";
    String wvarIBB = "";
    String wvarCLIENTIP = "";
    String wvarCobroCod = "";
    String wvarCobroTip = "";
    String wvarAgeCod = "";
    String wvarAgeCla = "";
    String wvarDatosPlan = "";
    String wvarModeAutCod = "";
    String wvarKMsrngCod = "";
    String wvarEfectAnn = "";
    String wvarSiGarage = "";
    String wvarSiniestros = "";
    String wvarGas = "";
    String wvarClubLBA = "";
    String wvarLuneta = "";
    String wvarGreen = "";
    String wvarGranizo = "";
    String wvarRoboCont = "";
    String wvarProvi = "";
    String wvarLocalidadCod = "";
    String wvarHijos = "";
    String wvarConHijos = "";
    String wvarAccesorios = "";
    String wvarCampacod = "";
    String wvarEsCero = "";
    String wvarDEST80 = "";
    String wvarSumaMin = "";
    String wvarSumAseg = "";
    String wvarSumaLBA = "";
    String wvarCotiID = "";
    String wvarFechaDia = "";
    String wvarFechaSig = "";
    String wvarCampaTelForm = "";
    String wvarCampaTel = "";
    String strParseString = "";
    int wvariCounter = 0;
    String wvarAuxCodAcc = "";
    String wvarAuxPlanCicle = "";
    String wvarAuxCobeCicle = "";
    int wvarContadorPlan = 0;
    int wvarContadorCobs = 0;
    
    Variant vv = new Variant();

    //
    //
    //
    //
    //
    //Fjo 2009-02-13
    //MMC 2012-11-28
    //Agregado para promos de los 0Km - 07/12/2005
    //Agregado para la Destrucci�n al 80% - 23/01/2006
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Cargar el XML de Entrada
      wobjXMLRequest = new XmlDomExtended();
      //FUE wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //
      //Obtener el Numero de Cotizaci�n
      wvarStep = 10;
      wvarRequest = "<Request></Request>";
      wobjClass = new lbawA_OfVirtualLBA.lbaw_GetNroCot();
      vv.set(wvarResponse);
      wobjClass.Execute(wvarRequest, vv, "");
      wvarResponse = (String)vv.toObject();
      // nop wobjClass = (lbawA_OfVirtualLBA.lbaw_GetPortalComerc) null;
      //
      wvarStep = 20;
      wobjXMLParams = new XmlDomExtended();
      //FUE wobjXMLParams.async = false;
      wobjXMLParams.loadXML( wvarResponse );
      wvarCotiID = XmlDomExtended.getText( wobjXMLParams.selectSingleNode( "//NROCOT" )  );
      wvarFechaDia = XmlDomExtended.getText( wobjXMLParams.selectSingleNode( "//FECHA_DIA" )  );
      wvarFechaSig = XmlDomExtended.getText( wobjXMLParams.selectSingleNode( "//FECHA_SIGUIENTE" )  );
      //
      wobjXMLParams = (XmlDomExtended) null;
      //
      //Obtener la M�nima Suma
      wvarStep = 30;
      wvarRequest = "<Request><CONCEPTO>SA-SCO-MIN</CONCEPTO></Request>";
      wobjClass = new lbawA_OfVirtualLBA.lbaw_GetParamGral();
      vv.set(wvarResponse);
      wobjClass.Execute(wvarRequest, vv, "");
      wvarResponse = (String)vv.toObject();
      // nop wobjClass = (lbawA_OfVirtualLBA.lbaw_GetPortalComerc) null;
      //
      wvarStep = 40;
      wobjXMLParams = new XmlDomExtended();
      //FUE wobjXMLParams.async = false;
      wobjXMLParams.loadXML( wvarResponse );
      wvarSumaMin = Strings.right( Strings.fill( 11, "0" ) + String.valueOf( Obj.toDecimal( XmlDomExtended.getText( wobjXMLParams.selectSingleNode( "//PARAMNUM" )  ) ).multiply( new java.math.BigDecimal( 100 ) ).doubleValue() ), 11 );
      //
      wobjXMLParams = (XmlDomExtended) null;
      //
      //Buscar la Suma Asegurada para AUPROCOD = 02
      wvarStep = 50;
      wvarRequest = "<Request><AUMODCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ModeAutCod ) ) + "</AUMODCOD><AUPROCOD>02</AUPROCOD></Request>";
      wobjClass = new lbawA_OVLBAMQ.lbaw_GetSumAseg();
      vv.set(wvarResponse);
      wobjClass.Execute(wvarRequest, vv, "");
      wvarResponse = (String)vv.toObject();
      //
      //Valor de SUMASEG
      wvarStep = 60;
      wobjXMLParams = new XmlDomExtended();
      //FUE wobjXMLParams.async = false;
      wobjXMLParams.loadXML( wvarResponse );
      wvarSumAseg = Strings.right( Strings.fill( 11, "0" ) + Strings.replace( XmlDomExtended.getText( wobjXMLParams.selectSingleNode( "//COMBO/OPTION[@value=" + XmlDomExtended.getText(  (wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn ) ) ) + "]/@sumaseg" )  ), ",", "" ), 11 );
      wobjXMLParams = (XmlDomExtended) null;
      //
      //Buscar la Suma Asegurada para AUPROCOD = 03
      wvarStep = 70;
      wvarRequest = "<Request><AUMODCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ModeAutCod ) ) + "</AUMODCOD><AUPROCOD>02</AUPROCOD></Request>";
      vv.set(wvarResponse);
      wobjClass.Execute(wvarRequest, vv, "");
      wvarResponse = (String)vv.toObject();
      //
      //Valor de SUMALBA
      wvarStep = 80;
      wobjXMLParams = new XmlDomExtended();
      //FUE wobjXMLParams.async = false;
      wobjXMLParams.loadXML( wvarResponse );
      wvarSumaLBA = Strings.right( Strings.fill( 11, "0" ) + 
    		  Strings.replace( XmlDomExtended.getText( wobjXMLParams.selectSingleNode( "//COMBO/OPTION[@value=" + 
    				  	XmlDomExtended.getText(  (wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn ) ) ) + "]/@sumaseg" )  ), ",", "" ), 11 );
      wobjXMLParams = (XmlDomExtended) null;
      // nop wobjClass = (lbawA_OfVirtualLBA.lbaw_GetPortalComerc) null;
      //
      wvarStep = 90;
      if( wobjXMLRequest.selectNodes( mcteParam_Cliensec ) .getLength() == 0 )
      {
        wvarCliensec = Strings.fill( 9, "0" );
      }
      else
      {
        wvarCliensec = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Cliensec )  ), 9 );
      }
      wvarNacimAnn = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NacimAnn )  );
      wvarNacimMes = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NacimMes )  );
      wvarNacimDia = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NacimDia )  );
      wvarSexo = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Sexo )  );
      wvarEstado = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Estado )  );
      wvarIVA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_IVA )  );
      wvarIBB = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_IBB )  );
      //
      if( wobjXMLRequest.selectNodes( mcteParam_CLIENTIP ) .getLength() == 0 )
      {
        //Para forzar la persona f�sica si no informa el nodo
        wvarCLIENTIP = "00";
      }
      else
      {
        wvarCLIENTIP = Strings.right( "00" + Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENTIP )  ) ), 2 );
      }
      //
      //Mercado Abierto siempre Cotiza con COBROCOD = 4 y COBROTIP = VI
      if( wobjXMLRequest.selectNodes( mcteParam_CobroCod ) .getLength() == 0 )
      {
        wvarCobroCod = "4";
      }
      else
      {
        wvarCobroCod = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CobroCod )  );
      }
      //
      if( wobjXMLRequest.selectNodes( mcteParam_CobroTip ) .getLength() == 0 )
      {
        wvarCobroTip = "VI";
      }
      else
      {
        wvarCobroTip = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CobroTip )  );
      }
      //
      wvarAgeCod = Strings.right( Strings.fill( 4, "0" ) + Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AgeCod )  ) ), 4 );
      //
      //2012-11-28
      wvarAgeCla = Strings.right( Strings.fill( 2, "0" ) + Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AgeCla )  ) ), 2 );
      //Plan y Franquicia
      if( wobjXMLRequest.selectNodes( mcteParam_DatosPlan ) .getLength() == 0 )
      {
        //Cotiza todos los Planes
        wvarDatosPlan = "00000";
      }
      else
      {
        wvarDatosPlan = Strings.right( Strings.fill( 5, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DatosPlan )  ), 5 );
      }
      //
      wvarStep = 100;
      wvarModeAutCod = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ModeAutCod )  );
      wvarKMsrngCod = Strings.right( Strings.fill( 7, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_KMsrngCod )  ), 7 );
      wvarEfectAnn = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn )  );
      wvarSiGarage = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SiGarage )  );
      wvarSiniestros = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Siniestros ) ), 2 );
      wvarGas = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Gas ) );
      wvarClubLBA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClubLBA ) );
      wvarLuneta = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Luneta ) );
      wvarGreen = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Green )  ), 1 );
      wvarGranizo = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Granizo )  ), 1 );
      wvarRoboCont = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RoboCont )  ), 1 );
      wvarProvi = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Provi )  ), 2 );
      wvarLocalidadCod = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_LocalidadCod )  ), 4 );
      // Marca de OKm Se agreg� para las promos - 07/12/2005
      wvarEsCero = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EsCero ) );
      // Destrucci�n al 80%  --- 23/01/2006
      if( wobjXMLRequest.selectNodes( mcteParam_DEST80 ) .getLength() > 0 )
      {
        wvarDEST80 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DEST80 ) );
      }
      if( wvarDEST80.equals( "" ) )
      {
        wvarDEST80 = "N";
      }
      //
      //~~~~~~~~~~~
      //Incio Hijos
      //~~~~~~~~~~~
      wvarStep = 110;
      wvarHijos = "";
      wvarConHijos = "N";
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Hijos ) ;
      //
      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarConHijos = "S";
        // TODO implementar funcion que ejecute xpath sobre el Node, que no soporta selectSingleNode
        wvarHijos = wvarHijos + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item( wvariCounter),mcteParam_NacimHijo ) );
        wvarHijos = wvarHijos + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item( wvariCounter),mcteParam_SexoHijo ) );
        wvarHijos = wvarHijos + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item( wvariCounter),mcteParam_EstadoHijo ) );
      }
      //
      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 9; wvariCounter++ )
      {
        wvarHijos = wvarHijos + Strings.fill( 8, "0" ) + "  ";
      }
      //~~~~~~~~~
      //Fin Hijos
      //~~~~~~~~~
      //
      //~~~~~~~~~~~~~~~~~
      //Inicio Accesorios
      //~~~~~~~~~~~~~~~~~
      wvarStep = 120;
      wvarAccesorios = "";
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) ;
      //
      wvarAuxCodAcc = "";
      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarAuxCodAcc = wvarAuxCodAcc + Strings.right( Strings.fill( 4, "0" ) + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item( wvariCounter),mcteParam_CodigoAcc )  ), 4 );
        wvarAccesorios = wvarAccesorios + Strings.right( Strings.fill( 14, "0" ) + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item( wvariCounter),mcteParam_PrecioAcc )  ), 14 );
        wvarAccesorios = wvarAccesorios + Strings.left( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item( wvariCounter),mcteParam_DescripcionAcc ) ) + Strings.fill( 30, " " ), 30 );
        wvarAccesorios = wvarAccesorios + "S";
      }
      //
      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 9; wvariCounter++ )
      {
        wvarAccesorios = wvarAccesorios + Strings.fill( 14, "0" ) + Strings.fill( 31, " " );
      }
      //~~~~~~~~~~~~~~
      //Fin Accesorios
      //~~~~~~~~~~~~~~
      wvarCampacod = Strings.right( Strings.fill( 4, "0" ) + Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Campacod )  ) ), 4 );
      //
      //Buscar los datos de Campa�a, Productor (Solo para Mercado Abierto)
      if( wobjXMLRequest.selectNodes( mcteParam_Portal ) .getLength() != 0 )
      {
        if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Portal ) ).equals( "LBA_PRODUCTORES" )) )
        {
          wvarStep = 130;
          wvarRequest = "<Request><PORTAL>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Portal )  ) + "</PORTAL><RAMOPCOD>AUS1</RAMOPCOD></Request>";
          wobjClass = new lbawA_OfVirtualLBA.lbaw_GetPortalComerc();
          vv.set(wvarResponse);
          wobjClass.Execute(wvarRequest, vv, "");
          wvarResponse = (String)vv.toObject();
          //
          wvarStep = 140;
          wobjXMLParams = new XmlDomExtended();
          //FUE wobjXMLParams.async = false;
          wobjXMLParams.loadXML( wvarResponse );
          wvarCampacod = Strings.left( XmlDomExtended.getText( wobjXMLParams.selectSingleNode( mcteParam_Campacod )  ) + Strings.fill( 4, " " ), 4 );
          wvarAgeCod = Strings.trim( XmlDomExtended.getText( wobjXMLParams.selectSingleNode( mcteParam_AgeCod )  ) );
          //MMC 2012-11-28
          wvarAgeCla = Strings.trim( XmlDomExtended.getText( wobjXMLParams.selectSingleNode( mcteParam_AgeCla )  ) );
          wvarCampaTel = XmlDomExtended.getText( wobjXMLParams.selectSingleNode( mcteParam_CampaTel )  );
          wvarCampaTelForm = Formateo_Telefono(XmlDomExtended.getText( wobjXMLParams.selectSingleNode( mcteParam_CampaTel )));
          wobjXMLParams = (XmlDomExtended) null;
          // nop wobjClass = (lbawA_OfVirtualLBA.lbaw_GetPortalComerc) null;
        }
      }
      //
      wobjXMLRequest = (XmlDomExtended) null;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      //
      wvarStep = 150;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      //FUE wobjXMLConfig.async = false;
      String filename = System.getProperty("user.dir") + "/" /* rgm, era "\\" */ + ModGeneral.gcteConfFileName;
      wobjXMLConfig.load(filename);
      //
      wvarMensaje = mcteOpID + wvarCliensec + wvarNacimAnn + wvarNacimMes + wvarNacimDia;
      wvarMensaje = wvarMensaje + wvarSexo + wvarEstado;
      wvarMensaje = wvarMensaje + wvarIVA + wvarIBB;
      //Fjo 2009-02-13
      wvarMensaje = wvarMensaje + wvarCLIENTIP;
      wvarMensaje = wvarMensaje + wvarCobroTip + wvarCobroCod + wvarFechaDia + wvarFechaSig + wvarAgeCod + wvarDatosPlan + wvarSumAseg + wvarSumaLBA;
      wvarMensaje = wvarMensaje + wvarModeAutCod + "001" + wvarKMsrngCod + wvarEfectAnn + wvarSiGarage;
      wvarMensaje = wvarMensaje + wvarSiniestros + wvarGas + wvarClubLBA + wvarLuneta + wvarGreen + wvarGranizo + wvarRoboCont + "00" + wvarProvi + wvarLocalidadCod;
      wvarMensaje = wvarMensaje + wvarHijos;
      wvarMensaje = wvarMensaje + wvarAccesorios;
      wvarMensaje = wvarMensaje + Strings.right( Strings.fill( 9, "0" ) + wvarCotiID, 9 ) + wvarSumaMin + wvarCampacod + Strings.fill( 36, " " );
      wvarMensaje = wvarMensaje + "000000000000000000000000NNNSSSN" + Strings.left( (wvarAuxCodAcc + Strings.fill( 40, "0" )), 40 );
      wvarMensaje = wvarMensaje + wvarEsCero + wvarDEST80 + wvarAgeCla;
      //
      wvarArea = wvarMensaje;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = new MQConnectionConnector();
      StringHolder sh = new StringHolder(strParseString);
      wvarMQError = wobjFrame2MQ.Execute( wvarArea, sh, wobjXMLConfig.selectSingleNode( "//MQCONFIG" ) .toString() );
      strParseString = sh.getValue();
      
      wobjFrame2MQ = null;
      //
      wvarStep = 155;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //DONE GoTo ClearObjects
        // LIBERO LOS OBJETOS
        wobjXMLConfig = (XmlDomExtended) null;
        return IAction_Execute;

      }
      //
      //GuardarLog strParseString
      //Cargo un archivo fijo de prueba
      //strParseString = CargarArchivoPrueba
      //strParseString = Replace(Replace(strParseString, Chr(10), ""), Chr(13), "")
      //
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //HRF Anexo I (Nueva Respuesta, Planes y Coberturas Variables)
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //~~~~~~~~~~~~~~~~~~~~~~~~~~
      //Incio Response del Mensaje
      //~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarStep = 200;
      wvarResult = "";
      //
      if( Strings.toUpperCase( Strings.left( strParseString, 2 ) ).equals( "OK" ) )
      {
        wvarResult = wvarResult + "<COT_NRO>" + wvarCotiID + "</COT_NRO>";
        wvarResult = wvarResult + "<CAMPA_COD>" + wvarCampacod + "</CAMPA_COD>";
        wvarResult = wvarResult + "<AGE_COD>" + wvarAgeCod + "</AGE_COD>";
        wvarResult = wvarResult + "<AGE_CLA>" + wvarAgeCla + "</AGE_COD>";
        //Solo para Mercado Abierto
        wvarResult = wvarResult + "<CAMPA_TEL>" + wvarCampaTel + "</CAMPA_TEL>";
        wvarResult = wvarResult + "<CAMPA_TEL_FORM>" + wvarCampaTelForm + "</CAMPA_TEL_FORM>";
        //
        wvarResult = wvarResult + "<SUMASEG>" + wvarSumAseg + "</SUMASEG>";
        wvarResult = wvarResult + "<SUMALBA>" + wvarSumaLBA + "</SUMALBA>";
        wvarResult = wvarResult + "<SILUNETA>" + Strings.trim( Strings.mid( strParseString, (mcteInicioMxDECA + 1 + 1), 1 ) ) + "</SILUNETA>";
        wvarResult = wvarResult + "<TIENEHIJOS>" + wvarConHijos + "</TIENEHIJOS>";
        //
        //~~~~~~~~~~~~
        //Incio Planes
        //~~~~~~~~~~~~
        wvarStep = 210;
        wvarResult = wvarResult + "<PLANES>";
        //
        wvarAuxPlanCicle = "1";
        wvarContadorPlan = 0;
        //Todos los planes
        while( (VB.val( wvarAuxPlanCicle ) > 0) && (wvarContadorPlan < 20) )
        {
          //Plan
          wvarResult = wvarResult + "<PLAN>";
          //Datos
          wvarResult = wvarResult + "<PLANNCOD>" + Strings.trim( Strings.mid( strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 0 + 1), 5 ) ) + "</PLANNCOD>";
          wvarResult = wvarResult + "<PLANNDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 0 + 1), 121 ) ) + "]]></PLANNDES>";
          wvarResult = wvarResult + "<ORDEN>" + Strings.trim( Strings.mid( strParseString, (mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 125 + 1), 4 ) ) + "</ORDEN>";
          wvarResult = wvarResult + "<LUNPAR>" + Strings.trim( Strings.mid( strParseString, (mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 121 + 1), 1 ) ) + "</LUNPAR>";
          wvarResult = wvarResult + "<GRANIZO>" + Strings.trim( Strings.mid( strParseString, (mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 122 + 1), 1 ) ) + "</GRANIZO>";
          wvarResult = wvarResult + "<ROBOCON>" + Strings.trim( Strings.mid( strParseString, (mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 123 + 1), 1 ) ) + "</ROBOCON>";
          wvarResult = wvarResult + "<ESRC>" + Strings.trim( Strings.mid( strParseString, (mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 124 + 1), 1 ) ) + "</ESRC>";
          //Precio c/Hijos
          wvarResult = wvarResult + "<CON_HIJOS>";
          //LR 11/10/2011 La PRIMA de CON_HIJOS pasa a ser "05 PLANNDES PIC X(1)"
          wvarResult = wvarResult + "<PRIMA>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 6 + 1), 11 )) } ) + "</PRIMA>";
          wvarResult = wvarResult + "<RECARGOS>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 17 + 1), 11 )) } ) + "</RECARGOS>";
          wvarResult = wvarResult + "<IVAIMPOR>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 28 + 1), 11 ))} ) + "</IVAIMPOR>";
          wvarResult = wvarResult + "<IVAIMPOA>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 39 + 1), 11 )) } ) + "</IVAIMPOA>";
          wvarResult = wvarResult + "<IVARETEN>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 50 + 1), 11 )) } ) + "</IVARETEN>";
          wvarResult = wvarResult + "<DEREMI>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 61 + 1), 11 )) } ) + "</DEREMI>";
          wvarResult = wvarResult + "<SELLADO>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 72 + 1), 11 )) } ) + "</SELLADO>";
          wvarResult = wvarResult + "<INGBRU>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 83 + 1), 11 )) } ) + "</INGBRU>";
          wvarResult = wvarResult + "<IMPUES>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 94 + 1), 11 )) } ) + "</IMPUES>";
          wvarResult = wvarResult + "<PRECIO>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 105 + 1), 11 )) } ) + "</PRECIO>";
          wvarResult = wvarResult + "</CON_HIJOS>";
          //Precio s/Hijos
          wvarResult = wvarResult + "<SIN_HIJOS>";
          wvarResult = wvarResult + "<PRIMA>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 55 + 1), 11 )) } ) + "</PRIMA>";
          wvarResult = wvarResult + "<RECARGOS>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 66 + 1), 11 )) } ) + "</RECARGOS>";
          wvarResult = wvarResult + "<IVAIMPOR>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 77 + 1), 11 )) } ) + "</IVAIMPOR>";
          wvarResult = wvarResult + "<IVAIMPOA>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 88 + 1), 11 )) } ) + "</IVAIMPOA>";
          wvarResult = wvarResult + "<IVARETEN>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 99 + 1), 11 )) } ) + "</IVARETEN>";
          wvarResult = wvarResult + "<DEREMI>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 110 + 1), 11 )) } ) + "</DEREMI>";
          wvarResult = wvarResult + "<SELLADO>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 121 + 1), 11 )) } ) + "</SELLADO>";
          wvarResult = wvarResult + "<INGBRU>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 132 + 1), 11 )) } ) + "</INGBRU>";
          wvarResult = wvarResult + "<IMPUES>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 143 + 1), 11 )) } ) + "</IMPUES>";
          wvarResult = wvarResult + "<PRECIO>" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 154 + 1), 11 )) } ) + "</PRECIO>";
          wvarResult = wvarResult + "</SIN_HIJOS>";
          wvarResult = wvarResult + "</PLAN>";
          //
          wvarContadorPlan = wvarContadorPlan + 1;
          //
          //Seleccionar el pr�ximo plan (si es cero cortar el ciclo)
          wvarAuxPlanCicle = Strings.trim( Strings.mid( strParseString, mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 0 + 1, 5 ) );
        }
        wvarResult = wvarResult + "</PLANES>";
        //~~~~~~~~~~
        //Fin Planes
        //~~~~~~~~~~
        //
        //~~~~~~~~~~~~~~~~~
        //Inicio Coberturas
        //~~~~~~~~~~~~~~~~~
        wvarStep = 220;
        //Coberturas solo si le pasa un plan
        if( !wvarDatosPlan.equals( "00000" ) )
        {
          //Coberturas
          //wvarResult = wvarResult & "<COBERTURAS>"
          //
          wvarAuxCobeCicle = "1";
          wvarContadorCobs = 0;
          //Todos las coberturas
          //Val(wvarAuxCobeCicle) > 0 And
          while( wvarContadorCobs < 30 )
          {
            //Cobertura
            //wvarResult = wvarResult & "<COBERTURA>"
            wvarResult = wvarResult + "<COBERCOD" + (wvarContadorCobs + 1) + ">" + Strings.trim( Strings.mid( strParseString, (mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 0 + 1), 3 ) ) + "</COBERCOD" + (wvarContadorCobs + 1) + ">";
            wvarResult = wvarResult + "<COBERORD" + (wvarContadorCobs + 1) + ">" + Strings.trim( Strings.mid( strParseString, (mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 3 + 1), 2 ) ) + "</COBERORD" + (wvarContadorCobs + 1) + ">";
            wvarResult = wvarResult + "<CAPITASG" + (wvarContadorCobs + 1) + ">" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 5 + 1), 15 )) } ) + "</CAPITASG" + (wvarContadorCobs + 1) + ">";
            wvarResult = wvarResult + "<CAPITIMP" + (wvarContadorCobs + 1) + ">" + invoke( "FormatoDec", new Variant[] { new Variant(Strings.mid( strParseString, (mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 20 + 1), 15 )) } ) + "</CAPITIMP" + (wvarContadorCobs + 1) + ">";
            //wvarResult = wvarResult & "</COBERTURA>"
            //
            wvarContadorCobs = wvarContadorCobs + 1;
            //
            //Seleccionar la pr�xima cobertura (si es cero cortar el ciclo)
            wvarAuxCobeCicle = Strings.trim( Strings.mid( strParseString, mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 0 + 1, 3 ) );
          }
          //wvarResult = wvarResult & "</COBERTURAS>"
        }
        //~~~~~~~~~~~~~~
        //Fin Coberturas
        //~~~~~~~~~~~~~~
        //
        wvarStep = 230;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      //~~~~~~~~~~~~~~~~~~~~~~~~
      //Fin Response del Mensaje
      //~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 240;
      /*NOP mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (XmlDomExtended) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
    	//Agregado RGM
    	_e_.printStackTrace();
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarMensaje + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*NOP mobjCOM_Context.SetAbort() */;
        //DONE Resume ClearObjects
        Err.clear();
        // LIBERO LOS OBJETOS
        wobjXMLConfig = (XmlDomExtended) null;
        return IAction_Execute;

      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  protected String Formateo_Telefono( String pvarTelefono ) throws Exception
  {
    String Formateo_Telefono = "";
    String wvarNewTelef = "";
    //
    if( Strings.trim( Strings.left( pvarTelefono, 4 ) ).equals( "0800" ) )
    {
      wvarNewTelef = Strings.trim( Strings.left( pvarTelefono, 4 ) ) + "-" + Strings.mid( pvarTelefono, 5, 3 ) + "-" + Strings.mid( pvarTelefono, 8, Strings.len( pvarTelefono ) );
    }
    else
    {
      wvarNewTelef = pvarTelefono;
    }
    //
    Formateo_Telefono = wvarNewTelef;
    return Formateo_Telefono;
  }

  protected void ObjectControl_Activate() throws Exception
  {
  }

  protected boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  protected void ObjectControl_Deactivate() throws Exception
  {
  }

  protected String FormatoDec( String pvarNumero ) throws Exception
  {
    String FormatoDec = "";
    FormatoDec = VB.val( Strings.left( pvarNumero, (Strings.len( pvarNumero ) - 2) ) ) + "," + Strings.right( pvarNumero, 2 );
    return FormatoDec;
  }

  protected String CargarArchivoPrueba() throws Exception
  {
    String CargarArchivoPrueba = "";
    String w_strLinea = "";
    String w_strCadena = "";
    //
    FileSystem.openInput( System.getProperty("user.dir") + "\\Mensaje047.txt", 1 );
    w_strCadena = "";
    w_strLinea = "";
    while( ! (FileSystem.isEOF( 1 )) )
    {
      w_strLinea = FileSystem.in(1).inputLine().toString();
      w_strCadena = w_strCadena + w_strLinea;
    }
    FileSystem.close( 1 );

    CargarArchivoPrueba = w_strCadena;
    return CargarArchivoPrueba;
  }

  protected void GuardarLog( String p_strCadena ) throws Exception
  {
    FileSystem.openOutput( System.getProperty("user.dir") + "\\Mensaje047.log", 1 );
    FileSystem.out(1).print( p_strCadena );
    FileSystem.out(1).println();
    FileSystem.close( 1 );
  }

  public void Activate() throws Exception
  {
    mobjCOM_Context = null /*FUE this.GetObjectContext() */;
    mobjEventLog = new EventLog();
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    ObjectControl_CanBePooled = true;
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
  }
}
