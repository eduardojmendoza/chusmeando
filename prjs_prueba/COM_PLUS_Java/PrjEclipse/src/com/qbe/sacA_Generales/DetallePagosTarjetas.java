import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * 
 */

public class DetallePagosTarjetas implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  static final String mcteClassName = "sacA_Generales.DetallePagosTarjetas";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   */
  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant xERR_UNEXPECTED = new Variant();
    Variant vbLogEventTypeError = new Variant();
    Variant xERR_TIMEOUT_RETURN = new Variant();
    Variant mCte_TimeOutError = new Variant();
    Variant mCte_BRDB = new Variant();
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLrequest = null;
    String wvarSesion = "";
    String wvarFecha = "";
    String wvarTipo = "";
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstOperacion = null;
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //
      wvarStep = 10;
      wobjXMLrequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLrequest.async = false;
      wobjXMLrequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      wvarSesion = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLrequest.selectSingleNode( "//GENERAL/SESION" ) */ );
      wvarFecha = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLrequest.selectSingleNode( "//GENERAL/FECHA" ) */ );
      wvarTipo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLrequest.selectSingleNode( "//GENERAL/TIPO" ) */ );
      //
      wvarStep = 30;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //
      wvarStep = 40;
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_BRDB)
      wobjDBCnn.execute( "SET NOCOUNT ON" );
      //
      wvarStep = 50;
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( "sp_detalleoperacionesclientes" );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 60;
      wobjDBParm = new Parameter();
      wobjDBParm.setName( "@Sesion" );
      wobjDBParm.setDirection( AdoConst.adParamInput );
      wobjDBParm.setType( AdoConst.adInteger );
      wobjDBParm.setValue( new Variant( VB.val( wvarSesion ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      //
      wvarStep = 70;
      wobjDBParm = new Parameter();
      wobjDBParm.setName( "@FechaReg" );
      wobjDBParm.setDirection( AdoConst.adParamInput );
      wobjDBParm.setType( AdoConst.adNumeric );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBParm.setValue( new Variant( wvarFecha ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      //
      wvarStep = 80;
      wobjDBParm = new Parameter();
      wobjDBParm.setName( "@TipoOperacion" );
      wobjDBParm.setDirection( AdoConst.adParamInput );
      wobjDBParm.setType( AdoConst.adVarChar );
      wobjDBParm.setSize( 16 );
      wobjDBParm.setValue( new Variant( wvarTipo ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      //
      wvarStep = 90;
      wrstOperacion = wobjDBCmd.execute();
      wrstOperacion.setActiveConnection( (Connection) null );
      //
      wvarStep = 100;
      pvarResponse.set( "<GENERAL>" );
      //
      if( ! (wrstOperacion.isEOF()) )
      {
        //
        pvarResponse.set( pvarResponse + "<ESTADO RESULTADO='TRUE' MENSAJE='' />" );
        pvarResponse.set( pvarResponse + "<DETALLES>" );
        do
        {
          wvarStep = 110;
          pvarResponse.set( pvarResponse + "<DETALLEOPERACION " + "NUMREFERENCIA='" + wrstOperacion.getFields().getField("NumReferencia").getValue() + "' " + "FECHA='" + invoke( "DateToString", new Variant[] { new Variant(wrstOperacion.getFields().getField("Fecha").getValue()) } ) + "' " + "TIPOTARJETA='" + wrstOperacion.getFields().getField("TipoTarjeta").getValue() + "' " + "CUENTATARJETA='" + wrstOperacion.getFields().getField("CuentaTarjeta").getValue() + "' " + "TIPOCUENTADEBITO='" + wrstOperacion.getFields().getField("TipoCuentaDebito").getValue() + "' " + "CUENTADEBITO='" + wrstOperacion.getFields().getField("CuentaDebito").getValue() + "' " + "MONTOADEBITAR='" + Strings.format( wrstOperacion.getFields().getField("MontoADebitar").getValue(), ".00" ) + "' " + "COTIZACIONCOMPRA='" + Strings.format( wrstOperacion.getFields().getField("CotizacionCompra").getValue(), ".00" ) + "' " + "COTIZACIONVENTA='" + Strings.format( wrstOperacion.getFields().getField("CotizacionVenta").getValue(), ".00" ) + "' " + "OPCIONFECHAPAGO='" + wrstOperacion.getFields().getField("OpcionFechaPago").getValue() + "' " + "FECHAPAGO='" + invoke( "DateToString", new Variant[] { new Variant(wrstOperacion.getFields().getField("FechaPago").getValue()) } ) + "' " + "ESTADO='" + wrstOperacion.getFields().getField("Estado").getValue() + "' " + "OPERADORA='" + wrstOperacion.getFields().getField("Operadora").getValue() + "' " + "SUPERVISORA='" + wrstOperacion.getFields().getField("Supervisora").getValue() + "' " + "FECHAREVERSA='" + wrstOperacion.getFields().getField("FechaReversa").getValue() + "' />" );
          wrstOperacion.moveNext();
          //
        }
        while( ! (wrstOperacion.isEOF()) );
        pvarResponse.set( pvarResponse + "</DETALLES>" );
      }
      else
      {
        pvarResponse.set( pvarResponse + "<ESTADO RESULTADO='FALSE' MENSAJE='No se ha encontrado el detalle de la operación' />" );
      }
      //
      pvarResponse.set( pvarResponse + "</GENERAL>" );
      //
      wvarStep = 120;
      wobjDBCmd = (Command) null;
      wobjDBCnn.close();
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 130;
      if( 0 /*unsup wrstOperacion.State */ != 0 )
      {
        wrstOperacion.close();
      }
      //
      wvarStep = 140;
      wrstOperacion = (Recordset) null;
      //
      wvarStep = 150;
      /*unsup mobjCOM_Context.SetComplete() */;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        if( Err.getError().getNumber() == mCte_TimeOutError.toInt() )
        {
          pvarResponse.set( new Variant() );
          IAction_Execute = xERR_TIMEOUT_RETURN.toInt();
          //error: function 'Log' was not found.
          //unsup: vbLogEventTypeError
        }
        else
        {
          //error: function 'Log' was not found.
          //unsup: vbLogEventTypeError
          IAction_Execute = xERR_UNEXPECTED.toInt();
        }
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  /**
   * 
   * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
