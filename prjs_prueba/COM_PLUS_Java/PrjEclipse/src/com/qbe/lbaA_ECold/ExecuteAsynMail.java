package com.qbe.lbaA_ECold;

import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;

import com.snoopconsulting.qbe.VBObjectClass;
import com.snoopconsulting.qbe.XmlDomExtended;

import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class ExecuteAsynMail extends VBObjectClass
{
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "lbaA_ECold.ExecuteAsynMail";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    Variant gcteParamAsyncExec = new Variant();
    Variant gcteParamFilesDir = new Variant();
    int wvarstep = 0;
    String wvarRequest = "";
    String wvarFileName = "";
    diamondedge.util.XmlDom wobjXMLRequest = null;


    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarstep = 10;

      //wvarRequest = "<Request>" & Request & "</Request>"
      wvarRequest = Request;
      //
      // CREO EL DIRECTORIO DE GENFILES SI NO EXISTE
      if( "" /*unsup this.Dir( (System.getProperty("user.dir") + gcteParamFilesDir), Constants.vbDirectory ) */.equals( "" ) )
      {
        FileSystem.mkDir( System.getProperty("user.dir") + gcteParamFilesDir );
      }
      //
      wvarstep = 20;
      wobjXMLRequest = new XmlDomExtended();
      //
      //unsup wobjXMLRequest.async = false;
      wvarstep = 30;
      wobjXMLRequest.loadXML( wvarRequest );
      VB.randomize( DateTime.toInt( DateTime.now() ) );
      wvarFileName = "Request" + String.valueOf( (int)Math.rint( (((99999999 - 1) + 1) * VB.rnd()) + 1 ) ) + ".xml";
      wvarstep = 40;
      wobjXMLRequest.save( System.getProperty("user.dir") + gcteParamFilesDir + wvarFileName );
      //
      wvarstep = 50;
      Runtime.getRuntime().exec( "\"" + System.getProperty("user.dir") + "\\" + gcteParamAsyncExec + "\"" + " " + wvarFileName );

      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"/></Response>" );

      IAction_Execute = 0;
      /*unsup mobjCOM_Context.SetComplete() */;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - ", vbLogEventTypeError );

        Response.set( "<Response><Estado resultado=\"False\" mensaje=\"No se pudo\"/></Response>" );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
