package com.qbe.lbawA_OVMQIII;

import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;

import com.snoopconsulting.qbe.VBObjectClass;

import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVIIIGetProvincias extends VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQIII.lbaw_OVIIIGetProvincias";
  static final String mcteOpID = "1531";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_Codigo_Zona = "//ZONA";
  static final String mcteParam_Tipo_Agrupacion = "//AGRUPTIP";
  static final String mcteParam_Codigo_Agrupacion = "//AGRUPCOD";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    Variant gcteClassMQConnection = new Variant();
    Variant gcteCIAASCOD = new Variant();
    Variant gcteNodosGenerales = new Variant();
    Variant gcteParamFileName = new Variant();
    Variant gcteConfFileName = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarTipoAgrupacion = "";
    String wvarCodagrupacion = "";
    String wvarCodZona = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //Deberá venir desde la página
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarTipoAgrupacion = Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Tipo_Agrupacion ) */ ), 4 );
      wvarCodagrupacion = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Agrupacion ) */ ), 2 );
      wvarCodZona = Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Zona ) */ ), 4 );
      //
      //
      wvarStep = 60;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + gcteConfFileName );
      //
      //Levanto los parámetros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( String.valueOf( gcteNodosGenerales ) + gcteCIAASCOD ) */ );
      //
      wvarStep = 110;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarTipoAgrupacion + wvarCodagrupacion + wvarCodZona;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 240;
      //
      parsear: 
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = "";
        //cantidad de caracteres ocupados por parámetros de entrada
        wvarPos = 77;
        wvarResult = invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseString), new Variant(wvarstrLen) } );
        //
        wvarStep = 340;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
        //
      }
      //
      wvarStep = 360;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarTipoAgrupacion + wvarCodagrupacion + wvarCodZona + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int pvarPos, String pstrParseString, int pvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    VbScript.RegExp wobjRegExp = new VbScript.RegExp();
    void wobjColMatch;
    Object wobjMatch = null;
    String wvarParseEvalString = "";
    diamondedge.util.XmlDom wobjXmlDOMResp = null;
    diamondedge.util.XmlDom wobjXslDOMResp = null;
    org.w3c.dom.Element wobjXmlElemenResp = null;
    org.w3c.dom.Element wobjXmlElemenRespE = null;
    int wvarCantRegistros = 0;
    int wvarCcurrRegistro = 0;
    //
    //RegExp
    //MatchCollection
    //Match
    //
    //
    wobjRegExp = new VbScript.RegExp();
    wobjRegExp.Global.set( true );
    //
    wobjRegExp.Pattern.set( "(\\S{2})(\\S{2})(\\S{20})" );
    //
    wvarParseEvalString = Strings.mid( pstrParseString, pvarPos + 3 );
    wvarParseEvalString = Strings.replace( wvarParseEvalString, " ", "_" );
    wvarCantRegistros = (int)Math.rint( VB.val( Strings.mid( pstrParseString, pvarPos, 3 ) ) );
    wvarCcurrRegistro = 0;

    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    wobjXmlDOMResp = new diamondedge.util.XmlDom();
    wobjXslDOMResp = new diamondedge.util.XmlDom();

    wobjXmlDOMResp.loadXML( "<RS></RS>" );
    //
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //
      if( ! (wvarCcurrRegistro < wvarCantRegistros) )
      {
        break;
      }
      wvarCcurrRegistro = wvarCcurrRegistro + 1;
      //
      wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "PROVINCIA" );
      /*unsup wobjXmlDOMResp.selectSingleNode( "//RS" ) */.appendChild( wobjXmlElemenResp );
      //
      wobjXmlElemenRespE = wobjXmlDOMResp.getDocument().createElement( "CODIGOPAIS" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespE );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlElemenRespE.Text = UCase(Trim(Replace(wobjMatch.SubMatches(0), "_", " ")))
      //
      wobjXmlElemenRespE = wobjXmlDOMResp.getDocument().createElement( "CODIGOPROVINCIA" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespE );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlElemenRespE.Text = Val(Replace(wobjMatch.SubMatches(1), "_", " "))
      //
      wobjXmlElemenRespE = wobjXmlDOMResp.getDocument().createElement( "DESCRIPCION" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespE );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlElemenRespE.Text = Trim(Replace(wobjMatch.SubMatches(2), "_", " "))
      //
    }
    //
    //unsup wobjXslDOMResp.async = false;
    wobjXslDOMResp.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
    ParseoMensaje = Strings.replace( new Variant() /*unsup wobjXmlDOMResp.transformNode( wobjXslDOMResp ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
    //ParseoMensaje = wobjXmlDOMResp.xml
    //
    wobjXmlDOMResp = (diamondedge.util.XmlDom) null;
    wobjXmlElemenResp = (org.w3c.dom.Element) null;
    wobjXmlElemenRespE = (org.w3c.dom.Element) null;
    wobjXslDOMResp = (diamondedge.util.XmlDom) null;
    //
    wobjRegExp = (VbScript.RegExp) null;
    //
    return ParseoMensaje;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<?xml version='1.0' encoding='UTF-8'?>";
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>";
    wvarStrXSL = wvarStrXSL + "<xsl:output method='html' version='1.0' encoding='UTF-8' indent='yes'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:template match='/' >";
    wvarStrXSL = wvarStrXSL + "<xsl:for-each select='//PROVINCIA'>";
    wvarStrXSL = wvarStrXSL + "     <option>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='value'><xsl:value-of select='./CODIGOPROVINCIA'/></xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='codigopais'><xsl:value-of select='./CODIGOPAIS'/></xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='descripcion'><xsl:value-of select='./DESCRIPCION'/></xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "     <xsl:value-of select='./DESCRIPCION'/>";
    wvarStrXSL = wvarStrXSL + "     </option>";
    wvarStrXSL = wvarStrXSL + "     </xsl:for-each>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template >";
    wvarStrXSL = wvarStrXSL + "     </xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    //
    return p_GetXSL;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
