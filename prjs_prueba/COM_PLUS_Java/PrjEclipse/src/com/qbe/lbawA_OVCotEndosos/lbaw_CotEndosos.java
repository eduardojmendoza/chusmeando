package com.qbe.lbawA_OVCotEndosos;

import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;

import com.snoopconsulting.qbe.VBObjectClass;

import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_CotEndosos  extends VBObjectClass
{
  /**
   * Implementacion de los objetos
   * ************************************************************************************
   * ************************************************************************************
   * ****                   RUTA DE LOS ARCHIVOS DE DEFINICION                       ****
   * ****         \\NTFSAPP01DES\COMP\LBAOVCONSULTAS\DEFINICIONESCOTIZADOR           ****
   * ************************************************************************************
   * ************************************************************************************
   * ************************************************************************************
   * DATOS PROPIOS DE CADA CLASE, TODO LO DEMAS SE PUEDE COPIAR SIN PROBLEMAS A UNA NUEVA CLASE
   * PARA DIVIDIR LA CARGA DEL COMPONENTE Y QUE NO SE INSTANCIE SIEMPRE LA MISMA CLASE
   * ************************************************************************************
   */
  static final String mcteClassName = "lbawA_OVCotEndosos.lbaw_CotEndosos";
  static final String mcteSubDirName = "DefinicionesCotizadorEndosos";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private String mvarConsultaRealizada = "";
  private boolean mvarCancelacionManual = false;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * ************************************************************************************
   * *************************************************************
   * DE ACA PARA ABAJO SE PUEDE COPIAR TODO A CUALQUIER OTRA CLASE
   * *************************************************************
   */
  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeWarning = new Variant();
    Variant vbLogEventTypeError = new Variant();
    Variant gcteClassMQConnection = new Variant();
    Variant gcteGMOWaitInterval = new Variant();
    String wvarOpID = "";
    diamondedge.util.XmlDom wobjXMLPrueba = null;
    diamondedge.util.XmlDom wobjXMLDefinition = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLLog = null;
    diamondedge.util.XmlDom wobjXMLOldLog = null;
    diamondedge.util.XmlDom wobjXMLRespuesta = null;
    org.w3c.dom.NodeList wobjNodosEntrada = null;
    org.w3c.dom.Node wobjNodoEntrada = null;
    org.w3c.dom.Element wobjNewNodo = null;
    Object wobjFrame2MQ = null;
    int wvarGMOWaitInterval = 0;
    int wvarStep = 0;
    String wvarResult = "";
    java.util.Date wvarFechaConsulta = DateTime.EmptyDate;
    String wvarArea = "";
    String wvarLastArea = "";
    int wvarPos = 0;
    int wvarMaxLen = 0;
    String strParseString = "";
    String strParseStringTemp = "";
    String wvarstrXMLEnvioArea = "";
    String wvarEstado = "";
    String wvarCodigoError = "";
    String wvarConfFileName = "";
    String wvarDefinitionFile = "";
    String wvarXMLAreas = "";
    String wvarErrorValidacion = "";
    String wvarError = "";
    String wvarRequestRetorno = "";
    String wvarPathLog = "";
    boolean wvarAppendRequest = false;
    boolean wvarConsultarMQ = false;
    int wvarCantRellamRest = 0;
    String wvarXMLHarcodeado = "";
    String wvarAreaHarcodeada = "";
    int wvarCantLlamadasALoguear = 0;
    int wvarCounter = 0;
    int wvarCantLlamadasLogueadas = 0;
    String wvarFiltroEstado = "";
    String wvarDescripcion = "";
    int wvarMQError = 0;
    //
    //
    //
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarFechaConsulta = DateTime.now();
      //
      // ***************************************************************************************************
      //Levanto los XMLs de Configuracion del area MQ y del Request de la Paguna
      // ***************************************************************************************************
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      wobjXMLDefinition = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      wvarDefinitionFile = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//DEFINICION" ) */ );
      mvarConsultaRealizada = Strings.left( wvarDefinitionFile, Strings.len( wvarDefinitionFile ) - 4 );
      //unsup wobjXMLDefinition.async = false;
      wobjXMLDefinition.load( System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + wvarDefinitionFile );
      //
      wvarStep = 30;
      wvarOpID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/MENSAJE" ) */ );
      //
      wvarStep = 31;
      wvarConfFileName = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//MQCONFIGFILE" ) */ );
      //
      wvarStep = 32;
      wvarGMOWaitInterval = Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//TIMEOUT" ) */ ) );
      //
      // ***************************************************************************************************
      //Recorro los campos del XML y voy armando el area MQ segun los parámetros que llegan desde la página
      // ***************************************************************************************************
      // *********************************************************************************************************************************
      //NOTA IMPORTANTISIMA: Este componente solo permite un nivel para los vectores, o sea que no funciona con un vector adentro de otro
      // *********************************************************************************************************************************
      wvarStep = 40;
      wobjNodosEntrada = null /*unsup wobjXMLDefinition.selectNodes( "//ENTRADA/CAMPO" ) */;
      wvarArea = "";
      for( int nwobjNodoEntrada = 0; nwobjNodoEntrada < wobjNodosEntrada.getLength(); nwobjNodoEntrada++ )
      {
        wobjNodoEntrada = wobjNodosEntrada.item( nwobjNodoEntrada );
        if( wobjNodoEntrada.getChildNodes().getLength() == 0 )
        {
          //Es un Dato Normal
          wvarArea = wvarArea + invoke( "GetDatoFormateado", new Variant[] { new Variant(wobjXMLRequest.getDocument().getChildNodes().item( 0 )), new Variant("//" + diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( new Variant("Nombre") ) )), new Variant(diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( new Variant("TipoDato") ) )), new Variant(wobjNodoEntrada.getAttributes().getNamedItem( new Variant("Enteros") )), new Variant(wobjNodoEntrada.getAttributes().getNamedItem( new Variant("Decimales") )), new Variant(wobjNodoEntrada.getAttributes().getNamedItem( new Variant("Default") )), new Variant(wobjNodoEntrada.getAttributes().getNamedItem( new Variant("Obligatorio") )) } );
        }
        else
        {
          //Es un Vector
          wvarArea = wvarArea + invoke( "GetVectorDatos", new Variant[] { new Variant(wobjXMLRequest.getDocument().getChildNodes().item( 0 )), new Variant("//" + diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( new Variant("Nombre") ) )), new Variant(wobjNodoEntrada.getChildNodes()), new Variant((int)Math.rint( VB.val( new Variant(diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( new Variant("Cantidad") ) )) ) )) } );
        }
      }
      //
      wvarStep = 50;
      //
      // ***********************************************************************
      //Se puede utilizar un Area IN / OUT preestablecidas para hacer pruebas
      // ***********************************************************************
      //
      wvarStep = 55;
      if( ! ((null /*unsup wobjXMLRequest.selectSingleNode( "//AREAOUTMQ" ) */ == (org.w3c.dom.Node) null)) || ! ((null /*unsup wobjXMLDefinition.selectSingleNode( "//AREAINMQ" ) */ == (org.w3c.dom.Node) null)) )
      {
        if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//AREAOUTMQ" ) */ == (org.w3c.dom.Node) null) )
        {
          //Desestimo el Nro de Mensaje porque se agrega despues
          wvarArea = Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//AREAOUTMQ" ) */ ), 5 );
        }
        else
        {
          //Desestimo el Nro de Mensaje porque se agrega despues
          wvarArea = Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//AREAOUTMQ" ) */ ), 5 );
        }
      }
      //
      wvarStep = 55;
      if( ! ((null /*unsup wobjXMLRequest.selectSingleNode( "//AREAINMQ" ) */ == (org.w3c.dom.Node) null)) || ! ((null /*unsup wobjXMLDefinition.selectSingleNode( "//AREAINMQ" ) */ == (org.w3c.dom.Node) null)) )
      {
        if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//AREAINMQ" ) */ == (org.w3c.dom.Node) null) )
        {
          //Desestimo el Nro de Mensaje porque se agrega despues
          wvarAreaHarcodeada = Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//AREAINMQ" ) */ ), 5 );
        }
        else
        {
          //Desestimo el Nro de Mensaje porque se agrega despues
          wvarAreaHarcodeada = Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//AREAINMQ" ) */ ), 5 );
        }
      }
      //
      wvarPos = Strings.len( wvarOpID ) + Strings.len( wvarArea ) + 1;
      wvarXMLAreas = "<AreaIN>" + wvarOpID + wvarArea + "</AreaIN>";
      //
      // *****************************************************************
      //Si solo se solicita el Area MQ la devuelvo y salgo del componente
      // *****************************************************************
      //
      wvarStep = 60;
      if( ! ((null /*unsup wobjXMLRequest.selectSingleNode( "//SOLORETORNARAREA" ) */ == (org.w3c.dom.Node) null)) || ! ((null /*unsup wobjXMLDefinition.selectSingleNode( "//SOLORETORNARAREA" ) */ == (org.w3c.dom.Node) null)) )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "No se Procesa por la existencia del TAG SOLORETORNARAREA" + String.valueOf( (char)(34) ) + " />" + wvarXMLAreas + "</Response>" );
        //unsup GoTo Salir
      }
      //
      // ***********************************************************************
      //Se puede utilizar un XML de respuesta preestablecido para hacer pruebas
      // ***********************************************************************
      //
      wvarStep = 70;
      wvarConsultarMQ = true;
      if( ! ((null /*unsup wobjXMLRequest.selectSingleNode( "//XMLPRUEBA" ) */ == (org.w3c.dom.Node) null)) || ! ((null /*unsup wobjXMLDefinition.selectSingleNode( "//XMLPRUEBA" ) */ == (org.w3c.dom.Node) null)) )
      {
        wvarConsultarMQ = false;
        wobjXMLPrueba = new diamondedge.util.XmlDom();
        if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//XMLPRUEBA" ) */ == (org.w3c.dom.Node) null) )
        {
          wobjXMLPrueba.load( System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + diamondedge.util.XmlDom.getText( /*unsup wobjXMLRequest.selectSingleNode( "//XMLPRUEBA" ) */ ) );
        }
        else
        {
          wobjXMLPrueba.load( System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + diamondedge.util.XmlDom.getText( /*unsup wobjXMLDefinition.selectSingleNode( "//XMLPRUEBA" ) */ ) );
        }
        wvarXMLHarcodeado = wobjXMLPrueba.getDocument().getDocumentElement().toString();
        wobjXMLPrueba = (diamondedge.util.XmlDom) null;
      }
      //
      wvarStep = 75;
      //
      // ***********************************************************************
      //Se puede utilizar un area preestablecida de Respuesta para hacer pruebas
      // ***********************************************************************
      //
      wvarCantRellamRest = 1;
      if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//PAGINASRETORNADAS" ) */ == (org.w3c.dom.Node) null) )
      {
        wvarCantRellamRest = Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//PAGINASRETORNADAS" ) */ ) );
      }
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//PAGINASRETORNADAS" ) */ == (org.w3c.dom.Node) null) )
      {
        wvarCantRellamRest = Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//PAGINASRETORNADAS" ) */ ) );
      }
      //
      if( wvarConsultarMQ )
      {
        //
        // ******************************************************************************************
        //Levanto los datos de la cola de MQ del archivo de configuración para actualizar el TimeOut
        // ******************************************************************************************
        //
        wvarStep = 80;
        //
        wobjXMLConfig = new diamondedge.util.XmlDom();
        //unsup wobjXMLConfig.async = false;
        wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + wvarConfFileName );
        diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( gcteGMOWaitInterval.toString() ) */, String.valueOf( wvarGMOWaitInterval ) );
        //
        wvarStep = 90;
        //
        // ***************************************************
        //Componente que realiza la conexion MQ
        // ***************************************************
        //
        RealizarRellamado: 
        //
        if( wvarAreaHarcodeada.equals( "" ) )
        {
          wobjFrame2MQ = new gcteClassMQConnection();
          //error: function 'Execute' was not found.
          //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarOpID & wvarArea, strParseString, wobjXMLConfig.xml)
          wobjFrame2MQ = null;
        }
        else
        {
          strParseString = wvarAreaHarcodeada;
          wvarMQError = 0;
        }
        //
        wvarStep = 100;
        //
        //
        if( wvarMQError != 0 )
        {
          Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
          mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Mensaje:" + wvarOpID + " Area:" + wvarOpID + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
          //unsup GoTo Terminar
        }
        //
        wvarStep = 120;
        //
        strParseString = wvarOpID + strParseString;
        //
        wvarXMLAreas = wvarXMLAreas + "<AreaOUT>" + strParseString + "</AreaOUT>";
        //
        // ****************************************************************
        //Agregamos las Areas en el XML de respuesta si es que solicitaron
        // ****************************************************************
        //
        if( ! ((null /*unsup wobjXMLRequest.selectSingleNode( "//RETORNARAREAS" ) */ == (org.w3c.dom.Node) null)) || ! ((null /*unsup wobjXMLDefinition.selectSingleNode( "//RETORNARAREAS" ) */ == (org.w3c.dom.Node) null)) )
        {
          wvarstrXMLEnvioArea = "<AreasMQ>" + wvarXMLAreas + "</AreasMQ>";
        }
        else
        {
          wvarstrXMLEnvioArea = "";
        }
        //
        wvarStep = 245;
        //
        // ****************************************************************
        //Agregamos en el XML de respuesta el Request si es que lo Solicitaron
        // ****************************************************************
        //
        if( ! ((null /*unsup wobjXMLRequest.selectSingleNode( "//RETORNARREQUEST" ) */ == (org.w3c.dom.Node) null)) || ! ((null /*unsup wobjXMLDefinition.selectSingleNode( "//RETORNARREQUEST" ) */ == (org.w3c.dom.Node) null)) )
        {
          wvarRequestRetorno = Request;
          wvarAppendRequest = true;
        }
        else
        {
          wvarRequestRetorno = "";
          wvarAppendRequest = false;
        }
      }
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //Corto el Codigo de Error
      wvarCodigoError = Strings.mid( strParseString, 21, 2 );
      //
      // ************************************************************
      //Cortamos el Area en caso que se determine el maximo del area
      // ************************************************************
      if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//SALIDA" ) */.getAttributes().getNamedItem( "MaxLen" ) == (org.w3c.dom.Node) null) )
      {
        wvarMaxLen = Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//SALIDA" ) */.getAttributes().getNamedItem( "MaxLen" ) ) );
        strParseString = Strings.mid( strParseString, 1, (wvarMaxLen + wvarPos) - 1 );
      }
      //
      // ***********************************************************************
      //Vamos Armando el Area en el String Temporal para agregar los rellamados
      // ***********************************************************************
      if( strParseStringTemp.equals( "" ) )
      {
        strParseStringTemp = strParseString;
      }
      else
      {
        strParseStringTemp = strParseStringTemp + Strings.mid( strParseString, wvarPos );
      }
      //
      if( (wvarEstado.equals( "ER" )) || ! ((null /*unsup wobjXMLDefinition.selectSingleNode( ("//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "']") ) */ == (org.w3c.dom.Node) null)) )
      {
        //
        // **********************************************************************************************
        //En el XML De Definicion se pueden establecer Estados Errones con descripciones predeterminadas
        // **********************************************************************************************
        wvarStep = 260;
        if( null /*unsup wobjXMLDefinition.selectSingleNode( ("//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "']") ) */ == (org.w3c.dom.Node) null )
        {
          wvarDescripcion = "SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA";
        }
        else
        {
          if( (!Strings.trim( wvarCodigoError ).equals( "" )) && ! ((null /*unsup wobjXMLDefinition.selectSingleNode( ("//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "' and @CodigoError='" + wvarCodigoError + "']") ) */ == (org.w3c.dom.Node) null)) )
          {
            wvarDescripcion = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "' and @CodigoError='" + wvarCodigoError + "']" ) */.getAttributes().getNamedItem( "Descripcion" ) );
          }
          else
          {
            wvarDescripcion = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "']" ) */.getAttributes().getNamedItem( "Descripcion" ) );
          }
        }
        Response.set( "<Response><Estado resultado='false' mensaje='" + wvarDescripcion + "' />" + wvarRequestRetorno + wvarstrXMLEnvioArea + "</Response>" );
        //
      }
      else if( (wvarEstado.equals( "TR" )) && ! ((wvarCantRellamRest == 1)) )
      {
        //Area Para el Rellamado
        wvarArea = Strings.mid( strParseString, 5, wvarPos - 1 );
        wvarCantRellamRest = wvarCantRellamRest - 1;
        //unsup GoTo RealizarRellamado
      }
      else
      {
        //
        wvarStep = 280;
        //
        //Area de la ultima llamada
        wvarLastArea = Strings.mid( strParseString, 1, wvarPos - 1 );
        strParseStringTemp = wvarLastArea + Strings.mid( strParseStringTemp, wvarPos );
        //
        wvarResult = invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseStringTemp), new Variant(null /*unsup wobjXMLDefinition.selectSingleNode( new Variant("//SALIDA") ) */), new Variant(wobjXMLRequest), new Variant(wvarAppendRequest), new Variant(wvarXMLHarcodeado) } );
        //
        wvarStep = 340;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + wvarstrXMLEnvioArea + "</Response>" );
        //
        if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//SALIDA" ) */.getAttributes().getNamedItem( "xPathParaVerificarExistenciaDeDatos" ) == (org.w3c.dom.Node) null) )
        {
          // ********************************************************************************************************
          //Se verifica la existencia de datos en la Respuesta segun el atributo xPathParaVerificarExistenciaDeDatos
          // ********************************************************************************************************
          wobjXMLRespuesta = new diamondedge.util.XmlDom();
          if( wobjXMLRespuesta.loadXML( wvarResult ) )
          {
            if( null /*unsup wobjXMLRespuesta.selectNodes( diamondedge.util.XmlDom.getText( null (*unsup wobjXMLDefinition.selectSingleNode( "//SALIDA" ) *).getAttributes().getNamedItem( "xPathParaVerificarExistenciaDeDatos" ) ) ) */.getLength() == 0 )
            {
              Response.set( "<Response><Estado resultado='false' mensaje='NO SE ENCONTRARON DATOS' />" + wvarResult + wvarstrXMLEnvioArea + "</Response>" );
            }
          }
        }
        //
      }
      //
      Salir: 
      wvarStep = 350;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      Terminar: 

      try 
      {

        wvarStep = 360;
        wvarStep = 361;
        //
        // ****************************************************************
        //Generamos un Log en caso que se solicite
        // ****************************************************************
        //
        wvarCantLlamadasALoguear = 1;
        wvarFiltroEstado = "SIN_DETERMINAR";
        if( ! ((null /*unsup wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) */ == (org.w3c.dom.Node) null)) || ! ((null /*unsup wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) */ == (org.w3c.dom.Node) null)) )
        {
          //DA
          wvarStep = 362;
          wvarPathLog = System.getProperty("user.dir") + "\\Log\\" + wvarDefinitionFile;
          //
          if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) */ == (org.w3c.dom.Node) null) )
          {
            if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
            {
              wvarPathLog = System.getProperty("user.dir") + "\\Log\\" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "LogFile" ) );
            }
            if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "CantLlamadasALoguear" ) == (org.w3c.dom.Node) null) )
            {
              wvarCantLlamadasALoguear = Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "CantLlamadasALoguear" ) ) );
            }
            if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "EstadosNoLogueables" ) == (org.w3c.dom.Node) null) )
            {
              wvarFiltroEstado = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "EstadosNoLogueables" ) );
            }
          }
          else
          {
            if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
            {
              wvarPathLog = System.getProperty("user.dir") + "\\Log\\" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "LogFile" ) );
            }
            if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "CantLlamadasALoguear" ) == (org.w3c.dom.Node) null) )
            {
              wvarCantLlamadasALoguear = Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "CantLlamadasALoguear" ) ) );
            }
            if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "EstadosNoLogueables" ) == (org.w3c.dom.Node) null) )
            {
              wvarFiltroEstado = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "EstadosNoLogueables" ) );
            }
          }
          //
          //DA
          wvarStep = 363;
          if( ! (wvarPathLog.matches( "*\\" )) && ! (Obj.toBoolean( (wvarEstado.matches( "*" ) + wvarFiltroEstado + "*") )) )
          {

            //Genero el Log solo si en el nodo se especifico un nombre de archivo
            wobjXMLLog = new diamondedge.util.XmlDom();
            //unsup wobjXMLLog.async = false;
            //
            //DA
            wvarStep = 364;
            wobjXMLLog.loadXML( "<LOGs><LOG MensajeMQ='" + wvarOpID + "' InicioConsulta='" + DateTime.format( wvarFechaConsulta, "dd/MM/yyyy HH:mm:ss" ) + "' TiempoIncurrido = '" + (int)Math.rint( (DateTime.diff( wvarFechaConsulta, DateTime.now() ) * 86400) ) + " Seg' >" + Request + Response + "</LOG></LOGs>" );
            //
            //DA
            wvarStep = 365;
            wobjNewNodo = wobjXMLLog.getDocument().createElement( "AreasMQ" );
            /*unsup wobjXMLLog.selectSingleNode( "//LOG" ) */.appendChild( wobjNewNodo );
            //
            //DA
            wvarStep = 366;
            //Agrego las areas a mano porque el LoadXML Falla con strings tan grandes como el Area Out
            wobjNewNodo = wobjXMLLog.getDocument().createElement( "AreaIN" );
            /*unsup wobjXMLLog.selectSingleNode( "//LOG/AreasMQ" ) */.appendChild( wobjNewNodo );
            diamondedge.util.XmlDom.setText( wobjNewNodo, wvarOpID + wvarArea );
            //
            //DA
            wvarStep = 368;
            wobjNewNodo = wobjXMLLog.getDocument().createElement( "AreaOUT" );
            /*unsup wobjXMLLog.selectSingleNode( "//LOG/AreasMQ" ) */.appendChild( wobjNewNodo );
            diamondedge.util.XmlDom.setText( wobjNewNodo, strParseString );
            //
            //DA
            wvarStep = 369;
            if( !"" /*unsup this.Dir( wvarPathLog, 0 ) */.equals( "" ) )
            {
              //DA
              wvarStep = 370;
              //Ya existe el Log, entonces agrego la nueva consulta al LOG
              wobjXMLOldLog = new diamondedge.util.XmlDom();
              //unsup wobjXMLOldLog.async = false;
              wobjXMLOldLog.load( wvarPathLog );
              //DA
              wvarStep = 371;
              if( null /*unsup wobjXMLOldLog.selectSingleNode( "//LOGs" ) */ == (org.w3c.dom.Node) null )
              {
                // *********LOG NO VALIDO***********
                wobjXMLLog.save( wvarPathLog );
              }
              else
              {
                //DA
                wvarStep = 372;
                wvarCantLlamadasLogueadas = null /*unsup wobjXMLOldLog.selectSingleNode( "//LOGs" ) */.getChildNodes().getLength();
                for( wvarCounter = 1; wvarCounter <= ((wvarCantLlamadasLogueadas - wvarCantLlamadasALoguear) + 1); wvarCounter++ )
                {
                  /*unsup wobjXMLOldLog.selectSingleNode( "//LOGs" ) */.removeChild( null /*unsup wobjXMLOldLog.selectSingleNode( "//LOGs" ) */.getChildNodes().item( 0 ) );
                }
                //DA
                wvarStep = 373;
                /*unsup wobjXMLOldLog.selectSingleNode( "//LOGs" ) */.appendChild( null /*unsup wobjXMLLog.selectSingleNode( "//LOGs" ) */.getChildNodes().item( 0 ) );
                //DA
                wvarStep = 374;
                wobjXMLOldLog.save( wvarPathLog );
              }
            }
            else
            {
              //DA
              wvarStep = 375;
              wobjXMLLog.save( wvarPathLog );
            }
          }
        }
        //
        //~~~~~~~~~~~~~~~
        ClearObjects: 
        //~~~~~~~~~~~~~~~
        wobjXMLOldLog = (diamondedge.util.XmlDom) null;
        wobjXMLLog = (diamondedge.util.XmlDom) null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;
        wobjXMLDefinition = (diamondedge.util.XmlDom) null;
        wobjXMLRespuesta = (diamondedge.util.XmlDom) null;
        wobjNewNodo = (org.w3c.dom.Element) null;
        wobjXMLConfig = (diamondedge.util.XmlDom) null;
        return IAction_Execute;
        //
        //~~~~~~~~~~~~~~~
      }
      catch( Exception _e_ )
      {
        Err.set( _e_ );
        try 
        {
          //~~~~~~~~~~~~~~~
          //
          wvarError = Err.getError().getDescription();
          mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Warning= [" + Err.getError().getNumber() + "] - " + wvarError + " Mensaje:" + wvarOpID + " Area:" + wvarOpID + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeWarning );

          if( mvarCancelacionManual )
          {
            Response.set( "<Response><Estado resultado='false' mensaje='" + wvarError + "' /></Response>" );
          }

          /*unsup mobjCOM_Context.SetComplete() */;
          IAction_Execute = 0;

          //unsup Resume ClearObjects
          Err.clear();
          return IAction_Execute;
          //~~~~~~~~~~~~~~~
        }
        catch( Exception _e2_ )
        {
        }
      }
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wvarError = Err.getError().getDescription();
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + wvarError + " Mensaje:" + wvarOpID + " Area:" + wvarOpID + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );

        if( mvarCancelacionManual )
        {
          Response.set( "<Response><Estado resultado='false' mensaje='" + wvarError + "' /></Response>" );
          /*unsup mobjCOM_Context.SetComplete() */;
          IAction_Execute = 0;
        }
        else
        {
          /*unsup mobjCOM_Context.SetAbort() */;
          IAction_Execute = 1;
        }
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int pvarPos, String pstrParseString, org.w3c.dom.Node pobjNodoDefiniciones, diamondedge.util.XmlDom pobjXMLRequest, boolean pvarAppendRequest, String pvarXMLHarcodeado ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarEvalString = "";
    diamondedge.util.XmlDom wobjXslDOMResp = null;
    diamondedge.util.XmlDom wobjXmlDOMResp = null;
    diamondedge.util.XmlDom wobjXMLRequestTemp = null;
    String wvarXSLFile = "";
    org.w3c.dom.Node wobjNodoXSL = null;
    org.w3c.dom.Node wobjNodoParseo = null;
    String wvarNuevoXML = "";

    wobjXmlDOMResp = new diamondedge.util.XmlDom();
    if( pvarXMLHarcodeado.equals( "" ) )
    {
      // *****************************************************************************
      //Envio el Area Recibida junto con el primer Pattern para que se genere el XML
      // *****************************************************************************
      wvarEvalString = Strings.mid( pstrParseString, pvarPos );
      //En la primera llamada determino si es un unico registro o un vector
      if( pobjNodoDefiniciones.getChildNodes().item( 0 ).getChildNodes().getLength() > 1 )
      {
        //Es un campo compuesto, se entiende que no es un vector
        wobjNodoParseo = invoke( "GetNodoParseado", new Variant[] { new Variant(wvarEvalString), new Variant(pobjNodoDefiniciones.getChildNodes().item( 0 )), new Variant(1) } );
      }
      else
      {
        //Tiene definido el largo de un solo campo, se entiene que es un vector
        wobjNodoParseo = invoke( "GetNodoParseado", new Variant[] { new Variant(wvarEvalString), new Variant(pobjNodoDefiniciones.getChildNodes().item( 0 )), new Variant(-1) } );
      }
      if( ! (wobjNodoParseo == (org.w3c.dom.Node) null) )
      {
        wobjXmlDOMResp.loadXML( Strings.replace( Strings.replace( wobjNodoParseo.toString(), "&gt;", ">" ), "&lt;", "<" ) );
      }
    }
    else
    {
      // *****************************************************************************
      //Genero el XML con lo harcodeado
      // *****************************************************************************
      wobjXmlDOMResp.loadXML( pvarXMLHarcodeado );
    }
    //
    //Quiere decir que levanto un XML Valido
    if( !wobjXmlDOMResp.getDocument().getDocumentElement().toString().equals( "" ) )
    {
      //
      // *************************************************************************************
      //Se agrega el Request en el area de respuesta en caso que haya sido solicitado
      //Esto sirve para utilizarlo si se aplica un XSL que necesite algun dato de la llamada
      // *************************************************************************************
      if( pvarAppendRequest )
      {
        wobjXMLRequestTemp = new diamondedge.util.XmlDom();
        wobjXMLRequestTemp.loadXML( pobjXMLRequest.getDocument().getDocumentElement().toString() );
        wobjXmlDOMResp.getDocument().getChildNodes().item( 0 ).appendChild( ((org.w3c.dom.Node) invoke( "GetRequestRetornado", new Variant[] { new Variant(wobjXMLRequestTemp), new Variant(/*unsup pobjNodoDefiniciones.selectSingleNode( new Variant("//ENTRADA") ) */), new Variant(Strings.mid( new Variant(pstrParseString), new Variant(1), new Variant(pvarPos) )) } ).toObject()) );
        wobjXMLRequestTemp = (diamondedge.util.XmlDom) null;
      }
      //
      // ***********************************************************************************
      //Aplico el o los XSL en caso que se solicite y efectivamente existan para aplicar
      // ***********************************************************************************
      wobjXslDOMResp = new diamondedge.util.XmlDom();
      for( int nwobjNodoXSL = 0; nwobjNodoXSL < null /*unsup pobjXMLRequest.selectNodes( "//AplicarXSL" ) */.getLength(); nwobjNodoXSL++ )
      {
        wobjNodoXSL = null /*unsup pobjXMLRequest.selectNodes( "//AplicarXSL" ) */.item( nwobjNodoXSL );
        wvarXSLFile = diamondedge.util.XmlDom.getText( wobjNodoXSL );
        if( !"" /*unsup this.Dir( (System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + wvarXSLFile), 0 ) */.equals( "" ) )
        {
          wobjXslDOMResp.load( System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + wvarXSLFile );
          wvarNuevoXML = Strings.replace( new Variant() /*unsup wobjXmlDOMResp.transformNode( wobjXslDOMResp ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
          if( ! (wobjXmlDOMResp.loadXML( wvarNuevoXML )) )
          {
            //Si no pudo levantar el XML luego de transformado, retorno el resultado de esa transformacion sin importar las que sigan despues
            ParseoMensaje = wvarNuevoXML;
            //unsup GoTo ClearObjects
          }
        }
      }
    }
    ParseoMensaje = wobjXmlDOMResp.getDocument().getDocumentElement().toString();
    //
    ClearObjects: 
    wobjNodoXSL = (org.w3c.dom.Node) null;
    wobjXmlDOMResp = (diamondedge.util.XmlDom) null;
    wobjXslDOMResp = (diamondedge.util.XmlDom) null;
    return ParseoMensaje;
  }

  private org.w3c.dom.Node GetNodoParseado( String pvarstrToParse, org.w3c.dom.Node pobjXMLDefinicion, int pvarCantidadRegistros ) throws Exception
  {
    org.w3c.dom.Node GetNodoParseado = null;
    org.w3c.dom.Element wobjNodo = null;
    org.w3c.dom.Element wobjNewNodo = null;
    org.w3c.dom.Element wobjNewNodoCopy = null;
    org.w3c.dom.Element wobjNodoContenedor = null;
    diamondedge.util.XmlDom wobjXMLDOM = null;
    String wvarstrXML = "";
    int wvarCounter = 0;
    VbScript.RegExp wobjRegExp = new VbScript.RegExp();
    void wobjColMatch;
    Object wobjMatch = null;
    String wvarParseEvalString = "";
    int wvarCcurrRegistro = 0;
    Variant wvarLastValue = new Variant();
    String wvarNombreOcurrencia = "";
    boolean wvarAreaConDatos = false;
    //
    //
    //RegExp
    //MatchCollection
    //Match
    // ************************************
    //Conversion del Area de Salida en XML
    // ************************************
    //
    wobjXMLDOM = new diamondedge.util.XmlDom();
    wobjRegExp = new VbScript.RegExp();

    wvarParseEvalString = Strings.replace( pvarstrToParse, " ", "_" );
    wvarstrXML = diamondedge.util.XmlDom.getText( pobjXMLDefinicion.getAttributes().getNamedItem( "Nombre" ) );
    wobjXMLDOM.loadXML( "<" + wvarstrXML + "></" + wvarstrXML + ">" );

    if( ! (pobjXMLDefinicion.getAttributes().getNamedItem( "NombrePorOcurrencia" ) == (org.w3c.dom.Node) null) )
    {
      wvarNombreOcurrencia = diamondedge.util.XmlDom.getText( pobjXMLDefinicion.getAttributes().getNamedItem( "NombrePorOcurrencia" ) );
    }
    else
    {
      wvarNombreOcurrencia = "";
    }
    //
    // *******************************
    //Evaluo el area segun el Pattern
    // *******************************
    wobjRegExp.Global.set( true );
    wobjRegExp.Pattern.set( diamondedge.util.XmlDom.getText( pobjXMLDefinicion.getAttributes().getNamedItem( "Pattern" ) ) );
    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    // *************************************
    //Recorro el resultado de la evaluacion
    // *************************************
    wvarCcurrRegistro = 0;
    wvarLastValue.set( "---" );
    wvarAreaConDatos = true;
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //Cada Registro Encontrado
      if( ! ((wvarCcurrRegistro < pvarCantidadRegistros)) && (pvarCantidadRegistros != -1) )
      {
        break;
      }
      //
      if( !wvarNombreOcurrencia.equals( "" ) )
      {
        wobjNodoContenedor = wobjXMLDOM.getDocument().createElement( wvarNombreOcurrencia );
        /*unsup wobjXMLDOM.selectSingleNode( "//" + wvarstrXML ) */.appendChild( wobjNodoContenedor );
      }
      else
      {
        wobjNodoContenedor = (org.w3c.dom.Element) null /*unsup wobjXMLDOM.selectSingleNode( "//" + wvarstrXML ) */;
      }
      //
      wvarCcurrRegistro = wvarCcurrRegistro + 1;
      //
      for( wvarCounter = 0; wvarCounter <= (pobjXMLDefinicion.getChildNodes().getLength() - 1); wvarCounter++ )
      {
        //Aca esta la definicion de cada campo
        //
        wobjNodo = (org.w3c.dom.Element) pobjXMLDefinicion.getChildNodes().item( wvarCounter );
        if( wobjNodo.getChildNodes().getLength() == 0 )
        {
          //error: function 'SubMatches' was not found.
          //unsup: wvarLastValue = Trim(Replace(wobjMatch.SubMatches(wvarCounter), "_", " "))
          if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( wobjNodo.getAttributes().getNamedItem( "Visible" ) ) ).equals( "SI" ) )
          {
            wobjNewNodo = wobjXMLDOM.getDocument().createElement( diamondedge.util.XmlDom.getText( wobjNodo.getAttributes().getNamedItem( "Nombre" ) ) );
            wobjNodoContenedor.appendChild( wobjNewNodo );
            //
            
            if( diamondedge.util.XmlDom.getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "ENTERO" ) )
            {
              diamondedge.util.XmlDom.setText( wobjNewNodo, String.valueOf( VB.val( wvarLastValue.toString() ) ) );
            }
            else if( diamondedge.util.XmlDom.getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "DECIMAL" ) )
            {
              diamondedge.util.XmlDom.setText( wobjNewNodo, String.valueOf( VB.val( wvarLastValue.toString() ) / Math.pow( 10, VB.val( diamondedge.util.XmlDom.getText( wobjNodo.getAttributes().getNamedItem( "Decimales" ) ) ) ) ) );
            }
            else if( diamondedge.util.XmlDom.getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "FECHA" ) )
            {
              diamondedge.util.XmlDom.setText( wobjNewNodo, Strings.right( wvarLastValue.toString(), 2 ) + "/" + Strings.mid( wvarLastValue.toString(), 5, 2 ) + "/" + Strings.left( wvarLastValue.toString(), 4 ) );
            }
            else
            {
              diamondedge.util.XmlDom.setText( wobjNewNodo, Strings.trim( wvarLastValue.toString() ) );
            }
            //
            wobjNewNodoCopy = (org.w3c.dom.Element) null;
            if( ! (wobjNodo.getAttributes().getNamedItem( "CopyTo" ) == (org.w3c.dom.Node) null) )
            {
              wobjNewNodoCopy = wobjXMLDOM.getDocument().createElement( diamondedge.util.XmlDom.getText( wobjNodo.getAttributes().getNamedItem( "CopyTo" ) ) );
              wobjNodoContenedor.appendChild( wobjNewNodoCopy );
              diamondedge.util.XmlDom.setText( wobjNewNodoCopy, diamondedge.util.XmlDom.getText( wobjNewNodo ) );
            }
            //
            //Verifico si se solicita algun Formato en especial del Origen
            if( ! (wobjNodo.getAttributes().getNamedItem( "FormatOuput" ) == (org.w3c.dom.Node) null) )
            {
              diamondedge.util.XmlDom.setText( wobjNewNodo, Strings.format( diamondedge.util.XmlDom.getText( wobjNewNodo ), diamondedge.util.XmlDom.getText( wobjNodo.getAttributes().getNamedItem( "FormatOuput" ) ) ) );
            }
            //
            //Verifico si se solicita algun Formato en especial del Destino
            if( ! ((wobjNodo.getAttributes().getNamedItem( "FormatOuputCopy" ) == (org.w3c.dom.Node) null)) && ! ((wobjNewNodoCopy == (org.w3c.dom.Element) null)) )
            {
              diamondedge.util.XmlDom.setText( wobjNewNodoCopy, Strings.format( diamondedge.util.XmlDom.getText( wobjNewNodo ), diamondedge.util.XmlDom.getText( wobjNodo.getAttributes().getNamedItem( "FormatOuputCopy" ) ) ) );
            }
            //
            //Verifico si se solicita cubrir el valor del campo en un CData
            if( ! (wobjNodo.getAttributes().getNamedItem( "CData" ) == (org.w3c.dom.Node) null) )
            {
              if( diamondedge.util.XmlDom.getText( wobjNodo.getAttributes().getNamedItem( "CData" ) ).equals( "SI" ) )
              {
                diamondedge.util.XmlDom.setText( wobjNewNodo, "<![CDATA[" + diamondedge.util.XmlDom.getText( wobjNewNodo ) + "]]>" );
              }
            }
          }
          if( null /*unsup wobjNodo.selectNodes( ".[./@Obligatorio='SI']" ) */.getLength() != 0 )
          {
            if( (diamondedge.util.XmlDom.getText( wobjNewNodo ).equals( "" )) || (diamondedge.util.XmlDom.getText( wobjNewNodo ).equals( "0" )) )
            {
              //El Dato Obligatorio no esta informado, por lo que no debe retornar ni este registro ni todos sus hijos
              wvarAreaConDatos = false;
            }
          }
        }
        else
        {
          //El nodo tiene hijos por lo que se debe procesar con un Pattern nuevo
          if( ! (wvarLastValue.isNumeric()) )
          {
            wvarLastValue.set( -1 );
          }
          //error: function 'SubMatches' was not found.
          //unsup: Set wobjNewNodo = GetNodoParseado(wobjMatch.SubMatches(wvarCounter), wobjNodo, Val(wvarLastValue))
          if( ! (wobjNewNodo == (org.w3c.dom.Element) null) )
          {
            /*unsup wobjXMLDOM.selectSingleNode( "//" + wvarstrXML ) */.appendChild( wobjNewNodo );
          }
        }
      }
    }

    if( wvarAreaConDatos )
    {
      GetNodoParseado = wobjXMLDOM.getDocument().getChildNodes().item( 0 );
    }

    ClearObjects: 
    wobjNodoContenedor = (org.w3c.dom.Element) null;
    wobjXMLDOM = (diamondedge.util.XmlDom) null;
    wobjNewNodo = (org.w3c.dom.Element) null;
    wobjNewNodoCopy = (org.w3c.dom.Element) null;
    wobjNodo = (org.w3c.dom.Element) null;
    wobjRegExp = (VbScript.RegExp) null;
    wobjMatch = null;
    wobjColMatch = (void) null;
    return GetNodoParseado;
  }

  private String GetVectorDatos( org.w3c.dom.Node pobjNodo, String pVarPathVector, org.w3c.dom.NodeList pobjSubNodosSolicitados, int pVarCantElementos ) throws Exception
  {
    String GetVectorDatos = "";
    int wvarCounterRelleno = 0;
    int wvarActualCounter = 0;
    int wvarActualNodoSolicitado = 0;
    String wvarNodoNombre = "";
    int wvarNodoTipoDato = 0;
    int wvarNodoLargoDato = 0;
    int wvarNodoCantDecimales = 0;
    org.w3c.dom.NodeList wobjSelectedNodos = null;
    org.w3c.dom.Element Nodo = null;

    wvarActualCounter = 1;
    if( ! (pobjNodo == (org.w3c.dom.Node) null) )
    {
      wobjSelectedNodos = null /*unsup pobjNodo.selectNodes( pVarPathVector ) */;

      for( wvarActualCounter = 0; wvarActualCounter <= (wobjSelectedNodos.getLength() - 1); wvarActualCounter++ )
      {
        //Recorro todos los nodos que formaran el vector
        for( wvarActualNodoSolicitado = 1; wvarActualNodoSolicitado <= pobjSubNodosSolicitados.getLength(); wvarActualNodoSolicitado++ )
        {
          //Recorro todos los nodos que se solicitan para cada uno
          Nodo = (org.w3c.dom.Element) pobjSubNodosSolicitados.item( wvarActualNodoSolicitado - 1 );
          if( Nodo.getChildNodes().getLength() == 0 )
          {
            //Es un Dato simple
            GetVectorDatos = GetVectorDatos + invoke( "GetDatoFormateado", new Variant[] { new Variant(wobjSelectedNodos.item( wvarActualCounter )), new Variant("./" + diamondedge.util.XmlDom.getText( Nodo.getAttributes().getNamedItem( new Variant("Nombre") ) )), new Variant(diamondedge.util.XmlDom.getText( Nodo.getAttributes().getNamedItem( new Variant("TipoDato") ) )), new Variant(Nodo.getAttributes().getNamedItem( new Variant("Enteros") )), new Variant(Nodo.getAttributes().getNamedItem( new Variant("Decimales") )), new Variant(Nodo.getAttributes().getNamedItem( new Variant("Obligatorio") )), new Variant(null) } );
          }
        }
        if( wvarActualCounter == (pVarCantElementos - 1) )
        {
          //unsup GoTo ClearObjects
        }
      }
    }
    for( wvarCounterRelleno = wvarActualCounter; wvarCounterRelleno <= (pVarCantElementos - 1); wvarCounterRelleno++ )
    {
      for( wvarActualNodoSolicitado = 1; wvarActualNodoSolicitado <= pobjSubNodosSolicitados.getLength(); wvarActualNodoSolicitado++ )
      {
        Nodo = (org.w3c.dom.Element) pobjSubNodosSolicitados.item( wvarActualNodoSolicitado - 1 );
        if( Nodo.getChildNodes().getLength() == 0 )
        {
          //Es un Dato simple
          GetVectorDatos = GetVectorDatos + invoke( "GetDatoFormateado", new Variant[] { new Variant(null), new Variant("//" + diamondedge.util.XmlDom.getText( Nodo.getAttributes().getNamedItem( new Variant("Nombre") ) )), new Variant(diamondedge.util.XmlDom.getText( Nodo.getAttributes().getNamedItem( new Variant("TipoDato") ) )), new Variant(Nodo.getAttributes().getNamedItem( new Variant("Enteros") )), new Variant(Nodo.getAttributes().getNamedItem( new Variant("Decimales") )), new Variant(null), new Variant(null) } );
        }
      }
    }
    ClearObjects: 
    wobjSelectedNodos = (org.w3c.dom.NodeList) null;
    return GetVectorDatos;
  }

  private String GetDatoFormateado( org.w3c.dom.Node pobjXMLContenedor, String pvarPathDato, String pvarTipoDato, org.w3c.dom.Node pobjLongitud, org.w3c.dom.Node pobjDecimales, org.w3c.dom.Node pobjDefault, org.w3c.dom.Node pobjObligatorio ) throws Exception
  {
    String GetDatoFormateado = "";
    org.w3c.dom.Node wobjNodoValor = null;
    String wvarDatoValue = "";
    int wvarCounter = 0;
    double wvarCampoNumerico = 0.0;
    String wvarErrorValidacion = "";
    String[] Arr = null;

    if( ! (pobjXMLContenedor == (org.w3c.dom.Node) null) )
    {
      //
      wvarErrorValidacion = invoke( "GetErrorInformacionDato", new Variant[] { new Variant(pobjXMLContenedor), new Variant(pvarPathDato), new Variant(pvarTipoDato), new Variant(pobjLongitud), new Variant(pobjDecimales), new Variant(pobjDefault), new Variant(pobjObligatorio) } ).toString();
      if( ! (wvarErrorValidacion.equals( "" )) )
      {
        mvarCancelacionManual = true;
        Err.raise( -1, mcteClassName + "--" + mvarConsultaRealizada, wvarErrorValidacion );
      }
      //
      wobjNodoValor = null /*unsup pobjXMLContenedor.selectSingleNode( "./" + Strings.mid( pvarPathDato, 3 ) ) */;
      if( wobjNodoValor == (org.w3c.dom.Node) null )
      {
        if( ! (pobjDefault == (org.w3c.dom.Node) null) )
        {
          wvarDatoValue = diamondedge.util.XmlDom.getText( pobjDefault );
        }
      }
      else
      {
        wvarDatoValue = diamondedge.util.XmlDom.getText( wobjNodoValor );
      }
    }
    //
    
    if( pvarTipoDato.equals( "TEXTO" ) )
    {
      //Dato del Tipo String
      GetDatoFormateado = Strings.left( wvarDatoValue + Strings.space( (int)Math.rint( VB.val( diamondedge.util.XmlDom.getText( pobjLongitud ) ) ) ), (int)Math.rint( VB.val( diamondedge.util.XmlDom.getText( pobjLongitud ) ) ) );
    }
    else if( pvarTipoDato.equals( "ENTERO" ) )
    {
      //Dato del Tipo Entero
      GetDatoFormateado = invoke( "CompleteZero", new Variant[] { new Variant(wvarDatoValue), new Variant(VB.val( new Variant(diamondedge.util.XmlDom.getText( pobjLongitud )) )) } ).toString();
    }
    else if( pvarTipoDato.equals( "DECIMAL" ) )
    {
      //Dato del Tipo "Con Decimales"
      if( new Variant( wvarDatoValue ).isNumeric() )
      {
        wvarCampoNumerico = Obj.toDouble( Strings.replace( wvarDatoValue, ",", "." ) );
      }
      for( wvarCounter = 1; wvarCounter <= Obj.toInt( diamondedge.util.XmlDom.getText( pobjDecimales ) ); wvarCounter++ )
      {
        wvarCampoNumerico = wvarCampoNumerico * 10;
      }
      GetDatoFormateado = invoke( "CompleteZero", new Variant[] { new Variant(String.valueOf( new Variant(wvarCampoNumerico) )), new Variant(VB.val( new Variant(diamondedge.util.XmlDom.getText( pobjLongitud )) ) + VB.val( new Variant(diamondedge.util.XmlDom.getText( pobjDecimales )) )) } ).toString();
    }
    else if( pvarTipoDato.equals( "FECHA" ) )
    {
      //Dato del Tipo "Fecha" Debe venir del tipo dd/mm/yyyy devuelve YYYYMMDD
      if( wvarDatoValue.matches( "*/*/*" ) )
      {
        Arr = Strings.split( wvarDatoValue, "/", -1 );
        GetDatoFormateado = Arr[2] + invoke( "CompleteZero", new Variant[] { new Variant(Arr[1]), new Variant(2) } ) + invoke( "CompleteZero", new Variant[] { new Variant(Arr[0]), new Variant(2) } );
      }
      else
      {
        GetDatoFormateado = "00000000";
      }
    }
    else if( pvarTipoDato.equals( "TEXTOIZQUIERDA" ) )
    {
      //Dato del Tipo String alineado a la izquierda
      GetDatoFormateado = Strings.right( Strings.space( (int)Math.rint( VB.val( diamondedge.util.XmlDom.getText( pobjLongitud ) ) ) ) + wvarDatoValue, (int)Math.rint( VB.val( diamondedge.util.XmlDom.getText( pobjLongitud ) ) ) );
    }
    //
    return GetDatoFormateado;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
