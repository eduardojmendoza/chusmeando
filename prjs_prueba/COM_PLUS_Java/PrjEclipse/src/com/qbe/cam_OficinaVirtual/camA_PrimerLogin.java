package com.qbe.cam_OficinaVirtual;

import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;

import com.snoopconsulting.qbe.VBObjectClass;

import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : camA_PrimerLogin
 *  File Name : camA_PrimerLogin.cls
 *  Creation Date: 29/11/2005
 *  Programmer : Fernando Osores
 *  Abstract :    Guarda la información del Primer Login del Usuario. Ya sea la primera vez,
 *        o el reseteo desde eChannels.
 * 
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class camA_PrimerLogin extends VBObjectClass 
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "cam_OficinaVirtual.camA_PrimerLogin";
  static final String mcteSPPrimerLogin = "SPCAM_CAM_PRIMER_LOGIN";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARIO = "//Usuario";
  static final String mcteParam_IDENTIFICADOR = "//Identificador";
  static final String mcteParam_EMAIL = "//Email";
  static final String mcteParam_PREGUNTA = "//Pregunta";
  static final String mcteParam_RESPUESTA = "//Respuesta";
  static final String mcteParam_IDSISTEMA = "//IdSistema";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Graba en la base la información del primer login del usuario.
   *        email, identificador, pregunta y respuesta secreta.
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  public int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    Variant gcteDB = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    boolean wvarError = false;
    int wvarStep = 0;
    String wvarUSUARIO = "";
    String wvarIDENTIFICADOR = "";
    String wvarEMAIL = "";
    String wvarPREGUNTA = "";
    String wvarRESPUESTA = "";
    int wvarIDSISTEMA = 0;
    String wvarResult = "";




    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarUSUARIO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_USUARIO ) */ );
      wvarIDENTIFICADOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_IDENTIFICADOR ) */ );
      wvarEMAIL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMAIL ) */ );
      wvarPREGUNTA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PREGUNTA ) */ );
      wvarRESPUESTA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RESPUESTA ) */ );
      wvarIDSISTEMA = Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_IDSISTEMA ) */ ) );

      wobjXMLRequest = (diamondedge.util.XmlDom) null;

      wvarStep = 20;
      wobjHSBC_DBCnn = new HSBC.DBConnection();

      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      wvarStep = 40;
      wobjDBCmd = new Command();

      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteSPPrimerLogin );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 60;
      wobjDBParm = new Parameter( "@USUARIO", AdoConst.adVarChar, AdoConst.adParamInput, 50, new Variant( wvarUSUARIO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 70;
      wvarIDENTIFICADOR = invoke( "CapicomEncrypt", new Variant[] { new Variant(wvarIDENTIFICADOR) } ).toString();

      wvarStep = 80;
      wobjDBParm = new Parameter( "@IDENTIFICADOR", AdoConst.adVarChar, AdoConst.adParamInput, 300, new Variant( wvarIDENTIFICADOR ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 90;
      wvarPREGUNTA = invoke( "CapicomEncrypt", new Variant[] { new Variant(wvarPREGUNTA) } ).toString();

      wvarStep = 100;
      wobjDBParm = new Parameter( "@PREGUNTA_SECRETA", AdoConst.adVarChar, AdoConst.adParamInput, 600, new Variant( wvarPREGUNTA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 110;
      wvarRESPUESTA = invoke( "CapicomEncrypt", new Variant[] { new Variant(wvarRESPUESTA) } ).toString();

      wvarStep = 120;
      wobjDBParm = new Parameter( "@RESPUESTA_SECRETA", AdoConst.adVarChar, AdoConst.adParamInput, 600, new Variant( wvarRESPUESTA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 130;
      wobjDBParm = new Parameter( "@EMAILSMS", AdoConst.adVarChar, AdoConst.adParamInput, 255, new Variant( wvarEMAIL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 140;
      wobjDBParm = new Parameter( "@IDSISTEMA", AdoConst.adInteger, AdoConst.adParamInput, 4, new Variant( wvarIDSISTEMA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 150;
      wobjDBCmd.execute();

      wvarStep = 160;
      wvarResult = "<Response><Estado resultado=" + "\"" + "true" + "\"" + " mensaje=" + "\"" + "Usuario '" + wvarUSUARIO + "' Registrado." + "\"" + " /><Usuario>" + wvarUSUARIO + "</Usuario></Response>";

      wvarStep = 170;
      Response.set( wvarResult );

      wvarStep = 180;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 190;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 200;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 210;
      wobjHSBC_DBCnn = null;

      wvarStep = 220;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
