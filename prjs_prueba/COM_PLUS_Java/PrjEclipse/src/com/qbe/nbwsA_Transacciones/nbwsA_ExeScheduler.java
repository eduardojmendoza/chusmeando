import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * ******************************************************************************
 * Fecha de Modificaci�n: 20/07/2010
 * Ticket:
 * Desarrollador: Daniel Armentano
 * Descripci�n: se reemplaza los espacios en blanco del EMAIL por gui�n bajo.
 * 
 * ******************************************************************************
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010.
 *  ALL RIGHTS RESERVED
 *  This software is only to be used for the purpose for which it has been provided.
 *  No part of it is to be reproduced, disassembled, transmitted, stored in a
 *  retrieval system or translated in any human or computer language in any way or
 *  for any other purposes whatsoever without the prior written consent of the Hong
 *  Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a
 *  serious civil and criminal offence, which can result in heavy fines and payment
 *  of substantial damages.
 * 
 *  Nombre del Fuente: nbwsA_ExeScheduler.cls
 *  Fecha de Creaci�n: desconocido
 *  PPcR: desconocido
 *  Desarrollador: Desconocido
 *  Descripci�n: se agrega Copyright para cumplir con las normas de QA
 * 
 * ******************************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_ExeScheduler implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_ExeScheduler";
  /**
   * Stored Procedures
   */
  static final String mcteStoreProcInsLog = "P_NBWS_INSERT_ENVIOEPOLIZAS_LOG";
  static final String mcteEPolizaConfig = "NBWS_ePolizaConfig.xml";
  static final String mcteOperacion = "EXESCH";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private NODOSREQUEST mvarNodosRequest = new NODOSREQUEST();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    HSBCInterfaces.IAction wobjClass = null;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    TEMPLATEMAIL mvarTemplate = new TEMPLATEMAIL();
    diamondedge.util.XmlDom wobjXMLRespuestas = null;
    diamondedge.util.XmlDom wobjXMLResponseAIS = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLListadoProd = null;
    diamondedge.util.XmlDom wobjXMLArchivos = null;
    org.w3c.dom.Node wobjXMLNode = null;
    org.w3c.dom.Node wobjXMLArchivo = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    String mvarResponseAIS = "";
    boolean mvarEnvioMDW = false;
    boolean mvarProcConErrores = false;
    boolean mvarErrorRecuperaPDF = false;
    boolean mvarProcesar = false;
    int mvarPendientes = 0;
    String mvarMSGError = "";
    String mvarPathPDF = "";
    String mvarCodError = "";
    String mvarEstadoEnvio = "";
    String mvarEstadoLog = "";
    String wvarPDFBase64 = "";
    String mvarRequestMDW = "";
    String wvarFalseAIS = "";
    String wvarPDFArchivos = "";
    String wvarArchivo = "";
    String wvarNombre = "";
    String mvarTemplateEmail = "";
    String mvarMailPrueba = "";
    String mvarCopiaOculta = "";
    boolean mvarBorrarArchivo = false;
    String mvarTemplateLBAALTAAUTO = "";
    String mvarTemplateLBAENDOSOAUTO = "";
    String mvarTemplateLBACARTERAAUTO = "";
    String mvarTemplateLBAALTANOAUTO = "";
    String mvarTemplateLBAENDOSONOAUTO = "";
    String mvarTemplateLBACARTERANOAUTO = "";
    String mvarTemplateLBAALTANORCP = "";
    String mvarTemplateLBAENDOSONORCP = "";
    //
    //
    //PG - 06-08-2012 RCP1
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializaci�n de variables
      mvarProcConErrores = false;
      mvarCodError = "";

      wvarStep = 10;
      //pvarRequest = Replace(pvarRequest, "&", "")
      //DoEvents
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //Dim wvarNroArch As Long
      //wvarNroArch = FreeFile()
      //Open App.Path & "\wvarStep10.txt" For Append As #1
      //Print #1, wobjXMLRequest.xml
      //Close #1
      wvarStep = 20;

      //Se obtienen los par�metros desde XML
      wobjXMLConfig = new diamondedge.util.XmlDom();
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + mcteEPolizaConfig );

      wvarStep = 30;
      mvarPathPDF = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//PATHPDF" ) */ );
      wvarStep = 31;
      mvarTemplate.AUSSYSPASS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/AUSSYSPASS" ) */ );
      wvarStep = 32;
      mvarTemplate.HOMSYSPASS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/HOMSYSPASS" ) */ );
      wvarStep = 33;
      mvarTemplate.NYLSYSPASS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/NYLSYSPASS" ) */ );
      wvarStep = 34;
      mvarTemplate.AUSUSRPASS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/AUSUSRPASS" ) */ );
      wvarStep = 35;
      mvarTemplate.HOMUSRPASS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/HOMUSRPASS" ) */ );
      wvarStep = 36;
      mvarTemplate.NYLUSRPASS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/NYLUSRPASS" ) */ );

      wvarStep = 40;
      // GED: ANEXO I - SE AGREGAN NUEVAS TEMPLATES
      mvarTemplateLBAALTAAUTO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBAALTAAUTO" ) */ );
      mvarTemplateLBAENDOSOAUTO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBAENDOSOAUTO" ) */ );
      mvarTemplateLBACARTERAAUTO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBACARTERAAUTO" ) */ );

      wvarStep = 50;
      mvarTemplateLBAALTANOAUTO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBAALTANOAUTO" ) */ );
      mvarTemplateLBAENDOSONOAUTO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBAENDOSONOAUTO" ) */ );
      mvarTemplateLBACARTERANOAUTO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBACARTERANOAUTO" ) */ );

      //PG - 06-08-2012 RCP1
      wvarStep = 55;
      mvarTemplateLBAALTANORCP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBAALTARCP" ) */ );
      mvarTemplateLBAENDOSONORCP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBAENDOSORCP" ) */ );

      //GED: para que no envie al mail que viene si no al que se define para pruebas, asi se evita mandar al exterior
      // correos con info no deseada accidentalmente
      wvarStep = 60;

      if( ! (null /*unsup wobjXMLConfig.selectSingleNode( "//MAILPRUEBA" ) */ == (org.w3c.dom.Node) null) )
      {
        mvarMailPrueba = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MAILPRUEBA" ) */ );
      }
      else
      {
        mvarMailPrueba = "";
      }

      wvarStep = 70;
      if( ! (null /*unsup wobjXMLConfig.selectSingleNode( "//COPIAOCULTA" ) */ == (org.w3c.dom.Node) null) )
      {
        mvarCopiaOculta = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//COPIAOCULTA" ) */ );
      }
      else
      {
        mvarCopiaOculta = "";
      }

      wvarStep = 80;

      if( ! (null /*unsup wobjXMLConfig.selectSingleNode( "//BORRARARCHIVO" ) */ == (org.w3c.dom.Node) null) )
      {
        if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//BORRARARCHIVO" ) */ ) ).equals( "S" ) )
        {
          mvarBorrarArchivo = true;
        }
        else
        {
          mvarBorrarArchivo = false;
        }
      }
      else
      {
        mvarBorrarArchivo = false;
      }

      //
      wvarStep = 90;
      mvarNodosRequest.CIAASCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//CIAASCOD" ) */ );
      mvarNodosRequest.RAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//RAMOPCOD" ) */ );
      mvarNodosRequest.POLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//POLIZANN" ) */ );
      mvarNodosRequest.POLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//POLIZSEC" ) */ );
      mvarNodosRequest.CERTIPOL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//CERTIPOL" ) */ );
      mvarNodosRequest.CERTIANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//CERTIANN" ) */ );
      mvarNodosRequest.CERTISEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//CERTISEC" ) */ );
      mvarNodosRequest.OPERAPOL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//OPERAPOL" ) */ );
      mvarNodosRequest.DOCUMTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//DOCUMTIP" ) */ );
      mvarNodosRequest.DOCUMDAT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//DOCUMDAT" ) */ );
      mvarNodosRequest.CLIENAP1 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//CLIENAP1" ) */ );
      mvarNodosRequest.CLIENAP2 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//CLIENAP2" ) */ );
      mvarNodosRequest.CLIENNOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//CLIENNOM" ) */ );
      mvarNodosRequest.NOMARCH = null /*unsup wobjXMLRequest.selectSingleNode( "//ARCHIVOS" ) */.toString();
      mvarNodosRequest.FORMUDES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//FORMUDES" ) */ );
      mvarNodosRequest.SWSUSCRI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//SWSUSCRI" ) */ );
      if( mvarMailPrueba.equals( "" ) )
      {
        //DA - 20/07/2010: se reemplaza el espacio en blanco por gui�n bajo ya
        //que hay fallos en las entregas de los emails por este motivo, porque
        //el MQ Gen�rico que viene del AIS reemplaza los guiones bajos por espacios.
        mvarNodosRequest.MAIL = Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//MAIL" ) */ ), " ", "_" );
      }
      else
      {
        mvarNodosRequest.MAIL = mvarMailPrueba;
      }

      mvarNodosRequest.CLAVE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//CLAVE" ) */ );
      mvarNodosRequest.SWCLAVE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//SW-CLAVE" ) */ );
      mvarNodosRequest.SWENDOSO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//SWENDOSO" ) */ );
      mvarNodosRequest.OPERATIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//OPERATIP" ) */ );
      mvarNodosRequest.ESTADO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//ESTADO" ) */ );
      mvarNodosRequest.DOCUMTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//DOCUMTIP" ) */ );
      mvarNodosRequest.CODESTAD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//CODESTAD" ) */ );


      //GED Asigna template segun producto
      //If Left(mvarNodosRequest.RAMOPCOD, 3) = "AUS" And mvarNodosRequest.SWCLAVE = "S" Then mvarTemplateEmail = mvarTemplate.AUSSYSPASS
      //If Left(mvarNodosRequest.RAMOPCOD, 3) = "AUS" And mvarNodosRequest.SWCLAVE = "U" Then mvarTemplateEmail = mvarTemplate.AUSUSRPASS
      //If Left(mvarNodosRequest.RAMOPCOD, 3) = "HOM" And mvarNodosRequest.SWCLAVE = "S" Then mvarTemplateEmail = mvarTemplate.HOMSYSPASS
      //If Left(mvarNodosRequest.RAMOPCOD, 3) = "HOM" And mvarNodosRequest.SWCLAVE = "U" Then mvarTemplateEmail = mvarTemplate.HOMUSRPASS
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "AUS" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "8" )) )
      {
        mvarTemplateEmail = mvarTemplateLBAALTAAUTO;
      }
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "AUS" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "9" )) )
      {
        mvarTemplateEmail = mvarTemplateLBAENDOSOAUTO;
      }
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "AUS" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "13" )) )
      {
        mvarTemplateEmail = mvarTemplateLBACARTERAAUTO;
      }

      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "HOM" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "8" )) )
      {
        mvarTemplateEmail = mvarTemplateLBAALTANOAUTO;
      }
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "HOM" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "9" )) )
      {
        mvarTemplateEmail = mvarTemplateLBAENDOSONOAUTO;
      }
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "HOM" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "13" )) )
      {
        mvarTemplateEmail = mvarTemplateLBACARTERANOAUTO;
      }

      //PG - 06-08-2012 RCP1
      wvarStep = 95;
      //If Left(mvarNodosRequest.RAMOPCOD, 3) = "RCP" Then
      //    pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /></Response>"
      //    Exit Function
      //End If
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "RCP" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "8" )) )
      {
        mvarTemplateEmail = mvarTemplateLBAALTANORCP;
      }
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "RCP" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "9" )) )
      {
        mvarTemplateEmail = mvarTemplateLBAENDOSONORCP;
      }
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "RCP" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "13" )) )
      {
        mvarTemplateEmail = mvarTemplateLBAENDOSONOAUTO;
      }

      //
      //Registraci�n en Log de inicio de ejecuci�n
      wvarStep = 100;
      //insertLog mcteOperacion, "I-OVEXESCH - Nueva ejecuci�n - (CLIENTE:" & mvarNodosRequest.DOCUMDAT & " " & mvarNodosRequest.DOCUMTIP & " - IMPRESO: " & mvarNodosRequest.FORMUDES & ")"
      //
      //En caso de error se resume para evitar el corte del proceso (se realiza control de err.Number)
      //On Error Resume Next
      //
      //Recuperaci�n de ARCHIVO del file server
      wvarPDFBase64 = "";
      mvarErrorRecuperaPDF = false;

      wobjXMLArchivos = new diamondedge.util.XmlDom();
      //unsup wobjXMLArchivos.async = false;
      wobjXMLArchivos.loadXML( mvarNodosRequest.NOMARCH );
      //
      // ************************************************************************************
      // Recupera del file server c/u de los .pdf
      // *****************************************************************************
      for( int nwobjXMLArchivo = 0; nwobjXMLArchivo < null /*unsup wobjXMLArchivos.selectNodes( "//ARCHIVOS/ARCHIVO" ) */.getLength(); nwobjXMLArchivo++ )
      {
        wobjXMLArchivo = null /*unsup wobjXMLArchivos.selectNodes( "//ARCHIVOS/ARCHIVO" ) */.item( nwobjXMLArchivo );


        wvarArchivo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLArchivo.selectSingleNode( "NOMARCH" ) */ );
        wvarNombre = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLArchivo.selectSingleNode( "FORMUDES" ) */ );
        if( Strings.right( wvarNombre, 8 ).equals( "Patronal" ) )
        {
          wvarNombre = "Poliza de RC Patronal";
        }

        wvarStep = 110;


        if( !wvarArchivo.equals( "" ) )
        {
          wvarPDFBase64 = invoke( "getFileToBase64", new Variant[] { new Variant(mvarPathPDF), new Variant(wvarArchivo), new Variant(wvarNombre + ".pdf"), new Variant(mvarBorrarArchivo) } );
        }
        else
        {
          break;
        }

        wvarStep = 120;

        if( wvarPDFBase64.equals( "" ) )
        {
          mvarErrorRecuperaPDF = true;
          break;
        }
        else
        {

          wvarPDFArchivos = wvarPDFArchivos + wvarPDFBase64;

        }

      }


      //Armar Request de envio a MDW
      wvarStep = 130;

      if( ! (mvarErrorRecuperaPDF) )
      {

        //Se cambia envio de mail por medio de MQ.
        if( Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "RCP" ) )
        {
          mvarRequestMDW = "<Request>" + "<DEFINICION>ismSendPdfReportCAC.xml</DEFINICION>" + "<Raiz>share:sendPdfReport</Raiz>" + "<files><files>" + wvarPDFArchivos + "</files></files>" + "<applicationId>NBWS</applicationId>" + "<reportId/>" + "<recipientTO>" + mvarNodosRequest.MAIL + "</recipientTO>" + "<recipientsCC/>" + "<recipientsBCC>" + mvarCopiaOculta + "</recipientsBCC>" + "<templateFileName>" + mvarTemplateEmail + "</templateFileName>" + "<parametersTemplate/>" + "<from/>" + "<replyTO/>" + "<bodyText/>" + "<subject/>" + "<importance/>" + "<imagePathFile/>" + "<attachPassword/>" + "<attachFileName/>" + "</Request>";
        }
        else
        {
          mvarRequestMDW = "<Request>" + "<DEFINICION>ismSendPdfReportLBAVO.xml</DEFINICION>" + "<Raiz>share:sendPdfReport</Raiz>" + "<files/>" + "<applicationId>NBWS</applicationId>" + "<reportId/>" + "<recipientTO>" + mvarNodosRequest.MAIL + "</recipientTO>" + "<recipientsCC/>" + "<recipientsBCC>" + mvarCopiaOculta + "</recipientsBCC>" + "<templateFileName>" + mvarTemplateEmail + "</templateFileName>" + "<parametersTemplate/>" + "<from/>" + "<replyTO/>" + "<bodyText/>" + "<subject/>" + "<importance/>" + "<imagePathFile/>" + "<attachPassword/>" + "<attachFileName/>" + "</Request>";
        }

        wvarStep = 140;
        //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        //Dim wvarNroArch As Long
        //wvarNroArch = FreeFile()
        //Open App.Path & "\wvarStep140.txt" For Append As #1
        //Print #1, wvarPDFArchivos
        //Close #1
        // ************************************************************************************
        // Env�a mail via MDW
        // *****************************************************************************
        //If Left(mvarNodosRequest.RAMOPCOD, 3) <> "RCP" Then
        if( Err.getError().getNumber() == 0 )
        {
          mvarEnvioMDW = invoke( "enviarXMLaMDW", new Variant[] { new Variant(mvarRequestMDW), new Variant(new Variant( mvarMSGError ))/*warning: ByRef value change will be lost.*/ } );
        }
        else
        {
          mvarMSGError = Err.getError().getDescription();
        }
        //Else
        //    mvarEnvioMDW = False
        //End If
      }
      // ***************************************************************************************
      //Procesa el envio del resultado del envio de mail al AIS mensaje 1188
      // ***************************************************************************************
      wvarStep = 150;

      //Si da error MDW envio estado "F"allo
      mvarEstadoLog = "E";

      if( mvarErrorRecuperaPDF || ! (mvarEnvioMDW) )
      {
        mvarEstadoLog = "P";
      }



      if( mvarErrorRecuperaPDF || ! (mvarEnvioMDW) )
      {
        mvarEstadoEnvio = "F";
      }


      // Si da OK MDW y no hubo ningun otro error "E"nviado
      if( mvarEnvioMDW && (Err.getError().getNumber() == 0) )
      {
        mvarEstadoEnvio = "E";
      }

      wvarStep = 160;
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant(mvarNodosRequest.RAMOPCOD + "-" + Strings.right( new Variant(("00" + mvarNodosRequest.POLIZANN)), new Variant(2) ) + "-" + Strings.right( new Variant(("000000" + mvarNodosRequest.POLIZSEC)), new Variant(6) ) + "/" + Strings.right( new Variant(("0000" + mvarNodosRequest.CERTIPOL)), new Variant(4) ) + "-" + Strings.right( new Variant(("0000" + mvarNodosRequest.CERTIANN)), new Variant(4) ) + "-" + Strings.right( new Variant(("000000" + mvarNodosRequest.CERTISEC)), new Variant(6) )), new Variant(mvarNodosRequest.CLIENAP1 + " " + mvarNodosRequest.CLIENAP2 + ", " + mvarNodosRequest.CLIENNOM), new Variant(mvarNodosRequest.MAIL), new Variant(mvarEstadoLog), new Variant(mvarNodosRequest.DOCUMTIP), new Variant(mvarNodosRequest.DOCUMDAT) } );



      wvarRequest = "";


      wvarStep = 110;
      //P�liza de LBA: Arma XML de entrada a la funci�n Multithreading (muchos request)
      wvarRequest = wvarRequest + "<Request id=\"1\" actionCode=\"nbwsA_MQGenericoAIS\" ciaascod=\"0001\">" + "<DEFINICION>LBA_1188_EnvioPoliza.xml</DEFINICION>" + "<LNK-CIAASCOD>0001</LNK-CIAASCOD>" + "<RAMOPCOD>" + mvarNodosRequest.RAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + mvarNodosRequest.POLIZANN + "</POLIZANN>" + "<POLIZSEC>" + mvarNodosRequest.POLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + mvarNodosRequest.CERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + mvarNodosRequest.CERTIANN + "</CERTIANN>" + "<CERTISEC>" + mvarNodosRequest.CERTISEC + "</CERTISEC>" + "<OPERAPOL>" + mvarNodosRequest.OPERAPOL + "</OPERAPOL>" + "<CODIESTA>" + mvarEstadoEnvio + "</CODIESTA>" + "</Request>";




      wvarStep = 170;
      //Ejecuta la funci�n Multithreading
      wvarRequest = "<Request>" + wvarRequest + "</Request>";
      invoke( "cmdp_ExecuteTrnMulti", new Variant[] { new Variant("nbwsA_MQGenericoAIS"), new Variant("nbwsA_MQGenericoAIS.biz"), new Variant(wvarRequest), new Variant(wvarResponse) } );


      wobjXMLRespuestas = new diamondedge.util.XmlDom();
      //unsup wobjXMLRespuestas.async = false;
      wobjXMLRespuestas.loadXML( wvarResponse );
      //Carga las dos respuestas en un XML.
      if( null /*unsup wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/Estado" ) */ == (org.w3c.dom.Node) null )
      {
        //
        //Fall� Response 1
        Err.getError().setDescription( "Response 1: fall� COM+ de Request 1" );
        //unsup GoTo ErrorHandler
        //
      }

      wvarStep = 180;


      //Arma un XML con los productos de las dos compa��as
      wvarResponse = "";
      //
      wvarStep = 190;
      if( ! (null /*unsup wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/CAMPOS/CANTRECI" ) */ == (org.w3c.dom.Node) null) )
      {
        wvarResponse = null /*unsup wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/CAMPOS/CANTRECI" ) */.toString();
      }
      else
      {
        wvarResponse = "";
        wvarFalseAIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/Estado/@mensaje" ) */ );
      }
      //
      if( wvarResponse.equals( "0" ) )
      {

        //insertLog mcteOperacion, "E-Fin OVEXESCH.  Estado: ERROR - " & " No se impacto resultado de env�o OK en el AIS"
      }

      wvarStep = 200;


      if( wvarResponse.equals( "" ) )
      {
        //unsup GoTo FalseAIS
      }

      //
      //Reestablece el estado de On Error
      //
      //Registraci�n en Log de fin de proceso
      wvarStep = 210;
      if( mvarProcConErrores )
      {
        //insertLog mcteOperacion, "E-Fin EXESCH.  Estado: ERROR - " & mvarMSGError
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + mvarMSGError + "</Response>" );
      }
      else
      {
        //insertLog mcteOperacion, "0-Fin EXESCH.  Estado: OK"
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //FIN
      FalseAIS: 


      //
      wvarStep = 220;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      wvarStep = 230;
      wobjXMLResponseAIS = (diamondedge.util.XmlDom) null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLListadoProd = (diamondedge.util.XmlDom) null;
      wobjXMLNode = (org.w3c.dom.Node) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        //Inserta error en Log
        //insertLog mcteOperacion, mcteClassName & " - " & _
        //                      wcteFnName & " - " & _
        //error: syntax error: near " & ":
        //unsup: wvarStep & " - " & _
        //insertLog mcteOperacion, "99-Fin EXESCH.  Estado: ABORTADO POR ERROR"
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String getFileToBase64( String pvarPath, String pvarNomArchivo, String pvarFormuDes, boolean pvarBorrarArchivo ) throws Exception
  {
    String getFileToBase64 = "";
    diamondedge.util.XmlDom wobjXMLImpreso = null;
    org.w3c.dom.Element wobjEle = null;
    String wvarFileName = "";
    int wvarFileGet = 0;
    byte[] wvarArrBytes = null;
    int wvarStep = 0;



    wvarFileName = pvarPath + "\\" + pvarNomArchivo;

    if( "" /*unsup this.Dir( wvarFileName, 0 ) */.equals( "" ) )
    {

      getFileToBase64 = "";

    }
    else
    {

      wvarStep = 300;

      wobjXMLImpreso = new diamondedge.util.XmlDom();
      //unsup wobjXMLImpreso.async = false;
      //wobjXMLPdf.loadXML "<PDF xmlns:dt=""urn:schemas-microsoft-com:datatypes"" dt:dt=""bin.base64"">" & pDatos & "</PDF>"
      wobjXMLImpreso.loadXML( "<FileParam></FileParam>" );

      //unsup wobjXMLImpreso.resolveExternals = true;

      wobjEle = wobjXMLImpreso.getDocument().createElement( "content" );

      /*unsup wobjXMLImpreso.selectSingleNode( "//FileParam" ) */.appendChild( wobjEle );

      wobjEle.setAttribute( "xmlns:dt", "urn:schemas-microsoft-com:datatypes" );

      //unsup wobjEle.dataType = "bin.base64";

      wvarStep = 310;

      //Leo el binario y lo guardo en wvarArrBytes
      wvarFileGet = FileSystem.getFreeFile();
      //error: Opening files for 'Binary' access is not supported.
      //unsup: Open wvarFileName For Binary Access Read As wvarFileGet
      wvarStep = 320;

      wvarArrBytes = new byte[FileSystem.getFileLen( wvarFileName ) - 1+1];
      //error: syntax error: near "Get":
      //unsup: Get wvarFileGet, , wvarArrBytes
      FileSystem.close( wvarFileGet );

      //Cargo el Array en el XML
      //unsup wobjEle.nodeTypedValue = wvarArrBytes;

      wobjEle = wobjXMLImpreso.getDocument().createElement( "name" );
      diamondedge.util.XmlDom.setText( wobjEle, pvarFormuDes );

      /*unsup wobjXMLImpreso.selectSingleNode( "//FileParam" ) */.appendChild( wobjEle );


      getFileToBase64 = wobjXMLImpreso.getDocument().getDocumentElement().toString();
      //
      //GED  borrar archivo
      //
      if( pvarBorrarArchivo )
      {
        FileSystem.kill( wvarFileName );
      }
      //
      wvarStep = 330;
    }



    return getFileToBase64;
  }

  private Variant insertLog( String pvarCODOP, String pvarDescripcion, String pvarCliente, String pvarEmail, String pvarEstado, String pvarDocumtip, String pvarDocumdat ) throws Exception
  {
    Variant insertLog = new Variant();
    diamondedge.util.XmlDom wobjXMLConfig = null;
    boolean wvarReqRespInTxt = false;
    int mvarDebugCode = 0;
    //
    //
    wvarReqRespInTxt = false;
    //
    wobjXMLConfig = new diamondedge.util.XmlDom();
    wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + mcteEPolizaConfig );
    //1=Debug to BD - 2=Debug to BD and File sin request/Responses - 3=Debug to BD andFile completo
    mvarDebugCode = Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//DEBUG" ) */ ) );
    //
    if( (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<REQUEST>", true ) > 0) || (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<RESPONSE>", true ) > 0) )
    {
      wvarReqRespInTxt = true;
    }
    //
    if( (mvarDebugCode >= 1) && ! (wvarReqRespInTxt) )
    {
      invoke( "debugToBD", new Variant[] { new Variant(pvarCODOP), new Variant(pvarDescripcion), new Variant(pvarCliente), new Variant(pvarEmail), new Variant(pvarEstado), new Variant(pvarDocumtip), new Variant(pvarDocumdat) } );
    }
    if( (mvarDebugCode == 3) || ((mvarDebugCode == 2) && ! (wvarReqRespInTxt)) )
    {
      invoke( "debugToFile", new Variant[] { new Variant(pvarCODOP), new Variant(pvarDescripcion), new Variant(pvarCliente), new Variant(pvarEmail), new Variant(pvarEstado), new Variant(pvarDocumtip), new Variant(pvarDocumdat) } );
    }
    //
    return insertLog;
  }

  private boolean debugToBD( String pvarCODOP, String pvarDescripcion, String pvarCliente, String pvarEmail, String pvarEstado, String pvarDocumtip, String pvarDocumdat ) throws Exception
  {
    boolean debugToBD = false;
    Variant gcteDBLOG = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    //
    //
    //GED 24-11-2009 Prueba de DBConnection
    wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
    //Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    //error: function 'GetDBConnection' was not found.
    //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDBLOG)
    wobjDBCmd = new Command();
    //
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( mcteStoreProcInsLog );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    //
    wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( "0" ) );
    wobjDBCmd.getParameters().append( wobjDBParm );
    wobjDBParm = (Parameter) null;
    //
    wobjDBParm = new Parameter( "@CODOP", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( Strings.left( pvarCODOP, 6 ) ) );
    wobjDBCmd.getParameters().append( wobjDBParm );
    wobjDBParm = (Parameter) null;
    //
    wobjDBParm = new Parameter( "@DESCRIPCION", AdoConst.adChar, AdoConst.adParamInput, 100, new Variant( Strings.left( pvarDescripcion, 100 ) ) );
    wobjDBCmd.getParameters().append( wobjDBParm );
    wobjDBParm = (Parameter) null;
    //
    wobjDBParm = new Parameter( "@CLIENTE", AdoConst.adChar, AdoConst.adParamInput, 70, new Variant( Strings.left( pvarCliente, 70 ) ) );
    wobjDBCmd.getParameters().append( wobjDBParm );
    wobjDBParm = (Parameter) null;

    wobjDBParm = new Parameter( "@EMAIL", AdoConst.adChar, AdoConst.adParamInput, 50, new Variant( Strings.left( pvarEmail, 50 ) ) );
    wobjDBCmd.getParameters().append( wobjDBParm );
    wobjDBParm = (Parameter) null;

    wobjDBParm = new Parameter( "@ESTADO", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( Strings.left( pvarEstado, 3 ) ) );
    wobjDBCmd.getParameters().append( wobjDBParm );
    wobjDBParm = (Parameter) null;

    wobjDBParm = new Parameter( "@DOCUMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( pvarDocumtip ) );
    wobjDBParm.setPrecision( (byte)( 2 ) );
    wobjDBParm.setScale( (byte)( 0 ) );
    wobjDBCmd.getParameters().append( wobjDBParm );
    wobjDBParm = (Parameter) null;

    wobjDBParm = new Parameter( "@DOCUMDAT", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( pvarDocumdat ) );
    wobjDBParm.setPrecision( (byte)( 11 ) );
    wobjDBParm.setScale( (byte)( 0 ) );
    wobjDBCmd.getParameters().append( wobjDBParm );
    wobjDBParm = (Parameter) null;

    wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
    //
    //Controlamos la respuesta del SQL
    if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
    {
      debugToBD = true;
    }
    else
    {
      debugToBD = false;
    }
    //
    wobjHSBC_DBCnn = null;
    wobjDBCnn = (Connection) null;
    wobjDBCmd = (Command) null;
    //
    return debugToBD;
  }

  private void debugToFile( String pvarCODOP, String pvarDescripcion, String pvarCliente, String pvarEmail, String pvarEstado, String pvarDocumtip, String pvarDocumdat ) throws Exception
  {
    diamondedge.util.XmlDom wobjXMLConfig = null;
    String wvarText = "";
    String wvarFileName = "";
    int wvarNroArch = 0;
    //
    //
    wvarText = "(" + DateTime.now() + "-" + DateTime.now() + "):" + pvarDescripcion + "-" + pvarCliente + "-" + pvarEmail + "-" + pvarEstado + "-" + pvarDocumtip + "-" + pvarDocumdat;

    wvarFileName = "debug-" + pvarCODOP + "-" + DateTime.year( DateTime.now() ) + Strings.right( ("00" + DateTime.month( DateTime.now() )), 2 ) + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) + ".log";
    wvarNroArch = FileSystem.getFreeFile();
    FileSystem.openAppend( System.getProperty("user.dir") + "\\DEBUG\\" + wvarFileName, 1 );
    FileSystem.out(1).writeValue( wvarText );
    FileSystem.out(1).println();
    FileSystem.close( 1 );
    //
  }

  private boolean enviarXMLaMDW( String pvarRequestMdw, Variant pvarMsgError ) throws Exception
  {
    boolean enviarXMLaMDW = false;
    Object wobjClass = null;
    diamondedge.util.XmlDom wvarXMLResponseMdw = null;
    String wvarResponseMdw = "";
    //
    wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
    //error: function 'Execute' was not found.
    //unsup: Call wobjClass.Execute(pvarRequestMdw, wvarResponseMdw, "")
    //
    wobjClass = null;
    //
    wvarXMLResponseMdw = new diamondedge.util.XmlDom();
    //unsup wvarXMLResponseMdw.async = false;
    wvarXMLResponseMdw.loadXML( wvarResponseMdw );
    //
    if( ! (null /*unsup wvarXMLResponseMdw.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
    {
      if( diamondedge.util.XmlDom.getText( null /*unsup wvarXMLResponseMdw.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
      {
        if( null /*unsup wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" ) */ == (org.w3c.dom.Node) null )
        {
          enviarXMLaMDW = true;
        }
        else
        {
          enviarXMLaMDW = false;
          pvarMsgError.set( diamondedge.util.XmlDom.getText( null /*unsup wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" ) */ ) );
        }
      }
      else
      {
        if( ! (null /*unsup wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" ) */ == (org.w3c.dom.Node) null) )
        {
          pvarMsgError.set( diamondedge.util.XmlDom.getText( null /*unsup wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" ) */ ) );
        }
        else
        {
          pvarMsgError.set( "Error en la ejecuci�n de LBAA_MWGenerico.lbaw_MQMW" );
        }
        enviarXMLaMDW = false;
      }
    }
    else
    {
      pvarMsgError.set( "Error en la ejecuci�n de LBAA_MWGenerico.lbaw_MQMW" );
      enviarXMLaMDW = false;
    }
    //
    wobjClass = null;
    wvarXMLResponseMdw = (diamondedge.util.XmlDom) null;

    return enviarXMLaMDW;
  }

  private void ObjectControl_Activate() throws Exception
  {
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;


    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
