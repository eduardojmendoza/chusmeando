import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 * TODO: poner COPYRIGHT
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_sImpresos implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_sImpresos";
  /**
   * 
   */
  static final String wcteXSL_sImpresos = "XSLs\\sImpresos.xsl";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   */
  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLRespuestas = null;
    diamondedge.util.XmlDom wobjXSLSalida = null;
    org.w3c.dom.NodeList wobjXML_PRODUCTOS_List = null;
    org.w3c.dom.Node wobjXML_PRODUCTOS_Node = null;
    org.w3c.dom.Element wobjElemento = null;
    int wvarCount = 0;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarResponse_HTML = "";
    String wvarResponse_XML = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanta las p�lizas a recorrer
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      //Solo p�lizas est�n habilitadas para NBWS y si est�n habilitadas para ser navegadas en el sitio
      wobjXML_PRODUCTOS_List = null /*unsup wobjXMLRequest.selectNodes( "Request/PRODUCTOS/PRODUCTO[./HABILITADO_NBWS = 'S' and ./HABILITADO_NAVEGACION = 'S' and ./POLIZA_EXCLUIDA = 'N']" ) */;
      wvarRequest = "";
      wvarCount = 1;
      //Recorre cada p�liza
      for( int nwobjXML_PRODUCTOS_Node = 0; nwobjXML_PRODUCTOS_Node < wobjXML_PRODUCTOS_List.getLength(); nwobjXML_PRODUCTOS_Node++ )
      {
        wobjXML_PRODUCTOS_Node = wobjXML_PRODUCTOS_List.item( nwobjXML_PRODUCTOS_Node );
        //
        wvarStep = 30;
        //Se fija de qu� compa��a es la p�liza
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "CIAASCOD" ) */ ).equals( "0020" ) )
        {
          //
          wvarStep = 40;
          //P�liza de NYL: Arma XML de entrada a la funci�n Multithreading (muchos request)
          wvarRequest = wvarRequest + "<Request id=\"" + wvarCount + "\" actionCode=\"nbwsA_getResuList\" ciaascod=\"0020\">" + "   <RAMOPCOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "RAMOPCOD" ) */ ) + "</RAMOPCOD>" + "   <POLIZANN>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "POLIZANN" ) */ ) + "</POLIZANN>" + "   <POLIZSEC>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "POLIZSEC" ) */ ) + "</POLIZSEC>" + "   <CERTIPOL>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTIPOL" ) */ ) + "</CERTIPOL>" + "   <CERTIANN>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTIANN" ) */ ) + "</CERTIANN>" + "   <CERTISEC>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTISEC" ) */ ) + "</CERTISEC>" + "   <SUPLENUM>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "SUPLENUM" ) */ ) + "</SUPLENUM>" + "</Request>";
        }
        else
        {
          //
          wvarStep = 50;
          //P�liza de LBA: Arma XML de entrada a la funci�n Multithreading (muchos request)
          wvarRequest = wvarRequest + "<Request id=\"" + wvarCount + "\" actionCode=\"lbaw_OVVerifReimpresion\" ciaascod=\"0001\">" + "   <USUARIO></USUARIO>" + "   <RAMOPCOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "RAMOPCOD" ) */ ) + "</RAMOPCOD>" + "   <POLIZANN>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "POLIZANN" ) */ ) + "</POLIZANN>" + "   <POLIZSEC>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "POLIZSEC" ) */ ) + "</POLIZSEC>" + "   <CERTIPOL>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTIPOL" ) */ ) + "</CERTIPOL>" + "   <CERTIANN>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTIANN" ) */ ) + "</CERTIANN>" + "   <CERTISEC>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTISEC" ) */ ) + "</CERTISEC>" + "   <SUPLENUM>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( "SUPLENUM" ) */ ) + "</SUPLENUM>" + "</Request>";
        }
        wvarCount = wvarCount + 1;
        /*unsup wobjXML_PRODUCTOS_List.nextNode() */;
        //
      }
      //
      wvarStep = 60;
      //Ejecuta la funci�n Multithreading
      wvarRequest = "<Request>" + wvarRequest + "</Request>";
      invoke( "cmdp_ExecuteTrnMulti", new Variant[] { new Variant("lbaw_OVVerifReimpresion"), new Variant("lbaw_OVVerifReimpresion.biz"), new Variant(wvarRequest), new Variant(wvarResponse) } );
      //
      wvarStep = 70;
      //Carga las respuestas
      wobjXMLRespuestas = new diamondedge.util.XmlDom();
      //unsup wobjXMLRespuestas.async = false;
      wobjXMLRespuestas.loadXML( wvarResponse );
      //
      wvarStep = 80;
      //Verifica si pinch� el COM+
      if( null /*unsup wobjXMLRespuestas.selectSingleNode( "//Response" ) */ == (org.w3c.dom.Node) null )
      {
        //
        wvarStep = 90;
        //Pinch� el COM+
        Err.getError().setDescription( "Fall� la ejecuci�n de la funci�n Multithreading." );
        //unsup GoTo ErrorHandler
      }
      else
      {
        //
        //Analiza las respuestas en base al request
        /*unsup wobjXML_PRODUCTOS_List.reset() */;
        wvarCount = 1;
        wvarResponse_HTML = "";
        for( int nwobjXML_PRODUCTOS_Node = 0; nwobjXML_PRODUCTOS_Node < wobjXML_PRODUCTOS_List.getLength(); nwobjXML_PRODUCTOS_Node++ )
        {
          wobjXML_PRODUCTOS_Node = wobjXML_PRODUCTOS_List.item( nwobjXML_PRODUCTOS_Node );
          //
          wvarStep = 100;
          if( null /*unsup wobjXMLRespuestas.selectSingleNode( ("Response/Response[@id='" + wvarCount + "']") ) */ == (org.w3c.dom.Node) null )
          {
            //
            wvarStep = 110;
            //Pinch� el COM+ para ese PRODUCTO.
            //En este caso el XSL no va a tener el response, con lo cual no dibujar� ninguna impresorita.
          }
          else
          {
            //
            wvarStep = 120;
            wobjElemento = (org.w3c.dom.Element) null /*unsup wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='" + wvarCount + "']" ) */.cloneNode( true );
            wobjXML_PRODUCTOS_Node.appendChild( wobjElemento );
            //
          }
          //
          //Al nodo original le agrega la respuesta
          wvarStep = 130;
          wvarResponse_XML = wvarResponse_XML + wobjXML_PRODUCTOS_Node.toString();
          wvarCount = wvarCount + 1;
          /*unsup wobjXML_PRODUCTOS_List.nextNode() */;
        }
        //
      }
      //
      wvarStep = 140;
      //Levanta XSL para armar XML de salida
      wobjXSLSalida = new diamondedge.util.XmlDom();
      //unsup wobjXSLSalida.async = false;
      wobjXSLSalida.load( System.getProperty("user.dir") + "\\" + wcteXSL_sImpresos );
      //
      wvarStep = 150;
      //Devuelve respuesta en formato HTML
      wobjXMLRespuestas.loadXML( "<PRODUCTOS>" + wvarResponse_XML + "</PRODUCTOS>" );
      wvarResponse_HTML = new Variant() /*unsup wobjXMLRespuestas.transformNode( wobjXSLSalida ) */.toString();
      //
      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"></Estado><Response_HTML><![CDATA[" + wvarResponse_HTML + "]]></Response_HTML></Response>" );
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLRespuestas = (diamondedge.util.XmlDom) null;
      wobjXML_PRODUCTOS_List = (org.w3c.dom.NodeList) null;
      wobjXML_PRODUCTOS_Node = (org.w3c.dom.Node) null;
      wobjElemento = (org.w3c.dom.Element) null;
      wobjXSLSalida = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 190;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = (diamondedge.util.XmlDom) null;
        wobjXMLRespuestas = (diamondedge.util.XmlDom) null;
        wobjXML_PRODUCTOS_List = (org.w3c.dom.NodeList) null;
        wobjXML_PRODUCTOS_Node = (org.w3c.dom.Node) null;
        wobjElemento = (org.w3c.dom.Element) null;
        wobjXSLSalida = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        /*unsup mobjCOM_Context.SetAbort() */;
        IAction_Execute = 1;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
