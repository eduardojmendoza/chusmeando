import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;

import com.snoopconsulting.qbe.VBObjectClass;

import diamondedge.swing.*;

/**
 * Request para probar <Request><INSCTRL>N</INSCTRL></Request> Objetos del
 * FrameWork
 */

public class nbwsA_FillScheduler extends VBObjectClass {
	/**
	 * Implementacion de los objetos Archivo de Configuracion
	 */
	static final String mcteEPolizaConfig = "NBWS_ePolizaConfig.xml";
	/**
	 * Datos de la accion
	 */
	static final String mcteClassName = "nbwsA_Transacciones.nbwsA_FillScheduler";
	/**
	 * Stored Procedures
	 */
	static final String mcteStoreProcInsBandejaSCH = "P_CP_SA_BANDEJA_INSERT";
	/**
	 * ANTERIORES
	 */
	static final String mcteStoreProcInsControl = "P_ALERTAS_INSERT_CONTROL";
	/**
	 * SI NUEVO
	 */
	static final String mcteStoreProcInsLog = "P_NBWS_INSERT_ENVIOEPOLIZAS_LOG";
	/**
   * 
   */
	static final String mcteParam_INSCTRL = "//INSCTRL";
	/**
   * 
   */
	static final String mcteOperacion = "FILLSCH";
	private Object mobjCOM_Context = null;
	private Object mobjEventLog = null;
	private NODOINSERTASCH mvarNodoInsertaSch = new NODOINSERTASCH();
	/**
	 * static variable for method: IAction_Execute
	 */
	private final String wcteFnName = "IAction_Execute";

	private int IAction_Execute(String pvarRequest, Variant pvarResponse,
			String pvarContextInfo) {
		int IAction_Execute = 0;
		Variant vbLogEventTypeError = new Variant();
		HSBCInterfaces.IAction wobjClass = null;
		int wvarStep = 0;
		String wvarRequest = "";
		String wvarResponse = "";
		String wvarResponseNYL = "";
		String wvarResponseLBA = "";
		String wvarResult = "";
		diamondedge.util.XmlDom wobjXMLRespuestas = null;
		org.w3c.dom.Node wobjXMLConfigNode = null;
		diamondedge.util.XmlDom wobjXMLConfig = null;
		diamondedge.util.XmlDom wobjXMLRequest = null;
		int mvarIDCONTROL = 0;
		String mvarINSCTRL = "";
		String mvarResponseAIS = "";
		boolean mvarEnvioMDW = false;
		boolean mvarProcConErrores = false;
		boolean mvarProcesar = false;
		int mvarPendientes = 0;
		String mvarMSGError = "";
		String mvarFECHA = "";
		String mvarCODOP = "";
		int wvarCount = 0;
		String wvarFalseAIS = "";
		//
		//
		//
		//
		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//
			// Inicializaci�n de variables
			wvarStep = 10;
			mvarFECHA = Strings.right("00" + DateTime.month(DateTime.now()), 2)
					+ "-"
					+ Strings.right(("00" + DateTime.day(DateTime.now())), 2)
					+ "-" + DateTime.year(DateTime.now());
			mvarINSCTRL = "N";
			//
			// Lectura de par�metros
			wvarStep = 20;
			wobjXMLRequest = new diamondedge.util.XmlDom();
			// unsup wobjXMLRequest.async = false;
			wobjXMLRequest.loadXML(pvarRequest);
			//
			wvarStep = 30;
			if (!(null /*
						 * unsup wobjXMLRequest.selectSingleNode(
						 * mcteParam_INSCTRL )
						 */== (org.w3c.dom.Node) null)) {
				mvarINSCTRL = diamondedge.util.XmlDom.getText(null /*
																	 * unsup
																	 * wobjXMLRequest
																	 * .
																	 * selectSingleNode
																	 * (
																	 * mcteParam_INSCTRL
																	 * )
																	 */);
			}
			//
			// Registraci�n en Log de inicio de ejecuci�n
			wvarStep = 40;
			// insertLog mcteOperacion, "I-NBWS_FILLSCH - Nueva ejecuci�n"
			wvarRequest = "";

			wvarStep = 50;
			// P�liza de LBA: Arma XML de entrada a la funci�n Multithreading
			// (muchos request)
			wvarRequest = wvarRequest
					+ "<Request id=\"1\" actionCode=\"nbwsA_MQGenericoAIS\" ciaascod=\"0001\">"
					+ "   <DEFINICION>LBA_1187_ListadoePolizas.xml</DEFINICION>"
					+ "   <LNK-CIAASCOD>0001</LNK-CIAASCOD>" + "</Request>";

			wvarStep = 60;
			// Ejecuta la funci�n Multithreading
			wvarRequest = "<Request>" + wvarRequest + "</Request>";
			invoke("cmdp_ExecuteTrnMulti", new Variant[] {
					new Variant("nbwsA_MQGenericoAIS"),
					new Variant("nbwsA_MQGenericoAIS.biz"),
					new Variant(wvarRequest), new Variant(wvarResponse) });

			wobjXMLRespuestas = new diamondedge.util.XmlDom();
			// unsup wobjXMLRespuestas.async = false;

			wobjXMLRespuestas.loadXML(wvarResponse);

			// Carga las dos respuestas en un XML.
			wvarStep = 70;
			if (null /*
					 * unsup wobjXMLRespuestas.selectSingleNode(
					 * "Response/Response[@id='1']/Estado" )
					 */== (org.w3c.dom.Node) null) {
				//
				// Fall� Response 1
				Err.getError().setDescription(
						"Response 1: fall� COM+ de Request 1");
				// unsup GoTo ErrorHandler
				//
			}
			//
			wvarStep = 80;

			wvarResponseNYL = "";

			wvarStep = 90;

			if (!(null /*
						 * unsup wobjXMLRespuestas.selectSingleNode(
						 * "Response/Response[@id='1']/CAMPOS/IMPRESOS" )
						 */== (org.w3c.dom.Node) null)) {
				wvarResponseLBA = null /*
										 * unsup
										 * wobjXMLRespuestas.selectSingleNode(
										 * "Response/Response[@id='1']/CAMPOS" )
										 */.toString();
				wvarResponseLBA = Strings.replace(
						Strings.replace(wvarResponseLBA, "<CAMPOS>", ""),
						"</CAMPOS>", "");

				wvarResponseLBA = "<Response>"
						+ Strings.replace(Strings.replace(wvarResponseLBA,
								"<IMPRESOS>", ""), "</IMPRESOS>", "")
						+ "</Response>";

			} else {
				wvarResponseLBA = "";
				wvarFalseAIS = wvarFalseAIS + String.valueOf((char) (13))
						+ diamondedge.util.XmlDom.getText(null /*
																 * unsup
																 * wobjXMLRespuestas
																 * .
																 * selectSingleNode
																 * (
																 * "Response/Response[@id='1']/Estado/@mensaje"
																 * )
																 */);
			}
			wvarStep = 100;

			if (!wvarResponseLBA.equals("")) {

				if (!(invoke("insertaImpresoSCH", new Variant[] {
						new Variant(wvarResponseLBA), new Variant("0001"),
						new Variant(mvarFECHA) }))) {
					// unsup GoTo ErrorHandler
				}

			}

			if (wvarResponseLBA.equals("")) {
				// unsup GoTo FalseAIS
			}

			wvarStep = 110;

			// Registraci�n en Log de fin de proceso
			if (mvarProcConErrores) {
				// insertLog mcteOperacion, "E- Fin NBWS_FILLSCH. Estado: ERROR"
				pvarResponse.set("<Response><Estado resultado="
						+ String.valueOf((char) (34)) + "false"
						+ String.valueOf((char) (34)) + " mensaje="
						+ String.valueOf((char) (34))
						+ String.valueOf((char) (34)) + " /></Response>");
			} else {
				// insertControlSCH
				// If mvarINSCTRL = "S" Then
				// insertControlSCH mvarFECHA
				// End If
				// insertLog mcteOperacion, "0- Fin NBWS_FILLSCH. Estado: OK"
				pvarResponse.set("<Response><Estado resultado="
						+ String.valueOf((char) (34)) + "true"
						+ String.valueOf((char) (34)) + " mensaje="
						+ String.valueOf((char) (34))
						+ String.valueOf((char) (34)) + " /></Response>");
			}

			wvarStep = 120;
			FalseAIS: if (wvarResponse.equals("")) {
				pvarResponse
						.set("<Response><Estado resultado=\"false\" mensaje=\""
								+ wvarFalseAIS
								+ "\"></Estado><Response_XML></Response_XML><Response_HTML>"
								+ wvarFalseAIS + "</Response_HTML></Response>");
			}
			//
			// FIN
			//
			wvarStep = 130;
			/* unsup mobjCOM_Context.SetComplete() */;
			IAction_Execute = 0;
			//
			wvarStep = 140;

			return IAction_Execute;

			// ~~~~~~~~~~~~~~~
		} catch (Exception _e_) {
			Err.set(_e_);
			try {
				// ~~~~~~~~~~~~~~~
				// Inserta Error en EventViewer
				mobjEventLog
						.Log(this
								.getValue("EventLog_Category.evtLog_Category_Unexpected"),
								mcteClassName, wcteFnName, wvarStep, Err
										.getError().getNumber(), "Error= ["
										+ Err.getError().getNumber() + "] - "
										+ Err.getError().getDescription(),
								vbLogEventTypeError);

				// Inserta error en Log
				// insertLog mcteOperacion, mcteClassName & " - " & _
				// wcteFnName & " - " & _
				// error: syntax error: near " & ":
				// unsup: wvarStep & " - " & _
				// insertLog mcteOperacion,
				// "99- Fin NBWS_FILLSCH. Estado: ABORTADO POR ERROR"
				IAction_Execute = 1;
				/* unsup mobjCOM_Context.SetAbort() */;
				Err.clear();
			} catch (Exception _e2_) {
			}
		}
		return IAction_Execute;
	}

	public boolean insertaImpresoSCH(String pvarResponse, String pvarCiaascod,
			String pvarFECHA) throws Exception {
		boolean insertaImpresoSCH = false;
		int mvarx = 0;
		String wvarNombreArchivos = "";
		diamondedge.util.XmlDom wobjXMLImpresos = null;
		diamondedge.util.XmlDom wobjXMLDOMArchivos = null;
		diamondedge.util.XmlDom wobjXSLResponse = null;
		org.w3c.dom.Node wobjXMLNode = null;
		org.w3c.dom.NodeList wobjXMLImpresosList = null;
		org.w3c.dom.NodeList wobjXMLNombreArchivos = null;
		org.w3c.dom.NodeList wobjXMLFormudes = null;
		org.w3c.dom.Node wobjXMLArchivos = null;
		String wvarResult = "";

		wobjXMLImpresos = new diamondedge.util.XmlDom();
		/* unsup wobjXMLImpresos.setProperty( "SelectionLanguage", "XPath" ) */;
		// unsup wobjXMLImpresos.async = false;
		wobjXMLImpresos.loadXML(pvarResponse);

		wobjXSLResponse = new diamondedge.util.XmlDom();

		// unsup wobjXSLResponse.async = false;
		wobjXSLResponse.loadXML(invoke("p_GetXSL", new Variant[] {}));

		wvarResult = Strings.replace(new Variant() /*
													 * unsup
													 * wobjXMLImpresos.transformNode
													 * ( wobjXSLResponse )
													 */.toString(),
				"<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "");

		wobjXMLDOMArchivos = new diamondedge.util.XmlDom();
		/* unsup wobjXMLDOMArchivos.setProperty( "SelectionLanguage", "XPath" ) */;
		// unsup wobjXMLDOMArchivos.async = false;
		wobjXMLDOMArchivos.loadXML(wvarResult);

		// wvarResult = wobjXMLImpresos.transformNode(wobjXSLResponse)
		mvarNodoInsertaSch.NOMARCH = "";

		wobjXMLImpresosList = null /*
									 * unsup wobjXMLDOMArchivos.selectNodes(
									 * "//IMPRESO[not(AGRUPAR=preceding-sibling::IMPRESO/AGRUPAR)]"
									 * )
									 */;

		for (int nwobjXMLNode = 0; nwobjXMLNode < wobjXMLImpresosList
				.getLength(); nwobjXMLNode++) {
			wobjXMLNode = wobjXMLImpresosList.item(nwobjXMLNode);

			mvarNodoInsertaSch.NOMARCH = "";
			mvarNodoInsertaSch.CIAASCOD = pvarCiaascod;
			mvarNodoInsertaSch.RAMOPCOD = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "RAMOPCOD" )
								 */);
			mvarNodoInsertaSch.POLIZANN = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "POLIZANN" )
								 */);
			mvarNodoInsertaSch.POLIZSEC = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "POLIZSEC" )
								 */);
			mvarNodoInsertaSch.CERTIPOL = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "CERTIPOL" )
								 */);
			mvarNodoInsertaSch.CERTIANN = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "CERTIANN" )
								 */);
			mvarNodoInsertaSch.CERTISEC = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "CERTISEC" )
								 */);
			mvarNodoInsertaSch.OPERAPOL = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "OPERAPOL" )
								 */);
			mvarNodoInsertaSch.DOCUMTIP = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "DOCUMTIP" )
								 */);
			mvarNodoInsertaSch.DOCUMDAT = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "DOCUMDAT" )
								 */);
			mvarNodoInsertaSch.CLIENAP1 = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "CLIENAP1" )
								 */);
			mvarNodoInsertaSch.CLIENAP2 = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "CLIENAP2" )
								 */);
			mvarNodoInsertaSch.CLIENNOM = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "CLIENNOM" )
								 */);

			wobjXMLNombreArchivos = null /*
										 * unsup wobjXMLDOMArchivos.selectNodes(
										 * "//IMPRESO[CIAASCOD='" +
										 * mvarNodoInsertaSch.CIAASCOD +
										 * "' and RAMOPCOD='" +
										 * mvarNodoInsertaSch.RAMOPCOD +
										 * "' and POLIZANN='" +
										 * mvarNodoInsertaSch.POLIZANN +
										 * "' and POLIZSEC='" +
										 * mvarNodoInsertaSch.POLIZSEC +
										 * "' and CERTIPOL='" +
										 * mvarNodoInsertaSch.CERTIPOL +
										 * "' and CERTIANN='" +
										 * mvarNodoInsertaSch.CERTIANN +
										 * "' and CERTISEC='" +
										 * mvarNodoInsertaSch.CERTISEC +
										 * "' and OPERAPOL='" +
										 * mvarNodoInsertaSch.OPERAPOL +
										 * "' ]/NOMARCH" )
										 */;

			wobjXMLFormudes = null /*
									 * unsup wobjXMLDOMArchivos.selectNodes(
									 * "//IMPRESO[CIAASCOD='" +
									 * mvarNodoInsertaSch.CIAASCOD +
									 * "' and RAMOPCOD='" +
									 * mvarNodoInsertaSch.RAMOPCOD +
									 * "' and POLIZANN='" +
									 * mvarNodoInsertaSch.POLIZANN +
									 * "' and POLIZSEC='" +
									 * mvarNodoInsertaSch.POLIZSEC +
									 * "' and CERTIPOL='" +
									 * mvarNodoInsertaSch.CERTIPOL +
									 * "' and CERTIANN='" +
									 * mvarNodoInsertaSch.CERTIANN +
									 * "' and CERTISEC='" +
									 * mvarNodoInsertaSch.CERTISEC +
									 * "' and OPERAPOL='" +
									 * mvarNodoInsertaSch.OPERAPOL +
									 * "' ]/FORMUDES" )
									 */;

			for (mvarx = 0; mvarx <= (wobjXMLNombreArchivos.getLength() - 1); mvarx++) {

				mvarNodoInsertaSch.NOMARCH = mvarNodoInsertaSch.NOMARCH
						+ "<ARCHIVO>"
						+ Strings.replace(wobjXMLNombreArchivos.item(mvarx)
								.toString(), String.valueOf((char) (32)), "_")
						+ wobjXMLFormudes.item(mvarx).toString() + "</ARCHIVO>";

			}

			mvarNodoInsertaSch.SWSUSCRI = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "SWSUSCRI" )
								 */);
			mvarNodoInsertaSch.MAIL = diamondedge.util.XmlDom
					.getText(null /* unsup wobjXMLNode.selectSingleNode( "MAIL" ) */);
			mvarNodoInsertaSch.CLAVE = diamondedge.util.XmlDom
					.getText(null /* unsup wobjXMLNode.selectSingleNode( "CLAVE" ) */);
			mvarNodoInsertaSch.SWCLAVE = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "SW-CLAVE" )
								 */);
			mvarNodoInsertaSch.SWENDOSO = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "SWENDOSO" )
								 */);
			mvarNodoInsertaSch.OPERATIP = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "OPERATIP" )
								 */);
			mvarNodoInsertaSch.ESTADO = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode( "ESTADO"
								 * )
								 */);
			mvarNodoInsertaSch.CODESTAD = diamondedge.util.XmlDom
					.getText(null /*
								 * unsup wobjXMLNode.selectSingleNode(
								 * "CODESTAD" )
								 */);

			if (!mvarNodoInsertaSch.RAMOPCOD.equals("")) {
				// Inserta c/u de los registros en la Bandeja de Jobs
				if (!(invoke("insertScheduler",
						new Variant[] { new Variant(mvarNodoInsertaSch),
								new Variant(pvarFECHA) }))) {
					insertaImpresoSCH = false;
					return insertaImpresoSCH;

				}
			}

		}

		wobjXMLNode = (org.w3c.dom.Node) null;
		wobjXMLImpresos = (diamondedge.util.XmlDom) null;
		insertaImpresoSCH = true;

		return insertaImpresoSCH;
	}

	private String p_GetXSL() throws Exception {
		String p_GetXSL = "";
		String wvarStrXSL = "";
		//
		wvarStrXSL = wvarStrXSL
				+ "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
		wvarStrXSL = wvarStrXSL
				+ "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
		wvarStrXSL = wvarStrXSL + "<xsl:template match='/'>";
		wvarStrXSL = wvarStrXSL + " <Request>";
		wvarStrXSL = wvarStrXSL + "  <xsl:for-each select='//IMPRESO'>";
		wvarStrXSL = wvarStrXSL
				+ "     <xsl:sort select='concat(CIAASCOD,RAMOPCOD,POLIZANN,POLIZSEC,CERTIPOL,CERTIANN,CERTISEC,OPERAPOL)' data-type='text' order='ascending'/>";
		wvarStrXSL = wvarStrXSL + " <xsl:if test='RAMOPCOD != \"\"' >";

		wvarStrXSL = wvarStrXSL + " <IMPRESO>";

		wvarStrXSL = wvarStrXSL
				+ "<AGRUPAR><xsl:value-of select='concat(CIAASCOD,RAMOPCOD,POLIZANN,POLIZSEC,CERTIPOL,CERTIANN,CERTISEC,OPERAPOL)'/></AGRUPAR>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CIAASCOD'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='RAMOPCOD'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='POLIZANN'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='POLIZSEC'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CERTIPOL'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CERTIANN'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CERTISEC'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='OPERAPOL'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='FORMUDES'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='DOCUMTIP'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='DOCUMDAT'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CLIENAP1'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CLIENAP2'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CLIENNOM'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='NOMARCH'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='SWSUSCRI'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='MAIL'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CLAVE'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='SW-CLAVE'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='SWENDOSO'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='OPERATIP'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='ESTADO'/>";
		wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CODESTAD'/>";
		wvarStrXSL = wvarStrXSL + " </IMPRESO>";
		wvarStrXSL = wvarStrXSL + " </xsl:if >";

		wvarStrXSL = wvarStrXSL + "   </xsl:for-each>";
		wvarStrXSL = wvarStrXSL + " </Request>";
		wvarStrXSL = wvarStrXSL + " </xsl:template>";
		wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
		//
		p_GetXSL = wvarStrXSL;
		return p_GetXSL;
	}

	private boolean insertScheduler(NODOINSERTASCH pvarNodo, String pvarFECHA)
			throws Exception {
		boolean insertScheduler = false;
		String wvarCodusu = "";
		String wvarAplicacion = "";
		String wvarDataIn = "";
		//
		//
		wvarAplicacion = "NBWS_ePoliza_Exe";

		wvarCodusu = "NBWSFILLSCH";
		wvarDataIn = "<CIAASCOD>" + pvarNodo.CIAASCOD + "</CIAASCOD>"
				+ "<RAMOPCOD>" + pvarNodo.RAMOPCOD + "</RAMOPCOD>"
				+ "<POLIZANN>" + pvarNodo.POLIZANN + "</POLIZANN>"
				+ "<POLIZSEC>" + pvarNodo.POLIZSEC + "</POLIZSEC>"
				+ "<CERTIPOL>" + pvarNodo.CERTIPOL + "</CERTIPOL>"
				+ "<CERTIANN>" + pvarNodo.CERTIANN + "</CERTIANN>"
				+ "<CERTISEC>" + pvarNodo.CERTISEC + "</CERTISEC>"
				+ "<OPERAPOL>" + pvarNodo.OPERAPOL + "</OPERAPOL>"
				+ "<DOCUMTIP>" + pvarNodo.DOCUMTIP + "</DOCUMTIP>"
				+ "<DOCUMDAT>" + pvarNodo.DOCUMDAT + "</DOCUMDAT>"
				+ "<CLIENAP1>" + pvarNodo.CLIENAP1 + "</CLIENAP1>"
				+ "<CLIENAP2>" + pvarNodo.CLIENAP2 + "</CLIENAP2>"
				+ "<CLIENNOM>" + pvarNodo.CLIENNOM + "</CLIENNOM>"
				+ "<ARCHIVOS>" + pvarNodo.NOMARCH + "</ARCHIVOS>"
				+ "<FORMUDES>" + pvarNodo.FORMUDES + "</FORMUDES>"
				+ "<SWSUSCRI>" + pvarNodo.SWSUSCRI + "</SWSUSCRI>" + "<MAIL>"
				+ pvarNodo.MAIL + "</MAIL>" + "<CLAVE>" + pvarNodo.CLAVE
				+ "</CLAVE>" + "<SW-CLAVE>" + pvarNodo.SWCLAVE + "</SW-CLAVE>"
				+ "<SWENDOSO>" + pvarNodo.SWENDOSO + "</SWENDOSO>"
				+ "<OPERATIP>" + pvarNodo.OPERATIP + "</OPERATIP>" + "<ESTADO>"
				+ pvarNodo.ESTADO + "</ESTADO>" + "<CODESTAD>"
				+ pvarNodo.CODESTAD + "</CODESTAD>";

		wvarDataIn = Strings.replace(wvarDataIn, "&", "");
		insertScheduler = invoke("insertBandejaSCH", new Variant[] {
				new Variant(wvarAplicacion), new Variant(wvarCodusu),
				new Variant(wvarDataIn), new Variant(pvarFECHA) });

		return insertScheduler;
	}

	private boolean insertBandejaSCH(String pvarAplicacion, String pvarCODUSU,
			String pvarDataIn, String pvarFECHA) throws Exception {
		boolean insertBandejaSCH = false;
		Variant gcteDBACTIONS = new Variant();
		diamondedge.util.XmlDom wobjXMLRequest = null;
		diamondedge.util.XmlDom wobjXMLConfig = null;
		Object wobjHSBC_DBCnn = null;
		Connection wobjDBCnn = null;
		Command wobjDBCmd = null;
		Parameter wobjDBParm = null;
		String wvarCodusu = "";
		String wvarAplicacion = "";
		String wvarDataIn = "";
		//
		//
		wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
		// error: function 'GetDBConnection' was not found.
		// unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDBACTIONS)
		wobjDBCmd = new Command();
		//
		wobjDBCmd.setActiveConnection(wobjDBCnn);
		wobjDBCmd.setCommandText(mcteStoreProcInsBandejaSCH);
		wobjDBCmd.setCommandType(AdoConst.adCmdStoredProc);
		//
		wobjDBParm = new Parameter("@RETURN_VALUE", AdoConst.adInteger,
				AdoConst.adParamReturnValue, 0, new Variant("0"));
		wobjDBCmd.getParameters().append(wobjDBParm);
		wobjDBParm = (Parameter) null;
		//
		wobjDBParm = new Parameter("@APLIC", AdoConst.adVarChar,
				AdoConst.adParamInput, 20, new Variant(pvarAplicacion));
		wobjDBCmd.getParameters().append(wobjDBParm);
		wobjDBParm = (Parameter) null;
		//
		wobjDBParm = new Parameter("@SOLICITUD", AdoConst.adInteger,
				AdoConst.adParamInput, 0, new Variant(0));
		wobjDBCmd.getParameters().append(wobjDBParm);
		wobjDBParm = (Parameter) null;
		//
		wobjDBParm = new Parameter("@PASO", AdoConst.adInteger,
				AdoConst.adParamInput, 0, new Variant(100));
		wobjDBCmd.getParameters().append(wobjDBParm);
		wobjDBParm = (Parameter) null;
		//
		wobjDBParm = new Parameter("@FECHAEXEC", AdoConst.adChar,
				AdoConst.adParamInput, 10, new Variant(pvarFECHA));
		wobjDBCmd.getParameters().append(wobjDBParm);
		wobjDBParm = (Parameter) null;
		//
		wobjDBParm = new Parameter("@DATAIN", AdoConst.adChar,
				AdoConst.adParamInput, 5000, new Variant(Strings.left(
						pvarDataIn, 5000)));
		wobjDBCmd.getParameters().append(wobjDBParm);
		wobjDBParm = (Parameter) null;
		//
		wobjDBParm = new Parameter("@COD_USU", AdoConst.adChar,
				AdoConst.adParamInput, 20, new Variant(pvarCODUSU));
		wobjDBCmd.getParameters().append(wobjDBParm);
		wobjDBParm = (Parameter) null;
		//
		//
		wobjDBCmd.execute(new Variant(AdoConst.adUnsupported), null,
				AdoConst.adCmdText);
		//
		// Controlamos la respuesta del SQL
		if (wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue()
				.toInt() >= 0) {
			insertBandejaSCH = true;
		} else {
			insertBandejaSCH = false;
		}
		//
		wobjHSBC_DBCnn = null;
		wobjDBCnn = (Connection) null;
		wobjDBCmd = (Command) null;
		//
		return insertBandejaSCH;
	}

	private Variant insertLog(String pvarCODOP, String pvarDescripcion)
			throws Exception {
		Variant insertLog = new Variant();
		diamondedge.util.XmlDom wobjXMLConfig = null;
		boolean wvarReqRespInTxt = false;
		int mvarDebugCode = 0;
		//
		//
		wvarReqRespInTxt = false;
		//
		wobjXMLConfig = new diamondedge.util.XmlDom();
		wobjXMLConfig.load(System.getProperty("user.dir") + "\\"
				+ mcteEPolizaConfig);
		// 1=Debug to BD - 2=Debug to BD and File sin request/Responses -
		// 3=Debug to BD andFile completo
		mvarDebugCode = Obj.toInt(diamondedge.util.XmlDom.getText(null /*
																		 * unsup
																		 * wobjXMLConfig
																		 * .
																		 * selectSingleNode
																		 * (
																		 * "//DEBUG"
																		 * )
																		 */));
		//
		if ((Strings.find(1, Strings.toUpperCase(pvarDescripcion), "<REQUEST>",
				true) > 0)
				|| (Strings.find(1, Strings.toUpperCase(pvarDescripcion),
						"<RESPONSE>", true) > 0)) {
			wvarReqRespInTxt = true;
		}
		//
		if ((mvarDebugCode >= 1) && !(wvarReqRespInTxt)) {
			invoke("debugToBD", new Variant[] { new Variant(pvarCODOP),
					new Variant(pvarDescripcion) });
		}
		if ((mvarDebugCode == 3)
				|| ((mvarDebugCode == 2) && !(wvarReqRespInTxt))) {
			invoke("debugToFile", new Variant[] { new Variant(pvarCODOP),
					new Variant(pvarDescripcion) });
		}
		//
		return insertLog;
	}

	private boolean debugToBD(String pvarCODOP, String pvarDescripci�n)
			throws Exception {
		boolean debugToBD = false;
		Variant gcteDBLOG = new Variant();
		diamondedge.util.XmlDom wobjXMLRequest = null;
		diamondedge.util.XmlDom wobjXMLConfig = null;
		Object wobjHSBC_DBCnn = null;
		Connection wobjDBCnn = null;
		Command wobjDBCmd = null;
		Parameter wobjDBParm = null;
		//
		//
		wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
		// error: function 'GetDBConnection' was not found.
		// unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDBLOG)
		wobjDBCmd = new Command();
		//
		wobjDBCmd.setActiveConnection(wobjDBCnn);
		wobjDBCmd.setCommandText(mcteStoreProcInsLog);
		wobjDBCmd.setCommandType(AdoConst.adCmdStoredProc);
		//
		wobjDBParm = new Parameter("@RETURN_VALUE", AdoConst.adInteger,
				AdoConst.adParamReturnValue, 0, new Variant("0"));
		wobjDBCmd.getParameters().append(wobjDBParm);
		wobjDBParm = (Parameter) null;
		//
		wobjDBParm = new Parameter("@CODOP", AdoConst.adChar,
				AdoConst.adParamInput, 6, new Variant(
						Strings.left(pvarCODOP, 6)));
		wobjDBCmd.getParameters().append(wobjDBParm);
		wobjDBParm = (Parameter) null;
		//
		wobjDBParm = new Parameter("@DESCRIPCION", AdoConst.adChar,
				AdoConst.adParamInput, 100, new Variant(Strings.left(
						pvarDescripci�n, 100)));
		wobjDBCmd.getParameters().append(wobjDBParm);
		wobjDBParm = (Parameter) null;
		//
		wobjDBCmd.execute(new Variant(AdoConst.adUnsupported), null,
				AdoConst.adCmdText);
		//
		// Controlamos la respuesta del SQL
		if (wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue()
				.toInt() >= 0) {
			debugToBD = true;
		} else {
			debugToBD = false;
		}
		//
		wobjHSBC_DBCnn = null;
		wobjDBCnn = (Connection) null;
		wobjDBCmd = (Command) null;
		//
		return debugToBD;
	}

	private void debugToFile(String pvarCODOP, String pvarDescripcion)
			throws Exception {
		diamondedge.util.XmlDom wobjXMLConfig = null;
		String wvarText = "";
		String wvarFileName = "";
		int wvarNroArch = 0;
		//
		//
		wvarText = "(" + DateTime.now() + "-" + DateTime.now() + "):"
				+ pvarDescripcion;

		wvarFileName = "debug-" + pvarCODOP + "-"
				+ DateTime.year(DateTime.now())
				+ Strings.right(("00" + DateTime.month(DateTime.now())), 2)
				+ Strings.right(("00" + DateTime.day(DateTime.now())), 2)
				+ ".log";
		wvarNroArch = FileSystem.getFreeFile();
		FileSystem.openAppend(System.getProperty("user.dir") + "\\DEBUG\\"
				+ wvarFileName, 1);
		FileSystem.out(1).writeValue(wvarText);
		FileSystem.out(1).println();
		FileSystem.close(1);
		//
	}

	private void ObjectControl_Activate() throws Exception {
		Connection wobjDBCnn = null;
		Command wobjDBCmd = null;
		Recordset wrstDBResult = null;
		Parameter wobjDBParm = null;
	}

	private boolean ObjectControl_CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		return ObjectControl_CanBePooled;
	}

	private void ObjectControl_Deactivate() throws Exception {
	}

	public void Activate() throws Exception {
		Connection wobjDBCnn = null;
		Command wobjDBCmd = null;
		Recordset wrstDBResult = null;
		Parameter wobjDBParm = null;

		mobjCOM_Context = null /* unsup this.GetObjectContext() */;
		mobjEventLog = new HSBC.EventLog();
		//
	}

	public boolean CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		//
		ObjectControl_CanBePooled = true;
		//
		return ObjectControl_CanBePooled;
	}

	public void Deactivate() throws Exception {
		//
		mobjCOM_Context = (Object) null;
		mobjEventLog = null;
		//
	}
}
