import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class TEMPLATEMAIL implements Cloneable
{
  public String AUSSYSPASS = "";
  public String HOMSYSPASS = "";
  public String NYLSYSPASS = "";
  public String AUSUSRPASS = "";
  public String HOMUSRPASS = "";
  public String NYLUSRPASS = "";

  public Object clone()
  {
    try 
    {
      TEMPLATEMAIL clone = (TEMPLATEMAIL) super.clone();
      //todo: make clones of any members that are objects ie. clone.obj = obj.clone();
      return clone;
    } 
    catch( CloneNotSupportedException e ) {}
    return null;
  }
}
