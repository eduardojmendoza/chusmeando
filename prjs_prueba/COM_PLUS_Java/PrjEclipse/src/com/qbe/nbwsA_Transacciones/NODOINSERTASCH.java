import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class NODOINSERTASCH implements Cloneable
{
  public String CIAASCOD = "";
  public String RAMOPCOD = "";
  public String POLIZANN = "";
  public String POLIZSEC = "";
  public String CERTIPOL = "";
  public String CERTIANN = "";
  public String CERTISEC = "";
  public String OPERAPOL = "";
  public String DOCUMTIP = "";
  public String DOCUMDAT = "";
  public String CLIENAP1 = "";
  public String CLIENAP2 = "";
  public String CLIENNOM = "";
  public String NOMARCH = "";
  public String FORMUDES = "";
  public String SWSUSCRI = "";
  public String MAIL = "";
  public String CLAVE = "";
  public String SWCLAVE = "";
  public String SWENDOSO = "";
  public String OPERATIP = "";
  public String ESTADO = "";
  public String CODESTAD = "";

  public Object clone()
  {
    try 
    {
      NODOINSERTASCH clone = (NODOINSERTASCH) super.clone();
      //todo: make clones of any members that are objects ie. clone.obj = obj.clone();
      return clone;
    } 
    catch( CloneNotSupportedException e ) {}
    return null;
  }
}
