import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 * TODO: poner COPYRIGHT
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_getPassword implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_getPassword";
  static final String mcteParam_EMAIL = "//EMAIL";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   *  *****************************************************************
   */
  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLDoc = null;
    org.w3c.dom.Element oNodo = null;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    Object wobjClass = null;
    String wvarEMAIL = "";
    String wvarPASSWORD = "";
    String wvarEstadoPassword = "";
    String wvarDescripcion = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Carga XML de entrada
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      wvarEMAIL = Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMAIL ) */ ) );
      //
      wvarStep = 30;
      wvarRequest = "<Request><DEFINICION>P_NBWS_ObtenerPassword.xml</DEFINICION><MAIL>" + wvarEMAIL + "</MAIL></Request>";
      //
      wvarStep = 40;
      wobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
      wobjClass = null;
      //
      wvarStep = 50;
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( wvarResponse );
      //
      if( ! (null /*unsup wobjXMLResponse.selectSingleNode( "//PASSWORD" ) */ == (org.w3c.dom.Node) null) )
      {
        wvarStep = 60;
        wvarPASSWORD = invoke( "CapicomDecrypt", new Variant[] { new Variant(diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( new Variant("//PASSWORD") ) */ )) } ).toString();
        wvarEstadoPassword = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//ESTADOPASSWORD" ) */ );
        wvarDescripcion = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//DESCRIPCION" ) */ );
      }
      else
      {
        wvarStep = 70;
        Err.getError().setDescription( "No se encontraron datos en el SQL o pinch� el COM+ de SQL Generico" );
        //unsup GoTo ErrorHandler
      }
      //
      wvarStep = 80;
      //
      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"></Estado>" + "<PASSWORD>" + wvarPASSWORD + "</PASSWORD><ESTADOPASSWORD>" + wvarEstadoPassword + "</ESTADOPASSWORD><DESCRIPCION>" + wvarDescripcion + "</DESCRIPCION></Response>" );
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLResponse = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 90;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = (diamondedge.util.XmlDom) null;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        /*unsup mobjCOM_Context.SetAbort() */;
        IAction_Execute = 1;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
