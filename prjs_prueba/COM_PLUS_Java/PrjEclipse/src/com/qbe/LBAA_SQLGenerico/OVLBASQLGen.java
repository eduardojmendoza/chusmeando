package com.qbe.LBAA_SQLGenerico;

import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;

import com.snoopconsulting.qbe.VBObjectClass;

import diamondedge.swing.*;
/**
 * ******************************************************************************
 * COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
 * LIMITED 2012. ALL RIGHTS RESERVED
 * This software is only to be used for the purpose for which it has been provided.
 * No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
 * system or translated in any human or computer language in any way or for any other
 * purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
 * Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
 * offence, which can result in heavy fines and payment of substantial damages
 * ******************************************************************************
 * Nombre del Modulo: OVLBASQLGen
 * Fecha de Creaci�n: 04/01/2012
 * PPcR: 2011-00390
 * Desarrollador: Leonardo Ruiz
 * Descripci�n:  Componente Generico de SQL, version 2
 * ******************************************************************************
 * LR 14/01/2009 Se agrega un campo Opcional "CANT_REG_RESP" q devuelve una X cantidad de
 * registros para mostrar
 */

public class OVLBASQLGen extends VBObjectClass
{
  static final String mcteClassName = "LBAA_SQLGenerico.OVLBASQLGen";
  static final String mcteSubDirName = "DEFINICIONES";
  static final String mcteLogPath = "LogSQL";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private String mvarConsultaRealizada = "";
  private String mvarStrCmdExec = "";
  /**
   * XML con constantes de ADO para tipos de dato
   */
  private diamondedge.util.XmlDom wobjCnstADO = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: generarCommand
   * static variable for method: generarXMLSalida
   * static variable for method: etiquetar
   */
  private final String wcteFnName = "etiquetar";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLOldLog = null;
    diamondedge.util.XmlDom wobjXMLLog = null;
    int mvarCount = 0;
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLDefinition = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    org.w3c.dom.NodeList wobjXMLRecordsets = null;
    org.w3c.dom.Node wobjNodoRec = null;
    String wvarDefinitionFile = "";
    String wvarStoredProcedure = "";
    String wvarTipoSP = "";
    String wvarUDL = "";
    int wvarCANT_REG_RESP = 0;
    String wobjSalida = "";
    HSBC.DBConnection wobjHSBC_DBCnn = new HSBC.DBConnection();
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wobjRecordset = null;
    String wvarPathLog = "";
    java.util.Date wvarFechaConsulta = DateTime.EmptyDate;
    int wvarCantLlamadasLogueadas = 0;
    int wvarCantLlamadasALoguear = 0;
    int wvarCounter = 0;

    //XML de definici�n
    //XML con el request
    //Nodos descriptores de recordsets
    //Nodo descriptor de un recordset
    //nombre del archivo XML de definici�n
    //nombre del stored procedure
    //tipo de store procedure. C (consulta) o ABM
    //nombre del archivo UDL
    //LR 14/01/2009 campo Opcional q devuelve una X cantidad de registros para mostrar
    //String de salida
    //interface de conexi�n a la base de datos
    //conexi�n a la base de datos
    //objeto recordset
    //Para LOGs
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarFechaConsulta = DateTime.now();
      //
      //carga del REQUEST
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );

      //Carga del XML de definici�n
      wvarStep = 20;
      wobjXMLDefinition = new diamondedge.util.XmlDom();
      wvarDefinitionFile = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//DEFINICION" ) */ );
      mvarConsultaRealizada = Strings.left( wvarDefinitionFile, Strings.len( wvarDefinitionFile ) - 4 );
      //unsup wobjXMLDefinition.async = false;
      wobjXMLDefinition.load( System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + wvarDefinitionFile );

      //Carga del nombre del SP
      wvarStep = 30;
      wvarStoredProcedure = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/NOMBRE" ) */ );

      //Carga del tipo de SP
      wvarStep = 40;
      wvarTipoSP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/TIPO" ) */ );

      //Carga del UDL
      wvarStep = 50;
      wvarUDL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/UDL" ) */ );

      //LR 14/01/2009 Se agrega campo Opcional: CANT_REG_RESP
      wvarStep = 55;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//Request/CANT_REG_RESP" ) */ == (org.w3c.dom.Node) null) )
      {
        //Compruebo q venga un nro o sino le muevo un cero
        if( new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//CANT_REG_RESP" ) */ ) ).isNumeric() )
        {
          wvarCANT_REG_RESP = Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//CANT_REG_RESP" ) */ ) );
        }
        else
        {
          //Cero significa q no va tomar en cuenta esta logica de cant de Reg devueltos
          wvarCANT_REG_RESP = 0;
        }
      }
      else
      {
        //Cero significa q no va tomar en cuenta esta logica de cant de Reg devueltos
        wvarCANT_REG_RESP = 0;
      }

      //selecci�n del tipo de interface de conexi�n dependiendo de si el SP hace altas, modificaciones o bajas o no
      wvarStep = 60;
      if( wvarTipoSP.equals( "ABM" ) )
      {
        wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
      }
      else
      {
        wobjHSBC_DBCnn = new HSBC.DBConnection();
      }

      //conexi�n a la base de datos
      wvarStep = 70;
      wobjDBCnn = (Connection) (Connection)( wobjHSBC_DBCnn.GetDBConnection( wvarUDL ) );


      //'''''''''''''''''''''''''''
      //armado de la llamada al SP'
      //'''''''''''''''''''''''''''
      wvarStep = 80;
      wobjDBCmd = new Command();
      wobjDBCmd = invoke( "generarCommand", new Variant[] { new Variant(wobjDBCnn), new Variant(wvarStoredProcedure), new Variant(null /*unsup wobjXMLDefinition.selectNodes( new Variant("//ENTRADA/PARAMETRO") ) */), new Variant(null /*unsup wobjXMLRequest.selectSingleNode( new Variant("Request") ) */) } );


      //'''''''''''''''''
      //ejecuci�n del SP'
      //'''''''''''''''''
      wvarStep = 90;
      wobjRecordset = wobjDBCmd.execute();



      //''''''''''''''''''''
      //armado de la salida'
      //''''''''''''''''''''
      //Carga del nodos descriptores de recordsets
      wvarStep = 100;
      wobjXMLRecordsets = null /*unsup wobjXMLDefinition.selectNodes( "//SALIDA/RECORDSET" ) */;

      wvarStep = 110;
      wobjSalida = "";

      //LR 14/01/2009 Se agrega contador para la cant de Reg devueltos
      mvarCount = 0;

      //por cada recordset definido en el XML de definici�n procesa un recordset devuelto por la consulta
      for( int nwobjNodoRec = 0; nwobjNodoRec < wobjXMLRecordsets.getLength(); nwobjNodoRec++ )
      {
        wobjNodoRec = wobjXMLRecordsets.item( nwobjNodoRec );
        //si hay menos recordsets que los indicados en la definici�n tira un error
        if( wobjRecordset == (Recordset) null )
        {
          Err.raise( 2504, "", "ERROR EN XML DE DEFINICION - se definieron m�s recordsets que la cantidad devuelta por el stored procedure" );
        }

        //LR 14/01/2009 le mando el param con wvarCANT_REG_RESP
        wobjSalida = wobjSalida + invoke( "generarXMLSalida", new Variant[] { new Variant(wobjRecordset)/*warning: ByRef value change will be lost.*/, new Variant(wobjNodoRec), new Variant(wvarCANT_REG_RESP) } );
        //wobjSalida = wobjSalida & generarXMLSalida(wobjRecordset, wobjNodoRec)
        wobjRecordset = wobjRecordset.nextRecordset();
      }

      wvarStep = 120;
      if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//RETORNARREQUEST" ) */ == (org.w3c.dom.Node) null) )
      {
        wobjSalida = wobjSalida + Request;
      }

      wvarStep = 130;
      Response.set( invoke( "etiquetar", new Variant[] { new Variant("Response"), new Variant(wobjSalida) } ) );

      // ****************************************************************
      //Generamos un Log en caso que se solicite
      // ****************************************************************
      //
      if( ! ((null /*unsup wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) */ == (org.w3c.dom.Node) null)) || ! ((null /*unsup wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) */ == (org.w3c.dom.Node) null)) )
      {
        //
        wvarStep = 135;
        wvarCantLlamadasALoguear = 1;
        if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "CantLlamadasALoguear" ) == (org.w3c.dom.Node) null) )
        {
          wvarCantLlamadasALoguear = Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "CantLlamadasALoguear" ) ) );
        }
        //
        wvarStep = 140;
        wvarPathLog = System.getProperty("user.dir") + "\\" + mcteLogPath + "\\" + wvarDefinitionFile;
        //
        wvarStep = 150;
        if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) */ == (org.w3c.dom.Node) null) )
        {
          if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
          {
            wvarPathLog = System.getProperty("user.dir") + "\\" + mcteLogPath + "\\" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "LogFile" ) );
          }
        }
        else
        {
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
          {
            wvarPathLog = System.getProperty("user.dir") + "\\" + mcteLogPath + "\\" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) */.getAttributes().getNamedItem( "LogFile" ) );
          }
        }
        //
        wvarStep = 160;
        if( ! (wvarPathLog.matches( "*\\" )) )
        {

          //Genero el Log solo si en el nodo se especifico un nombre de archivo
          wobjXMLLog = new diamondedge.util.XmlDom();
          //unsup wobjXMLLog.async = false;
          //
          wvarStep = 170;
          wobjXMLLog.loadXML( "<LOGs><LOG StoredProcedure='" + wvarStoredProcedure + "' InicioConsulta='" + DateTime.format( wvarFechaConsulta, "dd/MM/yyyy HH:mm:ss" ) + "' TiempoIncurrido = '" + (int)Math.rint( (DateTime.diff( wvarFechaConsulta, DateTime.now() ) * 86400) ) + " Seg' >" + Request + Response + "</LOG></LOGs>" );
          //
          wvarStep = 180;
          //
          if( !"" /*unsup this.Dir( wvarPathLog, 0 ) */.equals( "" ) )
          {
            wvarStep = 190;
            //Ya existe el Log, entonces agrego la nueva consulta al LOG
            wobjXMLOldLog = new diamondedge.util.XmlDom();
            //unsup wobjXMLOldLog.async = false;
            wobjXMLOldLog.load( wvarPathLog );
            wvarStep = 200;
            if( null /*unsup wobjXMLOldLog.selectSingleNode( "//LOGs" ) */ == (org.w3c.dom.Node) null )
            {
              // *********LOG NO VALIDO***********
              wobjXMLLog.save( wvarPathLog );
            }
            else
            {
              //
              //DA
              wvarStep = 210;
              wvarCantLlamadasLogueadas = null /*unsup wobjXMLOldLog.selectSingleNode( "//LOGs" ) */.getChildNodes().getLength();
              // para que pase
              // For wvarCounter = 1 To wvarCantLlamadasLogueadas - wvarCantLlamadasALoguear + 1
              //    wobjXMLOldLog.selectSingleNode("//LOGs").removeChild wobjXMLOldLog.selectSingleNode("//LOGs").childNodes(0)
              // Next wvarCounter
              //
              //DA
              wvarStep = 220;
              // para que pase
              // wobjXMLOldLog.selectSingleNode("//LOGs").appendChild wobjXMLLog.selectSingleNode("//LOGs").childNodes(0)
              //DA
              wvarStep = 225;
              wobjXMLOldLog.save( wvarPathLog );
              //
            }
          }
          else
          {
            wvarStep = 230;
            wobjXMLLog.save( wvarPathLog );
          }
        }
      }

      // ****************************************************************
      //FIN generaci�n de LOG
      // ****************************************************************
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjRecordset = (Recordset) null;
        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = (HSBC.DBConnection) null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private Command generarCommand( Connection pobjDBCnn, String pobjStoredProcedure, org.w3c.dom.NodeList pobjNodos, org.w3c.dom.Node pobjXMLRequest )
  {
    Command generarCommand = null;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Command wobjCommand = null;
    Parameter wobjParameter = null;
    org.w3c.dom.Node wobjNodeCampo = null;
    String wvarNombrePrm = "";
    int wvarTipoPrm = 0;
    String wvarValuePrm = "";

    //Command a crear
    //parametro
    // Nodo con campo de entrada
    //Nombre de un par�metro
    //Tipo de un par�metro
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarStep = 10;

      wobjCommand = new Command();
      wobjCommand.setActiveConnection( pobjDBCnn );

      wobjCommand.setCommandText( pobjStoredProcedure );
      wobjCommand.setCommandType( AdoConst.adCmdStoredProc );

      //Se almacena la cadena de ejecucion
      mvarStrCmdExec = pobjStoredProcedure + Strings.space( 1 );

      //por cada campo de entrada se crea un par�metro y se agrega al Command
      for( int nwobjNodeCampo = 0; nwobjNodeCampo < pobjNodos.getLength(); nwobjNodeCampo++ )
      {
        wobjNodeCampo = pobjNodos.item( nwobjNodeCampo );

        wvarStep = 20;
        wvarNombrePrm = diamondedge.util.XmlDom.getText( wobjNodeCampo.getAttributes().getNamedItem( "Nombre" ) );

        wvarStep = 30;
        wvarTipoPrm = Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjCnstADO.selectSingleNode( "//DATATYPE[@name=\"" + diamondedge.util.XmlDom.getText( wobjNodeCampo.getAttributes().getNamedItem( "Type" ) ) + "\"]" ) */ ) );
        wobjParameter = new Parameter( "", AdoConst.adInteger, AdoConst.adParamInput, 0, null );
        wobjParameter.setName( "@" + wvarNombrePrm );
        wobjParameter.setType( wvarTipoPrm );
        wobjParameter.setDirection( AdoConst.adParamInput );

        wvarStep = 40;

        if( ! (wobjNodeCampo.getAttributes().getNamedItem( "Xml" ) == (org.w3c.dom.Node) null) )
        {
          wvarValuePrm = null /*unsup pobjXMLRequest.selectSingleNode( wvarNombrePrm ) */.toString();
        }
        else
        {
          if( null /*unsup pobjXMLRequest.selectSingleNode( wvarNombrePrm ) */ == (org.w3c.dom.Node) null )
          {
            wvarValuePrm = diamondedge.util.XmlDom.getText( wobjNodeCampo.getAttributes().getNamedItem( "Default" ) );
          }
          else
          {
            wvarValuePrm = diamondedge.util.XmlDom.getText( null /*unsup pobjXMLRequest.selectSingleNode( wvarNombrePrm ) */ );
          }
        }

        //Actual manera de agregar par�metros
        
        if( wvarTipoPrm == 129 || wvarTipoPrm == 200 || wvarTipoPrm == 201 )
        {
          //adodb.adVarChar adodb.adChar adLongVarChar(text)
          mvarStrCmdExec = mvarStrCmdExec + "'" + wvarValuePrm + "',";
        }
        else
        {
          mvarStrCmdExec = mvarStrCmdExec + wvarValuePrm + ",";
        }

        //los tipos de dato num�rico y decimal (131 y 14) requieren determinar
        //Precision y NumericScale. Los demas requieres determinar Size
        if( (wvarTipoPrm == this.getValue( "ADODB.adNumeric" ).toInt()) || (wvarTipoPrm == this.getValue( "ADODB.adDecimal" ).toInt()) )
        {
          wvarStep = 50;
          wobjParameter.setPrecision( (byte)Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodeCampo.getAttributes().getNamedItem( "Precision" ) ) ) );

          wvarStep = 60;
          wobjParameter.setScale( (byte)Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodeCampo.getAttributes().getNamedItem( "Scale" ) ) ) );

        }
        else
        {

          //Tipo text
          if( wvarTipoPrm == this.getValue( "ADODB.adLongVarChar" ).toInt() )
          {
            wvarStep = 70;
            wobjParameter.setSize( Strings.len( wvarValuePrm ) );
          }
          else
          {
            wvarStep = 75;
            wobjParameter.setSize( Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodeCampo.getAttributes().getNamedItem( "Size" ) ) ) );
          }

        }

        //Tipo text
        if( wvarTipoPrm == this.getValue( "ADODB.adLongVarChar" ).toInt() )
        {
          wvarStep = 80;
          wobjParameter.appendChunk( new Variant( wvarValuePrm ) );
        }
        else
        {
          wvarStep = 85;
          wobjParameter.setValue( new Variant( wvarValuePrm ) );
        }

        wvarStep = 90;
        wobjCommand.getParameters().append( wobjParameter );
      }


      //Se elimina el ultimo caracter(coma) en la cadena de ejecucion
      wvarStep = 100;
      mvarStrCmdExec = Strings.left( mvarStrCmdExec, Strings.len( mvarStrCmdExec ) - 1 );

      generarCommand = wobjCommand;

      return generarCommand;

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Parametro: " + wvarNombrePrm, vbLogEventTypeError );

        wobjCommand = (Command) null;

        generarCommand = (Command) null;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return generarCommand;
  }

  private String generarXMLSalida( Recordset pobjRecordset, org.w3c.dom.Node pobjNodo, int wvarCANT_REG_RESP )
  {
    String generarXMLSalida = "";
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    org.w3c.dom.NodeList wobjNodosCampos = null;
    org.w3c.dom.Node wobjNodoCampo = null;
    String wobjNomRecordset = "";
    String wobjNomFila = "";
    String wobjNomCampo = "";
    Field wobjCampo = null;
    String wobjAux = "";
    diamondedge.util.XmlDom wobjAuxRegs = null;
    int wvari = 0;
    int mvarCount = 0;
    diamondedge.util.XmlDom wobjNodo = null;


    //Etiqueta de salida para un recordset
    //Etiqueta de salida para una fila
    //Etiqueta de salida para un campo
    //campo de un recordset
    //almacena temporalmente la salida
    //Dim wobjAuxRegs                As String 'almacena temporalmente la salida // REGISTROS
    //almacena temporalmente la salida // REGISTROS
    //contador
    //contador
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wobjAux = "";
      //LR 14/01/2009 Se agrega contador para la cant de Reg devueltos
      mvarCount = 0;

      wobjNomRecordset = diamondedge.util.XmlDom.getText( pobjNodo.getAttributes().getNamedItem( "Nombre" ) );
      wobjNomFila = diamondedge.util.XmlDom.getText( pobjNodo.getAttributes().getNamedItem( "NombreFila" ) );

      wobjNodosCampos = null /*unsup pobjNodo.selectNodes( "//RECORDSET[@Nombre=\"" + wobjNomRecordset + "\"]/CAMPO" ) */;

      wvarStep = 10;

      //02/11/2011 LR Crear obj para acumular
      wobjAuxRegs = new diamondedge.util.XmlDom();
      //unsup wobjAuxRegs.async = false;
      wobjAuxRegs.loadXML( "<" + wobjNomRecordset + "/>" );

      wobjNodo = new diamondedge.util.XmlDom();
      //unsup wobjNodo.async = false;

      //procesa cada registro del recordset
      while( ! (pobjRecordset.isEOF()) )
      {
        //si no coincide la cantidad de campos del recordset con la cantidad de campos definidas en el XML tira error
        if( wobjNodosCampos.getLength() != pobjRecordset.getFields().getCount() )
        {
          Err.raise( 2504, "", "ERROR EN XML DE DEFINICION - No coincide la cantidad de campos devueltos por el stored procedure (" + pobjRecordset.getFields().getCount() + " campos) con la cantidad de campos definidos en el XML (" + wobjNodosCampos.getLength() + " campos)" );
        }

        wvarStep = 20;
        wvari = 0;
        //procesa cada campo del registro
        for( int nwobjCampo = 0; nwobjCampo < pobjRecordset.getFields().getCount(); nwobjCampo++ )
        {
          wobjCampo = pobjRecordset.getFields().getField(nwobjCampo);
          wobjNodoCampo = wobjNodosCampos.item( wvari );
          //si debe mostrarse el campo, muestra su valor con las etiquetas indicadas
          if( wobjNodoCampo.getAttributes().getNamedItem( "OCULTO" ) == (org.w3c.dom.Node) null )
          {
            if( wobjNodoCampo.getAttributes().getNamedItem( "CDATA" ) == (org.w3c.dom.Node) null )
            {
              wobjAux = wobjAux + invoke( "etiquetar", new Variant[] { new Variant(diamondedge.util.XmlDom.getText( wobjNodoCampo.getAttributes().getNamedItem( new Variant("Nombre") ) )), new Variant(wobjCampo.getValue().toString()) } );
            }
            else
            {
              wobjAux = wobjAux + invoke( "etiquetar", new Variant[] { new Variant(diamondedge.util.XmlDom.getText( wobjNodoCampo.getAttributes().getNamedItem( new Variant("Nombre") ) )), new Variant("<![CDATA[" + wobjCampo.getValue() + "]]>") } );
            }
          }
          wvari = wvari + 1;
        }

        wvarStep = 30;
        //wobjAuxRegs = wobjAuxRegs + etiquetar(wobjNomFila, wobjAux)
        wobjNodo.loadXML( "<" + wobjNomFila + ">" + wobjAux + "</" + wobjNomFila + ">" );

        wvarStep = 35;
        // para que pase
        // Call wobjAuxRegs.selectSingleNode("//" & wobjNomRecordset).appendChild(wobjNodo.selectSingleNode("//" & wobjNomFila))
        wvarStep = 36;
        wobjAux = "";

        //LR 14/01/2009 Se agrega logica para salir de For si se ingresa un campo de cant de limitacion en la resp de Reg
        if( wvarCANT_REG_RESP != 0 )
        {
          mvarCount = mvarCount + 1;
          if( wvarCANT_REG_RESP <= mvarCount )
          {
            // Salir del loop hasta esta cant de reg.
            break;
          }
        }

        pobjRecordset.moveNext();
      }

      wvarStep = 40;

      generarXMLSalida = "<Estado resultado=\"true\"/>" + wobjAuxRegs.getDocument().getDocumentElement().toString();

      return generarXMLSalida;

      //~~~~~~~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjAuxRegs = (diamondedge.util.XmlDom) null;
        wobjNodo = (diamondedge.util.XmlDom) null;
        //warning: modifying ByRef argument 'pobjRecordset'. It must be a Variant.
        pobjRecordset = (Recordset) null;
        wobjCampo = (Field) null;
        generarXMLSalida = "<Response><Estado resultado='false'></Response>";
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return generarXMLSalida;
  }

  private String etiquetar( String pobjEtiqueta, String pvarContenido )
  {
    String etiquetar = "";
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarStep = 10;
      etiquetar = "<" + pobjEtiqueta + ">" + pvarContenido + "</" + pobjEtiqueta + ">";

      return etiquetar;

      //~~~~~~~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        etiquetar = "1";
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return etiquetar;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    wobjCnstADO = new diamondedge.util.XmlDom();
    //unsup wobjCnstADO.async = false;
    wobjCnstADO.load( System.getProperty("user.dir") + "\\constantesADO.xml" );
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
