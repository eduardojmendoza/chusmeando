package com.snoopconsulting.qbe;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import diamondedge.util.XmlDom;

public class XmlDomExtended extends XmlDom {

	public NodeList selectNodes( String xpathExpr) {

		try {
			Document doc = this.getDocument(); 
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile(xpathExpr);
			//Para obtener un conjunto de nodos:
			NodeList nl = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			return nl;
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public Node selectSingleNode(String xpathExpr) {
		try {
			Document doc = this.getDocument(); 
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile(xpathExpr);
			//Para obtener un conjunto de nodos:
			Node nl = (Node) expr.evaluate(doc, XPathConstants.NODE);
			return nl;
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static Node Node_selectSingleNode(Node node, String xpathExpr) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();
			doc.appendChild(node); // TODO Revisar si esto no lo saca del doc original, no s� si tiene ese efecto lateral
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile(xpathExpr);
			//Para obtener un conjunto de nodos:
			Node nl = (Node) expr.evaluate(doc, XPathConstants.NODE);
			return nl;
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public Object transformNode(String xslFileName) {
	    TransformerFactory tFactory = TransformerFactory.newInstance();
	    
	    try {
			Transformer transformer =
			  tFactory.newTransformer
			     (new StreamSource(
			        (new FileInputStream(xslFileName))));

			StringWriter sw = new StringWriter();
			transformer.transform
			  ( new DOMSource(this.getDocument()),
			   new StreamResult(
			        ( sw)));
			return sw.toString();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "ERROR";
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "ERROR";
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "ERROR";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "ERROR";
		}
	}

	public Object transformNode(XmlDomExtended mobjLocXSL) {
	    TransformerFactory tFactory = TransformerFactory.newInstance();
	    
	    try {
			Transformer transformer =
			  tFactory.newTransformer
			     (new DOMSource(mobjLocXSL.getDocument()));

			StringWriter sw = new StringWriter();
			transformer.transform
			  ( new DOMSource(this.getDocument()),
			   new StreamResult(
			        ( sw)));
			return sw.toString();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "ERROR";
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "ERROR";
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "ERROR";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "ERROR";
		}
	}


}
