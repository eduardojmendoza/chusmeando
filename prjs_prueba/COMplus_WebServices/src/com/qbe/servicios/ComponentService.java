package com.qbe.servicios;


public interface ComponentService {

	public Response executeRequest(String xmlRequest, String contextInfo);

}