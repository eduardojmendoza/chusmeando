package com.qbe.servicios.mqgenerico;

import javax.jws.WebService;

import com.qbe.servicios.ComponentService;
import com.qbe.servicios.Response;

@WebService(targetNamespace = "http://mqgenerico.servicios.qbe.com/", endpointInterface = "com.qbe.servicios.mqgenerico.MQGenerico", portName = "MQGenericoImplPort", serviceName = "MQGenericoImplService")
public class MQGenericoImpl implements ComponentService, MQGenerico {

	public Response executeRequest(String xmlRequest, String contextInfo) {
		  return new Response(42, "<pepe/>");
	  }
}
