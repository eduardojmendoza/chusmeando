
package com.qbe.servicios.mqgenerico;

import javax.jws.WebParam.Mode;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.qbe.servicios.Response;

/**
 * This class was generated by Apache CXF 2.6.1
 * 2013-04-18T19:19:17.673-03:00
 * Generated source version: 2.6.1
 * 
 */
@WebService
public interface MQGenerico {

	public com.qbe.servicios.Response executeRequest(
        java.lang.String arg0,
        java.lang.String arg1
    );

//	@WebMethod(operationName = "executeRequest", action = "urn:ExecuteRequest")
//	@RequestWrapper(className = "com.qbe.servicios.mqgenerico.jaxws.ExecuteRequest", localName = "executeRequest", targetNamespace = "http://mqgenerico.servicios.qbe.com/")
//	@ResponseWrapper(className = "com.qbe.servicios.mqgenerico.jaxws.ExecuteRequestResponse", localName = "executeRequestResponse", targetNamespace = "http://mqgenerico.servicios.qbe.com/")
	public Response executeRequest(@WebParam(name = "arg0") String xmlRequest, @WebParam(name = "arg1") String contextInfo);

}