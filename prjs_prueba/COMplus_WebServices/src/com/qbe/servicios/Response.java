package com.qbe.servicios;

public class Response {
	
	private int resultCode;
	private String result;
	public Response(){
		
	}
	public Response(int resultCode, String response) {
		this.result = response;
		this.resultCode = resultCode;
	}
	public int getResultCode() {
		return resultCode;
	}
	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}
	public String getResponse() {
		return result;
	}
	public void setResponse(String response) {
		this.result = response;
	}

}
