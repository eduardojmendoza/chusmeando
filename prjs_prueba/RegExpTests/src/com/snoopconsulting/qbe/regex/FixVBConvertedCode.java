package com.snoopconsulting.qbe.regex;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

public class FixVBConvertedCode {
	
	/**
	 * Recibe una linea de c�digo.
	 * Si encuenta una expresi�n selectSingleNode, la convierte
	 * 
	 * @param line
	 * @return
	 */
	public String convertSelectSingleNode(String line) {
		final String delimiter = "selectSingleNode(";
		return removeUnsup(line, delimiter, "null ");

	}

	/**
	 * Recibe una linea de c�digo.
	 * Si encuenta una expresi�n
	 * new Variant() /\*unsup wobjXMLRespuestas.transformNode( wobjXSLSalida ) *\/
	 * le saca el /\* unsup y el del final
	 * @param line
	 * @return
	 */
	public String convertTransformNode(String line) {
		final String delimiter = "transformNode(";
		return removeUnsup(line, delimiter, "new Variant() ");
	}

	
	/**
	 * Limpia la line que recibe como par�metro de expresiones "unsup" que genera el conversor de VB a Java
	 * Borra el \/\* unsup inicial y el \*\/ final para cada par que encuentra
	 * Si hay un unsupPrefix tambi�n lo elimina
	 * 
	 * @param line
	 * @param delimiter
	 * @param unsupPrefix prefijo del unsup. Se toma tal cual, o sea es un String NO una regexp
	 * @return
	 */
	protected String removeUnsup(String line, final String delimiter, final String unsupPrefix) {

		final String unsupexpr = Pattern.quote(unsupPrefix) + "/\\*unsup ";
		
		// 1er pasada, separo en bloques de delimiter
		List<String> tokens = new ArrayList<String>();
		Scanner s = new Scanner(line);
		s.useDelimiter(Pattern.quote(delimiter)); 
		while (s.hasNext()) {
			String t = s.next();
			System.out.println(t);
			tokens.add(t);
		}
		
		if ( tokens.size() == 1) {
			// No encontr� nada, devuelvo la linea sin modificarla
			return line;
		}
		
		StringBuffer sb = new StringBuffer(line.length());
		boolean first = true;
		for (int i = 0; i < tokens.size(); i++) {
			String token = (String)tokens.get(i);
			token = token.replaceFirst(unsupexpr, "");
			if (!first) {
				// Si ya pas� el mensaje, tengo que sacar el */ del token.
				// Solamente tomo el primero por si hay otros /* */ a continuaci�n
				token = token.replaceFirst(" \\*/", "");
			}
			sb.append(token);
			if (!(i == tokens.size() - 1)) {
				sb.append(delimiter);
			}
			first = false;
		}
		System.out.println(sb.toString());
		return sb.toString();
	}

}
