package com.snoopconsulting.qbe.regex;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FixVBConvertedCodeTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testConvertSelectSingleNode() {
		FixVBConvertedCode converter = new FixVBConvertedCode();
		String line = "wvarSesion = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLrequest.selectSingleNode( \"//GENERAL/SESION\" ) */ );";
		String result = converter.convertSelectSingleNode(line);
		assertEquals("wvarSesion = diamondedge.util.XmlDom.getText( wobjXMLrequest.selectSingleNode( \"//GENERAL/SESION\" ) );", result);

	}
	
	@Test
	public void testConvertSelectSingleNodeMulti() {
		FixVBConvertedCode converter = new FixVBConvertedCode();
		String line = "wvarRequest = wvarRequest + \"<Request id=\\\"\" + wvarCount + \"\\\" actionCode=\\\"lbaw_OVVerifReimpresion\\\" ciaascod=\\\"0001\\\">\" + \"   <USUARIO></USUARIO>\" + \"   <RAMOPCOD>\" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( \"RAMOPCOD\" ) */ ) + \"</RAMOPCOD>\" + \"   <POLIZANN>\" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( \"POLIZANN\" ) */ ) + \"</POLIZANN>\" + \"   <POLIZSEC>\" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( \"POLIZSEC\" ) */ ) + \"</POLIZSEC>\" + \"   <CERTIPOL>\" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( \"CERTIPOL\" ) */ ) + \"</CERTIPOL>\" + \"   <CERTIANN>\" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( \"CERTIANN\" ) */ ) + \"</CERTIANN>\" + \"   <CERTISEC>\" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( \"CERTISEC\" ) */ ) + \"</CERTISEC>\" + \"   <SUPLENUM>\" + diamondedge.util.XmlDom.getText( null /*unsup wobjXML_PRODUCTOS_Node.selectSingleNode( \"SUPLENUM\" ) */ ) + \"</SUPLENUM>\" + \"</Request>\";";
		String result = converter.convertSelectSingleNode(line);
		assertEquals("wvarRequest = wvarRequest + \"<Request id=\\\"\" + wvarCount + \"\\\" actionCode=\\\"lbaw_OVVerifReimpresion\\\" ciaascod=\\\"0001\\\">\" + \"   <USUARIO></USUARIO>\" + \"   <RAMOPCOD>\" + diamondedge.util.XmlDom.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( \"RAMOPCOD\" ) ) + \"</RAMOPCOD>\" + \"   <POLIZANN>\" + diamondedge.util.XmlDom.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( \"POLIZANN\" ) ) + \"</POLIZANN>\" + \"   <POLIZSEC>\" + diamondedge.util.XmlDom.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( \"POLIZSEC\" ) ) + \"</POLIZSEC>\" + \"   <CERTIPOL>\" + diamondedge.util.XmlDom.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( \"CERTIPOL\" ) ) + \"</CERTIPOL>\" + \"   <CERTIANN>\" + diamondedge.util.XmlDom.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( \"CERTIANN\" ) ) + \"</CERTIANN>\" + \"   <CERTISEC>\" + diamondedge.util.XmlDom.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( \"CERTISEC\" ) ) + \"</CERTISEC>\" + \"   <SUPLENUM>\" + diamondedge.util.XmlDom.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( \"SUPLENUM\" ) ) + \"</SUPLENUM>\" + \"</Request>\";", result);
	}
	
	@Test
	public void testConvertSelectSingleVariosComentarios() {
		FixVBConvertedCode converter = new FixVBConvertedCode();
		String line = "wvarSesion = /*A*/ diamondedge.util.XmlDom.getText( null /*unsup wobjXMLrequest.selectSingleNode( \"//GENERAL/SESION\" ) */ /*B*/ );";
		String result = converter.convertSelectSingleNode(line);
		assertEquals("wvarSesion = /*A*/ diamondedge.util.XmlDom.getText( wobjXMLrequest.selectSingleNode( \"//GENERAL/SESION\" ) /*B*/ );", result);
	}
	
	@Test
	public void testConvertSelectSingleNodeSinGetText1() {
		FixVBConvertedCode converter = new FixVBConvertedCode();
		String line = "if( null /*unsup wobjXMLRespuestas.selectSingleNode( \"//Response\" ) */ == (org.w3c.dom.Node) null )";
		String result = converter.convertSelectSingleNode(line);
		assertEquals("if( wobjXMLRespuestas.selectSingleNode( \"//Response\" ) == (org.w3c.dom.Node) null )", result);
	}
	
	@Test
	public void testConvertSelectSingleNodeSinGetText2() {
		FixVBConvertedCode converter = new FixVBConvertedCode();
		String line = "wobjElemento = (org.w3c.dom.Element) null /*unsup wobjXMLRespuestas.selectSingleNode( \"Response/Response[@id='\" + wvarCount + \"']\" ) */.cloneNode( true );";
		String result = converter.convertSelectSingleNode(line);
		assertEquals("wobjElemento = (org.w3c.dom.Element) wobjXMLRespuestas.selectSingleNode( \"Response/Response[@id='\" + wvarCount + \"']\" ).cloneNode( true );", result);
	}
	
	@Test
	public void testConvertTransformNode1() {
		FixVBConvertedCode converter = new FixVBConvertedCode();
		String line = "wvarResponse_HTML = new Variant() /*unsup wobjXMLRespuestas.transformNode( wobjXSLSalida ) */.toString();";
		String result = converter.convertTransformNode(line);
		assertEquals("wvarResponse_HTML = wobjXMLRespuestas.transformNode( wobjXSLSalida ).toString();", result);
	}
	
	@Test
	public void testRemoveUnsupEmpty() {
		FixVBConvertedCode converter = new FixVBConvertedCode();
		String line = "";
		String result = converter.removeUnsup(line, "mongo", "prefix");
		assertEquals(line, result);
	}
	
	@Test
	public void testRemoveUnsupOneComment() {
		FixVBConvertedCode converter = new FixVBConvertedCode();
		String line = "/* */";
		String result = converter.removeUnsup(line, "mongo", "prefix");
		assertEquals(line, result);
	}
	
	@Test
	public void testRemoveUnsupComments() {
		FixVBConvertedCode converter = new FixVBConvertedCode();
		String line = "/* */ /* *//* *//* *//* *//* */     /* */";
		String result = converter.removeUnsup(line, "mongo", "prefix");
		assertEquals(line, result);
	}


}
