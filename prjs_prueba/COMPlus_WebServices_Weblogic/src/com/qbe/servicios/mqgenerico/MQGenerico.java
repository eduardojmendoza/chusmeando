package com.qbe.servicios.mqgenerico;

import javax.jws.WebService;

@WebService(targetNamespace = "http://mqgenerico.servicios.qbe.com/", endpointInterface = "com.qbe.servicios.mqgenerico.ComponentInterface", portName = "MQGenericoPort", serviceName = "MQGenericoService")
public class MQGenerico implements ComponentInterface {

	  public Response executeRequest( String Request, String ContextInfo ) {
		  return new Response(42, "<pepe/>");
	  }
}
