package com.qbe.servicios.mqgenerico;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(name = "ComponentInterface", targetNamespace = "http://mqgenerico.servicios.qbe.com/")
public interface ComponentInterface {

	@WebMethod(operationName = "executeRequest", action = "urn:ExecuteRequest")
	@RequestWrapper(className = "com.qbe.servicios.mqgenerico.jaxws.ExecuteRequest", localName = "executeRequest", targetNamespace = "http://mqgenerico.servicios.qbe.com/")
	@ResponseWrapper(className = "com.qbe.servicios.mqgenerico.jaxws.ExecuteRequestResponse", localName = "executeRequestResponse", targetNamespace = "http://mqgenerico.servicios.qbe.com/")
	public Response executeRequest(@WebParam(name = "arg0") String Request, @WebParam(name = "arg2") String ContextInfo);

}