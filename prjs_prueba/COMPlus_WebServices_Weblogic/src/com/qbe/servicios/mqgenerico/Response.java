package com.qbe.servicios.mqgenerico;

public class Response {
	
	private int resultCode;
	private String response;
	public Response(){
		
	}
	public Response(int resultCode, String response) {
		this.response = response;
		this.resultCode = resultCode;
	}
	public int getResultCode() {
		return resultCode;
	}
	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}

}
