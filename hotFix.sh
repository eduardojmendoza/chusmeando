#! /bin/bash


if (( $# < 1 )); then
  echo >&2 "Usage: $0 commitOri commitFix versionTag"
  exit 1
fi

branchName=$1
commitPartida=$2
commitFix=$3
version=$4



cd integracion

echo >&2 "Creo el branch $pattern "
git branch $branchName $commitPartida
git checkout $branchName
echo >&2 "Aplico cambio del commit $commitFix"
git cherry-pick $commitFix


echo >&2 "Cambio la version $version"
mvn versions:set -DnewVersion=$version
mvn versions:commit
git add .
git commit -m '[HF2] Update de versiones'

git push origin $branchName

echo >&2 "Armado del jar hotFix "
export MAVEN_OPTS="-Xmx2048m -XX:MaxPermSize=128m"
mvn package

echo >&2 "Subida al artifactory "

mvn deploy -Dmaven.test.skip=true


