(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/OV_1202.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QbeFindPolicyInsuredsRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1202_To_FndPlcyByNumRq/";

declare function xf:OV_1202_To_FndPlcyByNumRq($request as element(Request))
    as element() {
        <ns0:QbeFindPolicyInsuredsRq>
        	<ns0:PolicyNo>
        		{
        			concat(	'0001',
                    	fn-bea:pad-right(data($request/PRODUCTO), 4),
                        fn-bea:pad-left(data($request/POLIZA), 8, '0'),
                        fn-bea:pad-left(data($request/CERTI), 14, '0'))
				}
			</ns0:PolicyNo>
        </ns0:QbeFindPolicyInsuredsRq>
};

declare variable $request as element(Request) external;

xf:OV_1202_To_FndPlcyByNumRq($request)
