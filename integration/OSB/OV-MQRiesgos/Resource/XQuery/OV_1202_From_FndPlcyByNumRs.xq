(:: pragma bea:global-element-parameter parameter="$service1232" element="Response" location="../XSD/Service1232.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$qbeFindPolicyInsuredsRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeFindPolicyInsuredsRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1202.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1202_From_FndPlcyByNumRs/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:OV_1202_From_FndPlcyByNumRs($LOVs as element()?
												, $service1232 as element()?
												, $driversTransRs as element()?
												, $qbeFindPolicyInsuredsRs as element())
    as element(Response) {
       		<Response>
           	{
        	if ( number(data($qbeFindPolicyInsuredsRs/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
		            <DATOS>
		                <COMUNES>
		                    <MOTOR>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:Engine) }</MOTOR>
		                    <PATENTE>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:RegNo) }</PATENTE>
		                    <CHASIS>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:Chassis) }</CHASIS>
		                    <NROVEH>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:ObjectID) }</NROVEH>
		                    (:<MARCA>{ data($LOVs/lov:LOV[@id = "Make"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($service1232/CAMPOS/AUMARCOD)]/ns0:ItemDescription) }</MARCA>:)
		                    <MARCA>{ data($service1232/CAMPOS/AUMARDES) }</MARCA>
		                    (:<MODELO>{ data($LOVs/lov:LOV[@id = "Model"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($service1232/CAMPOS/AUMODCOD)]/ns0:ItemDescription) }</MODELO>:)
		                    <MODELO>{ data($service1232/CAMPOS/AUMODDES) }</MODELO>
		                    <SUBMODEL>{ data($service1232/CAMPOS/AUSMODES) }</SUBMODEL>
		                    <ADIC>{ data($service1232/CAMPOS/AUADIDES) }</ADIC>
		                    (:<ORIGEN>{ data($LOVs/lov:LOV[@id = "ProdType"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($service1232/CAMPOS/AUMODORI)]/ns0:ItemDescription) }</ORIGEN>:)
		                    <ORIGEN>{ data($service1232/CAMPOS/AUMODODE) }</ORIGEN>
		                    <USO>{ data($LOVs/lov:LOV[@id = "CarUsage"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarUsage)]/ns0:ItemDescription) }</USO>
		                    <VTV>NO</VTV>
		                    <VTVFEC />
		                    <COLOR>{ data($LOVs/lov:LOV[@id = "CarColor"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarColor)]/ns0:ItemDescription) }</COLOR>
		                    <KM>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP1']/ns0:Value) }</KM>
		                    <CATEG />

		                    <TIPVEH>
		                    	{
		                    		let $tipo := data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarType)
		                    		let $cartype := data($LOVs/lov:LOV[@id = "CarType"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($tipo)]/ns0:ItemDescription)
		                    		return if ($cartype !='') then ($cartype) else ($tipo)
		                    	}
		                    </TIPVEH>

		                    <ANIOFAB>{ concat('01/', data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:ProdYear)) }</ANIOFAB>
		                    <GUARGARA>
		                    	{
		                    		let $value := data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP2']/ns0:Value)
		                    		return
			                    	 	if ($value = '1') then ('SI')
			                    	 	else if ($value = '2') then ('NO')
			                    	 	else ($value)
		                    	}
		                    </GUARGARA>
		                    <GUARDOMI> </GUARDOMI>
		                    <PAISSCOD>00</PAISSCOD>
		                    <RADI>ARGENTINA</RADI>
		                    <PROV>{ data($LOVs/lov:LOV[@id = "Province"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP8']/ns0:Value)]/ns0:ItemDescription) }</PROV>
		                    <ZONA>{ data($LOVs/lov:LOV[@id = "Zone"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP4']/ns0:Value)]/ns0:ItemDescription) }</ZONA>
		                    <CODPOS>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP5']/ns0:Value) }</CODPOS>
		                </COMUNES>
		                <LECTINI>
		                	<CIAANT />
		                	<ANTIG />
		                    <CANTSINI>{ data($qbeFindPolicyInsuredsRs/ns0:InsuranceConditions/ns0:Questionary/ns0:Question[ns0:ID='3005.31']/ns0:Answer) }</CANTSINI>
		                    <KMINI>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP1']/ns0:Value) }</KMINI>
		                    <GNC>
		                    	{
		                    		let $value := data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP13']/ns0:Value)
		                    		return
			                    	 	if ($value = '1') then ('SI')
			                    	 	else if ($value = '2') then ('NO')
			                    	 	else ($value)
		                    	}
		                    </GNC>
		                </LECTINI>
		                <CONDUCTORES>
		                    {
		                        for $driver in $qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:Drivers/ns0:Driver
		                        return
		                            <CONDUCTOR>{
		                            if (exists($driver/ns0:Entity/ns0:PersonalData)) then (
				                        <APENOM>{
				                        		let $nombre1 := data($driver/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Given)
				                        		let $nombre2 := data($driver/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Surname)
				                        		let $apellido := data($driver/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Family)
				                        		return if ($nombre2 != '')
				                        			then (concat($apellido, ', ', $nombre1, ' ', $nombre2))
				                        			else (concat($apellido, ', ', $nombre1))
				                        		}
										</APENOM>,
		                                <TIPDOC>{ substring-before(data($driver/ns0:Entity/ns0:PersonalData/ns0:PID),'-') }</TIPDOC>,
		                                <NRODOC>{ substring-after(data($driver/ns0:Entity/ns0:PersonalData/ns0:PID),'-') }</NRODOC>
		                            ) else (
		                            	<APENOM>{ data($driver/ns0:Entity/ns0:CompanyData/ns0:Name) }</APENOM>,
		                                <TIPDOC>{ data($LOVs/lov:LOV[@id = "DocTypeByName" and @ref = data($driver/ns0:Entity/ns0:CompanyData/ns0:CustomerID) ]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemDescription) }</TIPDOC>,
		                                <NRODOC>{ data($driver/ns0:Entity/ns0:CompanyData/ns0:PID) }</NRODOC>
		                            	)}
				                        <ESTCIV>
				                        	{
				                        		let $estCivil := data($driver/ns0:Entity/ns0:DataSource)
				                        		return
				                        			if ($estCivil = 'S') then ('SOLTERO')
				                        			else if ($estCivil = 'C') then ('CASADO')
				                        			else if ($estCivil = 'V') then ('VIUDO')
				                        			else if ($estCivil = 'D') then ('DIVORCIADO')
				                        			else if ($estCivil = 'E') then ('SEPARADO')
				                        			else if ($estCivil = 'N') then ('** NO INFORMADO **')
				                        			else ($estCivil)
				                        	}
				                        </ESTCIV>
				                        <SEXO>
				                        	{
				                        		let $sexo := data($driver/ns0:Entity/ns0:PersonalData/ns0:Gender)
				                        		return
				                        			if ($sexo = '1') then ('MASCULINO')
				                        			else if ($sexo = '2') then ('FEMENINO')
				                        			else ('** NO INFORMADO **')
				                        	}
				                        </SEXO>
										<FECNAC>
                        					{
                        					let $fecha := data($driver/ns0:Entity/ns0:PersonalData/ns0:BirthDate)
                        					return
                        						if ($fecha != '') then (fn-bea:date-to-string-with-format("dd/MM/yyyy", $fecha)) else ()
                        					}
                        				</FECNAC>
		                                <TIPO>{ substring(data($driver/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='Iod1']/ns0:Value), 1, 1) }</TIPO>
		                                <VINC>{
		                                (: Se cambia segun la tabla que nos brindan en el Ticket 804:)
				                        		let $vinculo := data($driversTransRs/ns0:RowSet/ns0:Row/ns0:Column[@name = "DRIVER_ID" and text() = data($driver/ns0:DriverID)]/parent::node()/ns0:Column[@name = "IOD2_DESC"])
				                        		return
				                        			if ($vinculo = 'Hermano') then ('HE')
				                        			else if ($vinculo = 'Hijo') then ('HI')
				                        			else if ($vinculo = 'cónyuge') then ('CO')
				                        			else if ($vinculo = 'Otros') then ('OT')
				                        			else ($vinculo)
				                        }</VINC>
		                            </CONDUCTOR>
		                    }
		                </CONDUCTORES>
		                <ACCESOS>
		                    {
		                    	for $OAdditionalEquipment in $qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:SupplementaryEquipment/ns0:OAdditionalEquipment
				                return
				                (
		                            <ACCESO>
		                                <COD>{ data($LOVs/lov:LOV[@id = "EquipmentType"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($OAdditionalEquipment/ns0:EquipmentType)]/ns0:ItemDescription) }</COD>
		                                <SUMAAS>{ data($OAdditionalEquipment/ns0:AvValue) }</SUMAAS>
										<DESC></DESC>
		                            </ACCESO>
		                        )
		                    }
		                </ACCESOS>
		                <ADICIONALES>
		                    {
		                        let $Document0 := $qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:Documents/ns0:Document[1]
		                        return
		                            <ASEGADIC>
		                                <DOCUADIC>{ data($Document0/ns0:DocumentNo) }</DOCUADIC>
		                                <APEADIC>{ data($Document0/ns0:DocumentID) }</APEADIC>
		                            </ASEGADIC>
		                    }
		                </ADICIONALES>
		            </DATOS>
		            )
	       	}
    		</Response>
};

declare variable $LOVs as element(lov:LOVs)? external;
declare variable $service1232 as element()? external;
declare variable $driversTransRs as element()? external;
declare variable $qbeFindPolicyInsuredsRs as element() external;

xf:OV_1202_From_FndPlcyByNumRs($LOVs, $service1232, $driversTransRs, $qbeFindPolicyInsuredsRs)
