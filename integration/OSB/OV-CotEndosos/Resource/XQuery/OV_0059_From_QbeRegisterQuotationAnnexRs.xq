(:: pragma bea:local-element-parameter parameter="$qbeRegisterQuotationAnnexRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeRegisterQuotationAnnexRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_0059.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-CotEndosos/Resource/XQuery/OV_0059_From_QbeRegisterQuotationAnnexRq/";

declare function xf:OV_0059_From_QbeRegisterQuotationAnnexRq($qbeRegisterQuotationAnnexRs1 as element())
    as element(Response) {
        <Response>
            <CAMPOS>
                <Request>
            		<PRECIO>{    fn-bea:format-number(number(fn-bea:format-number(data($qbeRegisterQuotationAnnexRs1/ns0:Premium), '#.00'))*100 , '#')   }</PRECIO>
            		<COBERTURAS>
            			<COBERTURA>
            				<RAMOPCOD>{ data($qbeRegisterQuotationAnnexRs1/ns0:ProductCode) }</RAMOPCOD>
            				<COBERCOD>{ data($qbeRegisterQuotationAnnexRs1/ns0:Covers/ns0:Cover[1]/ns0:CoverCode) }</COBERCOD>
            				<COBERORD>{ data($qbeRegisterQuotationAnnexRs1/ns0:Covers/ns0:Cover[1]/ns0:CoverOrder) }</COBERORD>
            				<CAPITASG>{ data($qbeRegisterQuotationAnnexRs1/ns0:Covers/ns0:Cover[1]/ns0:InsuredValue) }</CAPITASG>
            			</COBERTURA>
            		</COBERTURAS>
                </Request>
            </CAMPOS>
        </Response>
};

declare variable $qbeRegisterQuotationAnnexRs1 as element() external;

xf:OV_0059_From_QbeRegisterQuotationAnnexRq($qbeRegisterQuotationAnnexRs1)