(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_0059.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$service1230" element="Response" location="../XSD/Service_1230.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QbeRegisterQuotationAnnexRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-CotEndosos/Resource/XQuery/OV_0059_To_QbeRegisterQuotationAnnexRq/";
declare namespace functx = "http://www.functx.com";
declare function xf:OV_0059_To_QbeRegisterQuotationAnnexRq($req as element(Request), $service1230 as element(Response), $mapPlanPackage as element(*))

    as element() {
    
        <ns0:QbeRegisterQuotationAnnexRq>
            <ns0:PolicyNo>{ concat('0001AUS1000000010000', fn-bea:format-number(xs:double (fn-bea:trim($req/COTNRO)),'0000000000')) }</ns0:PolicyNo>
            <ns0:Annex>
                <ns0:BeginDate>{ 	if ((exists($req/EMISIANN) and exists($req/EMISIMES)  and exists($req/EMISIDIA)) and (data($req/EMISIANN) != '' and data($req/EMISIMES) != '' and data($req/EMISIDIA) != '' and data($req/EMISIANN) != '0' and data($req/EMISIMES) != '0' and data($req/EMISIDIA) != '0'))then
                					(
	                						fn-bea:date-from-string-with-format("yyyyMMdd", concat(data($req/EMISIANN) , data($req/EMISIMES) , data($req/EMISIDIA)))
	                				) else (fn-bea:date-from-string-with-format("yyyyMMdd", "19000101"))
				}</ns0:BeginDate>
                <ns0:DateGiven>{ 	if ((exists($req/EMISIANN) and exists($req/EMISIMES)  and exists($req/EMISIDIA)) and (data($req/EMISIANN) != '' and data($req/EMISIMES) != '' and data($req/EMISIDIA) != '' and data($req/EMISIANN) != '0' and data($req/EMISIMES) != '0' and data($req/EMISIDIA) != '0'))then
                					(
	                						fn-bea:date-from-string-with-format("yyyyMMdd", concat(data($req/EMISIANN) , data($req/EMISIMES) , data($req/EMISIDIA)))
	                				) else (fn-bea:date-from-string-with-format("yyyyMMdd", "19000101"))
				}</ns0:DateGiven>
            </ns0:Annex>
            <ns0:Client>
                <ns0:PaymentInstruments>
                    <ns0:PaymentInstrument>
                    	{ 	
                    	(:Este cableado està pendiente por correcciòn cuando lo pida QBE:)
                    	if (data($req/COBROCOD) = '4')
		                    then(
		                    <ns0:AccountType>{data($req/COBROTIP)}</ns0:AccountType>,
		                    <ns0:AccountNo>5323600000017000</ns0:AccountNo>
		                    ) else (
		                    if(data($req/COBROCOD) = '5' and data($req/COBROTIP) = 'DB') then (
	                    		 <ns0:AccountType>DB</ns0:AccountType>,
             					 <ns0:AccountNo>1508888888888888888688</ns0:AccountNo>,
             					 <ns0:BankCode>150</ns0:BankCode>,
              					 <ns0:Currency>ARS</ns0:Currency>
	                    )else (
	                       <ns0:AccountType>{ data($req/COBROTIP) }</ns0:AccountType>
	                    )
	                    )
	                    
					}</ns0:PaymentInstrument>
                </ns0:PaymentInstruments>
                <ns0:TaxData>
                    <ns0:IVAType>{ data($req/CLIENIVA) }</ns0:IVAType>
                </ns0:TaxData>
            </ns0:Client>
            <ns0:Vehicle>
            	<ns0:InfoAutoCode>{ data($service1230/CAMPOS/AUPRONUM) }</ns0:InfoAutoCode>
                <ns0:ProdYear>{ data($req/FABRICAN) }</ns0:ProdYear>
                <ns0:CarUsage>{ data($req/AUUSOCOD) }</ns0:CarUsage>
            	{
        			if (data($req/AUKLMNUM) != '') then
    				(
    					<ns0:KmPerYear>{data($req/AUKLMNUM)}</ns0:KmPerYear>
    				) else ()
				}
                <ns0:Garage>{ data($req/GUGARAGE) }</ns0:Garage>
                <ns0:HasGasEquipment>{ data($req/AUUSOGNC) }</ns0:HasGasEquipment>
                <ns0:Zone>{ data($req/PAISSCOD) }</ns0:Zone>
                <ns0:Region>{ data($req/PROVICOD) }</ns0:Region>
                <ns0:ZIP>{ data($req/POSTACOD) }</ns0:ZIP>
                {
                let $ACCESORIOS := $req/ACCESORIOS
                let $ACCESCODES := $req/ACCESORIOSADIC
                return
                
                	if ( count($ACCESORIOS/ACCESORIO) = count($ACCESCODES/ACCESORIOADIC)  ) then (
	                    <ns0:Accessories?> (: el ? es para que si no se genera ningun Accessory el tag Accessories no se genera tampoco :)	                                    	
	                          { 
	            	           let $counter := if (count($ACCESORIOS/ACCESORIO) = 0 )then (
    					                          1
                        	      				)else(
				            	                  count($ACCESORIOS/ACCESORIO)                              
				                	            )
	                           for $x in (1 to $counter)
	                            
	                            let $code := data($ACCESCODES/ACCESORIOADIC[$x]/AUACCCOD)
	                            let $codeDesc := data($ACCESORIOS/ACCESORIO[$x]/AUVEADES)
	                        	let $precio := data($ACCESORIOS/ACCESORIO[$x]/AUVEASUM)
	                        	
	                            return
	                           if (data($req/AUUSOGNC)='S' and  
	                           		$precio != '0' and 
	                           		$codeDesc = '5') then (                	
		                                <ns0:Accessory>
		                                	<ns0:Code>6</ns0:Code>
		                                	<ns0:SubCode>1</ns0:SubCode>
		                                    <ns0:Description>Equipo de GNC</ns0:Description>
		                                    <ns0:InsuredValue>{ data($ACCESORIOS/ACCESORIO[AUVEADES=5]/AUVEASUM) }</ns0:InsuredValue>
		                                </ns0:Accessory> 
	                          )else(
	                          if ($precio != '0' and $codeDesc != '5') then (
		                                <ns0:Accessory>
		                                	<ns0:Code>5</ns0:Code>
		                                	<ns0:SubCode>{ $code }</ns0:SubCode>
		                                    <ns0:Description>{ $codeDesc }</ns0:Description>
		                                    <ns0:InsuredValue>{ $precio }</ns0:InsuredValue>
		                                </ns0:Accessory>
	                                ) else ()
	                                )
								}
	                        
	                    </ns0:Accessories>
                	) else ()

            }<ns0:Drivers>
                    <ns0:Driver>
                       <ns0:BirthDate>{	if (((exists($req/NACIMANN) and exists($req/NACIMMES)  and exists($req/NACIMDIA)) and (data($req/NACIMANN) != '' and data($req/NACIMMES) != '' and data($req/NACIMDIA) != '' and data($req/NACIMANN) != '0' and data($req/NACIMMES) != '0' and data($req/NACIMDIA) != '0'))) then
                							(
			                						fn-bea:date-from-string-with-format("yyyyMMdd", concat(data($req/NACIMANN) , data($req/NACIMMES) , data($req/NACIMDIA)))
			                				) else (fn-bea:date-from-string-with-format("yyyyMMdd", "19000101"))
						}</ns0:BirthDate>
                        <ns0:Gender>{
		                		let $sexo := fn:substring(upper-case(data($req/CLIENSEX)), 1, 1)
		                		return
		                			if ($sexo = 'M') then ( 1 )
		                			else if ($sexo = 'F') then ( 2 )
		                			else ( 0 )
	                    }</ns0:Gender>
		                <ns0:MaritalStatus>{ fn:substring(normalize-space(data($req/CLIENEST)), 1, 1) }</ns0:MaritalStatus>
                        <ns0:DriverType>PRIMARY</ns0:DriverType>
                    </ns0:Driver>{
	                    if (exists($req/HIJOS/HIJO)) then
	                   	(
	                    	for $hijo in $req/HIJOS/HIJO[CLIENEST != '']
	                    	return
		                    (
			                    <ns0:Driver>
			                        <ns0:BirthDate>{	if (exists($hijo/FECNACIM) and (data($hijo/FECNACIM) != '' and data($hijo/FECNACIM) != '0')) then
	                									(
						                						fn-bea:date-from-string-with-format("ddMMyyyy", $hijo/FECNACIM)
						                				) else (fn-bea:date-from-string-with-format("ddMMyyyy", "01011900"))
									}</ns0:BirthDate>
			                        <ns0:Gender>{
					                		let $sexo := fn:substring(upper-case(data($hijo/CLIENSEX)), 1, 1)
					                		return
					                			if ($sexo = 'M') then ( 1 )
					                			else if ($sexo = 'F') then ( 2 )
					                			else ( 0 )
				                    }</ns0:Gender>
			                        <ns0:MaritalStatus>{ fn:substring( normalize-space(data($hijo/CLIENEST)), 1, 1) }</ns0:MaritalStatus>
			                        <ns0:DriverType>SECONDARY</ns0:DriverType>
			                    </ns0:Driver>
	                    	)
	                	) else ()
             	}</ns0:Drivers>
            </ns0:Vehicle>
			<ns0:Covers>
      	{
			            	
			            	let $FRANQCOD := functx:substring-after-if-contains(data($req/FRANQCOD),"0")  
			            	let $PLANNCOD := data($req/PLANCOD)
			            	let $packageID := $mapPlanPackage/Map[@MainPackage = 'S' and @PLANNCOD = $PLANNCOD and @FRANQCOD = $FRANQCOD]/@PackageCode
			            	return
					            	  <ns0:PackageCode>{ data($packageID) }</ns0:PackageCode>
		            }
                <ns0:CoverWindshield>
						{
                		if (exists($req/LUNETA)) then
						(
							let $luneta := data($req/LUNETA)
							return
							if ($luneta = '4' or $luneta = '7') then
							( 'S' )
							else ( 'N' )
						) else ()
                    }
                </ns0:CoverWindshield>
                <ns0:CoverHail>{ if (exists($req/GRANIZO)) then
						(
							if (data($req/GRANIZO) != '') then
							(
								data($req/GRANIZO)
							) else ()
						) else ()
								}
				</ns0:CoverHail>
                <ns0:CoverTheft>{ if (exists($req/ROBOCONT)) then
						(
							if (data($req/ROBOCONT) != '') then
							(
								data($req/ROBOCONT)
							) else ()
						) else ()
				}</ns0:CoverTheft>
            </ns0:Covers>
        </ns0:QbeRegisterQuotationAnnexRq>
};


declare function functx:substring-after-if-contains
  ( $arg as xs:string? ,
    $delim as xs:string )  as xs:string? {

   if (starts-with($arg,$delim) and string-length($arg)=2)
   then substring-after($arg,$delim)
   else $arg
 } ;

declare variable $req as element(Request) external;
declare variable $service1230 as element(Response) external;
declare variable $mapPlanPackage as element(*) external;

xf:OV_0059_To_QbeRegisterQuotationAnnexRq($req, $service1230, $mapPlanPackage)

