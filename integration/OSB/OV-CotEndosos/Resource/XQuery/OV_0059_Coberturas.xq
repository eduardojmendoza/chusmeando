xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeRegisterQuotationAnnexRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="COBERTURAS" location="../XSD/OV_0059_Coberturas.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-CotEndosos/Resource/XQuery/OV_0059_Coberturas/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:OV_0059_Coberturas($resp as element(), $LOVs as element()?)
    as element(COBERTURAS) {
    <COBERTURAS>
        {for $COBERTURA in $resp/ns0:Covers/ns0:Cover
        return
			<COBERTURA>
					<RAMOPCOD>{
					data($LOVs/lov:LOV[@id = "ProductTypeDescr" and @ref = data($resp/ns0:ProductCode) ]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID)
					}</RAMOPCOD>
					<POLIZANN>0</POLIZANN>
					<POLIZSEC>0</POLIZSEC>
					<CERTIPOL>0</CERTIPOL>
					<CERTIANN>0</CERTIANN>
					<CERTISEC>0</CERTISEC>
					<SUPLENUM>0</SUPLENUM>
					<COBERCOD>{
						data($LOVs/lov:LOV[@id = "CoverByInsisCode" and @ref = data($COBERTURA/ns0:CoverCode) ]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID)
					}</COBERCOD>
					<EFECTANN>0</EFECTANN>
					<EFECTMES>0</EFECTMES>
					<EFECTDIA>0</EFECTDIA>
					<VENCIANN>0</VENCIANN>
					<VENCIMES>0</VENCIMES>
					<VENCIDIA>0</VENCIDIA>
					<COBERORD>{ data($COBERTURA/ns0:CoverOrder) }</COBERORD>
					<CAPITASG>{ data($COBERTURA/ns0:InsuredValue) }</CAPITASG>
					<CAPITIMP>{ data($COBERTURA/ns0:InsuredValue) }</CAPITIMP>
					<INGARPOR>0</INGARPOR>
					<REVINANN>0</REVINANN>
					<REVINMES>0</REVINMES>
					<REVINDIA>0</REVINDIA>
					<INGARIMP>0</INGARIMP>
					<ININTPOR>0</ININTPOR>
					<REVALTIP/>
					<REVALPOR>0</REVALPOR>
					<REVALPER/>
					<REVALIMP>0</REVALIMP>
					<REDUCPOR>0</REDUCPOR>
					<BENEFPOR>0</BENEFPOR>
					<BENEFIMP>0</BENEFIMP>
					<PRIREANN>0</PRIREANN>
					<PRIREMES>0</PRIREMES>
					<PRIREDIA>0</PRIREDIA>
					<EFESOANN>0</EFESOANN>
					<EFESOMES>0</EFESOMES>
					<EFESODIA>0</EFESODIA>
					<VENSOANN>0</VENSOANN>
					<VENSOMES>0</VENSOMES>
					<VENSODIA>0</VENSODIA>
					<SOBPRCOD>0</SOBPRCOD>
					<SOBPRPOR>0</SOBPRPOR>
					<EFEEXANN>0</EFEEXANN>
					<EFEEXMES>0</EFEEXMES>
					<EFEEXDIA>0</EFEEXDIA>
					<VENEXANN>0</VENEXANN>
					<VENEXMES>0</VENEXMES>
					<VENEXDIA>0</VENEXDIA>
					<EXTPRCOD>0</EXTPRCOD>
					<FILLER/>
					<EXTPRPOR>0</EXTPRPOR>
					<EFEDEANN>0</EFEDEANN>
					<EFEDEMES>0</EFEDEMES>
					<EFEDEDIA>0</EFEDEDIA>
					<VENDEANN>0</VENDEANN>
					<VENDEMES>0</VENDEMES>
					<VENDEDIA>0</VENDEDIA>
					<DEDUCCOD>0</DEDUCCOD>
					<DEDUCPOR>0</DEDUCPOR>
					<COBERTIP/>
					<SWRECACD/>
					<CUMULCLA/>
					<COBEAMBI/>
					<MODACAPI/>
					<CUMULNUM>0</CUMULNUM>
				</COBERTURA>}
    </COBERTURAS>
};

declare variable $resp as element() external;
declare variable $LOVs as element(lov:LOVs)? external;

xf:OV_0059_Coberturas($resp, $LOVs )
