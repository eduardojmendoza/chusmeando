<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--  http://stackoverflow.com/questions/10912544/removing-duplicate-elements-with-xslt  -->
<xsl:output omit-xml-declaration="yes" indent="yes"/>
 <xsl:strip-space elements="*"/>

 <xsl:key name="kVehicleKey" match="VehicleKey" use="Key"/>

 <xsl:template match="node()|@*">
     <xsl:copy>
       <xsl:apply-templates select="node()|@*"/>
     </xsl:copy>
 </xsl:template>

 <xsl:template match=
  "VehicleKey[not(generate-id() = generate-id(key('kVehicleKey', Key)[1]))]"
  />
</xsl:stylesheet>