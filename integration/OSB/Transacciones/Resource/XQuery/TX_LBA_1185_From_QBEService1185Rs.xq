xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QBEService1185Rs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/TX_LBA_1185_SusyDesus_epoliza.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/Transacciones/Resource/XQuery/TX_LBA_1185_From_QBEService1185Rs/";

declare function xf:TX_LBA_1185_From_QBEService1185Rs($resp as element())
    as element(Response) {
        <Response>
             {
                 if ( exists($resp/ns0:Service1185Data) ) then (
                    <Estado resultado = "true" mensaje = ""/>,
		            <CAMPOS>
		                <EPOLIZAS>
		                    {
		                        for $ePoliza in $resp/ns0:Service1185Data
		                        let $policyNo := data($ePoliza/ns0:PolicyNo)
		                        return
		                            <EPOLIZA>
		                                <CIAASCOD>{ substring($policyNo, 1, 4) }</CIAASCOD>
		                                <RAMOPCOD>{ substring($policyNo, 5, 4) }</RAMOPCOD>
		                                <POLIZANN>{ substring($policyNo, 9, 2) }</POLIZANN>
		                                <POLIZSEC>{ substring($policyNo, 11, 6) }</POLIZSEC>
		                                <CERTIPOL>{ substring($policyNo, 17, 4) }</CERTIPOL>
		                                <CERTIANN>{ substring($policyNo, 21, 4) }</CERTIANN>
		                                <CERTISEC>{ substring($policyNo, 25, 6) }</CERTISEC>
		                                <SWCONFIR>{ data($ePoliza/ns0:SWCONFIRM) }</SWCONFIR>
		                            </EPOLIZA>
		                    }
		                </EPOLIZAS>
		            </CAMPOS>
                 ) else (
		            <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                 )
            }
        </Response>
};

declare variable $resp as element() external;

xf:TX_LBA_1185_From_QBEService1185Rs($resp)
