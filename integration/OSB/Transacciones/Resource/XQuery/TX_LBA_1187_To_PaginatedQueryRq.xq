xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/TX_LBA_1187_ListadoePolizas.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/TX_LBA_1187_To_PaginatedQueryRs.xq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:TX_LBA_1187_To_PaginatedQueryRs($req as element(Request))
    as element() {
        <ns0:PaginatedQueryRq>
            <ns0:QueryID>1187</ns0:QueryID>
        </ns0:PaginatedQueryRq>
};

declare variable $req as element(Request) external;

xf:TX_LBA_1187_To_PaginatedQueryRs($req)