xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/TX_LBA_1187_ListadoePolizas.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/TX_LBA_1187_From_PaginatedQueryRs.xq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:TX_LBA_1187_From_PaginatedQueryRs($resp as element())
    as element(Response) {
         <Response>
             {
                 if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                     <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                 ) else (
                     <Estado resultado = "true" mensaje = ""/>,
                     <CAMPOS>
                     <IMPRESOS>
                         {
                             for $item in $resp/ns0:RowSet/ns0:Row
                             return
                                 <IMPRESO>
										<CIAASCOD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],1,4)) }</CIAASCOD>
										<RAMOPCOD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)) }</RAMOPCOD>
				                  		<POLIZANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,2)) }</POLIZANN>
				                  		<POLIZSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],11,6)) }</POLIZSEC>
				                  		<CERTIPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERTIPOL>
								  		<CERTIANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERTIANN>
								  		<CERTISEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERTISEC>
										<OPERAPOL>{ data($item/ns0:Column[@name = 'OPERAPOL']) }</OPERAPOL>
										<FORMUHOJA>{ data($item/ns0:Column[@name = 'FORMUHOJA']) }</FORMUHOJA>
										<FORMUDES>{ data($item/ns0:Column[@name = 'FORMUDES']) }</FORMUDES>
										<DOCUMTIP>{ data($item/ns0:Column[@name = 'DOCUMTIP']) }</DOCUMTIP>
										<DOCUMDAT>{ data($item/ns0:Column[@name = 'DOCUMDAT']) }</DOCUMDAT>
										<CLIENAP1>{ data($item/ns0:Column[@name = 'CLIENAP1']) }</CLIENAP1>
										<CLIENAP2>{ data($item/ns0:Column[@name = 'CLIENAP2']) }</CLIENAP2>
										<CLIENNOM>{ data($item/ns0:Column[@name = 'CLIENNOM']) }</CLIENNOM>
										<NOMARCH>{ data($item/ns0:Column[@name = 'NOMARCH']) }</NOMARCH>
										<SWSUSCRI>{ data($item/ns0:Column[@name = 'SWSUSCRI']) }</SWSUSCRI>
										<MAIL>{ data($item/ns0:Column[@name = 'MAIL']) }</MAIL>
										<CLAVE>{ data($item/ns0:Column[@name = 'CLAVE']) }</CLAVE>
										<SW-CLAVE>{ data($item/ns0:Column[@name = 'SW-CLAVE']) }</SW-CLAVE>
										<SWENDOSO>{ data($item/ns0:Column[@name = 'SWENDOSO']) }</SWENDOSO>
										<OPERATIP>{ data($item/ns0:Column[@name = 'OPERATIP']) }</OPERATIP>
										<ESTADO>{ data($item/ns0:Column[@name = 'ESTADO']) }</ESTADO>
										<CODESTAD>{ data($item/ns0:Column[@name = 'CODESTAD']) }</CODESTAD>
                                 </IMPRESO>
                         }
					 </IMPRESOS>
                     </CAMPOS>

                 )
            }
        </Response>
};

declare variable $resp as element() external;

xf:TX_LBA_1187_From_PaginatedQueryRs($resp)