xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/TX_LBA_1430_SeguimientoCobranzas.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/TX_LBA_1430_From_PaginatedQueryRs.xq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:TX_LBA_1430_From_PaginatedQueryRs($resp as element())
    as element(Response) {
         <Response>
             {
                 if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                     <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                 ) else (
                     <Estado resultado = "true" mensaje = ""/>,
                     <CAMPOS>
                     <CANTLINEST>{ data($resp/ns0:TotalRowCount) }</CANTLINEST>
                     <RECIBOS>
                         {
                             for $item in $resp/ns0:RowSet/ns0:Row
                             return
                                 <RECIBO>
										<OPERAPOL>{data($item/ns0:Column[@name = 'OPERAPOL'])}</OPERAPOL>
										<RECIBANN>{ substring(data($item/ns0:Column[@name = 'RECIBO']), 1, 2) }</RECIBANN>
										<RECIBTIP>{ substring(data($item/ns0:Column[@name = 'RECIBO']), 3, 1) }</RECIBTIP>
										<RECIBSEC>{ substring(data($item/ns0:Column[@name = 'RECIBO']), 4, 9) }</RECIBSEC>
										<FECHADDE>{ data($item/ns0:Column[@name = 'FECHADDE']) }</FECHADDE>
										<FECHAHTA>{ data($item/ns0:Column[@name = 'FECHAHTA']) }</FECHAHTA>
										<FECHAVTO>{ data($item/ns0:Column[@name = 'FECHAVTO']) }</FECHAVTO>
										<CANALCOBR>{ data($item/ns0:Column[@name = 'CANALCOBR']) }</CANALCOBR>
										<MONEDA>{ data($item/ns0:Column[@name = 'MONEDA']) }</MONEDA>
										<SIGNOPRI>{ data($item/ns0:Column[@name = 'SIGNOPRI']) }</SIGNOPRI>
										<PRIMAIMP>{ data($item/ns0:Column[@name = 'PRIMAIMP']) }</PRIMAIMP>
										<SIGNOPRE>{ data($item/ns0:Column[@name = 'SIGNOPRE']) }</SIGNOPRE>
										<RECTOIMP>{ data($item/ns0:Column[@name = 'RECTOIMP']) }</RECTOIMP>
										<SITUADES>{ data($item/ns0:Column[@name = 'SITUADES']) }</SITUADES>
										<SITUCREC>{ data($item/ns0:Column[@name = 'SITUCREC']) }</SITUCREC>
										<FECHACOB>{ data($item/ns0:Column[@name = 'FECHACOB']) }</FECHACOB>
										<SITUCMOT>{ data($item/ns0:Column[@name = 'SITUCMOT']) }</SITUCMOT>
										<MOTIVDAB>{ data($item/ns0:Column[@name = 'MOTIVDAB']) }</MOTIVDAB>
										<SWIMPRES>{ data($item/ns0:Column[@name = 'SWIMPRES']) }</SWIMPRES>
										<ORIGENRC>{ data($item/ns0:Column[@name = 'ORIGENRC']) }</ORIGENRC>
										<SWCONSTAN>{ data($item/ns0:Column[@name = 'SWCONSTAN']) }</SWCONSTAN>
                                 </RECIBO>
                         }
					 </RECIBOS>
                     </CAMPOS>

                 )
            }
        </Response>
};

declare variable $resp as element() external;

xf:TX_LBA_1430_From_PaginatedQueryRs($resp)