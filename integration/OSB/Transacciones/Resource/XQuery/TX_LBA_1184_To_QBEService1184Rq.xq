(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$request1" element="Request" location="../XSD/TX_LBA_1184_ProductosClientes.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QBEService1184Rq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/Transaccion/Resource/XQuery/xf:TX_LBA_1184_To_QBEService1184Rq/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:TX_LBA_1184_To_QBEService1184Rq($request1 as element(Request),
                                              $LOVs as element(lov:LOVs))
    as element() {
        <ns0:QBEService1184Rq>
            <ns0:QueryID>1184</ns0:QueryID>
            <ns0:FilterCriteria>
				                <ns0:FilterCriterion field = "PID"
                                     operation = "EQ"
                                     value = "{ 
								            	let $docType :=  data($LOVs/lov:LOV[@id = "DocTypeNameById"]/ns0:ListItems/ns0:ListItem/ns0:ItemID) 
								            	return (
								            	concat($docType , '-' , $request1/NUMEDOCU))
            									}"/>
            </ns0:FilterCriteria>
        </ns0:QBEService1184Rq>
};

declare variable $request1 as element(Request) external;
declare variable $LOVs as element(lov:LOVs) external;

xf:TX_LBA_1184_To_QBEService1184Rq($request1,$LOVs)

