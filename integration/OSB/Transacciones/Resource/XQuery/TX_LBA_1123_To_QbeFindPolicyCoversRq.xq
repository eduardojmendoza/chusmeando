xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/TX_LBA_1123_DetalleRiesgo.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QbeFindPolicyCoversRq" location="../../../INSIS-OV/Resource/XSD/Insis_QBE_v3.0.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-Transacciones/Resource/XQuery/OV_1123_To_QbeFindPolicyCoversRq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1123_To_QbeFindPolicyCoversRq($request as element(Request))
    as element() {
         <ns0:QbeFindPolicyCoversRq>
         			(: <!-- Extends FindPolicyByNumberRq --> :)
					 <ns0:PolicyNo>{ concat('0001',
                    						fn-bea:pad-right($request/RAMOPCOD, 4) ,
                    						fn-bea:format-number(xs:double($request/POLIZANN), '00') ,
											fn-bea:format-number(xs:double($request/POLIZSEC), '000000') ,
											fn-bea:format-number(xs:double($request/CERTIPOL), '0000') ,
											fn-bea:format-number(xs:double($request/CERTIANN), '0000') ,
											fn-bea:format-number(xs:double($request/CERTISEC), '000000')
									) }</ns0:PolicyNo>
					<ns0:IncludeClosedPolicies>true</ns0:IncludeClosedPolicies>
        </ns0:QbeFindPolicyCoversRq>
};

declare variable $request as element(Request) external;

xf:OV_1123_To_QbeFindPolicyCoversRq($request)