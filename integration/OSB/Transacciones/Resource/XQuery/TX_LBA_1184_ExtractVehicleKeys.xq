xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QBEService1184Rs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/TX_LBA_1184_ExtractVehicleKeys.xsd" ::)

declare namespace xf = "http://tempuri.org/Transacciones/Resource/XQuery/xf:TX_LBA_1184_ExtractVehicleKeys/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:TX_LBA_1184_ExtractVehicleKeys($resp as element())
    as element(Response) {
    <Response>
        <VehicleKeys>
                    	{
                         for $item in $resp/ns0:RowSet/ns0:Row
                         return
						(
	                    	<VehicleKey><Key>{ data($item/ns0:Column[@name = 'DATO1']) }</Key></VehicleKey>
	                    )}
        </VehicleKeys>
    </Response>
};

declare variable $resp as element() external;

xf:TX_LBA_1184_ExtractVehicleKeys($resp)

