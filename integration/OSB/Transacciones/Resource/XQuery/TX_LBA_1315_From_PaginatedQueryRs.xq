(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma  parameter="$ordenesResult" type="anyType" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/TX_LBA_1315_SeguimientoSiniestros.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1315_From_PaginatedQueryRs/";

declare function xf:OV_1315_From_PaginatedQueryRs($resp as element() ,$ordenesResult as element(*))
    as element(Response) {
        <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <CAMPOS>
                    	<CANTREG>{ data($resp/ns0:TotalRowCount) }</CANTREG>
                    	<SINIESTROS>
	                    {
	                        for $item in $resp/ns0:RowSet/ns0:Row
	                        return
                            (
                            	<SINIESTRO>
	                            	<FECSINIE>{ data($item/ns0:Column[@name = 'FECSINIE']) }</FECSINIE>
									<FECDENUN>{ data($item/ns0:Column[@name = 'FECDENUN']) }</FECDENUN>
									<RAMOPCOD>{ data(substring($item/ns0:Column[@name = 'CLAIM_NO'],5,4)) }</RAMOPCOD>
									<SINIEANN>{ data(substring($item/ns0:Column[@name = 'CLAIM_NO'],9,2)) }</SINIEANN>
									<SINIENUM>{ data(substring($item/ns0:Column[@name = 'CLAIM_NO'],11,6)) }</SINIENUM>
									<ESTADSIN>{ data($item/ns0:Column[@name = 'ESTADSIN']) }</ESTADSIN>
									<CANTORD>{ if (data($item/ns0:Column[@name = 'PAYMENT_NO']) != "") then (1) else (0) }</CANTORD>
								    <ORDENES>{data($ordenesResult/ORDENES[@numSiniestro=$item/ns0:Column[@name = 'CLAIM_NO']])}</ORDENES>
								</SINIESTRO>
	                         )
	                    }
	                    </SINIESTROS>
                    </CAMPOS>
                )
            }
        </Response>
};

declare variable $resp as element() external;
declare variable $ordenesResult as element(*) external;

xf:OV_1315_From_PaginatedQueryRs($resp,$ordenesResult)