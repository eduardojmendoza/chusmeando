(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QBEService1188Rs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/TX_LBA_1188_EnvioPoliza.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/Transacciones/Resource/XQuery/TX_LBA_1185_From_QBEService1188Rs/";

declare function xf:TX_LBA_1188_From_QBEService1188Rs($resp as element())
    as element(Response) {
        <Response>
            <Estado resultado = "true" mensaje = ""/>
            <CAMPOS>
                <CANTRECI>{ if (data($resp/ns0:CANTRECI) = '') then (0) else (data($resp/ns0:CANTRECI)) }</CANTRECI>
            </CAMPOS>
        </Response>
};

declare variable $resp as element() external;

xf:TX_LBA_1188_From_QBEService1188Rs($resp)
