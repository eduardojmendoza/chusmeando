xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QBEService1184Rs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$infoautoMap" element="InfoAutoMap" location="../XSD/InfoautoMap.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/TX_LBA_1184_ProductosClientes.xsd" ::)

declare namespace xf = "http://tempuri.org/Transacciones/Resource/XQuery/xf:TX_LBA_1184_From_QBEService1184Rs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:TX_LBA_1184_From_QBEService1184Rs($resp as element(),$infoautoMap as element(), $tipodocu as xs:string)
    as element(Response) {
        <Response>
        	{ if (exists($resp/ns0:CLIENSEC) and data($resp/ns0:CLIENSEC) != '') then
        	(
	        	<Estado resultado="true" mensaje="" />,
	    		<CAMPOS>
						<CLIENSEC>{data($resp/ns0:CLIENSEC)}</CLIENSEC>
						<TIPODOCU>{$tipodocu}</TIPODOCU>
						<NUMEDOCU>{data($resp/ns0:NUMEDOCU)}</NUMEDOCU>
						<CLIENAP1>{data($resp/ns0:CLIENAP1)}</CLIENAP1>
						<CLIENAP2>{data($resp/ns0:CLIENAP2)}</CLIENAP2>
						<CLIENNOM>{data($resp/ns0:CLIENNOM)}</CLIENNOM>
	                	<PRODUCTOS>
	                	{
	                     for $item in $resp/ns0:RowSet/ns0:Row
	                     return
						(
	                    	<PRODUCTO>
								<CIAASCOD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],1,4)) }</CIAASCOD>
								<RAMOPCOD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)) }</RAMOPCOD>
		                  		<POLIZANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,2)) }</POLIZANN>
		                  		<POLIZSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],11,6)) }</POLIZSEC>
		                  		<CERTIPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERTIPOL>
						  		<CERTIANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERTIANN>
						  		<CERTISEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERTISEC>
								<SUPLENUM>{ data($item/ns0:Column[@name = 'SUPLENUM']) }</SUPLENUM>
								<RAMOPDES>{ data($item/ns0:Column[@name = 'RAMOPDES']) }</RAMOPDES>
								<CLIENSECV>{ data($item/ns0:Column[@name = 'CLIENSECV']) }</CLIENSECV>
								<CLIENAP1V>{ data($item/ns0:Column[@name = 'CLIENAP1V']) }</CLIENAP1V>
								<CLIENAP2V>{ data($item/ns0:Column[@name = 'CLIENAP2V']) }</CLIENAP2V>
								<CLIENNOMV>{ data($item/ns0:Column[@name = 'CLIENNOMV']) }</CLIENNOMV>
								<AGENTCOD>{ data($item/ns0:Column[@name = 'AGENTCOD'])}</AGENTCOD>
								<AGENTCLA>{ data($item/ns0:Column[@name = 'AGENTCLA'])}</AGENTCLA>
								<TOMARIES>{data($infoautoMap/VehicleKey[Key = data($item/ns0:Column[@name = 'DATO1'])]/AUMARDES)} {data($infoautoMap/VehicleKey[Key = data($item/ns0:Column[@name = 'DATO1'])]/AUMODDES)} {data($item/ns0:Column[@name = 'DATO2']) }</TOMARIES>
								<SITUCPOL>{ data($item/ns0:Column[@name = 'SITUCPOL']) }</SITUCPOL>
								<EMISIANN>{ year-from-date(data($item/ns0:Column[@name = 'ISSUE_DATE'])) }</EMISIANN>
								<EMISIMES>{ month-from-date(data($item/ns0:Column[@name = 'ISSUE_DATE'])) }</EMISIMES>
								<EMISIDIA>{ day-from-date(data($item/ns0:Column[@name = 'ISSUE_DATE'])) }</EMISIDIA>
								<TIPOPROD>C</TIPOPROD>
								<PATENTE>{ data($item/ns0:Column[@name = 'PATENTE']) }</PATENTE>
								<DATO1>{ data($infoautoMap/VehicleKey[Key = data($item/ns0:Column[@name = 'DATO1'])]/AUMARCOD) }</DATO1>
								<DATO2>{ data($item/ns0:Column[@name = 'DATO2']) }</DATO2>
								<DATO3>{ data($item/ns0:Column[@name = 'DATO3']) }</DATO3>
								<DATO4>{ data($item/ns0:Column[@name = 'DATO4']) }</DATO4>
								<COBRODES>{ data($item/ns0:Column[@name = 'COBRODES'])}</COBRODES>
								<SWSUSCRI>{ data($item/ns0:Column[@name = 'SWSUSCRI']) }</SWSUSCRI>
								<MAIL>{ data($item/ns0:Column[@name = 'MAIL']) }</MAIL>
								<CLAVE>{ data($item/ns0:Column[@name = 'CLAVE']) }</CLAVE>
								<SWCLAVE>{ data($item/ns0:Column[@name = 'SWCLAVE']) }</SWCLAVE>
								<MENSASIN>{ data($item/ns0:Column[@name = 'MENSASIN'])}</MENSASIN>
								<MENSASUS>{ data($item/ns0:Column[@name = 'MENSASUS']) }</MENSASUS>
								<SWIMPRIM>{ data($item/ns0:Column[@name = 'SWIMPRIM']) }</SWIMPRIM>
							 	<COBRANZA>{ data($item/ns0:Column[@name = 'COBRANZA']) }</COBRANZA>
								<SINIESTRO>{data($item/ns0:Column[@name = 'SINIESTRO'])}</SINIESTRO>
								<CONSTANCIA>{ data($item/ns0:Column[@name = 'CONSTANCIA']) }</CONSTANCIA>
	                    	</PRODUCTO>
	                    	)}
	                    </PRODUCTOS>
	                </CAMPOS>)
        	else (
        			<Estado resultado="false" mensaje="" />,
        			<CAMPOS/>
        		)
        	}
        </Response>
};

declare variable $resp as element() external;
declare variable $infoautoMap as element() external;
declare variable $tipodocu as xs:string external;


xf:TX_LBA_1184_From_QBEService1184Rs($resp,$infoautoMap, $tipodocu)

