xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$service1232" element="Response" location="../../../OV-mqgen/Resource/XSD/Service1232.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$QbeFindPolicyCoversRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeFindPolicyCoversRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/TX_LBA_1123_DetalleRiesgo.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";
declare namespace xf = "http://tempuri.org/OV-Transacciones/Resource/XQuery/OV_1123_From_FndPlcyByNumRs/";

declare function xf:OV_1123_From_FndPlcyByNumRs($QbeFindPolicyCoversRs as element(), $service1232 as element(), $LOVs as element() , $grupo as xs:integer)
    as element(Response) {
        <Response>
        	<Estado resultado="true" mensaje=""/>
            {
                let $Insured := $QbeFindPolicyCoversRs/ns0:Insureds/ns0:Insured[1]
                return
                    <CAMPOS>
                        <GRUPO> {$grupo}</GRUPO>
                    	<AUMARCOD>{ data($service1232/CAMPOS/AUMARCOD) }</AUMARCOD>
                    	<AUMARDES>{ data($service1232/CAMPOS/AUMARDES) }</AUMARDES>
                    	<AUMODCOD>{ data($service1232/CAMPOS/AUMODCOD) } </AUMODCOD>
                    	<AUMODDES>{ data($service1232/CAMPOS/AUMODDES) } </AUMODDES>
                    	<RIESGDAB>(:Not Used for Scoring:)</RIESGDAB>
						<NUMTARJ>(:Not Used for Scoring:)</NUMTARJ>
						<COBROTIP>(:Not Used for Scoring:)</COBROTIP>
						<COBRODES>(:Not Used for Scoring:)</COBRODES>
						<CUENTNUM>(:Not Used for Scoring:)</CUENTNUM>
						<PLANNCODA>(:Not Used for Scoring:)</PLANNCODA>
						<PLANNDABA>(:Not Used for Scoring:)</PLANNDABA>
						<MARCACOD>(:Not Used for Scoring:)</MARCACOD>
						<MARCADES>(:Not Used for Scoring:)</MARCADES>
						<ARTICCOD>(:Not Used for Scoring:)</ARTICCOD>
						<ARTICDES>(:Not Used for Scoring:)</ARTICDES>
						<PLANNCODE>(:Not Used for Scoring:)</PLANNCODE>
						<PLANNDESE>(:Not Used for Scoring:)</PLANNDESE>
						<SUMAASEG>(:Not Used for Scoring:)</SUMAASEG>
						<BUMARCOD>(:Not Used for Scoring:)</BUMARCOD>
						<BUMARDES>(:Not Used for Scoring:)</BUMARDES>
						<BUMODCOD>(:Not Used for Scoring:)</BUMODCOD>
						<BUMODDES>(:Not Used for Scoring:)</BUMODDES>
						<CANTREG>{ count($Insured/ns0:CoverPackages/ns0:CoverPackage/ns0:Covers/ns0:Cover )}</CANTREG>
						<COBERTURAS>
			                {
			                     for $Cover in $Insured/ns0:CoverPackages/ns0:CoverPackage/ns0:Covers/ns0:Cover
			                     return
			                         <COBERTURA>
				                       	<COBERCOD>{data($LOVs/lov:LOV[@id = "CoverByInsisCode"]/ns0:ListItems/ns0:ListItem[ ns0:ItemDescription = data($Cover/ns0:CoverDescription)]/ns0:ItemID)}</COBERCOD>
										<COBERDES>{upper-case(data($Cover/ns0:CoverDescription))}</COBERDES>
										<SUMAASEG>{fn-bea:format-number(data($Cover/ns0:InsuredAmount)* 100, '000')}</SUMAASEG>
										<RIESGTIP>{data($LOVs/lov:LOV[@id = "RiskByInsisCode"]/ns0:ListItems/ns0:ListItem[ ns0:ItemDescription = data($Cover/ns0:Risks/ns0:Risk[1]/ns0:RiskType)]/ns0:ItemID)}</RIESGTIP>
			                 			<SEWRIEDES>(:Not Used for Scoring:)</SEWRIEDES>
			                 		</COBERTURA>
			                }
			            </COBERTURAS>


                    </CAMPOS>
            }
        </Response>
};

declare variable $QbeFindPolicyCoversRs as element() external;
declare variable $service1232 as element() external;
declare variable $LOVs as element(lov:LOVs) external;
declare variable $grupo as xs:integer external;
 

xf:OV_1123_From_FndPlcyByNumRs($QbeFindPolicyCoversRs, $service1232, $LOVs, $grupo)