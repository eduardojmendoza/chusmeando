xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/TX_LBA_1430_SeguimientoCobranzas.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/TX_LBA_1430_To_PaginatedQueryRs.xq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:TX_LBA_1430_To_PaginatedQueryRs($req as element(Request))
    as element() {
        <ns0:PaginatedQueryRq>
            <ns0:QueryID>1430</ns0:QueryID>
             <ns0:FilterCriteria>
            		<ns0:FilterCriterion field = "POLICY_NO"
                                     operation = "EQ"
                                     value = "{ xqubh:buildPolicyNo-splitted('0001', data($req/RAMOPCOD), data($req/POLIZANN), data($req/POLIZSEC), data($req/CERTIPOL), data($req/CERTIANN), data($req/CERTISEC)) }"/>
           </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $req as element(Request) external;

xf:TX_LBA_1430_To_PaginatedQueryRs($req)