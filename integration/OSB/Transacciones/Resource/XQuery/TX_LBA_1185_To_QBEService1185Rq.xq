(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/TX_LBA_1185_SusyDesus_epoliza.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QBEService1185Rq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/Transacciones/Resource/XQuery/TX_LBA_1185_To_QBEService1185Rq/";

declare function xf:TX_LBA_1185_To_QBEService1185Rq($req as element(Request))
    as element() {
        <ns0:QBEService1185Rq>
            {
                for $ePoliza in $req/EPOLIZAS/EPOLIZA
                return
                    <ns0:Service1185Data>
                        <ns0:PolicyNo>
                        	{
                        		concat( if (data($ePoliza/CIAASCOD) != '' ) then (data($ePoliza/CIAASCOD)) else ('0001'),
										fn-bea:pad-right($ePoliza/RAMOPCOD,4),
										fn-bea:format-number(xs:double($ePoliza/POLIZANN), '00'),
										fn-bea:format-number(xs:double($ePoliza/POLIZSEC), '000000'),
										fn-bea:format-number(xs:double($ePoliza/CERTIPOL), '0000'),
										fn-bea:format-number(xs:double($ePoliza/CERTIANN), '0000'),
										fn-bea:format-number(xs:double($ePoliza/CERTISEC), '000000'))
                        	}
                        </ns0:PolicyNo>
                        <ns0:SWSUSCRI>{ data($ePoliza/SWSUSCRI) }</ns0:SWSUSCRI>
                        <ns0:MAIL>{ data($ePoliza/MAIL) }</ns0:MAIL>
                        <ns0:SWTIPOSUS>{ data($ePoliza/SWTIPOSUS) }</ns0:SWTIPOSUS>
                    </ns0:Service1185Data>
            }
        </ns0:QBEService1185Rq>
};

declare variable $req as element(Request) external;

xf:TX_LBA_1185_To_QBEService1185Rq($req)
