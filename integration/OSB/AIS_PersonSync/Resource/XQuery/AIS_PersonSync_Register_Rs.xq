(:: pragma bea:mfl-element-parameter parameter="$resp" type="AIS_PersonSync_Register_Rs@" location="../MFL/AIS_PersonSync_Register_Rs.mfl" ::)
(:: pragma bea:global-element-return element="ns0:QbeRegisterNewPersonRs" location="../WSDL/AIS_PersonSync.wsdl" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/AIS_PersonSync/Resource/XQuery/AIS_PersonSync_Register_Rs/";

declare function xf:AIS_PersonSync_Register_Rs($resp as element())
    as element(ns0:QbeRegisterNewPersonRs) {
        <ns0:QbeRegisterNewPersonRs>
            <ns0:ManID>{ data($resp/MANID) }</ns0:ManID>
            <ns0:ClientID>{ data($resp/CLIENTID) }</ns0:ClientID>
        </ns0:QbeRegisterNewPersonRs>
};

declare variable $resp as element() external;

xf:AIS_PersonSync_Register_Rs($resp)