(:: pragma bea:global-element-parameter parameter="$resp" element="ns0:personUpdatedResponse" location="../../../INSIS-OV/Resource/WSDL/SynchPerson.wsdl" ::)
(:: pragma bea:mfl-element-return type="AIS_PersonSync_Trigger_Rs@" location="../MFL/AIS_PersonSync_Trigger_Rs.mfl" ::)

declare namespace ns1 = "http://fadata.eu/integration/qbe/SynchPersonService/v1.0/types";
declare namespace ns0 = "http://fadata.eu/integration/qbe/SynchPersonService/v1.0";
declare namespace xf = "http://tempuri.org/AIS_PersonSync/Resource/XQuery/AIS_PersonSync_Trigger_Rs/";

declare function xf:AIS_PersonSync_Trigger_Rs($resp as element(ns0:personUpdatedResponse))
    as element() {
        <AIS_PersonSync_Trigger_Rs>
            <MENSAJE>{ data($resp/ns0:return/message) }</MENSAJE>
            <STATUS-CODE>{ data($resp/ns0:return/statusCode) }</STATUS-CODE>
        </AIS_PersonSync_Trigger_Rs>
};

declare variable $resp as element(ns0:personUpdatedResponse) external;

xf:AIS_PersonSync_Trigger_Rs($resp)