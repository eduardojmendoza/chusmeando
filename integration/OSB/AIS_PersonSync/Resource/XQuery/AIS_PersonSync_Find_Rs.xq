xquery version "1.0" encoding "UTF-8";
(:: pragma bea:mfl-element-parameter parameter="$contact" type="PersonSync@" location="../MFL/AIS_PersonSync.mfl" ::)
(:: pragma bea:global-element-return element="ns0:QbeFindPersonByPIDRs" location="../WSDL/AIS_PersonSync.wsdl" ::)

declare namespace xf = "http://tempuri.org/AIS_PersonSync/Resource/XQuery/AIS_PersonSync_Find_Rs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:AIS_PersonSync_Find_Rs($contact as element())
    as element(ns0:QbeFindPersonByPIDRs) {
        <ns0:QbeFindPersonByPIDRs>
            <ns0:Entity>
            	{
                    if (data($contact/CLIENTIP) = '00') then (
        				<ns0:PersonalData>
		                    <ns0:Name>
		                    	<ns0:Given>{ data($contact/CLIENNOM) }</ns0:Given>
		                        <ns0:Family>{ fn-bea:trim(concat(data($contact/CLIENAP1), ' ', data($contact/CLIENAP2))) }</ns0:Family>
		                    </ns0:Name>
		                	<ns0:PID>{ concat(data($contact/DOCUMTIP), '-', data($contact/DOCUMDAT)) }</ns0:PID>
		                    <ns0:Gender>
		                    	{
		                    		let $sexo := upper-case(data($contact/CLIENSEX))
		                    		return
		                    			if ($sexo = 'M') then ( 1 )
		                    			else if ($sexo = 'F') then ( 2 )
		                    			else ( 0 ) (: N o vacio :)
		                    	}
		                    </ns0:Gender>
		                    <ns0:BirthDate>{ local:getDate(data($contact/NACIM)) }</ns0:BirthDate>
							<ns0:HomeCountry>{ data($contact/PAISSCOD) }</ns0:HomeCountry>
        				</ns0:PersonalData>
		            ) else (
		                <ns0:CompanyData>
		                    <ns0:Name>{ fn-bea:trim(concat(data($contact/CLIENAP1), ' ', data($contact/CLIENAP2), ' ', data($contact/CLIENNOM))) }</ns0:Name>
		                	<ns0:CustomerID>{ concat(data($contact/DOCUMTIP), '-', data($contact/DOCUMDAT)) }</ns0:CustomerID>
		                    <ns0:CompType>{ data($contact/CLIENTIP) }</ns0:CompType>
		                </ns0:CompanyData>
		            )
		        }
                <ns0:ManID>{ data($contact/CLIENSEC) }</ns0:ManID>
                <ns0:NameSuffix>{ data($contact/DOCUMTIP) }</ns0:NameSuffix>
                <ns0:Nationality>{ data($contact/PAISSCOD) }</ns0:Nationality>
                <ns0:IndustryCode>{ data($contact/PROFECOD) }</ns0:IndustryCode>
                {
                	if (data($contact/CLIENTIP) = '00') then (
						<ns0:DataSource>{ data($contact/CLIENEST) }</ns0:DataSource>
					) else ()
				}
                <ns0:Questionary>
                	<ns0:Question>
                		<ns0:ID>3005.32</ns0:ID>
                		<ns0:Answer>{ fn-bea:date-to-string-with-format('dd-MM-yyyy', local:getDate(data($contact/EFECT))) }</ns0:Answer>
                	</ns0:Question>
                	<ns0:Question>
                		<ns0:ID>3005.36</ns0:ID>
                		<ns0:Answer>{ fn-bea:date-to-string-with-format('dd-MM-yyyy', local:getDate(data($contact/FBAJA))) }</ns0:Answer>
                	</ns0:Question>
                </ns0:Questionary>
                <ns0:Documents?>
                	{
                		for $i in (1 to 2)
                			let $id := ('ADD_DOC1', 'ADD_DOC2')
                			let $tipo := ($contact/DOCUMTIP2 | $contact/DOCUMTIP3)
                			let $detalle := ($contact/DOCUMDAT2 | $contact/DOCUMDAT3)
                		return
                			if (data($tipo[$i]) != '0') then (
	                			<ns0:Document>
			                        <ns0:DocumentID>{ $id[$i] }</ns0:DocumentID>
			                        <ns0:DocumentSerial>{ data($tipo[$i]) }</ns0:DocumentSerial>
			                        <ns0:DocumentNo>{ data($detalle[$i]) }</ns0:DocumentNo>
	                			</ns0:Document>
                			) else ()
                	}
                	{
                		for $aml at $i in $contact/AMLs/AML
                		let $DocumentSerial := data($aml/AML_DOC_TYPE)
                		return
                			if ($DocumentSerial != '0')
                			then (
	                			<ns0:Document>
			                        <ns0:DocumentID>{ concat('LAUNDRY_', $i) }</ns0:DocumentID>
			                        <ns0:DocumentSerial>{ $DocumentSerial }</ns0:DocumentSerial>
			                        <ns0:DocumentNo>{ data($aml/AML_DOC_LETTER_NUMBER) }</ns0:DocumentNo>
			                        <ns0:DocumentDate>{ local:getDate(data($aml/AML_EFFEC_FROM_DATE)) }</ns0:DocumentDate>
			                        <ns0:DocumentValidTo>{ local:getDate(data($aml/AML_EXPIRATION_DATE)) }</ns0:DocumentValidTo>
	                			</ns0:Document>
                			) else ()
                	}
                </ns0:Documents>
            </ns0:Entity>
            <ns0:Addresses?>
                {
                    for $Address in $contact/Addresses/Address
					let $AddressType := data($Address/DOMICCAL)
            		return
            			if (fn-bea:trim($AddressType) != '')
            			then (
	                        <ns0:Address>
	                            <ns0:CountryCode>{ data($Address/PAISSCOD_2) }</ns0:CountryCode>
	                            <ns0:StateRegion>{ data($Address/PROVICOD) }</ns0:StateRegion>
	                            <ns0:City>{ data($Address/DOMICPOB) }</ns0:City>
	                            <ns0:ResidentialAddress>
	                                <ns0:StreetName>{ data($Address/DOMICDOM) }</ns0:StreetName>
	                                <ns0:StreetNo>{ data($Address/DOMICDNU) }</ns0:StreetNo>
	                                <ns0:Floor>{ data($Address/DOMICPIS) }</ns0:Floor>
	                                <ns0:Apartment>{ data($Address/DOMICPTA) }</ns0:Apartment>
	                            </ns0:ResidentialAddress>
	                            <ns0:ZipCode>{ data($Address/DOMICCPO) }</ns0:ZipCode>
	                            <ns0:AddressType>{ $AddressType }</ns0:AddressType>
	                            <ns0:TerritoryClass>{ data($Address/CPACODPO) }</ns0:TerritoryClass>
	                            <ns0:PrimaryFlag>{ data($Address/PrimaryFlag) }</ns0:PrimaryFlag>
	                        </ns0:Address>
	                    ) else ()
                }
            </ns0:Addresses>
            <ns0:Contacts?>
            	{
                    for $Address in $contact/Addresses/Address
					let $DOMICPRE := data($Address/DOMICPRE)
					let $DOMICTLF := data($Address/DOMICTLF)
					return
						if (not(fn-bea:trim($DOMICPRE) = '' and fn-bea:trim($DOMICTLF) = ''))
						then (
			                <ns0:Contact>
			                    <ns0:Type>ADDR</ns0:Type>
			                    <ns0:Details>{ concat($DOMICPRE, $DOMICTLF) }</ns0:Details>
			                </ns0:Contact>
			            ) else ()
            	}
                {
                    for $Contact in $contact/Contacts/Contact
					let $Type := data($Contact/MEDCOCOD)
            		return
            			if (fn-bea:trim($Type) != '')
            			then (
	                        <ns0:Contact>
	                            <ns0:Type>{ $Type }</ns0:Type>
	                            <ns0:Details>{ data($Contact/MEDCODAT) }</ns0:Details>
	                        </ns0:Contact>
                        ) else ()
                }
            </ns0:Contacts>
            <ns0:PaymentInstruments?>
                {
                    for $PaymentInstrument in $contact/PaymentInstruments/PaymentInstrument
					let $AccountType := data($PaymentInstrument/COBROTIP)
					let $VENCIANN := fn-bea:pad-left(data($PaymentInstrument/VENCIANN), 4, '0')
					let $VENCIMES := fn-bea:pad-left(data($PaymentInstrument/VENCIMES), 2, '0')
					let $TITULAP1 := fn-bea:trim(data($PaymentInstrument/TITULAP1))
					let $TITULAP2 := fn-bea:trim(data($PaymentInstrument/TITULAP2))
					let $TITULAPE := fn-bea:trim(concat($TITULAP1, ' ', $TITULAP2))
					let $TITULNOM := fn-bea:trim(data($PaymentInstrument/TITULNOM))
                    return
            			if (fn-bea:trim($AccountType) != '')
            			then (
	                        <ns0:PaymentInstrument>
								<ns0:AccountType>{ $AccountType }</ns0:AccountType>
								<ns0:AccountNo?>{ data($PaymentInstrument/CUENTNUM) }</ns0:AccountNo>
								<ns0:BankCode?>{ local:zeroToEmpty(data($PaymentInstrument/BANCOCOD)) }</ns0:BankCode>
								<ns0:BranchCode?>{ local:zeroToEmpty(data($PaymentInstrument/SUCURCOD)) }</ns0:BranchCode>
								<ns0:Currency?>{ data($PaymentInstrument/MONEDA) }</ns0:Currency>
								<ns0:ValidTo?>{ local:getDate(concat($VENCIANN, $VENCIMES, '01')) }</ns0:ValidTo>
								<ns0:AccountHolderName?>{ fn-bea:trim(concat($TITULAPE, ' ', $TITULNOM)) }</ns0:AccountHolderName>
	                        </ns0:PaymentInstrument>
                        ) else ()
                }
            </ns0:PaymentInstruments>
            <ns0:TaxData>
                <ns0:Rate>{ data($contact/CLIENDGI) }</ns0:Rate>
                <ns0:IVAType>{ data($contact/CLIENIVA) }</ns0:IVAType>
                <ns0:GANType>{ data($contact/CLIEGNTP) }</ns0:GANType>
                <ns0:IBRType>{ data($contact/CLIEIBTP) }</ns0:IBRType>
                <ns0:IBRNumber>{ data($contact/CLIEIBNU) }</ns0:IBRNumber>
                <ns0:IITType>{ data($contact/CLIEIITP) }</ns0:IITType>
                {
                    let $IVATax := $contact/TaxData/IVATax
                    return
		                <ns0:IVATax?>
		                    <ns0:Perception?>
		                        <ns0:ExemptionPerc?>{ local:zeroToEmpty(data($IVATax/Perception/ExemptionPerc)) }</ns0:ExemptionPerc>
		                        <ns0:ExemptionDate?>{ local:getDate(data($IVATax/Perception/ExemptionDate)) }</ns0:ExemptionDate>
		                        <ns0:Reason?>{ local:nullIfEmpty(data($IVATax/Perception/Reason)) }</ns0:Reason>
		                    </ns0:Perception>
		                    <ns0:Retention?>
		                        <ns0:ExemptionPerc?>{ local:zeroToEmpty(data($IVATax/Retention/ExemptionPerc)) }</ns0:ExemptionPerc>
		                        <ns0:ExemptionDate?>{ local:getDate(data($IVATax/Retention/ExemptionDate)) }</ns0:ExemptionDate>
	                        	<ns0:Reason?>{ local:nullIfEmpty(data($IVATax/Retention/Reason)) }</ns0:Reason>
		                    </ns0:Retention>
		                </ns0:IVATax>
		        }
                {
                    let $GANTax := $contact/TaxData/GANTax
                    return
                        <ns0:GANTax?>
                            <ns0:ExemptionPerc?>{ local:zeroToEmpty(data($GANTax/ExemptionPerc)) }</ns0:ExemptionPerc>
                            <ns0:ExemptionDate?>{ local:getDate(data($GANTax/ExemptionDate)) }</ns0:ExemptionDate>
                            <ns0:Reason?>{ local:nullIfEmpty(data($GANTax/Reason)) }</ns0:Reason>
                        </ns0:GANTax>
                }
                <ns0:IBRTaxes?>
                    {
                        for $IBRTax in $contact/TaxData/IBRTaxes/IBRTax
                        return
                        	if (data($IBRTax/Province) != '0') then (
	                            <ns0:IBRTax?>
	                                <ns0:Province?>{ local:zeroToEmpty(data($IBRTax/Province)) }</ns0:Province>
	                                <ns0:Perception?>
	                                    <ns0:ExemptionPerc?>{ local:zeroToEmpty(data($IBRTax/Perception/ExemptionPerc)) }</ns0:ExemptionPerc>
	                                    <ns0:ExemptionDate?>{ local:getDate(data($IBRTax/Perception/ExemptionDate)) }</ns0:ExemptionDate>
	                                    <ns0:Reason?>{ local:nullIfEmpty(data($IBRTax/Perception/Reason)) }</ns0:Reason>
	                                </ns0:Perception>
	                                <ns0:Retention?>
	                                    <ns0:ExemptionPerc?>{ local:zeroToEmpty(data($IBRTax/Retention/ExemptionPerc)) }</ns0:ExemptionPerc>
	                                    <ns0:ExemptionDate?>{ local:getDate(data($IBRTax/Retention/ExemptionDate)) }</ns0:ExemptionDate>
	                                    <ns0:Reason?>{ local:nullIfEmpty(data($IBRTax/Retention/Reason)) }</ns0:Reason>
	                                </ns0:Retention>
	                                <ns0:CM05Form?>
	                                    <ns0:GIMPerc?>{ local:zeroToEmpty(data($IBRTax/CM05Form/GIMPerc)) }</ns0:GIMPerc>
	                                    <ns0:GIMDate?>{ local:getDate(data($IBRTax/CM05Form/GIMDate)) }</ns0:GIMDate>
	                                </ns0:CM05Form>
	                                <ns0:ContribType>{ data($IBRTax/ContribType) }</ns0:ContribType>
	                            </ns0:IBRTax>
                        	) else ()
                    }
                </ns0:IBRTaxes>
                {
                    let $IITTax := $contact/TaxData/IITTax
                    return
		                <ns0:IITTax?>
		                    <ns0:ExemptionPerc?>{ local:zeroToEmpty(data($IITTax/ExemptionPerc)) }</ns0:ExemptionPerc>
		                    <ns0:ExemptionDate?>{ local:getDate(data($IITTax/ExemptionDate)) }</ns0:ExemptionDate>
		                    <ns0:Reason?>{ local:nullIfEmpty(data($IITTax/Reason)) }</ns0:Reason>
		                </ns0:IITTax>
                }
                {
                    let $ISSTax := $contact/TaxData/ISSTax
                    return
		                <ns0:ISSTax?>
		                    <ns0:AliquotPerc?>{ local:zeroToEmpty(data($ISSTax/AliquotPerc)) }</ns0:AliquotPerc>
		                    <ns0:AliquotDate?>{ local:getDate(data($ISSTax/AliquotDate)) }</ns0:AliquotDate>
		                    <ns0:Reason?>{ local:nullIfEmpty(data($ISSTax/Reason)) }</ns0:Reason>
		                </ns0:ISSTax>
                }
                <ns0:SELTaxes?>
                    {
                        for $SELTax in $contact/TaxData/SELTaxes/SELTax
                        return
                            if (data($SELTax/Province) != '0') then (
	                            <ns0:SELTax?>
	                                <ns0:Province?>{ local:zeroToEmpty(data($SELTax/Province)) }</ns0:Province>
	                                <ns0:ExemptionPerc?>{ local:zeroToEmpty(data($SELTax/ExemptionPerc)) }</ns0:ExemptionPerc>
	                                <ns0:ExemptionDate?>{ local:getDate(data($SELTax/ExemptionDate)) }</ns0:ExemptionDate>
	                                <ns0:Reason?>{ local:nullIfEmpty(data($SELTax/Reason)) }</ns0:Reason>
	                            </ns0:SELTax>
                            ) else ()
                    }
                </ns0:SELTaxes>
                <ns0:ApplySS>{ data($contact/TaxData/ApplySS) }</ns0:ApplySS>
                <ns0:Activity>{ data($contact/TaxData/Activity) }</ns0:Activity>
            </ns0:TaxData>
        </ns0:QbeFindPersonByPIDRs>
};

declare variable $contact as element() external;

declare function local:getDate($date as xs:string?) as xs:date?
{
	if ( $date != '00000000' and matches($date, '\d{8}') and substring($date, 1, 4) != '0000' and substring($date, 5, 2) != '00' and substring($date, 7, 2) != '00' )
	then ( fn-bea:date-from-string-with-format("yyyyMMdd", $date) )
	else ( )
};

declare function local:zeroToEmpty($value as xs:string?) as xs:string?
{
	if ( exists($value) and $value != '0' and xs:decimal($value) != 0 )
	then ( xs:string($value) )
	else ( )
};

declare function local:nullIfEmpty($value as xs:string?) as xs:string?
{
	if ( exists($value) and $value != '' )
	then ( $value )
	else ( )
};


xf:AIS_PersonSync_Find_Rs($contact)