xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$contact" element="ns0:QbeRegisterNewPersonRq" location="../WSDL/AIS_PersonSync.wsdl" ::)
(:: pragma bea:mfl-element-return type="PersonSync@" location="../MFL/AIS_PersonSync.mfl" ::)

declare namespace xf = "http://tempuri.org/AIS_PersonSync/Resource/XQuery/AIS_PersonSync_Register_Rq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:AIS_PersonSync_Register_Rq($contact as element(ns0:QbeRegisterNewPersonRq))
    as element() {
        <PersonSync>
            <CLIENSEC>{ local:adaptNumber(data($contact/ns0:Entity/ns0:ManID), 9) }</CLIENSEC>
            {
            	if ( exists($contact/ns0:Entity/ns0:PersonalData) ) then (
		            <DOCUMTIP>{ local:adaptNumber(substring-before(data($contact/ns0:Entity/ns0:PersonalData/ns0:PID), '-'), 2) }</DOCUMTIP>,
            		<DOCUMDAT>{ local:adaptString(substring-after(data($contact/ns0:Entity/ns0:PersonalData/ns0:PID), '-'), 11) }</DOCUMDAT>,
            		<CLIENTIP>00</CLIENTIP>,
		            <CLIENAP1>{ local:adaptString(substring(data($contact/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Family), 1, 20), 20) }</CLIENAP1>,
		            <CLIENAP2>{ local:adaptString(substring(data($contact/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Family), 21, 20), 20) }</CLIENAP2>,
		            <CLIENNOM>{ local:adaptString(fn-bea:trim(concat(data($contact/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Given), data($contact/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Surname))), 30) }</CLIENNOM>,
            		<NACIM>{ local:adaptDate($contact/ns0:Entity/ns0:PersonalData/ns0:BirthDate) }</NACIM>
            	) else (
	            	<DOCUMTIP>{ local:adaptNumber(substring-before(data($contact/ns0:Entity/ns0:CompanyData/ns0:CustomerID), '-'), 2) }</DOCUMTIP>,
	            	<DOCUMDAT>{ local:adaptString(substring-after(data($contact/ns0:Entity/ns0:CompanyData/ns0:CustomerID), '-'), 11) }</DOCUMDAT>,
	            	<CLIENTIP>{ local:adaptString(data($contact/ns0:Entity/ns0:CompanyData/ns0:CompType), 2) }</CLIENTIP>,
		            <CLIENAP1>{ local:adaptString(substring(data($contact/ns0:Entity/ns0:CompanyData/ns0:Name), 1, 20), 20) }</CLIENAP1>,
		            <CLIENAP2>{ local:adaptString(substring(data($contact/ns0:Entity/ns0:CompanyData/ns0:Name), 21, 20), 20) }</CLIENAP2>,
            		<CLIENNOM>{ local:adaptString(substring(data($contact/ns0:Entity/ns0:CompanyData/ns0:Name), 41, 30), 30) }</CLIENNOM>,
            		<NACIM>00000000</NACIM>
            	)
            }
            <EFECT>{ local:adaptDateAnswer($contact/ns0:Entity/ns0:Questionary/ns0:Question[ns0:ID='3005.32']/ns0:Answer) }</EFECT>
			{
				if ( exists($contact/ns0:Entity/ns0:PersonalData) ) then (
            		<CLIENSEX>
            			{
                    		let $Gender := data($contact/ns0:Entity/ns0:PersonalData/ns0:Gender)
                    		return
                    			if ($Gender = 1) then ( 'M' )
                    			else if ($Gender = 2) then ( 'F' )
                    			else if ($Gender = 0) then ( 'N' )
                    			else ( local:adaptString($Gender, 1) )
            			}
            		</CLIENSEX>,
            		<CLIENEST>{ local:adaptString(data($contact/ns0:Entity/ns0:DataSource), 1) }</CLIENEST>,
            		<PAISSCOD>{ local:adaptString(data($contact/ns0:Entity/ns0:PersonalData/ns0:HomeCountry), 2) }</PAISSCOD>
				) else (
				    <CLIENSEX>{ local:adaptString('N', 1) }</CLIENSEX>,
            		<CLIENEST>{ local:adaptString('N', 1) }</CLIENEST>,
            		<PAISSCOD>{ local:adaptString('00', 2) }</PAISSCOD>
				)
			}
            <FBAJA>{ local:adaptDateAnswer($contact/ns0:Entity/ns0:Questionary/ns0:Question[ns0:ID='3005.36']/ns0:Answer) }</FBAJA>
			<DOCUMTIP2>{ local:adaptNumber(data($contact/ns0:Entity/ns0:Documents/ns0:Document[ns0:DocumentID = "ADD_DOC1"]/ns0:DocumentSerial), 2) }</DOCUMTIP2>
			<DOCUMDAT2>{ local:adaptString(data($contact/ns0:Entity/ns0:Documents/ns0:Document[ns0:DocumentID = "ADD_DOC1"]/ns0:DocumentNo), 11) }</DOCUMDAT2>
			<DOCUMTIP3>{ local:adaptNumber(data($contact/ns0:Entity/ns0:Documents/ns0:Document[ns0:DocumentID = "ADD_DOC2"]/ns0:DocumentSerial), 2) }</DOCUMTIP3>
			<DOCUMDAT3>{ local:adaptString(data($contact/ns0:Entity/ns0:Documents/ns0:Document[ns0:DocumentID = "ADD_DOC2"]/ns0:DocumentNo), 11) }</DOCUMDAT3>
            <CLIENDGI>{ local:adaptString(data($contact/ns0:TaxData/ns0:Rate), 1) }</CLIENDGI>
            <CLIENIVA>{ local:adaptString(data($contact/ns0:TaxData/ns0:IVAType), 1) }</CLIENIVA>
            <CLIEGNTP>{ local:adaptString(data($contact/ns0:TaxData/ns0:GANType), 1) }</CLIEGNTP>
            <CLIEIBTP>{ local:adaptString(data($contact/ns0:TaxData/ns0:IBRType), 1) }</CLIEIBTP>
            <CLIEIBNU>{ local:adaptString(data($contact/ns0:TaxData/ns0:IBRNumber), 15) }</CLIEIBNU>
            <CLIEIITP>{ local:adaptString(data($contact/ns0:TaxData/ns0:IITType), 1) }</CLIEIITP>
            {
                let $TaxData := $contact/ns0:TaxData
                return
                    <TaxData>
                        {
                            let $GANTax := $TaxData/ns0:GANTax
                            return
                                <GANTax>
                                    <ExemptionPerc>{ local:adaptDecimal(data($GANTax/ns0:ExemptionPerc), 5) }</ExemptionPerc>
                                    <ExemptionDate>{ local:adaptDate($GANTax/ns0:ExemptionDate) }</ExemptionDate>
                                    <Reason>{ local:adaptString(data($GANTax/ns0:Reason), 1) }</Reason>
                                </GANTax>
                        }
                        {
                            let $IBRTaxes := $TaxData/ns0:IBRTaxes
                            return
                                <IBRTaxes>
                                    {
						        		let $items := $IBRTaxes/ns0:IBRTax
						        		return
							        		for $i in (1 to 24)
							        			let $IBRTax := $items[$i]
						        			return
                                            <IBRTax>
                                            	<Province>{ local:adaptNumber(data($IBRTax/ns0:Province), 2) }</Province>
                                                {
                                                    let $Perception := $IBRTax/ns0:Perception
                                                    return
                                                        <Perception>
                                                            <ExemptionPerc>{ local:adaptDecimal(data($Perception/ns0:ExemptionPerc), 5) }</ExemptionPerc>
                                                            <ExemptionDate>{ local:adaptDate($Perception/ns0:ExemptionDate) }</ExemptionDate>
                                                            <Reason>{ local:adaptString(data($Perception/ns0:Reason), 1) }</Reason>
                                                        </Perception>
                                                }
                                                {
                                                    let $Retention := $IBRTax/ns0:Retention
                                                    return
                                                        <Retention>
                                                            <ExemptionPerc>{ local:adaptDecimal(data($Retention/ns0:ExemptionPerc), 5) }</ExemptionPerc>
                                                            <ExemptionDate>{ local:adaptDate($Retention/ns0:ExemptionDate) }</ExemptionDate>
                                                            <Reason>{ local:adaptString(data($Retention/ns0:Reason), 1) }</Reason>
                                                        </Retention>
                                                }
                                                {
                                                    let $CM05Form := $IBRTax/ns0:CM05Form
                                                    return
                                                        <CM05Form>
                                                            <GIMPerc>{ local:adaptDecimal(data($CM05Form/ns0:GIMPerc), 5) }</GIMPerc>
                                                            <GIMDate>{ local:adaptDate($CM05Form/ns0:GIMDate) }</GIMDate>
                                                        </CM05Form>
                                                }
                                                <ContribType>{ local:adaptString(data($IBRTax/ns0:ContribType), 1) }</ContribType>
                                            </IBRTax>
                                    }
                                </IBRTaxes>
                        }
                        {
                            let $IITTax := $TaxData/ns0:IITTax
                            return
                                <IITTax>
                                    <ExemptionPerc>{ local:adaptDecimal(data($IITTax/ns0:ExemptionPerc), 5) }</ExemptionPerc>
                                    <ExemptionDate>{ local:adaptDate($IITTax/ns0:ExemptionDate) }</ExemptionDate>
                                    <Reason>{ local:adaptString(data($IITTax/ns0:Reason), 1) }</Reason>
                                </IITTax>
                        }
                        {
                            let $ISSTax := $TaxData/ns0:ISSTax
                            return
                                <ISSTax>
                                    <AliquotPerc>{ local:adaptDecimal(data($ISSTax/ns0:AliquotPerc), 5) }</AliquotPerc>
                                    <AliquotDate>{ local:adaptDate($ISSTax/ns0:AliquotDate) }</AliquotDate>
                                    <Reason>{ local:adaptString(data($ISSTax/ns0:Reason), 1) }</Reason>
                                </ISSTax>
                        }
                        {
                            let $IVATax := $TaxData/ns0:IVATax
                            return
                                <IVATax>
                                    {
                                        let $Perception := $IVATax/ns0:Perception
                                        return
                                            <Perception>
                                                <ExemptionPerc>{ local:adaptDecimal(data($Perception/ns0:ExemptionPerc), 5) }</ExemptionPerc>
                                                <ExemptionDate>{ local:adaptDate($Perception/ns0:ExemptionDate) }</ExemptionDate>
                                                <Reason>{ local:adaptString(data($Perception/ns0:Reason), 1) }</Reason>
                                            </Perception>
                                    }
                                    {
                                        let $Retention := $IVATax/ns0:Retention
                                        return
                                            <Retention>
                                                <ExemptionPerc>{ local:adaptDecimal(data($Retention/ns0:ExemptionPerc), 5) }</ExemptionPerc>
                                                <ExemptionDate>{ local:adaptDate($Retention/ns0:ExemptionDate) }</ExemptionDate>
                                                <Reason>{ local:adaptString(data($Retention/ns0:Reason), 1) }</Reason>
                                            </Retention>
                                    }
                                </IVATax>
                        }
                        {
                            let $SELTaxes := $TaxData/ns0:SELTaxes
                            return
                                <SELTaxes>
                                    {
						        		let $items := $SELTaxes/ns0:SELTax
						        		return
							        		for $i in (1 to 24)
							        			let $SELTax := $items[$i]
						        			return
	                                            <SELTax>
	                                                <Province>{ local:adaptNumber(data($SELTax/ns0:Province), 2) }</Province>
	                                                <ExemptionPerc>{ local:adaptDecimal(data($SELTax/ns0:ExemptionPerc), 5) }</ExemptionPerc>
	                                                <ExemptionDate>{ local:adaptDate($SELTax/ns0:ExemptionDate) }</ExemptionDate>
	                                                <Reason>{ local:adaptString(data($SELTax/ns0:Reason), 1) }</Reason>
	                                            </SELTax>
                                    }
                                </SELTaxes>
                        }
                        <ApplySS>{ local:adaptString(data($TaxData/ns0:ApplySS), 1) }</ApplySS>
                        <Activity>{ local:adaptString(data($TaxData/ns0:Activity), 6) }</Activity>
                    </TaxData>
            }
            {
                let $Addresses := $contact/ns0:Addresses
                return
                    <Addresses>
                        {
			        		let $items := $Addresses/ns0:Address
			        		return
				        		for $i in (1 to 132)
				        			let $Address := $items[$i]
			        			return
	                                <Address>
	                                    <DOMICCAL>{ local:adaptString(data($Address/ns0:AddressType), 5) }</DOMICCAL>
	                                    <DOMICDOM>{ local:adaptString(data($Address/ns0:ResidentialAddress/ns0:StreetName), 40) }</DOMICDOM>
	                                    <DOMICDNU>{ local:adaptString(data($Address/ns0:ResidentialAddress/ns0:StreetNo), 5) }</DOMICDNU>
	                                    <DOMICPIS>{ local:adaptString(data($Address/ns0:ResidentialAddress/ns0:Floor), 4) }</DOMICPIS>
	                                    <DOMICPTA>{ local:adaptString(data($Address/ns0:ResidentialAddress/ns0:Apartment), 4) }</DOMICPTA>
	                                    <DOMICPOB>{ local:adaptString(data($Address/ns0:City), 40) }</DOMICPOB>
	                                    <DOMICCPO>{ local:adaptNumber(data($Address/ns0:ZipCode), 5) }</DOMICCPO>
	                                    <PROVICOD>{ local:adaptNumber(data($Address/ns0:StateRegion), 2) }</PROVICOD>
	                                    <PAISSCOD_2>{ local:adaptString(data($Address/ns0:CountryCode), 2) }</PAISSCOD_2>
										<DOMICPRE>{ local:adaptString('', 5) }</DOMICPRE>
										<DOMICTLF>{ local:adaptString(data($contact/ns0:Contacts/ns0:Contact[ns0:Type='ADDR'][$i]/ns0:Details), 30) }</DOMICTLF>
	                                    <CPACODPO>{ local:adaptString(data($Address/ns0:TerritoryClass), 8) }</CPACODPO>
	                                    <PrimaryFlag>{ local:adaptString(data($Address/ns0:PrimaryFlag), 1) }</PrimaryFlag>
	                                </Address>
                        }
                    </Addresses>
            }
            {
                let $Contacts := $contact/ns0:Contacts
                return
                    <Contacts>
                        {
			        		let $items := $Contacts/ns0:Contact[ns0:Type != 'ADDR']
			        		return
				        		for $i in (1 to 75)
				        			let $Contact := $items[$i]
			        			return
	                                <Contact>
	                                    <MEDCOCOD>{ local:adaptString(data($Contact/ns0:Type), 3) }</MEDCOCOD>
	                                    <MEDCODAT>{ local:adaptString(data($Contact/ns0:Details), 50) }</MEDCODAT>
	                                </Contact>
                        }
                    </Contacts>
            }
            {
                let $PaymentInstruments := $contact/ns0:PaymentInstruments
                return
                    <PaymentInstruments>
                        {
			        		let $items := $PaymentInstruments/ns0:PaymentInstrument
			        		return
				        		for $i in (1 to 50)
				        			let $PaymentInstrument := $items[$i]
			        			return
	                                <PaymentInstrument>
	                                    <COBROTIP>{ local:adaptString(data($PaymentInstrument/ns0:AccountType), 2) }</COBROTIP>
	                                    <BANCOCOD>{ local:adaptNumber(data($PaymentInstrument/ns0:BankCode), 4) }</BANCOCOD>
	                                    <SUCURCOD>{ local:adaptNumber(data($PaymentInstrument/ns0:BranchCode), 4) }</SUCURCOD>
	                                    <CUENTNUM>{ local:adaptString(data($PaymentInstrument/ns0:AccountNo), 22) }</CUENTNUM>
	                                    <VENCIANN>{ local:adaptNumber( if ( data($PaymentInstrument/ns0:ValidTo) != '' ) then ( string(year-from-date($PaymentInstrument/ns0:ValidTo)) ) else ( '' ), 4) }</VENCIANN>
	                                    <VENCIMES>{ local:adaptNumber( if ( data($PaymentInstrument/ns0:ValidTo) != '' ) then ( string(month-from-date($PaymentInstrument/ns0:ValidTo)) ) else ( '' ), 2) }</VENCIMES>
							            <TITULAP1>{ local:adaptString(substring(data($PaymentInstrument/ns0:AccountHolderName), 1, 20), 20) }</TITULAP1>
							            <TITULAP2>{ local:adaptString(substring(data($PaymentInstrument/ns0:AccountHolderName), 21, 20), 20) }</TITULAP2>
					            		<TITULNOM>{ local:adaptString(substring(data($PaymentInstrument/ns0:AccountHolderName), 41, 20), 20) }</TITULNOM>
	                                </PaymentInstrument>
                        }
                    </PaymentInstruments>
            }
            <PROFECOD>{ local:adaptString(data($contact/ns0:Entity/ns0:IndustryCode), 6) }</PROFECOD>
            <AMLs>
	        	{
	        		let $items := $contact/ns0:Entity/ns0:Documents/ns0:Document[starts-with(ns0:DocumentID,'LAUNDRY_')]
	        		return
		        		for $i in (1 to 12)
		        			let $Document := $items[$i]
	        			return
			                <AML>
			                    <AML_DOC_TYPE>{ local:adaptNumber(data($Document/ns0:DocumentSerial), 3) }</AML_DOC_TYPE>
			                    <AML_EFFEC_FROM_DATE>{ local:adaptDate($Document/ns0:DocumentDate) }</AML_EFFEC_FROM_DATE>
			                    <AML_EXPIRATION_DATE>{ local:adaptDate($Document/ns0:DocumentValidTo) }</AML_EXPIRATION_DATE>
			                    <AML_DOC_LETTER_NUMBER>{ local:adaptString(data($Document/ns0:DocumentNo), 20) }</AML_DOC_LETTER_NUMBER>
			                </AML>
	        	}
            </AMLs>
        </PersonSync>
};

declare variable $contact as element(ns0:QbeRegisterNewPersonRq) external;

declare function local:adaptString($in as xs:string?, $needLength as xs:integer) as xs:string
{
	local:adapt($in, $needLength, ' ', false())
};

declare function local:adaptNumber($in as xs:string?, $needLength as xs:integer) as xs:string
{
	local:adapt($in, $needLength, '0', true())
};

declare function local:adaptDecimal($in as xs:double?, $needLength as xs:integer) as xs:string
{
	if(string(number($in)) != 'NaN') then (
		fn-bea:format-number($in, fn-bea:pad-left('.00', $needLength + 1, '0'))
	) else (
		fn-bea:pad-left('', $needLength, '0')
	)
};

declare function local:adaptDate($in as xs:string?) as xs:string
{
	if(not(exists($in)) or string-length($in) = 0) then (
		local:adaptNumber('', 8)
	) else (
		fn-bea:date-to-string-with-format('yyyyMMdd', xs:date($in))
	)
};

declare function local:adaptDateAnswer($in as xs:string?) as xs:string
{
	if(not(exists($in)) or string-length($in) = 0) then (
		local:adaptNumber('', 8)
	) else (
		fn-bea:date-to-string-with-format('yyyyMMdd', fn-bea:date-from-string-with-format("dd-MM-yyyy", $in))
	)
};

declare function local:adapt($in as xs:string?, $needLength as xs:integer, $padChar as xs:string, $padLeft as xs:boolean) as xs:string
{
	if (not(exists($in))) then (
		fn-bea:pad-right('', $needLength, $padChar)
	)
	else (
		let $inLength := string-length($in)
		return
			if ($inLength > $needLength) then (
				substring($in, 1, $needLength)
			) else (
				if ($inLength < $needLength) then (
					if ($padLeft) then (
						fn-bea:pad-left($in, $needLength, $padChar)
					) else (
						fn-bea:pad-right($in, $needLength, $padChar)
					)
				) else (
					$in
				)
			)
	)
};

xf:AIS_PersonSync_Register_Rq($contact)