(:: pragma bea:global-element-parameter parameter="$req" element="ns0:QbeFindPersonByPIDRq" location="../WSDL/AIS_PersonSync.wsdl" ::)
(:: pragma bea:mfl-element-return type="AIS_PersonSync_Find_Rq@" location="../MFL/AIS_PersonSync_Find_Rq.mfl" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/AIS_PersonSync/Resource/XQuery/AIS_PersonSync_Find_Rq/";

declare function xf:AIS_PersonSync_Find_Rq($req as element(ns0:QbeFindPersonByPIDRq))
    as element() {
        <AIS_PersonSync_Find_Rq>
            <DOCUMTIP>{ substring-before(data($req/ns0:PID),'-') }</DOCUMTIP>
            <DOCUMDAT>{ substring-after(data($req/ns0:PID),'-') }</DOCUMDAT>
            <CLIENTIP>{ data($req/ns0:EntityType) }</CLIENTIP>
        </AIS_PersonSync_Find_Rq>
};

declare variable $req as element(ns0:QbeFindPersonByPIDRq) external;

xf:AIS_PersonSync_Find_Rq($req)