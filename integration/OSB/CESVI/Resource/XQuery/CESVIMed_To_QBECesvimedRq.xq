(:: pragma bea:global-element-parameter parameter="$req" element="med:RegistrarRq" location="../XSD/CESVIMedWS.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$findClaimRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:FindClaimRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QBECesvimedRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace med = "http://xml.qbe.com/CESVI/CESVIMED";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/CESVI/Resource/XQuery/CESVIMed_To_QBECesvimedRq/";

declare function xf:CESVIMed_To_QBECesvimedRq($req as element(med:RegistrarRq), $findClaimRs as element())
    as element() {
        <ns0:QBECesvimedRq>
            <ns0:ClaimID>{ data($findClaimRs/ns0:Claim/ns0:ClaimID) }</ns0:ClaimID>
            <ns0:RequestNo>{ xs:integer(substring(data($req/med:Peritacion/med:Reclamo/med:NroSiniestro), 17, 1)) }</ns0:RequestNo>
            <ns0:ClaimObjSeq>{ xs:long(substring(data($req/med:Peritacion/med:Reclamo/med:NroSiniestro), 18, 9)) }</ns0:ClaimObjSeq>
            <ns0:ExpertPID>CESVIMED</ns0:ExpertPID>
            {
                let $Incapacidades := $req/med:Peritacion/med:Resultado/med:Incapacidades
                return
                    <ns0:Incapacity>
                        <ns0:EstimatedPhysical>{ data($Incapacidades/med:Estimada/med:Fisica) }</ns0:EstimatedPhysical>
                        <ns0:EstimatedPsychological>{ data($Incapacidades/med:Estimada/med:Psiquica) }</ns0:EstimatedPsychological>
                        <ns0:EstimatedAesthetic>{ data($Incapacidades/med:Estimada/med:Estetica) }</ns0:EstimatedAesthetic>
                        <ns0:FuturePhysical>{ data($Incapacidades/med:Futura/med:Fisica) }</ns0:FuturePhysical>
                        <ns0:FuturePsychological>{ data($Incapacidades/med:Futura/med:Psiquica) }</ns0:FuturePsychological>
                        <ns0:FutureAesthetic>{ data($Incapacidades/med:Futura/med:Estetica) }</ns0:FutureAesthetic>
                        <ns0:Total>{ data($Incapacidades/med:Total) }</ns0:Total>
                        <ns0:Agreed>{ data($Incapacidades/med:Consensuada) }</ns0:Agreed>
                        <ns0:LegalRisk>{ data($req/med:Peritacion/med:Resultado/med:RiesgoJudicial) }</ns0:LegalRisk>
                        <ns0:Lesion>{ data($req/med:Peritacion/med:Resultado/med:Lesiones/med:Lesion/med:Ubicacion) }</ns0:Lesion>
                    </ns0:Incapacity>
            }
        </ns0:QBECesvimedRq>
};

declare variable $req as element(med:RegistrarRq) external;
declare variable $findClaimRs as element() external;

xf:CESVIMed_To_QBECesvimedRq($req, $findClaimRs)
