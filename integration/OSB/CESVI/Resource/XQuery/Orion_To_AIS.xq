(:: pragma bea:global-element-parameter parameter="$req" element="ori:RegistrarRq" location="../WSDL/OrionWS.wsdl" ::)
(:: pragma bea:mfl-element-return type="Orion_AIS@" location="../MFL/Orion_AIS.mfl" ::)

declare namespace ori = "http://xml.qbe.com/CESVI/Orion";
declare namespace xf = "http://tempuri.org/CESVI/Resource/XQuery/Orion_To_AIS/";

declare function xf:Orion_To_AIS($req as element(ori:RegistrarRq))
    as element() {
        <Orion_AIS>
            <CIAASCOD>{ substring(data($req/ori:ResultadoPlus/ori:idSolicitudCia), 1, 4) }</CIAASCOD>
            <RAMOPCOD>{ substring(data($req/ori:ResultadoPlus/ori:idSolicitudCia), 5, 4) }</RAMOPCOD>
            <SINIEANN>{ xs:long(substring(data($req/ori:ResultadoPlus/ori:idSolicitudCia), 9, 2)) }</SINIEANN>
            <SINIENUM>{ xs:long(substring(data($req/ori:ResultadoPlus/ori:idSolicitudCia), 11, 6)) }</SINIENUM>
            <GESTICLA>{ xs:long(substring(data($req/ori:ResultadoPlus/ori:idSolicitudCia), 17, 1)) }</GESTICLA>
            <GESTISEC>{ xs:long(substring(data($req/ori:ResultadoPlus/ori:idSolicitudCia), 18, 9)) }</GESTISEC>
            <NROSINIE>{ data($req/ori:ResultadoPlus/ori:nroSiniestro) }</NROSINIE>
            <FECSINIE>{ fn-bea:dateTime-to-string-with-format("yyyy-MM-dd", $req/ori:ResultadoPlus/ori:fechaSiniestro) }</FECSINIE>
            <NUMPOLIZ>{ data($req/ori:ResultadoPlus/ori:nroPoliza) }</NUMPOLIZ>
            <Titular>
                <APELLIDO>{ data($req/ori:ResultadoPlus/ori:titular/ori:apellido) }</APELLIDO>
                <NOMBRE>{ data($req/ori:ResultadoPlus/ori:titular/ori:nombre) }</NOMBRE>
                <TIPODOC>{ xs:long(data($req/ori:ResultadoPlus/ori:titular/ori:idTipoDocumento)) }</TIPODOC>
                <NRODOCU>{ data($req/ori:ResultadoPlus/ori:titular/ori:nroDocumento) }</NRODOCU>
            </Titular>
            <Vehiculo>
                <MARCA>{ data($req/ori:ResultadoPlus/ori:vehiculo/ori:marca) }</MARCA>
                <MODELO>{ data($req/ori:ResultadoPlus/ori:vehiculo/ori:modelo) }</MODELO>
                <TIPO>{ data($req/ori:ResultadoPlus/ori:vehiculo/ori:tipo) }</TIPO>
                <FRABRI>{ xs:long(data($req/ori:ResultadoPlus/ori:vehiculo/ori:anioFabricacion)) }</FRABRI>
                <COLOR>{ data($req/ori:ResultadoPlus/ori:vehiculo/ori:color) }</COLOR>
                <TIPOPIN>{ data($req/ori:ResultadoPlus/ori:vehiculo/ori:tipoPintura) }</TIPOPIN>
                <NROMOTOR>{ data($req/ori:ResultadoPlus/ori:vehiculo/ori:nroMotor) }</NROMOTOR>
                <NROCARRO>{ data($req/ori:ResultadoPlus/ori:vehiculo/ori:nroCarroceria) }</NROCARRO>
                <PATENTE>{ data($req/ori:ResultadoPlus/ori:vehiculo/ori:patente) }</PATENTE>
                <KILOMET>{ xs:long(data($req/ori:ResultadoPlus/ori:vehiculo/ori:kilometraje)) }</KILOMET>
                <VALORVEN>{ xs:long(xs:decimal(data($req/ori:ResultadoPlus/ori:vehiculo/ori:valorVenal)) * 100) }</VALORVEN>
            </Vehiculo>
            {
                let $lstEquipamiento := $req/ori:ResultadoPlus/ori:vehiculo/ori:lstEquipamiento
                return
                    <Equipamientos>
                        {
                            for $i in 1 to 20
                            let $Equipamiento := $lstEquipamiento/ori:Equipamiento[$i]
                            return
                                <Equipamiento>
                                    <CODEQUI?>{ xs:long(data($Equipamiento/ori:codEquipamiento)) }</CODEQUI>
                                    <DESCRIP?>{ data($Equipamiento/ori:descripcion) }</DESCRIP>
                                </Equipamiento>
                        }
                    </Equipamientos>
            }
            {
                let $lstPeritaciones := $req/ori:ResultadoPlus/ori:lstPeritaciones
                return
                    <Peritaciones>
                        {
                            for $i in 1 to 10
                            let $Peritaciones := $lstPeritaciones/ori:Peritaciones[$i]
                            return
                                <Peritacion>
                                    <NROINFO?>{ xs:long(data($Peritaciones/ori:nroInforme)) }</NROINFO>
                                    <AMPLIAC?>{ data($Peritaciones/ori:ampliacion) }</AMPLIAC>
                                    <FECINFO?>{ fn-bea:dateTime-to-string-with-format("yyyy-MM-dd", $Peritaciones/ori:fechaInforme) }</FECINFO>
                                    <PERIFIN?>{ data($Peritaciones/ori:peritacionFinalizada) }</PERIFIN>
                                    <CENTINS?>{ data($Peritaciones/ori:centroInspeccion) }</CENTINS>
                                    {
                                        let $lugarPeritacion := $Peritaciones/ori:lugarPeritacion
                                        return
                                            <LugarPeritacion>
                                                <PROVINC?>{ data($lugarPeritacion/ori:provincia) }</PROVINC>
                                                <CIUDAD?>{ data($lugarPeritacion/ori:ciudad) }</CIUDAD>
                                                <DIRECCI?>{ data($lugarPeritacion/ori:direccion) }</DIRECCI>
                                            </LugarPeritacion>
                                    }
                                    <TIPOPER?>{ data($Peritaciones/ori:tipoPeritacion) }</TIPOPER>
                                    <DEPREMO?>{ xs:long(xs:decimal(data($Peritaciones/ori:depreciacionMO)) * 100) }</DEPREMO>
                                    <DEPREREP?>{ xs:long(xs:decimal(data($Peritaciones/ori:depreciacionRep)) * 100) }</DEPREREP>
                                    <TOTALMO?>{ xs:long(xs:decimal(data($Peritaciones/ori:totalMO)) * 100) }</TOTALMO>
                                    <TOTALREP?>{ xs:long(xs:decimal(data($Peritaciones/ori:totalRepuestos)) * 100) }</TOTALREP>
                                    <SUBTOTAL?>{ xs:long(xs:decimal(data($Peritaciones/ori:subtotal)) * 100) }</SUBTOTAL>
                                    <IMPUESTO?>{ xs:long(xs:decimal(data($Peritaciones/ori:impuestos)) * 100) }</IMPUESTO>
                                    <DEDUFRAN?>{ xs:long(xs:decimal(data($Peritaciones/ori:deduccionFranquicia)) * 100) }</DEDUFRAN>
                                    <TOTALPER?>{ xs:long(xs:decimal(data($Peritaciones/ori:totalPeritado)) * 100) }</TOTALPER>
                                    <AJUSTNEG?>{ xs:long(xs:decimal(data($Peritaciones/ori:ajusteNegociado)) * 100) }</AJUSTNEG>
                                    {
                                        let $usuario := $Peritaciones/ori:usuario
                                        return
                                            <Usuario>
                                                <APELLIDO?>{ data($usuario/ori:apellido) }</APELLIDO>
                                                <NOMBRE?>{ data($usuario/ori:nombre) }</NOMBRE>
                                                <DOCUMEN?>{ xs:long(data($usuario/ori:nroDocumento)) }</DOCUMEN>
                                            </Usuario>
                                    }
                                </Peritacion>
                        }
                    </Peritaciones>
            }
            <ESTADO />
            <ERRCOD />
        </Orion_AIS>
};

declare variable $req as element(ori:RegistrarRq) external;

xf:Orion_To_AIS($req)
