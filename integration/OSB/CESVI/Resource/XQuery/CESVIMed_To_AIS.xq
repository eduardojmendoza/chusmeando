(:: pragma bea:global-element-parameter parameter="$req" element="med:RegistrarRq" location="../XSD/CESVIMedWS.xsd" ::)
(:: pragma bea:mfl-element-return type="CESVIMed_AIS@" location="../MFL/CESVIMed_AIS.mfl" ::)

declare namespace med = "http://xml.qbe.com/CESVI/CESVIMED";
declare namespace xf = "http://tempuri.org/CESVI/Resource/XQuery/CESVIMed_To_AIS/";

declare function xf:CESVIMed_To_AIS($req as element(med:RegistrarRq))
    as element() {
        <CESVIMed_AIS>
            <CIAASCOD>{ substring(data($req/med:Peritacion/med:Reclamo/med:NroSiniestro), 1, 4) }</CIAASCOD>
            <RAMOPCOD>{ substring(data($req/med:Peritacion/med:Reclamo/med:NroSiniestro), 5, 4) }</RAMOPCOD>
            <SINIEANN>{ xs:long(substring(data($req/med:Peritacion/med:Reclamo/med:NroSiniestro), 9, 2)) }</SINIEANN>
            <SINIENUM>{ xs:long(substring(data($req/med:Peritacion/med:Reclamo/med:NroSiniestro), 11, 6)) }</SINIENUM>
            <BIENASEC>{ xs:long(substring(data($req/med:Peritacion/med:Reclamo/med:NroSiniestro), 17, 3)) }</BIENASEC>
            <Incapacidad>
                <Estimada>
                    <InFisiEst>{ xs:long(xs:decimal(data($req/med:Peritacion/med:Resultado/med:Incapacidades/med:Estimada/med:Fisica)) * 100) }</InFisiEst>
                    <InPsiqEst>{ xs:long(xs:decimal(data($req/med:Peritacion/med:Resultado/med:Incapacidades/med:Estimada/med:Psiquica)) * 100) }</InPsiqEst>
                    <InEsteEst>{ xs:long(xs:decimal(data($req/med:Peritacion/med:Resultado/med:Incapacidades/med:Estimada/med:Estetica)) * 100) }</InEsteEst>
                </Estimada>
                <Futura>
                    <InFisiFut>{ xs:long(xs:decimal(data($req/med:Peritacion/med:Resultado/med:Incapacidades/med:Futura/med:Fisica)) * 100) }</InFisiFut>
                    <InPsiqFut>{ xs:long(xs:decimal(data($req/med:Peritacion/med:Resultado/med:Incapacidades/med:Futura/med:Psiquica)) * 100) }</InPsiqFut>
                    <InEsteFut>{ xs:long(xs:decimal(data($req/med:Peritacion/med:Resultado/med:Incapacidades/med:Futura/med:Estetica)) * 100) }</InEsteFut>
                </Futura>
                <InTotal>{ xs:long(xs:decimal(data($req/med:Peritacion/med:Resultado/med:Incapacidades/med:Total)) * 100) }</InTotal>
                <InConsen>{ xs:long(xs:decimal(data($req/med:Peritacion/med:Resultado/med:Incapacidades/med:Consensuada)) * 100) }</InConsen>
                <RiesgoJu>{ xs:long(xs:decimal(data($req/med:Peritacion/med:Resultado/med:RiesgoJudicial)) * 100) }</RiesgoJu>
                <Lesion>{ data($req/med:Peritacion/med:Resultado/med:Lesiones/med:Lesion/med:Ubicacion) }</Lesion>
            </Incapacidad>
        </CESVIMed_AIS>
};

declare variable $req as element(med:RegistrarRq) external;

xf:CESVIMed_To_AIS($req)
