(:: pragma bea:global-element-parameter parameter="$req" element="ori:RegistrarRq" location="../WSDL/OrionWS.wsdl" ::)
(:: pragma bea:local-element-parameter parameter="$findClaimRs" type="ns1:InsuranceSvc/ns1:InsuranceSvcRs/ns1:FindClaimRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:local-element-return type="ns1:InsuranceSvc/ns1:InsuranceSvcRq/ns1:RegisterClaimEvaluationRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns1 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace ori = "http://xml.qbe.com/CESVI/Orion";
declare namespace xf = "http://tempuri.org/CESVI/Resource/XQuery/Orion_To_RegisterClaimEvaluationRq/";

declare function xf:Orion_To_RegisterClaimEvaluationRq($req as element(ori:RegistrarRq), $findClaimRs as element())
    as element() {
        <ns1:RegisterClaimEvaluationRq>
            <ns1:ClaimObjectSeq>{ xs:long(substring(data($req/ori:ResultadoPlus/ori:idSolicitudCia), 19, 10)) }</ns1:ClaimObjectSeq>
            <ns1:ClaimID>{ data($findClaimRs/ns1:Claim/ns1:ClaimID) }</ns1:ClaimID>
            <ns1:RequestNo>{ xs:integer(substring(data($req/ori:ResultadoPlus/ori:idSolicitudCia), 17, 2)) }</ns1:RequestNo>
            <ns1:PID>ORION</ns1:PID>
            <ns1:EntityType>2</ns1:EntityType> (: Legal :)
            <ns1:InspectionDate>{ fn-bea:date-from-dateTime($req/ori:ResultadoPlus/ori:lstPeritaciones/ori:Peritaciones[1]/ori:fechaInforme) }</ns1:InspectionDate>
            <ns1:ExpertiseAmount>{ data(sum($req/ori:ResultadoPlus/ori:lstPeritaciones/ori:Peritaciones/ori:totalPeritado)) }</ns1:ExpertiseAmount>
            {
        		let $totalMO  := data(sum($req/ori:ResultadoPlus/ori:lstPeritaciones/ori:Peritaciones/ori:totalMO))
        		let $totalRep := data(sum($req/ori:ResultadoPlus/ori:lstPeritaciones/ori:Peritaciones/ori:totalRepuestos))
        		return
		            <ns1:ClaimDamageList>
		                <ns1:Damage>
		                    <ns1:DamageID>LB</ns1:DamageID>
		                    <ns1:RequestedAmnt>{ $totalMO }</ns1:RequestedAmnt>
		                    <ns1:Price>{ $totalMO }</ns1:Price>
		                    <ns1:ConfirmedAmnt>{ $totalMO }</ns1:ConfirmedAmnt>
		                </ns1:Damage>
		                <ns1:Damage>
		                    <ns1:DamageID>SP</ns1:DamageID>
		                    <ns1:RequestedAmnt>{ $totalRep }</ns1:RequestedAmnt>
		                    <ns1:Price>{ $totalRep }</ns1:Price>
		                    <ns1:ConfirmedAmnt>{ $totalRep }</ns1:ConfirmedAmnt>
		                </ns1:Damage>
		            </ns1:ClaimDamageList>
            }
        </ns1:RegisterClaimEvaluationRq>
};

declare variable $req as element(ori:RegistrarRq) external;
declare variable $findClaimRs as element() external;

xf:Orion_To_RegisterClaimEvaluationRq($req, $findClaimRs)
