(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QBECesvimedRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$req" element="med:RegistrarRq" location="../XSD/CESVIMedWS.xsd" ::)
(:: pragma bea:global-element-return element="med:RegistrarRs" location="../XSD/CESVIMedWS.xsd" ::)

declare namespace med = "http://xml.qbe.com/CESVI/CESVIMED";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/CESVI/Resource/XQuery/CESVIMed_From_QBECesvimedRs/";

declare function xf:CESVIMed_From_QBECesvimedRs($resp as element(), $req as element(med:RegistrarRq))
    as element(med:RegistrarRs) {
        <med:RegistrarRs>
            <med:Respuesta>
                <med:IdEmpresa>{ data($req/med:Peritacion/med:Datos/med:IdEmpresa) }</med:IdEmpresa>
                <med:NroPedidoInspeccion>{ data($resp/ns0:InspectionNo) }</med:NroPedidoInspeccion>
            </med:Respuesta>
        </med:RegistrarRs>
};

declare variable $resp as element() external;
declare variable $req as element(med:RegistrarRq) external;

xf:CESVIMed_From_QBECesvimedRs($resp, $req)
