(:: pragma bea:global-element-parameter parameter="$req" element="ns0:QbeRetrieveAgentInfoRq" location="../WSDL/AIS_AgentSync.wsdl" ::)
(:: pragma bea:mfl-element-return type="AIS_AgentSync_Find_Rq@" location="../MFL/AIS_AgentSync_Find_Rq.mfl" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/AIS_AgentSync/Resource/XQuery/AIS_AgentSync_Find_Rq/";

declare function xf:AIS_AgentSync_Find_Rq($req as element(ns0:QbeRetrieveAgentInfoRq))
    as element() {
        <AIS_AgentSync_Find_Rq>
            <AGENTCLA>{ substring(data($req/ns0:AgentNo), 1, 2) }</AGENTCLA>
            <AGENTCOD>{ substring(data($req/ns0:AgentNo), 3) }</AGENTCOD>
        </AIS_AgentSync_Find_Rq>
};

declare variable $req as element(ns0:QbeRetrieveAgentInfoRq) external;

xf:AIS_AgentSync_Find_Rq($req)
