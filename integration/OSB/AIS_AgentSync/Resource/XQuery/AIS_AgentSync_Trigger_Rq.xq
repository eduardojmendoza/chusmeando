(:: pragma bea:mfl-element-parameter parameter="$req" type="AIS_AgentSync_Trigger_Rq@" location="../MFL/AIS_AgentSync_Trigger_Rq.mfl" ::)
(:: pragma bea:global-element-return element="ns0:personUpdated" location="../../../INSIS-OV/Resource/WSDL/SynchPerson.wsdl" ::)

declare namespace ns1 = "http://fadata.eu/integration/qbe/SynchPersonService/v1.0/types";
declare namespace ns0 = "http://fadata.eu/integration/qbe/SynchPersonService/v1.0";
declare namespace xf = "http://tempuri.org/AIS_AgentSync/Resource/XQuery/AIS_AgentSync_Trigger_Rq/";

declare function xf:AIS_AgentSync_Trigger_Rq($req as element())
    as element(ns0:personUpdated) {
        <ns0:personUpdated>
            <data>
                <pid>{ concat(xs:long(substring(data($req/PID), 1, 2)), '-', substring(data($req/PID), 3)) }</pid>
                <entityType>
                	{
                		let $tipo := data($req/ENTITY-TYPE)
                		return
                			if ($tipo = '' or $tipo = '00') then ( 1 )
                			else if ($tipo = '15') then ( 2 )
                			else ( 0 )
                	}
                </entityType>
                <agentNo?>{ data($req/AGENTNO) }</agentNo>
                <role>{ data($req/ROLE) }</role>
                <sourceSystem>{ data($req/SOURCE-SYSTEM) }</sourceSystem>
            </data>
        </ns0:personUpdated>
};

declare variable $req as element() external;

xf:AIS_AgentSync_Trigger_Rq($req)
