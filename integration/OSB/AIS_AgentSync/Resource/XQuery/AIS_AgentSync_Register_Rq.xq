xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$agent" element="ns0:QbeRegisterNewAgentRq" location="../WSDL/AIS_AgentSync.wsdl" ::)
(:: pragma bea:mfl-element-return type="AgentSync@" location="../MFL/AIS_AgentSync.mfl" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/AIS_AgentSync/Resource/XQuery/AIS_AgentSync_Register/";

declare function xf:AIS_AgentSync_Register_Rq($agent as element(ns0:QbeRegisterNewAgentRq))
    as element() {
        <AgentSync>
            {
                let $AgentData := $agent/ns0:AgentData
                return
                    <AgentData>
                        <CLIENSEC>{ local:adaptNumber(data($AgentData/ns0:ManId), 9) }</CLIENSEC>
                        <AGENTCLA>{ local:adaptString(data($AgentData/ns0:AgentType), 2) }</AGENTCLA>
                        <AGENTCOD>{ local:adaptNumber(data($AgentData/ns0:AgentNo), 4) }</AGENTCOD>
                        <NIVELCLA>{ local:adaptString(data($AgentData/ns0:QualificationLevel), 2) }</NIVELCLA>
                        <VIADECO4>{ local:adaptNumber(data($AgentData/ns0:DispatchMode), 4) }</VIADECO4>
                        <CODDESPA>{ local:adaptString(data($AgentData/ns0:DispatchCode), 1) }</CODDESPA>
                        <EFECT>{ local:adaptNumber(fn-bea:date-to-string-with-format('yyyyMMdd', $AgentData/ns0:HireDate), 8) }</EFECT>
                        <VENCI>{ local:adaptNumber(fn-bea:date-to-string-with-format('yyyyMMdd', $AgentData/ns0:FireDate), 8) }</VENCI>
                        <SITUCMOT>{ local:adaptNumber(data($AgentData/ns0:CancelReason), 3) }</SITUCMOT>
                        <ELEMTIPO>{ local:adaptString(data($AgentData/ns0:CommercialPersonType), 1) }</ELEMTIPO>
                        <CANALSIT>{ local:adaptString(data($AgentData/ns0:HasCommission), 1) }</CANALSIT>
                    </AgentData>
            }
            {
        		let $items := $agent/ns0:AgentData/ns0:CommissionAgreement
        		return
	        		for $i in (1 to 10)
	        			let $CommissionAgreement := $items[$i]
        			return
	                    <CommissionAgreement>
	                        <CONVECOD>{ local:adaptString(data($CommissionAgreement/ns0:AgreementCode), 2) }</CONVECOD>
	                        <SITUCEST>{ local:adaptString(data($CommissionAgreement/ns0:AgreementStatus), 1) }</SITUCEST>
	                        <SITUC>{ local:adaptNumber(fn-bea:date-to-string-with-format('yyyyMMdd', $CommissionAgreement/ns0:ValidFrom), 8) }</SITUC>
	                        <CONBATIP>{ local:adaptNumber(data($CommissionAgreement/ns0:EnforcementKind), 4) }</CONBATIP>
	                        <CONBAMET>{ local:adaptNumber(data($CommissionAgreement/ns0:EnforcementMode), 2) }</CONBAMET>
	                    </CommissionAgreement>
            }
            {
                let $Address := $agent/ns0:AgentData/ns0:Addresses/ns0:Address[ns0:AddressType='DOMAGENT'][1],
                	$Contact := $agent/ns0:AgentData/ns0:Contacts/ns0:Contact[ns0:Type='CONAGENT'][1]
                return
                    <AgentAddress>
                        <DOMICCAL>{ local:adaptString(data($Address/ns0:AddressType), 5) }</DOMICCAL>
                        <DOMICDOM>{ local:adaptString(data($Address/ns0:ResidentialAddress/ns0:StreetName), 40) }</DOMICDOM>
                        <DOMICDNU>{ local:adaptString(data($Address/ns0:ResidentialAddress/ns0:StreetNo), 5) }</DOMICDNU>
                        <DOMICPIS>{ local:adaptString(data($Address/ns0:ResidentialAddress/ns0:Floor), 4) }</DOMICPIS>
                        <DOMICPTA>{ local:adaptString(data($Address/ns0:ResidentialAddress/ns0:Apartment), 4) }</DOMICPTA>
                        <DOMICPOB>{ local:adaptString(data($Address/ns0:City), 40) }</DOMICPOB>
                        <DOMICCPO>{ local:adaptNumber(data($Address/ns0:ZipCode), 5) }</DOMICCPO>
                        <PROVICOD>{ local:adaptNumber(data($Address/ns0:StateRegion), 2) }</PROVICOD>
                        <PAISSCOD>{ local:adaptString(data($Address/ns0:CountryCode), 2) }</PAISSCOD>
						<DOMICPRE>{ local:adaptString(substring(data($Contact/ns0:Details), 1, 5), 5) }</DOMICPRE>
						<DOMICTLF>{ local:adaptString(substring(data($Contact/ns0:Details), 6, 30), 30) }</DOMICTLF>
                        <CPACODPO>{ local:adaptString(data($Address/ns0:TerritoryClass), 8) }</CPACODPO>
                    </AgentAddress>
            }
            {
                let $LiquidationData := $agent/ns0:AgentData/ns0:LiquidationData[1],
                    $PaymentInstrument := $agent/ns0:AgentData/ns0:PaymentInstruments/ns0:PaymentInstrument[1]
                return
                    <LiquidationData>
                        <AGENTSSN>{ local:adaptString(data($LiquidationData/ns0:SSNNumber), 10) }</AGENTSSN>
                        <PERIOCAL>{ local:adaptNumber(data($LiquidationData/ns0:SettlementFrequency), 3) }</PERIOCAL>
                        <MONENCOD>{ local:adaptNumber(data($PaymentInstrument/ns0:Currency), 4) }</MONENCOD>
                        <CENTRCOD>{ local:adaptString(data($LiquidationData/ns0:OfficeNo), 4) }</CENTRCOD>
                        <CAMPOPAG>{ local:adaptNumber(data($LiquidationData/ns0:PaymentType), 2) }</CAMPOPAG>
                        <COBROTIP>{ local:adaptString(data($PaymentInstrument/ns0:AccountType), 2) }</COBROTIP>
                        <BANCOCOD>{ local:adaptNumber(data($PaymentInstrument/ns0:Bankcode), 4) }</BANCOCOD>
                        <SUCURCOD>{ local:adaptNumber(data($PaymentInstrument/ns0:BranchCode), 4) }</SUCURCOD>
                        <CUENTNUM>{ local:adaptString(data($PaymentInstrument/ns0:AccountNo), 22) }</CUENTNUM>
                        <DESTCHEQ>{ local:adaptString(data($LiquidationData/ns0:ChequeDestination), 4) }</DESTCHEQ>
                        <SWLIQCOM>{ local:adaptString(data($LiquidationData/ns0:AutomaticPaymentOrders), 1) }</SWLIQCOM>
                        <IMPRICTA>{ local:adaptString(data($LiquidationData/ns0:PrintCurrentAccount), 1) }</IMPRICTA>
                        <MONIMMIN>{ local:adaptNumber(data($LiquidationData/ns0:MinimumAccountAmount), 9) }</MONIMMIN>
                        <CALIFPEC>{ local:adaptString(data($LiquidationData/ns0:Qualification), 2) }</CALIFPEC>
                    </LiquidationData>
            }
            {
        		let $items := $agent/ns0:AgentData/ns0:HigherCommercialStructure[ns0:Type != 'GO']
        		return
	        		for $i in (1 to 10)
	        			let $HigherCommercialStructure := $items[$i]
        			return
			            <HigherCommercialStructure>
			                <Type>{ local:adaptString(data($HigherCommercialStructure/ns0:Type), 2) }</Type>
			                <Code>{ local:adaptNumber(data($HigherCommercialStructure/ns0:Code), 4) }</Code>
			                <EFECT>{ local:adaptNumber(fn-bea:date-to-string-with-format('yyyyMMdd', $HigherCommercialStructure/ns0:ValidFrom), 8) }</EFECT>
			                <VENCI>{ local:adaptNumber(fn-bea:date-to-string-with-format('yyyyMMdd', $HigherCommercialStructure/ns0:ValidTo), 8) }</VENCI>
			            </HigherCommercialStructure>
            }
            {
        		let $items := $agent/ns0:AgentData/ns0:HorizontalCommercialStructure
        		return
	        		for $i in (1 to 5)
	        			let $HorizontalCommercialStructure := $items[$i]
        			return
			            <HorizontalCommercialStructure>
			                <Type>{ local:adaptString(data($HorizontalCommercialStructure/ns0:Type), 2) }</Type>
			                <Code>{ local:adaptNumber(data($HorizontalCommercialStructure/ns0:Code), 4) }</Code>
			                <EFECT>{ local:adaptNumber(fn-bea:date-to-string-with-format('yyyyMMdd', $HorizontalCommercialStructure/ns0:ValidFrom), 8) }</EFECT>
			                <VENCI>{ local:adaptNumber(fn-bea:date-to-string-with-format('yyyyMMdd', $HorizontalCommercialStructure/ns0:ValidTo), 8) }</VENCI>
			            </HorizontalCommercialStructure>
            }
            {
        		let $HigherCommercialStructure := $agent/ns0:AgentData/ns0:HigherCommercialStructure[ns0:Type = 'GO'][1]
    			return
		            <GroupComercialStructure>
	                    <Type>{ local:adaptString(data($HigherCommercialStructure/ns0:Type), 2) }</Type>
	                    <Code>{ local:adaptNumber(data($HigherCommercialStructure/ns0:Code), 4) }</Code>
	                    <EFECT>{ local:adaptNumber(fn-bea:date-to-string-with-format('yyyyMMdd', $HigherCommercialStructure/ns0:ValidFrom), 8) }</EFECT>
	                    <VENCI>{ local:adaptNumber(fn-bea:date-to-string-with-format('yyyyMMdd', $HigherCommercialStructure/ns0:ValidTo), 8) }</VENCI>
		            </GroupComercialStructure>
            }
            {
        		let $items := $agent/ns0:AgentData/ns0:BusinessAccountExecutive
        		return
	        		for $i in (1 to 10)
	        			let $BusinessAccountExecutive := $items[$i]
        			return
	                    <BusinessAccountExecutive>
	                        <BAEType>{ local:adaptString(data($BusinessAccountExecutive/ns0:BAEType), 2) }</BAEType>
	                        <BAECode>{ local:adaptNumber(data($BusinessAccountExecutive/ns0:BAECode), 4) }</BAECode>
	                        <MUType>{ local:adaptString(data($BusinessAccountExecutive/ns0:MUType), 2) }</MUType>
	                        <MUCode>{ local:adaptNumber(data($BusinessAccountExecutive/ns0:MUCode), 4) }</MUCode>
	                        <ProductCode>{ local:adaptString(data($BusinessAccountExecutive/ns0:ProductCode), 4) }</ProductCode>
	                    </BusinessAccountExecutive>
            }
        </AgentSync>
};

declare variable $agent as element(ns0:QbeRegisterNewAgentRq) external;

declare function local:adaptString($in as xs:string?, $needLength as xs:integer) as xs:string
{
	local:adapt($in, $needLength, ' ', false())
};

declare function local:adaptNumber($in as xs:string?, $needLength as xs:integer) as xs:string
{
	local:adapt($in, $needLength, '0', true())
};

declare function local:adaptDecimal($in as xs:double?, $needLength as xs:integer) as xs:string
{
	if(string(number($in)) != 'NaN') then (
		fn-bea:format-number($in, fn-bea:pad-left('.00', $needLength + 1, '0'))
	) else (
		fn-bea:pad-left('', $needLength, '0')
	)
};

declare function local:adapt($in as xs:string?, $needLength as xs:integer, $padChar as xs:string, $padLeft as xs:boolean) as xs:string
{
	if (not(exists($in))) then (
		fn-bea:pad-right('', $needLength, $padChar)
	)
	else (
		let $inLength := string-length($in)
		return
			if ($inLength > $needLength) then (
				substring($in, 1, $needLength)
			) else (
				if ($inLength < $needLength) then (
					if ($padLeft) then (
						fn-bea:pad-left($in, $needLength, $padChar)
					) else (
						fn-bea:pad-right($in, $needLength, $padChar)
					)
				) else (
					$in
				)
			)
	)
};

xf:AIS_AgentSync_Register_Rq($agent)
