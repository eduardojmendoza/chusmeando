xquery version "1.0" encoding "UTF-8";
(:: pragma bea:mfl-element-parameter parameter="$agent" type="AgentSync@" location="../MFL/AIS_AgentSync.mfl" ::)
(:: pragma bea:global-element-return element="ns0:QbeRetrieveAgentInfoRs" location="../WSDL/AIS_AgentSync.wsdl" ::)

declare namespace xf = "http://tempuri.org/AIS_AgentSync/Resource/XQuery/AIS_AgentSync_Find/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:AIS_AgentSync_Find_Rs($agent as element())
    as element(ns0:QbeRetrieveAgentInfoRs) {
        <ns0:QbeRetrieveAgentInfoRs>
            {
                let $AgentData := $agent/AgentData
                return (
            		<ns0:PID>{ concat(xs:long(data($AgentData/DOCUMTIP)), '-', data($AgentData/DOCUMDAT)) }</ns0:PID>,
            		<ns0:EntityType>{ data($AgentData/AGENTTIP) }</ns0:EntityType>,
                    <ns0:AgentData>
                        <ns0:AgentNo>{ concat(data($AgentData/AGENTCLA),data($AgentData/AGENTCOD)) }</ns0:AgentNo>
                        <ns0:ManId?>{ local:nullIfEmpty(data($AgentData/CLIENSEC)) }</ns0:ManId>
                        <ns0:AgentType>{ data($AgentData/AGENTTIP) }</ns0:AgentType>
                        <ns0:QualificationLevel>{ data($AgentData/NIVELCLA) }</ns0:QualificationLevel>
                        <ns0:HireDate>{ local:getDate(data($AgentData/EFECT)) }</ns0:HireDate>
                        <ns0:FireDate?>{ local:getDate(data($AgentData/VENCI)) }</ns0:FireDate>
                        <!-- <ns0:OfficeNo>5010000142</ns0:OfficeNo> -->
                         <ns0:OfficeNo>ARG01</ns0:OfficeNo>
                        <ns0:DefaultAgent>0</ns0:DefaultAgent>
                        <ns0:AgentState>{ data($AgentData/AgentState) }</ns0:AgentState>
                        <ns0:AgentClass?>{ local:nullIfEmpty(data($AgentData/AGENTCLA)) }</ns0:AgentClass>
                        <ns0:DispatchMode>{ data($AgentData/VIADECO4) }</ns0:DispatchMode>
                        <ns0:DispatchCode>{ data($AgentData/CODDESPA) }</ns0:DispatchCode>
                        <ns0:CancelReason?>{ local:zeroToEmpty(data($AgentData/SITUCMOT)) }</ns0:CancelReason>
                        <ns0:CommercialPersonType>{ data($AgentData/ELEMTIPO) }</ns0:CommercialPersonType>
                        <ns0:HasCommission>{ data($AgentData/CANALSIT) }</ns0:HasCommission>
                        <ns0:Addresses?>
                            {
                                let $AgentAddress := $agent/AgentAddress
                                let $AddressType := data($AgentAddress/DOMICCAL)
                                return
									if (fn-bea:trim($AddressType) != '') then (
	                                    <ns0:Address?>
	                                        <ns0:CountryCode?>{ local:nullIfEmpty(data($AgentAddress/PAISSCOD)) }</ns0:CountryCode>
	                                        <ns0:StateRegion?>{ local:nullIfEmpty(data($AgentAddress/PROVICOD)) }</ns0:StateRegion>
	                                        <ns0:City?>{ local:nullIfEmpty(data($AgentAddress/DOMICPOB)) }</ns0:City>
	                                        <ns0:ResidentialAddress?>
	                                            <ns0:StreetName?>{ local:nullIfEmpty(data($AgentAddress/DOMICDOM)) }</ns0:StreetName>
	                                            <ns0:StreetNo?>{ local:nullIfEmpty(data($AgentAddress/DOMICDNU)) }</ns0:StreetNo>
	                                            <ns0:Floor?>{ local:nullIfEmpty(data($AgentAddress/DOMICPIS)) }</ns0:Floor>
	                                            <ns0:Apartment?>{ local:nullIfEmpty(data($AgentAddress/DOMICPTA)) }</ns0:Apartment>
	                                        </ns0:ResidentialAddress>
	                                        <ns0:ZipCode?>{ local:nullIfEmpty(data($AgentAddress/DOMICCPO)) }</ns0:ZipCode>
	                                        <ns0:AddressType>{ $AddressType }</ns0:AddressType>
	                                        <ns0:TerritoryClass?>{ local:nullIfEmpty(data($AgentAddress/CPACODPO)) }</ns0:TerritoryClass>
	                                    </ns0:Address>
                                    ) else ()
                            }
                        </ns0:Addresses>
						{
							let $prefijo := data($agent/AgentAddress/DOMICPRE)
							let $telefono := data($agent/AgentAddress/DOMICTLF)
							return
								if (fn-bea:trim($prefijo) != '' or fn-bea:trim($telefono) != '') then (
			                        <ns0:Contacts>
			                        	<ns0:Contact>
			                        		<ns0:Type>CONAGENT</ns0:Type>
			                        		<ns0:Details>{ concat($prefijo, $telefono) }</ns0:Details>
			                        	</ns0:Contact>
			                        </ns0:Contacts>
								) else ()
						}
						{
							let $AccountType := data($agent/LiquidationData/COBROTIP)
		                    return
		            			if (fn-bea:trim($AccountType) != '') then (
			                        <ns0:PaymentInstruments?>
			                            <ns0:PaymentInstrument?>
			                                <ns0:AccountType>{ $AccountType }</ns0:AccountType>
			                                <ns0:AccountNo?>{ local:nullIfEmpty(data($agent/LiquidationData/CUENTNUM)) }</ns0:AccountNo>
			                                <ns0:BankCode?>{ local:zeroToEmpty(data($agent/LiquidationData/BANCOCOD)) }</ns0:BankCode>
			                                <ns0:BranchCode?>{ local:zeroToEmpty(data($agent/LiquidationData/SUCURCOD)) }</ns0:BranchCode>
			                                <ns0:Currency?>{ local:nullIfEmpty(data($agent/LiquidationData/MONENCOD)) }</ns0:Currency>
			                            </ns0:PaymentInstrument>
			                        </ns0:PaymentInstruments>
		            			) else ()
		            	}
                        {
                            for $CommissionAgreement in $agent/CommissionAgreements/CommissionAgreement
                            let $AgreementCode := data($CommissionAgreement/CONVECOD)
                            return
								if (fn-bea:trim($AgreementCode) != '') then (
	                                <ns0:CommissionAgreement?>
	                                    <ns0:AgreementCode>{ $AgreementCode }</ns0:AgreementCode>
	                                    <ns0:AgreementStatus>{ data($CommissionAgreement/SITUCEST) }</ns0:AgreementStatus>
	                                    <ns0:ValidFrom>{ local:getDate(data($CommissionAgreement/SITUC)) }</ns0:ValidFrom>
	                                    <ns0:EnforcementKind?>{ local:zeroToEmpty(data($CommissionAgreement/CONBATIP)) }</ns0:EnforcementKind>
	                                    <ns0:EnforcementMode?>{ local:zeroToEmpty(data($CommissionAgreement/CONBAMET)) }</ns0:EnforcementMode>
	                                </ns0:CommissionAgreement>
	                            ) else ()
                        }
                        {
                            let $LiquidationData := $agent/LiquidationData
                            return
                                <ns0:LiquidationData?>
                                    <ns0:SSNNumber?>{ local:nullIfEmpty(data($LiquidationData/AGENTSSN)) }</ns0:SSNNumber>
                                    <ns0:SettlementFrequency?>{ local:zeroToEmpty(data($LiquidationData/PERIOCAL)) }</ns0:SettlementFrequency>
                                    <ns0:OfficeNo?>{ local:nullIfEmpty(data($LiquidationData/CENTRCOD)) }</ns0:OfficeNo>
                                    <ns0:PaymentType?>{ local:nullIfEmpty(data($LiquidationData/CAMPOPAG)) }</ns0:PaymentType>
                                    <ns0:ChequeDestination?>{ local:nullIfEmpty(data($LiquidationData/DESTCHEQ)) }</ns0:ChequeDestination>
                                    <ns0:AutomaticPaymentOrders?>{ local:nullIfEmpty(data($LiquidationData/SWLIQCOM)) }</ns0:AutomaticPaymentOrders>
                                    <ns0:PrintCurrentAccount?>{ local:nullIfEmpty(data($LiquidationData/IMPRICTA)) }</ns0:PrintCurrentAccount>
                                    <ns0:MinimumAccountAmount?>{ local:zeroToEmpty(data($LiquidationData/MONIMMIN)) }</ns0:MinimumAccountAmount>
                                    <ns0:Qualification?>{ local:nullIfEmpty(data($LiquidationData/CALIFPEC)) }</ns0:Qualification>
                                </ns0:LiquidationData>
                        }
                        {
                            for $HigherCommercialStructure in $agent/HigherCommercialStructures/HigherCommercialStructure
                            let $Type := data($HigherCommercialStructure/Type)
                            return
								if (fn-bea:trim($Type) != '') then (
	                                <ns0:HigherCommercialStructure?>
	                                    <ns0:Type>{ $Type }</ns0:Type>
	                                    <ns0:Code>{ data($HigherCommercialStructure/Code) }</ns0:Code>
	                                    <ns0:ValidFrom>{ local:getDate(data($HigherCommercialStructure/EFECT)) }</ns0:ValidFrom>
	                                    <ns0:ValidTo>{ local:getDate(data($HigherCommercialStructure/VENCI)) }</ns0:ValidTo>
	                                </ns0:HigherCommercialStructure>
                                ) else ()
                        }
                        {
                            for $HorizontalCommercialStructure in $agent/HorizontalCommercialStructures/HorizontalCommercialStructure
                            let $Type := data($HorizontalCommercialStructure/Type)
                            return
								if (fn-bea:trim($Type) != '') then (
	                                <ns0:HorizontalCommercialStructure?>
	                                    <ns0:Type>{ $Type }</ns0:Type>
	                                    <ns0:Code>{ data($HorizontalCommercialStructure/Code) }</ns0:Code>
	                                    <ns0:ValidFrom>{ local:getDate(data($HorizontalCommercialStructure/EFECT)) }</ns0:ValidFrom>
	                                    <ns0:ValidTo>{ local:getDate(data($HorizontalCommercialStructure/VENCI)) }</ns0:ValidTo>
	                                </ns0:HorizontalCommercialStructure>
                                ) else ()
                        }
                        {
                            for $BusinessAccountExecutive in $agent/BusinessAccountExecutives/BusinessAccountExecutive
                            let $BAEType := data($BusinessAccountExecutive/BAEType)
                            return
								if (fn-bea:trim($BAEType) != '') then (
	                                <ns0:BusinessAccountExecutive?>
	                                    <ns0:BAEType?>{ local:nullIfEmpty($BAEType) }</ns0:BAEType>
	                                    <ns0:BAECode?>{ local:nullIfEmpty(data($BusinessAccountExecutive/BAECode)) }</ns0:BAECode>
	                                    <ns0:MUType?>{ local:nullIfEmpty(data($BusinessAccountExecutive/MUType)) }</ns0:MUType>
	                                    <ns0:MUCode?>{ local:nullIfEmpty(data($BusinessAccountExecutive/MUCode)) }</ns0:MUCode>
	                                    <ns0:ProductCode?>{ local:nullIfEmpty(data($BusinessAccountExecutive/ProductCode)) }</ns0:ProductCode>
	                                </ns0:BusinessAccountExecutive>
                                ) else ()
                        }
                    </ns0:AgentData>
                )
            }
        </ns0:QbeRetrieveAgentInfoRs>
};

declare variable $agent as element() external;

declare function local:getDate($date as xs:string?) as xs:date?
{
	if ( $date != '00000000' and matches($date, '\d{8}') and substring($date, 1, 4) != '0000' and substring($date, 5, 2) != '00' and substring($date, 7, 2) != '00' )
	then ( fn-bea:date-from-string-with-format("yyyyMMdd", $date) )
	else ( )
};

declare function local:zeroToEmpty($value as xs:string?) as xs:string?
{
	if ( exists($value) and $value != '0' and xs:decimal($value) != 0 )
	then ( xs:string($value) )
	else ( )
};

declare function local:nullIfEmpty($value as xs:string?) as xs:string?
{
	if ( exists($value) and $value != '' )
	then ( $value )
	else ( )
};

xf:AIS_AgentSync_Find_Rs($agent)
