(:: pragma bea:mfl-element-parameter parameter="$resp" type="AIS_AgentSync_Register_Rs@" location="../MFL/AIS_AgentSync_Register_Rs.mfl" ::)
(:: pragma bea:global-element-return element="ns0:QbeRegisterNewAgentRs" location="../WSDL/AIS_AgentSync.wsdl" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/AIS_AgentSync/Resource/XQuery/AIS_AgentSync_Register_Rs/";

declare function xf:AIS_AgentSync_Register_Rs($resp as element())
    as element(ns0:QbeRegisterNewAgentRs) {
        <ns0:QbeRegisterNewAgentRs>
            <ns0:ManID>{ data($resp/MANID) }</ns0:ManID>
            <ns0:AgentID>{ data($resp/AGENTID) }</ns0:AgentID>
        </ns0:QbeRegisterNewAgentRs>
};

declare variable $resp as element() external;

xf:AIS_AgentSync_Register_Rs($resp)
