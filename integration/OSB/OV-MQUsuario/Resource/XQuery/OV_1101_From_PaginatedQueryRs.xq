xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1101.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQUsuario/Resource/XQuery/OV_1101_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1101_From_PaginatedQueryRs($resp as element())
    as element(Response) {
         <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <DATOS_POLIZA>
                    {
                        let $item := $resp/ns0:RowSet/ns0:Row[1]
                        return
                            (
                            <ALERT_OP>{ data($item/ns0:Column[@name = 'OPERPTE']) }</ALERT_OP>,
                            <CLIENSEC>{ data($item/ns0:Column[@name = 'CLIENSEC']) }</CLIENSEC>,
                            <CLIENDES>{ data($item/ns0:Column[@name = 'CLIENDES']) }</CLIENDES>,
                            <DOMICSEC>{ data($item/ns0:Column[@name = 'DOMICSEC']) }</DOMICSEC>,
                            <CUENTSEC>{ data($item/ns0:Column[@name = 'CUENTSEC']) }</CUENTSEC>,
                            <ESTADO>{ data($item/ns0:Column[@name = 'SITUCPOL']) }</ESTADO>,
                            <ACREEDOR>{ data($item/ns0:Column[@name = 'ACREEDOR']) }</ACREEDOR>,
                            <FEC_INI>{ data($item/ns0:Column[@name = 'FECINI']) }</FEC_INI>,
                            <FEC_RENOV>{ data($item/ns0:Column[@name = 'FECULTRE']) }</FEC_RENOV>,
                            <CANAL_COB>{ data($item/ns0:Column[@name = 'COBRODAB']) }</CANAL_COB>,
                            <TIPO_CUE>{ data($item/ns0:Column[@name = 'COBRODES']) }</TIPO_CUE>,
                            <NRO_CUE>{ data($item/ns0:Column[@name = 'CUENTNUM']) }</NRO_CUE>,
                            <CAMPA_COD>{ data($item/ns0:Column[@name = 'CAMPACOD']) }</CAMPA_COD>,
                            <CAMPA_DES>{ data($item/ns0:Column[@name = 'CAMPADES']) }</CAMPA_DES>,
                            <AFINIDAD>{ data($item/ns0:Column[@name = 'GRUPOASE']) }</AFINIDAD>,
                            <CLUB_ECO></CLUB_ECO>
                           )
                    }
                    </DATOS_POLIZA>
                )
            }
        </Response>
};

declare variable $resp as element() external;

xf:OV_1101_From_PaginatedQueryRs($resp)
