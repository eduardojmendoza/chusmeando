xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$RequestOriginal" element="Request" location="../XSD/OV_1540.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$lovsRs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$esRenovacion" type="xs:string" ::)
(:: pragma bea:local-element-return type="xs:anyType" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/OV_1540_To_QBERegisterApplicationRq/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:getInsisAddressType($OVAddresType as xs:string? ) as xs:string {
    if (exists($OVAddresType) and $OVAddresType != '') then
    (
		let $at := upper-case($OVAddresType)
                		return
                			if (starts-with($at,'PAR')) then ( 'H' )
                			else if (starts-with($at,'LAB')) then ( 'W' )
                			else if (starts-with($at,'COR')) then ( 'H' )                			
                			else if (starts-with($at,'COM')) then ( 'C' ) 
                			else if (starts-with($at,'DOM')) then ( 'DOMAGENT' )
                			else 'NO-VALID-TYPE'
	)
    else '' 
};

declare function xf:OV_1540_To_QbeChangePolicyAddressRq(
	$RequestOriginal as element(Request),
	$lovsRs as element()?,
	$policy as xs:string?,
	$esRenovacion as xs:string?
	) as element(*) {
        <ns0:QbeChangePolicyAddressRq>
			<ns0:PolicyNo>{ $policy }</ns0:PolicyNo>
			<ns0:AddressPurpose>CORRESP</ns0:AddressPurpose>
			<ns0:Address>
				<ns0:CountryCode>AR</ns0:CountryCode>
				(: en el bug 107 piden que se mande por defecto Argentina :)
				{
				if (data($RequestOriginal/CODPROV) != '') then
				(
				<ns0:StateRegion>{ xs:int(data($RequestOriginal/CODPROV)) }</ns0:StateRegion>
				) else ()
				}
				<ns0:City>{ normalize-space(data($RequestOriginal/CLIDOMICPOB)) }</ns0:City>
				<ns0:CityCode>{ data($lovsRs/lov:LOV[@id = "City" and @ref = xs:int(data($RequestOriginal/CODPROV))][1]/ns0:ListItems/ns0:ListItem[ns0:ItemDescription = normalize-space(data($RequestOriginal/CLIDOMICPOB))]/ns0:ItemID) }</ns0:CityCode>
				<ns0:ResidentialAddress>
					<ns0:StreetName>{ normalize-space(data($RequestOriginal/CLIDOMICDOM)) }</ns0:StreetName>
					{
					let $cityCode := data($lovsRs/lov:LOV[@id = "City" and @ref = xs:int(data($RequestOriginal/CODPROV))][1]/ns0:ListItems/ns0:ListItem[ns0:ItemDescription = normalize-space(data($RequestOriginal/CLIDOMICPOB))]/ns0:ItemID)
					return
					if (xs:int($cityCode) = 21762) then (: CASO DE QUE LA CIUDAD SEA CAPITAL FEDERAL :)
					(
					<ns0:StreetID>{ data($lovsRs/lov:LOV[@id = "Street" and @ref =
						concat('AR',data($RequestOriginal/CLIDOMICCPO))][1]/ns0:ListItems/ns0:ListItem[ns0:ItemDescription
						= normalize-space(data($RequestOriginal/CLIDOMICDOM))]/ns0:ItemID) }</ns0:StreetID>
					) else ()
					}
					<ns0:StreetNo>{ data($RequestOriginal/CLIDOMICDNU) }</ns0:StreetNo>
					<ns0:Floor>{ data($RequestOriginal/CLIDOMICPIS) }</ns0:Floor>
					<ns0:Apartment>{ data($RequestOriginal/CLIDOMICPTA) }</ns0:Apartment>
				</ns0:ResidentialAddress>
				<ns0:ZipCode>{ data($RequestOriginal/CLIDOMICCPO) }</ns0:ZipCode>
				<ns0:AddressType>{
					if ($esRenovacion = 'S') then
					(
						if(data($RequestOriginal/CLIDOMICCAL) != '') then xf:getInsisAddressType(data($RequestOriginal/CLIDOMICCAL)) else 'H'
					)
					(: En caso de que la persona sea Jurídica tiene default tipo de dirección C, si es física default tipo H:) 
                	else if (data($RequestOriginal/CLICLIENTIP) = '15' or ( data($RequestOriginal/CLICLIENTIP) = '' and data($RequestOriginal/RAZONSOC) != '')  ) then 
        				('C') 
            		else  'H'
				}</ns0:AddressType>
			</ns0:Address>
		</ns0:QbeChangePolicyAddressRq>
};

declare variable $RequestOriginal as element(Request) external;
declare variable $lovsRs as element(lov:LOVs)? external;
declare variable $policy as xs:string? external;
declare variable $esRenovacion as xs:string? external;
	
xf:OV_1540_To_QbeChangePolicyAddressRq($RequestOriginal, $lovsRs, $policy, $esRenovacion)
