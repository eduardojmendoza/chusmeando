(:: pragma bea:global-element-parameter parameter="$request1" element="Request" location="../XSD/OV_1540.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$esRenovacion" type="xs:string" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QbeRegisterNewPersonRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/OV_1540_To_QbeRegisterNewPersonRq/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:getInsisAddressType($OVAddresType as xs:string? ) as xs:string {
    if (exists($OVAddresType) and $OVAddresType != '') then
    (
		let $at := upper-case($OVAddresType)
                		return
                			if (starts-with($at,'PAR')) then ( 'H' )
                			else if (starts-with($at,'LAB')) then ( 'W' )
                			else if (starts-with($at,'COR')) then ( 'H' )                			
                			else if (starts-with($at,'COM')) then ( 'C' ) 
                			else if (starts-with($at,'DOM')) then ( 'DOMAGENT' )
                			else 'NO-VALID-TYPE'
	)
    else '' 
};

declare function xf:getInsisDocType($OVDocType as xs:string, $LOVs as element(*) ) as xs:string{
    if (  exists($OVDocType) and exists($LOVs) and data($OVDocType) != '') then (
    	let $docTypeInt := xs:int(data($OVDocType))
    	return
		if ($docTypeInt = 2 or $docTypeInt = 3) then
			let $docType := if($docTypeInt = 2) then '3' else '2'
			let $insisDocType := $LOVs/lov:LOV[@id = "DocTypeNameById" and @ref = concat('',$docTypeInt) ][1]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID
			return
				if(exists($insisDocType)) then data($insisDocType) else ''
		else 
			let $insisDocType := $LOVs/lov:LOV[@id = "DocTypeNameById" and @ref = $OVDocType ][1]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID
			return
				if(exists($insisDocType)) then data($insisDocType) else ''
			)
    else ''
};

declare function xf:OV_1540_To_QbeRegisterNewPersonRq($request1 as element(Request), $LOVs as element()?, $estadoPersona as xs:string?, $esRenovacion as xs:string?)
    as element() {
        <ns0:QbeRegisterNewPersonRq>
           <ns0:Entity>{
           	  let $docType := xf:getInsisDocType(data($request1/TIPODOCU), $LOVs)
				return
				if (data($request1/CLICLIENTIP) = '15' or data($request1/RAZONSOC) != '') then
	            (
	                <ns0:CompanyData>
	                    <ns0:Name>{ data($request1/RAZONSOC) }</ns0:Name>
	                    <ns0:CustomerID>{ 	
	                    	if ($estadoPersona = 'E') then 
        						concat('CUIT-E-', data($request1/UIFCUIT))
        					else concat('CUIT-', data($request1/UIFCUIT)) 
	                    }</ns0:CustomerID>
	                    <ns0:CompType>15</ns0:CompType>
	                </ns0:CompanyData>,
	                <ns0:NameSuffix>CUIT</ns0:NameSuffix>
	            ) else
	            (
	                <ns0:PersonalData>
	                    <ns0:Name>
	                        <ns0:Given>{ normalize-space(data($request1/AISCLICLIENNOM)) }</ns0:Given>
	                        <ns0:Family>{ normalize-space(concat(data($request1/CLICLIENAP1),data($request1/CLICLIENAP2))) }</ns0:Family>
	                    </ns0:Name>
	                    <ns0:PID>{ 	
	                    	if ($estadoPersona = 'E') then 
                				concat('DNI-E-', data($request1/NUMEDOCU))
                			else concat($docType, '-', data($request1/NUMEDOCU)) 
	                    }</ns0:PID>
	                    <ns0:Gender>{
                		let $sexo := upper-case(data($request1/SEXO))
                		return
                			if ($sexo = 'M') then ('1')
                			else if ($sexo = 'F') then ('2')
                			else ('1')
	                    }</ns0:Gender>
	                    { if( data($request1/NACIMANN) != '' and xs:long(data($request1/NACIMANN)) > 1) then     
	                    		<ns0:BirthDate>{ fn-bea:date-from-string-with-format('yyyyMMdd', concat(fn-bea:pad-left(data($request1/NACIMANN),4,'0'),fn-bea:pad-left(data($request1/NACIMMES),2,'0'),fn-bea:pad-left(data($request1/NACIMDIA),2,'0'))) }</ns0:BirthDate>
	                    	else <ns0:BirthDate />
	                    }		
	                </ns0:PersonalData>,
	                <ns0:NameSuffix>{ $docType }</ns0:NameSuffix>
	            )
                }
               
                <ns0:Nationality>00</ns0:Nationality>
                <ns0:IndustryCode>{ data($request1/PROFECOD) }</ns0:IndustryCode>
                <ns0:DataSource>
                {
	                if (not(data($request1/CLICLIENTIP) = '15' or data($request1/RAZONSOC) != '')) then
		            (
		            	data($request1/ESTCIV)
		            ) else ()
		        }
	            </ns0:DataSource>
                {if(exists($request1/UIFCUIT) and data($request1/UIFCUIT) != '' and data($request1/UIFCUIT) != '0') then
                <ns0:Documents>
                    <ns0:Document>
                        <ns0:DocumentID>ADD_DOC1</ns0:DocumentID>
                        <ns0:DocumentName>CUIT</ns0:DocumentName>
                        <ns0:DocumentSerial>CUIT</ns0:DocumentSerial>
                        <ns0:DocumentNo>{ normalize-space(data($request1/UIFCUIT)) }</ns0:DocumentNo>
                    </ns0:Document>
                </ns0:Documents>
                else ()
                }
            </ns0:Entity>
            <ns0:Addresses>
                <ns0:Address>
                	(:{ if (data($request1/CLIDOMPAISSCOD) != '00') then
                    <ns0:CountryCode>{ data($request1/CLIDOMPAISSCOD) }</ns0:CountryCode>
                    else () } :)
                    <ns0:CountryCode>AR</ns0:CountryCode> (: en el bug 107 piden que se mande por defecto Argentina :)
                    { 
    					if (data($request1/CODPROV) != '') then
    					(
							<ns0:StateRegion>{ xs:int(data($request1/CODPROV)) }</ns0:StateRegion>
    					) else ()
    				 }
                    <ns0:City>{ normalize-space(data($request1/CLIDOMICPOB)) }</ns0:City>
                    <ns0:CityCode>{ data($LOVs/lov:LOV[@id = "City" and @ref = xs:int(data($request1/CODPROV))][1]/ns0:ListItems/ns0:ListItem[ns0:ItemDescription = normalize-space(data($request1/CLIDOMICPOB))]/ns0:ItemID) }</ns0:CityCode>
                    <ns0:ResidentialAddress>
                        <ns0:StreetName>{ normalize-space(data($request1/CLIDOMICDOM)) }</ns0:StreetName>
                        {
                        	let $cityCode := data($LOVs/lov:LOV[@id = "City" and @ref = xs:int(data($request1/CODPROV))][1]/ns0:ListItems/ns0:ListItem[ns0:ItemDescription = normalize-space(data($request1/CLIDOMICPOB))]/ns0:ItemID)
                        	return
                        	if (xs:int($cityCode) = 21762) then (: CASO DE QUE LA CIUDAD SEA CAPITAL FEDERAL :)
                        	(
                        		<ns0:StreetID>{ data($LOVs/lov:LOV[@id = "Street" and @ref = concat('AR',data($request1/CLIDOMICCPO))][1]/ns0:ListItems/ns0:ListItem[ns0:ItemDescription = normalize-space(data($request1/CLIDOMICDOM))]/ns0:ItemID) }</ns0:StreetID>
                        	) else ()
                        }
                        <ns0:StreetNo>{ data($request1/CLIDOMICDNU) }</ns0:StreetNo>
                        <ns0:Floor>{ data($request1/CLIDOMICPIS) }</ns0:Floor>
                        <ns0:Apartment>{ data($request1/CLIDOMICPTA) }</ns0:Apartment>
                    </ns0:ResidentialAddress>
                    <ns0:ZipCode>{ data($request1/CLIDOMICCPO) }</ns0:ZipCode>
                    <ns0:AddressType>{
                    	if ($esRenovacion = 'S') then
                    	(	
                    		if(data($request1/CLIDOMICCAL) != '') then xf:getInsisAddressType(data($request1/CLIDOMICCAL)) else 'H'
                    	)
                    	else ( 
                    			if (data($request1/CLICLIENTIP) = '15' or data($request1/RAZONSOC) != '') then 
								('C')
								else 'H'
                    	)
            		}</ns0:AddressType>
                    (: <ns0:Address>{ data($request1/CLIDOMCPACODPO) }</ns0:Address> COMENTADO PORQUE ESTE CAMPO HACE OVERRIDE DE LA ADDRESS:)
                </ns0:Address>
            </ns0:Addresses>
            <ns0:Contacts?>
            	{ 
	            	if (data($request1/CLIDOMICTLF) != '') then
	                <ns0:Contact>
	                    <ns0:Type>ADDR</ns0:Type>
	                    <ns0:Details>{ data($request1/CLIDOMICTLF) }</ns0:Details>
	                </ns0:Contact>
	                else ()
                }
                { 
                	if ($esRenovacion = 'S') then
                	()
                	else (
                		for $medio in $request1/MEDIOSCONTACTO/MEDIO[replace(data(MEDCODAT), '\s*\n\s*', '','m') != '']
	                	return
		                <ns0:Contact>
		                    <ns0:Type>{ data($medio/MEDCOCOD) }</ns0:Type>
		                    <ns0:Details>{ replace(data($medio/MEDCODAT),' ','') }</ns0:Details>
		                </ns0:Contact>
		            )
                }
            </ns0:Contacts>
            <ns0:PaymentInstruments>
                <ns0:PaymentInstrument>
                    <ns0:AccountType>{ data($request1/COBROTIP) }</ns0:AccountType>
                    {
	                    if(data($request1/COBROTIP) != 'DB') then (
	                      <ns0:AccountNo>{ data($request1/CUENNUME) }</ns0:AccountNo>,
	                      <ns0:BankCode>{ data($request1/BANCOCODCUE) }</ns0:BankCode>,
	                      <ns0:BranchCode>{ data($request1/SUCURCODCUE) }</ns0:BranchCode> )
	                    else if (exists($request1/CUENNUME) and data($request1/CUENNUME) != '' 
	                    		and exists($request1/VENCIMES) and data($request1/VENCIMES) != ''
	                    		and exists($request1/VENCIANN) and data($request1/VENCIANN) != '') then
	                    (
	                      	  let $cuennume := data($request1/CUENNUME)
	                      	  let $vencimes := xs:int(data($request1/VENCIMES))
	                      	  let $venciann := xs:int(data($request1/VENCIANN))
	                      	  return
		                      <ns0:AccountNo>{ concat($cuennume, fn-bea:format-number($vencimes, '00'), fn-bea:format-number($venciann, '0000')) }</ns0:AccountNo>,
		                      <ns0:BankCode>{ 
		      					if (data($request1/BANCOCODCUE) != '' and data($request1/BANCOCODCUE) != '0') 
		      					then data($request1/BANCOCODCUE)
		      					else replace(substring(data($request1/CUENNUME),1,3),'^0+','')
	                      	  }</ns0:BankCode>
	                      
	                     ) else ()			 
                    }
                    (: *******COMENTADO POR BUG 690, NO VA EL MAPEO EN ESTE CAMPO********
                    {  if( data($request1/COBROTIP) != 'DB' and data($request1/VENCIANN) != '' and xs:long(data($request1/VENCIANN)) > 1) then     
                      <ns0:ValidTo>{ fn-bea:date-from-dateTime(funUg:last-date-of-month-from-year-month(data($request1/VENCIANN), data($request1/VENCIMES))) }</ns0:ValidTo>
                      else ()
                    }:)
                </ns0:PaymentInstrument>
            </ns0:PaymentInstruments>
            <ns0:TaxData>
            	(: EL CAMPO CLIENIVA ES OBLIGATORIO :)
	        	<ns0:IVAType>{ data($request1/CLIENIVA) }</ns0:IVAType>
	        	{ 
	    			if (data($request1/CLIENIVA) != '3' ) then
		        		( <ns0:GANType>1</ns0:GANType>
		        		) else ()
		        }
				{
					if (data($request1/CLIENIVA) != '3') then 
						if (exists($request1/CLIEIBTP) and data($request1/CLIEIBTP) != '') then
							<ns0:IBRType>{ data($request1/CLIEIBTP) }</ns0:IBRType>
						else <ns0:IBRType>3</ns0:IBRType>
					else ()
				}
				{
					if (data($request1/CLIENIVA) != '3' and exists($request1/NROIIBB) and data($request1/NROIIBB[text() != '0']) != '') then
						<ns0:IBRNumber>{ data($request1/NROIIBB) }</ns0:IBRNumber>
					else ()
				}
				{
	    			if (exists($request1/CLIEIiTP) and data($request1/CLIEIiTP[text() != '0']) != '') then
	        		(
		        		<ns0:IITType>{ data($request1/CLIEIiTP) }</ns0:IITType>
	        		) else ()
	        	}
            </ns0:TaxData>
        </ns0:QbeRegisterNewPersonRq>
};

declare variable $request1 as element(Request) external;
declare variable $LOVs as element(lov:LOVs)? external;
declare variable $estadoPersona as xs:string? external;
declare variable $esRenovacion as xs:string? external;

xf:OV_1540_To_QbeRegisterNewPersonRq($request1, $LOVs, $estadoPersona, $esRenovacion)
