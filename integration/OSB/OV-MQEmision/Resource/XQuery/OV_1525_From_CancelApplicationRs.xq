xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1525.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1525_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1525_From_CancelApplicationRs($resp as element())
    as element(Response) {
        <Response>
            <Estado resultado = "true" mensaje = ""/>
			<TEXTO>{ data($resp/TEXTO) }</TEXTO>
        </Response>
};

declare variable $resp as element() external;

xf:OV_1525_From_CancelApplicationRs($resp)
