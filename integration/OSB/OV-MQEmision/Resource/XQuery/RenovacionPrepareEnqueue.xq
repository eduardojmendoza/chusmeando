xquery version "1.0" encoding "UTF-8";
(:: pragma  parameter="$renovaciones" type="xs:anyType" ::)
(:: pragma  parameter="$notasInternas" type="xs:anyType" ::)
(:: pragma  parameter="$textoFrentes" type="xs:anyType" ::)
(:: pragma  parameter="$infoArchivos" type="xs:anyType" ::)
(:: pragma  type="xs:anyType" ::)

declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/MFL/RenovacionPrepareEnqueue/";

(:Se encarga de agrupar cada poliza con su respectivo NI y TF:)

declare function xf:RenovacionPrepareEnqueue($renovaciones as element(*),
    $notasInternas as element(*),
    $textoFrentes as element(*),
    $infoArchivos as element(*))
    as element(*) {
       
       <Renovacion>
         {
         for $request in $renovaciones/REQUEST
                let $poliza:=substring(data($request/RENEWED_POLICY),25,6)
                return
              <RenovacionRequest>
                  <INFO>{$infoArchivos}</INFO>
	              <RENOVACION>{$request}</RENOVACION>
				  <NOTAS>{$notasInternas/Request[CERTISER=$poliza]}</NOTAS>
				  <TEXTO>{$textoFrentes/Request[CERTISER=$poliza]}</TEXTO>
		       </RenovacionRequest>
         }
     </Renovacion>

	
};

declare variable $renovaciones as element(*) external;
declare variable $notasInternas as element(*) external;
declare variable $textoFrentes as element(*) external;
declare variable $infoArchivos as element(*) external;

xf:RenovacionPrepareEnqueue($renovaciones,
    $notasInternas,
    $textoFrentes,
    $infoArchivos)
