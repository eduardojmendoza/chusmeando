xquery version "1.0" encoding "UTF-8";
(:: pragma  type="xs:anyType" ::)

declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/docNumDBSequence/";

(: Retorna 
<RESULTS>
  <RESULT><NUMERO>37</NUMERO></RESULT>
  <RESULT><NUMERO>38</NUMERO></RESULT>
</RESULTS>
:)
declare function xf:docNumDBSequence($tipo as xs:string,
    $cantidad as xs:int)
    as element(*) {
        <RESULTS>{
        for $i in 1 to $cantidad return
        fn-bea:execute-sql('jdbc/SNCV', xs:QName('RESULT'), 'EXEC P_NUMERADOR_DOCUMENTO ?', $tipo)
        }</RESULTS>
};

declare variable $tipo as xs:string external;
declare variable $cantidad as xs:int external;

xf:docNumDBSequence($tipo,
    $cantidad)
