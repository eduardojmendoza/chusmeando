xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$RequestOriginal" type="xs:anyType ::)
(:: pragma bea:global-element-parameter parameter="$QBEFindPersonByPIDRs" type="xs:anyType ::)
(:: pragma bea:global-element-parameter parameter="$estadoPersona" type="xs:string ::)
(:: pragma bea:local-element-return type="xs:string" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/OV_1540_CompareAddresses/";

declare function xf:OV_1540_CompareAddresses(
	$RequestOriginal as element(),
	$QBEFindPersonByPIDRs as element()?,
	$estadoPersona as xs:string?
	) as xs:string {
	
        let $streetNameN 	:= if (exists($RequestOriginal/CLIDOMICDOM) and data($RequestOriginal/CLIDOMICDOM) != '') then normalize-space(data($RequestOriginal/CLIDOMICDOM)) else ''
		let $zipCodeN 		:= if (exists($RequestOriginal/CLIDOMICCPO) and data($RequestOriginal/CLIDOMICCPO) != '') then xs:int(normalize-space(data($RequestOriginal/CLIDOMICCPO))) else ''
		let $stateRegionN 	:= if (exists($RequestOriginal/CODPROV) and data($RequestOriginal/CODPROV) != '') then xs:int(normalize-space(data($RequestOriginal/CODPROV))) else ''
		let $streetNoN 		:= if (exists($RequestOriginal/CLIDOMICDNU) and data($RequestOriginal/CLIDOMICDNU) != '') then normalize-space(data($RequestOriginal/CLIDOMICDNU)) else ''
		let $floorN 		:= if (exists($RequestOriginal/CLIDOMICPIS) and data($RequestOriginal/CLIDOMICPIS) != '') then normalize-space(data($RequestOriginal/CLIDOMICPIS)) else ''
		let $apartmentN 	:= if (exists($RequestOriginal/CLIDOMICPTA) and data($RequestOriginal/CLIDOMICPTA) != '') then normalize-space(data($RequestOriginal/CLIDOMICPTA)) else ''
		return
		
		if ($estadoPersona != 'M') then 'N'
		else if 
		(
		count($QBEFindPersonByPIDRs/ns0:Addresses/ns0:Address
				[
					(not(exists(./ns0:ResidentialAddress/ns0:StreetName)) or (data(./ns0:ResidentialAddress/ns0:StreetName) != '' and normalize-space(data(./ns0:ResidentialAddress/ns0:StreetName)) = $streetNameN))
					and (not(exists(./ns0:ZipCode)) or (data(./ns0:ZipCode) != '' and xs:int(normalize-space(data(./ns0:ZipCode))) = $zipCodeN))
					and (not(exists(./ns0:StateRegion)) or (data(./ns0:StateRegion) != '' and xs:int(normalize-space(data(./ns0:StateRegion))) = $stateRegionN))
					and (not(exists(./ns0:ResidentialAddress/ns0:StreetNo)) or (data(./ns0:ResidentialAddress/ns0:StreetNo) != '' and normalize-space(data(./ns0:ResidentialAddress/ns0:StreetNo)) = $streetNoN))		
					and (not(exists(./ns0:ResidentialAddress/ns0:Floor)) or (data(./ns0:ResidentialAddress/ns0:Floor) != '' and normalize-space(data(./ns0:ResidentialAddress/ns0:Floor)) = $floorN))
					and (not(exists(./ns0:ResidentialAddress/ns0:Apartment)) or (data(./ns0:ResidentialAddress/ns0:Apartment) != '' and normalize-space(data(./ns0:ResidentialAddress/ns0:Apartment)) = $apartmentN))
				]
			) > 0
		) then 'N'
		else 'S'
};

declare variable $RequestOriginal as element() external;
declare variable $QBEFindPersonByPIDRs as element()? external;
declare variable $estadoPersona as xs:string? external;
	
xf:OV_1540_CompareAddresses($RequestOriginal, $QBEFindPersonByPIDRs, $estadoPersona)
