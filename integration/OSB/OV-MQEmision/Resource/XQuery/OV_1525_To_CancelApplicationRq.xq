(:: pragma bea:local-element-parameter parameter="$findPolicyByNumberRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:FindPolicyByNumberRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:CancelApplicationRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1525_To_CancelApplicationRq/";

declare function xf:OV_1525_To_CancelApplicationRq($findPolicyByNumberRs as element(findPolicyByNumberRs))
    as element() {
        <ns0:CancelApplicationRq>
	        <ns0:PolicyId>
                {
                    data($findPolicyByNumberRs/ns0:Policy/ns0:InsuranceConditions/ns0:PolicyId)
                }
			</ns0:PolicyId>
        </ns0:CancelApplicationRq>
};

declare variable $findPolicyByNumberRs as element(findPolicyByNumberRs) external;

xf:OV_1525_To_CancelApplicationRq($findPolicyByNumberRs)