(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QBEService1701Rs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1701C.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/Transacciones/Resource/XQuery/OV_1701C_From_QBEService1701Rs/";

declare function xf:OV_1701C_From_QBEService1701Rs($resp as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <MSGEST>OK</MSGEST>,
            		<FILES>
		                    {
		                    	for $item in $resp/ns0:GeneratedPrintouts/ns0:GeneratedPrintout
		                    	return
		                    	(
				                    <FILE>
										<TIPOHOJA>{ data($item/ns0:PageFormat) }</TIPOHOJA>
										<DESCRIPCION>{ data($item/ns0:ReportDescription) }</DESCRIPCION>
										<RUTA>{ concat('insis://', data($item/ns0:FilePath)) }</RUTA>				                        
				                    </FILE>
						)
					}
		            </FILES>
            	)
			}
        </Response>
};

declare variable $resp as element() external;

xf:OV_1701C_From_QBEService1701Rs($resp)
