(:: pragma bea:mfl-element-parameter parameter="$rqArchRenovacion" type="OV_1540_ARCHIVO_RENOVACION@" location="../../../OV-MQEmision/Resource/MFL/OV_1540_ArchivoRenovacion.mfl" ::)
(:: pragma bea:mfl-element-parameter parameter="$isRenovacion" type="anyType"::)
(:: pragma bea:global-element-return element="Request" location="../../../OV-MQEmision/Resource/XSD/OV_1540ArchivoRenovacion.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/OV_1540ArchRenovacion_To_1540/";

declare function xf:OV_1540ArchRenovacion_To_1540($rqArchRenovacion as element())
    as element(Request) {
        <Request>
            <RAMOPCOD>{ data($rqArchRenovacion/RAMOPCOD) }</RAMOPCOD>
            <USUARCOD>{ data($rqArchRenovacion/USUARCOD) }</USUARCOD>
            <POLIZANN>{ data($rqArchRenovacion/POLIZANN) }</POLIZANN>
            <POLIZSEC>{ data($rqArchRenovacion/POLIZSEC) }</POLIZSEC>
            <CERTIPOL>{ data($rqArchRenovacion/CERTIPOL) }</CERTIPOL>
            <CERTIANN>{ data($rqArchRenovacion/CERTIANN) }</CERTIANN>
            <CERTISEC>{ data($rqArchRenovacion/CERTISEC) }</CERTISEC>
            <EXPEDNUM>{ data($rqArchRenovacion/EXPEDNUM) }</EXPEDNUM>
            <EFECTANN2>{ data($rqArchRenovacion/EFECTANN2) }</EFECTANN2>
            <EFECTMES>{ data($rqArchRenovacion/EFECTMES) }</EFECTMES>
            <EFECTDIA>{ data($rqArchRenovacion/EFECTDIA) }</EFECTDIA>
            <FESOLANN>{ data($rqArchRenovacion/FESOLANN) }</FESOLANN>
            <FESOLMES>{ data($rqArchRenovacion/FESOLMES) }</FESOLMES>
            <FESOLDIA>{ data($rqArchRenovacion/FESOLDIA) }</FESOLDIA>
(:
            <PRECIO_MENSUAL>{ data($rqArchRenovacion/PRECIO_MENSUAL) }</PRECIO_MENSUAL>
:)
            <PRECIO_MENSUAL>{  
					if(string(number(data($rqArchRenovacion/PRECIO_MENSUAL))) != 'NaN') then (
						data($rqArchRenovacion/PRECIO_MENSUAL) div 100.0	
						) else '0.00' 
			}</PRECIO_MENSUAL>

            <CAMP_CODIGO>{ data($rqArchRenovacion/CAMP_CODIGO) }</CAMP_CODIGO>
            <RECARPOR>{ data($rqArchRenovacion/RECARPOR) }</RECARPOR>
            <SN_EPOLIZA>{ data($rqArchRenovacion/SN_EPOLIZA) }</SN_EPOLIZA>
            <EMAIL_EPOLIZA>{ data($rqArchRenovacion/EMAIL_EPOLIZA) }</EMAIL_EPOLIZA>
            <AGECOD>{ data($rqArchRenovacion/AGECOD) }</AGECOD>
            <AGECLA>{ data($rqArchRenovacion/AGECLA) }</AGECLA>
            <AGECOD2>{ data($rqArchRenovacion/AGECOD2) }</AGECOD2>
            <AGECLA2>{ data($rqArchRenovacion/AGECLA2) }</AGECLA2>
            <RENEWED_POLICY>{ data($rqArchRenovacion/RENEWED_POLICY) }</RENEWED_POLICY>
            <RAZONSOC>{ data($rqArchRenovacion/RAZONSOC) }</RAZONSOC>
            <CLICLIENAP1>{ data($rqArchRenovacion/CLICLIENAP1) }</CLICLIENAP1>
            <CLICLIENAP2>{ data($rqArchRenovacion/CLICLIENAP2) }</CLICLIENAP2>
            <AISCLICLIENNOM>{ data($rqArchRenovacion/AISCLICLIENNOM) }</AISCLICLIENNOM>
            <CLIENSEC>{ data($rqArchRenovacion/CLIENSEC) }</CLIENSEC>
            <TIPODOCU>{ data($rqArchRenovacion/TIPODOCU) }</TIPODOCU>
            <NUMEDOCU>{ data($rqArchRenovacion/NUMEDOCU) }</NUMEDOCU>
            <NACIMANN>{ data($rqArchRenovacion/NACIMANN) }</NACIMANN>
            <NACIMMES>{ data($rqArchRenovacion/NACIMMES) }</NACIMMES>
            <NACIMDIA>{ data($rqArchRenovacion/NACIMDIA) }</NACIMDIA>
            <SEXO>{ data($rqArchRenovacion/SEXO) }</SEXO>
            <ESTCIV>{ data($rqArchRenovacion/ESTCIV) }</ESTCIV>
            <CLIDOMICCAL>{ data($rqArchRenovacion/CLIDOMICCAL) }</CLIDOMICCAL>
            <CLIDOMICDOM>{ data($rqArchRenovacion/CLIDOMICDOM) }</CLIDOMICDOM>
            <CLIDOMICDNU>{ data($rqArchRenovacion/CLIDOMICDNU) }</CLIDOMICDNU>
            <CLIDOMICESC>{ data($rqArchRenovacion/CLIDOMICESC) }</CLIDOMICESC>
            <CLIDOMICPIS>{ data($rqArchRenovacion/CLIDOMICPIS) }</CLIDOMICPIS>
            <CLIDOMICPTA>{ data($rqArchRenovacion/CLIDOMICPTA) }</CLIDOMICPTA>
            <CLIDOMICPOB>{ data($rqArchRenovacion/CLIDOMICPOB) }</CLIDOMICPOB>
            <CLIDOMICCPO>{ data($rqArchRenovacion/CLIDOMICCPO) }</CLIDOMICCPO>
            <CODPROV>{ data($rqArchRenovacion/CODPROV) }</CODPROV>
            <CLIDOMPAISSCOD>{ if(data($rqArchRenovacion/CLIDOMPAISSCOD) = '') 
            	then 0
            	else xs:int($rqArchRenovacion/CLIDOMPAISSCO) 
            }</CLIDOMPAISSCOD>
            <CLIDOMCPACODPO>{ data($rqArchRenovacion/CLIDOMCPACODPO) }</CLIDOMCPACODPO>
            <MEDIOSCONTACTO>{ $rqArchRenovacion/MEDIOSCONTACTO/* }</MEDIOSCONTACTO>
            <BANCOCOD>{ data($rqArchRenovacion/BANCOCOD) }</BANCOCOD>
            <SUCURSAL_CODIGO>{ data($rqArchRenovacion/SUCURSAL_CODIGO) }</SUCURSAL_CODIGO>
            <LEGAJO_VEND>{ data($rqArchRenovacion/LEGAJO_VEND) }</LEGAJO_VEND>
            <LEGAJO_GTE>{ data($rqArchRenovacion/LEGAJO_GTE) }</LEGAJO_GTE>
            <BANCOCODCUE>{ data($rqArchRenovacion/BANCOCODCUE) }</BANCOCODCUE>
            <SUCURCODCUE>{ data($rqArchRenovacion/SUCURCODCUE) }</SUCURCODCUE>
            <COBROTIP>{ data($rqArchRenovacion/COBROTIP) }</COBROTIP>
            <CUENNUME>{ data($rqArchRenovacion/CUENNUME) }</CUENNUME>
            <VENCIANN>{ data($rqArchRenovacion/VENCIANN) }</VENCIANN>
            <VENCIMES>{ data($rqArchRenovacion/VENCIMES) }</VENCIMES>
            <VENCIDIA>{ fn:day-from-dateTime(funUg:last-date-of-month-from-year-month(data($rqArchRenovacion/VENCIANN), data($rqArchRenovacion/VENCIMES))) }</VENCIDIA>
            <CLIENIVA>{ data($rqArchRenovacion/CLIENIVA) }</CLIENIVA>
            <PROFECOD>{ data($rqArchRenovacion/PROFECOD) }</PROFECOD>
            <AUMARCOD>{ data($rqArchRenovacion/AUMARCOD) }</AUMARCOD>
            <AUMODCOD>{ data($rqArchRenovacion/AUMODCOD) }</AUMODCOD>
            <AUSUBCOD>{ data($rqArchRenovacion/AUSUBCOD) }</AUSUBCOD>
            <AUADICOD>{ data($rqArchRenovacion/AUADICOD) }</AUADICOD>
            <AUMODORI>{ data($rqArchRenovacion/AUMODORI) }</AUMODORI>
            <SIFMVEHI_DES>{ data($rqArchRenovacion/SIFMVEHI_DES) }</SIFMVEHI_DES>
            <AUTIPCOD>{ data($rqArchRenovacion/AUTIPCOD) }</AUTIPCOD>
            <FABRICAN>{ data($rqArchRenovacion/FABRICAN) }</FABRICAN>
            <MOTORNUM>{ data($rqArchRenovacion/MOTORNUM) }</MOTORNUM>
            <CHASINUM>{ data($rqArchRenovacion/CHASINUM) }</CHASINUM>
            <PATENNUM>{ data($rqArchRenovacion/PATENNUM) }</PATENNUM>
            <VEHCLRCOD>{ data($rqArchRenovacion/VEHCLRCOD) }</VEHCLRCOD>
            <AUCATCOD>{ data($rqArchRenovacion/AUCATCOD) }</AUCATCOD>
            <AUUSOCOD>{ data($rqArchRenovacion/AUUSOCOD) }</AUUSOCOD>
            <AUKLMNUM>{ data($rqArchRenovacion/AUKLMNUM) }</AUKLMNUM>
            <AUPAISCOD>{ data($rqArchRenovacion/AUPAISCOD) }</AUPAISCOD>
            <AUPROVICOD>{ data($rqArchRenovacion/AUPROVICOD) }</AUPROVICOD>
            <AUDOMCPACODPO>{ data($rqArchRenovacion/AUDOMCPACODPO) }</AUDOMCPACODPO>
            <GUGARAGE>{ data($rqArchRenovacion/GUGARAGE) }</GUGARAGE>
            <NROFACTU>{ data($rqArchRenovacion/NROFACTU) }</NROFACTU>
            <AUNUMSIN>{ data($rqArchRenovacion/AUNUMSIN) }</AUNUMSIN>
            <AUNUMKMT>{ data($rqArchRenovacion/AUNUMKMT) }</AUNUMKMT>
            <ESCERO>{ data($rqArchRenovacion/ESCERO) }</ESCERO>
            <CONDUCTORES>{
            	for $conduc in $rqArchRenovacion/CONDUCTORES/CONDUCTOR[APELLIDOHIJO != '' and NOMBREHIJO != '']
            	return
            	(
	            	<CONDUCTOR>
						<CONDUTDO>{ data($conduc/CONDUTDO) }</CONDUTDO>
						<CONDUDOC>{ data($conduc/CONDUDOC) }</CONDUDOC>
						<APELLIDOHIJO>{ data($conduc/APELLIDOHIJO) }</APELLIDOHIJO>
						<NOMBREHIJO>{ data($conduc/NOMBREHIJO) }</NOMBREHIJO>
						<CONDUVIN>{ data($conduc/CONDUVIN) }</CONDUVIN>
						<NACIMHIJO>{ concat(data($conduc/NACIMHIJODIA),'/',data($conduc/NACIMHIJOMES),'/',data($conduc/NACIMHIJOANN)) }</NACIMHIJO>
						<SEXOHIJO>{ data($conduc/SEXOHIJO) }</SEXOHIJO>
						<ESTADOHIJO>{ data($conduc/ESTADOHIJO) }</ESTADOHIJO>
						<EXCLUIDO>{ data($conduc/EXCLUIDO) }</EXCLUIDO>
					</CONDUCTOR>
				) 
             }</CONDUCTORES>
            <ACCESORIOS>{ $rqArchRenovacion/ACCESORIOS/* }</ACCESORIOS>
            <ASEGURADOS>
            {
                for $ASEGURADO in $rqArchRenovacion/ASEGURADOS/ASEGURADO
                return
                    <ASEGURADO>
                    	{if( concat($ASEGURADO/APE01, $ASEGURADO/APE02, $ASEGURADO/NOM) != '') then
                        	<NOMBREAS>{ concat($ASEGURADO/APE01 , $ASEGURADO/APE02 , ',', $ASEGURADO/NOM) }</NOMBREAS>
                          else
                            <NOMBREAS></NOMBREAS>
                        }
                        <DOCUMTIP>{ data($ASEGURADO/DOCUMTIP) }</DOCUMTIP>
                        <DOCUMDAT>{ data($ASEGURADO/DOCUMDAT) }</DOCUMDAT>
                    </ASEGURADO>
            }
            </ASEGURADOS>
            <RASTREO>{ data($rqArchRenovacion/RASTREO) }</RASTREO>
            <SWACREPR>{ data($rqArchRenovacion/SWACREPR) }</SWACREPR>
            <APELLIDO>{ data($rqArchRenovacion/APELLIDO) }</APELLIDO>
            <APELLIDO2>{ data($rqArchRenovacion/APELLIDO2) }</APELLIDO2>
            <NOMBRES>{ data($rqArchRenovacion/NOMBRES) }</NOMBRES>
            <TIPODOCU_ACRE>{ data($rqArchRenovacion/TIPODOCU_ACRE) }</TIPODOCU_ACRE>
            <NUMEDOCU_ACRE>{ data($rqArchRenovacion/NUMEDOCU_ACRE) }</NUMEDOCU_ACRE>
            <DOMICCAL>{ data($rqArchRenovacion/DOMICCAL) }</DOMICCAL>
            <DOMICDOM>{ data($rqArchRenovacion/DOMICDOM) }</DOMICDOM>
            <DOMICDNU>{ data($rqArchRenovacion/DOMICDNU) }</DOMICDNU>
            <DOMICESC>{ data($rqArchRenovacion/DOMICESC) }</DOMICESC>
            <DOMICPIS>{ data($rqArchRenovacion/DOMICPIS) }</DOMICPIS>
            <DOMICPTA>{ data($rqArchRenovacion/DOMICPTA) }</DOMICPTA>
            <DOMICPOB>{ data($rqArchRenovacion/DOMICPOB) }</DOMICPOB>
            <DOMICCPO>{ data($rqArchRenovacion/DOMICCPO) }</DOMICCPO>
            <PAISSCOD>{ data($rqArchRenovacion/PAISSCOD) }</PAISSCOD>
            <PROVICOD>{ data($rqArchRenovacion/PROVICOD) }</PROVICOD>
            <DOMICPRE>{ data($rqArchRenovacion/DOMICPRE) }</DOMICPRE>
            <DOMICTLF>{ data($rqArchRenovacion/DOMICTLF) }</DOMICTLF>
            <PLANPOLICOD>{ data($rqArchRenovacion/PLANPOLICOD) }</PLANPOLICOD>
            <FRANQCOD>{ data($rqArchRenovacion/FRANQCOD) }</FRANQCOD>
            <COBERTURAS>{
                for $COBERTURA in $rqArchRenovacion/COBERTURAS/COBERTURA 
                return	<COBERTURA>
                 			<COBERCOD>{data($COBERTURA/COBERCOD)}</COBERCOD>
			                <COBERORD>{data($COBERTURA/COBERORD)}</COBERORD>
			                <CAPITASG>{data($COBERTURA/CAPITASG)}</CAPITASG>
			                <CAPITIMP>{data($COBERTURA/CAPITIMP)}</CAPITIMP>
			                <MONENC>{data($COBERTURA/MONENC)}</MONENC>
        					<ENDOSO>{data($COBERTURA/ENDOSO)}</ENDOSO>
			                { (:Si es renovacion y tiene ajustes :)
			                	if ( count($rqArchRenovacion/COBERAJUSTES/AJUSTE[COBAJU=data($COBERTURA/COBERCOD)])>0) then (	<AJUSTES>
			                			{
			                			$rqArchRenovacion/COBERAJUSTES/AJUSTE[COBAJU=data($COBERTURA/COBERCOD)]
			                			}
			                		</AJUSTES>
			                	) else ()
			                
			                }
			    		</COBERTURA>
           	 }</COBERTURAS>
            <APOD>{ $rqArchRenovacion/APOD/* }</APOD>
            <UIFCUIT>{ data($rqArchRenovacion/UIFCUIT) }</UIFCUIT>
            <AUANTANN>{ data($rqArchRenovacion/AUANTANN) }</AUANTANN>
            <AUUSOGNC>{ data($rqArchRenovacion/AUUSOGNC) }</AUUSOGNC>
            <DATOSPLAN>{ concat(fn-bea:pad-left(data($rqArchRenovacion/PLANPOLICOD), 3, '0'), fn-bea:pad-left(data($rqArchRenovacion/FRANQCOD), 2, '0')) }</DATOSPLAN>
            <PORTAL>{ data($rqArchRenovacion/PORTAL) }</PORTAL>
            <CIAASCRE>{ data($rqArchRenovacion/CIAASCRE) }</CIAASCRE>
            <RAMOPCRE>{ data($rqArchRenovacion/RAMOPCRE) }</RAMOPCRE>
            <POLIZANNREFERIDO>{ data($rqArchRenovacion/POLIZANNREFERIDO) }</POLIZANNREFERIDO>
            <POLIZSECREFERIDO>{ data($rqArchRenovacion/POLIZSECREFERIDO) }</POLIZSECREFERIDO>
            <CERTIPOLREFERIDO>{ data($rqArchRenovacion/CERTIPOLREFERIDO) }</CERTIPOLREFERIDO>
            <CERTIANNREFERIDO>{ data($rqArchRenovacion/CERTIANNREFERIDO) }</CERTIANNREFERIDO>
            <CERTISECREFERIDO>{ data($rqArchRenovacion/CERTISECREFERIDO) }</CERTISECREFERIDO>
            
            	<QMESPERM>{ data($rqArchRenovacion/QMESPERM) }</QMESPERM>
				<QMESRENOV>{ data($rqArchRenovacion/QMESRENOV) }</QMESRENOV>
				<SWTIPCOMP>{ data($rqArchRenovacion/SWTIPCOMP) }</SWTIPCOMP>
				<QCOMIPORP>{ data($rqArchRenovacion/QCOMIPORP) }</QCOMIPORP>
				<SWTIPCOMO>{ data($rqArchRenovacion/SWTIPCOMP) }</SWTIPCOMO>
				<QCOMIPORO>{ data($rqArchRenovacion/QCOMIPORO) }</QCOMIPORO>
				<FEINIANN>{ data($rqArchRenovacion/FEINIANN) }</FEINIANN>
				<FEINIMES>{ data($rqArchRenovacion/FEINIMES) }</FEINIMES>
				<FEINIDIA>{ data($rqArchRenovacion/FEINIDIA) }</FEINIDIA>
				<VIADEPOL>{ data($rqArchRenovacion/VIADEPOL) }</VIADEPOL>
				<VIADECUP>{ data($rqArchRenovacion/VIADECUP) }</VIADECUP>
				<VIADEIVA>{ data($rqArchRenovacion/VIADEIVA) }</VIADEIVA>
				<ININSCOI>{ data($rqArchRenovacion/ININSCOI) }</ININSCOI>
				<FEINSANN>{ data($rqArchRenovacion/FEINSANN) }</FEINSANN>
				<FEINSMES>{ data($rqArchRenovacion/FEINSMES) }</FEINSMES>
				<FEINSDIA>{ data($rqArchRenovacion/FEINSDIA) }</FEINSDIA>
				<ININSCOR>{ data($rqArchRenovacion/ININSCOR) }</ININSCOR>
				<FERASANN>{ data($rqArchRenovacion/FERASANN) }</FERASANN>
				<FERASMES>{ data($rqArchRenovacion/FERASMES) }</FERASMES>
				<FERASDIA>{ data($rqArchRenovacion/FERASDIA) }</FERASDIA>
				<SWDISPOS>{ data($rqArchRenovacion/SWDISPOS) }</SWDISPOS>
				<CIAASCN>{ data($rqArchRenovacion/CIAASCN) }</CIAASCN>
		        <RAMOPCN>{ data($rqArchRenovacion/RAMOPCN) }</RAMOPCN>
			   	<POLIZAN>{ data($rqArchRenovacion/POLIZAN) }</POLIZAN>
			   	<POLIZSN>{ data($rqArchRenovacion/POLIZSN) }</POLIZSN> 
			   	<CERTIPN>{ data($rqArchRenovacion/CERTIPN) }</CERTIPN>
			   	<CERTIAN>{ data($rqArchRenovacion/CERTIAN) }</CERTIAN>
			  	<CERTISN>{ data($rqArchRenovacion/CERTISN) }</CERTISN>
			  	<EMPRESA_CODIGO>{ data($rqArchRenovacion/LEGAJCIA) }</EMPRESA_CODIGO>
	     </Request>
};

declare variable $rqArchRenovacion as element() external;

xf:OV_1540ArchRenovacion_To_1540($rqArchRenovacion)
