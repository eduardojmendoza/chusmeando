xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1584.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1584_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1584_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else
               	(
                    <Estado resultado = "true" mensaje = ""/>,
                    <RespuestaMQ>
					<CERTIFICADOS>
				 	{
	            		for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row
	            		return
	            		<CERTIFICADO>
		            		(:'0001'; RAMOPCOD C(4); POLIZANN N(2);
						POLIZSEC N(6);CERTIPOL N(4); CERTIANN N(4);CERTISEC N(6) :)

							<RAMOPCOD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)) }</RAMOPCOD>
							<POLIZANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,2)) }</POLIZANN>
							<POLIZSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],11,6)) }</POLIZSEC>
							<CERTIPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERTIPOL>
							<CERTIANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERTIANN>
							<CERTISEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERTISEC>

							<NOMBRECLIENTE>{ data($item/ns0:Column[@name = 'NOMBRECLIENTE']) }</NOMBRECLIENTE>
							<TOMARIES>{ data($item/ns0:Column[@name = 'TOMARIES']) }</TOMARIES>
							<PATENTE>{ data($item/ns0:Column[@name = 'PATENTE']) }</PATENTE>
							<SITUCPOL>{ data($item/ns0:Column[@name = 'SITUCPOL']) }</SITUCPOL>
							<IMPRIME_CM>{ data($item/ns0:Column[@name = 'IMPRIME_CM']) }</IMPRIME_CM>

	                    </CERTIFICADO>
	                }
	                </CERTIFICADOS>
	                </RespuestaMQ>
            	)
			}
        </Response>
};

declare variable $paginatedQueryRs1 as element() external;

xf:OV_1584_From_PaginatedQueryRs($paginatedQueryRs1)
