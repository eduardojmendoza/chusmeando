(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QBEService1581Rs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1581.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/Transacciones/Resource/XQuery/OV_1581_From_QBEService1581Rs/";

declare function xf:OV_1581_From_QBEService1581Rs($resp as element())
    as element(Response) {
        <Response>
            <Estado resultado = "true"
                    mensaje = ""/>
            <IMPRESIONES>
                <COPIAPOLIZA>{ data($resp/ns0:COPIAPOLIZA) }</COPIAPOLIZA>
                <CERTIFICADOCOBERTURA>{ data($resp/ns0:CERTIFICADOCOBERTURA) }</CERTIFICADOCOBERTURA>
                <CERTIFICADOMERCOSUR>{ data($resp/ns0:CERTIFICADOMERCOSUR) }</CERTIFICADOMERCOSUR>
                <ORIGINALPOLIZA>{ data($resp/ns0:ORIGINALPOLIZA) }</ORIGINALPOLIZA>
                <TARJETACIRCULACION>{ data($resp/ns0:TARJETACIRCULACION) }</TARJETACIRCULACION>
                <CONSTANCIAPAGOS>{ data($resp/ns0:CONSTANCIAPAGOS) }</CONSTANCIAPAGOS>
            </IMPRESIONES>
        </Response>
};

declare variable $resp as element() external;

xf:OV_1581_From_QBEService1581Rs($resp)
