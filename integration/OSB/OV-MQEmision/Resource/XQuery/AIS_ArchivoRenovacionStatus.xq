(:: pragma bea:global-element-parameter parameter="$archivoRenovacionStatus1" element="ns0:ArchivoRenovacionStatus" location="../XSD/AIS_ArchivoRenovacionStatus.xsd" ::)
(:: pragma bea:mfl-element-return type="ARCHIVO_RENOVACION_STATUS@" location="../MFL/AIS_ArchivoRenovacionStatus.mfl" ::)

declare namespace ns0 = "http://xml.qbe.com.ar/AIS_ArchivoRenovacionStatus";
declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/AIS_ArchivoRenovacionStatus/";

declare function xf:AIS_ArchivoRenovacionStatus($archivoRenovacionStatus1 as element(ns0:ArchivoRenovacionStatus))
    as element() {
        let $ArchivoRenovacionStatus := $archivoRenovacionStatus1
        return
            <ARCHIVO_RENOVACION_STATUS>
                <POLICY_NUMBER>{ data($ArchivoRenovacionStatus/ns0:aisPolicyNumber) }</POLICY_NUMBER>
                <ORIGIN_FLOW>{ data($ArchivoRenovacionStatus/ns0:originFlow) }</ORIGIN_FLOW>
                <RESULT>{ data($ArchivoRenovacionStatus/ns0:result) }</RESULT>
                <NEW_POLICY_APPLICATION_NUMBER>{ data($ArchivoRenovacionStatus/ns0:newPolicyApplicationNumber) }</NEW_POLICY_APPLICATION_NUMBER>
                <NEW_POLICY_NUMBER>{ data($ArchivoRenovacionStatus/ns0:newPolicyNumber) }</NEW_POLICY_NUMBER>
                <ORIGIN_FILENAME>{ data($ArchivoRenovacionStatus/ns0:originFilename) }</ORIGIN_FILENAME>
                <TRANSACTION_ID>{ data($ArchivoRenovacionStatus/ns0:transactionID) }</TRANSACTION_ID>
                <ERROR_CODE>{ data($ArchivoRenovacionStatus/ns0:errorCode) }</ERROR_CODE>
                <ERROR_DETAIL>{ data($ArchivoRenovacionStatus/ns0:erroDetail) }</ERROR_DETAIL>
            </ARCHIVO_RENOVACION_STATUS>
};

declare variable $archivoRenovacionStatus1 as element(ns0:ArchivoRenovacionStatus) external;

xf:AIS_ArchivoRenovacionStatus($archivoRenovacionStatus1)
