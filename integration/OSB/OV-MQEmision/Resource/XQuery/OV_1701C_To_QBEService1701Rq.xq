(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_1701C.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QBEService1701Rq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/Transacciones/Resource/XQuery/OV_1701C_To_QBEService1701Rq/";

declare function xf:OV_1701C_To_QBEService1701Rq($req as element(Request))
    as element() {
        <ns0:QBEService1701Rq>
            <ns0:PolicyNo>
            	{
            		concat( '0001',
							fn-bea:pad-right($req/RAMOPCOD,4),
							fn-bea:format-number(xs:double($req/POLIZANN), '00'),
							fn-bea:format-number(xs:double($req/POLIZSEC), '000000'),
							fn-bea:format-number(xs:double($req/CERTIPOL), '0000'),
							fn-bea:format-number(xs:double($req/CERTIANN), '0000'),
							fn-bea:format-number(xs:double($req/CERTISEC), '000000'))
            	}
            </ns0:PolicyNo>
            {
            	if (data($req/SUPLENUM)!="") then (
            		<ns0:CorrelNum>{ data($req/SUPLENUM) }</ns0:CorrelNum>
            	) else ()
            }
            <ns0:PrintoutPackage>{ data($req/TIPODOCU) }</ns0:PrintoutPackage>
            <ns0:PrintoutType>{ data($req/TIPOIMPR) }</ns0:PrintoutType>
        </ns0:QBEService1701Rq>
};

declare variable $req as element(Request) external;

xf:OV_1701C_To_QBEService1701Rq($req)