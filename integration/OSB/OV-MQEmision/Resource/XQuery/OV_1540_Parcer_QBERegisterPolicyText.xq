xquery version "1.0" encoding "UTF-8";
(:: pragma  parameter="$Request" type="xs:anyType" ::)
(:: pragma  type="xs:anyType" ::)

declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/OV_1540_InternalNotes/";

declare function xf:OV_1540_InternalNotes($Request as element(*))
    as element(*) {


let $certis :=  <X>
{
for $certi  
in $Request//CERTISER
return $certi}
</X>

return

<Result>
{
for $certi in distinct-values($certis/CERTISER)
  	return 
    <Request>
		<CIAASCOR>{data($Request/Request[CERTISER eq $certi][1]/CIAASCOR)}</CIAASCOR>
		<RAMOPCOR>{data($Request/Request[CERTISER eq $certi][1]/RAMOPCOR)}</RAMOPCOR>
		<POLIZANR>{data($Request/Request[CERTISER eq $certi][1]/POLIZANR)}</POLIZANR>
		<POLIZSER>{data($Request/Request[CERTISER eq $certi][1]/POLIZSER)}</POLIZSER>
		<CERTIPOR>{data($Request/Request[CERTISER eq $certi][1]/CERTIPOR)}</CERTIPOR>
		<CERTIANR>{data($Request/Request[CERTISER eq $certi][1]/CERTIANR)}</CERTIANR>
		<CERTISER>{data($Request/Request[CERTISER eq $certi][1]/CERTISER)}</CERTISER>
		<NUMERLIN>0001</NUMERLIN>
		<LINTEXTO>{string-join($Request/Request[CERTISER eq $certi]/LINTEXTO, "")}</LINTEXTO>
	</Request>
	}
</Result>
};

declare variable $Request as element(*) external;

xf:OV_1540_InternalNotes($Request)
