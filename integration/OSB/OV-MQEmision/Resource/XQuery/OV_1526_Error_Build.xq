
declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/OV_1526_Error_Build/";

declare function xf:OV_1526_Error_Build($errores as element())
    as element() {
        <RESPONSE>
        	<TEXTO>ER</TEXTO>
			<POLIZA>
                <RAMOPCOD />
                <POLIZANN />
                <POLIZSEC />
                <CERTIPOL />
                <CERTIANN />
                <CERTISEC />
                <SUPLENUM />
                <OPERAPOL />
            </POLIZA>
            <ERRORES>{
            for $error in $errores/ERROR
            return
                <ERROR>{ data($error) }</ERROR>
            }</ERRORES>
        </RESPONSE>
};

declare variable $errores as element() external;

xf:OV_1526_Error_Build($errores)
