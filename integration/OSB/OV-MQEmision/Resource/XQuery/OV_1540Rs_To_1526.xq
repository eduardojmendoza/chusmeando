(:: pragma bea:global-element-parameter parameter="$response1540" element="Response" location="../XSD/OV_1540.xsd" ::)
(:: pragma bea:global-element-return element="Request" location="../XSD/OV_1526.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/OV_1540Rs_To_1526/";

declare function xf:OV_1540Rs_To_1526($usuario as xs:string, $response1540 as element(Response))
    as element(Request) {
        <Request>
            <USUARIO>{ $usuario }</USUARIO>
            <RAMOPCOD>{ data($response1540/RESULTADO/RAMOPCOD) }</RAMOPCOD>
            <POLIZANN>{ data($response1540/RESULTADO/POLIZANN) }</POLIZANN>
            <POLIZSEC>{ data($response1540/RESULTADO/POLIZSEC) }</POLIZSEC>
            <CERTIPOL>{ data($response1540/RESULTADO/CERTIPOL) }</CERTIPOL>
            <CERTIANN>{ data($response1540/RESULTADO/CERTIANN) }</CERTIANN>
            <CERTISEC>{ data($response1540/RESULTADO/CERTISEC) }</CERTISEC>
            <SUPLENUM>{ data($response1540/RESULTADO/SUPLENUM) }</SUPLENUM>
        </Request>
};

declare variable $usuario as xs:string external;
declare variable $response1540 as element(Response) external;

xf:OV_1540Rs_To_1526($usuario, $response1540)
