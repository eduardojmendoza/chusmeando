(:: pragma bea:global-element-parameter parameter="$response" element="Response" location="../XSD/OV_1540.xsd" ::)
(:: pragma bea:mfl-element-return type="OV_1540MQ@" location="../MFL/OV_1540MQ_Rq-ISO-8859-1.mfl" ::)

declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/OV_1540MQ_From_1540Rs/";

declare function xf:OV_1540MQ_From_1540Rs($response as element(Response)?,
    $estado as xs:string?,
    $mensajeError as xs:string?)
    as element() {
        <OV_1540MQ>
            <RESPONSE>
                <ESTADO>{ $estado }</ESTADO>
                <CLAVE-SALIDA>
                    <RAMOPCOD>{ data($response/RESULTADO/RAMOPCOD) }</RAMOPCOD>
                    <POLIZANN>{ data($response/RESULTADO/POLIZANN) }</POLIZANN>
                    <POLIZSEC>{ data($response/RESULTADO/POLIZSEC) }</POLIZSEC>
                    <CERTIPOL>{ data($response/RESULTADO/CERTIPOL) }</CERTIPOL>
                    <CERTIANN>{ data($response/RESULTADO/CERTIANN) }</CERTIANN>
                    <CERTISEC>{ data($response/RESULTADO/CERTISEC) }</CERTISEC>
                </CLAVE-SALIDA>
                <ERROR>{ $mensajeError }</ERROR>
            </RESPONSE>
        </OV_1540MQ>
};

declare variable $response as element(Response) ? external;
declare variable $estado as xs:string ? external;
declare variable $mensajeError as xs:string ? external;

xf:OV_1540MQ_From_1540Rs($response,
    $estado,
    $mensajeError)
