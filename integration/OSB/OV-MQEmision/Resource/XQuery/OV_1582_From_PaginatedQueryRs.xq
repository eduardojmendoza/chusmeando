(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1582.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/OV_1582_From_PaginatedQueryRs/";

declare function xf:OV_1582_From_PaginatedQueryRs($resp as element(), $req as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <REGS>
                        {
                            for $item in $resp/ns0:RowSet/ns0:Row
                            return
                            (
                                <REG>
                                    <PROD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)) }</PROD>
                                    <POL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,8)) }</POL>
                                    <CERPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERPOL>
                                    <CERANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERANN>
                                    <CERSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERSEC>
									<TOMA>{ data($item/ns0:Column[@name = 'TOMA']) }</TOMA>
									<EST>{ data($item/ns0:Column[@name = 'EST']) }</EST>
									<SINI>{ data($item/ns0:Column[@name = 'SINI']) }</SINI>
									<LUPA>{ data($item/ns0:Column[@name = 'LUPA']) }</LUPA>
									<IMPRCERTIFICADOCOBERTURA>{ data($item/ns0:Column[@name = 'IMPRCERTIFICADOCOBERTURA']) }</IMPRCERTIFICADOCOBERTURA>
									<IMPRTARJETACIRCULACION>{ data($item/ns0:Column[@name = 'IMPRCERTIFICADOMERCOSUR']) }</IMPRTARJETACIRCULACION>
									<IMPRCERTIFICADOMERCOSUR>{ data($item/ns0:Column[@name = 'IMPRCERTIFICADOMERCOSUR']) }</IMPRCERTIFICADOMERCOSUR>
									<PATENTENUM>{ data($item/ns0:Column[@name = 'PATENTENUM']) }</PATENTENUM>
									<RAMO>{ data($item/ns0:Column[@name = 'RAMO']) }</RAMO>
                                </REG>
                            )
                        }
                    </REGS>,
					<Request>
						<USUARIO>{ data($req/USUARIO) }</USUARIO>
						<STATUS />
						<ERR_DESC />
						<RAMOPCOD>{ data($req/RAMOPCOD) }</RAMOPCOD>
						<POLIZANN>{ data($req/POLIZANN) }</POLIZANN>
						<POLIZSEC>{ data($req/POLIZSEC) }</POLIZSEC>
						<CERTIPOL>{ data($req/CERTIPOL) }</CERTIPOL>
						<CERTIANN>{ data($req/CERTIANN) }</CERTIANN>
						<CERTISEC>{ data($req/CERTISEC) }</CERTISEC>
						<NROQRY_S />
						<RAMOPCOD_S />
						<POLIZANN_S />
						<POLIZSEC_S />
						<CERTIPOL_S />
						<CERTIANN_S />
						<CERTISEC_S />
					</Request>
                )
            }
        </Response>
};

declare variable $resp as element() external;
declare variable $req as element() external;

xf:OV_1582_From_PaginatedQueryRs($resp, $req)
