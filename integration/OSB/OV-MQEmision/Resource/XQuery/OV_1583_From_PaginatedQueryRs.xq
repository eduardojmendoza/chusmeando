xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1583.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1583_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1583_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else
               	(
                    <Estado resultado = "true" mensaje = ""/>,
					<REGS>
				 	{
	            		for $row in $paginatedQueryRs1/ns0:RowSet/ns0:Row
	            		return
	            		<REG>
	            			<SEC>
	            				{
	            					let $sec := data($row/ns0:Column[@name = 'SEC'])
	            					return
	            						if ($sec != '') then ($sec) else ('0')
	            				}
	            			</SEC>
	            			<ENDOSO>{ fn-bea:format-number(xs:double($row/ns0:Column[@name = 'ENDOSO']), '0000') }</ENDOSO>
	            			<VIGDES>{ fn-bea:date-to-string-with-format("dd/MM/yyyy", data($row/ns0:Column[@name = 'VIGDES'])) }</VIGDES>
	            			<VIGHAS>{ fn-bea:date-to-string-with-format("dd/MM/yyyy", data($row/ns0:Column[@name = 'VIGHAS'])) }</VIGHAS>
	            			<MOT>{ data($row/ns0:Column[@name = 'MOT']) }</MOT>
	            			<ALERTPRIMA>{ data($row/ns0:Column[@name = 'ALERTPRIMA']) }</ALERTPRIMA>
	            			<ALERTCOMI>{ data($row/ns0:Column[@name = 'ALERTCOMI']) }</ALERTCOMI>
	            			<IMPRIMECOPIA>{ data($row/ns0:Column[@name = 'IMPRIMECOPIA']) }</IMPRIMECOPIA>
	            			<ULTIMOENDOSO>{ data($row/ns0:Column[@name = 'ULTIMOENDOSO']) }</ULTIMOENDOSO>
	            			<ENDOSABLE>{ data($row/ns0:Column[@name = 'ENDOSABLE']) }</ENDOSABLE>
	                    </REG>
	                }
	                </REGS>
            	)
			}
        </Response>
};

declare variable $paginatedQueryRs1 as element() external;

xf:OV_1583_From_PaginatedQueryRs($paginatedQueryRs1)
