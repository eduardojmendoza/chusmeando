xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$notasInternas" element="Request" location="../XSD/OV_1540QBERegisterPolicyText.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$textosFrente" element="Request" location="../XSD/OV_1540QBERegisterPolicyText.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QbeRegisterPolicyTextRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/Renovacion_To_QBERegisterPolicyTextRq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:Renovacion_To_QBERegisterPolicyTextRq($notasInternas as element()?,
    $textosFrente as element(*)?)
    as element() {
    
       <ns0:QbeRegisterPolicyTextRq>
        	<ns0:AISPolicyNo> 
        	{
               let $poliza:= if ($notasInternas!='') then ($notasInternas) else ($textosFrente)
				return
							concat('0001' ,
	         					fn-bea:pad-right($poliza/RAMOPCOR,4) ,
	         					fn-bea:format-number(xs:double($poliza/POLIZANR), '00') ,
								fn-bea:format-number(xs:double($poliza/POLIZSER), '000000') ,
								fn-bea:format-number(xs:double($poliza/CERTIPOR), '0000') ,
								fn-bea:format-number(xs:double($poliza/CERTIANR), '0000') ,
								fn-bea:format-number(xs:double($poliza/CERTISER), '000000')
							) } 
            </ns0:AISPolicyNo>
            <ns0:FrontEndText>
            {
 	        	(: Primero le saca los saltos de linea y luego reemplaza los '*' con saltos de linea (para que no queden lineas vacías) :)
            	(: Cambia los '-' de las lineas separadoras de texto por '*':)
				data(replace(data(replace(replace(data($textosFrente/LINTEXTO), '(\r?\n|\r)$', ''),'\*','&#xa;')),'-','_'))
				
            }
            </ns0:FrontEndText>
            
            { if (data($notasInternas/LINTEXTO) != '' ) then (
	            
            <ns0:Notes>
                <ns0:Note>
                    <ns0:Text>{ data(replace(replace(data($notasInternas/LINTEXTO), '(\r?\n|\r)$', ''),'\*','&#xa;'))   }</ns0:Text>
                </ns0:Note>
            </ns0:Notes>
            	) else ()
         	}
        </ns0:QbeRegisterPolicyTextRq>
};

declare variable $notasInternas as element(*)? external;
declare variable $textosFrente as element(*)? external;

xf:Renovacion_To_QBERegisterPolicyTextRq($notasInternas,
    $textosFrente)
