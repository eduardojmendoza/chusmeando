(:: pragma bea:local-element-parameter parameter="$qBERegisterApplicationRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QBERegisterApplicationRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1540.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/OV_1540_From_QBERegisterApplicationRs/";

declare function xf:OV_1540_From_QBERegisterApplicationRs($qBERegisterApplicationRs1 as element())
    as element(Response) {
        <Response>
        	<Estado resultado = "true" mensaje = ""/>
            <RESULTADO>{
            let $policy := data($qBERegisterApplicationRs1/ns0:PolicyNo)
            return
                (<RAMOPCOD>{ substring($policy,5,4) }</RAMOPCOD>,
                <POLIZANN>{ substring($policy,9,2) }</POLIZANN>,
                <POLIZSEC>{ substring($policy,11,6) }</POLIZSEC>,
                <CERTIPOL>{ substring($policy,17,4) }</CERTIPOL>,
                <CERTIANN>{ substring($policy,21,4) }</CERTIANN>,
                <CERTISEC>{ substring($policy,25,6) }</CERTISEC>,
                <SUPLENUM>0</SUPLENUM>,
                <TEXTOS>
                	<TEXTO/>
                </TEXTOS>)
           } </RESULTADO>
        </Response>
};

declare variable $qBERegisterApplicationRs1 as element() external;

xf:OV_1540_From_QBERegisterApplicationRs($qBERegisterApplicationRs1)
