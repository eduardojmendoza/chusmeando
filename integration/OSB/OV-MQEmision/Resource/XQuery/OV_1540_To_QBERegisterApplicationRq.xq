xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$request1" element="Request" location="../XSD/OV_1540.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$esRenovacion" type="xs:string" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QBERegisterApplicationRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/OV_1540_To_QBERegisterApplicationRq/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:getInsisAddressType($OVAddresType as xs:string? ) as xs:string {
    if (exists($OVAddresType) and $OVAddresType != '') then
    (
		let $at := upper-case($OVAddresType)
                		return
                			if (starts-with($at,'PAR')) then ( 'H' )
                			else if (starts-with($at,'LAB')) then ( 'W' )
                			else if (starts-with($at,'COR')) then ( 'H' )
                			else if (starts-with($at,'COM')) then ( 'C' )
                			else if (starts-with($at,'DOM')) then ( 'DOMAGENT' )
                			else 'NO-VALID-TYPE'
	)
    else ''
};

declare function xf:getInsisDocType($OVDocType as xs:string, $LOVs as element(*) ) as xs:string{
    if (  exists($OVDocType) and exists($LOVs) and data($OVDocType) != '') then (
    	let $docTypeInt := xs:int(data($OVDocType))
    	return
		if ($docTypeInt = 2 or $docTypeInt = 3) then
			let $docType := if($docTypeInt = 2) then '3' else '2'
			let $insisDocType := $LOVs/lov:LOV[@id = "DocTypeNameById" and @ref = concat('',$docTypeInt) ][1]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID
			return
				if(exists($insisDocType)) then data($insisDocType) else ''
		else
			let $insisDocType := $LOVs/lov:LOV[@id = "DocTypeNameById" and @ref = $docTypeInt ][1]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID
			return
				if(exists($insisDocType)) then data($insisDocType) else ''
			)
    else ''
};


declare function xf:OV_1540_To_QBERegisterApplicationRq(
	$request1 as element(Request),
	$LOVs as element()?,
	$vehicleKey as xs:string ?,
	$mapPlanPackage as element(*),
	$mockDocNums as element()?,
	$estadoVehiculo as element(*)?, (: Acá adentro viene una estructura, que contiene tanto el resultado como el código de error si lo hay :)
	$estadoPersona as xs:string?,
	$esRenovacion as xs:string?
	) as element() {
        <ns0:QBERegisterApplicationRq>
        	<ns0:Client>
				<ns0:Entity>{
				let $docType :=  xf:getInsisDocType(data($request1/TIPODOCU), $LOVs)
				return
	            if (data($request1/CLICLIENTIP) = '15' or data($request1/RAZONSOC) != '') then
	            (
	                <ns0:CompanyData>
	                	<ns0:Name>{ data($request1/RAZONSOC) }</ns0:Name>
	                    <ns0:CustomerID>{ if (data($estadoPersona) = 'E') then (: Si hay error se concatena "E-" bug 144 :)
                            			 (
                            				concat('CUIT-E-', xs:long(normalize-space(data($request1/UIFCUIT))))
                            			 )
                            			 else concat('CUIT-', xs:long(normalize-space(data($request1/UIFCUIT))))
         				}</ns0:CustomerID>
         				<ns0:CompType>15</ns0:CompType>
	                </ns0:CompanyData>,
	                <ns0:NameSuffix>CUIT</ns0:NameSuffix>
	            ) else (
	                <ns0:PersonalData>
	                	<ns0:Name>
	                        <ns0:Given>{ normalize-space(data($request1/AISCLICLIENNOM)) }</ns0:Given>
	                        <ns0:Family>{ normalize-space(concat(data($request1/CLICLIENAP1),data($request1/CLICLIENAP2))) }</ns0:Family>
	                    </ns0:Name>
	                    <ns0:PID>{
	                    			if (data($estadoPersona) = 'E') then (: Si hay error se concatena "E-" bug 144 :)
                    			 	(
                    					concat($docType, '-E-', normalize-space(data($request1/NUMEDOCU)))
                            		) else concat($docType, '-', normalize-space(data($request1/NUMEDOCU)))
 						}</ns0:PID>
						<ns0:Gender>{
							let $sexo := upper-case(data($request1/SEXO))
	                		return
	                			if ($sexo = 'M') then ('1')
	                			else if ($sexo = 'F') then ('2')
	                			else ('1')
            			}</ns0:Gender>
	                </ns0:PersonalData>,
	                <ns0:NameSuffix>{ $docType }</ns0:NameSuffix>
	            )
                }</ns0:Entity>
            </ns0:Client>
            <ns0:Insureds>
                <ns0:Insured>
                    <ns0:InsuredEntity>
                    	<ns0:ObjectType>111</ns0:ObjectType>
                        <ns0:OCar>
                            <ns0:RegNo>{ if (
                            					data($estadoVehiculo/ns0:Result) = 'N' and
                            					(
                            						data($estadoVehiculo/ns0:ErrorCase) = 'ErrorPatente' or
                            						data($estadoVehiculo/ns0:ErrorCase) = 'ErrorADNoMotorChasis'
                            					)
                            				) then (: Si hay error se concatena "E-" bug 144 y 643 :)
                            			 (
                        					concat('E-',data($request1/PATENNUM))
                            			 )
                            			 else (
                			 				data($request1/PATENNUM)
                            			 )
                           	}</ns0:RegNo>
                            <ns0:CarType></ns0:CarType>
                            <ns0:Model />
                            <ns0:Make />
                            <ns0:Chassis>{ if (
                            					data($estadoVehiculo/ns0:Result) = 'N' and
                            					(
                            						data($estadoVehiculo/ns0:ErrorCase) = 'ErrorChasis' or
                            						data($estadoVehiculo/ns0:ErrorCase) = 'ErrorMotorChasis' or
                            						data($estadoVehiculo/ns0:ErrorCase) = 'ErrorADExistMotorChasis' or
                            						data($estadoVehiculo/ns0:ErrorCase) = 'ErrorADExistChasis'
                            					)
                            				) then (: Si hay error se concatena "E-" bug 144 y 643 :)
                            			 (
                            				concat('E-',data($request1/CHASINUM))
                            			 )
                            			 else (data($request1/CHASINUM)) }</ns0:Chassis>
                            <ns0:Engine>{ if (
                            					data($estadoVehiculo/ns0:Result) = 'N' and
                            					(
                            						data($estadoVehiculo/ns0:ErrorCase) = 'ErrorMotor' or
                            						data($estadoVehiculo/ns0:ErrorCase) = 'ErrorMotorChasis' or
                            						data($estadoVehiculo/ns0:ErrorCase) = 'ErrorADExistMotorChasis' or
                            						data($estadoVehiculo/ns0:ErrorCase) = 'ErrorADExistMotor'
                            					)
                            				) then (: Si hay error se concatena "E-" bug 144 y 643 :)
                            			 (
                            				concat('E-',data($request1/MOTORNUM))
                            			 )
                            			 else (data($request1/MOTORNUM))
                            }</ns0:Engine>
                            <ns0:CarUsage>{ data($request1/AUUSOCOD) }</ns0:CarUsage>
                            <ns0:PaintType />
                            <ns0:ProdYear>{ data($request1/FABRICAN) }</ns0:ProdYear>
                            <ns0:ProdType/>
                            <ns0:CarColor>{ data($request1/VEHCLRCOD) }</ns0:CarColor>
                            <ns0:Modified>0</ns0:Modified>
                            <ns0:VehicleKey>{ $vehicleKey }</ns0:VehicleKey>
                            <ns0:CustomProperties>
                                <ns0:CustomProperty>
                                    <ns0:FieldName>OCP1</ns0:FieldName>
                                    <ns0:Value>{ data($request1/AUKLMNUM) }</ns0:Value>
                                </ns0:CustomProperty>
                                {if(data($request1/AUPAISCOD) != '' and data($request1/AUPAISCOD) != '00') then
                                (
	                                <ns0:CustomProperty>,
	                                    <ns0:FieldName>OCP4</ns0:FieldName>,
	                                    <ns0:Value>{ data($request1/AUPAISCOD) }</ns0:Value>
	                                </ns0:CustomProperty>
                                )
                                else ()}
                                <ns0:CustomProperty>
                                    <ns0:FieldName>OCP8</ns0:FieldName>
                                    <ns0:Value>{ data($request1/AUPROVICOD) }</ns0:Value>
                                </ns0:CustomProperty>
                                <ns0:CustomProperty>
                                    <ns0:FieldName>OCP5</ns0:FieldName>
                                    <ns0:Value>{ if (data($request1/AUDOMCPACODPO)!='') then (	
                                    				xs:long($request1/AUDOMCPACODPO) 
                                    				) else () }</ns0:Value>
                                  
                                </ns0:CustomProperty>
                                <ns0:CustomProperty>
                                    <ns0:FieldName>OCP2</ns0:FieldName>
                                    <ns0:Value>{ if (data($request1/GUGARAGE) = 'S') then 1 else 2 }</ns0:Value>
                                </ns0:CustomProperty>
                                <ns0:CustomProperty>
                                    <ns0:FieldName>OCP6</ns0:FieldName>
                                    <ns0:Value>{ if (data($request1/ESCERO) = 'S') then 1 else 2 }</ns0:Value>
                                </ns0:CustomProperty>
                                <ns0:CustomProperty>
                                    <ns0:FieldName>OCP3</ns0:FieldName>
                                    <ns0:Value>{  if (data($request1/RASTREO) = '0') then 1 else 2 }</ns0:Value>
                                </ns0:CustomProperty>
                                <ns0:CustomProperty>
                                    <ns0:FieldName>OCP13</ns0:FieldName>
                                    <ns0:Value>{ if (data($request1/AUUSOGNC) = 'S') then 1 else 2 }</ns0:Value>
                                </ns0:CustomProperty>
                                <ns0:CustomProperty>
                                    <ns0:FieldName>OCP7</ns0:FieldName> (: EN OV viene 0 = NO, 1 = SI :)
                                    <ns0:Value>{ if (data($request1/SWACREPR) = '1') then 1 else 2 }</ns0:Value>
                                </ns0:CustomProperty>
                            </ns0:CustomProperties>
                            <ns0:SupplementaryEquipment?>{
                            	if (data($request1/AUUSOGNC) = 'S') then
                            	(
                            		<ns0:OAdditionalEquipment>
										<ns0:ParentObjectID>{ 
											if (data($estadoVehiculo/ns0:Result) = 'N' and
                            					(
                            						data($estadoVehiculo/ns0:ErrorCase) = 'ErrorPatente' or
                            						data($estadoVehiculo/ns0:ErrorCase) = 'ErrorADNoMotorChasis'
                            					)
                            				) then (: Si hay error se concatena "E-" bug 144 y 643 :)
                            			 	(
                        						concat('E-',data($request1/PATENNUM))
                            			 	)
                            			 	else (data($request1/PATENNUM))
                           				}</ns0:ParentObjectID>
										<ns0:EquipmentType>6</ns0:EquipmentType>
										<ns0:EquipmentSubtype>1</ns0:EquipmentSubtype>
										<ns0:Model>GNC</ns0:Model>
										<ns0:SerialNo>1</ns0:SerialNo>
										<ns0:AvValue>{ data($request1/ACCESORIOS/ACCESORIO[CODIGOACC = 5]/PRECIOACC) }</ns0:AvValue>
									</ns0:OAdditionalEquipment>
                            	) else ()
                            }
                            {
	                                for $ACCESORIO in $request1/ACCESORIOS/ACCESORIO[DESCRIPCIONACC != '' and CODIGOACC != 5]
	                                return
	                                    <ns0:OAdditionalEquipment>
	                                    	<ns0:ParentObjectID>{ data($request1/PATENNUM) }</ns0:ParentObjectID>
	                                        <ns0:EquipmentType>5</ns0:EquipmentType>
	                                        <ns0:EquipmentSubtype>{
	                                        	if (data($ACCESORIO/CODIGOACC) != '') then
	                                        	(xs:int(data($ACCESORIO/CODIGOACC)))
	                                        	else ()
	                                        }</ns0:EquipmentSubtype>
	                                        (: CAMPO MODEL CORTADO A 20 CARACTERES POR BUGS 215 Y 333 :)
	                                        <ns0:Model>{ substring(normalize-space(data($ACCESORIO/DESCRIPCIONACC)),1,20) }</ns0:Model>
	                                        <ns0:SerialNo>0</ns0:SerialNo>
	                                        <ns0:AvValue>{ data($ACCESORIO/PRECIOACC) }</ns0:AvValue>
	                                    </ns0:OAdditionalEquipment>
                           }</ns0:SupplementaryEquipment>
                        </ns0:OCar>
                    </ns0:InsuredEntity>
                    <ns0:InsuredAmount>0</ns0:InsuredAmount>
                    <ns0:Currency>ARS</ns0:Currency>
                   {
 		            	if ((not(empty($request1/PLANPOLICOD)) and not(empty($request1/FRANQCOD))) or not(matches(data($request1/DATOSPLAN), '^[0 ]*$')) or not(empty($request1/COBERTURAS/COBERTURA/COBERCOD[. != '']))) then (
 			            	let $PLANNCOD := if(not(matches(data($request1/DATOSPLAN), '^[0 ]*$'))) then xs:long(substring(data($request1/DATOSPLAN), 1, 3))
 			            	  else if (not(empty($request1/PLANPOLICOD))) then ( data($request1/PLANPOLICOD) )
 			            	  else ( '' )
 			            	let $FRANQCOD := if(not(matches(data($request1/DATOSPLAN), '^[0 ]*$'))) then xs:long(substring(data($request1/DATOSPLAN), 4, 2))
			            	  else if (not(empty($request1/FRANQCOD))) then ( data($request1/FRANQCOD) )
 			            	  else ( '' )
 			            	let $coberCods := for $a in $request1/COBERTURAS/COBERTURA/COBERCOD[. != ''] return xs:string(xs:long(data($a)))
 			            	let $packageID := $mapPlanPackage/Map[@MainPackage = 'S' and @PLANNCOD = $PLANNCOD and @FRANQCOD = $FRANQCOD]/@PackageCode
 			            	let $packageIDs :=  $packageID | $mapPlanPackage/Map[@MainPackage = 'N' and exists(index-of($coberCods, @COBERCOD))]/@PackageCode
 			            	return
 			            		
 			            		if(not(empty($packageIDs))) then
 			            	      <ns0:CoverPackages>{
                                     for $packageID in $packageIDs
                                     return
                                     <ns0:CoverPackage>
 					            	  <ns0:PackageID>{ data($packageID) }</ns0:PackageID>
 					               </ns0:CoverPackage>
 					              }</ns0:CoverPackages> 
 					            else () 
 		            	) else()
 
 		            }
                    <ns0:Drivers>
                            (: Si es persona jurídica :)
                        {   let $conductores := if ( data($request1/CLICLIENTIP) = '15' or data($request1/RAZONSOC) != '' )
                                   (: Cargo todos los conductores ya que son personas físicas :)
                              then $request1/CONDUCTORES/CONDUCTOR[concat(data(NOMBREHIJO), data(APELLIDOHIJO)) != '' or CONDUDOC != '']
                                   (: No debo enviar la persona principal como conductor ya que se crea automáticamente en INSIS :)
                              else $request1/CONDUCTORES/CONDUCTOR[(concat(data(NOMBREHIJO), data(APELLIDOHIJO)) != '' or CONDUDOC != '')
                                 and (CONDUDOC != data($request1/NUMEDOCU) or (CONDUDOC = data($request1/NUMEDOCU) and  CONDUTDO != data($request1/TIPODOCU)))]
                             let $cantDummys:=count($mockDocNums/RESULT)+1 
                             return
                            for $CONDUCTOR at $i in $conductores
                            return
                                <ns0:Driver>
                                    <ns0:Entity>
                                        <ns0:PersonalData>
                                            <ns0:Name>
                                                <ns0:Given>{ data($CONDUCTOR/NOMBREHIJO) }</ns0:Given>
                                                <ns0:Family>{ data($CONDUCTOR/APELLIDOHIJO) }</ns0:Family>
                                            </ns0:Name>
                                            <ns0:PID>{
                                            		if (data($CONDUCTOR/CONDUDOC) != '0' and data($CONDUCTOR/CONDUDOC) != '' and data($CONDUCTOR/CONDUTDO) != '' and data($CONDUCTOR/CONDUTDO) != '0') then
                                            		(
                                            			let $docType :=  xf:getInsisDocType(data($CONDUCTOR/CONDUTDO), $LOVs)
														return
														concat($docType,'-',normalize-space(data($CONDUCTOR/CONDUDOC)))
                                            		)
                                            		
                                            		else 
                                            		let $cantDummys:=$cantDummys - 1 
                                            		return 
                                            		concat('DCA-',data($mockDocNums/RESULT[$cantDummys]/NUMERO))
                                            }</ns0:PID>
                                            <ns0:Gender>{
						                		let $sexo := upper-case(data($CONDUCTOR/SEXOHIJO))
						                		return
						                			if ($sexo = 'M') then ('1')
						                			else if ($sexo = 'F') then ('2')
						                			else ('1')
						                    }</ns0:Gender>
                                            {
                                            	if (data($CONDUCTOR/NACIMHIJO) != '') then
	                                            	<ns0:BirthDate>{ fn-bea:date-from-string-with-format('dd/MM/yyyy', data($CONDUCTOR/NACIMHIJO)) }</ns0:BirthDate>
	                                            else (<ns0:BirthDate>{ fn-bea:date-from-string-with-format('dd/MM/yyyy', '01/01/1900') }</ns0:BirthDate>)
                                            }
                                        </ns0:PersonalData>
                                        <ns0:NameSuffix>{
                                            		if (data($CONDUCTOR/CONDUDOC) != '0' and data($CONDUCTOR/CONDUDOC) != '' and data($CONDUCTOR/CONDUTDO) != '' and data($CONDUCTOR/CONDUTDO) != '0') then
                                            		(
                                            			let $docType :=  xf:getInsisDocType(data($CONDUCTOR/CONDUTDO), $LOVs)
														return
														$docType
                                            		)
                                            		else 'DCA'
                                        }</ns0:NameSuffix>
                                        <ns0:DataSource>{
                                        					if (data($CONDUCTOR/ESTADOHIJO) != '') then
                                        						data($CONDUCTOR/ESTADOHIJO)
                                        					else 'N'

                                        }</ns0:DataSource>
                                    </ns0:Entity>
                                    <ns0:CustomProperties>
                                    	{
	                                    	if (exists($CONDUCTOR/CONDUVIN) and data($CONDUCTOR/CONDUVIN) != '') then
	                                    	(
		                                        <ns0:CustomProperty>
		                                            <ns0:FieldName>IOD1</ns0:FieldName>
		                                            <ns0:Value>SECONDARY</ns0:Value>
	                                        	</ns0:CustomProperty>,
	                                        	<ns0:CustomProperty>
		                                            <ns0:FieldName>IOD2</ns0:FieldName>
		                                            {
			                                            if (data($CONDUCTOR/CONDUVIN) = 'HI') then
			                                            (
			                                            	<ns0:Value>SON</ns0:Value>
			                                            ) else (
			                                            		<ns0:Value>OTHERS</ns0:Value>
			                                            		)
		                                            }
	                                        	</ns0:CustomProperty>
	                                        )else (
	                                        	 <ns0:CustomProperty>
		                                            <ns0:FieldName>IOD1</ns0:FieldName>
		                                            <ns0:Value>PRIMARY</ns0:Value>
	                                        	</ns0:CustomProperty>,
	                                        	<ns0:CustomProperty>
		                                            <ns0:FieldName>IOD2</ns0:FieldName>
		                                            <ns0:Value></ns0:Value>
	                                        	</ns0:CustomProperty>
	                                      	)
                                        }
                                        {
	                                        if(  data($CONDUCTOR/EXCLUIDO) != '') then
	                                        <ns0:CustomProperty>
	                                            <ns0:FieldName>IOD3</ns0:FieldName>
	                                            <ns0:Value>{ if (data($CONDUCTOR/EXCLUIDO) = 'N') then 'Y' else 'N' }</ns0:Value>
	                                        </ns0:CustomProperty>
	                                        else()
                                        }
                                    </ns0:CustomProperties>
                                </ns0:Driver>
                        }
                    </ns0:Drivers>
                </ns0:Insured>
            </ns0:Insureds>
            <ns0:InsuranceConditions>
                <ns0:PolicyRef>{ if(data($request1/EXPEDNUM) != '') then (: REVISAR PORQUE EXPEDNUM FIGURA COMO NO MAPEADO EN LA PLANILLA :)
                	data($request1/EXPEDNUM)
                else
                	concat(
	 					fn-bea:pad-left(data($request1/POLIZANN), 2, '0') ,
						fn-bea:pad-left(data($request1/POLIZSEC), 6, '0') ,
						fn-bea:pad-left(data($request1/CERTIPOL), 4, '0') ,
						fn-bea:pad-left(data($request1/CERTIANN), 4, '0') ,
						fn-bea:pad-left(data($request1/CERTISEC), 6, '0')
					)
                 }</ns0:PolicyRef>
                 (: *******EN EL AGENTNO SE TOMAN LA PRIMERA POSICION 1 DE AGECLA Y AGECOD PORQUE DESDE OV VIENEN DUPLICADOS, VER BUG 691******* :)
                <ns0:AgentNo>{ concat(data($request1/AGECLA[1]),data($request1/AGECOD[1])) }</ns0:AgentNo>
                <ns0:ProductCode>3005</ns0:ProductCode>
                <ns0:IssueDate>{ fn-bea:date-from-string-with-format('yyyyMMdd', concat(fn-bea:pad-left(data($request1/FESOLANN),4,'0'),fn-bea:pad-left(data($request1/FESOLMES),2,'0'),fn-bea:pad-left(data($request1/FESOLDIA),2,'0'))) }</ns0:IssueDate>
                <ns0:ConclusionDate>{ fn-bea:date-from-string-with-format('yyyyMMdd', concat(fn-bea:pad-left(data($request1/FESOLANN),4,'0'),fn-bea:pad-left(data($request1/FESOLMES),2,'0'),fn-bea:pad-left(data($request1/FESOLDIA),2,'0'))) }</ns0:ConclusionDate>
                <ns0:EffectiveDate>{ fn-bea:date-from-string-with-format('yyyyMMdd', concat(fn-bea:pad-left(data($request1/EFECTANN2),4,'0'),fn-bea:pad-left(data($request1/EFECTMES),2,'0'),fn-bea:pad-left(data($request1/EFECTDIA),2,'0'))) }</ns0:EffectiveDate>
                (: Debería ser 12 pero recibo error INSIS-806 :)
                <ns0:Installments>1</ns0:Installments>
                <ns0:PaymentType></ns0:PaymentType>
                <ns0:PaymentWay>1</ns0:PaymentWay>
                <ns0:Renewal>1</ns0:Renewal>
                <ns0:CommonConditions>
                    <ns0:Condition>
                        <ns0:Type>QUOTE_GWP</ns0:Type>
                        <ns0:Value>{ data($request1/PRECIO_MENSUAL) }</ns0:Value>
                    </ns0:Condition>
                    (: AGREGADO POR BUG 596 :)
                    {if (( exists($request1/POLIZANNREFERIDO) and data($request1/POLIZANNREFERIDO) != '0')
                			or ( exists($request1/POLIZSECREFERIDO) and data($request1/POLIZSECREFERIDO) != '0')
                			or ( exists($request1/CERTIPOLREFERIDO) and data($request1/CERTIPOLREFERIDO) != '0')
                			or ( exists($request1/CERTIANNREFERIDO) and data($request1/CERTIANNREFERIDO) != '0')
                			or ( exists($request1/CERTISECREFERIDO) and data($request1/CERTISECREFERIDO) != '0')) then
	                    <ns0:Condition>
	                        <ns0:Type>RECOM_NO</ns0:Type>
	                        <ns0:Value>{ concat(fn-bea:pad-left(data($request1/CIAASCRE), 4,' ') ,
	         					fn-bea:pad-left(data($request1/RAMOPCRE), 4,' ') ,
	         					fn-bea:pad-left(data($request1/POLIZANNREFERIDO), 2, '0') ,
								fn-bea:pad-left(data($request1/POLIZSECREFERIDO), 6, '0') ,
								fn-bea:pad-left(data($request1/CERTIPOLREFERIDO), 4, '0') ,
								fn-bea:pad-left(data($request1/CERTIANNREFERIDO), 4, '0') ,
								fn-bea:pad-left(data($request1/CERTISECREFERIDO), 6, '0')
						) }</ns0:Value>
	                	</ns0:Condition>
                    else ()
                    }
                  {
                 if (data($request1/PORTAL)='RENOVACION') then (
                   <ns0:Condition>
                        <ns0:Type>CNT_INST</ns0:Type>
                        <ns0:Value>{ data($request1/QMESPERM) }</ns0:Value>
                    </ns0:Condition>,
                    
                    <ns0:Condition>
                        <ns0:Type>CNTRENMON</ns0:Type>
                        <ns0:Value>{ data($request1/QMESRENOV) }</ns0:Value>
                    </ns0:Condition>,
                    
                     <ns0:Condition>
                        <ns0:Type>MANCOMM_PR</ns0:Type>
                        <ns0:Dimension>{ data($request1/SWTIPCOMP) }</ns0:Dimension>
                    </ns0:Condition>,
                    <ns0:Condition>
                        <ns0:Type>MANCOMM_PR</ns0:Type>
                        <ns0:Value>{ if (data($request1/QCOMIPORP) = '00000') then '' else (data($request1/QCOMIPORP)) }</ns0:Value>
                       </ns0:Condition>,
                      <ns0:Condition>
                        <ns0:Type>MANCOMM_OR</ns0:Type>
                        <ns0:Dimension>{ data($request1/SWTIPCOMO) }</ns0:Dimension>
                    </ns0:Condition>,
                     <ns0:Condition>
                        <ns0:Type>MANCOMM_OR</ns0:Type>
                        <ns0:Value>{ if (data($request1/QCOMIPORO) = '00000') then '' else (data($request1/QCOMIPORO)) }</ns0:Value>
                    </ns0:Condition>
                    )else ()
                   } 
                   { (: AGREGADO BUG 974 :)
						if (exists($request1/RNKEEPCOND) and data($request1/RNKEEPCOND) != '') then
						(
		               		<ns0:Condition>
		                        <ns0:Type>RNKEEPCOND</ns0:Type>
		                        <ns0:Value>{ data($request1/RNKEEPCOND) }</ns0:Value>
		                	</ns0:Condition>
	                	) else ()
                   }
                </ns0:CommonConditions>
                <ns0:Questionary>
                {
                	if (data($request1/PORTAL)='RENOVACION') then (
                	<ns0:Question>
                		<ns0:ID>3005.51</ns0:ID>
                        <ns0:Answer>{ data($request1/VIADEPOL) }</ns0:Answer>
                	</ns0:Question>,
                	<ns0:Question>
                		<ns0:ID>3005.52</ns0:ID>
                        <ns0:Answer>{ data($request1/VIADECUP) }</ns0:Answer>
                	</ns0:Question>,
                	<ns0:Question>
                		<ns0:ID>3005.53</ns0:ID>
                        <ns0:Answer>{ data($request1/VIADEIVA) }</ns0:Answer>
                	</ns0:Question>,
					
					
					if (data($request1/ININSCOI)!="00") then (
						<ns0:Question>
	                		<ns0:ID>3005.10</ns0:ID>
	                        <ns0:Answer>{ data($request1/ININSCOI) }</ns0:Answer>
	                	</ns0:Question>,
	                	<ns0:Question> 
	                		<ns0:ID>3005.20</ns0:ID>
	                        <ns0:Answer>{ fn-bea:date-from-string-with-format('yyyyMMdd', concat(fn-bea:pad-left(data($request1/FEINSANN),4,'0'),fn-bea:pad-left(data($request1/FEINSMES),2,'0'),fn-bea:pad-left(data($request1/FEINSDIA),2,'0'))) }</ns0:Answer>
	                	</ns0:Question>
                	) else ()
					
                	
                	) else ()
                	}
                	
                	 <ns0:Question>
                        <ns0:ID>3005.14</ns0:ID>
                        <ns0:Answer>{ data($request1/CLIENIVA) }</ns0:Answer>
                    </ns0:Question>
                    <ns0:Question>
                        <ns0:ID>3005.17</ns0:ID>
                        <ns0:Answer>{ data($request1/AUANTANN) }</ns0:Answer>
                    </ns0:Question>
                    <ns0:Question>
                        <ns0:ID>3005.23</ns0:ID>
                        <ns0:Answer>{ data($request1/USUARCOD) }</ns0:Answer>
                    </ns0:Question>
                    <ns0:Question>
                        <ns0:ID>3005.24</ns0:ID>
                        <ns0:Answer>{ data($request1/PORTAL) }</ns0:Answer>
                    </ns0:Question>
                    <ns0:Question>
                        <ns0:ID>3005.31</ns0:ID>
                        <ns0:Answer>{ if (data($request1/AUNUMSIN) = '') then '0' else data($request1/AUNUMSIN) }</ns0:Answer>
                    </ns0:Question>
                    <ns0:Question>
                        <ns0:ID>3005.33</ns0:ID>
                        <ns0:Answer>{ if (fn:not(exists($request1/SN_EPOLIZA)) or data($request1/SN_EPOLIZA) = '' or data($request1/SN_EPOLIZA) = 'N') then 2 else 1}</ns0:Answer>
                    </ns0:Question>
                    { if(exists($request1/SN_EPOLIZA) and data($request1/SN_EPOLIZA) = 'S' and data($request1/EMAIL_EPOLIZA) != '' and data($request1/EMAIL_EPOLIZA) != 'N') then
                    <ns0:Question>
                        <ns0:ID>3005.54</ns0:ID>
                        <ns0:Answer>{ data($request1/EMAIL_EPOLIZA) }</ns0:Answer>
                    </ns0:Question>
                    else () }
                </ns0:Questionary>
                <ns0:Participants>
                	<ns0:Participant>
                        <ns0:Entity>
                        {
					        if (data($request1/CLICLIENTIP) = '15' or data($request1/RAZONSOC) != '') then
							(
					            <ns0:CompanyData>
					            	<ns0:Name>{ data($request1/RAZONSOC) }</ns0:Name>
					                <ns0:CustomerID>{ if (data($estadoPersona) = 'E') then (: Si hay error se concatena "E-" bug 144 :)
					                    			 (
					                    				concat('CUIT-E-', xs:long(normalize-space(data($request1/UIFCUIT))))
					                    			 )
					                    			 else concat('CUIT-', xs:long(normalize-space(data($request1/UIFCUIT))))
					                 				}
					                 </ns0:CustomerID>
					                 <ns0:CompType>15</ns0:CompType>
					            </ns0:CompanyData>,
					            <ns0:NameSuffix>CUIT</ns0:NameSuffix>
					        ) else (
					            <ns0:PersonalData>
					                <ns0:PID>
					                		{
					                			let $docType := xf:getInsisDocType(data($request1/TIPODOCU), $LOVs)
												return
					                			if (data($estadoPersona) = 'E') then (: Si hay error se concatena "E-" bug 144 :)
					            			 	(
					            					concat($docType, '-E-', normalize-space(data($request1/NUMEDOCU)))
					                    		) else concat($docType, '-', normalize-space(data($request1/NUMEDOCU)))
					 						}
									</ns0:PID>
									<ns0:Gender>1</ns0:Gender>
					            </ns0:PersonalData>,
					            <ns0:NameSuffix>{ let $docType := xf:getInsisDocType(data($request1/TIPODOCU), $LOVs)
					            				return
					            				$docType
	            				}</ns0:NameSuffix>,
	            				<ns0:DataSource>{
							        if (not(data($request1/CLICLIENTIP) = '15' or data($request1/RAZONSOC) != '')) then
									('N')
									else ()
								}</ns0:DataSource>
				        	)
				        }
                        </ns0:Entity>
                        <ns0:ParticipantRole>PAYOR</ns0:ParticipantRole>
                        <ns0:Status>ACTIVE</ns0:Status>
                        <ns0:Language>SPANISH</ns0:Language>
                        <ns0:PolicyAddresses>
                            <ns0:PolicyAddress>
                                <ns0:Address>
                                    <ns0:AddressType>{
				                    	if ($esRenovacion = 'S') then
				                    	(	
				                    		if(data($request1/CLIDOMICCAL) != '') then xf:getInsisAddressType(data($request1/CLIDOMICCAL)) else 'H'
				                    	)
				                    	(: En caso de que la persona sea Jurídica tiene default tipo de dirección C, si es física default tipo H:) 
										else (
											if (data($request1/CLICLIENTIP) = '15' or data($request1/RAZONSOC) != '') then 
					            				('C')
				                    		else  'H'
				                    	)
											                    		
				            		}</ns0:AddressType>
                                </ns0:Address>
                                <ns0:AddressPurpose>CORRESP</ns0:AddressPurpose>
                            </ns0:PolicyAddress>
                        </ns0:PolicyAddresses>
                        {
	                        if (data($request1/CLIDOMICTLF) != '' or count($request1/MEDIOSCONTACTO/MEDIO[replace(data(MEDCODAT), '\s*\n\s*', '','m') != '']) > 0) then
	                        (
		                        <ns0:PolicyContacts>{
					            	if (data($request1/CLIDOMICTLF) != '') then
						            	<ns0:PolicyContact>
							                <ns0:Contact>
							                    <ns0:Type>ADDR</ns0:Type>
							                    <ns0:Details>{ data($request1/CLIDOMICTLF) }</ns0:Details>
							                </ns0:Contact>
							                <ns0:ContactPurpose>SALES</ns0:ContactPurpose>
							           	</ns0:PolicyContact>
					                else ()
				                }
				                {
				                	let $tipoContacto := data($request1/MEDIOSCONTACTO/MEDIO[replace(data(MEDCODAT), '\s*\n\s*', '','m') != ''][1]/MEDCOCOD)
                    				return
					                if ($tipoContacto != '') then
					                (
						                <ns0:PolicyContact>
							                <ns0:Contact>
							                    <ns0:Type>{ $tipoContacto }</ns0:Type>
							                    <ns0:Details>{ replace(data($request1/MEDIOSCONTACTO/MEDIO[replace(data(MEDCODAT), '\s*\n\s*', '','m') != ''][1]/MEDCODAT),' ','') }</ns0:Details>
							                </ns0:Contact>
							                <ns0:ContactPurpose>SALES</ns0:ContactPurpose>
						             	</ns0:PolicyContact>
						             ) else ()
				                }</ns0:PolicyContacts>
		                    ) else ()
	                    }
                    </ns0:Participant>
                {
                	if(data($request1/NUMEDOCU_ACRE) != '' and data($request1/SWACREPR) = '1') then
                	(
	                    <ns0:Participant>
	                        <ns0:Entity>
							{
								if (xf:getInsisDocType(data($request1/TIPODOCU_ACRE), $LOVs) = 'CUIT') then
								(
		                        	<ns0:CompanyData>
					                	<ns0:Name>{ data($request1/APELLIDO) }</ns0:Name>
					                    <ns0:CustomerID>{
				                    		if (data($request1/NUMEDOCU_ACRE) != '') then
                            			 		concat('CUIT-', normalize-space(data($request1/NUMEDOCU_ACRE)))
                            			 	else ()
	                     				}</ns0:CustomerID>
	                     				<ns0:CompType>15</ns0:CompType>
					                </ns0:CompanyData>,
					                <ns0:NameSuffix>CUIT</ns0:NameSuffix>,
					                <ns0:DataSource></ns0:DataSource>
				            	) else (
		                            <ns0:PersonalData>
		                                <ns0:Name>
		                                    <ns0:Given>{ data($request1/NOMBRES) }</ns0:Given>
		                                    <ns0:Family>{ normalize-space(concat(data($request1/APELLIDO),data($request1/APELLIDO2))) }</ns0:Family>
		                                </ns0:Name>
		                                <ns0:PID>{ concat(xf:getInsisDocType(data($request1/TIPODOCU_ACRE), $LOVs),'-',normalize-space(data($request1/NUMEDOCU_ACRE))) }</ns0:PID>
		                                <ns0:Gender>1</ns0:Gender>
		                                (: Birth Date de CREDITOR fijado porque en QBE no se tiene el dato y es mandatory en INSIS :)
										<ns0:BirthDate>{ fn-bea:date-from-string-with-format('yyyyMMdd', '19000102') }</ns0:BirthDate>
		                            </ns0:PersonalData>,
		                            <ns0:NameSuffix>{ data(xf:getInsisDocType(data($request1/TIPODOCU_ACRE), $LOVs)) }</ns0:NameSuffix>,
		                            <ns0:DataSource>N</ns0:DataSource>
	                           )
                           }
	                        </ns0:Entity>
	                        <ns0:Addresses>
	                            <ns0:Address>
	                                <ns0:CountryCode>AR</ns0:CountryCode> (: en el bug 107 piden que siempre se mande Argentina :)
	                                <ns0:StateRegion>{
	                                					if (data($request1/PROVICOD) != '') then
	                                					(
	                            							xs:int(data($request1/PROVICOD))
	                                					) else ()
                				 	}</ns0:StateRegion>
	                                <ns0:City>{ normalize-space(data($request1/DOMICPOB)) }</ns0:City>
	                                <ns0:CityCode>{ data($LOVs/lov:LOV[@id = "City" and @ref = data($request1/PROVICOD)][1]/ns0:ListItems/ns0:ListItem[ns0:ItemDescription = normalize-space(data($request1/DOMICPOB))]/ns0:ItemID) }</ns0:CityCode>
	                                <ns0:ResidentialAddress>
	                                    <ns0:StreetName>{ data($request1/DOMICDOM) }</ns0:StreetName>
	                                    <ns0:Floor>{ data($request1/DOMICPIS) }</ns0:Floor>
	                                    <ns0:Apartment>{ data($request1/DOMICPTA) }</ns0:Apartment>
	                                </ns0:ResidentialAddress>
	                                <ns0:ZipCode>{ data($request1/DOMICCPO) }</ns0:ZipCode>
	                                <ns0:AddressType>
	                    				{
	                    					if ($esRenovacion = 'S') then
	                    					(
		                    					if (data($request1/DOMICCAL) != '' and data($request1/DOMICCAL) != '0') then
		                    					(
		                    						xf:getInsisAddressType(data($request1/DOMICCAL))
		                    					) else 'H'
		                    				) else 'C'
	        							}
	                        		</ns0:AddressType>
	                            </ns0:Address>
	                        </ns0:Addresses>
	                        { if (exists($request1/DOMICPRE) and data($request1/DOMICPRE) != '' and exists($request1/DOMICTLF) and data($request1/DOMICTLF) != '') then
		                    (
			                    <ns0:Contacts>
			                    	<ns0:Contact>
					                    <ns0:Type>ADDR</ns0:Type>
					                    <ns0:Details>{ concat(data($request1/DOMICPRE),'-',data($request1/DOMICTLF)) }</ns0:Details>
					                </ns0:Contact>
								</ns0:Contacts>
							) else ()
							}
	                        <ns0:ParticipantRole>CREDITOR</ns0:ParticipantRole>
	                        <ns0:Status>ACTIVE</ns0:Status>
	                        <ns0:Language>SPANISH</ns0:Language>
	                    </ns0:Participant>
                    )  else()
                }
                {
                	for $apod in $request1/APOD[CLIENTIP != '']
                    return
                    (
	                    <ns0:Participant>
				            <ns0:Entity>
				                <ns0:PersonalData>
				                    <ns0:Name>
				                        <ns0:Given>{ data($apod/CLIENNOM) }</ns0:Given>
				                        <ns0:Family>{ normalize-space(concat(data($apod/CLIENAP1),data($apod/CLIENAP2))) }</ns0:Family>
				                    </ns0:Name>
				                    <ns0:PID>{ concat(xf:getInsisDocType(data($apod/TIPODOCU), $LOVs), '-', normalize-space(data($apod/NUMEDOCU))) }</ns0:PID>
				                    <ns0:Gender>{
				                		let $sexo := upper-case(data($apod/CLIENSEX))
				                		return
				                			if ($sexo = 'M') then ('1')
				                			else if ($sexo = 'F') then ('2')
				                			else ('1')
						            }</ns0:Gender>
				                    {  if( data($apod/NACIMANN) != '' and xs:long(data($apod/NACIMMES)) > 1) then
				                    	<ns0:BirthDate>{ fn-bea:date-from-string-with-format('yyyyMMdd', concat(fn-bea:pad-left(data($apod/NACIMANN),4,'0'),fn-bea:pad-left(data($apod/NACIMMES),2,'0'),fn-bea:pad-left(data($request1/NACIMDIA),2,'0'))) }</ns0:BirthDate>
				                      else ()
				                    }
				                </ns0:PersonalData>
				                <ns0:NameSuffix>{ xf:getInsisDocType(data($apod/TIPODOCU), $LOVs) }</ns0:NameSuffix>
				                <ns0:Nationality>00</ns0:Nationality> (: en el bug 107 piden que siempre se mande Argentina :)
				                <ns0:IndustryCode>{ data($apod/OCUPACOD) }</ns0:IndustryCode>
				                <ns0:DataSource>{
                					if (data($apod/CLIENEST) != '') then
                						data($apod/CLIENEST)
                					else 'N'
				                }</ns0:DataSource>
				                {
					                if (exists($apod/UIFCUIT) and data($apod/UIFCUIT) != '' and data($apod/UIFCUIT) != '0') then
					                (
						                <ns0:Documents>
						                    <ns0:Document>
						                        <ns0:DocumentID>ADD_DOC1</ns0:DocumentID>
						                        <ns0:DocumentName>CUIT</ns0:DocumentName>
						                        <ns0:DocumentSerial>CUIT</ns0:DocumentSerial>
						                        <ns0:DocumentNo>{ xs:long(normalize-space(data($apod/UIFCUIT))) }</ns0:DocumentNo>
						                    </ns0:Document>
						                </ns0:Documents>
					                ) else ()
				                }
				            </ns0:Entity>
				            <ns0:Addresses>
				                <ns0:Address>
				                    <ns0:CountryCode>AR</ns0:CountryCode> (: en el bug 107 piden que siempre se mande Argentina :)
				                    <ns0:StateRegion>{
                    					if (data($apod/PROVICOD) != '') then
                    					(
                							xs:int(data($apod/PROVICOD))
                    					) else ()
                				 	}</ns0:StateRegion>
				                    <ns0:City>{ normalize-space(data($apod/DOMICPOB)) }</ns0:City>
				                    <ns0:CityCode>{ data($LOVs/lov:LOV[@id = "City" and @ref = data($apod/PROVICOD)][1]/ns0:ListItems/ns0:ListItem[ns0:ItemDescription = normalize-space(data($apod/DOMICPOB))]/ns0:ItemID) }</ns0:CityCode>
				                    <ns0:ResidentialAddress>
				                        <ns0:StreetName>{ data($apod/DOMICDOM) }</ns0:StreetName>
				                        <ns0:StreetNo>{ data($apod/DOMICDNU) }</ns0:StreetNo>
				                        <ns0:Floor>{ data($apod/DOMICPIS) }</ns0:Floor>
				                        <ns0:Apartment>{ data($apod/DOMICPTA) }</ns0:Apartment>
				                    </ns0:ResidentialAddress>
				                    <ns0:ZipCode>{ data($apod/DOMICCPO) }</ns0:ZipCode>
				                    <ns0:AddressType>{
				                    	if ($esRenovacion = 'S') then
				                    	(
		                					if (data($apod/DOMICCAL) != '' and data($apod/DOMICCAL) != '0') then
		                					(
		                						xf:getInsisAddressType(data($apod/DOMICCAL))
		                					) else 'H'
		                				) else 'H'
                        			}</ns0:AddressType>
				                    (: <ns0:Address>{ data($apod/CPACODPO) }</ns0:Address> DON'T USED ACCORDING TO BUG 107:)
				                    (:PROBAR SI EL TELEFONO VA ASI :)
				                    <ns0:Phone>{ concat(data($apod/TELCOD),'-',data($apod/TELNRO)) }</ns0:Phone>
				                </ns0:Address>
				            </ns0:Addresses>
	                        <ns0:ParticipantRole>LEGAL</ns0:ParticipantRole>
	                        <ns0:Status>ACTIVE</ns0:Status>
	                        <ns0:Language>SPANISH</ns0:Language>
	                    </ns0:Participant>
					)
				}
				{
					for $aseg in $request1/ASEGURADOS/ASEGURADO[DOCUMDAT != '']
                    return
                    (
	                    <ns0:Participant>
				            <ns0:Entity>
					                <ns0:PersonalData>
					                    <ns0:Name>{ if (contains(data($aseg/NOMBREAS),',')) then (
						                        <ns0:Given>{ substring-after(data($aseg/NOMBREAS),',') }</ns0:Given>,
						                        <ns0:Family>{ substring-before(data($aseg/NOMBREAS),',') }</ns0:Family>
					                        ) else (
						                        <ns0:Given>{ substring-after(data($aseg/NOMBREAS),' ') }</ns0:Given>,
						                        <ns0:Family>{ substring-before(data($aseg/NOMBREAS),' ') }</ns0:Family>
						                    )
					                    }</ns0:Name>
					                    <ns0:PID>{ if (data($aseg/DOCUMTIP) != '') then
					                    			(concat(xf:getInsisDocType(data($aseg/DOCUMTIP), $LOVs),'-', replace(replace(data($aseg/DOCUMDAT),'\s+$',''),'^\s+','')))
					                    			else (concat('DNI-', replace(replace(data($aseg/DOCUMDAT),'\s+$',''),'^\s+',''))) }</ns0:PID>
					                    <ns0:Gender>1</ns0:Gender>
					                    <ns0:BirthDate>{ fn-bea:date-from-string-with-format('yyyyMMdd', '19000102') }</ns0:BirthDate>
					                </ns0:PersonalData>
					                <ns0:NameSuffix>{ if (data($aseg/DOCUMTIP) != '') then (xf:getInsisDocType(data($aseg/DOCUMTIP), $LOVs) ) else ('DNI') }</ns0:NameSuffix> (: desde OV se envia vacio , debemos enviar DNI . En AIS si lo envian con valor:)
					                <ns0:DataSource>N</ns0:DataSource>
				            </ns0:Entity>
	                        <ns0:ParticipantRole>PARTICIP</ns0:ParticipantRole>
	                        <ns0:Status>ACTIVE</ns0:Status>
	                        <ns0:Language>SPANISH</ns0:Language>
	                    </ns0:Participant>
					)
				}
                </ns0:Participants>
                <ns0:CustomProperties?>
                    {
	                    if (exists($request1/RENEWED_POLICY) and data($request1/RENEWED_POLICY) != '') then
	                	(<ns0:CustomProperty>
	                        <ns0:FieldName>Attr6</ns0:FieldName>
	                        <ns0:Value>{ data($request1/RENEWED_POLICY) }</ns0:Value>
	                    </ns0:CustomProperty>)
	                    else ()
                    }
                </ns0:CustomProperties>
            </ns0:InsuranceConditions>
            <ns0:Userexit>
                <ns0:CommercialStructure>
                    (:COMENTADO PORQUE NO VA ESTE MAPEO <ns0:PRAgentNo>{ concat(data($request1/AGECLA[1]),data($request1/AGECOD[1])) }</ns0:PRAgentNo>:)
                    <ns0:CampaignId>{	if (exists(data($request1/CAMP_CODIGO)) and data($request1/CAMP_CODIGO) != '') then
                    						xs:int(data($request1/CAMP_CODIGO))
                    					else ()
                    }</ns0:CampaignId>
                    <ns0:SellerAgentNo>{ if (exists(data($request1/LEGAJO_VEND)) and data($request1/LEGAJO_VEND) != '') then
                    						xs:int(data($request1/LEGAJO_VEND))
                    					else ()
                    }</ns0:SellerAgentNo>
                    <ns0:CloseSellerAgentNo>{ if (exists(data($request1/LEGAJO_GTE)) and data($request1/LEGAJO_GTE) != '') then
                    						xs:int(data($request1/LEGAJO_GTE))
                    					else ()
                    }</ns0:CloseSellerAgentNo>
                    <ns0:SaleChannel>{ if ((exists($request1/BANCOCOD)) and (data($request1/BANCOCOD) != '')) then
                    						(xs:int(data($request1/BANCOCOD))) else ()
                    }</ns0:SaleChannel>
                    <ns0:Branch>{ if ((exists($request1/SUCURSAL_CODIGO)) and (data($request1/SUCURSAL_CODIGO) != '')) then
                						(xs:int(data($request1/SUCURSAL_CODIGO)))
                				else ()
                	}</ns0:Branch>
                </ns0:CommercialStructure>
                <ns0:Billing>
                    <ns0:PaymentInstrument>{ data($request1/COBROTIP) }</ns0:PaymentInstrument>
                </ns0:Billing>
            </ns0:Userexit>
        </ns0:QBERegisterApplicationRq>
};

declare variable $request1 as element(Request) external;
declare variable $LOVs as element(lov:LOVs)? external;
declare variable $vehicleKey as xs:string ? external;
declare variable $mapPlanPackage as element(*) external;
declare variable $mockDocNums as element()? external;
declare variable $estadoVehiculo as element(*) external;
declare variable $estadoPersona as xs:string? external;
declare variable $esRenovacion as xs:string? external;

xf:OV_1540_To_QBERegisterApplicationRq($request1, $LOVs, $vehicleKey, $mapPlanPackage, $mockDocNums, $estadoVehiculo, $estadoPersona, $esRenovacion)
 
