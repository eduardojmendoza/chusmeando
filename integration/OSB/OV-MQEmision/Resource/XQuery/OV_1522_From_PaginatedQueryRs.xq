xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1522.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1522_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1522_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else
               	(
                    <Estado resultado = "true" mensaje = ""/>,
					<CLIENTE>
						<CLIENSEC>{ data($paginatedQueryRs1/ns0:RowSet/ns0:Row[1]/ns0:Column[@name = 'CLIENSEC']) }</CLIENSEC>
						<CLIENDES>{ data($paginatedQueryRs1/ns0:RowSet/ns0:Row[1]/ns0:Column[@name = 'CLIENDES']) }</CLIENDES>
					</CLIENTE>,
					<OPERASEC>{ data($paginatedQueryRs1/ns0:RowSet/ns0:Row[1]/ns0:Column[@name = 'OPERASEC']) }</OPERASEC>,
					<RETENCIONES>
				 	{
	            		for $row in $paginatedQueryRs1/ns0:RowSet/ns0:Row
	            		return
	            		<RETENCION>
	            			<SITUACOD>{ data($row/ns0:Column[@name = 'SITUACOD']) }</SITUACOD>
	            			<SITUASEC>{ data($row/ns0:Column[@name = 'SITUASEC']) }</SITUASEC>
	            			<SITUADES>{ data($row/ns0:Column[@name = 'SITUADES']) }</SITUADES>
	            			<FECRETEN>{ data($row/ns0:Column[@name = 'FECRETEN']) }</FECRETEN>
	            			<USUARORI>{ data($row/ns0:Column[@name = 'USUARORI']) }</USUARORI>
	            			<ORIGECOD>{ data($row/ns0:Column[@name = 'ORIGECOD']) }</ORIGECOD>
	            			<FECRESOL>{ data($row/ns0:Column[@name = 'FECRESOL']) }</FECRESOL>
	            			<USUARRES>{ data($row/ns0:Column[@name = 'USUARRES']) }</USUARRES>
	                    </RETENCION>
	                }
					</RETENCIONES>
            	)
			}
        </Response>
};

declare variable $paginatedQueryRs1 as element() external;

xf:OV_1522_From_PaginatedQueryRs($paginatedQueryRs1)
