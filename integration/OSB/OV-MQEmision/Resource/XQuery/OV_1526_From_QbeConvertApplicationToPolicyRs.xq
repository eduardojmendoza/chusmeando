(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeConvertApplicationToPolicyRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1526.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/OV_1526_From_QbeConvertApplicationToPolicyRs/";
declare variable $pTEXTO as xs:string external;
declare variable $tablaMapeos as element() external;

declare function xf:OV_1526_From_QbeConvertApplicationToPolicyRs($resp as element())
    as element(Response) {
        <Response>
            <Estado resultado = "true"
                    mensaje = ""/>
            <TEXTO>{$pTEXTO}</TEXTO>
            {
                let $policyNo := data($resp/ns0:PolicyNo)
                return
                    <POLIZA>
                        <RAMOPCOD>{ substring($policyNo, 5, 4) }</RAMOPCOD>
                        <POLIZANN>{ substring($policyNo, 9, 2) }</POLIZANN>
                        <POLIZSEC>{ substring($policyNo, 11, 6) }</POLIZSEC>
                        <CERTIPOL>{ substring($policyNo, 17, 4) }</CERTIPOL>
                        <CERTIANN>{ substring($policyNo, 21, 4) }</CERTIANN>
                        <CERTISEC>{ substring($policyNo, 25, 6) }</CERTISEC>
		                <SUPLENUM>0000</SUPLENUM>
		                <OPERAPOL>000000</OPERAPOL>
                    </POLIZA>
            }
            <ERRORES>
                <ERROR>{data($resp/ns0:CertificateType)}</ERROR>
            </ERRORES>
            <TIPOCERTI>
      		{
      		let $certi := data($tablaMapeos//mapeo[contains(./id,substring-before( data($resp/ns0:CertificateType), ":"))][1]/certificado)
      		return if (string-length($certi) > 0) then
      		 $certi
      		else
      		 '1'
      		}
			</TIPOCERTI>
            <INSPECCION>{ data($resp/ns0:Inspection) }</INSPECCION>
            <DISPOSITIVO>{ data($resp/ns0:TrackDevice) }</DISPOSITIVO>
        </Response>
};

declare variable $resp as element() external;

xf:OV_1526_From_QbeConvertApplicationToPolicyRs($resp)
