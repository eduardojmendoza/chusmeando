(:: pragma bea:mfl-element-parameter parameter="$oV_1540MQ1" type="OV_1540MQ@" location="../MFL/OV_1540MQ_Rq-ISO-8859-1.mfl" ::)
(:: pragma bea:mfl-element-parameter parameter="$isRenovacion" type="anyType"::)
(:: pragma bea:global-element-return element="Request" location="../XSD/OV_1540.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQEmision/Resource/XQuery/OV_1540MQ_To_1540/";

declare function xf:OV_1540MQ_To_1540($oV_1540MQ1 as element())
    as element(Request) {
        <Request>
            <RAMOPCOD>{ data($oV_1540MQ1/RAMOPCOD) }</RAMOPCOD>
            <USUARCOD>{ data($oV_1540MQ1/USUARCOD) }</USUARCOD>
            <POLIZANN>{ data($oV_1540MQ1/POLIZANN) }</POLIZANN>
            <POLIZSEC>{ data($oV_1540MQ1/POLIZSEC) }</POLIZSEC>
            <CERTIPOL>{ data($oV_1540MQ1/CERTIPOL) }</CERTIPOL>
            <CERTIANN>{ data($oV_1540MQ1/CERTIANN) }</CERTIANN>
            <CERTISEC>{ data($oV_1540MQ1/CERTISEC) }</CERTISEC>
            <EXPEDNUM>{ data($oV_1540MQ1/EXPEDNUM) }</EXPEDNUM>
            <EFECTANN2>{ data($oV_1540MQ1/EFECTANN2) }</EFECTANN2>
            <EFECTMES>{ data($oV_1540MQ1/EFECTMES) }</EFECTMES>
            <EFECTDIA>{ data($oV_1540MQ1/EFECTDIA) }</EFECTDIA>
            <FESOLANN>{ data($oV_1540MQ1/FESOLANN) }</FESOLANN>
            <FESOLMES>{ data($oV_1540MQ1/FESOLMES) }</FESOLMES>
            <FESOLDIA>{ data($oV_1540MQ1/FESOLDIA) }</FESOLDIA>
            <PRECIO_MENSUAL>{ data($oV_1540MQ1/PRECIO_MENSUAL) }</PRECIO_MENSUAL>
            <CAMP_CODIGO>{ data($oV_1540MQ1/CAMP_CODIGO) }</CAMP_CODIGO>
            <RECARPOR>{ data($oV_1540MQ1/RECARPOR) }</RECARPOR>
            <SN_EPOLIZA>{ data($oV_1540MQ1/SN_EPOLIZA) }</SN_EPOLIZA>
            <EMAIL_EPOLIZA>{ data($oV_1540MQ1/EMAIL_EPOLIZA) }</EMAIL_EPOLIZA>
            <AGECOD>{ data($oV_1540MQ1/AGECOD) }</AGECOD>
            <AGECLA>{ data($oV_1540MQ1/AGECLA) }</AGECLA>
            <AGECOD2>{ data($oV_1540MQ1/AGECOD2) }</AGECOD2>
            <AGECLA2>{ data($oV_1540MQ1/AGECLA2) }</AGECLA2>
            <RENEWED_POLICY>{ data($oV_1540MQ1/RENEWED_POLICY) }</RENEWED_POLICY>
            <RAZONSOC>{ data($oV_1540MQ1/RAZONSOC) }</RAZONSOC>
            <CLICLIENAP1>{ data($oV_1540MQ1/CLICLIENAP1) }</CLICLIENAP1>
            <CLICLIENAP2>{ data($oV_1540MQ1/CLICLIENAP2) }</CLICLIENAP2>
            <AISCLICLIENNOM>{ data($oV_1540MQ1/AISCLICLIENNOM) }</AISCLICLIENNOM>
            <CLIENSEC>{ data($oV_1540MQ1/CLIENSEC) }</CLIENSEC>
            <TIPODOCU>{ data($oV_1540MQ1/TIPODOCU) }</TIPODOCU>
            <NUMEDOCU>{ data($oV_1540MQ1/NUMEDOCU) }</NUMEDOCU>
            <NACIMANN>{ data($oV_1540MQ1/NACIMANN) }</NACIMANN>
            <NACIMMES>{ data($oV_1540MQ1/NACIMMES) }</NACIMMES>
            <NACIMDIA>{ data($oV_1540MQ1/NACIMDIA) }</NACIMDIA>
            <SEXO>{ data($oV_1540MQ1/SEXO) }</SEXO>
            <ESTCIV>{ data($oV_1540MQ1/ESTCIV) }</ESTCIV>
            <CLIDOMICCAL>{ data($oV_1540MQ1/CLIDOMICCAL) }</CLIDOMICCAL>
            <CLIDOMICDOM>{ data($oV_1540MQ1/CLIDOMICDOM) }</CLIDOMICDOM>
            <CLIDOMICDNU>{ data($oV_1540MQ1/CLIDOMICDNU) }</CLIDOMICDNU>
            <CLIDOMICESC>{ data($oV_1540MQ1/CLIDOMICESC) }</CLIDOMICESC>
            <CLIDOMICPIS>{ data($oV_1540MQ1/CLIDOMICPIS) }</CLIDOMICPIS>
            <CLIDOMICPTA>{ data($oV_1540MQ1/CLIDOMICPTA) }</CLIDOMICPTA>
            <CLIDOMICPOB>{ data($oV_1540MQ1/CLIDOMICPOB) }</CLIDOMICPOB>
            <CLIDOMICCPO>{ data($oV_1540MQ1/CLIDOMICCPO) }</CLIDOMICCPO>
            <CODPROV>{ data($oV_1540MQ1/CODPROV) }</CODPROV>
            <CLIDOMPAISSCOD>{ if(data($oV_1540MQ1/CLIDOMPAISSCOD) = '') 
            	then 0
            	else xs:int($oV_1540MQ1/CLIDOMPAISSCO) 
            }</CLIDOMPAISSCOD>
            <CLIDOMCPACODPO>{ data($oV_1540MQ1/CLIDOMCPACODPO) }</CLIDOMCPACODPO>
            <MEDIOSCONTACTO>{ $oV_1540MQ1/MEDIOSCONTACTO/* }</MEDIOSCONTACTO>
            <BANCOCOD>{ data($oV_1540MQ1/BANCOCOD) }</BANCOCOD>
            <SUCURSAL_CODIGO>{ data($oV_1540MQ1/SUCURSAL_CODIGO) }</SUCURSAL_CODIGO>
            <LEGAJO_VEND>{ data($oV_1540MQ1/LEGAJO_VEND) }</LEGAJO_VEND>
            <LEGAJO_GTE>{ data($oV_1540MQ1/LEGAJO_GTE) }</LEGAJO_GTE>
            <BANCOCODCUE>{ data($oV_1540MQ1/BANCOCODCUE) }</BANCOCODCUE>
            <SUCURCODCUE>{ data($oV_1540MQ1/SUCURCODCUE) }</SUCURCODCUE>
            <COBROTIP>{ data($oV_1540MQ1/COBROTIP) }</COBROTIP>
            <CUENNUME>{ data($oV_1540MQ1/CUENNUME) }</CUENNUME>
            <VENCIANN>{ data($oV_1540MQ1/VENCIANN) }</VENCIANN>
            <VENCIMES>{ data($oV_1540MQ1/VENCIMES) }</VENCIMES>
            <VENCIDIA>{ fn:day-from-dateTime(funUg:last-date-of-month-from-year-month(data($oV_1540MQ1/VENCIANN), data($oV_1540MQ1/VENCIMES))) }</VENCIDIA>
            <CLIENIVA>{ data($oV_1540MQ1/CLIENIVA) }</CLIENIVA>
            <PROFECOD>{ data($oV_1540MQ1/PROFECOD) }</PROFECOD>
            <AUMARCOD>{ data($oV_1540MQ1/AUMARCOD) }</AUMARCOD>
            <AUMODCOD>{ data($oV_1540MQ1/AUMODCOD) }</AUMODCOD>
            <AUSUBCOD>{ data($oV_1540MQ1/AUSUBCOD) }</AUSUBCOD>
            <AUADICOD>{ data($oV_1540MQ1/AUADICOD) }</AUADICOD>
            <AUMODORI>{ data($oV_1540MQ1/AUMODORI) }</AUMODORI>
            <SIFMVEHI_DES>{ data($oV_1540MQ1/SIFMVEHI_DES) }</SIFMVEHI_DES>
            <AUTIPCOD>{ data($oV_1540MQ1/AUTIPCOD) }</AUTIPCOD>
            <FABRICAN>{ data($oV_1540MQ1/FABRICAN) }</FABRICAN>
            <MOTORNUM>{ data($oV_1540MQ1/MOTORNUM) }</MOTORNUM>
            <CHASINUM>{ data($oV_1540MQ1/CHASINUM) }</CHASINUM>
            <PATENNUM>{ data($oV_1540MQ1/PATENNUM) }</PATENNUM>
            <VEHCLRCOD>{ data($oV_1540MQ1/VEHCLRCOD) }</VEHCLRCOD>
            <AUCATCOD>{ data($oV_1540MQ1/AUCATCOD) }</AUCATCOD>
            <AUUSOCOD>{ data($oV_1540MQ1/AUUSOCOD) }</AUUSOCOD>
            <AUKLMNUM>{ data($oV_1540MQ1/AUKLMNUM) }</AUKLMNUM>
            <AUPAISCOD>{ data($oV_1540MQ1/AUPAISCOD) }</AUPAISCOD>
            <AUPROVICOD>{ data($oV_1540MQ1/AUPROVICOD) }</AUPROVICOD>
            <AUDOMCPACODPO>{ data($oV_1540MQ1/AUDOMCPACODPO) }</AUDOMCPACODPO>
            <GUGARAGE>{ data($oV_1540MQ1/GUGARAGE) }</GUGARAGE>
            <NROFACTU>{ data($oV_1540MQ1/NROFACTU) }</NROFACTU>
            <AUNUMSIN>{ data($oV_1540MQ1/AUNUMSIN) }</AUNUMSIN>
            <AUNUMKMT>{ data($oV_1540MQ1/AUNUMKMT) }</AUNUMKMT>
            <ESCERO>{ data($oV_1540MQ1/ESCERO) }</ESCERO>
            <CONDUCTORES>{
            	for $conduc in $oV_1540MQ1/CONDUCTORES/CONDUCTOR[APELLIDOHIJO != '' and NOMBREHIJO != '']
            	return
            	(
	            	<CONDUCTOR>
						<CONDUTDO>{ data($conduc/CONDUTDO) }</CONDUTDO>
						<CONDUDOC>{ data($conduc/CONDUDOC) }</CONDUDOC>
						<APELLIDOHIJO>{ data($conduc/APELLIDOHIJO) }</APELLIDOHIJO>
						<NOMBREHIJO>{ data($conduc/NOMBREHIJO) }</NOMBREHIJO>
						<CONDUVIN>{ data($conduc/CONDUVIN) }</CONDUVIN>
						<NACIMHIJO>{ concat(data($conduc/NACIMHIJODIA),'/',data($conduc/NACIMHIJOMES),'/',data($conduc/NACIMHIJOANN)) }</NACIMHIJO>
						<SEXOHIJO>{ data($conduc/SEXOHIJO) }</SEXOHIJO>
						<ESTADOHIJO>{ data($conduc/ESTADOHIJO) }</ESTADOHIJO>
						<EXCLUIDO>{ data($conduc/EXCLUIDO) }</EXCLUIDO>
					</CONDUCTOR>
				) 
             }</CONDUCTORES>
            <ACCESORIOS>{ $oV_1540MQ1/ACCESORIOS/* }</ACCESORIOS>
            <ASEGURADOS>
            {
                for $ASEGURADO in $oV_1540MQ1/ASEGURADOS/ASEGURADO
                return
                    <ASEGURADO>
                    	{if( concat($ASEGURADO/APE01, $ASEGURADO/APE02, $ASEGURADO/NOM) != '') then
                        	<NOMBREAS>{ concat($ASEGURADO/APE01 , $ASEGURADO/APE02 , ',', $ASEGURADO/NOM) }</NOMBREAS>
                          else
                            <NOMBREAS></NOMBREAS>
                        }
                        <DOCUMTIP>{ data($ASEGURADO/DOCUMTIP) }</DOCUMTIP>
                        <DOCUMDAT>{ data($ASEGURADO/DOCUMDAT) }</DOCUMDAT>
                    </ASEGURADO>
            }
            </ASEGURADOS>
            <RASTREO>{ data($oV_1540MQ1/RASTREO) }</RASTREO>
            <SWACREPR>{ data($oV_1540MQ1/SWACREPR) }</SWACREPR>
            <APELLIDO>{ data($oV_1540MQ1/APELLIDO) }</APELLIDO>
            <APELLIDO2>{ data($oV_1540MQ1/APELLIDO2) }</APELLIDO2>
            <NOMBRES>{ data($oV_1540MQ1/NOMBRES) }</NOMBRES>
            <TIPODOCU_ACRE>{ data($oV_1540MQ1/TIPODOCU_ACRE) }</TIPODOCU_ACRE>
            <NUMEDOCU_ACRE>{ data($oV_1540MQ1/NUMEDOCU_ACRE) }</NUMEDOCU_ACRE>
            <DOMICCAL>{ data($oV_1540MQ1/DOMICCAL) }</DOMICCAL>
            <DOMICDOM>{ data($oV_1540MQ1/DOMICDOM) }</DOMICDOM>
            <DOMICDNU>{ data($oV_1540MQ1/DOMICDNU) }</DOMICDNU>
            <DOMICESC>{ data($oV_1540MQ1/DOMICESC) }</DOMICESC>
            <DOMICPIS>{ data($oV_1540MQ1/DOMICPIS) }</DOMICPIS>
            <DOMICPTA>{ data($oV_1540MQ1/DOMICPTA) }</DOMICPTA>
            <DOMICPOB>{ data($oV_1540MQ1/DOMICPOB) }</DOMICPOB>
            <DOMICCPO>{ data($oV_1540MQ1/DOMICCPO) }</DOMICCPO>
            <PAISSCOD>{ data($oV_1540MQ1/PAISSCOD) }</PAISSCOD>
            <PROVICOD>{ data($oV_1540MQ1/PROVICOD) }</PROVICOD>
            <DOMICPRE>{ data($oV_1540MQ1/DOMICPRE) }</DOMICPRE>
            <DOMICTLF>{ data($oV_1540MQ1/DOMICTLF) }</DOMICTLF>
            <PLANPOLICOD>{ data($oV_1540MQ1/PLANPOLICOD) }</PLANPOLICOD>
            <FRANQCOD>{ data($oV_1540MQ1/FRANQCOD) }</FRANQCOD>
            <COBERTURAS>{
                for $COBERTURA in $oV_1540MQ1/COBERTURAS/COBERTURA 
                return	<COBERTURA>
                 			<COBERCOD>{data($COBERTURA/COBERCOD)}</COBERCOD>
			                <COBERORD>{data($COBERTURA/COBERORD)}</COBERORD>
			                <CAPITASG>{data($COBERTURA/CAPITASG)}</CAPITASG>
			                <CAPITIMP>{data($COBERTURA/CAPITIMP)}</CAPITIMP>
			              
                		</COBERTURA>
           	 }</COBERTURAS>
            <APOD>{ $oV_1540MQ1/APOD/* }</APOD>
            <UIFCUIT>{ data($oV_1540MQ1/UIFCUIT) }</UIFCUIT>
            <AUANTANN>{ data($oV_1540MQ1/AUANTANN) }</AUANTANN>
            <AUUSOGNC>{ data($oV_1540MQ1/AUUSOGNC) }</AUUSOGNC>
            <DATOSPLAN>{ concat(fn-bea:pad-left(data($oV_1540MQ1/PLANPOLICOD), 3, '0'), fn-bea:pad-left(data($oV_1540MQ1/FRANQCOD), 2, '0')) }</DATOSPLAN>
            <PORTAL>{ data($oV_1540MQ1/PORTAL) }</PORTAL>
            <CIAASCRE>{ data($oV_1540MQ1/CIAASCRE) }</CIAASCRE>
            <RAMOPCRE>{ data($oV_1540MQ1/RAMOPCRE) }</RAMOPCRE>
            <POLIZANNREFERIDO>{ data($oV_1540MQ1/POLIZANNREFERIDO) }</POLIZANNREFERIDO>
            <POLIZSECREFERIDO>{ data($oV_1540MQ1/POLIZSECREFERIDO) }</POLIZSECREFERIDO>
            <CERTIPOLREFERIDO>{ data($oV_1540MQ1/CERTIPOLREFERIDO) }</CERTIPOLREFERIDO>
            <CERTIANNREFERIDO>{ data($oV_1540MQ1/CERTIANNREFERIDO) }</CERTIANNREFERIDO>
            <CERTISECREFERIDO>{ data($oV_1540MQ1/CERTISECREFERIDO) }</CERTISECREFERIDO>
           
        </Request>
};

declare variable $oV_1540MQ1 as element() external;

xf:OV_1540MQ_To_1540($oV_1540MQ1)
