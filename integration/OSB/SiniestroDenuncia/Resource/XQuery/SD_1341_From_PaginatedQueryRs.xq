xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$service1232" element="Response" location="../../../OV-mqgen/Resource/XSD/Service1232.xsd" ::)

(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/SD_1341.xsd" ::)

declare namespace xf = "http://tempuri.org/SiniestroDenuncia/Resource/XQuery/SD_1341_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:SD_1341_To_PaginatedQueryRs($paginatedQueryRs as element(),$service1232 as element())
    as element(Response) {
         <Response>
        			<Estado resultado = "{ if(number(data($paginatedQueryRs/ns0:TotalRowCount)) <= 0 ) then 'false' else 'true'}" mensaje = ""/>

           			<CAMPOS>
           			{
			        		let $item := $paginatedQueryRs/ns0:RowSet/ns0:Row[1]
                        return
                            (<CLIENNOM>{ data($item/ns0:Column[@name='CLIENNOM']) }</CLIENNOM>,
						 	<CLIENAP1>{ data($item/ns0:Column[@name = 'CLIENAP1']) }</CLIENAP1>,
							<DOCUMTIP>{ data($item/ns0:Column[@name = 'DOCUMTIP']) }</DOCUMTIP>,
							<DOCUMDAT>{ data($item/ns0:Column[@name = 'DOCUMDAT']) }</DOCUMDAT>,
							<CLIENSEX>{ data($item/ns0:Column[@name = 'CLIENSEX']) }</CLIENSEX >,
							<NACIMANN>{ data($item/ns0:Column[@name = 'NACIMANN']) }</NACIMANN >,
							<NACIMMES>{ data($item/ns0:Column[@name = 'NACIMMES']) }</NACIMMES >,
							<NACIMDIA>{ data($item/ns0:Column[@name = 'NACIMDIA']) }</NACIMDIA >,
							<DOMICDOM>{ data($item/ns0:Column[@name = 'DOMICDOM']) }</DOMICDOM >,
							<DOMICDNU>{ data($item/ns0:Column[@name = 'DOMICDNU']) }</DOMICDNU >,
							<DOMICPOB>{ data($item/ns0:Column[@name = 'DOMICPOB']) }</DOMICPOB >,
							<PROVICOD>{ data($item/ns0:Column[@name = 'PROVICOD']) }</PROVICOD >,
							<PAISSCOD>{ data($item/ns0:Column[@name = 'PAISSCOD']) }</PAISSCOD >,
							<DOMICTLF>{ data($item/ns0:Column[@name = 'DOMICTLF']) }</DOMICTLF>,
							<DOMICCEL>{ data($item/ns0:Column[@name = 'DOMICCEL']) }</DOMICCEL >,
							<CLIENEST>{ data($item/ns0:Column[@name = 'CLIENEST']) }</CLIENEST>,
							<DOMICPIS>{ data($item/ns0:Column[@name = 'DOMICPIS']) }</DOMICPIS>,
							<DOMICPTA>{ data($item/ns0:Column[@name = 'DOMICPTA']) }</DOMICPTA>,
							<PROFECOD>{ data($item/ns0:Column[@name = 'PROFECOD']) }</PROFECOD>,
							<PROFEDES>{ data($item/ns0:Column[@name = 'PROFEDES']) }</PROFEDES>,
							<AUTIPCOD>{ data($item/ns0:Column[@name = 'AUTIPCOD']) }</AUTIPCOD>,
							<AUTIPDES>{ data($item/ns0:Column[@name = 'AUTIPDES']) }</AUTIPDES>,

							<AUMARCOD>{ data($service1232/return/Response/CAMPOS/AUMARCOD)(:data($item/ns0:Column[@name = 'AUMARCOD']):) }</AUMARCOD>,
							<AUMARDES>{ data($service1232/return/Response/CAMPOS/AUMARDES)(:data($item/ns0:Column[@name = 'AUMARDES']):) }</AUMARDES>,
							<AUMODCOD>{ data($service1232/return/Response/CAMPOS/AUMODCOD)(:data($item/ns0:Column[@name = 'AUMODCOD']):) }</AUMODCOD>,
							<AUMODDES>{ data($service1232/return/Response/CAMPOS/AUMODDES)(:data($item/ns0:Column[@name = 'AUMODDES']):) }</AUMODDES>,

							<AUCOLCOD>{ data($item/ns0:Column[@name = 'AUCOLCOD']) }</AUCOLCOD>,
							<AUCOLDES>{ data($item/ns0:Column[@name = 'AUCOLDES']) }</AUCOLDES>,
							<AUUSOCOD>{ data($item/ns0:Column[@name = 'AUUSOCOD']) }</AUUSOCOD>,
							<AUUSODES>{ data($item/ns0:Column[@name = 'AUUSODES']) }</AUUSODES>,
							<FABRIANN>{ data($item/ns0:Column[@name = 'FABRIANN']) }</FABRIANN>,
							<PATENNUM>{ data($item/ns0:Column[@name = 'PATENNUM']) }</PATENNUM>,
							<MOTORNUM>{ data($item/ns0:Column[@name = 'MOTORNUM']) }</MOTORNUM>,
							<CHASINUM>{ data($item/ns0:Column[@name = 'CHASINUM']) }</CHASINUM>,
							<ITEMTOTA>{ data($item/ns0:Column[@name = 'ITEMTOTA']) }</ITEMTOTA>,
        					<REPARACIONES>
        							<REPARACION>
										<MONTOMAXIMO>{ data($item/ns0:Column[@name = 'MONTOMAXIMO1']) }</MONTOMAXIMO>
										<HABILITADO>{ local:adaptHabilitado(data($item/ns0:Column[@name = 'HABILITADO1'])) }</HABILITADO>
									</REPARACION>
									<REPARACION>
										<MONTOMAXIMO>{ data($item/ns0:Column[@name = 'MONTOMAXIMO2']) }</MONTOMAXIMO>
										<HABILITADO>{ local:adaptHabilitado(data($item/ns0:Column[@name = 'HABILITADO2'])) }</HABILITADO>
									</REPARACION>
									<REPARACION>
										<MONTOMAXIMO>{ data($item/ns0:Column[@name = 'MONTOMAXIMO3']) }</MONTOMAXIMO>
										<HABILITADO>{ local:adaptHabilitado(data($item/ns0:Column[@name = 'HABILITADO3'])) }</HABILITADO>
									</REPARACION>
					        </REPARACIONES>)
        			}
        			</CAMPOS>

        </Response>
};

declare variable $paginatedQueryRs as element() external;
declare variable $service1232 as element()? external;

declare function local:adaptHabilitado($value as xs:string?) as xs:string?
{
	if ( $value = '0' )
	then ( 'S' )
	else ( $value )
};

xf:SD_1341_To_PaginatedQueryRs($paginatedQueryRs, $service1232)
