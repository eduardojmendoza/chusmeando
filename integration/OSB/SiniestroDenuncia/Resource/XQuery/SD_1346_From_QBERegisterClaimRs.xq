(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$qbeRegisterClaimRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeRegisterClaimRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/SD_1346.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";
declare namespace xf = "http://tempuri.org/SiniestroDenuncia/Resource/XQuery/SD_1346_From_QBERegisterClaimRs/";

declare function xf:SD_1346_From_QBERegisterClaimRs($qbeRegisterClaimRs as element(),
                                              $LOVs as element(lov:LOVs))
    as element(Response) {
        <Response>
            <Estado mensaje="" resultado="true"/>
            <CAMPOS>
                <GRABAANN>{
                substring(data($LOVs/lov:LOV[@id = "ClaimSysDate"]/ns0:ListItems/ns0:ListItem/ns0:ItemID),1,4)}</GRABAANN>
                <GRABAMES>{substring(data($LOVs/lov:LOV[@id = "ClaimSysDate"]/ns0:ListItems/ns0:ListItem/ns0:ItemID),6,2)}</GRABAMES>
                <GRABADIA>{substring(data($LOVs/lov:LOV[@id = "ClaimSysDate"]/ns0:ListItems/ns0:ListItem/ns0:ItemID),9,2)}</GRABADIA>
                <GRABAHOR>{substring(data($LOVs/lov:LOV[@id = "ClaimSysDate"]/ns0:ListItems/ns0:ListItem/ns0:ItemID),12,2)}</GRABAHOR>
                <GRABAMIN>{substring(data($LOVs/lov:LOV[@id = "ClaimSysDate"]/ns0:ListItems/ns0:ListItem/ns0:ItemID),15,2)}</GRABAMIN>
                <SINIEANN>{ substring(data($qbeRegisterClaimRs/ns0:ClaimNo),9,2) }</SINIEANN>
                <SINIENUM>{ substring(data($qbeRegisterClaimRs/ns0:ClaimNo),11,6) }</SINIENUM>
            </CAMPOS>
        </Response>
};

declare variable $qbeRegisterClaimRs as element() external;
declare variable $LOVs as element(lov:LOVs) external;

xf:SD_1346_From_QBERegisterClaimRs($qbeRegisterClaimRs,$LOVs)
