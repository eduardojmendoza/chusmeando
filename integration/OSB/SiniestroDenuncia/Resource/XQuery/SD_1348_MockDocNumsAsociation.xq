xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/SD_1348.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma  parameter="$mockDocNums" type="xs:anyType" ::)
(:: pragma  type="xs:anyType" ::)

declare namespace xf = "http://tempuri.org/SiniestroDenuncia/Resource/XQuery/SD_1348_MockDocNumsAsociation/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:SD_1348_MockDocNumsAsociation($mockDocNums as element(*), 
	$request as element(Request),
	$LOVs as element(lov:LOVs))
    as element(*) {
    let $cantOtros := if( data($request/OTR_VIVDANIOS)!="" ) then 1 else 0
    let $indexVehiculos := 1 + count($request/TESTIGOS/TESTIGO[fn-bea:trim(concat(data(./DOCUMTIP),data(./DOCUMDAT))) = ''])
    let $indexLesionados := $indexVehiculos + count($request/VEHICULOS/VEHICULO[fn-bea:trim(concat(data(./DOCUMTIP),data(./DOCUMDAT))) = ''])
    let $indexOtros := $indexLesionados + count($request/LESIONADOS/LESIONADO[fn-bea:trim(concat(data(./DOCUMTIP),data(./DOCUMDAT))) = ''])
    return
    <RESULTS>
    	<TESTIGOS>{
    		xf:getDocs($request/TESTIGOS/TESTIGO, $mockDocNums/RESULT, $LOVs)
 		}</TESTIGOS>
 		<VEHICULOS>{
 			xf:getDocs($request/VEHICULOS/VEHICULO, subsequence($mockDocNums/RESULT, $indexVehiculos), $LOVs)
 		}</VEHICULOS>
 		<LESIONADOS>{
 			xf:getDocs($request/LESIONADOS/LESIONADO, subsequence($mockDocNums/RESULT,$indexLesionados), $LOVs)
 		}</LESIONADOS>
 		<OTROS>{
 			if($cantOtros > 0) then <NUMERO>{ data($mockDocNums/RESULT[$indexOtros]/NUMERO)}</NUMERO> else ()
 		}</OTROS>
 </RESULTS>
};

declare function xf:getDocs($entidades as element(*)*, $mocks as element(*)*, $LOVs)
    as element(*)* {
    let $entidad := $entidades[1]
	let $tieneDoc := fn-bea:trim(concat(data($entidad/DOCUMTIP),data($entidad/DOCUMDAT))) != ''
	let $numero := if($tieneDoc) 
		then concat(data($LOVs/lov:LOV[@id = "DocTypeNameById"  and @ref=data($entidad/DOCUMTIP)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemID), '-', data($entidad/DOCUMDAT)) 
		else concat('DCA-', data($mocks[1]/NUMERO))
    return
    	if (count($entidades)>1) then
			( <NUMERO>{$numero}</NUMERO>, xf:getDocs( subsequence($entidades,2),if($tieneDoc) then $mocks  else subsequence($mocks,2), $LOVs) )
		else
			<NUMERO>{$numero}</NUMERO>
};

declare variable $mockDocNums as element(*) external;
declare variable $request as element(Request) external;
declare variable $LOVs as element(lov:LOVs) external;

xf:SD_1348_MockDocNumsAsociation($mockDocNums, $request, $LOVs)
