xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/SD_1340.xsd" ::)

declare namespace xf = "http://tempuri.org/SiniestroDenuncia/Resource/XQuery/SD_1340_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:SD_1340_To_PaginatedQueryRs($paginatedQueryRs as element()) as element(Response) {
         <Response>
        			<Estado resultado = "{ if(number(data($paginatedQueryRs/ns0:TotalRowCount)) <= 0 ) then 'false' else 'true'}" mensaje = ""/>
           			<CAMPOS>
           			<CANT>{ data($paginatedQueryRs/ns0:TotalRowCount) }</CANT>
           			<ResultSetID>{data($paginatedQueryRs/ns0:ResultSetID)}</ResultSetID>
           			<ITEMS>
           			{
			        	for $item in $paginatedQueryRs/ns0:RowSet/ns0:Row
                        return
                            (
                            <ITEM>
	                            <RAMOPCOD>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),5,4) }</RAMOPCOD>
				                <POLIZANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,2)) }</POLIZANN>
				                <POLIZSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],11,6)) }</POLIZSEC>
				                <CERTIPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERTIPOL>
								<CERTIANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERTIANN>
								<CERTISEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERTISEC>
							 	<CLIENDES>{ data($item/ns0:Column[@name='CLIENDES']) }</CLIENDES>
							 	<TOMARIES>{ data($item/ns0:Column[@name='TOMARIES']) }</TOMARIES>
							 	<PATENNUM>{ data($item/ns0:Column[@name='PATENNUM']) }</PATENNUM>
							 	<SITUCPOL>{ data($item/ns0:Column[@name='SITUCPOL']) }</SITUCPOL>
							 	<SWDENHAB>{ data($item/ns0:Column[@name='SWDENHAB']) }</SWDENHAB>
							 	<SWSINIES>{ if (data($item/ns0:Column[@name='SWSINIES']) = '1') then 'S' else 'N' }</SWSINIES>
							 	<SWEXIGIB>{ data($item/ns0:Column[@name='SWEXIGIB']) }</SWEXIGIB>
							 	{
								 	let $ramo := data($item/ns0:Column[@name='RAMO'])
	                            	return
	                            		<RAMO>
	                            			{
	                            				if ($ramo = 'M') then (1)
	                            				else if ($ramo = 'C') then (2)
	                            				else ()
	                            			}
	                            		</RAMO>
                            	}
								<VIGDESDE>	{
						 						if (data($item/ns0:Column[@name='VIGDESDE']) != '' and data($item/ns0:Column[@name='VIGDESDE']) != '0') then
							 					(
							 						fn-bea:date-to-string-with-format("yyyyMMdd", data($item/ns0:Column[@name='VIGDESDE']))
							 					) else ()
							 				}
							 	</VIGDESDE>
								<VIGHASTA>	{
						 						if (data($item/ns0:Column[@name='VIGHASTA']) != '' and data($item/ns0:Column[@name='VIGHASTA']) != '0') then
							 					(
													fn-bea:date-to-string-with-format("yyyyMMdd", data($item/ns0:Column[@name='VIGHASTA']))
												) else ()
											}
								</VIGHASTA>
							 	<FULTSTRO>	{
							 					if (data($item/ns0:Column[@name='FULTSTRO']) != '' and data($item/ns0:Column[@name='FULTSTRO']) != '0') then
							 					(
								 					fn-bea:date-to-string-with-format("yyyyMMdd", data($item/ns0:Column[@name='FULTSTRO']))
								 				) else ()
							 			 	}
						 		</FULTSTRO>
						 	</ITEM>
  							)
        			}
        			</ITEMS>
        			</CAMPOS>

        </Response>
};

declare variable $paginatedQueryRs as element() external;

xf:SD_1340_To_PaginatedQueryRs($paginatedQueryRs)
