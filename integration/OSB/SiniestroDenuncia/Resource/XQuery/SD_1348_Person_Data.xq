xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$requestToINSIS" type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QbeRegisterClaimRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma  type="xs:anyType" ::)

declare namespace xf = "http://tempuri.org/SiniestroDenuncia/Resource/XQuery/SD_1348_Person_Data/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare namespace functx = "http://www.functx.com";

declare function xf:SD_1348_Person_Data($requestToINSIS as element())
    as element() {
    	 <Persons>
    	 	(: CLIENT, ONLY SEND CONTACTS :)
    	 	
    	 	 { (: OTHER DAMAGES - INJUREDS - OTHER VEHICLE DAMAGED :)
    	 	 for $request in $requestToINSIS/ns0:Requests/ns0:Request
    	 	 return 
    	 		(<Person>
    	 		{
    	 		   $request/ns0:Entity
    	 		}{
    	 		   $request/ns0:Addresses
    	 		}{
    	 		   $request/ns0:Contacts
    	 		}
    	 		</Person>)
    	 	
    	 	 }
    	 	 {
    	 	  for $participante in $requestToINSIS/ns0:Participants//ns0:Participant
    	 	  return
    	 	   (<Person>
    	 	  {
    	 		   $participante/ns0:Entity
    	 		}{
    	 		   $participante/ns0:Addresses
    	 		}{
    	 		   $participante/ns0:Contacts
    	 		}
    	 	   </Person>)
    	 	 }
    	 	
    	 </Persons>	
};



declare variable $requestToINSIS as element() external;


xf:SD_1348_Person_Data($requestToINSIS)
