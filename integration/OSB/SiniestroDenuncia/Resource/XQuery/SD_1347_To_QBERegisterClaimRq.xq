(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/SD_1347.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QbeRegisterClaimRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";
declare namespace xf = "http://tempuri.org/SiniestroDenuncia/Resource/XQuery/SD_1347_To_QBERegisterClaimRq/";

declare function xf:SD_1347_To_QBERegisterClaimRq($request as element(Request),
    $LOVs as element(lov:LOVs))
    as element() {
        <ns0:QbeRegisterClaimRq>
            
            {
             if (fn-bea:trim(data($request/RAMOPCOD))!="" and 
                fn-bea:trim(data($request/POLIZANN))!="" and 
             	fn-bea:trim(data($request/POLIZSEC))!="" and 
             	fn-bea:trim(data($request/CERTIPOL))!="" and 
             	fn-bea:trim(data($request/CERTIANN))!="" and 
             	fn-bea:trim(data($request/CERTISEC))!="") then (
              <ns0:PolicyNo>
                {
                    concat('0001' ,
                    fn-bea:pad-right($request/RAMOPCOD,4) ,
                    fn-bea:format-number(xs:double($request/POLIZANN), '00') ,
                    fn-bea:format-number(xs:double($request/POLIZSEC), '000000') ,
                    fn-bea:format-number(xs:double($request/CERTIPOL), '0000') ,
                    fn-bea:format-number(xs:double($request/CERTIANN), '0000') ,
                    fn-bea:format-number(xs:double($request/CERTISEC), '000000'))
                }
				</ns0:PolicyNo>
             ) else ()
            
            }
            <ns0:ClaimStarted>
                {
                    concat(data($request/DENUCANN),'-',
                    fn-bea:format-number(xs:double($request/DENUCMES), '00') ,'-',
                    fn-bea:format-number(xs:double($request/DENUCDIA), '00'))
                }
			</ns0:ClaimStarted>
			<ns0:ClaimType>CASCO</ns0:ClaimType>
            <ns0:CauseID>1</ns0:CauseID>
            <ns0:EventType>{ data($LOVs/lov:LOV[@id = "ClaimEventType"]/ns0:ListItems/ns0:ListItem/ns0:ItemID) }</ns0:EventType>
           	{
           	if (data($request/SINFEANN)!="" and data($request/SINFEMES)!="" and data($request/SINFEDIA)!="") then (
           		<ns0:EventDate>{ concat($request/SINFEANN ,'-', fn-bea:format-number(xs:double($request/SINFEMES), '00') ,'-', fn-bea:format-number(xs:double($request/SINFEDIA),'00')) }</ns0:EventDate>
	  		
           	) else ()
           	
           	}
         	{
   			if (fn-bea:trim(data($request/SINFEANN))!="" and fn-bea:trim(data($request/SINFEHOR))!="" and fn-bea:trim(data($request/SINFEMIN))!="" ) then (
   				<ns0:EventTime>{ concat(fn-bea:format-number(xs:double($request/SINFEHOR),'00') ,':', fn-bea:format-number(xs:double($request/SINFEMIN),'00'),':00') }</ns0:EventTime>
   				) else ()
   			}
   			<ns0:EventDescription>{data($request/SINIEDES_DR)}</ns0:EventDescription>
            <ns0:ClaimAddresses>
            {
            if (fn-bea:trim(data($request/SINIEDOM))!="") then ( (:es zonaUrbana:)
            	<ns0:ClaimAddress>
            			(: <ns0:CountryCode>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/SINIEPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:CountryCode>:)
                     	<ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/SINIEPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
                        <ns0:StateRegion>{ data($request/SINIEPRO) }</ns0:StateRegion>
            			<ns0:City>{ data($request/SINIEPOB) }</ns0:City>
            			{
            			  if (fn-bea:trim(data($request/SINIEDNU))!="" or (fn-bea:trim(data($request/SINIEDOM))!="") ) then (
            			  	<ns0:ResidentialAddress>
            			  		<ns0:StreetName>{ data($request/SINIEDOM) }</ns0:StreetName>
            				{
            			  	   if (fn-bea:trim(data($request/SINIEDNU))!="") then (
            					<ns0:StreetNo>{ data($request/SINIEDNU) }</ns0:StreetNo>
            					) else ()
            				}
            				</ns0:ResidentialAddress>
            			  ) else ()
            			}
            			<ns0:AddressType>EVENT</ns0:AddressType>
            		</ns0:ClaimAddress>

            ) else ( (:es zonaRural:)
		                <ns0:ClaimAddress>
		                    <ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/SINIEPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
		                    <ns0:StateRegion>{ data($request/SINIEPRO) }</ns0:StateRegion>
		                    <ns0:City>{ data($request/SINIEPOB) }</ns0:City>
		                    <ns0:ResidentialAddress>
		                        <ns0:StreetName>{ concat("Ruta ",data($request/RUTANRO)) }</ns0:StreetName>
		                        <ns0:Apartment>{ concat("KM. ",data($request/KILOMETRO)) }</ns0:Apartment>
		                    </ns0:ResidentialAddress>
		                    <ns0:AddressType>EVENT</ns0:AddressType>
		                    <ns0:Notes>{data($request/CRUCERUT)}</ns0:Notes>
		                    <ns0:TerritoryClass>{data($request/TIPORUTA)}</ns0:TerritoryClass>
		                </ns0:ClaimAddress>
            	)

            }{
            	if ((data($request/APAREPRO)!="") or (data($request/APAREPOB)!="") or (data($request/APAREDOM)!="") or
	                    (data($request/APAREDNU)!="") ) then (
					<ns0:ClaimAddress>
            		   (: <ns0:CountryCode>{data($request/APAREPAI)}</ns0:CountryCode>:)
            			<ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/APAREPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>

            			<ns0:StateRegion>{data($request/APAREPRO)}</ns0:StateRegion>
            			<ns0:City>{data($request/APAREPOB)}</ns0:City>
            			{
            			  if (fn-bea:trim(data($request/APAREDOM))!="" or (fn-bea:trim(data($request/APAREDNU))!="") ) then (
            			  <ns0:ResidentialAddress>
            			  	{
            			  	   if (fn-bea:trim(data($request/APAREDOM))!="") then (
            					<ns0:StreetName>{ data($request/APAREDOM) }</ns0:StreetName>
            					) else ()
            				}
            				{
            					if (fn-bea:trim(data($request/APAREDNU))!="") then (
            					<ns0:StreetNo>{ data($request/APAREDNU) }</ns0:StreetNo>
            					) else ()
            				}
            			  </ns0:ResidentialAddress>
            			  ) else ()
            			}
            			<ns0:AddressType>FIND</ns0:AddressType>
            		    <ns0:TerritoryClass>{data($request/INTEADOM)}</ns0:TerritoryClass>
            		</ns0:ClaimAddress>
	                    ) else ()
            		}

            		<ns0:ClaimAddress>
            		    (:<ns0:CountryCode>{data($request/INSPEPAI)}</ns0:CountryCode>:)
            			<ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/INSPEPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>

            			<ns0:StateRegion>{data($request/INSPEPRO)}</ns0:StateRegion>
            			<ns0:City>{data($request/INSPEPOB)}</ns0:City>
            			{
            			  if (fn-bea:trim(data($request/INSPEDOM))!="" or
            			  	 (fn-bea:trim(data($request/INSPEDNU))!="") or
            			  	 (fn-bea:trim(data($request/INSPEPTA))!="") or
            			  	 (fn-bea:trim(data($request/INSPEPIS))!="")) then (
            			  	<ns0:ResidentialAddress>
            			  	{
            			  	   if (fn-bea:trim(data($request/INSPEDOM))!="") then (
            					<ns0:StreetName>{ data($request/INSPEDOM) }</ns0:StreetName>
            					) else ()
            				}
            				{
            					if (fn-bea:trim(data($request/INSPEDNU))!="") then (
            					<ns0:StreetNo>{ data($request/INSPEDNU) }</ns0:StreetNo>
            					) else ()
            				}
            				{
            					if (fn-bea:trim(data($request/INSPEPTA))!="") then (
            					<ns0:Entrance>{data($request/INSPEPTA)}</ns0:Entrance>
            					) else ()
            				}
            				{
            					if (fn-bea:trim(data($request/INSPEPIS))!="") then (
            					<ns0:Apartment>{data($request/INSPEPIS)}</ns0:Apartment>
            					) else ()
            				}
            			  </ns0:ResidentialAddress>
            			  ) else ()
            			}
            			<ns0:AddressType>INSPECT</ns0:AddressType>
            			<ns0:Phone>{data($request/INSPETLF)}</ns0:Phone>
            		</ns0:ClaimAddress>
            </ns0:ClaimAddresses>

            {
            	if ((fn-bea:trim(data($request/CONDUNOM))!="")  (:or (data($request/CONDUAP1)!=""):)) then (
            	<ns0:Participants>
            	<ns0:Participant>
            		 <ns0:Entity>
                    	<ns0:PersonalData>
                    		<ns0:Name>
                    			<ns0:Given>{data($request/CONDUNOM)}</ns0:Given>
                    			<ns0:Family>{data($request/CONDUAP1)}</ns0:Family>
                    		</ns0:Name>
                    		<ns0:PID>{ concat( data($LOVs/lov:LOV[@id = "DocTypeNameById"]/ns0:ListItems/ns0:ListItem/ns0:ItemID) ,'-', $request/CONDUDAT) }</ns0:PID>
                    		 (: pasar en el LOV CONDUTPO :)
                        	<ns0:Gender>{
                                    if (upper-case(data($request/CONDUSEX)) = 'F') then
                                        ('2')
                                    else
                                        if (upper-case(data($request/CONDUSEX)) = 'M') then
                                            ('1')
                                        else
                                            if (upper-case(data($request/CONDUSEX)) = 'N') then
                                                ('0')
                                            else
                                                ('1')
                                }</ns0:Gender>
                                <ns0:BirthDate>{ if (exists($request/CONDUANN) and data($request/CONDUANN) != ''
							   						and exists($request/CONDUMES) and data($request/CONDUMES) != ''
							   						and exists($request/CONDUDIA) and data($request/CONDUDIA) != '') then 
							   						concat($request/CONDUANN ,'-', fn-bea:format-number(xs:double($request/CONDUMES), '00') ,'-', fn-bea:format-number(xs:double($request/CONDUDIA),'00'))
							   					else
							   						'1900-01-01'
								}</ns0:BirthDate>
                        </ns0:PersonalData>
                    	<ns0:IndustryCode>{data($request/CONDUPRF)}</ns0:IndustryCode>
                    	<ns0:DataSource>{data($request/CONDUEST)}</ns0:DataSource>
                    </ns0:Entity>
                    <ns0:Addresses>
                        <ns0:Address>

                     		(:<ns0:CountryCode>{data($request/CONDUPAI)}</ns0:CountryCode>:)
                     		<ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/CONDUPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
                   			<ns0:StateRegion>{data($request/CONDUPRO)}</ns0:StateRegion>
                     		<ns0:City>{data($request/CONDUPOB)}</ns0:City>
                     		<ns0:ResidentialAddress>
                                <ns0:StreetName>{ data($request/CONDUDOM) }</ns0:StreetName>
                            	<ns0:StreetNo>{ data($request/CONDUDNU) }</ns0:StreetNo>
                            	<ns0:Floor>{data($request/CONDUPIS)}</ns0:Floor>
                            	<ns0:Apartment>{data($request/CONDUPTA)}</ns0:Apartment>
                            </ns0:ResidentialAddress>
                            <ns0:AddressType>C_CLM</ns0:AddressType>
                        </ns0:Address>
                    </ns0:Addresses>
                    <ns0:Contacts>
                    {
                    	if (fn-bea:trim(data($request/CONDUTLF))!="") then
                    	(<ns0:Contact>
			                    <ns0:Type>TAC</ns0:Type>
			                    <ns0:Details>{ data($request/CONDUTLF) }</ns0:Details>
			             </ns0:Contact>
			            )else()

                    }{
                    	if (fn-bea:trim(data($request/CONDUCEL))!="") then
                    	(<ns0:Contact>
			                    <ns0:Type>TEP</ns0:Type>
			                    <ns0:Details>{ data($request/CONDUCEL) }</ns0:Details>
			             </ns0:Contact>
			            )else()

                    }</ns0:Contacts>
                    <ns0:ParticipantRole>DRIVER</ns0:ParticipantRole>
                	</ns0:Participant>
            	</ns0:Participants>
            	) else ()

            }

            <ns0:Requests>
                <ns0:Request>
                    <ns0:Entity>
                        <ns0:PersonalData>
                            <ns0:Name>
                               <ns0:Given>{ data($request/DENUNNOM) }</ns0:Given>
                               <ns0:Family>{ data($request/DENUNAP1) }</ns0:Family>
                            </ns0:Name>

                             <ns0:PID>{ concat( data($LOVs/lov:LOV[@id = "DocTypeNameById"]/ns0:ListItems/ns0:ListItem/ns0:ItemID) ,'-', $request/DENUNDAT) }</ns0:PID>

                            <ns0:Gender>
                                {
                                    if (upper-case(data($request/DENUNSEX)) = 'F') then
                                        ('2')
                                    else
                                        if (upper-case(data($request/DENUNSEX)) = 'M') then
                                            ('1')
                                        else
                                            if (upper-case(data($request/DENUNSEX)) = 'N') then
                                                ('0')
                                            else
                                                ('1')
                                }
							</ns0:Gender>

                           {
							   	<ns0:BirthDate>{ if (exists($request/DENUNANN) and data($request/DENUNANN) != ''
							   						and exists($request/DENUNMES) and data($request/DENUNMES) != ''
							   						and exists($request/DENUNDIA) and data($request/DENUNDIA) != '') then 
							   						concat($request/DENUNANN ,'-', fn-bea:format-number(xs:double($request/DENUNMES), '00') ,'-', fn-bea:format-number(xs:double($request/DENUNDIA),'00'))
							   					else
							   						'1900-01-01'
								}</ns0:BirthDate>
                           }
                       
                        </ns0:PersonalData>
                        <ns0:DataSource>{ if (exists($request/DENUNEST) and data($request/DENUNEST) != '') then data($request/DENUNEST) else 'N' }</ns0:DataSource>
                     </ns0:Entity>
                    <ns0:Addresses>
                        <ns0:Address>
                     		<ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/DENUNPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
                   			<ns0:StateRegion>{ data($request/DENUNPRO) }</ns0:StateRegion>
                            <ns0:City>{ data($request/DENUNPOB) }</ns0:City>
                     		<ns0:ResidentialAddress>
                                <ns0:StreetName>{ data($request/DENUNDOM) }</ns0:StreetName>
                                <ns0:StreetNo> {data($request/DENUNDNU)}</ns0:StreetNo>
                            	<ns0:Entrance>{ data($request/DENUNPTA) }</ns0:Entrance>
                           		<ns0:Apartment>{data($request/DENUNPIS)}</ns0:Apartment>
                            </ns0:ResidentialAddress>

                            <ns0:AddressType>C_CLM</ns0:AddressType>
                        </ns0:Address>
                    </ns0:Addresses>
                    <ns0:Contacts>
                    {
                    	if (fn-bea:trim(data($request/DENUNCEL))!="") then
                    	(<ns0:Contact>
			                    <ns0:Type>TEP</ns0:Type>
			                    <ns0:Details>{ data($request/DENUNCEL) }</ns0:Details>
			             </ns0:Contact>
			            )else()

                    }{
                    	if (fn-bea:trim(data($request/DENUNTLF))!="") then
                    	(<ns0:Contact>
			                  <ns0:Type>TAC</ns0:Type>
			                  <ns0:Details>{ data($request/DENUNTLF) }</ns0:Details>
			             </ns0:Contact>
			            )else()

                    }{
                    if (fn-bea:trim(data($request/DENUNMAIL))!="") then
                    	(<ns0:Contact>
			                  <ns0:Type>EMA</ns0:Type>
			                  <ns0:Details>{ data($request/DENUNMAIL) }</ns0:Details>
			             </ns0:Contact>
			            )else()}
                    </ns0:Contacts>
                 <ns0:ClaimantType>25</ns0:ClaimantType>
                 <ns0:InjuredObjects>
					<ns0:InjuredObject>
						<ns0:ObjectData>
							<ns0:ObjectType>111</ns0:ObjectType>
						</ns0:ObjectData>
						<ns0:InsuredData>
							<ns0:CustomOID>
								<ns0:ObjectType>111</ns0:ObjectType>
								<ns0:FilterCriteria>
									<ns0:FilterCriterion field="reg_no" operation="EQ" value="{ data($LOVs/lov:LOV[@id = "RegNoByPolicyNo"]/ns0:ListItems/ns0:ListItem/ns0:ItemID)}" />
								</ns0:FilterCriteria>
							</ns0:CustomOID>
						</ns0:InsuredData>
					</ns0:InjuredObject>
			 	 </ns0:InjuredObjects>
			 	 {
			 	 if (fn-bea:trim(data($request/MARCAAPA))!="" and (fn-bea:trim(data($request/MARCAAPA)) = "N" or fn-bea:trim(data($request/MARCAAPA)) = "S")) then(
			 	 <ns0:Questionary>
			 	 		<ns0:Question>
	                       <ns0:ID>3005.CLM.049</ns0:ID>
	                       <ns0:Answer>{fn-bea:trim(data($request/MARCAAPA))}</ns0:Answer>
	                    </ns0:Question>
	                 	<ns0:Question>
		                    <ns0:ID>3005.CLM.009</ns0:ID>
		                    <ns0:Answer>{data($request/DENUNPOL)}</ns0:Answer>
		                </ns0:Question>
		             	<ns0:Question>
		                    <ns0:ID>3005.CLM.031</ns0:ID>
		                    <ns0:Answer>{data($request/TIPO_CONDUCTOR)}</ns0:Answer>
		                </ns0:Question>
		           </ns0:Questionary>
                   ) else ()
			 	 }


			     { if ( fn-bea:trim(data($request/SINIEDES_DF))!="" ) then(
			 	 <ns0:ClaimAdditions>
	                {(:Detail of the things that were stolen:)
	                if (fn-bea:trim(data($request/SINIEDES_DF))!="") then(
		                <ns0:ClaimAddition>
		                    <ns0:ID>SINIECDE</ns0:ID>
		                    <ns0:Value>DF</ns0:Value>
		                </ns0:ClaimAddition>)else ()
		             }
		             {
	                if (fn-bea:trim(data($request/SINIEDES_DF))!="") then(
		                <ns0:ClaimAddition>
		                    <ns0:ID>SINIEDES</ns0:ID>
		                    <ns0:Value>{data($request/SINIEDES_DF)}</ns0:Value>
		                </ns0:ClaimAddition>)else ()
		             }
	              </ns0:ClaimAdditions>
	            ) else () 
	            }
               </ns0:Request>
            </ns0:Requests>
            <ns0:ClaimMatrix>
                {
                if (fn-bea:trim(data($request/CLEASOPC))!="") then(
                	<ns0:CleasClaim>{ data($request/CLEASOPC) }</ns0:CleasClaim>
                ) else ()
                }
                {
                if (fn-bea:trim(data($request/SINIECOD))!="") then (
                  <ns0:ClaimType>{ fn-bea:format-number(xs:double($request/SINIECOD), '0') }</ns0:ClaimType>
                ) else ()
                }
                <ns0:ClaimCause>{ data($request/SINIECAU) }</ns0:ClaimCause>
            	<ns0:EventType>{data($request/GESTITIP)}</ns0:EventType>
            </ns0:ClaimMatrix>
            <ns0:ClaimAdditions>
                 	{
			     		local:agregar_claim('GRABAVIA',$request/GRABSVIA)
                	}                
                 	{
			     		local:agregar_claim('SINIETPO',$request/SINIETPO)
                	}	             
                 	{
			     		local:agregar_claim('DENUCPOB',$request/DENUCPOB)
                	}	            
                 	{
			     		local:agregar_claim('DENUCPRO',$request/DENUCPRO)
                	}	             
                 	{
			     		local:agregar_claim('DENUCPAI',$request/DENUCPAI)
                	}	             
                 	{
			     		local:agregar_claim('ZONAURB',$request/ZONAURB)
                	}	             
                 	{
			     		local:agregar_claim('INTERDOM',$request/INTERDOM)
                	}	             
                 	{
			     		local:agregar_claim('KILOMETRO',$request/KILOMETRO)
                	}	             
                 	{
			     		local:agregar_claim('RUTANRO',$request/RUTANRO)
                	}	             
                 	{
			     		local:agregar_claim('TIPORUTA',$request/TIPORUTA)
                	}	             
                 	{
			     		local:agregar_claim('CRUCERUT',$request/CRUCERUT)
                	}	             
                 	{
			     		local:agregar_claim('DOMICTLF',$request/DOMICTLF)
                	}	             
                 	{
			     		local:agregar_claim('ROBOSLUG',$request/ROBOSLUG)
                	}	             
                 	{
			     		local:agregar_claim('MARCAAPA',$request/MARCAAPA)
                	}	             
                 	{
			     		local:agregar_claim('PERIODIA',$request/PERIODIA)
                	}	             
                 	{
			     		local:agregar_claim('COMISAPA',$request/COMISAPA)
                	}	             
	             	{
	                	if (fn-bea:trim(data($request/APAREANN))!="" and fn-bea:trim(data($request/APAREMES))!="" and fn-bea:trim(data($request/APAREDIA))!="" ) then (
		                <ns0:ClaimAddition>
		                    <ns0:ID>CARAPARE</ns0:ID>
		                    <ns0:Value>{ concat($request/APAREANN ,'-', fn-bea:format-number(xs:double($request/APAREMES), '00') ,'-', fn-bea:format-number(xs:double($request/APAREDIA),'00')) }</ns0:Value>
		                </ns0:ClaimAddition>)else ()
	             	}
                 	{
			     		local:agregar_claim('APARECOD',$request/APARECOD)
                	}	             
                 	{
			     		local:agregar_claim('DENUNPOL',$request/DENUNPOL)
                	}	             
                 	{
			     		local:agregar_claim('COMISPOB',$request/COMISPOB)
                	}	             
                 	{
			     		local:agregar_claim('COMISNUM',$request/COMISNUM)
                	}	             
                 	{
			     		local:agregar_claim('CONDRELA',$request/CONDRELA)
                	}	             
                 	{
			     		local:agregar_claim('CENSICOD',$request/CENSICOD)
                	}	             
                 	{
			     		local:agregar_claim('RASTRCOD',$request/RASTRCOD)
                	}	             
                 	{
			     		local:agregar_claim('SWLUGINS',$request/SWLUGINS)
                	}	             
                 	{
			     		local:agregar_claim('COAPAPOB',$request/COAPAPOB)
                	}	             
                 	{
			     		local:agregar_claim('CHASINUM',$request/CHASINUM)
                	}	             
                 	{
			     		local:agregar_claim('MOTORNUM',$request/MOTORNUM)
                	}	             
                 	{
			     		local:agregar_claim('PATENNUM',$request/PATENNUM)
                	}	             
	             	{(:'Comments about how the robbery was made':)
	                	if (fn-bea:trim(data($request/SINIEDES_DR))!="") then(
		                <ns0:ClaimAddition>
		                    <ns0:ID>SINIECDE</ns0:ID>
		                    <ns0:Value>DR</ns0:Value>
		                </ns0:ClaimAddition>)else ()
	             	}
                 	{
			     		local:agregar_claim('USUARCOD',$request/GRABAUSU)
                	}	             
                 	{
			     		local:agregar_claim('DOMICDOM',$request/DOMICDOM)
                	}	                 
                 	{
			     		local:agregar_claim('DOMICDNU',$request/DOMICDNU)
                	}	                 
                 	{
			     		local:agregar_claim('DOMICPOB',$request/DOMICPOB)
                	}	                 
                 	{
			     		local:agregar_claim('DOMICCEL',$request/DOMICCEL)
                	}	                 
                 	{
			     		local:agregar_claim('DOMICPIS',$request/DOMICPIS)
                	}
					{
				     	local:agregar_claim('DOMICPTA',$request/DOMICPTA)
	                }
                    {
                        local:agregar_claim('SINIESQL',$request/SINIESQL)
                    }
					{ 
	               	<ns0:ClaimAddition>
	                    <ns0:ID>HORADEN</ns0:ID>
	                    <ns0:Value>{fn:substring(string(fn:current-time()),1,8)  }</ns0:Value>
	                </ns0:ClaimAddition>
					 }
			</ns0:ClaimAdditions>
        </ns0:QbeRegisterClaimRq>
};

declare function local:agregar_claim($ID as xs:string,$value as element(*)?)
{
	if (exists($value) and normalize-space(data($value))!="") then
	(
                  	<ns0:ClaimAddition>
	                    <ns0:ID>{ $ID }</ns0:ID>
	                    <ns0:Value>{ normalize-space(data($value)) }</ns0:Value>
	            	</ns0:ClaimAddition>
	) else ()
};

declare variable $request as element(Request) external;
declare variable $LOVs as element(lov:LOVs) external;

xf:SD_1347_To_QBERegisterClaimRq($request,
    $LOVs)
