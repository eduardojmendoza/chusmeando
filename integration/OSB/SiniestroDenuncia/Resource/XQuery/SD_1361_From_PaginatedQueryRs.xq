xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/SD_1361.xsd" ::)

declare namespace xf = "http://tempuri.org/SiniestroDenuncia/Resource/XQuery/SD_1361_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:SD_1341_To_PaginatedQueryRs($paginatedQueryRs as element())
    as element(Response) {
         <Response>
        			<Estado resultado = "{ if(number(data($paginatedQueryRs/ns0:TotalRowCount)) <= 0 ) then 'false' else 'true'}" mensaje = ""/>
           			<CAMPOS>
           			<CANT>{data($paginatedQueryRs/ns0:TotalRowCount)}</CANT>
           			<ITEMS>
           				{
			        		for $item in $paginatedQueryRs/ns0:RowSet/ns0:Row
                        	return
                            (
                            <ITEM>
                            	<RAMOPCOD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)) }</RAMOPCOD>
								<POLIZANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,2)) }</POLIZANN>
								<POLIZSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],11,6)) }</POLIZSEC>
								<CERTIPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERTIPOL>
								<CERTIANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERTIANN>
								<CERTISEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERTISEC>
								<SUPLENUM>{ data($item/ns0:Column[@name='SUPLENUM']) }</SUPLENUM>
								<TOMARIES>{ data($item/ns0:Column[@name='TOMARIES']) }</TOMARIES>
								<PATENUM>{ data($item/ns0:Column[@name='PATENNUM']) }</PATENUM>
								<SITUCPOL>{ data($item/ns0:Column[@name='SITUCPOL']) }</SITUCPOL>
								<SWDENHAB>{ data($item/ns0:Column[@name='SWDENHAB']) }</SWDENHAB>
                             </ITEM>
                            )

        			}
           			</ITEMS>
           		</CAMPOS>

        </Response>
};

declare variable $paginatedQueryRs as element() external;

xf:SD_1341_To_PaginatedQueryRs($paginatedQueryRs)
