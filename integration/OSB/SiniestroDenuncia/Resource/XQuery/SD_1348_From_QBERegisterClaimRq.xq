xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/SD_1348.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QbeRegisterClaimRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/SiniestroDenuncia/Resource/XQuery/SD_1348_From_QBERegisterClaimRq/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare namespace functx = "http://www.functx.com";

declare function xf:SD_1348_From_QBERegisterClaimRq($request as element(Request),
    $LOVs as element(lov:LOVs),$docNums as element()?,$DummyVehiculeRegNo as element()?)
    as element() {
    	 <ns0:QbeRegisterClaimRq>
            <ns0:PolicyNo>
                {
                    concat('0001' ,
                    fn-bea:pad-right(xs:string(normalize-space($request/RAMOPCOD)), 4) ,
                    fn-bea:format-number(xs:double($request/POLIZANN), '00') ,
                    fn-bea:format-number(xs:double($request/POLIZSEC), '000000') ,
                    fn-bea:format-number(xs:double($request/CERTIPOL), '0000') ,
                    fn-bea:format-number(xs:double($request/CERTIANN), '0000') ,
                    fn-bea:format-number(xs:double($request/CERTISEC), '000000'))
                }
			</ns0:PolicyNo>

			<ns0:ClaimStarted>
                {
                    concat(data($request/DENUCANN),'-',
                    fn-bea:format-number(xs:double($request/DENUCMES), '00') ,'-',
                    fn-bea:format-number(xs:double($request/DENUCDIA), '00'))
                }
			</ns0:ClaimStarted>
			<ns0:ClaimType>CASCO</ns0:ClaimType>
            <ns0:CauseID>1</ns0:CauseID>
            <ns0:EventType>{ data($LOVs/lov:LOV[@id = "ClaimEventType"]/ns0:ListItems/ns0:ListItem/ns0:ItemID) }</ns0:EventType>
           	{
           		if (fn-bea:trim(data($request/SINFEANN))!="" and fn-bea:trim(data($request/SINFEMES))!="" and fn-bea:trim(data($request/SINFEDIA))!="") then (

           		 <ns0:EventDate>{ concat($request/SINFEANN ,'-', fn-bea:format-number(xs:double($request/SINFEMES), '00') ,'-', fn-bea:format-number(xs:double($request/SINFEDIA),'00')) }</ns0:EventDate>

           		) else ()
           	}
           	{
           	if (fn-bea:trim(data($request/SINFEHOR))!="" and fn-bea:trim(data($request/SINFEMIN))!="") then (

           	<ns0:EventTime>{ concat(fn-bea:format-number(xs:double($request/SINFEHOR),'00') ,':', fn-bea:format-number(xs:double($request/SINFEMIN),'00'),':00') }</ns0:EventTime>

           		) else ()
           	}
            <ns0:EventDescription>{normalize-space(data($request/SINIEDES_DA))}</ns0:EventDescription>
   			<ns0:ClaimAddresses>
   		    	{
   		    	if ((data($request/SINIEDNU)!="") or
	                 (data($request/SINIEDOM)!="")) then ( (:es zonaUrbana:)
	              <ns0:ClaimAddress>
            			<ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/SINIEPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
                        <ns0:CountryCode>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/SINIEPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemID) }</ns0:CountryCode>

                        <ns0:StateRegion>{ data($request/SINIEPRO) }</ns0:StateRegion>
            			<ns0:City>{ data($request/SINIEPOB) }</ns0:City>
            			{
            			  if (fn-bea:trim(data($request/SINIEDNU))!="" or (fn-bea:trim(data($request/SINIEDOM))!="") ) then (
            			 <ns0:ResidentialAddress>
            			  	{
            			  	   if (fn-bea:trim(data($request/SINIEDOM))!="") then (
            					<ns0:StreetName>{ data($request/SINIEDOM) }</ns0:StreetName>
            					) else ()
            				}
            				{
            					if (fn-bea:trim(data($request/SINIEDNU))!="") then (
            					<ns0:StreetNo>{ data($request/SINIEDNU) }</ns0:StreetNo>
            					) else ()
            				}
            		   </ns0:ResidentialAddress>
            			  ) else ()

            			  }
            		<ns0:AddressType>EVENT</ns0:AddressType>
           		   </ns0:ClaimAddress>

	                    ) else ( (:es zonaRural:)

		                <ns0:ClaimAddress>
		                    <ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/SINIEPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
		                    <ns0:CountryCode>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/SINIEPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemID) }</ns0:CountryCode>
                            <ns0:StateRegion>{ data($request/SINIEPRO) }</ns0:StateRegion>
		                    <ns0:City>{ data($request/SINIEPOB) }</ns0:City>
		                    <ns0:ResidentialAddress>
		                        <ns0:StreetName>{ concat("Ruta ",data($request/RUTANRO)) }</ns0:StreetName>
		                        <ns0:Apartment>{ concat("KM. ",data($request/KILOMETRO)) }</ns0:Apartment>
		                    </ns0:ResidentialAddress>
		                    <ns0:AddressType>EVENT</ns0:AddressType>
		                    <ns0:Notes>{data($request/CRUCERUT)}</ns0:Notes>
		                    <ns0:TerritoryClass>{data($request/TIPORUTA)}</ns0:TerritoryClass>

		                </ns0:ClaimAddress>
	                  )
   		    	}
   		    	{
					if ((data($request/INSPEPRO)!="") or
						(data($request/INSPEPOB)!="") or
						(data($request/INSPEDOM)!="") or
	                    (data($request/INSPEDNU)!="") or
	                    (data($request/INSPEPTA)!="") or
	                    (data($request/INSPEPIS)!="")) then (
	              <ns0:ClaimAddress>
					<ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/INSPEPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
                    <ns0:CountryCode>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/INSPEPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemID) }</ns0:CountryCode>

                    <ns0:StateRegion>{data($request/INSPEPRO)}</ns0:StateRegion>
                    <ns0:City>{data($request/INSPEPOB)}</ns0:City>
                    {
                     			if ((data($request/INSPEDOM)!="") or
                     				(data($request/INSPEDNU)!="") or
                     				 (data($request/INSPEPTA)!="") or
                     				 (data($request/INSPEPIS)!=""))
                     				 then (
                     			<ns0:ResidentialAddress>
                     				{ if (data($request/INSPEDOM)!="") then (
                     				<ns0:StreetName>{ data($request/INSPEDOM) }</ns0:StreetName>)
                     				else ()
                     				}{ if (data($request/INSPEDNU)!="") then (
                     				<ns0:StreetNo>{ data($request/INSPEDNU) }</ns0:StreetNo>)
                     				else ()
                     				}
                     				{ if (data($request/INSPEPIS)!="") then (
                     				<ns0:Floor>{ data($request/INSPEPIS) }</ns0:Floor>)
                     				else ()
                     				}
                     				{ if (data($request/INSPEPTA)!="") then (
                     				<ns0:Apartment>{ data($request/INSPEPTA) }</ns0:Apartment>)
                     				else ()
                     				}
                     			 </ns0:ResidentialAddress>
                     			)else ()
                     		}
                    <ns0:AddressType>INSPECT</ns0:AddressType>
                    <ns0:Phone>{data($request/INSPETLF)}</ns0:Phone>
				</ns0:ClaimAddress>
	                 ) else ()
				}
			</ns0:ClaimAddresses>
			{
			if ( count($request/TESTIGOS/TESTIGO)>0) then 
				<ns0:Participants>

             { (: witness TESTIGO:)

                for $testigo at $t in $request/TESTIGOS/TESTIGO
                        return
                <ns0:Participant>
                    <ns0:Entity>
                    	<ns0:PersonalData>
                    		<ns0:Name>
                    			<ns0:Given>{ if (exists($testigo/CLIENNOM) and data($testigo/CLIENNOM) != '') then
                    							data($testigo/CLIENNOM)
                    						else 'UNKNOWNWITNESS'
                    			}</ns0:Given>(:ok:)
                    			<ns0:Family>{ if (exists($testigo/CLIENAP1) and data($testigo/CLIENAP1) != '') then
                    							data($testigo/CLIENAP1)
                    						else 'UNKNOWNWITNESS'
                    			}</ns0:Family>
                    		</ns0:Name>
                    		<ns0:PID>{ data($docNums/TESTIGOS/NUMERO[$t]) }</ns0:PID>
							(: 01/01/1900 to witness birth date, because we don’t enter this data in virtual office.:)
                    	    <ns0:BirthDate>{'1900-01-01'}</ns0:BirthDate>

                    	</ns0:PersonalData>
                    	<ns0:DataSource>{'N'}</ns0:DataSource>
                    	<ns0:Questionary>
                             <ns0:Question>
                                   <ns0:ID>3005.CLM.021</ns0:ID>
                                   <ns0:Answer>TES</ns0:Answer>
                                </ns0:Question>
                        </ns0:Questionary>
                     </ns0:Entity>
                     {
                     if (data($testigo/PROVICOD) or data($testigo/DOMICDOM)!="") then (
                    <ns0:Addresses>
                        <ns0:Address>
                     		(:pasar a Country mediante LOV:)
                     		<ns0:CountryCode>{data($testigo/PAISSCOD)}</ns0:CountryCode>
                     		<ns0:StateRegion>{data($testigo/PROVICOD)}</ns0:StateRegion>
                     		<ns0:City>{data($testigo/DOMICPOB)}</ns0:City>
                     		 {
                     			if ((data($testigo/DOMICDOM)!="") or
                     				(data($testigo/DOMICDNU)!="") or
                     				 (data($testigo/DOMICPTA)!="") or
                     				 (data($testigo/DOMICPIS)!=""))
                     				 then (
                     			<ns0:ResidentialAddress>
                     				{ if (data($testigo/DOMICDOM)!="") then (
                     				<ns0:StreetName>{ data($testigo/DOMICDOM) }</ns0:StreetName>)
                     				else ()
                     				}{ if (data($testigo/DOMICDNU)!="") then (
                     				<ns0:StreetNo>{ data($testigo/DOMICDNU) }</ns0:StreetNo>)
                     				else ()
                     				}
                     				{ if (data($testigo/DOMICPIS)!="") then (
                     				<ns0:Floor>{ data($testigo/DOMICPIS) }</ns0:Floor>)
                     				else ()
                     				}
                     				{ if (data($testigo/DOMICPTA)!="") then (
                     				<ns0:Apartment>{ data($testigo/DOMICPTA) }</ns0:Apartment>)
                     				else ()
                     				}
                     			 </ns0:ResidentialAddress>
                     			)else ()
                     		}
                            <ns0:AddressType>C_CLM</ns0:AddressType>
                        </ns0:Address>
                    </ns0:Addresses>
                     ) else ()

                     }


	                {
	                	if ((fn-bea:trim(data($testigo/DOMICCEL))!="") or (fn-bea:trim(data($testigo/DOMICTLF))!="")) then (
	                	  <ns0:Contacts?>
		                    {
		                    	if (fn-bea:trim(data($testigo/DOMICTLF))!="") then
		                    	(<ns0:Contact>
					                    <ns0:Type>TAC</ns0:Type>
					                    <ns0:Details>{ data($testigo/DOMICTLF) }</ns0:Details>
					             </ns0:Contact>
					            )else()

		                    }{
		                    	if (fn-bea:trim(data($testigo/DOMICCEL))!="") then
		                    	(<ns0:Contact>
					                    <ns0:Type>TEP</ns0:Type>
					                    <ns0:Details>{ data($testigo/DOMICCEL) }</ns0:Details>
					             </ns0:Contact>
					            )else()
		                    }
                   		 </ns0:Contacts>
	                	) else ()
	                }

                    <ns0:ParticipantRole>TES</ns0:ParticipantRole>
                </ns0:Participant>
                }
            </ns0:Participants>
			else ()

			}
			 <ns0:Requests>
            		<ns0:Request> <!--Request Client Mandatory -->
            			<ns0:Entity>
					        	<ns0:PersonalData>
					        			<ns0:Name>
					        				<ns0:Given>{data($request/CLIENNOM)}</ns0:Given>
					        			    <ns0:Family>{data($request/CLIENAP1)}</ns0:Family>
					        			 </ns0:Name>
					          	<ns0:PID>{ concat(data($LOVs/lov:LOV[@id = "DocTypeNameById"  and @ref=data($request/DOCUMTPO)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemID), '-', data($request/DOCUMDAT)) }</ns0:PID>
    						  	
    						  	<ns0:Gender>{ Genero (data($request/CLIENSEX)) }</ns0:Gender>
									{
									if (fn-bea:trim(data($request/NACIMANN))!="" and fn-bea:trim(data($request/NACIMMES))!="" and fn-bea:trim(data($request/NACIMDIA))!="") then (
										 <ns0:BirthDate>{ concat($request/NACIMANN ,'-', fn-bea:format-number(xs:double($request/NACIMMES), '00') ,'-', fn-bea:format-number(xs:double($request/NACIMDIA),'00')) }</ns0:BirthDate>
		       						) else ()

									}
	                  		</ns0:PersonalData>
		       					<ns0:DataSource>{data($request/CLIENEST)}</ns0:DataSource>

					     </ns0:Entity>
                     <ns0:ClaimantType>11</ns0:ClaimantType>
	            	<ns0:InjuredObjects>
						<ns0:InjuredObject>
							<ns0:ObjectData>
								<ns0:ObjectType>111</ns0:ObjectType>
							</ns0:ObjectData>
							<ns0:InsuredData>
								<ns0:CustomOID>
									<ns0:ObjectType>111</ns0:ObjectType>
									<ns0:FilterCriteria>
										<ns0:FilterCriterion field="reg_no" operation="EQ" value="{ data($LOVs/lov:LOV[@id = "RegNoByPolicyNo"]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID)}" />
									</ns0:FilterCriteria>
								</ns0:CustomOID>
							</ns0:InsuredData>
						</ns0:InjuredObject>
					</ns0:InjuredObjects>

						{
							if (fn-bea:trim(data($request/CONDUNOM))!="" or fn-bea:trim(data($request/CONDUDOM))!="" or
								fn-bea:trim(data($request/CONDUCEL))!="" or (fn-bea:trim(data($request/CONDUTLF))!="")) then (
							<ns0:Participants>
								   		<ns0:Participant>
				                		{
											if (data($request/CONDUNOM)!="" or data($request/CONDUAP1)!="" or
											     data($request/CONDUDAT)!="" or data($request/CONDUSEX)!="" or
											     ((data($request/CONDUANN)!="" and data($request/CONDUMES)!="" and data($request/CONDUDIA)!="")) or
											      data($request/CONDUPRF)!="" or data($request/CONDUEST)!="")  then (
											<ns0:Entity>
				                    	<ns0:PersonalData>
				                    		<ns0:Name>
				                    			<ns0:Given>{data($request/CONDUNOM)}</ns0:Given>
				                    			<ns0:Family>{data($request/CONDUAP1)}</ns0:Family>
				                    		</ns0:Name>
				                    		<ns0:PID>{ concat(data($LOVs/lov:LOV[@id = "DocTypeNameById"  and @ref=data($request/CONDUTPO)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemID), '-', data($request/CONDUDAT)) }</ns0:PID>
				                    		<ns0:Gender>{ Genero(data($request/CONDUSEX)) }</ns0:Gender>

				                             {
												if (fn-bea:trim(data($request/CONDUANN))!="" and fn-bea:trim(data($request/CONDUMES))!="" and fn-bea:trim(data($request/CONDUDIA))!="") then (
													 <ns0:BirthDate>{ concat($request/CONDUANN ,'-', fn-bea:format-number(xs:double($request/CONDUMES), '00') ,'-', fn-bea:format-number(xs:double($request/CONDUDIA),'00')) }</ns0:BirthDate>
					       						) else (
      												 <ns0:BirthDate>{'1900-01-01'}</ns0:BirthDate>

					       						)

											}
									    </ns0:PersonalData>
				                     	<ns0:IndustryCode>{data($request/CONDUPRF)}</ns0:IndustryCode>

				                     	{
				                     	  if (fn-bea:trim(data($request/CONDUEST))!="") then (
				                     		<ns0:DataSource>{data($request/CONDUEST)}</ns0:DataSource>
											) else (
											<ns0:DataSource>{'N'}</ns0:DataSource>
											)
				                     	}
				                     	{
							                if ((fn-bea:trim(data($request/CONDVANN))!="") and (fn-bea:trim(data($request/CONDVMES))!="") and (fn-bea:trim(data($request/CONDVDIA))!="")) then(
								           	<ns0:Questionary>
								           		<ns0:Question>
								                    <ns0:ID>3005.CLM.050</ns0:ID>
								                    <ns0:Answer>{ fn-bea:date-from-string-with-format("yyyyMMdd", concat(fn-bea:trim(data($request/CONDVANN)),fn-bea:trim(data($request/CONDVMES)),fn-bea:trim(data($request/CONDVDIA)))) }</ns0:Answer>
							                    </ns0:Question>
							                </ns0:Questionary>
							                )else ()
						                 }
					                </ns0:Entity>
									) else ()
								     }
				                    {
					                	if ((data($request/CONDUPRO)!="") or
					                		(data($request/CONDUPOB)!="") or
					                		(data($request/CONDUDOM)!="") or
				                     		(data($request/CONDUDNU)!="") or
				                     		(data($request/CONDUPTA)!="") or
				                     		(data($request/CONDUPIS)!="")) then (
					                <ns0:Addresses>
				                        <ns0:Address>
				                     		<ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/CONDUPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
				                   			<ns0:StateRegion>{data($request/CONDUPRO)}</ns0:StateRegion>
				                     		<ns0:City>{data($request/CONDUPOB)}</ns0:City>
				                     		 {
				                     			if ((data($request/CONDUDOM)!="") or
				                     				(data($request/CONDUDNU)!="") or
				                     				 (data($request/CONDUPTA)!="") or
				                     				 (data($request/CONDUPIS)!=""))
				                     				 then (
				                     			<ns0:ResidentialAddress>
				                     				{ if (data($request/CONDUDOM)!="") then (
				                     				<ns0:StreetName>{ data($request/CONDUDOM) }</ns0:StreetName>)
				                     				else ()
				                     				}{ if (data($request/CONDUDNU)!="") then (
				                     				<ns0:StreetNo>{ data($request/CONDUDNU) }</ns0:StreetNo>)
				                     				else ()
				                     				}
				                     				{ if (data($request/CONDUPIS)!="") then (
				                     				<ns0:Floor>{ data($request/CONDUPIS) }</ns0:Floor>)
				                     				else ()
				                     				}
				                     				{ if (data($request/CONDUPTA)!="") then (
				                     				<ns0:Apartment>{ data($request/CONDUPTA) }</ns0:Apartment>)
				                     				else ()
				                     				}
				                     			 </ns0:ResidentialAddress>
				                     			)else ()
				                     		}
				                            <ns0:AddressType>C_CLM</ns0:AddressType>
				                        </ns0:Address>
				                    </ns0:Addresses>

					                	) else ()

					                }
					                {
					                 if ((fn-bea:trim(data($request/CONDUTLF))!="") or (fn-bea:trim(data($request/CONDUCEL))!="") ) then (

					                <ns0:Contacts?>
				                    {
				                    	if (fn-bea:trim(data($request/CONDUTLF))!="") then
				                    	(<ns0:Contact>
							                    <ns0:Type>TAC</ns0:Type>
							                    <ns0:Details>{ data($request/CONDUTLF) }</ns0:Details>
							             </ns0:Contact>
							            )else()

				                    }{
				                    	if (fn-bea:trim(data($request/CONDUCEL))!="") then
				                    	(<ns0:Contact>
							                    <ns0:Type>TEP</ns0:Type>
							                    <ns0:Details>{ data($request/CONDUCEL) }</ns0:Details>
							             </ns0:Contact>
							            )else()

				                    }</ns0:Contacts>

					                 )else ()

					                }
					                <ns0:ParticipantRole>DRIVER</ns0:ParticipantRole>
				                </ns0:Participant>
						</ns0:Participants>
							) else ()
						}
						{
 								if (normalize-space(data($request/SINIEDES_DV))!="") then(
 								<ns0:ClaimAdditions?>
					                <ns0:ClaimAddition>
					                    <ns0:ID>SINIECDE</ns0:ID>
					                    <ns0:Value>DV</ns0:Value>
					                </ns0:ClaimAddition>
					                 <ns0:ClaimAddition>
					                    <ns0:ID>SINIEDES</ns0:ID>
					                    <ns0:Value>{ normalize-space(data($request/SINIEDES_DV)) }</ns0:Value>
					                </ns0:ClaimAddition>
					             </ns0:ClaimAdditions>
					                )else ()
					             }
	 			</ns0:Request>
				<!-- OTHER VEHICLE DAMAGED -->
				{
				if (count($request/VEHICULOS/VEHICULO)>0) then 
				(
				for $vehiculo at $v in $request/VEHICULOS/VEHICULO
                return
				<ns0:Request>
                    <ns0:Entity>
                        <ns0:PersonalData>
	                          <ns0:Name>
	                          	<ns0:Given>{  NombreDemandanteDesconocido(data($vehiculo/CLIENNOM)) }</ns0:Given>
	                             <ns0:Family>{ data($vehiculo/CLIENAP1) }</ns0:Family>
	                         </ns0:Name>
	                         <ns0:PID>{  data($docNums/VEHICULOS/NUMERO[$v]) }</ns0:PID>
							<ns0:Gender>{  Genero (data($vehiculo/CLIENSEX))}</ns0:Gender>

				             {
								if (fn-bea:trim(data($vehiculo/NACIMANN))!="" and fn-bea:trim(data($vehiculo/NACIMMES))!="" and fn-bea:trim(data($vehiculo/NACIMDIA))!="") then (

					       			<ns0:BirthDate>{ concat($vehiculo/NACIMANN ,'-', fn-bea:format-number(xs:double($vehiculo/NACIMMES), '00') ,'-', fn-bea:format-number(xs:double($vehiculo/NACIMDIA),'00')) }</ns0:BirthDate>

					       		) else (
					       			<ns0:BirthDate>{'1900-01-01'}</ns0:BirthDate>
       							)

							}


       					 </ns0:PersonalData>
       			   	{
                    		if (fn-bea:trim(data($vehiculo/CLIENEST))!="") then (
                    		<ns0:DataSource>{ data($vehiculo/CLIENEST) }</ns0:DataSource>

                    		) else (
                    	<ns0:DataSource>{ 'N'}</ns0:DataSource>

                    		)
                    	}
                    </ns0:Entity>
                    {
                    	if ((data($vehiculo/PROVICOD)!="") or
                    		(data($vehiculo/DOMICPOB)!="") or
                    		(data($vehiculo/DOMICDOM)!="") or
                     				(data($vehiculo/DOMICDNU)!="") or
                     				 (data($vehiculo/DOMICPTA)!="") or
                     				 (data($vehiculo/DOMICPIS)!=""))
                     				 then (
                     <ns0:Addresses>
                        <ns0:Address>
                     		<ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($vehiculo/PAISSCOD)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
                   			<ns0:StateRegion>{ data($vehiculo/PROVICOD) }</ns0:StateRegion>
                            <ns0:City>{ data($vehiculo/DOMICPOB) }</ns0:City>
                     		{if ((data($vehiculo/DOMICDOM)!="") or
                     				(data($vehiculo/DOMICDNU)!="") or
                     				 (data($vehiculo/DOMICPTA)!="") or
                     				 (data($vehiculo/DOMICPIS)!=""))
                     				 then (
                     			<ns0:ResidentialAddress>
                     				{ if (data($vehiculo/DOMICDOM)!="") then (
                     				<ns0:StreetName>{ data($vehiculo/DOMICDOM) }</ns0:StreetName>)
                     				else ()
                     				}{ if (data($vehiculo/DOMICDNU)!="") then (
                     				<ns0:StreetNo>{ data($vehiculo/DOMICDNU) }</ns0:StreetNo>)
                     				else ()
                     				}
                     				{ if (data($vehiculo/DOMICPTA)!="") then (
                     				<ns0:Entrance>{ data($vehiculo/DOMICPTA) }</ns0:Entrance>)
                     				else ()
                     				}
                     				{ if (data($vehiculo/DOMICPIS)!="") then (
                     				<ns0:Apartment>{ data($vehiculo/DOMICPIS) }</ns0:Apartment>)
                     				else ()
                     				}
                     			 </ns0:ResidentialAddress>
                     			)else (<ns0:ResidentialAddress>
                     						<ns0:StreetNo>{ 0}</ns0:StreetNo>
                     				   </ns0:ResidentialAddress>) (:TODO :ver estoo:)
                     		}
                     		<ns0:ZipCode>{ fn:substring-after(data($docNums/VEHICULOS/NUMERO[$v]),'-') }</ns0:ZipCode>
                     		<ns0:AddressType>C_CLM</ns0:AddressType>
                        </ns0:Address>
                    </ns0:Addresses>
                    	) else ()
                    }

                  {
                  	if ((fn-bea:trim(data($vehiculo/DOMICTLF))!="")or (fn-bea:trim(data($vehiculo/DOMICCEL))!="")) then (
                  			<ns0:Contacts?>
                    {
                    	if (fn-bea:trim(data($vehiculo/DOMICTLF))!="") then
                    	(<ns0:Contact>
			                  <ns0:Type>TAC</ns0:Type>
			                  <ns0:Details>{ data($vehiculo/DOMICTLF) }</ns0:Details>
			             </ns0:Contact>
			            )else()

                		}{
                    	if (fn-bea:trim(data($vehiculo/DOMICCEL))!="") then
                    	(<ns0:Contact>
			                    <ns0:Type>TEP</ns0:Type>
			                    <ns0:Details>{ data($vehiculo/DOMICCEL) }</ns0:Details>
			             </ns0:Contact>
			            )else()

                    }
			        </ns0:Contacts>
                  	) else ()

                  }
					(: OTHER VEHICLE DAMAGED :)
			        <ns0:ClaimantType>25</ns0:ClaimantType>
			        <ns0:InjuredObjects>
						<ns0:InjuredObject>
			        			<ns0:ObjectData>
					        			  <ns0:ObjectType>111</ns0:ObjectType>
					        			   <ns0:OCar>
													{
					        						if (data($vehiculo/PATENNUM)!="") then (
														<ns0:RegNo>{data($vehiculo/PATENNUM)}</ns0:RegNo>
													) else (
													    <ns0:RegNo>{data($DummyVehiculeRegNo/DummyRegNo[@regID=$vehiculo/ID])}</ns0:RegNo>
					        						)
					        					}
					        					{
					        						if (data($vehiculo/AUTIPCOD)!="") then (
					        						<ns0:CarType>{ data($LOVs/lov:LOV[@id = "LkpVehicleType" and @ref=data($vehiculo/AUTIPCOD)][1]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID) }</ns0:CarType>
					        						) else (
					        						<ns0:CarType>3051</ns0:CarType>)
					        					}
												{
					        						if (data($vehiculo/AUMODCOD)!="") then (
					        						<ns0:Model>{data($vehiculo/AUMODCOD)}</ns0:Model>
					        						) else (<ns0:Model>'GENERICO***NO USAR***'</ns0:Model>)
					        					}{
					        						if (data($vehiculo/AUMARCOD)!="") then (
					        						<ns0:Make>{data($vehiculo/AUMARCOD)}</ns0:Make>
					        						) else (
					        						<ns0:Make>'GENERICO***NO USAR***'</ns0:Make>)
					        					}{
					        						if (data($vehiculo/CHASINUM)!="") then (
					        						<ns0:Chassis>{data($vehiculo/CHASINUM)}</ns0:Chassis>
					        						) else (
							        						if (data($vehiculo/PATENNUM)!="") then (
																<ns0:Chassis>{data($vehiculo/PATENNUM)}</ns0:Chassis>
															) else (
															   <ns0:Chassis>{data($DummyVehiculeRegNo/DummyRegNo[@regID=$vehiculo/ID])}</ns0:Chassis>
							        						)
					        						)
					        					}{
					        						if (data($vehiculo/MOTORNUM)!="") then (
					        						<ns0:Engine>{data($vehiculo/MOTORNUM)}</ns0:Engine>
					        						) else (<ns0:Engine>*</ns0:Engine>)
					        			 		}{
					        			 			if (data($vehiculo/AUUSOCOD)!="") then (
					        						<ns0:CarUsage>{data($vehiculo/AUUSOCOD)}</ns0:CarUsage>
					        						) else ()
					        			 		}{
					        						<ns0:PaintType>{data($LOVs/lov:LOV[@id = "PaintType"][1]/ns0:ListItems/ns0:ListItem/ns0:ItemID)}</ns0:PaintType>
					        					}
					        			     	{
					        			     		if (data($vehiculo/FABRIANN)!="") then (
					        			     			<ns0:ProdYear>{data($vehiculo/FABRIANN)}</ns0:ProdYear>
					        			  			) else ()
					        			  		}
					        			  		<ns0:ProdType>{data($LOVs/lov:LOV[@id = "ProdType"][1]/ns0:ListItems/ns0:ListItem/ns0:ItemID)}</ns0:ProdType>
					        			  		<ns0:CarColor>{data($vehiculo/AUCOLCOD)}</ns0:CarColor>
					        			  		<ns0:Modified>0</ns0:Modified>(:- alwatys 0 - INSIS internal:)
					        			  		<ns0:CustomProperties>
						        			  		<ns0:CustomProperty>
					                                    <ns0:FieldName>OCP8</ns0:FieldName>
					                                    <!-- Car Region -->
					                                    <ns0:Value>1</ns0:Value>   <!-- 1 is for CAPITAL FEDERAL-->
					                                 </ns0:CustomProperty>
						                                <ns0:CustomProperty>
						                                    <ns0:FieldName>OCP5</ns0:FieldName>
						                                    <ns0:Value>1500</ns0:Value> <!-- possible post codes according supplied region, can be found using LOV LkpRegionZone         -->
						                             	 </ns0:CustomProperty>
												</ns0:CustomProperties>
					        			  	</ns0:OCar>
					        		</ns0:ObjectData>
					        		<ns0:Driver>
				        				<ns0:Entity>
					        					<ns0:PersonalData>
					        					    <ns0:Name>
					        					    	<ns0:Given>{NombreDemandanteDesconocido(data($vehiculo/CLIENNOM))}</ns0:Given>
					        					    	<ns0:Family>{data($vehiculo/CLIENAP1)}</ns0:Family>
					        					    </ns0:Name>
					        						<ns0:PID>{  data($docNums/VEHICULOS/NUMERO[$v]) }</ns0:PID>
													<ns0:Gender> {  Genero (data($vehiculo/CLIENSEX)) }</ns0:Gender>

		       										{
													if (fn-bea:trim(data($vehiculo/NACIMANN))!="" and fn-bea:trim(data($vehiculo/NACIMMES))!="" and fn-bea:trim(data($vehiculo/NACIMDIA))!="") then (
												      	<ns0:BirthDate>{ concat($vehiculo/NACIMANN ,'-', fn-bea:format-number(xs:double($vehiculo/NACIMMES), '00') ,'-', fn-bea:format-number(xs:double($vehiculo/NACIMDIA),'00')) }</ns0:BirthDate>
		       									      		) else (
		       									      <ns0:BirthDate>{'1900-01-01'}</ns0:BirthDate>
		       									   			)
													}

		       									</ns0:PersonalData>

		       									{
		       										if (fn-bea:trim(data($vehiculo/CLIENEST))!="") then (
		       										<ns0:DataSource>{data($vehiculo/CLIENEST)}</ns0:DataSource>

		       										) else (
		       										<ns0:DataSource>{'N'}</ns0:DataSource>)

		       									}
					        			</ns0:Entity>
					        			{
					                    	if (
					                    		(data($vehiculo/PROVICOD)!="") or
					                    		(data($vehiculo/DOMICPOB)!="") or
					                     				(data($vehiculo/DOMICDOM)!="") or
					                     				(data($vehiculo/DOMICDNU)!="") or
					                     				 (data($vehiculo/DOMICPTA)!="") or
					                     				 (data($vehiculo/DOMICPIS)!=""))
					                     				 then (
					                    <ns0:Addresses>
					                        <ns0:Address>
					                     		<ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($vehiculo/PAISSCOD)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
					                   			<ns0:StateRegion>{ data($vehiculo/PROVICOD) }</ns0:StateRegion>
					                            <ns0:City>{ data($vehiculo/DOMICPOB) }</ns0:City>
					                     		{
					                     			if ((data($vehiculo/DOMICDOM)!="") or
					                     				(data($vehiculo/DOMICDNU)!="") or
					                     				 (data($vehiculo/DOMICPTA)!="") or
					                     				 (data($vehiculo/DOMICPIS)!=""))
					                     				 then (
					                     			<ns0:ResidentialAddress>
					                     				{ if (data($vehiculo/DOMICDOM)!="") then (
					                     				<ns0:StreetName>{ data($vehiculo/DOMICDOM) }</ns0:StreetName>)
					                     				else ()
					                     				}{ if (data($vehiculo/DOMICDNU)!="") then (
					                     				<ns0:StreetNo>{ data($vehiculo/DOMICDNU) }</ns0:StreetNo>)
					                     				else ()
					                     				}
					                     				{ if (data($vehiculo/DOMICPTA)!="") then (
					                     				<ns0:Entrance>{ data($vehiculo/DOMICPTA) }</ns0:Entrance>)
					                     				else ()
					                     				}
					                     				{ if (data($vehiculo/DOMICPIS)!="") then (
					                     				<ns0:Apartment>{ data($vehiculo/DOMICPIS) }</ns0:Apartment>)
					                     				else ()
					                     				}
					                     			 </ns0:ResidentialAddress>
					                     			)else ()
					                     		}
					                     		<ns0:ZipCode>{ fn:substring-after(data($docNums/VEHICULOS/NUMERO[$v]),'-') }</ns0:ZipCode>
										         <ns0:AddressType>C_CLM</ns0:AddressType>
					                        </ns0:Address>
					                    </ns0:Addresses>
					                    ) else ()
					                    }
					        			{
					        			if ((fn-bea:trim(data($vehiculo/DOMICCEL))!="") or (fn-bea:trim(data($vehiculo/DOMICTLF))!="")) then (

					        			 <ns0:Contacts?>
						                    {
						                    	if (fn-bea:trim(data($vehiculo/DOMICCEL))!="") then
						                    	(<ns0:Contact>
									                    <ns0:Type>TEP</ns0:Type>
									                    <ns0:Details>{ data($vehiculo/DOMICCEL) }</ns0:Details>
									             </ns0:Contact>
									            )else()

						                	}{
						                    	if (fn-bea:trim(data($vehiculo/DOMICTLF))!="") then
						                    	(<ns0:Contact>
									                    <ns0:Type>TAC</ns0:Type>
									                    <ns0:Details>{ data($vehiculo/DOMICTLF) }</ns0:Details>
									             </ns0:Contact>
									            )else()

						                    }
									       </ns0:Contacts>
					        			 ) else ()
					                    }

					        			<ns0:LicenseID>{data($vehiculo/CONDLICN)}</ns0:LicenseID>
					        		</ns0:Driver>
			        	</ns0:InjuredObject>


            		</ns0:InjuredObjects>
            		{

	                   if (data($vehiculo/COMPANIA)!="" or data($vehiculo/POLIZA)!="") then (
	                   	let $compania := data($vehiculo/COMPANIA)
						return
	                   <ns0:Participants>
	                   		<ns0:Participant>
	                   			<ns0:Entity>
	                   				<ns0:CompanyData>
	                   					<ns0:Name>{ data($LOVs/lov:LOV[@id = "GetInsurerData" and @ref=$compania][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Name>
	                   					<ns0:CustomerID>{ data($LOVs/lov:LOV[@id = "GetInsurerData" and @ref=$compania][1]/ns0:ListItems/ns0:ListItem/ns0:ItemID) }</ns0:CustomerID>
	                   				</ns0:CompanyData>
	                   			</ns0:Entity>
	                   			<ns0:ParticipantRole>INSURER</ns0:ParticipantRole>
	                   			<ns0:ReportNo>{data($vehiculo/POLIZA)}</ns0:ReportNo>
	                   		</ns0:Participant>


	                   </ns0:Participants>

	                    ) else ()

                   }
                  	<ns0:Questionary>
                            {
                            if (data($vehiculo/CONDALCO)!="") then (
                         	<ns0:Question>
                               <ns0:ID>3005.CLM.CONDALCO</ns0:ID>
                               <ns0:Answer>{data($vehiculo/CONDALCO)}</ns0:Answer>
                        	</ns0:Question>
                            ) else ()
                            }
                            {
                            if (data($vehiculo/CONDRELA)!="") then (
                         	<ns0:Question>
                               <ns0:ID>3005.CLM.CONDRELA</ns0:ID>
                               <ns0:Answer>{data($vehiculo/CONDRELA)}</ns0:Answer>
                        	</ns0:Question>
                            ) else ()
                            }

                        	<ns0:Question>
                               <ns0:ID>3005.CLM.009</ns0:ID>
                               <ns0:Answer>{data($request/DENUNPOL)}</ns0:Answer>
                        	</ns0:Question>
                        	<ns0:Question>
                               <ns0:ID>3005.CLM.031</ns0:ID>
                               <ns0:Answer>{data($request/TIPO_CONDUCTOR)}</ns0:Answer>
                        	</ns0:Question>
                        </ns0:Questionary>

                     	
                     	{
 							if (normalize-space(data($vehiculo/SINIEDES))!="") then
 							(
	 							<ns0:ClaimAdditions?>
				                	<ns0:ClaimAddition>
				                    	<ns0:ID>SINIECDE</ns0:ID>
				                    	<ns0:Value>{ normalize-space(data($vehiculo/SINIECDE)) }</ns0:Value>
					               	</ns0:ClaimAddition>
				                	<ns0:ClaimAddition>
				                    	<ns0:ID>SINIEDES</ns0:ID>
				                    	<ns0:Value>{ normalize-space(data($vehiculo/SINIEDES)) }</ns0:Value>
					                </ns0:ClaimAddition>
			                	</ns0:ClaimAdditions>
		                	) else ()
			            }
						
				</ns0:Request>
				) else ()
				}
				<!-- INJURED -->
				{
					if (count($request/LESIONADOS/LESIONADO)>0) then (
						for $lesionado at $l in $request/LESIONADOS/LESIONADO
                        return
				<ns0:Request>
					<ns0:Entity>
					        <ns0:PersonalData>
					        		<ns0:Name>
					        			<ns0:Given>{NombreLesionadoDesconocido(data($lesionado/CLIENNOM))}</ns0:Given>
					        			<ns0:Family>{data($lesionado/CLIENAP1)}</ns0:Family>
					        		</ns0:Name>
					        		<ns0:PID>{  data($docNums/LESIONADOS/NUMERO[$l]) }</ns0:PID>
	                           		<ns0:Gender>{ Genero (data($lesionado/CLIENSEX))}</ns0:Gender>
	                           		{
									if (fn-bea:trim(data($lesionado/NACIMANN))!="" and fn-bea:trim(data($lesionado/NACIMMES))!="" and fn-bea:trim(data($lesionado/NACIMDIA))!="") then (
											<ns0:BirthDate>{ concat($lesionado/NACIMANN ,'-', fn-bea:format-number(xs:double($lesionado/NACIMMES), '00') ,'-', fn-bea:format-number(xs:double($lesionado/NACIMDIA),'00')) }</ns0:BirthDate>
	                            	) else (
	                            			<ns0:BirthDate>{'1900-01-01' }</ns0:BirthDate>
	                           		)
									}
	                           </ns0:PersonalData>

	                           		{
	                           			if (fn-bea:trim(data($lesionado/CLIENEST))!="") then (
											<ns0:DataSource>{data($lesionado/CLIENEST)}</ns0:DataSource>
	                            		) else (
	                            			<ns0:DataSource>{'N'}</ns0:DataSource>
	                           			)
	                           		}

					     </ns0:Entity>
                    <ns0:ClaimantType>25</ns0:ClaimantType>
			 		<ns0:InjuredObjects>
						<ns0:InjuredObject>
                   	 			 	<ns0:ObjectData>
					        			<ns0:ObjectType>991</ns0:ObjectType>
									    <ns0:OAccidentInsured>
									    	<ns0:Entity>
					        					<ns0:PersonalData>
					        						<ns0:Name>
					        							<ns0:Given>{NombreLesionadoDesconocido(data($lesionado/CLIENNOM))}</ns0:Given>
					        							<ns0:Family>{data($lesionado/CLIENAP1)}</ns0:Family>
					        						</ns0:Name>
					        						<ns0:PID>{  data($docNums/LESIONADOS/NUMERO[$l]) }</ns0:PID>
	                           						<ns0:Gender>
						                                { Genero (data($lesionado/CLIENSEX))   }</ns0:Gender>
	                           						{
														if (fn-bea:trim(data($lesionado/NACIMANN))!="" and fn-bea:trim(data($lesionado/NACIMMES))!="" and fn-bea:trim(data($lesionado/NACIMDIA))!="") then (
															<ns0:BirthDate>{ concat($lesionado/NACIMANN ,'-', fn-bea:format-number(xs:double($lesionado/NACIMMES), '00') ,'-', fn-bea:format-number(xs:double($lesionado/NACIMDIA),'00')) }</ns0:BirthDate>
				                            				) else (
				                            					<ns0:BirthDate>{'1900-01-01'}</ns0:BirthDate>
				                            				)
													}
	                           					</ns0:PersonalData>

	                           					{
	                           						if (fn-bea:trim(data($lesionado/CLIENEST))!="") then (
	                           						<ns0:DataSource>{data($lesionado/CLIENEST)}</ns0:DataSource>

	                           						) else (
	                           							<ns0:DataSource>{'N'}</ns0:DataSource>
					        							)
	                           					}

		       								</ns0:Entity>
					        				{
					        				if (data($lesionado/DOMICDOM)!="" or data($lesionado/PROVICOD)!="" ) then (
					        					<ns0:Addresses>
					                        <ns0:Address>
					                     		(:TODO@ cargar el pais con PAISSCOD:)
					                     		<ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($lesionado/PAISSCOD)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
					                   			<ns0:StateRegion>{ data($lesionado/PROVICOD) }</ns0:StateRegion>
					                            <ns0:City>{ data($lesionado/DOMICPOB) }</ns0:City>
					                     		{
					                     			if ((data($lesionado/DOMICDOM)!="") or
					                     				(data($lesionado/DOMICDNU)!="") or
					                     				 (data($lesionado/DOMICPTA)!="") or
					                     				 (data($lesionado/DOMICPIS)!=""))
					                     				 then (
					                     			<ns0:ResidentialAddress>
					                     				{ if (data($lesionado/DOMICDOM)!="") then (
					                     				<ns0:StreetName>{ data($lesionado/DOMICDOM) }</ns0:StreetName>)
					                     				else ()
					                     				}{ if (data($lesionado/DOMICDNU)!="") then (
					                     				<ns0:StreetNo>{ data($lesionado/DOMICDNU) }</ns0:StreetNo>)
					                     				else ()
					                     				}
					                     				{ if (data($lesionado/DOMICPTA)!="") then (
					                     				<ns0:Entrance>{ data($lesionado/DOMICPTA) }</ns0:Entrance>)
					                     				else ()
					                     				}
					                     				{ if (data($lesionado/DOMICPIS)!="") then (
					                     				<ns0:Apartment>{ data($lesionado/DOMICPIS) }</ns0:Apartment>)
					                     				else ()
					                     				}
					                     			 </ns0:ResidentialAddress>
					                     			)else ()
					                     		}
										         <ns0:AddressType>C_CLM</ns0:AddressType>
					                        </ns0:Address>
					                    </ns0:Addresses>
					        				) else ()

					        				}

					                    {
						                    if ((fn-bea:trim(data($lesionado/DOMICCEL))!="") or (fn-bea:trim(data($lesionado/DOMICTLF))!="")) then (
						                     <ns0:Contacts?>
						                   		 {
						                    	if (fn-bea:trim(data($lesionado/DOMICCEL))!="") then
						                    	(<ns0:Contact>
									                    <ns0:Type>TEP</ns0:Type>
									                    <ns0:Details>{ data($lesionado/DOMICCEL) }</ns0:Details>
									             </ns0:Contact>
									            )else()

						                	}{
						                    	if (fn-bea:trim(data($lesionado/DOMICTLF))!="") then
						                    	(<ns0:Contact>
									                    <ns0:Type>TAC</ns0:Type>
									                    <ns0:Details>{ data($lesionado/DOMICTLF) }</ns0:Details>
									             </ns0:Contact>
									            )else()

						                    }
						                   	</ns0:Contacts>
						                   		) else ()
						                }
						                </ns0:OAccidentInsured>
								     </ns0:ObjectData>
				        	</ns0:InjuredObject>

					</ns0:InjuredObjects>

					<ns0:Questionary>

	                        	(:
	                        	 3005.CLM.021 - Lesionado participa como
							       Possible values:
							       1    Pedestrian
							       2    Other driver
							       3    Other passanger
	                        	:)
	                        	<ns0:Question>
	                               <ns0:ID>3005.CLM.021</ns0:ID>
	                               <ns0:Answer>{data($lesionado/PARTICOD)}</ns0:Answer>
	                        	</ns0:Question>
	                        	(:
	                        	     3005.CLM.023 - Estado
								       Possible values:
								       1    Death
								       2    Alive
								       3    Moving
								       4    Hospital
								       5	To be confirmed
							    :)
	                        	<ns0:Question>
	                               <ns0:ID>3005.CLM.023</ns0:ID>
	                               <ns0:Answer>5</ns0:Answer>
	                        	</ns0:Question>
	                        	{
	                        		if (data($lesionado/CENTASIS) != '') then
	                        		(
			                        	<ns0:Question>
			                               <ns0:ID>3005.CLM.CENTASIS</ns0:ID>
			                               <ns0:Answer>{data($lesionado/CENTASIS)}</ns0:Answer>
			                        	</ns0:Question>
	                        		) else ()
	                        	}
                        </ns0:Questionary>

                </ns0:Request>
					) else ()
				}
			   <!-- OTHER DAMAGES -->
				{
				 if ((exists($request/OTR_VIVIENDA) and data($request/OTR_VIVIENDA)!="NO") or (exists($request/OTR_OTROS) and data($request/OTR_OTROS)!="NO")) then 
				 	(
						<ns0:Request>
								<ns0:Entity>
									<ns0:PersonalData>
					 				 	<ns0:Name>
				 							<ns0:Given>{ NombreDemandanteDesconocido(data($request/OTR_CLIENNOM)) }</ns0:Given>	
				 							<ns0:Family>{ NombreDemandanteDesconocido(data($request/OTR_CLIENAP1)) } </ns0:Family>
					 				 	</ns0:Name>
										<ns0:PID>{ concat('DCA-',data($docNums/OTROS/NUMERO[1])) }</ns0:PID>
										<ns0:BirthDate>{'1900-01-01'}</ns0:BirthDate>
									</ns0:PersonalData>
									<ns0:DataSource>N</ns0:DataSource>
								</ns0:Entity>
								<ns0:ClaimantType>25</ns0:ClaimantType>
							<ns0:InjuredObjects>
								<ns0:InjuredObject>
									 	<ns0:ObjectData>
									 		<ns0:ObjectType>114</ns0:ObjectType>
									 		<ns0:Owners>
									 			<ns0:Owner>
									 				<ns0:Entity>
									 					<ns0:PersonalData>
									 						<ns0:Name>
									 							<ns0:Given>{ NombreDemandanteDesconocido(data($request/OTR_CLIENNOM)) }</ns0:Given>	
									 							<ns0:Family>{ NombreDemandanteDesconocido(data($request/OTR_CLIENAP1)) } </ns0:Family>
									 						</ns0:Name>
									 						<ns0:PID>{ concat('DCA-',data($docNums/OTROS/NUMERO[1])) }</ns0:PID>
									 						<ns0:BirthDate>{'1900-01-01'}</ns0:BirthDate>
									 					</ns0:PersonalData>
									 					<ns0:DataSource>N</ns0:DataSource>
									 				</ns0:Entity>
									 			{
							                    if ((fn-bea:trim(data($request/OTR_DOMICCEL))!="") or (fn-bea:trim(data($request/OTR_DOMICTLF))!="")) then (
								                     <ns0:Contacts?>
								                   		 {
								                    	if (fn-bea:trim(data($request/OTR_DOMICCEL))!="") then
								                    	(<ns0:Contact>
											                    <ns0:Type>TEP</ns0:Type>
											                    <ns0:Details>{ data($request/OTR_DOMICCEL) }</ns0:Details>
											             </ns0:Contact>
											            )else()
		
								                	}{
								                    	if (fn-bea:trim(data($request/OTR_DOMICTLF))!="") then
								                    	(<ns0:Contact>
											                    <ns0:Type>TAC</ns0:Type>
											                    <ns0:Details>{ data($request/OTR_DOMICTLF) }</ns0:Details>
											             </ns0:Contact>
											            )else()
		
								                    }
								                   	</ns0:Contacts>
								                   		) else ()
								                }
								                <ns0:BeginDate>{'1900-01-01'}</ns0:BeginDate>
									 			</ns0:Owner>
									 		</ns0:Owners>
									 		<ns0:OProperty>
									 					<ns0:PropertyKind>114</ns0:PropertyKind>
									 					<ns0:PropertyType> (:Flag that indicates if there were damages to Building/Apartment:)
									 					 {
									 					 	if (normalize-space(data($request/OTR_VIVIENDA))="SI" and normalize-space(data($request/OTR_OTROS))="NO") then (
									 					 	1141
									 					 	) else if (normalize-space(data($request/OTR_OTROS))="SI" and normalize-space(data($request/OTR_VIVIENDA))="NO") then (
									 					 	  1143
									 					 	  ) else ()
									 					 }
									 					</ns0:PropertyType>
									 					<ns0:Name>{ NombreDemandanteDesconocido(data($request/OTR_CLIENNOM))}</ns0:Name>
									 					{
									 					if (data($request/OTR_DOMICDOM)!='') then (
									 					
									 					<ns0:NewAddress>
									 						<ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/OTR_PAISSCOD)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
						                   				    <ns0:StateRegion>{data($request/OTR_PROVICOD)}</ns0:StateRegion>
									 						<ns0:City>{data($request/OTR_DOMICPOB)}</ns0:City>
									 						<ns0:ResidentialAddress>
									 							<ns0:StreetName>{data($request/OTR_DOMICDOM) }</ns0:StreetName>
									 							<ns0:StreetNo>{ data($request/OTR_DOMICDNU)}</ns0:StreetNo>
									 						</ns0:ResidentialAddress>
									 						<ns0:ZipCode>{ data($docNums/OTROS/NUMERO[1]) }</ns0:ZipCode>
									 					</ns0:NewAddress>
									 					
									 					) else ( (: sino viene cargado el address usar el cargado en el eventAddress :)
									 					
									 					<ns0:NewAddress>
									 						<ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/SINIEPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
						                   				    <ns0:StateRegion>{data($request/SINIEPRO)}</ns0:StateRegion>
									 						<ns0:City>{ data($request/SINIEPOB) }</ns0:City>
												   		   {
												   		    if ((data($request/SINIEDNU)!="") or (data($request/SINIEDOM)!="")) then ( (:es zonaUrbana:)
									 						<ns0:ResidentialAddress>
									 							<ns0:StreetName>{ data($request/SINIEDOM) }</ns0:StreetName>
									 							<ns0:StreetNo>{ data($request/SINIEDNU) }</ns0:StreetNo>
									 						</ns0:ResidentialAddress>
									 					
									 						) else (  											(:es zonaRural:)
										 						<ns0:ResidentialAddress>
										 							<ns0:StreetName>{ concat("Ruta ",data($request/RUTANRO)) }</ns0:StreetName>
										 							<ns0:StreetNo>{ concat("KM. ",data($request/KILOMETRO)) }</ns0:StreetNo>
										 						</ns0:ResidentialAddress>
										 					 )
										 					 
									 					}
									 					<ns0:ZipCode>{data($docNums/OTROS/NUMERO[1]) }</ns0:ZipCode>
									 					
									 					</ns0:NewAddress> )
									 					}
									 		</ns0:OProperty>
									 	</ns0:ObjectData>
					        		</ns0:InjuredObject>
							</ns0:InjuredObjects>
							{
							  	if (fn-bea:trim(data($request/SINIEDES_DO))!="") then(
									<ns0:ClaimAdditions?>
							                <ns0:ClaimAddition>
							                    <ns0:ID>SINIECDE</ns0:ID>
							                    <ns0:Value>DO</ns0:Value>
							                </ns0:ClaimAddition>
							                 <ns0:ClaimAddition>
						                    <ns0:ID>SINIEDES</ns0:ID>
						                    	<ns0:Value>{normalize-space(data($request/SINIEDES_DO))}</ns0:Value>
						                	</ns0:ClaimAddition>
						               </ns0:ClaimAdditions>  
						                )else ()
						      }
						</ns0:Request>
				 	) else ()
				} 
			</ns0:Requests>

			<ns0:ClaimMatrix>
	            <ns0:DriverResponsibility>{normalize-space(upper-case(data($request/RESPCOND)))}</ns0:DriverResponsibility>
	            <ns0:ThirdPartiesDamages>{normalize-space(upper-case(data($request/VTERCERO)))}</ns0:ThirdPartiesDamages>
                <ns0:ThirdPartiesInjuries>{normalize-space(upper-case(data($request/LTERCERO)))}</ns0:ThirdPartiesInjuries>
                 {
                if (fn-bea:trim(data($request/CLEASOPC))!="") then(
                	<ns0:CleasClaim>{ data($request/CLEASOPC) }</ns0:CleasClaim>
                ) else ()
                }
                <ns0:ClaimType>{ fn-bea:format-number(xs:double($request/SINIECOD), '0') }</ns0:ClaimType>
                <ns0:ClaimCause>{ data($request/SINIECAU) }</ns0:ClaimCause>
				<ns0:EventType>{data($request/GESTITIP)}</ns0:EventType>
            </ns0:ClaimMatrix>
			<ns0:ClaimAdditions?>
					{
						local:agregar_claim('ASISTENCIA',$request/ASISTENCIA)
	             	}
	             	{
	             		local:agregar_claim('BARRERA',$request/BARRERA)
	             	}
	             	{
	             		local:agregar_claim('CAMINO',$request/CAMINO)
	             	}
	             	{
	             		local:agregar_claim('CENSICOD',$request/CENSICOD)
	             	}
	            	{ (: inyecto los ClaimAddition de colisiones:)
                		CodigosDeColisiones(data($request/COLISION))
	             	}
	             	{ 
 						local:agregar_claim('COMISPOB',$request/COMISPOB)
	             	}
	             	{
	             		local:agregar_claim('COMISNUM',$request/COMISNUM)
	             	}
	             	{
	             		local:agregar_claim('CONDLICE',$request/CONDLICE)
	             	}
	             	{
	             		local:agregar_claim('CONDLICN',$request/CONDLICN)
	             	}
	             	{
	             		local:agregar_claim('CONDRELA',$request/CONDRELA)
	             	}
	             	{
	             		local:agregar_claim('CONDUSUA',$request/CONDUSUA)
	             	}
	             	{
	             		local:agregar_claim('CRUCESEN',$request/CRUCESEN)
	             	}
	             	{
	             		local:agregar_claim('CRUCETREN',$request/CRUCETREN)
	            	}
	            	{
	                 	if (fn-bea:trim(data($request/DENUCPAI))!="") then(
			                <ns0:ClaimAddition>
			                    <ns0:ID>DENUCPAI</ns0:ID>
			                    <ns0:Value>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/DENUCPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Value>
			                </ns0:ClaimAddition>
		                )else ()
	             	}
	             	{
	             		local:agregar_claim('DENUCPOB',$request/DENUCPOB)
	             	}
	             	{
		             	local:agregar_claim('DENUCPRO',$request/DENUCPRO)
	             	}
	             	{
	             		local:agregar_claim('DENUNPOL',$request/DENUNPOL)
	            	}
	            	{
	            		local:agregar_claim('DOMICTLF',$request/DOMICTLF)
	             	}
	             	{
	             		local:agregar_claim('DOMICCEL',$request/DOMICCEL)
	             	}
	             	{
	             		local:agregar_claim('CRUCERUT',$request/CRUCERUT)
	             	}
	             	{
	             		local:agregar_claim('FUEROCOD',$request/FUEROCOD)
	             	}
	             	{
	             		local:agregar_claim('GRABAVIA',$request/GRABAVIA)
	             	}
	             	{
		             	local:agregar_claim('INTERDOM',$request/INTERDOM)
	             	}
	             	{
	             		local:agregar_claim('JURISCOD',$request/JURISCOD)
	             	}
	             	{
		             	local:agregar_claim('JUZGACOD',$request/JUZGACOD)
	             	}
	             	{
		             	local:agregar_claim('KILOMETRO',$request/KILOMETRO)
	             	}
             		{
		             	local:agregar_claim('PERIODIA',$request/PERIODIA)
	             	}
	             	{
	             		local:agregar_claim('RUTANRO',$request/RUTANRO)
	             	}
	             	{
	             		local:agregar_claim('SECRECOD',$request/SECRECOD)
	             	}
	             	{
	             		local:agregar_claim('SEMAFORO',$request/SEMAFORO)
	             	}
	             	{
	             		local:agregar_claim('SINIETPO',$request/SINIETPO)
	             	}
	             	{
	             		local:agregar_claim('SITUCBAR',$request/SITUCBAR)
	             	}
	             	{
	             		local:agregar_claim('SITUCCAL',$request/SITUCCAL)
	             	}
	             	{
	             		local:agregar_claim('SITUCLIMA',$request/SITUCLIMA)
	             	}
	             	{
	             		local:agregar_claim('SITUCSEM',$request/SITUCSEM)
	             	}
	             	{
	             		local:agregar_claim('SWLUGINS',$request/SWLUGINS)
	             	}
	             	{
		             	local:agregar_claim('TIPORUTA',$request/TIPORUTA)
	             	}
	             	{
	             		local:agregar_claim('TIPPENAL',$request/TIPPENAL)
	             	}
	             	{
		             	local:agregar_claim('ZONAURB',$request/ZONAURB)
		            }
	             	{
		                if (normalize-space(data($request/SINIEDES_DA))!="") then
		                (
			                <ns0:ClaimAddition>
			                    <ns0:ID>SINIECDE</ns0:ID>
			                    <ns0:Value>DA</ns0:Value>
			                </ns0:ClaimAddition>,
			                <ns0:ClaimAddition>
			                    <ns0:ID>SINIEDES</ns0:ID>
			                    <ns0:Value>{ normalize-space(data($request/SINIEDES_DA)) }</ns0:Value>
			                </ns0:ClaimAddition>
		                ) else ()
	             	}
             		{
	             		local:agregar_claim('USUARCOD',$request/GRABAUSU)
               		}
	               	{
	               		local:agregar_claim('TIPO_CONDUCTOR',$request/TIPO_CONDUCTOR)
               		}
             		{
		             	if (substring(data($request/COLISION),20,1)!="0") then(
				           	<ns0:ClaimAddition>
			                    <ns0:ID>HUBOTEST</ns0:ID>
			                    <ns0:Value>S</ns0:Value>
			                </ns0:ClaimAddition>
		                )else ()
	             	}
					{
						local:agregar_claim('DOMICDOM',$request/DOMICDOM)
	                }
                	{
	                	local:agregar_claim('DOMICDNU',$request/DOMICDNU)
	                 }
	                 {
	                 	local:agregar_claim('DOMICPOB',$request/DOMICPOB)
	                 }
	                 {
	                 	local:agregar_claim('DOMICPIS',$request/DOMICPIS)
	                }
				  	{
				     	local:agregar_claim('DOMICPTA',$request/DOMICPTA)
	                }
					{
		                if ((fn-bea:trim(data($request/CONDVANN))!="") and (fn-bea:trim(data($request/CONDVMES))!="") and (fn-bea:trim(data($request/CONDVDIA))!="")) then
		                (
				           	<ns0:ClaimAddition>
			                    <ns0:ID>CONDVLIC</ns0:ID>
			                    <ns0:Value>{ fn-bea:date-from-string-with-format("yyyyMMdd", concat(fn-bea:trim(data($request/CONDVANN)),fn-bea:trim(data($request/CONDVMES)),fn-bea:trim(data($request/CONDVDIA)))) }</ns0:Value>
			                </ns0:ClaimAddition>
		                ) else ()
	                }
	                {
		                if (exists($request/TIPOACCI) and data($request/TIPOACCI) != '') then (
			                CodigosDeTipoAcci(data($request/TIPOACCI))
			            ) else ()
		         	}
		     
	       			{
				     	local:agregar_claim('SINIESQL',$request/SINIESQL)
	                }
	       		    { 
	               	<ns0:ClaimAddition>
	                    <ns0:ID>HORADEN</ns0:ID>
	                    <ns0:Value>{fn:substring(string(fn:current-time()),1,8)  }</ns0:Value>
	                </ns0:ClaimAddition>
					 }
	       		
			</ns0:ClaimAdditions>
		 </ns0:QbeRegisterClaimRq>


};

declare variable $request as element(Request) external;
declare variable $LOVs as element(lov:LOVs) external;
declare variable $colisionCod as xs:string external;
declare variable $docNums as element()? external;
declare variable $DummyVehiculeRegNo as element()? external;


declare function Genero ($OVGenero as xs:string) {
	   if (upper-case($OVGenero) = 'F') then (2)
			else if (upper-case($OVGenero) = 'M') then (1)
			else if (upper-case($OVGenero) = 'N') then (0)
			else (1) (:en el caso vacio se manda Masculino:)
};



declare function NombreDemandanteDesconocido($OVGiven as xs:string) {

	 ( if (fn-bea:trim($OVGiven)!="") then (
			$OVGiven
			) else (
			'UNKNOWNCLAIMANT'
			))
	
};

declare function NombreLesionadoDesconocido($OVGiven as xs:string) {

	 ( if (fn-bea:trim($OVGiven)!="") then (
			$OVGiven
			) else (
			'UNKNOWNINJURED'
			))
};



declare function CodigosDeColisiones
  ( $OVCodColision as xs:string )  {

  	(: Solo se debe evaluar hasta la posición 12 del vector,
  		generando COLISION a COLISION12 en claim addition De la posicion 13 al 19 no se usan.:)

 	 for $valor in functx:index-of-string(substring($OVCodColision,1,12),'1')
	   return
	     	<ns0:ClaimAddition>
	                    <ns0:ID>{ if ($valor!=1) then (
	                    			concat('COLISION',fn-bea:format-number(xs:double($valor), '00'))
	                    		) else(
	                    			'COLISION'
	                    		)
	                    		}</ns0:ID>
	                    <ns0:Value>{fn-bea:format-number(xs:double($valor), '000')}</ns0:Value>
	       </ns0:ClaimAddition>

 } ;

declare function CodigosDeTipoAcci
  ( $OVCodAcci as xs:string )  {

 	 for $valor in functx:index-of-string(substring($OVCodAcci,1,6),'1')
	   return
	     	<ns0:ClaimAddition>
	                    <ns0:ID>{ 
	                    			concat('TIPOACCI',fn-bea:format-number(xs:double($valor), '00'))
                		}</ns0:ID>
	                    <ns0:Value>{fn-bea:format-number(xs:double($valor), '0')}</ns0:Value>
	       </ns0:ClaimAddition>

 } ;

declare function local:agregar_claim($ID as xs:string,$value as element(*)?)
{
	if (exists($value) and normalize-space(data($value))!="") then
	(
                  	<ns0:ClaimAddition>
	                    <ns0:ID>{ $ID }</ns0:ID>
	                    <ns0:Value>{ normalize-space(data($value)) }</ns0:Value>
	            	</ns0:ClaimAddition>
	) else ()
};

declare function functx:index-of-string
  ( $arg as xs:string? ,
    $substring as xs:string )  as xs:integer*  {

  if (contains($arg, $substring))
  then (string-length(substring-before($arg, $substring))+1,
        for $other in
           functx:index-of-string(substring-after($arg, $substring),
                               $substring)
        return
          $other +
          string-length(substring-before($arg, $substring)) +
          string-length($substring))
  else ()
 } ;

 
xf:SD_1348_From_QBERegisterClaimRq($request,$LOVs,$docNums,$DummyVehiculeRegNo)
