xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/SD_1361.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/SiniestroDenuncia/Resource/XQuery/SD_1361_To_PaginatedQueryRq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:SD_1361_To_PaginatedQueryRq($request as element(Request))
    as element() {
    <ns0:PaginatedQueryRq>
            <ns0:QueryID>1361</ns0:QueryID>
            <ns0:FilterCriteria>
            		<ns0:FilterCriterion field = "CLIENSEC"
                                     operation = "EQ"
                                     value = "{data($request/CLIENSEC)}"/>
                    <ns0:FilterCriterion field = "CHASINUM"
                                     operation = "EQ"
                                     value = "{data($request/CHASINUM)}"/>
                    <ns0:FilterCriterion field = "FECHASINIE"
                                     operation = "EQ"
                                     value = "{fn-bea:date-from-string-with-format("yyyyMMdd",data($request/FECSTRO))}"/>   
                    <ns0:FilterCriterion field = "TIPOSINIE"
                                     operation = "EQ"
                                     value = "{data($request/TIPSTRO)}"/>
 					<ns0:FilterCriterion field = "NROSECU"
                                     operation = "EQ"
                                     value = "{data($request/NROSECU)}"/>   		
         	</ns0:FilterCriteria>
         
    </ns0:PaginatedQueryRq>
};

declare variable $request as element(Request) external;

xf:SD_1361_To_PaginatedQueryRq($request)
