xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/SD_1347.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:local-element-return type="anyType" ::)

declare namespace xf = "http://tempuri.org/SiniestroDenuncia/Resource/XQuery/SD_1348_From_QBERegisterClaimRq/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare namespace functx = "http://www.functx.com";

declare function xf:SD_1348_Person_Data($request as element(Request),
    $LOVs as element(lov:LOVs))
    as element() {
    	 <Persons>
                
					{
            		if ((fn-bea:trim(data($request/CONDUNOM))!="")  (:or (data($request/CONDUAP1)!=""):)) then (
            		 <Person?>
            		 <ns0:Entity>
                    	<ns0:PersonalData>
                    		<ns0:Name>
                    			<ns0:Given>{data($request/CONDUNOM)}</ns0:Given>
                    			<ns0:Family>{data($request/CONDUAP1)}</ns0:Family>
                    		</ns0:Name>
                    		<ns0:PID>{ concat( data($LOVs/lov:LOV[@id = "DocTypeNameById"]/ns0:ListItems/ns0:ListItem/ns0:ItemID) ,'-', $request/CONDUDAT) }</ns0:PID>
                    		 (: pasar en el LOV CONDUTPO :)
                        	<ns0:Gender>{
                                    if (upper-case(data($request/CONDUSEX)) = 'F') then
                                        ('2')
                                    else
                                        if (upper-case(data($request/CONDUSEX)) = 'M') then
                                            ('1')
                                        else
                                            if (upper-case(data($request/CONDUSEX)) = 'N') then
                                                ('0')
                                            else
                                                ('1')
                                }</ns0:Gender>
                                <ns0:BirthDate>{ if (exists($request/CONDUANN) and data($request/CONDUANN) != ''
							   						and exists($request/CONDUMES) and data($request/CONDUMES) != ''
							   						and exists($request/CONDUDIA) and data($request/CONDUDIA) != '') then 
							   						concat($request/CONDUANN ,'-', fn-bea:format-number(xs:double($request/CONDUMES), '00') ,'-', fn-bea:format-number(xs:double($request/CONDUDIA),'00'))
							   					else
							   						'1900-01-01'
								}</ns0:BirthDate>
                        </ns0:PersonalData>
                    	<ns0:IndustryCode>{data($request/CONDUPRF)}</ns0:IndustryCode>
                    	<ns0:DataSource>{data($request/CONDUEST)}</ns0:DataSource>
                    </ns0:Entity>,
                    <ns0:Addresses>
                        <ns0:Address>

                     		(:<ns0:CountryCode>{data($request/CONDUPAI)}</ns0:CountryCode>:)
                     		<ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/CONDUPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
                   			<ns0:StateRegion>{data($request/CONDUPRO)}</ns0:StateRegion>
                     		<ns0:City>{data($request/CONDUPOB)}</ns0:City>
                     		<ns0:ResidentialAddress>
                                <ns0:StreetName>{ data($request/CONDUDOM) }</ns0:StreetName>
                            	<ns0:StreetNo>{ data($request/CONDUDNU) }</ns0:StreetNo>
                            	<ns0:Floor>{data($request/CONDUPIS)}</ns0:Floor>
                            	<ns0:Apartment>{data($request/CONDUPTA)}</ns0:Apartment>
                            </ns0:ResidentialAddress>
                            <ns0:AddressType>C_CLM</ns0:AddressType>
                        </ns0:Address>
                    </ns0:Addresses>,
                    <ns0:Contacts>
                    {
                    	if (fn-bea:trim(data($request/CONDUTLF))!="") then
                    	(<ns0:Contact>
			                    <ns0:Type>TAC</ns0:Type>
			                    <ns0:Details>{ data($request/CONDUTLF) }</ns0:Details>
			             </ns0:Contact>
			            )else()

                    }{
                    	if (fn-bea:trim(data($request/CONDUCEL))!="") then
                    	(<ns0:Contact>
			                    <ns0:Type>TEP</ns0:Type>
			                    <ns0:Details>{ data($request/CONDUCEL) }</ns0:Details>
			             </ns0:Contact>
			            )else()

                    }</ns0:Contacts>
                    </Person>
            	) else ()
            	}
                
		</Persons>
};

declare function Genero ($OVGenero as xs:string) {
	   if (upper-case($OVGenero) = 'F') then (2)
			else if (upper-case($OVGenero) = 'M') then (1)
			else if (upper-case($OVGenero) = 'N') then (0)
			else (1) (:en el caso vacio se manda Masculino:)
};

declare variable $request as element(Request) external;
declare variable $LOVs as element(lov:LOVs) external;
 
xf:SD_1348_Person_Data($request,$LOVs)
