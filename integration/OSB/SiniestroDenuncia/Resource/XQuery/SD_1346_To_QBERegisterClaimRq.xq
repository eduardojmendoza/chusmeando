(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/SD_1346.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QbeRegisterClaimRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";
declare namespace xf = "http://tempuri.org/SiniestroDenuncia/Resource/XQuery/SD_1346/";

declare function xf:SD_1346_To_QBERegisterClaimRq($request as element(Request),
                                              $LOVs as element(lov:LOVs))
    as element() {
        <ns0:QbeRegisterClaimRq>
            <ns0:PolicyNo>
                {
                    concat('0001' ,
                    fn-bea:pad-right($request/RAMOPCOD,4) ,
                    fn-bea:format-number(xs:double($request/POLIZANN), '00') ,
                    fn-bea:format-number(xs:double($request/POLIZSEC), '000000') ,
                    fn-bea:format-number(xs:double($request/CERTIPOL), '0000') ,
                    fn-bea:format-number(xs:double($request/CERTIANN), '0000') ,
                    fn-bea:format-number(xs:double($request/CERTISEC), '000000'))
                }
			</ns0:PolicyNo>
            <ns0:ClaimStarted>
                {
                    concat(data($request/DENUCANN),'-',
                    fn-bea:format-number(xs:double($request/DENUCMES), '00') ,'-',
                    fn-bea:format-number(xs:double($request/DENUCDIA), '00'))
                }
			</ns0:ClaimStarted>
            <ns0:ClaimType>CASCO</ns0:ClaimType>
            <ns0:CauseID>1</ns0:CauseID>
            <ns0:EventType>{ data($LOVs/lov:LOV[@id = "ClaimEventType"]/ns0:ListItems/ns0:ListItem/ns0:ItemID) }</ns0:EventType>
           	<ns0:EventDate>{ concat($request/SINFEANN ,'-', fn-bea:format-number(xs:double($request/SINFEMES), '00') ,'-', fn-bea:format-number(xs:double($request/SINFEDIA),'00')) }</ns0:EventDate>
            <ns0:EventTime>{ concat(fn-bea:format-number(xs:double($request/SINFEHOR),'00') ,':', fn-bea:format-number(xs:double($request/SINFEMIN),'00'),':00') }</ns0:EventTime>
            <ns0:EventDescription>{ data($request/SINIEDES) }</ns0:EventDescription>
            {
            	if (fn-bea:trim(data($request/SINIEDOM))!="") then ( (:es zonaUrbana:)
            		<ns0:ClaimAddresses>
		                <ns0:ClaimAddress>
		                   (: <ns0:CountryCode>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/SINIEPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:CountryCode>:)
		                     <ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/SINIEPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>

		                    <ns0:StateRegion>{ data($request/SINIEPRO) }</ns0:StateRegion>
		                    <ns0:City>{ data($request/SINIEPOB) }</ns0:City>
		                    <ns0:ResidentialAddress>
		                        <ns0:StreetName>{ data($request/SINIEDOM) }</ns0:StreetName>
		                        <ns0:StreetNo>{ data($request/SINIEDNU) }</ns0:StreetNo>
		                    </ns0:ResidentialAddress>
		                    <ns0:AddressType>EVENT</ns0:AddressType>
		                </ns0:ClaimAddress>
		            </ns0:ClaimAddresses>
            	) else ( (: es zona Rural:)
            		<ns0:ClaimAddresses>
		                <ns0:ClaimAddress>
		                    <ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/SINIEPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
		                    <ns0:StateRegion>{ data($request/SINIEPRO) }</ns0:StateRegion>
		                    <ns0:City>{ data($request/SINIEPOB) }</ns0:City>
		                    <ns0:ResidentialAddress>
		                        <ns0:StreetName>{ concat("Ruta ",data($request/RUTANRO)) }</ns0:StreetName>
		                        <ns0:Apartment>{ concat("KM. ",data($request/KILOMETRO)) }</ns0:Apartment>
		                    </ns0:ResidentialAddress>
		                    <ns0:AddressType>EVENT</ns0:AddressType>
		                    <ns0:Notes>{data($request/CRUCERUT)}</ns0:Notes>
		                    <ns0:TerritoryClass>{data($request/TIPORUTA)}</ns0:TerritoryClass>

		                </ns0:ClaimAddress>
		            </ns0:ClaimAddresses>
            	)

            }
            <ns0:Requests>
                <ns0:Request>
                    <ns0:Entity>
                        <ns0:PersonalData>
                            <ns0:Name>
                                <ns0:Given>{ data($request/DENUNNOM) }</ns0:Given>
                                <ns0:Family>{ data($request/DENUNAP1) }</ns0:Family>
                            </ns0:Name>
                            (:@TODO: uncomment when LOV DocTypeNameById this productive :)
                             <ns0:PID>{ concat( data($LOVs/lov:LOV[@id = "DocTypeNameById"]/ns0:ListItems/ns0:ListItem/ns0:ItemID) ,'-', $request/DENUNDAT) }</ns0:PID>
                            <ns0:Gender>
                                {
                                    if (upper-case(data($request/DENUNSEX)) = 'F') then
                                        ('2')
                                    else
                                        if (upper-case(data($request/DENUNSEX)) = 'M') then
                                            ('1')
                                        else
                                            if (upper-case(data($request/DENUNSEX)) = 'N') then
                                                ('0')
                                            else
                                                ()
                                }
							</ns0:Gender>
                            <ns0:BirthDate>
                            { 
                            if ($request/DENUNANN != '' and $request/DENUNMES != '' and  $request/DENUNDIA != '') then
                            	
                            	concat($request/DENUNANN ,'-', fn-bea:format-number(xs:double($request/DENUNMES), '00') ,'-', fn-bea:format-number(xs:double($request/DENUNDIA),'00')) 
                            
                            else ()
                            }
                            </ns0:BirthDate>
                        </ns0:PersonalData>
                        <ns0:DataSource>{ data($request/DENUNEST) }</ns0:DataSource>
                    </ns0:Entity>
                    <ns0:Addresses>
                        <ns0:Address>
                            (:<ns0:CountryCode>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/DENUNPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:CountryCode>:)
                             <ns0:Country>{ data($LOVs/lov:LOV[@id = "LkpCountry" and @ref=data($request/DENUNPAI)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</ns0:Country>
                   			<ns0:StateRegion>{ data($request/DENUNPRO) }</ns0:StateRegion>
                            <ns0:City>{ data($request/DENUNPOB) }</ns0:City>
                            <ns0:ResidentialAddress>
                                <ns0:StreetName>{ data($request/DENUNDOM) }</ns0:StreetName>
                                <ns0:StreetNo>{ data($request/DENUNDNU) }</ns0:StreetNo>
                                <ns0:Entrance>{ data($request/DENUNPTA) }</ns0:Entrance>
                                <ns0:Apartment>{ data($request/DENUNPIS) }</ns0:Apartment>
                            </ns0:ResidentialAddress>
                            <ns0:AddressType>C_CLM</ns0:AddressType>
                        </ns0:Address>
                    </ns0:Addresses>
                    <ns0:Contacts>
                    {
                    	if (fn-bea:trim(data($request/DENUNCEL))!="") then
                    	(<ns0:Contact>
			                    <ns0:Type>TEP</ns0:Type>
			                    <ns0:Details>{ data($request/DENUNCEL) }</ns0:Details>
			             </ns0:Contact>
			            )else()

                    }
                    {
                    	if (fn-bea:trim(data($request/DENUNTLF))!="") then
                    	(<ns0:Contact>
			                  <ns0:Type>TAC</ns0:Type>
			                  <ns0:Details>{ data($request/DENUNTLF) }</ns0:Details>
			             </ns0:Contact>
			            )else()

                    }{
                    	if (fn-bea:trim(data($request/DENUNMAIL))!="") then
                    	(<ns0:Contact>
			                  <ns0:Type>EMA</ns0:Type>
			                  <ns0:Details>{ data($request/DENUNMAIL) }</ns0:Details>
			             </ns0:Contact>
			            )else()

                    }
                  </ns0:Contacts>
                  <ns0:ClaimantType>25</ns0:ClaimantType>
                  <ns0:InjuredObjects>
					<ns0:InjuredObject>
						<ns0:ObjectData>
							<ns0:ObjectType>111</ns0:ObjectType>
						</ns0:ObjectData>
						<ns0:InsuredData>
							<ns0:CustomOID>
								<ns0:ObjectType>111</ns0:ObjectType>
								<ns0:FilterCriteria>
									<ns0:FilterCriterion field="reg_no" operation="EQ" value="{ data($LOVs/lov:LOV[@id = "RegNoByPolicyNo"]/ns0:ListItems/ns0:ListItem/ns0:ItemID)}" />
								</ns0:FilterCriteria>
							</ns0:CustomOID>
						</ns0:InsuredData>
					</ns0:InjuredObject>
			 	 </ns0:InjuredObjects>
                    (:<ns0:InjuredObjects>
                        <ns0:InjuredObject>
                            <ns0:Driver>
                                <ns0:Contacts>
                                    <ns0:Contact>
                                        <ns0:Type>TAC</ns0:Type>
                                        <ns0:Details>{ data($request/DOMICTLF) }</ns0:Details>
                                    </ns0:Contact>
                                </ns0:Contacts>
                            </ns0:Driver>
                        </ns0:InjuredObject>
                    </ns0:InjuredObjects>:)
                </ns0:Request>
            </ns0:Requests>
            <ns0:ClaimMatrix>
          		<ns0:DriverResponsibility>N</ns0:DriverResponsibility>
				<ns0:ThirdPartiesDamages>N</ns0:ThirdPartiesDamages>
				<ns0:ThirdPartiesInjuries>N</ns0:ThirdPartiesInjuries>
                <ns0:CleasClaim>{ 'N' (:data($request/CLEASOPC):) }</ns0:CleasClaim>
                <ns0:ClaimType>{ fn-bea:format-number(xs:double($request/SINIECOD),"0") }</ns0:ClaimType>
                <ns0:ClaimCause>{ data($request/SINIECAU) }</ns0:ClaimCause>
                <ns0:EventType>{ data($request/GESTITIP) }</ns0:EventType>
                <ns0:Repairs>
                    <ns0:Repair>
                        <ns0:SupplierCategory>{ data($request/CATEGTAL) }</ns0:SupplierCategory>
                        <ns0:SupplierState>{ data($request/PROVICOD) }</ns0:SupplierState>
                        <ns0:SupplierCode>{ data($request/TALLECOD) }</ns0:SupplierCode>
                        <ns0:RepairOrderNo>{ data($request/REPARACIONES/REPARACION/TALLEORDR) }</ns0:RepairOrderNo>
                    </ns0:Repair>
                </ns0:Repairs>
            </ns0:ClaimMatrix>
            <ns0:ClaimAdditions>
            (:@TODO: Mejorar esta implementacion.:)
	                {
	                (:When you receive <DANIOTIP><![CDATA[000000]]></DANIOTIP>
						Do not fill anything in ClaimAddition for this item.:)

	                 if (fn-bea:trim(data($request/DANIOTIP))!="" and
	                 fn-bea:format-number(xs:double($request/DANIOTIP), '000000')!="000000"
	                 ) then(
		                <ns0:ClaimAddition>
		                    <ns0:ID>DANIOTIP</ns0:ID>
		                    <ns0:Value>{ data($request/DANIOTIP) }</ns0:Value>
		                </ns0:ClaimAddition>
	                )else ()
	                }
	                {
	                 if (fn-bea:trim(data($request/REPARACIONES/REPARACION/TALLEORDR))!="") then(
		                <ns0:ClaimAddition>
		                    <ns0:ID>ORDENREP</ns0:ID>
		                    <ns0:Value>{ data($request/REPARACIONES/REPARACION/TALLEORDR) }</ns0:Value>
		                </ns0:ClaimAddition>
	                )else ()
	                }                
	                {
	                 if (fn-bea:trim(data($request/DENUCPAI))!="") then(
		                <ns0:ClaimAddition>
		                    <ns0:ID>DENUCPAI</ns0:ID>
		                    <ns0:Value>{ data($request/DENUCPAI) }</ns0:Value>
		                </ns0:ClaimAddition>
	                )else ()
	                }
	                {
	                 if (fn-bea:trim(data($request/DENUCPOB))!="") then(
		                <ns0:ClaimAddition>
	                    <ns0:ID>DENUCPOB</ns0:ID>
	                    <ns0:Value>{ data($request/DENUCPOB) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                }
	              	{
	                 if (fn-bea:trim(data($request/DENUCPRO))!="") then(
		           <ns0:ClaimAddition>
	                    <ns0:ID>DENUCPRO</ns0:ID>
	                    <ns0:Value>{ data($request/DENUCPRO) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                }
	                {
	                 if (fn-bea:trim(data($request/DOMICTLF))!="") then(
		           <ns0:ClaimAddition>
	                    <ns0:ID>DOMICTLF</ns0:ID>
	                    <ns0:Value>{ data($request/DOMICTLF) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                }
					{
	                 if (fn-bea:trim(data($request/GRABSVIA))!="") then(
		           <ns0:ClaimAddition>
	                    <ns0:ID>GRABAVIA</ns0:ID>
	                    <ns0:Value>{ data($request/GRABSVIA) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                }
					{ if (fn-bea:trim(data($request/INTERDOM))!="") then(
		           <ns0:ClaimAddition>
	                    <ns0:ID>INTERDOM</ns0:ID>
	                    <ns0:Value>{ data($request/INTERDOM) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()}
					(:{
	                 if (fn-bea:trim(data($request/KILOMETRO))!="") then(
		           <ns0:ClaimAddition>
	                    <ns0:ID>KILOMETRO</ns0:ID>
	                    <ns0:Value>{ data($request/KILOMETRO) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                }
					{
	                 if (fn-bea:trim(data($request/RUTANRO))!="") then(
		           	<ns0:ClaimAddition>
	                    <ns0:ID>RUTANRO</ns0:ID>
	                    <ns0:Value>{ data($request/RUTANRO) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                }
					{
	                 if (fn-bea:trim(data($request/CRUCERUT))!="") then(
		           	<ns0:ClaimAddition>
	                    <ns0:ID>CRUCERUT</ns0:ID>
	                    <ns0:Value>{ data($request/CRUCERUT) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                }:)
					{
	                 if (fn-bea:trim(data($request/SINIETPO))!="") then(
		           	<ns0:ClaimAddition>
	                    <ns0:ID>SINIETPO</ns0:ID>
	                    <ns0:Value>{ data($request/SINIETPO) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                }
	                (:{
	                 if (fn-bea:trim(data($request/TIPORUTA))!="") then(
		           	<ns0:ClaimAddition>
	                    <ns0:ID>TIPORUTA</ns0:ID>
	                    <ns0:Value>{ data($request/TIPORUTA) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                }:)
					{
	                 if (fn-bea:trim(data($request/ZONAURB))!="") then(
		           	<ns0:ClaimAddition>
	                    <ns0:ID>ZONAURB</ns0:ID>
	                    <ns0:Value>{ data($request/ZONAURB) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                }
					{
	                (: if (fn-bea:trim(data($request/SINIECDE))!="") then( :)
		           	if (fn-bea:trim(data($request/SINIEDES))!="") then(
		           	<ns0:ClaimAddition>
	                    <ns0:ID>SINIECDE</ns0:ID>
	                    <ns0:Value>DH(:{ data($request/SINIECDE) }:)</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                }{
	                if (fn-bea:trim(data($request/GRABAUSU))!="") then(
		           	<ns0:ClaimAddition>
	                    <ns0:ID>USUARCOD</ns0:ID>
	                    <ns0:Value>{ data($request/GRABAUSU) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                 }

	                 {
					 if (fn-bea:trim(data($request/DOMICDOM))!="") then(
		           	<ns0:ClaimAddition>
	                    <ns0:ID>DOMICDOM</ns0:ID>
	                    <ns0:Value>{ data($request/DOMICDOM) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                 }
	                {
	                if (fn-bea:trim(data($request/DOMICDNU))!="") then(
		           	<ns0:ClaimAddition>
	                    <ns0:ID>DOMICDNU</ns0:ID>
	                    <ns0:Value>{ data($request/DOMICDNU) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                 }
	                 {
	                if (fn-bea:trim(data($request/DOMICPOB))!="") then(
		           	<ns0:ClaimAddition>
	                    <ns0:ID>DOMICPOB</ns0:ID>
	                    <ns0:Value>{ data($request/DOMICPOB) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                 }
	                 {
	                if (fn-bea:trim(data($request/DOMICCEL))!="") then(
		           	<ns0:ClaimAddition>
	                    <ns0:ID>DOMICCEL</ns0:ID>
	                    <ns0:Value>{ data($request/DOMICCEL) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                 }
	                 {
	                if (fn-bea:trim(data($request/DOMICPIS))!="") then(
		           	<ns0:ClaimAddition>
	                    <ns0:ID>DOMICPIS</ns0:ID>
	                    <ns0:Value>{ data($request/DOMICPIS) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                 }
					{
	                if (fn-bea:trim(data($request/DOMICPTA))!="") then(
		           	<ns0:ClaimAddition>
	                    <ns0:ID>DOMICPTA</ns0:ID>
	                    <ns0:Value>{ data($request/DOMICPTA) }</ns0:Value>
	                </ns0:ClaimAddition>
	                )else ()
	                 }
	                 { 
	               	<ns0:ClaimAddition>
	                    <ns0:ID>HORADEN</ns0:ID>
	                    <ns0:Value>{fn:substring(string(fn:current-time()),1,8)  }</ns0:Value>
	                </ns0:ClaimAddition>
					 }
                    {
                        local:agregar_claim('SINIESQL',$request/SINIESQL)
                    }

				</ns0:ClaimAdditions>
        </ns0:QbeRegisterClaimRq>
};

declare function local:agregar_claim($ID as xs:string,$value as element(*)?)
{
	if (exists($value) and normalize-space(data($value))!="") then
	(
                  	<ns0:ClaimAddition>
	                    <ns0:ID>{ $ID }</ns0:ID>
	                    <ns0:Value>{ normalize-space(data($value)) }</ns0:Value>
	            	</ns0:ClaimAddition>
	) else ()
};

declare variable $request as element(Request) external;
declare variable $LOVs as element(lov:LOVs) external;
xf:SD_1346_To_QBERegisterClaimRq($request,$LOVs)
