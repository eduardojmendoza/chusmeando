xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/SD_1340.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/SiniestroDenuncia/Resource/XQuery/SD_1340_To_PaginatedQueryRq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:getInsisDocType($OVDocType as xs:string, $LOVs as element(*) ) as xs:string{
    if (  exists($OVDocType) and exists($LOVs) and data($OVDocType) != '') then (
    	let $docTypeInt := xs:int(data($OVDocType))
    	return
		if ($docTypeInt = 2 or $docTypeInt = 3) then
			let $docType := if($docTypeInt = 2) then '3' else '2'
			let $insisDocType := $LOVs/lov:LOV[@id = "DocTypeNameById" and @ref = concat('',$docTypeInt) ][1]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID
			return
				if(exists($insisDocType)) then data($insisDocType) else ''
		else 
			let $insisDocType := $LOVs/lov:LOV[@id = "DocTypeNameById" and @ref = $OVDocType ][1]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID
			return
				if(exists($insisDocType)) then data($insisDocType) else ''
			)
    else ''
};

declare function xf:SD_1340_To_PaginatedQueryRq($LOVs as element()?, $request as element(Request))
    as element() {
    <ns0:PaginatedQueryRq>
            <ns0:QueryID>1340</ns0:QueryID>
            <ns0:FilterCriteria>
            {
	            if (data($request/USUARCOD) != '' ) then
	            (
	            		<ns0:FilterCriterion field = "USUARCOD"
	                                     operation = "EQ"
	                                     value = "{ data($request/USUARCOD) }"/>
				) else ()
			}
			 
			{
			let $nivel := if(exists($request/NIVELCLAS)) then (data($request/NIVELCLAS)) else(data($request/NIVELAS))
			return			 
	        	if ($nivel != '' and data($request/CLIENSECAS) != '') then
	        	(
	                <ns0:FilterCriterion field = "NIVELCLAS"
	                                     operation = "EQ"
	                                     value = "{ $nivel }"/>,
	                <ns0:FilterCriterion field = "CLIENSECAS"
	                                     operation = "EQ"
	                                     value = "{ data($request/CLIENSECAS) }"/>
	           	)
	           	else ()
            }
			{
				if (data($request/DOCUMTIP) != '' ) then
	            (
						<ns0:FilterCriterion field = "DOCUMTIP"
	                                     operation = "EQ"
	                                     value = "{
                                		let $docType :=  xf:getInsisDocType(data($request/DOCUMTIP), $LOVs)
										return $docType
								 	}"/>
				) else ()
			}
			{
				if (data($request/DOCUMDAT) != '' ) then
	            (
						<ns0:FilterCriterion field = "DOCUMDAT"
	                                     operation = "EQ"
	                                     value = "{ data($request/DOCUMDAT) }"/>
				) else ()
			}
			{
				if (data($request/APELLIDO) != '' ) then
	            (
						<ns0:FilterCriterion field = "APELLIDO"
	                                     operation = "EQ"
	                                     value = "{ data($request/APELLIDO) }"/>
				) else ()
			}

					<ns0:FilterCriterion field = "POLICY_NO"
                                     operation = "EQ"
                                     value = "{ 
                                     let $producto := data($request/RAMO)
 			                                         let $poliza := data($request/POLIZA)
 			                                         let $certi := data($request/CERTIF)
                                     return
                                     if ($producto != '' and $poliza != '') then
 			                           (xqubh:buildPolicyNo('0001', data($producto), data($poliza), concat('1',data($certi)))
 			                                            ) else ()
 
                                      }"/>

			{
				if (data($request/PATENTE) != '' ) then
	            (
						<ns0:FilterCriterion field = "PATENTE"
	                                     operation = "EQ"
	                                     value = "{ data($request/PATENTE) }"/>
				) else ()
			}
			{
				if (data($request/FECHASINIE) != '' ) then
	            (
						<ns0:FilterCriterion field = "FECHASINIE"
	                                     operation = "EQ"
	                                     value = "{ fn-bea:date-from-string-with-format("yyyyMMdd", data($request/FECHASINIE)) }"/>
				) else ()
			}
			{
				if (data($request/TIPOSINIE) != '' ) then
	            (
						<ns0:FilterCriterion field = "TIPOSINIE"
	                                     operation = "EQ"
	                                     value = "{ data($request/TIPOSINIE) }"/>
				) else ()
			}
			{
				if (data($request/SWBUSCA) != '' ) then
	            (
						<ns0:FilterCriterion field = "SWBUSCA"
	                                     operation = "EQ"
	                                     value = "{ data($request/SWBUSCA) }"/>
				) else ()
			}
			{
				if (data($request/ORDEN) != '' ) then
	            (
						<ns0:FilterCriterion field = "ORDEN"
	                                     operation = "EQ"
	                                     value = "{ data($request/ORDEN) }"/>
				) else ()
			}
			{
				if (data($request/COLUMNA) != '' ) then
	            (
						<ns0:FilterCriterion field = "COLUMNA"
	                                     operation = "EQ"
	                                     value = "{ data($request/COLUMNA) }"/>
				) else ()
			}
            </ns0:FilterCriteria>
    </ns0:PaginatedQueryRq>
};

declare variable $LOVs as element(lov:LOVs)? external;
declare variable $request as element(Request) external;


xf:SD_1340_To_PaginatedQueryRq($LOVs, $request)

