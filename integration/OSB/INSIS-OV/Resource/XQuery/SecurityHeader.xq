xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-return element="wsse:Security" location="../XSD/Acord/oasis-200401-wss-wssecurity-secext-1.0.xsd" ::)

declare namespace xf = "http://tempuri.org/OV/Resource/XQuery/SecurityHeader/";
declare namespace wsse = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";

declare function xf:SecurityHeader($user as xs:string, $pass as xs:string)
    as element(wsse:Security) {
        <wsse:Security>
            <wsse:UsernameToken>
                <wsse:Username>{ $user }</wsse:Username>
                <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">{ $pass }</wsse:Password>
            </wsse:UsernameToken>
        </wsse:Security>
};

declare variable $user as xs:string external;
declare variable $pass as xs:string external;

xf:SecurityHeader($user, $pass)