xquery version "1.0" encoding "UTF-8";
(:: pragma  parameter="$msg" type="xs:anyType" ::)
(:: pragma  parameter="$attachUUID" type="xs:string" ::)
(:: pragma  type="xs:anyType" ::)

declare namespace xf = "http://tempuri.org/OV/Resource/XQuery/Attachment/";
declare namespace ctx = "http://www.bea.com/wli/sb/context";

declare function xf:Attachment($msg as element(*), $attachUUID as xs:string)
    as element() {
	    <ctx:attachments>
			<ctx:attachment>
				<ctx:Content-Type>text/xml; charset=UTF-8</ctx:Content-Type>
				<ctx:Content-ID>{ $attachUUID }</ctx:Content-ID>
				<ctx:body>
				     { $msg }
				</ctx:body>
			</ctx:attachment>
		</ctx:attachments>
};

declare variable $msg as element(*) external;
declare variable $attachUUID as xs:string external;

xf:Attachment($msg, $attachUUID)
