xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-return element="ns1:CallRq" location="../XSD/Acord/AcordMsgSvc_v-1-5-0.xsd" ::)

declare namespace xf = "http://tempuri.org/OV/Resource/XQuery/InsuranceSvc_Rq/";
declare namespace ns2 = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
declare namespace ns1 = "http://www.ACORD.org/Standards/AcordMsgSvc/1";
declare namespace ns4 = "http://www.w3.org/2005/08/addressing";
declare namespace ns3 = "http://www.w3.org/2000/09/xmldsig%23";
declare namespace ns0 = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
declare namespace ns5 = "http://www.w3.org/2001/04/xmlenc%23";

declare function xf:InsuranceSvc_Rq($msgTypeRq as xs:string, $attachUUID as xs:string)
    as element(ns1:CallRq) {
		<ns1:CallRq>
			<ns1:Sender>
				<ns1:PartyId>mailto:info@qbe.com</ns1:PartyId>
				<ns1:PartyRoleCd>broker</ns1:PartyRoleCd>
				<ns1:PartyName>QBE OSB</ns1:PartyName>
			</ns1:Sender>
			<ns1:Receiver>
				<ns1:PartyId>urn:fadata:insis</ns1:PartyId>
				<ns1:PartyRoleCd/>
				<ns1:PartyName>INSIS</ns1:PartyName>
			</ns1:Receiver>
			<ns1:Application>
				<ns1:ApplicationCd>Insis-PC-Surety</ns1:ApplicationCd>
				<ns1:SchemaVersion>http://www.fadata.bg/Insurance_Messages/v3.0/xml/</ns1:SchemaVersion>
				<ns1:SchemaMinorVersion>3.0</ns1:SchemaMinorVersion>
			</ns1:Application>
			<ns1:TimeStamp>{ current-dateTime() }</ns1:TimeStamp>
			<ns1:MsgItem>
				<ns1:MsgId>{ fn-bea:uuid() }</ns1:MsgId>
				<ns1:MsgTypeCd>{ $msgTypeRq }</ns1:MsgTypeCd>
			</ns1:MsgItem>
			<ns1:WorkFolder>
				<ns1:MsgFile>
					<ns1:FileId>{ concat('cid:', $attachUUID) }</ns1:FileId>
					<ns1:FileFormatCd>text/xml</ns1:FileFormatCd>
				</ns1:MsgFile>
			</ns1:WorkFolder>
		</ns1:CallRq>
};

declare variable $msgTypeRq as xs:string external;
declare variable $attachUUID as xs:string external;

xf:InsuranceSvc_Rq($msgTypeRq, $attachUUID)