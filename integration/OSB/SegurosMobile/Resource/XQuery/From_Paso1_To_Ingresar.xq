xquery version "1.0" encoding "UTF-8";
(:: pragma bea:schema-type-parameter parameter="$ingresarPaso11" type="ns0:ingresarPaso1" location="../WSDL/SegurosMobile_types.wsdl" ::)
(:: pragma bea:global-element-return element="ns1:executeRequest" location="../../../OV/Resource/WSDL/OVSvc.wsdl" ::)

declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/From_Paso1_To_Ingresar/";
declare namespace ns1 = "http://cms.services.qbe.com/";
declare namespace ns0 = "http://cms.services.qbe.com/segurosmobile";

declare function xf:From_Paso1_To_Ingresar($ingresarPaso11 as element())
    as element(ns1:executeRequest) {
        <ns1:executeRequest>
        <actionCode>nbwsA_Ingresar</actionCode>
		<Request>
			<IPORIGEN>{data($ingresarPaso11/ipOrigen)}</IPORIGEN>
			<PASSWORD>{data($ingresarPaso11/password)}</PASSWORD>
			<USUARIO>{data($ingresarPaso11/usuario)}</USUARIO>
		</Request>
		<contextInfo/>
		</ns1:executeRequest>
};

declare variable $ingresarPaso11 as element() external;

xf:From_Paso1_To_Ingresar($ingresarPaso11)
