xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$executeRequestResponse1" element="ns1:executeRequestResponse" location="../../../OV/Resource/WSDL/OVSvc.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:ingresarPaso1Response" location="../WSDL/SegurosMobile_types.wsdl" ::)

declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/From_Ingresar_To_Paso1Response/";
declare namespace ns1 = "http://cms.services.qbe.com/";
declare namespace ns0 = "http://cms.services.qbe.com/segurosmobile";

declare function xf:From_Ingresar_To_Paso1Response($executeRequestResponse1 as element(ns1:executeRequestResponse))
    as element(ns0:ingresarPaso1Response) {
        <ns0:ingresarPaso1Response>
         <return>
            <codResultado>{data($executeRequestResponse1/return/Response/CODRESULTADO)}</codResultado>
            <codError>{data($executeRequestResponse1/return/Response/CODERROR)}</codError>
            <msgResultado>{data($executeRequestResponse1/return/Response/MSGRESULTADO)}</msgResultado>
			<documentoIncompleto>
               <numero>{data($executeRequestResponse1/return/Response/RCCs/@idn)}</numero>
               <longitud>{data($executeRequestResponse1/return/Response/RCCs/@longitud)}</longitud>
               <digitos>
                  <digito>
                     <id>1</id>
                     <pos>{data($executeRequestResponse1/return/Response/RCCs/rcc[@id="1"])}</pos>
                  </digito>
                  <digito>
                     <id>2</id>
                     <pos>{data($executeRequestResponse1/return/Response/RCCs/rcc[@id="2"])}</pos>
                  </digito>
                  <digito>
                     <id>3</id>
                     <pos>{data($executeRequestResponse1/return/Response/RCCs/rcc[@id="3"])}</pos>
                  </digito>
               </digitos>
            </documentoIncompleto>
         </return>
      </ns0:ingresarPaso1Response>
};

declare variable $executeRequestResponse1 as element(ns1:executeRequestResponse) external;

xf:From_Ingresar_To_Paso1Response($executeRequestResponse1)
