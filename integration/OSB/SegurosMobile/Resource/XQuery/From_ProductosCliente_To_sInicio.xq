xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$productosCliente1" element="ns0:productosCliente" location="../WSDL/SegurosMobile_types.wsdl" ::)
(:: pragma bea:global-element-return element="ns1:executeRequest" location="../../../OV/Resource/WSDL/OVSvc.wsdl" ::)

declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/From_ProductosCliente_To_sInicio/";
declare namespace ns1 = "http://cms.services.qbe.com/";
declare namespace ns0 = "http://cms.services.qbe.com/segurosmobile";

declare function xf:TipoDoc_Tabla() as element() {

    	let $tipoDocs := <TIPOSDOC> <TIPODOC codigo="1" nombre="DNI"/>,
               <TIPODOC codigo="2" nombre="LE"/>,
               <TIPODOC codigo="3" nombre="LC"/>,
               <TIPODOC codigo="4" nombre="CUIT"/>,
               <TIPODOC codigo="5" nombre="CUIL"/>,
               <TIPODOC codigo="6" nombre="CI"/>,
               <TIPODOC codigo="47" nombre="PASAPORTE"/>,
               <TIPODOC codigo="99" nombre="OTRO"/>,
               </TIPOSDOC>
		return $tipoDocs
};

declare function xf:TipoDoc_Nombre_To_Codigo($nombre as xs:string)
    as xs:string {

    	let $tipoDocs := xf:TipoDoc_Tabla()
		return $tipoDocs/TIPODOC[@nombre=$nombre]/@codigo
};

declare function xf:TipoDoc_Codigo_To_Nombre($codigo as xs:string)
    as xs:string {

    	let $tipoDocs := xf:TipoDoc_Tabla()
		return $tipoDocs/TIPODOC[@codigo=xs:int($codigo)]/@nombre
};


declare function xf:From_ProductosCliente_To_sInicio($productosCliente1 as element(ns0:productosCliente))
    as element(ns1:executeRequest) {
        <ns1:executeRequest>
			<actionCode>nbwsA_sInicio</actionCode>
			<Request>
			      <DOCUMTIP>{xf:TipoDoc_Nombre_To_Codigo(data($productosCliente1/documento/tipo))}</DOCUMTIP>
			      <DOCUMDAT>{data($productosCliente1/documento/numero)}</DOCUMDAT>
			      (: <SESSION_ID>12345678</SESSION_ID> :)
			</Request>
            <contextInfo xsi:nil="true"/>
        </ns1:executeRequest>
};

declare variable $productosCliente1 as element(ns0:productosCliente) external;

xf:From_ProductosCliente_To_sInicio($productosCliente1)
