(:: pragma bea:local-element-parameter parameter="$producto1" type="ns0:productosClienteResponse/return/productos/producto" location="../WSDL/SegurosMobile_types.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:getDetallesPoliza" location="../WSDL/SegurosMobile_types.wsdl" ::)

declare namespace ns0 = "http://cms.services.qbe.com/segurosmobile";
declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/From_Producto_To_GetDetalles/";

declare function xf:From_Producto_To_GetDetalles($producto1 as element())
    as element(ns0:getDetallesPoliza) {
        <ns0:getDetallesPoliza>
            <arg0>
                <ramopcod>{ data($producto1/ramopcod) }</ramopcod>
                <polizann>{ xs:integer(data($producto1/polizann)) }</polizann>
                <polizsec>{ xs:integer(data($producto1/polizsec)) }</polizsec>
                <certipol>{ xs:integer(data($producto1/certipol)) }</certipol>
                <certiann>{ xs:integer(data($producto1/certiann)) }</certiann>
                <certisec>{ xs:integer(data($producto1/certisec)) }</certisec>
                <suplenum>{ xs:integer(data($producto1/suplenum)) }</suplenum>
            </arg0>
        </ns0:getDetallesPoliza>
};

declare variable $producto1 as element() external;

xf:From_Producto_To_GetDetalles($producto1)
