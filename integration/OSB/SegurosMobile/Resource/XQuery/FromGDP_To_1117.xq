xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$getDetallesPoliza1" element="ns0:getDetallesPoliza" location="../WSDL/SegurosMobile_types.wsdl" ::)
(:: pragma bea:global-element-return element="ns1:executeRequest" location="../../../OV/Resource/WSDL/OVSvc.wsdl" ::)

declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/FromGDP_To_1101/";
declare namespace ns1 = "http://cms.services.qbe.com/";
declare namespace ns0 = "http://cms.services.qbe.com/segurosmobile";

declare function xf:FromGDP_To_1101($getDetallesPoliza1 as element(ns0:getDetallesPoliza))
    as element(ns1:executeRequest) {
        <ns1:executeRequest>
			<actionCode>lbaw_GetConsultaMQGestion</actionCode>
			<Request>
				<DEFINICION>1117_Coberturas.xml</DEFINICION>
				<USUARCOD></USUARCOD>
				<RAMOPCOD>{data($getDetallesPoliza1/arg0/ramopcod)}</RAMOPCOD>
				<POLIZANN>{data($getDetallesPoliza1/arg0/polizann)}</POLIZANN>
				<POLIZSEC>{data($getDetallesPoliza1/arg0/polizsec)}</POLIZSEC>
				<CERTIPOL>{data($getDetallesPoliza1/arg0/certipol)}</CERTIPOL>
				<CERTIANN>{data($getDetallesPoliza1/arg0/certiann)}</CERTIANN>
				<CERTISEC>{data($getDetallesPoliza1/arg0/certisec)}</CERTISEC>
				<SUPLENUM>0</SUPLENUM> (: FIXME si lo manda, copiarle el valor :)
			</Request>
            <contextInfo/>
        </ns1:executeRequest>
};

declare variable $getDetallesPoliza1 as element(ns0:getDetallesPoliza) external;

xf:FromGDP_To_1101($getDetallesPoliza1)
