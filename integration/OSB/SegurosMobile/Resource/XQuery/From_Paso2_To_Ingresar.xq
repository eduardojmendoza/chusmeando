xquery version "1.0" encoding "UTF-8";
(:: pragma bea:schema-type-parameter parameter="$ingresarPaso21" type="ns0:ingresarPaso2" location="../WSDL/SegurosMobile_types.wsdl" ::)
(:: pragma bea:global-element-return element="ns1:executeRequest" location="../../../OV/Resource/WSDL/OVSvc.wsdl" ::)

declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/From_Paso2_To_Ingresar/";
declare namespace ns1 = "http://cms.services.qbe.com/";
declare namespace ns0 = "http://cms.services.qbe.com/segurosmobile";

declare function xf:TipoDoc_Tabla() as element() {

    	let $tipoDocs := <TIPOSDOC> <TIPODOC codigo="1" nombre="DNI"/>,
               <TIPODOC codigo="2" nombre="LE"/>,
               <TIPODOC codigo="3" nombre="LC"/>,
               <TIPODOC codigo="4" nombre="CUIT"/>,
               <TIPODOC codigo="5" nombre="CUIL"/>,
               <TIPODOC codigo="6" nombre="CI"/>,
               <TIPODOC codigo="47" nombre="PASAPORTE"/>,
               <TIPODOC codigo="99" nombre="OTRO"/>,
               </TIPOSDOC>
		return $tipoDocs
};

declare function xf:TipoDoc_Nombre_To_Codigo($nombre as xs:string)
    as xs:string {

    	let $tipoDocs := xf:TipoDoc_Tabla()
		return $tipoDocs/TIPODOC[@nombre=$nombre]/@codigo
};

declare function xf:TipoDoc_Codigo_To_Nombre($codigo as xs:string)
    as xs:string {

    	let $tipoDocs := xf:TipoDoc_Tabla()
		return $tipoDocs/TIPODOC[@codigo=xs:int($codigo)]/@nombre
};

declare function xf:From_Paso2_To_Ingresar($ingresarPaso21 as element())
    as element(ns1:executeRequest) {
        <ns1:executeRequest>
        <actionCode>nbwsA_Ingresar</actionCode>
		<Request>
			<IPORIGEN>{data($ingresarPaso21/ipOrigen)}</IPORIGEN>
			<PASSWORD>{data($ingresarPaso21/password)}</PASSWORD>
			<USUARIO>{data($ingresarPaso21/usuario)}</USUARIO>
			<DOCUMTIP>{xf:TipoDoc_Nombre_To_Codigo(data($ingresarPaso21/tipoDocumento))}</DOCUMTIP>
			<RCCs>
				<rcc id="1">{data($ingresarPaso21/digitos/digito[id='1']/valor)}</rcc>
			    <rcc id="2">{data($ingresarPaso21/digitos/digito[id="2"]/valor)}</rcc>
			    <rcc id="3">{data($ingresarPaso21/digitos/digito[id="3"]/valor)}</rcc>
			</RCCs>
		</Request>
		<contextInfo/>
		</ns1:executeRequest>
};

declare variable $ingresarPaso21 as element() external;

xf:From_Paso2_To_Ingresar($ingresarPaso21)
