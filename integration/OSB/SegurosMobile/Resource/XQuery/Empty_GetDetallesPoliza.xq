xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-return element="ns0:getDetallesPolizaResponse" location="../WSDL/SegurosMobile_types.wsdl" ::)

declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/Empty_GetDetallesPoliza/";
declare namespace ns0 = "http://cms.services.qbe.com/segurosmobile";

declare function xf:Empty_GetDetallesPoliza()
as element(ns0:getDetallesPolizaResponse) {
       <ns0:getDetallesPolizaResponse>
        <return>
        <!-- Ocurrio un error al recuperar los detalles de esta poliza -->
         <operpte></operpte>
         <cliensec></cliensec>
         <cliendes></cliendes>
         <domicsec></domicsec>
         <cuentsec></cuentsec>
         <situcpol></situcpol>
         <acreedor></acreedor>
         <fecini></fecini>
         <fecultre></fecultre>
         <vigenciaDesde></vigenciaDesde>
         <vigenciaHasta></vigenciaHasta>
         <cobrocod></cobrocod>
         <cobrodab></cobrodab>
         <cobrotip></cobrotip>
         <cobrodes></cobrodes>
         <cuentnum></cuentnum>
         <campacod></campacod>
         <campades></campades>
         <grupoase></grupoase>
         <clubeco></clubeco>
         <tarjecod></tarjecod>
         <optp6511></optp6511>

        </return>
        </ns0:getDetallesPolizaResponse>
};


xf:Empty_GetDetallesPoliza()
