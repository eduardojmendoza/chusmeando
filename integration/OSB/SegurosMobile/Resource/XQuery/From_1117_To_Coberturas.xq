xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$executeRequestResponse1" element="ns1:executeRequestResponse" location="../../../OV/Resource/WSDL/OVSvc.wsdl" ::)
(:: pragma bea:schema-type-return type="ns0:Coberturas" location="../WSDL/SegurosMobile_types.wsdl" ::)

declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/From_1117_To_Coberturas/";
declare namespace ns1 = "http://cms.services.qbe.com/";
declare namespace ns0 = "http://cms.services.qbe.com/segurosmobile";

declare function xf:From_1117_To_Coberturas($executeRequestResponse1 as element(ns1:executeRequestResponse))
    as element() {
        <coberturas>
			<codigoPlan>{data($executeRequestResponse1/return/Response/CAMPOS/PLANNCOD)}</codigoPlan>
			<descripcionPlan>{data($executeRequestResponse1/return/Response/CAMPOS/PLANNDAB)}</descripcionPlan>
			<sumaLBA>{data($executeRequestResponse1/return/Response/CAMPOS/SUMALBA)}</sumaLBA>
			<sumaAsegurada>{data($executeRequestResponse1/return/Response/CAMPOS/SUMAASEG)}</sumaAsegurada>
		</coberturas>
};

declare variable $executeRequestResponse1 as element(ns1:executeRequestResponse) external;

xf:From_1117_To_Coberturas($executeRequestResponse1)
