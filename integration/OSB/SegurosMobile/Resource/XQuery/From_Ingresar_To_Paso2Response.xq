xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$executeRequestResponse1" element="ns1:executeRequestResponse" location="../../../OV/Resource/WSDL/OVSvc.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:ingresarPaso2Response" location="../WSDL/SegurosMobile_types.wsdl" ::)

declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/From_Ingresar_To_Paso2Response/";
declare namespace ns1 = "http://cms.services.qbe.com/";
declare namespace ns0 = "http://cms.services.qbe.com/segurosmobile";

declare function xf:TipoDoc_Tabla() as element() {

    	let $tipoDocs := <TIPOSDOC> <TIPODOC codigo="1" nombre="DNI"/>,
               <TIPODOC codigo="2" nombre="LE"/>,
               <TIPODOC codigo="3" nombre="LC"/>,
               <TIPODOC codigo="4" nombre="CUIT"/>,
               <TIPODOC codigo="5" nombre="CUIL"/>,
               <TIPODOC codigo="6" nombre="CI"/>,
               <TIPODOC codigo="47" nombre="PASAPORTE"/>,
               <TIPODOC codigo="99" nombre="OTRO"/>,
               </TIPOSDOC>
		return $tipoDocs
};

declare function xf:TipoDoc_Nombre_To_Codigo($nombre as xs:string)
    as xs:string {

    	let $tipoDocs := xf:TipoDoc_Tabla()
		return $tipoDocs/TIPODOC[@nombre=$nombre]/@codigo
};

declare function xf:TipoDoc_Codigo_To_Nombre($codigo as xs:string)
    as xs:string {

    	let $tipoDocs := xf:TipoDoc_Tabla()
		return $tipoDocs/TIPODOC[@codigo=xs:int($codigo)]/@nombre
};

declare function xf:From_Ingresar_To_Paso2Response($executeRequestResponse1 as element(ns1:executeRequestResponse))
    as element(ns0:ingresarPaso2Response) {
        <ns0:ingresarPaso2Response>
         <return>
         	{
                if ( data($executeRequestResponse1/return/Response/Estado/@resultado) = "false" ) then (
               		(: FIXME esto es otro tipo de error, deberia levantar una Fault :)
               		<type>IngresoErroneo</type>,
               		<codError>99</codError>,
               		<codResultado>99</codResultado>,
               		<msgResultado>Error interno al ejecutar el llamado a CMS</msgResultado>
                ) else if ( data($executeRequestResponse1/return/Response/CODRESULTADO) = "ERR") then (
               		<type>IngresoErroneo</type>,
               		<codError>{data($executeRequestResponse1/return/Response/CODERROR)}</codError>,
               		<codResultado>{data($executeRequestResponse1/return/Response/CODRESULTADO)}</codResultado>,
               		<msgResultado>{data($executeRequestResponse1/return/Response/MSGRESULTADO)}</msgResultado>
                ) else if ( data($executeRequestResponse1/return/Response/INGRESO) = "OK") then (
               			<type>IngresoActivo</type>,
						<resultado>{data($executeRequestResponse1/return/Response/INGRESO)}</resultado>,
			            <cliente>
			               <nombre>{data($executeRequestResponse1/return/Response/CLIENNOM)}</nombre>
			               <apellido>{data($executeRequestResponse1/return/Response/CLIENAPE)}</apellido>
			            </cliente>,
			            <lastLogon>{data($executeRequestResponse1/return/Response/LASTLOGON)}</lastLogon>,
			            <documento>
			               <tipo>{xf:TipoDoc_Codigo_To_Nombre(data($executeRequestResponse1/return/Response/DOCUMTIP))}</tipo>
			               <numero>{data($executeRequestResponse1/return/Response/DOCUMDAT)}</numero>
			            </documento>
	            ) else if ( data($executeRequestResponse1/return/Response/INGRESO="CAI") and data($executeRequestResponse1/return/Response/CODRESULTADO="ERR")) then (
               			<type>CAI: NO_IMPLEMENTADO_AUN</type>
(:           wvarBodyResp = "<INGRESO>CAI</INGRESO>" + "<CODRESULTADO>ERR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "<ACCION>" + wvarProximoPaso + "</ACCION>" + "<COMBOPID>" + wvarComboPID + "</COMBOPID>" + "<VIA_INSCRIPCION>" + wvarVIA_INSCRIPCION + "</VIA_INSCRIPCION>";
:)
	            ) else if ( data($executeRequestResponse1/return/Response/INGRESO="CAI")) then (
               			<type>PrimerIngreso</type>,
						<medioIngreso>CAI</medioIngreso>,
						<accion>{data($executeRequestResponse1/return/Response/ACCION)}</accion>,
						<viaInscripcion>{data($executeRequestResponse1/return/Response/VIA_INSCRIPCION)}</viaInscripcion>,
						<polizas>
                        {
                            for $item in $executeRequestResponse1/return/Response/COMBOPID/OPTION[starts-with(@value,"AUS1")]
                            return
                            (
                                <poliza>
                                	<cobrodes>{data($item/@COBRODES)}</cobrodes>
                                	<numeroMotor>{data($item/@DATO2)}</numeroMotor>
                                	<patente>{data($item/@DATO3)}</patente>
                                	<pid>{data($item/@PID)}</pid>
                                	<codigoPoliza>{data($item/@value)}</codigoPoliza>
                                	<textoPoliza>{data($item)}</textoPoliza>
                                </poliza>
(:
    <COMBOPID>
      <OPTION COBRODES="EP" DATO2="T85068417" DATO3="JMD293" PID="A" value="APL1|0|6859|6859|13|1852">SEGUROS DEL AUTOMOTOR PLANES (APL1-00-006859/6859-0013-001852)</OPTION>
      <OPTION COBRODES="VI" DATO2="T85068417" DATO3="JMD293" PID="A" value="AUS1|00|000001|0000|0001|559139">AUTOSCORING (AUS1-00-000001/0000-0001-559139)</OPTION>
    </COMBOPID>
:)
                            )
                        }
                  		</polizas>
	            ) else if ( data($executeRequestResponse1/return/Response/INGRESO="CAP")) then (
               			<type>CAP: NO_IMPLEMENTADO_AUN</type>
	            ) else (
               			<type>CASO_NO_IMPLEMENTADO_AUN</type>
				)
			}
         </return>
      </ns0:ingresarPaso2Response>
};

declare variable $executeRequestResponse1 as element(ns1:executeRequestResponse) external;

xf:From_Ingresar_To_Paso2Response($executeRequestResponse1)
