(:: pragma bea:global-element-parameter parameter="$executeRequestResponse1" element="ns1:executeRequestResponse" location="../../../OV/Resource/WSDL/OVSvc.wsdl" ::)
(:: pragma bea:schema-type-return type="ns0:Riesgo" location="../WSDL/SegurosMobile_types.wsdl" ::)

declare namespace ns1 = "http://cms.services.qbe.com/";
declare namespace ns0 = "http://cms.services.qbe.com/segurosmobile";
declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/From_1202_To_Riesgo/";

declare function xf:From_1202_To_Riesgo($executeRequestResponse1 as element(ns1:executeRequestResponse))
    as element() {
        <riesgo>
			<numeroMotor>{data($executeRequestResponse1/return/Response/CAMPOS/MOTORNUM)}</numeroMotor>
			<numeroChasis>{data($executeRequestResponse1/return/Response/CAMPOS/CHASINUM)}</numeroChasis>
			<marca>{data($executeRequestResponse1/return/Response/CAMPOS/AUMARDES)}</marca>
			<modelo>{data($executeRequestResponse1/return/Response/CAMPOS/AUMODDES)} {data($executeRequestResponse1/return/Response/CAMPOS/AUSMODES)} {data($executeRequestResponse1/return/Response/CAMPOS/AUADIDES)}</modelo>
			<uso>{data($executeRequestResponse1/return/Response/CAMPOS/AUUSODES)}</uso>
			<color>{data($executeRequestResponse1/return/Response/CAMPOS/AUCOLDES)}</color>
        </riesgo>
};


declare variable $executeRequestResponse1 as element(ns1:executeRequestResponse) external;

xf:From_1202_To_Riesgo($executeRequestResponse1)
