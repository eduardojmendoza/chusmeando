xquery version "1.0" encoding "UTF-8";

declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/TipoDoc_To_Int/";

declare function xf:TipoDoc_Tabla() as element() {

    	let $tipoDocs := <TIPOSDOC> <TIPODOC codigo="1" nombre="DNI"/>,
               <TIPODOC codigo="2" nombre="LE"/>,
               <TIPODOC codigo="3" nombre="LC"/>,
               <TIPODOC codigo="4" nombre="CUIT"/>,
               <TIPODOC codigo="5" nombre="CUIL"/>,
               <TIPODOC codigo="6" nombre="CI"/>,
               <TIPODOC codigo="47" nombre="PASAPORTE"/>,
               <TIPODOC codigo="99" nombre="OTRO"/>,
               </TIPOSDOC>
		return $tipoDocs
};

declare function xf:TipoDoc_Nombre_To_Codigo($nombre as xs:string)
    as xs:string {

    	let $tipoDocs := xf:TipoDoc_Tabla()
		return $tipoDocs/TIPODOC[@nombre=$nombre]/@codigo
};

declare function xf:TipoDoc_Codigo_To_Nombre($codigo as xs:string)
    as xs:string {

    	let $tipoDocs := xf:TipoDoc_Tabla()
		return $tipoDocs/TIPODOC[@codigo=xs:int($codigo)]/@nombre
};

declare variable $nombre as xs:string external;

xf:TipoDoc_Nombre_To_Codigo($nombre)
