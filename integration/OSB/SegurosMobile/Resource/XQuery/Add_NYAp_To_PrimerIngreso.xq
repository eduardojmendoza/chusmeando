(:: pragma bea:global-element-parameter parameter="$primerIngresoResponse1" element="ns0:primerIngresoResponse" location="../WSDL/SegurosMobile_types.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$productosClienteResponse1" element="ns0:productosClienteResponse" location="../WSDL/SegurosMobile_types.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:primerIngresoResponse" location="../WSDL/SegurosMobile_types.wsdl" ::)

declare namespace ns0 = "http://cms.services.qbe.com/segurosmobile";
declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/Add_NYAp_To_PrimerIngreso/";

declare function xf:Add_NYAp_To_PrimerIngreso($primerIngresoResponse1 as element(ns0:primerIngresoResponse),
    $productosClienteResponse1 as element(ns0:productosClienteResponse))
    as element(ns0:primerIngresoResponse) {
        <ns0:primerIngresoResponse>
            <return>
                <resultadoIngreso>{ $primerIngresoResponse1/return/resultadoIngreso/@* , $primerIngresoResponse1/return/resultadoIngreso/node() }</resultadoIngreso>
                <dataInicio>{ $productosClienteResponse1/return/@* , $productosClienteResponse1/return/node() }</dataInicio>
            </return>
        </ns0:primerIngresoResponse>
};

declare variable $primerIngresoResponse1 as element(ns0:primerIngresoResponse) external;
declare variable $productosClienteResponse1 as element(ns0:productosClienteResponse) external;

xf:Add_NYAp_To_PrimerIngreso($primerIngresoResponse1,
    $productosClienteResponse1)
