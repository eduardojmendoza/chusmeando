xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$executeRequestResponse1" element="ns1:executeRequestResponse" location="../../../OV/Resource/WSDL/OVSvc.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:productosClienteResponse" location="../WSDL/SegurosMobile_types.wsdl" ::)

declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/From_sInicio_To_ProductosCliente/";
declare namespace ns1 = "http://cms.services.qbe.com/";
declare namespace ns0 = "http://cms.services.qbe.com/segurosmobile";


declare function xf:TipoDoc_Tabla() as element() {

    	let $tipoDocs := <TIPOSDOC> <TIPODOC codigo="1" nombre="DNI"/>,
               <TIPODOC codigo="2" nombre="LE"/>,
               <TIPODOC codigo="3" nombre="LC"/>,
               <TIPODOC codigo="4" nombre="CUIT"/>,
               <TIPODOC codigo="5" nombre="CUIL"/>,
               <TIPODOC codigo="6" nombre="CI"/>,
               <TIPODOC codigo="47" nombre="PASAPORTE"/>,
               <TIPODOC codigo="99" nombre="OTRO"/>,
               </TIPOSDOC>
		return $tipoDocs
};

declare function xf:TipoDoc_Nombre_To_Codigo($nombre as xs:string)
    as xs:string {

    	let $tipoDocs := xf:TipoDoc_Tabla()
		return $tipoDocs/TIPODOC[@nombre=$nombre]/@codigo
};

declare function xf:TipoDoc_Codigo_To_Nombre($codigo as xs:string)
    as xs:string {

    	let $tipoDocs := xf:TipoDoc_Tabla()
		return $tipoDocs/TIPODOC[@codigo=xs:int($codigo)]/@nombre
};

declare function xf:From_sInicio_To_ProductosCliente($executeRequestResponse1 as element(ns1:executeRequestResponse))
    as element(ns0:productosClienteResponse) {
        <ns0:productosClienteResponse>
			<return>
            {
                if ( data($executeRequestResponse1/return/Response/Estado/@resultado) = "false" ) then (
                    (: FIXME como levanto un fault? :)
                ) else (
		         <productos>
                        {
                            for $prod in $executeRequestResponse1/return/Response/Response_XML/Response/PRODUCTO
                            return
                            (
               <producto>
                  <ciaascod>{data($prod/CIAASCOD)}</ciaascod>
                  <ramopcod>{data($prod/RAMOPCOD)}</ramopcod>
                  <polizann>{data($prod/POLIZANN)}</polizann>
                  <polizsec>{data($prod/POLIZSEC)}</polizsec>
                  <certipol>{data($prod/CERTIPOL)}</certipol>
                  <certiann>{data($prod/CERTIANN)}</certiann>
                  <certisec>{data($prod/CERTISEC)}</certisec>
                  <suplenum>{data($prod/SUPLENUM)}</suplenum>
                  <ramopdes>{data($prod/RAMOPDES)}</ramopdes>
                  <cliensecv>{data($prod/CLIENSECV)}</cliensecv>
                  <clienap1v>{data($prod/CLIENAP1V)}</clienap1v>
                  <clienap2v>{data($prod/CLIENAP2V)}</clienap2v>
                  <cliennomv>{data($prod/CLIENNOMV)}</cliennomv>
                  <agentcod>{data($prod/AGENTCOD)}</agentcod>
                  <agentcla>{data($prod/AGENTCLA)}</agentcla>
                  <tomaries>{data($prod/TOMARIES)}</tomaries>
                  <situcpol>{data($prod/SITUCPOL)}</situcpol>
                  <emisiann>{data($prod/EMISIANN)}</emisiann>
                  <emisimes>{data($prod/EMISIMES)}</emisimes>
                  <emisidia>{data($prod/EMISIDIA)}</emisidia>
                  <tipoprod>{data($prod/TIPOPROD)}</tipoprod>
                  <patente>{data($prod/PATENTE)}</patente>
                  <dato1>{data($prod/DATO1)}</dato1>
                  <dato2>{data($prod/DATO2)}</dato2>
                  <dato3>{data($prod/DATO3)}</dato3>
                  <dato4>{data($prod/DATO4)}</dato4>
                  <cobrodes>{data($prod/COBRODES)}</cobrodes>
                  <swsuscri>{data($prod/SWSUSCRI)}</swsuscri>
                  <mail>{data($prod/MAIL)}</mail>
                  <clave>{data($prod/CLAVE)}</clave>
                  <swclave>{data($prod/SWCLAVE)}</swclave>
                  <mensasin>{data($prod/MENSASIN)}</mensasin>
                  <mensasus>{data($prod/MENSASUS)}</mensasus>
                  <swimprim>{data($prod/SWPRIM)}</swimprim>
                  <cobranza>{data($prod/COBRANZA)}</cobranza>
                  <siniestro>{data($prod/SINIESTRO)}</siniestro>
                  <constancia>{data($prod/CONSTANCIA)}</constancia>
                  <habilitadoNBWS>{data($prod/HABILITADO_NBWS)}</habilitadoNBWS>
                  <positiveid>{data($prod/POSITIVEID)}</positiveid>
                  <polizaExcluida>{data($prod/POLIZA_EXCLUIDA)}</polizaExcluida>
                  <habilitadoNavegacion>{data($prod/HABILITADO_NAVEGACION)}</habilitadoNavegacion>
                  <masDeCincoCert>{data($prod/MAS_DE_CINCO_CERT)}</masDeCincoCert>
                  <habilitadoEPoliza>{data($prod/HABILITADO_EPOLIZA)}</habilitadoEPoliza>
               </producto>
               )}
         </productos>,
			(:
			<CLIENSEC>101879610</CLIENSEC>
              <TIPODOCU>1</TIPODOCU>
              <NUMEDOCU>18069158</NUMEDOCU>
              <CLIENAP1>ARMATI</CLIENAP1>
              <CLIENAP2/>
              <CLIENNOM>MARIA ADRIANA</CLIENNOM>
              :)
              <usuarioInicio>
              	<cliensec>{data($executeRequestResponse1/return/Response/Response_XML/Response/CLIENSEC)}</cliensec>
              	<documento>
              		<tipo>{xf:TipoDoc_Codigo_To_Nombre(data($executeRequestResponse1/return/Response/Response_XML/Response/TIPODOCU))}</tipo>
              		<numero>{data($executeRequestResponse1/return/Response/Response_XML/Response/NUMEDOCU)}</numero>
              	</documento>
              	<apellido>{data($executeRequestResponse1/return/Response/Response_XML/Response/CLIENAP1)}{data($executeRequestResponse1/return/Response/Response_XML/Response/CLIENAP2)}</apellido>
              	<nombre>{data($executeRequestResponse1/return/Response/Response_XML/Response/CLIENNOM)}</nombre>
              </usuarioInicio>
         )}
         	</return>
        </ns0:productosClienteResponse>

};

declare variable $executeRequestResponse1 as element(ns1:executeRequestResponse) external;

xf:From_sInicio_To_ProductosCliente($executeRequestResponse1)
