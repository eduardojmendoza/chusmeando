xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$executeRequestResponse1" element="ns1:executeRequestResponse" location="../../../OV/Resource/WSDL/OVSvc.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:getDetallesPolizaResponse" location="../WSDL/SegurosMobile_types.wsdl" ::)

declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/From_1101_To_GDP/";
declare namespace ns1 = "http://cms.services.qbe.com/";
declare namespace ns0 = "http://cms.services.qbe.com/segurosmobile";

declare function xf:From_1101_To_GDP($executeRequestResponse1 as element(ns1:executeRequestResponse))
    as element(ns0:getDetallesPolizaResponse) {
        <ns0:getDetallesPolizaResponse>
        <return>
         <operpte>{data($executeRequestResponse1//CAMPOS/OPERPTE)}</operpte>
         <cliensec>{data($executeRequestResponse1//CAMPOS/CLIENSEC)}</cliensec>
         <cliendes>{data($executeRequestResponse1//CAMPOS/CLIENDES)}</cliendes>
         <domicsec>{data($executeRequestResponse1//CAMPOS/DOMICSEC)}</domicsec>
         <cuentsec>{data($executeRequestResponse1//CAMPOS/CUENTSEC)}</cuentsec>
         <situcpol>{data($executeRequestResponse1//CAMPOS/SITUCPOL)}</situcpol>
         <acreedor>{data($executeRequestResponse1//CAMPOS/ACREEDOR)}</acreedor>
         <fecini>{data($executeRequestResponse1//CAMPOS/FECINI)}</fecini>
         <fecultre>{data($executeRequestResponse1//CAMPOS/FECULTRE)}</fecultre>
         <vigenciaDesde>{substring-before($executeRequestResponse1//CAMPOS/FECULTRE,' - ')}</vigenciaDesde>
         <vigenciaHasta>{substring-after($executeRequestResponse1//CAMPOS/FECULTRE,' - ')}</vigenciaHasta>
         <cobrocod>{data($executeRequestResponse1//CAMPOS/COBROCOD)}</cobrocod>
         <cobrodab>{data($executeRequestResponse1//CAMPOS/COBRODAB)}</cobrodab>
         <cobrotip>{data($executeRequestResponse1//CAMPOS/COBROTIP)}</cobrotip>
         <cobrodes>{data($executeRequestResponse1//CAMPOS/COBRODES)}</cobrodes>
         <cuentnum>{data($executeRequestResponse1//CAMPOS/CUENTNUM)}</cuentnum>
         <campacod>{data($executeRequestResponse1//CAMPOS/CAMPACOD)}</campacod>
         <campades>{data($executeRequestResponse1//CAMPOS/CAMPADES)}</campades>
         <grupoase>{data($executeRequestResponse1//CAMPOS/GRUPOASE)}</grupoase>
         <clubeco>{data($executeRequestResponse1//CAMPOS/CLUBECO)}</clubeco>
         <tarjecod>{data($executeRequestResponse1//CAMPOS/TARJECOD)}</tarjecod>
         <optp6511>{data($executeRequestResponse1//CAMPOS/OPTP6511)}</optp6511>

        </return>
        </ns0:getDetallesPolizaResponse>
};

declare variable $executeRequestResponse1 as element(ns1:executeRequestResponse) external;

xf:From_1101_To_GDP($executeRequestResponse1)
