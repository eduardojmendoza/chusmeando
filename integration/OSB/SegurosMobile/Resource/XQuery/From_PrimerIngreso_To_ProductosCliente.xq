(:: pragma bea:global-element-parameter parameter="$primerIngresoResponse1" element="ns0:primerIngresoResponse" location="../WSDL/SegurosMobile_types.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:productosCliente" location="../WSDL/SegurosMobile_types.wsdl" ::)

declare namespace ns0 = "http://cms.services.qbe.com/segurosmobile";
declare namespace xf = "http://tempuri.org/SegurosMobile/Resource/XQuery/From_PrimerIngreso_To_ProductosCliente/";

declare function xf:From_PrimerIngreso_To_ProductosCliente($primerIngresoResponse1 as element(ns0:primerIngresoResponse))
    as element(ns0:productosCliente) {
        <ns0:productosCliente>
            <documento>
                <tipo>{ data($primerIngresoResponse1/return/dataInicio/usuarioInicio/documento/tipo) }</tipo>
                <numero>{ data($primerIngresoResponse1/return/dataInicio/usuarioInicio/documento/numero) }</numero>
            </documento>
        </ns0:productosCliente>
};

declare variable $primerIngresoResponse1 as element(ns0:primerIngresoResponse) external;

xf:From_PrimerIngreso_To_ProductosCliente($primerIngresoResponse1)
