xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_1109.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1109_To_PaginatedQueryRq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1109_To_PaginatedQueryRq($req as element(Request), $StartRow as xs:integer, $RowCount as xs:integer, $ResultSetID as xs:string)
    as element() {
        <ns0:PaginatedQueryRq>
            <ns0:QueryID>1109</ns0:QueryID>
            <ns0:StartRow>{ $StartRow }</ns0:StartRow>
            <ns0:RowCount>{ $RowCount }</ns0:RowCount>
            (:: <ns0:ResultSetID>{ $ResultSetID }</ns0:ResultSetID> ::)
            (:: <ns0:ResultSetID>PEH9O0HVCM</ns0:ResultSetID> ::)

            <ns0:FilterCriteria>
                {
                    if (data($req/NIVEL1) = '' and data($req/CLIENSEC1) = ''
                        and data($req/NIVEL2) = '' and data($req/CLIENSEC2) = ''
                        and data($req/NIVEL3) = '' and data($req/CLIENSEC3) = '')
                    then (
                        <ns0:FilterCriterion field = "NIVEL{ index-of(('GO', 'OR', 'PR'), upper-case(data($req/NIVELAS))) }"
                                             operation = "EQ"
                                             value = "{ data($req/NIVELCLAS) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC{ index-of(('GO', 'OR', 'PR'), upper-case(data($req/NIVELAS))) }"
                                             operation = "EQ"
                                             value = "{ data($req/CLIENSECAS) }"/>
                    )
                    else (
                        <ns0:FilterCriterion field = "NIVEL1"
                                             operation = "EQ"
                                             value = "{ data($req/NIVEL1) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC1"
                                             operation = "EQ"
                                             value = "{ data($req/CLIENSEC1) }"/>,
                        <ns0:FilterCriterion field = "NIVEL2"
                                             operation = "EQ"
                                             value = "{ data($req/NIVEL2) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC2"
                                             operation = "EQ"
                                             value = "{ data($req/CLIENSEC2) }"/>,
                        <ns0:FilterCriterion field = "NIVEL3"
                                             operation = "EQ"
                                             value = "{ data($req/NIVEL3) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC3"
                                             operation = "EQ"
                                             value = "{ data($req/CLIENSEC3) }"/>
                    )
                }
                <ns0:FilterCriterion field = "FECDES"
                                     operation = "EQ"
                                     value = "{ fn-bea:date-from-string-with-format("yyyyMMdd", data($req/FECDES)) }"/>
                <ns0:FilterCriterion field = "FECHAS"
                                     operation = "EQ"
                                     value = "{ fn-bea:date-from-string-with-format("yyyyMMdd", data($req/FECHAS)) }"/>
            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $req as element(Request) external;
declare variable $StartRow as xs:integer external;
declare variable $RowCount as xs:integer external;
declare variable $ResultSetID as xs:string external;
xf:OV_1109_To_PaginatedQueryRq($req, $StartRow, $RowCount, $ResultSetID)