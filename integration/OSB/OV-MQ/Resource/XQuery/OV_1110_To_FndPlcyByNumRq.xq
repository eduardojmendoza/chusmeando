xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_1110.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:FindPolicyByNumberRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1110/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1110($req as element(Request))
    as element() {
        <ns0:FindPolicyByNumberRq>
            <ns0:PolicyNo>
               {
                    concat('0001',
                           fn-bea:pad-right($req/PRODUCTO, 4) ,
                           fn-bea:format-number(xs:double($req/POLIZA), '00000000') ,
                           fn-bea:format-number(xs:double($req/CERTI), '00000000000000')
                    )
               }
            </ns0:PolicyNo>
        </ns0:FindPolicyByNumberRq>
};

declare variable $req as element(Request) external;

xf:OV_1110($req)