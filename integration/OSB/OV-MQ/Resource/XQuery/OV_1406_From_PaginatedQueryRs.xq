	xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1406.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1406_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1406_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <MSGEST>OK</MSGEST>,
                    <REGS>
                    {
                    	for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row
                    	return
                    	(
                        	<REG>
								<RAMO>
                                    {
                                    	let $ramo := data($item/ns0:Column[@name = 'RAMO'])
                                    	return
											if ($ramo = 'M') then (1)
                                   			else if ($ramo = 'C') then (2)
                                    		else ()
                                    }
								</RAMO>
                        		<PROD>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),5,4) }</PROD>
                        		<POL>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),9,8) }</POL>
                        		<CERPOL>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),17,4) }</CERPOL>
                        		<CERANN>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),21,4) }</CERANN>
                        		<CERSEC>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),25,6) }</CERSEC>
                        		<FECENV>{ data($item/ns0:Column[@name = 'FECENV']) }</FECENV> 
                        		<MON>{ data($item/ns0:Column[@name = 'MON']) }</MON>
                        		<IMP>{ replace(data($item/ns0:Column[@name = 'IMP']), '\.', ',') }</IMP>
                        		<CLIDES>{ data($item/ns0:Column[@name = 'CLIDES']) }</CLIDES>
                        		<COD>{ concat(substring(data($item/ns0:Column[@name = 'COD']), 1, 2), '-', substring(data($item/ns0:Column[@name = 'COD']), 3, 4)) }</COD>
                        		<SINI>{ data($item/ns0:Column[@name = 'SINI']) }</SINI>
							</REG>
						)
					}
                    </REGS>

            	)
			}
        </Response>
};

declare variable $paginatedQueryRs1 as element() external;

xf:OV_1406_From_PaginatedQueryRs($paginatedQueryRs1)
