	xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1407.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1407_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1407_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <REGS>
                    {
                    	for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row
                    	return
                    	(
                        	<REG>
                        		<CLIDES>{ data($item/ns0:Column[@name = 'CLIDES']) }</CLIDES>
								<PROD>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),5,4) }</PROD>
								<POL>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),9,8) }</POL>
                        		<CERPOL>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),17,4) }</CERPOL>
                        		<CERANN>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),21,4) }</CERANN>
                        		<CERSEC>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),25,6) }</CERSEC>
                        		<ENDOSO>{ fn-bea:format-number(xs:double(data($item/ns0:Column[@name = 'ENDOSO'])), '000000') }</ENDOSO>
                        		<EST>{data($item/ns0:Column[@name = 'EST'])}</EST>	
                        		<FECEMI>{ fn-bea:date-to-string-with-format("dd/MM/yyyy", data($item/ns0:Column[@name = 'FECEMI'])) }</FECEMI>
                        		<PRIMA>{ replace(replace(replace(data(fn-bea:format-number(data($item/ns0:Column[@name = 'PRIMA']),'#,###.00')), '\.', 'c'), '\,', '.'), 'c', ',') }</PRIMA>
                        		<IVA>{ replace(replace(replace(data(fn-bea:format-number(data($item/ns0:Column[@name = 'IVA']),'#,###.00')), '\.', 'c'), '\,', '.'), 'c', ',') }</IVA>
                        		<IVARET>{ data($item/ns0:Column[@name = 'IVARET']) }</IVARET>
                        		<IMPTOT>{ replace(replace(replace(data(fn-bea:format-number(data($item/ns0:Column[@name = 'IMPTOT']),'#,###.00')), '\.', 'c'), '\,', '.'), 'c', ',') }</IMPTOT>		
                        		<CANAL>{  data($item/ns0:Column[@name = 'CANAL'])  }</CANAL>
                        		<RAMO>{
                        		data($item/ns0:Column[@name = 'RAMO'])
                        		}</RAMO>
                        		<MON>{ data($item/ns0:Column[@name = 'MON']) }</MON>
							</REG>
						)
					}
                    </REGS>
            	)
			}
        </Response>
};

declare variable $paginatedQueryRs1 as element() external;

xf:OV_1407_From_PaginatedQueryRs($paginatedQueryRs1)
