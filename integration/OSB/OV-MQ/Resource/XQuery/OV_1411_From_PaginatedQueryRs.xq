xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1411.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1411_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1411_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
					<MSGEST>OK</MSGEST>,
					<REGS>
				 	{
	            		for $row in $paginatedQueryRs1/ns0:RowSet/ns0:Row
	            		return
	            		<REG>
	            			<ENDOSO>{ fn-bea:format-number(xs:double(data($row/ns0:Column[@name = 'ENDOSO'])), '000000')}</ENDOSO>
	            			<RECNUM>{ data($row/ns0:Column[@name = 'RECNUM']) }</RECNUM>
	            			<IMPPRIMA>{ data($row/ns0:Column[@name = 'IMPRIMA']) }</IMPPRIMA>
	            			<IMPTOT>{ data($row/ns0:Column[@name = 'IMPTOT']) }</IMPTOT>
	            			<COB>{ data($row/ns0:Column[@name = 'COB'])}</COB>
	            			<FECVTO>{ fn-bea:date-to-string-with-format('dd/MM/yyyy', data($row/ns0:Column[@name = 'FECVTO'])) }</FECVTO>
	            			<MON>{ data($row/ns0:Column[@name = 'MON']) }</MON>
	            			<EST>{ data($row/ns0:Column[@name = 'EST']) }</EST>
	            			<FECCOB>
	            			{ 
	            			if (data($row/ns0:Column[@name = 'FECCOB']) != '') then 
	            			fn-bea:date-to-string-with-format('dd/MM/yyyy', data($row/ns0:Column[@name = 'FECCOB'])) 
	            			else ()
	            			}
	            			</FECCOB>
	            			<IMP>
	            				{ 
	            				data($row/ns0:Column[@name = 'IMP'])
	            				}
	            			</IMP>
	            			<ORI>{ data($row/ns0:Column[@name = 'ORI']) }</ORI>
	            			<ORDENJUDICIAL>{ data($row/ns0:Column[@name = 'ORDENJUDICIAL']) }</ORDENJUDICIAL>
	                    </REG>
	                }
	                </REGS>

            	)
			}
        </Response>
};

declare variable $paginatedQueryRs1 as element() external;

xf:OV_1411_From_PaginatedQueryRs($paginatedQueryRs1)
