xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_1416.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1416_To_PaginatedQueryRq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1416_To_PaginatedQueryRq($req as element(Request), $StartRow as xs:integer, $RowCount as xs:integer, $ResultSetID as xs:string)
    as element() {
        <ns0:PaginatedQueryRq>
        	<ns0:QueryID>1416</ns0:QueryID>
        	<ns0:StartRow>{ $StartRow }</ns0:StartRow>
            <ns0:RowCount>{ $RowCount }</ns0:RowCount>
            <ns0:FilterCriteria>
                <ns0:FilterCriterion field = "NIVELAS"
                                     operation = "EQ"
                                     value = "{ data($req/NIVELAS) }" />
                <ns0:FilterCriterion field = "CLIENSECAS"
                                     operation = "EQ"
                                     value = "{ data($req/CLIENSECAS) }" />
                <ns0:FilterCriterion field = "POLICY_NO"
                                     operation = "EQ"
                                     value = "{
                                     	for $pol at $i in $req/POLIZAS/POLIZA
                                     	return
                                         let $producto := data($pol/PROD)
                                         let $poliza := data($pol/POL)
                                         let $certi := data($pol/CERTI)
                                         return
                                                 concat(xqubh:buildPolicyNo('0001', data($pol/PROD), data($pol/POL), data($pol/CERTI)), 
                                                        if (count($req/POLIZAS/POLIZA) != $i) then ',' else '')
                                     }" />
                 {
                 	if( exists($req/FLAGBUSQ) and data($req/FLAGBUSQ) != '' )
                 	then( <ns0:FilterCriterion field = "FLAGBUSQ"
                                     operation = "EQ"
                                     value = "{ data($req/FLAGBUSQ) }" /> )
                    else()
                 }
            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $req as element(Request) external;
declare variable $StartRow as xs:integer external;
declare variable $RowCount as xs:integer external;
declare variable $ResultSetID as xs:string external;

xf:OV_1416_To_PaginatedQueryRq($req, $StartRow, $RowCount, $ResultSetID)