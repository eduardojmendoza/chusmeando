(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/CommStructure.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$reg" element="REG" location="../XSD/CommStructure.xsd" ::)
(:: pragma bea:global-element-return element="Request" location="../XSD/CommStructure.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/SetCommStructure/";

declare function xf:SetCommStructure($request as element(Request),
    $reg as element(REG))
    as element(Request) {
        <Request>
        (: Los tags USUARIO y USERCOD no se usan ni se envian a INSIS, asi que no es necesario pasar el que venga :)
            <USUARIO/>
            <NIVELAS>{ data($reg/NIVEL) }</NIVELAS>
            <CLIENSECAS>{ data($reg/CLIENSEC) }</CLIENSECAS>
            <NIVEL1/>
            <CLIENSEC1/>
            <NIVEL2/>
            <CLIENSEC2/>
            <NIVEL3/>
            <CLIENSEC3/>
        </Request>
};

declare variable $request as element(Request) external;
declare variable $reg as element(REG) external;

xf:SetCommStructure($request,
    $reg)