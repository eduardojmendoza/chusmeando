xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$totsAis" element="TOTS" location="../XSD/OV_Merge_TOTS.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$totsInsis" element="TOTS" location="../XSD/OV_Merge_TOTS.xsd" ::)
(:: pragma bea:global-element-return element="TOTS" location="../XSD/OV_Merge_TOTS.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_Merge_TOTS/";

declare function xf:OV_Merge_TOTS($totsAis as element(),$totsInsis as element())
    as element(TOTS) {
            
		<TOTS>
            <TOT>
            	<TMON>$</TMON>
            	<T_IMPTOS/>
        	    <T_IMPTO>
            	{
                    replace(
            		local:format_Num_aQBE(local:sumar($totsAis/TOT[normalize-space(TMON)='$'][1]/T_IMPTO,$totsInsis/TOT[normalize-space(TMON)='$'][1]/T_IMPTO))
                    , '\.', '')
            	}
            	</T_IMPTO>
				<T_30S/>
				<T_30>
            	{
                    replace(
            		local:format_Num_aQBE(local:sumar($totsAis/TOT[normalize-space(TMON)='$'][1]/T_30,$totsInsis/TOT[normalize-space(TMON)='$'][1]/T_30))
                    , '\.', '')
            	}
				</T_30>
				<T_60S/>
				<T_60>
            	{
                    replace(
            		local:format_Num_aQBE(local:sumar($totsAis/TOT[normalize-space(TMON)='$'][1]/T_60,$totsInsis/TOT[normalize-space(TMON)='$'][1]/T_60))
                    , '\.', '')
            	}
				</T_60>
				<T_90S/>
				<T_90>
            	{
					replace(
            		local:format_Num_aQBE(local:sumar($totsAis/TOT[normalize-space(TMON)='$'][1]/T_90,$totsInsis/TOT[normalize-space(TMON)='$'][1]/T_90))
					, '\.', '')
				}
				</T_90>
				<T_M90S/>
				<T_M90>
            	{
					replace(
            		local:format_Num_aQBE(local:sumar($totsAis/TOT[normalize-space(TMON)='$'][1]/T_M90,$totsInsis/TOT[normalize-space(TMON)='$'][1]/T_M90))
					, '\.', '')
				}
				</T_M90>
			</TOT>
            <TOT>
            	<TMON>U$S</TMON>
            	<T_IMPTOS/>
        	    <T_IMPTO>
            	{
					replace(
            		local:format_Num_aQBE(local:sumar($totsAis/TOT[normalize-space(TMON)='U$S'][1]/T_IMPTO,$totsInsis/TOT[normalize-space(TMON)='U$S'][1]/T_IMPTO))
					, '\.', '')
				}
            	</T_IMPTO>
				<T_30S/>
				<T_30>
            	{
					replace(
            		local:format_Num_aQBE(local:sumar($totsAis/TOT[normalize-space(TMON)='U$S'][1]/T_30,$totsInsis/TOT[normalize-space(TMON)='U$S'][1]/T_30))
					, '\.', '')
				}
				</T_30>
				<T_60S/>
				<T_60>
            	{
					replace(
            		local:format_Num_aQBE(local:sumar($totsAis/TOT[normalize-space(TMON)='U$S'][1]/T_60,$totsInsis/TOT[normalize-space(TMON)='U$S'][1]/T_60))
					, '\.', '')
				}
				</T_60>
				<T_90S/>
				<T_90>
            	{
					replace(
            		local:format_Num_aQBE(local:sumar($totsAis/TOT[normalize-space(TMON)='U$S'][1]/T_90,$totsInsis/TOT[normalize-space(TMON)='U$S'][1]/T_90))
					, '\.', '')
				}
				</T_90>
				<T_M90S/>
				<T_M90>
            	{
					replace(
            		local:format_Num_aQBE(local:sumar($totsAis/TOT[normalize-space(TMON)='U$S'][1]/T_M90,$totsInsis/TOT[normalize-space(TMON)='U$S'][1]/T_M90))
					, '\.', '')
				}
				</T_M90>
			</TOT>
		</TOTS>

};

declare variable $totsAis as element() external;
declare variable $totsInsis as element() external;

declare function local:sumar($ais as element()?, $insis as element()?)
    as xs:decimal {
	
	let $aiss := if (exists($ais) and data($ais) != '') then replace(replace(data($ais),'\.',''),',','.')
						else ('0.0')
	let $total := xs:double($aiss) + xs:double(data($insis))
	return xs:decimal($total)
};

declare function local:format_Num_aQBE($input as xs:decimal)
    as xs:string {
	
	if(string(number($input)) != 'NaN') then (
		replace(replace(replace(xs:string(fn-bea:format-number(fn-bea:decimal-round($input,2),'#,##0.0#')),',','!'),'\.',','),'!','.')
		(:fn-bea:format-number(fn-bea:decimal-round($input,2),'#,##0.0#'):)
	) else (
		'0.00'
	)
	
};

xf:OV_Merge_TOTS($totsAis,$totsInsis)