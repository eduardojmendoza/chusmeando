(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$QbeFindPolicyCoversRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeFindPolicyCoversRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1110.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1110_From_FndPlcyByNumRs/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:OV_1110_From_FndPlcyByNumRs($LOVs as element(), $QbeFindPolicyCoversRs as element())
    as element(Response) {
        <Response>
            <Estado resultado = "true" mensaje = "" />
            <AFINIDAD>
            </AFINIDAD>
            <PLAN>{ normalize-space(data($QbeFindPolicyCoversRs/ns0:Insureds/ns0:Insured[1]/ns0:CoverPackages/ns0:CoverPackage[1]/ns0:PackageName)) }</PLAN>
            <FRANQ>{ data($LOVs/lov:LOV[@id = "FranchiseByPackage"]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</FRANQ>
            <BOTON>
                {
                    if (exists($QbeFindPolicyCoversRs/ns0:Insureds/ns0:Insured[1]/ns0:CoverPackages/ns0:CoverPackage[1]/ns0:PackageID)) then
                        'S'
                    else
                        ''
                }
            </BOTON>
            <REGS>
                {
                    for $CoverPackage in $QbeFindPolicyCoversRs/ns0:Insureds/ns0:Insured[1]/ns0:CoverPackages/ns0:CoverPackage
                    return
                        for $Cover in $CoverPackage/ns0:Covers/ns0:Cover
                        return
                            <REG>
                                <COBERCOD>{ 
											if (data($LOVs/lov:LOV[@id = "CoverByInsisCode"]/ns0:ListItems/ns0:ListItem[normalize-space(ns0:ItemDescription) = normalize-space(data($Cover/ns0:CoverDescription))]/ns0:ItemID) != '') then
		                            			fn-bea:format-number(xs:double(data($LOVs/lov:LOV[@id = "CoverByInsisCode"]/ns0:ListItems/ns0:ListItem[normalize-space(ns0:ItemDescription) = normalize-space(data($Cover/ns0:CoverDescription))]/ns0:ItemID)),"000")
		                            		else ()
								}</COBERCOD>
                                <COBER>{ normalize-space(data($Cover/ns0:CoverDescription)) }</COBER>
                                <SUMA>{ translate(fn-bea:format-number(data($Cover/ns0:InsuredAmount), '#,##0.00'), '.,', ',.') }</SUMA>
                                <TIPRIES>02</TIPRIES>
                                <LUPA>N</LUPA>
                            </REG>
                }
            </REGS>
        </Response>
};

declare variable $LOVs as element(lov:LOVs) external;
declare variable $QbeFindPolicyCoversRs as element() external;

xf:OV_1110_From_FndPlcyByNumRs($LOVs, $QbeFindPolicyCoversRs)
