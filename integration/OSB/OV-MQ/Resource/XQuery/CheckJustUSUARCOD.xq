xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/CommStructure.xsd" ::)
(:: pragma bea:global-element-return xs:boolean ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/CheckJustUSUARCOD/";

declare function xf:CheckJustUSUARCOD($request as element(Request)) as xs:boolean {

      	   if (
      	   ((exists($request/USUARIO) and data($request/USUARIO) != '')
      	         or ( exists($request/USUARCOD) and data($request/USUARCOD) != '' )
      	       )
           and (not (exists($request/NIVELAS)) or data($request/NIVELAS) = '')
           and (not (exists($request/CLIENSECAS)) or data($request/CLIENSECAS) = '')
           and (not (exists($request/NIVEL1)) or data($request/NIVEL1) = '')
           and (not (exists($request/CLIENSEC1)) or data($request/CLIENSEC1) = '')
           and (not (exists($request/NIVEL2)) or data($request/NIVEL2) = '')
           and (not (exists($request/CLIENSEC2)) or data($request/CLIENSEC2) = '')
           and (not (exists($request/NIVEL3)) or data($request/NIVEL3) = '')
           and (not (exists($request/CLIENSEC3)) or data($request/CLIENSEC3) = '') ) then (
           true()
           ) else (
           false()
           )

};

declare variable $request as element(Request) external;

xf:CheckJustUSUARCOD($request)