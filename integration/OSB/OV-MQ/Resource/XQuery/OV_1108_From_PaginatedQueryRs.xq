(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QBETotalsRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1108.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1108_From_PaginatedQueryRs/";

declare function xf:OV_1108_From_PaginatedQueryRs($resp as element())
    as element(Response) {
        <Response>
            <Estado resultado = "true" mensaje = ""/>
            <EST>
                {
                    let $items := local:GetRow($resp)
                    return
                        $items/*
                }
               </EST>
        </Response>
};


declare function local:GetRow($row as element())
    as element() {
    <items>
        {
            if(exists($row) and count($row/*) > 0) then (
                for $rowChild in $row/*
                return
                    if( data($rowChild/@level) = 'GO') then (
                        <GO CLI = "{ data($rowChild/@cliensec1) }"
                            NOM = "{ data($rowChild/@name) }"
                            NIV = "{ data($rowChild/@level) }"
                            ESTSTR = "{ data($rowChild/@eststr) }"
                            TOTPOL = "{ data($rowChild/@TOTPOL) }">
                            {
                                let $items := local:GetRow($rowChild)
                                return
                                    $items/*
                            }
                        </GO>
                    ) else if( data($rowChild/@level) = 'OR') then (
                        <OR CLI = "{ data($rowChild/@cliensec2) }"
                            NOM = "{ data($rowChild/@name) }"
                            NIV = "{ data($rowChild/@level) }"
                            ESTSTR = "{ data($rowChild/@eststr) }"
                            TOTPOL = "{ data($rowChild/@TOTPOL) }">
                            {
                                let $items := local:GetRow($rowChild)
                                return
                                    $items/*
                            }
                        </OR>
                    ) else if( data($rowChild/@level) = 'PR') then (
                        <PR CLI = "{ data($rowChild/@cliensec3) }"
                            NOM = "{ data($rowChild/@name) }"
                            NIV = "{ data($rowChild/@level) }"
                            ESTSTR = "{ data($rowChild/@eststr) }"
                            TOTPOL = "{ replace(data($rowChild/@TOTPOL), '\,', '') }">
                            {
                                let $items := local:GetRow($rowChild)
                                return
                                    $items/*
                            }
                        </PR>
                    ) else ()
            ) else ()
        }
    </items>
};


declare variable $resp as element() external;

xf:OV_1108_From_PaginatedQueryRs($resp)
