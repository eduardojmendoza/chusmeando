	xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1301.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1301_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1301_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <MSGEST>OK</MSGEST>,
                    <REGS>
                    {
                    	for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row
                    	return
                    	(
                        	<REG>
								<PROD>{ substring(data($item/ns0:Column[@name = 'CLAIM_REQUEST_ID']),5,4) }</PROD>
								<SINIAN>{ substring(data($item/ns0:Column[@name = 'CLAIM_REQUEST_ID']),9,2) }</SINIAN>
								<SININUM>{ substring(data($item/ns0:Column[@name = 'CLAIM_REQUEST_ID']),11,6) }</SININUM>
								<CLIDES>{ data($item/ns0:Column[@name = 'CLIDES']) }</CLIDES>
								{
                            	let $ramo := data($item/ns0:Column[@name = 'RAMO'])
                            	return
                            		<RAMO>
                            			{
                            				if ($ramo = 'M') then (1)
                            				else if ($ramo = 'C') then (2)
                            				else ()
                            			}
                            		</RAMO>
                                }
								<POL>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),9,8) }</POL>
								<CERPOL>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),17,4) }</CERPOL>
								<CERANN>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),21,4) }</CERANN>
								<CERSEC>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),25,6) }</CERSEC>
								<EST>{
									let $ramo := data($item/ns0:Column[@name = 'EST'])
									return
									if ($ramo = 'OPEN') then ('A') else ('C')
								}</EST>
								<FECSINI>{
									let $fecha := data($item/ns0:Column[@name = 'FECSINI'])
									return
									if ($fecha != '') then (fn-bea:date-to-string-with-format("dd/MM/yyyy", $fecha)) else ()
								}</FECSINI>
								<AGE>{ data($item/ns0:Column[@name = 'AGE']) }</AGE>
							</REG>
						)
					}
                    </REGS>
            	)
			}
        </Response>
};

declare variable $paginatedQueryRs1 as element() external;

xf:OV_1301_From_PaginatedQueryRs($paginatedQueryRs1)
