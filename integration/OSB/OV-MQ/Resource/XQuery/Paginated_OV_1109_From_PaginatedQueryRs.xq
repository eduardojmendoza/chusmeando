xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1109.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1109_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1109_From_PaginatedQueryRs($resp as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>,
                    <MSGEST>ER</MSGEST>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    (: Tiene un TR porque lo usamos cuando consultamos por paginas. En caso de que no haya mas, la proxima consulta devolvera un ER :)
                    <MSGEST>TR</MSGEST>,
                    <REGS>
                        {
                            for $item in $resp/ns0:RowSet/ns0:Row
                            return
                            (
                                <REG>
                                    <PROD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)) }</PROD>
                                    <POL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,8)) }</POL>
                                    <CERPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERPOL>
                                    <CERANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERANN>
                                    <CERSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERSEC>
                                    <SUPLENUM>0000</SUPLENUM>
                                    <OPERAPOL>{data($item/ns0:Column[@name = 'OPERAPOL'])}</OPERAPOL>
                                    <CLIDES>{ data($item/ns0:Column[@name = 'CLIDES']) }</CLIDES>
                                    <FECVIGDES>{ local:normalizeDate(data($item/ns0:Column[@name = 'FECVIGDES'])) }</FECVIGDES>
                                    <FECVIGHAS>{ local:normalizeDate(data($item/ns0:Column[@name = 'FECVIGHAS'])) }</FECVIGHAS>
                                    <MOTIV>{ data($item/ns0:Column[@name = 'MOT']) }</MOTIV>
                                    <FECEMI>{ local:normalizeDate(data($item/ns0:Column[@name = 'FECEMI'])) }</FECEMI>
                                    (: No deberia ocurrir pero puede haber datos mal cargados :)
                                    {
                                    if ( exists($item/ns0:Column[@name = 'PRIMA']) and data($item/ns0:Column[@name = 'PRIMA']) != '' ) then (
	                                    <PRIMA>{ translate(fn-bea:format-number(data($item/ns0:Column[@name = 'PRIMA']), '0.00'), '.', ',') }</PRIMA>
                                    ) else (
										<PRIMA/>
                                    )
                                    }
                                    {
                                    if ( exists($item/ns0:Column[@name = 'PRECIO']) and data($item/ns0:Column[@name = 'PRECIO']) != '' ) then (
	                                    <PRECIO>{ translate(fn-bea:format-number(data($item/ns0:Column[@name = 'PRECIO']), '0.00'), '.', ',') }</PRECIO>
                                    ) else (
										<PRECIO/>
                                    )
                                    }
                                    <COD>{ concat(substring(data($item/ns0:Column[@name = 'COD']), 1, 2), '-', substring(data($item/ns0:Column[@name = 'COD']), 3)) }</COD>
                                    {
                                    	let $ramo := data($item/ns0:Column[@name = 'RAMO'])
                                    	return
                                    		<RAMO>
                                    			{
                                    				if ($ramo = 'M') then (1)
                                    				else if ($ramo = 'C') then (2)
                                    				else ()
                                    			}
                                    		</RAMO>
                                    }
                                </REG>
                            )
                        }
                    </REGS>
                )
            }
        </Response>
};

declare variable $resp as element() external;

declare function local:normalizeDate($value as xs:string?) as xs:string?
{
	if ( exists($value) and $value != '' and contains($value, 'T')) then (
		fn-bea:dateTime-to-string-with-format('dd/MM/yyyy', xs:dateTime($value))
	) else (
		fn-bea:date-to-string-with-format('dd/MM/yyyy', xs:date($value))
	)
};

xf:OV_1109_From_PaginatedQueryRs($resp)