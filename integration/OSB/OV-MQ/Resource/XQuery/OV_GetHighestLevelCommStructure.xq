xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$cmsResponse" element="Response" location="../XSD/OV_1413.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1413.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1413_GetTopLevelStructure/";

declare function xf:OV_1413_GetTopLevelStructure($cmsResponse as element(Response))
    as element(Response) {
        <Response>
        <Estado mensaje="" resultado="true" />
        <REGS>
        {
        let $cantGOs := count($cmsResponse/REGS/*[data(NIVEL) = 'GO' ])
        let $cantORs := count($cmsResponse/REGS/*[data(NIVEL) = 'OR' ])
        let $cantPRs := count($cmsResponse/REGS/*[data(NIVEL) = 'PR' ])
        return
        if ( $cantGOs > 0 ) then (
         $cmsResponse/REGS/*[data(NIVEL) = 'GO']
        ) else (
	        if ( $cantORs > 0 ) then (
	         $cmsResponse/REGS/*[data(NIVEL) = 'OR']
	        ) else (
	         $cmsResponse/REGS/*[data(NIVEL) = 'PR']
	        )
        )

        }
        </REGS>
        </Response>
};

declare variable $cmsResponse as element(Response) external;

xf:OV_1413_GetTopLevelStructure($cmsResponse)
