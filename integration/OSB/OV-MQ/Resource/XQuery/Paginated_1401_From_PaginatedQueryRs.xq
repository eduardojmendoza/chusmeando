xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1401.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1401_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1401_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>,
                    <MSGEST>ER</MSGEST>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <MSGEST>TR</MSGEST>,
					<PRODUCTO>{ data($paginatedQueryRs1 /PRODUCTO) }</PRODUCTO>,
					<POLIZA>{ data($paginatedQueryRs1 /POLIZA) }</POLIZA>,
                    <NROCONS>{ data($paginatedQueryRs1 /NROCONS) }</NROCONS>,
					<NROORDEN>{ data($paginatedQueryRs1 /NROORDEN) }</NROORDEN>,
					<DIRORDEN>{ data($paginatedQueryRs1 /DIRORDEN) }</DIRORDEN>,
					<TFILTRO>{ data($paginatedQueryRs1 /TFILTRO) }</TFILTRO>,
					<VFILTRO>{ data($paginatedQueryRs1 /VFILTRO) }</VFILTRO>,
					<PAGINADO/>,
					<ResultSetID>{data($paginatedQueryRs1/ns0:ResultSetID)}</ResultSetID>,
					<REGS>
				 	{
	            		for $row in $paginatedQueryRs1/ns0:RowSet/ns0:Row
	            		return
	            		<REG>
	            			<AGE>{ data($row/ns0:Column[@name = 'AGE']) }</AGE>
							<SINI>{ data($row/ns0:Column[@name = 'SINI']) }</SINI>
							<RAMO>{ data($row/ns0:Column[@name = 'RAMO']) }</RAMO>
							<CLIDES>{ data($row/ns0:Column[@name = 'CLIDES']) }</CLIDES>
							<PROD>{ data(substring($row/ns0:Column[@name = 'POLICY_NO'],5,4)) }</PROD>
							<POL>{ substring(data($row/ns0:Column[@name = 'POLICY_NO']),9,8) }</POL>
                        	<CERPOL>{ substring(data($row/ns0:Column[@name = 'POLICY_NO']),17,4) }</CERPOL>
                        	<CERANN>{ substring(data($row/ns0:Column[@name = 'POLICY_NO']),21,4) }</CERANN>
                        	<CERSEC>{ substring(data($row/ns0:Column[@name = 'POLICY_NO']),25,6) }</CERSEC>
							<MON>{ data($row/ns0:Column[@name = 'MON']) }</MON>
							<IMPTO>{ 
							if ((data($row/ns0:Column[@name = 'IMPTO']) = 0) 
							or (data($row/ns0:Column[@name = 'IMPTO']) > 0 and data($row/ns0:Column[@name = 'IMPTO']) < 1) 
							or (data($row/ns0:Column[@name = 'IMPTO']) < 0 and data($row/ns0:Column[@name = 'IMPTO'] > -1))) 
							then(local:format_Num_aQBE(data($row/ns0:Column[@name = 'IMPTO'])))
							else(replace(fn-bea:format-number(data($row/ns0:Column[@name = 'IMPTO']),'#.00'),'\.',',')) }</IMPTO>
							<I_30>{ if ((data($row/ns0:Column[@name = 'I_30']) = 0)
							or (data($row/ns0:Column[@name = 'I_30']) > 0 and data($row/ns0:Column[@name = 'I_30']) < 1) 
							or (data($row/ns0:Column[@name = 'I_30']) < 0 and data($row/ns0:Column[@name = 'I_30'] > -1))) 
							then(local:format_Num_aQBE(data($row/ns0:Column[@name = 'I_30']))) 
								else(replace(fn-bea:format-number(data($row/ns0:Column[@name = 'I_30']),'#.00'),'\.',',')) }</I_30>
							<I_60>{ if ((data($row/ns0:Column[@name = 'I_60']) = 0)
							or (data($row/ns0:Column[@name = 'I_60']) > 0 and data($row/ns0:Column[@name = 'I_60']) < 1) 
							or (data($row/ns0:Column[@name = 'I_60']) < 0 and data($row/ns0:Column[@name = 'I_60'] > -1))) 
							then(local:format_Num_aQBE(data($row/ns0:Column[@name = 'I_60']))) 
									else(replace(fn-bea:format-number(data($row/ns0:Column[@name = 'I_60']),'#.00'),'\.',',')) }</I_60>
							<I_90>{ if ((data($row/ns0:Column[@name = 'I_90']) = 0)
							or (data($row/ns0:Column[@name = 'I_90']) > 0 and data($row/ns0:Column[@name = 'I_90']) < 1) 
							or (data($row/ns0:Column[@name = 'I_90']) < 0 and data($row/ns0:Column[@name = 'I_90'] > -1))) 
							then(local:format_Num_aQBE(data($row/ns0:Column[@name = 'I_90']))) 
									else(replace(fn-bea:format-number(data($row/ns0:Column[@name = 'I_90']),'#.00'),'\.',',')) }</I_90>
							<I_M90>{ if ((data($row/ns0:Column[@name = 'I_M90']) = 0)
							or (data($row/ns0:Column[@name = 'I_M90']) > 0 and data($row/ns0:Column[@name = 'I_M90']) < 1) 
							or (data($row/ns0:Column[@name = 'I_M90']) < 0 and data($row/ns0:Column[@name = 'I_M90'] > -1))) 
							then(local:format_Num_aQBE(data($row/ns0:Column[@name = 'I_M90']))) 
									else(replace(fn-bea:format-number(data($row/ns0:Column[@name = 'I_M90']),'#.00'),'\.',',')) }</I_M90>
							<EST>{ data($row/ns0:Column[@name = 'EST']) }</EST>
							<COB>{ data($row/ns0:Column[@name = 'COB']) }</COB>
	                    </REG>
	                }
	                </REGS>,
					<TOTS>
                        <TOT>
                        	<TMON>$</TMON>
                        	<T_IMPTOS/>
                    	    <T_IMPTO>
                        	{
                        		let $tot := sum($paginatedQueryRs1/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = '$']/ns0:Column[@name = 'IMPTO'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
                        	</T_IMPTO>
							<T_30S/>
							<T_30>
                        	{
                        		let $tot := sum($paginatedQueryRs1/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = '$']/ns0:Column[@name = 'I_30'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_30>
							<T_60S/>
							<T_60>
                        	{
                        		let $tot := sum($paginatedQueryRs1/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = '$']/ns0:Column[@name = 'I_60'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_60>
							<T_90S/>
							<T_90>
                        	{
                        		let $tot := sum($paginatedQueryRs1/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = '$']/ns0:Column[@name = 'I_90'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_90>
							<T_M90S/>
							<T_M90>
                        	{
                        		let $tot := sum($paginatedQueryRs1/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = '$']/ns0:Column[@name = 'I_M90'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_M90>
						</TOT>
                        <TOT>
                        	<TMON>U$S</TMON>
                        	<T_IMPTOS/>
                    	    <T_IMPTO>
                        	{
                        		let $tot := sum($paginatedQueryRs1/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = 'U$S']/ns0:Column[@name = 'IMPTO'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
                        	</T_IMPTO>
							<T_30S/>
							<T_30>
                        	{
                        		let $tot := sum($paginatedQueryRs1/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = 'U$S']/ns0:Column[@name = 'I_30'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_30>
							<T_60S/>
							<T_60>
                        	{
                        		let $tot := sum($paginatedQueryRs1/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = 'U$S']/ns0:Column[@name = 'I_60'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_60>
							<T_90S/>
							<T_90>
                        	{
                        		let $tot := sum($paginatedQueryRs1/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = 'U$S']/ns0:Column[@name = 'I_90'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_90>
							<T_M90S/>
							<T_M90>
                        	{
                        		let $tot := sum($paginatedQueryRs1/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = 'U$S']/ns0:Column[@name = 'I_M90'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_M90>
						</TOT>
					</TOTS>
            	)
			}
        </Response>
};

declare variable $paginatedQueryRs1 as element() external;

declare function local:format_Num_aQBE($input as xs:decimal)
    as xs:string {
	
	if(string(number($input)) != 'NaN') then (
		replace(replace(replace(xs:string(fn-bea:format-number(fn-bea:decimal-round($input,2),'#,##0.00')),',','!'),'\.',','),'!','.')
	) else (
		'0.00'
	)
	
};

xf:OV_1401_From_PaginatedQueryRs($paginatedQueryRs1)
