xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1415.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1415_From_PaginatedQueryRs/";

declare function xf:OV_1415_From_PaginatedQueryRs($resp as element())
    as element(Response) {
		<Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>,
                    <MSGEST>ER</MSGEST>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <MSGEST>OK</MSGEST>,
                    <COTIDOLAR>{ data($resp/ns0:RowSet/ns0:Row[0]/ns0:Column[@name = 'COTIDOLAR']) }</COTIDOLAR>,
                    <REGS>
                        {
                            for $item in $resp/ns0:RowSet/ns0:Row
                            return
                            (
                                <REG>
                                    <CLIDES>{ data($item/ns0:Column[@name = 'CLIDES']) }</CLIDES>
                                    <CLIENSEC>{ data($item/ns0:Column[@name = 'CLIENSEC']) }</CLIENSEC>
                                    <PROD>{ data($item/ns0:Column[@name = 'PROD']) }</PROD>
                                    <POL>{ data($item/ns0:Column[@name = 'POL']) }</POL>
                                    <CERPOL>{ data($item/ns0:Column[@name = 'CERPOL']) }</CERPOL>
                                    <CERANN>{ data($item/ns0:Column[@name = 'CERANN']) }</CERANN>
                                    <CERSEC>{ data($item/ns0:Column[@name = 'CERSEC']) }</CERSEC>
                                    <OPERAPOL>{ data($item/ns0:Column[@name = 'OPERAPOL']) }</OPERAPOL>
                                    <RECNUM>{ data($item/ns0:Column[@name = 'RECNUM']) }</RECNUM>
                                    <MON>{ data($item/ns0:Column[@name = 'MON']) }</MON>
                                    <SIG>{ data($item/ns0:Column[@name = 'SIG']) }</SIG>
                                    <IMP>{ data($item/ns0:Column[@name = 'IMP']) }</IMP>
                                    <IMPCALC>{ data($item/ns0:Column[@name = 'IMPCALC']) }</IMPCALC>                                    
                                    <RENDIDO>{ data($item/ns0:Column[@name = 'RENDIDO']) }</RENDIDO>
                                    <RAMO>{ data($item/ns0:Column[@name = 'RAMO']) }</RAMO>
                                    <FECVTO>{ data($item/ns0:Column[@name = 'FECVTO']) }</FECVTO>
                                    <AGE>{ data($item/ns0:Column[@name = 'AGE']) }</AGE>
                                    <ESTADO>{ data($item/ns0:Column[@name = 'ESTADO']) }</ESTADO>
                                </REG>
                            )
                        }
                    </REGS>
                )
            }
        </Response>
};

declare variable $resp as element() external;

xf:OV_1415_From_PaginatedQueryRs($resp)