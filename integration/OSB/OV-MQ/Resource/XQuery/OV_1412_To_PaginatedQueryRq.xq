(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_1412.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1412_To_PaginatedQueryRq/";

declare function xf:OV_1412_To_PaginatedQueryRq($req as element(Request))
    as element() {
        <ns0:PaginatedQueryRq>
            <ns0:QueryID>1412</ns0:QueryID>
            <ns0:FilterCriteria>
                <ns0:FilterCriterion field = "POLICY_NO"
                                     operation = "EQ"
                                     value = "{ xqubh:buildPolicyNo('0001', data($req/PRODUCTO), data($req/POLIZA), data($req/CERTI)) }"/>
            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $req as element(Request) external;

xf:OV_1412_To_PaginatedQueryRq($req)