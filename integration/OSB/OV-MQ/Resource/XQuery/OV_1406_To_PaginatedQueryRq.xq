(:: pragma bea:global-element-parameter parameter="$request1uest1" element="Request" location="../XSD/OV_1406.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1406_To_PaginatedQueryRq/";

declare function xf:OV_1406_To_PaginatedQueryRq($request1 as element(Request))
    as element() {
        <ns0:PaginatedQueryRq>
            <ns0:QueryID>1406</ns0:QueryID>
            <ns0:FilterCriteria>
                {
                    if (data($request1/NIVEL1) = '' and data($request1/CLIENSEC1) = ''
                        and data($request1/NIVEL2) = '' and data($request1/CLIENSEC2) = ''
                        and data($request1/NIVEL3) = '' and data($request1/CLIENSEC3) = '')
                    then (
                        <ns0:FilterCriterion field = "NIVEL{ index-of(('GO', 'OR', 'PR'), upper-case(data($request1/NIVELAS))) }"
                                             operation = "EQ"
                                             value = "{ data($request1/NIVELAS) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC{ index-of(('GO', 'OR', 'PR'), upper-case(data($request1/NIVELAS))) }"
                                             operation = "EQ"
                                             value = "{ data($request1/CLIENSECAS) }"/>
                    )
                    else (
                        <ns0:FilterCriterion field = "NIVEL1"
                                             operation = "EQ"
                                             value = "{ data($request1/NIVEL1) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC1"
                                             operation = "EQ"
                                             value = "{ data($request1/CLIENSEC1) }"/>,
                        <ns0:FilterCriterion field = "NIVEL2"
                                             operation = "EQ"
                                             value = "{ data($request1/NIVEL2) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC2"
                                             operation = "EQ"
                                             value = "{ data($request1/CLIENSEC2) }"/>,
                        <ns0:FilterCriterion field = "NIVEL3"
                                             operation = "EQ"
                                             value = "{ data($request1/NIVEL3) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC3"
                                             operation = "EQ"
                                             value = "{ data($request1/CLIENSEC3) }"/>
                    )
                }
            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $request1uest1 as element(Request) external;

xf:OV_1406_To_PaginatedQueryRq($request1uest1)
