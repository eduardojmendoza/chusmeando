xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1305.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1305_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1305_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else
               	(
                    <Estado resultado = "true" mensaje = ""/>,

            		let $row := $paginatedQueryRs1/ns0:RowSet/ns0:Row[1]
            		return
            		<DATOS>
                    	<DA_PROPIO>{ data($row/ns0:Column[@name = 'DA_PROPIO']) }</DA_PROPIO>
                     	<DA_TERCE>{ data($row/ns0:Column[@name = 'DA_TERCE']) }</DA_TERCE>
                    	<LE_TERCE>{ data($row/ns0:Column[@name = 'LE_TERCE']) }</LE_TERCE>
                    	<CONDUTIP>{ data($row/ns0:Column[@name = 'CONDUTIP']) }</CONDUTIP>
                    	<CONDUDOCU>{ substring-after(data($row/ns0:Column[@name = 'CONDUDOCU']),'-')  }</CONDUDOCU>
                    	<CONDUAPE>{ data($row/ns0:Column[@name = 'CONDUAPE']) }</CONDUAPE>
                    	<CONDUNOM>{ data($row/ns0:Column[@name = 'CONDUNOM']) }</CONDUNOM>
                    </DATOS>
            	)
			}
        </Response>
};

declare variable $paginatedQueryRs1 as element() external;

xf:OV_1305_From_PaginatedQueryRs($paginatedQueryRs1)
