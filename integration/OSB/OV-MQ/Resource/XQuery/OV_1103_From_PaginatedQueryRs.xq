(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1103.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1103_From_PaginatedQueryRs/";

declare function xf:OV_1103_From_PaginatedQueryRs($resp as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <REGS>
                        {
                            let $RowSet := $resp/ns0:RowSet
                            return
                                for $Row in $RowSet/ns0:Row
                                return
                                    <REG>
                                        <SEC>{ '0' }</SEC>
                                        <ENDOSO>{ data($Row/ns0:Column[@name = 'ENDOSO']) }</ENDOSO>
                                        <VIGDES>{ data($Row/ns0:Column[@name = 'VIGDES']) }</VIGDES>
                                        <VIGHAS>{ data($Row/ns0:Column[@name = 'VIGHAS']) }</VIGHAS>
                                        <MOT>{ data($Row/ns0:Column[@name = 'MOT']) }</MOT>
                                        <ALERTPRIMA>{ data($Row/ns0:Column[@name = 'ALERTPRIMA']) }</ALERTPRIMA>
                                        <ALERTCOMI>{ data($Row/ns0:Column[@name = 'ALERTCOMI']) }</ALERTCOMI>
                                    </REG>
                        }
                    </REGS>
                )
            }
        </Response>
};

declare variable $resp as element() external;

xf:OV_1103_From_PaginatedQueryRs($resp)