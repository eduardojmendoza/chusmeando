xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1802.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1802_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1802_From_PaginatedQueryRs($resp as element())
    as element(Response) {
                <Response>
        	{
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <REGS>
                    {
                    	for $item in $resp/ns0:RowSet/ns0:Row
                    	return
                    	(
                        	<REG>
                        		<AGE>{ data($item/ns0:Column[@name = 'AGE']) }</AGE>
                        		<CLIDES>{ data($item/ns0:Column[@name = 'CLIDES']) }</CLIDES>
                        		<TIPCOMI>{ data('Base Prima') }</TIPCOMI>
                        		<MON>{ data($item/ns0:Column[@name = 'MON']) }</MON>
                        		<SIG>{ data($item/ns0:Column[@name = 'SIG']) }</SIG>
                        		<COMI>{ replace(data($item/ns0:Column[@name = 'COMI']), '\.', ',')  }</COMI>
                        		<SIGBAS>{ data($item/ns0:Column[@name = 'SIGBAS']) }</SIGBAS>
                        		<BASECOMI>{ replace(data($item/ns0:Column[@name = 'BASECOMI']), '\.', ',') }</BASECOMI>
                        		<PORCOMI>{ replace(fn-bea:format-number($item/ns0:Column[@name = 'PORCOMI'],'####.00'), '\.', ',')  }</PORCOMI>
							</REG>
						)
					}
                    </REGS>
            	)
			}
        </Response>
};

declare variable $resp as element() external;

xf:OV_1802_From_PaginatedQueryRs($resp)
