xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1308.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1308_From_PaginatedQueryRs/";

declare function xf:OV_1308_From_PaginatedQueryRs($resp as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>,
                    <MSGEST>ER</MSGEST>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
					<FECCONT>{ data($resp /FECCONT) }</FECCONT>,
					<PAGOCONT>{ data($resp /PAGOCONT) }</PAGOCONT>,
                    <MSGEST>OK</MSGEST>,
                    <REGS>
                        {
                            for $item in $resp/ns0:RowSet/ns0:Row
                            return
                            (
                                <REG>
                                	<NROP></NROP>
                                    <BENEF>{ data($item/ns0:Column[@name = 'BENEF']) }</BENEF>
                                    <EST>{ data($item/ns0:Column[@name = 'EST']) }</EST>
                                    <FEC>{ local:normalizeDate(data($item/ns0:Column[@name = 'FEC'])) }</FEC>
                                    <MON>{ data($item/ns0:Column[@name = 'MON']) }</MON>
                                    <SIG>{ data($item/ns0:Column[@name = 'SIG']) }</SIG>
                                    <IMP>{ fn-bea:format-number(data($item/ns0:Column[@name = 'IMP']), '#,##0.00') }</IMP>
                                </REG>
                            )
                        }
                    </REGS>                )
            }
        </Response>
};

declare variable $resp as element() external;

declare function local:normalizeDate($value as xs:string?) as xs:string?
{
	if ( exists($value) and $value != '' and contains($value, 'T')) then (
		fn-bea:dateTime-to-string-with-format('dd/MM/yyyy', xs:dateTime($value))
	) else (
		fn-bea:date-to-string-with-format('dd/MM/yyyy', xs:date($value))
	)
};

xf:OV_1308_From_PaginatedQueryRs($resp)