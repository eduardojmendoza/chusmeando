(:: pragma bea:global-element-parameter parameter="$request1" element="Request" location="../XSD/OV_1408.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1408_To_PaginatedQueryRq/";

declare function xf:OV_1408_To_PaginatedQueryRq($request1 as element(Request))
    as element() {
        <ns0:PaginatedQueryRq>
            <ns0:QueryID>1408</ns0:QueryID>
            <ns0:FilterCriteria>
				<ns0:FilterCriterion field = "RECIBO"
                                     operation = "EQ"
                                     value = "{ 
                                     			concat(
                                     				fn-bea:format-number(xs:double(substring($request1/RECIBO, 1, 2)), '00') 
                                     				,'-'
                                     				,substring($request1/RECIBO, 3, 1) 
                                     				,'-'
                                     				,fn-bea:format-number(xs:double(substring($request1/RECIBO, 4, 6)), '000000') 
                                     				)
                                     			}" />
            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $request1 as element(Request) external;

xf:OV_1408_To_PaginatedQueryRq($request1)
