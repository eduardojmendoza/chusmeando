(:: pragma bea:global-element-parameter parameter="$request1" element="Request" location="../XSD/OV_1302.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1302_To_PaginatedQueryRq/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:OV_1302_To_PaginatedQueryRq($request1 as element(Request),$LOVs as element()?)
    as element() {
        <ns0:PaginatedQueryRq>
            <ns0:QueryID>1302</ns0:QueryID>
            <ns0:FilterCriteria>
            	{

            		if (data($request1/DOCUMTIP) != '') then
            		(
						<ns0:FilterCriterion field = "DOCUMTIP"
		                                     operation = "EQ"
		                                     value = "{ let $doc := xs:int($request1/DOCUMTIP)
		                                     			return
		                                     			let $docType :=  data($LOVs/lov:LOV[@id = "DocTypeNameById" and @ref = $doc ][1]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID)
		                                     			return
		                                     			$docType }" />
					) else ()
				}
				{

            		if (data($request1/NIVELAS) != '') then
            		(
						<ns0:FilterCriterion field = "NIVELCLAS"
		                                     operation = "EQ"
		                                     value = "{ data($request1/NIVELAS) }" />
					) else ()
				}
				{

            		if (data($request1/CLIENSECAS) != '') then
            		(
						<ns0:FilterCriterion field = "CLIENSECAS"
		                                     operation = "EQ"
		                                     value = "{ data($request1/CLIENSECAS) }" />
					) else ()
				}
				{
            		if (data($request1/DOCUMNRO) != '') then
            		(
						<ns0:FilterCriterion field = "DOCUMNRO"
		                                     operation = "EQ"
		                                     value = "{ data($request1/DOCUMNRO) }" />
					) else ()
				}
				{
            		if (data($request1/CLIENDES) != '') then
            		(
						<ns0:FilterCriterion field = "CLIENDES"
		                                     operation = "EQ"
		                                     value = "{ data($request1/CLIENDES) }" />
					) else ()
				}
				{
            		if (data($request1/PRODUCTO) != '' and data($request1/POLIZA) != '') then
            		(
						<ns0:FilterCriterion field = "POLICY_NO"
		                                     operation = "EQ"
		                                    value = "{ xqubh:buildPolicyNo('0001', data($request1/PRODUCTO), data($request1/POLIZA), data($request1/CERTI) ) }" />
					) else ()
				}
				{
            		if (data($request1/PATENTE) != '') then
            		(
						<ns0:FilterCriterion field = "PATENTE"
		                                     operation = "EQ"
		                                     value = "{ data($request1/PATENTE) }" />
					) else ()
				}
				{
            		if (data($request1/SININUM)!="") then
            		(
						<ns0:FilterCriterion field = "CLAIM_NO"
		                                     	operation = "EQ"
		                                    	value = "{ fn-bea:trim(data($request1/SININUM))}" />
					) else ()
				}
            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $request1 as element(Request) external;
declare variable $LOVs as element(lov:LOVs)? external;

xf:OV_1302_To_PaginatedQueryRq($request1,$LOVs)
