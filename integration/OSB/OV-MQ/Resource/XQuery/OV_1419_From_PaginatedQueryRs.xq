xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1419.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1419_From_PaginatedQueryRs/";



declare function xf:OV_1419_From_PaginatedQueryRs($resp as element(), $tipo_display as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""  />,
                    (: 
                    Segun la variable tipo_display se determina si se muestra en formato TXT o CSV. 
                    concat("CODIGO,NOMBRE,PROD,POLIZA,CERTIF,ESTADO,ENDOSO,RECIBO,MONEDA,IMPORTE,CANAL,FEC.VTO.,DIAS,MOTIVO",'&#10;'),
                    :)
                    if (data($tipo_display) = 2) then(
						concat(
		                    for $item in $resp/ns0:RowSet/ns0:Row
		                    		let $nl := '&#10;' (: caracter de nueva linea :)
						            return
										(
						                    concat( substring(data($item/ns0:Column[@name = 'AGE']),1,2), substring(data($item/ns0:Column[@name = 'AGE']),4, 6), ",",
													data($item/ns0:Column[@name = 'CLIDES']), ",",
													data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)), ",", (: PROD :) 
													data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,8)), ",", (: POLIZA :)
													data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,14)), ",", (: CERTIF :)
													data($item/ns0:Column[@name = 'EST']), ",",
													fn-bea:format-number(xs:double(data($item/ns0:Column[@name = 'ENDOSO'])), '000000'), ",",  
													data($item/ns0:Column[@name = 'RECIBO']), ",",
													fn-bea:pad-right(data($item/ns0:Column[@name = 'MON']), 3), ",",
													data($item/ns0:Column[@name = 'SIGNODIA']), fn-bea:format-number(xs:double(data($item/ns0:Column[@name = 'IMPORTE'])),'#.00'), ",",
													data($item/ns0:Column[@name = 'COB']), ",",
													data($item/ns0:Column[@name = 'FECVENCI']), ",",
													data($item/ns0:Column[@name = 'SIGNODIA']) ,
													data($item/ns0:Column[@name = 'DIASDEUDA']), ",",
													data($item/ns0:Column[@name = 'MOTRECHA']), $nl				                    
						                    ) 			                    		
								)
							)
					) else (
						concat(
							for $item in $resp/ns0:RowSet/ns0:Row
		                		let $nl := '&#10;' (: caracter de nueva linea :)
					            return
									(
				                    concat( substring(data($item/ns0:Column[@name = 'AGE']),1,2), substring(data($item/ns0:Column[@name = 'AGE']),4, 6) ,
										fn-bea:pad-right(data($item/ns0:Column[@name = 'CLIDES']), 30) ,
										fn-bea:pad-right(data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)), 4) , (: PROD :) 
										fn-bea:pad-right(data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,8)), 8) , (: POLIZA :)
										fn-bea:pad-right(data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,14)), 14) , (: CERTIF :)
										fn-bea:pad-right(data($item/ns0:Column[@name = 'EST']), 3) ,
										fn-bea:format-number(xs:double(data($item/ns0:Column[@name = 'ENDOSO'])), '000000') ,  
										fn-bea:pad-right(data($item/ns0:Column[@name = 'RECIBO']), 9) ,
										fn-bea:pad-right(data($item/ns0:Column[@name = 'MON']), 3) ,
										data($item/ns0:Column[@name = 'SIGNODIA']), fn-bea:format-number(xs:double(data($item/ns0:Column[@name = 'IMPORTE'])),'#.00'),
										fn-bea:pad-right(data($item/ns0:Column[@name = 'COB']), 4) ,
										fn-bea:pad-right(data($item/ns0:Column[@name = 'FECVENCI']), 10) ,
										data($item/ns0:Column[@name = 'SIGNODIA']) ,
										fn-bea:pad-right(data($item/ns0:Column[@name = 'DIASDEUDA']), 3) ,
										fn-bea:pad-right(data($item/ns0:Column[@name = 'MOTRECHA']), 20) , '&#xa;'				                    
				                    ) 		                    		
								)
							)
					)
                )    
            }
        </Response>
};

declare variable $resp as element() external;
declare variable $tipo_display as element() external;

xf:OV_1419_From_PaginatedQueryRs($resp, $tipo_display)