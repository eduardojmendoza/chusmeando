xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$item" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1304.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1304_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:OV_1304_From_PaginatedQueryRs($LOVs as element()?
												,$item as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($item/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    for $item in $item/ns0:RowSet/ns0:Row
					return
					(
	                    <DATOS>
	                        	<CLIDES>{ data($item/ns0:Column[@name = 'CLIDES']) }</CLIDES>
								<RAMO>{ data($item/ns0:Column[@name = 'RAMO']) }</RAMO>
								<PROD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)) }</PROD>
		                  		<POL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,8)) }</POL>
		                  		<CERPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERPOL>
		                  		<CERANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERANN>
						  		<CERSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERSEC>
								<EST>{
										let $ramo := data($item/ns0:Column[@name = 'EST'])
										return if ($ramo = 'OPEN') then ('A') 
										else(if($ramo = 'INCOMPLETE') then ('I') 
										else ('C'))
									}
								</EST>
								<FECSINI>{
                        			let $fecha := data($item/ns0:Column[@name = 'FECSINI'])
                        			return
                        				if ($fecha != '') then (fn-bea:date-to-string-with-format("dd/MM/yyyy", $fecha)) else ()
                        			}
                        		</FECSINI>
								<DATOSADI>S</DATOSADI>
								<D_DOCU>{ substring-after(data($item/ns0:Column[@name = 'D_DOCU']),'-') }</D_DOCU>
								<D_CLIAPE>{ data($item/ns0:Column[@name = 'D_CLIENPE']) }</D_CLIAPE>
								<D_CLINOM>{ data($item/ns0:Column[@name = 'D_CLINOM']) }</D_CLINOM>
								<D_CAUSA>{ data($item/ns0:Column[@name = 'D_CAUSA']) }</D_CAUSA>
								<D_CALLE>{ data($item/ns0:Column[@name = 'D_CALLE']) }</D_CALLE>
								<D_NRO>{ data($item/ns0:Column[@name = 'D_NRO']) }</D_NRO>
								<D_LOCAL>{ data($item/ns0:Column[@name = 'D_LOCAL'])}</D_LOCAL>
								<D_CP>{data($item/ns0:Column[@name = 'D_CP'])}</D_CP>
								<D_PROV>{ data($LOVs/lov:LOV[@id = "Province"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($item/ns0:Column[@name = 'D_PROV'])]/ns0:ItemDescription)}</D_PROV>
								<D_PAIS>{ data($item/ns0:Column[@name = 'D_PAIS'])}</D_PAIS>
								<D_COMI>{ data($item/ns0:Column[@name = 'D_COMI']) }</D_COMI>

								{
									if (data($item/ns0:Column[@name = 'D_HORA']) != '')
									then 
									<D_HORA>{
										let $fecha := fn-bea:time-from-dateTime(data($item/ns0:Column[@name = 'D_HORA']))
										return 
											fn-bea:time-to-string-with-format("hh:mm", $fecha) 
										}</D_HORA>
									else <D_HORA/>
								}
	                    </DATOS>
					)
            	)
			}
        </Response>
};

declare variable $item as element() external;
declare variable $LOVs as element(lov:LOVs)? external;

xf:OV_1304_From_PaginatedQueryRs($LOVs,$item)
