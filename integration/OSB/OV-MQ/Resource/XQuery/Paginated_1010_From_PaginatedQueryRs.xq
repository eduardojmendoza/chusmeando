xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1010.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/Paginated_1010_From_PaginatedQueryRs/";

declare function xf:Paginated_1010_From_PaginatedQueryRs($resp as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>,
                    <MSGEST>ER</MSGEST>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <MSGEST>TR</MSGEST>,
                    <ResultSetID>{data($resp/ns0:ResultSetID)}</ResultSetID>,
                    <REGS>
                        {
                            for $item in $resp/ns0:RowSet/ns0:Row
                            return
                            (
                                <REG>
                                    <PROD>{ data($item/ns0:Column[@name = 'PROD']) }</PROD>
                                    <POL>{ data($item/ns0:Column[@name = 'POL']) }</POL>
                                    <CERPOL>{ data($item/ns0:Column[@name = 'CERPOL']) }</CERPOL>
                                    <CERANN>{ data($item/ns0:Column[@name = 'CERANN']) }</CERANN>
                                    <CERSEC>{ data($item/ns0:Column[@name = 'CERSEC']) }</CERSEC>
                                    <CLIDES>{ data($item/ns0:Column[@name = 'CLIDES']) }</CLIDES>
                                    <TOMA>{ data($item/ns0:Column[@name = 'TOMA']) }</TOMA>
                                    <EST>{ data($item/ns0:Column[@name = 'EST']) }</EST>
                                    <SINI>{ data($item/ns0:Column[@name = 'SINI']) }</SINI>
                                    <ALERTEXI>{ data($item/ns0:Column[@name = 'ALERTEXI']) }</ALERTEXI>
                                    <AGE>{ data($item/ns0:Column[@name = 'AGE']) }</AGE>
                                    {
                                    	let $ramo := data($item/ns0:Column[@name = 'RAMO'])
                                    	return
                                    		<RAMO>
                                    			{
                                    				if ($ramo = 'M') then (1)
                                    				else if ($ramo = 'C') then (2)
                                    				else ()
                                    			}
                                    		</RAMO>
                                    }
                                    <MARCAREIMPRESION>{ data($item/ns0:Column[@name = 'MARCAREIMPRESION']) }</MARCAREIMPRESION>
                                    <MARCAENDOSABLE>{ data($item/ns0:Column[@name = 'MARCAENDOSABLE']) }</MARCAENDOSABLE>
                                </REG>
                            )
                        }
                    </REGS>
                )
            }
        </Response>
};

declare variable $resp as element() external;

xf:Paginated_1010_From_PaginatedQueryRs($resp)