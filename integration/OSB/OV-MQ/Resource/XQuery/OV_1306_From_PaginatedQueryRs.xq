xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1306.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1306_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1306_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else
               	(
                    <Estado resultado = "true" mensaje = ""/>,
					<REGS>
				 	{
	            		for $row in $paginatedQueryRs1/ns0:RowSet/ns0:Row
	            		return
	            		<REG>
	            			<GESCLA>{ data($row/ns0:Column[@name = 'GESCLA']) }</GESCLA>
							<GESSEC>{ data($row/ns0:Column[@name = 'GESSEC']) }</GESSEC>
							<GESTIP>{ data($row/ns0:Column[@name = 'GESTIP']) }</GESTIP>
							<GESDES>{ data($row/ns0:Column[@name = 'GESDES']) }</GESDES>
							<EST>{ data($row/ns0:Column[@name = 'EST']) }</EST>
							<RECLANOM>{ data($row/ns0:Column[@name = 'RECLANOM']) }</RECLANOM>
							<GESNOM>{ if(data($row/ns0:Column[@name = 'GESNOM']) != '')then(data($row/ns0:Column[@name = 'GESNOM']))else('SIN ASIGNAR') }</GESNOM>
							<SUSTI>{ data($row/ns0:Column[@name = 'SUSTI']) }</SUSTI>
							<RESPEXP>{ if(data($row/ns0:Column[@name = 'RESPEXP']) != '')then(data($row/ns0:Column[@name = 'RESPEXP']))else('SIN ASIGNAR') }</RESPEXP>
							<MAILRESP>{ data($row/ns0:Column[@name = 'MAILRESP']) }</MAILRESP>
							<TELERESP>{ data($row/ns0:Column[@name = 'TELERESP']) }</TELERESP>
	                    </REG>
	                }
	                </REGS>
            	)
			}
        </Response>
};

declare variable $paginatedQueryRs1 as element() external;

xf:OV_1306_From_PaginatedQueryRs($paginatedQueryRs1)
