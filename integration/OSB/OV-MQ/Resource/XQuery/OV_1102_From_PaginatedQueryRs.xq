xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs" type="ns1:InsuranceSvc/ns1:InsuranceSvcRs/ns1:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="ns0:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1102.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1102_From_PaginatedQueryRs/";
declare namespace ns1 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace ns0 = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:OV_1102_From_PaginatedQueryRs($paginatedQueryRs as element(),
    $LOVs as element(ns0:LOVs))
    as element(Response) {
        <Response>
        {
                if(not(data($paginatedQueryRs/ns1:RowSet/ns1:Row/ns1:Column[@name = "RECORD_TYPE"] = 'PD'))) then (
                	<Estado resultado = "false" mensaje = "No se han encontrado resultados para PD (Personal Data)"/>
                	) else if ( number(data($paginatedQueryRs/ns1:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                	) else if (count($paginatedQueryRs/ns1:RowSet/ns1:Row/ns1:Column[@name = "RECORD_TYPE"][. eq 'PD']) >1) then (
                	<Estado resultado = "false" mensaje = "Se han obtenido 2 o + resultados para PD (Personal Data)"/>
                	) else if (count($paginatedQueryRs/ns1:RowSet/ns1:Row/ns1:Column[@name = "RECORD_TYPE"][. eq 'AD']) >1) then (
                	<Estado resultado = "false" mensaje = "Se han obtenido 2 o + resultados para AD (Adress Data)"/>
                	) else (
                    <Estado resultado = "true" mensaje = ""></Estado>,
           			  <DATOS>
               			<CLIENTE>
               				{
               				for $item in $paginatedQueryRs/ns1:RowSet/ns1:Row
                			return
                			if ($item/ns1:Column[@name = "RECORD_TYPE"] = 'PD' and not(data($paginatedQueryRs/ns1:RowSet/ns1:Row/ns1:Column[@name = "RECORD_TYPE"] = 'AD'))) then (
                			<CLIENSEC>{ data($item/ns1:Column[@name = 'CLIENSEC']) }</CLIENSEC>,
                        	<CLIENDES>{ data($item/ns1:Column[@name = 'CLIENDES']) }</CLIENDES>,
                    		<TIPODOCU>{ data($item/ns1:Column[@name = 'TIPODOCU']) }</TIPODOCU>,
         					<NUMEDOCU>{ data($item/ns1:Column[@name = 'NUMEDOCU']) }</NUMEDOCU>,
         					<NACIONALI>ARGENTINA</NACIONALI>,
         					<SEXO>{
                        		let $sexo := data($item/ns1:Column[@name = 'SEXO'])
                        		return
                        			if ($sexo = '1') then ('MASCULINO')
                        			else if ($sexo = '2') then ('FEMENINO')
                        			else ()
                        	}</SEXO>,
                        	<FECNAC>{ let $fecha := data($item/ns1:Column[@name = 'FECNAC'])
                        			return 
                        			if ($fecha != '') then (fn-bea:date-to-string-with-format("dd/MM/yyyy", $fecha)) else ()
                        	 }</FECNAC>,
                        	 <ESTCIVIL>
                        	 {
                        		let $estCivil := data($item/ns1:Column[@name = 'ESTCIVIL'])
                        		return
                        			if ($estCivil = 'S') then ('SOLTERO')
                        			else if ($estCivil = 'C') then ('CASADO')
                        			else if ($estCivil = 'V') then ('VIUDO')
                        			else if ($estCivil = 'D') then ('DIVORCIADO')
                        			else if ($estCivil = 'E') then ('SEPARADO')
                        			else if ($estCivil = 'N') then ('** NO INFORMADO **')
                        			else ($estCivil)
                        	 }
                        	 </ESTCIVIL>,
                        	 <FUMADOR>{ data($item/ns1:Column[@name = 'FUMADOR']) }</FUMADOR>,
                        	 <DIESZUR>{ data($item/ns1:Column[@name = 'DIESZUR']) }</DIESZUR>,
                        	 <FECFALLE>{ data($item/ns1:Column[@name = 'FECFALLE']) }</FECFALLE>,
                        	 <CALLEPART/>,
                       		 <CPOPART/>,
                       		 <LOCALPART/>,
                       		 <PROVCOD/>,
                       		 <PROVPART/>
                        	 ) else if($item/ns1:Column[@name = "RECORD_TYPE"] = 'PD') then(
                        	 <CLIENSEC>{ data($item/ns1:Column[@name = 'CLIENSEC']) }</CLIENSEC>,
                        	<CLIENDES>{ data($item/ns1:Column[@name = 'CLIENDES']) }</CLIENDES>,
                    		<TIPODOCU>{ data($item/ns1:Column[@name = 'TIPODOCU']) }</TIPODOCU>,
         					<NUMEDOCU>{ data($item/ns1:Column[@name = 'NUMEDOCU']) }</NUMEDOCU>,
         					<NACIONALI>ARGENTINA</NACIONALI>,
         					<SEXO>{
                        		let $sexo := data($item/ns1:Column[@name = 'SEXO'])
                        		return
                        			if ($sexo = '1') then ('MASCULINO')
                        			else if ($sexo = '2') then ('FEMENINO')
                        			else ()
                        	}</SEXO>,
                        	<FECNAC>{ let $fecha := data($item/ns1:Column[@name = 'FECNAC'])
                        			return 
                        			if ($fecha != '') then (fn-bea:date-to-string-with-format("dd/MM/yyyy", $fecha)) else ()
                        	 }</FECNAC>,
                        	 <ESTCIVIL>
                        	 {
                        		let $estCivil := data($item/ns1:Column[@name = 'ESTCIVIL'])
                        		return
                        			if ($estCivil = 'S') then ('SOLTERO')
                        			else if ($estCivil = 'C') then ('CASADO')
                        			else if ($estCivil = 'V') then ('VIUDO')
                        			else if ($estCivil = 'D') then ('DIVORCIADO')
                        			else if ($estCivil = 'E') then ('SEPARADO')
                        			else if ($estCivil = 'N') then ('** NO INFORMADO **')
                        			else ($estCivil)
                        	 }
                        	 </ESTCIVIL>,
                        	 <FUMADOR>{ data($item/ns1:Column[@name = 'FUMADOR']) }</FUMADOR>,
                        	 <DIESZUR>{ data($item/ns1:Column[@name = 'DIESZUR']) }</DIESZUR>,
                        	 <FECFALLE>{ data($item/ns1:Column[@name = 'FECFALLE']) }</FECFALLE>
                        	 ) else if($item/ns1:Column[@name = "RECORD_TYPE"] = 'AD') then (
                       		 <CALLEPART>{ data($item/ns1:Column[@name = 'CALLEPART']) }</CALLEPART>,
                       		 <CPOPART>{ data($item/ns1:Column[@name = 'CPOPART']) }</CPOPART>,
                       		 <LOCALPART>{ data($item/ns1:Column[@name = 'LOCALPART']) }</LOCALPART>,
                       		 <PROVCOD>{ data($item/ns1:Column[@name = 'PROVICOD']) }</PROVCOD>,
                       		 <PROVPART>{ data($item/ns1:Column[@name = 'PROVIDES']) }</PROVPART>
                       		 ) else()
                       		}
                       	</CLIENTE>
                      	<CONTACTOS>
                    	{for $item in $paginatedQueryRs/ns1:RowSet/ns1:Row
                		return
                		if($item/ns1:Column[@name = 'RECORD_TYPE'] = 'CD') then (
                           <CONTACTO>
                               <MEDCOCOD>{ data($item/ns1:Column[@name = 'MEDCOCOD']) }</MEDCOCOD>
                               <DESCRIPCION>{ data($item/ns1:Column[@name = 'MEDCODAB']) }</DESCRIPCION>
                               <DATOCONT>{ data($item/ns1:Column[@name = 'MEDCODAT']) }</DATOCONT>
                           </CONTACTO>)else()
                   		 }
                		</CONTACTOS>
             		</DATOS>
          )}
         </Response>
};

declare variable $paginatedQueryRs as element() external;
declare variable $LOVs as element(ns0:LOVs) external;

xf:OV_1102_From_PaginatedQueryRs($paginatedQueryRs,$LOVs)
