(:: pragma bea:schema-type-parameter parameter="$paginatedQueryRq" type="ns0:PaginatedQueryRq_Type" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:schema-type-return type="ns0:PaginatedQueryRq_Type" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/AddExcludedProducersToPaginatedQueryRq/";

declare function xf:AddExcludedProducersToPaginatedQueryRq($paginatedQueryRq as element(),
    $agentesExcluidosList as xs:string)
    as element() {
        <ns0:PaginatedQueryRq_Type>
            <ns0:QueryID>{ data($paginatedQueryRq/ns0:QueryID) }</ns0:QueryID>
            {
                for $StartRow in $paginatedQueryRq/ns0:StartRow
                return
                    <ns0:StartRow>{ data($StartRow) }</ns0:StartRow>
            }
            {
                for $RowCount in $paginatedQueryRq/ns0:RowCount
                return
                    <ns0:RowCount>{ data($RowCount) }</ns0:RowCount>
            }
            {
                for $ResultSetID in $paginatedQueryRq/ns0:ResultSetID
                return
                    <ns0:ResultSetID>{ data($ResultSetID) }</ns0:ResultSetID>
            }
            <ns0:FilterCriteria>
            {
                for $FilterCriterion in $paginatedQueryRq/ns0:FilterCriteria/ns0:FilterCriterion
                return
                    <ns0:FilterCriterion>{ $FilterCriterion/@* , $FilterCriterion/node() }</ns0:FilterCriterion>
            }
                 <ns0:FilterCriterion field="EXCLUDE" operation="EQ" value="{ $agentesExcluidosList }"/>
            </ns0:FilterCriteria>

        </ns0:PaginatedQueryRq_Type>
};

declare variable $paginatedQueryRq as element() external;
declare variable $agentesExcluidosList as xs:string external;

xf:AddExcludedProducersToPaginatedQueryRq($paginatedQueryRq,
    $agentesExcluidosList)
