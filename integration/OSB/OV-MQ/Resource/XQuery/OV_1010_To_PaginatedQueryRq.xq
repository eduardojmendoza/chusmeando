xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_1010.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1010_To_PaginatedQueryRq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:OV_1010_To_PaginatedQueryRq($LOVs as element()?, $req as element(Request))
    as element() {
        <ns0:PaginatedQueryRq>
        	<ns0:QueryID>1010</ns0:QueryID>
            <ns0:FilterCriteria>
                <ns0:FilterCriterion field = "NIVELAS"
                                     operation = "EQ"
                                     value = "{ data($req/NIVELAS) }" />
                <ns0:FilterCriterion field = "CLIENSECAS"
                                     operation = "EQ"
                                     value = "{ data($req/CLIENSECAS) }" />

				{
				if (data($req/DOCUMTIP) != '') then
            		(
            		                <ns0:FilterCriterion field = "DOCUMTIP"
                                     operation = "EQ"
                                     value = "{
                                		let $docType := data($LOVs/lov:LOV[@id = "DocTypeNameById" and @ref = xs:int($req/DOCUMTIP) ][1]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID)
                            			return ($docType)
								 	}" />
					) else ()
				}
                                     
                <ns0:FilterCriterion field = "DOCUMNRO"
                                     operation = "EQ"
                                     value = "{ data($req/DOCUMNRO) }" />
                <ns0:FilterCriterion field = "CLIENDES"
                                     operation = "EQ"
                                     value = "{  data($req/CLIENDES) }" />
                <ns0:FilterCriterion field = "POLICY_NO"
                                     operation = "EQ"
                                     value = "{ 
                                     			if (data($req/PRODUCTO) != '' and data($req/POLIZA) != '') then (
                                     				xqubh:buildPolicyNo('0001', data($req/PRODUCTO), data($req/POLIZA), data($req/CERTI)) 
                                     			) else() 
                                     		   }" />
                <ns0:FilterCriterion field = "PATENTE"
                                     operation = "EQ"
                                     value = "{ data($req/PATENTE) }" />
                <ns0:FilterCriterion field = "MOTOR"
                                     operation = "EQ"
                                     value = "{ data($req/MOTOR) }" />
                <ns0:FilterCriterion field = "ESTPOL"
                                     operation = "EQ"
                                     value = "{ data($req/ESTPOL) }" />
            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $req as element(Request) external;
declare variable $LOVs as element(lov:LOVs)? external;


xf:OV_1010_To_PaginatedQueryRq($LOVs, $req)