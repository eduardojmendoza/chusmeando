xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1404.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/Paginated_1404_From_PaginatedQueryRs/";

declare function xf:Paginated_1404_From_PaginatedQueryRs($resp as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>,
                    <MSGEST>ER</MSGEST>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
					<PRODUCTO>{ data($resp /PRODUCTO) }</PRODUCTO>,
					<POLIZA>{ data($resp /POLIZA) }</POLIZA>,
                    <MSGEST>TR</MSGEST>,
                    <NROCONS>{ data($resp /NROCONS) }</NROCONS>,
					<NROORDEN>{ data($resp /NROORDEN) }</NROORDEN>,
					<DIRORDEN>{ data($resp /DIRORDEN) }</DIRORDEN>,
					<TFILTRO>{ data($resp /TFILTRO) }</TFILTRO>,
					<VFILTRO>{ data($resp /VFILTRO) }</VFILTRO>,
                    <ResultSetID>{data($resp/ns0:ResultSetID)}</ResultSetID>,
                    <REGS>
                        {
                            for $item in $resp/ns0:RowSet/ns0:Row
                            return
                            (
                                <REG>
                                    <AGE>{ data($item/ns0:Column[@name = 'AGE']) }</AGE>
                                    <SINI>{ data($item/ns0:Column[@name = 'SINI']) }</SINI>
                                    <RAMO>{ data($item/ns0:Column[@name = 'RAMO']) }</RAMO>
                                    <CLIDES>{ data($item/ns0:Column[@name = 'CLIDES']) }</CLIDES>
		                  			<PROD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)) }</PROD>
		                  			<POL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,8)) }</POL>
									<CERPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERPOL>
							  		<CERANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERANN>
							  		<CERSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERSEC>
                                    <MON>{ data($item/ns0:Column[@name = 'MON']) }</MON>
                                    <IMPTO>{ local:format_Num_aQBE(data($item/ns0:Column[@name = 'IMPTO'])) }</IMPTO>
									<I_30>{ local:format_Num_aQBE(data($item/ns0:Column[@name = 'I_30'])) }</I_30>
									<I_60>{ local:format_Num_aQBE(data($item/ns0:Column[@name = 'I_60'])) }</I_60>
									<I_90>{ local:format_Num_aQBE(data($item/ns0:Column[@name = 'I_90'])) }</I_90>
									<I_M90>{ local:format_Num_aQBE(data($item/ns0:Column[@name = 'I_M90'])) }</I_M90>
                                    {
                                    <EST>
                                    		{
                                          	let $est := data($item/ns0:Column[@name = 'EST'])
                                          	return
                                          	if ($est = 'VIG') then ('VIG')
                                          	else ('SUS')
                                   			}
                                   	</EST>
                                   	}
                                    <FECVTO>{ fn-bea:date-to-string-with-format("dd/MM/yyyy",data($item/ns0:Column[@name = 'FECVTO'])) }</FECVTO>
                                    <ALERTEXI>{ data($item/ns0:Column[@name = 'ALERTEX']) }</ALERTEXI>
                                </REG>
                            )
                        }
                    </REGS>,
					<TOTS>
                        <TOT>
                        	<TMON>$</TMON>
                        	<T_IMPTOS/>
                    	    <T_IMPTO>
                        	{
                        		let $tot := sum($resp/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = '$']/ns0:Column[@name = 'IMPTO'])
                        		return fn-bea:format-number($tot,'#.00')

                        	}
                        	</T_IMPTO>
							<T_30S/>
							<T_30>
                        	{
                        		let $tot := sum($resp/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = '$']/ns0:Column[@name = 'I_30'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_30>
							<T_60S/>
							<T_60>
                        	{
                        		let $tot := sum($resp/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = '$']/ns0:Column[@name = 'I_60'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_60>
							<T_90S/>
							<T_90>
                        	{
                        		let $tot := sum($resp/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = '$']/ns0:Column[@name = 'I_90'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_90>
							<T_M90S/>
							<T_M90>
                        	{
                        		let $tot := sum($resp/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = '$']/ns0:Column[@name = 'I_M90'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_M90>
						</TOT>
                        <TOT>
                        	<TMON>U$S</TMON>
                        	<T_IMPTOS/>
                    	    <T_IMPTO>
                        	{
                        		let $tot := sum($resp/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = 'U$S']/ns0:Column[@name = 'IMPTO'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
                        	</T_IMPTO>
							<T_30S/>
							<T_30>
                        	{
                        		let $tot := sum($resp/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = 'U$S']/ns0:Column[@name = 'I_30'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_30>
							<T_60S/>
							<T_60>
                        	{
                        		let $tot := sum($resp/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = 'U$S']/ns0:Column[@name = 'I_60'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_60>
							<T_90S/>
							<T_90>
                        	{
                        		let $tot := sum($resp/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = 'U$S']/ns0:Column[@name = 'I_90'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_90>
							<T_M90S/>
							<T_M90>
                        	{
                        		let $tot := sum($resp/ns0:RowSet/ns0:Row[ns0:Column[@name='MON'] = 'U$S']/ns0:Column[@name = 'I_M90'])
                        		return fn-bea:format-number($tot,'#.00')
                        	}
							</T_M90>
						</TOT>
					</TOTS>
                )
            }
        </Response>
};

declare variable $resp as element() external;

declare function local:format_Num_aQBE($input as xs:decimal)
    as xs:string {

	if(string(number($input)) != 'NaN') then (
		replace(replace(replace(xs:string(fn-bea:format-number(fn-bea:decimal-round($input,2),'#,##0.00')),',','!'),'\.',','),'!','.')
	) else (
		'0,00'
	)

};

xf:Paginated_1404_From_PaginatedQueryRs($resp)