xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1107.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1107_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1107_From_PaginatedQueryRs($resp as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>,
                    <MSGEST>ER</MSGEST>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    (: Tiene un TR porque lo usamos cuando consultamos por paginas. En caso de que no haya mas, la proxima consulta devolvera un ER :)
                    <MSGEST>TR</MSGEST>,
                    <FECCONT></FECCONT>,
                    <POLIZACONT></POLIZACONT>,
                    <PRIMERAPAGINA/>,
                    <REGS>
                        {
                            for $item in $resp/ns0:RowSet/ns0:Row
                            return
                            (
                                <REG>
                                    <PROD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)) }</PROD>
                                    <POL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,8)) }</POL>
                                    <CERPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERPOL>
                                    <CERANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERANN>
                                    <CERSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERSEC>
                                    <CLIDES>{ data($item/ns0:Column[@name = 'CLIDES']) }</CLIDES>
                                    <TOMARIES>{ data($item/ns0:Column[@name = 'TOMARIES']) }</TOMARIES>
                                    <EST>{ data($item/ns0:Column[@name = 'EST']) }</EST>
                                    <TIPOPE>{ data($item/ns0:Column[@name = 'TIPOPE']) }</TIPOPE>
                                    <FECING>{ fn-bea:date-to-string-with-format("dd/MM/yyyy", data($item/ns0:Column[@name = 'FECING'])) }</FECING>
                                    <COD>
                                    	{
                                    		let $cod := data($item/ns0:Column[@name = 'COD'])
                                    		return concat( substring($cod, 1, 2), '-', substring($cod, 3) )
                                    	}
                                    </COD>
                                    {
                                    	let $ramo := data($item/ns0:Column[@name = 'RAMO'])
                                    	return
                                    		<RAMO>
                                    			{
                                    				if ($ramo = 'M') then (1)
                                    				else if ($ramo = 'C') then (2)
                                    				else ()
                                    			}
                                    		</RAMO>
                                    }
                                    <LUPA>{ data($item/ns0:Column[@name = 'LUPA']) }</LUPA>
                                    <RENOVACION>{ data($item/ns0:Column[@name = 'RENOVACION']) }</RENOVACION>
                                    <MARCAOV>{ data($item/ns0:Column[@name = 'MARCAOV']) }</MARCAOV>
                                    <SQLCERTIPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_REF'],9,4)) }</SQLCERTIPOL>
                                    <SQLCERTIANN>{ data(substring($item/ns0:Column[@name = 'POLICY_REF'],14,4)) }</SQLCERTIANN>
                                    <SQLCERTISEC>{ data(substring($item/ns0:Column[@name = 'POLICY_REF'],17,6)) }</SQLCERTISEC>
                                </REG>
                            )
                        }
                    </REGS>
                )
            }
        </Response>
};

declare variable $resp as element() external;

xf:OV_1107_From_PaginatedQueryRs($resp)