xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1412.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1412_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1412_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
					<REGS>
				 	{
	            		for $row in $paginatedQueryRs1/ns0:RowSet/ns0:Row
	            		return
	            		<REG>
	            			<ENDOSO>{ data($row/ns0:Column[@name = 'ENDOSO']) }</ENDOSO>
	            			<RECNUM>{ data($row/ns0:Column[@name = 'RECNUM']) }</RECNUM>
	            			<SIGIMP>{ data($row/ns0:Column[@name = 'SIGIMP']) }</SIGIMP>
	            			<IMP>{ 
	            				let $imp := data($row/ns0:Column[@name = 'IMP']) 
	            			    return $imp div 1000
	            				}
	            			</IMP> (:este valor deberia venir con coma ,se consulta a FADATA:)
	            			<COB>{ data($row/ns0:Column[@name = 'COB']) }</COB>
	            			<FECVTO>{ fn-bea:date-to-string-with-format("dd/MM/yyyy", data($row/ns0:Column[@name = 'FECVTO'])) }</FECVTO>
	            			<SIGDIA>{ data($row/ns0:Column[@name = 'SIGDIA']) }</SIGDIA>
	            			<DIAS>{ fn-bea:format-number(floor(xs:double($row/ns0:Column[@name = 'DIAS'])), '0') }</DIAS>
	            			<MON>{ data($row/ns0:Column[@name = 'MON']) }</MON>
	                    </REG>
	                }
	                </REGS>

            	)
			}
        </Response>
};

declare variable $paginatedQueryRs1 as element() external;

xf:OV_1412_From_PaginatedQueryRs($paginatedQueryRs1)
