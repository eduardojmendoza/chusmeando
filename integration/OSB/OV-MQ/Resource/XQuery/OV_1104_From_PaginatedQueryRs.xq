xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1104.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1104_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1104_From_PaginatedQueryRs($resp as element())
    as element(Response) {
         <Response>
             {
                 if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                     <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                 ) else (
                     <Estado resultado = "true" mensaje = ""/>,
                     <REGS>
                         {
                             for $item in $resp/ns0:RowSet/ns0:Row
                             return
                                 <REG>
                                     <DESC>{ data($item/ns0:Column[@name = 'DESCR']) }</DESC>
                                     <IMP>{ data($item/ns0:Column[@name = 'IMP']) }</IMP>
                                     {
                                     if ( $item/ns0:Column[@name = 'SIG'] = 'plus' )
                                     then ( <SIG>{' '}</SIG>)
                                     else ( <SIG>-</SIG>)
                                     }
                                 </REG>
                         }
                     </REGS>
                 )
            }
        </Response>
};

declare variable $resp as element() external;

xf:OV_1104_From_PaginatedQueryRs($resp)