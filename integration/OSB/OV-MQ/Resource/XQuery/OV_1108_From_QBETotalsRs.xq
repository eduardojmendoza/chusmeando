(:: pragma bea:global-element-parameter parameter="$response1" element="Response" location="../XSD/OV_1108.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1108.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1108_From_QBETotalsRs/";

declare function xf:OV_1108_From_QBETotalsRs($response1 as element(Response))
    as element(Response) {
        <Response>
            <Estado>{ data($response1/Estado) }</Estado>
            <EST>
                {
                if  (exists($response1/EST/GO)) then ( 
 				<GO>{ $response1/EST/GO/@* , $response1/EST/GO/node() }</GO> 
				) else ()
				}   
                {
                if  (exists($response1/EST/OR)) then ( 
 				<OR>{ $response1/EST/OR/@* , $response1/EST/OR/node() }</OR> 
				) else ()
				}
                <PR CLI = "{ data($response1/EST/PR/@CLI) }"
                    NOM = "{ data($response1/EST/PR/@NOM) }"
                    NIV = "{ data($response1/EST/PR/@NIV) }"
                    ESTSTR = "{ data($response1/EST/PR/@ESTSTR) }"
                    TOTPOL = "{ replace( data($response1/EST/PR/@TOTPOL), '\,', '') }"/>
                    
            </EST>
        </Response>
};

declare variable $response1 as element(Response) external;

xf:OV_1108_From_QBETotalsRs($response1)
