xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1401.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1401_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1401_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>,
                    <MSGEST>ER</MSGEST>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <MSGEST>TR</MSGEST>,
                    <CONTINUAR>{ data($paginatedQueryRs1 /CONTINUAR) }</CONTINUAR>,
					<ResultSetID>{data($paginatedQueryRs1/ns0:ResultSetID)}</ResultSetID>,
					<REGS>
				 	{
	            		for $row in $paginatedQueryRs1/ns0:RowSet/ns0:Row
	            		return
	            		<REG>
							<PROD>{ substring(data($row/ns0:Column[@name = 'CLAIM_REQUEST_ID']),5,4) }</PROD>
							<SINIAN>{ substring(data($row/ns0:Column[@name = 'CLAIM_REQUEST_ID']),9,2) }</SINIAN>
	            			<SININUM>{ substring(data($row/ns0:Column[@name = 'CLAIM_REQUEST_ID']),11,6) }</SININUM>
	            			<CLIDES>{ data($row/ns0:Column[@name = 'CLIDES']) }</CLIDES>
	            			{
                            	let $ramo := data($row/ns0:Column[@name = 'RAMO'])
                            	return
                            <RAMO>{
	                           if ($ramo = 'M') then (1)
	                           else if ($ramo = 'C') then (2)
	                           else ()
                           }</RAMO>
                            }
	            			<POL>{ substring(data($row/ns0:Column[@name = 'POLICY_NO']),9,8) }</POL>
                        	<CERPOL>{ substring(data($row/ns0:Column[@name = 'POLICY_NO']),17,4) }</CERPOL>
                        	<CERANN>{ substring(data($row/ns0:Column[@name = 'POLICY_NO']),21,4) }</CERANN>
                        	<CERSEC>{ substring(data($row/ns0:Column[@name = 'POLICY_NO']),25,6) }</CERSEC>
                        	<EST>{
									let $est := data($row/ns0:Column[@name = 'EST'])
									return
									if ($est = 'OPEN') then ('A') else ('C')
							}</EST>
							<FECSINI>{
									let $fecha := data($row/ns0:Column[@name = 'FECSINI'])
									return
									if ($fecha != '') then (fn-bea:date-to-string-with-format("dd/MM/yyyy", $fecha)) else ()
							}</FECSINI>	
	            			<AGE>{ data($row/ns0:Column[@name = 'AGE']) }</AGE>
	                    </REG>
	                }
	                </REGS> )
			}
        </Response>
};

declare variable $paginatedQueryRs1 as element() external;

declare function local:format_Num_aQBE($input as xs:decimal)
    as xs:string {
	
	if(string(number($input)) != 'NaN') then (
		replace(replace(replace(xs:string(fn-bea:format-number(fn-bea:decimal-round($input,2),'#,##0.00')),',','!'),'\.',','),'!','.')
	) else (
		'0.00'
	)
	
};

xf:OV_1401_From_PaginatedQueryRs($paginatedQueryRs1)
