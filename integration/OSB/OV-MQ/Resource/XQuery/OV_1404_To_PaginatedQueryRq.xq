xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_1404.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1404_To_PaginatedQueryRq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1404_To_PaginatedQueryRq($req as element(Request))
    as element() {
        <ns0:PaginatedQueryRq>
        	<ns0:QueryID>1404</ns0:QueryID>
            <ns0:FilterCriteria>
                <ns0:FilterCriterion field = "NIVELAS"
                                     operation = "EQ"
                                     value = "{ data($req/NIVELAS) }" />
                <ns0:FilterCriterion field = "CLIENSECAS"
                                     operation = "EQ"
                                     value = "{ data($req/CLIENSECAS) }" />
                <ns0:FilterCriterion field = "NIVEL1"
                                     operation = "EQ"
                                     value = "{ data($req/NIVEL1) }" />
                <ns0:FilterCriterion field = "CLIENSEC1"
                                     operation = "EQ"
                                     value = "{ data($req/CLIENSEC1) }" />
                <ns0:FilterCriterion field = "NIVEL2"
                                     operation = "EQ"
                                     value = "{ data($req/NIVEL2) }" />
                <ns0:FilterCriterion field = "CLIENSEC2"
                                     operation = "EQ"
                                     value = "{ data($req/CLIENSEC2) }" />
                <ns0:FilterCriterion field = "NIVEL3"
                                     operation = "EQ"
                                     value = "{ data($req/NIVEL3) }" />
                <ns0:FilterCriterion field = "CLIENSEC3"
                                     operation = "EQ"
                                     value = "{ data($req/CLIENSEC3) }" />
                <ns0:FilterCriterion field = "TFILTRO"
                                     operation = "EQ"
                                     value = "{ data($req/TFILTRO) }" />
                <ns0:FilterCriterion field = "VFILTRO"
                                     operation = "EQ"
                                     value = "{ data($req/VFILTRO) }" />
            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $req as element(Request) external;

xf:OV_1404_To_PaginatedQueryRq($req)