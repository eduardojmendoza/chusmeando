xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_1308.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1308_To_PaginatedQueryRq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1308_To_PaginatedQueryRq($req as element(Request))
    as element() {
        <ns0:PaginatedQueryRq>
            <ns0:QueryID>1308</ns0:QueryID>
	            <ns0:FilterCriteria>
						<ns0:FilterCriterion field = "CLAIM_NO"
		                                     	operation = "EQ"
		                                    	value = "{ concat('0001'
		                                    			,data($req/PRODUCTO)
                                     					,data($req/SINIAN)
                                     					,data($req/SININUM))}" />

	            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $req as element(Request) external;

xf:OV_1308_To_PaginatedQueryRq($req)
