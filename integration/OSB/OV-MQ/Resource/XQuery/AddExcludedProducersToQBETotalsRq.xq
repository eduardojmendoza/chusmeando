(:: pragma bea:local-element-parameter parameter="$qBETotalsRq" type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QBETotalsRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QBETotalsRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/AddExcludedProducersToQBETotalsRq/";

declare function xf:AddExcludedProducersToQBETotalsRq($qBETotalsRq as element(),
    $agentesExcluidosList as xs:string)
    as element() {
        <ns0:QBETotalsRq>
            <ns0:QueryID>{ data($qBETotalsRq/ns0:QueryID) }</ns0:QueryID>
            <ns0:Cliensec>{ data($qBETotalsRq/ns0:Cliensec) }</ns0:Cliensec>
            <ns0:Nivel>{ data($qBETotalsRq/ns0:Nivel) }</ns0:Nivel>
            <ns0:FilterCriteria>
            {
                for $FilterCriterion in $qBETotalsRq/ns0:FilterCriteria/ns0:FilterCriterion
                return
                    <ns0:FilterCriterion>{ $FilterCriterion/@* , $FilterCriterion/node() }</ns0:FilterCriterion>
            }
                 <ns0:FilterCriterion field="EXCLUDE" operation="EQ" value="{ $agentesExcluidosList }"/>
            </ns0:FilterCriteria>

        </ns0:QBETotalsRq>
};

declare variable $qBETotalsRq as element() external;
declare variable $agentesExcluidosList as xs:string external;

xf:AddExcludedProducersToQBETotalsRq($qBETotalsRq,
    $agentesExcluidosList)
