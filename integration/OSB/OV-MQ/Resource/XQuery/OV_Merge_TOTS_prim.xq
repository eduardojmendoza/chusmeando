xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$totsLeft" element="TOTS" location="../XSD/OV_Merge_TOTS.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$totsRight" element="TOTS" location="../XSD/OV_Merge_TOTS.xsd" ::)
(:: pragma bea:global-element-return element="TOTS" location="../XSD/OV_Merge_TOTS.xsd" ::)


(: OV_Merge_TOTS_prim mergea nodos TOTS sin formatear los numeros en el resultado, como hace OV_Merge_TOTS :)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_Merge_TOTS/";

declare function xf:OV_Merge_TOTS($totsLeft as element(),$totsRight as element())
    as element(TOTS) {

		<TOTS>
            <TOT>
            	<TMON>$</TMON>
            	<T_IMPTOS/>
        	    <T_IMPTO>
            	{
            		local:sumar($totsLeft/TOT[normalize-space(TMON)='$'][1]/T_IMPTO,$totsRight/TOT[normalize-space(TMON)='$'][1]/T_IMPTO)
            	}
            	</T_IMPTO>
				<T_30S/>
				<T_30>
            	{
            		local:sumar($totsLeft/TOT[normalize-space(TMON)='$'][1]/T_30,$totsRight/TOT[normalize-space(TMON)='$'][1]/T_30)
            	}
				</T_30>
				<T_60S/>
				<T_60>
            	{
            		local:sumar($totsLeft/TOT[normalize-space(TMON)='$'][1]/T_60,$totsRight/TOT[normalize-space(TMON)='$'][1]/T_60)
            	}
				</T_60>
				<T_90S/>
				<T_90>
            	{
            		local:sumar($totsLeft/TOT[normalize-space(TMON)='$'][1]/T_90,$totsRight/TOT[normalize-space(TMON)='$'][1]/T_90)
            	}
				</T_90>
				<T_M90S/>
				<T_M90>
            	{
            		local:sumar($totsLeft/TOT[normalize-space(TMON)='$'][1]/T_M90,$totsRight/TOT[normalize-space(TMON)='$'][1]/T_M90)
            	}
				</T_M90>
			</TOT>
            <TOT>
            	<TMON>U$S</TMON>
            	<T_IMPTOS/>
        	    <T_IMPTO>
            	{
            		local:sumar($totsLeft/TOT[normalize-space(TMON)='U$S'][1]/T_IMPTO,$totsRight/TOT[normalize-space(TMON)='U$S'][1]/T_IMPTO)
            	}
            	</T_IMPTO>
				<T_30S/>
				<T_30>
            	{
            		local:sumar($totsLeft/TOT[normalize-space(TMON)='U$S'][1]/T_30,$totsRight/TOT[normalize-space(TMON)='U$S'][1]/T_30)
            	}
				</T_30>
				<T_60S/>
				<T_60>
            	{
            		local:sumar($totsLeft/TOT[normalize-space(TMON)='U$S'][1]/T_60,$totsRight/TOT[normalize-space(TMON)='U$S'][1]/T_60)
            	}
				</T_60>
				<T_90S/>
				<T_90>
            	{
            		local:sumar($totsLeft/TOT[normalize-space(TMON)='U$S'][1]/T_90,$totsRight/TOT[normalize-space(TMON)='U$S'][1]/T_90)
            	}
				</T_90>
				<T_M90S/>
				<T_M90>
            	{
            		local:sumar($totsLeft/TOT[normalize-space(TMON)='U$S'][1]/T_M90,$totsRight/TOT[normalize-space(TMON)='U$S'][1]/T_M90)
            	}
				</T_M90>
			</TOT>
		</TOTS>

};

declare variable $totsLeft as element() external;
declare variable $totsRight as element() external;

declare function local:sumar($left as element()?, $right as element()?)
    as xs:decimal {

	let $leftSafe := if ( not(exists($left)) or data($left) = '' ) then ('0.0') else ( $left )
	let $rightSafe := if ( not(exists($right)) or data($right) = '' ) then ('0.0') else ( $right )
	let $total := xs:double($leftSafe) + xs:double(data($rightSafe))
	return xs:decimal($total)
};


xf:OV_Merge_TOTS($totsLeft,$totsRight)
