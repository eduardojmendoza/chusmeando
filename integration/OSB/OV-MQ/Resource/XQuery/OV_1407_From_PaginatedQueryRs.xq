	xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1407.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1407_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1407_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <REGS>
                    {
                    	for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row
                    	return
                    	(
                        	<REG>
                        		<RECNUM>{ data($item/ns0:Column[@name = 'RECNUM']) }</RECNUM>
                        		<SIG>{ data($item/ns0:Column[@name = 'SIG']) }</SIG>
                        		<IMP>{ data($item/ns0:Column[@name = 'IMP']) }</IMP>
                        		<FECVTO>{ fn-bea:date-to-string-with-format("dd/MM/yyyy", data($item/ns0:Column[@name = 'FECVTO'])) }</FECVTO>
                        		<MON>{ data($item/ns0:Column[@name = 'MON']) }</MON>
                        		<FEPAGO>{ 
                        		if ( exists($item/ns0:Column[@name = 'FEPAGO']) and ($item/ns0:Column[@name = 'FEPAGO']) != '' )
									then ( fn-bea:date-to-string-with-format("yyyyMMdd",  data($item/ns0:Column[@name = 'FEPAGO'])))
								else ( )
                        		 }</FEPAGO>
							</REG>
						)
					}
                    </REGS>
            	)
			}
        </Response>
};

declare variable $paginatedQueryRs1 as element() external;

xf:OV_1407_From_PaginatedQueryRs($paginatedQueryRs1)
