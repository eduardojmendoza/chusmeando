(:: pragma bea:global-element-parameter parameter="$executeRequest1" element="ns0:executeRequest" location="../../../OV/Resource/WSDL/OVSvc.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:executeRequest" location="../../../OV/Resource/WSDL/OVSvc.wsdl" ::)

declare namespace ns0 = "http://cms.services.qbe.com/";
declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/AddPaginatedToActionCode/";

declare function xf:AddPaginatedToActionCode($executeRequest1 as element(ns0:executeRequest))
    as element(ns0:executeRequest) {
        <ns0:executeRequest>
            <actionCode>{ concat(data($executeRequest1/actionCode),'_Paginado') }</actionCode>
            <Request>{ $executeRequest1/Request/@* , $executeRequest1/Request/node() }</Request>
            <contextInfo>{ data($executeRequest1/contextInfo) }</contextInfo>
        </ns0:executeRequest>
};

declare variable $executeRequest1 as element(ns0:executeRequest) external;

xf:AddPaginatedToActionCode($executeRequest1)
