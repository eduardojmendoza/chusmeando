xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1416.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1416_From_PaginatedQueryRs/";

declare function xf:OV_1416_From_PaginatedQueryRs($resp as element(), $Rendidos as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>,
                    <MSGEST>ER</MSGEST>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <MSGEST>OK</MSGEST>,
                    <COTIDOLAR>{ data($resp/ns0:RowSet/ns0:Row[0]/ns0:Column[@name = 'COTIDOLAR']) }</COTIDOLAR>,
                    <REGS>
                        {
                            for $item in $resp/ns0:RowSet/ns0:Row
                            return
                            (
                                <REG>
                                    <RAMO>{ data($item/ns0:Column[@name = 'RAMO']) }</RAMO>
                                    <CLIDES>{ data($item/ns0:Column[@name = 'CLIDES']) }</CLIDES>
                                    <CLIENSEC>{ data($item/ns0:Column[@name = 'CLIENSEC']) }</CLIENSEC>
                                    <PROD>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']), 5, 4) }</PROD>
                                    <POL>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']), 9, 8) }</POL>
                                    <CERPOL>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']), 17, 4) }</CERPOL>
                                    <CERANN>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']), 21, 4) }</CERANN>
                                    <CERSEC>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']), 25, 6) }</CERSEC>
                                    <OPERAPOL>{ data($item/ns0:Column[@name = 'OPERAPOL'])  }</OPERAPOL>
                                    <RECNUM>{ data($item/ns0:Column[@name = 'RECNUM']) }</RECNUM>
                                    <MON>{ data($item/ns0:Column[@name = 'MON']) }</MON>
                                    <SIG>{
                                    if (data($item/ns0:Column[@name = 'SIG']) = '-')
                                    then(data($item/ns0:Column[@name = 'SIG']))
                                    else(' ')
                                    }</SIG>
                                    <IMP>{ data($item/ns0:Column[@name = 'IMP']) }</IMP>
                                    <IMPCALC>{ data($item/ns0:Column[@name = 'IMPCALC']) }</IMPCALC>                                    
                                    <RENDIDO>{ data(
                                    	$Rendidos/Recibos/Recibo[NumRecibo = concat("0001",data($item/ns0:Column[@name = 'RECNUM']))]/Rendido
                                    )}</RENDIDO>
                                    <FECVTO>{ fn-bea:date-to-string-with-format('dd/MM/yyyy', data($item/ns0:Column[@name = 'FECVTO'])) }</FECVTO>
                            		<AGE>{ concat (substring(
                        			data($item/ns0:Column[@name = 'AGE']),1,2)
                        			,'-'
                        			,substring(data($item/ns0:Column[@name = 'AGE']),3,4)) 
                        			}</AGE>
                                    <ESTADO>{ data($item/ns0:Column[@name = 'ESTADO']) }</ESTADO>
                                </REG>
                            )
                        }
                    </REGS>
                )
            }
        </Response>
};

declare variable $resp as element() external;
declare variable $Rendidos as element() external;

xf:OV_1416_From_PaginatedQueryRs($resp, $Rendidos)