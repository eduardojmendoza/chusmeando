(:: pragma bea:global-element-parameter parameter="$request1" element="Request" location="../XSD/OV_1407.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1407_To_PaginatedQueryRq/";

declare function xf:OV_1407_To_PaginatedQueryRq($request1 as element(Request))
    as element() {
        <ns0:PaginatedQueryRq>
            <ns0:QueryID>1407</ns0:QueryID>
            <ns0:FilterCriteria>
				<ns0:FilterCriterion field = "POLICY_NO"
                                     operation = "EQ"
                                     value = "{ concat('0001'
                                     			,data($request1/PRODUCTO),data($request1/POLIZA)) }" />

				<ns0:FilterCriterion field = "FECENVIO"
                                     operation = "EQ"
                                     value = "{ fn-bea:date-from-string-with-format('dd/MM/yyyy', data($request1/FECENVIO)) }" />
            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $request1 as element(Request) external;

xf:OV_1407_To_PaginatedQueryRq($request1)
