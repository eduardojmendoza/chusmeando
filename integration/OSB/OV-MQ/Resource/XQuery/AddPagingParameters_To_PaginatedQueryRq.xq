xquery version "1.0" encoding "UTF-8";
(:: pragma bea:schema-type-parameter parameter="$paginatedQueryRq" type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)


declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/AddPagingParameters_To_PaginatedQueryRq/";

declare function xf:AddPagingParameters_To_PaginatedQueryRq($paginatedQueryRq as element(),
    $StartRow as xs:integer,
    $RowCount as xs:integer,
    $ResultSetID as xs:string)
    as element() {
        <ns0:PaginatedQueryRq>
            <ns0:QueryID>{ data($paginatedQueryRq/ns0:QueryID) }</ns0:QueryID>
            <ns0:StartRow>{ $StartRow }</ns0:StartRow>
            <ns0:RowCount>{ $RowCount }</ns0:RowCount>
            {
            if ( exists($ResultSetID) and data($ResultSetID) != '') then (
            <ns0:ResultSetID>{ $ResultSetID }</ns0:ResultSetID>
            ) else ()
            }
            {
                for $FilterCriteria in $paginatedQueryRq/ns0:FilterCriteria
                return
                    <ns0:FilterCriteria>{ $FilterCriteria/@* , $FilterCriteria/node() }</ns0:FilterCriteria>
            }
        </ns0:PaginatedQueryRq>
};

declare variable $paginatedQueryRq as element() external;
declare variable $StartRow as xs:integer external;
declare variable $RowCount as xs:integer external;
declare variable $ResultSetID as xs:string external;

xf:AddPagingParameters_To_PaginatedQueryRq($paginatedQueryRq,
    $StartRow,
    $RowCount,
    $ResultSetID)
