(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_1405.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QBETotalsRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1405_To_PaginatedQueryRq/";

declare function xf:OV_1405_To_PaginatedQueryRq($req as element(Request))
    as element() {
        <ns0:QBETotalsRq>
            <ns0:QueryID>1405</ns0:QueryID>
            <ns0:Cliensec>{ data($req/CLIENSECAS) }</ns0:Cliensec>
            <ns0:Nivel>{ data($req/NIVELAS) }</ns0:Nivel>
            <ns0:FilterCriteria>
                <ns0:FilterCriterion field = "DUMMY"
                                     operation = "EQ"
                                     value = "" />
            </ns0:FilterCriteria>
        </ns0:QBETotalsRq>
};

declare variable $req as element(Request) external;

xf:OV_1405_To_PaginatedQueryRq($req)
