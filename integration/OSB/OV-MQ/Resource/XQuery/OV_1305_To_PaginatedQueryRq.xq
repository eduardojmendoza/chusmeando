(:: pragma bea:global-element-parameter parameter="$request1" element="Request" location="../XSD/OV_1305.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1305_To_PaginatedQueryRq/";

declare function xf:OV_1305_To_PaginatedQueryRq($request1 as element(Request))
    as element() {
        <ns0:PaginatedQueryRq>
            <ns0:QueryID>1305</ns0:QueryID>
            <ns0:FilterCriteria>
            	{
            	if (data($request1/PRODUCTO) != '' and data($request1/SININUM) != '' and data($request1/SINIAN) != '') then
            	(
	                <ns0:FilterCriterion field = "CLAIM_NO"
	                                     operation = "EQ"
	                                     value = "{ 			concat('0001'
	                                     						,fn-bea:trim(data($request1/PRODUCTO))
	                                     						,fn-bea:trim(data($request1/SINIAN))
	                                     						,fn-bea:trim(data($request1/SININUM))) }" />
	           	) else (
	           			<ns0:FilterCriterion field = "DUMMY"
                                     operation = "EQ"
                                     value = "" />
                      	)
	           	}
            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $request1 as element(Request) external;

xf:OV_1305_To_PaginatedQueryRq($request1)
