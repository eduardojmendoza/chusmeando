(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:mfl-element-return type="Msg0008Rs@" location="../MFL/Msg_0008_Rs.mfl" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/Msg_0008_From_PaginatedQueryRs/";

declare function xf:Msg_0008_From_PaginatedQueryRs($resp as element())
    as element() {
        <Msg0008Rs>
        	<POLIZAS>
            {
	    		let $RowSet := $resp/ns0:RowSet
	    		return
	        		for $i in (1 to 50)
	        			let $Row := $RowSet[1]/ns0:Row[$i]
	    			return

                            <POLIZA>
                                <RAMOPCOD>{ substring(data($Row/ns0:Column[@name = 'POLICY_NO']), 5, 4) }</RAMOPCOD>
                                <POLIZANN>{ substring(data($Row/ns0:Column[@name = 'POLICY_NO']), 9, 2) }</POLIZANN>
                           		<POLIZSEC>{ substring(data($Row/ns0:Column[@name = 'POLICY_NO']), 11, 6) }</POLIZSEC>
                                <CERTIPOL>{ substring(data($Row/ns0:Column[@name = 'POLICY_NO']), 17, 4) }</CERTIPOL>
                         		<CERTIANN>{ substring(data($Row/ns0:Column[@name = 'POLICY_NO']), 21, 4) }</CERTIANN>
                                <CERTISEC>{ substring(data($Row/ns0:Column[@name = 'POLICY_NO']), 25, 6) }</CERTISEC>
                                <EMISI>{ fn-bea:date-to-string-with-format('yyyyMMdd', $Row/ns0:Column[@name = 'EMISIDATE']) }</EMISI>
                                
                            </POLIZA>

            }
        	</POLIZAS>
        </Msg0008Rs>
};

declare variable $resp as element() external;

xf:Msg_0008_From_PaginatedQueryRs($resp)