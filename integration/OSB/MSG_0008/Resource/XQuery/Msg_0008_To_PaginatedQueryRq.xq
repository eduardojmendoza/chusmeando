(:: pragma bea:mfl-element-parameter parameter="$req" type="Msg0008Rq@" location="../MFL/Msg_0008_Rq.mfl" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/Msg_0008_To_PaginatedQueryRq/";

declare function xf:Msg_0008_To_PaginatedQueryRq($req as element())
    as element() {
        <ns0:PaginatedQueryRq>
            <ns0:QueryID>0008</ns0:QueryID>
            <ns0:FilterCriteria>
                <ns0:FilterCriterion field = "CLIENSEC"
                					 operation = "EQ"
                                     value = "{ data($req/CLIENSEC) }" />
                <ns0:FilterCriterion field = "EFFECTIVE_DATE"
                					 operation = "EQ"
                                     value = "{ fn-bea:date-from-string-with-format('yyyyMMdd', data($req/EFECT)) }" />
            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $req as element() external;

xf:Msg_0008_To_PaginatedQueryRq($req)