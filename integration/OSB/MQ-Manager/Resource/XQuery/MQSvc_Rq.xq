(:: pragma bea:global-element-return element="ns0:MQRq" location="../XSD/MQSvc.xsd" ::)

declare namespace ns0 = "http://xml.qbe.com/MQ-Manager/MQSvc";
declare namespace xf = "http://tempuri.org/MQ-Manager/Resource/XQuery/MQSvc_Rq/";

declare function xf:MQSvc_Rq($path as xs:string, $payload as element(*))
    as element(ns0:MQRq) {
        <ns0:MQRq>
            <ns0:Path>{ $path }</ns0:Path>
            <ns0:Payload>{ $payload }</ns0:Payload>
        </ns0:MQRq>
};

declare variable $path as xs:string external;
declare variable $payload as element(*) external;

xf:MQSvc_Rq($path, $payload)
