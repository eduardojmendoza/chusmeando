(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1101_DatosGralPoliza.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1101_From_PaginatedQueryRs/";

declare function xf:OV_1101_From_PaginatedQueryRs($resp as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <CAMPOS>
                    {
                        let $item := $resp/ns0:RowSet/ns0:Row[1]
                        return
                            (<OPERPTE>{ data($item/ns0:Column[@name = 'OPERPTE']) }</OPERPTE>,
                            <CLIENSEC>{ data($item/ns0:Column[@name = 'CLIENSEC']) }</CLIENSEC>,
                            <CLIENDES>{ data($item/ns0:Column[@name = 'CLIENDES']) }</CLIENDES>,
                            <DOMICSEC>{ data($item/ns0:Column[@name = 'DOMICSEC']) }</DOMICSEC>,
                            <CUENTSEC>{ data($item/ns0:Column[@name = 'CUENTSEC']) }</CUENTSEC>,
                            <SITUCPOL>{ data($item/ns0:Column[@name = 'SITUCPOL']) }</SITUCPOL>,
                            <ACREEDOR>{ data($item/ns0:Column[@name = 'ACREEDOR']) }</ACREEDOR>,
                            <FECINI>{ data($item/ns0:Column[@name = 'FECINI']) }</FECINI>,
                            <FECULTRE>{  data($item/ns0:Column[@name = 'FECULTRE']) }</FECULTRE>,
                            <COBROCOD>{ data($item/ns0:Column[@name = 'COBROCOD']) }</COBROCOD>,
                            <COBRODAB>{ data($item/ns0:Column[@name = 'COBRODAB']) }</COBRODAB>,
                            <COBROTIP>{ data($item/ns0:Column[@name = 'COBROTIP']) }</COBROTIP>,
                            <COBRODES>{ data($item/ns0:Column[@name = 'COBRODES']) }</COBRODES>,
                            <CUENTNUM>{ data($item/ns0:Column[@name = 'CUENTNUM']) }</CUENTNUM>,
                            <CAMPACOD>{ data($item/ns0:Column[@name = 'CAMPACOD']) }</CAMPACOD>,
                            <CAMPADES>{ data($item/ns0:Column[@name = 'CAMPADES']) }</CAMPADES>,
                            <GRUPOASE>{ data($item/ns0:Column[@name = 'GRUPOASE']) }</GRUPOASE>,
                            <CLUBECO>{ data($item/ns0:Column[@name = 'CLUBECO']) }</CLUBECO>,
                            <TARJECOD>{ 0 }</TARJECOD>,
                            <OPTP6511>{ data($item/ns0:Column[@name = 'OPTP6511']) }</OPTP6511>)
                    }
                    </CAMPOS>
                )
            }
        </Response>
};

declare variable $resp as element() external;

xf:OV_1101_From_PaginatedQueryRs($resp)
