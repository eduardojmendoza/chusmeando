xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_1427_GetDeudaCobradaCanales.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1427_To_PaginatedQueryRq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1427_To_PaginatedQueryRq($req as element(Request))
    as element() {

	<ns0:PaginatedQueryRq>
            <ns0:QueryID>1427</ns0:QueryID>
            <ns0:FilterCriteria>
                {
                    if (data($req/NIVELCLA1) = '' and data($req/CLIENSEC1) = ''
                        and data($req/NIVELCLA2) = '' and data($req/CLIENSEC2) = ''
                        and data($req/NIVELCLA3) = '' and data($req/CLIENSEC3) = '')
                    then (
                        <ns0:FilterCriterion field = "CLIENSEC{ index-of(('GO', 'OR', 'PR'), upper-case(data($req/NIVELAS))) }"
                            				 operation = "EQ"
                           					  value = "{ data($req/CLIENSECAS) }"/>,
                        <ns0:FilterCriterion field = "NIVEL{ index-of(('GO', 'OR', 'PR'), upper-case(data($req/NIVELAS))) }"
                                             operation = "EQ"
                                             value = "{ data($req/NIVELAS) }"/>
                    )
                    else (
                        <ns0:FilterCriterion field = "NIVEL1"
                                             operation = "EQ"
                                             value = "{ data($req/NIVELCLA1) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC1"
                                             operation = "EQ"
                                             value = "{ data($req/CLIENSEC1) }"/>,
                        <ns0:FilterCriterion field = "NIVEL2"
                                             operation = "EQ"
                                             value = "{ data($req/NIVELCLA2) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC2"
                                             operation = "EQ"
                                             value = "{ data($req/CLIENSEC2) }"/>,
                        <ns0:FilterCriterion field = "NIVEL3"
                                             operation = "EQ"
                                             value = "{ data($req/NIVELCLA3) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC3"
                                             operation = "EQ"
                                             value = "{ data($req/CLIENSEC3) }"/>
                    )
                }
                <ns0:FilterCriterion field = "FECDES"
                                     operation = "EQ"
                                     value = "{ fn-bea:date-from-string-with-format('dd/MM/yyyy', data($req/FECHADESDE)) }"/>
                <ns0:FilterCriterion field = "FECHAS"
                                     operation = "EQ"
                                     value = "{ fn-bea:date-from-string-with-format('dd/MM/yyyy', data($req/FECHAHASTA)) }"/>
            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $req as element(Request) external;

xf:OV_1427_To_PaginatedQueryRq($req)
