xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1114.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1114_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1114_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <CAMPOS>
                        <IDBPM>{ data($paginatedQueryRs1/ns0:RowSet/ns0:Row[1]/ns0:Column[@name = 'IDBPM']) }</IDBPM>
                        <CANT>{ count($paginatedQueryRs1/ns0:RowSet/ns0:Row) }</CANT>
                    	{
                    		for $row in $paginatedQueryRs1/ns0:RowSet/ns0:Row
                    		return
                    		(
                    			<T-RET>
                    				<RETENCION>
                    					<DESC>{ data($row/ns0:Column[@name='DESCRIPTION']) }</DESC>
                    				</RETENCION>
                    			</T-RET>
                    		)
                    	}
                    </CAMPOS>
                )
            }
        </Response>
};

declare variable $paginatedQueryRs1 as element() external;

xf:OV_1114_From_PaginatedQueryRs($paginatedQueryRs1)
