xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/OV_1202.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QbeFindPolicyInsuredsRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1202_To_FndPlcyByNumRq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1202_To_FndPlcyByNumRq($request as element(Request))
    as element() {
        <ns0:QbeFindPolicyInsuredsRq>
        	<ns0:PolicyNo>
        		{
        			concat('0001' ,
        					fn-bea:pad-right($request/RAMOPCOD, 4) ,
							fn-bea:format-number(xs:double($request/POLIZANN), '00') ,
							fn-bea:format-number(xs:double($request/POLIZSEC), '000000') ,
							fn-bea:format-number(xs:double($request/CERTIPOL), '0000') ,
							fn-bea:format-number(xs:double($request/CERTIANN), '0000') ,
							fn-bea:format-number(xs:double($request/CERTISEC), '000000'))
				}
			</ns0:PolicyNo>
        </ns0:QbeFindPolicyInsuredsRq>
};

declare variable $request as element(Request) external;

xf:OV_1202_To_FndPlcyByNumRq($request)
