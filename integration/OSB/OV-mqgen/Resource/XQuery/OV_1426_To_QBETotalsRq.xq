(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_1426_GetDeudaCobradaTotales.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QBETotalsRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1426_To_QBETotalsRq/";

declare function xf:OV_1426_To_QBETotalsRq($req as element(Request))
    as element() {
        <ns0:QBETotalsRq>
            <ns0:QueryID>1426</ns0:QueryID>
            <ns0:Cliensec>{ data($req/CLIENSECAS) }</ns0:Cliensec>
            <ns0:Nivel>{ data($req/NIVELCLAS) }</ns0:Nivel>
            <ns0:FilterCriteria>
                <ns0:FilterCriterion field = "FECDES"
                                     operation = "EQ"
                                     value = "{ fn-bea:date-from-string-with-format('dd/MM/yyyy', data($req/FECHADESDE)) }"/>
                <ns0:FilterCriterion field = "FECHAS"
                                     operation = "EQ"
                                     value = "{ fn-bea:date-from-string-with-format('dd/MM/yyyy', data($req/FECHAHASTA)) }"/>
            </ns0:FilterCriteria>
        </ns0:QBETotalsRq>
};

declare variable $req as element(Request) external;

xf:OV_1426_To_QBETotalsRq($req)
