xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1427_GetDeudaCobradaCanales.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1427_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1427_From_PaginatedQueryRs($paginatedQueryRs as element())
    as element(Response) {
               <Response>
       	{
           if ( number(data($paginatedQueryRs/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
		            <CAMPOS>
		            	{
			            	for $row in $paginatedQueryRs/ns0:RowSet/ns0:Row
				            return
								(
									<CANAL>
										<COBROCOD>{ data($row/ns0:Column[@name = 'COBROCOD'])}</COBROCOD>
						                <COBRODES>{ data($row/ns0:Column[@name = 'COBRODES'])}</COBRODES>
						                <IMPORTEP>{ data($row/ns0:Column[@name = 'IMPORTEP'])}</IMPORTEP>
						                <IMPORTED>{ data($row/ns0:Column[@name = 'IMPORTED'])}</IMPORTED>
									</CANAL>
								)
							}
		            </CAMPOS>
		        )
		       }
        </Response>
};

declare variable $paginatedQueryRs as element() external;

xf:OV_1427_From_PaginatedQueryRs($paginatedQueryRs)
