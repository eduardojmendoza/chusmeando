xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1428_GetDeudaCobradaCanalesDetalle.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/Paginated_1404_From_PaginatedQueryRs/";

declare function xf:Paginated_1428_From_PaginatedQueryRs($resp as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>,
                    <MSGEST>ER</MSGEST>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <MSGEST>TR</MSGEST>,
                    <ResultSetID>{data($resp/ns0:ResultSetID)}</ResultSetID>,
                    <CAMPOS>
                        {
                            for $item in $resp/ns0:RowSet/ns0:Row
                            return
                            (
                                <CANAL>		                  			
		                  			<RAMOPCOD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)) }</RAMOPCOD>
		                  			<POLIZANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,2)) }</POLIZANN>
		                  			<POLIZSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],11,6)) }</POLIZSEC>
									<CERTIPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERTIPOL>
							  		<CERTIANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERTIANN>
							  		<CERTISEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERTISEC>
							  		<SUPLENUM>{ data($item/ns0:Column[@name = 'SUPLENUM']) }</SUPLENUM>
							  		<CLIENSEC>{ data($item/ns0:Column[@name = 'CLIENSEC']) }</CLIENSEC>
							  		<CLIENDES>{ data($item/ns0:Column[@name = 'CLIENDES']) }</CLIENDES>
							  		<SITUCPOL>{ data($item/ns0:Column[@name = 'SITUCPOL']) }</SITUCPOL>
							  		<RAMO>{ data($item/ns0:Column[@name = 'RAMO']) }</RAMO>
                                    <SINIESTRO>{ 
									let $siniestro := data($item/ns0:Column[@name = 'SINIESTRO']) 
									return
										if ($siniestro = 'S') then ('1')
										else ('0')
									}</SINIESTRO>
                                    <EXIGIBLE>{ 
									let $exigible := data($item/ns0:Column[@name='EXIGIBLE']) 
									return
										if ($exigible = 'S') then ('1')
										else ('0')
									}</EXIGIBLE>
                                    <RECIBANN>{ substring(data($item/ns0:Column[@name = 'RECIBO']),1,2) }</RECIBANN>
									<RECIBTIP>{ substring(data($item/ns0:Column[@name = 'RECIBO']),3,1) }</RECIBTIP>
									<RECIBSEC>{ substring(data($item/ns0:Column[@name = 'RECIBO']),4,9) }</RECIBSEC>
									<SECUENUM>{ data($item/ns0:Column[@name = 'SECUENUM']) }</SECUENUM>
									<REMESANN>{ substring(data($item/ns0:Column[@name = 'REMITTANCE']),1,4) }</REMESANN>
									<REMESSEC>{ 
									substring-after(data($item/ns0:Column[@name = 'REMITTANCE']),substring(data($item/ns0:Column[@name = 'REMITTANCE']),1,4)) 
									}
									</REMESSEC>
									<FECSITUE>{ fn-bea:date-to-string-with-format("dd/MM/yyyy",data($item/ns0:Column[@name = 'FECTISUE'])) }</FECSITUE>
									<COBROTIPC>{ data($item/ns0:Column[@name = 'COBROTIPC']) }</COBROTIPC>
									<COBRODESC>{ data($item/ns0:Column[@name = 'COBRODESC']) }</COBRODESC>
									<COBROCODC>{ data($item/ns0:Column[@name = 'COBROCODC']) }</COBROCODC>
									<COBRODABC>{ data($item/ns0:Column[@name = 'COBRODABC']) }</COBRODABC>
									<MONENCOD>{ data($item/ns0:Column[@name = 'MONENCOD']) }</MONENCOD>
									<MONENDES>{ data($item/ns0:Column[@name = 'MONENDES']) }</MONENDES>
									<IMPORTE>{ data($item/ns0:Column[@name = 'IMPORTE']) }</IMPORTE>
									<ORDENJUDICIAL>{ data($item/ns0:Column[@name = 'ORDENJUDICIAL']) }</ORDENJUDICIAL>
                                </CANAL>
                            )
                        }
                    </CAMPOS>
                )
            }
        </Response>
};

declare variable $resp as element() external;

xf:Paginated_1428_From_PaginatedQueryRs($resp)