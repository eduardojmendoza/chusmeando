xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/OV_1120.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1120_To_PaginatedQueryRq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:OV_1120_To_PaginatedQueryRq($request as element(Request), $LOVs as element()?)
    as element() {
        <ns0:PaginatedQueryRq>
            <ns0:QueryID>1120</ns0:QueryID>
            <ns0:FilterCriteria>
                {
				if (data($request/DOCUMTIP) != '') then
            		(
            		                <ns0:FilterCriterion field = "DOCUMTIP"
                                     operation = "EQ"
                                     value = "{
                                		let $docType := data($LOVs/lov:LOV[@id = "DocTypeNameById" and @ref = xs:int($request/DOCUMTIP) ][1]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID)
                            			return ($docType)
								 	}" />
					) else ()
				}

                 <ns0:FilterCriterion field = "DOCUMDAT"
                                     operation = "EQ"
                                     value = "{ data($request/DOCUMDAT) }"/>

                <ns0:FilterCriterion field = "APELLIDO"
                                     operation = "EQ"
                                     value = "{ data($request/APELLIDO) }"/>

                <ns0:FilterCriterion field = "POLICY_NO"
                                     operation = "EQ"
                                     value = "{ 
                                          let $ramo := data($request/RAMO)
                                          let $poliza := data($request/POLIZA)
                                          let $certif := data($request/CERTIF)
                                          return
                                              if ($ramo != '' and $poliza != '') then (
                                                   xqubh:buildPolicyNo('0001', data($ramo), data($poliza), concat('1',data($certif))) 
                                             ) else ()
                                     
                                     }" />

                <ns0:FilterCriterion field = "PATENTE"
                                     operation = "EQ"
                                     value = "{ data($request/PATENTE) }"/>

                <ns0:FilterCriterion field = "SWBUSCA"
                                     operation = "EQ"
                                     value = "{ data($request/SWBUSCA) }"/>

                 <ns0:FilterCriterion field = "COLUMNA"
                                     operation = "EQ"
                                     value = "{ data($request/COLUMNA) }"/>

                 <ns0:FilterCriterion field = "ORDEN"
                                     operation = "EQ"
                                     value = "{ data($request/ORDEN) }"/>

             </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $request as element(Request) external;
declare variable $LOVs as element(lov:LOVs)? external;

xf:OV_1120_To_PaginatedQueryRq($request, $LOVs)
