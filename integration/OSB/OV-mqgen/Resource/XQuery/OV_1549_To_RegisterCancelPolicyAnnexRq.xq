(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_1549.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:RegisterCancelPolicyAnnexRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";
declare namespace xf = "http://tempuri.org/Transacciones/Resource/XQuery/OV_1549_To_RegisterCancelPolicyAnnexRq/";

declare function xf:OV_1549_To_RegisterCancelPolicyAnnexRq($req as element(Request),
    $LOVs as element(lov:LOVs))
    as element() {
        <ns0:RegisterCancelPolicyAnnexRq>
            <ns0:PolicyNo>
                {
                    concat( '0001',
                    fn-bea:pad-right($req/RAMOPCOD,4),
                    fn-bea:format-number(xs:double($req/POLIZANN), '00'),
                    fn-bea:format-number(xs:double($req/POLIZSEC), '000000'),
                    fn-bea:format-number(xs:double($req/CERTIPOL), '0000'),
                    fn-bea:format-number(xs:double($req/CERTIANN), '0000'),
                    fn-bea:format-number(xs:double($req/CERTISEC), '000000'))
                }
			</ns0:PolicyNo>
            <ns0:Annex>
                <ns0:BeginDate>{ concat( data($req/FECVIGENANN), '-', data($req/FECVIGENMES), '-', data($req/FECVIGENDIA)) }</ns0:BeginDate>
                <ns0:DateGiven>{ concat( data($req/FESOLANN), '-', data($req/FESOLMES), '-', data($req/FESOLDIA)) }</ns0:DateGiven>
	            <ns0:ReasonID>{ data($LOVs/lov:LOV[@id = "AnnexReason"]/ns0:ListItems/ns0:ListItem/ns0:ItemID) }</ns0:ReasonID>
                <ns0:Username>{ data($req/USUARCOD) }</ns0:Username>
            </ns0:Annex>
        </ns0:RegisterCancelPolicyAnnexRq>
};

declare variable $req as element(Request) external;
declare variable $LOVs as element(lov:LOVs) external;

xf:OV_1549_To_RegisterCancelPolicyAnnexRq($req,
    $LOVs)
