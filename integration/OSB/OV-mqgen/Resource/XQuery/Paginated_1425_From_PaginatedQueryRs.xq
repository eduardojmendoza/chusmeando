xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1425.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1425_From_PaginatedQueryRs/";

declare function xf:OV_1425_From_PaginatedQueryRs($resp as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>,
                    <MSGEST>ER</MSGEST>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <MSGEST>OK</MSGEST>,
                    <CAMPOS>
	                    <COTIDOLAR>{ data($resp/ns0:RowSet/ns0:Row[1]/ns0:Column[@name = 'COTIDOLAR']) }</COTIDOLAR>
	                    <CANTLIN>{ count($resp/ns0:RowSet/ns0:Row) }</CANTLIN>
	                    <REGS>
	                        {
	                            for $item in $resp/ns0:RowSet/ns0:Row
	                            return
	                            (
	                                <REG>   
	                                    <RAMO>{ data($item/ns0:Column[@name = 'RAMO']) }</RAMO>
	                                    <CLIDES>{ data($item/ns0:Column[@name = 'CLIDES']) }</CLIDES>
	                                    <CLIENSEC>{ data($item/ns0:Column[@name = 'CLIENSEC']) }</CLIENSEC>
	                                    <PROD>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),5,4) }</PROD>
	                                    <POL>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),9,8) }</POL>
	                                    <CERPOL>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),17,4) }</CERPOL>
	                                    <CERANN>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),21,4) }</CERANN>
	                                    <CERSEC>{ substring(data($item/ns0:Column[@name = 'POLICY_NO']),25,6) }</CERSEC>
	                                    <OPERAPOL>{ data($item/ns0:Column[@name = 'OPERAPOL']) }</OPERAPOL>
	                                    <RECNUM>{ data($item/ns0:Column[@name = 'RECNUM']) }</RECNUM>
	                                    <ESTADO>{ data($item/ns0:Column[@name = 'ESTADO']) }</ESTADO>
	                                    <MON>{ data($item/ns0:Column[@name = 'MON']) }</MON>
	                                    <SIG>{ data($item/ns0:Column[@name = 'SIG']) }</SIG>
	                                    <IMP>{ data($item/ns0:Column[@name = 'IMP']) }</IMP>
	                                    <IMPCALC>{ data($item/ns0:Column[@name = 'IMPCALC']) }</IMPCALC>                                    
	                                    <RENDIDO>{ data($item/ns0:Column[@name = 'RENDIDO']) }</RENDIDO>
	                                    <FECVTO>{
										if (exists($item/ns0:Column[@name = 'FECVTO']) and(data($item/ns0:Column[@name = 'FECVTO']) != ''))
										then(fn-bea:date-to-string-with-format("dd/MM/yyyy",data($item/ns0:Column[@name = 'FECVTO'])))
										else(data($item/ns0:Column[@name = 'FECVTO']))
	                                    }</FECVTO>
	                                    <AGE>{ 
	                                    let $age := data($item/ns0:Column[@name = 'AGE']) 
	                                    return
	                                    concat(substring($age,1,2),'-',substring($age,3,6))
	                                    }
	                                    </AGE>
	                                </REG>
	                            )
	                        }
	                    </REGS>
                    </CAMPOS>
                )
            }
        </Response>
};

declare variable $resp as element() external;

xf:OV_1425_From_PaginatedQueryRs($resp)