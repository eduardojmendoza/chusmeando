xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1549.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQ/Resource/XQuery/OV_1549_From_RegisterCancelPolicyAnnexRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1549_From_RegisterCancelPolicyAnnexRs($resp as element())
    as element(Response) {
        <Response>
            <Estado resultado = "true" mensaje = ""/>
			<CAMPOS>
				<MENSAJEO>{ data($resp/MENSAJEO) }</MENSAJEO>
				<RAMOPCODO>{ data($resp/RAMOPCODO) }</RAMOPCODO>
				<POLIZANNO>{ data($resp/POLIZANNO) }</POLIZANNO>
				<POLIZSECO>{ data($resp/POLIZSECO) }</POLIZSECO>
				<CERTIPOLO>{ data($resp/CERTIPOLO) }</CERTIPOLO>
				<CERTIANNO>{ data($resp/CERTIANNO) }</CERTIANNO>
				<CERTISECO>{ data($resp/CERTISECO) }</CERTISECO>
				<SUPLENUMO>{ data($resp/SUPLENUMO) }</SUPLENUMO>
				<CANTTEXTO>{ data($resp/CANTTEXTO) }</CANTTEXTO>
				<REGS/>
			</CAMPOS>
        </Response>
};

declare variable $resp as element() external;

xf:OV_1549_From_RegisterCancelPolicyAnnexRs($resp)
