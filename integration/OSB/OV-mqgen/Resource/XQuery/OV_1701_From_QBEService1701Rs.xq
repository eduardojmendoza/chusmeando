(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QBEService1701Rs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1701.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/Transacciones/Resource/XQuery/OV_1701_From_QBEService1701Rs/";

declare function xf:OV_1701_From_QBEService1701Rs($resp as element())
    as element(Response) {
        <Response>
        	{
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <MSGEST>OK</MSGEST>,
            		<CAMPOS>
		                <CANDEVUE>{ count($resp/ns0:GeneratedPrintouts/ns0:GeneratedPrintout) }</CANDEVUE>
		                <RESULTADOS>
		                    {
		                    	for $item in $resp/ns0:GeneratedPrintouts/ns0:GeneratedPrintout
		                    	return
		                    	(
				                    <RESULTADO>
				                        <FORMUDES>{ data($item/ns0:PageFormat) }</FORMUDES>
				                        <NOMARCH>{ concat('insis://', data($item/ns0:FilePath)) }</NOMARCH>
				                    </RESULTADO>
						)
					}
		                </RESULTADOS>
		            </CAMPOS>
            	)
			}
        </Response>
};

declare variable $resp as element() external;

xf:OV_1701_From_QBEService1701Rs($resp)
