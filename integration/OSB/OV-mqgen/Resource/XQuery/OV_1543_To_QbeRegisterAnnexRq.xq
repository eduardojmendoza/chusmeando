(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/OV_1543.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QbeRegisterAnnexRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1543_To_QbeRegisterAnnexRq/";

declare function xf:getInsisAddressType($OVAddresType as xs:string? ) as xs:string {
    if (exists($OVAddresType) and $OVAddresType != '') then
    (
		let $at := upper-case($OVAddresType)
                		return
                			if (starts-with($at,'PAR')) then ( 'H' )
                			else if (starts-with($at,'LAB')) then ( 'W' )
                			else if (starts-with($at,'COR')) then ( 'C' )
                			else if (starts-with($at,'DOM')) then ( 'DOMAGENT' )
                			else 'NO-VALID-TYPE'
	)
    else '' 
};

declare function xf:OV_1543_To_QbeRegisterAnnexRq($request as element(Request), $LOVs as element(lov:LOVs)?)
    as element() {
        <ns0:QbeRegisterAnnexRq>
            <ns0:PolicyNo>
                {
                    concat('0001',
                    fn-bea:pad-right(xs:string(normalize-space($request/RAMOPCOD)), 4) ,
                    fn-bea:format-number(xs:double($request/POLIZANN), '00') ,
                    fn-bea:format-number(xs:double($request/POLIZSEC), '000000') ,
                    fn-bea:format-number(xs:double($request/CERTIPOL), '0000') ,
                    fn-bea:format-number(xs:double($request/CERTIANN), '0000') ,
                    fn-bea:format-number(xs:double($request/CERTISEC), '000000')
                    )
                }
</ns0:PolicyNo>
            <ns0:Annex>
                <ns0:BeginDate>{ local:getDate( concat(fn-bea:pad-left(data($request/FESOLANN),4,'0'),
                	fn-bea:pad-left(data($request/FESOLMES),2,'0'),fn-bea:pad-left(data($request/FESOLDIA),2,'0')))
                }</ns0:BeginDate>
                <ns0:AnnexReason>{ data($request/SUPLENUM) }</ns0:AnnexReason>
            </ns0:Annex>
            <ns0:Client>
                <ns0:Entity>
                    <ns0:PersonalData>
                        <ns0:Name>
                            <ns0:Given>{ data($request/CLIENNOM) }</ns0:Given>
                            <ns0:Family>{ data($request/CLIENAP1) }</ns0:Family>
                        </ns0:Name>
                        <ns0:Gender>{ 
	                		let $sexo := normalize-space(upper-case(data($request/CLIENSEX)))
	                		return
	                			if ($sexo = 'M') then ( 1 )
	                			else if ($sexo = 'F') then ( 2 )
	                			else ( 0 )
	                    }</ns0:Gender>
                        <ns0:BirthDate>{ local:getDate( concat(fn-bea:pad-left(data($request/NACIMANN),4,'0'),
                		fn-bea:pad-left(data($request/NACIMMES),2,'0'),fn-bea:pad-left(data($request/NACIMDIA),2,'0'))) }</ns0:BirthDate>
                    </ns0:PersonalData>
                    <ns0:DataSource>{ data($request/ESTCIV) }</ns0:DataSource>
                </ns0:Entity>
                <ns0:Addresses>
                    <ns0:Address>
                        <ns0:Country>{ data( $LOVs/lov:LOV[@id = "LkpCountry" and @ref = data($request/DOMIPAISSCOD)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemID ) }</ns0:Country>
                        <ns0:State>{ data($request/PROVICOD) }</ns0:State>
                        <ns0:City>{ data($request/DOMICPOB) }</ns0:City>
                        <ns0:ResidentialAddress>
                            <ns0:StreetName>{ data($request/DOMICDOM) }</ns0:StreetName>
                            <ns0:StreetNo>{ data($request/DOMICDNU) }</ns0:StreetNo>
                            <ns0:Floor>{ data($request/DOMICPIS) }</ns0:Floor>
                            <ns0:Apartment>{ data($request/DOMICPTA) }</ns0:Apartment>
                        </ns0:ResidentialAddress>
                        <ns0:ZipCode>{ data($request/DOMICCPO) }</ns0:ZipCode>
                        <ns0:AddressType>{ xf:getInsisAddressType(data($request/DOMICCAL)) }</ns0:AddressType>
                        <ns0:Address></ns0:Address>
                    </ns0:Address>
                </ns0:Addresses>
                
                {if (exists($request/REG1S/REG1/MEDCODAT) and (data($request/REG1S/REG1/MEDCODAT) != ""))
            	then(
                <ns0:Contacts>
                		<ns0:Contact>
		                    <ns0:Type>{ data($request/REG1S/REG1/MEDCOCOD) }</ns0:Type>
							<ns0:Details>{ data($request/REG1S/REG1/MEDCODAT) }</ns0:Details>
						</ns0:Contact>
					
                </ns0:Contacts>
                )
                else ()
                }
                <ns0:PaymentInstruments>
                    <ns0:PaymentInstrument>
                        <ns0:AccountType>{ data($request/COBROTIP) }</ns0:AccountType>
                        (: <ns0:AccountNo>{ data($request/CUENNUME),data($request/VENCIMES),data($request/VENCIANN) }</ns0:AccountNo> :)
                        <ns0:AccountNo>{ concat(replace(data($request/CUENNUME),' ','') , replace(data($request/VENCIMES),' ','') , replace(data($request/VENCIANN),' ','')) }</ns0:AccountNo>
                        <ns0:BankCode>{ data($request/BANCOCOD) }</ns0:BankCode>
                        <ns0:BranchCode>{ data($request/SUCURCOD) }</ns0:BranchCode>
                    </ns0:PaymentInstrument>
                </ns0:PaymentInstruments>
            </ns0:Client>
            <ns0:Vehicle>
                <ns0:ProdYear>{ data($request/FABRIFEC) }</ns0:ProdYear>
                <ns0:Engine>{ data($request/MOTORNUM) }</ns0:Engine>
                <ns0:Chassis>{ data($request/CHASINUM) }</ns0:Chassis>
                <ns0:RegNo>{ data($request/PATENNUM) }</ns0:RegNo>
                <ns0:KmPerYear>{ data($request/AUKLMNUM) }</ns0:KmPerYear>
                <ns0:Accessories?>
                	{
                    if (data($request/AUUSOGNC) = 'S') then (
                            <ns0:Accessory>
                                <ns0:Code>6</ns0:Code>
                                <ns0:SubCode>1</ns0:SubCode>
                                <ns0:Description>Equipo de GNC</ns0:Description>
                                <ns0:InsuredValue>{data($request/ACCESORIOS/ACCESORIO[CODIGOACC = 5]/PRECIOACC)}</ns0:InsuredValue>
                           </ns0:Accessory>
                             	) else ()
                   }
                    {
                        for $ACCESORIOS in $request/ACCESORIOSS/ACCESORIOSS/ACCESORIOS[AUVEASUM != '' and CODIGOACC != 5]
                        return
                            <ns0:Accessory>
                                <ns0:Code>5</ns0:Code>
                                <ns0:SubCode>{ data($ACCESORIOS/AUACCCOD) }</ns0:SubCode>
                                <ns0:Description>{ data($ACCESORIOS/AUACCDES) }</ns0:Description>
                                <ns0:InsuredValue>{ data($ACCESORIOS/AUVEASUM) }</ns0:InsuredValue>
                            </ns0:Accessory>
                    }
                </ns0:Accessories>
            </ns0:Vehicle>
        </ns0:QbeRegisterAnnexRq>
};

declare variable $request as element(Request) external;
declare variable $LOVs as element(lov:LOVs) external;

declare function local:getDate($date as xs:string?) as xs:date?
{
	if ( $date != '00000000' and matches($date, '\d{8}') and substring($date, 1, 4) != '0000' and substring($date, 5, 2) != '00' and substring($date, 7, 2) != '00' )
	then ( fn-bea:date-from-string-with-format("yyyyMMdd", $date) )
	else ( )
};

xf:OV_1543_To_QbeRegisterAnnexRq($request, $LOVs)