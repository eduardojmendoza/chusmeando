xquery version "1.0" encoding "UTF-8";
(:: pragma  parameter="$recibo" type="xs:anyType" ::)
(:: pragma  type="xs:anyType" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1425_RecNoEncontrados/";

declare function xf:OV_1425_RecNoEncontrados($recibo as element(*) *)
    as element(*) {
       <REGS> {
       for $reg in $recibo
       return 
       <REG>
        <RAMO></RAMO>
        <CLIDES></CLIDES>
      <CLIENSEC></CLIENSEC>
      <PROD></PROD>
      <POL></POL>
      <CERPOL></CERPOL>
      <CERANN></CERANN>
      <CERSEC></CERSEC>
      <OPERAPOL></OPERAPOL>
      <RECNUM>{ data($reg/RECNUM) }</RECNUM>
      <ESTADO>N</ESTADO>
      <MON></MON>
	  <SIG></SIG>
	  <IMP></IMP>
	  <IMPCALC></IMPCALC>
	  <RENDIDO></RENDIDO>
	  <FECVTO></FECVTO>
	  <AGE></AGE>
     </REG>
     }
</REGS>
};

declare variable $recibo as element(*) * external;

xf:OV_1425_RecNoEncontrados($recibo)
