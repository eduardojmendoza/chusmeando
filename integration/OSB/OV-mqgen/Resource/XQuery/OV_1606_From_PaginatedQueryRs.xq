xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1606_GetCuponesPendientesDetalle.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1606_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1606_From_PaginatedQueryRs($resp as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <RESPUESTA>

                    	<CANT-CUP>{ count($resp/ns0:RowSet/ns0:Row) }</CANT-CUP>
                    	<RECIBOS>
	                    {
	                        for $item in $resp/ns0:RowSet/ns0:Row
	                        return
                            (
	                            <RECIBO>
	                            	    <ENDOSO>{ data($item/ns0:Column[@name = 'ENDOSO']) }</ENDOSO>
		                            	<NRO-CUPON>{ data($item/ns0:Column[@name = 'NRO_CUPON']) }</NRO-CUPON>
										<FECHAVTO>{ data($item/ns0:Column[@name = 'FECHAVTO']) }</FECHAVTO>
										<SIGNO>{ data($item/ns0:Column[@name = 'SIGNO']) }</SIGNO>
										<PRIMNETA>{ data($item/ns0:Column[@name = 'PRIMETA']) }</PRIMNETA>
										<TOTRECIB>{ data($item/ns0:Column[@name = 'TOTRECIB']) }</TOTRECIB>
										<ESTADOPOL>{ data($item/ns0:Column[@name = 'ESTADOPOL']) }</ESTADOPOL>
										<IMPRIMIR>{ data($item/ns0:Column[@name = 'IMPRIMIR']) }</IMPRIMIR>
								</RECIBO>
	                         )
	                    }
	                    </RECIBOS>

                    </RESPUESTA>
                )
            }
        </Response>
};

declare variable $resp as element() external;

xf:OV_1606_From_PaginatedQueryRs($resp)
