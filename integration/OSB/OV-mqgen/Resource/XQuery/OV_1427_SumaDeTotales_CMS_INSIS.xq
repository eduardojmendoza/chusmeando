(:: pragma bea:global-element-parameter parameter="$returnCMS" element="return" location="../XSD/OV_1427_SumaTotales.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$returnINSIS" element="return" location="../XSD/OV_1427_SumaTotales.xsd" ::)
(:: pragma bea:global-element-return element="return" location="../XSD/OV_1427_SumaTotales.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1427_SumaDeTotales_CMS_INSIS/";


declare function xf:OV_1427_SumaDeTotales_CMS_INSIS($returnCMS as element(),
    $returnINSIS as element())
    as element() {
        <return>
            <Response>
                <Estado mensaje = "{ data($returnCMS/Response/Estado/@mensaje) }"
                        resultado = "{ data($returnCMS/Response/Estado/@resultado) }"/>
                
          
               {
               	for $canal in $returnCMS/Response/CAMPOS/CANAL
                return 
                
                 <CANAL>{ 
                 $canal[COBROCOD=($returnINSIS/Response/CAMPOS/CANAL/COBROCOD)]/COBROCOD,
	             if (data($canal/COBRODES) = "EFECTIVO" ) then ($canal/COBRODES) else ($canal[COBROCOD=($returnINSIS/Response/CAMPOS/CANAL/COBROCOD)]/COBRODES),
                 <IMPORTEP>{ number($canal[COBROCOD=($returnINSIS/Response/CAMPOS/CANAL/COBROCOD)]/IMPORTEP) + number($returnINSIS/Response/CAMPOS/CANAL[COBROCOD=($canal/COBROCOD)]/IMPORTEP) }</IMPORTEP>,
                 <IMPORTED>{ number($canal[COBROCOD=($returnINSIS/Response/CAMPOS/CANAL/COBROCOD)]/IMPORTED) + number($returnINSIS/Response/CAMPOS/CANAL[COBROCOD=($canal/COBROCOD)]/IMPORTED) }</IMPORTED>
                 }
                </CANAL>
                                
                }                 
            
            </Response>
                      
                    
        </return>
};

declare variable $returnCMS as element() external;
declare variable $returnINSIS as element() external;

xf:OV_1427_SumaDeTotales_CMS_INSIS($returnCMS,
    $returnINSIS)
