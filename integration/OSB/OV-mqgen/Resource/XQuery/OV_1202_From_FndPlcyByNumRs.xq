(:: pragma bea:global-element-parameter parameter="$service1232" element="Response" location="../XSD/Service1232.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$qbeFindPolicyInsuredsRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeFindPolicyInsuredsRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1202.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1202_From_FndPlcyByNumRs/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:OV_1202_From_FndPlcyByNumRs($LOVs as element()?
												, $service1232 as element()?
												, $driversTransRs as element()?
												, $qbeFindPolicyInsuredsRs as element())
    as element(Response) {
        <Response>
        {
        	if ( number(data($qbeFindPolicyInsuredsRs/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
		            <CAMPOS>
		                <MOTORNUM>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:Engine) }</MOTORNUM>
		                <PATENNUM>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:RegNo) }</PATENNUM>
		                <CHASINUM>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:Chassis) }</CHASINUM>
		                <VEHICSEC>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:ObjectID) }</VEHICSEC>
		                <AUMARCOD>{ data($service1232/CAMPOS/AUMARCOD) }</AUMARCOD>
		                (:<AUMARDES>{ data($LOVs/lov:LOV[@id = "Make"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($service1232/CAMPOS/AUMARCOD)]/ns0:ItemDescription) }</AUMARDES>:)
		                <AUMARDES>{ data($service1232/CAMPOS/AUMARDES) }</AUMARDES>
		                <AUMODCOD>{ data($service1232/CAMPOS/AUMODCOD) }</AUMODCOD>
		                (:<AUMODDES>{ data($LOVs/lov:LOV[@id = "Model"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($service1232/CAMPOS/AUMODCOD)]/ns0:ItemDescription) }</AUMODDES>:)
		                <AUMODDES>{ data($service1232/CAMPOS/AUMODDES) }</AUMODDES>
		                <AUSUBCOD>{ data($service1232/CAMPOS/AUSUBCOD) }</AUSUBCOD>
		                <AUSMODES>{ data($service1232/CAMPOS/AUSMODES) }</AUSMODES>
		                <AUADICOD>{ data($service1232/CAMPOS/AUADICOD) }</AUADICOD>
		                <AUADIDES>{ data($service1232/CAMPOS/AUADIDES) }</AUADIDES>
		                <AUMODORI>{ data($service1232/CAMPOS/AUMODORI) }</AUMODORI>
		                (:<AUMODODE>{ data($LOVs/lov:LOV[@id = "ProdType"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($service1232/CAMPOS/AUMODORI)]/ns0:ItemDescription) }</AUMODODE>:)
		                <AUMODODE>{ data($service1232/CAMPOS/AUMODODE) }</AUMODODE>
		                <AUUSOCOD>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarUsage) }</AUUSOCOD>
		                <AUUSODES>{ data($LOVs/lov:LOV[@id = "CarUsage"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarUsage)]/ns0:ItemDescription) }</AUUSODES>
		                <AUVTVCOD></AUVTVCOD>
		                <AUVTVFEC></AUVTVFEC>
		                <AUCOLCOD>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarColor) }</AUCOLCOD>
		                <AUCOLDES>{ data($LOVs/lov:LOV[@id = "CarColor"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarColor)]/ns0:ItemDescription) }</AUCOLDES>
		                <AUKLMNUM>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP1']/ns0:Value) }</AUKLMNUM>
		                <AUCATCOD></AUCATCOD>
		                <AUCATDES></AUCATDES>
		                <AUTIPCOD>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarType) }</AUTIPCOD>
		                <AUTIPDES>
		                    {
	                    		let $tipo := data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarType)
	                    		let $cartype := data($LOVs/lov:LOV[@id = "CarType"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($tipo)]/ns0:ItemDescription)
	                    		return if ($cartype !='') then ($cartype) else ($tipo)
		                    }
						</AUTIPDES>
		                <FABRIFEC>{ concat('01/', data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:ProdYear)) }</FABRIFEC>
		                <GUGARAGE>
		                	{
		                		let $valOcp2 := data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP2']/ns0:Value)
		                		return if ($valOcp2 = '1') then ('S') else ('N')
		                	}
		                </GUGARAGE>
		                <GUDOMICI/>
	                	<PAISSCOD>{ '00' }</PAISSCOD>
		                <PAISSDES>{ 'ARGENTINA' }</PAISSDES>
		                <PROVICOD>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP8']/ns0:Value) }</PROVICOD>
		                <PROVIDES>{ data($LOVs/lov:LOV[@id = "Province"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP8']/ns0:Value)]/ns0:ItemDescription) }</PROVIDES>
		                <CODIZONA>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP4']/ns0:Value) }</CODIZONA>
		                <ZONASDES>{ data($LOVs/lov:LOV[@id = "Zone"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP4']/ns0:Value)]/ns0:ItemDescription) }</ZONASDES>
		                <CPACODPO>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP5']/ns0:Value) }</CPACODPO>
		                <AUNUMSIN>{ data($qbeFindPolicyInsuredsRs/ns0:Policy/ns0:InsuranceConditions/ns0:Questionary/ns0:Question[ns0:ID='3005.31']/ns0:Answer) }</AUNUMSIN>
		                <AUNUMKMT>{ data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP1']/ns0:Value) }</AUNUMKMT>
		                <AUUSOGNC>
		                	{
		                		let $valOcp13 := data($qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP13']/ns0:Value)
		                		return if ($valOcp13 = '1') then ('S') else ('N')
		                	}
		                </AUUSOGNC>

		                <CONDUCTORES>
		                {
		                	for $driver in $qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:Drivers/ns0:Driver
		                    return
		                    (
		                        <CONDUCTOR>
			                        <CLIENAP1>{ data($driver/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Family) }</CLIENAP1>
			                        <CLIENNOM>{
			                        		let $nombre1 := data($driver/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Given)
			                        		let $nombre2 := data($driver/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Surname)
			                        		return if ($nombre2 != '') then (concat($nombre1, ' ', $nombre2)) else ($nombre1)
			                        		}
									</CLIENNOM>
			                        {
				                        if (fn:exists($driver/ns0:Entity/ns0:PersonalData/ns0:PID)) then
				                        (
											<DOCUMTIP>{ data($LOVs/lov:LOV[@id = "DocTypeByName" and @ref = data($driver/ns0:Entity/ns0:PersonalData/ns0:PID) ]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID) }</DOCUMTIP>,
											<DOCUMDAB>{ data(substring-before($driver/ns0:Entity/ns0:PersonalData/ns0:PID,'-')) }</DOCUMDAB>,
					        				<DOCUMDAT>{ if (data(substring-after($driver/ns0:Entity/ns0:PersonalData/ns0:PID,'-'))!="") then (
					        								data(substring-after($driver/ns0:Entity/ns0:PersonalData/ns0:PID,'-'))
					        							) else (
					        								data($driver/ns0:Entity/ns0:PersonalData/ns0:PID)
					        							)
					        								 }</DOCUMDAT>
										) else
										(
											<DOCUMTIP>{ data($LOVs/lov:LOV[@id = "DocTypeByName" and @ref = data($driver/ns0:Entity/ns0:CompanyData/ns0:CustomerID) ]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID) }</DOCUMTIP>,
											<DOCUMDAB>{ data(substring-before($driver/ns0:Entity/ns0:CompanyData/ns0:CustomerID,'-')) }</DOCUMDAB>,
				        					<DOCUMDAT>{ data(substring-after($driver/ns0:Entity/ns0:CompanyData/ns0:CustomerID,'-')) }</DOCUMDAT>
				        				)
			        				}
			                        <ESTCIV>
			                        	{
			                        		let $estCivil := data($driver/ns0:Entity/ns0:DataSource)
			                        		return
			                        			if ($estCivil = 'S') then ('SOLTERO')
			                        			else if ($estCivil = 'C') then ('CASADO')
			                        			else if ($estCivil = 'V') then ('VIUDO')
			                        			else if ($estCivil = 'D') then ('DIVORCIADO')
			                        			else if ($estCivil = 'E') then ('SEPARADO')
			                        			else if ($estCivil = 'N') then ('** NO INFORMADO **')
			                        			else ($estCivil)
			                        	}
			                        </ESTCIV>
			                        <SEXO>
			                        	{
			                        		let $sexo := data($driver/ns0:Entity/ns0:PersonalData/ns0:Gender)
			                        		return
			                        			if ($sexo = '1') then ('MASCULINO')
			                        			else if ($sexo = '2') then ('FEMENINO')
			                        			else ('** NO INFORMADO **')
			                        	}
			                        </SEXO>
									<NACIMFEC>
										{
                        					let $fecha := data($driver/ns0:Entity/ns0:PersonalData/ns0:BirthDate)
                        					return
                        						if ($fecha != '') then (fn-bea:date-to-string-with-format("dd/MM/yyyy", $fecha)) else ()
                        				}
									</NACIMFEC>
									<TIPOCOND>{ substring(data($driver/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='Iod1']/ns0:Value), 1, 1) }</TIPOCOND>
									<VINCCOND>{ data($driversTransRs/ns0:RowSet/ns0:Row/ns0:Column[@name = "DRIVER_ID" and text() = data($driver/ns0:DriverID)]/parent::node()/ns0:Column[@name = "IOD2_DESC"]) }</VINCCOND>
		        				</CONDUCTOR>
		    				)
		        		}
		                </CONDUCTORES>
		                <ACCESORIOS>
		                {
		                	for $accesorio in $qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:SupplementaryEquipment/ns0:OAdditionalEquipment
		                    return
		                    (
			                    <ACCESORIO>
			                        <AUACCCOD>{ data($accesorio/ns0:EquipmentType) }</AUACCCOD>
			                        <AUACCDES>{ data($LOVs/lov:LOV[@id = "EquipmentType"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($accesorio/ns0:EquipmentType)]/ns0:ItemDescription) }</AUACCDES>
			                        <AUVEASUM>{ data($accesorio/ns0:AvValue) }</AUVEASUM>
			                        <AUVEADES>{ data($accesorio/ns0:Model) }</AUVEADES>
			                    </ACCESORIO>
		                    )
		                }
		                </ACCESORIOS>
		                <ADICIONALES>
		                {
		                	for $participant in $qbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:Participants/ns0:Participant
		                    return
		                    (
			                    <ADIC>
			                    {
				                    if (fn:exists($participant/ns0:Entity/ns0:PersonalData/ns0:PID)) then
				                    (
											<AUVEASUM>{ data(substring-before($participant/ns0:Entity/ns0:PersonalData/ns0:PID,'-')) }</AUVEASUM>,
					        				<AUVEADES>{ data(substring-after($participant/ns0:Entity/ns0:PersonalData/ns0:PID,'-')) }</AUVEADES>
					        		) else if (fn:exists($participant/ns0:Entity/ns0:CompanyData/ns0:CustomerID)) then
					        			(
					        				<AUVEASUM>{ data(substring-before($participant/ns0:Entity/ns0:CompanyData/ns0:CustomerID,'-')) }</AUVEASUM>,
					        				<AUVEADES>{ data(substring-after($participant/ns0:Entity/ns0:CompanyData/ns0:CustomerID,'-')) }</AUVEADES>
					        			)
					        		else ()
			                    }
			                    </ADIC>
		                    )
		                }
		                </ADICIONALES>
		            </CAMPOS>
				)
		}
        </Response>
};

declare variable $LOVs as element(lov:LOVs)? external;
declare variable $service1232 as element()? external;
declare variable $driversTransRs as element()? external;
declare variable $qbeFindPolicyInsuredsRs as element() external;

xf:OV_1202_From_FndPlcyByNumRs($LOVs, $service1232, $driversTransRs, $qbeFindPolicyInsuredsRs)
