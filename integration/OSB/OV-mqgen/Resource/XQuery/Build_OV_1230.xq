(:: pragma bea:global-element-return element="ns0:executeRequest" location="../WSDL/OV_1230_Conversion_LBA_INFOAUTO.wsdl" ::)

declare namespace ns0 = "http://cms.services.qbe.com/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/Build_OV_1230/";

declare function xf:Build_OV_1230($AUMARCOD as xs:int ?,
    $AUMODCOD as xs:int ?,
    $AUSUBCOD as xs:int ?,
    $AUADICOD as xs:int ?,
    $AUMODORI as xs:string ?)
    as element(ns0:executeRequest) {
        <ns0:executeRequest>
            <actionCode>lbaw_GetConsultaMQ</actionCode>
    		<Request>
        		<DEFINICION>1230_Conversion_LBA_INFOAUTO.xml</DEFINICION>
                <AUMARCOD>{ $AUMARCOD }</AUMARCOD>
                <AUMODCOD>{ $AUMODCOD }</AUMODCOD>
                <AUSUBCOD>{ $AUSUBCOD }</AUSUBCOD>
                <AUADICOD>{ $AUADICOD }</AUADICOD>
                <AUMODORI>{ $AUMODORI }</AUMODORI>
            </Request>
        </ns0:executeRequest>
};

declare variable $AUMARCOD as xs:int ? external;
declare variable $AUMODCOD as xs:int ? external;
declare variable $AUSUBCOD as xs:int ? external;
declare variable $AUADICOD as xs:int ? external;
declare variable $AUMODORI as xs:string ? external;

xf:Build_OV_1230($AUMARCOD,
    $AUMODCOD,
    $AUSUBCOD,
    $AUADICOD,
    $AUMODORI)
