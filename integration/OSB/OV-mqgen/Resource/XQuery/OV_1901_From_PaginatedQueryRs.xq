(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1901.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1607_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1901_From_PaginatedQueryRs($paginatedQueryRs1 as element(), $orden as element(), $columna as element())
    as element(Response) {
       <Response>
       	{
           if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
		            <CAMPOS>
						<CANT>{data($paginatedQueryRs1/ns0:TotalRowCount)}</CANT>
		            	<ITEMS>
		            	{
					    	if ($orden = 'D' and $columna = '1') then
								for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row order by data($item/ns0:Column[@name = 'POLICY_NO']) descending
									let $detail := local:GetDetail($item)
									return $detail
					    	else if ($orden = 'A' and $columna = '1') then
								for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row order by data($item/ns0:Column[@name = 'POLICY_NO']) 
									let $detail := local:GetDetail($item)
									return $detail
					    	else if ($orden = 'D' and $columna = '2') then
								for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row order by data($item/ns0:Column[@name = 'CLIENDES']) descending
									let $detail := local:GetDetail($item)
									return $detail
					    	else if ($orden = 'A' and $columna = '2') then
								for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row order by data($item/ns0:Column[@name = 'CLIENDES']) 
									let $detail := local:GetDetail($item)
									return $detail
					    	else if ($orden = 'D' and $columna = '3') then
								for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row order by data($item/ns0:Column[@name = 'TOMARIES']) descending
									let $detail := local:GetDetail($item)
									return $detail
					    	else if ($orden = 'A' and $columna = '3') then
								for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row order by data($item/ns0:Column[@name = 'TOMARIES']) 
									let $detail := local:GetDetail($item)
									return $detail
					    	else if ($orden = 'D' and $columna = '4') then
								for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row order by data($item/ns0:Column[@name = 'PATENNUM']) descending
									let $detail := local:GetDetail($item)
									return $detail
					    	else if ($orden = 'A' and $columna = '4') then
								for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row order by data($item/ns0:Column[@name = 'PATENNUM']) 
									let $detail := local:GetDetail($item)
									return $detail
					    	else if ($orden = 'D' and $columna = '5') then
								for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row order by data($item/ns0:Column[@name = 'SITUCPOL']) descending
									let $detail := local:GetDetail($item)
									return $detail
					    	else if ($orden = 'A' and $columna = '5') then
								for $item in $paginatedQueryRs1/ns0:RowSet/ns0:Row order by data($item/ns0:Column[@name = 'SITUCPOL']) 
									let $detail := local:GetDetail($item)
									return $detail
							else ()
						}
						</ITEMS>
		            </CAMPOS>
		        )
		       }
        </Response>
};

declare function local:GetDetail($item as element())
    as element() {
		<ITEM>
			<RAMOPCOD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)) }</RAMOPCOD>
            <POLIZANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,2)) }</POLIZANN>
            <POLIZSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],11,6)) }</POLIZSEC>
            <CERTIPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERTIPOL>
			<CERTIANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERTIANN>
			<CERTISEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERTISEC>
			<VIGDESDE>{ fn-bea:date-to-string-with-format("yyyyMMdd", data($item/ns0:Column[@name='VIGDESDE'])) }</VIGDESDE>
 			<VIGHASTA>{ fn-bea:date-to-string-with-format("yyyyMMdd", data($item/ns0:Column[@name='VIGHASTA'])) }</VIGHASTA>
			<SUPLENUM>{ data($item/ns0:Column[@name = 'SUPLENUM']) }</SUPLENUM>
			<CLIENDES>{ data($item/ns0:Column[@name = 'CLIENDES']) }</CLIENDES>
			<TOMARIES>{ data($item/ns0:Column[@name = 'TOMARIES']) }</TOMARIES>
			<SITUCPOL>{ data($item/ns0:Column[@name = 'SITUCPOL']) }</SITUCPOL>
			<SWDENHAB>{ data($item/ns0:Column[@name = 'SWDENHAB']) }</SWDENHAB>
			<PATENNUM>{ data($item/ns0:Column[@name = 'PATENNUM']) }</PATENNUM>
			<SWSINIES>{ data($item/ns0:Column[@name = 'SWSINIES']) }</SWSINIES>
			<SWEXIGIB>{ data($item/ns0:Column[@name = 'SWEXIGIB']) }</SWEXIGIB>
			<RAMO>
                {
                	let $ramo := data($item/ns0:Column[@name = 'RAMO'])
                	return
						if ($ramo = 'M') then (1)
               			else if ($ramo = 'C') then (2)
                		else ()
                }
			</RAMO>
			<FULTSTRO>{ data($item/ns0:Column[@name = 'FULTSTRO']) }</FULTSTRO>
		</ITEM>
};

declare variable $paginatedQueryRs1 as element() external;
declare variable $orden as element() external;
declare variable $columna as element() external;

xf:OV_1901_From_PaginatedQueryRs($paginatedQueryRs1, $orden, $columna)
