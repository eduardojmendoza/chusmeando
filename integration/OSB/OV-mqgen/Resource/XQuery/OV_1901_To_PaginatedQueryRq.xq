(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/OV_1901.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1901_To_PaginatedQueryRq/";

declare function xf:OV_1901_To_PaginatedQueryRq($request as element(Request), $LOVs as element(lov:LOVs))
    as element() {
        <ns0:PaginatedQueryRq>
            <ns0:QueryID>1901</ns0:QueryID>
            <ns0:FilterCriteria>
               <ns0:FilterCriterion field = "DOCUMTIP"
                                     operation = "EQ"
                                     value = "{ data($LOVs/lov:LOV[@id = "DocTypeNameById" and @ref = data($request/DOCUMTIP) ]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID) }"/>

                 <ns0:FilterCriterion field = "DOCUMDAT"
                                     operation = "EQ"
                                     value = "{ data($request/DOCUMDAT) }"/>

                <ns0:FilterCriterion field = "APELLIDO"
                                     operation = "EQ"
                                     value = "{ data($request/APELLIDO) }"/>

                 <ns0:FilterCriterion field = "POLICY_NO"
                                     operation = "EQ"
                                     value = "{ 
	                                    let $ramo := if (exists($request/RAMO) and data($request/RAMO) != '') then data($request/RAMO) else ('')
	                         			let $poliza := if (exists($request/POLIZA) and data($request/POLIZA) != '') then data($request/POLIZA) else ('')
	                         			return
	                         			if ($ramo != '' and $poliza != '') then (
		                                     xqubh:buildPolicyNo-splitted('0001', 
		                                     $ramo, 
		                                     substring($poliza,1,2), 
		                                     substring($poliza,3,8), 
		                                     '0000', 
		                                     '0001', 
		                                     data($request/CERTIF))
		                                     ) else () }"/>
                                     					
                <ns0:FilterCriterion field = "PATENTE"
                                     operation = "EQ"
                                     value = "{ data($request/PATENTE) }"/>


                 <ns0:FilterCriterion field = "CLIENSECAS"
                                     operation = "EQ"
                                     value = "{ data($request/CLIENSECAS) }"/>

                  <ns0:FilterCriterion field = "NIVELCLAS"
                                     operation = "EQ"
                                     value = "{ data($request/NIVELCLAS) }"/>


                 <ns0:FilterCriterion field = "COLUMNA"
                                     operation = "EQ"
                                     value = "{ data($request/COLUMNA) }"/>

                 <ns0:FilterCriterion field = "ORDEN"
                                     operation = "EQ"
                                     value = "{ data($request/ORDEN) }"/>


            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $request as element(Request) external;
declare variable $LOVs as element(lov:LOVs) external;

xf:OV_1901_To_PaginatedQueryRq($request, $LOVs)
