xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$QbeFindPolicyClientRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeFindPolicyClientRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$qbeFindPersonByPIDRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeFindPersonByPIDRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1116_DomiciliodeCorrespondencia.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1116_From_FndPlcyNumRs/";

declare function xf:OV_1116_From_FndPlcyNumRs($QbeFindPolicyClientRs as element(),
                                              $qbeFindPersonByPIDRs as element(),
                                              $LOVs as element(lov:LOVs))
    as element(Response) {
        <Response>
            <Estado resultado="true" mensaje=""/>
            <CAMPOS>
                <CLIENSEC>{data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:ManID)}</CLIENSEC>
                <CLIENAP1>
                    {
                        if (exists($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData)) then
                            fn:substring(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Family), 1, 20)
                        else
                            fn:substring(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:CompanyData/ns0:Name), 1, 20)
                    }
                </CLIENAP1>
                <CLIENAP2>
                    {
                        if (exists($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData)) then
                            fn:substring(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Family), 21, 40)
                        else
                            fn:substring(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:CompanyData/ns0:Name), 21, 40)
                    }
                </CLIENAP2>
                <CLIENNOM>
                    {
                        if (exists($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData)) then
                            data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Given)
                        else
                            ''
                    }
                </CLIENNOM>
                <DOMICSEC></DOMICSEC>
                <CUENTSEC></CUENTSEC>
                <DOCUMTIP>
                    {
                        if (exists($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData)) then
                            substring-before(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData/ns0:PID),'-')
                        else
                        	substring-before(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:CompanyData/ns0:CustomerID),'-')
                    }
                </DOCUMTIP>
                <DOCUMDAT>
                    {
                        if (exists($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData)) then
                            substring-after(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData/ns0:PID),'-')
                        else
                            substring-after(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:CompanyData/ns0:CustomerID),'-')
                    }
                </DOCUMDAT>
                <PAISCOD>{  data($LOVs/lov:LOV[@id = "LkpCountryByInsisCode"]/ns0:ListItems/ns0:ListItem/ns0:ItemID) }</PAISCOD>
                <PAISDES>{ data($LOVs/lov:LOV[@id = "LkpCountryByInsisCode"]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) } </PAISDES>
                <SEXO>
                	{
                		let $sexo := data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData/ns0:Gender)
                		return
                			if ($sexo = '1') then ('MASCULINO')
                			else if ($sexo = '2') then ('FEMENINO')
                			else ('** NO INFORMADO **')
                	}
                </SEXO>
                <FECNACIM>
                    {
                        let $fecha := $QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData/ns0:BirthDate
                        return
                            if (string-length($fecha) > 0)
                            then ( fn-bea:date-to-string-with-format("dd/MM/yyyy", $fecha) )
                            else ('')
                    }
                </FECNACIM>
                <CLIENEST>{ data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:DataSource) }</CLIENEST>
                <ESTCIV>{ data($LOVs/lov:LOV[@id = "MaritalStatus"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:DataSource)]/ns0:ItemDescription) }</ESTCIV>

                <FUMADOR></FUMADOR>
                <CLIENDOZ></CLIENDOZ>
                <DIESZUR></DIESZUR>
                <FECFALLE></FECFALLE>

                <CLIENTIP>
                    {
                        if (exists($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData)) then
                            '00'
                        else
                            '15'
                    }
                </CLIENTIP>

                <IDIOMCOD></IDIOMCOD>

                <EFECTANN>{ fn:substring(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:Questionary/ns0:Question[ns0:ID="3005.32"]/ns0:Answer ),1,4)}</EFECTANN>
                <EFECTMES>{ fn:substring(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:Questionary/ns0:Question[ns0:ID="3005.32"]/ns0:Answer ),5,6)}</EFECTMES>
                <EFECTDIA>{ fn:substring(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:Questionary/ns0:Question[ns0:ID="3005.32"]/ns0:Answer ),7,8)}</EFECTDIA>

                <ABRIDTIP></ABRIDTIP>
                <ABRIDNUM></ABRIDNUM>
                <CLIENORG></CLIENORG>

                <CLIENIVA>{ data($qbeFindPersonByPIDRs/ns0:TaxData/ns0:IVAType )}</CLIENIVA>
                <CLIESEEX>0</CLIESEEX>
                <CLIEIBTP>{ data($qbeFindPersonByPIDRs/ns0:TaxData/ns0:IBRType )}</CLIEIBTP>
                <CLIEIBEX>0</CLIEIBEX>
                <CLIEIITP>{ data($qbeFindPersonByPIDRs/ns0:TaxData/ns0:IITType )} </CLIEIITP>
                <CLIEIIEX>0</CLIEIIEX>

                <CLIEIBNU>{ data($qbeFindPersonByPIDRs/ns0:TaxData/ns0:IBRNumber )}</CLIEIBNU>
                <CLIEGNTP>{ data($qbeFindPersonByPIDRs/ns0:TaxData/ns0:GANType )} </CLIEGNTP>
                <CUIT>
                    {
                        if (exists($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:CompanyData)) then
                               substring-after(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:CompanyData/ns0:CustomerID),'-')
                     	else ()
                     } 
                </CUIT>

                <PROFECOD>{ data( $QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:IndustryCode) }</PROFECOD>
                <PROFEDES>
                    {
                            (: Los parametros de seleccion estan en el Proxy porque son obligatorios para el LOV :)
                            data($LOVs/lov:LOV[@id = "IndustryCode"]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription)
                    }
                </PROFEDES>
                <DOMICCAL>PAR</DOMICCAL>
                <DOMICDOM>{ data( $QbeFindPolicyClientRs/ns0:Client/ns0:Addresses/ns0:Address[1]/ns0:ResidentialAddress/ns0:StreetName)}</DOMICDOM>
                <DOMICDNU>{ data( $QbeFindPolicyClientRs/ns0:Client/ns0:Addresses/ns0:Address[1]/ns0:ResidentialAddress/ns0:StreetNo)}</DOMICDNU>
                <DOMICPIS>{ data( $QbeFindPolicyClientRs/ns0:Client/ns0:Addresses/ns0:Address[1]/ns0:ResidentialAddress/ns0:Floor)}</DOMICPIS>
                <DOMICPTA>{ data( $QbeFindPolicyClientRs/ns0:Client/ns0:Addresses/ns0:Address[1]/ns0:ResidentialAddress/ns0:Apartment)}</DOMICPTA>
                <DOMICCPO>{ data( $QbeFindPolicyClientRs/ns0:Client/ns0:Addresses/ns0:Address[1]/ns0:ZipCode)}</DOMICCPO>
                
                <CPACODPO>{ data( $QbeFindPolicyClientRs/ns0:Client/ns0:Addresses/ns0:Address[1]/ns0:ZipCode)}</CPACODPO>
                
                <DOMICPOB>{ data( $QbeFindPolicyClientRs/ns0:Client/ns0:Addresses/ns0:Address[1]/ns0:City)} </DOMICPOB>
                <PROVICOD>{ data( $QbeFindPolicyClientRs/ns0:Client/ns0:Addresses/ns0:Address[1]/ns0:StateRegion)}</PROVICOD>
                <PROVIDES>{ data($LOVs/lov:LOV[@id = "Province"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($QbeFindPolicyClientRs/ns0:Client/ns0:Addresses/ns0:Address[1]/ns0:StateRegion)]/ns0:ItemDescription) }</PROVIDES>
                <PAISSCOD> { data($LOVs/lov:LOV[@id = "LkpCountryByInsisCode"]/ns0:ListItems/ns0:ListItem/ns0:ItemID) } </PAISSCOD>
                <PAISSDES></PAISSDES>
                <DOMICTLF>{ data($QbeFindPolicyClientRs/ns0:Client/ns0:Contacts/ns0:Contact[1]/ns0:Details) }</DOMICTLF>

                <MEDCODSEC>{ count($QbeFindPolicyClientRs/ns0:Client/ns0:Contacts/* )}</MEDCODSEC>
                <MEDIOCONTACTOS>
                    {
                        for $Contact in $QbeFindPolicyClientRs/ns0:Client/ns0:Contacts/ns0:Contact
                        return
                            <MEDIOCONTACTO>
                                <MEDCOCOD>{ data($Contact/ns0:Type) }</MEDCOCOD>
                                <MEDCODAB>{ data($LOVs/lov:LOV[@id = "Contact"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($Contact/ns0:Type)]/ns0:ItemDescription) }</MEDCODAB>
                                <MEDCODAT>{ data($Contact/ns0:Details) }</MEDCODAT>
                            </MEDIOCONTACTO>
                    }
                </MEDIOCONTACTOS>
                <PERMITIR>
                    {
                        if( compare(data($QbeFindPolicyClientRs/ns0:InsuranceConditions/ns0:PolicyState), '0') or
                            compare(data($QbeFindPolicyClientRs/ns0:InsuranceConditions/ns0:PolicyState), '11') or
                            compare(data($QbeFindPolicyClientRs/ns0:InsuranceConditions/ns0:PolicyState), '12')) then
                            'S'
                        else
                            'N'
                    }
                </PERMITIR>

                (: <DOCUMTIPCOD>{data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:Documents/ns0:Document[ns0:DocumentID = 'DOCTIPCOD']/ns0:DocumentNo)}</DOCUMTIPCOD>
                <DOCUMTIPPEDF>{data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:Documents/ns0:Document[ns0:DocumentID = 'DOCTIPPEDF']/ns0:DocumentNo)}</DOCUMTIPPEDF> :)
                
                <DOCUMTIPPEDF>
                {
                        if (exists($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData)) then
                              	data($LOVs/lov:LOV[@id = "DocTypeByName" and @ref= substring-before(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData/ns0:PID),'-')]/ns0:ListItems/ns0:ListItem/ns0:ItemID)
                        else
                        		data($LOVs/lov:LOV[@id = "DocTypeByName" and @ref= substring-before(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:CompanyData/ns0:CustomerID),'-')]/ns0:ListItems/ns0:ListItem/ns0:ItemID)
              
                    }</DOCUMTIPPEDF>
                <DOCUMTIPCOD>{
                        if (exists($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData)) then
                              	data($LOVs/lov:LOV[@id = "DocTypeByName" and @ref= substring-before(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:PersonalData/ns0:PID),'-')]/ns0:ListItems/ns0:ListItem/ns0:ItemID)
                        else
                        		data($LOVs/lov:LOV[@id = "DocTypeByName" and @ref= substring-before(data($QbeFindPolicyClientRs/ns0:Client/ns0:Entity/ns0:CompanyData/ns0:CustomerID),'-')]/ns0:ListItems/ns0:ListItem/ns0:ItemID)
              
                    }</DOCUMTIPCOD>
                
              </CAMPOS>
        </Response>
};

declare variable $QbeFindPolicyClientRs as element() external;
declare variable $qbeFindPersonByPIDRs as element() external;
declare variable $LOVs as element(lov:LOVs) external;

xf:OV_1116_From_FndPlcyNumRs($QbeFindPolicyClientRs,
                             $qbeFindPersonByPIDRs,
                             $LOVs)
