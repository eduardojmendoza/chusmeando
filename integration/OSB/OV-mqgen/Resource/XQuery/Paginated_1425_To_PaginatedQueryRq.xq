xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_1425.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1425_To_PaginatedQueryRq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:Paginated_1425_To_PaginatedQueryRq($req as element(Request), $StartRow as xs:integer, $RowCount as xs:integer, $ResultSetID as xs:string)
    as element() {
        <ns0:PaginatedQueryRq>
        	<ns0:QueryID>1425</ns0:QueryID>
        	<ns0:StartRow>{ $StartRow }</ns0:StartRow>
            <ns0:RowCount>{ $RowCount }</ns0:RowCount>
            <ns0:FilterCriteria>
                <ns0:FilterCriterion field = "NIVELAS"
                                     operation = "EQ"
                                     value = "{ data($req/NIVELCLAS) }" />
                <ns0:FilterCriterion field = "CLIENSECAS"
                                     operation = "EQ"
                                     value = "{ data($req/CLIENSECAS) }" />

                		<ns0:FilterCriterion field = "RECNUM"
                                     operation = "EQ"
                                     value = "{
                                     	for $rec at $i in $req/RECIBOS/RECIBO/RECNUM
                                     	return
                                             if (data($rec) != '') then (
                                                 concat(substring(data($rec),1,2),'-',substring(data($rec),3,1),'-',substring(data($rec),4,9), 
                                                        if (count($req/RECIBOS/RECIBO/RECNUM) != $i) then ',' else '')
                                            ) else ()
                                     }" />
            </ns0:FilterCriteria>
        </ns0:PaginatedQueryRq>
};

declare variable $req as element(Request) external;
declare variable $StartRow as xs:integer external;
declare variable $RowCount as xs:integer external;
declare variable $ResultSetID as xs:string external;

xf:Paginated_1425_To_PaginatedQueryRq($req, $StartRow, $RowCount, $ResultSetID)