(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$service1232" element="Response" location="../XSD/Service1232.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$QbeFindPolicyInsuredsRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeFindPolicyInsuredsRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1213.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1213_From_FndPlcyByNumRs/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:OV_1213_From_FndPlcyByNumRs($LOVs as element()?
												, $service1232 as element()?
												, $driversTransRs as element()?
												, $QbeFindPolicyInsuredsRs as element())
    as element(Response) {
        <Response>
            <Estado resultado = "true"
                    mensaje = ""/>
            <CAMPOS>
                <MOTORNUM>{ data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:Engine) }</MOTORNUM>
                <PATENNUM>{ data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:RegNo) }</PATENNUM>
                <CHASINUM>{ data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:Chassis) }</CHASINUM>
                <VEHICSEC>{ data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:ObjectID) }</VEHICSEC>
                <AUMARCOD>{ data($service1232/CAMPOS/AUMARCOD) }</AUMARCOD>
                (:<AUMARDES>{ data($LOVs/lov:LOV[@id = "Make"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($service1232/CAMPOS/AUMARCOD)]/ns0:ItemDescription) }</AUMARDES>:)
                <AUMARDES>{ data($service1232/CAMPOS/AUMARDES) }</AUMARDES>
                <AUMODCOD>{ data($service1232/CAMPOS/AUMODCOD) }</AUMODCOD>
                (:<AUMODDES>{ data($LOVs/lov:LOV[@id = "Model"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($service1232/CAMPOS/AUMODCOD)]/ns0:ItemDescription) }</AUMODDES>:)
                <AUMODDES>{ data($service1232/CAMPOS/AUMODDES) }</AUMODDES>
                <AUSUBCOD>{ data($service1232/CAMPOS/AUSUBCOD) }</AUSUBCOD>
                <AUSMODES>{ data($service1232/CAMPOS/AUSMODES) }</AUSMODES>
                <AUADICOD>{ data($service1232/CAMPOS/AUADICOD) }</AUADICOD>
                <AUADIDES>{ data($service1232/CAMPOS/AUADIDES) }</AUADIDES>
                <AUMODORI>{ data($service1232/CAMPOS/AUMODORI) }</AUMODORI>
                (:<AUMODODE>{ data($LOVs/lov:LOV[@id = "ProdType"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($service1232/CAMPOS/AUMODORI)]/ns0:ItemDescription) }</AUMODODE>:)
                <AUMODODE>{ data($service1232/CAMPOS/AUMODODE) }</AUMODODE>
                <AUUSOCOD>{ data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarUsage) }</AUUSOCOD>
                <AUUSODES>{ data($LOVs/lov:LOV[@id = "CarUsage"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarUsage)]/ns0:ItemDescription) }</AUUSODES>
				<AUVTVCOD></AUVTVCOD>(:EMPTY TAG:)
				<AUVTVFEC></AUVTVFEC>(:EMPTY TAG:)
                <AUCOLCOD>{ data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarColor) }</AUCOLCOD>
                <AUCOLDES>{ data($LOVs/lov:LOV[@id = "CarColor"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarColor)]/ns0:ItemDescription) }</AUCOLDES>
				<AUKLMNUM>{ data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP1']/ns0:Value) }</AUKLMNUM>
				<AUES0KLM>
	            	{
	            		let $value := data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP6']/ns0:Value)
	            		return
	                	 	if ($value = '1') then ('SI')
	                	 	else if ($value = '2') then ('NO')
	                	 	else ($value)
	            	}
				</AUES0KLM>
                <AUNROFAC>{ data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:Documents/ns0:Document[ns0:DocumentID='PURCHINV']/ns0:DocumentNo) }</AUNROFAC>
                <AUCATCOD></AUCATCOD>(:EMPTY TAG:)
				<AUCATDES></AUCATDES>(:EMPTY TAG:)
				<AUTIPCOD>{ data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarType) }</AUTIPCOD>
				<AUTIPDES>{ data($LOVs/lov:LOV[@id = "CarType"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CarType)]/ns0:ItemDescription) }</AUTIPDES>
				(: <FABRIFEC>{ fn-bea:date-to-string-with-format("yyyyMMdd", data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:ProdYear)) }</FABRIFEC> :)
				<FABRIFEC>{ data(concat('01/',data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:ProdYear))) }</FABRIFEC>
                <GUGARAGE>
                	{
                		let $valOcp2 := data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP2']/ns0:Value)
                		return if ($valOcp2 = '1') then ('S') else ('N')
                	}
                </GUGARAGE>
                <GUDOMICI>
                	{
                		let $valOcp2 := data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP2']/ns0:Value)
                		return if ($valOcp2 = '1') then ('S') else ('N')
                	}
				</GUDOMICI>
            	<PAISSCOD>{ '00' }</PAISSCOD>
                <PAISSDES>{ 'ARGENTINA' }</PAISSDES>
                <PROVICOD>{ data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP8']/ns0:Value) }</PROVICOD>
                <PROVIDES>{ data($LOVs/lov:LOV[@id = "Province"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP8']/ns0:Value)]/ns0:ItemDescription) }</PROVIDES>
                <CODIZONA>{ data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP4']/ns0:Value) }</CODIZONA>
                <ZONASDES>{ data($LOVs/lov:LOV[@id = "Zone"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP4']/ns0:Value)]/ns0:ItemDescription) }</ZONASDES>
                <CPACODPO>{ data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName = "OCP5"]/ns0:Value) }</CPACODPO>
                <AUCIASAN></AUCIASAN>(:EMPTY TAG:)
                <CIAASDAB></CIAASDAB>(:EMPTY TAG:)
                <AUANTANN>{ data($QbeFindPolicyInsuredsRs/ns0:InsuranceConditions/ns0:Questionary/ns0:Question[ns0:ID='3005.17']/ns0:Answer) }</AUANTANN>
                <AUNUMSIN>{ data($QbeFindPolicyInsuredsRs/ns0:InsuranceConditions/ns0:Questionary/ns0:Question[ns0:ID='3005.31']/ns0:Answer) }</AUNUMSIN>
                <AUNUMKMT>{ data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP1']/ns0:Value) }</AUNUMKMT>
                <AUUSOGNC>
                	{
                		let $valOcp13 := data($QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='OCP13']/ns0:Value)
                		return if ($valOcp13 = '1') then ('S') else ('N')
                	}
                </AUUSOGNC>
				<DATOSCONDUCTORES>
				{
					for $driver in $QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:Drivers/ns0:Driver
					order by substring(data($driver/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='Iod1']/ns0:Value), 1, 1)
					return
					if ((upper-case(data($driversTransRs/ns0:RowSet/ns0:Row/ns0:Column[@name = "DRIVER_ID" and text() = data($driver/ns0:DriverID)]/parent::node()/ns0:Column[@name = "IOD2"]))="SON") or (upper-case(data($driversTransRs/ns0:RowSet/ns0:Row/ns0:Column[@name = "DRIVER_ID" and text() = data($driver/ns0:DriverID)]/parent::node()/ns0:Column[@name = "IOD1"]))="PRIMARY")) then 
					(
						<DATOSCONDUCTOR>
							<CONDCLIENAP1>{ data($driver/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Family) }</CONDCLIENAP1>
							<CONDCLIENNOM>{ data($driver/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Given) }</CONDCLIENNOM>
							{
		                        if (fn:exists($driver/ns0:Entity/ns0:PersonalData/ns0:PID)) then
		                        (
									<CONDDOCUMTIP>{ data($LOVs/lov:LOV[@id = "DocTypeByName" and @ref=data($driver/ns0:Entity/ns0:PersonalData/ns0:PID)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemID) }</CONDDOCUMTIP>,
			        				(:<CONDDOCUMDAB>{ data($LOVs/lov:LOV[@id = "DocTypeByName" and @ref = data($driver/ns0:Entity/ns0:PersonalData/ns0:PID) ]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemId) }</CONDDOCUMDAB>,:)
			        				<CONDDOCUMDAB>{ data(substring-before($driver/ns0:Entity/ns0:PersonalData/ns0:PID,'-')) }</CONDDOCUMDAB>,
			        				
			        				<CONDDOCUMDAT>{ if (substring-after($driver/ns0:Entity/ns0:PersonalData/ns0:PID,'-')!="") then (
			        								data(substring-after($driver/ns0:Entity/ns0:PersonalData/ns0:PID,'-'))
			        								) else(
			        								data($driver/ns0:Entity/ns0:PersonalData/ns0:PID)
			        								)
			        								 }</CONDDOCUMDAT>
							
			        				
								) else
								(
									<CONDDOCUMTIP>{ data(substring-before($driver/ns0:Entity/ns0:CompanyData/ns0:CustomerID,'-')) }</CONDDOCUMTIP>,
		        					(:<CONDDOCUMDAB>{ data($LOVs/lov:LOV[@id = "DocTypeByName" and @ref = data($driver/ns0:Entity/ns0:CompanyData/ns0:CustomerID) ]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemId) }</CONDDOCUMDAB>,:)
		        					<CONDDOCUMDAB>{ data(substring-before($driver/ns0:Entity/ns0:CompanyData/ns0:CustomerID,'-')) }</CONDDOCUMDAB>,
		        					<CONDDOCUMDAT>{ data(substring-after($driver/ns0:Entity/ns0:CompanyData/ns0:CustomerID,'-')) }</CONDDOCUMDAT>
		        				)
	        				}
	        				<CONDESTCIV>{ data($LOVs/lov:LOV[@id = "MaritalStatus"  and @ref=data($driver/ns0:Entity/ns0:DataSource)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</CONDESTCIV>
							<CONDSEXO>{
                        		let $sexo := data($driver/ns0:Entity/ns0:PersonalData/ns0:Gender)
                        		return
                        			if ($sexo = '1') then ('MASCULINO')
                        			else if ($sexo = '2') then ('FEMENINO')
                        			else ('** NO INFORMADO **')
                        	}</CONDSEXO>
							<CONDNACIMFEC>{ fn-bea:date-to-string-with-format("dd/MM/yyyy", data($driver/ns0:Entity/ns0:PersonalData/ns0:BirthDate)) }</CONDNACIMFEC>
							<CONDTIPOCOND>{ substring(data($driver/ns0:CustomProperties/ns0:CustomProperty[ns0:FieldName='Iod1']/ns0:Value), 1, 1) }</CONDTIPOCOND>
							<CONDVINCCOND>{
							if (upper-case(data($driversTransRs/ns0:RowSet/ns0:Row/ns0:Column[@name = "DRIVER_ID" and text() = data($driver/ns0:DriverID)]/parent::node()/ns0:Column[@name = "IOD1"]))="PRIMARY") then 
							() else('HI') 
							}</CONDVINCCOND>
							<CONDINCLUIDO>
							{
							if ((upper-case(data($driversTransRs/ns0:RowSet/ns0:Row/ns0:Column[@name = "DRIVER_ID" and text() = data($driver/ns0:DriverID)]/parent::node()/ns0:Column[@name = "IOD1"]))="PRIMARY")) then
							('S') else (
							if ((upper-case(data($driversTransRs/ns0:RowSet/ns0:Row/ns0:Column[@name = "DRIVER_ID" and text() = data($driver/ns0:DriverID)]/parent::node()/ns0:Column[@name = "IOD1"]))="SECONDARY") and (upper-case(data($driversTransRs/ns0:RowSet/ns0:Row/ns0:Column[@name = "DRIVER_ID" and text() = data($driver/ns0:DriverID)]/parent::node()/ns0:Column[@name = "IOD3"]))="Y")) then
							('S')
							else
							('N')
							)
							}
							</CONDINCLUIDO>
						</DATOSCONDUCTOR>
					) else ()
				}
				</DATOSCONDUCTORES>
				<DATOSACCESORIOS>
				{
					for $SupEquip in $QbeFindPolicyInsuredsRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredEntity/ns0:OCar/ns0:SupplementaryEquipment/ns0:OAdditionalEquipment
					return
						<DATOSACCESORIO>
							<AUACCCOD>{ data($SupEquip/ns0:EquipmentSubtype) }</AUACCCOD>
							<AUACCDES>{ data($SupEquip/ns0:Model) }</AUACCDES>
							<AUVEASUM>{ xs:integer(data($SupEquip/ns0:AvValue) * 100) }</AUVEASUM>
							<AUVEADES>{ data($SupEquip/ns0:Model) }</AUVEADES>
							<AUVEADEP></AUVEADEP>
						</DATOSACCESORIO>
				}
				</DATOSACCESORIOS>
				<DATOSASEGADICIONALES>
				{
					for $adic in $QbeFindPolicyInsuredsRs/ns0:InsuranceConditions/ns0:Participants/ns0:Participant[ns0:ParticipantRole = "PARTICIP"]
					return
					(
						<DATOSASEGADICIONAL>
						{
							if (fn:exists($adic/ns0:Entity/ns0:PersonalData/ns0:PID)) then
		                    (
									<ASEGDOCUMDAT>{ data($adic/ns0:Entity/ns0:PersonalData/ns0:PID) }</ASEGDOCUMDAT>,
			        				<ASEGNOMBRE>{ concat(data($adic/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Given), ' ' , data($adic/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Family)) }</ASEGNOMBRE>
			        		) else if (fn:exists($adic/ns0:Entity/ns0:CompanyData/ns0:CustomerID)) then
			        			(
			        				<ASEGDOCUMDAT>{ data(substring-before($adic/ns0:Entity/ns0:CompanyData/ns0:CustomerID,'-')) }</ASEGDOCUMDAT>,
			        				<ASEGNOMBRE>{ data(substring-after($adic/ns0:Entity/ns0:CompanyData/ns0:CustomerID,'-')) }</ASEGNOMBRE>
			        			)
			        		else ()
			        	}
						</DATOSASEGADICIONAL>
					)
				}
				</DATOSASEGADICIONALES>
				<PERMITIRENDOSO>
				{
					let $state := data($QbeFindPolicyInsuredsRs/ns0:InsuranceConditions/ns0:PolicyState)
					return
					if ($state = "0" or $state = "11" or $state = "12") then
						'S'
					else
						'N'
				}
				</PERMITIRENDOSO>
            </CAMPOS>
        </Response>
};

declare variable $LOVs as element(lov:LOVs)? external;
declare variable $service1232 as element()? external;
declare variable $driversTransRs as element()? external;
declare variable $QbeFindPolicyInsuredsRs as element() external;

xf:OV_1213_From_FndPlcyByNumRs($LOVs, $service1232, $driversTransRs, $QbeFindPolicyInsuredsRs)
