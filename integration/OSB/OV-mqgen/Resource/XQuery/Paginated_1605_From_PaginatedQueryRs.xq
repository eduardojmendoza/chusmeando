xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1605.xsd" ::)

declare namespace xf = "http://tempuri.org/SiniestroDenuncia/Resource/XQuery/OV_1605_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:Paginated_1605_From_PaginatedQueryRs($paginatedQueryRs as element())
    as element(Response) {
        <Response>
            {
                if ( number(data($paginatedQueryRs/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la consulta solicitada"/>,
                    <MSGEST>ER</MSGEST>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <MSGEST>TR</MSGEST>,
                    <NROQRYS>{ data($paginatedQueryRs /NROQRYS) }</NROQRYS>,
                    <ResultSetID>{data($paginatedQueryRs/ns0:ResultSetID)}</ResultSetID>,
                    <REGS>
                        {
                            for $reg in $paginatedQueryRs/ns0:RowSet/ns0:Row
                        return
                            (
                            <REG>
	                            <RAMOPCOD>{ substring(data($reg/ns0:Column[@name = 'POLICY_NO']),5,4) }</RAMOPCOD>
				                <POL>{ data(substring($reg/ns0:Column[@name = 'POLICY_NO'],9,8)) }</POL>
				                <CERTIPOL>{ data(substring($reg/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERTIPOL>
								<CERTIANN>{ data(substring($reg/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERTIANN>
								<CERTISEC>{ data(substring($reg/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERTISEC>
								<SUPLENUM>{ data($reg/ns0:Column[@name='SUPLENUM']) }</SUPLENUM>
							 	<CLIENSEC>{ data($reg/ns0:Column[@name='CLIENSEC']) }</CLIENSEC>
							 	<CLIENDES>{ data($reg/ns0:Column[@name='CLIENDES']) }</CLIENDES>
							 	<TOMARIES>{ data($reg/ns0:Column[@name='TOMARIES']) }</TOMARIES>
							 	<SITUCPOL>{ data($reg/ns0:Column[@name='SITUCPOL']) }</SITUCPOL>
							 	<AGENTCLA>{ data($reg/ns0:Column[@name='AGENTCLA']) }</AGENTCLA>
							 	<AGENTCOD>{ data($reg/ns0:Column[@name='AGENTCOD']) }</AGENTCOD>
							 	<COBRODES>{ data($reg/ns0:Column[@name='COBRODES']) }</COBRODES>
							 	<RAMO>
							 		{ 
							 		let $ramo := data($reg/ns0:Column[@name='RAMO']) 
							 	    return 
							 	    if ($ramo = 'M') then ('1')
							 		else ('0')
							 		}
							 	</RAMO>
								<IMPRIME>{ data($reg/ns0:Column[@name='IMPRIME']) }</IMPRIME>
								<EXIGIBLE>
									{ 
									(: let $exigible := data($reg/ns0:Column[@name='EXIGIBLE']) return if ($exigible = 'S') then ('1') else ('2') :)
									data($reg/ns0:Column[@name='EXIGIBLE'])
									}
								</EXIGIBLE>
								<SINIESTR>{ data($reg/ns0:Column[@name='SINIESTR']) }</SINIESTR>
						 	</REG>
  							)
                        }
                    </REGS>
                                    )
            }
        </Response>
};

declare variable $paginatedQueryRs as element() external;




xf:Paginated_1605_From_PaginatedQueryRs($paginatedQueryRs)
