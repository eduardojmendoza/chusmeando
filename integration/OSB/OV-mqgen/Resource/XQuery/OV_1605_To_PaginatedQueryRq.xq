xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/OV_1605.xsd" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:PaginatedQueryRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace xf = "http://tempuri.org/SiniestroDenuncia/Resource/XQuery/OV_1605_To_PaginatedQueryRq/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:OV_1605_To_PaginatedQueryRq($request as element(Request), $LOVs as element()?)
    as element() {
    <ns0:PaginatedQueryRq>
            <ns0:QueryID>1605</ns0:QueryID>
            <ns0:FilterCriteria>
			{
	        	if ( (not (exists($request/NIVEL1)) or data($request/NIVEL1) = '')
		           and (not (exists($request/CLIENSEC1)) or data($request/CLIENSEC1) = '')
		           and (not (exists($request/NIVEL2)) or data($request/NIVEL2) = '')
		           and (not (exists($request/CLIENSEC2)) or data($request/CLIENSEC2) = '')
		           and (not (exists($request/NIVEL3)) or data($request/NIVEL3) = '')
		           and (not (exists($request/CLIENSEC3)) or data($request/CLIENSEC3) = ''))
                    then (
                        if (not(exists($request/NIVELAS)) or data($request/NIVELAS) = '' )
                    	then (
                        <ns0:FilterCriterion field = "NIVEL{ index-of(('GO', 'OR', 'PR'), upper-case(data($request/NIVELCLAS))) }"
                         					operation = "EQ"
                         					value = "{ data($request/NIVELCLAS) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC{ index-of(('GO', 'OR', 'PR'), upper-case(data($request/NIVELCLAS))) }"
                            				 operation = "EQ"
                           					  value = "{ data($request/CLIENSECAS) }"/> 
                          )else(
                          	<ns0:FilterCriterion field = "NIVEL{ index-of(('GO', 'OR', 'PR'), upper-case(data($request/NIVELAS))) }"
                         					operation = "EQ"
                         					value = "{ data($request/NIVELAS) }"/>,
                        	<ns0:FilterCriterion field = "CLIENSEC{ index-of(('GO', 'OR', 'PR'), upper-case(data($request/NIVELAS))) }"
                            				 operation = "EQ"
                           					  value = "{ data($request/CLIENSECAS) }"/>
                          )
                    )
                    else (
                        <ns0:FilterCriterion field = "NIVEL1"
                                             operation = "EQ"
                                             value = "{ data($request/NIVEL1) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC1"
                                             operation = "EQ"
                                             value = "{ data($request/CLIENSEC1) }"/>,
                        <ns0:FilterCriterion field = "NIVEL2"
                                             operation = "EQ"
                                             value = "{ data($request/NIVEL2) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC2"
                                             operation = "EQ"
                                             value = "{ data($request/CLIENSEC2) }"/>,
                        <ns0:FilterCriterion field = "NIVEL3"
                                             operation = "EQ"
                                             value = "{ data($request/NIVEL3) }"/>,
                        <ns0:FilterCriterion field = "CLIENSEC3"
                                             operation = "EQ"
                                             value = "{ data($request/CLIENSEC3) }"/>
                    )
            }
         	{

            		if (data($request/TIPODOCU) != '') then
            		(
						<ns0:FilterCriterion field = "TIPODOCU"
		                                     operation = "EQ"
		                                     value = "{ let $doc := xs:int($request/TIPODOCU)
		                                     			return
		                                     			let $docType :=  data($LOVs/lov:LOV[@id = "DocTypeNameById" and @ref = $doc ][1]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID)
		                                     			return
		                                     			$docType }" />
					) else ()
				}
			(:{
				if (data($request/TIPODOCU) != '' ) then
	            (
						<ns0:FilterCriterion field = "TIPODOCU"
	                                     operation = "EQ"
	                                     value = "{ data($request/TIPODOCU) }"/>
				) else ()
			}:)
			{
				if (data($request/NUMEDOCU) != '' ) then
	            (
						<ns0:FilterCriterion field = "NUMEDOCU"
	                                     operation = "EQ"
	                                     value = "{ data($request/NUMEDOCU) }"/>
				) else ()
			}
			{
				if (data($request/APELLIDO) != '' ) then
	            (
						<ns0:FilterCriterion field = "APELLIDO"
	                                     operation = "EQ"
	                                     value = "{ data($request/APELLIDO) }"/>
				) else ()
			}
            
			<ns0:FilterCriterion field = "POLICY_NO"
                                     operation = "EQ"
                                            value = "{
			                                         let $producto := data($request/RAMOPCOD)
			                                         let $poliza := data($request/POLIZA)
			                                         let $certi := data($request/CERTI)
			                                         return
			                                             if ($producto != '' and $poliza != '') then
			                                             (
			                                              xqubh:buildPolicyNo('0001', $producto, $poliza , $certi)
			                                            ) else ()
			                                     
			                                     		}"/>

			{
				if (data($request/ESTAPOL) != '' ) then
	            (
						<ns0:FilterCriterion field = "ESTAPOL"
	                                     operation = "EQ"
	                                     value = "{ data($request/ESTAPOL) }"/>
				) else ()
			}
            </ns0:FilterCriteria>
    </ns0:PaginatedQueryRq>
};

declare variable $request as element(Request) external;
declare variable $LOVs as element(lov:LOVs)? external;

xf:OV_1605_To_PaginatedQueryRq($request, $LOVs)
