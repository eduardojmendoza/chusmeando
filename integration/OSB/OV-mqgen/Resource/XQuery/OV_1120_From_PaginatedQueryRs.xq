xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1120.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1120_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1120_From_PaginatedQueryRs($paginatedQueryRs as element())
    as element(Response) {
        
         <Response>
        <Estado resultado = "true" mensaje = ""/>
           
            <CAMPOS>
    		<CANT>{data($paginatedQueryRs/ns0:TotalRowCount)}</CANT>
    		<ITEMS>
    			
    		{
	            for $item in $paginatedQueryRs/ns0:RowSet/ns0:Row
	            return
				(
				
					<ITEM>
						(:'0001'; RAMOPCOD C(4); POLIZANN N(2); 
						POLIZSEC N(6);CERTIPOL N(4); CERTIANN N(4);CERTISEC N(6) :)
						
						<RAMOPCOD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)) }</RAMOPCOD>
						<POLIZANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,2)) }</POLIZANN>
						<POLIZSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],11,6)) }</POLIZSEC>
						<CERTIPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERTIPOL>
						<CERTIANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERTIANN>
						<CERTISEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERTISEC>
						
						<CLIENDES>{ data($item/ns0:Column[@name = 'CLIENDES']) }</CLIENDES>
						<TOMARIES>{ data($item/ns0:Column[@name = 'TOMARIES']) }</TOMARIES>
						<PATENNUM>{ data($item/ns0:Column[@name = 'PATENNUM']) }</PATENNUM>
						<SITUCPOL>{ data($item/ns0:Column[@name = 'SITUCPOL']) }</SITUCPOL>
						<SWSINIES>{ data($item/ns0:Column[@name = 'SWSINIES']) }</SWSINIES>
						<SWEXIGIB>{ data($item/ns0:Column[@name = 'SWEXIGIB']) }</SWEXIGIB>
						<COLECTIP>{ data($item/ns0:Column[@name = 'COLECTIP']) }</COLECTIP>
						<VIGDESDE>{ data($item/ns0:Column[@name = 'VIGDESDE']) }</VIGDESDE>
						<VIGHASTA>{ data($item/ns0:Column[@name = 'VIGHASTA']) }</VIGHASTA>
						<FULTSTRO>{ data($item/ns0:Column[@name = 'FULTSTRO']) }</FULTSTRO>
						
					</ITEM>
				)
			}
    		
    		</ITEMS>
    		
			</CAMPOS>
		</Response>
        
        
       
        
};

declare variable $paginatedQueryRs as element() external;

xf:OV_1120_From_PaginatedQueryRs($paginatedQueryRs)
