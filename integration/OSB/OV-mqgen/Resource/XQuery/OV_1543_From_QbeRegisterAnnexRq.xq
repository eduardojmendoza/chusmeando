(:: pragma bea:global-element-parameter parameter="$request" element="Request" location="../XSD/OV_1543.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$qbeRegisterAnnexRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeRegisterAnnexRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1543.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1543_From_QbeRegisterAnnexRq/";

declare function xf:OV_1543_From_QbeRegisterAnnexRq($request as element(Request),$qbeRegisterAnnexRs as element())
    as element(Response) {
        <Response>
        { let $annexID := data($qbeRegisterAnnexRs/ns0:AnnexID) return
        if ( not(exists($qbeRegisterAnnexRs/ns0:AnnexID))) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
            
            <CAMPOS>
                <MENSAJEO>PROPUESTA EMITIDA</MENSAJEO>
                <RAMOPCODO>{ data($request/RAMOPCOD) }</RAMOPCODO>
                <POLIZANNO>{ data($request/POLIZANN) }</POLIZANNO>
           		<POLIZSECO>{ data($request/POLIZSEC) }</POLIZSECO>
                <CERTIPOLO>{ data($request/CERTIPOL) }</CERTIPOLO>
         		<CERTIANNO>{ data($request/CERTIANN) }</CERTIANNO>
                <CERTISECO>{ data($request/CERTISEC) }</CERTISECO>
                <SUPLENUMO>0</SUPLENUMO> (:Esto debe proveerlo INSIS pero no viene ni en el AnnexID ni en el PolicyNo:)
                <CANTTEXTO>0</CANTTEXTO>
            </CAMPOS>
            )}
        </Response>
};
declare variable $request as element(Request) external;
declare variable $qbeRegisterAnnexRs as element() external;

xf:OV_1543_From_QbeRegisterAnnexRq($request,$qbeRegisterAnnexRs)
