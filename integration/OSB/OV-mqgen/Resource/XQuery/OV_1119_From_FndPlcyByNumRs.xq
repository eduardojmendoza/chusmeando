	xquery version "1.0" encoding "UTF-8";
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$QbeFindPolicyParticipantsRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeFindPolicyParticipantsRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1119_DatosApoderadoAcreedor.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";
declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1119_From_FndPlcyByNumRs/";

declare function xf:OV_1119_From_FndPlcyByNumRs($QbeFindPolicyParticipantsRs as element(), $LOVs as element(lov:LOVs))
    as element(Response) {
        <Response>
        	<Estado resultado="true" mensaje=""/>
         {
         let $participants := $QbeFindPolicyParticipantsRs/ns0:InsuranceConditions/ns0:Participants/ns0:Participant[ns0:ParticipantRole = 'CREDITOR' or ns0:ParticipantRole = 'LEGAL']
    	 return
         
         if ( not(empty($participants)) ) 
         then(
            <REGS>
    	    	{
    	    		for $Participant in $participants
			        return
					(
						<REG>
							<TIPODATO>
								{
				    				if (data($Participant/ns0:ParticipantRole) = 'CREDITOR') then (
										'AC'
									) else if (data($Participant/ns0:ParticipantRole) = 'LEGAL')  then (
										'AP'
				    				) else ()
								}
							</TIPODATO>
						
							{
			    				if (exists($Participant/ns0:Entity/ns0:PersonalData/ns0:PID)) then (
			        				<DOCUMTIP>{ data($LOVs/lov:LOV[@id = "DocTypeByName" and @ref = data($Participant/ns0:Entity/ns0:PersonalData/ns0:PID) ]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID) }</DOCUMTIP>,
			        				<DOCUMDAT>{ data(substring-after($Participant/ns0:Entity/ns0:PersonalData/ns0:PID,'-')) }</DOCUMDAT>
								) else (
									if (exists($Participant/ns0:Entity/ns0:CompanyData/ns0:CustomerID)) then (
										<DOCUMTIP>{ data($LOVs/lov:LOV[@id = "DocTypeByName" and @ref = data($Participant/ns0:Entity/ns0:CompanyData/ns0:CustomerID) ]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID) }</DOCUMTIP>,
			        					<DOCUMDAT>{ data(substring-after($Participant/ns0:Entity/ns0:CompanyData/ns0:CustomerID,'-')) }</DOCUMDAT>
									) else ()
								)
							}

							{
								if (exists($Participant/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Family)) then (
									<CLIENAP1>{ data(substring($Participant/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Family,1,20)) }</CLIENAP1>,
									<CLIENAP2>{ data(substring($Participant/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Family,21,40)) }</CLIENAP2>
								) else (
									<CLIENAP1>{ data(substring($Participant/ns0:Entity/ns0:CompanyData/ns0:Name,1,20)) }</CLIENAP1>,
									<CLIENAP2>{ data(substring($Participant/ns0:Entity/ns0:CompanyData/ns0:Name,21,40)) }</CLIENAP2>
								)
							}
							{
								if (exists($Participant/ns0:Entity/ns0:PersonalData)) then (
									<CLIENNOM>{ data($Participant/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Given) }</CLIENNOM>,
			        				<CLIENTIP>00</CLIENTIP>
								) else (
									<CLIENNOM>{' '}</CLIENNOM>,
			        				<CLIENTIP>15</CLIENTIP>
								)
							}
							<NACIMANN>{ data(year-from-date($Participant/ns0:Entity/ns0:PersonalData/ns0:BirthDate)) }</NACIMANN>
							<NACIMMES>{ data(month-from-date($Participant/ns0:Entity/ns0:PersonalData/ns0:BirthDate)) }</NACIMMES>
							<NACIMDIA>{ data(day-from-date($Participant/ns0:Entity/ns0:PersonalData/ns0:BirthDate)) }</NACIMDIA>

							<CLIENSEX>
			                	{
			                		let $sexo := data($Participant/ns0:Entity/ns0:PersonalData/ns0:Gender)
			                		return
			                			if ($sexo = '1') then ('M')
			                			else if ($sexo = '2') then ('F')
			                			else ()
			                	}
							</CLIENSEX>

							<CLIENEST>{ data($Participant/ns0:Entity/ns0:DataSource) }</CLIENEST>
							{
								let $address := $Participant/ns0:Addresses/ns0:Address[1]
                				return
                				(
									<DOMICCAL>{ data($address/ns0:AddressType) }</DOMICCAL>,
									<DOMICDOM>{ data($address/ns0:ResidentialAddress/ns0:StreetName) }</DOMICDOM>,
									<DOMICDNU>{ data($address/ns0:ResidentialAddress/ns0:StreetNo) }</DOMICDNU>,
									<DOMICESC></DOMICESC>,
									<DOMICPIS>{ data($address/ns0:ResidentialAddress/ns0:Floor) }</DOMICPIS>,
									<DOMICPTA>{ data($address/ns0:ResidentialAddress/ns0:Apartment) }</DOMICPTA>,
									<DOMICPOB>{ data($address/ns0:CityCode) }</DOMICPOB>,
									<DOMICPRE></DOMICPRE>,
									<DOMICTLF></DOMICTLF>,
									<PROVICOD>{ data($address/ns0:StateRegion) }</PROVICOD>,
									<PROVIDES>{ data($LOVs/lov:LOV[@id = "Province" and @ref=data($address/ns0:StateRegion)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</PROVIDES>,
			                        <PAISSCOD>{ data($LOVs/lov:LOV[@id = "LkpCountryByInsisCode" and @ref=data($address/ns0:CountryCode)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemID) }</PAISSCOD>,
									<PAISSDES>{ data($LOVs/lov:LOV[@id = "Country" and @ref=data($address/ns0:CountryCode)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</PAISSDES>,
									<CPACODPO>{ data($address/ns0:ZipCode) }</CPACODPO>
								)
							}
							{
								if (exists($Participant/ns0:Entity/ns0:CompanyData/ns0:CustomerID)) then (
			        				<CUIT>{ data(substring-after($Participant/ns0:Entity/ns0:CompanyData/ns0:CustomerID,'CUIT-')) }</CUIT>
								) else ()
							}
							<PROFECOD>{ data($Participant/ns0:Entity/ns0:IndustryCode) }</PROFECOD>
							<PROFEDES>{ data($LOVs/lov:LOV[@id = "IndustryCode" and @ref = data($Participant/ns0:Entity/ns0:IndustryCode)][1]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</PROFEDES>
							
							<DOMICSEC></DOMICSEC>
							<CUENTSEC></CUENTSEC>
							<EMPRESEC></EMPRESEC>
							<DOCUMSEC></DOCUMSEC>
						</REG>,
						<MEDIOSCONTACTOS>
							{
					            for $Contact in $Participant/ns0:Contacts/ns0:Contact
					        	return
									<MEDIOCONTACTO>
										<MEDCOCOD>{ data($Contact/ns0:Type) }</MEDCOCOD>
										<MEDCODAB>{ data($LOVs/lov:LOV[@id = "Contact"]/ns0:ListItems/ns0:ListItem[ns0:ItemID = data($Contact/ns0:Type)]/ns0:ItemDescription) }</MEDCODAB>
										<MEDCODAT>{ data($Contact/ns0:Details) }</MEDCODAT>
						        	</MEDIOCONTACTO>
					        }
					    </MEDIOSCONTACTOS>
	    			)
    			}
            </REGS>
	    ) else(
	    <REGS>
	    	<REG>    
			    <TIPODATO/>
			    <DOCUMTIP>0</DOCUMTIP>
			    <DOCUMDAT/>
			    <CLIENAP1/>
			    <CLIENAP2/>
			    <CLIENNOM/>
			    <CLIENTIP/>
			    <NACIMANN>0</NACIMANN>
			    <NACIMMES>0</NACIMMES>
			    <NACIMDIA>0</NACIMDIA>
			    <CLIENSEX/>
			    <CLIENEST/>
			    <DOMICCAL/>
			    <DOMICDOM/>
			    <DOMICDNU/>
			    <DOMICESC/>
			    <DOMICPIS/>
			    <DOMICPTA/>
			    <DOMICPOB/>
			    <DOMICPRE/>
			    <DOMICTLF/>
			    <PROVICOD>0</PROVICOD>
			    <PROVIDES/>
			    <PAISSCOD/>
			    <PAISSDES/>
			    <CPACODPO/>
			    <CUIT/>
			    <PROFECOD/>
			    <PROFEDES/>
			    <DOMICSEC/>
			    <CUENTSEC/>
			    <EMPRESEC>0</EMPRESEC>
			    <DOCUMSEC>0</DOCUMSEC>
			</REG>
	    	<MEDIOSCONTACTOS>
				<MEDIOCONTACTO>
					<MEDCOCOD/>
					<MEDCODAB/>
					<MEDCODAT/>
	        	</MEDIOCONTACTO>
	        </MEDIOSCONTACTOS>
	    </REGS>
	    )
        }</Response>
};

declare variable $QbeFindPolicyParticipantsRs as element() external;
declare variable $LOVs as element(lov:LOVs) external;

xf:OV_1119_From_FndPlcyByNumRs($QbeFindPolicyParticipantsRs, $LOVs)
