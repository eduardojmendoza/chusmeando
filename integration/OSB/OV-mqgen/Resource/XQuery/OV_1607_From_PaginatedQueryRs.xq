(:: pragma bea:local-element-parameter parameter="$paginatedQueryRs1" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1607_getHistorialPoliza.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1607_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1607_From_PaginatedQueryRs($paginatedQueryRs1 as element())
    as element(Response) {
       <Response>
       	{
           if ( number(data($paginatedQueryRs1/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
		            <POLIZAS>
			        	{
			            	let $item := $paginatedQueryRs1/ns0:RowSet/ns0:Row[1]
			                return
			                	(
			                	<POLIZAACTUAL>
			                		<RAMOPCOD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)) }</RAMOPCOD>
			                  		<POLIZANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,2)) }</POLIZANN>
			                  		<POLIZSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],11,6)) }</POLIZSEC>
			                  		<CERTIPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERTIPOL>
							  		<CERTIANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERTIANN>
							  		<CERTISEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERTISEC>
							  		<CLIDES>{ data($item/ns0:Column[@name = 'CLIDES']) }</CLIDES>
							  		<VIGENCIADESDE>{ local:normalizeDate(data($item/ns0:Column[@name = 'VIGENCIADESDE'])) }</VIGENCIADESDE>
									<VIGENCIAHASTA>{ local:normalizeDate(data($item/ns0:Column[@name = 'VIGENCIAHASTA'])) }</VIGENCIAHASTA>
							  		<ESTADO>{ data($item/ns0:Column[@name = 'ESTADO']) }</ESTADO>
							  		<TIPO>{ data($item/ns0:Column[@name = 'TIPO']) }</TIPO>
							  	</POLIZAACTUAL>
								)
						}
(:						<CANTLIN>{count($paginatedQueryRs1/ns0:RowSet/ns0:Row/* )}</CANTLIN> :)
				        <HISTORIA>
				           	{
				            	for $item at $row in $paginatedQueryRs1/ns0:RowSet/ns0:Row
					            return
									(
									 if (number($row) > 1) then (
										<POLIZA>
											<RAMOPCOD>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],5,4)) }</RAMOPCOD>
							                <POLIZANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],9,2)) }</POLIZANN>
							                <POLIZSEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],11,6)) }</POLIZSEC>
							                <CERTIPOL>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],17,4)) }</CERTIPOL>
											<CERTIANN>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],21,4)) }</CERTIANN>
											<CERTISEC>{ data(substring($item/ns0:Column[@name = 'POLICY_NO'],25,6)) }</CERTISEC>
									  		<VIGENCIADESDE>{ local:normalizeDate(data($item/ns0:Column[@name = 'VIGENCIADESDE'])) }</VIGENCIADESDE>
											<VIGENCIAHASTA>{ local:normalizeDate(data($item/ns0:Column[@name = 'VIGENCIAHASTA'])) }</VIGENCIAHASTA>
											<ESTADO>{ data($item/ns0:Column[@name = 'ESTADO']) }</ESTADO>
											<AGENTCLA>{ data(substring($item/ns0:Column[@name = 'AGENTCOD'], 1, 2)) }</AGENTCLA>
											<AGENTCOD>{ data(substring($item/ns0:Column[@name = 'AGENTCOD'], 3, 4)) }</AGENTCOD>
											<MARCAPROPUESTA>{ data($item/ns0:Column[@name = 'MARCAPROPUESTA']) }</MARCAPROPUESTA>
										</POLIZA>
										) else ()
									)
							}
						</HISTORIA>
		            </POLIZAS>
		        )
		       }
        </Response>
};

declare function local:normalizeDate($value as xs:string?) as xs:string?
{
	if ( exists($value) and $value != '' and contains($value, 'T')) then (
		fn-bea:dateTime-to-string-with-format('dd/MM/yyyy', xs:dateTime($value))
	) else (
		fn-bea:date-to-string-with-format('dd/MM/yyyy', xs:date($value))
	)
};

declare variable $paginatedQueryRs1 as element() external;

xf:OV_1607_From_PaginatedQueryRs($paginatedQueryRs1)
