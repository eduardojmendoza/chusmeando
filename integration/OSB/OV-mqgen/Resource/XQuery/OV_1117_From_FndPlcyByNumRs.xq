xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$QbeFindPolicyCoversRs" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeFindPolicyCoversRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$LOVs" element="lov:LOVs" location="../../../INSIS-OV/Resource/XSD/LOVHelper.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1117_Coberturas.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1117_From_FndPlcyByNumRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace lov = "http://xml.qbe.com/INSIS-OV/LOVHelper";

declare function xf:OV_1117_From_FndPlcyByNumRs($QbeFindPolicyCoversRs as element(), $LOVs as element(),$mapPlanPackage as element(*))
    as element(Response) {
         <Response>
        	<Estado resultado="true" mensaje=""/>
        	<CAMPOS>
	            <GRUPOAFI></GRUPOAFI>(: Qbe lo pide vacio, vacio lo tendra. Referencia BUG: 67 :)
				{

					let $cover := $QbeFindPolicyCoversRs/ns0:Insureds/ns0:Insured[1]/ns0:CoverPackages/ns0:CoverPackage[1]
					let $PACKAGECODE := data($cover/ns0:PackageID)
					let $PLANNCOD := data($mapPlanPackage/Map[@MainPackage = 'S' and @PackageCode = $PACKAGECODE]/@PLANNCOD)
					let $FRANQCOD := data($mapPlanPackage/Map[@MainPackage = 'S' and @PackageCode = $PACKAGECODE]/@FRANQCOD)
					return (

						(: <PLANNCOD>{ data($cover/ns0:PackageID }</PLANNCOD>, :)
						<PLANNCOD>{ $PLANNCOD }</PLANNCOD>,
						<PLANNDAB>{ data($cover/ns0:PackageName) }</PLANNDAB>,
						(: <FRANQCOD>{ data($LOVs/lov:LOV[@id = "FranchiseByPackage"]/ns0:ListItems/ns0:ListItem/ns0:ItemID) }</FRANQCOD>, :)
						<FRANQCOD>{ $FRANQCOD }</FRANQCOD>,
						<FRANQDES>{ data($LOVs/lov:LOV[@id = "FranchiseByPackage"]/ns0:ListItems/ns0:ListItem/ns0:ItemDescription) }</FRANQDES>
						(: <FRANQDES>{ data($LOVs/lov:LOV[@id = "FranchiseByPackage" and @ref = $FRANQCOD]/ns0:ListItems/ns0:ListItem[1]/ns0:ItemID) }</FRANQDES> :)
					)
				}
				<SUMALBA>{ fn-bea:format-number(data($QbeFindPolicyCoversRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredAmount) * 100, '000') }</SUMALBA>
                <SUMAASEG>{ fn-bea:format-number(data($QbeFindPolicyCoversRs/ns0:Insureds/ns0:Insured[1]/ns0:InsuredAmount) * 100, '000') }</SUMAASEG>
	            <PRECIOMS>{ data($LOVs/lov:LOV[@id = "GetPolicyPrice"]/ns0:ListItems/ns0:ListItem/ns0:ItemID) }</PRECIOMS>
	            <SWRIECOB>U</SWRIECOB>
	            <COBERTURAS>
	            {
	                for $Cover in $QbeFindPolicyCoversRs/ns0:Insureds/ns0:Insured[1]/ns0:CoverPackages/ns0:CoverPackage[1]/ns0:Covers/ns0:Cover
	                return
	                    <COBERTURA>
				            <COBERORD>0</COBERORD> (: No mapeado :)
				            <COBERCOD>{data($LOVs/lov:LOV[@id = "CoverByInsisCode"]/ns0:ListItems/ns0:ListItem[ ns0:ItemDescription = data($Cover/ns0:CoverDescription)]/ns0:ItemID)}</COBERCOD>
				            <COBERDAB>{ data($Cover/ns0:CoverDescription) }</COBERDAB>
							<CAPITIMP>{ fn-bea:format-number(data($Cover/ns0:InsuredAmount) * 100, '000') }</CAPITIMP>
                            <SUMASEG>{ fn-bea:format-number(data($Cover/ns0:InsuredAmount) * 100, '000') }</SUMASEG>
							<RIESGTIP>{ '2' }</RIESGTIP>
				            <SEWRIEDES>S</SEWRIEDES>
	            		</COBERTURA>
	            }
	            </COBERTURAS>
            </CAMPOS>
        </Response>
};

declare variable $QbeFindPolicyCoversRs as element() external;
declare variable $lovFranchiseByPackage as element() external;
declare variable $mapPlanPackage as element(*) external;

xf:OV_1117_From_FndPlcyByNumRs($QbeFindPolicyCoversRs, $lovFranchiseByPackage, $mapPlanPackage)
