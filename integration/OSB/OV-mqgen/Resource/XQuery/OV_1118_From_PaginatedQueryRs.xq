xquery version "1.0" encoding "UTF-8";
(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:PaginatedQueryRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_1118_MediosdePago.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-mqgen/Resource/XQuery/OV_1118_From_PaginatedQueryRs/";
declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";

declare function xf:OV_1118_From_PaginatedQueryRs($resp as element())
    as element(Response) {
        <Response>
        {
                if ( number(data($resp/ns0:TotalRowCount)) <= 0 ) then (
                    <Estado resultado = "false" mensaje = "No se han encontrado resultados para la póliza buscada"/>
                ) else (
                    <Estado resultado = "true" mensaje = ""/>,
                    <CAMPOS>
                    {
		            	let $item := $resp/ns0:RowSet/ns0:Row[1]
		                return
							(<OPERPTE>{ data($item/ns0:Column[@name = 'OPER_PTE']) }</OPERPTE>,
							<CLIENSEC>{ data($item/ns0:Column[@name = 'CLIENSEC']) }</CLIENSEC>,
							<CLIENDES>{ data($item/ns0:Column[@name = 'CLIENDES']) }</CLIENDES>,
							<DOMICSEC>{ data($item/ns0:Column[@name = 'DOMICSEC']) }</DOMICSEC>,
							<CUENTSEC>{ data($item/ns0:Column[@name = 'CUENTSEC']) }</CUENTSEC>,
							<SITUCPOL>{ data($item/ns0:Column[@name = 'SITUCPOL']) }</SITUCPOL>,
							<ACREEDOR>{ data($item/ns0:Column[@name = 'ACREEDOR']) }</ACREEDOR>,
							<FECINI>{  data($item/ns0:Column[@name = 'FECINI'])  }</FECINI>,
							<FECULTRE>{ data($item/ns0:Column[@name = 'FECULTRE']) }</FECULTRE>,
							<COBROCOD>{ data($item/ns0:Column[@name = 'COBROCOD']) }</COBROCOD>,
							<COBRODAB>{ data($item/ns0:Column[@name = 'COBRODAB']) }</COBRODAB>,
							<COBROTIP>{ data($item/ns0:Column[@name = 'COBROTIP']) }</COBROTIP>,
							<COBRODES>{ data($item/ns0:Column[@name = 'COBRODES']) }</COBRODES>,
							<CUENTNUM>{ data($item/ns0:Column[@name = 'CUENTNUM']) }</CUENTNUM>,
							<VENCIANN>{ '0' }</VENCIANN>,
							<VENCIMES>{ '0' }</VENCIMES>,
							<CAMPACOD>{ data($item/ns0:Column[@name = 'CAMPACOD']) }</CAMPACOD>,
							<CAMPADES>{ data($item/ns0:Column[@name = 'CAMPADES']) }</CAMPADES>,
							<GRUPOASE>{ data($item/ns0:Column[@name = 'GRUPOASE']) }</GRUPOASE>,
							<FSOL1AMD>{  data($item/ns0:Column[@name = 'FSOL1AMD'])  }</FSOL1AMD>,
							<EFESUAMD>{
								let $fecha1 := data($item/ns0:Column[@name = 'EFESUAMD'])
                        			return
                        		if (exists($item/ns0:Column[@name = 'EFESUAMD']) and $fecha1 != '') then (fn-bea:date-to-string-with-format('yyyyMMdd', $fecha1)) else ()
							 }</EFESUAMD>,
							<VENFIAMD>{
								let $fecha2 := data($item/ns0:Column[@name = 'VENFIAMD'])
                        			return
                        		if (exists($item/ns0:Column[@name = 'VENFIAMD']) and $fecha2 != '') then (fn-bea:date-to-string-with-format('yyyyMMdd', $fecha2)) else ()
							 }</VENFIAMD>,
							<COASETIP>{ data($item/ns0:Column[@name = 'COASETIP']) }</COASETIP>,
							<EXESEPOR>{ data($item/ns0:Column[@name = 'EXESEPOR']) }</EXESEPOR>,
							<RECARPOR>{ data($item/ns0:Column[@name = 'RECARPOR']) }</RECARPOR>,
							<VIENEDMS>{ data($item/ns0:Column[@name = 'VIENEDMS']) }</VIENEDMS>,
							<AGENTCLA>{ data($item/ns0:Column[@name = 'AGENTCLA']) }</AGENTCLA>,
							<AGENTCOD>{ data(substring-after($item/ns0:Column[@name = 'AGENTCOD'],'PR')) }</AGENTCOD>,
							<VENINAMD>{
								let $fecha3 := data($item/ns0:Column[@name = 'VENINAMD'])
                        			return
                        		if ( exists($item/ns0:Column[@name = 'VENINAMD']) and $fecha3 != '') then (fn-bea:date-to-string-with-format('yyyyMMdd', $fecha3)) else ()
							 }</VENINAMD>,
							<BANCOCOD>{ data($item/ns0:Column[@name = 'BANCOCOD']) }</BANCOCOD>,
							<SUCURCOD>{ data($item/ns0:Column[@name = 'SUCURCOD']) }</SUCURCOD>,
							<CLIENIVAPOL>{ data($item/ns0:Column[@name = 'CLIENIVAPOL']) }</CLIENIVAPOL>)
					}
					</CAMPOS>
				)
            }
        </Response>
};

declare variable $resp as element() external;

xf:OV_1118_From_PaginatedQueryRs($resp)