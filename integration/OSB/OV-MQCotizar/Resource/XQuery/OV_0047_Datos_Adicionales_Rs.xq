(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_0047_Datos_Adicionales.xsd" ::)
(:: pragma  parameter="$nroCotRs" type="anyType" ::)
(:: pragma  parameter="$portalComercRs" type="anyType" ::)
(:: pragma  parameter="$sumAsegRs" type="anyType" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_0047_Datos_Adicionales.xsd" ::)

declare namespace xf = "http://tempuri.org/OV-MQCotizar/Resource/XQuery/OV_0047_Datos_Adicionales_Rs/";

declare function xf:OV_0047_Datos_Adicionales_Rs($req as element(Request),
											     $nroCotRs as element(*),
											     $portalComercRs as element(*)?,
											     $sumAsegRs as element(*))
    as element(Response) {
		<Response>
			<COT_NRO>{ data($nroCotRs/ROW/NROCOT) }</COT_NRO>
			{
				if (data($req/PORTAL) = 'LBA_PRODUCTORES')
				then (
					<then>
						<CAMPA_COD>{ fn-bea:pad-left(data($req/CAMPACOD), 4, '0') }</CAMPA_COD>
						<AGE_COD>{ fn-bea:pad-left(data($req/AGECOD), 4, '0') }</AGE_COD>
						<AGE_CLA>
							{
								if (data($req/AGECLA) != '') then (
									fn-bea:pad-right(data($req/AGECLA), 2)
								) else (
									'PR'
								)
							}
						</AGE_CLA>
						<CAMPA_TEL/>
						<CAMPA_TEL_FORM/>
					</then>
				)/*
				else (
					<else>
						<CAMPA_COD>{ fn-bea:pad-left(data($portalComercRs/ROW/CAMPACOD), 4, '0') }</CAMPA_COD>
						<AGE_COD>{ fn-bea:pad-left(data($portalComercRs/ROW/AGECOD), 4, '0') }</AGE_COD>
						<AGE_CLA>{ fn-bea:pad-right(data($portalComercRs/ROW/AGECLA), 2) }</AGE_CLA>
						{
							let $tel := data($portalComercRs/ROW/TELEFONO)
							return (
								<CAMPA_TEL>{ $tel }</CAMPA_TEL>,
								<CAMPA_TEL_FORM>
									{
									    if ( fn-bea:trim( substring( $tel, 1, 4 ) ) = "0800" ) then (
									      concat( fn-bea:trim( substring( $tel, 1, 4 ) ), "-", substring( $tel, 5, 3 ), "-", substring( $tel, 8, string-length( $tel ) ) )
									    ) else (
									      $tel
									    )
									}
								</CAMPA_TEL_FORM>
							)
						}
					</else>
				)/*
			}
			{
				let $sumAseg := fn-bea:pad-left(replace(data($sumAsegRs/COMBO/OPTION[@value = data($req/EFECTANN)]/@sumaseg), ',', ''), 11, '0')
				return (
					<SUMASEG>{ $sumAseg }</SUMASEG>,
					<SUMALBA>{ $sumAseg }</SUMALBA>
				)
			}
			<SILUNETA>S</SILUNETA>
			<TIENEHIJOS>{ data($req/TIENEHIJOS) }</TIENEHIJOS>
		</Response>
};

declare variable $req as element(Request) external;
declare variable $nroCotRs as element(*) external;
declare variable $portalComercRs as element(*)? external;
declare variable $sumAsegRs as element(*) external;

xf:OV_0047_Datos_Adicionales_Rs($req,
    $nroCotRs,
    $portalComercRs,
    $sumAsegRs)
