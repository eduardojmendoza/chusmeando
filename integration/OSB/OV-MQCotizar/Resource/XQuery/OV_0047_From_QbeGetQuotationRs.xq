(:: pragma bea:local-element-parameter parameter="$resp" type="ns0:InsuranceSvc/ns0:InsuranceSvcRs/ns0:QbeGetQuotationRs" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)
(:: pragma bea:local-element-parameter parameter="$req" type="Request" location="../XSD/OV_0047_A_Insis.xsd" ::)
(:: pragma parameter="$mapPlanPackage" type="xs:anyType" ::)
(:: pragma parameter="$conHijos" type="xs:boolean" ::)
(:: pragma bea:mfl-element-return type="Response@" location="../MFL/OV_0047_Rs.mfl" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQCotizar/Resource/XQuery/OV_0047_From_QbeGetQuotationRs/";

declare function xf:OV_0047_From_QbeGetQuotationRs($resp as element(), $req as element(), $mapPlanPackage as element(*))
    as element() {
	    <Response>
	    	<ESTADO>OK</ESTADO>
	        <PLANES>
	        	{
					for $MetaPackage in $resp/ns0:Policies/ns0:Policy
					let $map := $mapPlanPackage/Map[@MainPackage = 'S' and @PackageCode = data($MetaPackage/ns0:Packages/ns0:Package[@mainPackage = 'S']/ns0:PackageCode)]
					let $datosPlan := local:GetPlan($MetaPackage/ns0:Packages, $mapPlanPackage)
					let $orden := data($map/@ORDEN)
					order by $orden
					return
		                <PLAN>
		                    <CON_HIJOS>{ $datosPlan/* }</CON_HIJOS>
		                    <SIN_HIJOS>{ $datosPlan/* }</SIN_HIJOS>
		                </PLAN>
	        	}
	        </PLANES>
	        <TIEMPO-PROCESO>0</TIEMPO-PROCESO>
			<CODIZONA>0</CODIZONA>
	        {
	        	if (count($resp/ns0:Policies/ns0:Policy) = 1) then (
	        		let $Packages := $resp/ns0:Policies/ns0:Policy/ns0:Packages
		    		let $Covers :=
		    			if ($Packages/ns0:Package[@mainPackage = 'S']/@allowsAdditionalPackages = 'N') then (
		    				$Packages/ns0:Package[@mainPackage = 'S']/ns0:Covers/ns0:Cover
		    			) else (
		    				$Packages/ns0:Package/ns0:Covers/ns0:Cover
		    			)
					let $CoversLenght := count($Covers)
		    		return (
		        		<COBERTURAS>
		        			{
				        		for $Cover in $Covers
				        		order by $Cover/ns0:CoverOrder
			        			return
								    <COBERTURA>
								        <COBERCOD>{ data($Cover/ns0:CoverCode) }</COBERCOD>
								        <COBERORD>{ data($Cover/ns0:CoverOrder) }</COBERORD>
								        <CAPITASG>{ local:adaptDecimal($Cover/ns0:InsuredValue, 11) }</CAPITASG>
								        <CAPITIMP>{ local:adaptDecimal($Cover/ns0:InsuredValue, 11) }</CAPITIMP>
								    </COBERTURA>
		        			}
				        </COBERTURAS>,
						<MAX-DECA>{ if ($CoversLenght <= 30) then ( $CoversLenght ) else ( 30 ) }</MAX-DECA>
					)
				) else (
					<COBERTURAS />,
					<MAX-DECA>0</MAX-DECA>
				)
	        }
			<SINCONTROL>{ local:GetSiNo($req/SINCONTROL) }</SINCONTROL>
			<LUNDISPO>S</LUNDISPO>
			<PREMIER>N</PREMIER>
	        <DESCRIPCIONES>
	        	{
	        		for $MainPackage at $i in $resp/ns0:Policies/ns0:Policy/ns0:Packages/ns0:Package[@mainPackage = 'S']
	        		let $allowsAdditionals := $MainPackage/@allowsAdditionalPackages
					let $map := $mapPlanPackage/Map[@MainPackage = 'S' and @PackageCode = data($MainPackage/ns0:PackageCode)]
	        		let $orden := data($map/@ORDEN)
	        		order by $orden
	        		return
		                <DESCRIPCION>
		                    <PLANNDES>{ data($MainPackage/ns0:PackageDescription) }</PLANNDES>
		                    <PLANDES>{ normalize-space(data($map/@PLANDES)) }</PLANDES>
		                    <FRANQDES>{ data($map/@FRANQDES) }</FRANQDES>
		                    {
		                    	if ($allowsAdditionals = 'N') then (
				                    <LUNPAR>N</LUNPAR>,
				                    <GRANIZO>N</GRANIZO>,
				                    <ROBOCON>N</ROBOCON>
		                    	) else (
				                    <LUNPAR>{ local:GetSiNo($MainPackage/ns0:CoverWindshield) }</LUNPAR>,
				                    <GRANIZO>{ local:GetSiNo($MainPackage/ns0:CoverHail) }</GRANIZO>,
				                    <ROBOCON>{ local:GetSiNo($MainPackage/ns0:CoverTheft) }</ROBOCON>
		                    	)
		                    }
		                    <ESRC>{ data($map/@ESRC) }</ESRC>
		                    <ORDEN>{ fn-bea:pad-left( if ($orden != '') then ($orden) else (xs:string($i)), 4, '0') }</ORDEN>
		                </DESCRIPCION>
	        	}
	        </DESCRIPCIONES>
	    </Response>
};

declare variable $resp as element() external;
declare variable $req as element() external;
declare variable $mapPlanPackage as element(*) external;

declare function local:GetPlan($Packages as element(ns0:Packages), $mapPlanPackage as element(*))
    as element() {
    	let $MainPackage := $Packages/ns0:Package[@mainPackage = 'S']
    	let $PackageColl :=
			if ($MainPackage/@allowsAdditionalPackages = 'N') then (
				$MainPackage
			) else (
				$Packages/ns0:Package
			)
		let $map := $mapPlanPackage/Map[@MainPackage = 'S' and @PackageCode = data($MainPackage/ns0:PackageCode)]
        return
	        <DATOS>
	            <PLANNCOD>{ fn-bea:pad-left(data($map/@PLANNCOD), 3, '0') }</PLANNCOD>
	            <FRANQCOD>{ fn-bea:pad-left(data($map/@FRANQCOD), 2, '0') }</FRANQCOD>
	            <PLANDES>{ normalize-space(data($MainPackage/ns0:PackageDescription)) }</PLANDES>
	            <PRIMA>{ local:adaptDecimal(sum($PackageColl/ns0:PRIMA), 11) }</PRIMA>
	            <RECARGOS>{ local:adaptDecimal(sum($PackageColl/ns0:RECARGOS), 11) }</RECARGOS>
	            <IVAIMPOR>{ local:adaptDecimal(sum($PackageColl/ns0:IVAIMPOR), 11) }</IVAIMPOR>
	            <IVAIMPOA>{ local:adaptDecimal(sum($PackageColl/ns0:IVAIMPOA), 11) }</IVAIMPOA>
	            <IVARETEN>{ local:adaptDecimal(sum($PackageColl/ns0:IVARETEN), 11) }</IVARETEN>
	            <DEREMI>{ local:adaptDecimal(sum($PackageColl/ns0:DEREMI), 11) }</DEREMI>
	            <SELLADO>{ local:adaptDecimal(sum($PackageColl/ns0:SELLADO), 11) }</SELLADO>
	            <INGBRU>{ local:adaptDecimal(sum($PackageColl/ns0:INGBRU), 11) }</INGBRU>
	            <IMPUES>{ local:adaptDecimal(sum($PackageColl/ns0:IMPUES), 11) }</IMPUES>
	            <PRECIO>{ local:adaptDecimal(sum($PackageColl/ns0:PRECIO), 11) }</PRECIO>
	        </DATOS>
};

declare function local:GetSiNo($field as element(*)?)
    as xs:string {
    	if (data($field) != '') then (
    		data($field)
    	) else (
    		'N'
    	)
};

declare function local:adaptDecimal($in as xs:double?, $needLength as xs:integer) as xs:string
{
	if(string(number($in)) != 'NaN') then (
		fn-bea:format-number($in, fn-bea:pad-left('0.00', $needLength + 1, '#'))
	) else (
		'0.00'
	)
};

xf:OV_0047_From_QbeGetQuotationRs($resp, $req, $mapPlanPackage)
