(:: pragma bea:local-element-parameter parameter="$resp" type="anyType" ::)
(:: pragma bea:mfl-element-return type="Response@" location="../MFL/OV_0047_Rs.mfl" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQCotizar/Resource/XQuery/OV_0047_From_QbeGetQuotationRs/";

declare function xf:OV_0047_From_ErrorHandlerRs($resp as element())
    as element() {
	    <Response>
	    	<ESTADO>ER</ESTADO>
	        <PLANES>
	        	<PLAN>
		             <PLAN_CON_HIJOS> 
		              {
		                for $i in 1 to 20
		                return  
		                    <CON_HIJOS/>
		            }
		            </PLAN_CON_HIJOS>
		            <PLAN_SIN_HIJOS>
				      {
		                for $i in 1 to 20
		                return 
                    <SIN_HIJOS/>
            		}
            	</PLAN_SIN_HIJOS>
		        </PLAN>    	
	        </PLANES>
	        <TIEMPO-PROCESO>999999</TIEMPO-PROCESO>
	        <COBERTURAS>
	        {
	            for $i in 1 to 30
	            return 
	                <COBERTURA/>
	        }
	    	</COBERTURAS>
		    <DESCRIPCIONES>
	        {
	            for $i in 1 to 20
	            return 
	                <DESCRIPCION>
	                	<PLANNDES>{
	                	
	                	
	                	
	                	$resp/*
	                	}</PLANNDES>
	                </DESCRIPCION>
	        }
    </DESCRIPCIONES>
	    </Response>
};

declare variable $resp as element() external; 

xf:OV_0047_From_ErrorHandlerRs($resp)
