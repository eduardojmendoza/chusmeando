(:: pragma bea:global-element-parameter parameter="$req" element="Request" location="../XSD/OV_0047_A_Insis.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$service1230" element="Response" location="../XSD/Service_1230.xsd" ::)
(:: pragma parameter="$mapPlanPackage" type="xs:anyType" ::)
(:: pragma bea:local-element-return type="ns0:InsuranceSvc/ns0:InsuranceSvcRq/ns0:QbeGetQuotationRq" location="../../../INSIS-OV/Resource/XSD/Insis_Composition.xsd" ::)

declare namespace ns0 = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/";
declare namespace xf = "http://tempuri.org/OV-MQCotizar/Resource/XQuery/OV_0047_To_QbeGetQuotationRq/";

declare function xf:OV_0047_To_QbeGetQuotationRq($req as element(Request), $service1230 as element(Response), $mapPlanPackage as element(*))
    as element() {
        <ns0:QbeGetQuotationRq>
            <ns0:PaymentInstrument>{ if (data($req/COBROTIP) != '') then (data($req/COBROTIP)) else ('VI') }</ns0:PaymentInstrument>
            <ns0:IssueDate>{ local:GetDate($req/EMISIANN, $req/EMISIMES, $req/EMISIDIA, true()) }</ns0:IssueDate>
            <ns0:EffectiveDate>{ local:GetDate($req/EFECTANN_1, $req/EFECTMES_1, $req/EFECTDIA_1, true()) }</ns0:EffectiveDate>
            <ns0:AgentNo>{ concat( if (data($req/AGECLA) != '') then (fn-bea:pad-left($req/AGECLA, 2, '0')) else ('PR'), fn-bea:pad-left($req/AGECOD, 4, '0')) }</ns0:AgentNo>
            {
            	if (matches(data($req/DATOSPLAN), '^[0 ]*$')) then (
            		<ns0:PackageCode /> (: Quotates All Packages :)
            	) else (
	            	let $PLANNCOD := xs:long(substring(data($req/DATOSPLAN), 1, 3))
	            	let $FRANQCOD := xs:long(substring(data($req/DATOSPLAN), 4, 2))
	            	return
			            <ns0:PackageCode>{ data($mapPlanPackage/Map[@MainPackage = 'S' and @PLANNCOD = $PLANNCOD and @FRANQCOD = $FRANQCOD]/@PackageCode) }</ns0:PackageCode>
            	)
            }
            <ns0:CoverWindshield>{ local:GetSiNo($req/LUNETA) }</ns0:CoverWindshield>
            <ns0:CoverHail>{ local:GetSiNo($req/GRANIZO) }</ns0:CoverHail>
            <ns0:CoverTheft>{ local:GetSiNo($req/ROBOCONT) }</ns0:CoverTheft>
            <ns0:CampaignID>{ xs:long(data($req/LK_TAB_CAMP/CAMPA[1]/CAMPACOD)) }</ns0:CampaignID>
            <ns0:QuotationNo>{ data($req/COTNRO) }</ns0:QuotationNo>
            <ns0:DectivateCustomControls>{ local:GetSiNo($req/SINCONTROL) }</ns0:DectivateCustomControls>
            <ns0:Client>
                <ns0:BirthDate>{ local:GetDate($req/NACIMANN, $req/NACIMMES, $req/NACIMDIA, false()) }</ns0:BirthDate>
                <ns0:Gender>
                	{
                		let $sexo := upper-case(data($req/SEXO))
                		return
                			if ($sexo = 'M') then ( 1 )
                			else if ($sexo = 'F') then ( 2 )
                			else ( 0 )
                	}
                </ns0:Gender>
                <ns0:MaritalStatus>{ data($req/ESTADO) }</ns0:MaritalStatus>
                <ns0:IVAStatus>{ data($req/IVA) }</ns0:IVAStatus>
                { 
                	if (data($req/IVA) != '3') then
    					if (exists(data($req/IBB)) and data($req/IBB) != '' and data($req/IBB) != '0') then
    						<ns0:IBBStatus>{ data($req/IBB) }</ns0:IBBStatus>
    					else <ns0:IBBStatus>3</ns0:IBBStatus>
    				else (<ns0:IBBStatus></ns0:IBBStatus>)
                }
                <ns0:EntityType>
                	{
                		let $tipo := data($req/CLIENTIP)
                		return
                			if ($tipo = '' or $tipo = '00') then ( 1 )
                			else if ($tipo = '15' or $tipo = '45' or $tipo = '55') then ( 2 )
                			else ( 0 )
                	}
                </ns0:EntityType>
            </ns0:Client>
            <ns0:Vehicle>
                <ns0:InfoAutoCode>{ data($service1230/CAMPOS/AUPRONUM) }</ns0:InfoAutoCode>
                <ns0:ProdYear>{ data($req/EFECTANN) }</ns0:ProdYear>
                <ns0:InsuredValue>{ if ( data($req/SUMAASEG) != '' ) then ( data($req/SUMAASEG) ) else ( xs:decimal('0') ) }</ns0:InsuredValue>
                <ns0:CarUsage>{ xs:long(if ( data($req/AUUSOCOD) != '' ) then ( data($req/AUUSOCOD) ) else ( '1' )) }</ns0:CarUsage>
                <ns0:KmPerYear>{ data($req/KMSRNGCOD) }</ns0:KmPerYear>
                <ns0:Garage>{ local:GetSiNo($req/SIGARAGE) }</ns0:Garage>
                <ns0:NumberOfClaims>{ if ( data($req/SINIESTROS) != '' ) then ( data($req/SINIESTROS) ) else ( 0 ) }</ns0:NumberOfClaims>
                <ns0:HasGasEquipment>{ local:GetSiNo($req/GAS) }</ns0:HasGasEquipment>
                <ns0:Zone />
                <ns0:Region>{ data($req/PROVI) }</ns0:Region>
                <ns0:ZIP>{ data($req/LOCALIDADCOD) }</ns0:ZIP>
                <ns0:IsNew>{ local:GetSiNo($req/ESCERO) }</ns0:IsNew>
            </ns0:Vehicle>
            {
                let $HIJOS := $req/HIJOS
                return
                	if ( count($HIJOS/HIJO) > 0 ) then (
	                    <ns0:Drivers?> (: el ? es para que si no se genera ningun Driver el tag Drivers no se genera tampoco :)
	                        {
	                        	for $HIJO in $HIJOS/HIJO
	                        	let $nacim := data($HIJO/NACIMHIJO)
	                        	let $sexo := fn-bea:trim(data($HIJO/SEXOHIJO))
	                        	let $estado := fn-bea:trim(data($HIJO/ESTADOHIJO))
	                            return
	                            	if (not(matches($nacim, '^[0]*$')) or not(matches($sexo, '^[0 ]*$')) or not(matches($estado, '^[0 ]*$'))) then (
		                                <ns0:Driver>
		                                    <ns0:BirthDate>{ fn-bea:date-from-string-with-format("ddMMyyyy", $nacim) }</ns0:BirthDate>
		                                    <ns0:Gender>
							                	{
						                			if ($sexo = 'M') then ( 1 )
						                			else if ($sexo = 'F') then ( 2 )
						                			else ( 0 )
							                	}
											</ns0:Gender>
		                                    <ns0:MaritalStatus>{ $estado }</ns0:MaritalStatus>
		                                </ns0:Driver>
	                                ) else ()
	                        }
	                    </ns0:Drivers>
                	) else ()
            }
            {
                let $ACCESORIOS := $req/ACCESORIOS
                return
                	if ( count($ACCESORIOS/ACCESORIO) > 0 ) then (
	                    <ns0:Accessories?> (: el ? es para que si no se genera ningun Accessory el tag Accessories no se genera tampoco :)
	                        { (: agrego accesorio GNC :)
	                         if (data($req/GAS)='S') then (
	                            
	                            if ( not(matches($ACCESORIOS/ACCESORIO[CODIGOACC = 5]/DESCRIPCIONACC, '^[0 ]*$')) ) then (
		                                <ns0:Accessory>
		                                    <ns0:Code>6</ns0:Code>
		                                    <ns0:SubCode>1</ns0:SubCode>
		                                    <ns0:Description>{ 'Equipo de GNC' }</ns0:Description>
		                                    <ns0:InsuredValue>{ data($ACCESORIOS/ACCESORIO[CODIGOACC = 5]/PRECIOACC)}</ns0:InsuredValue>
		                                </ns0:Accessory>
	                                ) else ()
	                            
	                            )else ()
	                        }
	                         { (: agrego los demas accesorios que no son GNC:)
	                            for $ACCESORIO in $ACCESORIOS/ACCESORIO[CODIGOACC != 5]
	                        	let $code := data($ACCESORIO/CODIGOACC)
	                        	let $desc := fn-bea:trim(data($ACCESORIO/DESCRIPCIONACC))
	                        	let $precio := data($ACCESORIO/PRECIOACC)
	                            return
	                            	if ($code != '0' or not(matches($desc, '^[0 ]*$')) or $precio != '0') then (
		                                <ns0:Accessory>
		                                    <ns0:Code>5</ns0:Code>
		                                    <ns0:SubCode>{ $code }</ns0:SubCode>
		                                    <ns0:Description>{ $desc }</ns0:Description>
		                                    <ns0:InsuredValue>{ $precio }</ns0:InsuredValue>
		                                </ns0:Accessory>
	                                ) else ()
	                        }
	                    </ns0:Accessories>
                	) else ()

            }
        </ns0:QbeGetQuotationRq>
};

declare variable $req as element(Request) external;
declare variable $service1230 as element(Response) external;
declare variable $mapPlanPackage as element(*) external;

declare function local:GetSiNo($field as element(*)?)
    as xs:string {
    	if (data($field) != '') then (
    		data($field)
    	) else (
    		'N'
    	)
};

declare function local:GetDate($anio as element(*)?, $mes as element(*)?, $dia as element(*)?, $defaultCurrentDate as xs:boolean)
    as xs:date? {
    	let $yyyy := fn-bea:pad-left(data($anio), 4, '0')
    	let $mm := fn-bea:pad-left(data($mes), 2, '0')
    	let $dd := fn-bea:pad-left(data($dia), 2, '0')
    	return
	        if ($yyyy != '' and $mm != '' and $dd != '') then (
	        	fn-bea:date-from-string-with-format("yyyyMMdd", concat($yyyy, $mm, $dd))
	        ) else if ($defaultCurrentDate) then (
	        	fn-bea:date-from-string-with-format("yyyy-MM-dd", substring(xs:string(current-date()), 1, 10))  (: Tomamos solo la fecha y descartamos el timezone :)
	        ) else (
	        )
};

xf:OV_0047_To_QbeGetQuotationRq($req, $service1230, $mapPlanPackage)
