xquery version "1.0" encoding "UTF-8";
(:: pragma  parameter="$anyType1" type="xs:anyType" ::)
(:: pragma  type="xs:anyType" ::)

declare namespace xf = "http://tempuri.org/OV-OSB-Commons/Resource/XQuery/lalala/";
declare variable $xf as xs:string := "http://tempuri.org/OV-OSB-Commons/Resource/XQuery/lalala/";

(: Retorna un conjunto de elementos del tree que matcheen con path. 
  El path no debe contener el nodo root del xml. El Path no soporta ni necesita namespaces.
 :)
declare function xf:dynaXPath($xml as element(*) ?, $path as xs:string)
    as element(*)* {
	if(exists($xml)) then
      if(contains($path,'/') and not(empty($xml))) then
        xf:dynaXPath($xml/*[local-name(.) = substring-before($path,'/')],substring-after($path,'/'))
      else
        <result>{ $xml/*[local-name(.) = $path or $path = '*'] }</result>
    else
       <result></result>
};

declare variable $xml as element(*) ? external;
declare variable $path as xs:string external;

xf:dynaXPath($xml, $path)
