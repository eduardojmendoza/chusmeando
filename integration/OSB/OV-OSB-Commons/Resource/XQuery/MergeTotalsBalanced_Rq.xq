xquery version "1.0" encoding "UTF-8";
(:: pragma  parameter="$Ais" type="xs:anyType" ::)
(:: pragma  parameter="$Insis" type="xs:anyType" ::)
(:: pragma bea:global-element-return element="ns0:mergeTotalsBalanced" location="../WSDL/MergeTotalsService.wsdl" ::)

declare namespace xf = "http://tempuri.org/OV-OSB-Commons/Resource/XQuery/MergeTotalsBalanced_Rq/";
declare namespace ns0 = "http://cms.services.qbe.com/mergetotals";

declare function xf:MergeTotalsBalanced_Rq($Ais as element(*),
    $Insis as element(*))
    as element(ns0:mergeTotalsBalanced) {
        <ns0:mergeTotalsBalanced>
        	<left>{ $Ais }</left>
            <right>{ $Insis }</right>
        </ns0:mergeTotalsBalanced>
};

declare variable $Ais as element(*) external;
declare variable $Insis as element(*) external;

xf:MergeTotalsBalanced_Rq($Ais,
    $Insis)
