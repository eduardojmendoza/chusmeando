(:: pragma bea:global-element-return element="ns0:paginateTokenInfo" location="../XSD/PaginateTokenInfo.xsd" ::)

declare namespace ns0 = "http://www.example.org/PaginateTokenInfo";
declare namespace xf = "http://tempuri.org/OV-OSB-Commons/Resource/XQuery/GetPaginateTokenInfo/";

declare function xf:GetPaginateTokenInfo($token as xs:string?, $pageSize as xs:long)
    as element(ns0:paginateTokenInfo) {
    if(empty($token) or $token = '')
    then
      let $uuid := fn-bea:uuid()
      let $nextOffset := $pageSize + 1
      return
        <ns0:paginateTokenInfo>
            <ns0:nextToken>{ concat('##PAGER##',$uuid,'#',$nextOffset,'#',$pageSize,'##PAGER##') }</ns0:nextToken>
			<ns0:nextOffset>{ $nextOffset }</ns0:nextOffset>
            <ns0:uuid>{ $uuid }</ns0:uuid>
            <ns0:offset>1</ns0:offset>
            <ns0:pageSize>{ $pageSize }</ns0:pageSize>
        </ns0:paginateTokenInfo>
    else
      let $values := tokenize(replace($token, "##PAGER##", ""), "#")
      let $nextOffset := xs:long($values[2]) + xs:long($values[3])
	  return
        <ns0:paginateTokenInfo>
            <ns0:nextToken>{ concat('##PAGER##',$values[1],'#',$nextOffset,'#',$values[3],'##PAGER##')  }</ns0:nextToken>
            <ns0:nextOffset>{ $nextOffset }</ns0:nextOffset>
            <ns0:uuid>{ $values[1] }</ns0:uuid>
            <ns0:offset>{ $values[2] }</ns0:offset>
            <ns0:pageSize>{ $values[3] }</ns0:pageSize>
        </ns0:paginateTokenInfo>
};

declare variable $token as xs:string? external;
declare variable $pageSize as xs:long external;

xf:GetPaginateTokenInfo($token, $pageSize)
