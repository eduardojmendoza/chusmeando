(:: pragma  parameter="$Ais" type="anyType" ::)
(:: pragma  parameter="$Insis" type="anyType" ::)
(:: pragma bea:global-element-return element="ns0:mergeTotals" location="../WSDL/MergeTotalsService.wsdl" ::)

declare namespace ns0 = "http://cms.services.qbe.com/mergetotals";
declare namespace xf = "http://tempuri.org/OV-OSB-Commons/Resource/XQuery/MergeTotals_Rq/";

declare function xf:MergeTotals_Rq($Ais as element(*),
    $Insis as element(*))
    as element(ns0:mergeTotals) {
        <ns0:mergeTotals>
            <left>{ $Ais }</left>
            <right>{ $Insis }</right>
        </ns0:mergeTotals>
};

declare variable $Ais as element(*) external;
declare variable $Insis as element(*) external;

xf:MergeTotals_Rq($Ais,
    $Insis)
