xquery version "1.0" encoding "UTF-8";

(:: pragma  parameter="$rootNode" type="xs:anyType" ::)
(:: pragma  type="xs:anyType" ::)

declare namespace xf = "http://tempuri.org/OV-OSB-Commons/Resources/XQuery/RemoveNamespace/";

(: Basado en http://souviksoa.blogspot.com.ar/2013/05/in-this-tutorial-i-have-tried-to.html :)
declare function xf:RemoveNamespace($rootNode as element(*))
    as element(*) {

    element { xs:QName(local-name($rootNode)) }
        {
            for $child in $rootNode/(@*,node())
            return
                if ($child instance of element())
                then xf:RemoveNamespace($child)
                else $child
        }
};

declare variable $rootNode as element(*) external;

xf:RemoveNamespace($rootNode)