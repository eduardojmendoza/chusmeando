xquery version "1.0" encoding "UTF-8";

declare namespace xf = "http://tempuri.org/OV-OSB-Commons/Resource/XQuery/EmptySoapFault/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:EmptySoapFault($isServerError as xs:boolean?, $faultString as xs:string?, $faultactor as xs:string?, $detail as element()?)
    as element(soap-env:Fault) {
  <soap-env:Fault>
    <faultcode xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">
    	{
			if ($isServerError) then (
				'soap-env:Server'
			) else (
				'soap-env:Client'
			)
    	}
    </faultcode>
    <faultstring>{ $faultString }</faultstring>
	<faultactor>{ $faultactor }</faultactor>
	<detail>
		{
			if (local-name($detail) = 'detail')
			then ( $detail/* )
			else ( $detail )
		}
	</detail>
  </soap-env:Fault>
};

declare variable $isServerError as xs:boolean? external;
declare variable $faultString as xs:string? external;
declare variable $faultactor as xs:string? external;
declare variable $detail as element()? external;

xf:EmptySoapFault($isServerError, $faultString, $faultactor, $detail)
