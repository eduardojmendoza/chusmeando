xquery version "1.0" encoding "UTF-8";
(:: pragma  type="xs:anyType" ::)

declare namespace xf = "http://tempuri.org/OV/Resource/XQuery/DynamicRouting/";
declare namespace ctx = "http://www.bea.com/wli/sb/context";

declare function xf:DynamicRouting($dynamicRoute as xs:string, $isProxy as xs:boolean?)
    as element(*) {
		<ctx:route>
			<ctx:service isProxy="{ exists($isProxy) and $isProxy }">
				{ $dynamicRoute }
			</ctx:service>
		</ctx:route>
};

declare variable $dynamicRoute as xs:string external;
declare variable $isProxy as xs:boolean? external;

xf:DynamicRouting($dynamicRoute, $isProxy)
