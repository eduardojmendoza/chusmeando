(:: pragma  parameter="$request" type="anyType" ::)
(:: pragma bea:global-element-return element="ns0:executeRequest" location="../WSDL/OVSvc.wsdl" ::)

declare namespace ns0 = "http://cms.services.qbe.com/";
declare namespace xf = "http://tempuri.org/OV/Resource/XQuery/OVSvc_Rq/";

declare function xf:OVSvc_Rq($actionCode as xs:string,
    $request as element(*))
    as element(ns0:executeRequest) {
        <ns0:executeRequest>
            <actionCode>{ $actionCode }</actionCode>
            <Request>{ $request/@* , $request/node() }</Request>
        </ns0:executeRequest>
};

declare variable $actionCode as xs:string external;
declare variable $request as element(*) external;

xf:OVSvc_Rq($actionCode,
    $request)
