(:: pragma bea:global-element-parameter parameter="$CMSexecuteRequestResponse" element="ns0:executeRequestResponse" location="../WSDL/OVSvc.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$INSISexecuteRequestResponse" element="ns0:executeRequestResponse" location="../WSDL/OVSvc.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:executeRequestResponse" location="../WSDL/OVSvc.wsdl" ::)

declare namespace ns0 = "http://cms.services.qbe.com/";
declare namespace xf = "http://tempuri.org/OV/Resource/XQuery/Join_CMS_INSIS/";

declare function xf:Join_CMS_INSIS($CMSexecuteRequestResponse as element(ns0:executeRequestResponse),
    $INSISexecuteRequestResponse as element(ns0:executeRequestResponse))
    as element(ns0:executeRequestResponse) {
        <ns0:executeRequestResponse>
            <return>
                <code>{ concat($CMSexecuteRequestResponse/return/code , $INSISexecuteRequestResponse/return/code) }</code>
                <Response/>
            </return>
        </ns0:executeRequestResponse>
};

declare variable $CMSexecuteRequestResponse as element(ns0:executeRequestResponse) external;
declare variable $INSISexecuteRequestResponse as element(ns0:executeRequestResponse) external;

xf:Join_CMS_INSIS($CMSexecuteRequestResponse,
    $INSISexecuteRequestResponse)
