xquery version "1.0" encoding "UTF-8";

<Routing>
	<Systems default="cms">
		<System id="cms">OV/Proxy/CMSSvc</System>
		<System id="insis">OV/Proxy/INSISSvc</System>
		<System id="integration">OV/Proxy/OVSvc</System>
	</Systems>
	<Routes>
		(: Package 1 :)

(: Implementacion multi comm structure :)
		<Route msgNro="1010" actionCode="lbaw_OVClientesConsulta" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
		<Route msgNro="1010" actionCode="lbaw_OVClientesConsulta_Paginado" system="integration" path="OV/Proxy/CMSPS" />

		<Route msgNro="1101" actionCode="lbaw_GetConsultaMQ" definicion="1101_OVDatosGralPoliza.xml" system="integration" path="OV-mqgen/Proxy/OV_1101_OVDatosGralPoliza" />
		<Route msgNro="1101" actionCode="lbaw_OVDatosGralPoliza" system="integration" path="OV-MQUsuario/Proxy/OV_1101" />
		<Route msgNro="1102" actionCode="lbaw_OVDatosGralCliente" system="integration" path="OV-MQ/Proxy/OV_1102" />
		<Route msgNro="1103" actionCode="lbaw_OVEndososDetalles" system="integration" path="OV-MQ/Proxy/OV_1103" />
		<Route msgNro="1104" actionCode="lbaw_OVEndososPrima" system="integration" path="OV-MQ/Proxy/OV_1104" />
(: Implementacion multi comm structure :)
		<Route msgNro="1106" actionCode="lbaw_OVOpePendTotales" system="integration" path="OV-MQ/Proxy/OV_1106_MultiComm" />

(: Implementacion multi comm structure :)
		<Route msgNro="1107" actionCode="lbaw_OVOpePendDetalles" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
		<Route msgNro="1107" actionCode="lbaw_OVOpePendDetalles_Paginado" system="integration" path="OV/Proxy/CMSPS" />

(: Implementacion multi comm structure :)
		<Route msgNro="1108" actionCode="lbaw_OVOpeEmiTotales" system="integration" path="OV-MQ/Proxy/OV_1108_MultiComm" />

(: Implementacion multi comm structure :)
		<Route msgNro="1109" actionCode="lbaw_OVOpeEmiDetalles" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
		<Route msgNro="1109" actionCode="lbaw_OVOpeEmiDetalles_Paginado" system="integration" path="OV/Proxy/CMSPS" />

		<Route msgNro="1110" actionCode="lbaw_OVCoberturasDetalle" system="integration" path="OV-MQ/Proxy/OV_1110" />
		<Route msgNro="1116" actionCode="lbaw_GetConsultaMQGestion" definicion="1116_DomiciliodeCorrespondencia.xml" system="integration" path="OV-mqgen/Proxy/OV_1116_DomiciliodeCorrespondencia" />
		<Route msgNro="1117" actionCode="lbaw_GetConsultaMQGestion" definicion="1117_Coberturas.xml" system="integration" path="OV-mqgen/Proxy/OV_1117_Coberturas" />
		<Route msgNro="1118" actionCode="lbaw_GetConsultaMQGestion" definicion="1118_MediosdePago.xml" system="integration" path="OV-mqgen/Proxy/OV_1118_MediosdePago" />
		<Route msgNro="1119" actionCode="lbaw_GetConsultaMQGestion" definicion="1119_DatosApoderadoAcreedor.xml" system="integration" path="OV-mqgen/Proxy/OV_1119_DatosApoderadoAcreedor" />
		<Route msgNro="1120" actionCode="lbaw_GetConsultaMQGestion" definicion="1120_GetPolizasEndosables.xml" system="integration" path="OV-mqgen/Proxy/OV_1120_MultiComm" />
		<Route msgNro="1123" actionCode="lbaw_GetConsultaMQ" definicion="1123_OVDetalleCoberXProd.xml" system="integration" path="OV-mqgen/Proxy/OV_1123_OVDetalleCoberXProd" />
		<Route msgNro="1123" actionCode="nbwsA_MQGenericoAIS" definicion="LBA_1123_DetalleRiesgo.xml" system="integration" path="Transacciones/Proxy/TX_LBA_1123_DetalleRiesgo" />
		<Route msgNro="1184" actionCode="nbwsA_MQGenericoAIS" definicion="LBA_1184_ProductosClientes.xml" system="integration" path="Transacciones/Proxy/TX_LBA_1184_ProductosClientes" />
		<Route msgNro="1202" actionCode="lbaw_GetConsultaMQ" definicion="1202_OVRiesAutos.xml" system="integration" path="OV-mqgen/Proxy/OV_1202_OVRiesAutos" />
		<Route msgNro="1202" actionCode="lbaw_OVRiesAutos" system="integration" path="OV-MQRiesgos/Proxy/OV_1202" />
		<Route msgNro="1213" actionCode="lbaw_GetConsultaMQGestion" definicion="1213_DatoPolizaRiesgo.xml" system="integration" path="OV-mqgen/Proxy/OV_1213_DatoPolizaRiesgo" />
		<Route msgNro="1582" actionCode="lbaw_OVRiesgosListadoImpre" system="integration" path="OV-MQEmision/Proxy/OV_1582" />
		<Route msgNro="1607" actionCode="lbaw_GetConsultaMQGestion" definicion="getHistorialPoliza.xml" system="integration" path="OV-mqgen/Proxy/OV_1607_GetHistorialPoliza" />

		(: Package 2 :)
(: Implementacion multi comm structure :)


        <Route msgNro="1301" actionCode="lbaw_OVSiniListadoDetalles" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
        <Route msgNro="1301" actionCode="lbaw_OVSiniListadoDetalles_Paginado" system="integration" path="OV/Proxy/CMSPS" />
(: Implementacion multi comm structure :)

		(: <Route msgNro="1302" actionCode="lbaw_OVSiniConsulta" system="integration" path="OV-MQ/Proxy/OV_1302_MultiComm" /> :)
		<Route msgNro="1302" actionCode="lbaw_OVSiniConsulta" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
		<Route msgNro="1302" actionCode="lbaw_OVSiniConsulta_Paginado" system="integration" path="OV/Proxy/CMSPS" />

		<Route msgNro="1304" actionCode="lbaw_OVSiniDetalle" system="integration" path="OV-MQ/Proxy/OV_1304" />
		<Route msgNro="1305" actionCode="lbaw_OVSiniDatosAdic" system="integration" path="OV-MQ/Proxy/OV_1305" />
		<Route msgNro="1306" actionCode="lbaw_OVSiniGestiones" system="integration" path="OV-MQ/Proxy/OV_1306" />
		<Route msgNro="1307" actionCode="lbaw_OVSiniSubcober" system="integration" path="OV-MQ/Proxy/OV_1307" />
(: Implementacion multi comm structure :)
		<Route msgNro="1309" actionCode="lbaw_OVSiniListadoTotales" system="integration" path="OV-MQ/Proxy/OV_1309_MultiComm" />
		<Route msgNro="1315" actionCode="nbwsA_MQGenericoAIS" definicion="LBA_1315_SeguimientoSiniestros.xml" system="integration" path="Transacciones/Proxy/TX_LBA_1315_SeguimientoSiniestros" />

(: Implementacion multi comm structure :)
		<Route msgNro="1340" actionCode="lbaw_siniMQGenerico" definicion="siniestroDenuncia_getListadoClientes.xml" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
		<Route msgNro="1340" actionCode="lbaw_GetListadoClientes_Paginado"  system="integration" path="OV/Proxy/CMSPS" />


		<Route msgNro="1341" actionCode="lbaw_siniMQGenerico" definicion="siniestroDenuncia_getDatosCLIENTE_RIESGO.xml" system="integration" path="SiniestroDenuncia/Proxy/SD_siniestroDenuncia_getDatosCLIENTE_RIESGO" />
		<Route msgNro="1346" actionCode="lbaw_siniMQGenerico" definicion="siniestroDenuncia_putForm1AIS.xml" system="integration" path="SiniestroDenuncia/Proxy/SD_siniestroDenuncia_putForm1AIS" />
		<Route msgNro="1347" actionCode="lbaw_siniMQGenerico" definicion="siniestroDenuncia_putForm2AIS.xml" system="integration" path="SiniestroDenuncia/Proxy/SD_siniestroDenuncia_putForm2AIS" />
		<Route msgNro="1348" actionCode="lbaw_siniMQGenerico" definicion="siniestroDenuncia_putForm3AIS.xml" system="integration" path="SiniestroDenuncia/Proxy/SD_siniestroDenuncia_putForm3AIS" />
		<Route msgNro="1361" actionCode="lbaw_siniMQGenerico" definicion="siniestroDenuncia_getListadoRiesgos_MA.xml" system="integration" path="SiniestroDenuncia/Proxy/SD_siniestroDenuncia_getListadoRiesgos_MA" />

		(: Package 3 :)
		<Route msgNro="1402" actionCode="lbaw_OVExigibleDetPoliza" system="integration" path="OV-MQ/Proxy/OV_1402" />
(: Implementacion multi comm structure :)
		<Route msgNro="1403" actionCode="lbaw_OVDeudaVceTotales" system="integration" path="OV-MQ/Proxy/OV_1403_MultiComm" />

(: Implementacion multi comm structure :)
	    <Route msgNro="1404" actionCode="lbaw_OVDeudaVceDetalles" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
		<Route msgNro="1404" actionCode="lbaw_OVDeudaVceDetalles_Paginado" system="integration" path="OV/Proxy/CMSPS" />

(: Implementacion multi comm structure :)
		<Route msgNro="1405" actionCode="lbaw_OVCartasReclaTotales" system="integration" path="OV-MQ/Proxy/OV_1405_MultiComm" />

(: Implementacion multi comm structure :)
        <Route msgNro="1406" actionCode="lbaw_OVCartasReclaDetalle" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
		<Route msgNro="1406" actionCode="lbaw_OVCartasReclaDetalle_Paginado" system="integration" path="OV/Proxy/CMSPS" />

		<Route msgNro="1407" actionCode="lbaw_OVCartasDetRecibos" system="integration" path="OV-MQ/Proxy/OV_1407" />
		<Route msgNro="1408" actionCode="lbaw_OVDetalleRecibo" system="integration" path="OV-MQ/Proxy/OV_1408" />
(: Implementacion multi comm structure :)
		<Route msgNro="1409" actionCode="lbaw_OVAnulxPagoTotales" system="integration" path="OV-MQ/Proxy/OV_1409_MultiComm" />

(: Implementacion multi comm structure :)

		(:<Route msgNro="1410" actionCode="lbaw_OVAnulxPagoDetalles" system="integration" path="OV-MQUsuario/Proxy/OV_1410_MultiComm" /> :)
		<Route msgNro="1410" actionCode="lbaw_OVAnulxPagoDetalles" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
		<Route msgNro="1410" actionCode="lbaw_OVAnulxPagoDetalles_Paginado" system="integration" path="OV/Proxy/CMSPS" />

		<Route msgNro="1411" actionCode="lbaw_OVSitCobranzas" system="integration" path="OV-MQ/Proxy/OV_1411" />
		<Route msgNro="1419" actionCode="lbaw_OVExigibleaPC" system="integration" path="OV-MQ/Proxy/OV_1419_MultiComm" />
(: Implementacion multi comm structure :)
		<Route msgNro="1426" actionCode="lbaw_GetConsultaMQGestion" definicion="GetDeudaCobradaTotales.xml" system="integration" path="OV-mqgen/Proxy/OV_1426_MultiComm" />

		<Route msgNro="1427" actionCode="lbaw_GetConsultaMQGestion" definicion="GetDeudaCobradaCanales.xml" system="integration" path="OV-mqgen/Proxy/OV_1427_MultiComm" />

(: Implementacion CMS-Paginate :)
		<Route msgNro="1428" actionCode="lbaw_GetConsultaMQGestion" definicion="GetDeudaCobradaCanalesDetalle.xml" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
		<Route msgNro="1428" actionCode="lbaw_GetDeudaCobradaCanalesDetalle_Paginado" system="integration" path="OV/Proxy/CMSPS" />

		<Route msgNro="1430" actionCode="nbwsA_MQGenericoAIS" definicion="LBA_1430_SeguimientoCobranzas.xml" system="integration" path="Transacciones/Proxy/TX_LBA_1430_SeguimientoCobranzas" />
		<Route msgNro="1605" actionCode="lbaw_GetConsultaMQGestion" definicion="GetCuponesPendientes.xml" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
		<Route msgNro="1605" actionCode="lbaw_GetCuponesPendientes_Paginado" system="integration" path="OV/Proxy/CMSPS" />

		<Route msgNro="1606" actionCode="lbaw_GetConsultaMQGestion" definicion="GetCuponesPendientesDetalle.xml" system="integration" path="OV-mqgen/Proxy/OV_1606_GetCuponesPendientesDetalle" />

		(: Package 4 :)
		<Route msgNro="47" actionCode="lbaw_OVGetCotiAUS" system="integration" path="OV-MQCotizar/Proxy/OV_0047_CMS" />
		<Route msgNro="59" actionCode="lbaw_CotEndosos" definicion="0059_CotizadorEndosos.xml" system="integration" path="OV-CotEndosos/Proxy/OV_0059_CotizadorEndosos" />
		<Route msgNro="1114" actionCode="lbaw_GetConsultaMQ" definicion="1114_OpRet.xml" system="integration" path="OV-mqgen/Proxy/OV_1114_OpRet" />
		<Route msgNro="1185" actionCode="nbwsA_MQGenericoAIS" definicion="LBA_1185_SusyDesus_epoliza.xml" system="integration" path="Transacciones/Proxy/TX_LBA_1185_SusyDesus_epoliza" />
		<Route msgNro="1187" actionCode="nbwsA_MQGenericoAIS" definicion="LBA_1187_ListadoePolizas.xml" system="integration" path="Transacciones/Proxy/TX_LBA_1187_ListadoePolizas" />
		<Route msgNro="1188" actionCode="nbwsA_MQGenericoAIS" definicion="LBA_1188_EnvioPoliza.xml" system="integration" path="Transacciones/Proxy/TX_LBA_1188_EnvioPoliza" />
		<Route msgNro="1522" actionCode="lbaw_GetListaRetenciones" system="integration" path="OV-MQEmision/Proxy/OV_1522" />
		<Route msgNro="1525" actionCode="lbaw_BajaPropuesta" system="integration" path="OV-MQEmision/Proxy/OV_1525" />
		<Route msgNro="1526" actionCode="lbaw_EmitirPropuesta" system="integration" path="OV-MQEmision/Proxy/OV_1526_A_Insis" />
		<Route msgNro="1540" actionCode="lbaw_AisPutSolicAUS" system="integration" path="OV-MQEmision/Proxy/OV_1540_Pre_A_Insis" />
		<Route msgNro="1543" actionCode="lbaw_GetConsultaMQGestion" definicion="1543_AltaEndoso.xml" system="integration" path="OV-mqgen/Proxy/OV_1543_AltaEndosos" />
		<Route msgNro="1549" actionCode="lbaw_GetConsultaMQ" definicion="1549_BajaEndoso.xml" system="integration" path="OV-mqgen/Proxy/OV_1549_BajaEndoso" />
		<Route msgNro="1581" actionCode="lbaw_OVVerifReimpresion" system="integration" path="OV-MQEmision/Proxy/OV_1581_VerifReimpresion" />
		<Route msgNro="1583" actionCode="lbaw_OVEndososDetallesImpre" system="integration" path="OV-MQEmision/Proxy/OV_1583" />
		<Route msgNro="1584" actionCode="lbaw_GetCertMercosur" system="integration" path="OV-MQEmision/Proxy/OV_1584_Merge" />
		<Route msgNro="1701" actionCode="lbaw_GetConsultaMQ" definicion="1701_ImprimirPoliza.xml" system="integration" path="OV-mqgen/Proxy/OV_1701_ImprimirPoliza" />
		<Route msgNro="1701C" actionCode="lbaw_OVImprimirPoliza" system="integration" path="OV-MQEmision/Proxy/OV_1701C_ImprimirPoliza" />
		<Route msgNro="1901" actionCode="lbaw_GetConsultaMQGestion" definicion="1901_BusquedaConstanciasPago.xml" system="integration" path="OV-mqgen/Proxy/OV_1901_BusquedaConstanciasPago" />
		<Route msgNro="1903" actionCode="lbaw_GetConsultaMQ" definicion="1903_Productores_Excluidos.xml" system="integration" path="OV/Proxy/CMSPS" />

		(: Package 5 :)
		<Route msgNro="1308" actionCode="lbaw_OVSiniConsPagos" system="integration" path="OV-MQ/Proxy/OV_1308" />
(: Implementacion multi comm structure :)
		<Route msgNro="1400" actionCode="lbaw_OVExigibleTotales" system="integration" path="OV-MQ/Proxy/OV_1400_MultiComm" />

(: Implementacion multi comm structure :)

		<Route msgNro="1401" actionCode="lbaw_OVExigibleDetalles" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
		<Route msgNro="1401" actionCode="lbaw_OVExigibleDetalles_Paginado" system="integration" path="OV/Proxy/CMSPS" />

		<Route msgNro="1412" actionCode="lbaw_OVDeudaVceDetPoliza" system="integration" path="OV-MQ/Proxy/OV_1412" />

		<Route msgNro="1415" actionCode="lbaw_OVLOGetCantRecibos" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
		<Route msgNro="1415" actionCode="lbaw_OVLOGetCantRecibos_Paginado" system="integration" path="OV/Proxy/CMSPS" />

		<Route msgNro="1416" actionCode="lbaw_OVLOGetRecibos" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
		<Route msgNro="1416" actionCode="lbaw_OVLOGetRecibos_Paginado" system="integration" path="OV/Proxy/CMSPS" />
		
		
		<Route msgNro="1417" actionCode="lbaw_OVLOGetNroLiqui" system="integration" path="OV-MQ/Proxy/OV_1417" />
		<Route msgNro="1420" actionCode="lbaw_OVLOLiquixProd" system="integration" path="OV-MQ/Proxy/OV_1420" />
		<Route msgNro="1421" actionCode="lbaw_OVLOReimpLiqui" system="integration" path="OV-MQ/Proxy/OV_1421" />
		
		<Route msgNro="1425" actionCode="lbaw_GetConsultaMQGestion" definicion="getRecibosNotasDeCredito.xml" system="integration" path="OV-MQ/Proxy/Paginate_Router" />
		<Route msgNro="1425" actionCode="lbaw_GetConsultaMQGestion_Paginado" system="integration" path="OV/Proxy/CMSPS" />
		
		
		
		<Route msgNro="1802" actionCode="lbaw_OVEndososComi" system="integration" path="OV-MQ/Proxy/OV_1802" />
		<Route msgNro="1900" actionCode="lbaw_GetConsultaMQGestion" definicion="cupones_1900_ConstanciasPago.xml" system="integration" path="OV-mqgen/Proxy/OV_1900" />

		(: Custom :)
		<Route msgNro="" actionCode="int_GetHighestLevelCommStructure" system="integration" path="OV-MQ/Proxy/OV_GetHighestLevelCommStructure" />

        (:nuevo BPM:)
        <Route msgNro="" actionCode="lbaw_ServiciosBPM" system="integration" path="OV-BPM/Proxy/BPM_Dispatcher" />
	</Routes>
</Routing>
