(:: pragma  parameter="$response" type="anyType" ::)
(:: pragma bea:global-element-return element="ns1:executeRequestResponse" location="../WSDL/OVSvc.wsdl" ::)

declare namespace ns1 = "http://cms.services.qbe.com/";
declare namespace ns0 = "http://cms.services.qbe.com/RequestTypes";
declare namespace xf = "http://tempuri.org/OV/Resource/XQuery/OVSvc_Rs/";

declare function xf:executeRequestResponse($response as element(*))
    as element(ns1:executeRequestResponse) {
        <ns1:executeRequestResponse>
            <return>
        		<code>
        		    {
        		        if ( lower-case(data($response/Estado/@resultado)) = 'true')
        		        then ( '0' )
        		        else ( '1' )
        		    }
        		</code>
                <Response>{ $response/@* , $response/node() }</Response>
            </return>
        </ns1:executeRequestResponse>
};

declare variable $response as element(*) external;

xf:executeRequestResponse($response)
