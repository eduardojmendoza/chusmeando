(:: pragma bea:global-element-parameter parameter="$retrieveHoldStatusResponseBPM" element="ns0:retrieveHoldStatusResponse" location="../WSDL/BPMOVServices_types.wsdl" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_retrieveHoldStatus.xsd" ::)

declare namespace ns0 = "http://integration.qbe.com/BPMOVServices";
declare namespace xf = "http://tempuri.org/OV-BPM/Resource/XQuery/BPMretrieveHoldStatusRs/";

declare function xf:BPMretrieveHoldStatusRs($retrieveHoldStatusResponseBPM as element(ns0:retrieveHoldStatusResponse))
    as element(Response) {
        <Response>
            {local:estado($retrieveHoldStatusResponseBPM)}
            <responseCod>{ data($retrieveHoldStatusResponseBPM/return/response/codigo) }</responseCod>
            {
                for $retenciones in $retrieveHoldStatusResponseBPM/return/retenciones
                return
                    <retenciones>
                        {
                            for $item in $retenciones/item
                            return
                                <retencion>
                                    <estado>{ data($item/estado) }</estado>
                                    <motivo>{ data($item/motivo) }</motivo>
                                    <sectorAsignado>{ data($item/sectorAsignado) }</sectorAsignado>
                                    <fechaAlta>{ local:getFecha (data($item/fechaAlta))}</fechaAlta>
                                    <ultimaModificacion>{ if (exists($item/ultimaModificacion))  
                                                            then local:getFecha (data($item/ultimaModificacion)) 
                                                          else ()}</ultimaModificacion>
                                </retencion>
                        }
                    </retenciones>
            }
        </Response>
};

declare variable $retrieveHoldStatusResponseBPM as element(ns0:retrieveHoldStatusResponse) external;


declare function local:estado($responseBPM as element(ns0:retrieveHoldStatusResponse)) {
	<Estado mensaje = ""  resultado = "true"/>
};

declare function local:getFecha($fechaBPM as xs:dateTime) {
	
	fn-bea:date-to-string-with-format("dd-MM-yyyy", fn-bea:date-from-dateTime($fechaBPM))
	
};
xf:BPMretrieveHoldStatusRs($retrieveHoldStatusResponseBPM)
