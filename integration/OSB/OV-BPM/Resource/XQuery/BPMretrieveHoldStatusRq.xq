(:: pragma bea:global-element-parameter parameter="$requestOV" element="Request" location="../XSD/OV_retrieveHoldStatus.xsd" ::)
(:: pragma bea:global-element-return element="ns0:retrieveHoldStatus" location="../WSDL/BPMOVServices_types.wsdl" ::)

declare namespace ns0 = "http://integration.qbe.com/BPMOVServices";
declare namespace xf = "http://tempuri.org/OV-BPM/Resource/XQuery/BPMretrieveHoldStatusRq/";

declare function xf:BPMretrieveHoldStatusRq($requestOV as element(Request))
    as element(ns0:retrieveHoldStatus) {
        <ns0:retrieveHoldStatus>
            <request>
                <id>{ data($requestOV/id) }</id>
            </request>
        </ns0:retrieveHoldStatus>
};

declare variable $requestOV as element(Request) external;

xf:BPMretrieveHoldStatusRq($requestOV)
