(:: pragma bea:global-element-parameter parameter="$retrieveReportListResponseBPM" element="ns0:retrieveReportListResponse" location="../WSDL/BPMOVServices_types.wsdl" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_retrieveReportList.xsd" ::)

declare namespace ns0 = "http://integration.qbe.com/BPMOVServices";
declare namespace xf = "http://tempuri.org/OV-BPM/Resource/XQuery/BPMretrieveReportListRs/";

declare function xf:BPMretrieveReportListRs($retrieveReportListResponseBPM as element(ns0:retrieveReportListResponse))
    as element(Response) {
        <Response>
           {local:estado($retrieveReportListResponseBPM)}
           <responseCod>{ data($retrieveReportListResponseBPM/return/response/codigo) }</responseCod>
            <documentList>
                {
                    for $item in $retrieveReportListResponseBPM/return/documentList/item
                    return
                        <item>
                                     <fileId>{ data($item/fileId) }</fileId>
                                      <nombre>{ data($item/nombre) }</nombre>
                        </item>
                }
            </documentList>
        </Response>
};

declare variable $retrieveReportListResponseBPM as element(ns0:retrieveReportListResponse) external;

declare function local:estado($responseBPM as element(ns0:retrieveReportListResponse)) {
	(:Falta definir cuando es resultado es False:)
	<Estado mensaje = ""  resultado = "true"/>
};

xf:BPMretrieveReportListRs($retrieveReportListResponseBPM)



