(:: pragma bea:global-element-parameter parameter="$requestOV" element="Request" location="../XSD/OV_publishReport.xsd" ::)
(:: pragma bea:global-element-return element="ns0:publishReport" location="../WSDL/BPMOVServices_types.wsdl" ::)

declare namespace ns0 = "http://integration.qbe.com/BPMOVServices";
declare namespace xf = "http://tempuri.org/OV-BPM/Resource/XQuery/BPMpublishReportRq/";

declare function xf:BPMpublishReportRq($requestOV as element(Request))
    as element(ns0:publishReport) {
        <ns0:publishReport>
        
            <request>
                <id>{ data($requestOV/id) }</id>
                <reportName>{ data($requestOV/reportName) }</reportName>
                <reportContent>{ data($requestOV/reportContent) }</reportContent>
                <tipoOperacion>{ data($requestOV/tipoOperacion) }</tipoOperacion>
                <tipoDocumentoCliente>{ data($requestOV/tipoDocumentoCliente) }</tipoDocumentoCliente>
                <numeroDocumentoCliente>{ data($requestOV/numeroDocumentoCliente) }</numeroDocumentoCliente>
                <codigoProductor>{ data($requestOV/codigoProductor) }</codigoProductor>
                <claseProductor>{ data($requestOV/claseProductor) }</claseProductor>
                <codigoProducto>{ data($requestOV/codigoProducto) }</codigoProducto>
                <fechaOE>{ data($requestOV/fechaOE) }</fechaOE>
                {
                    for $numeroDePoliza in $requestOV/numeroDePoliza
                    return
                        if (data($numeroDePoliza)!='') then (
                        	<numeroDePoliza>{ data($numeroDePoliza) }</numeroDePoliza>
                        )else () 
                        
                }
                {
                    for $tipoEndoso in $requestOV/tipoEndoso
                    return
                        if ( data($tipoEndoso) != '') then (
                        	<tipoEndoso>{ data($tipoEndoso) }</tipoEndoso>
                        ) else ()
                        
                }
            </request>
        </ns0:publishReport>
};

declare variable $requestOV as element(Request) external;

xf:BPMpublishReportRq($requestOV)
