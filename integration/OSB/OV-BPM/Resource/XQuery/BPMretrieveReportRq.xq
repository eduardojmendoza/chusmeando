(:: pragma bea:global-element-parameter parameter="$requestOV" element="Request" location="../XSD/OV_retrieveReport.xsd" ::)
(:: pragma bea:global-element-return element="ns0:retrieveReport" location="../WSDL/BPMOVServices_types.wsdl" ::)

declare namespace ns0 = "http://integration.qbe.com/BPMOVServices";
declare namespace xf = "http://tempuri.org/OV-BPM/Resource/XQuery/BPMretrieveReportRq/";

declare function xf:BPMretrieveReportRq($requestOV as element(Request))
    as element(ns0:retrieveReport) {
        <ns0:retrieveReport>
            <request>
                <fileId>{ data($requestOV/fileid) }</fileId>
            </request>
        </ns0:retrieveReport>
};

declare variable $requestOV as element(Request) external;

xf:BPMretrieveReportRq($requestOV)
