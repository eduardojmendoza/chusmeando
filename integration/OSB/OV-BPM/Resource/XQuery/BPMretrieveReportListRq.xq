(:: pragma bea:global-element-parameter parameter="$requestOV" element="Request" location="../XSD/OV_retrieveReportList.xsd" ::)
(:: pragma bea:global-element-return element="ns0:retrieveReportList" location="../WSDL/BPMOVServices_types.wsdl" ::)

declare namespace ns0 = "http://integration.qbe.com/BPMOVServices";
declare namespace xf = "http://tempuri.org/OV-BPM/Resource/XQuery/BPMretrieveReportListRq/";

declare function xf:BPMretrieveReportListRq($requestOV as element(Request))
    as element(ns0:retrieveReportList) {
        <ns0:retrieveReportList>
            <request>
                <id>{ data($requestOV/id) }</id>
            </request>
        </ns0:retrieveReportList>
};

declare variable $requestOV as element(Request) external;

xf:BPMretrieveReportListRq($requestOV)
