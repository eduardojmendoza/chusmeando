(:: pragma bea:global-element-parameter parameter="$retrieveReportResponseBPM" element="ns0:retrieveReportResponse" location="../WSDL/BPMOVServices_types.wsdl" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_retrieveReport.xsd" ::)

declare namespace ns0 = "http://integration.qbe.com/BPMOVServices";
declare namespace xf = "http://tempuri.org/AIS_AgentSync/BPMretrieveReportRs/";

declare function xf:BPMretrieveReportRs($retrieveReportResponseBPM as element(ns0:retrieveReportResponse))
    as element(Response) {
        <Response>
           {local:estado($retrieveReportResponseBPM)}
         
            <responseCod>{ data($retrieveReportResponseBPM/return/response/codigo) }</responseCod>
            {
                let $descripcion := $retrieveReportResponseBPM/return/descripcion
                return
                    <descripcion>
                        <item>
                            <fileId>{ data($descripcion/fileId) }</fileId>
                            <nombre>{ data($descripcion/nombre) }</nombre>
                        </item>
                    </descripcion>
            }
            <reportContent>{ data($retrieveReportResponseBPM/return/report) }</reportContent>
        </Response>
};

declare variable $retrieveReportResponseBPM as element(ns0:retrieveReportResponse) external;

declare function local:estado($retrieveReportResponseBPM as element(ns0:retrieveReportResponse)) {
	(:Falta definir cuando es resultado es False:)
	<Estado mensaje = ""  resultado = "true"/>
};
xf:BPMretrieveReportRs($retrieveReportResponseBPM)
