(:: pragma bea:local-element-parameter parameter="$responseBPM" type="ns0:retrieveHoldStatusResponse/return/response" location="../WSDL/BPMOVServices_types.wsdl" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_publishReport.xsd" ::)

declare namespace ns0 = "http://integration.qbe.com/BPMOVServices";
declare namespace xf = "http://tempuri.org/OV-BPM/Resource/XQuery/OVresponseError/";

declare function xf:OVresponseError($esError as xs:boolean, 
									$mensaje as xs:string,
									$response as element()) as element(Response) {
        <Response>
            <Estado mensaje = "{$mensaje}"  resultado = "{$esError}"/>
            <responseCod>{ data($response/codigo) }</responseCod>
        </Response>
};

declare variable $response as element() external;
declare variable $esError as xs:boolean external;
declare variable $mensaje as xs:string external;

xf:OVresponseError($esError,$mensaje,$response)
