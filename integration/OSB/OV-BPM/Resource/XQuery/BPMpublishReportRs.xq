(:: pragma bea:global-element-parameter parameter="$publishReportResponseBPM" element="ns0:publishReportResponse" location="../WSDL/BPMOVServices_types.wsdl" ::)
(:: pragma bea:global-element-return element="Response" location="../XSD/OV_publishReport.xsd" ::)

declare namespace ns0 = "http://integration.qbe.com/BPMOVServices";
declare namespace xf = "http://tempuri.org/OV-BPM/Resource/XQuery/BPMpublishReportRs/";

declare function xf:BPMpublishReportRs($publishReportResponseBPM as element(ns0:publishReportResponse))
    as element(Response) {
        <Response>
            <Estado resultado="true" mensaje=""/>   <!-- error “controlado” en OSB . Falta ver cuando es "false", por ahora siempre "true"-->
            {
                let $__nullable := ( data($publishReportResponseBPM/return/response/codigo) )
                return
                    if (fn:boolean($__nullable))
                    then
                        <responseCod>{ $__nullable }</responseCod>
                    else
                        ()
            }
        </Response>
};

declare variable $publishReportResponseBPM as element(ns0:publishReportResponse) external;

xf:BPMpublishReportRs($publishReportResponseBPM)
