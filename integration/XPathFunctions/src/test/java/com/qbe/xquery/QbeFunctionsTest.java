package com.qbe.xquery;

import org.junit.Assert;
import org.junit.Test;

public class QbeFunctionsTest {

	@Test
	public void getPolicyNo() throws Exception {
		
		String ciasscode = "0001";
		String producto = "AUS1";
		String poliza = "00000001";
		String certi = "00000001558342";
		String expected = "0001AUS10000000100000001558342";
		
		String result = QbeFunctions.buildPolicyNo(ciasscode, producto, poliza, certi);
		
		Assert.assertEquals(expected, result);

	}
	
	@Test
	public void getPolicyNoSplitted() throws Exception {
		
		String ciasscode = "0001";
		String producto = "AUS1";
		String polizann = "00";
		String polizsec = "000001";
		String certipol = "0000";
		String certiann = "0001";
		String certisec = "558342";
		String expected = "0001AUS10000000100000001558342";

		
		String result = QbeFunctions.buildPolicyNoSplitted(ciasscode, producto, polizann, polizsec, certipol, certiann, certisec);
		
		Assert.assertEquals(expected, result);

	}

}
