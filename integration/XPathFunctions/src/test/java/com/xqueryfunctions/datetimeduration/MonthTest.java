package com.xqueryfunctions.datetimeduration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

public class MonthTest {

	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Test
	public void getFebruaryWithDate() throws Exception {
		Calendar calendar = Calendar.getInstance();
		Date date = dateFormat.parse("2012-02-02");
		calendar.setTime(Month.lastDateOfMonth(date));
		Assert.assertEquals(29, calendar.get(Calendar.DAY_OF_MONTH));
		date = dateFormat.parse("2013-02-02");
		calendar.setTime(Month.lastDateOfMonth(date));
		Assert.assertEquals(28, calendar.get(Calendar.DAY_OF_MONTH));
	}

	@Test
	public void getFebruaryYearMonth() throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(Month.lastDateOfMonthFromYearMonth(2012, 2));
		Assert.assertEquals(29, calendar.get(Calendar.DAY_OF_MONTH));
		calendar.setTime(Month.lastDateOfMonthFromYearMonth(2013, 2));
		Assert.assertEquals(28, calendar.get(Calendar.DAY_OF_MONTH));
	}
}
