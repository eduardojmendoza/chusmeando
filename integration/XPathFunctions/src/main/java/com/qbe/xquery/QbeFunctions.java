package com.qbe.xquery;



public class QbeFunctions {

	/**
	 * Genera el numero de poliza utilizando los parametros Producto, Poliza y Certi
	 */
	public static String buildPolicyNo(String ciasscode, String producto, String poliza, String certi) {
		
		StringBuilder policyNo = new StringBuilder();
		
		policyNo.append(ciasscode);
		policyNo.append(String.format("%4s", producto == null ? "" : producto));
		policyNo.append(String.format("%8s", poliza == null ? "" : poliza));
		policyNo.append(String.format("%14s", certi == null ? "" : certi));
		
		return policyNo.toString().replace(' ', '0');
	}
	
	/**
	 * Genera el numero de poliza utilizando los parametros Ramo, Polizan, Polisec, Certipol, Certian, Certisec.
	 */
	
	public static String buildPolicyNoSplitted(String ciasscode, String producto, String polizann, String polizsec, String certipol, String certiann, String certisec) {
		
		StringBuilder policyNo = new StringBuilder();
		
		policyNo.append(String.format("%4s",ciasscode == null ? "" : ciasscode));
		policyNo.append(String.format("%4s", producto == null ? "" : producto));
		policyNo.append(String.format("%2s", polizann == null ? "" : polizann));
		policyNo.append(String.format("%6s", polizsec == null ? "" : polizsec));
		policyNo.append(String.format("%4s", certipol == null ? "" : certipol));
		policyNo.append(String.format("%4s", certiann == null ? "" : certiann));
		policyNo.append(String.format("%6s", certisec == null ? "" : certisec));
		
		return policyNo.toString().replace(' ', '0');
	}

}
