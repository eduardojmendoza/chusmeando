# Ejecutar estos comandos para probar varios servicios:
#
# En cada caso no debe haber error de ejecución (HTTP 200), y además se 
# pueden revisar los resultados comparando con los archivos *.expected
#
# Todos estos archivos se crearon ejecutando contra el 10.1.10.70:7002

export OSBHOST=10.1.10.98
export OSBPORT=8011

##############################
## INSIS
##############################

curl -v -X POST -H "Content-Type: text/xml;charset=UTF-8" \
--data @INSIS-LOV_AllNomenclatures.xml \
http://$OSBHOST:$OSBPORT/INSIS-OV/Proxy/InsuranceWS


##############################
## CMS
##############################

curl -v -X POST -H "Content-Type: text/xml;charset=UTF-8" \
--data @CMS-Msg_1230.xml \
http://$OSBHOST:$OSBPORT/OV/Proxy/CMSSvc

curl -v -X POST -H "Content-Type: text/xml;charset=UTF-8" \
--data @CMS-Msg_1308.xml \
http://$OSBHOST:$OSBPORT/OV/Proxy/CMSSvc

curl -v -X POST -H "Content-Type: text/xml;charset=UTF-8" \
--data @CMS-Msg_1419.xml \
http://$OSBHOST:$OSBPORT/OV/Proxy/CMSSvc


##############################
## OSB
##############################

curl -v -X POST -H "Content-Type: text/xml;charset=UTF-8" \
--data @OSB-QBESvc-Msg_1106.xml \
http://$OSBHOST:$OSBPORT/OV/Proxy/QBESvc


##############################
## Synchronization
##############################

curl -v -X POST -H "Content-Type: text/xml;charset=UTF-8" -H 'SOAPAction: "http://www.qbe.com.ar/AIS/AIS_AgentSync/Find"' \
--data @Sync-Agent-Find.xml \
http://$OSBHOST:$OSBPORT/AIS_AgentSync/Proxy/AIS_AgentSync

curl -v -X POST -H "Content-Type: text/xml;charset=UTF-8" -H 'SOAPAction: "http://www.qbe.com.ar/AIS/AIS_PersonSync/Find"' \
--data @Sync-Person-Find.xml \
http://$OSBHOST:$OSBPORT/AIS_PersonSync/Proxy/AIS_PersonSync


##############################
## Tecnored
##############################

curl -v -X POST -H "Content-Type: text/xml;charset=UTF-8" \
--data @Tecnored-AgendaService.xml \
http://$OSBHOST:$OSBPORT/Tecnored/Proxy/AgendaService

