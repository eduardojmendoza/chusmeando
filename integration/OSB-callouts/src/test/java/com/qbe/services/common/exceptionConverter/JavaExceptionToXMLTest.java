package com.qbe.services.common.exceptionConverter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.xmlbeans.XmlObject;
import org.custommonkey.xmlunit.NamespaceContext;
import org.custommonkey.xmlunit.SimpleNamespaceContext;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.custommonkey.xmlunit.exceptions.XpathException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

public class JavaExceptionToXMLTest {

	@Before
	public void setUp() throws Exception {
		XMLUnit.setIgnoreWhitespace(true);

		Map<String, String> nses = new HashMap<String, String>();
		nses.put("j2ex", JavaExceptionToXML.NS);

		NamespaceContext ctx = new SimpleNamespaceContext(nses);
		XMLUnit.setXpathNamespaceContext(ctx);
	}

	@Test
	public void testConvertWrapped() throws XpathException, SAXException, IOException, JavaExceptionToXMLException {
		RuntimeException rte = new RuntimeException("Original");
		Exception e = new Exception("Wrapper", rte);

		String expected =
				"<JavaExceptions xmlns='" + JavaExceptionToXML.NS + "'>\n" +
				"	<Exception>\n" +
				"		<Msg>Wrapper</Msg>\n" +
				"		<Class>java.lang.Exception</Class>\n" +
				"		<Stacktrace>java.lang.Exception: Wrapper\n" +
				"	at com.qbe.services.common.exceptionConverter.JavaExceptionToXML.main(JavaExceptionToXML.java:106)\n" +
				"Caused by: java.lang.RuntimeException: Original\n" +
				"	at com.qbe.services.common.exceptionConverter.JavaExceptionToXML.main(JavaExceptionToXML.java:105)\n" +
				"		</Stacktrace>\n" +
				"	</Exception>\n" +
				"	<Exception>\n" +
				"		<Msg>Original</Msg>\n" +
				"		<Class>java.lang.RuntimeException</Class>\n" +
				"		<Stacktrace>java.lang.RuntimeException: Original\n" +
				"	at com.qbe.services.common.exceptionConverter.JavaExceptionToXML.main(JavaExceptionToXML.java:105)\n" +
				"		</Stacktrace>\n" +
				"	</Exception>\n" +
				"</JavaExceptions>";

		XmlObject result = JavaExceptionToXML.convert(e);

		String resultString = result.xmlText();

		String xpathMsg = "//j2ex:JavaExceptions/j2ex:Exception/j2ex:Msg";
		String xpathClass = "//j2ex:JavaExceptions/j2ex:Exception/j2ex:Class";

		XMLAssert.assertXpathsEqual(xpathMsg, expected, xpathMsg, resultString);
		XMLAssert.assertXpathsEqual(xpathClass, expected, xpathClass, resultString);
	}

	/**
	 * Disponible unicamente para generar ejemplos para tests
	 */
	public static void main(String[] args) throws JavaExceptionToXMLException {
		RuntimeException rte = new RuntimeException("Original");
		Exception e = new Exception("Wrapper", rte);

		XmlObject result = JavaExceptionToXML.convert(e);

		String resultString = result.xmlText();
		System.out.println(resultString);
	}

}
