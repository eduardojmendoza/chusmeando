package com.qbe.osbcallouts.batchrenewals;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.junit.Test;

public class BatchFilesMergerTest {

	@Test
	public void testMergeBatchFiles() throws IOException {
		
		String renewalFilePath = "/tmp/renewals/R20140925.TXT";
		String frontNotesPath = "/tmp/renewals/F20140925.TXT";
		String internalNotesPath = "/tmp/renewals/N20140925.TXT";
		String merged = BatchFilesMerger.mergeBatchFiles(renewalFilePath, internalNotesPath, frontNotesPath, "ISO-8859-1");
		System.out.println(merged);
	}

	@Test
	public void testMergeBatchFilesXml() throws IOException, ParserConfigurationException, XmlException {
		
		String renewalFilePath = "/tmp/renewals/R20140925.TXT";
		String frontNotesPath = "/tmp/renewals/F20140925.TXT";
		String internalNotesPath = "/tmp/renewals/N20140925.TXT";
		XmlObject xmlO = BatchFilesMerger.mergeBatchFilesXML(renewalFilePath, internalNotesPath, frontNotesPath, "ISO-8859-1");
		System.out.println(xmlO);
	}
}
