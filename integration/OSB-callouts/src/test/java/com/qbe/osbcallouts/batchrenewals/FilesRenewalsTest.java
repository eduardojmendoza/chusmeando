package com.qbe.osbcallouts.batchrenewals;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class FilesRenewalsTest {
	

	public static final String NS = "http://xml.qbe.com.ar/AIS_ArchivoRenovacionStatus";

   @Test
	public void testWriteFileXml() throws IOException {
		
		try {
		
			FilesRenewals.writeFileStatus(getXmlStatusExample());
	
		} catch (XmlException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
	}
   @Test
	public void testWriteFile() {
		
		
			FilesRenewals.writeFile("lalalaal", "/home/oracle/lote/ok","aaa.xml");	
		
	}

	@Test 
	public void moveFileStatusTest() {
		
		
		FilesRenewals.moveFileStatus("test.TXT", "/home/oracle/lote/ok", "/home/oracle/lote/archive");
		
	} 
	
	/**
	 * Arma un xml ejemplo del RenovacionStatus.
	 * @return xml 
	 * @throws XmlException
	 * @throws ParserConfigurationException
	 */
	private XmlObject getXmlStatusExample () throws XmlException, ParserConfigurationException{
	    	 
	    	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	 		DocumentBuilder db;
		
	 		db = dbf.newDocumentBuilder();
			Document doc = db.newDocument();

	 		Element archRenovacionStatus = doc.createElementNS(NS, "ArchivoRenovacionStatus");
	 		doc.appendChild(archRenovacionStatus);

	 		Element aisPolNumber = doc.createElementNS(NS, "aisPolicyNumber");
	 		aisPolNumber.appendChild(doc.createTextNode("0001AUS10000000100000001708036"));
	 		archRenovacionStatus.appendChild(aisPolNumber);
	 		
	 		Element originFlow = doc.createElementNS(NS, "originFlow");
	 		originFlow.appendChild(doc.createTextNode("R"));
	 		archRenovacionStatus.appendChild(originFlow);
	 		
	 		Element result = doc.createElementNS(NS, "result");
	 		result.appendChild(doc.createTextNode("ER"));
	 		archRenovacionStatus.appendChild(result);
	 	    
	 		Element newPolicyApplicationNumber = doc.createElementNS(NS, "newPolicyApplicationNumber");
	 		archRenovacionStatus.appendChild(newPolicyApplicationNumber);
	 	   
	 	    Element newPolicyNumber = doc.createElementNS(NS, "newPolicyNumber");
	 		archRenovacionStatus.appendChild(newPolicyNumber);
	 		
	 		Element originFilename = doc.createElementNS(NS, "originFilename");
	 		archRenovacionStatus.appendChild(originFilename);
	 		
	 		Element transactionID = doc.createElementNS(NS, "transactionID");
	 		archRenovacionStatus.appendChild(transactionID);
	 		
	 		Element errorCode = doc.createElementNS(NS, "errorCode");
	 		errorCode.appendChild(doc.createTextNode("ERROR_APLICACION"));
	 		archRenovacionStatus.appendChild(errorCode);
	 		
	 		Element erroDetail = doc.createElementNS(NS, "erroDetail");
	 		erroDetail.appendChild(doc.createTextNode("TEST TEST TEST TEST"));
	 		archRenovacionStatus.appendChild(erroDetail);
	 		

	 		return XmlObject.Factory.parse(doc);
	     }

}
