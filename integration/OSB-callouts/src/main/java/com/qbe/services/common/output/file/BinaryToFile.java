package com.qbe.services.common.output.file;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class BinaryToFile {

	/**
	 * Guarda el contenido del binario pasado como array en el archivo indicado
	 * sin realizar ninguna conversion.
	 *
	 * Util para ver el contenido de un &lt;ctx:binary-content&gt;
	 *
	 * @param binaryContent byte[] con el contenido a guardar en el archivo.
	 * @param filepath Path completo al archivo en donde se desea guardar el contenido.
	 *
	 * @throws IOException En caso de que ocurra algun error al realizar operaciones de I/O.
	 */
	public static void saveToFile(Object binaryContent, String filepath) throws BinaryToFileException {
		if (binaryContent == null) {
			return;
		}

		try {
			byte[] bytes = (byte[])binaryContent;

			FileOutputStream fos;
			fos = new FileOutputStream(filepath);
			fos.write(bytes);
			fos.close();
		} catch (Exception e) {
			throw new BinaryToFileException("Ocurrio un error al guardar el binario en archivo", e);
		}
	}

	/**
	 * Guarda el contenido del binario pasado como array en el archivo indicado
	 * pero permite realizar conversion de encoding del contenido.
	 * Para ello se debe especificar el encoding actual del contenido binario
	 * y el encoding al cual se desea convertir.
	 *
	 * Util para ver el contenido de un &lt;ctx:binary-content&gt;
	 *
	 * Para ver la lista de Encodings soportados:
	 * http://docs.oracle.com/javase/7/docs/technotes/guides/intl/encoding.doc.html
	 *
	 * @param binaryContent byte[] con el contenido a guardar en el archivo.
	 * @param filepath Path completo al archivo en donde se desea guardar el contenido.
	 * @param inCharset Encoding en el que se encuentran los datos binarios.
	 * @param outCharset Encoding en el que se desea guardar en el archivo.
	 *
	 * @throws IOException En caso de que ocurra algun error al realizar operaciones de I/O
	 * 						o que el Encoding indicado no este soportado.
	 */
	public static void saveToFile(Object binaryContent, String filepath, String inCharset, String outCharset) throws BinaryToFileException {
		if (binaryContent == null) {
			return;
		}

		try {
			String in = new String((byte[])binaryContent, inCharset);
			byte[] outputData = in.getBytes(outCharset);

			saveToFile(outputData, filepath);
		} catch (UnsupportedEncodingException e) {
			throw new BinaryToFileException("Uno de los encodings indicados no se encuentra soportado", e);
		}

	}

}
