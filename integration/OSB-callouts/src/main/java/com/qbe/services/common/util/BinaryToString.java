package com.qbe.services.common.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.activation.DataSource;

public class BinaryToString {

	public static String convertToString(DataSource binaryContent)
			throws Exception {
		byte[] buffer = new byte[4096];
		InputStream content = binaryContent.getInputStream();
		int i = 0;
		ByteArrayOutputStream out = new ByteArrayOutputStream(4096);
		while ((i = content.read(buffer)) >= 0) {
			out.write(buffer, 0, i);
		}
		return new String(out.toByteArray(), "UTF-8");
	}

	public static String convertToString(Object binaryContent)
			throws UnsupportedEncodingException {

		return convertToString((byte[]) binaryContent);
	}

	public static String convertToString(byte[] content, String encoding)
			throws UnsupportedEncodingException {

		return new String(content, encoding);

	}

	public static String convertToString(byte[] content)
			throws UnsupportedEncodingException {

		return convertToString(content, "UTF-8");

	}

}
