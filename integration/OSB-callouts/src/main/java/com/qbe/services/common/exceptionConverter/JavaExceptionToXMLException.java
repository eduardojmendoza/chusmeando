package com.qbe.services.common.exceptionConverter;

public class JavaExceptionToXMLException extends Exception {

	private static final long serialVersionUID = -8357945299767003221L;

	public JavaExceptionToXMLException() {
	}

	public JavaExceptionToXMLException(String message) {
		super(message);
	}

	public JavaExceptionToXMLException(Throwable cause) {
		super(cause);
	}

	public JavaExceptionToXMLException(String message, Throwable cause) {
		super(message, cause);
	}

}
