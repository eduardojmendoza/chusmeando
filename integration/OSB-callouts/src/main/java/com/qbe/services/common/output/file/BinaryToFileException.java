package com.qbe.services.common.output.file;

public class BinaryToFileException extends Exception {

	private static final long serialVersionUID = 5029121351945310926L;

	public BinaryToFileException() {
	}

	public BinaryToFileException(String message) {
		super(message);
	}

	public BinaryToFileException(Throwable cause) {
		super(cause);
	}

	public BinaryToFileException(String message, Throwable cause) {
		super(message, cause);
	}

}
