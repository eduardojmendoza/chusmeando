package com.qbe.services.common.exceptionConverter;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xmlbeans.XmlObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class JavaExceptionToXML {

	public static final String NS = "http://xml.qbe.com/OSB-callouts/exceptionConverter/JavaExceptionToXML";

	/**
	 * Convierte una Exception de Java en un XML con el formato:
	 *
	 * <pre>
	 *   &lt;JavaExceptions xmlns="http://xml.qbe.com/OSB-callouts/exceptionConverter/JavaExceptionToXML"&gt;
	 *     &lt;Exception&gt;
	 *       &lt;Msg&gt;El message de la Exception si tiene, sino vacio&lt;/Msg&gt;
	 *       &lt;Class&gt;El FQCN de la Exception&lt;/Class&gt;
	 *       &lt;Stacktrace&gt;El Stacktrace correspondiente a la Exception&lt;/Stacktrace&gt;
	 *     &lt;/Exception&gt;
	 *     &lt;Exception&gt;
	 *       [...]
	 *     &lt;/Exception&gt;
	 *   &lt;/JavaExceptions&gt;
	 * </pre>
	 *
	 * El primer elemento &lt;Exception&gt; corresponde a la Exception mas general
	 * y el último a la causa original, similar a los stacktrace de Java.
	 *
	 * Util para procesar: $fault/ctx:java-exception/ctx:java-content
	 *
	 * @param exception Instancia de alguna subclase de {@link Throwable}.
	 *
	 * @return Instancia de {@link XmlObject} con el formato de XML indicado.
	 *
	 * @throws JavaExceptionToXMLException En caso de que ocurra algo que impida convertir la excepcion a XML.
	 */
	public static XmlObject convert(Object exception) throws JavaExceptionToXMLException {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.newDocument();

			Element elJavaExceptions = doc.createElementNS(NS, "JavaExceptions");
			doc.appendChild(elJavaExceptions);

			Throwable ex = (Throwable) exception;

			do {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				ex.printStackTrace(pw);

				String msg = ex.getMessage();
				String clazz = ex.getClass().getName();
				String stacktrace = sw.getBuffer().toString();

				Element elException = doc.createElementNS(NS, "Exception");
				Element elMsg = doc.createElementNS(NS, "Msg");
				Element elClass = doc.createElementNS(NS, "Class");
				Element elStacktrace = doc.createElementNS(NS, "Stacktrace");

				elMsg.appendChild(doc.createTextNode(msg));
				elClass.appendChild(doc.createTextNode(clazz));
				elStacktrace.appendChild(doc.createTextNode(stacktrace));

				elException.appendChild(elMsg);
				elException.appendChild(elClass);
				elException.appendChild(elStacktrace);

				elJavaExceptions.appendChild(elException);

				ex = ex.getCause();
			} while (ex != null);

			return XmlObject.Factory.parse(doc);
		} catch (Exception e) {
			throw new JavaExceptionToXMLException("Ocurrio un error al convertir la Java Exception a XML", e);
		}
	}

}