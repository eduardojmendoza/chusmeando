package com.qbe.services.common.logger;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CustomLogger {
	
	private static Map<String, Logger> loggerMap = new HashMap<String, Logger>(); 
	
	private static final String DEFAULT_LOGGER_NAME = "com.qbe.integration";

	public static void log(java.lang.String config,  java.lang.String nivel , java.lang.String mensaje) {

		String loggerName = ( config == null || config.length() == 0 ) ? DEFAULT_LOGGER_NAME : config;
		
		getLogger(loggerName).log(Level.parse(nivel), mensaje);
	}
	
	public static Logger getLogger(java.lang.String loggerName){
		Logger logger = loggerMap.get(loggerName);
		if ( logger == null )  {
			logger = Logger.getLogger(loggerName);
			loggerMap.put(loggerName, logger);
		}
		logger.log(Level.FINE, "Logging with this logger: " + loggerName);
		return logger; 
	}
}
