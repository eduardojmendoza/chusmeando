package com.qbe.services.common.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;


public class CustomLogger {
	
	public static void main(String[] args) {
	/*	
	  	Logger logger = (Logger) LoggerFactory.getLogger(CustomLogger.class);
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		try {
		      JoranConfigurator configurator = new JoranConfigurator();
		      configurator.setContext(lc);
		      lc.reset();
		      configurator.doConfigure(args[0]);
		    } catch (JoranException je) {
		      je.printStackTrace();
		    }
		String level = args[1];
		String logstring = args[2]; 
		if (level.equals("DEBUG")) { 
			logger.debug(logstring);  
		} if (level.equals("INFO")) { 
			logger.info(logstring); 
		} if (level.equals("WARN")) {
			logger.warn(logstring); 
		} if (level.equals("ERROR")) {
			logger.error(logstring); 
		} else { logger.trace(logstring); 
		} 
*/
		//log("/home/oracle/conf.xml", "INFO", "Escribo aquì");
		//log(args[0], args[1], args[2]);
}

	public static void log (String conf, String level, String msg) {
			Logger logger = (Logger) LoggerFactory.getLogger(CustomLogger.class);
		//    LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
			LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
			try {
			      JoranConfigurator configurator = new JoranConfigurator();
			      configurator.setContext(lc);
			      lc.reset();
			      configurator.doConfigure(conf);
			    } catch (JoranException je) {
			      je.printStackTrace();
			      System.out.println("aqui");
			    }
			newLogEntry(logger, level, msg);
	}
	public static void newLogEntry (Logger logger, String level, String msg){
	    	   String logstring = "Custom logging: " + msg; 
		       if (level.equals("DEBUG")) { 
		       logger.debug(logstring);  
		       } if (level.equals("INFO")) { 
		       logger.info(logstring); 
		       } if (level.equals("WARN")) {
		       logger.warn(logstring); 
		       } if (level.equals("ERROR")) {
		       logger.error(logstring); 
		       } else { logger.trace(logstring); 
		       }
	} 	 	
}
