from java.io import FileInputStream

SINTAX = {}
SINTAX['jmsModule'] = 'resourceType,jmsModuleName,targetName'
SINTAX['jmsSubDeployment'] = 'resourceType,jmsSubDeploymentName,jmsModuleName,jmsTargetName'
SINTAX['jmsConnectionFactory'] = 'resourceType,jmsConnectionFactoryName,jmsModuleName,jmsTargetName,messagesMaximum'
SINTAX['queue'] = 'resourceType,queueName,jmsModuleName,jmsSubDeploymentName,redeliveryLimit,redeliveryDelay,expirationPolicy,errorDestination'
SINTAX['dataSource'] = 'resourceType,dsName, dsJNDIName, initialCapacity, maxCapacity, capacityIncrement, jdbcURL, jdbcUser, jdbcPass'

ERROR_CANT_PARAM = '==>Error: Cantidad de parámentros inválido para el objeto "%s" en la linea: \n==>%s\n==>Sintaxis %s'

JDBC_DRIVER = 'oracle.jdbc.OracleDriver'

# Creates a JMS Module
def createModule(jmsModuleName, targetName):
  print '==>Creando Module "%s" y seteando Target "%s"' % (jmsModuleName, targetName)
  try:
    module = getMBean('/SystemResources/' + jmsModuleName)
    if(module == None):
      module = cmo.createJMSSystemResource(jmsModuleName, jmsModuleName + '-jms.xml')
      print '==>Se creó el Module %s: %s' % (jmsModuleName, module)
    else:
      print '==>El Module %s ya existe: %s' % (jmsModuleName, module)

    cd('/SystemResources/' + jmsModuleName)
    set('Targets', jarray.array([ObjectName('com.bea:Name=' + targetName + ',Type=Server')], ObjectName))
    print '==>Se creó el Module %s: %s' % (jmsModuleName, module)

  except Exception, e:
    print '===>Error creando Module %s' % jmsModuleName
    raise e

def createJMSSubDeployment(jmsSubDeploymentName, jmsModuleName, jmsTargetName):
  print '==>Creando SubDeployment "%s" en Module "%s" y seteando JMSTarget "%s"' % (jmsSubDeploymentName, jmsModuleName, jmsTargetName)
  try:
    jmsSubDeploymentPath = '/SystemResources/%s/SubDeployments/%s' % (jmsModuleName, jmsSubDeploymentName)
    jmsSubDeployment = getMBean(jmsSubDeploymentPath)
    if(jmsSubDeployment == None):
      jmsSubDeployment = cmo.createSubDeployment(jmsSubDeploymentName)
      print '==>Se creó el SubDeployment %s: %s' % (jmsSubDeploymentName, jmsSubDeployment)
    else:
      print '==>El SubDeployments %s ya existe: %s' % (jmsSubDeploymentName, jmsSubDeployment)

    cd(jmsSubDeploymentPath)
    set('Targets',jarray.array([ObjectName('com.bea:Name=%s,Type=JMSServer' % jmsTargetName)], ObjectName))

  except Exception, e:
    print '===>Error creando SubDeployments %s' % jmsSubDeploymentName
    raise e
  
def createConnectioFactory(jmsConnectioFactoryName, jmsModuleName, jmsTargetName, messagesMaximum):
  print '==>Creando ConnectioFactory "%s" en Module %s y seteando Target "%s"' % (jmsConnectioFactoryName, jmsModuleName, jmsTargetName)
  try:
    modulePath = '/JMSSystemResources/%s/JMSResource/%s' % (jmsModuleName, jmsModuleName)
    connectionFactoryPath = modulePath + '/ConnectionFactories/' + jmsConnectioFactoryName
    connectionFactory = getMBean(connectionFactoryPath)
    if(connectionFactory == None):
      cd(modulePath)
      connectionFactory = cmo.createConnectionFactory(jmsConnectioFactoryName)
      print '==>Se creó el ConnectioFactory %s: %s' % (jmsConnectioFactoryName, connectionFactory)
    else:
      print '==>El ConnectioFactory %s ya existe: %s' % (jmsConnectioFactoryName, connectionFactory)
  
    cd(connectionFactoryPath)
    cmo.setJNDIName(jmsConnectioFactoryName)
    cmo.setSubDeploymentName(jmsTargetName)
    
    cd(connectionFactoryPath + '/SecurityParams/' + jmsConnectioFactoryName)
    cmo.setAttachJMSXUserId(false)
    
    cd(connectionFactoryPath + '/ClientParams/' + jmsConnectioFactoryName)
    cmo.setClientIdPolicy('Restricted')
    cmo.setSubscriptionSharingPolicy('Exclusive')
    cmo.setMessagesMaximum(int(messagesMaximum))
    
    cd(connectionFactoryPath + '/TransactionParams/' + jmsConnectioFactoryName)
    cmo.setXAConnectionFactoryEnabled(true)
  except Exception, e:
    print '===>Error creando ConnectioFactory %s' % jmsConnectioFactoryName
    raise e

def createQueue(queueName, jmsModuleName, jmsSubDeploymentName, redeliveryLimit, redeliveryDelay, expirationPolicy, errorDestination):
  print '==>Creando Queue "%s" en Module "%s" y seteando SubDeployments "%s"' % (queueName, jmsModuleName, jmsSubDeploymentName)
  try:
    queuePathPattern = '/JMSSystemResources/%s/JMSResource/%s/Queues/%s'
    queuePath = queuePathPattern % (jmsModuleName, jmsModuleName, queueName)
    jmsSubDeploymentPath = '/SystemResources/%s/SubDeployments/%s' % (jmsModuleName, jmsSubDeploymentName)
    queue = getMBean(queuePath)
    if(queue == None):
      modulePath = '/JMSSystemResources/%s/JMSResource/%s' % (jmsModuleName, jmsModuleName)
      cd(modulePath)
      queue = cmo.createQueue(queueName)
      print '==>Se creó la Queue %s: %s' % (queueName, queue)
    else:
      print '==>La Queue %s ya existe: %s' % (queueName, queue)
      
    cd(queuePath)
    cmo.setJNDIName(queueName)
    cmo.setSubDeploymentName(jmsSubDeploymentName)
    
    cd(queuePath + '/DeliveryParamsOverrides/' + queueName)
    cmo.setRedeliveryDelay(int(redeliveryDelay))
    
    cd(queuePath + '/DeliveryFailureParams/' + queueName)
    cmo.setRedeliveryLimit(int(redeliveryLimit))
    
    if expirationPolicy == 'Redirect':
      errorQueuePath = queuePathPattern % (jmsModuleName, jmsModuleName, errorDestination)
      errorQueue = getMBean(errorQueuePath)
      if(errorQueue != None):
        cmo.setErrorDestination(errorQueue)
      else:
        raise Exception('==>No existe Queue de error "%s" en el Module "%s"' % (errorDestination, jmsModuleName))
    else:
      cmo.setExpirationPolicy('Discard')
      cmo.unSet('ErrorDestination')
  		
  except Exception, e:
    print '===>Error creando Queue %s' % queueName
    raise e

def createDS(dsName, dsJNDIName, initialCapacity, maxCapacity, capacityIncrement, jdbcURL, jdbcUser, jdbcPass):
  print '==>Creando DataSource ' + dsName + '.'
  print '\t dsJNDIName: ' + dsJNDIName
  print '\t initialCapacity: ' + initialCapacity
  print '\t maxCapacity: ' + maxCapacity
  print '\t capacityIncrement: '   + capacityIncrement
  
  try:
    jdbcSystemResource = getMBean('/JDBCSystemResources/' + dsName)
    if(jdbcSystemResource == None):
      jdbcSystemResource = create(dsName, 'JDBCSystemResource')
      print '==>Se creó el DataSource %s: %s' % (dsName, jdbcSystemResource)
    else:
      print '==>El DataSource %s ya existe: %s' % (dsName, jdbcSystemResource)

    # Create data source
    jdbcResource = jdbcSystemResource.getJDBCResource()
    jdbcResource.setName(dsName)
    # Set JNDI name
    jdbcResourceParameters = jdbcResource.getJDBCDataSourceParams()
    jdbcResourceParameters.setJNDINames([dsJNDIName])
    jdbcResourceParameters.setGlobalTransactionsProtocol('None')
    # Create connection pool
    connectionPool = jdbcResource.getJDBCConnectionPoolParams()
    connectionPool.setInitialCapacity(int(initialCapacity))
    connectionPool.setMaxCapacity(int(maxCapacity))
    connectionPool.setCapacityIncrement(int(capacityIncrement))
    connectionPool.setTestConnectionsOnReserve(true)
    connectionPool.setTestTableName('SQL SELECT 1 FROM DUAL')
    connectionPool.setConnectionCreationRetryFrequencySeconds(30)
    connectionPool.setTestFrequencySeconds(30)
    connectionPool.setRemoveInfectedConnections(false)
    connectionPool.setSecondsToTrustAnIdlePoolConnection(0)
    # Create driver settings
    driver = jdbcResource.getJDBCDriverParams()    
    driver.setDriverName(JDBC_DRIVER)
    driver.setUrl(jdbcURL)
    driver.setPassword(jdbcPass)
    driverProperties = driver.getProperties()
    userProperty = driverProperties.createProperty('user')
    userProperty.setValue(jdbcUser)
    oraclenetProperty = driverProperties.createProperty('oracle.net.CONNECT_TIMEOUT')
    oraclenetProperty.setValue('10000')
    # Set data source target
    jdbcSystemResource.addTarget(targetServer)
  except Exception, e:
    print '===>Error creando DataSource %s' % dsName
    raise e

def readFile(fileName):
  f = open(fileName)
  for line in f.readlines():
    
    ### Strip the comment lines
    if line.strip().startswith('#'):
      continue
    else:
      ### Split the comma separated values
      items = line.split(',')
      items = [item.strip() for item in items]
      if items[0] == 'jmsModule':
        if len(items) != 3:
          print ERROR_CANT_PARAM % (items[0], line, SINTAX['jmsModule'])
        else:
          (resourceType, jmsModuleName, targetName) = items
          createModule(jmsModuleName, targetName)
      elif items[0] == 'jmsSubDeployment':
        if len(items) != 4:
          print ERROR_CANT_PARAM % (items[0], line, SINTAX['jmsSubDeployment'])
        else:
          (resourceType, jmsSubDeploymentName, jmsModuleName, jmsTargetName) = items
          createJMSSubDeployment(jmsSubDeploymentName, jmsModuleName, jmsTargetName)
      elif items[0] == 'jmsConnectionFactory':
        if len(items) != 5:
          print ERROR_CANT_PARAM % (items[0], line, SINTAX['jmsConnectionFactory'])
        else:
          (resourceType, jmsConnectioFactoryName, jmsModuleName, jmsTargetName, messagesMaximum) = items
          createConnectioFactory(jmsConnectioFactoryName, jmsModuleName, jmsTargetName, messagesMaximum)
      elif items[0] == 'queue':
        if len(items) != 8:
          print ERROR_CANT_PARAM % (items[0], line, SINTAX['queue'])
        else:
          (resourceType, queueName, jmsModuleName, jmsSubDeploymentName, redeliveryLimit, redeliveryDelay, expirationPolicy, errorDestination) = items
          createQueue(queueName, jmsModuleName, jmsSubDeploymentName, redeliveryLimit, redeliveryDelay, expirationPolicy, errorDestination)
      elif items[0] == 'dataSource':
        if len(items) != 9:
          print ERROR_CANT_PARAM % (items[0], line, SINTAX['dataSource'])
        else:
          (resourceType, dsName, dsJNDIName, initialCapacity, maxCapacity, capacityIncrement, jdbcURL, jdbcUser, jdbcPass) = items
          createDS(dsName, dsJNDIName, initialCapacity, maxCapacity, capacityIncrement, jdbcURL, jdbcUser, jdbcPass)

      else:
        raise Exception('==>Error: No se reconoce el objeto "%s" de la linea %s' % (items[0], line))

argslength = len(sys.argv)
if argslength < 2 :
  print argslength
  print '==>Faltan argumentos'
  print '==>Syntax: wlst.sh createWLResources.py [context]'
  print '==>Context puede ser: local, desa, uat, prod'
  exit()
    
context = sys.argv[1]
propInputStream = FileInputStream(context + "-resources.properties")
configProps = Properties()
configProps.load(propInputStream)

username = configProps.get("wl.admin.user")
password = configProps.get("wl.admin.pass")
host = configProps.get("wl.host")
port = configProps.get("wl.admin.port")
fileName = context + "-resources.csv"

connect(username, password, 't3://' + host + ':' + port) 
edit()
try:
  startEdit()
  readFile(fileName)
  save()
  activate()
except Exception, e:
  cancelEdit()
  print e
disconnect()
#exit()


