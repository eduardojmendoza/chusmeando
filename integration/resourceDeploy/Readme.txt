Como usar
=========
1) Colocar el script y los archivos de configuracion en algún lugar del 
   server (suponemos "/home/oracle/scriptRecursos").
2) Cargar las variables de entorno del dominio en el que voy a trabajar.

$ export SCRIPT_HOME=/home/oracle/scriptRecursos
$ export MW_HOME=/u01/app/oracle/product/fmw
$ export MY_DOMAIN=qbe_domain
$ . MW_HOME/user_projects/domains/$MY_DOMAIN/bin/setDomainEnv.sh

3) Volver a la carpeta donde tengo los scripts

$ cd $SCRIPT_HOME

4) Ejecutar el script

$ /wlserver_10.3/common/bin/wlst.sh createWLResources.py [ambiente]

Configuración
=============
Por cada ambiente debo tener un archivo ".properties" con los datos de 
conexión al server y un archivo ".csv". con los recursos que deseo crear.
Cada linea del CSV puede tener alguno de estos formatos:

- jmsModule,[jmsModuleName],[targetName]
- jmsSubDeployment,[jmsSubDeploymentName],[jmsModuleName],[jmsTargetName]
- jmsConnectioFactoryName,[jmsConnectioFactoryName],[jmsModuleName],[jmsTargetName],[messagesMaximum]
- queue,[queueName],[jmsModuleName],[jmsSubdeploymentName],[redeliveryLimit],[redeliveryDelay],[expirationPolicy],[errorDestination]
- dataSource,[dsName],[dsJNDIName],[initialCapacity],[maxCapacity],[capacityIncrement],[jdbcURL],[jdbcUser],[jdbcPass]

Los recursos deben ser creados en orden ya que si tienen dependencias pueden 
fallar.


