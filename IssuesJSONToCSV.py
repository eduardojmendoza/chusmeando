"""
Exports Issues from a specified repository to a CSV file

Uses basic authentication (Github username + password) to retrieve Issues
from a repository that username has access to. Supports Github API v3.
"""
import csv
import requests
import json

REPO = 'snoopconsulting/migracioncomplus'  # format is username/repo


def write_issues(response):
    "output a list of issues to csv"
    
    for issue in r:
        if issue['milestone'] is None:
            issue['milestone']={'title': "Bug"}
        csvout.writerow([issue['number'], issue['title'].encode('utf-8'), issue['milestone']['title'].encode('utf-8'), issue['created_at'], issue['updated_at'], issue['state']])

f = open('%s.json' % (REPO.replace('/', '-'))) 
r = json.load(f) 
f.close()


csvfile = '%s-issues.csv' % (REPO.replace('/', '-'))
csvout = csv.writer(open(csvfile, 'wb'))
csvout.writerow(('id', 'Title', 'Milestone', 'Created At', 'Updated At', 'State'))
print "Convirtiendo a CSV..."
write_issues(r)
#more pages? examine the 'link' header returned

print "FIN"