#!/bin/bash

repo=$1
repo="snoopconsulting/migracioncomplus"

filename=$(echo "$repo.json" | tr / -)
echo "Dumping $1 to $filename..."
echo
echo

# remove -u if not private
curl -u "cbonilla20:cbam2002" \
  "https://api.github.com/repos/$repo/issues?per_page=1000&state=all" \
  > $filename

python IssuesJSONToCSV.py
