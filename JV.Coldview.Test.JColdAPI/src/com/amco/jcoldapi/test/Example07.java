/**
 * 
 */
package com.amco.jcoldapi.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.amco.jcoldapi.ColdViewApi;
import com.amco.jcoldapi.Exceptions.BadServiceCallException;
import com.amco.jcoldapi.Exceptions.CVConnectionException;
import com.amco.jcoldapi.cvobjects.BinaryOperator;
import com.amco.jcoldapi.cvobjects.IndexedResult;
import com.amco.jcoldapi.cvobjects.Issue;
import com.amco.jcoldapi.cvobjects.IssueInfo;
import com.amco.jcoldapi.cvobjects.Library;
import com.amco.jcoldapi.cvobjects.LogicalOperator;
import com.amco.jcoldapi.cvobjects.Page;
import com.amco.jcoldapi.cvobjects.Report;
import com.amco.jcoldapi.cvobjects.Task;
import com.amco.jcoldapi.cvobjects.Tuple;
import com.amco.jcoldapi.test.conf.Configuration;
import com.amco.jcoldapi.test.conf.Configuration.SEARCH;

/**
 * Example 7 - Page Navigation
 * 
 * An example that shows how to connect to an AcoldServer Connection Service, requests libraries, processes and tasks allowed for a user. The iterates through all the published pages of a particular issue.
 * 
 * @author
 * 
 */
public class Example07 {

	private static Logger logger = Logger.getLogger(Example07.class);
	
	public static void main(String[] args) {
		if (Configuration.loadConfiguration()) {
			final ColdViewApi api = new ColdViewApi();
			try {
				/*
				 * Connect to the Connection Service
				 */
				api.connect(Configuration.HOST, Configuration.PORT, Configuration.USER, Configuration.PASS);

				/*
				 * Execute Test
				 */
				executeTest(api);
				
				/*
				 * Close API
				 */
				api.close();
				logger.info("close api");
				
			} catch (UnknownHostException e) {
				logger.error("No route to host.", e);
			} catch (IOException e) {
				logger.error("Error writing to the socket", e);
			} catch (CVConnectionException e) {
				logger.error("Could not log into Connection Service", e);	
			}
		}
	}
	
	public static void executeTest(final ColdViewApi api) {
		/*
		 * Get a list of published libraries
		 */
		try {

			final List<Library> libraries = api.struc.getLibraries();
			final Iterator<Library> librariesIter = libraries.iterator();
			if (librariesIter.hasNext()) {
				final Library library = librariesIter.next();

				/*
				 * Get list of tasks from a given library
				 */
				final List<Task> tasks = api.struc.getTasks(library);
				final Iterator<Task> tasksIter = tasks.iterator();
				if (tasksIter.hasNext()) {
					final Task task = tasksIter.next();

					/*
					 * Get list of reports from a given library and task
					 */
					final List<Report> reports = api.struc.getReports(library, task);
					final Iterator<Report> reportsIter = reports.iterator();
					if (reportsIter.hasNext()) {
						final Report report = reportsIter.next();

						final List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false, "R2016022520160302");

						for (Iterator<IssueInfo> issueInfosIter = issueInfos.iterator(); issueInfosIter.hasNext();) {
							final IssueInfo info = issueInfosIter.next();

							logger.info("Library: " + library.getName());
							logger.info("Task: " + task.getName());
							logger.info("Report name:" + report.getName());
							logger.info("Issue name:" + info.getName());

							if (report.getName().equals(SEARCH.REPORT) && info.getName().equals(SEARCH.ISSUE_INFO) && library.getName().equals(SEARCH.LIBRARY)) {
								logger.info("Encontrado:" + info.getName());

								Issue issue = api.struc.getIssue(library, info);
								logger.info("Issue: " + issue);

								/*
								 * Perform an indexed search on this issue
								 */
								final List<Tuple> tuples = new ArrayList<Tuple>();

								final Tuple tuple = new Tuple(SEARCH.SEARCH_FIELDNAME, BinaryOperator.LIKE, SEARCH.TO_SEARCH, LogicalOperator.NOTHING);
								tuples.add(tuple);

								final List<IndexedResult> indexedResults = api.struc.getIndexResults(issue, tuples);

								/*
								 * Prints page number and the result fields
								 */
								int pageNum = 1;
								for (Iterator<IndexedResult> iter = indexedResults.iterator(); iter.hasNext();) {
									IndexedResult result = iter.next();
									pageNum = result.getPageNumber();
									logger.info("Page number '" + pageNum + "' result fields: " + result.getFields());
								}

								/*
								 * Go to last page searched
								 */
								issue.setNextPage(pageNum);
								final Page page = api.struc.getPage(issue, Configuration.getCHARSET());
								logger.info(page);
								logger.info(page.toString());
								final File folderOut = new File("output");
								if (folderOut.exists() || folderOut.mkdirs()) {
									final File output = new File(folderOut, "Example07_" + System.currentTimeMillis() + ".pdf");
									final String pdfBase64 = api.struc.getPDFBase64(issue, "", "password", false, false, false, true, false, false, false, true);
									final FileOutputStream fos = new FileOutputStream(output);
									fos.write(Base64.decodeBase64(pdfBase64));
									fos.close();
									
								} else {
									logger.error("Error creando la carpeta "+folderOut.getAbsolutePath());
									
								}

								/*
								 * Close issue
								 */
								boolean closeOk = api.struc.closeIssue(issue);
								logger.info("" + closeOk);
							}
						}
					}
				}
			}
		} catch (UnknownHostException e) {
			logger.error("No route to host.", e);
		} catch (IOException e) {
			logger.error("Error writing to the socket", e);
		} catch (BadServiceCallException e) {
			logger.error("Invalida parameters", e);
		} catch(Exception e) {
			logger.error( "Connection", e );
		}
	}

}