/**
 * 
 */
package com.amco.jcoldapi.test;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import org.apache.log4j.Logger;

import com.amco.jcoldapi.ColdViewApi;
import com.amco.jcoldapi.Exceptions.CVConnectionException;
import com.amco.jcoldapi.cvobjects.Library;
import com.amco.jcoldapi.test.conf.Configuration;

/**
 * Example 1 - Allowed libraries
 * 
 * An example that shows how to connect to an AcoldServer Connection Service and request the allowed libraries for 
 * a list of users.
 * 
 * @author 
 *
 */
public class Example01 {
	
	private static Logger logger = Logger.getLogger(Example01.class);
	
	public static void main(String[] args) {
		if (Configuration.loadConfiguration()) {
			final ColdViewApi api = new ColdViewApi();
			try {
				/*
				 * Connect to the Connection Service
				 */
				api.connect(Configuration.HOST, Configuration.PORT, Configuration.USER, Configuration.PASS);
				

				/*
				 * Execute Test
				 */
				executeTest(api);
				
				/*
				 * Close API
				 */
				api.close();
				logger.info("close api");
				
			} catch (UnknownHostException e) {
				logger.error("No route to host.", e);
			} catch (IOException e) {
				logger.error("Error writing to the socket", e);
			} catch (CVConnectionException e) {
				logger.error("Could not log into Connection Service", e);	
			}
		}
	}
	
	public static void executeTest(final ColdViewApi api) {
		try {
			
			/*
			 * Get a list of published libraries
			 */
			List<Library> libraries = api.struc.getLibraries();
			logger.info( "Allowed libraries: " + libraries );
			
			
		} catch (UnknownHostException e) {
			logger.error( "No route to host.", e );
		} catch (IOException e) {
			logger.error( "Error writing to the socket", e );
		} catch(Exception e) {
			logger.error( "Connection", e );
		}
	}
}