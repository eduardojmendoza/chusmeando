/**
 * 
 */
package com.amco.jcoldapi.test;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.amco.jcoldapi.ColdViewApi;
import com.amco.jcoldapi.Exceptions.BadServiceCallException;
import com.amco.jcoldapi.Exceptions.CVConnectionException;
import com.amco.jcoldapi.cvobjects.Library;
import com.amco.jcoldapi.cvobjects.Report;
import com.amco.jcoldapi.cvobjects.Task;
import com.amco.jcoldapi.test.conf.Configuration;

/**
 * Example 3 - Libraries Tasks list
 * 
 * An example that shows how to connect to an AcoldServer Connection Service, request the allowed libraries and 
 * list all the tasks for each one of libraries.
 *  
 * @author aacuna
 *
 */
public class Example03 {
	
	private static Logger logger = Logger.getLogger(Example03.class);
	
	public static void main(String[] args) {
		if (Configuration.loadConfiguration()) {
			final ColdViewApi api = new ColdViewApi();
			try {
				/*
				 * Connect to the Connection Service
				 */
				api.connect(Configuration.HOST, Configuration.PORT, Configuration.USER, Configuration.PASS);

				/*
				 * Execute Test
				 */
				executeTest(api);
				
				/*
				 * Close API
				 */
				api.close();
				logger.info("close api");
				
			} catch (UnknownHostException e) {
				logger.error("No route to host.", e);
			} catch (IOException e) {
				logger.error("Error writing to the socket", e);
			} catch (CVConnectionException e) {
				logger.error("Could not log into Connection Service", e);	
			}
		}
	}
	
	public static void executeTest(final ColdViewApi api) {
		try {
			/*
			 * Get a list of published libraries
			 */
			final List<Library> libraries = api.struc.getLibraries();

			for (Iterator<Library> iter = libraries.iterator(); iter.hasNext();) {
				final Library library = (Library) iter.next();
				
				/*
				 * Get list of tasks from a given library
				 */
				final List<Task> tasks = api.struc.getTasks(library);
				
				for (Iterator<Task> iterator = tasks.iterator(); iterator.hasNext();) {
					final Task task = (Task) iterator.next();
					/*
					 * Get list of reports from a given library and task
					 */
					List<Report> reports = api.struc.getReports( library, task );
					logger.info( "Library: " + library.getName());
					logger.info( "Task: " + task.getName());
					logger.info( "Reports: " + reports );
				}
			}
			
		} catch (UnknownHostException e) {
			logger.error( "No route to host.", e );
		} catch (IOException e) {
			logger.error( "Error writing to the socket", e );
		} catch (BadServiceCallException e) {
			logger.error( "Invalida parameters", e );
		} catch(Exception e) {
			logger.error( "Connection", e );
		}
	}
	
}