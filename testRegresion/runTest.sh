#! /bin/bash
	#soapPath=/opt/soapui-4.5.2/
	source ./configTest.cfg
	soapPath=$SOAPHOME
	testsPath=$TESTSHOME		
#testsPath=$1
rm -r Faileds/
mkdir Faileds
if [[ -z "$testsPath" && -z "$soapPath" ]]; then
    echo >&2 "You must supply an argument!"
	    exit 1
elif [[ ! (-d "$soapPath" && -d "$testsPath") ]]; then
    echo >&2 "$1 is not a valid directory!"
    exit 1

fi
ls ${testsPath}/*.xml> /tmp/test.xml
while read line
do
    name=$line
    echo "Text read from file - $name"
    sh ${soapPath}/bin/testrunner.sh -f Faileds $name  
done < /tmp/test.xml

