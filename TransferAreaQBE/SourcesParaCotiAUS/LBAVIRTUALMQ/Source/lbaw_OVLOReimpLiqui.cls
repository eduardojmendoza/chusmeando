VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVLOReimpLiqui"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQ.lbaw_OVLOReimpLiqui"
Const mcteOpID              As String = "1421"

'Parametros XML de Entrada
Const mcteParam_Usuario    As String = "//USUARIO"
Const mcteParam_NroLiq     As String = "//NROLIQ"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarResultInt       As String
    Dim wvarResultPagos     As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarNroLiq          As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarNroLiq = Right(String(9, "0") & .selectSingleNode(mcteParam_NroLiq).Text, 9)
        '
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los Parametros de la cola de MQ del archivo de configuración
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.Load (App.Path & "\" & gcteParamFileName)
    wvarCiaAsCod = wobjXMLParametros.selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 120
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarNroLiq
    wvarStep = 240
    wvarResult = ""
    wvarPos = 76  'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarStep = 125
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 10
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 250
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 260
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 280
        wvarResult = ""
        wvarResult = wvarResult & "<DATOS>"
        wvarResult = wvarResult & "<NROLIQ>" & Mid(strParseString, wvarPos - 9, 9) & "</NROLIQ>"
        wvarResult = wvarResult & "<CLIENSEC>" & Mid(strParseString, wvarPos, 9) & "</CLIENSEC>"
        wvarResult = wvarResult & "<FECVTO>" & Mid(strParseString, wvarPos + 9, 10) & "</FECVTO>"
        'DA - 26/09/2008: se agregan 4 codigos de barras mas de 32 char y se expande el antiguo de 29 a 32
        'wvarResult = wvarResult & "<BARCODE>" & Mid(strParseString, wvarPos + 19, 29) & "</BARCODE>"
        wvarResult = wvarResult & "<BARCODE>" & Mid(strParseString, wvarPos + 19, 32) & "</BARCODE>"
        wvarResult = wvarResult & "<BARCODE_EFEC>" & Mid(strParseString, wvarPos + 51, 32) & "</BARCODE_EFEC>"
        wvarResult = wvarResult & "<BARCODE_CHEQ>" & Mid(strParseString, wvarPos + 83, 32) & "</BARCODE_CHEQ>"
        wvarResult = wvarResult & "<BARCODE_CHEQ48>" & Mid(strParseString, wvarPos + 115, 32) & "</BARCODE_CHEQ48>"
        wvarResult = wvarResult & "<BARCODE_CHEQ48DIF>" & Mid(strParseString, wvarPos + 147, 32) & "</BARCODE_CHEQ48DIF>"
        'DA - 26/09/2008: se ajusta esto ya que hay que contemplar los nuevos codigos de barra
        'wvarResult = wvarResult & "<COTIDOLAR>" & Format(CStr(CDbl(Mid(strParseString, wvarPos + 162, 15)) / 10000000), "0.0000000") & " </COTIDOLAR>"
        wvarResult = wvarResult & "<COTIDOLAR>" & Format(CStr(CDbl(Mid(strParseString, wvarPos + 293, 15)) / 10000000), "0.0000000") & " </COTIDOLAR>"
        wvarResult = wvarResult & "</DATOS>"
        '
        'DA - 26/09/2008: se ajusta esto ya que hay que contemplar los nuevos codigos de barra
        'wvarResultPagos = ParseoMensajePagos(wvarPos + 48, strParseString, wvarstrLen)
        wvarResultPagos = ParseoMensajePagos(wvarPos + 179, strParseString, wvarstrLen)
        '
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 280
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML (wvarResultPagos)
        '
        wvarStep = 290
        If wobjXMLResponse.selectNodes("//RP").length <> 0 Then
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSLPagos())
            '
            wvarStep = 300
            wvarResultPagos = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
            '
            wvarStep = 310
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
        End If
        '
        'DA - 26/09/2008: se ajusta esto ya que hay que contemplar los nuevos codigos de barra
        'wvarResultInt = ParseoMensaje(wvarPos + 177, strParseString)
        wvarResultInt = ParseoMensaje(wvarPos + 308, strParseString)
        '
        wvarStep = 270
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 280
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML (wvarResultInt)
        '
        wvarStep = 290
        If wobjXMLResponse.selectNodes("//R").length <> 0 Then
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSL())
            '
            wvarStep = 300
            wvarResultInt = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
            '
            wvarStep = 310
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
            '
        End If
        '
        wvarStep = 320
        Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & wvarResultPagos & wvarResultInt & "</Response>"
        '
    End If
    '
    wvarStep = 360
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing

Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
Private Function ParseoMensaje(wvarPos As Long, strParseString As String) As String
Dim wvarResult As String
Dim wvarCantLin As Long
Dim wvarCounter As Long
    '
    If Trim(Mid(strParseString, wvarPos, 3)) <> "" Then
        wvarCantLin = Mid(strParseString, wvarPos, 3)
        wvarPos = wvarPos + 3
        '
        wvarResult = wvarResult & "<RS>"
        For wvarCounter = 0 To wvarCantLin - 1
            wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 106) & "]]></R>"
            wvarPos = wvarPos + 106
        Next
    '
        wvarResult = wvarResult & "</RS>"
    '
    End If
    '
    ParseoMensaje = wvarResult
    '
End Function
Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""eeuu"" decimal-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/'> "
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REGS'>"
    wvarStrXSL = wvarStrXSL & "              <xsl:apply-templates select='/RS/R'/>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RS/R'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REG'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CLIDES'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,1,30))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='PROD'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,31,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='POL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,35,8)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERPOL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,43,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERANN'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,47,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERSEC'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,51,6)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='OPERAPOL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='number(substring(.,57,6))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='RECNUM'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,63,9)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='MON'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,72,3))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='SIG'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,75,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='normalize-space(substring(.,75,1))' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='IMP'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,76,14)) div 100), ""###.###.##0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='IMPCALC'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,76,14)) div 100), ""########0.00"", ""eeuu"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='RAMO'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,90,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='FECVTO'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,101,10)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='AGE'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,111,2)' />-<xsl:value-of select='substring(.,113,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIDES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='MON'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='FECVTO'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AGE'/>"
    '
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
    '
End Function

Private Function ParseoMensajePagos(wvarPos As Long, strParseString As String, wvarstrLen As Long) As String
Dim wvarResult As String
    '
    wvarResult = wvarResult & "<RPS>"
    '
    While wvarPos < wvarstrLen And Trim(Mid(strParseString, wvarPos, 2)) <> "00"
        wvarResult = wvarResult & "<RP><![CDATA[" & Mid(strParseString, wvarPos, 19) & "]]></RP>"
        wvarPos = wvarPos + 19
    Wend
    '
    wvarResult = wvarResult & "</RPS>"
    '
    ParseoMensajePagos = wvarResult
    '
End Function
Private Function p_GetXSLPagos() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""eeuu"" decimal-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/'> "
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='PAGOS'>"
    wvarStrXSL = wvarStrXSL & "              <xsl:apply-templates select='/RPS/RP'/>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RPS/RP'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='PAGO'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='FORMA'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='number(substring(.,1,2))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='IMPO'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,3,14)) div 100), ""###.###.##0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CANT'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='number(substring(.,17,3))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLPagos = wvarStrXSL
    '
End Function




