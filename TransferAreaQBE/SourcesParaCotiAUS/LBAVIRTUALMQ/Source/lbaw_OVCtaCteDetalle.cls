VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVCtaCteDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQ.lbaw_OVCtaCteDetalle"
Const mcteOpID              As String = "1800"

'Parametros XML de Entrada
Const mcteParam_Usuario      As String = "//USUARIO"
Const mcteParam_NivelAs      As String = "//NIVELAS"
Const mcteParam_ClienSecAs   As String = "//CLIENSECAS"
Const mcteParam_ClienSec1    As String = "//CLIENSEC1"
Const mcteParam_EfectAnn     As String = "//EFECTANN"
Const mcteParam_EfectMes     As String = "//EFECTMES"


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarNivelAs         As String
    Dim wvarClienSecAs      As String
    Dim wvarClienSec1       As String
    Dim wvarUsuario         As String
    Dim wvarEfectAnn        As String
    Dim wvarEfectMes        As String
    Dim wvarControl         As String
    Dim wvarCombo           As String
    Dim wvarLiquidacionMensual As String
    '
    Dim wvarPos             As Long
    Dim wvarVecPos          As Integer
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarNivelAs = Left(.selectSingleNode(mcteParam_NivelAs).Text & Space(2), 2)
        wvarClienSecAs = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSecAs).Text, 9)
        wvarClienSec1 = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSec1).Text, 9)
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarEfectAnn = Right(String(4, "0") & CStr(Year("01/01/" & Trim(.selectSingleNode(mcteParam_EfectAnn).Text))), 4)
        wvarEfectMes = Right(String(2, "0") & Trim(.selectSingleNode(mcteParam_EfectMes).Text), 2)
        '
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    If wvarEfectAnn = "" Then
        wvarEfectAnn = Year(Date)
        wvarEfectMes = Format(Month(Date), "00")
    End If
    '
    wvarStep = 40
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los Parametros de la cola de MQ del archivo de configuración
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.Load (App.Path & "\" & gcteParamFileName)
    wvarCiaAsCod = wobjXMLParametros.selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & Space(4) & wvarClienSecAs & wvarNivelAs & "  " & wvarClienSec1 & "  000000000  000000000" & wvarEfectAnn & wvarEfectMes
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 10
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        IAction_Execute = 1
        mobjCOM_Context.SetAbort
        GoTo ClearObjects:
    End If
        
    wvarStep = 160
    wvarResult = ""
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 170
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    wvarPos = 73
    If wvarEstado = "OK" Then
        '
        wvarLiquidacionMensual = "<LIQUIDAMENSUAL>" & Mid(strParseString, wvarPos + 10, 1) & "</LIQUIDAMENSUAL>"
        wvarControl = Mid(strParseString, wvarPos + 9, 1)
        '
        If wvarControl = "N" Then
            '
            wvarStep = 180
            wvarCombo = wvarCombo & "<SELECT name=""cliensec"">"
            wvarCombo = wvarCombo & "<OPTION value= """
            wvarCombo = wvarCombo & Mid(strParseString, wvarPos, 9) & """>" & Mid(strParseString, wvarPos, 9)
            wvarCombo = wvarCombo & "</OPTION>"
            wvarCombo = wvarCombo & "</SELECT>"
            '
            'Se cambio la longitud del vector de Cliensec de 20*9 a 200*9 29/12/2005
            'wvarPos = wvarPos + 190
            wvarPos = wvarPos + 1 + 1810
        Else
            '
            wvarStep = 190
            wvarPos = wvarPos + 10 + 1
            wvarVecPos = 0
            wvarCombo = ""
            wvarCombo = wvarCombo & "<SELECT name=""cliensec"">"
            wvarStep = 200
            While Mid(strParseString, wvarPos + wvarVecPos, 9) <> "000000000"
                wvarCombo = wvarCombo & "<OPTION value=""" & Mid(strParseString, wvarPos + wvarVecPos, 9) & """>" & Mid(strParseString, wvarPos + wvarVecPos, 9) & "</OPTION>"
                wvarVecPos = wvarVecPos + 9
            Wend
            wvarCombo = wvarCombo & "</SELECT>"
            'Se cambio la longitud del vector de Cliensec de 20*9 a 200*9 29/12/2005
            'wvarPos = wvarPos + 180
            wvarPos = wvarPos + 1800
            '
        End If
        '
        wvarStep = 210
        wvarResult = wvarResult & "<CTACTES>"
        '
        wvarStep = 220
        While wvarPos < wvarstrLen And Mid(strParseString, wvarPos + 8, 6) <> "000000"
            wvarResult = wvarResult & "<CTACTE>"
            wvarResult = wvarResult & "<FECHA>" & Mid(strParseString, wvarPos + 6, 2) & "/" & Mid(strParseString, wvarPos + 4, 2) & "/" & Mid(strParseString, wvarPos, 4) & "</FECHA>"
            wvarResult = wvarResult & "<LIQUISEC>" & Mid(strParseString, wvarPos + 8, 6) & "</LIQUISEC>"
            wvarResult = wvarResult & "<MOALFCOD>" & Trim(Mid(strParseString, wvarPos + 14, 3)) & "</MOALFCOD>"
            wvarResult = wvarResult & "<CONCEPTO>" & Mid(strParseString, wvarPos + 17, 1) & "</CONCEPTO>"
            If Mid(strParseString, wvarPos + 17, 1) = "D" Then
                wvarResult = wvarResult & "<IMPORTE>" & Replace(CStr(CDbl(Mid(strParseString, wvarPos + 18, 15)) / 10000000), ".", ",") & "</IMPORTE>"
            Else
                wvarResult = wvarResult & "<IMPORTE>" & Replace(CStr(CDbl(Mid(strParseString, wvarPos + 18, 15)) / 100), ".", ",") & "</IMPORTE>"
            End If
            wvarResult = wvarResult & "<SIGNO>" & IIf(Mid(strParseString, wvarPos + 33, 1) = "+", "", Mid(strParseString, wvarPos + 33, 1)) & "</SIGNO>"
            wvarResult = wvarResult & "</CTACTE>"
            wvarPos = wvarPos + 34
        Wend
        '
        wvarStep = 230
        wvarResult = wvarResult & "</CTACTES>"
        '
        wvarStep = 240
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 250
        wobjXMLResponse.loadXML wvarResult
        '
        wvarStep = 260
        wobjXSLResponse.async = False
        Call wobjXSLResponse.loadXML(p_GetXSL())
        '
        wvarStep = 270
        wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
        '
        wvarStep = 280
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarCombo & wvarLiquidacionMensual & wvarResult & "</Response>"
    Else
        wvarStep = 290
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "NO SE ENCONTRARON DATOS " & Chr(34) & " /></Response>"
    End If
    '
    wvarStep = 300
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing

Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarClienSecAs & wvarEfectAnn & wvarEfectMes & "00" & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:key name=""cuentas"" match=""CTACTE"" use=""FECHA"" />"
  
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='CTACTES'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:for-each select=""//CTACTE[count(. | key('cuentas', FECHA)[1]) = 1]"">"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CTACTE'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:element name='FECHA'>"
    wvarStrXSL = wvarStrXSL & "                      <xsl:value-of select=""FECHA""/>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:element name='LIQUISEC'>"
    wvarStrXSL = wvarStrXSL & "                      <xsl:value-of select=""LIQUISEC""/>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""not(//CTACTE[CONCEPTO='F' and MOALFCOD='$' and FECHA=current()/FECHA])"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='BRUTO_PESOS'>0</xsl:element>"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='BRUTO_PESOS_SIGNO'></xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""not(//CTACTE[CONCEPTO='F' and MOALFCOD='U$S' and FECHA=current()/FECHA])"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='BRUTO_DOLAR'>0</xsl:element>"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='BRUTO_DOLAR_SIGNO'></xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""not(//CTACTE[CONCEPTO='C' and MOALFCOD='$' and FECHA=current()/FECHA])"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='NETO_PESOS'>0</xsl:element>"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='NETO_PESOS_SIGNO'></xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""not(//CTACTE[CONCEPTO='C' and MOALFCOD='U$S' and FECHA=current()/FECHA])"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='NETO_DOLAR'>0</xsl:element>"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='NETO_DOLAR_SIGNO'></xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""not(//CTACTE[CONCEPTO='P' and MOALFCOD='$' and FECHA=current()/FECHA])"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='PEND_PESOS'>0</xsl:element>"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='PEND_PESOS_SIGNO'></xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""not(//CTACTE[CONCEPTO='P' and MOALFCOD='U$S' and FECHA=current()/FECHA])"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='PEND_DOLAR'>0</xsl:element>"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='PEND_DOLAR_SIGNO'></xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""not(//CTACTE[CONCEPTO='D' and MOALFCOD='U$S' and FECHA=current()/FECHA])"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='COTIZ_DOLAR'>0</xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    
    wvarStrXSL = wvarStrXSL & "                 <xsl:for-each select=""key('cuentas', FECHA)"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                         <xsl:when test=""CONCEPTO='F'"">"
    wvarStrXSL = wvarStrXSL & "                             <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                                 <xsl:when test=""MOALFCOD='$'"">"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='BRUTO_PESOS'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""IMPORTE""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='BRUTO_PESOS_SIGNO'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""SIGNO""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                 </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                                 <xsl:when test=""MOALFCOD='U$S'"">"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='BRUTO_DOLAR'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""IMPORTE""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='BRUTO_DOLAR_SIGNO'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""SIGNO""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                 </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                             </xsl:choose>                                                                "
    wvarStrXSL = wvarStrXSL & "                         </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                     </xsl:choose>"
    
    wvarStrXSL = wvarStrXSL & "                     <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                         <xsl:when test=""CONCEPTO='C'"">"
    wvarStrXSL = wvarStrXSL & "                             <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                                 <xsl:when test=""MOALFCOD='$'"">"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='NETO_PESOS'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""IMPORTE""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='NETO_PESOS_SIGNO'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""SIGNO""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                 </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                                 <xsl:when test=""MOALFCOD='U$S'"">"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='NETO_DOLAR'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""IMPORTE""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='NETO_DOLAR_SIGNO'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""SIGNO""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                 </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                             </xsl:choose>                                                                "
    wvarStrXSL = wvarStrXSL & "                         </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                     </xsl:choose>"
    
    wvarStrXSL = wvarStrXSL & "                     <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                         <xsl:when test=""CONCEPTO='P'"">"
    wvarStrXSL = wvarStrXSL & "                             <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                                 <xsl:when test=""MOALFCOD='$'"">"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='PEND_PESOS'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""IMPORTE""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='PEND_PESOS_SIGNO'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""SIGNO""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                 </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                                 <xsl:when test=""MOALFCOD='U$S'"">"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='PEND_DOLAR'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""IMPORTE""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='PEND_DOLAR_SIGNO'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""SIGNO""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                 </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                             </xsl:choose>                                                                "
    wvarStrXSL = wvarStrXSL & "                         </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                     </xsl:choose>"
    
    wvarStrXSL = wvarStrXSL & "                     <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                         <xsl:when test=""CONCEPTO='D'"">"
    wvarStrXSL = wvarStrXSL & "                             <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                                 <xsl:when test=""MOALFCOD='U$S'"">"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='COTIZ_DOLAR'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""IMPORTE""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                 </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                             </xsl:choose>                                                                "
    wvarStrXSL = wvarStrXSL & "                         </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                     </xsl:choose>"
    
    wvarStrXSL = wvarStrXSL & "                 </xsl:for-each>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "      </xsl:for-each>"
    wvarStrXSL = wvarStrXSL & "   </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PRODUCTOR'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function









