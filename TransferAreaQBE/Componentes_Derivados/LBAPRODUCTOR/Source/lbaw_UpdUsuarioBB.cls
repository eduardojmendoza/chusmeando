VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_UpdUsuarioBB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog
Private mobjHSBC_DBCnn          As HSBCInterfaces.IDBConnection

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_ProductorLBA.lbaw_UpdUsuarioBB"
Const mcteStoreProc             As String = "P_BB_UPDATE_USUARIO"

' Parametros XML de Entrada
Const mcteParam_USUARCOD As String = "//USUARCOD"
Const mcteParam_USUARIOBB As String = "//USUARIOBB"
Const mcteParam_PWDBB As String = "//PWDBB"


Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    '
    ' DATOS GENERALES
     Dim wvarUSUARCOD   As String
     Dim wvarUSUARIOBB  As String
     Dim wvarPWDBB      As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
        '
        wvarStep = 20
        wvarUSUARCOD = .selectSingleNode(mcteParam_USUARCOD).Text
        wvarUSUARIOBB = .selectSingleNode(mcteParam_USUARIOBB).Text
        wvarPWDBB = .selectSingleNode(mcteParam_PWDBB).Text
        '
    End With
    
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 40
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc

    wvarStep = 50
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
        
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARCOD", adChar, adParamInput, 10, wvarUSUARCOD)
    wobjDBParm.Precision = 9
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARIOBB", adChar, adParamInput, 20, wvarUSUARIOBB)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PWDBB", adChar, adParamInput, 600, CapicomEncrypt(wvarPWDBB))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 90
    wobjDBCmd.Execute adExecuteNoRecords

    wvarStep = 100
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value = 0 Then
        wvarMensaje = "Modificacion de contrasena de usuario exitosa."
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & wvarMensaje & Chr(34) & " /></Response>"
    Else
        Select Case wobjDBCmd.Parameters("@RETURN_VALUE").Value
            Case -1
                wvarMensaje = "ERROR AL INSERTAR"
        End Select
        
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & wvarMensaje & Chr(34) & " /></Response>"
    End If
            
    wvarStep = 110
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 120
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub





