VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_PutCuenta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog
Private mobjHSBC_DBCnn          As HSBCInterfaces.IDBConnection

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_ProductorLBA.lbaw_PutCuenta"
Const mcteStoreProc             As String = "SPSNCV_PROD_SIFMCUEN_INSERT"

' Parametros XML de Entrada
' DATOS GENERALES
Const mcteParam_CLIENSEC As String = "//CLIENSEC"
Const mcteParam_TIPOCUEN As String = "//TIPOCUEN"
Const mcteParam_COBROTIP As String = "//COBROTIP"
Const mcteParam_BANCOCOD As String = "//BANCOCOD"
Const mcteParam_SUCURCOD As String = "//SUCURCOD"
Const mcteParam_CUENTDC  As String = "//CUENTDC"
Const mcteParam_CUENNUME As String = "//CUENNUME"
Const mcteParam_TARJECOD As String = "//TARJECOD"
Const mcteParam_VENCIANN As String = "//VENCIANN"
Const mcteParam_VENCIMES As String = "//VENCIMES"
Const mcteParam_VENCIDIA As String = "//VENCIDIA"




Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    '
    ' DATOS GENERALES
     Dim wvarCLIENSEC    As String
     Dim wvarTIPOCUEN    As String
     Dim wvarCOBROTIP    As String
     Dim wvarBANCOCOD    As String
     Dim wvarSUCURCOD    As String
     Dim wvarCUENTDC     As String
     Dim wvarCUENNUME    As String
     Dim wvarTARJECOD    As String
     Dim wvarVENCIANN    As String
     Dim wvarVENCIMES    As String
     Dim wvarVENCIDIA    As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
        '
        wvarStep = 20
        ' DATOS GENERALES
        wvarCLIENSEC = .selectSingleNode(mcteParam_CLIENSEC).Text
        wvarTIPOCUEN = .selectSingleNode(mcteParam_TIPOCUEN).Text
        wvarCOBROTIP = .selectSingleNode(mcteParam_COBROTIP).Text
        wvarBANCOCOD = .selectSingleNode(mcteParam_BANCOCOD).Text
        wvarSUCURCOD = .selectSingleNode(mcteParam_SUCURCOD).Text
        wvarCUENTDC = .selectSingleNode(mcteParam_CUENTDC).Text
        wvarCUENNUME = .selectSingleNode(mcteParam_CUENNUME).Text
        wvarTARJECOD = .selectSingleNode(mcteParam_TARJECOD).Text
        wvarVENCIANN = .selectSingleNode(mcteParam_VENCIANN).Text
        wvarVENCIMES = .selectSingleNode(mcteParam_VENCIMES).Text
        wvarVENCIDIA = .selectSingleNode(mcteParam_VENCIDIA).Text
    End With
    
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 60
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc

    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
        
    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEC", adNumeric, adParamInput, , wvarCLIENSEC)
    wobjDBParm.Precision = 9
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 90
    Set wobjDBParm = wobjDBCmd.CreateParameter("@TIPOCUEN", adChar, adParamInput, 1, wvarTIPOCUEN)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 100
    Set wobjDBParm = wobjDBCmd.CreateParameter("@COBROTIP", adChar, adParamInput, 2, wvarCOBROTIP)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 110
    Set wobjDBParm = wobjDBCmd.CreateParameter("@BANCOCOD", adNumeric, adParamInput, , wvarBANCOCOD)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 120
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SUCURCOD", adNumeric, adParamInput, , wvarSUCURCOD)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing


    wvarStep = 130
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CUENTDC", adChar, adParamInput, 2, wvarCUENTDC)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing


    wvarStep = 140
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CUENNUME", adChar, adParamInput, 22, wvarCUENNUME)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 150
    Set wobjDBParm = wobjDBCmd.CreateParameter("@TARJECOD", adNumeric, adParamInput, , wvarTARJECOD)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 150
    Set wobjDBParm = wobjDBCmd.CreateParameter("@VENCIANN", adNumeric, adParamInput, , wvarVENCIANN)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing


    wvarStep = 160
    Set wobjDBParm = wobjDBCmd.CreateParameter("@VENCIMES", adNumeric, adParamInput, , wvarVENCIMES)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing


    wvarStep = 170
    Set wobjDBParm = wobjDBCmd.CreateParameter("@VENCIDIA", adNumeric, adParamInput, , wvarVENCIDIA)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

''''''''''''''''''''''''''''''''''''''''''''''
   
    wvarStep = 400
    wobjDBCmd.Execute adExecuteNoRecords

    wvarStep = 410
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value > 0 Then
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><CUENSEC>" & wobjDBCmd.Parameters("@RETURN_VALUE").Value & "</CUENSEC></Response>"
    Else
        Select Case wobjDBCmd.Parameters("@RETURN_VALUE").Value
            Case -1
                wvarMensaje = "ERROR AL INSERTAR"
        End Select
        
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & wvarMensaje & " /></Response>"
    End If
            
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 420
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub













