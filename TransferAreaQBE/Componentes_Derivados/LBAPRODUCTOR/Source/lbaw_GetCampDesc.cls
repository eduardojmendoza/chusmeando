VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetCampDesc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context As ObjectContext
Private mobjEventLog    As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName     As String = "lbawA_ProductorLBA.lbaw_GetCampDesc"
Const mcteStoreProc     As String = "SPSNCV_PROD_LBATCAMP_SELECT3"

'Parametros XML de Entrada
Const mcteParam_INSTACOD    As String = "//INSTACOD"
Const mcteParam_RAMOPCOD    As String = "//RAMOPCOD"
Const mcteParam_INSTICOD    As String = "//INSTICOD"
Const mcteParam_CAMPACOD    As String = "//CAMPACOD"
Const mcteParam_AGENTCOD    As String = "//AGENTCOD"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarINSTACOD        As String
    Dim wvarRAMOPCOD        As String
    Dim wvarINSTICOD        As String
    Dim wvarCAMPACOD        As String
    Dim wvarAGENTCOD        As String
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarINSTACOD = .selectSingleNode(mcteParam_INSTACOD).Text
        wvarRAMOPCOD = .selectSingleNode(mcteParam_RAMOPCOD).Text
        wvarINSTICOD = .selectSingleNode(mcteParam_INSTICOD).Text
        wvarCAMPACOD = .selectSingleNode(mcteParam_CAMPACOD).Text
        wvarAGENTCOD = .selectSingleNode(mcteParam_AGENTCOD).Text
    End With
    
    Set wobjXMLRequest = Nothing
    
    wvarStep = 10
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 20
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    '
    wvarStep = 30
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    wvarStep = 40
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = mcteStoreProc
        .CommandType = adCmdStoredProc
    End With
    '
    wvarStep = 50
    Set wobjDBParm = wobjDBCmd.CreateParameter("@INSTACOD", adNumeric, adParamInput, , wvarINSTACOD)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RAMOPCOD", adChar, adParamInput, 4, wvarRAMOPCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@INSTICOD", adNumeric, adParamInput, , wvarINSTICOD)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CAMPACOD", adChar, adParamInput, 4, wvarCAMPACOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 90
    Set wobjDBParm = wobjDBCmd.CreateParameter("@AGENTCOD", adChar, adParamInput, 4, wvarAGENTCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 100
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    '
    wvarStep = 110
    If Not wrstDBResult.EOF Then
        '
        wvarStep = 120
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 130
        wrstDBResult.Save wobjXMLResponse, adPersistXML
        '
        wvarStep = 140
        wobjXSLResponse.async = False
        Call wobjXSLResponse.loadXML(p_GetXSL())
        '
        wvarStep = 150
        wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
        '
        wvarStep = 160
        Set wobjXMLResponse = Nothing
        Set wobjXSLResponse = Nothing
        '
        wvarStep = 170
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        wvarStep = 180
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "NO SE ENCONTRARON DATOS." & Chr(34) & " /></Response>"
    End If
    '
    wvarStep = 190
    Set wobjDBCmd = Nothing
    '
    wvarStep = 200
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 210
    Set wobjDBCnn = Nothing
    '
    wvarStep = 220
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 230
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 240
    Set wrstDBResult = Nothing
    '
    wvarStep = 250
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='CAMPANA'>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CAMPADAB'><xsl:value-of select='@CAMPADAB' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AGENTDAB'><xsl:value-of select='@AGENTDAB' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CAMPADAB'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AGENTDAB'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub







