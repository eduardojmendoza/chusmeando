VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetDomicItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context As ObjectContext
Private mobjEventLog    As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_ProductorLBA.lbaw_GetDomicItem"
Const mcteStoreProc         As String = "SPSNCV_PROD_SIFMDOMI_SELECT"
Const mcteStoreProc_Item    As String = "SPSNCV_PROD_SIFMDOMI_SELECT_ITEM"

'Parametros XML de Entrada
Const mcteParam_CLIENSEC    As String = "//CLIENSEC"
Const mcteParam_DOMICSEC    As String = "//DOMICSEC"
Const mcteParam_UsarNuevoSP As String = "//USARNUEVOSP"


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCLIENSEC        As String
    Dim wvarDOMICSEC        As String
    Dim wvarNameSP          As String
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarCLIENSEC = .selectSingleNode(mcteParam_CLIENSEC).Text
        wvarDOMICSEC = "1"
        If Not .selectSingleNode(mcteParam_DOMICSEC) Is Nothing Then wvarDOMICSEC = .selectSingleNode(mcteParam_DOMICSEC).Text
        If Not .selectSingleNode(mcteParam_UsarNuevoSP) Is Nothing Then
            wvarNameSP = mcteStoreProc_Item
        Else
            wvarNameSP = mcteStoreProc
        End If
    End With
    
    Set wobjXMLRequest = Nothing
    
    wvarStep = 10
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 20
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    '
    wvarStep = 30
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    wvarStep = 40
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = wvarNameSP
        .CommandType = adCmdStoredProc
    End With
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEC", adNumeric, adParamInput, , wvarCLIENSEC)
    wobjDBParm.Precision = 9
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    If wvarNameSP = mcteStoreProc_Item Then
        'Este Parametro es solo para el SP SPSNCV_PROD_SIFMDOMI_SELECT_ITEM
        Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICSEC", adNumeric, adParamInput, , wvarDOMICSEC)
        wobjDBParm.Precision = 3
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    End If
    '
    wvarStep = 50
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    '
    wvarStep = 60
    If Not wrstDBResult.EOF Then
        '
        wvarStep = 70
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 80
        wrstDBResult.Save wobjXMLResponse, adPersistXML
        '
        wvarStep = 90
        wobjXSLResponse.async = False
        Call wobjXSLResponse.loadXML(p_GetXSL())
        '
        wvarStep = 100
        wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
        '
        wvarStep = 120
        Set wobjXMLResponse = Nothing
        Set wobjXSLResponse = Nothing
        '
        wvarStep = 130
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        wvarStep = 140
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "NO SE ENCONTRARON DATOS." & Chr(34) & " /></Response>"
    End If
    wvarStep = 150
    Set wobjDBCmd = Nothing
    '
    wvarStep = 160
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 170
    Set wobjDBCnn = Nothing
    '
    wvarStep = 180
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 190
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 200
    Set wrstDBResult = Nothing
    '
    wvarStep = 210
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='ROW'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='DOMICSEC'><xsl:value-of select='@DOMICSEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='DOMICDOM'><xsl:value-of select='@DOMICDOM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='DOMICDNU'><xsl:value-of select='@DOMICDNU' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='DOMICPIS'><xsl:value-of select='@DOMICPIS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='DOMICPTA'><xsl:value-of select='@DOMICPTA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='DOMICPOB'><xsl:value-of select='@DOMICPOB' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='DOMICCPO'><xsl:value-of select='@DOMICCPO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='PROVICOD'><xsl:value-of select='@PROVICOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='TELCOD'><xsl:value-of select='@TELCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='TELNRO'><xsl:value-of select='@TELNRO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='PRINCIPAL'><xsl:value-of select='@PRINCIPAL' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='PROVIDES'><xsl:value-of select='@PROVIDES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICDOM'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICPOB'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PROVIDES'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub








