VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_UpdDomicilio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog
Private mobjHSBC_DBCnn          As HSBCInterfaces.IDBConnection

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_ProductorLBA.lbaw_UpdDomicilio"
Const mcteStoreProc             As String = "SPSNCV_PROD_SIFMDOMI_UPDATE"

' Parametros XML de Entrada
Const mcteParam_CLIENSEC As String = "//CLIENSEC"
Const mcteParam_DDOMICSEC As String = "//DOMICSEC"
Const mcteParam_DOMICCAL As String = "//DOMICCAL"
Const mcteParam_DOMICDOM As String = "//DOMICDOM"
Const mcteParam_DOMICDNU As String = "//DOMICDNU"
Const mcteParam_DOMICESC As String = "//DOMICESC"
Const mcteParam_DOMICPIS As String = "//DOMICPIS"
Const mcteParam_DOMICPTA As String = "//DOMICPTA"
Const mcteParam_DOMICPOB As String = "//DOMICPOB"
Const mcteParam_DOMICCPO As String = "//DOMICCPO"
Const mcteParam_PROVICOD As String = "//PROVICOD"
Const mcteParam_PAISSCOD As String = "//PAISSCOD"
Const mcteParam_TELCOD As String = "//TELCOD"
Const mcteParam_TELNRO As String = "//TELNRO"
Const mcteParam_PRINCIPAL As String = "//PRINCIPAL"



Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    '
    ' DATOS GENERALES
     Dim wvarCLIENSEC    As String
     Dim wvarDDOMICSEC   As String
     Dim wvarDOMICCAL    As String
     Dim wvarDOMICDOM    As String
     Dim wvarDOMICDNU    As String
     Dim wvarDOMICESC    As String
     Dim wvarDOMICPIS    As String
     Dim wvarDOMICPTA    As String
     Dim wvarDOMICPOB    As String
     Dim wvarDOMICCPO    As String
     Dim wvarPROVICOD    As String
     Dim wvarPAISSCOD    As String
     Dim wvarTELCOD      As String
     Dim wvarTELNRO      As String
     Dim wvarPRINCIPAL   As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
        '
        wvarStep = 20
        wvarCLIENSEC = .selectSingleNode(mcteParam_CLIENSEC).Text
        wvarDDOMICSEC = .selectSingleNode(mcteParam_DDOMICSEC).Text
        wvarDOMICCAL = .selectSingleNode(mcteParam_DOMICCAL).Text
        wvarDOMICDOM = .selectSingleNode(mcteParam_DOMICDOM).Text
        wvarDOMICDNU = .selectSingleNode(mcteParam_DOMICDNU).Text
        wvarDOMICESC = .selectSingleNode(mcteParam_DOMICESC).Text
        wvarDOMICPIS = .selectSingleNode(mcteParam_DOMICPIS).Text
        wvarDOMICPTA = .selectSingleNode(mcteParam_DOMICPTA).Text
        wvarDOMICPOB = .selectSingleNode(mcteParam_DOMICPOB).Text
        wvarDOMICCPO = .selectSingleNode(mcteParam_DOMICCPO).Text
        wvarPROVICOD = .selectSingleNode(mcteParam_PROVICOD).Text
        wvarPAISSCOD = .selectSingleNode(mcteParam_PAISSCOD).Text
        wvarTELCOD = .selectSingleNode(mcteParam_TELCOD).Text
        wvarTELNRO = .selectSingleNode(mcteParam_TELNRO).Text
        wvarPRINCIPAL = .selectSingleNode(mcteParam_PRINCIPAL).Text
    End With
    
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 40
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc

    wvarStep = 50
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
        
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEC", adNumeric, adParamInput, , wvarCLIENSEC)
    wobjDBParm.Precision = 9
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 65
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICSEC", adNumeric, adParamInput, , wvarDDOMICSEC)
    wobjDBParm.Precision = 3
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing


    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICCAL", adChar, adParamInput, 5, wvarDOMICCAL)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICDOM", adChar, adParamInput, 30, wvarDOMICDOM)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 90
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICDNU", adChar, adParamInput, 5, wvarDOMICDNU)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 100
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICESC", adChar, adParamInput, 1, wvarDOMICESC)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 110
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICPIS", adChar, adParamInput, 4, wvarDOMICPIS)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 120
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICPTA", adChar, adParamInput, 4, wvarDOMICPTA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 130
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICPOB", adChar, adParamInput, 60, wvarDOMICPOB)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 140
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICCPO", adNumeric, adParamInput, , wvarDOMICCPO)
    wobjDBParm.Precision = 5
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 150
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PROVICOD", adNumeric, adParamInput, , wvarPROVICOD)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 160
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PAISSCOD", adChar, adParamInput, 2, wvarPAISSCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 170
    Set wobjDBParm = wobjDBCmd.CreateParameter("@TELCOD", adChar, adParamInput, 5, wvarTELCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 180
    Set wobjDBParm = wobjDBCmd.CreateParameter("@TELNRO", adChar, adParamInput, 30, wvarTELNRO)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
   
    wvarStep = 190
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PRINCIPAL", adChar, adParamInput, 1, wvarPRINCIPAL)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 200
    wobjDBCmd.Execute adExecuteNoRecords

    wvarStep = 210
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value = 0 Then
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /></Response>"
    Else
        Select Case wobjDBCmd.Parameters("@RETURN_VALUE").Value
            Case -1
                wvarMensaje = "ERROR AL ACTUALIZAR"
        End Select
        
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & wvarMensaje & " /></Response>"
    End If
            
    wvarStep = 220
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 230
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub


