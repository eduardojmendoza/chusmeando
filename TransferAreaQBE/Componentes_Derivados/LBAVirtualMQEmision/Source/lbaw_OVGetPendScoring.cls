VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVGetPendScoring"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQEmision.lbaw_OVGetPendScoring"
Const mcteOpID              As String = "1520"

'Parametros XML de Entrada
Const mcteParam_Usuario     As String = "//USUARIO"
Const mcteParam_FechaDesde  As String = "//FECHADESDE"
Const mcteParam_FechaHasta  As String = "//FECHAHASTA"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarFechaDesde      As String
    Dim wvarFechaHasta      As String
    Dim wvarFDesdeVal       As String
    Dim wvarFHastaVal       As String
    '
    Dim wvarPos             As Long
    Dim wvarParseString     As String
    Dim wvarStringLen       As Long
    Dim wvarCantLin         As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Parámetros:
        '- Usuario      PIC X(10)
        '- Fecha Desde  PIC 9(8)
        '- Fecha Hasta  PIC 9(8)
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarFechaDesde = .selectSingleNode(mcteParam_FechaDesde).Text
        wvarFechaHasta = .selectSingleNode(mcteParam_FechaHasta).Text
    End With
    '
    'Valido las fechas recibidas, en formato mm/dd/aaaa
    wvarFDesdeVal = Mid(wvarFechaDesde, 5, 2) & "/" & Mid(wvarFechaDesde, 7, 2) & "/" & Mid(wvarFechaDesde, 1, 4)
    If Not IsDate(wvarFDesdeVal) Then
        Err.Raise -1, mcteClassName & "." & wcteFnName, "Parámetro 'Fecha Desde' No Válido."
    End If
    wvarFHastaVal = Mid(wvarFechaHasta, 5, 2) & "/" & Mid(wvarFechaHasta, 7, 2) & "/" & Mid(wvarFechaHasta, 1, 4)
    If Not IsDate(wvarFHastaVal) Then
        Err.Raise -1, mcteClassName & "." & wcteFnName, "Parámetro 'Fecha Hasta' No válido."
    End If
    '
    wvarStep = 60
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 70
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 100
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        If .childNodes.length > 0 Then
            wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
        End If
    End With
    '
    wvarStep = 110
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 120
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarFechaDesde & wvarFechaHasta
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, wvarParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & wvarParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    wvarStep = 240
    wvarResult = ""
    wvarPos = 102  'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarStringLen = Len(wvarParseString)
    '
    wvarStep = 250
    wvarEstado = Mid(wvarParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 260
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 280
        wvarResult = ParseoMensaje(wvarPos, wvarParseString, wvarStringLen, wvarCantLin)
        '
        wvarStep = 310
        If wvarCantLin > 0 Then
            wvarStep = 340
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
            '
        Else
            '
            wvarStep = 350
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>"
            '
        End If
        '
    End If
    '
    wvarStep = 360
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarFechaDesde & wvarFechaHasta & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(pvarPos As Long, pvarParseString As String, pvarStringLen As Long, pvarCantLin As Long) As String
    Dim wvarResult As String
    Dim wvarCantLin As Long
    Dim wvarCounter As Long
    '
    pvarPos = pvarPos + 1
    wvarResult = wvarResult & "<RS>"
    '
    'NOTA:
    'En el doc del mensaje se indica que la salida incluye los campos NIVELCLA y CLIENSEC
    'que son parte de la entrada.
    'Como no es así en el resto de los documentos, entiendo que es un error del doc y
    'solo parseo los 3 ultimos campos particulares de la salida de este mensaje.
    'Tampoco se indica el campo CANTLIN, que supongo tambien vendrá en la salidad
    'ya que se recibe un VECTOR.
    '
    If Trim(Mid(pvarParseString, pvarPos, 3)) = "" Then
        pvarCantLin = 0
    Else
        wvarCantLin = Mid(pvarParseString, pvarPos, 3)
        pvarCantLin = wvarCantLin
        pvarPos = pvarPos + 3
        '
        For wvarCounter = 0 To wvarCantLin - 1
            wvarResult = wvarResult & "<R>"
            wvarResult = wvarResult & "<NOMBRE>" & Mid(pvarParseString, pvarPos, 30) & "</NOMBRE>"
            wvarResult = wvarResult & "<CANTIDAD>" & Mid(pvarParseString, pvarPos + 30, 6) & "</CANTIDAD>"
            wvarResult = wvarResult & "<VIGENTE>" & Mid(pvarParseString, pvarPos + 36, 1) & "</VIGENTE>"
            wvarResult = wvarResult & "</R>"
            pvarPos = pvarPos + 37
        Next
        '
    End If
    wvarResult = wvarResult & "</RS>"
    '
    ParseoMensaje = wvarResult
End Function



