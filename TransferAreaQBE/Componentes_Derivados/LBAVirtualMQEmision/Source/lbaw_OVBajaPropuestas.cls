VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVBajaPropuestas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQEmision.lbaw_OVBajaPropuestas"
Const mcteOpID              As String = "1525"

'Parametros XML de Entrada
Const mcteParam_USUARCOD    As String = "//USUARIO"
Const mcteParam_RAMOPCOD    As String = "//RAMOPCOD"
Const mcteParam_POLIZANN    As String = "//POLIZANN"
Const mcteParam_POLIZSEC    As String = "//POLIZSEC"
Const mcteParam_CERTIPOL    As String = "//CERTIPOL"
Const mcteParam_CERTIANN    As String = "//CERTIANN"
Const mcteParam_CERTISEC    As String = "//CERTISEC"
Const mcteParam_Suplenum    As String = "//SUPLENUM"
Const mcteParam_CAUSA       As String = "//CAUSA"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarRamo            As String
    Dim wvarPolizaAnn       As String
    Dim wvarPolizaSec       As String
    Dim wvarCertiPol        As String
    Dim wvarCertiAnn        As String
    Dim wvarCertiSec        As String
    Dim wvarSuplenum        As String
    Dim wvarCausa           As String
    '
    Dim wvarPos             As Long
    Dim wvarParseString     As String
    Dim wvarStringLen       As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUsuario = Left(.selectSingleNode(mcteParam_USUARCOD).Text & Space(10), 10)
        wvarRamo = Left(.selectSingleNode(mcteParam_RAMOPCOD).Text & Space(4), 4)
        wvarPolizaAnn = Right("00" & .selectSingleNode(mcteParam_POLIZANN).Text, 2)
        wvarPolizaSec = Right("000000" & .selectSingleNode(mcteParam_POLIZSEC).Text, 6)
        wvarCertiPol = Right("0000" & .selectSingleNode(mcteParam_CERTIPOL).Text, 4)
        wvarCertiAnn = Right("0000" & .selectSingleNode(mcteParam_CERTIANN).Text, 4)
        wvarCertiSec = Right("000000" & .selectSingleNode(mcteParam_CERTISEC).Text, 6)
        wvarSuplenum = Right("0000" & .selectSingleNode(mcteParam_Suplenum).Text, 4)
        wvarCausa = Left(.selectSingleNode(mcteParam_CAUSA).Text & Space(60), 60)
    End With
    '
    wvarStep = 60
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 70
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 100
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        If .childNodes.length > 0 Then
            wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
        End If
    End With
    '
    wvarStep = 110
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 120
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & _
                "    000000000    000000000  000000000  000000000" & _
                wvarRamo & wvarPolizaAnn & wvarPolizaSec & wvarCertiPol & _
                wvarCertiAnn & wvarCertiSec & wvarSuplenum
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, wvarParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & wvarParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    wvarStep = 240
    wvarPos = 156  'cantidad de caracteres ocupados por parámetros de entrada
    wvarStringLen = Len(wvarParseString)
    '
    wvarStep = 250
    wvarEstado = Mid(wvarParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 260
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA BAJA' /></Response>"
        '
    Else
        '
        If wvarStringLen > wvarPos Then
            '
            'Armamos la respuesta con el mensaje recibido
            wvarStep = 280
            Response = "<Response>"
            Response = Response & "<Estado resultado='true' mensaje=''/>"
            Response = Response & "<TEXTO>" & Mid(wvarParseString, wvarPos + 1, 30) & "</TEXTO>"
            Response = Response & "</Response>"
            '
        Else
            'No hay datos para devolver
            Response = "<Response><Estado resultado='false' mensaje='No se ha recibido el mensaje de respuesta'/></Response>"
            '
        End If
        '
    End If
    '
    wvarStep = 290
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & _
                        " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & _
                        wvarRamo & wvarPolizaAnn & wvarPolizaSec & wvarCertiPol & _
                        wvarCertiAnn & wvarCertiSec & wvarSuplenum & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

