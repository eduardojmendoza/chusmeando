VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "GetCotizacionATM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Datos de la accion
Const mcteClassName             As String = "LBA_InterWSBrok.GetCotizacionATM"

Const mcteStoreProc             As String = "SPSNCV_BRO_ALTA_OPER"

'Archivo de Configuracion
Const mcteArchivoConfATM_XML    As String = "LBA_PARAM_ATM.XML"
Const mcteArchivoATM_XML        As String = "LBA_VALIDACION_COT_ATM.XML"

'Parametros XML de Entrada
Const mcteParam_REQUESTID            As String = "REQUESTID"
Const mcteParam_CODINST              As String = "CODINST"
Const mcteParam_Provi                As String = "PROVCOD"
Const mcteParam_LocalidadCod         As String = "LOCALIDAD"
Const mcteParam_POLIZANN             As String = "POLIZANN"
Const mcteParam_POLIZSEC             As String = "POLIZSEC"
Const mcteParam_COBROCOD             As String = "FPAGO"
Const mcteParam_COBROTIP             As String = "COBROTIP"
Const mcteParam_CLIENIVA             As String = "CLIENIVA"

'Parámetros agregados
Const mcteParam_NROCOT               As String = "COT_NRO"
Const mcteParam_TipoOperac           As String = "TIPOOPERAC"
Const mcteParam_EstadoProc           As String = "ESTADOPROC"
Const mcteParam_PedidoAno            As String = "MSGANO"
Const mcteParam_PedidoMes            As String = "MSGMES"
Const mcteParam_PedidoDia            As String = "MSGDIA"
Const mcteParam_PedidoHora           As String = "MSGHORA"
Const mcteParam_PedidoMinuto         As String = "MSGMINUTO"
Const mcteParam_PedidoSegundo        As String = "MSGSEGUNDO"
Const mcteParam_TiempoProceso        As String = "TIEMPOPROCESO"
Const mcteParam_CERTISEC             As String = "CERTISEC"
Const mcteParam_RAMOPCOD             As String = "RAMOPCOD"


Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    'declaracion de variables
    Dim wvarMensaje         As String
    Dim wvarCodErr          As String
    Dim pvarRes             As String
    Dim wobjClass           As HSBCInterfaces.IAction

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    pvarRequest = Mid(pvarRequest, 1, InStr(1, pvarRequest, ">")) & _
                  "<MSGANO>" & Year(Now) & "</MSGANO>" & _
                  "<MSGMES>" & Month(Now) & "</MSGMES>" & _
                  "<MSGDIA>" & Day(Now) & "</MSGDIA>" & _
                  "<MSGHORA>" & Hour(Now) & "</MSGHORA>" & _
                  "<MSGMINUTO>" & Minute(Now) & "</MSGMINUTO>" & _
                  "<MSGSEGUNDO>" & Second(Now) & "</MSGSEGUNDO>" & _
                  Mid(pvarRequest, InStr(1, pvarRequest, ">") + 1)
    wvarStep = 10
    pvarRes = ""
    wvarMensaje = ""
    wvarCodErr = ""
    '
    If Not fncGetAll(pvarRequest, wvarMensaje, wvarCodErr, pvarRes) Then
            pvarResponse = pvarRes
        If mcteIfFunctionFailed_failClass Then
            wvarStep = 20
            IAction_Execute = 1
            mobjCOM_Context.SetAbort
        Else
            wvarStep = 30
            IAction_Execute = 0
            mobjCOM_Context.SetComplete
        End If
        Exit Function
    End If
    '
    pvarResponse = pvarRes
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
'
pvarResponse = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
   
    IAction_Execute = 1
    mobjCOM_Context.SetComplete
End Function
Private Function fncGetAll(ByVal pvarRequest As String, _
                           ByRef wvarMensaje As String, _
                           ByRef wvarCodErr As String, _
                           ByRef pvarRes As String) As Boolean
  
  Const wcteFnName             As String = "fncGetAll"
  Dim wvarStep                 As Long
  Dim wobjXMLRequest           As MSXML2.DOMDocument
  Dim wobjXMLParams            As MSXML2.DOMDocument
  Dim wobjXMLReturnVal         As MSXML2.DOMDocument
  Dim wobjXMLError             As MSXML2.DOMDocument
  Dim wobjXMLChildNode         As MSXML2.IXMLDOMNode
  '
  Dim wobjClass                As HSBCInterfaces.IAction
  '
  Dim wvarMensajeStoreProc     As String
  Dim wvarMensajePutTran       As String
  Dim mvar_Estado              As String
  Dim mvar_precio              As String
  Dim mvar_CodZona             As String

  ' variables para calcular tiempo proceso AIS
  Dim mvar_TiempoProceso       As String
  Dim mvarInicioAIS            As String
  Dim mvarFinAIS               As String
  Dim mvarTiempoAIS            As String
  
  
On Error GoTo ErrorHandler

'Le agrega los Nodos de Valor Fijo
  wvarStep = 10
  Set wobjXMLParams = CreateObject("MSXML2.DOMDocument")
  wobjXMLParams.async = False
  Call wobjXMLParams.Load(App.Path & "\" & mcteArchivoConfATM_XML)
  '
  wvarStep = 15
  Call fncInsertNode(pvarRequest, "//Request", wobjXMLParams.selectSingleNode("//BROKERS/ATM/COTIZACION").Text, "TIPOOPERAC")
  Call fncInsertNode(pvarRequest, "//Request", wobjXMLParams.selectSingleNode("//BROKERS/ATM/RAMOPCOD").Text, "RAMOPCOD")
  '
'Instancia la clase de validacion
  wvarMensaje = ""
  wvarMensajeStoreProc = ""
  '
  wvarStep = 20
  Set wobjClass = mobjCOM_Context.CreateInstance("LBA_InterWSBrok.GetValidacionCotATM")
  Call wobjClass.Execute(pvarRequest, wvarMensajeStoreProc, "")
  Set wobjClass = Nothing
  '
'Analizo la respuesta del validador
  wvarStep = 25
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(wvarMensajeStoreProc)
  '
'Si la respuesta viene con error
  wvarStep = 30
  If Not wobjXMLRequest.selectSingleNode("//LBA_WS/@res_code").Text = "0" Then
    wvarCodErr = wobjXMLRequest.selectSingleNode("//LBA_WS/@res_code").Text
    wvarMensaje = wobjXMLRequest.selectSingleNode("//LBA_WS/@res_msg").Text
    If wvarCodErr = "-2" Then
        mvar_Estado = "NI"
    Else
        mvar_Estado = "ER"
    End If
    'En el caso de que no llegue a cotizar, tomo el XML original para el ALTA OPER
    wvarStep = 35
    pvarRes = wvarMensajeStoreProc
    wvarMensajeStoreProc = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """>" & _
                            pvarRequest & _
                            "</LBA_WS>"
  Else
  
    'En el caso de que la Validacion esté Ok, obtengo la Cotizacion
    'De la validación vienen los datos de NROCOTI , CERTISEC = 0 , PLANDEFEC/TPLANATM
    wvarStep = 40
    
    mvarInicioAIS = Now()
    'Cotizo . Llamo al msg genérico 2200
    If fncCotizarAtmMQ(wvarMensajeStoreProc, wvarMensaje, wvarCodErr, pvarRes) Then
    mvarFinAIS = Now()
        ' devuelvo pvarRes con precio y prima
        wvarStep = 45
        Set wobjXMLReturnVal = CreateObject("MSXML2.DOMDocument")
        wobjXMLReturnVal.async = False
        Call wobjXMLReturnVal.loadXML(pvarRes)
            If wobjXMLReturnVal.selectSingleNode("//Response/Estado/@resultado").Text = "false" Then
               mvar_Estado = "ER"
            Else
               mvar_Estado = wobjXMLReturnVal.selectSingleNode("//Response/Estado/@resultado").Text
            End If
        wvarStep = 50
            If Not wobjXMLReturnVal.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
              'hubo un error
              pvarRes = "<LBA_WS res_code=""-300"" res_msg=""" & wobjXMLReturnVal.selectSingleNode("//Response/Estado/@mensaje").Text & """></LBA_WS>"
            Else
            
          ' Devuelvo el mensaje de salida
           pvarRes = "<LBA_WS res_code=""0"" res_msg="""">" & pvarRes & "</LBA_WS>"
            
            'Tomo el valor del Precio
              wvarStep = 90
            'EG 10/2010 No se toma en cuenta  la zona
             '  mvar_precio = wobjXMLReturnVal.selectSingleNode("//RECTOIMP").Text
              mvar_CodZona = "0"
              
              mvar_TiempoProceso = DateDiff("S", mvarInicioAIS, mvarFinAIS)
              
              wvarStep = 96
              Set wobjXMLReturnVal = Nothing
              '

            End If
          End If
          
      ' paso el estado para guardar en la tabla BRO_OPERACIONES
      Call fncInsertNode(wvarMensajeStoreProc, "//LBA_WS/Request", mvar_Estado, "ESTADOPROC")
      
      ' paso el tiempo de proceso AIS para guardar en la tabla BRO_OPERACIONES
      Call fncInsertNode(wvarMensajeStoreProc, "//LBA_WS/Request", mvar_TiempoProceso, "TIEMPOPROCESO")
    
    'Alta de la OPERACION . SPSNCV_BRO_ALTA_OPER
        wvarStep = 130
        '
            If fncPutCotHO(wvarMensajeStoreProc, wvarMensaje, wvarCodErr, wvarMensajePutTran) Then
                Set wobjXMLReturnVal = CreateObject("MSXML2.DOMDocument")
                wobjXMLReturnVal.async = False
                Call wobjXMLReturnVal.loadXML(wvarMensajePutTran)
                wvarStep = 140
                If Not wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text = "0" Then
                    wvarStep = 150
                    wvarCodErr = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text
                    wvarMensaje = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_msg").Text
                    pvarRes = wvarMensajePutTran
                    Exit Function
                End If
                Set wobjXMLReturnVal = Nothing
                Set wobjClass = Nothing
                fncGetAll = True
            End If
    '
End If


fin:
  Set wobjClass = Nothing
  Set wobjXMLRequest = Nothing
  Set wobjXMLParams = Nothing
  Set wobjXMLReturnVal = Nothing
  Set wobjXMLError = Nothing
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
wvarCodErr = mcteErrorInesperadoCod

    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    fncGetAll = False
    mobjCOM_Context.SetComplete

End Function
Private Function fncInsertNode(ByRef pvarRequest, _
                               ByVal pvarHeader, _
                               ByVal pvarNodeValue, _
                               ByVal pvarNodeName) As Boolean
    
  Const wcteFnName      As String = "fncInsertNode"
  Dim wvarStep          As Long
  Dim wobjXMLRequest    As MSXML2.DOMDocument

  On Error GoTo ErrorHandler
  '
  wvarStep = 10
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRequest)
  '
  wvarStep = 20
  Call wobjXMLRequest.selectSingleNode(pvarHeader).appendChild(wobjXMLRequest.createNode(NODE_ELEMENT, pvarNodeName, ""))
  wobjXMLRequest.selectSingleNode("//" & pvarNodeName).Text = pvarNodeValue
  '
  pvarRequest = wobjXMLRequest.xml
  Set wobjXMLRequest = Nothing
  fncInsertNode = True
  '
fin:
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
'AM Debug
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & "-" & pvarNodeName, _
                     vbLogEventTypeError
    
    fncInsertNode = False
    mobjCOM_Context.SetComplete

End Function


Private Function fncPutCotHO(ByVal pvarRequest As String, _
                           ByRef wvarMensaje As String, _
                           ByRef wvarCodErr As String, _
                           ByRef pvarRes As String) As Boolean

  Const wcteFnName                    As String = "fncPutCotHO"
  Dim wvarStep                        As Long
  '
  Dim wobjClass                        As HSBCInterfaces.IAction
  Dim wobjHSBC_DBCnn                   As HSBCInterfaces.IDBConnection
  Dim wobjXMLRequest                   As MSXML2.DOMDocument
  '
  Dim wobjDBCnn                        As ADODB.Connection
  Dim wobjDBCmd                        As ADODB.Command
  Dim wobjDBParm                       As ADODB.Parameter
  Dim wrstRes                          As ADODB.Recordset
  '
  Dim mvarWDB_CODINST                  As String
  Dim mvarWDB_REQUESTID                As String
  Dim mvarWDB_NROCOT                   As String
  Dim mvarWDB_TIPOOPERAC               As String
  Dim mvarWDB_ESTADO                   As String
  Dim mvarWDB_RECEPCION_PEDIDOANO      As String
  Dim mvarWDB_RECEPCION_PEDIDOMES      As String
  Dim mvarWDB_RECEPCION_PEDIDODIA      As String
  Dim mvarWDB_RECEPCION_PEDIDOHORA     As String
  Dim mvarWDB_RECEPCION_PEDIDOMINUTO   As String
  Dim mvarWDB_RECEPCION_PEDIDOSEGUNDO  As String
  Dim mvarWDB_RAMOPCOD                 As String
  Dim mvarWDB_POLIZANN                 As String
  Dim mvarWDB_POLIZSEC                 As String
  Dim mvarWDB_CERTISEC                 As String
  Dim mvarWDB_TIEMPOPROCESO            As String
  Dim mvarWDB_XML                      As String

On Error GoTo ErrorHandler

'Alta de la OPERACION
  wvarStep = 10
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRequest)
  '
  wvarStep = 20
  mvarWDB_CODINST = 0
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST) Is Nothing Then
     mvarWDB_CODINST = wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST).Text
  End If
  '
  wvarStep = 30
  mvarWDB_REQUESTID = 0
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_REQUESTID) Is Nothing Then
     mvarWDB_REQUESTID = wobjXMLRequest.selectSingleNode("//" & mcteParam_REQUESTID).Text
  End If
  '
  wvarStep = 40
  mvarWDB_NROCOT = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_NROCOT) Is Nothing Then
     mvarWDB_NROCOT = wobjXMLRequest.selectSingleNode("//" & mcteParam_NROCOT).Text
  End If
  '
  wvarStep = 50
  mvarWDB_TIPOOPERAC = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_TipoOperac) Is Nothing Then
     mvarWDB_TIPOOPERAC = wobjXMLRequest.selectSingleNode("//" & mcteParam_TipoOperac).Text
  End If
  '
  wvarStep = 60
  mvarWDB_ESTADO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_EstadoProc) Is Nothing Then
    'EG 10/2010 cambia el valor del tag al actualizarce componente
     mvarWDB_ESTADO = wobjXMLRequest.selectSingleNode("//" & mcteParam_EstadoProc).Text
     If mvarWDB_ESTADO = "true" Then
        mvarWDB_ESTADO = "OK"
     Else
        mvarWDB_ESTADO = "ER"
     End If
   End If
  '
  wvarStep = 70
  mvarWDB_RECEPCION_PEDIDOANO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoAno) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOANO = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoAno).Text
  End If
  '
  wvarStep = 80
  mvarWDB_RECEPCION_PEDIDOMES = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoMes) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOMES = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoMes).Text
  End If
  '
  wvarStep = 90
  mvarWDB_RECEPCION_PEDIDODIA = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoDia) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDODIA = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoDia).Text
  End If
  '
  wvarStep = 100
  mvarWDB_RECEPCION_PEDIDOHORA = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoHora) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOHORA = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoHora).Text
  End If
  '
  wvarStep = 110
  mvarWDB_RECEPCION_PEDIDOMINUTO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoMinuto) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOMINUTO = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoMinuto).Text
  End If
  '
  wvarStep = 120
  mvarWDB_RECEPCION_PEDIDOSEGUNDO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoSegundo) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOSEGUNDO = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoSegundo).Text
  End If
  '
  wvarStep = 130
  mvarWDB_RAMOPCOD = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_RAMOPCOD) Is Nothing Then
    mvarWDB_RAMOPCOD = wobjXMLRequest.selectSingleNode("//" & mcteParam_RAMOPCOD).Text
  End If
  '
  wvarStep = 140
  mvarWDB_POLIZANN = 0
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZANN) Is Nothing Then
     If IsNumeric(wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZANN).Text) Then
        If wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZANN).Text < 100 Then
            mvarWDB_POLIZANN = Val(wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZANN).Text)
        End If
    End If
  End If
  '
  wvarStep = 150
  mvarWDB_POLIZSEC = 0
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZSEC) Is Nothing Then
     If IsNumeric(wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZSEC).Text) Then
        If wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZSEC).Text < 1000000 Then
             mvarWDB_POLIZSEC = wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZSEC).Text
        End If
     End If
  End If
  '
  wvarStep = 160
  mvarWDB_CERTISEC = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_CERTISEC) Is Nothing Then
     mvarWDB_CERTISEC = wobjXMLRequest.selectSingleNode("//" & mcteParam_CERTISEC).Text
  End If
  '
  wvarStep = 170
  mvarWDB_TIEMPOPROCESO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_TiempoProceso) Is Nothing Then
     mvarWDB_TIEMPOPROCESO = wobjXMLRequest.selectSingleNode("//" & mcteParam_TiempoProceso).Text
  End If
  '
  wvarStep = 180
  mvarWDB_XML = ""
  mvarWDB_XML = pvarRequest
  '
  wvarStep = 190
  Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
  Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
  '
  Set wobjDBCmd = CreateObject("ADODB.Command")
  Set wobjDBCmd.ActiveConnection = wobjDBCnn
  wobjDBCmd.CommandText = mcteStoreProc
  wobjDBCmd.CommandType = adCmdStoredProc
  '
  wvarStep = 200
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_IDENTIFICACION_BROKER", adNumeric, adParamInput, , IIf(mvarWDB_CODINST = "", 0, mvarWDB_CODINST))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 210
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_NRO_OPERACION_BROKER", adNumeric, adParamInput, , IIf(mvarWDB_REQUESTID = "", 0, mvarWDB_REQUESTID))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 14
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 220
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_NRO_COTIZACION_LBA", adNumeric, adParamInput, , IIf(mvarWDB_NROCOT = "", 0, mvarWDB_NROCOT))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 18
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 230
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_TIPO_OPERACION", adChar, adParamInput, 1, IIf(mvarWDB_TIPOOPERAC = "", "", mvarWDB_TIPOOPERAC))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 240
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_ESTADO", adChar, adParamInput, 2, IIf(mvarWDB_ESTADO = "", "", mvarWDB_ESTADO))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 250
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_RECEPCION_PEDIDO", adDate, adParamInput, 8, _
                                         mvarWDB_RECEPCION_PEDIDOANO & "-" & _
                                         mvarWDB_RECEPCION_PEDIDOMES & "-" & _
                                         mvarWDB_RECEPCION_PEDIDODIA & " " & _
                                         mvarWDB_RECEPCION_PEDIDOHORA & ":" & _
                                         mvarWDB_RECEPCION_PEDIDOMINUTO & ":" & _
                                         mvarWDB_RECEPCION_PEDIDOSEGUNDO)
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 260
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_ENVIO_RESPUESTA", adDate, adParamInput, 8, _
                                         Year(Now) & "-" & _
                                         Month(Now) & "-" & _
                                         Day(Now) & " " & _
                                         Hour(Now) & ":" & _
                                         Minute(Now) & ":" & _
                                         Second(Now))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 270
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_RAMOPCOD", adChar, adParamInput, 4, IIf(mvarWDB_RAMOPCOD = "", "", mvarWDB_RAMOPCOD))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 280
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_POLIZANN", adNumeric, adParamInput, , IIf(mvarWDB_POLIZANN = "", 0, mvarWDB_POLIZANN))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 290
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_POLIZSEC", adNumeric, adParamInput, , IIf(mvarWDB_POLIZSEC = "", 0, mvarWDB_POLIZSEC))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 6
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 300
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CERTIPOL", adNumeric, adParamInput, , IIf(mvarWDB_CODINST = "", 0, mvarWDB_CODINST))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 310
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CERTIANN", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 320
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CERTISEC", adNumeric, adParamInput, , IIf(mvarWDB_CERTISEC = "", 0, mvarWDB_CERTISEC))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 6
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 330
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_TIEMPO_PROCESO_AIS", adNumeric, adParamInput, , IIf(mvarWDB_TIEMPOPROCESO = "", 0, mvarWDB_TIEMPOPROCESO))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 6
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 340
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_OPERACION_XML", adVarChar, adParamInput, 8000, IIf(mvarWDB_XML = "", "", Left(mvarWDB_XML, 8000)))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 350
  '
  wobjDBCmd.Execute adExecuteNoRecords
  '
  wobjDBCnn.Close
  '
  wvarStep = 360
  fncPutCotHO = True
  pvarRes = "<LBA_WS res_code=""" & wobjXMLRequest.selectSingleNode("//LBA_WS/@res_code").Text & """ res_msg=""" & wobjXMLRequest.selectSingleNode("//LBA_WS/@res_msg").Text & """></LBA_WS>"
fin:
'libero los objectos
  Set wrstRes = Nothing
  Set wobjDBCmd = Nothing
  Set wobjDBCnn = Nothing
  Set wobjHSBC_DBCnn = Nothing
  Set wobjXMLRequest = Nothing
  Set wobjClass = Nothing
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
wvarCodErr = mcteErrorInesperadoCod

    fncPutCotHO = False
    mobjCOM_Context.SetComplete


End Function


Private Function fncCotizarAtmMQ(ByVal pvarRequest As String, _
                                ByRef wvarMensaje As String, _
                                ByRef wvarCodErr As String, _
                                ByRef pvarRes As String) As Boolean
  
  Const wcteFnName              As String = "fncCotizarAtmMQ"
  Dim wvarStep    As Long
  
  Dim mvarRequest As String
  Dim mvarResponse As String
  
  Dim oProd
  Dim wobjXMLRequest As MSXML2.DOMDocument
  Dim mobjXMLDoc   As MSXML2.DOMDocument
  Dim wobjClass    As HSBCInterfaces.IAction
  Dim mvarProdok As String
  Dim wobjXMLList               As MSXML2.IXMLDOMNodeList
  Dim wvarContNode              As Long
  Dim vNROCOTI, vMES, vANO, vFECINIVG, vMESsig, vANOsig As String
  Dim vFECFINVG, vTPLANATM, vPOLIZANN, vPOLIZSEC, vREQUESTID, vUSUARCOD As String
  

  wvarStep = 0
  wvarCodErr = 0
  On Error GoTo ErrorHandler

  wvarStep = 2
  ' Traigo los parámetros del request
   Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
   wobjXMLRequest.async = False
   Call wobjXMLRequest.loadXML(pvarRequest)
              '
  wvarStep = 3
  vNROCOTI = wobjXMLRequest.selectSingleNode("//LBA_WS/Request/COT_NRO").Text
  vREQUESTID = wobjXMLRequest.selectSingleNode("//LBA_WS/Request/REQUESTID").Text
  
    Dim Primer As String
    
  vFECINIVG = convertirFecha(DateSerial(Year(Now()), Month(Now()), 1))
  vFECFINVG = convertirFecha(DateSerial(Year(Now()), Month(Now()) + 1, 1))
  vPOLIZANN = wobjXMLRequest.selectSingleNode("//LBA_WS/Request/POLIZANN").Text
  vPOLIZSEC = wobjXMLRequest.selectSingleNode("//LBA_WS/Request/POLIZSEC").Text
  vTPLANATM = wobjXMLRequest.selectSingleNode("//LBA_WS/Request/TPLANATM").Text
  vUSUARCOD = wobjXMLRequest.selectSingleNode("//LBA_WS/Request/USUARCOD").Text
 
  'Armo el Request para cotizar
  mvarRequest = "<Request><DEFINICION>2200_OVMsjGralCotizacion.xml</DEFINICION>"
  mvarRequest = mvarRequest & "  <NROCOTI>" & vNROCOTI & "</NROCOTI>"
  mvarRequest = mvarRequest & "  <RAMOPCOD>ATD1</RAMOPCOD>"
  mvarRequest = mvarRequest & "  <POLIZANN>" & vPOLIZANN & "</POLIZANN>"
  mvarRequest = mvarRequest & "  <POLIZSEC>" & vPOLIZSEC & "</POLIZSEC>"
  mvarRequest = mvarRequest & "  <FECSOLIC>0</FECSOLIC>"
  mvarRequest = mvarRequest & "  <FECINIVG>" & vFECINIVG & "</FECINIVG>"
  mvarRequest = mvarRequest & "  <FECFINVG>" & vFECFINVG & "</FECFINVG>"
  mvarRequest = mvarRequest & "  <AGENTCLAPR1>PR</AGENTCLAPR1>"
  mvarRequest = mvarRequest & "  <AGENTCODPR1>0</AGENTCODPR1>"
  mvarRequest = mvarRequest & "  <AGENTCLAPR2/>"
  mvarRequest = mvarRequest & "  <AGENTCODPR2>0</AGENTCODPR2>"
  mvarRequest = mvarRequest & "  <AGENTCLAOR>OR</AGENTCLAOR>"
  mvarRequest = mvarRequest & "  <AGENTCODOR>0</AGENTCODOR>"
  mvarRequest = mvarRequest & "  <COMISIONPR1/>"
  mvarRequest = mvarRequest & "  <COMISIONPR2>0</COMISIONPR2>"
  mvarRequest = mvarRequest & "  <COMISIONOR/>"
  mvarRequest = mvarRequest & "  <PLANPCOD>0</PLANPCOD>"
  mvarRequest = mvarRequest & "  <COBROCOD>0</COBROCOD>"
  mvarRequest = mvarRequest & "  <COBROTIP/>"
  mvarRequest = mvarRequest & "  <PROFECOD/>"
  mvarRequest = mvarRequest & "  <OPERATIP>AP</OPERATIP>"
  mvarRequest = mvarRequest & "  <COBROFOR>0</COBROFOR>"
  mvarRequest = mvarRequest & "  <CAUSAEND>0</CAUSAEND>"
  mvarRequest = mvarRequest & "  <CLIENTIP>0</CLIENTIP>"
  mvarRequest = mvarRequest & "  <CLIENSEX/>"
  mvarRequest = mvarRequest & "  <FECNACIM>0</FECNACIM>"
  mvarRequest = mvarRequest & "  <CLIENEST/>"
  mvarRequest = mvarRequest & "  <NACIONAL>00</NACIONAL>"
  mvarRequest = mvarRequest & "  <CLIENIVA>3</CLIENIVA>"
  mvarRequest = mvarRequest & "  <CLIEIBTP>0</CLIEIBTP>"
  mvarRequest = mvarRequest & "  <CLAUAJUS>0</CLAUAJUS>"
  mvarRequest = mvarRequest & "  <TPLANATM>" & vTPLANATM & "</TPLANATM>"
  mvarRequest = mvarRequest & "  <PARTICOD>0</PARTICOD>"
  mvarRequest = mvarRequest & "  <LOCALCOD>0</LOCALCOD>"
  mvarRequest = mvarRequest & "  <DOMICCPO>X0000</DOMICCPO>"
  mvarRequest = mvarRequest & "  <PROVICOD>1</PROVICOD>"
  mvarRequest = mvarRequest & "  <PAISSCOD>00</PAISSCOD></Request>"

  Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVMQGen.lbaw_MQMensaje")
  Call wobjClass.Execute(mvarRequest, mvarResponse, "")
  Set wobjClass = Nothing
  Set mobjXMLDoc = CreateObject("MSXML2.DOMDocument")
  mobjXMLDoc.async = False
  Call mobjXMLDoc.loadXML(mvarResponse)
    
  mvarProdok = "ER"
  If Not mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
    If (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").Text) = "true" Then
        'Armo la salida
        pvarRes = ""
        With mobjXMLDoc
            pvarRes = pvarRes & "<Response><Estado resultado='true' mensaje='' />"
            pvarRes = pvarRes & "<REQUESTID>" & vREQUESTID & "</REQUESTID>"
            pvarRes = pvarRes & "<COT_NRO>" & vNROCOTI & "</COT_NRO>"
            pvarRes = pvarRes & "<TPLANATM>" & vTPLANATM & "</TPLANATM>"
            pvarRes = pvarRes & "<USUARCOD>" & vUSUARCOD & "</USUARCOD>"
            pvarRes = pvarRes & "<COMP_PRECIO>"
            pvarRes = pvarRes & "<PRIMA>" & .selectSingleNode("//PRIMAIMP").Text & "</PRIMA>"
            pvarRes = pvarRes & "<RECARGOS>" & .selectSingleNode("//RECAIMPO").Text & "</RECARGOS>"
            pvarRes = pvarRes & "<IVAIMPOR>" & .selectSingleNode("//IVAIMPOR").Text & "</IVAIMPOR>"
            pvarRes = pvarRes & "<IVAIMPOA>" & .selectSingleNode("//IVAIMPOA").Text & "</IVAIMPOA>"
            pvarRes = pvarRes & "<IVARETEN>" & .selectSingleNode("//IVARETEN").Text & "</IVARETEN>"
            pvarRes = pvarRes & "<DEREMI>" & .selectSingleNode("//DEREMIMP").Text & "</DEREMI>"
            pvarRes = pvarRes & "<SELLADO>" & .selectSingleNode("//SELLAIMP").Text & "</SELLADO>"
            pvarRes = pvarRes & "<INGBRU>" & .selectSingleNode("//INGBRIMP").Text & "</INGBRU>"
            pvarRes = pvarRes & "<IMPUES>" & .selectSingleNode("//IMPUEIMP").Text & "</IMPUES>"
            pvarRes = pvarRes & "<PRECIO>" & .selectSingleNode("//RECTOIMP").Text & "</PRECIO>"
            pvarRes = pvarRes & "</COMP_PRECIO></Response>"
       
        End With
        'pvarRes = mvarResponse
        mvarProdok = "OK"
    End If
  End If
        
  If mvarProdok = "ER" Then
     wvarCodErr = -53
     pvarRes = "<Response><Estado resultado=""false"" mensaje=Poliza  no habilitada /></Response>"
     Exit Function
  End If

        
  Set mobjXMLDoc = Nothing
  Set wobjXMLRequest = Nothing
  Set wobjXMLList = Nothing
        
  wvarStep = 240
  If wvarCodErr = 0 Then
      fncCotizarAtmMQ = True
  Else
      fncCotizarAtmMQ = False
  End If
  
fin:
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<Response><Estado resultado=""false"" mensaje=""" & mcteErrorInesperadoDescr & """ /></Response>"
wvarCodErr = -1000
  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  'fncCotizarAtmMQ False
  GoTo fin
                                
End Function












