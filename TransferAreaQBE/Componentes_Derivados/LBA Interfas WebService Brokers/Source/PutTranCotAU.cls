VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "PutTranCotAU"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements ObjectControl
Implements HSBCInterfaces.IAction

Const mcteClassName         As String = "LBA_InterWSBrok.PutTranCot"
Const mcteStoreProc         As String = "sp_NOMBRESTORE"

Dim mobjCOM_Context     As ObjectContext
Dim mobjEventLog        As HSBCInterfaces.IEventLog

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    
    'declaracion de variables
    Dim wvarMensaje         As String
    Dim pvarRes             As String

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
   
    pvarRes = ""
    If Not fncPutAll(pvarRequest, wvarMensaje, pvarRes) Then
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & wvarMensaje & Chr(34) & " /></Response>"
        If mcteIfFunctionFailed_failClass Then
            wvarStep = 10
            IAction_Execute = 1
            mobjCOM_Context.SetAbort
        Else
            wvarStep = 20
            IAction_Execute = 0
            mobjCOM_Context.SetComplete
        End If
        Exit Function
    End If
    
    pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & pvarRes & "</Response>"
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarResponse = "<Response><Estado resultado=""false"" mensaje=""" & mcteErrorInesperadoDescr & """ /></Response>"

    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function fncPutAll(ByVal pvarRequest As String, _
                           ByRef wvarMensaje As String, _
                           ByRef pvarRes As String) As Boolean ''

End Function
                           

