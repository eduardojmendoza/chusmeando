Attribute VB_Name = "Funciones"
Option Explicit

Function ValidarFechayRango(ByRef pvarStrFecha As String, _
                            ByVal pvarIntervaloMenor As Integer, _
                            ByVal pvarIntervaloMayor As Integer, _
                            ByRef pvarMensaje As String) As Boolean
  Dim wvarDatFecha As Date
  
  Dim wvarIntAnio As Integer
  Dim wvarIntMes As Integer
  Dim wvarIntDia As Integer
  
  
  Dim warrSplit() As String
  
  On Error GoTo ErrValidarFecha

  If Not validarFecha(pvarStrFecha, pvarMensaje) Then
    ValidarFechayRango = False
    Exit Function
  End If

  If Len(pvarStrFecha) <> 10 Then
    pvarMensaje = "Formato de fecha invalido"
    ValidarFechayRango = False
    Exit Function
  End If
  
  warrSplit = Split(pvarStrFecha, "/")
  wvarIntAnio = CInt(warrSplit(2))
  wvarIntMes = CInt(warrSplit(1))
  wvarIntDia = CInt(warrSplit(0))
  
  wvarDatFecha = DateSerial(wvarIntAnio, wvarIntMes, wvarIntDia)
  
  If wvarDatFecha > DateAdd("yyyy", -pvarIntervaloMenor, Date) Or _
    wvarDatFecha < DateAdd("yyyy", -pvarIntervaloMayor, Date) Then
    pvarMensaje = "La edad permitida comprende entre " & pvarIntervaloMenor & " - " & pvarIntervaloMayor & " Anos"
    ValidarFechayRango = False
    Exit Function
  End If

  ValidarFechayRango = True
  Exit Function
ErrValidarFecha:
  ValidarFechayRango = False
End Function

Function validarFecha(ByRef pvarStrFecha As String, _
                      ByRef pvarMensaje As String) As Boolean

  Dim wvarIntAnio As Integer
  Dim wvarIntMes As Integer
  Dim wvarIntDia As Integer
  Dim warrSplit() As String


  warrSplit = Split(pvarStrFecha, "/")
  wvarIntAnio = CInt(warrSplit(2))
  wvarIntMes = CInt(warrSplit(1))
  wvarIntDia = CInt(warrSplit(0))

  pvarMensaje = ""
  validarFecha = True
  
  If Int(wvarIntAnio / 4) = (wvarIntAnio / 4) Then
    If wvarIntMes = 2 Then
       If wvarIntDia > 29 Then validarFecha = False
    End If
  Else
    If wvarIntMes = 2 Then
       If wvarIntDia > 28 Then validarFecha = False
    End If
  End If
  If wvarIntMes = 4 Then
     If wvarIntDia > 30 Then validarFecha = False
  End If
  If wvarIntMes = 6 Then
     If wvarIntDia > 30 Then validarFecha = False
  End If
  If wvarIntMes = 9 Then
     If wvarIntDia > 30 Then validarFecha = False
  End If
  If wvarIntMes = 11 Then
     If wvarIntDia > 30 Then validarFecha = False
  End If
  If wvarIntMes = 1 Or wvarIntMes = 3 Or wvarIntMes = 5 Or wvarIntMes = 7 Or _
     wvarIntMes = 8 Or wvarIntMes = 10 Or wvarIntMes = 12 Then
     If wvarIntDia > 31 Then validarFecha = False
  End If
  If wvarIntMes > 12 Then validarFecha = False
  
  If Not validarFecha Then pvarMensaje = "Formato de fecha invalido"
  
End Function

Function convertirFecha(ByRef pvarStrFecha As String) As String

  Dim wvarIntAnio As String
  Dim wvarIntMes As String
  Dim wvarIntDia As String
  Dim warrSplit() As String

  pvarStrFecha = Format(pvarStrFecha, "General Date")
  
  wvarIntAnio = Year(pvarStrFecha)
  wvarIntMes = Right("0" & Month(pvarStrFecha), 2)
  wvarIntDia = Right("0" & Day(pvarStrFecha), 2)
  
  convertirFecha = wvarIntAnio & wvarIntMes & wvarIntDia
  
End Function



