VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "GetVehiculo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Objetos del FrameWork
Private mobjCOM_Context           As ObjectContext
Private mobjEventLog              As HSBCInterfaces.IEventLog

'Datos de la accion
Const mcteClassName               As String = "LBA_InterWSBrok.GetVehiculo"

Const mcteStoreProc               As String = "SPSNCV_BRO_ALTA_OPER"

'Parametros XML de Entrada
Const mcteParam_REQUESTID            As String = "REQUESTID"
Const mcteParam_CODINST              As String = "CODINST"

Const mcteParam_Marca                As String = "MARCA"
Const mcteParam_Modelo               As String = "MODELO"
Const mcteParam_EfectAnn             As String = "EFECTANN"


Const mcteParam_EstadoProc           As String = "ESTADOPROC"
Const mcteParam_TiempoProceso        As String = "TIEMPOPROCESO"
Const mcteParam_PedidoAno            As String = "MSGANO"
Const mcteParam_PedidoMes            As String = "MSGMES"
Const mcteParam_PedidoDia            As String = "MSGDIA"
Const mcteParam_PedidoHora           As String = "MSGHORA"
Const mcteParam_PedidoMinuto         As String = "MSGMINUTO"
Const mcteParam_PedidoSegundo        As String = "MSGSEGUNDO"
Const mcteParam_USUARCOD             As String = "USUARCOD"



Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    
    'declaracion de variables
    Dim wvarMensaje         As String
    Dim wvarCodErr          As String
    Dim pvarRes             As String

Dim wobjClass           As HSBCInterfaces.IAction

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    pvarRequest = Mid(pvarRequest, 1, InStr(1, pvarRequest, ">")) & _
                  "<MSGANO>" & Year(Now) & "</MSGANO>" & _
                  "<MSGMES>" & Month(Now) & "</MSGMES>" & _
                  "<MSGDIA>" & Day(Now) & "</MSGDIA>" & _
                  "<MSGHORA>" & Hour(Now) & "</MSGHORA>" & _
                  "<MSGMINUTO>" & Minute(Now) & "</MSGMINUTO>" & _
                  "<MSGSEGUNDO>" & Second(Now) & "</MSGSEGUNDO>" & _
                  Mid(pvarRequest, InStr(1, pvarRequest, ">") + 1)
    wvarStep = 10
    wvarMensaje = ""
    wvarCodErr = ""
    pvarRes = ""
    '
    If Not fncGetAll(pvarRequest, wvarMensaje, wvarCodErr, pvarRes) Then
            pvarResponse = pvarRes
        If mcteIfFunctionFailed_failClass Then
            wvarStep = 20
            IAction_Execute = 1
            mobjCOM_Context.SetAbort
        Else
            wvarStep = 30
            IAction_Execute = 0
            mobjCOM_Context.SetComplete
        End If
        Exit Function
    End If
    
    pvarResponse = pvarRes
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarResponse = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
    
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
   
    IAction_Execute = 1
    mobjCOM_Context.SetComplete
End Function
Private Function fncGetAll(ByVal pvarRequest As String, _
                           ByRef wvarMensaje As String, _
                           ByRef wvarCodErr As String, _
                           ByRef pvarRes As String) As Boolean

  Const wcteFnName         As String = "fncGetAll"
  Dim wvarStep             As Long
  Dim wobjXMLRequest       As MSXML2.DOMDocument
  Dim wobjXMLReturnVal     As MSXML2.DOMDocument
  Dim wobjXMLError         As MSXML2.DOMDocument
  Dim wobjXMLRequestExists As MSXML2.DOMDocument
  '
  Dim wobjClass            As HSBCInterfaces.IAction
  '
  Dim wvarMensajeStoreProc As String
  Dim wvarMensajePutTran   As String
  Dim mvar_Estado          As String
  Dim mvarCertiSec         As String
  '
  Dim wobjHSBC_DBCnn       As HSBCInterfaces.IDBConnection
  Dim wobjDBCnn            As ADODB.Connection
  Dim wobjDBCmd            As ADODB.Command
  Dim wobjDBParm           As ADODB.Parameter
  Dim wrstRes              As ADODB.Recordset
  Dim pTiempoAIS           As String
  
  
On Error GoTo ErrorHandler

'Instancia la clase de validacion
  wvarMensaje = ""
  wvarMensajeStoreProc = ""
  '
  wvarStep = 20
  Set wobjClass = mobjCOM_Context.CreateInstance("LBA_InterWSBrok.GetValidacionVehiculo")
  Call wobjClass.Execute(pvarRequest, wvarMensajeStoreProc, "")
  Set wobjClass = Nothing
  '
'Analizo la respuesta del validador
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(wvarMensajeStoreProc)
  '
'Si la respuesta viene con error
  wvarStep = 30
  If Not wobjXMLRequest.selectSingleNode("//LBA_WS/@res_code").Text = "0" Then
    wvarCodErr = wobjXMLRequest.selectSingleNode("//LBA_WS/@res_code").Text
    wvarMensaje = wobjXMLRequest.selectSingleNode("//LBA_WS/@res_msg").Text

            mvar_Estado = "ER"
            pvarRes = wvarMensajeStoreProc
            wvarMensajeStoreProc = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """>" & _
                                    pvarRequest & _
                                    "</LBA_WS>"
  ' si no viene con error de la BD
  Else
  
    'Guarda la Solicitud en el SP
    wvarStep = 60
    
    If fncPutSolicitud(wvarMensajeStoreProc, wvarMensaje, wvarCodErr, pvarRes, pTiempoAIS) Then
  
        Set wobjXMLReturnVal = CreateObject("MSXML2.DOMDocument")
        wobjXMLReturnVal.async = False
        Call wobjXMLReturnVal.loadXML(pvarRes)
    '
        If Not wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text = "0" Then
      'hubo un error
            wvarStep = 65
            wvarCodErr = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text
            wvarMensaje = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_msg").Text
            wvarMensajeStoreProc = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """>" & _
                                      pvarRequest & _
                                   "</LBA_WS>"
            mvar_Estado = "ER"
  
            'Este es el caso en que no haya contestado el SP o sea un error no identificado
        Else
           wvarStep = 70
           mvar_Estado = "OK"
           wvarMensajeStoreProc = wobjXMLRequest.xml
        End If
    '
        Set wobjXMLReturnVal = Nothing
  
    End If

  
    '
  End If
  
  Call fncInsertNode(wvarMensajeStoreProc, "//LBA_WS/Request", mvar_Estado, "ESTADOPROC")
  
  ' Agrego el tiempo de impresion
  Call fncInsertNode(wvarMensajeStoreProc, "//LBA_WS/Request", pTiempoAIS, "TIEMPOPROCESO")
  
'Alta de la OPERACION
    wvarStep = 75
    If fncPutSolHO(wvarMensajeStoreProc, wvarMensaje, wvarCodErr, wvarMensajePutTran) Then
    '
        Set wobjXMLReturnVal = CreateObject("MSXML2.DOMDocument")
        wobjXMLReturnVal.async = False
        Call wobjXMLReturnVal.loadXML(wvarMensajePutTran)
    '
        wvarStep = 80
        If Not wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text = "0" Then
            wvarCodErr = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text
            wvarMensaje = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_msg").Text
            pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
            fncGetAll = False
            Exit Function
        End If
    '
        fncGetAll = True
    '
    End If
fin:
  Set wobjClass = Nothing
  Set wobjXMLRequest = Nothing
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
wvarCodErr = mcteErrorInesperadoCod

    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    fncGetAll = False
    mobjCOM_Context.SetComplete

End Function

Private Function fncInsertNode(ByRef pvarRequest, _
                               ByVal pvarHeader, _
                               ByVal pvarNodeValue, _
                               ByVal pvarNodeName) As Boolean
    
  Const wcteFnName      As String = "fncInsertNode"
  Dim wvarStep          As Long
  Dim wobjXMLRequest    As MSXML2.DOMDocument

  On Error GoTo ErrorHandler

  wvarStep = 10
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRequest)
  '
  wvarStep = 20
  Call wobjXMLRequest.selectSingleNode(pvarHeader).appendChild(wobjXMLRequest.createNode(NODE_ELEMENT, pvarNodeName, ""))
  wobjXMLRequest.selectSingleNode("//" & pvarNodeName).Text = pvarNodeValue
  '
  pvarRequest = wobjXMLRequest.xml
  Set wobjXMLRequest = Nothing
  fncInsertNode = True
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    fncInsertNode = False
    mobjCOM_Context.SetComplete

End Function


Private Function fncPutSolHO(ByVal pvarRequest As String, _
                           ByRef wvarMensaje As String, _
                           ByRef wvarCodErr As String, _
                           ByRef pvarRes As String) As Boolean

  Const wcteFnName                     As String = "fncPutSolHo"
  Dim wvarStep                         As Long
  '
  Dim wobjClass                        As HSBCInterfaces.IAction
  Dim wobjHSBC_DBCnn                   As HSBCInterfaces.IDBConnection
  Dim wobjXMLRequest                   As MSXML2.DOMDocument
  '
  Dim wobjDBCnn                        As ADODB.Connection
  Dim wobjDBCmd                        As ADODB.Command
  Dim wobjDBParm                       As ADODB.Parameter
  Dim wrstRes                          As ADODB.Recordset
  '
  Dim mvarWDB_CODINST                  As String
  Dim mvarWDB_REQUESTID                As String
  Dim mvarWDB_NROCOT                   As String
  Dim mvarWDB_TIPOOPERAC               As String
  Dim mvarWDB_ESTADO                   As String
  Dim mvarWDB_RECEPCION_PEDIDOANO      As String
  Dim mvarWDB_RECEPCION_PEDIDOMES      As String
  Dim mvarWDB_RECEPCION_PEDIDODIA      As String
  Dim mvarWDB_RECEPCION_PEDIDOHORA     As String
  Dim mvarWDB_RECEPCION_PEDIDOMINUTO   As String
  Dim mvarWDB_RECEPCION_PEDIDOSEGUNDO  As String
  Dim mvarWDB_RAMOPCOD                 As String
  Dim mvarWDB_POLIZANN                 As String
  Dim mvarWDB_POLIZSEC                 As String
  Dim mvarWDB_CERTISEC                 As String
  Dim mvarWDB_TIEMPOPROCESO            As String
  Dim mvarWDB_XML                      As String
                           

On Error GoTo ErrorHandler

'Alta de la OPERACION
  wvarStep = 10
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRequest)
  '
  wvarStep = 20
  mvarWDB_CODINST = 0
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST) Is Nothing Then
     mvarWDB_CODINST = wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST).Text
  End If
  '
  wvarStep = 30
  mvarWDB_REQUESTID = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_REQUESTID) Is Nothing Then
     mvarWDB_REQUESTID = wobjXMLRequest.selectSingleNode("//" & mcteParam_REQUESTID).Text
  End If
  '
  wvarStep = 60
  mvarWDB_ESTADO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_EstadoProc) Is Nothing Then
     mvarWDB_ESTADO = wobjXMLRequest.selectSingleNode("//" & mcteParam_EstadoProc).Text
  End If
  '
  wvarStep = 70
  mvarWDB_RECEPCION_PEDIDOANO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoAno) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOANO = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoAno).Text
  End If
  '
  wvarStep = 80
  mvarWDB_RECEPCION_PEDIDOMES = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoMes) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOMES = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoMes).Text
  End If
  '
  wvarStep = 90
  mvarWDB_RECEPCION_PEDIDODIA = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoDia) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDODIA = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoDia).Text
  End If
  '
  wvarStep = 100
  mvarWDB_RECEPCION_PEDIDOHORA = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoHora) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOHORA = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoHora).Text
  End If
  '
  wvarStep = 110
  mvarWDB_RECEPCION_PEDIDOMINUTO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoMinuto) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOMINUTO = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoMinuto).Text
  End If
  '
  wvarStep = 120
  mvarWDB_RECEPCION_PEDIDOSEGUNDO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoSegundo) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOSEGUNDO = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoSegundo).Text
  End If
  '
  wvarStep = 170
  mvarWDB_TIEMPOPROCESO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_TiempoProceso) Is Nothing Then
     mvarWDB_TIEMPOPROCESO = wobjXMLRequest.selectSingleNode("//" & mcteParam_TiempoProceso).Text
  End If
  

  
  wvarStep = 180
  mvarWDB_XML = ""
  mvarWDB_XML = pvarRequest
  '
  wvarStep = 190
  Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
  Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
  '
  Set wobjDBCmd = CreateObject("ADODB.Command")
  Set wobjDBCmd.ActiveConnection = wobjDBCnn
  wobjDBCmd.CommandText = mcteStoreProc
  wobjDBCmd.CommandType = adCmdStoredProc
  '
  wvarStep = 200
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_IDENTIFICACION_BROKER", adNumeric, adParamInput, , IIf(mvarWDB_CODINST = "", 0, mvarWDB_CODINST))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 210
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_NRO_OPERACION_BROKER", adNumeric, adParamInput, , IIf(mvarWDB_REQUESTID = "", 0, mvarWDB_REQUESTID))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 14
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 220
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_NRO_COTIZACION_LBA", adNumeric, adParamInput, , IIf(mvarWDB_NROCOT = "", 0, mvarWDB_NROCOT))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 18
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 230
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_TIPO_OPERACION", adChar, adParamInput, 1, IIf(mvarWDB_TIPOOPERAC = "", "V", mvarWDB_TIPOOPERAC))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 240
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_ESTADO", adChar, adParamInput, 2, IIf(mvarWDB_ESTADO = "", "ER", mvarWDB_ESTADO))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 250
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_RECEPCION_PEDIDO", adDate, adParamInput, 8, _
                                         mvarWDB_RECEPCION_PEDIDOANO & "-" & _
                                         mvarWDB_RECEPCION_PEDIDOMES & "-" & _
                                         mvarWDB_RECEPCION_PEDIDODIA & " " & _
                                         mvarWDB_RECEPCION_PEDIDOHORA & ":" & _
                                         mvarWDB_RECEPCION_PEDIDOMINUTO & ":" & _
                                         mvarWDB_RECEPCION_PEDIDOSEGUNDO)
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 260
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_ENVIO_RESPUESTA", adDate, adParamInput, 8, _
                                         Year(Now) & "-" & _
                                         Month(Now) & "-" & _
                                         Day(Now) & " " & _
                                         Hour(Now) & ":" & _
                                         Minute(Now) & ":" & _
                                         Second(Now))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 270
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_RAMOPCOD", adChar, adParamInput, 4, IIf(mvarWDB_RAMOPCOD = "", "VHI1", mvarWDB_RAMOPCOD))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 280
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_POLIZANN", adNumeric, adParamInput, , IIf(mvarWDB_POLIZANN = "", 0, mvarWDB_POLIZANN))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 290
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_POLIZSEC", adNumeric, adParamInput, , IIf(mvarWDB_POLIZSEC = "", 0, mvarWDB_POLIZSEC))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 6
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 300
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CERTIPOL", adNumeric, adParamInput, , IIf(mvarWDB_CODINST = "", 0, mvarWDB_CODINST))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 310
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CERTIANN", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 320
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CERTISEC", adNumeric, adParamInput, , IIf(mvarWDB_CERTISEC = "", 0, mvarWDB_CERTISEC))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 6
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 330
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_TIEMPO_PROCESO_AIS", adNumeric, adParamInput, , IIf(mvarWDB_TIEMPOPROCESO = "", 0, mvarWDB_TIEMPOPROCESO))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 6
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 340
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_OPERACION_XML", adVarChar, adParamInput, 8000, IIf(mvarWDB_XML = "", "", Left(mvarWDB_XML, 8000)))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 350
  '
  wobjDBCmd.Execute adExecuteNoRecords
  wobjDBCnn.Close
  
  fncPutSolHO = True
  pvarRes = "<LBA_WS res_code=""" & wobjXMLRequest.selectSingleNode("//LBA_WS/@res_code").Text & """ res_msg=""" & wobjXMLRequest.selectSingleNode("//LBA_WS/@res_msg").Text & """></LBA_WS>"
fin:
'libero los objectos
  Set wrstRes = Nothing
  Set wobjDBCmd = Nothing
  Set wobjDBCnn = Nothing
  Set wobjHSBC_DBCnn = Nothing
  Set wobjXMLRequest = Nothing
  Set wobjClass = Nothing
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
wvarCodErr = mcteErrorInesperadoCod

    fncPutSolHO = False
    'mobjCOM_Context.SetComplete

End Function

Private Function fncPutSolicitud(ByVal pvarRequest As String, _
                           ByRef wvarMensaje As String, _
                           ByRef wvarCodErr As String, _
                           ByRef pvarRes As String, _
                           ByRef pTiempoAIS As String) As Boolean

  Const wcteFnName                     As String = "fncPutSolicitud"
  Dim wvarStep                         As Long
  '
  Dim wobjClass                        As HSBCInterfaces.IAction
  Dim wobjHSBC_DBCnn                   As HSBCInterfaces.IDBConnection
  Dim wobjXMLRequest                   As MSXML2.DOMDocument
  Dim mobjXMLDoc                       As MSXML2.DOMDocument
  Dim wobjXMLNodeList                  As MSXML2.IXMLDOMNodeList
  Dim wvarcounter                      As Long

  Dim mvarWDB_CODINST                  As String
  Dim mvarWDB_REQUESTID                As String
  Dim mvarWDB_MARCA                    As String
  Dim mvarWDB_MODELO                   As String
  Dim mvarWDB_EFECTANN                 As String
  Dim mvarWDB_USUARCOD                 As String
  Dim mvarWDB_MENSAJE                  As String

  Dim mvarRequest                      As String
  Dim mvarResponse                     As String
  Dim mvarOk                           As String
  
  ' variables para calcular tiempo proceso de la impresión
  Dim mvar_TiempoProceso       As String
  Dim mvarInicioAIS            As String
  Dim mvarFinAIS               As String
  Dim mvarTiempoAIS            As String
                           
On Error GoTo ErrorHandler

 
 'Recupero datos del request
  wvarStep = 100
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRequest)
  
  wvarStep = 140
  mvarWDB_REQUESTID = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_REQUESTID) Is Nothing Then
     mvarWDB_REQUESTID = wobjXMLRequest.selectSingleNode("//" & mcteParam_REQUESTID).Text
  End If
  '
  wvarStep = 150
  mvarWDB_CODINST = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST) Is Nothing Then
     mvarWDB_CODINST = wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST).Text
  End If
  '
  wvarStep = 160
  mvarWDB_MARCA = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_Marca) Is Nothing Then
     mvarWDB_MARCA = wobjXMLRequest.selectSingleNode("//" & mcteParam_Marca).Text
  End If
  '
  wvarStep = 170
  mvarWDB_MODELO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_Modelo) Is Nothing Then
     mvarWDB_MODELO = wobjXMLRequest.selectSingleNode("//" & mcteParam_Modelo).Text
  End If
  '
  wvarStep = 180
  mvarWDB_EFECTANN = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_EfectAnn) Is Nothing Then
     mvarWDB_EFECTANN = wobjXMLRequest.selectSingleNode("//" & mcteParam_EfectAnn).Text
  End If
  '
  wvarStep = 420
  mvarWDB_USUARCOD = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_USUARCOD) Is Nothing Then
     mvarWDB_USUARCOD = wobjXMLRequest.selectSingleNode("//" & mcteParam_USUARCOD).Text
  End If

'conforma xml para el request

 wvarStep = 610
 mvarRequest = "<Request>" & _
                "<DEFINICION>1050_WSvehiculos.xml</DEFINICION>" & _
                "<USUARCOD>PR21072060</USUARCOD>" & _
                "<MARCA>" & mvarWDB_MARCA & "</MARCA>" & _
                "<MODELO>" & UCase(mvarWDB_MODELO) & "</MODELO>" & _
                "<ANIO>" & mvarWDB_EFECTANN & "</ANIO>" & _
                "<FECANN>" & Year(Now()) & "</FECANN>" & _
                "<FECMES>" & Month(Now()) & "</FECMES>" & _
                "<FECDIA>" & Day(Now()) & "</FECDIA>" & _
                "</Request>"

 wvarStep = 620


  Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVMQGen.lbaw_MQMensaje")
  Call wobjClass.Execute(mvarRequest, mvarResponse, "")
  Set wobjClass = Nothing
  
  Set mobjXMLDoc = CreateObject("MSXML2.DOMDocument")
  mobjXMLDoc.async = False
  Call mobjXMLDoc.loadXML(mvarResponse)
  'Call mobjXMLDoc.Load(App.Path & "\pruebaVehiculo.xml")

    
  mvarOk = "ER"
  If Not mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
    If (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").Text) = "true" Then
     mvarOk = "OK"
    Else
     mvarOk = "ER"
    End If
  Else
     mvarOk = "ER"
  End If
        
  If mvarOk = "ER" Then
     wvarCodErr = -101
     wvarMensaje = "No se pudo recuperar el Vehiculo"
     pvarRes = "<LBA_WS res_code=""-54"" res_msg=""Error""><Response><Estado resultado=""false"" mensaje=" & wvarMensaje & " /></Response></LBA_WS>"
     Exit Function
  Else
    fncPutSolicitud = True
    
    ' Armo el XML de salida
    Set wobjXMLNodeList = mobjXMLDoc.selectNodes("//Response/CAMPOS/RESULTADOS/VEHICULOS")
    
    pvarRes = "<LBA_WS res_code=""0"" res_msg=""""><Response><REQUESTID>" & mvarWDB_REQUESTID & "</REQUESTID><VEHICULOS>"
    
    For wvarcounter = 0 To (wobjXMLNodeList.length - 1)
        wvarStep = 931
        pvarRes = pvarRes & "<VEHICULO>"
        pvarRes = pvarRes & "<MARCA>" & wobjXMLNodeList.Item(wvarcounter).selectSingleNode("AUMARCOD").Text & "</MARCA>"
        pvarRes = pvarRes & "<MODELO>" & wobjXMLNodeList.Item(wvarcounter).selectSingleNode("AUMODCOD").Text & "</MODELO>"
        pvarRes = pvarRes & "<SUBMODELO>" & wobjXMLNodeList.Item(wvarcounter).selectSingleNode("AUSUBCOD").Text & "</SUBMODELO>"
        pvarRes = pvarRes & "<ADICIONAL>" & wobjXMLNodeList.Item(wvarcounter).selectSingleNode("AUADICOD").Text & "</ADICIONAL>"
        pvarRes = pvarRes & "<ORIGEN>" & wobjXMLNodeList.Item(wvarcounter).selectSingleNode("AUMODORI").Text & "</ORIGEN>"
        pvarRes = pvarRes & "<DESCRIPCION>" & wobjXMLNodeList.Item(wvarcounter).selectSingleNode("AUVEHDES").Text & "</DESCRIPCION>"
        pvarRes = pvarRes & "<INFOAUTO>" & wobjXMLNodeList.Item(wvarcounter).selectSingleNode("AUPRONUM").Text & "</INFOAUTO>"
        pvarRes = pvarRes & "<TIPOVEHI>" & wobjXMLNodeList.Item(wvarcounter).selectSingleNode("AUTIPCOD").Text & "</TIPOVEHI>"
        pvarRes = pvarRes & "<CATEGO>" & wobjXMLNodeList.Item(wvarcounter).selectSingleNode("AUCATCOD").Text & "</CATEGO>"
        pvarRes = pvarRes & "<COMBUSTIBLE>" & wobjXMLNodeList.Item(wvarcounter).selectSingleNode("AUTIPCOM").Text & "</COMBUSTIBLE>"
        pvarRes = pvarRes & "<SUMA>" & CStr(CDec(wobjXMLNodeList.Item(wvarcounter).selectSingleNode("AUSUMASE").Text) / 100) & "</SUMA>"
        pvarRes = pvarRes & "</VEHICULO>"
      
    Next
        pvarRes = pvarRes & "</VEHICULOS></Response></LBA_WS>"

  End If

fin:
'libero los objectos
  Set wobjXMLRequest = Nothing
  Set wobjClass = Nothing
  Set mobjXMLDoc = Nothing
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

 pvarRes = "<LBA_WS res_code=""-54"" res_msg=""Error""><Response><Estado resultado=""false"" mensaje=" & wvarMensaje & " /></Response></LBA_WS>"

 wvarCodErr = mcteErrorInesperadoCod

 mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError

  fncPutSolicitud = False
  'mobjCOM_Context.SetComplete

End Function

Function fechaVige(ByRef pvarStrFecha As String) As String

  Dim wvarIntAnio As String
  Dim wvarIntMes As String
  Dim wvarIntDia As String
  Dim warrSplit() As String

  pvarStrFecha = Format(pvarStrFecha, "General Date")
  
  wvarIntAnio = Year(pvarStrFecha)
  wvarIntMes = Right("0" & Month(pvarStrFecha), 2)
  wvarIntDia = Right("0" & Day(pvarStrFecha), 2)
  
  fechaVige = wvarIntDia & "/" & wvarIntMes & "/" & wvarIntAnio
  
End Function




