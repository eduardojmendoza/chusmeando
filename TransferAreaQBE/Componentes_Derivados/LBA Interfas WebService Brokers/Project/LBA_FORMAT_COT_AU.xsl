<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:output method="html" encoding="iso-8859-1"/>
	<xsl:template match="Response">
		<LBA_WS res_code="0" res_msg="">
			<Response>
				<REQUESTID>
					<xsl:value-of select="//REQUESTID"/>
					<!-- ojo falta agregar-->
				</REQUESTID>
				<COT_NRO>
					<xsl:value-of select="//COT_NRO"/>
				</COT_NRO>
				<VEHDES>
					<xsl:value-of select="//VEHDES"/>
					<!-- ojo falta agregar-->
				</VEHDES>
				<HIJOS_MENORES_29>
					<xsl:value-of select="//TIENEHIJOS"/>
				</HIJOS_MENORES_29>
				<PLANES>
				<xsl:if test="count(//PLAN) != 0">
					<xsl:for-each select="//PLAN">
						<xsl:sort select="ORDEN"/>
							<PLAN>
								<PLAN_COD>
									<xsl:value-of select="PLANNCOD"/>
								</PLAN_COD>
								<PLANNDES>
									<xsl:value-of select="PLANNDES"/>
								</PLANNDES>
								<COMP_PRECIO>
								<xsl:if test="//TIENEHIJOS = 'S'">
									<PRIMA>
										<xsl:value-of select="CON_HIJOS/PRIMA"/>
									</PRIMA>
									<RECARGOS>
										<xsl:value-of select="CON_HIJOS/RECARGOS"/>
									</RECARGOS>
									<IVAIMPOR>
										<xsl:value-of select="CON_HIJOS/IVAIMPOR"/>
									</IVAIMPOR>
									<IVAIMPOA>
										<xsl:value-of select="CON_HIJOS/IVAIMPOA"/>
									</IVAIMPOA>
									<IVARETEN>
										<xsl:value-of select="CON_HIJOS/IVARETEN"/>
									</IVARETEN>
									<DEREMI>
										<xsl:value-of select="CON_HIJOS/DEREMI"/>
									</DEREMI>
									<SELLADO>
										<xsl:value-of select="CON_HIJOS/SELLADO"/>
									</SELLADO>
									<INGBRU>
										<xsl:value-of select="CON_HIJOS/INGBRU"/>
									</INGBRU>
									<IMPUES>
										<xsl:value-of select="CON_HIJOS/IMPUES"/>
									</IMPUES>
									<PRECIO>
										<xsl:value-of select="CON_HIJOS/PRECIO"/>
									</PRECIO>
								</xsl:if>
								<xsl:if test="//TIENEHIJOS = 'N'">
									<PRIMA>
										<xsl:value-of select="SIN_HIJOS/PRIMA"/>
									</PRIMA>
									<RECARGOS>
										<xsl:value-of select="SIN_HIJOS/RECARGOS"/>
									</RECARGOS>
									<IVAIMPOR>
										<xsl:value-of select="SIN_HIJOS/IVAIMPOR"/>
									</IVAIMPOR>
									<IVAIMPOA>
										<xsl:value-of select="SIN_HIJOS/IVAIMPOA"/>
									</IVAIMPOA>
									<IVARETEN>
										<xsl:value-of select="SIN_HIJOS/IVARETEN"/>
									</IVARETEN>
									<DEREMI>
										<xsl:value-of select="SIN_HIJOS/DEREMI"/>
									</DEREMI>
									<SELLADO>
										<xsl:value-of select="SIN_HIJOS/SELLADO"/>
									</SELLADO>
									<INGBRU>
										<xsl:value-of select="SIN_HIJOS/INGBRU"/>
									</INGBRU>
									<IMPUES>
										<xsl:value-of select="SIN_HIJOS/IMPUES"/>
									</IMPUES>
									<PRECIO>
										<xsl:value-of select="SIN_HIJOS/PRECIO"/>
									</PRECIO>
								</xsl:if>
								</COMP_PRECIO>
								<CLUBLBA>S</CLUBLBA>
								<DESTRUCCION_80>S</DESTRUCCION_80>
								<LUNETA><xsl:value-of select="LUNPAR"/></LUNETA>
								<CLUBECO>S</CLUBECO>
								<ROBOCONT><xsl:value-of select="ROBOCON"/></ROBOCONT>
								<GRANIZO><xsl:value-of select="GRANIZO"/></GRANIZO>
							</PLAN>
				</xsl:for-each>
				</xsl:if>
				</PLANES>
				<SUMASEG>
					<xsl:value-of select="//SUMASEG"/>
				</SUMASEG>
				<SUMALBA>
					<xsl:value-of select="//SUMALBA"/>
				</SUMALBA>
			</Response>
		</LBA_WS>
	</xsl:template>
</xsl:stylesheet>
