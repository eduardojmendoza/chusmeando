SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



/*  
---------------------------------------------------------------------------------------  
Fecha de Modificación: 19/10/2010
PPCR: 2009-00845  
Desarrollador: Jose Casais
Descripción: Se actualiza la sp por volver a uso  
---------------------------------------------------------------------------------------  
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION  
LIMITED 2010. ALL RIGHTS RESERVED  
  
This software is only to be used for the purpose for which it has been provided.  
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval  
system or translated in any human or computer language in any way or for any other  
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai  
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal  
offence, which can result in heavy fines and payment of substantial damages.  
  
Nombre del Programador: desconocido
Nombre del Stored:  SPSNCV_BRO_COTI_AUS1_GRABA
Fecha de Creación: desconocido
Numero de PPCR: desconocido
Descripción: a efectos QA    
---------------------------------------------------------------------------------------  
*/


--VALORES DE ERROR
--.-1)error al insertar en SIFSPOLI
--.-2)error en la grabacion de SEFMDECA #1
--.-3)error en la grabacion de SEFMDECA #2

/****** Object:  Stored Procedure dbo.SPSNCV_BRO_COTI_AUS1_GRABA   Script Date: 10/04/03 15:04:16 ******/

ALTER           PROCEDURE SPSNCV_BRO_COTI_AUS1_GRABA
--CABECERA
@RAMOPCOD              char(4),
@POLIZANN              numeric(2),
@POLIZSEC              numeric(6),
@CERTIPOL              numeric(4),
@CERTIANN              numeric(4),
@CERTISEC              numeric(6) OUTPUT, -- SI ES IGUAL A CERO ES UNA ALTA, SI NO, UNA ACTUALIZACION
@SUPLENUM              numeric(4),

-- DATOS DEL CLIENTE :
@USUARCOD char(10),
@NUMEDOCU char (11),
@TIPODOCU numeric(2, 0),
@CLIENAP1 char (20),
@CLIENAP2 char (20),
@CLIENNOM char (20),
@NACIMANN numeric(4, 0),
@NACIMMES numeric(2, 0),
@NACIMDIA numeric(2, 0),
@CLIENSEX char (1),
@CLIENEST char (1),
@PAISSCOD char (2),
@IDIOMCOD char (3),
@NUMHIJOS numeric(2, 0),
@EFECTANN numeric(4, 0),
@EFECTMES numeric(2, 0),
@EFECTDIA numeric(2, 0),
@CLIENTIP char (2),
@CLIENFUM char (1),
@CLIENDOZ char (1),
@ABRIDTIP char (2),
@ABRIDNUM char (10),
@CLIENORG char (3),
@PERSOTIP char (1),
--@DOMICSEC char (3),SE OBTIENE DESDE EL PECO
@CLIENCLA char (9),
@FALLEANN numeric(4, 0),
@FALLEMES numeric(2, 0),
@FALLEDIA numeric(2, 0),
@FBAJAANN numeric(4, 0),
@FBAJAMES numeric(2, 0),
@FBAJADIA numeric(2, 0),
@EMAIL char(60),

-- DATOS DEL DOMICILIO DE RIESGO:
@DOMICCAL char (5),
@DOMICDOM char (30),
@DOMICDNU char (5),
@DOMICESC char (1),
@DOMICPIS char (4),
@DOMICPTA char (4),
@DOMICPOB varchar (60),
@DOMICCPO numeric(5, 0),
@PROVICOD numeric(2, 0),
--@PAISSCOD char (2), DECLARADO EN EL CLIENTE
@TELCOD   char (5),
@TELNRO   char(30),
@PRINCIPAL char(1),

-- DATOS DE LA CUENTA :
@TIPOCUEN char (1),
@COBROTIP char (2),
@BANCOCOD numeric(4, 0),
@SUCURCOD numeric(4, 0),
@CUENTDC  char (2),
@CUENNUME char (16),
@TARJECOD numeric(2, 0),
@VENCIANN numeric(4, 0),
@VENCIMES numeric(2, 0),
@VENCIDIA numeric(2, 0),

-- ***** DATOS DE LA OPERACION *****
--FIMA TARI
@FRANQCOD	char (2),
@PQTDES		char (60),
@PLANCOD	numeric(3, 0),
@HIJOS1416	numeric(1, 0),
@HIJOS1729	numeric(1, 0),
@ZONA		numeric(4, 0),
@CODPROV	numeric(2, 0),
@SUMALBA	numeric(9, 2),
@CLIENIVA	char (1),
@SUMAASEG	numeric(9, 2),
@CLUB_LBA	char (1),
@CPAANO		numeric(8, 0),
@CTAKMS		numeric(6, 0),
@ESCERO		char (1),
@TIENEPLAN	char (1),
@COND_ADIC	char (1),
@ASEG_ADIC	char (1),
@FH_NAC		numeric(8, 0),
@SEXO		char (1),
@ESTCIV		char (1),
@SIFMVEHI_DES	char (35),
@PROFECOD	char (6),
@ACCESORIOS	char (1),
@REFERIDO	numeric(7, 0),

--FIMA VEHI
@MOTORNUM	char (25) ,
@CHASINUM	char (30) ,
@PATENNUM	char (10) ,
@AUMARCOD	numeric(5, 0) ,
@AUMODCOD	numeric(5, 0) ,
@AUSUBCOD	numeric(5, 0) ,
@AUADICOD	numeric(5, 0) ,
@AUMODORI	char (1) ,
@AUUSOCOD	numeric(3, 0) ,
@AUVTVCOD	char (1) ,
@AUVTVDIA	numeric(2, 0) ,
@AUVTVMES	numeric(2, 0) ,
@AUVTVANN	numeric(4, 0) ,
@VEHCLRCOD	numeric(3, 0) ,
@AUKLMNUM	numeric(7, 0) ,
@FABRICAN	numeric(4, 0) ,
@FABRICMES	numeric(2, 0) ,
@GUGARAGE	char (1) ,
@GUDOMICI	char (1) ,
@AUCATCOD	numeric(2, 0) ,
@AUTIPCOD	numeric(4, 0) ,
@AUCIASAN	char (4) ,
@AUANTANN	numeric(2, 0) ,
@AUNUMSIN	numeric(2, 0) ,
@AUNUMKMT	numeric(7, 0) ,
@AUUSOGNC	char (1),

--MOVI
--@EMISIANN		numeric(4,0), /
--@EMISIMES		numeric(2,0),|    GETDATE()
--@EMISIDIA		numeric(2,0), \
--@USUARCOD		char(10), PARAMETRO PRINCIPAL
@SITUCPOL		char(1),
--@CLIENSEC		numeric(9,0),--YA DECLARADO EN BENE

--PECO
--@CLIENSEC              numeric(9),--YA DECLARADO EN MOVI
--@NUMEDOCU              char(11),PARAMETRO PRINCIPAL
--@TIPODOCU              numeric(2),PARAMETRO PRINCIPAL
--@DOMICSEC              numeric(3),PARAMETRO PRINCIPAL
--@CUENTSEC              numeric(3),SE OBTIENE DESDE EL PECO

--POLI
--@COBROCOD		numeric(4, 0) ,--YA DECLARADO EN PECO
--@EMISIANN		numeric(4, 0) ,/
--@EMISIMES		numeric(2, 0) ,|   GETDATE()
--@EMISIDIA		numeric(2, 0) ,\
@COBROFOR		numeric(1, 0) ,
--@SITUCPOL		char (1) ,--YA DECLARADO EN MOVI
--@EFEVGANN		numeric(4, 0) ,
--@EFEVGMES		numeric(2, 0) ,
--@EFEVGDIA		numeric(2, 0) ,
@EDADACTU		numeric(3, 0) ,

--DECA
--1.)
@COBERCOD1 		numeric(3, 0),
--@EFECTANN1 		numeric(4, 0),--YA DECLARADO EN POLI
--@EFECTMES1 		numeric(2, 0),--YA DECLARADO EN POLI
--@EFECTDIA1 		numeric(2, 0),--YA DECLARADO EN POLI
--@VENCIANN1 		numeric(4, 0),--YA DECLARADO EN POLI
--@VENCIMES1 		numeric(2, 0),--YA DECLARADO EN POLI
--@VENCIDIA1 		numeric(2, 0),--YA DECLARADO EN POLI
@COBERORD1 		numeric(2, 0),
@CAPITASG1 		numeric(15, 0),
@CAPITIMP1 		numeric(15, 0),
--@CUMULNUM1 numeric(6, 0),--YA DECLARADO EN POLI
--2.)
@COBERCOD2 		numeric(3, 0),
@COBERORD2 		numeric(2, 0),
@CAPITASG2 		numeric(15, 0),
@CAPITIMP2 		numeric(15, 0),
--3.)
@COBERCOD3 		numeric(3, 0),
@COBERORD3 		numeric(2, 0),
@CAPITASG3 		numeric(15, 0),
@CAPITIMP3 		numeric(15, 0),
--4.)
@COBERCOD4 		numeric(3, 0),
@COBERORD4 		numeric(2, 0),
@CAPITASG4 		numeric(15, 0),
@CAPITIMP4 		numeric(15, 0),
--5.)
@COBERCOD5 		numeric(3, 0),
@COBERORD5 		numeric(2, 0),
@CAPITASG5 		numeric(15, 0),
@CAPITIMP5 		numeric(15, 0),
--6.)
@COBERCOD6 		numeric(3, 0),
@COBERORD6 		numeric(2, 0),
@CAPITASG6 		numeric(15, 0),
@CAPITIMP6 		numeric(15, 0),
--7.)
@COBERCOD7 		numeric(3, 0),
@COBERORD7 		numeric(2, 0),
@CAPITASG7 		numeric(15, 0),
@CAPITIMP7 		numeric(15, 0),
--8.)
@COBERCOD8 		numeric(3, 0),
@COBERORD8 		numeric(2, 0),
@CAPITASG8 		numeric(15, 0),
@CAPITIMP8 		numeric(15, 0),
--9.)
@COBERCOD9 		numeric(3, 0),
@COBERORD9 		numeric(2, 0),
@CAPITASG9 		numeric(15, 0),
@CAPITIMP9 		numeric(15, 0),
--10.)
@COBERCOD10		numeric(3, 0),
@COBERORD10		numeric(2, 0),
@CAPITASG10		numeric(15, 0),
@CAPITIMP10		numeric(15, 0),
--11.)
@COBERCOD11		numeric(3, 0),
@COBERORD11		numeric(2, 0),
@CAPITASG11		numeric(15, 0),
@CAPITIMP11		numeric(15, 0),
--12.)
@COBERCOD12		numeric(3, 0),
@COBERORD12		numeric(2, 0),
@CAPITASG12		numeric(15, 0),
@CAPITIMP12		numeric(15, 0),
--13.)
@COBERCOD13		numeric(3, 0),
@COBERORD13		numeric(2, 0),
@CAPITASG13		numeric(15, 0),
@CAPITIMP13		numeric(15, 0),
--14.)
@COBERCOD14		numeric(3, 0),
@COBERORD14		numeric(2, 0),
@CAPITASG14		numeric(15, 0),
@CAPITIMP14		numeric(15, 0),
--15.)
@COBERCOD15		numeric(3, 0),
@COBERORD15		numeric(2, 0),
@CAPITASG15		numeric(15, 0),
@CAPITIMP15		numeric(15, 0),
--16.)
@COBERCOD16		numeric(3, 0),
@COBERORD16		numeric(2, 0),
@CAPITASG16		numeric(15, 0),
@CAPITIMP16		numeric(15, 0),
--17.)
@COBERCOD17		numeric(3, 0),
@COBERORD17		numeric(2, 0),
@CAPITASG17		numeric(15, 0),
@CAPITIMP17		numeric(15, 0),
--18.)
@COBERCOD18		numeric(3, 0),
@COBERORD18		numeric(2, 0),
@CAPITASG18		numeric(15, 0),
@CAPITIMP18		numeric(15, 0),
--19.)
@COBERCOD19		numeric(3, 0),
@COBERORD19		numeric(2, 0),
@CAPITASG19		numeric(15, 0),
@CAPITIMP19		numeric(15, 0),
--20.)
@COBERCOD20		numeric(3, 0),
@COBERORD20		numeric(2, 0),
@CAPITASG20		numeric(15, 0),
@CAPITIMP20		numeric(15, 0),
--FIN DE DECA

--SEFMDERI_ASEGADIC
--1.)
@DOCUMDAT1	numeric(9, 0),
@NOMBREAS1	char(49),
--2.)
@DOCUMDAT2	numeric(9, 0),
@NOMBREAS2	char(49),
--3.)
@DOCUMDAT3	numeric(9, 0),
@NOMBREAS3	char(49),
--4.)
@DOCUMDAT4	numeric(9, 0),
@NOMBREAS4	char(49),
--5.)
@DOCUMDAT5	numeric(9, 0),
@NOMBREAS5	char(49),
--6.)
@DOCUMDAT6	numeric(9, 0),
@NOMBREAS6	char(49),
--7.)
@DOCUMDAT7	numeric(9, 0),
@NOMBREAS7	char(49),
--8.)
@DOCUMDAT8	numeric(9, 0),
@NOMBREAS8	char(49),
--9.)
@DOCUMDAT9	numeric(9, 0),
@NOMBREAS9	char(49),
--10.)
@DOCUMDAT10	numeric(9, 0),
@NOMBREAS10	char(49),

--FIN DE SEFMDERI

--DERI CAMP
@CAMP_CODIGO           NUMERIC(4),
@CAMP_DESC             char(30),

--DERI GTE
@LEGAJO_GTE            CHAR(10),

--DERI PROD
@NRO_PROD              NUMERIC(4),

--DERI SUCU
@SUCURSAL_CODIGO       CHAR(4),

--DERI VEND
@LEGAJO_VEND           CHAR(10),

--HIJOS: SIFMVECO
--.1)
--@DECLASEC		numeric(2, 0),--SE DECLARA SEGUN EL ORDEN
@CONDUAPE1	char(20),
@CONDUNOM1	char(30),
@CONDUFEC1	numeric(8, 0),
@CONDUSEX1	char(1),
@CONDUEST1	char(1),
@CONDUEXC1	char(1), 
--.2)
@CONDUAPE2	char(20),
@CONDUNOM2	char(30),
@CONDUFEC2	numeric(8, 0),
@CONDUSEX2	char(1),
@CONDUEST2	char(1),
@CONDUEXC2	char(1),
--.3)
@CONDUAPE3	char(20),
@CONDUNOM3	char(30),
@CONDUFEC3	numeric(8, 0),
@CONDUSEX3	char(1),
@CONDUEST3	char(1),
@CONDUEXC3	char(1), 
--.4)
@CONDUAPE4	char(20),
@CONDUNOM4	char(30),
@CONDUFEC4	numeric(8, 0),
@CONDUSEX4	char(1),
@CONDUEST4	char(1),
@CONDUEXC4	char(1), 
--.5)
@CONDUAPE5	char(20),
@CONDUNOM5	char(30),
@CONDUFEC5	numeric(8, 0),
@CONDUSEX5	char(1),
@CONDUEST5	char(1),
@CONDUEXC5	char(1), 
--.6
@CONDUAPE6	char(20),
@CONDUNOM6	char(30),
@CONDUFEC6	numeric(8, 0),
@CONDUSEX6	char(1),
@CONDUEST6	char(1),
@CONDUEXC6	char(1), 
--.7)
@CONDUAPE7	char(20),
@CONDUNOM7	char(30),
@CONDUFEC7	numeric(8, 0),
@CONDUSEX7	char(1),
@CONDUEST7	char(1),
@CONDUEXC7	char(1), 
--.8)
@CONDUAPE8	char(20),
@CONDUNOM8	char(30),
@CONDUFEC8	numeric(8, 0),
@CONDUSEX8	char(1),
@CONDUEST8	char(1),
@CONDUEXC8	char(1), 
--.9)
@CONDUAPE9	char(20),
@CONDUNOM9	char(30),
@CONDUFEC9	numeric(8, 0),
@CONDUSEX9	char(1),
@CONDUEST9	char(1),
@CONDUEXC9	char(1), 
--.10)
@CONDUAPE10	char(20),
@CONDUNOM10	char(30),
@CONDUFEC10	numeric(8, 0),
@CONDUSEX10	char(1),
@CONDUEST10	char(1),
@CONDUEXC10	char(1), 

--ACCESORIOS: SIFSVEAC
--.1)
--@VEACCSEC              NUMERIC(2),--SECUANCIA OTORGADA POR EL ORDEN
@AUACCCOD1              NUMERIC(4),
@AUVEASUM1              NUMERIC(14),
@AUVEADES1              CHAR(30),
@AUVEADEP1              CHAR(1),
--.2)
@AUACCCOD2              NUMERIC(4),
@AUVEASUM2              NUMERIC(14),
@AUVEADES2              CHAR(30),
@AUVEADEP2              CHAR(1),
--.3)
@AUACCCOD3              NUMERIC(4),
@AUVEASUM3              NUMERIC(14),
@AUVEADES3              CHAR(30),
@AUVEADEP3              CHAR(1),
--.4)
@AUACCCOD4              NUMERIC(4),
@AUVEASUM4              NUMERIC(14),
@AUVEADES4              CHAR(30),
@AUVEADEP4              CHAR(1),
--.5)
@AUACCCOD5              NUMERIC(4),
@AUVEASUM5              NUMERIC(14),
@AUVEADES5              CHAR(30),
@AUVEADEP5              CHAR(1),
--.6)
@AUACCCOD6              NUMERIC(4),
@AUVEASUM6              NUMERIC(14),
@AUVEADES6              CHAR(30),
@AUVEADEP6              CHAR(1),
--.7)
@AUACCCOD7              NUMERIC(4),
@AUVEASUM7              NUMERIC(14),
@AUVEADES7              CHAR(30),
@AUVEADEP7              CHAR(1),
--.8)
@AUACCCOD8              NUMERIC(4),
@AUVEASUM8              NUMERIC(14),
@AUVEADES8              CHAR(30),
@AUVEADEP8              CHAR(1),
--.9)
@AUACCCOD9              NUMERIC(4),
@AUVEASUM9              NUMERIC(14),
@AUVEADES9              CHAR(30),
@AUVEADEP9              CHAR(1),
--.10)
@AUACCCOD10             NUMERIC(4),
@AUVEASUM10             NUMERIC(14),
@AUVEADES10             CHAR(30),
@AUVEADEP10             CHAR(1),

--**NUEVOS PARAMETROS
--@BANCOCOD               char(4),PARAMETRO PRINCIPAL
@COBROCOD		numeric(4),
--@EFECTANN		numeric(4, 0),PARAMETRO PRINCIPAL
--@EFECTMES		numeric(2, 0),PARAMETRO PRINCIPAL
--@EFECTDIA		numeric(2, 0),PARAMETRO PRINCIPAL
--@COBROTIP               char(2)PARAMETRO PRINCIPAL

--jc 08/2010 campos nuevos
@DESTRUCCION_80		CHAR(1),
@LUNETA			CHAR(1),
@CLUBECO		CHAR(1),
@ROBOCONT		CHAR(1),
@GRANIZO		CHAR(1),
@IBB			CHAR(1)
-- jc fin
AS

SET NOCOUNT ON

BEGIN TRANSACTION

--VARIABLE DE CONTROL DE RETORNO
DECLARE @RET 		numeric

--VARIOS
DECLARE @COUNT 		numeric(18)

--CLAVES
DECLARE @ID_CLIENSEC 	numeric(6,0)
DECLARE @ID_DOMICSEC 	numeric(6,0)
DECLARE @ID_CUENTSEC 	numeric(6,0)

--VALORES POR DEFECTO

 
-- SI VIENE CON # DE OPERACION DIFERENTE DE 0, VERIFICAMOS SI LA OPERACION EXISTE
IF @CERTISEC <> 0
BEGIN
	SELECT @COUNT = COUNT(*) FROM PROD_SIFSMOVI 
		WHERE 	RAMOPCOD = @RAMOPCOD AND
			POLIZANN = @POLIZANN AND
			POLIZSEC = @POLIZSEC AND
			CERTIPOL = @CERTIPOL AND
			CERTIANN = @CERTIANN AND			CERTISEC = @CERTISEC AND
			SUPLENUM = @SUPLENUM 
	IF @COUNT = 0
	BEGIN
		--Operacion inexistente
		ROLLBACK TRANSACTION
		RETURN -1
	END
END
 

IF @CERTISEC = 0
BEGIN
	--**************** ALTA ********************	

	--ALTA DEL CLIENTE
	EXEC @ID_CLIENSEC = SPSNCV_PROD_SIFMPERS_INSERT
			@USUARCOD ,@NUMEDOCU ,@TIPODOCU ,
			@CLIENAP1 ,@CLIENAP2 ,@CLIENNOM ,
			@NACIMANN ,@NACIMMES ,@NACIMDIA ,
			@CLIENSEX ,@CLIENEST ,@PAISSCOD ,
			@IDIOMCOD ,@NUMHIJOS ,@EFECTANN ,
			@EFECTMES ,@EFECTDIA ,@CLIENTIP ,
			@CLIENFUM ,@CLIENDOZ ,@ABRIDTIP ,
			@ABRIDNUM ,@CLIENORG ,@PERSOTIP ,
			/*@DOMICSEC*/0 ,@CLIENCLA ,@FALLEANN ,
			@FALLEMES ,@FALLEDIA ,@FBAJAANN ,
			@FBAJAMES ,@FBAJADIA ,@EMAIL
	IF @ID_CLIENSEC = -1
	BEGIN
		--ERROR AL INSERTAR EN CLIENTES
		ROLLBACK TRANSACTION
		RETURN -2
	END

	--ALTA DE DOMICILIO
	EXEC @ID_DOMICSEC = SPSNCV_PROD_SIFMDOMI_INSERT
			@ID_CLIENSEC,@DOMICCAL,
			@DOMICDOM,@DOMICDNU,
			@DOMICESC,@DOMICPIS,
			@DOMICPTA,@DOMICPOB,
			@DOMICCPO,@PROVICOD,
			@PAISSCOD,@TELCOD,  
			@TELNRO  ,@PRINCIPAL
	IF @ID_DOMICSEC = -1
	BEGIN
		--ERROR AL INSERTAR DOMICILIOS
		ROLLBACK TRANSACTION
		RETURN -3
	END

	-- ALTA DE CUENTA
	EXEC @ID_CUENTSEC = SPSNCV_PROD_SIFMCUEN_INSERT
			@ID_CLIENSEC ,@TIPOCUEN ,
			@COBROTIP ,@BANCOCOD ,
			@SUCURCOD ,@CUENTDC  ,
			@CUENNUME ,@TARJECOD ,
			@VENCIANN ,@VENCIMES ,
			@VENCIDIA 
	IF @ID_CUENTSEC = -1
	BEGIN
		--ERROR AL INSERTAR LA CUENTA
		ROLLBACK TRANSACTION
		RETURN -4
	END

	--INGRESO DE LA COTIZACION
	EXEC @CERTISEC = SPSNCV_PROD_COTI_INSERT @RAMOPCOD, 
		@POLIZANN, @POLIZSEC, @CERTIPOL, @CERTIANN, @SUPLENUM, @FRANQCOD, @PQTDES, 
		@PLANCOD, @HIJOS1416, @HIJOS1729, @ZONA, @CODPROV, @SUMALBA, @CLIENIVA, @SUMAASEG, 
		@CLUB_LBA, @CPAANO, @CTAKMS, @ESCERO, @TIENEPLAN, @COND_ADIC, @ASEG_ADIC, @FH_NAC, 
		@SEXO, @ESTCIV, @SIFMVEHI_DES, @PROFECOD, @ACCESORIOS, @REFERIDO, @MOTORNUM, @CHASINUM, 
		@PATENNUM, @AUMARCOD, @AUMODCOD, @AUSUBCOD, @AUADICOD, @AUMODORI, @AUUSOCOD, @AUVTVCOD, 
		@AUVTVDIA, @AUVTVMES, @AUVTVANN, @VEHCLRCOD, @AUKLMNUM, @FABRICAN, @FABRICMES, @GUGARAGE, 
		@GUDOMICI, @AUCATCOD, @AUTIPCOD, @AUCIASAN, @AUANTANN, @AUNUMSIN, @AUNUMKMT, @AUUSOGNC, @USUARCOD, 
		@SITUCPOL, @ID_CLIENSEC, @NUMEDOCU, @TIPODOCU, @ID_DOMICSEC, @ID_CUENTSEC, @COBROFOR, @EDADACTU, @COBERCOD1, 
		@COBERORD1, @CAPITASG1, @CAPITIMP1, @COBERCOD2, @COBERORD2, @CAPITASG2, @CAPITIMP2, @COBERCOD3, 
		@COBERORD3, @CAPITASG3, @CAPITIMP3, @COBERCOD4, @COBERORD4, @CAPITASG4, @CAPITIMP4, @COBERCOD5, 
		@COBERORD5, @CAPITASG5, @CAPITIMP5, @COBERCOD6, @COBERORD6, @CAPITASG6, @CAPITIMP6, @COBERCOD7, 
		@COBERORD7, @CAPITASG7, @CAPITIMP7, @COBERCOD8, @COBERORD8, @CAPITASG8, @CAPITIMP8, @COBERCOD9, 
		@COBERORD9, @CAPITASG9, @CAPITIMP9, @COBERCOD10, @COBERORD10, @CAPITASG10, @CAPITIMP10, @COBERCOD11, 
		@COBERORD11, @CAPITASG11, @CAPITIMP11, @COBERCOD12, @COBERORD12, @CAPITASG12, @CAPITIMP12, @COBERCOD13, 
		@COBERORD13, @CAPITASG13, @CAPITIMP13, @COBERCOD14, @COBERORD14, @CAPITASG14, @CAPITIMP14, @COBERCOD15, 
		@COBERORD15, @CAPITASG15, @CAPITIMP15, @COBERCOD16, @COBERORD16, @CAPITASG16, @CAPITIMP16, @COBERCOD17, 
		@COBERORD17, @CAPITASG17, @CAPITIMP17, @COBERCOD18, @COBERORD18, @CAPITASG18, @CAPITIMP18, @COBERCOD19, 
		@COBERORD19, @CAPITASG19, @CAPITIMP19, @COBERCOD20, @COBERORD20, @CAPITASG20, @CAPITIMP20, @DOCUMDAT1, 
		@NOMBREAS1, @DOCUMDAT2, @NOMBREAS2, @DOCUMDAT3, @NOMBREAS3, @DOCUMDAT4, @NOMBREAS4, @DOCUMDAT5, 
		@NOMBREAS5, @DOCUMDAT6, @NOMBREAS6, @DOCUMDAT7, @NOMBREAS7, @DOCUMDAT8, @NOMBREAS8, @DOCUMDAT9, 
		@NOMBREAS9, @DOCUMDAT10, @NOMBREAS10, @CAMP_CODIGO, @CAMP_DESC, @LEGAJO_GTE, @NRO_PROD, @SUCURSAL_CODIGO, 
		@LEGAJO_VEND, @CONDUAPE1, @CONDUNOM1, @CONDUFEC1, @CONDUSEX1, @CONDUEST1, @CONDUEXC1, @CONDUAPE2, 
		@CONDUNOM2, @CONDUFEC2, @CONDUSEX2, @CONDUEST2, @CONDUEXC2, @CONDUAPE3, @CONDUNOM3, @CONDUFEC3, 
		@CONDUSEX3, @CONDUEST3, @CONDUEXC3, @CONDUAPE4, @CONDUNOM4, @CONDUFEC4, @CONDUSEX4, @CONDUEST4, 
		@CONDUEXC4, @CONDUAPE5, @CONDUNOM5, @CONDUFEC5, @CONDUSEX5, @CONDUEST5, @CONDUEXC5, @CONDUAPE6, 
		@CONDUNOM6, @CONDUFEC6, @CONDUSEX6, @CONDUEST6, @CONDUEXC6, @CONDUAPE7, @CONDUNOM7, @CONDUFEC7, 
		@CONDUSEX7, @CONDUEST7, @CONDUEXC7, @CONDUAPE8, @CONDUNOM8, @CONDUFEC8, @CONDUSEX8, @CONDUEST8, 
		@CONDUEXC8, @CONDUAPE9, @CONDUNOM9, @CONDUFEC9, @CONDUSEX9, @CONDUEST9, @CONDUEXC9, @CONDUAPE10, 
		@CONDUNOM10, @CONDUFEC10, @CONDUSEX10, @CONDUEST10, @CONDUEXC10, @AUACCCOD1, @AUVEASUM1, @AUVEADES1, 
		@AUVEADEP1, @AUACCCOD2, @AUVEASUM2, @AUVEADES2, @AUVEADEP2, @AUACCCOD3, @AUVEASUM3, @AUVEADES3, 
		@AUVEADEP3, @AUACCCOD4, @AUVEASUM4, @AUVEADES4, @AUVEADEP4, @AUACCCOD5, @AUVEASUM5, @AUVEADES5, 
		@AUVEADEP5, @AUACCCOD6, @AUVEASUM6, @AUVEADES6, @AUVEADEP6, @AUACCCOD7, @AUVEASUM7, @AUVEADES7, 
		@AUVEADEP7, @AUACCCOD8, @AUVEASUM8, @AUVEADES8, @AUVEADEP8, @AUACCCOD9, @AUVEASUM9, @AUVEADES9, 
		@AUVEADEP9, @AUACCCOD10, @AUVEASUM10, @AUVEADES10, @AUVEADEP10, @BANCOCOD, @COBROCOD, @EFECTANN, 
		@EFECTMES, @EFECTDIA, @COBROTIP,
		--jc 08/2010 campos nuevos
		'', '', @IBB, '', @LUNETA, 0, '', '', '', '', @DESTRUCCION_80, '', @CLUBECO, @GRANIZO, @ROBOCONT
-- jc fin
	IF @CERTISEC = -1
	BEGIN
		--ERROR AL INSERTAR EN CLIENTES
		ROLLBACK TRANSACTION
		RETURN -5
	END

END
ELSE
BEGIN
	PRINT 'PARA MODIFICAR'
	--************ MODIFICACION *****************	

	--OBTENEMOS LAS CLAVES DE CLIENTE, DOMICILIO Y CUENTA
	SELECT 	@ID_CLIENSEC = PROD_SIFSPECO.CLIENSEC, 
		@ID_DOMICSEC = PROD_SIFSPECO.DOMICSEC,
		@ID_CUENTSEC = PROD_SIFSPECO.CUENTSEC
		FROM PROD_SIFSPECO
		WHERE 	RAMOPCOD = @RAMOPCOD AND
			POLIZANN = @POLIZANN AND
			POLIZSEC = @POLIZSEC AND
			CERTIPOL = @CERTIPOL AND
			CERTIANN = @CERTIANN AND			CERTISEC = @CERTISEC AND
			SUPLENUM = @SUPLENUM 
	IF @COUNT = 0
	BEGIN
		--Operacion inexistente
		ROLLBACK TRANSACTION
		RETURN -6
	END

	--MODIFICACION DEL CLIENTE
	EXEC @RET = SPSNCV_PROD_SIFMPERS_UPDATE
			@ID_CLIENSEC, @NUMEDOCU, @TIPODOCU, 
			@CLIENAP1, @CLIENAP2, @CLIENNOM, 
			@NACIMANN, @NACIMMES, @NACIMDIA, 
			@CLIENSEX, @CLIENEST, @PAISSCOD, 
			@IDIOMCOD, @NUMHIJOS, @EFECTANN, 
			@EFECTMES, @EFECTDIA, @CLIENTIP, 
			@CLIENFUM, @CLIENDOZ, @ABRIDTIP, 
			@ABRIDNUM, @CLIENORG, @PERSOTIP, 
			@ID_DOMICSEC, @CLIENCLA, @FALLEANN, 
			@FALLEMES, @FALLEDIA, @FBAJAANN, 
			@FBAJAMES, @FBAJADIA, @EMAIL
	IF @RET = -1
	BEGIN
		--ERROR AL INSERTAR EN CLIENTES
		ROLLBACK TRANSACTION
		RETURN -7
	END

	--MODIFICACION DE DOMICILIO
	EXEC @RET = SPSNCV_PROD_SIFMDOMI_UPDATE
			@ID_CLIENSEC, @ID_DOMICSEC, 
			@DOMICCAL, @DOMICDOM, 
			@DOMICDNU, @DOMICESC, 
			@DOMICPIS, @DOMICPTA, 
			@DOMICPOB, @DOMICCPO, 
			@PROVICOD, @PAISSCOD, 
			@TELCOD, @TELNRO, 
			@PRINCIPAL
	IF @RET = -1
	BEGIN
		--ERROR AL INSERTAR DOMICILIOS
		ROLLBACK TRANSACTION
		RETURN -8
	END

	-- MODIFICACION DE CUENTA
	EXEC @RET = SPSNCV_PROD_SIFMCUEN_UPDATE
			@ID_CLIENSEC, @ID_CUENTSEC, 
			@TIPOCUEN, @COBROTIP, 
			@BANCOCOD, @SUCURCOD, 
			@CUENTDC, @CUENNUME, 
			@TARJECOD, @VENCIANN, 
			@VENCIMES, @VENCIDIA
	IF @RET = -1
	BEGIN
		--ERROR AL INSERTAR LA CUENTA
		ROLLBACK TRANSACTION
		RETURN -9
	END

	-- ***** ACTUALIZACION DE LA COTIZACION *****
	EXEC @CERTISEC = SPSNCV_PROD_COTI_UPDATE
		@RAMOPCOD, @POLIZANN, @POLIZSEC, @CERTIPOL, @CERTIANN, @CERTISEC, @SUPLENUM, @FRANQCOD, 
		@PQTDES, @PLANCOD, @HIJOS1416, @HIJOS1729, @ZONA, @CODPROV, @SUMALBA, @CLIENIVA, @SUMAASEG, 
		@CLUB_LBA, @CPAANO, @CTAKMS, @ESCERO, @TIENEPLAN, @COND_ADIC, @ASEG_ADIC, @FH_NAC, @SEXO, 
		@ESTCIV, @SIFMVEHI_DES, @PROFECOD, @ACCESORIOS, @REFERIDO, @MOTORNUM, @CHASINUM, @PATENNUM, 
		@AUMARCOD, @AUMODCOD, @AUSUBCOD, @AUADICOD, @AUMODORI, @AUUSOCOD, @AUVTVCOD, @AUVTVDIA, 
		@AUVTVMES, @AUVTVANN, @VEHCLRCOD, @AUKLMNUM, @FABRICAN, @FABRICMES, @GUGARAGE, @GUDOMICI, 
		@AUCATCOD, @AUTIPCOD, @AUCIASAN, @AUANTANN, @AUNUMSIN, @AUNUMKMT, @AUUSOGNC, @USUARCOD, 
		@SITUCPOL, @ID_CLIENSEC, @NUMEDOCU, @TIPODOCU, @ID_DOMICSEC, @ID_CUENTSEC, @COBROFOR, @EDADACTU, 
		@COBERCOD1, @COBERORD1, @CAPITASG1, @CAPITIMP1, @COBERCOD2, @COBERORD2, @CAPITASG2, 
		@CAPITIMP2, @COBERCOD3, @COBERORD3, @CAPITASG3, @CAPITIMP3, @COBERCOD4, @COBERORD4, 
		@CAPITASG4, @CAPITIMP4, @COBERCOD5, @COBERORD5, @CAPITASG5, @CAPITIMP5, @COBERCOD6, 
		@COBERORD6, @CAPITASG6, @CAPITIMP6, @COBERCOD7, @COBERORD7, @CAPITASG7, @CAPITIMP7, 
		@COBERCOD8, @COBERORD8, @CAPITASG8, @CAPITIMP8, @COBERCOD9, @COBERORD9, @CAPITASG9, 
		@CAPITIMP9, @COBERCOD10, @COBERORD10, @CAPITASG10, @CAPITIMP10, @COBERCOD11, @COBERORD11, 
		@CAPITASG11, @CAPITIMP11, @COBERCOD12, @COBERORD12, @CAPITASG12, @CAPITIMP12, @COBERCOD13, 
		@COBERORD13, @CAPITASG13, @CAPITIMP13, @COBERCOD14, @COBERORD14, @CAPITASG14, @CAPITIMP14, 
		@COBERCOD15, @COBERORD15, @CAPITASG15, @CAPITIMP15, @COBERCOD16, @COBERORD16, @CAPITASG16, 
		@CAPITIMP16, @COBERCOD17, @COBERORD17, @CAPITASG17, @CAPITIMP17, @COBERCOD18, @COBERORD18, 
		@CAPITASG18, @CAPITIMP18, @COBERCOD19, @COBERORD19, @CAPITASG19, @CAPITIMP19, @COBERCOD20, 
		@COBERORD20, @CAPITASG20, @CAPITIMP20, @DOCUMDAT1, @NOMBREAS1, @DOCUMDAT2, @NOMBREAS2, 
		@DOCUMDAT3, @NOMBREAS3, @DOCUMDAT4, @NOMBREAS4, @DOCUMDAT5, @NOMBREAS5, @DOCUMDAT6, 
		@NOMBREAS6, @DOCUMDAT7, @NOMBREAS7, @DOCUMDAT8, @NOMBREAS8, @DOCUMDAT9, @NOMBREAS9, 
		@DOCUMDAT10, @NOMBREAS10, @CAMP_CODIGO, @CAMP_DESC, @LEGAJO_GTE, @NRO_PROD, 
		@SUCURSAL_CODIGO, @LEGAJO_VEND, @CONDUAPE1, @CONDUNOM1, @CONDUFEC1, @CONDUSEX1, 
		@CONDUEST1, @CONDUEXC1, @CONDUAPE2, @CONDUNOM2, @CONDUFEC2, @CONDUSEX2, @CONDUEST2, 
		@CONDUEXC2, @CONDUAPE3, @CONDUNOM3, @CONDUFEC3, @CONDUSEX3, @CONDUEST3, @CONDUEXC3, 
		@CONDUAPE4, @CONDUNOM4, @CONDUFEC4, @CONDUSEX4, @CONDUEST4, @CONDUEXC4, @CONDUAPE5, 
		@CONDUNOM5, @CONDUFEC5, @CONDUSEX5, @CONDUEST5, @CONDUEXC5, @CONDUAPE6, @CONDUNOM6, 
		@CONDUFEC6, @CONDUSEX6, @CONDUEST6, @CONDUEXC6, @CONDUAPE7, @CONDUNOM7, @CONDUFEC7, 
		@CONDUSEX7, @CONDUEST7, @CONDUEXC7, @CONDUAPE8, @CONDUNOM8, @CONDUFEC8, @CONDUSEX8, 
		@CONDUEST8, @CONDUEXC8, @CONDUAPE9, @CONDUNOM9, @CONDUFEC9, @CONDUSEX9, @CONDUEST9, 
		@CONDUEXC9, @CONDUAPE10, @CONDUNOM10, @CONDUFEC10, @CONDUSEX10, @CONDUEST10, @CONDUEXC10, 
		@AUACCCOD1, @AUVEASUM1, @AUVEADES1, @AUVEADEP1, @AUACCCOD2, @AUVEASUM2, @AUVEADES2, 
		@AUVEADEP2, @AUACCCOD3, @AUVEASUM3, @AUVEADES3, @AUVEADEP3, @AUACCCOD4, @AUVEASUM4, 
		@AUVEADES4, @AUVEADEP4, @AUACCCOD5, @AUVEASUM5, @AUVEADES5, @AUVEADEP5, @AUACCCOD6, 
		@AUVEASUM6, @AUVEADES6, @AUVEADEP6, @AUACCCOD7, @AUVEASUM7, @AUVEADES7, @AUVEADEP7, 
		@AUACCCOD8, @AUVEASUM8, @AUVEADES8, @AUVEADEP8, @AUACCCOD9, @AUVEASUM9, @AUVEADES9, 
		@AUVEADEP9, @AUACCCOD10, @AUVEASUM10, @AUVEADES10, @AUVEADEP10, @BANCOCOD, @COBROCOD, 
		@EFECTANN, @EFECTMES, @EFECTDIA, @COBROTIP,
--jc 08/2010 campos nuevos
		'', '', @IBB, '', @LUNETA, 0, '', '', '', '', @DESTRUCCION_80, '', @CLUBECO, @GRANIZO, @ROBOCONT
-- jc fin


	IF @RET = -1
	BEGIN
		--ERROR AL INSERTAR DOMICILIOS
		ROLLBACK TRANSACTION
		RETURN -10
	END

END


--FIN DEL SP
COMMIT TRANSACTION

WHILE @@TRANCOUNT > 0
	commit

RETURN @CERTISEC








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

