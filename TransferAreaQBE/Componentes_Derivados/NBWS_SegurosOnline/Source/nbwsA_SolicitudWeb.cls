VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_SolicitudWeb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "nbwsA_Transacciones.nbwsA_SolicitudWeb"

Const mcteParam_MAIL            As String = "//MAIL"
Const mcteParam_DOCUMDAT        As String = "//DOCUMDAT"
Const mcteParam_DOCUMTIP        As String = "//DOCUMTIP"
Const mcteParam_CONFORMIDAD     As String = "//CONFORMIDAD"

'Constantes de error
Private mcteMsg_DESCRIPTION(0 To 15)  As String
Const mcteMsg_OK                    As Integer = 0
Const mcteMsg_EXISTEUSR             As Integer = 1
Const mcteMsg_EXISTEDOC             As Integer = 2
Const mcteMsg_EXISTEMAIL            As Integer = 3
Const mcteMsg_NOPRODNBWS            As Integer = 4
Const mcteMsg_NOPRODNAVEXVEND       As Integer = 5
Const mcteMsg_NOPRODNAVEXCERT       As Integer = 6
Const mcteMsg_NOEXISTEUSR           As Integer = 7
Const mcteMsg_NOEXISTEUSRSQL        As Integer = 8
Const mcteMsg_NOEXISTEUSRAIS        As Integer = 9
Const mcteMsg_ERRCONSULTA           As Integer = 10
Const mcteMsg_ERRCONSULTASQL        As Integer = 11
Const mcteMsg_ERRCONSULTAAIS        As Integer = 12
Const mcteMsg_NOPRODNBWS_HAB        As Integer = 13
Const mcteMsg_NOPRODNBWS_POSITIVEID As Integer = 14
Const mcteMsg_POLIZAEXCLUIDA        As Integer = 15

Private Function IAction_Execute(ByVal pvarRequest As String, pvarResponse As String, ByVal ContextInfo As String) As Long
    Const wcteFnName                As String = "IAction_Execute"
    Dim wobjClass                   As HSBCInterfaces.IAction
    Dim wvarStep                    As Integer
    '
    Dim wobjXMLRequest              As MSXML2.DOMDocument 'XML con el request
    Dim wobjXMLResponse             As MSXML2.DOMDocument 'XML con el response del SQL
    '
    Dim wobjNodeList                As MSXML2.IXMLDOMNodeList
    '
    Dim wvarRequest                 As String
    Dim wvarResponse                As String
    '
    Dim wvarMail                    As String
    Dim wvarDocumtip                As String
    Dim wvarDocumdat                As String
    Dim wvarCONFORMIDAD             As String
    Dim wvarError                   As Integer
    Dim wvarReturnMsg               As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'Inicializacion de variables
    wvarStep = 10
    wvarError = mcteMsg_OK
    wvarReturnMsg = ""
    '
    'Definicion de mensajes de error
    wvarStep = 15
    mcteMsg_DESCRIPTION(mcteMsg_OK) = getMensaje(mcteClassName, "mcteMsg_OK")
    mcteMsg_DESCRIPTION(mcteMsg_EXISTEUSR) = getMensaje(mcteClassName, "mcteMsg_EXISTEUSR")
    mcteMsg_DESCRIPTION(mcteMsg_EXISTEDOC) = getMensaje(mcteClassName, "mcteMsg_EXISTEDOC")
    mcteMsg_DESCRIPTION(mcteMsg_EXISTEMAIL) = getMensaje(mcteClassName, "mcteMsg_EXISTEMAIL")
    mcteMsg_DESCRIPTION(mcteMsg_NOPRODNBWS) = getMensaje(mcteClassName, "mcteMsg_NOPRODNBWS")
    mcteMsg_DESCRIPTION(mcteMsg_NOPRODNAVEXVEND) = getMensaje(mcteClassName, "mcteMsg_NOPRODNAVEXVEND")
    mcteMsg_DESCRIPTION(mcteMsg_NOPRODNAVEXCERT) = getMensaje(mcteClassName, "mcteMsg_NOPRODNAVEXCERT")
    mcteMsg_DESCRIPTION(mcteMsg_NOEXISTEUSR) = getMensaje(mcteClassName, "mcteMsg_NOEXISTEUSR")
    mcteMsg_DESCRIPTION(mcteMsg_NOEXISTEUSRSQL) = getMensaje(mcteClassName, "mcteMsg_NOEXISTEUSRSQL")
    mcteMsg_DESCRIPTION(mcteMsg_NOEXISTEUSRAIS) = getMensaje(mcteClassName, "mcteMsg_NOEXISTEUSRAIS")
    mcteMsg_DESCRIPTION(mcteMsg_ERRCONSULTA) = getMensaje(mcteClassName, "mcteMsg_ERRCONSULTA")
    mcteMsg_DESCRIPTION(mcteMsg_ERRCONSULTASQL) = getMensaje(mcteClassName, "mcteMsg_ERRCONSULTASQL")
    mcteMsg_DESCRIPTION(mcteMsg_ERRCONSULTAAIS) = getMensaje(mcteClassName, "mcteMsg_ERRCONSULTAAIS")
    mcteMsg_DESCRIPTION(mcteMsg_NOPRODNBWS_HAB) = getMensaje(mcteClassName, "mcteMsg_NOPRODNBWS_HAB")
    mcteMsg_DESCRIPTION(mcteMsg_NOPRODNBWS_POSITIVEID) = getMensaje(mcteClassName, "mcteMsg_NOPRODNBWS_POSITIVEID")
    mcteMsg_DESCRIPTION(mcteMsg_POLIZAEXCLUIDA) = getMensaje(mcteClassName, "mcteMsg_POLIZAEXCLUIDA")
    '
    'Carga del REQUEST
    wvarStep = 20
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    wobjXMLRequest.loadXML pvarRequest
    '
    'Obtiene parametros
    wvarStep = 30
    With wobjXMLRequest
        wvarMail = .selectSingleNode(mcteParam_MAIL).Text
        wvarDocumtip = .selectSingleNode(mcteParam_DOCUMTIP).Text
        wvarDocumdat = .selectSingleNode(mcteParam_DOCUMDAT).Text
        wvarCONFORMIDAD = .selectSingleNode(mcteParam_CONFORMIDAD).Text
    End With
    '
    'Armado del Request para nbwsA_ccBuscaSuscr
    wvarStep = 40
    wvarRequest = "<Request>" & _
                    "<DOCUMTIP>" & wvarDocumtip & "</DOCUMTIP>" & _
                    "<DOCUMDAT>" & wvarDocumdat & "</DOCUMDAT>" & _
                    "<MAIL>" & wvarMail & "</MAIL>" & _
                    "<ACCION>A</ACCION>" & _
                  "</Request>"
    '
    wvarStep = 50
    Set wobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_ccBuscaSuscr")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    '
    wvarStep = 60
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    wobjXMLResponse.async = False
    wobjXMLResponse.setProperty "SelectionLanguage", "XPath"
    wobjXMLResponse.loadXML wvarResponse
    '
    'Analiza resultado del componente
    wvarStep = 80
    If Not wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            '
            wvarStep = 90
            If wobjXMLResponse.selectSingleNode("//CODRESULTADO").Text = "OK" Then
                '
                'HABILITADO_NBWS = S (habilitado seg�n tabla de prod hab)
                'HABILITADO_NAVEGACION = S (habilitado seg�n vendedores)
                'POLIZA_EXCLUIDA = 'N' (la p�liza no est� excludia x esquema de encuadradas)
                'MAS_DE_CINCO_CERT = 'N' (la p�liza tiene menos de 5 certificados)
                'Con que haya al menos UNA p�liza que cumpla con dicha validaci�n se puede dar de alta
                'DA - 12/11/2009: se agrega la validaci�n MAS_DE_CINCO_CERT = 'N'
                wvarStep = 100
                If wobjXMLResponse.selectNodes("//Response_XML/Response/PRODUCTO[HABILITADO_NBWS='S' and substring(HABILITADO_NAVEGACION,1,1)='S' and POLIZA_EXCLUIDA = 'N' and MAS_DE_CINCO_CERT = 'N']").length <> 0 Then
                    '
                    'DA - 12/11/2009: se separa la validaci�n del PositiveID (defect 22)
                    If wobjXMLResponse.selectNodes("//Response_XML/Response/PRODUCTO[HABILITADO_NBWS='S' and substring(HABILITADO_NAVEGACION,1,1)='S' and POLIZA_EXCLUIDA = 'N' and MAS_DE_CINCO_CERT = 'N' and POSITIVEID != '']").length <> 0 Then
                        wvarError = mcteMsg_OK
                    Else
                        wvarError = mcteMsg_NOPRODNBWS_POSITIVEID
                    End If
                    '
                Else
                    wvarError = mcteMsg_NOPRODNBWS_HAB
                End If
                '
            Else
                wvarStep = 130
                wvarError = CInt(wobjXMLResponse.selectSingleNode("//CODERROR").Text)
            End If
        End If
    End If
    '
    'Si wvarError = mcteMsg_OK entonces procede a dar el alta
    wvarStep = 140
    If wvarError = mcteMsg_OK Then
        '
        wvarStep = 150
        wvarRequest = "<Request>" & _
                        "<DOCUMTIP>" & wvarDocumtip & "</DOCUMTIP>" & _
                        "<DOCUMDAT>" & wvarDocumdat & "</DOCUMDAT>" & _
                        "<MAIL>" & wvarMail & "</MAIL>" & _
                        "<VIAINSCRIPCION>NBWS</VIAINSCRIPCION>" & _
                        "<RESPONSABLEALTA></RESPONSABLEALTA>" & _
                        "<CONFORMIDAD>" & wvarCONFORMIDAD & "</CONFORMIDAD>" & _
                      "</Request>"
        '
        wvarStep = 160
        Set wobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_Alta")
        Call wobjClass.Execute(wvarRequest, wvarResponse, "")
        Set wobjClass = Nothing
        '
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML wvarResponse
        '
        'Analiza resultado del componente
        wvarStep = 170
        If Not wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
            If wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
                '
                wvarStep = 180
                If wobjXMLResponse.selectSingleNode("//CODRESULTADO").Text <> "OK" Then
                    wvarStep = 190
                    wvarError = CInt(wobjXMLResponse.selectSingleNode("//CODERROR").Text)
                End If
            End If
        End If
    End If
    '
    wvarStep = 300
    If wvarError = mcteMsg_OK Then
        wvarStep = 310
        pvarResponse = "<Response>" & _
                            "<Estado resultado=" & Chr(34) & "true" & Chr(34) & " estado=" & Chr(34) & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & _
                            "<CODRESULTADO>OK</CODRESULTADO>" & _
                            "<CODERROR>" & wvarError & "</CODERROR>" & _
                            "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                        "</Response>"
    Else
        wvarStep = 320
        pvarResponse = "<Response>" & _
                            "<Estado resultado=" & Chr(34) & "true" & Chr(34) & " estado=" & Chr(34) & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & _
                            "<CODRESULTADO>ERROR</CODRESULTADO>" & _
                            "<CODERROR>" & wvarError & "</CODERROR>" & _
                            "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                        "</Response>"
    End If
    '
    'Finaliza y libera objetos
    wvarStep = 500
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjNodeList = Nothing
    '
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    'Inserta Error en EventViewer
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjNodeList = Nothing
    '
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function
      
Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub





