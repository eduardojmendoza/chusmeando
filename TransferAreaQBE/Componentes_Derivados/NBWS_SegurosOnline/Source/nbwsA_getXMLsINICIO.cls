VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_getXMLsINICIO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-----------------------------------------------------------------------------------------------------------------------------------
'TODO: poner COPYRIGHT
' *****************************************************************
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "nbwsA_Transacciones.nbwsA_getXMLsINICIO"
Const mcteParam_SESSION_ID  As String = "//SESSION_ID"

'
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest              As MSXML2.DOMDocument
    Dim wobjXMLResponse             As MSXML2.DOMDocument
    Dim wobjXMLNBWS_TempFilesServer As MSXML2.DOMDocument
    '
    Dim wvarStep            As Long
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    '
    Dim wvarSESSION_ID      As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Carga XML de entrada
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
        wobjXMLRequest.async = False
    Call wobjXMLRequest.loadXML(Request)
    '
    wvarStep = 20
    wvarSESSION_ID = wobjXMLRequest.selectSingleNode(mcteParam_SESSION_ID).Text
    '
    'DA - 16/10/2009: se agrega esto para guardar en temporal el XML de inicio y evitar XSS en los ASPs.
    If wvarSESSION_ID <> "" Then
        '
        wvarStep = 30
        'Levanta XML de configuración donde se indica la ruta de grabación de archivos temporales.
        Set wobjXMLNBWS_TempFilesServer = CreateObject("MSXML2.DOMDocument")
            wobjXMLNBWS_TempFilesServer.async = False
        Call wobjXMLNBWS_TempFilesServer.Load(App.Path & "\" & wcteNBWS_TempFilesServer)
        '
        wvarStep = 40
        'Levanta el XML de solapa Inicio
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
            wobjXMLResponse.async = False
        Call wobjXMLResponse.Load(wobjXMLNBWS_TempFilesServer.selectSingleNode("//PATH").Text & "\sINICIO_" & wvarSESSION_ID & ".xml")
        '
    End If
    '
    wvarStep = 50
    If Not wobjXMLResponse.selectSingleNode("//PRODUCTO") Is Nothing Then
        Response = wobjXMLResponse.xml
    Else
        Response = ""
    End If
    '
    wvarStep = 60
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjXMLNBWS_TempFilesServer = Nothing
    '
    wvarStep = 70
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjXMLNBWS_TempFilesServer = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    mobjCOM_Context.SetAbort
    IAction_Execute = 1
    '
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub



