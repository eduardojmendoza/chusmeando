VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_ccDesbloquea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "nbwsA_Transacciones.nbwsA_ccDesbloquea"

Const mcteParam_MAIL            As String = "//MAIL"

'Archivo Configuracion CAI / CAP
Const mcteEMailTemplateConfig    As String = "NBWS_EnvioMailConfig.xml"



'Constantes de XML de definiciones para SQL Generico
Const mcteParam_XMLSQLGENDESBL As String = "P_NBWS_Desbloqueo.xml"

'Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
Private mcteMsg_DESCRIPTION(0 To 12)  As String
Const mcteMsg_OK                As Integer = 0
Const mcteMsg_ERROR             As Integer = 1

Private Function IAction_Execute(ByVal pvarRequest As String, pvarResponse As String, ByVal ContextInfo As String) As Long
    Const wcteFnName                As String = "IAction_Execute"
    Dim wobjClass                   As HSBCInterfaces.IAction
    Dim wvarStep                    As Integer
    '
    Dim wobjXMLRequest              As MSXML2.DOMDocument 'XML con el request
    Dim wobjXMLResponse             As MSXML2.DOMDocument
    '
    Dim wobjRequestSQL              As String
    Dim wobjResponseSQL             As String
    Dim wobjRequestAIS              As String
    Dim wobjResponseAIS             As String
    '
    Dim wvarMail                    As String
    Dim wvarCAP                     As String
    Dim wvarError                   As String
    Dim wvarCAPEnc                  As String
    Dim wvarRetVal                  As String
    Dim wvarRequest                 As String
    Dim wvarResponse                As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'Inicializacion de variables
    wvarStep = 10
    wvarError = mcteMsg_OK
    '
    'Definicion de mensajes de error
    wvarStep = 15
    mcteMsg_DESCRIPTION(mcteMsg_OK) = getMensaje(mcteClassName, "mcteMsg_OK")
    mcteMsg_DESCRIPTION(mcteMsg_ERROR) = getMensaje(mcteClassName, "mcteMsg_ERROR")
    '
    'Carga del REQUEST
    wvarStep = 20
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    wobjXMLRequest.loadXML pvarRequest
    '
    'Obtiene parametros
    wvarStep = 40
    With wobjXMLRequest
        wvarStep = 50
        wvarMail = .selectSingleNode(mcteParam_MAIL).Text
    End With
    '
    wvarStep = 100
    wvarCAP = UCase(generar_RNDSTR(8))
    wvarStep = 130
    wvarCAPEnc = CapicomEncrypt(wvarCAP)
    '
    wvarStep = 150
    wvarRequest = "<Request>" & _
                        "<DEFINICION>" & mcteParam_XMLSQLGENDESBL & "</DEFINICION>" & _
                        "<MAIL>" & wvarMail & "</MAIL>" & _
                        "<CAP><![CDATA[" & wvarCAPEnc & "]]></CAP>" & _
                   "</Request>"
    '
    wvarStep = 200
    Set wobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    '
    fncEnviarMail wvarMail, wvarCAP
    '
    wvarStep = 290
    pvarResponse = "<Response>" & _
                        "<Estado resultado=" & Chr(34) & "true" & Chr(34) & " estado=" & Chr(34) & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & _
                        "<CODRESULTADO>OK</CODRESULTADO>" & _
                        "<CODERROR>" & wvarError & "</CODERROR>" & _
                        "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                    "</Response>"
    '
    'Finaliza y libera objetos
    wvarStep = 700
    Set wobjXMLRequest = Nothing
    '
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    'Inserta Error en EventViewer
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    Set wobjXMLRequest = Nothing
    '
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function


'GD: 17/11/2009 - Envio de mail via SOAPMW

Private Function fncEnviarMail(pvarMail As String, pvarCAP As String) As Boolean
    '
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarXMLResponseMdw  As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wvarResponseMdw     As String
    Dim mvarRequestMDW      As String
    Dim mvarTemplateCAP     As String
    Dim mvarMailPrueba      As String
    Dim mvarCopiaOculta     As String
        
     'Se obtienen los par�metros desde XML
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.Load App.Path & "\" & mcteEMailTemplateConfig
    
   
    mvarTemplateCAP = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/ENVIOCAP").Text
   
    'GED: para que no envie al mail que viene si no al que se define para pruebas, asi se evita mandar al exterior
    ' correos con info no deseada accidentalmente
    
    If Not wobjXMLConfig.selectSingleNode("//MAILPRUEBA") Is Nothing Then
        mvarMailPrueba = wobjXMLConfig.selectSingleNode("//MAILPRUEBA").Text
    Else
        mvarMailPrueba = ""
    End If
    
    If Not wobjXMLConfig.selectSingleNode("//COPIAOCULTA") Is Nothing Then
        mvarCopiaOculta = wobjXMLConfig.selectSingleNode("//COPIAOCULTA").Text
    Else
        mvarCopiaOculta = ""
    End If
    
    'Se cambia envio de mail por medio de MQ.
    mvarRequestMDW = "<Request>" & _
                        "<DEFINICION>ismSendPdfReportLBAVO.xml</DEFINICION>" & _
                        "<Raiz>share:sendPdfReport</Raiz>" & _
                        "<files/>" & _
                        "<applicationId>NBWS</applicationId>" & _
                        "<reportId/>" & _
                        "<recipientTO>" & pvarMail & "</recipientTO>" & _
                        "<recipientsCC/>" & _
                        "<recipientsBCC>" & mvarCopiaOculta & "</recipientsBCC>" & _
                        "<templateFileName>" & mvarTemplateCAP & "</templateFileName>" & _
                        "<parametersTemplate>CLAVE=" & pvarCAP & "</parametersTemplate>" & _
                        "<from/>" & _
                        "<replyTO/>" & _
                        "<bodyText/>" & _
                        "<subject/>" & _
                        "<importance/>" & _
                        "<imagePathFile/>" & _
                        "<attachPassword/>" & _
                        "<attachFileName/>" & _
                    "</Request>"
    
    '
    Set wobjClass = mobjCOM_Context.CreateInstance("LBAA_MWGenerico.lbaw_MQMW")
    Call wobjClass.Execute(mvarRequestMDW, wvarResponseMdw, "")
    '
    Set wobjClass = Nothing
    '
    Set wvarXMLResponseMdw = CreateObject("MSXML2.DOMDocument")
        wvarXMLResponseMdw.async = False
        wvarXMLResponseMdw.loadXML (wvarResponseMdw)
    '
    If Not wvarXMLResponseMdw.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If wvarXMLResponseMdw.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            If wvarXMLResponseMdw.selectSingleNode("//Response/faultstring") Is Nothing Then
                fncEnviarMail = True
            Else
                fncEnviarMail = False
            End If
        Else
            If Not wvarXMLResponseMdw.selectSingleNode("//Response/faultstring") Is Nothing Then
                fncEnviarMail = False
            Else
                fncEnviarMail = False
            End If
            fncEnviarMail = False
        End If
    Else
        fncEnviarMail = False
    End If
    '
    Set wobjClass = Nothing
    Set wvarXMLResponseMdw = Nothing
    Set wobjXMLConfig = Nothing
    
End Function


Private Function fncEnviarMail_bak(pvarMail As String, pvarCAP As String) As Boolean
    '
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarXMLRequest      As MSXML2.DOMDocument
    Dim wvarXMLResponse     As MSXML2.DOMDocument
    Dim wobjXMLParametro    As MSXML2.DOMDocument
    Dim wobjXMLCODOPParam   As MSXML2.DOMDocument
    Dim wvarRellamar        As Boolean
    Dim wvarResponseAIS     As String
    Dim wvarCODOPDesc       As String
    '
    Set wobjXMLParametro = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametro.Load App.Path & "\XMLs\NBWSEnvioCAP.xml"
    '
    agregarParametro wobjXMLParametro, "%%CAP%%", pvarCAP
    '
    wobjXMLParametro.selectSingleNode("//TO").Text = pvarMail
    '
    wvarRequest = wobjXMLParametro.selectSingleNode("//Request").xml
    '
    Set wobjClass = mobjCOM_Context.CreateInstance("cam_OficinaVirtual.camA_EnviarMail")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    '
    Set wvarXMLResponse = CreateObject("MSXML2.DOMDocument")
        wvarXMLResponse.async = False
        wvarXMLResponse.loadXML (wvarResponse)
    '
    If Not wvarXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If wvarXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            fncEnviarMail_bak = True
        Else
            fncEnviarMail_bak = False
        End If
    Else
        fncEnviarMail_bak = False
    End If
    '
    Set wvarXMLResponse = Nothing
    Set wobjClass = Nothing
    Set wobjXMLParametro = Nothing
End Function

Private Sub agregarParametro(ByRef pobjXMLParametro As MSXML2.DOMDocument, pvarParam As String, pvarValor As String)
    Dim wobjXMLElement      As MSXML2.IXMLDOMElement
    Dim wobjXMLElementAux   As MSXML2.IXMLDOMElement
    Dim wobjXMLElementCda   As MSXML2.IXMLDOMCDATASection
    Dim wobjXMLParametro    As MSXML2.DOMDocument
    
    Set wobjXMLElement = pobjXMLParametro.createElement("PARAMETRO")
    '
    Set wobjXMLElementAux = pobjXMLParametro.createElement("PARAM_NOMBRE")
    Set wobjXMLElementCda = pobjXMLParametro.createCDATASection(pvarParam)
        wobjXMLElementAux.appendChild wobjXMLElementCda
        wobjXMLElement.appendChild wobjXMLElementAux
    Set wobjXMLElementAux = pobjXMLParametro.createElement("PARAM_VALOR")
    Set wobjXMLElementCda = pobjXMLParametro.createCDATASection(pvarValor)
        wobjXMLElementAux.appendChild wobjXMLElementCda
    '
    wobjXMLElement.appendChild wobjXMLElementAux
    '
    pobjXMLParametro.selectSingleNode("//PARAMETROS").appendChild wobjXMLElement
    '
End Sub

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub



