VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_setBlanqueo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "nbwsA_Transacciones.nbwsA_setBlanqueo"

Const mcteParam_MAIL            As String = "//MAIL"
Const mcteParam_DOCUMTIP        As String = "//DOCUMTIP"
Const mcteParam_DOCUMDAT        As String = "//DOCUMDAT"
Const mcteParam_RESPUESTA       As String = "//RESPUESTA"


'Constantes de XML de definiciones para SQL Generico
Const mcteParam_XMLSQLGENCLISUS As String = "P_NBWS_ClienteSuscripto.xml"

'Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
Private mcteMsg_DESCRIPTION(0 To 12)  As String
Const mcteMsg_OK                As Integer = 0
Const mcteMsg_USRNOEXISTE       As Integer = 1
Const mcteMsg_DATOSINVALIDOS    As Integer = 2

Private Function IAction_Execute(ByVal pvarRequest As String, pvarResponse As String, ByVal ContextInfo As String) As Long
    Const wcteFnName                As String = "IAction_Execute"
    Dim wobjClass                   As HSBCInterfaces.IAction
    Dim wvarStep                    As Integer
    '
    Dim wobjXMLRequest              As MSXML2.DOMDocument 'XML con el request
    '
    Dim wobjRequestSQL              As String
    Dim wobjResponseSQL             As String
    Dim wobjRequestAIS              As String
    Dim wobjResponseAIS             As String
    '
    Dim wvarMail                    As String
    Dim wvarDocumtip                As String
    Dim wvarDocumdat                As String
    Dim wvarRespuesta               As String
    Dim wvarError                   As Integer
    Dim wvarEstadoIdentificador     As String
    Dim wvarEstadoPassword          As String
    Dim wvarUsrValido               As Boolean
    Dim wvarResChk                  As Boolean
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'Inicializacion de variables
    wvarStep = 10
    wvarError = mcteMsg_OK
    wvarResChk = False
    '
    'Definicion de mensajes de error
    wvarStep = 15
    mcteMsg_DESCRIPTION(mcteMsg_OK) = getMensaje(mcteClassName, "mcteMsg_OK")
    mcteMsg_DESCRIPTION(mcteMsg_USRNOEXISTE) = getMensaje(mcteClassName, "mcteMsg_USRNOEXISTE")
    mcteMsg_DESCRIPTION(mcteMsg_DATOSINVALIDOS) = getMensaje(mcteClassName, "mcteMsg_DATOSINVALIDOS")
    '
    'Carga del REQUEST
    wvarStep = 20
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    wobjXMLRequest.loadXML pvarRequest
    '
    'Obtiene parametros
    wvarStep = 40
    With wobjXMLRequest
        wvarStep = 50
        wvarMail = .selectSingleNode(mcteParam_MAIL).Text
        wvarStep = 60
        wvarDocumtip = .selectSingleNode(mcteParam_DOCUMTIP).Text
        wvarStep = 70
        wvarDocumdat = .selectSingleNode(mcteParam_DOCUMDAT).Text
        wvarStep = 80
        wvarRespuesta = .selectSingleNode(mcteParam_RESPUESTA).Text
    End With
    '
    '1) Validar existencia de usuario
    wvarStep = 150
    wvarUsrValido = fncValidarDatosUsuario(wvarMail, wvarDocumtip, wvarDocumdat)
    If Not wvarUsrValido Then
        wvarError = mcteMsg_DATOSINVALIDOS
    End If
    '
    wvarStep = 190
    If wvarError = mcteMsg_OK Then
        '
        wvarResChk = fncChkRespuesta(wvarMail, wvarRespuesta)
        '
        If Not wvarResChk Then
            wvarStep = 200
            wvarError = mcteMsg_DATOSINVALIDOS
        Else
            fncDesbloquear wvarMail
        End If
    End If
     
    If wvarError = mcteMsg_OK Then
        pvarResponse = "<Response>" & _
                       "<Estado resultado=" & Chr(34) & "true" & Chr(34) & " estado=" & Chr(34) & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & _
                       "<CODRESULTADO>OK</CODRESULTADO>" & _
                       "<CODERROR>" & wvarError & "</CODERROR>" & _
                       "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                   "</Response>"
    Else
        wvarStep = 210
        pvarResponse = "<Response>" & _
                            "<Estado resultado=" & Chr(34) & "true" & Chr(34) & " estado=" & Chr(34) & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & _
                            "<CODRESULTADO>ERR</CODRESULTADO>" & _
                            "<CODERROR>" & wvarError & "</CODERROR>" & _
                            "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                        "</Response>"
    End If
    '
    'Finaliza y libera objetos
    wvarStep = 500
    Set wobjXMLRequest = Nothing
    '
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    'Inserta Error en EventViewer
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    Set wobjXMLRequest = Nothing
    '
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function
      
Private Function fncValidarDatosUsuario(ByVal pvarMail As String, ByRef pvarDocumtip As String, ByRef pvarDocumdat As String) As Boolean
    '
    Dim mvarRequest     As String
    Dim mvarResponse    As String
    Dim mvarRetVal      As Boolean
    Dim mobjXMLResponse As MSXML2.DOMDocument
    Dim mobjClass       As HSBCInterfaces.IAction
    '
    mvarRetVal = False
    '
    mvarRequest = "<Request>" & _
                        "<DEFINICION>" & mcteParam_XMLSQLGENCLISUS & "</DEFINICION>" & _
                        "<DOCUMTIP>" & pvarDocumtip & "</DOCUMTIP>" & _
                        "<DOCUMDAT>" & pvarDocumdat & "</DOCUMDAT>" & _
                        "<MAIL>" & pvarMail & "</MAIL>" & _
                   "</Request>"
    '
    Set mobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
    Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    Set mobjClass = Nothing
    '
    Set mobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    mobjXMLResponse.async = False
    mobjXMLResponse.loadXML mvarResponse
    '
    'Analiza resultado del SP
    If Not mobjXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If mobjXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            '
            If mobjXMLResponse.selectSingleNode("//CORRECTO").Text = "S" Then
                mvarRetVal = True
            End If
        End If
    End If
    '
    fncValidarDatosUsuario = mvarRetVal
    Set mobjClass = Nothing
    Set mobjXMLResponse = Nothing
    '
End Function

Private Function fncChkRespuesta(pvarMail, pvarRespuesta) As Boolean
    '
    Dim mvarRequest     As String
    Dim mvarResponse    As String
    Dim mobjClass       As HSBCInterfaces.IAction
    Dim wobjXMLResponse As MSXML2.DOMDocument
    Dim wvarRespuesta   As String
    Dim mvarRetVal      As Boolean
    '
    mvarRetVal = False
    '
    mvarRequest = "<Request>" & _
                      "<EMAIL>" & pvarMail & "</EMAIL>" & _
                  "</Request>"
    '
    Set mobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_getMisDatos")
    Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    Set mobjClass = Nothing
    '
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    wobjXMLResponse.async = False
    wobjXMLResponse.loadXML mvarResponse
    '
    If Not wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            wvarRespuesta = wobjXMLResponse.selectSingleNode("//RESPUESTA").Text
            If UCase(wvarRespuesta) = UCase(pvarRespuesta) Then
                mvarRetVal = True
            End If
        End If
    End If
    '
    fncChkRespuesta = mvarRetVal
    '
End Function

Private Function fncDesbloquear(pvarMail) As Boolean
    '
    Dim mvarRequest     As String
    Dim mvarResponse    As String
    Dim mobjClass       As HSBCInterfaces.IAction
    Dim wobjXMLResponse As MSXML2.DOMDocument
    Dim wvarRespuesta   As String
    Dim mvarRetVal      As Boolean
    '
    mvarRetVal = False
    '
    mvarRequest = "<Request>" & _
                      "<MAIL>" & pvarMail & "</MAIL>" & _
                  "</Request>"
    '
    Set mobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_ccDesbloquea")
    Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    Set mobjClass = Nothing
    '
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    wobjXMLResponse.async = False
    wobjXMLResponse.loadXML mvarResponse
    '
    If Not wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            mvarRetVal = True
        End If
    End If
    '
    fncDesbloquear = mvarRetVal
    '
End Function

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub








