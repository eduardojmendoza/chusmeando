VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_chkPositiveId"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "nbwsA_Transacciones.nbwsA_chkPositiveId"

Const mcteParam_USUARIO         As String = "//USUARIO"
Const mcteParam_PRODUCTO        As String = "//PRODUCTO"
Const mcteParam_DATO1           As String = "//DATO1"
Const mcteParam_DATO2           As String = "//DATO2"
Const mcteParam_DATO3           As String = "//DATO3"
Const mcteParam_DATO4           As String = "//DATO4"

'Constantes de XML de definiciones para SQL Generico
Const mcteParam_XMLSQLGENVALUSR As String = "P_NBWS_ValidaUsuario.xml"
Const mcteParam_XMLSQLGENOBTIDN As String = "P_NBWS_ObtenerIdentificador.xml"

'Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
Private mcteMsg_DESCRIPTION(0 To 12)  As String
Const mcteMsg_OK                As Integer = 0
Const mcteMsg_USRNOEXISTE       As Integer = 1
Const mcteMsg_DATOSINVALIDOS    As Integer = 2

Private Function IAction_Execute(ByVal pvarRequest As String, pvarResponse As String, ByVal ContextInfo As String) As Long
    Const wcteFnName                As String = "IAction_Execute"
    Dim wobjClass                   As HSBCInterfaces.IAction
    Dim wvarStep                    As Integer
    '
    Dim wobjXMLRequest              As MSXML2.DOMDocument 'XML con el request
    '
    Dim wobjRequestSQL              As String
    Dim wobjResponseSQL             As String
    Dim wobjRequestAIS              As String
    Dim wobjResponseAIS             As String
    '
    Dim wvarUsuario                 As String
    Dim wvarProducto                As String
    Dim wvarDato1                   As String
    Dim wvarDato2                   As String
    Dim wvarDato3                   As String
    Dim wvarDato4                   As String
    Dim wvarError                   As Integer
    Dim wvarEstadoIdentificador     As String
    Dim wvarEstadoPassword          As String
    Dim wvarEstadoUsr               As String
    Dim wvarResChk                  As Boolean
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'Inicializacion de variables
    wvarStep = 10
    wvarError = mcteMsg_OK
    wvarResChk = False
    '
    'Definicion de mensajes de error
    wvarStep = 15
    mcteMsg_DESCRIPTION(mcteMsg_OK) = getMensaje(mcteClassName, "mcteMsg_OK")
    mcteMsg_DESCRIPTION(mcteMsg_USRNOEXISTE) = getMensaje(mcteClassName, "mcteMsg_USRNOEXISTE")
    mcteMsg_DESCRIPTION(mcteMsg_DATOSINVALIDOS) = getMensaje(mcteClassName, "mcteMsg_DATOSINVALIDOS")
    '
    'Carga del REQUEST
    wvarStep = 20
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
        wobjXMLRequest.async = False
        wobjXMLRequest.loadXML pvarRequest
    '
    'Obtiene parametros
    wvarStep = 40
    With wobjXMLRequest
        wvarStep = 50
        wvarUsuario = .selectSingleNode(mcteParam_USUARIO).Text
        wvarStep = 60
        wvarProducto = .selectSingleNode(mcteParam_PRODUCTO).Text
        wvarStep = 70
        wvarDato1 = .selectSingleNode(mcteParam_DATO1).Text
        wvarStep = 80
        wvarDato2 = .selectSingleNode(mcteParam_DATO2).Text
        wvarStep = 90
        wvarDato3 = .selectSingleNode(mcteParam_DATO3).Text
        wvarStep = 100
        wvarDato4 = .selectSingleNode(mcteParam_DATO4).Text
    End With
    '
    '1) Validar existencia de usuario
    wvarStep = 150
    wvarEstadoUsr = fncValidarUsuario(wvarUsuario, wvarEstadoIdentificador, wvarEstadoPassword)
    If wvarEstadoUsr = "ERR" Then
        wvarError = mcteMsg_USRNOEXISTE
    Else
        wvarError = mcteMsg_OK
    End If
    '
    wvarStep = 190
    If wvarError = mcteMsg_OK Then
        '
        wvarResChk = fncChkPositiveId(wvarUsuario, wvarProducto, wvarDato1, wvarDato2, wvarDato3, wvarDato4)
        '
        If wvarResChk Then
            wvarStep = 200
            pvarResponse = "<Response>" & _
                                "<Estado resultado=" & Chr(34) & "true" & Chr(34) & " estado=" & Chr(34) & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & _
                                "<CODRESULTADO>OK</CODRESULTADO>" & _
                                "<CODERROR>" & wvarError & "</CODERROR>" & _
                                "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                            "</Response>"
        Else
            wvarError = mcteMsg_DATOSINVALIDOS
            wvarStep = 210
            pvarResponse = "<Response>" & _
                                "<Estado resultado=" & Chr(34) & "true" & Chr(34) & " estado=" & Chr(34) & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & _
                                "<CODRESULTADO>ERR</CODRESULTADO>" & _
                                "<CODERROR>" & wvarError & "</CODERROR>" & _
                                "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                            "</Response>"
        End If
    Else
        wvarStep = 220
        pvarResponse = "<Response>" & _
                            "<Estado resultado=" & Chr(34) & "true" & Chr(34) & " estado=" & Chr(34) & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & _
                            "<CODRESULTADO>ERR</CODRESULTADO>" & _
                            "<CODERROR>" & wvarError & "</CODERROR>" & _
                            "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                        "</Response>"
    End If
    '
    'Finaliza y libera objetos
    wvarStep = 500
    Set wobjXMLRequest = Nothing
    '
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    'Inserta Error en EventViewer
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    Set wobjXMLRequest = Nothing
    '
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function
      
Private Function fncValidarUsuario(ByVal pvarUsuario As String, ByRef pvarEstadoIdentificador As String, ByRef pvarEstadoPassword As String) As String
    '
    Dim mvarRequest     As String
    Dim mvarResponse    As String
    Dim mvarRetVal      As String
    Dim mobjXMLResponse As MSXML2.DOMDocument
    Dim mobjClass       As HSBCInterfaces.IAction
    '
    mvarRequest = "<Request>" & _
                        "<DEFINICION>" & mcteParam_XMLSQLGENVALUSR & "</DEFINICION>" & _
                        "<MAIL>" & pvarUsuario & "</MAIL>" & _
                   "</Request>"
    '
    Set mobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
    Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    Set mobjClass = Nothing
    '
    Set mobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    mobjXMLResponse.async = False
    mobjXMLResponse.loadXML mvarResponse
    '
    'Analiza resultado del SP
    If Not mobjXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If mobjXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            '
            If mobjXMLResponse.selectSingleNode("//EXISTE").Text = "S" Or mobjXMLResponse.selectSingleNode("//EXISTE").Text = "N" Then
                mvarRetVal = mobjXMLResponse.selectSingleNode("//ESTADO").Text
                pvarEstadoIdentificador = mobjXMLResponse.selectSingleNode("//ESTADOIDENTIFICADOR").Text
                pvarEstadoPassword = mobjXMLResponse.selectSingleNode("//ESTADOPASSWORD").Text
            End If
        End If
    End If
    '
    If mvarRetVal = "" Then
        mvarRetVal = "ERR"
    End If
    '
    fncValidarUsuario = mvarRetVal
    Set mobjClass = Nothing
    Set mobjXMLResponse = Nothing
    '
End Function

Private Function fncChkPositiveId(pvarUsuario, pvarProducto, pvarDato1, pvarDato2, pvarDato3, pvarDato4) As Boolean
    '
    Dim mvarRequest     As String
    Dim mvarResponse    As String
    Dim mobjClass       As HSBCInterfaces.IAction
    Dim wobjXMLResponse As MSXML2.DOMDocument
    Dim wobjNodeList    As MSXML2.IXMLDOMNodeList
    Dim mvarRetVal      As Boolean
    Dim mvarDOCUMTIP    As String
    Dim mvarDOCUMDAT    As String
    Dim mvarRCCPrevio   As String
    Dim wArrProducto
    Dim mvarRAMOPCOD    As String
    Dim mvarPOLIZANN    As String
    Dim mvarPOLIZSEC    As String
    Dim mvarCERTIPOL    As String
    Dim mvarCERTIANN    As String
    Dim mvarCERTISEC    As String
    Dim wvarFiltro      As String
    Dim mvarDato1       As String
    Dim mvarDato2       As String
    Dim mvarDato3       As String
    Dim mvarDato4       As String
    '
    mvarRetVal = False
    '
    fncObtenerIdentificador pvarUsuario, mvarDOCUMTIP, mvarDOCUMDAT, mvarRCCPrevio
    '
    mvarRequest = "<Request>" & _
                      "<DOCUMTIP>" & mvarDOCUMTIP & "</DOCUMTIP>" & _
                      "<DOCUMDAT>" & mvarDOCUMDAT & "</DOCUMDAT>" & _
                  "</Request>"
    '
    Set mobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_sInicio")
    Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    Set mobjClass = Nothing
    '
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    wobjXMLResponse.async = False
    wobjXMLResponse.loadXML mvarResponse
    '
    'Replica los pasos inversos de nbwsA_Ingresar
    '
    wArrProducto = Split(pvarProducto, "|")
    mvarRAMOPCOD = wArrProducto(0)
    mvarPOLIZANN = wArrProducto(1)
    mvarPOLIZSEC = wArrProducto(2)
    mvarCERTIPOL = wArrProducto(3)
    mvarCERTIANN = wArrProducto(4)
    mvarCERTISEC = wArrProducto(5)
    '
    'Arma el query XPATH
    '
    If Not IsNumeric(pvarDato1) Then mvarDato1 = "'" & UCase(pvarDato1) & "'" Else mvarDato1 = pvarDato1
    If Not IsNumeric(pvarDato2) Then mvarDato2 = "'" & UCase(pvarDato2) & "'" Else mvarDato2 = pvarDato2
    If Not IsNumeric(pvarDato3) Then mvarDato3 = "'" & UCase(pvarDato3) & "'" Else mvarDato3 = pvarDato3
    If Not IsNumeric(pvarDato4) Then mvarDato4 = "'" & UCase(pvarDato4) & "'" Else mvarDato4 = pvarDato4
    '
    wvarFiltro = "//PRODUCTO[RAMOPCOD='" & mvarRAMOPCOD & "'" & _
                    " and POLIZANN='" & mvarPOLIZANN & "'" & _
                    " and POLIZSEC='" & mvarPOLIZSEC & "'" & _
                    " and CERTIPOL='" & mvarCERTIPOL & "'" & _
                    " and CERTIANN='" & mvarCERTIANN & "'" & _
                    " and CERTISEC='" & mvarCERTISEC & "'" & _
                    " and DATO1=" & mvarDato1 & _
                    " and DATO2=" & mvarDato2 & _
                    " and DATO3=" & mvarDato3 & _
                    " and DATO4=" & mvarDato4 & _
                    "]"
    '
    If Not wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            Set wobjNodeList = wobjXMLResponse.selectNodes(wvarFiltro)
            If wobjNodeList.length > 0 Then
                mvarRetVal = True
            End If
        End If
    End If
    '
    fncChkPositiveId = mvarRetVal
    '
End Function

Private Function fncObtenerIdentificador(ByVal pvarUsuario As String, ByRef pvarDocumtip As String, ByRef pvarDocumdat As String, ByRef pvarRCCPrevio As String) As String
    '
    Dim mvarRequest     As String
    Dim mvarResponse    As String
    Dim mobjXMLResponse As MSXML2.DOMDocument
    Dim mobjClass       As HSBCInterfaces.IAction
    '
    mvarRequest = "<Request>" & _
                        "<DEFINICION>" & mcteParam_XMLSQLGENOBTIDN & "</DEFINICION>" & _
                        "<MAIL>" & pvarUsuario & "</MAIL>" & _
                   "</Request>"
    '
    Set mobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
    Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    Set mobjClass = Nothing
    '
    Set mobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    mobjXMLResponse.async = False
    mobjXMLResponse.loadXML mvarResponse
    '
    'Analiza resultado del SP
    If Not mobjXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If mobjXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            pvarDocumtip = mobjXMLResponse.selectSingleNode("//DOCUMTIP").Text
            pvarDocumdat = mobjXMLResponse.selectSingleNode("//DOCUMDAT").Text
            pvarRCCPrevio = mobjXMLResponse.selectSingleNode("//RCCULTIMO").Text
        End If
    End If
    '
    If pvarDocumdat = "" Then
        pvarDocumdat = "ERR"
    End If
    '
    Set mobjClass = Nothing
    Set mobjXMLResponse = Nothing
    '
End Function


Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub






