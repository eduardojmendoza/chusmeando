VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_setPreguntas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "nbwsA_Transacciones.nbwsA_setPreguntas"

Const mcteParam_USUARIO         As String = "//USUARIO"
Const mcteParam_PREGUNTA        As String = "//PREGUNTA"
Const mcteParam_RESPUESTA        As String = "//RESPUESTA"

'Constantes de XML de definiciones para SQL Generico
Const mcteParam_XMLSQLGENSetPregResp    As String = "P_NBWS_SetPregResp.xml"
Const mcteParam_XMLSQLGENVALUSR         As String = "P_NBWS_ValidaUsuario.xml"

'Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
Private mcteMsg_DESCRIPTION(0 To 12)  As String
Const mcteMsg_OK                As Integer = 0
Const mcteMsg_USRNOEXISTE       As Integer = 1

Private Function IAction_Execute(ByVal pvarRequest As String, pvarResponse As String, ByVal ContextInfo As String) As Long
    Const wcteFnName                As String = "IAction_Execute"
    Dim wobjClass                   As HSBCInterfaces.IAction
    Dim wvarStep                    As Integer
    '
    Dim wobjXMLRequest              As MSXML2.DOMDocument 'XML con el request
    '
    Dim wobjRequestSQL              As String
    Dim wobjResponseSQL             As String
    Dim wobjRequestAIS              As String
    Dim wobjResponseAIS             As String
    '
    Dim wvarUsuario                 As String
    Dim wvarPREGUNTA                As String
    Dim wvarRespuesta               As String
    Dim wvarError                   As Integer
    Dim wvarEstadoIdentificador     As String
    Dim wvarEstadoPassword          As String
    Dim wvarEstadoUsr               As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'Inicializacion de variables
    wvarStep = 10
    wvarError = mcteMsg_OK
    '
    'Definicion de mensajes de error
    wvarStep = 15
    mcteMsg_DESCRIPTION(mcteMsg_OK) = getMensaje(mcteClassName, "mcteMsg_OK")
    mcteMsg_DESCRIPTION(mcteMsg_USRNOEXISTE) = getMensaje(mcteClassName, "mcteMsg_USRNOEXISTE")
    '
    'Carga del REQUEST
    wvarStep = 20
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    wobjXMLRequest.loadXML pvarRequest
    '
    'Obtiene parametros
    wvarStep = 40
    With wobjXMLRequest
        wvarStep = 50
        wvarUsuario = .selectSingleNode(mcteParam_USUARIO).Text
        wvarStep = 60
        wvarPREGUNTA = .selectSingleNode(mcteParam_PREGUNTA).Text
        wvarStep = 70
        wvarRespuesta = .selectSingleNode(mcteParam_RESPUESTA).Text
    End With
    '
    '1) Validar existencia de usuario
    wvarStep = 150
    wvarEstadoUsr = fncValidarUsuario(wvarUsuario, wvarEstadoIdentificador, wvarEstadoPassword)
    If wvarEstadoUsr = "ERR" Then
        wvarError = mcteMsg_USRNOEXISTE
    End If
    '
    'Si el login es previo a la verificacion del RCC
    wvarStep = 190
    If wvarError = mcteMsg_OK Then
        '
        fncSetPreguntas wvarUsuario, wvarPREGUNTA, wvarRespuesta
        '
        wvarStep = 200
        pvarResponse = "<Response>" & _
                            "<Estado resultado=" & Chr(34) & "true" & Chr(34) & " estado=" & Chr(34) & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & _
                            "<CODRESULTADO>OK</CODRESULTADO>" & _
                            "<CODERROR>" & wvarError & "</CODERROR>" & _
                            "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                        "</Response>"
    Else
        wvarStep = 220
        pvarResponse = "<Response>" & _
                            "<Estado resultado=" & Chr(34) & "true" & Chr(34) & " estado=" & Chr(34) & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & _
                            "<CODRESULTADO>ERR</CODRESULTADO>" & _
                            "<CODERROR>" & wvarError & "</CODERROR>" & _
                            "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                        "</Response>"
    End If
    '
    'Finaliza y libera objetos
    wvarStep = 500
    Set wobjXMLRequest = Nothing
    '
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    'Inserta Error en EventViewer
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    Set wobjXMLRequest = Nothing
    '
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function
      
Private Function fncValidarUsuario(ByVal pvarUsuario As String, ByRef pvarEstadoIdentificador As String, ByRef pvarEstadoPassword As String) As String
    '
    Dim mvarRequest     As String
    Dim mvarResponse    As String
    Dim mvarRetVal      As String
    Dim mobjXMLResponse As MSXML2.DOMDocument
    Dim mobjClass       As HSBCInterfaces.IAction
    '
    mvarRequest = "<Request>" & _
                        "<DEFINICION>" & mcteParam_XMLSQLGENVALUSR & "</DEFINICION>" & _
                        "<MAIL>" & pvarUsuario & "</MAIL>" & _
                   "</Request>"
    '
    Set mobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
    Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    Set mobjClass = Nothing
    '
    Set mobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    mobjXMLResponse.async = False
    mobjXMLResponse.loadXML mvarResponse
    '
    'Analiza resultado del SP
    If Not mobjXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If mobjXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            '
            If mobjXMLResponse.selectSingleNode("//EXISTE").Text = "S" Or mobjXMLResponse.selectSingleNode("//EXISTE").Text = "N" Then
                mvarRetVal = mobjXMLResponse.selectSingleNode("//ESTADO").Text
                pvarEstadoIdentificador = mobjXMLResponse.selectSingleNode("//ESTADOIDENTIFICADOR").Text
                pvarEstadoPassword = mobjXMLResponse.selectSingleNode("//ESTADOPASSWORD").Text
            End If
        End If
    End If
    '
    If mvarRetVal = "" Then
        mvarRetVal = "ERR"
    End If
    '
    fncValidarUsuario = mvarRetVal
    Set mobjClass = Nothing
    Set mobjXMLResponse = Nothing
    '
End Function

Private Function fncSetPreguntas(pvarUsuario As String, pvarPregunta As String, pvarRespuesta As String) As Boolean
    '
    Dim mvarRequest     As String
    Dim mvarResponse    As String
    Dim mvarRetVal      As Boolean
    Dim mvarErrorRCC    As Boolean
    Dim mobjXMLResponse As MSXML2.DOMDocument
    Dim mobjClass       As HSBCInterfaces.IAction
    '
    mvarRetVal = False
    '
    mvarRequest = "<Request>" & _
                        "<DEFINICION>" & mcteParam_XMLSQLGENSetPregResp & "</DEFINICION>" & _
                        "<RESPUESTA>" & CapicomEncrypt(pvarRespuesta) & "</RESPUESTA>" & _
                        "<PREGUNTA>" & CapicomEncrypt(pvarPregunta) & "</PREGUNTA>" & _
                        "<MAIL>" & pvarUsuario & "</MAIL>" & _
                   "</Request>"
    '
    Set mobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
    Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    Set mobjClass = Nothing
    '
    Set mobjClass = Nothing
    Set mobjXMLResponse = Nothing
    '
End Function

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub


