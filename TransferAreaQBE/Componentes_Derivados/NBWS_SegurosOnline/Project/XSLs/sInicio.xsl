<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
	Fecha de Modificaci�n: 07/02/2012
	PPCR: 2011-00637
	Desarrollador: Leonardo Ruiz
	Descripci�n: Se agrega SITUCPOL a la funcion fncRedirigir()

	COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
	LIMITED 2009. ALL RIGHTS RESERVED
	
	This software is only to be used for the purpose for which it has been provided.
	No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
	system or translated in any human or computer language in any way or for any other
	purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
	Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
	offence, which can result in heavy fines and payment of substantial damages.
	
	Nombre del Fuente: sInicio.xsl
	
	Fecha de Creaci�n: 31/07/2009
	
	PPcR: 50055/6010662
	
	Desarrollador: Leonardo Ruiz
	
	Descripci�n: XSL para armar la solapa de Inicio.-
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:decimal-format name="europeo" decimal-separator="," grouping-separator="."/>
	<xsl:output method="html" encoding="ISO-8859-1"/>
	<!-- ******************************************************************** -->
	<!-- TEMPLATE que dibuja tabla con datos de p�lizas del cliente -->
	<!-- ******************************************************************** -->
	<xsl:template match="/">
		<table cellspacing="0" border="0" style="border-left:0px;width:800px;" cellpadding="0">
			<xsl:choose>
				<xsl:when test="count(//PRODUCTO[./HABILITADO_NBWS = 'S' and ./POLIZA_EXCLUIDA = 'N']) = 0">
					<tr>
						<th align="center">No se encontraron p�lizas que cumplan las condiciones para ser publicadas en este servicio.</th>
					</tr>
				</xsl:when>
				<xsl:otherwise>
					<tr>
						<th colspan="2">Productos Inclu&#237;dos
							<img src="../images/ico_faqs.gif" onmouseover="this.style.cursor='hand';">
								<xsl:attribute name="onclick">ayudas_impresos(document.getElementById("AYUDA_PROD_INCL").value);</xsl:attribute>
							</img>
						</th>
						<th>Riesgo  Asegurado</th>
						<th>Impresos </th>
						<th>Pagos </th>
						<th>Siniestros </th>
						<th>Mensajes </th>
						<th>Detalle </th>
						<th>Impresos<br/>por e-mail </th>
					</tr>
					<!-- SOLO POLIZAS HABILITADAS PARA NBWS SEGUN TABLA QUE PASO ECHANNELS OPORTUNAMENTE -->
					<xsl:for-each select="//PRODUCTO[./HABILITADO_NBWS = 'S' and ./POLIZA_EXCLUIDA = 'N']">
						<xsl:choose>
							<xsl:when test="TIPOPROD != 'G'  or count(//PRODUCTO) = 1">
								<tr>
									<td style="border-left: 1px solid #EDEDF1;">
										<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
									</td>
									<td style="width:150px;">
										<strong>
											<xsl:value-of select="RAMOPDES"/>
										</strong>
									</td>
									<td>
										<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
										<xsl:value-of select="TOMARIES"/>
										<!-- DA - 10/11/2009: LG pidi� por tel�fono que esto aparezca solo para productos de NYL Masivos -->
										<xsl:if test="CIAASCOD = 20 and TIPOPROD = 'M'">
											<br/>(Cert <xsl:value-of select="CERTISEC"/>)
										</xsl:if>
										<!-- DA - 30/11/2009: defect 14 punto 22 a pedido de CV-->
										<xsl:if test="CIAASCOD = 20 and TIPOPROD = 'C'">
											<br/>(P�liza <xsl:value-of select="RAMOPCOD"/>-<xsl:value-of select="POLIZANN"/>-<xsl:value-of select="POLIZSEC"/>)
										</xsl:if>
									</td>
									<xsl:choose>
										<!-- SI FUE VENDIDA POR UN CANAL DISTINTO A VENTA DIRECTA NO PERMITE NAVEGAR LA POLIZA -->
										<xsl:when test="HABILITADO_NAVEGACION = 'N'">
											<td colspan="6" id="txt_leyenda_tablas">
												<xsl:text disable-output-escaping="yes">Esta p�liza fu� vendida por un productor. Por cualquier consulta llamanos a nuestro centro de atenci�n al cliente.</xsl:text>
											</td>
										</xsl:when>
										<xsl:when test="HABILITADO_NAVEGACION = 'S'">
											<td>
												<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
												<xsl:if test="SWIMPRIM = 'S'">
													<img src="../images/boton_imprimir.gif" border="0" style="cursor:hand;">
														<xsl:attribute name="onclick">fncRedirigir('sImpresos.asp','<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="RAMOPDES"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','<xsl:value-of select="SUPLENUM"/>','<xsl:value-of select="HABILITADO_NBWS"/>','<xsl:value-of select="HABILITADO_NAVEGACION"/>','<xsl:value-of select="CIAASCOD"/>','<xsl:value-of select="TIPOPROD"/>',"<xsl:value-of select="TOMARIES"/>","<xsl:value-of select="SITUCPOL"/>","<xsl:value-of select="CONSTANCIA"/>")</xsl:attribute>
													</img>
												</xsl:if>
											</td>
											<td>
												<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
												<xsl:if test="COBRANZA = 'S'">
													<img src="../images/icon-pay.gif" border="0" style="cursor:hand;">
														<xsl:attribute name="onclick">fncRedirigir('sPagos.asp','<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="RAMOPDES"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','<xsl:value-of select="SUPLENUM"/>','<xsl:value-of select="HABILITADO_NBWS"/>','<xsl:value-of select="HABILITADO_NAVEGACION"/>','<xsl:value-of select="CIAASCOD"/>','<xsl:value-of select="TIPOPROD"/>',"<xsl:value-of select="TOMARIES"/>","<xsl:value-of select="SITUCPOL"/>")</xsl:attribute>
													</img>
												</xsl:if>
											</td>
											<td>
												<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
												<!-- DA - 16/11/2009: punto 76 del excel de Ajustes FG Y FD, no debe mostrarse siniestros para EGE1-->
												<xsl:if test="SINIESTRO = 'S' and RAMOPCOD != 'EGE1'">
													<img src="../images/icon-CBU.gif" border="0" style="cursor:hand;">
														<xsl:attribute name="onclick">fncRedirigir('sSiniestros.asp','<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="RAMOPDES"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','<xsl:value-of select="SUPLENUM"/>','<xsl:value-of select="HABILITADO_NBWS"/>','<xsl:value-of select="HABILITADO_NAVEGACION"/>','<xsl:value-of select="CIAASCOD"/>','<xsl:value-of select="TIPOPROD"/>',"<xsl:value-of select="TOMARIES"/>","<xsl:value-of select="SITUCPOL"/>")</xsl:attribute>
													</img>
												</xsl:if>
											</td>
											<td>
												<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
												<!-- DA - 20/11/2009: defect 14 item 27 punto 2 -->
												<xsl:if test="MENSASIN != '' or MENSASUS='FP'">
													<xsl:choose>
														<xsl:when test="MENSASUS='FP'">
															<img src="../images/ico_Mensaje.gif" border="0" style="cursor:hand;">
																<xsl:attribute name="onclick">javascript:alert_customizado('<xsl:value-of select="MENSASIN"/>' + '<br/>' + 'Te informamos que la cobertura de tu seguro se encuentra suspendida por falta de pago. Si resid�s en Capital Federal o Gran Buenos Aires, por favor comunicate al 4338-6950 o si resid�s en el Interior del pa�s, por favor comun�cate al 0-800-222-8575. Muchas gracias.')</xsl:attribute>
															</img>
														</xsl:when>
														<xsl:otherwise>
															<img src="../images/ico_Mensaje.gif" border="0" style="cursor:hand;">
																<xsl:attribute name="onclick">javascript:alert_customizado('<xsl:value-of select="MENSASIN"/>')</xsl:attribute>
															</img>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:if>
											</td>
											<td>
												<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
												<img src="../images/icon-det.gif" border="0" style="cursor:hand;">
													<xsl:attribute name="onclick">pantallaDetalle('<xsl:value-of select="CIAASCOD"/>','<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','<xsl:value-of select="SUPLENUM"/>')</xsl:attribute>
												</img>
											</td>
											<td>
												<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
												<xsl:if test="HABILITADO_EPOLIZA = 'S'">
													<img src="../images/IcoArroba.gif" border="0" style="cursor:hand;">
														<xsl:attribute name="onclick">fncRedirigir('sImpresosEmail.asp','<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="RAMOPDES"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','<xsl:value-of select="SUPLENUM"/>','<xsl:value-of select="HABILITADO_NBWS"/>','<xsl:value-of select="HABILITADO_NAVEGACION"/>','<xsl:value-of select="CIAASCOD"/>','<xsl:value-of select="TIPOPROD"/>',"<xsl:value-of select="TOMARIES"/>","<xsl:value-of select="SITUCPOL"/>")</xsl:attribute>
													</img>
												</xsl:if>
											</td>
										</xsl:when>
									</xsl:choose>
								</tr>
							</xsl:when>
							<xsl:when test="TIPOPROD='G' and count(//PRODUCTO) > 1">
								<!-- Aplica un template con corte de control para agrupar los certificados -->
								<!-- DA - 06/10/2009: se agrega la condici�n de la posici�n 1 ya que en caso de venir un GENERAL en la primer posici�n, no entraba en este bloque de c�digo, con lo cual no mostraba la p�liza -->
								<xsl:if test="(./RAMOPCOD!= preceding-sibling::*[1]/RAMOPCOD or ./POLIZANN != preceding-sibling::*[1]/POLIZANN or ./POLIZSEC!= preceding-sibling::*[1]/POLIZSEC) or position() = 1">
									<tr tipoProd="G">
										<td style="width:10px; border-left: 1px solid #EDEDF1;">
											<img id="imgMasMenos{position()}" src="../images/Mas.gif" border="0" onclick="javascript:fncMasMenos('{position()}');" style="cursor:hand;"/>
										</td>
										<td>
											<strong>
												<a>
													<xsl:attribute name="href">javascript:fncMasMenos('<xsl:value-of select="position()"/>');</xsl:attribute>
													<xsl:attribute name="style">cursor:hand;</xsl:attribute>
													<xsl:value-of select="RAMOPDES"/>
												</a>
											</strong>
										</td>
										<td>
											<xsl:value-of select="RAMOPCOD"/>-<xsl:value-of select="format-number(POLIZANN, '00', 'europeo')"/>-<xsl:value-of select="format-number(POLIZSEC, '000000', 'europeo')"/>
										</td>
										<td colspan="6">
											<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
										</td>
									</tr>
									<tr style="display:none;" id="trSubTable{position()}">
										<td style="border-left:0px;">
											<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
										</td>
										<td colspan="8">
											<!-- LLAMA AL TEMPLATE DE CORTE DE CONTROL -->
											<xsl:call-template name="GAgrup">
												<xsl:with-param name="ramo" select="./RAMOPCOD"/>
												<xsl:with-param name="polizann" select="./POLIZANN"/>
												<xsl:with-param name="polizsec" select="./POLIZSEC"/>
											</xsl:call-template>
										</td>
									</tr>
								</xsl:if>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</table>
		<!--<input type="button" value="cotizar endoso" onclick="nuevoEndoso('0','0','0','0','0','0','0','0');"/>-->
	</xsl:template>
	<!-- ******************************************************************** -->
	<!-- TEMPLATE que agrupa los certificados de una misma p�liza-->
	<!-- ******************************************************************** -->
	<xsl:template name="GAgrup">
		<xsl:param name="ramo"/>
		<xsl:param name="polizann"/>
		<xsl:param name="polizsec"/>
		<table id="subtable" style="width:750px;" cellpadding="0" border="0" cellspacing="1">
			<tr style="height:15px;">
				<th width="150px">Riesgo Asegurado</th>
				<th>Impresos</th>
				<th>Pagos</th>
				<th>Siniestros</th>
				<th>Mensajes</th>
				<th style="border-right:0px;">Detalle</th>
				<th>Impresos<br/>por e-mail </th>
			</tr>
			<xsl:choose>
				<!-- SI TIENE MAS DE 5 CERTIFICADOS NO PERMITE NAVEGAR LA POLIZA -->
				<xsl:when test="count(//PRODUCTO[./RAMOPCOD=$ramo and ./POLIZANN = $polizann and ./POLIZSEC= $polizsec and ./TIPOPROD='G']) > 5">
					<tr>
						<td colspan="7" id="txt_leyenda_tablas">Esta p�liza tiene mas de 5 riesgos, su informaci�n no se encuentra disponible en HSBC Seguros On Line. Por cualquier consulta llamanos a nuestro centro de atenci�n al cliente.</td>
					</tr>
				</xsl:when>
				<!-- SI FUE VENDIDA POR UN CANAL DISTINTO A VENTA DIRECTA NO PERMITE NAVEGAR LA POLIZA -->
				<xsl:when test="./HABILITADO_NAVEGACION = 'N'">
					<tr>
						<td colspan="7" id="txt_leyenda_tablas">
							<xsl:text disable-output-escaping="yes">Esta p�liza fu� vendida por un productor. Por cualquier consulta llamanos a nuestro centro de atenci�n al cliente.</xsl:text>
						</td>
					</tr>
				</xsl:when>
				<xsl:otherwise>
					<!-- DA - 10/11/2009: se agrega la validaci�n de CERTISEC mayor a cero para no mostrar la p�liza colectiva -->
					<xsl:for-each select="//PRODUCTO[./RAMOPCOD=$ramo and ./POLIZANN = $polizann and ./POLIZSEC= $polizsec and ./TIPOPROD='G' and ./CERTISEC > 0]">
						<tr>
							<td>
								<xsl:value-of select="./TOMARIES"/>
							</td>
							<td>
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
								<xsl:if test="./SWIMPRIM = 'S'">
									<img src="../images/boton_imprimir.gif" border="0" style="cursor:hand;">
										<xsl:attribute name="onclick">fncRedirigir('sImpresos.asp','<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="RAMOPDES"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','<xsl:value-of select="SUPLENUM"/>','<xsl:value-of select="HABILITADO_NBWS"/>','<xsl:value-of select="HABILITADO_NAVEGACION"/>','<xsl:value-of select="CIAASCOD"/>','<xsl:value-of select="TIPOPROD"/>',"<xsl:value-of select="TOMARIES"/>","<xsl:value-of select="SITUCPOL"/>")</xsl:attribute>
									</img>
								</xsl:if>
							</td>
							<td>
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
								<xsl:if test="./COBRANZA = 'S'">
									<img src="../images/icon-pay.gif" border="0" style="cursor:hand;">
										<xsl:attribute name="onclick">fncRedirigir('sPagos.asp','<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="RAMOPDES"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','<xsl:value-of select="SUPLENUM"/>','<xsl:value-of select="HABILITADO_NBWS"/>','<xsl:value-of select="HABILITADO_NAVEGACION"/>','<xsl:value-of select="CIAASCOD"/>','<xsl:value-of select="TIPOPROD"/>',"<xsl:value-of select="TOMARIES"/>","<xsl:value-of select="SITUCPOL"/>")</xsl:attribute>
									</img>
								</xsl:if>
							</td>
							<td>
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
								<xsl:if test="./SINIESTRO = 'S'">
									<img src="../images/icon-CBU.gif" border="0" style="cursor:hand;">
										<xsl:attribute name="onclick">fncRedirigir('sSiniestros.asp','<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="RAMOPDES"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','<xsl:value-of select="SUPLENUM"/>','<xsl:value-of select="HABILITADO_NBWS"/>','<xsl:value-of select="HABILITADO_NAVEGACION"/>','<xsl:value-of select="CIAASCOD"/>','<xsl:value-of select="TIPOPROD"/>',"<xsl:value-of select="TOMARIES"/>","<xsl:value-of select="SITUCPOL"/>")</xsl:attribute>
									</img>
								</xsl:if>
							</td>
							<td>
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
								<xsl:if test="MENSASIN != '' or MENSACOB !=''">
									<img src="../images/ico_Mensaje.gif" border="0" style="cursor:hand;">
										<xsl:attribute name="onclick">javascript:alert_customizado('<xsl:value-of select="MENSASIN"/>' + '<br/>' + '<xsl:value-of select="MENSACOB"/>')</xsl:attribute>
									</img>
								</xsl:if>
							</td>
							<td>
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
								<img src="../images/icon-det.gif" border="0" style="cursor:hand;">
									<xsl:attribute name="onclick">pantallaDetalle('<xsl:value-of select="CIAASCOD"/>','<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','<xsl:value-of select="SUPLENUM"/>')</xsl:attribute>
								</img>
							</td>
							<td>
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
								<xsl:if test="HABILITADO_EPOLIZA = 'S'">
									<img src="../images/IcoArroba.gif" border="0" style="cursor:hand;">
										<xsl:attribute name="onclick">fncRedirigir('sImpresosEmail.asp','<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="RAMOPDES"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','<xsl:value-of select="SUPLENUM"/>','<xsl:value-of select="HABILITADO_NBWS"/>','<xsl:value-of select="HABILITADO_NAVEGACION"/>','<xsl:value-of select="CIAASCOD"/>','<xsl:value-of select="TIPOPROD"/>',"<xsl:value-of select="TOMARIES"/>","<xsl:value-of select="SITUCPOL"/>")</xsl:attribute>
									</img>
								</xsl:if>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</table>
	</xsl:template>
</xsl:stylesheet>
