<?xml version="1.0" encoding="UTF-8"?>
<!--
      Fecha de Modificación: 17-05-2012
	PPCR: 2011-00389 anexo1
	Desarrollador: Adriana Armati
	Descripción: Se habilita la opcion  Carta de Siniestralidad para AUT1

	Fecha de Modificación: 26/01/2012
	PPCR: 2011-00637
	Desarrollador: Elizabeth Gregorio
	Descripción: Se habilita la opcion para Carta de Siniestralidad

	COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
	LIMITED 2009. ALL RIGHTS RESERVED
	
	This software is only to be used for the purpose for which it has been provided.
	No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
	system or translated in any human or computer language in any way or for any other
	purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
	Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
	offence, which can result in heavy fines and payment of substantial damages.
	
	Nombre del Fuente: sImpresos.xsl
	
	Fecha de Creación: 31/07/2009
	
	PPcR: 50055/6010662
	
	Desarrollador: Leonardo Ruiz
	
	Descripción: XSL para armar la solapa de Impresos
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:decimal-format name="europeo" decimal-separator="," grouping-separator="."/>
	<xsl:output method="html" encoding="iso-8859-1"/>
	<!-- ******************************************************************** -->
	<!-- TEMPLATE que dibuja tabla de solapa impresos -->
	<!-- ******************************************************************** -->
	<xsl:template match="/">
		<table cellspacing="0" cellpadding="0" style="width:800px;">
			<tr>
				<th class="impresos">Impresos</th>
				<th class="producto">Producto</th>
				<th class="poliza">Riesgo  Asegurado</th>
			</tr>
			<xsl:for-each select="//PRODUCTO[SWIMPRIM = 'S']">
				<xsl:choose>
					<!-- LBA -->
					<xsl:when test="CIAASCOD = '0001'">
						<xsl:choose>
							<xsl:when test="TIPOPROD='M' or count(//PRODUCTO) = 1">
								<tr>
									<td class="impresos">
										<img id="imgMasMenos{position()}" src="../images/Mas.gif" border="0" onclick="javascript:fncMasMenos('{position()}');" style="cursor:hand;"/>
									</td>
									<td class="producto">
										<strong>
											<a>
												<xsl:attribute name="href">javascript:fncMasMenos('<xsl:value-of select="position()"/>');</xsl:attribute>
												<xsl:attribute name="style">cursor:hand;</xsl:attribute>
												<xsl:value-of select="RAMOPDES"/>
											</a>
										</strong>
									</td>
									<td class="poliza">
										<!-- DA - 10/11/2009: LG pidió que se quite el dato Riesgo para los Generales de LBA -->
										<xsl:choose>
											<xsl:when test="TIPOPROD != 'G'">
												<xsl:value-of select="TOMARIES"/>
											</xsl:when>
											<xsl:when test="TIPOPROD = 'G'">
												<xsl:value-of select="RAMOPCOD"/>-<xsl:value-of select="format-number(POLIZANN, '00', 'europeo')"/>-<xsl:value-of select="format-number(POLIZSEC, '000000', 'europeo')"/>
											</xsl:when>
										</xsl:choose>
									</td>
								</tr>
								<tr style="display:none;" id="trSubTable{position()}">
									<td>
										<img src="../images/ico_faqs.gif" onmouseover="this.style.cursor='hand';">
											<xsl:attribute name="onclick">ayudas_impresos(document.getElementById("AYUDA_IMPRESOS").value);</xsl:attribute>
										</img>
									</td>
									<td colspan="2">
										<table id="subtable" style="width:720px">
											<tr>
												<th>Riesgo</th>
												<th>
													Copia de<br/>P<xsl:text disable-output-escaping="yes">&amp;oacute;</xsl:text>liza
												</th>
												<th>
													Mercosur<br/>
												</th>
												<th>
													Tarjeta de<br/>Circulaci<xsl:text disable-output-escaping="yes">&amp;oacute;</xsl:text>n
												</th>
												<th>
													Certificado de<br/>Cobertura

												</th>
												<th>
													Libre Deuda<br/>
												</th>
												<xsl:if test="(SITUCPOL = 'VIGENTE' or SITUCPOL = 'ANULADA' or SITUCPOL = 'VENCIDAS' or SITUCPOL = 'VENCIDA' ) and (RAMOPCOD = 'AUA1' or RAMOPCOD = 'AUI1' or RAMOPCOD = 'AUP1' or RAMOPCOD = 'ALT1' or RAMOPCOD = 'APR1' or RAMOPCOD = 'AUS1' or RAMOPCOD = 'APL1' or RAMOPCOD = 'AUB1' or RAMOPCOD = 'AUT1' )">
													<th>
														Carta Siniestralidad<br/>
													</th>
												</xsl:if>
											</tr>
											<tr>
												<td>
													<xsl:value-of select="TOMARIES"/>
												</td>
												<td>
													<xsl:choose>
														<xsl:when test="Response/IMPRESIONES[COPIAPOLIZA = 'S']">
															<!-- DA - 10/11/2009: se quita Copia de Póliza para todos los General a pedido de LG, ver "Ajuste FD - FG.xls" punto 83 -->
															<xsl:choose>
																<xsl:when test="TIPOPROD != 'G'">
																	<img src="../images/boton_impresora.gif" alt="Copia de póliza" style="Cursor:hand">
																		<xsl:attribute name="onclick">JavaScript:aImprimeDocumentos('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','CO')</xsl:attribute>
																	</img>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:when>
														<xsl:when test="Response/IMPRESIONES[COPIAPOLIZA = 'N']">
															<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
														</xsl:when>
													</xsl:choose>
												</td>
												<td>
													<xsl:choose>
														<xsl:when test="Response/IMPRESIONES[CERTIFICADOMERCOSUR= 'S']">
															<img src="../images/boton_impresora.gif" alt="Certificado Mercosur" style="Cursor:hand">
																<xsl:attribute name="onclick">JavaScript:aImprimeDocumentos('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','CM')</xsl:attribute>
															</img>
														</xsl:when>
														<xsl:when test="Response/IMPRESIONES[CERTIFICADOMERCOSUR= 'N']">
															<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
														</xsl:when>
													</xsl:choose>
												</td>
												<td>
													<xsl:choose>
														<xsl:when test="Response/IMPRESIONES[TARJETACIRCULACION= 'S']">
															<img src="../images/boton_impresora.gif" alt="Tarjeta de Circulación" style="Cursor:hand">
																<xsl:attribute name="onclick">JavaScript:aImprimeDocumentos('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','TC')</xsl:attribute>
															</img>
														</xsl:when>
														<xsl:when test="Response/IMPRESIONES[TARJETACIRCULACION= 'N']">
															<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
														</xsl:when>
													</xsl:choose>
												</td>
												<td>
													<xsl:choose>
														<xsl:when test="Response/IMPRESIONES[CERTIFICADOCOBERTURA= 'S']">
															<img src="../images/boton_impresora.gif" alt="Certificado de Cobertura" style="Cursor:hand">
																<xsl:attribute name="onclick">JavaScript:aImprimeDocumentos('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','CC')</xsl:attribute>
															</img>
														</xsl:when>
														<xsl:when test="Response/IMPRESIONES[CERTIFICADOCOBERTURA= 'N']">
															<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
														</xsl:when>
													</xsl:choose>
												</td>
												<td>
													<xsl:choose>
														<xsl:when test="Response/IMPRESIONES[CONSTANCIAPAGOS= 'S'] and CONSTANCIA = 'S'">
															<xsl:choose>
																<!-- DA - 01/12/2009: cuando se trata de un producto de cartera general, se pide la constancia de pago a nivel póliza colectiva. Defect 50 item 1 -->
																<xsl:when test="TIPOPROD = 'G'">
																	<img src="../images/boton_impresora.gif" alt="Libre Deuda" style="Cursor:hand">
																		<xsl:attribute name="onclick">JavaScript:aImprimeDocumentos('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','0','0','0','CP')</xsl:attribute>
																	</img>
																</xsl:when>
																<xsl:otherwise>
																	<img src="../images/boton_impresora.gif" alt="Libre Deuda" style="Cursor:hand">
																		<xsl:attribute name="onclick">JavaScript:aImprimeDocumentos('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','CP')</xsl:attribute>
																	</img>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:when>
														<xsl:otherwise>
															<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
														</xsl:otherwise>
													</xsl:choose>
												</td>
												<!-- 23/01/2012 LR - Carta de Siniestralidad -->
												<xsl:if test="(SITUCPOL = 'VIGENTE' or SITUCPOL = 'ANULADA' or SITUCPOL = 'VENCIDAS' or SITUCPOL = 'VENCIDA' ) and (RAMOPCOD = 'AUA1' or RAMOPCOD = 'AUI1' or RAMOPCOD = 'AUP1' or RAMOPCOD = 'ALT1' or RAMOPCOD = 'APR1' or RAMOPCOD = 'AUS1' or RAMOPCOD = 'APL1' or RAMOPCOD = 'AUB1' or RAMOPCOD = 'AUT1')">
													<td align="center">
														<span class="mdtxt" style="cursor:hand">
															<a onmouseout="return window.status='QBE Seguros Online';" onmouseover="return window.status='Comprobar';">
																<xsl:attribute name="onclick">Javascript:AbrirModalReimprCartSini('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>',this)</xsl:attribute>
																<img border="0" src="../images/boton_impresora.gif"  align="absmiddle" alt="Carta de Siniestralidad"/>
															</a>
														</span>
													</td>
												</xsl:if>
											</tr>
										</table>
									</td>
								</tr>
							</xsl:when>
							<xsl:when test="TIPOPROD='G' and count(//PRODUCTO) > 1">
								<!-- Aplica un template con corte de control para agrupar los certificados -->
								<!-- DA - 10/11/2009: se agrega la condición de la posición 1 ya que en caso de venir un GENERAL en la primer posición, no entraba en este bloque de código, con lo cual no mostraba la póliza -->
								<xsl:if test="(RAMOPCOD != preceding-sibling::*[1]/RAMOPCOD or POLIZANN != preceding-sibling::*[1]/POLIZANN or POLIZSEC != preceding-sibling::*[1]/POLIZSEC) or position() = 1">
									<tr>
										<td class="impresos">
											<img id="imgMasMenos{position()}" src="../images/Mas.gif" border="0" style="cursor:hand;">
												<xsl:attribute name="onclick">javascript:fncMasMenos('<xsl:value-of select="position()"/>');</xsl:attribute>
											</img>
										</td>
										<td class="producto">
											<strong>
												<a>
													<xsl:attribute name="href">javascript:fncMasMenos('<xsl:value-of select="position()"/>');</xsl:attribute>
													<xsl:attribute name="style">cursor:hand;</xsl:attribute>
													<xsl:value-of select="RAMOPDES"/>
												</a>
											</strong>
										</td>
										<td class="poliza">
											<!-- DA - 10/11/2009: LG pidió que se quite el ID de póliza para los Generales de LBA -->
											<!-- DA - 11/11/2009: LG pidió que se ponga el ID de póliza para los Generales de LBA -->
											<xsl:value-of select="RAMOPCOD"/>-<xsl:value-of select="format-number(POLIZANN, '00', 'europeo')"/>-<xsl:value-of select="format-number(POLIZSEC, '000000', 'europeo')"/>
										</td>
									</tr>
									<tr style="display:none;" id="trSubTable{position()}">
										<td>
											<img src="../images/ico_faqs.gif" onmouseover="this.style.cursor='hand';">
												<xsl:attribute name="onclick">ayudas_impresos(document.getElementById("AYUDA_IMPRESOS").value);</xsl:attribute>
											</img>
										</td>
										<td colspan="2">
											<!-- LLAMA AL TEMPLATE DE CORTE DE CONTROL -->
											<xsl:call-template name="GAgrup">
												<xsl:with-param name="ramo" select="RAMOPCOD"/>
												<xsl:with-param name="polizann" select="POLIZANN"/>
												<xsl:with-param name="polizsec" select="POLIZSEC"/>
											</xsl:call-template>
										</td>
									</tr>
								</xsl:if>
							</xsl:when>
						</xsl:choose>
					</xsl:when>
					<!-- NYL -->
					<xsl:when test="CIAASCOD = '0020'">
						<tr>
							<td class="impresos">
								<img id="imgMasMenos{position()}" src="../images/Mas.gif" border="0" onclick="javascript:fncMasMenos('{position()}');" style="cursor:hand;"/>
							</td>
							<td class="producto">
								<strong>
									<a>
										<xsl:attribute name="href">javascript:fncMasMenos('<xsl:value-of select="position()"/>');</xsl:attribute>
										<xsl:attribute name="style">cursor:hand;</xsl:attribute>
										<xsl:value-of select="RAMOPDES"/>
									</a>
								</strong>
							</td>
							<td class="poliza">
								<xsl:value-of select="TOMARIES"/>
								<!-- DA - 10/11/2009: LG pidió por teléfono que esto aparezca solo para productos de NYL Masivos -->
								<xsl:if test="CIAASCOD = 20 and TIPOPROD = 'M'">
									<br/>(Cert <xsl:value-of select="CERTISEC"/>)
								</xsl:if>
								<!-- DA - 30/11/2009: defect 14 punto 22 a pedido de CV-->
								<xsl:if test="CIAASCOD = 20 and TIPOPROD = 'C'">
									<br/>(Póliza <xsl:value-of select="RAMOPCOD"/>-<xsl:value-of select="POLIZANN"/>-<xsl:value-of select="POLIZSEC"/>)
								</xsl:if>
							</td>
						</tr>
						<tr style="display:none;" id="trSubTable{position()}">
							<td>
								<img src="../images/ico_faqs.gif" onmouseover="this.style.cursor='hand';">
									<xsl:attribute name="onclick">ayudas_impresos(document.getElementById("AYUDA_IMPRESOS").value);</xsl:attribute>
								</img>
							</td>
							<td colspan="2">
								<table id="subtable" style="width:720px">
									<xsl:choose>
										<xsl:when test="TIPOPROD != 'C'">
											<tr>
												<td style="background-color:#EEEEEE;font-family: Arial;	font-size: 11px;">
													<em>La póliza no tiene impresos disponibles.</em>
												</td>
											</tr>
										</xsl:when>
										<xsl:when test="TIPOPROD = 'C'">
											<tr>
												<th>Ahorro Plus <br/> News Letter</th>
												<th>Estado de póliza al<br/>
													<xsl:value-of select="substring(Response/IMPRESO1/PERIODO,7,2)"/>-<xsl:value-of select="substring(Response/IMPRESO1/PERIODO,5,2)"/>-<xsl:value-of select="substring(Response/IMPRESO1/PERIODO,1,4)"/>
												</th>
												<th>Estado de póliza al<br/>
													<xsl:value-of select="substring(Response/IMPRESO2/PERIODO,7,2)"/>-<xsl:value-of select="substring(Response/IMPRESO2/PERIODO,5,2)"/>-<xsl:value-of select="substring(Response/IMPRESO2/PERIODO,1,4)"/>
												</th>
												<th>Estado de póliza al<br/>
													<xsl:value-of select="substring(Response/IMPRESO3/PERIODO,7,2)"/>-<xsl:value-of select="substring(Response/IMPRESO3/PERIODO,5,2)"/>-<xsl:value-of select="substring(Response/IMPRESO3/PERIODO,1,4)"/>
												</th>
											</tr>
											<tr>
												<td>
													<a target="_blank">
														<xsl:attribute name="href">../Informacion/PDF/<xsl:value-of select="RAMOPCOD"/>InfoNyl.PDF</xsl:attribute>
														<img src="../images/boton_impresora.gif" style="cursor:hand;"/>
													</a>
												</td>
												<td>
													<xsl:if test="substring(Response/IMPRESO1/PERIODO,5,2) >= 6 and substring(Response/IMPRESO1/PERIODO,1,4) >= 2009">
														<img src="../images/boton_impresora.gif" style="cursor:hand;">
															<xsl:attribute name="onclick">javascript: aImprimeResumen('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="format-number(POLIZANN, '00', 'europeo')"/>','<xsl:value-of select="format-number(POLIZSEC, '000000', 'europeo')"/>','<xsl:value-of select="format-number(CERTIPOL, '0000', 'europeo')"/>','<xsl:value-of select="format-number(CERTIANN, '0000', 'europeo')"/>','<xsl:value-of select="format-number(CERTISEC, '000000', 'europeo')"/>','<xsl:value-of select="format-number(SUPLENUM, '0000', 'europeo')"/>','<xsl:value-of select="Response/IMPRESO1/PERIODO"/>');</xsl:attribute>
														</img>
													</xsl:if>
												</td>
												<td>
													<xsl:if test="substring(Response/IMPRESO2/PERIODO,5,2) >= 6 and substring(Response/IMPRESO2/PERIODO,1,4) >= 2009">
														<img src="../images/boton_impresora.gif" style="cursor:hand;">
															<xsl:attribute name="onclick">javascript: aImprimeResumen('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="format-number(POLIZANN, '00', 'europeo')"/>','<xsl:value-of select="format-number(POLIZSEC, '000000', 'europeo')"/>','<xsl:value-of select="format-number(CERTIPOL, '0000', 'europeo')"/>','<xsl:value-of select="format-number(CERTIANN, '0000', 'europeo')"/>','<xsl:value-of select="format-number(CERTISEC, '000000', 'europeo')"/>','<xsl:value-of select="format-number(SUPLENUM, '0000', 'europeo')"/>','<xsl:value-of select="Response/IMPRESO2/PERIODO"/>');</xsl:attribute>
														</img>
													</xsl:if>
												</td>
												<td>
													<xsl:if test="substring(Response/IMPRESO3/PERIODO,5,2) >= 6 and substring(Response/IMPRESO3/PERIODO,1,4) >= 2009">
														<img src="../images/boton_impresora.gif" style="cursor:hand;">
															<xsl:attribute name="onclick">javascript: aImprimeResumen('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="format-number(POLIZANN, '00', 'europeo')"/>','<xsl:value-of select="format-number(POLIZSEC, '000000', 'europeo')"/>','<xsl:value-of select="format-number(CERTIPOL, '0000', 'europeo')"/>','<xsl:value-of select="format-number(CERTIANN, '0000', 'europeo')"/>','<xsl:value-of select="format-number(CERTISEC, '000000', 'europeo')"/>','<xsl:value-of select="format-number(SUPLENUM, '0000', 'europeo')"/>','<xsl:value-of select="Response/IMPRESO3/PERIODO"/>');</xsl:attribute>
														</img>
													</xsl:if>
												</td>
											</tr>
										</xsl:when>
									</xsl:choose>
								</table>
							</td>
						</tr>
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
		</table>
	</xsl:template>
	<!-- ******************************************************************** -->
	<!-- TEMPLATE que agrupa los certificados de una misma póliza-->
	<!-- ******************************************************************** -->
	<xsl:template name="GAgrup">
		<xsl:param name="ramo"/>
		<xsl:param name="polizann"/>
		<xsl:param name="polizsec"/>
		<table id="subtable" style="width:720px;" cellpadding="0" border="0" cellspacing="1">
			<tr style="height:15px;">
				<th>Riesgo</th>
				<th>Copia de<br/> P<xsl:text disable-output-escaping="yes">&amp;oacute;</xsl:text>liza</th>
				<th>Mercosur<br/>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</th>
				<th>Tarjeta de<br/> Circulaci<xsl:text disable-output-escaping="yes">&amp;oacute;</xsl:text>n</th>
				<th>Certificado <br/>de Cobertura</th>
				<th>Libre Deuda</th>
			</tr>
			<!-- DA - 10/11/2009: se agrega la validación de CERTISEC mayor a cero para no mostrar la póliza colectiva -->
			<xsl:for-each select="//PRODUCTO[RAMOPCOD=$ramo and POLIZANN = $polizann and POLIZSEC= $polizsec and TIPOPROD='G' and CERTISEC > 0]">
				<tr>
					<td>
						<xsl:value-of select="TOMARIES"/>
					</td>
					<td>
						<!-- DA - 01/09/2009: se quita Copia de Póliza para todos los General a pedido de LG, ver "Ajuste FD - FG.xls" punto 83
						<xsl:choose>
							<xsl:when test="Response/IMPRESIONES[COPIAPOLIZA = 'S']">
								<img src="../images/boton_impresora.gif" alt="Copia de Póliza" style="Cursor:hand">
									<xsl:attribute name="onclick">JavaScript:aImprimeDocumentos('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','CO')</xsl:attribute>
								</img>
							</xsl:when>
							<xsl:when test="Response/IMPRESIONES[COPIAPOLIZA = 'N']">
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							</xsl:when>
						</xsl:choose>
						-->
						<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="Response/IMPRESIONES[CERTIFICADOMERCOSUR= 'S']">
								<img src="../images/boton_impresora.gif" alt="Certificado de Mercosur" style="Cursor:hand">
									<xsl:attribute name="onclick">JavaScript:aImprimeDocumentos('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','CM')</xsl:attribute>
								</img>
							</xsl:when>
							<xsl:when test="Response/IMPRESIONES[CERTIFICADOMERCOSUR= 'N']">
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							</xsl:when>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="Response/IMPRESIONES[TARJETACIRCULACION= 'S']">
								<img src="../images/boton_impresora.gif" alt="Tarjeta de Circulación" style="Cursor:hand">
									<xsl:attribute name="onclick">JavaScript:aImprimeDocumentos('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','TC')</xsl:attribute>
								</img>
							</xsl:when>
							<xsl:when test="Response/IMPRESIONES[TARJETACIRCULACION= 'N']">
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							</xsl:when>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="Response/IMPRESIONES[CERTIFICADOCOBERTURA= 'S']">
								<img src="../images/boton_impresora.gif" alt="Certificado de Cobertura" style="Cursor:hand">
									<xsl:attribute name="onclick">JavaScript:aImprimeDocumentos('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','CC')</xsl:attribute>
								</img>
							</xsl:when>
							<xsl:when test="Response/IMPRESIONES[CERTIFICADOCOBERTURA= 'N']">
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							</xsl:when>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="Response/IMPRESIONES[CONSTANCIAPAGOS= 'S'] and CONSTANCIA='S'">
								<xsl:choose>
									<!-- DA - 01/12/2009: cuando se trata de un producto de cartera general, se pide la constancia de pago a nivel póliza colectiva. Defect 50 item 1 -->
									<xsl:when test="TIPOPROD = 'G'">
										<img src="../images/boton_impresora.gif" alt="Libre Deuda" style="Cursor:hand">
											<xsl:attribute name="onclick">JavaScript:aImprimeDocumentos('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','0','0','0','CP')</xsl:attribute>
										</img>
									</xsl:when>
									<xsl:otherwise>
										<img src="../images/boton_impresora.gif" alt="Libre Deuda" style="Cursor:hand">
											<xsl:attribute name="onclick">JavaScript:aImprimeDocumentos('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','CP')</xsl:attribute>
										</img>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
</xsl:stylesheet>
