SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO







/*
---------------------------------------------------------------------------------------
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2009. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Programador: Matias Coaker
Nombre del Stored:  P_NBWS_ValidaUsuario
Fecha de Creación: 10/06/2009
Numero de PPCR: 50055/6010662
Descripción: verificación de usuarios suscriptos a NBWS.
---------------------------------------------------------------------------------------
*/

ALTER    PROCEDURE P_NBWS_ValidaUsuario
@MAIL char(50)
AS

SET NOCOUNT ON

DECLARE @EXISTEUSR char(1)
DECLARE @ESTADO char(1)
DECLARE @ESTADOPASSWORD int
DECLARE @ESTADOIDENTIFICADOR int
DECLARE @TIEMPO_BLOQUEO_RCC as varchar(100)
DECLARE @TIEMPO_INHABIL_RCC as datetime


--DA - 09/10/2009: se agrega restriccion de estado = A
--IF exists (SELECT 1 FROM NBWS_Usuarios WHERE Mail = @MAIL)
IF exists (SELECT 1 FROM NBWS_Usuarios WHERE Mail = @MAIL and ESTADO='A')
BEGIN
	SET @EXISTEUSR = 'S'
	SELECT 	@ESTADO = Estado,
		@ESTADOPASSWORD = EstadoPassword,
		@ESTADOIDENTIFICADOR = EstadoIdentificador
		FROM NBWS_Usuarios
		WHERE Mail = @MAIL AND ESTADO='A'
END
ELSE
BEGIN
	SET @EXISTEUSR = 'N'
END

IF(@@ERROR <> 0)
BEGIN
	--ERROR
	RETURN -1
END

--DA 05/10/2009: se agrega este codigo para desbloquear al RCC si ya paso el tiempo correspondiente.
IF @EXISTEUSR = 'S' and @ESTADOIDENTIFICADOR = 2
BEGIN
	--Recupera tiempo de bloqueo y fecha de inhabilitacion
	SELECT @TIEMPO_BLOQUEO_RCC = valor FROM Parametros WHERE variable='NBWSBloqueoRCC'
	SELECT @TIEMPO_INHABIL_RCC = TiempoInhabilitado FROM NBWS_Usuarios WHERE Mail = @MAIL AND ESTADO='A'
	IF DATEDIFF(minute, @TIEMPO_INHABIL_RCC, getdate()) > @TIEMPO_BLOQUEO_RCC
	BEGIN
		SET @ESTADOIDENTIFICADOR = 0
	END
END


SELECT  @EXISTEUSR AS EXISTE,
	isnull(@ESTADO,'') AS ESTADO, 
	isnull(@ESTADOPASSWORD,-1) AS ESTADOPASSWORD, 
	isnull(@ESTADOIDENTIFICADOR,-1) AS ESTADOIDENTIFICADOR

RETURN 0







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

