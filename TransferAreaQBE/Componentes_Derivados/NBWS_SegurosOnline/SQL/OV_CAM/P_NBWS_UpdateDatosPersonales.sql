SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO




/*
---------------------------------------------------------------------------------------
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2009. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Programador: Matias Coaker
Nombre del Stored:  P_NBWS_UpdateDatosPersonales
Fecha de Creaci�n: 10/06/2009
Numero de PPCR: 50055/6010662
Descripci�n: Actualiza los datos del usuario en base al mail pasado por par�metro.
---------------------------------------------------------------------------------------
*/

ALTER   PROCEDURE P_NBWS_UpdateDatosPersonales
@MAIL char(50),
@MAILNUEVO char(50),
@PREGUNTA varchar(600),
@RESPUESTA varchar(600)
AS

SET NOCOUNT ON
SET xact_abort ON
BEGIN TRAN

DECLARE @IDPregunta int
SELECT @IDPregunta = prg.IDPregunta 
	FROM NBWS_UsuarioPregunta prg, NBWS_Usuarios usr 
	WHERE prg.IDUsuario = usr.ID 
	AND usr.Mail = @Mail AND usr.ESTADO='A'

IF @PREGUNTA <> '' AND @RESPUESTA <> '' 
BEGIN
	UPDATE 	NBWS_Preguntas
		SET Pregunta = @PREGUNTA,
		Respuesta = @RESPUESTA
		WHERE ID = @IDPregunta
END

IF @MAILNUEVO <> ''
BEGIN
	UPDATE 	NBWS_Usuarios
		SET Mail = @MAILNUEVO, FechaUltAct = getdate()
		WHERE Mail = @MAIL AND ESTADO='A' 
END

IF(@@ERROR <> 0)
BEGIN
	--ERROR
	ROLLBACK TRANSACTION
	RETURN -1
END

COMMIT TRAN

RETURN 0




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

