SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO




/*
---------------------------------------------------------------------------------------
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2009. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Programador: Matias Coaker
Nombre del Stored:  P_NBWS_Registracion
Fecha de Creación: 10/06/2009
Numero de PPCR: 50055/6010662
Descripción: Registración de usuarios de NBWS.
---------------------------------------------------------------------------------------
*/

ALTER   PROCEDURE P_NBWS_Registracion
@DOCUMTIP int,
@DOCUMDAT numeric(11),
@CAI varchar(300),
@MAIL char(50),
@VIAINSCRIPCION char(50),
@RESPONSABLEALTA char(50) = '',
@CONFORMIDAD char(1)

AS

SET NOCOUNT ON

SET xact_abort ON
BEGIN TRAN

DECLARE @EXIST INT
DECLARE @ESTADO CHAR(1)
DECLARE @RESULTADO INT

SELECT @EXIST=COUNT(ID) 
	FROM NBWS_Usuarios 
	WHERE  (Mail = @MAIL AND ESTADO='A');

IF @EXIST = 0
BEGIN
	--Si no existe lo inserta
	INSERT INTO NBWS_Usuarios
		(DOCUMTIP, DOCUMDAT, Mail, Password, EstadoPassword, PasswordIntentosFallidos, ViaInscripcion, ResponsableAlta, EstadoIdentificador, RCCIntentosFallidos, FechaSuscripcion, FechaUltAct, Estado, Conformidad)
	VALUES(@DOCUMTIP, @DOCUMDAT, @MAIL, @CAI, 3, 0, @VIAINSCRIPCION, @RESPONSABLEALTA, 0, 0, getdate(), getdate(), 'A', @CONFORMIDAD)
	SET @RESULTADO=0
END
ELSE
BEGIN
	SET @RESULTADO=1
END

IF(@@ERROR <> 0)
BEGIN
	--ERROR
	ROLLBACK TRANSACTION
	RETURN -1
END

COMMIT TRANSACTION

SELECT @RESULTADO AS RESULTADO

RETURN 0




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

