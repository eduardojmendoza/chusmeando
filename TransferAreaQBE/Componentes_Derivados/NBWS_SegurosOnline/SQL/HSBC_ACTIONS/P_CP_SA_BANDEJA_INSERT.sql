SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

/*  
--------------------------------------------------------------------------------  
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION  
LIMITED 2009. ALL RIGHTS RESERVED  
  
This software is only to be used for the purpose for which it has been provided.  
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval  
system or translated in any human or computer language in any way or for any other  
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai  
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal  
offence, which can result in heavy fines and payment of substantial damages.  
  
Nombre del Programador: Matias Coaker  
  
Nombre del Store: P_CP_SA_BANDEJA_INSERT 
  
Fecha de Creaci�n: 03/04/2009
  
Descripci�n: Inserta un registro para la maquina de resolucion automatica de estados.
  
--------------------------------------------------------------------------------  
*/  

ALTER  PROCEDURE P_CP_SA_BANDEJA_INSERT
(  
 @APLIC   VARCHAR(20),     	-- Identificador de aplicaci�n  
 @SOLICITUD  INT,       	-- Numero de Solicitud  
 @PASO   VARCHAR(20),    	-- Paso que identifica la Tarea a ejecutar  
 @FECHAEXEC DATETIME,		-- Fecha de ejecuci�n del job
 @DATAIN   VARCHAR(1000),    	-- Data para el xml de entrada al IAction  
 @COD_USU  VARCHAR(20)     	-- Usuario que inserto la tarea  
)  
  
AS  
  
SET NOCOUNT ON  
 
INSERT  INTO SERVAUT_BANDEJA 
 	(APLIC, SOLICITUD, PASO, INTENTO, DATAIN, DATAEX, TERMINADO, EST_FINAL, FECHA_INI, FECHA_PASO, COD_USU, LOCK)
 	VALUES 
	(@APLIC, @SOLICITUD, @PASO, 0, @DATAIN, '', 0, '', @FECHAEXEC, @FECHAEXEC, @COD_USU,  0)  

SET NOCOUNT OFF  



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

