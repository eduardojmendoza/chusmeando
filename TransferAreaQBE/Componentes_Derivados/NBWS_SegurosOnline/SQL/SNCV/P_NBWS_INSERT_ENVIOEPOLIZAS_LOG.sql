SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




/*  
---------------------------------------------------------------------------------------
Fecha de Modificaci�n: 10/03/2010
PPCR: 2010/00018
Desarrollador: Matias Casanova
Descripci�n: Se saca el borrado de las entradas de log con m�s de 30 d�as.
---------------------------------------------------------------------------------------
Fecha de Modificaci�n: 09/03/2010
PPCR: 2010/00018
Desarrollador: Matias Casanova
Descripci�n: Se agregan los campos tipo y nro de documento en el log.
---------------------------------------------------------------------------------------  
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION  
LIMITED 2009. ALL RIGHTS RESERVED  
  
This software is only to be used for the purpose for which it has been provided.  
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval  
system or translated in any human or computer language in any way or for any other  
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai  
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal  
offence, which can result in heavy fines and payment of substantial damages.  
  
Nombre del Programador: Gabriel D'Agnone
Nombre del Stored:  P_NBWS_INSERT_ENVIOEPOLIZAS_LOG 
Fecha de Creaci�n: 20/08/2009
Numero de PPCR: 50055/6010662  
Descripci�n: alta de log del env�o de eP�liza de LBA
---------------------------------------------------------------------------------------  
*/ 

ALTER  PROCEDURE P_NBWS_INSERT_ENVIOEPOLIZAS_LOG

	@CODOP       VARCHAR(6),
	@DESCRIPCION VARCHAR(100),
	@CLIENTE     VARCHAR(70),
	@EMAIL       VARCHAR(50),
	@ESTADO      VARCHAR(3),
	@DOCUMTIP    NUMERIC(2) = 0,
	@DOCUMDAT    NUMERIC(11) = 0

AS

SET NOCOUNT ON

DECLARE @FECHA DATETIME
SET @FECHA = GETDATE()
SET xact_abort ON

if (@DOCUMTIP=0) set @DOCUMTIP = null
if (@DOCUMDAT=0) set @DOCUMDAT = null

BEGIN TRAN

	INSERT INTO NBWS_ENVIOEPOLIZAS_LOG
	(FECHA, CODOP, DESCRIPCION, CLIENTE, EMAIL, ESTADO, DOCUMTIP, DOCUMDAT)
	VALUES
	(@FECHA, @CODOP, @DESCRIPCION, @CLIENTE, @EMAIL, @ESTADO, @DOCUMTIP, @DOCUMDAT)

	IF(@@ERROR <> 0)
	BEGIN
		--ERROR
		ROLLBACK TRANSACTION
		RETURN -1
	END
COMMIT TRAN

SET xact_abort OFF

RETURN 0




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

