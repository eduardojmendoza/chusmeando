<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2007. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: siniestroDenuncia_getComboRamo.xsl

Fecha de Creación: 11/06/2007

Desarrollador: Matias Coaker

Descripción: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos 
			siniestroDenuncia_getComboXXXX.xml
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:output method="html" encoding ="iso-8859-1"/>
	<msxsl:script language="JScript" implements-prefix="user"><![CDATA[
		function insertaEspacio()
		{
			return ' - ';
		}
		]]>
	</msxsl:script>
	<xsl:template match="/">
		<options>
			<xsl:for-each select="//ITEM[RAMOPCOD != '']">
				<xsl:sort select=".//RAMOPDAB"/>
				<option>
					<xsl:attribute name="value"><xsl:value-of select="RAMOPCOD"/></xsl:attribute>
					<xsl:attribute name="tiporamo"><xsl:value-of select="COLECTIP"/></xsl:attribute>
					<xsl:value-of select="RAMOPCOD"/><xsl:value-of select="user:insertaEspacio()"/><xsl:value-of select="RAMOPDAB"/>
				</option>
			</xsl:for-each>
		</options>
	</xsl:template>
</xsl:stylesheet>
