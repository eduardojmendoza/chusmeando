VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_siniInsertEXP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context As ObjectContext
Private mobjEventLog    As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction


'Datos de la accion
Const mcteClassName             As String = "lbawA_SiniDenuncia.lbaw_siniInsertEXP"
Const mcteStoreProc             As String = "P_SINI_INSERT_EXPORTAR"
'
Const mcteParam_RAMOPCOD        As String = "//RAMOPCOD"
Const mcteParam_SINIEANNAIS     As String = "//SINIEANNAIS"
Const mcteParam_SINIENUMAIS     As String = "//SINIENUMAIS"
Const mcteParam_SINIENUMSQL     As String = "//SINIENUMSQL"
Const mcteParam_GRABFECHA       As String = "//GRABFECHA"
Const mcteParam_USUARCOD        As String = "//USUARCOD"
Const mcteParam_CLIENSEC        As String = "//CLIENSEC"
Const mcteParam_XMLDATOS        As String = "//XMLDATOS"

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    
    'Parámetros de entrada
    Dim mvarRAMOPCOD        As String
    Dim mvarSINIEANNAIS     As String
    Dim mvarSINIENUMAIS     As String
    Dim mvarSINIENUMSQL     As String
    Dim mvarGRABFECHA       As String
    Dim mvarUSUARCOD        As String
    Dim mvarCLIENSEC        As String
    Dim mvarXMLDATOS        As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    
    wvarStep = 20
    With wobjXMLRequest
        wvarStep = 21
        mvarRAMOPCOD = .selectSingleNode(mcteParam_RAMOPCOD).Text
        wvarStep = 22
        mvarSINIEANNAIS = .selectSingleNode(mcteParam_SINIEANNAIS).Text
        wvarStep = 23
        mvarSINIENUMAIS = .selectSingleNode(mcteParam_SINIENUMAIS).Text
        wvarStep = 24
        mvarSINIENUMSQL = .selectSingleNode(mcteParam_SINIENUMSQL).Text
        wvarStep = 25
        mvarGRABFECHA = .selectSingleNode(mcteParam_GRABFECHA).Text
        wvarStep = 26
        If Len(Trim(.selectSingleNode(mcteParam_CLIENSEC).Text)) > 0 Then
            mvarCLIENSEC = .selectSingleNode(mcteParam_CLIENSEC).Text
        Else
            mvarCLIENSEC = 0
        End If
        wvarStep = 27
        mvarXMLDATOS = .selectSingleNode(mcteParam_XMLDATOS).xml
        wvarStep = 28
        mvarUSUARCOD = .selectSingleNode(mcteParam_USUARCOD).Text
    End With
    
    wvarStep = 40
    'TODO: OJOOOOOOOOOOOO En mi PC no funciona con Trx. Para debuggear cambiar. Luego volver a su forma con Trx
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 50
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , "0")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 61
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RAMOPCOD", adChar, adParamInput, 4, mvarRAMOPCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 62
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SINIEANNAIS", adInteger, adParamInput, , mvarSINIEANNAIS)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 63
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SINIENUMAIS", adInteger, adParamInput, , mvarSINIENUMAIS)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 64
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SINIENUMSQL", adInteger, adParamInput, , mvarSINIENUMSQL)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 64
    Set wobjDBParm = wobjDBCmd.CreateParameter("@GRABFECHA", adVarChar, adParamInput, 8, Left(mvarGRABFECHA, 8))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 65
    Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARCOD", adVarChar, adParamInput, 10, Left(mvarUSUARCOD, 10))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 66
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEC", adInteger, adParamInput, , mvarCLIENSEC)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@XMLDATOS", adLongVarChar, adParamInput, Len(mvarXMLDATOS), mvarXMLDATOS)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 80
    wobjDBCmd.Execute adExecuteNoRecords
    
    wvarStep = 250
    'Controlamos la respuesta del SQL
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 Then
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & "Se ha registrado exitosamente la información del siniestro para su posterior exportacion." & Chr(34) & " /></Response>"
    Else
        wvarMensaje = "Error al insertar en la tabla SQL SINI_EXPORTAR"
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje='" & wvarMensaje & "' /></Response>"
    End If

    wvarStep = 260
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 270
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub










