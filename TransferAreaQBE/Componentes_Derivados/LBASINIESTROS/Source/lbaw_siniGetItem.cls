VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_siniGetItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbawA_SiniDenuncia.lbaw_siniGetItem"
Const mcteStoreProc                 As String = "P_SINI_SELECT_SINIESTRO_ITEM"

Const mcteParam_SINIENUMSQL                As String = "//SINIENUMSQL"

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    
    Dim wvarResXMLDatosAIS  As String
    Dim wvarResXMLDatosMID  As String
    Dim wvarResult          As String
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    
    'Parámetros de entrada
    Dim mvarSINIEANNSQL       As String
    Dim mvarSINIENUMSQL       As String

    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    
    wvarStep = 20
    With wobjXMLRequest
        mvarSINIENUMSQL = .selectSingleNode(mcteParam_SINIENUMSQL).Text
    End With
    
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SINIENUMSQL", adInteger, adParamInput, , mvarSINIENUMSQL)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 40
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc
    
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    
    If Not wrstDBResult.EOF Then
        '
        wvarStep = 150
        
        wvarResXMLDatosAIS = wrstDBResult("XMLDatosAIS").Value
        wvarResXMLDatosMID = wrstDBResult("XMLDatosMID").Value
        '
        wvarResult = wvarResXMLDatosAIS & wvarResXMLDatosMID
        '
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
        '
        wvarStep = 180
        
    Else
        wvarMensaje = "No se encontró el item solicitado en la base de SQL"
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje='" & wvarMensaje & "' /></Response>"
    End If
    

    wvarStep = 140
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 150
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Set wrstDBResult = Nothing
    Set wobjDBParm = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub


