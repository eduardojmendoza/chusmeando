VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_siniInsertSQL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context As ObjectContext
Private mobjEventLog    As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction


'Datos de la accion
Const mcteClassName             As String = "lbawA_SiniDenuncia.lbaw_siniInsertSQL"
Const mcteStoreProc             As String = "P_SINI_INSERT_SINIESTRO"
'
Const mcteParam_RAMOPCOD                   As String = "//RAMOPCOD"
Const mcteParam_SINIEANNAIS                As String = "//SINIEANNAIS"
Const mcteParam_SINIENUMAIS                As String = "//SINIENUMAIS"

Const mcteParam_GRABSVIA                   As String = "//GRABSVIA"
Const mcteParam_CLIENSEC                   As String = "//CLIENSEC"
Const mcteParam_GRABAUSU                   As String = "//GRABAUSU"

Const mcteParam_NROFORM                    As String = "//NROFORM"
Const mcteParam_ESTADODAT                  As String = "//ESTADODAT"
Const mcteParam_MSINIENUM                  As String = "//MSINIENUM"
Const mcteParam_RECARGA                    As String = "//RECARGA"
Const mcteParam_MARENVAIS                  As String = "//MARENVAIS"
Const mcteParam_ESTADOAIS                  As String = "//ESTADOAIS"
Const mcteParam_MARENVMID                  As String = "//MARENVMID"
Const mcteParam_ESTADOMID                  As String = "//ESTADOMID"
Const mcteParam_XMLDatosAIS                As String = "//XMLDatosAIS"
Const mcteParam_XMLDatosMID                As String = "//XMLDatosMID"




Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    
    'Par�metros de entrada
    Dim mvarRAMOPCOD        As String
    Dim mvarSINIEANNAIS     As String
    Dim mvarSINIENUMAIS     As String
    
    Dim mvarGRABSVIA        As String
    Dim mvarCLIENSEC        As String
    Dim mvarGRABAUSU        As String

    Dim mvarNROFORM         As String
    Dim mvarESTADODAT       As String
    Dim mvarMSINIENUM       As String
    Dim mvarRECARGA         As String
    Dim mvarMARENVAIS       As String
    Dim mvarESTADOAIS       As String
    Dim mvarMARENVMID       As String
    Dim mvarESTADOMID       As String
    Dim mvarXMLDatosAIS     As String
    Dim mvarXMLDatosMID     As String
    
    'Par�metros de salida
    Dim mvarID_SINIESTRO    As Integer
    Dim mvarFECHA_ALTA      As Date
    
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    
    wvarStep = 20
    With wobjXMLRequest
        wvarStep = 21
        mvarRAMOPCOD = .selectSingleNode(mcteParam_RAMOPCOD).Text
        wvarStep = 22
        mvarNROFORM = .selectSingleNode(mcteParam_NROFORM).Text
        wvarStep = 23
        mvarSINIEANNAIS = .selectSingleNode(mcteParam_SINIEANNAIS).Text
        wvarStep = 24
        mvarSINIENUMAIS = .selectSingleNode(mcteParam_SINIENUMAIS).Text
        wvarStep = 25
        mvarGRABSVIA = .selectSingleNode(mcteParam_GRABSVIA).Text
        wvarStep = 26
        mvarCLIENSEC = .selectSingleNode(mcteParam_CLIENSEC).Text
        wvarStep = 27
        mvarGRABAUSU = .selectSingleNode(mcteParam_GRABAUSU).Text
        wvarStep = 28
        mvarESTADODAT = .selectSingleNode(mcteParam_ESTADODAT).Text
        wvarStep = 29
        mvarMSINIENUM = .selectSingleNode(mcteParam_MSINIENUM).Text
        wvarStep = 30
        mvarRECARGA = .selectSingleNode(mcteParam_RECARGA).Text
        wvarStep = 31
        mvarMARENVAIS = .selectSingleNode(mcteParam_MARENVAIS).Text
        wvarStep = 32
        mvarESTADOAIS = .selectSingleNode(mcteParam_ESTADOAIS).Text
        wvarStep = 33
        mvarMARENVMID = .selectSingleNode(mcteParam_MARENVMID).Text
        wvarStep = 34
        mvarESTADOMID = .selectSingleNode(mcteParam_ESTADOMID).Text
        wvarStep = 35
        mvarXMLDatosAIS = .selectSingleNode(mcteParam_XMLDatosAIS).Text
        wvarStep = 36
        mvarXMLDatosMID = .selectSingleNode(mcteParam_XMLDatosMID).Text
    End With
    
    wvarStep = 40
    'TODO: OJOOOOOOOOOOOO En mi PC no funciona con Trx. Para debuggear cambiar. Luego volver a su forma con Trx
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 50
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , "0")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 65
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RAMOPCOD", adChar, adParamInput, 4, mvarRAMOPCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NROFORM", adSmallInt, adParamInput, , mvarNROFORM)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SINIEANNAIS", adInteger, adParamInput)
    'Si mvarSINIEANNAIS vino vac��, env�o NULL
    If mvarSINIEANNAIS = "" Then wobjDBParm.Value = Null Else wobjDBParm.Value = mvarSINIEANNAIS
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 90
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SINIENUMAIS", adInteger, adParamInput)
    'Si mvarSINIENUMAIS vino vac��, env�o NULL
    If mvarSINIENUMAIS = "" Then wobjDBParm.Value = Null Else wobjDBParm.Value = mvarSINIENUMAIS
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 100
    Set wobjDBParm = wobjDBCmd.CreateParameter("@GRABSVIA", adChar, adParamInput, 2, mvarGRABSVIA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 110
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEC", adInteger, adParamInput)
    'Si mvarCLIENSEC vino vac��, env�o NULL
    If mvarCLIENSEC = "" Then wobjDBParm.Value = Null Else wobjDBParm.Value = mvarCLIENSEC
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 120
    Set wobjDBParm = wobjDBCmd.CreateParameter("@GRABAUSU", adChar, adParamInput, 10, mvarGRABAUSU)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 130
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTADODAT", adChar, adParamInput, 1, mvarESTADODAT)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 140
    Set wobjDBParm = wobjDBCmd.CreateParameter("@MSINIENUM", adChar, adParamInput, 1, mvarMSINIENUM)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 150
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RECARGA", adChar, adParamInput, 1, mvarRECARGA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 160
    Set wobjDBParm = wobjDBCmd.CreateParameter("@MARENVAIS", adChar, adParamInput, 1, mvarMARENVAIS)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 170
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTADOAIS", adChar, adParamInput, 2, mvarESTADOAIS)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 180
    Set wobjDBParm = wobjDBCmd.CreateParameter("@MARENVMID", adChar, adParamInput, 1, mvarMARENVMID)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 190
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTADOMID", adChar, adParamInput, 3, mvarESTADOMID)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 200
    Set wobjDBParm = wobjDBCmd.CreateParameter("@XMLDatosAIS", adLongVarChar, adParamInput, Len(wobjXMLRequest.xml), wobjXMLRequest.selectSingleNode("//XMLDatosAIS").xml)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 210
    Set wobjDBParm = wobjDBCmd.CreateParameter("@XMLDatosMID", adLongVarChar, adParamInput, Len(wobjXMLRequest.xml), wobjXMLRequest.selectSingleNode("//XMLDatosMID").xml)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 220
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ID_SINIESTRO", adInteger, adParamOutput, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 230
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FECHA_ALTA", adChar, adParamOutput, 20, "")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 240
    wobjDBCmd.Execute adExecuteNoRecords
    
    wvarStep = 250
    'Controlamos la respuesta del SQL
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 Then
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><ID_SINIESTRO>" & Right("0000000000" & wobjDBCmd.Parameters("@ID_SINIESTRO").Value, 10) & "</ID_SINIESTRO><FECHA_ALTA>" & wobjDBCmd.Parameters("@FECHA_ALTA").Value & "</FECHA_ALTA></Response>"
    Else
        wvarMensaje = "Error al insertar en la tabla SQL SINI_SINIESTROS"
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje='" & wvarMensaje & "' /></Response>"
    End If

    wvarStep = 260
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 270
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub








