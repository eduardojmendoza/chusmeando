VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVValidaFecIns"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog
Private mobjHSBC_DBCnn          As HSBCInterfaces.IDBConnection

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVSQLCotizar.lbaw_OVValidaFecIns"
Const mcteStoreProc         As String = "SPSNCV_PROD_VALIDA_FECHAS"

'Parametros XML de Entrada
Const mcteParam_EMISIANN    As String = "//EMISIANN"
Const mcteParam_EMISIMES    As String = "//EMISIMES"
Const mcteParam_EMISIDIA    As String = "//EMISIDIA"
Const mcteParam_EFECTANN    As String = "//EFECTANN"
Const mcteParam_EFECTMES    As String = "//EFECTMES"
Const mcteParam_EFECTDIA    As String = "//EFECTDIA"
Const mcteParam_INSPEANN    As String = "//INSPEANN"
Const mcteParam_INSPEMES    As String = "//INSPEMES"
Const mcteParam_INSPEDIA    As String = "//INSPEDIA"
Const mcteParam_ESTAINSP    As String = "//ESTAINSP"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarEMISIANN        As String
    Dim wvarEMISIMES        As String
    Dim wvarEMISIDIA        As String
    Dim wvarEFECTANN        As String
    Dim wvarEFECTMES        As String
    Dim wvarEFECTDIA        As String
    Dim wvarINSPEANN        As String
    Dim wvarINSPEMES        As String
    Dim wvarINSPEDIA        As String
    Dim wvarESTAINSP        As String

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        If .selectNodes(mcteParam_EMISIANN).length > 0 Then
            wvarEMISIANN = .selectSingleNode(mcteParam_EMISIANN).Text
        Else
            wvarEMISIANN = Year(Date)
        End If
        If .selectNodes(mcteParam_EMISIMES).length > 0 Then
            wvarEMISIMES = .selectSingleNode(mcteParam_EMISIMES).Text
        Else
            wvarEMISIMES = Right(String(2, "0") & Month(Date), 2)
        End If
        If .selectNodes(mcteParam_EMISIDIA).length > 0 Then
            wvarEMISIDIA = .selectSingleNode(mcteParam_EMISIDIA).Text
        Else
            wvarEMISIDIA = Right(String(2, "0") & Day(Date), 2)
        End If
        wvarEFECTANN = .selectSingleNode(mcteParam_EFECTANN).Text
        wvarEFECTMES = .selectSingleNode(mcteParam_EFECTMES).Text
        wvarEFECTDIA = .selectSingleNode(mcteParam_EFECTDIA).Text
        wvarINSPEANN = .selectSingleNode(mcteParam_INSPEANN).Text
        wvarINSPEMES = .selectSingleNode(mcteParam_INSPEMES).Text
        wvarINSPEDIA = .selectSingleNode(mcteParam_INSPEDIA).Text
        wvarESTAINSP = .selectSingleNode(mcteParam_ESTAINSP).Text
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    '
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    wvarStep = 50
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = mcteStoreProc
        .CommandType = adCmdStoredProc
    End With
        
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EMISIANN", adNumeric, adParamInput, , wvarEMISIANN)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EMISIMES", adNumeric, adParamInput, , wvarEMISIMES)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EMISIDIA", adNumeric, adParamInput, , wvarEMISIDIA)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 90
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTANN", adNumeric, adParamInput, , wvarEFECTANN)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 100
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTMES", adNumeric, adParamInput, , wvarEFECTMES)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 110
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTDIA", adNumeric, adParamInput, , wvarEFECTDIA)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 120
    Set wobjDBParm = wobjDBCmd.CreateParameter("@INSPEANN", adNumeric, adParamInput, , wvarINSPEANN)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 130
    Set wobjDBParm = wobjDBCmd.CreateParameter("@INSPEMES", adNumeric, adParamInput, , wvarINSPEMES)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 140
    Set wobjDBParm = wobjDBCmd.CreateParameter("@INSPEDIA", adNumeric, adParamInput, , wvarINSPEDIA)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 150
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTAINSP", adNumeric, adParamInput, , wvarESTAINSP)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 160
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTADO", adNumeric, adParamOutput, , 0)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 170
    Set wobjDBParm = wobjDBCmd.CreateParameter("@LEYENDA", adChar, adParamOutput, 200, "")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 120
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    '
    wvarStep = 130
    If Not wrstDBResult.EOF Then
        '
        If wrstDBResult.Fields.Item("ESTADO").Value <> 0 Then
            Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & Trim(wrstDBResult.Fields.Item("LEYENDA").Value) & Chr(34) & " /></Response>"
        Else
            wvarResult = ""
            Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
        End If
    End If
    wvarStep = 210
    Set wobjDBCmd = Nothing
    '
    wvarStep = 220
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 230
    Set wobjDBCnn = Nothing
    '
    wvarStep = 240
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 250
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 260
    Set wrstDBResult = Nothing
    '
    wvarStep = 270
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='ROW'>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESTADO'><xsl:value-of select='@ESTADO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='LEYENDA'><xsl:value-of select='@LEYENDA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='LEYENDA'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

