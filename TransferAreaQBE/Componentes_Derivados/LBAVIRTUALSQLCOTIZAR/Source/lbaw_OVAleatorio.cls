VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVAleatorio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context As ObjectContext
Private mobjEventLog    As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName     As String = "lbawA_OVSQLCotizar.lbaw_OVAleatorio"
Const mcteStoreProc     As String = "SPSNCV_DDJJ_VER_ALEATORIO"

'Parametros XML de Entrada
Const mcteParam_EfectAnn        As String = "//EFECTANN"
Const mcteParam_PatenNum        As String = "//PATENNUM"
Const mcteParam_MotorNum        As String = "//MOTORNUM"
Const mcteParam_NacimAnn        As String = "//NACIMANN"
Const mcteParam_NacimMes        As String = "//NACIMMES"
Const mcteParam_NacimDia        As String = "//NACIMDIA"
Const mcteParam_LocalidadCod    As String = "//LOCALIDADCOD"
Const mcteParam_CampaCod        As String = "//CAMPACOD"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    '
    Dim mvarEfectAnn        As String
    Dim mvarPatenNum        As String
    Dim mvarMotorNum        As String
    Dim mvarNacimAnn        As String
    Dim mvarNacimMes        As String
    Dim mvarNacimDia        As String
    Dim mvarLocalidadCod    As String
    Dim mvarCampaCod        As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        mvarEfectAnn = .selectSingleNode(mcteParam_EfectAnn).Text
        mvarPatenNum = .selectSingleNode(mcteParam_PatenNum).Text
        mvarMotorNum = .selectSingleNode(mcteParam_MotorNum).Text
        mvarNacimAnn = .selectSingleNode(mcteParam_NacimAnn).Text
        mvarNacimMes = .selectSingleNode(mcteParam_NacimMes).Text
        mvarNacimDia = .selectSingleNode(mcteParam_NacimDia).Text
        mvarLocalidadCod = .selectSingleNode(mcteParam_LocalidadCod).Text
        mvarCampaCod = .selectSingleNode(mcteParam_CampaCod).Text
        '
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    '
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    wvarStep = 50
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = mcteStoreProc
        .CommandType = adCmdStoredProc
    End With
    '
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ANIO", adNumeric, adParamInput, , mvarEfectAnn)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PATENTE", adVarChar, adParamInput, 6, mvarPatenNum)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@MOTOR", adVarChar, adParamInput, 25, mvarMotorNum)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 90
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ANIO_NAC", adNumeric, adParamInput, , mvarNacimAnn)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 100
    Set wobjDBParm = wobjDBCmd.CreateParameter("@MES_NAC", adNumeric, adParamInput, , mvarNacimMes)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 110
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DIA_NAC", adNumeric, adParamInput, , mvarNacimDia)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 120
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CODPOS", adNumeric, adParamInput, , mvarLocalidadCod)
    wobjDBParm.Precision = 5
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 130
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CAMPACOD", adVarChar, adParamInput, 4, mvarCampaCod)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 140
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    '
    wvarStep = 150
    If Not wrstDBResult.EOF Then
        '
        wvarStep = 160
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 170
        wrstDBResult.Save wobjXMLResponse, adPersistXML
        '
        wvarStep = 180
        wobjXSLResponse.async = False
        Call wobjXSLResponse.loadXML(p_GetXSL())
        '
        wvarStep = 190
        wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
        '
        wvarStep = 200
        Set wobjXMLResponse = Nothing
        Set wobjXSLResponse = Nothing
        '
        wvarStep = 210
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        wvarStep = 220
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "NO SE ENCONTRARON DATOS." & Chr(34) & " /></Response>"
    End If
    '
    wvarStep = 230
    Set wobjDBCmd = Nothing
    '
    wvarStep = 240
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 250
    Set wobjDBCnn = Nothing
    '
    wvarStep = 260
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 270
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 280
    Set wrstDBResult = Nothing
    '
    wvarStep = 290
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='ESTADO'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:value-of select='@RES' />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    
    p_GetXSL = wvarStrXSL
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub








