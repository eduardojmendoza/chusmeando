VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_OVUpdInspeAUS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'04/09/2007 - FO
'PPCR 20060393-1 - Se agrega un nuevo elemento en el XML, que es el que tiene la info de la oblea para el caso GNC.

Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog
Private mobjHSBC_DBCnn          As HSBCInterfaces.IDBConnection

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_OVSQLCotizar.lbaw_OVUpdInspeAUS"
Const mcteStoreProc             As String = "SPSNCV_PROD_SOLI_UPDATE_INSPE"

' Parametros XML de Entrada
Const mcteParam_RAMOPCOD                As String = "//RAMOPCOD"
Const mcteParam_POLIZANN                As String = "//POLIZANN"
Const mcteParam_POLIZSEC                As String = "//POLIZSEC"
Const mcteParam_CERTIPOL                As String = "//CERTIPOL"
Const mcteParam_CERTIANN                As String = "//CERTIANN"
Const mcteParam_CERTISEC                As String = "//CERTISEC"
Const mcteParam_SUPLENUM                As String = "//SUPLENUM"
Const mcteParam_MotorNum                As String = "//MOTORNUM"
Const mcteParam_CHASINUM                As String = "//CHASINUM"
Const mcteParam_PatenNum                As String = "//PATENNUM"
Const mcteParam_ESTAINSP                As String = "//ESTAINSP" '"//SITUCEST"
Const mcteParam_INSPECOD                As String = "//INSPECOD"
Const mcteParam_INSPEDES                As String = "//INSPECTOR"
Const mcteParam_INSPADOM                As String = "//INSPEADOM"
Const mcteParam_INSPEOBS                As String = "//OBSERTXT"
Const mcteParam_INSPEKMS                As String = "//INSPEKMS" '"//INSPENUM"
Const mcteParam_INSPEOBLEA              As String = "//INSPEOBLEA"
Const mcteParam_CENSICOD                As String = "//CENTRCOD_INS"
Const mcteParam_CENSIDES                As String = "//CENTRDES"
Const mcteParam_INSPEANN                As String = "//INSPEANN"
Const mcteParam_INSPEMES                As String = "//INSPEMES"
Const mcteParam_INSPEDIA                As String = "//INSPEDIA"
Const mcteParam_EFECTANN                As String = "//EFECTANN"
Const mcteParam_EFECTMES                As String = "//EFECTMES"
Const mcteParam_EFECTDIA                As String = "//EFECTDIA"
Const mcteParam_SITUCPOL                As String = "//SITUCPOL"
Const mcteParam_ESTAIDES                As String = "//ESTAIDES"
Const mcteParam_DISPODES                As String = "//DISPODES"
'-

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLValidacion   As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLCoberturas   As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    '
    ' DATOS GENERALES
    Dim mvarRAMOPCOD                As String
    Dim mvarPOLIZANN                As String
    Dim mvarPOLIZSEC                As String
    Dim mvarCERTIPOL                As String
    Dim mvarCERTIANN                As String
    Dim mvarCERTISEC                As String
    Dim mvarSUPLENUM                As String
    Dim mvarMotorNum                As String
    Dim mvarCHASINUM                As String
    Dim mvarPatenNum                As String
    Dim mvarESTAINSP                As String
    Dim mvarINSPECOD                As String
    Dim mvarINSPEDES                As String
    Dim mvarINSPADOM                As String
    Dim mvarINSPEOBS                As String
    Dim mvarINSPEKMS                As String
    Dim mvarINSPEOBLEA              As String
    Dim mvarCENSICOD                As String
    Dim mvarCENSIDES                As String
    Dim mvarINSPEANN                As String
    Dim mvarINSPEMES                As String
    Dim mvarINSPEDIA                As String
    Dim mvarEfectAnn                As String
    Dim mvarEFECTMES                As String
    Dim mvarEFECTDIA                As String
    Dim mvarSITUCPOL                As String
    Dim mvarESTAIDES                As String
    Dim mvarDISPODES                As String
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ' Cargo el xml de entrada
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    Call wobjXMLRequest.loadXML(pvarRequest)
    '
    wvarStep = 20
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVSQLCotizar.lbaw_OVValidaFecIns")
    Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
    Set wobjClass = Nothing
    '
    ' Guardo las coberturas y las sumas aseguradas
    wvarStep = 30
    Set wobjXMLValidacion = CreateObject("MSXML2.DOMDocument")
    wobjXMLValidacion.async = False
    wobjXMLValidacion.loadXML wvarResponse
    '
    wvarStep = 40
    ' Controlo como devolvi� la cotizaci�n
    If wobjXMLValidacion.selectSingleNode("//Response/Estado/@resultado").Text = "false" Then
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje='" & wobjXMLValidacion.selectSingleNode("//Response/Estado/@mensaje").Text & "' /></Response>"
    Else
    '
        wvarStep = 50
        With wobjXMLRequest
            '
            wvarStep = 60
            ' DATOS GENERALES
            mvarRAMOPCOD = .selectSingleNode(mcteParam_RAMOPCOD).Text
            mvarPOLIZANN = .selectSingleNode(mcteParam_POLIZANN).Text
            mvarPOLIZSEC = .selectSingleNode(mcteParam_POLIZSEC).Text
            mvarCERTIPOL = .selectSingleNode(mcteParam_CERTIPOL).Text
            mvarCERTIANN = .selectSingleNode(mcteParam_CERTIANN).Text
            mvarCERTISEC = .selectSingleNode(mcteParam_CERTISEC).Text
            wvarStep = 70
            mvarSUPLENUM = .selectSingleNode(mcteParam_SUPLENUM).Text
            mvarMotorNum = .selectSingleNode(mcteParam_MotorNum).Text
            mvarCHASINUM = .selectSingleNode(mcteParam_CHASINUM).Text
            mvarPatenNum = .selectSingleNode(mcteParam_PatenNum).Text
            mvarESTAINSP = .selectSingleNode(mcteParam_ESTAINSP).Text
            mvarINSPECOD = .selectSingleNode(mcteParam_INSPECOD).Text
            wvarStep = 80
            mvarINSPEDES = Left(Trim(.selectSingleNode(mcteParam_INSPEDES).Text), 30)
            mvarINSPADOM = .selectSingleNode(mcteParam_INSPADOM).Text
            mvarINSPEOBS = Left(Trim(.selectSingleNode(mcteParam_INSPEOBS).Text), 50)
            mvarINSPEKMS = .selectSingleNode(mcteParam_INSPEKMS).Text
            ' ------------------------------------------------------------------
            'La oblea se guarda como parte de la descripci�n de la inspecci�n.
            ' S�lo se guardan los primeros 50 caracteres.
            mvarINSPEOBLEA = Trim(.selectSingleNode(mcteParam_INSPEOBLEA).Text)
            If Len(mvarINSPEOBLEA) > 0 Then
                mvarINSPEOBS = Left("Nro Oblea: " + mvarINSPEOBLEA + ". " + mvarINSPEOBS, 50)
            End If
            ' ------------------------------------------------------------------
            mvarCENSICOD = .selectSingleNode(mcteParam_CENSICOD).Text
            mvarCENSIDES = Left(Trim(.selectSingleNode(mcteParam_CENSIDES).Text), 30)
            wvarStep = 90
            mvarINSPEANN = .selectSingleNode(mcteParam_INSPEANN).Text
            mvarINSPEMES = .selectSingleNode(mcteParam_INSPEMES).Text
            mvarINSPEDIA = .selectSingleNode(mcteParam_INSPEDIA).Text
            mvarEfectAnn = .selectSingleNode(mcteParam_EFECTANN).Text
            mvarEFECTMES = .selectSingleNode(mcteParam_EFECTMES).Text
            mvarEFECTDIA = .selectSingleNode(mcteParam_EFECTDIA).Text
            wvarStep = 100
            mvarSITUCPOL = .selectSingleNode(mcteParam_SITUCPOL).Text
            mvarESTAIDES = Left(.selectSingleNode(mcteParam_ESTAIDES).Text, 30)
            mvarDISPODES = Left(.selectSingleNode(mcteParam_DISPODES).Text, 30)
        End With
        '
        wvarStep = 110
        Set wobjXMLParametros = Nothing
        '
        wvarStep = 120
        Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
        Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
        Set wobjDBCmd = CreateObject("ADODB.Command")
        
        wvarStep = 130
        Set wobjDBCmd.ActiveConnection = wobjDBCnn
        wobjDBCmd.CommandText = mcteStoreProc
        wobjDBCmd.CommandType = adCmdStoredProc
    
        wvarStep = 140
        Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , "0")
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        '
        ' Parametros
        wvarStep = 150
        Set wobjDBParm = wobjDBCmd.CreateParameter("@RAMOPCOD", adChar, adParamInput, 4, mvarRAMOPCOD)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 160
        Set wobjDBParm = wobjDBCmd.CreateParameter("@POLIZANN", adNumeric, adParamInput, , mvarPOLIZANN)
        wobjDBParm.Precision = 2
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 170
        Set wobjDBParm = wobjDBCmd.CreateParameter("@POLIZSEC", adNumeric, adParamInput, , mvarPOLIZSEC)
        wobjDBParm.Precision = 6
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 180
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIPOL", adNumeric, adParamInput, , mvarCERTIPOL)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 190
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIANN", adNumeric, adParamInput, , mvarCERTIANN)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 200
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTISEC", adNumeric, adParamInput, , mvarCERTISEC)
        wobjDBParm.Precision = 6
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 210
        Set wobjDBParm = wobjDBCmd.CreateParameter("@SUPLENUM", adNumeric, adParamInput, , mvarSUPLENUM)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 220
        Set wobjDBParm = wobjDBCmd.CreateParameter("@MOTORNUM", adChar, adParamInput, 25, mvarMotorNum)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 230
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CHASINUM", adChar, adParamInput, 30, mvarCHASINUM)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 240
        Set wobjDBParm = wobjDBCmd.CreateParameter("@PATENNUM", adChar, adParamInput, 10, mvarPatenNum)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
            
        wvarStep = 250
        Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTAINSP", adNumeric, adParamInput, , mvarESTAINSP)
        wobjDBParm.Precision = 2
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
                                                    
        wvarStep = 260
        Set wobjDBParm = wobjDBCmd.CreateParameter("@INSPECOD", adNumeric, adParamInput, , mvarINSPECOD)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
                                                    
        wvarStep = 270
        Set wobjDBParm = wobjDBCmd.CreateParameter("@INSPEDES", adChar, adParamInput, 30, mvarINSPEDES)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
                                                    
        wvarStep = 280
        Set wobjDBParm = wobjDBCmd.CreateParameter("@INSPADOM", adChar, adParamInput, 1, mvarINSPADOM)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
                                                    
        wvarStep = 290
        Set wobjDBParm = wobjDBCmd.CreateParameter("@INSPEOBS", adChar, adParamInput, 50, mvarINSPEOBS)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 300
        Set wobjDBParm = wobjDBCmd.CreateParameter("@INSPEKMS", adNumeric, adParamInput, , mvarINSPEKMS)
        wobjDBParm.Precision = 8
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 310
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CENSICOD", adChar, adParamInput, 4, mvarCENSICOD)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 320
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CENSIDES", adChar, adParamInput, 30, mvarCENSIDES)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
            
        wvarStep = 330
        Set wobjDBParm = wobjDBCmd.CreateParameter("@INSPEANN", adNumeric, adParamInput, , mvarINSPEANN)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 340
        Set wobjDBParm = wobjDBCmd.CreateParameter("@INSPEMES", adNumeric, adParamInput, , mvarINSPEMES)
        wobjDBParm.Precision = 2
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 350
        Set wobjDBParm = wobjDBCmd.CreateParameter("@INSPEDIA", adNumeric, adParamInput, , mvarINSPEDIA)
        wobjDBParm.Precision = 2
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 360
        Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTANN", adNumeric, adParamInput, , mvarEfectAnn)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 370
        Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTMES", adNumeric, adParamInput, , mvarEFECTMES)
        wobjDBParm.Precision = 2
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 380
        Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTDIA", adNumeric, adParamInput, , mvarEFECTDIA)
        wobjDBParm.Precision = 2
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 390
        Set wobjDBParm = wobjDBCmd.CreateParameter("@SITUCPOL", adChar, adParamInput, 1, mvarSITUCPOL)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 400
        Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTAIDES", adChar, adParamInput, 30, mvarESTAIDES)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 410
        Set wobjDBParm = wobjDBCmd.CreateParameter("@DISPODES", adChar, adParamInput, 30, mvarDISPODES)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 420
        wobjDBCmd.Execute adExecuteNoRecords
    
        wvarStep = 430
        If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 Then
            pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><CADENA>" & wobjDBCmd.Parameters("@RETURN_VALUE").Value & "</CADENA></Response>"
        Else
            Select Case wobjDBCmd.Parameters("@RETURN_VALUE").Value
                Case -1
                wvarMensaje = "Error en la tabla SIFWINSVA"
                Case -2
                wvarMensaje = "Error en la tabla SEFWFIMA_VEHI"
                Case -3
                wvarMensaje = "Error en la tabla SEFWFIMA_POLI"
                Case -4
                wvarMensaje = "Error en la tabla SEFWFIMA_MOVI"
            End Select
            pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje='" & wvarMensaje & "' /></Response>"
        End If
    '
    End If
    '
    wvarStep = 440
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
    wvarStep = 450
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Set wobjXMLValidacion = Nothing
    '
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Set wobjXMLValidacion = Nothing
    '
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub








        
