VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_PutCotizacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog
Private mobjHSBC_DBCnn          As HSBCInterfaces.IDBConnection

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_OfVirtualLBA.lbaw_PutCotizacion"
Const mcteStoreProc             As String = "SPSNCV_ADM_INTERNET_COTI_INSERT"

' Parametros XML de Entrada
' DATOS GENERALES
Const mcteParam_FECHACOTI As String = "//FECHACOTI"
Const mcteParam_NACIMDIA As String = "//NACIMDIA"
Const mcteParam_NACIMMES As String = "//NACIMMES"
Const mcteParam_NACIMANN As String = "//NACIMANN"
Const mcteParam_ESTADO_OP As String = "//ESTADO_OP"
Const mcteParam_ESTADO As String = "//ESTADO"
Const mcteParam_LOCALIDAD As String = "//LOCALIDAD"
Const mcteParam_LOCALIDADCOD As String = "//LOCALIDADCOD"
Const mcteParam_SEXO As String = "//SEXO"
Const mcteParam_PROVI As String = "//PROVI"
Const mcteParam_PORTAL As String = "//PORTAL"
Const mcteParam_RAMOPCOD As String = "//RAMOPCOD"

' DATOS DEL VEHICULO
Const mcteParam_MARCAAUT As String = "//MARCAAUT"
Const mcteParam_MODEAUT As String = "//MODEAUT"
Const mcteParam_EFECTANN As String = "//EFECTANN"
Const mcteParam_KMSRNGCOD As String = "//KMSRNGCOD"
Const mcteParam_SINIESTROS As String = "//SINIESTROS"
Const mcteParam_RBOKM As String = "//RBOKM"
Const mcteParam_SUMASEG As String = "//SUMASEG"
Const mcteParam_HIJOS1729 As String = "//HIJOS1729"
Const mcteParam_CONDADIC As String = "//CONDADIC"
Const mcteParam_MODEAUTCOD As String = "//MODEAUTCOD"
Const mcteParam_GAS As String = "//GAS"

' DATOS DE CONDUCTORES ADICIONALES
Const mcteParam_HIJOS As String = "//HIJOS/HIJO"
Const mcteParam_EDADHIJO As String = "EDADHIJO"
Const mcteParam_ESTADOHIJO As String = "ESTADOHIJO"
Const mcteParam_SEXOHIJO As String = "SEXOHIJO"

' --- CAMPOS AGREGADOS 03-12-2005. ---
' Modificación Realizada: 12-12-2005. Fernando Osores
Const mcteParam_TELEFONO As String = "//TELEFONO"
Const mcteParam_NOMBRE As String = "//NOMBRE"
Const mcteParam_APELLIDO As String = "//APELLIDO"
Const mcteParam_GARAGE As String = "//GARAGE"
Const mcteParam_POSEEACC As String = "//POSEEACC"
Const mcteParam_DOCUMTIP As String = "//DOCUMTIP"
Const mcteParam_DOCUMDAT As String = "//DOCUMDAT"
Const mcteParam_EMAILP As String = "//EMAILP"
Const mcteParam_EMAILL As String = "//EMAILL"
Const mcteParam_CLIENTE As String = "//CLIENTE"
Const mcteParam_CONFORM As String = "//CONFORM"
Const mcteParam_CAMCMLCOD As String = "//CAMCMLCOD"
Const mcteParam_TIPOCLI As String = "//TIPOCLI"

' DATOS DE ACCESORIOS ADICIONALES
Const mcteParam_AU As String = "//ACCESORIOS/ACCESORIO"
Const mcteParam_AUACCCOD As String = "CODIGOACC"
Const mcteParam_AUVEASUM As String = "PRECIOACC"
Const mcteParam_AUVEADES As String = "DESCRIPCIONACC"
' --- FIN MODIFICACION 12-12-2005 ---


Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    '
    ' DATOS GENERALES
    Dim wvarFechaCoti As String
    Dim wvarNACIMDIA As String
    Dim wvarNACIMMES As String
    Dim wvarNACIMANN As String
    Dim wvarEstado_OP As String
    Dim wvarEstado As String
    Dim wvarLOCALIDAD As String
    Dim wvarLocalidadCod As String
    Dim wvarSexo As String
    Dim wvarProvi As String
    Dim wvarPORTAL As String
    Dim wvarRAMOPCOD As String
    '
    ' DATOS DEL VEHICULO
    Dim wvarMarcaAut As String
    Dim wvarModeAut As String
    Dim wvarEFECTANN As String
    Dim wvarKmsRngCod As String
    Dim wvarSiniestros As String
    Dim wvarRbOkm As String
    Dim wvarSumAseg As String
    Dim wvarHijos1729 As String
    Dim wvarCondAdic As String
    Dim wvarMarcaAutCod As String
    Dim wvarModeAutCod As String
    Dim wvarAuSubCod As String
    Dim wvarAuAdiCod As String
    Dim wvarAuModOri As String
    Dim wvarGas As String
    '
    ' DATOS DE CONDUCTORES ADICIONALES
    Dim wvarHijos As String
    Dim wvarEdadHijo As String
    Dim wvarEstadoHijo As String
    Dim wvarSexoHijo As String
    '
    ' --- CAMPOS AGREGADOS 03-12-2005. ---
    ' Modificación Realizada: 12-12-2005. Fernando Osores
    Dim wvarTELEFONO As String
    Dim wvarNOMBRE As String
    Dim wvarAPELLIDO As String
    Dim wvarAPELLIDO_NOMBRE As String
    Dim wvarGARAGE As String
    Dim wvarPOSEEACC As String
    Dim wvarDOCUMTIP As String
    Dim wvarDOCUMDAT As String
    Dim wvarEMAILP As String
    Dim wvarEMAILL As String
    Dim wvarCLIENTE As String
    Dim wvarCONFORM As String
    Dim wvarCAMCMLCOD As String
    Dim wvarAUACCCOD As String
    Dim wvarAUVEASUM As String
    Dim wvarAUVEADES As String
    Dim wvarTIPOCLI As String
    ' --- FIN MODIFICACION 12-12-2005 ---
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
        '
        wvarStep = 20
        ' DATOS GENERALES
        wvarFechaCoti = .selectSingleNode(mcteParam_FECHACOTI).Text
        wvarNACIMDIA = .selectSingleNode(mcteParam_NACIMDIA).Text
        wvarNACIMMES = .selectSingleNode(mcteParam_NACIMMES).Text
        wvarNACIMANN = .selectSingleNode(mcteParam_NACIMANN).Text
        wvarEstado_OP = .selectSingleNode(mcteParam_ESTADO_OP).Text
        wvarEstado = .selectSingleNode(mcteParam_ESTADO).Text
        wvarLOCALIDAD = Left(.selectSingleNode(mcteParam_LOCALIDAD).Text, 50)
        wvarLocalidadCod = .selectSingleNode(mcteParam_LOCALIDADCOD).Text
        wvarSexo = .selectSingleNode(mcteParam_SEXO).Text
        wvarProvi = .selectSingleNode(mcteParam_PROVI).Text
        wvarPORTAL = .selectSingleNode(mcteParam_PORTAL).Text
        wvarRAMOPCOD = "AUS1"
        ' DATOS DEL VEHICULO
        wvarMarcaAut = .selectSingleNode(mcteParam_MARCAAUT).Text
        wvarModeAut = .selectSingleNode(mcteParam_MODEAUT).Text
        wvarEFECTANN = .selectSingleNode(mcteParam_EFECTANN).Text
        wvarKmsRngCod = .selectSingleNode(mcteParam_KMSRNGCOD).Text
        wvarSiniestros = .selectSingleNode(mcteParam_SINIESTROS).Text
        wvarRbOkm = .selectSingleNode(mcteParam_RBOKM).Text
        wvarSumAseg = .selectSingleNode(mcteParam_SUMASEG).Text
        wvarHijos1729 = .selectSingleNode(mcteParam_HIJOS1729).Text
        wvarCondAdic = .selectSingleNode(mcteParam_CONDADIC).Text
        wvarMarcaAutCod = Left(.selectSingleNode(mcteParam_MODEAUTCOD).Text, 5)
        wvarModeAutCod = Mid(.selectSingleNode(mcteParam_MODEAUTCOD).Text, 6, 5)
        wvarAuSubCod = Mid(.selectSingleNode(mcteParam_MODEAUTCOD).Text, 11, 5)
        wvarAuAdiCod = Mid(.selectSingleNode(mcteParam_MODEAUTCOD).Text, 16, 5)
        wvarGas = .selectSingleNode(mcteParam_GAS).Text
        '
        ' --- CAMPOS AGREGADOS 03-12-2005. ---
        ' Modificación Realizada: 12-12-2005. Fernando Osores
        If Not .selectSingleNode(mcteParam_NOMBRE) Is Nothing Then
            wvarNOMBRE = .selectSingleNode(mcteParam_NOMBRE).Text
        Else
            wvarNOMBRE = ""
        End If
        If Not .selectSingleNode(mcteParam_APELLIDO) Is Nothing Then
            wvarAPELLIDO = .selectSingleNode(mcteParam_APELLIDO).Text
        Else
            wvarAPELLIDO = ""
        End If
        wvarAPELLIDO_NOMBRE = wvarAPELLIDO & ", " & wvarNOMBRE
        If Len(wvarAPELLIDO_NOMBRE) > 50 Then wvarAPELLIDO_NOMBRE = Mid(wvarAPELLIDO_NOMBRE, 1, 50)
        
        If Not .selectSingleNode(mcteParam_TELEFONO) Is Nothing Then
            wvarTELEFONO = .selectSingleNode(mcteParam_TELEFONO).Text
        Else
            wvarTELEFONO = ""
        End If
        If Not .selectSingleNode(mcteParam_GARAGE) Is Nothing Then
            wvarGARAGE = .selectSingleNode(mcteParam_GARAGE).Text
        Else
            wvarGARAGE = ""
        End If
        If Not .selectSingleNode(mcteParam_POSEEACC) Is Nothing Then
            wvarPOSEEACC = .selectSingleNode(mcteParam_POSEEACC).Text
        Else
            wvarPOSEEACC = ""
        End If
        If Not .selectSingleNode(mcteParam_DOCUMTIP) Is Nothing Then
            wvarDOCUMTIP = .selectSingleNode(mcteParam_DOCUMTIP).Text
        Else
            wvarDOCUMTIP = 0
        End If
        If Not .selectSingleNode(mcteParam_DOCUMDAT) Is Nothing Then
            wvarDOCUMDAT = .selectSingleNode(mcteParam_DOCUMDAT).Text
        Else
            wvarDOCUMDAT = ""
        End If
        If Not .selectSingleNode(mcteParam_EMAILP) Is Nothing Then
            wvarEMAILP = .selectSingleNode(mcteParam_EMAILP).Text
        Else
            wvarEMAILP = ""
        End If
        If Not .selectSingleNode(mcteParam_EMAILL) Is Nothing Then
            wvarEMAILL = .selectSingleNode(mcteParam_EMAILL).Text
        Else
            wvarEMAILL = ""
        End If
        If Not .selectSingleNode(mcteParam_CLIENTE) Is Nothing Then
            wvarCLIENTE = .selectSingleNode(mcteParam_CLIENTE).Text
        Else
            wvarCLIENTE = ""
        End If
        If Not .selectSingleNode(mcteParam_CONFORM) Is Nothing Then
            wvarCONFORM = .selectSingleNode(mcteParam_CONFORM).Text
        Else
            wvarCONFORM = ""
        End If
        If Not .selectSingleNode(mcteParam_CAMCMLCOD) Is Nothing Then
            wvarCAMCMLCOD = .selectSingleNode(mcteParam_CAMCMLCOD).Text
        Else
            wvarCAMCMLCOD = ""
        End If
        
        If Not .selectSingleNode(mcteParam_TIPOCLI) Is Nothing Then
            wvarTIPOCLI = .selectSingleNode(mcteParam_TIPOCLI).Text
        Else
            wvarTIPOCLI = ""
        End If

        ' --- FIN MODIFICACION 12-12-2005 ---
        '
    End With
    
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 60
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc

    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
        
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FECHACOTI", adVarChar, adParamInput, 8, wvarFechaCoti)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NACIMDIA", adInteger, adParamInput, , wvarNACIMDIA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NACIMMES", adInteger, adParamInput, , wvarNACIMMES)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NACIMANN", adInteger, adParamInput, , wvarNACIMANN)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTADO_ID", adInteger, adParamInput, , wvarEstado_OP)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    'Set wobjDBParm = wobjDBCmd.CreateParameter("@APELLIDO_NOMBRE", adVarChar, adParamInput, 50, "")
    Set wobjDBParm = wobjDBCmd.CreateParameter("@APELLIDO_NOMBRE", adVarChar, adParamInput, 50, wvarAPELLIDO_NOMBRE)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    'Set wobjDBParm = wobjDBCmd.CreateParameter("@TELEFONO", adVarChar, adParamInput, 50, "")
    Set wobjDBParm = wobjDBCmd.CreateParameter("@TELEFONO", adVarChar, adParamInput, 50, wvarTELEFONO)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTADO_CIVIL_ID", adVarChar, adParamInput, 1, wvarEstado)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@LOCALIDAD", adVarChar, adParamInput, 50, wvarLOCALIDAD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@POSTACOD", adInteger, adParamInput, , wvarLocalidadCod)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SEXO", adChar, adParamInput, 1, wvarSexo)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PROVINCIAID", adInteger, adParamInput, , wvarProvi)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PORTAL", adChar, adParamInput, 10, wvarPORTAL)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RAMOPCOD", adChar, adParamInput, 4, wvarRAMOPCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
                                                            
    Set wobjDBParm = wobjDBCmd.CreateParameter("@MARCADES", adVarChar, adParamInput, 50, wvarMarcaAut)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    'Set wobjDBParm = wobjDBCmd.CreateParameter("@VEHÍCULO", adVarChar, adParamInput, 50, wvarModeAut) ' MQ 0093
    Set wobjDBParm = wobjDBCmd.CreateParameter("@VEHÍCULO", adVarChar, adParamInput, 80, wvarModeAut) ' MQ 0051
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FABRICAN", adInteger, adParamInput, , wvarEFECTANN)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@AUKLMNUM", adInteger, adParamInput, , wvarKmsRngCod)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@AUNUMSIN", adInteger, adParamInput, , wvarSiniestros)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ESCERO", adBoolean, adParamInput, , wvarRbOkm)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SUMAASEG", adNumeric, adParamInput, , wvarSumAseg)
    wobjDBParm.Precision = 18
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CBITVRTA", adBoolean, adParamInput, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    Set wobjDBParm = wobjDBCmd.CreateParameter("@ITVLSDIA", adInteger, adParamInput, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    Set wobjDBParm = wobjDBCmd.CreateParameter("@ITVLSMES", adInteger, adParamInput, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    Set wobjDBParm = wobjDBCmd.CreateParameter("@CBITVANN", adInteger, adParamInput, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    Set wobjDBParm = wobjDBCmd.CreateParameter("@HIJOS1729", adBoolean, adParamInput, , wvarHijos1729)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@INCLUYEHIJOS", adBoolean, adParamInput, , wvarCondAdic)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@AUMARCOD", adNumeric, adParamInput, , wvarMarcaAutCod)
    wobjDBParm.Precision = 5
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@AUMODCOD", adNumeric, adParamInput, , wvarModeAutCod)
    wobjDBParm.Precision = 5
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@AUSUBCOD", adNumeric, adParamInput, , wvarAuSubCod)
    wobjDBParm.Precision = 5
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@AUADICOD", adNumeric, adParamInput, , wvarAuAdiCod)
    wobjDBParm.Precision = 5
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@AUMODORI", adChar, adParamInput, 1, wvarAuModOri)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NUMECOLO", adNumeric, adParamInput, , 0)
    wobjDBParm.Precision = 3
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@AUUSOGNC", adChar, adParamInput, 1, wvarGas)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    ' DATOS DE CONDUCTORES ADICIONALES
    wvarStep = 70
    Set wobjXMLList = wobjXMLRequest.selectNodes(mcteParam_HIJOS)

    For wvarCounter = 0 To wobjXMLList.length - 1
        wvarEdadHijo = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_EDADHIJO).Text
        wvarEstadoHijo = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_ESTADOHIJO).Text
        wvarSexoHijo = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_SEXOHIJO).Text
    
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CAEDAD" & CStr(wvarCounter + 1), adInteger, adParamInput, , wvarEdadHijo)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
            
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CAESTADO_CIVIL" & CStr(wvarCounter + 1), adChar, adParamInput, 1, wvarEstadoHijo)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
            
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CASEXO" & CStr(wvarCounter + 1), adChar, adParamInput, 1, wvarSexoHijo)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    Next
    
    wvarStep = 80
    For wvarCounter = wobjXMLList.length + 1 To 10
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CAEDAD" & CStr(wvarCounter), adInteger, adParamInput, , 0)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
            
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CAESTADO_CIVIL" & CStr(wvarCounter), adChar, adParamInput, 1, "")
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
            
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CASEXO" & CStr(wvarCounter), adChar, adParamInput, 1, "")
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    Next
    Set wobjXMLList = Nothing
    
    ' --- CAMPOS AGREGADOS 03-12-2005. ---
    ' Modificación Realizada: 12-12-2005. Fernando Osores
    wvarStep = 90
    Set wobjDBParm = wobjDBCmd.CreateParameter("@GARAGE", adChar, adParamInput, 1, wvarGARAGE)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@POSEEACC", adChar, adParamInput, 1, wvarPOSEEACC)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOCUMTIP", adNumeric, adParamInput, , CInt("0" & wvarDOCUMTIP))
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOCUMDAT", adChar, adParamInput, 11, wvarDOCUMDAT)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EMAILP", adChar, adParamInput, 80, wvarEMAILP)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EMAILL", adChar, adParamInput, 80, wvarEMAILL)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTE", adChar, adParamInput, 1, wvarCLIENTE)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CONFORM", adChar, adParamInput, 1, wvarCONFORM)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CAMCMLCOD", adChar, adParamInput, 4, wvarCAMCMLCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@TIPOCLI", adChar, adParamInput, 1, wvarTIPOCLI)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    
    wvarStep = 100
    Set wobjXMLList = wobjXMLRequest.selectNodes(mcteParam_AU)
    wvarStep = 110
    For wvarCounter = 0 To wobjXMLList.length - 1
        wvarAUACCCOD = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_AUACCCOD).Text
        wvarAUVEASUM = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_AUVEASUM).Text
        wvarAUVEADES = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_AUVEADES).Text
        
        Set wobjDBParm = wobjDBCmd.CreateParameter("@AUACCCOD" & CStr(wvarCounter + 1), adNumeric, adParamInput, , CInt(wvarAUACCCOD))
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
            
        Set wobjDBParm = wobjDBCmd.CreateParameter("@AUVEASUM" & CStr(wvarCounter + 1), adNumeric, adParamInput, , wvarAUVEASUM)
        wobjDBParm.Precision = 15
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
            
        Set wobjDBParm = wobjDBCmd.CreateParameter("@AUVEADES" & CStr(wvarCounter + 1), adChar, adParamInput, 30, wvarAUVEADES)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    Next
    For wvarCounter = wobjXMLList.length + 1 To 10
        Set wobjDBParm = wobjDBCmd.CreateParameter("@AUACCCOD" & CStr(wvarCounter), adNumeric, adParamInput, , 0)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
            
        Set wobjDBParm = wobjDBCmd.CreateParameter("@AUVEASUM" & CStr(wvarCounter), adNumeric, adParamInput, , 0)
        wobjDBParm.Precision = 15
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
            
        Set wobjDBParm = wobjDBCmd.CreateParameter("@AUVEADES" & CStr(wvarCounter), adChar, adParamInput, 30, "")
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    Next
    Set wobjXMLList = Nothing
    ' --- FIN MODIFICACION 12-12-2005 ---
    
    
    wvarStep = 140
    wobjDBCmd.Execute adExecuteNoRecords

    wvarStep = 150
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value > 0 Then
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><COTIID>" & wobjDBCmd.Parameters("@RETURN_VALUE").Value & "</COTIID></Response>"
    Else
        Select Case wobjDBCmd.Parameters("@RETURN_VALUE").Value
            Case -1
                wvarMensaje = "PARAMETROS INCORRECTOS"
            Case -2
                wvarMensaje = "ERROR AL INSERTAR"
            Case -3
                wvarMensaje = "ERROR AL ACTUALIZAR"
            Case -4
                wvarMensaje = "ERROR AL ELIMINAR"
        End Select
        
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & wvarMensaje & " /></Response>"
    End If
            
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 180
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub







