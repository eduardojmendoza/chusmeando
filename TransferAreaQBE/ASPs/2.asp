<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2009. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: DatosAdicionales.asp

Fecha de Creacion: Desconocida, se agrega el 08/10/2009 por modif. ppcr para QA

PPcR: 2008-00848

Desarrollador: Jose Casais

Descripcion: Detalle Consulta de pagos de siniestros
-->

<!--#include virtual="/Oficina_Virtual/Library/IncludesFiles.asp"-->     
<!--#include virtual="/Oficina_Virtual/Library/funcionesGenerales.asp"-->

<%          
                               
	'Variable que tomo para saber que funcion debo ejecutar.
	objlocal = Request.Form("objlocal")          	  
	                                       
	Response.ContentType = "text/xml"      
	Response.charset= "iso-8859-1"	       
	
	Select Case objlocal
          Case "DatosAdic"
              Response.Write DatosAdic()                   
          Case "DatosGestion"
              Response.Write DatosGestion()
          Case "ConsultaPagos"
              Response.Write ConsultaPagos()
          Case "DatosWorkflow"
              Response.Write DatosWorkflow()
          Case "SubCoberturas"
              Response.Write SubCoberturas()
          Case Else
              Response.Write ("ERROR: No se encuentra la funcion pedida.")
        End Select
 
 
'Funcion que muestra los Gestiones de Expedientes de Siniestros. Arma la Tabla con las Gestiones.
Function DatosGestion()

	mvarUSUARIO  = session("USER")
	mvarPRODUCTO = Request.Form("Producto")
	mvarSINIAN   = Request.Form("Sinian")
	mvarSININUM  = Request.Form("Sininum")
	
	mvarPERFIL_AS   = session("PERFIL_AS")
	mvarCLIENSEC_AS = session("CLIENSEC_AS")
	
	mvarRequest = "<Request>		  	  		       	" & _
		      "		<USUARIO> " & mvarUSUARIO  & "</USUARIO>  	" & _
				"	<NIVELAS>"	& mvarPERFIL_AS & "</NIVELAS>" & _
				"	<CLIENSECAS>" & mvarCLIENSEC_AS & "</CLIENSECAS>" & _
				"	<NIVEL1></NIVEL1>" & _
				"	<CLIENSEC1></CLIENSEC1>" & _
				"	<NIVEL2></NIVEL2>" & _
				"	<CLIENSEC2></CLIENSEC2>" & _
				"	<NIVEL3></NIVEL3>" & _
				"	<CLIENSEC3></CLIENSEC3>" & _		      
		     	"	<PRODUCTO>" & mvarPRODUCTO & "</PRODUCTO>	" & _
		      	"	<SINIAN>" & mvarSINIAN & "</SINIAN>    	" & _
		      	"	<SININUM>" & mvarSININUM  & "</SININUM>  	" & _
		      "</Request>"
		
	Call cmdp_ExecuteTrn("lbaw_OVSiniGestiones", _
						 "lbaw_OVSiniGestiones.biz", _
						 mvarRequest, _
						 mvarResponse)
	
	
	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
	mobjXMLDoc.async = false
	Call mobjXMLDoc.loadXML(mvarResponse)	

	if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text = "true") then	   
		Set wvarXMLList = mobjXMLDoc.selectNodes("//REGS/REG")

		Adicionales = "<table width='100%' border='1' cellpadding='0' cellspacing='1' bordercolor='#FFFFFF'>"& _
			      "        <tr>                                                                        "& _
			      "          <td bordercolor='#dedfe7'>                                                "& _
			      "			<table width='100%' border='0' cellpadding='2' cellspacing='1'>    "& _
			      "       		                                                                   "& _
			      "                    <tr class='form_titulos02'>                 			   "& _
			      "                    <td>Tipo de Gesti&oacute;n</td>                                        "& _
			      "                    <td colspan=2>Estado</td>                                                 "& _
			      "                    <td>Reclamante</td>                                             "& _
			      "                    <td >Gestionador-Liquidador</td>                                 "& _
			      "                    <td colspan=2>Responsable</td>                                 "& _
			      "                    <td>Auto Sustituto </td>                                        "& _
			      "                    <td>SC</td>                                        		   "& _
				  "                </tr>                  			      "
				  
		 For wvarCounter = 0 To wvarXMLList.length-1
		  		PEPE = ""
		  		If (wvarXMLList.Item(wvarCounter).selectSingleNode("EST").text = "C") Then
		  			mvarEstado = "Cerrado"
		  		Else	
		  			mvarEstado = "Abierto"
				End If
				If (wvarXMLList.Item(wvarCounter).selectSingleNode("SUSTI").text = "S") Then
					mvarSusti = "Solicitado"
				ElseIf (wvarXMLList.Item(wvarCounter).selectSingleNode("SUSTI").text = "N")Then	
					mvarSusti = "No Solicitado"
				End If
	

				tmp = "<div style='display:none' id='BotonAbrirCerrar'></div><div id='botonWorkflow" & wvarCounter & "B' name='BotonAbrirWorkFlow'> <a href=javascript:on_off('botonWorkflow" & wvarCounter & "','WorkFlow','" & wvarXMLList.Item(wvarCounter).selectSingleNode("GESCLA").text & "','" & wvarXMLList.Item(wvarCounter).selectSingleNode("GESSEC").text & "'); class='linksdos'><img src='/Oficina_virtual/HTML/Images/boton_buscar.gif' border='0' alt='Detalle de workflow' align='absmiddle'></a></div><div style='display:none' id='botonWorkflow" & wvarCounter & "E' name='BotonCerrarWorkFlow'> <a href=javascript:off_botones_workflow('WorkFlow'); class='linksdos'><img src='/Oficina_virtual/HTML/Images/boton_cancel.gif' border='0' alt='Detalle de workflow' align='absmiddle'></a></div>"
					      ' " 	<td class='linksdos' style='cursor:hand' id='" & wvarCounter & "' onclick='javascript:abrirPopUp();'" & ">"&trim(wvarXMLList.Item(wvarCounter).selectSingleNode("GESNOM").text)	&" <img src='/Oficina_virtual/HTML/Images/boton_buscar.gif' border='0' alt='Ver datos del gestionador' align='absmiddle' valign='center'></td>"& _
				 
				Adicionales  =  Adicionales & "<tr bgcolor='#FFFFFF' class='Form_campname_bcoL'> 		"& _
					      "		<td>"&trim(wvarXMLList.Item(wvarCounter).selectSingleNode("GESDES").text)	&"</td>"& _
					      " 	<td>"&trim(mvarEstado)& "</td><td align=right>" & tmp & "</td>"& _
					      " 	<td>"&trim(wvarXMLList.Item(wvarCounter).selectSingleNode("RECLANOM").text)	&"</td>"& _
					      "     <td>"&trim(wvarXMLList.Item(wvarCounter).selectSingleNode("GESNOM").text)	& "</td>"& _
					      "     <td>"&trim(wvarXMLList.Item(wvarCounter).selectSingleNode("RESPEXP").text)	& "</td><td align=right  id='" & wvarCounter & "' onclick=""" & "javascript:abrirPopUp();""" & ">" &"<img src='/Oficina_virtual/HTML/Images/boton_buscar.gif' style='cursor:hand' border='0' alt='Ver datos del responsable' align='absmiddle' valign='rigth' onclick=""" & "javascript:abrirPopUp();""" & "></td>"& _
					      " 	<td>"&trim(mvarSusti)&"</td>"& _
			      		  "		<td align=center><img src='/Oficina_virtual/HTML/Images/boton_buscar.gif' style='cursor:hand' border='0' alt='Detalle de Subcobertura' align='absmiddle' valign='rigth' onclick=""" & "javascript:MostrarSubCoberturas('" & mvarUSUARIO & "','" & mvarPRODUCTO & "','" & mvarSINIAN & "','" & mvarSININUM & "','" & wvarXMLList.Item(wvarCounter).selectSingleNode("GESCLA").text & "','" & wvarXMLList.Item(wvarCounter).selectSingleNode("GESSEC").text & "');""" & "></td>"& _
					      " </tr>" 					      
				
				If Not wvarXMLList.Item(wvarCounter).selectSingleNode("RESPEXP") Is Nothing then
					mvarRESPEXP = wvarXMLList.Item(wvarCounter).selectSingleNode("RESPEXP").text
				End If
				
				If Not wvarXMLList.Item(wvarCounter).selectSingleNode("MAILRESP") Is Nothing then
					mvarMAILRESP = wvarXMLList.Item(wvarCounter).selectSingleNode("MAILRESP").text   
				End If
				
				If Not wvarXMLList.Item(wvarCounter).selectSingleNode("TELERESP") Is Nothing then				
					mvarTELERESP = wvarXMLList.Item(wvarCounter).selectSingleNode("TELERESP").text
				End If
				If len(trim(mvarRESPEXP)) = 0 Then mvarRESPEXP = "no hay datos."
				If len(trim(mvarMAILRESP)) = 0 Then mvarMAILRESP = "no hay datos."
				If len(trim(mvarTELERESP)) = 0 Then mvarTELERESP = "no hay datos."	
				
				PEPE = PEPE & 				"<table>" & _
											"<!--tr><td><font face='verdana,arial,helvetica' size='1'><b>Responsable: </b></td>" & "<td><font face='verdana,arial,helvetica' size='1'>" & mvarRESPEXP & "</td></tr-->" & _
			    							"<tr><td><font face='verdana,arial,helvetica' size='1'><b>Teléfono: </b></td>" & "<td><font face='verdana,arial,helvetica' size='1'>" & mvarTELERESP & "</td></tr>" & _
			    							"<tr><td><font face='verdana,arial,helvetica' size='1'><b>Email: </b></td>" & "<td><font face='verdana,arial,helvetica' size='1'>" & mvarMAILRESP & "</td></tr>" & _
		 	    							"</table>"		 
			%>
			<input type="hidden" id="div<%=wvarCounter%>" value="<%=PEPE%>">
			<%
			 Next
		
		
		Adicionales  = Adicionales & "   </table> "& _
				  "			</td>     "& _
			      "		</tr>             "& _
			      "</table>                   "& _
	             "<BR>			  "
	Else	
		Adicionales = "<table width='100%' border='1' cellpadding='0' cellspacing='1' bordercolor='#FFFFFF'>"& _
			      "        <tr>                                                                         "& _
			      "          <td bordercolor='#dedfe7'>                                                 "& _
			      "			<table width='100%' border='0' cellpadding='2' cellspacing='1'>     "& _
			      "       		                                                                    "& _
			      "                    <tr class='Form_campname_bcoC'>                 			    "& _
			      "                    <td>No Hay Gestiones de Expedientes de Siniestros.</td>                     "& _                    
			      "                    </tr>                     					    "& _
			      "                 </table>                     					    "& _
			      "          </td>                     					    	    "& _
			      "        </tr>                     					            "& _
			      "</table>                     					                    "
	End If
	
		
	DatosGestion = Adicionales 

End function
         
'Funcion que muestra los Datos Adicionales del Siniestro. Arma una tabla con todos lo datos adentro.
Function DatosAdic()

	mvarUSUARIO  = session("USER")
	mvarPRODUCTO = Request.Form("Producto")
	mvarPERFIL_AS   = session("PERFIL_AS")
	mvarCLIENSEC_AS = session("CLIENSEC_AS")	
	mvarSINIAN   = Request.Form("Sinian")
	mvarSININUM  = Request.Form("Sininum")

	mvarRequest = "<Request>		  	  		       	" & _
				"	<USUARIO> " & mvarUSUARIO  & "</USUARIO>  	" & _
				"	<NIVELAS>"	& mvarPERFIL_AS & "</NIVELAS>" & _
				"	<CLIENSECAS>" & mvarCLIENSEC_AS & "</CLIENSECAS>" & _
				"	<NIVEL1></NIVEL1>" & _
				"	<CLIENSEC1></CLIENSEC1>" & _
				"	<NIVEL2></NIVEL2>" & _
				"	<CLIENSEC2></CLIENSEC2>" & _
				"	<NIVEL3></NIVEL3>" & _
				"	<CLIENSEC3></CLIENSEC3>" & _
				"	<PRODUCTO>" & mvarPRODUCTO & "</PRODUCTO>	" & _
				"	<SINIAN>  " & mvarSINIAN   & "</SINIAN>    	" & _
				"	<SININUM> " & mvarSININUM  & "</SININUM>  	" & _
		      "</Request>"
		
	Call cmdp_ExecuteTrn("lbaw_OVSiniDatosAdic", _
						 "lbaw_OVSiniDatosAdic.biz", _
						 mvarRequest, _
						 mvarResponse)
	
	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
	mobjXMLDoc.async = false
	 
	Call mobjXMLDoc.loadXML(mvarResponse)
	if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text = "true") then	 	
				Adicionales ="<table width='100%' border='1' cellpadding='0' cellspacing='1' bordercolor='#FFFFFF'>                                                                       "& _
			        "      			 <tr>                                                                                                                                    	 "& _
			        "       			 <td bordercolor='#dedfe7'>                                                                                                              "& _
				"						<table width='100%' border='0' cellspacing='1' cellpadding='0'>                                                          "& _
				"						<tr>                                                                                                                     "& _
				"						  <td width='50%'><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='form_titulos_camp'>  "& _
				"							  <tr>                                                                                                           "& _
				"								<td width='23'>&nbsp;</td>                                                                               "& _
				"								<td width='263'>Da&ntilde;os Propios:</td>                                                              "& _
				"							  </tr>                                                                                                          "& _
				"						  </table></td>                                                                                                          "& _
				"						  <td width='50%'><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='form_titulos_camp'>  "& _
				"							  <tr>                                                                                                           "& _
				"								<td width='23'>&nbsp;</td>                                                                               "& _
				"								<td width='246'>Da&ntilde;os Terceros:</td>                                                             "& _
				"							  </tr>                                                                                                          "& _
				"						  </table></td>                                                                                                          "& _
				"						</tr>                                                                                                                    "& _
				"						<tr>                                                                                                                     "& _
				"						  <td><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='Form_campname_bcoL'>             "& _
				"							<tr>                                                                                                             "& _
				"							  <td width='23'></td>                                                                                     	 "& _
				"							  <td width='263'>" & mobjXMLDoc.selectSingleNode("//Response/DATOS/DA_PROPIO").text  & " </td>                  "& _                                                                      
				"							</tr>                                                                                                            "& _
				"						  </table></td>                                                                                                          "& _
				"						  <td><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='Form_campname_bcoL'>             "& _
				"							<tr>                                                                                                             "& _
				"							  <td width='23'>&nbsp;</td>                                                                                     "& _
				"							  <td width='246'>" & mobjXMLDoc.selectSingleNode("//Response/DATOS/DA_TERCE").text  & "</td>                    "& _
				"							</tr>                                                                                                            "& _
				"						  </table></td>                                                                                                          "& _
				"						</tr>                                                                                                                    "& _
				"						<tr>                                                                                                                     "& _
				"						  <td><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='form_titulos_camp'>              "& _
				"							<tr>                                                                                                             "& _
				"							  <td width='23'>&nbsp;</td>                                                                                     "& _
				"							  <td width='263'>Lesiones Terceros:</td>                                                                       "& _
				"							</tr>                                                                                                            "& _
				"						  </table></td>                                                                                                          "& _
				"						  <td bgcolor='#EDEDF1'>&nbsp;</td>                                                                                      "& _
				"						</tr>                                                                                                                    "& _
				"						<tr>                                                                                                                     "& _
				"						  <td><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='Form_campname_bcoL'>             "& _
				"							<tr>                                                                                                             "& _
				"							  <td width='23'>&nbsp;</td>                                                                                     "& _
				"							  <td width='263'>" & mobjXMLDoc.selectSingleNode("//Response/DATOS/LE_TERCE").text  & "</td>                    "& _
				"							</tr>                                                                                                            "& _
				"						  </table></td>                                                                                                          "& _
				"						  <td>&nbsp;</td>                                                                                                        "& _
				"						</tr>                                                                                                                    "& _
				"						<tr>                                                                                                                     "& _
				"						  <td><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='form_titulos_camp'>              "& _
				"							<tr class='linksdos'>                                                                                            "& _
				"							  <td width='22'>&nbsp;</td>                                                                                     "& _
				"							  <td width='264'>Conductor</td>                                                                                "& _
				"							</tr>                                                                                                            "& _
				"						  </table></td>                                                                                                          "& _
				"						  <td><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='form_titulos_camp'>              "& _
				"							<tr>                                                                                                             "& _
				"							  <td width='25'>&nbsp;</td>                                                                                     "& _
				"							  <td width='263'>&nbsp;</td>                                                                                    "& _
				"							</tr>                                                                                                            "& _
				"						  </table></td>                                                                                                          "& _
				"						</tr>                                                                                                                    "& _
				"						<tr>                                                                                                                     "& _
				"						  <td><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='form_titulos_camp'>              "& _
				"							<tr>                                                                                                             "& _
				"							  <td width='22'>&nbsp;</td>                                                                                     "& _
				"							  <td width='264'>Tipo Conductor:</td>                                                                          "& _
				"							</tr>                                                                                                            "& _
				"						  </table></td>                                                                                                          "& _
				"						  <td><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='form_titulos_camp'>              "& _
				"							<tr>                                                                                                             "& _
				"							  <td width='25'>&nbsp;</td>                                                                                     "& _
				"							  <td width='263'>Documento:</td>                                                                               "& _
				"							</tr>                                                                                                            "& _
				"						  </table></td>                                                                                                          "& _
				"						</tr>                                                                                                                    "& _
				"						<tr>                                                                                                                     "& _
				"						  <td><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='Form_campname_bcoL'>             "& _
				"							<tr>                                                                                                             "& _
				"							  <td width='22'>&nbsp;</td>                                                                                     "& _
				"							  <td width='264'>" & mobjXMLDoc.selectSingleNode("//Response/DATOS/CONDUTIP").text  & "</td>                    "& _
				"							</tr>                                                                                                            "& _
				"						  </table></td>                                                                                                          "& _
				"						  <td><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='Form_campname_bcoL'>             "& _
				"							<tr>                                                                                                             "& _
				"							  <td width='23'>&nbsp;</td>                                                                                     "& _
				"							  <td width='246'>" & mobjXMLDoc.selectSingleNode("//Response/DATOS/CONDUDOCU").text  & "</td>                   "& _
				"							</tr>                                                                                                            "& _
				"						  </table></td>                                                                                                          "& _
				"						</tr>                                                                                                                    "& _
				"						<tr>                                                                                                                     "& _
				"						  <td><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='form_titulos_camp'>              "& _
				"							<tr>                                                                                                             "& _
				"							  <td width='22'>&nbsp;</td>                                                                                     "& _
				"							  <td width='264'>Apellidos:</td>                                                                                "& _
				"							</tr>                                                                                                            "& _
				"						  </table></td>                                                                                                          "& _
				"						  <td><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='form_titulos_camp'>              "& _
				"							<tr>                                                                                                             "& _
				"							  <td width='25'>&nbsp;</td>                                                                                     "& _
				"							  <td width='263'>Nombre:</td>                                                                                   "& _
				"							</tr>                                                                                                            "& _
				"						  </table></td>                                                                                                          "& _
				"						</tr>                                                                                                                    "& _
				"						<tr>                                                                                                                     "& _
				"						  <td><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='Form_campname_bcoL'>             "& _
				"							<tr>                                                                                                             "& _
				"							  <td width='22'>&nbsp;</td>                                                                                     "& _
				"							  <td width='264'>" & mobjXMLDoc.selectSingleNode("//Response/DATOS/CONDUAPE").text  & "</td>                    "& _
				"							</tr>                                                                                                            "& _
				"						  </table></td>                                                                                                          "& _
				"						  <td><table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='Form_campname_bcoL'>             "& _
				"							<tr>                                                                                                             "& _
				"							  <td width='23'>&nbsp;</td>                                                                                     "& _
				"							  <td width='246'>" & mobjXMLDoc.selectSingleNode("//Response/DATOS/CONDUNOM").text  & "</td>                    "& _
				"							</tr>                                                                                                            "& _
				"						  </table></td>                                                                                                          "& _
				"						</tr>                                                                                                                    "& _
				"					</table>                                                                                                                         "& _
				"				  </td>                                                                                                                                  "& _
				"				 </TR>                                                                                                                                   "& _
				"			</table>                                                                                        						 "& _
				"			<br> 																		 "
	Else	
		Adicionales = "<table width='100%' border='1' cellpadding='0' cellspacing='1' bordercolor='#FFFFFF'>"& _
			      "        <tr>                                                                         "& _
			      "          <td bordercolor='#dedfe7'>                                                 "& _
			      "			<table width='100%' border='0' cellpadding='2' cellspacing='1'>     "& _
			      "       		                                                                    "& _
			      "                    <tr class='Form_campname_bcoC'>                 			    "& _
			      "                    <td>No Hay Datos Adicionales del Siniestro.</td>                     "& _                    
			      "                    </tr>                     					    "& _
			      "                 </table>                     					    "& _
			      "          </td>                     					    	    "& _
			      "        </tr>                     					            "& _
			      "</table>                     					                    "
	End If
	DatosAdic = Adicionales
	
		
End function

function ConsultaPagos()
	
	mvarUSUARIO		= session("USER")
	mvarPERFIL_AS   = session("PERFIL_AS")
	mvarCLIENSEC_AS = session("CLIENSEC_AS")
	mvarPRODUCTO	= Request.Form("Producto")
	mvarSINIAN		= Request.Form("Sinian")
	mvarSININUM		= Request.Form("Sininum")
	mvarMSGEST		= Request.Form("MSGEST")  
	mvarFECCONT		= Request.Form("FecCont")  
	mvarPAGOCONT	= Request.Form("PagoCont")  
	
	mvarRequest = "<Request>					"& _
		      	"	<USUARIO>"  & mvarUSUARIO	& "</USUARIO>	"& _																																
				"	<NIVELAS>"	& mvarPERFIL_AS & "</NIVELAS>" & _
				"	<CLIENSECAS>" & mvarCLIENSEC_AS & "</CLIENSECAS>" & _
				"	<NIVEL1></NIVEL1>" & _
				"	<CLIENSEC1></CLIENSEC1>" & _
				"	<NIVEL2></NIVEL2>" & _
				"	<CLIENSEC2></CLIENSEC2>" & _
				"	<NIVEL3></NIVEL3>" & _
				"	<CLIENSEC3></CLIENSEC3>" & _				
		      	"	<PRODUCTO>" & mvarPRODUCTO   & "</PRODUCTO>  	"& _																																
		      	"	<SINIAN>"   & mvarSINIAN     & "</SINIAN>      	"& _																																
		      	"	<SININUM>"  & mvarSININUM    & "</SININUM>  	"& _																																
		      	"	<MSGEST>"   & mvarMSGEST     & "</MSGEST>      	"& _																																
		      	"	<FECCONT>"  & mvarFECCONT    & "</FECCONT>     	"& _																																
		      	"	<PAGOCONT>" & mvarPAGOCONT   & "</PAGOCONT>    	"& _																																
		      "</Request>						"
		
	Call cmdp_ExecuteTrn("lbaw_OVSiniConsPagos", _
						 "lbaw_OVSiniConsPagos.biz", _
						 mvarRequest, _
						 mvarResponse)
	
	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
	mobjXMLDoc.async = false
	                
        Call mobjXMLDoc.loadXML(mvarResponse)	
        
        
        If (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text = "true") then	   
		Set wvarXMLList = mobjXMLDoc.selectNodes("//REGS/REG")
		
        if (mobjXMLDoc.selectSingleNode("//Response/MSGEST").text = "TR") then	   
	        mvarFecCont  = mobjXMLDoc.selectSingleNode("//Response/FECCONT").text
	        mvarPagoCont = mobjXMLDoc.selectSingleNode("//Response/PAGOCONT").text	
	        mvarMSGEST   = mobjXMLDoc.selectSingleNode("//Response/MSGEST").text
		End If
	
		
	Dim HistoPag
	HistoPag       = Request("HistoPag")	
	PagActual      = Request("PagActual")
	
	If (PagActual = "")  Then
		PagActual = 0
	End If
	
		
		 
	If (HistoPag = "") Then
		HistoPag = mvarFecCont & ";" & mvarPagoCont & ";" & mvarMSGEST 
	Else
		HistoPag = HistoPag & "|" & mvarFecCont & ";" & mvarPagoCont & ";" & mvarMSGEST 
		PagActual = PagActual + 1	
		         
	End If          
        
	
	'Preparo los parametros para el boton anterior
	If (PagActual = 1) Then
		mvarFecContA  = ""
		mvarPagoContA = ""
		mvarMSGESTA   = ""
	Else
		If (PagActual > 1)Then
			HistoPag1     = split(HistoPag, "|")
			PagAnt        = split(HistoPag1(PagActual-2), ";")		
			mvarFecContA  = PagAnt(0)
			mvarPagoContA = PagAnt(1)
			mvarMSGESTA   = PagAnt(2)
		End If
	End If
	 
	Adicionales =	"<table width='100%' border='1' cellpadding='0' cellspacing='1' bordercolor='#FFFFFF'>"& _
			"        <tr>                                                                         "& _
			"          <td bordercolor='#dedfe7'>                                                 "& _
			"	      <table width='100%' border='0' cellpadding='2' cellspacing='1'>         "& _
			"                    <tr class='form_titulos02'>                 		      "& _ 
			"	                    <td> N&uacute;mero de Orden de Pago </td>                 "& _                      
			"	                    <td> Beneficiario / Pagador </td>                         "& _                       
			"	                    <td> Estado </td>                                         "& _   
			"	                    <td> Fecha Estado </td>                                   "& _
			"	                    <td> Moneda </td>                                         "& _   
			"	                    <td> Importe </td>                                   "& _
			"                </tr>                                                                "
	'jc 10/2009 se agrego moneda e importe	
		
		For wvarCounter = 0 to wvarXMLList.length-1 
			mvarEstado = wvarXMLList.Item(wvarCounter).selectSingleNode("EST").text
		
			SELECT Case mvarEstado
		          Case "A"
		              mvarEstado = "Alta OP"           
		          Case "T"
		              mvarEstado = "OP en Tesorería"
		          Case "E"
		              mvarEstado = "Valor Enviado a Caja"
		          Case "R"
		              mvarEstado = "Valor Retirado"
'jc 10/2009 nuevos estados y nuevos campos
				  Case "N"
		              mvarEstado = "OP Anulada"
				  Case "S"
		              mvarEstado = "OP Devuelta"
'jc fin
				  Case Else	              
		              mvarEstado = mvarEstado
		        End Select			
			
			Adicionales = Adicionales & "<tr bgcolor='#FFFFFF' class='Form_campname_bcoL'> "& _
					"			<td>"&wvarXMLList.Item(wvarCounter).selectSingleNode("NROP").text	&"</td>  "& _
					"			<td>"&wvarXMLList.Item(wvarCounter).selectSingleNode("BENEF").text	&"</td>  "& _
					"			<td>"&mvarEstado							&"</td>  "& _
					"			<td>"&wvarXMLList.Item(wvarCounter).selectSingleNode("FEC").text	&"</td>  "& _
					"			<td>"&wvarXMLList.Item(wvarCounter).selectSingleNode("MON").text	&"</td>  "& _
					"			<td align='right'>"&wvarXMLList.Item(wvarCounter).selectSingleNode("IMP").text	&"</td>  "& _
					"	       	</tr> 											 "
		Next
							
		Adicionales = Adicionales & "</table>                                                 			      "& _
					"                                                                                     "& _
					"	    </td>                                                                     "& _
					"	</tr>                                                                         "& _
					"</table><BR>" 
	       
	        If (mobjXMLDoc.selectSingleNode("//Response/MSGEST").text  = "TR") Then
		
			Adicionales = Adicionales & "<table width='100%'> 					      "& _
				"  <tr>                                                                               "& _
				"    <td align='center' class='linksubir' width='50%' >							      "
					
					If (PagActual > 0) Then			
						'Adicionales = Adicionales & "<a href=Javascript:MostrarConsultaPagosANT('"& mvarFecContA & "','" & mvarPagoContA & "','" & mvarMSGESTA & "','" & PagActual & "','" & FIN & "','" & HistoPag & "');><img type='image' alt='Pagina Anterior' src='/Oficina_Virtual/html/images/boton_volver.gif' style='cursor:hand'></img> Anterior </a> "					
						Adicionales = Adicionales & "<a href='#Pagos' class='linksubir' Onclick=Javascript:MostrarConsultaPagosANT('"& mvarFecContA & "','" & mvarPagoContA & "','" & mvarMSGESTA & "','" & PagActual & "','" & FIN & "','" & HistoPag & "');><img type='image' alt='Pagina Anterior' src='/Oficina_Virtual/html/images/boton_volver.gif' style='cursor:hand' border='0'></img> Anterior </a> "					
					End If
				
				'Adicionales = Adicionales & "</td> <td align='center' class='linksubir' width='50%' >	<a href=Javascript:MostrarConsultaPagosSIG('" & HistoPag & "','" & PagActual & "','" & mvarFecCont & "','" & mvarPagoCont & "','" & mvarMSGEST & "','" & FIN & "');><img type='image' alt='Pagina Siguiente' src='/Oficina_Virtual/html/images/boton_continuar.gif' style='cursor:hand'></img> Siguiente </a> </td>   "& _					
				Adicionales = Adicionales & "</td> <td align='center' class='linksubir' width='50%' >	<a href='#Pagos' class='linksubir' Onclick=Javascript:MostrarConsultaPagosSIG('" & HistoPag & "','" & PagActual & "','" & mvarFecCont & "','" & mvarPagoCont & "','" & mvarMSGEST & "','" & FIN & "');><img type='image' alt='Pagina Siguiente' src='/Oficina_Virtual/html/images/boton_continuar.gif' border='0' style='cursor:hand'></img> Siguiente </a> </td>   "& _					
						"  </tr>                                                                              "& _
						"</table> 									      " &_
						"<BR>	            								      "
		End If
	     
		
		
		If (mobjXMLDoc.selectSingleNode("//Response/MSGEST").text  = "OK") AND (PagActual > 0) Then		
			'Aclarar de que se termino de ver hasta la ultima pagina. 
			'Por lo tanto debo armar el boton Siguiente con lo guardado en HIST.
				
			FIN     = "S"
				
			Adicionales = Adicionales & "<table width='100%'> 					      "& _
				"  <tr>                                                                               "& _
				"    <td align='center' class='linksubir' width='50%' >							      "
					
					If (PagActual > 0) Then			
						If (PagActual = 1 ) Then
							'Adicionales = Adicionales & "<a href=Javascript:MostrarConsultaPagosANT('','','" & mvarMSGESTA & "','" & PagActual & "','" & FIN & "','" & HistoPag & "');><img type='image' alt='Pagina Anterior' src='/Oficina_Virtual/html/images/boton_volver.gif' style='cursor:hand'></img> Anterior </a> "					
							Adicionales = Adicionales & "<a href='#Pagos' class='linksubir' Onclick=Javascript:MostrarConsultaPagosANT('','','" & mvarMSGESTA & "','" & PagActual & "','" & FIN & "','" & HistoPag & "');><img type='image' alt='Pagina Anterior' src='/Oficina_Virtual/html/images/boton_volver.gif' style='cursor:hand' border='0'></img> Anterior </a> "					
						Else	
							'Adicionales = Adicionales & "<a href=Javascript:MostrarConsultaPagosANT('"& mvarFecContA & "','" & mvarPagoContA & "','" & mvarMSGESTA & "','" & PagActual & "','" & FIN & "','" & HistoPag & "');><img type='image' alt='Pagina Anterior' src='/Oficina_Virtual/html/images/boton_volver.gif' style='cursor:hand'></img> Anterior</a> "					
							Adicionales = Adicionales & "<a href='#Pagos' class='linksubir' Onclick=Javascript:MostrarConsultaPagosANT('"& mvarFecContA & "','" & mvarPagoContA & "','" & mvarMSGESTA & "','" & PagActual & "','" & FIN & "','" & HistoPag & "');><img type='image' alt='Pagina Anterior' src='/Oficina_Virtual/html/images/boton_volver.gif' style='cursor:hand' border='0'></img> Anterior</a> "					
						End If
					End If
				
				Adicionales = Adicionales & "</td> <td align='center' class='linksubir' width='50%' ></td>   "& _					
						"  </tr>                                                                              "& _
						"</table> 									      " &_
						"<BR>	            								      "
		End If
		
	

	Else	
		Adicionales = "<table width='100%' border='1' cellpadding='0' cellspacing='1' bordercolor='#FFFFFF'>"& _
			      "        <tr>                                                                         "& _
			      "          <td bordercolor='#dedfe7'>                                                 "& _
			      "			<table width='100%' border='0' cellpadding='2' cellspacing='1'>     "& _
			      "       		                                                                    "& _
			      "                    <tr class='Form_campname_bcoC'>                 			    "& _
			      "                    <td>No se encontraron Pagos.</td>                     "& _                    
			      "                    </tr>                     					    "& _
			      "                 </table>                     					    "& _
			      "          </td>                     					    	    "& _
			      "        </tr>                     					            "& _
			      "</table>                     					                    "
	End If
		
	ConsultaPagos = Adicionales
		
End Function

Function DatosWorkflow()

	mvarUSUARIO  = session("USER")
	mvarPRODUCTO = Request.Form("Producto")
	mvarSINIAN   = Request.Form("Sinian")
	mvarSININUM  = Request.Form("Sininum")
	mvarGESTICLA = Request.Form("GESTICLA")
	mvarGESTISEC = Request.Form("GESTISEC")

	mvarRequest = "<Request>" & _
										"<DEFINICION>workflows.xml</DEFINICION>" & _
										"<AplicarXSL>workflows.xsl</AplicarXSL>" & _
										"<CIAASCOD>0001</CIAASCOD>" & _
										"<USUARCOD>"   & mvarUSUARIO  & "</USUARCOD>" & _
										"<ESTADO>OK</ESTADO>" & _
										"<ERROR/>" & _
										"<CLIENSECAS>0</CLIENSECAS>" & _
										"<NIVELCLAS/>" & _
										"<NIVELCLA1/>" & _
										"<CLIENSEC1>0</CLIENSEC1>" & _
										"<NIVELCLA2/>" & _
										"<CLIENSEC2>0</CLIENSEC2>" & _
										"<NIVELCLA3/>" & _
										"<CLIENSEC3>0</CLIENSEC3>" & _
										"<RAMOPCOD>"   & mvarPRODUCTO  & "</RAMOPCOD>" & _
										"<SINIEANN>"    & mvarSINIAN   & "</SINIEANN>" & _
										"<SINIENUM>"   & mvarSININUM  & "</SINIENUM>" & _
										"<GESTICLA>" & mvarGESTICLA & "</GESTICLA>" & _
										"<GESTISEC>" & mvarGESTISEC & "</GESTISEC>" & _
									"</Request>"
				
				Call cmdp_ExecuteTrn("lbaw_GetConsultaMQGestion", _
						"lbaw_GetConsultaMQGestion.biz", _
						mvarRequest, _
						mvarResponse)
						
					
						Dim xmlDOM
						Dim xslDOM
						
						Set xmlDOM = CreateObject("MSXML2.DomDocument")
						Set xslDOM = CreateObject("MSXML2.DomDocument")
						
						Call xmlDOM.loadXML(mvarResponse)
						
						'DatosWorkflow =  "<tr><td>" & xmlDOM.xml & "</td></tr>" 
						Call xslDOM.load (Server.MapPath("workflow.xsl"))
						
						DatosWorkflow = xmlDOM.transformNode (xslDOM)
						
End Function

Function SubCoberturas()

	mvarUSUARIO  = Request("Usuario")
	mvarPRODUCTO = Request("Producto")
	mvarSINIAN   = Request("Sinian")
	mvarSININUM  = Request("Sininum")
	mvarGESCLA   = Request("GesCla")
	mvarGESSEC   = Request("GesSec")

	mvarRequest = "<Request>		  	  		       	" & _
		      "		<USUARIO> " & mvarUSUARIO   & "</USUARIO>  	" & _
		      "		<PRODUCTO>" & mvarPRODUCTO  & "</PRODUCTO>	" & _
		      "		<SINIAN>  " & mvarSINIAN    & "</SINIAN>    	" & _
		      "		<SININUM> " & mvarSININUM   & "</SININUM>  	" & _
		      "		<GESCLA>  " & mvarGESCLA    & "</GESCLA>  	" & _
		      "		<GESSEC>  " & mvarGESSEC    & "</GESSEC>  	" & _
		      "</Request>						"

	Call cmdp_ExecuteTrn("lbaw_OVSiniSubcober", _
						 "lbaw_OVSiniSubcober.biz", _
						 mvarRequest, _
						 mvarResponse)
						 
	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
	mobjXMLDoc.async = false
	Call mobjXMLDoc.loadXML(mvarResponse)	

	
	'SubCoberturas = SubCoberturas & "<html><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'><link rel='stylesheet' type='text/css' href='/Oficina_virtual/Html/Css/Css_gral.css'></head>"
	
	SubCoberturas = SubCoberturas & "<html><head><style type='text/css'>BODY.Scroll{SCROLLBAR-FACE-COLOR: white;SCROLLBAR-HIGHLIGHT-COLOR: #BFBFBF;SCROLLBAR-SHADOW-COLOR: #BFBFBF;SCROLLBAR-3DLIGHT-COLOR: white;SCROLLBAR-ARROW-COLOR: #BFBFBF;SCROLLBAR-TRACK-COLOR: white;SCROLLBAR-DARKSHADOW-COLOR: white}</style></head>"
	SubCoberturas = SubCoberturas & "<BODY>"
	SubCoberturas = SubCoberturas & "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='1' bordercolor='#FFFFFF'>"

	if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text) = "true" then
		SubCoberturas = SubCoberturas & "<tr><td>SI se encontraron Datos</td></tr>" 
		
		Set wvarXMLList = mobjXMLDoc.selectNodes("//REGS/REG")		
		SubCoberturas = SubCoberturas & "<tr Style='font-weight: bold; color: 003366; text-decoration: none; background-color: #9CBACE;'>" & _
											"<td align= 'center' width='38%'><font face='verdana,arial,helvetica' size='1'><b>SubCobertura</b></font></td>" & _
											"<td align= 'center' width='62%'><font face='verdana,arial,helvetica' size='1'><b>Bien Afectado</b></font></td>" & _
										"</tr></table>"
		
		SubCoberturas = SubCoberturas & "<div id=scrolldiv STYLE='overflow-y:scroll;height=65px;' width='100%'>"
		SubCoberturas = SubCoberturas & "<table width='230' border='0' align='center' cellpadding='0' cellspacing='1' bordercolor='#FFFFFF'>"

		mvarColor = 0  
		For wvarCounter = 0 to wvarXMLList.length - 1
			mvarColor = 1 - mvarColor
			if mvarColor = 1 then 
				SubCoberturas = SubCoberturas	& "<tr bgcolor='#FFFFFF'>"
			Else
				SubCoberturas = SubCoberturas	& "<tr bgcolor='#EDEDF1'>"
			End If
			
			SubCoberturas = SubCoberturas	& 	"<td width='43%'>" & _
													"<table width='100%' height='20' border='0' cellpadding='0' cellspacing='0' class='Form_campname_bco'>" & _
														"<tr>" & _
															"<td class='Form_campname_bcoL'><font face='verdana,arial,helvetica' size='1'>" & wvarXMLList.Item(wvarCounter).selectSingleNode("SUBCOBER").text & "</font></td>" & _
														"</tr>" & _
													"</table>" & _
												"</td>" & _
												"<td width='57%'>" & _
													"<table width='100%' height='20' border='0' cellpadding='0' cellspacing='0'>" & _
														"<tr>" & _
															"<td><font face='verdana,arial,helvetica' size='1'>" & wvarXMLList.Item(wvarCounter).selectSingleNode("BIENDES").text & "</font></td>" & _
														"</tr>" & _
													"</table>" & _
												"</td>" & _
											  "</tr>"
		Next
	Else
		SubCoberturas = SubCoberturas & "<tr><td>No se encontraron Datos</td></tr>" 
	End if
	'SubCoberturas = SubCoberturas & "</table></td></tr></table></body></html>"
	SubCoberturas = SubCoberturas & "</table></td></tr></table></div></BODY></html>"
End Function

%>


