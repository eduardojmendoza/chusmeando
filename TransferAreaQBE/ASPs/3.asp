<script language="JavaScript">

function ResultadoBusquedaOnLoad()
{
	var resultTabla;
	ImaActual=1;
	ImaDir=2;
	resultTabla = xmlDoc.transformNode(xslDoc.XMLDocument);
	divTabla.innerHTML = resultTabla;
	//Si no viene paginado tengo que ordenar por nombre la primera vez
	if (document.frmMain.PAGINAR.value == 'OK')
	{
		sort('CLIDES');
		cambiarImagen1('2');
	}
}

//Muestra la imagen identificando por que columna se ordeno
function ImgOrden()
{
	var orden = '<%=request.form("orden")%>';
	//Muestro la img en esa columna
	if (orden != '')
	{

		if (orden.substr(0,1) == '0'){
			orden = orden.substr(1,1);
		}	
		document.getElementById("imgH" + orden).style.height=12;	
		document.getElementById("imgH" + orden).style.width=13;
		document.getElementById("imgH" + orden).src="/Oficina_Virtual/Html/Images/arrowbco_DW.gif";
	}
}

function AReciboReclamado(pRAMOPCOD, pPOLIZANN, pPOLIZSEC, pCERTIPOL, pCERTIANN, pCERTISEC, pSUPLENUM, pCLIENDES, pTIPOSEG, pGRUPOAFIN)
{
	var frm = document.getElementsByTagName("FORM")[0];
	if(frm.name == "formulario" || frm.name == "frmMain")
	{	
		frm.action ="/Productores/prototipo/ReciboReclamo.asp";
	

			frm.RAMOPCOD.value = pRAMOPCOD;
			frm.POLIZANN.value = pPOLIZANN;
			frm.POLIZSEC.value = pPOLIZSEC;
			frm.CERTIPOL.value = pCERTIPOL;
			frm.CERTIANN.value = pCERTIANN;
			frm.CERTISEC.value = pCERTISEC;
			frm.CLIENDES.value = pCLIENDES;
			frm.TIPOSEG.value = pTIPOSEG;
			if (pGRUPOAFIN)
				frm.GRUPOAFIN.value = pGRUPOAFIN;

		frm.SUPLENUM.value = pSUPLENUM;
		frm.submit();
		return;
		
	};
}

function sortMQ(idorden,nodo,nroConsulta,imagen)
{
	if (nroConsulta=='0000')
	{
		sort(nodo);
		cambiarImagen1(imagen);
	}
	else
	{
		frmMain.orden.value = idorden;
		frmMain.productoc.value = '';
		frmMain.polizac.value = '';
		frmMain.submit();
	}
}
</script>
<form name="frmMain" method="POST" action="/Oficina_Virtual/ASP/Siniestros/CSiniestros/Res_Busq_Sini.asp">
<table width="660" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td valign="top">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
		 	<tr> 
			    <td height="27">
			   	<%	dim texto
			   		   		
				   	titulo= "Consulta de siniestros"
					texto = "csiniestros.htm"
				%>
				<!-- #include virtual="/Oficina_Virtual/Library/encabezado.asp"-->
				<!-- #include virtual="/Oficina_Virtual/Library/incUsuario.asp"-->
				</td>
			</tr>
			<tr> 
				<td height="20"></td>
			</tr>
			<tr> 
				<td bordercolor="#dedfe7">
					<%
					dim mvarConsulta
					dim mvarContinuar
					dim mvarNivelas
					dim mvarCliensecas
					dim mvarPoliza1
					dim mvarPoliza2
					dim mvarSiniestro
					dim mvarDocumDats
					dim mvarDocumTips
					dim mvarClienApes
					dim mvarPatente
					dim mvarProducto
					dim mvarPaginar
					dim mvarRamo

					mvarContinuar	= Request.form("CONTINUAR")
					
					mvarNivelas		= session("PERFIL_AS")
					mvarCliensecas	= session("CLIENSEC_AS")
					mvarPoliza1		= replace(Request.Form("POLIZA1"),"-","")
					mvarPoliza2		= replace(Request.Form("POLIZA2"),"-","")
					mvarSiniestro	= ucase(replace(Request.Form("SINIESTRO"),"-",""))
					mvarDocumDats	= Request.Form("DOCUMDATS")
					mvarDocumTips	= Request.Form("DOCUMTIPS")
					mvarClienApes	= UCase(Request.Form("CLIENAPES"))
					mvarPatente		= ucase(Request.Form("PATENTE"))
					mvarPaginar		= Request.Form("PAGINAR")
					mvarRamo		= Request.Form("RAMO")
					
					'Verifico de donde saco el producto
					if mvarPoliza1 <> "" then
						mvarProducto = mid(mvarPoliza1,1,4)
					elseif mvarSiniestro <> "" then
						mvarProducto = mid(mvarSiniestro,1,4)
					end if
					
					mvarRequest = "<Request><USUARIO>"&session("USER")&"</USUARIO><NIVELAS>"&mvarNivelas&"</NIVELAS><CLIENSECAS>"&mvarCliensecas&"</CLIENSECAS><DOCUMTIP>"&mvarDocumTips&"</DOCUMTIP><DOCUMNRO>"&mvarDocumDats&"</DOCUMNRO><CLIENDES>"&mvarClienApes&"</CLIENDES><PRODUCTO>"&mvarProducto&"</PRODUCTO><POLIZA>"&mid(mvarPoliza1,5,len(mvarPoliza1))&"</POLIZA><CERTI>"&mvarPoliza2&"</CERTI><PATENTE>"&mvarPatente&"</PATENTE><SINIAN>"&mid(mvarSiniestro,5,2)&"</SINIAN><SININUM>"&mid(mvarSiniestro,7,6)&"</SININUM><MSGEST>"&mvarPaginar&"</MSGEST><CONTINUAR>"&mvarContinuar&"</CONTINUAR></Request>"

					Call cmdp_ExecuteTrn("lbaw_OVSiniConsulta", _
					"lbaw_OVSiniConsulta.biz", _
					mvarRequest, _
					mvarResponse)
					 					 			
					Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
					mobjXMLDoc.async = false
					mobjXMLDoc.loadXML(mvarResponse)
					'Tomo los valores de salida del comp
					if not mobjXMLDoc.selectSingleNode("/Response/CONTINUAR") is nothing then
						mvarContinuar	= mobjXMLDoc.selectSingleNode("/Response/CONTINUAR").text
					end if
					if not mobjXMLDoc.selectSingleNode("/Response/MSGEST") is nothing then
						mvarPaginar		= mobjXMLDoc.selectSingleNode("/Response/MSGEST").text
					end if
					
					
					Response.write "<xml id='xmlDoc'>"&mvarResponse&"</xml>"
					%>
					<xml id='xslDoc' src='/Oficina_Virtual/ASP/Siniestros/CSiniestros/Resultado_Busq_Sini.xsl'></xml>
					<div id="divTabla" name="divTabla" style="WIDTH: 100%">
					</div>
					<BR>
				</td>
			</tr>
	  </table>	 
	  </td>
	</tr>
	<tr>
		<td>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center" valign="middle">

			<%if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text) = "true" then %>
			<a href="javascript:window.history.back();" class="linksubir"><img border="0" src="/Oficina_Virtual/html/images/boton_volver.gif" align="absmiddle">&nbsp;Volver</a>
			&nbsp;&nbsp;<a href="javascript:SeleccionarPoliza()" class="linksubir"><img src="/Oficina_Virtual/Html/Images/btn_datospza.gif" border="0">&nbsp;Datos Póliza</a>
			&nbsp;&nbsp;<a href="javascript:DatosClientes()" class="linksubir"><img src="/Oficina_Virtual/Html/Images/boton_buscarclie.gif" border="0">&nbsp;Datos Cliente</a>
			&nbsp;&nbsp;<a href="javascript:SituacionCobranza()" class="linksubir"><img src="/Oficina_Virtual/Html/Images/btn_exigible.gif" border="0">&nbsp;Situación Cobranzas</a>
			&nbsp;&nbsp;<a href="javascript:SeleccionarEndoso();" class="linksubir"><img src="/Oficina_Virtual/Html/Images/btn_siniestro.gif" border="0">&nbsp;Endosos</a>
			&nbsp;&nbsp;<a href="Javascript:AListadoRiesgos()" class="linksubir"><img src="/Oficina_Virtual/Html/Images/btn_riesgo.gif" border="0">&nbsp;Riesgo</a>
			<%else%>
			<a href="javascript:window.history.back();" class="linksubir"><img border="0" src="/Oficina_Virtual/html/images/boton_volver.gif" align="absmiddle">&nbsp;Volver</a>
			<%end if%>
			</td>
		</tr>          
		</table><tr><td>&nbsp;</td></tr>
		</td>
	</tr>
</table>

<input type="hidden" name="POLIZA1" value="<%=Request.form("POLIZA1")%>">
<input type="hidden" name="POLIZA2" value="<%=Request.form("POLIZA2")%>">
<input type="hidden" name="SINIESTRO" value="<%=Request.form("SINIESTRO")%>">
<input type="hidden" name="SINIAN" value="">
<input type="hidden" name="SININUM" value="">
<input type="hidden" name="DOCUMDATS" value="<%=Request.form("DOCUMDATS")%>">
<input type="hidden" name="DOCUMTIPS" value="<%=Request.form("DOCUMTIPS")%>">
<input type="hidden" name="CLIENAPES" value="<%=Request.form("CLIENAPES")%>">
<input type="hidden" name="PATENTE" value="<%=Request.form("PATENTE")%>">
<input type="hidden" name="NIVELAS" value="<%=Request.form("NIVELAS")%>">
<input type="hidden" name="CLIENSECAS" value="<%=Request.form("CLIENSECAS")%>">
<input type="hidden" name="NIVEL1" value="<%=Request.form("NIVEL1")%>">
<input type="hidden" name="CLIENSEC1" value="<%=Request.Form("CLIENSEC1")%>">
<input type="hidden" name="NIVEL2" value="<%=Request.Form("NIVEL2")%>">
<input type="hidden" name="CLIENSEC2" value="<%=Request.Form("CLIENSEC2")%>">
<input type="hidden" name="NIVEL3" value="<%=Request.Form("NIVEL3")%>">
<input type="hidden" name="CLIENSEC3" value="<%=Request.Form("CLIENSEC3")%>">
<input type="hidden" name="CONTINUAR" value="<%=mvarContinuar%>">
<input type="hidden" name="PAGINAR" value="<%=mvarPaginar%>">
<input type="hidden" name="ESTADO" value="<%=mvarEstado%>">
<input type="hidden" name="PROD" value="<%=mvarProducto%>">
<input type="hidden" name="POLIZA" value="<%=mid(mvarPoliza1,5,len(mvarPoliza1))%>">
<input type="hidden" name="CERTI" value="<%=mvarPoliza2%>">
<input type="hidden" name="CERPOL" value="<%=mid(Request.form("POLIZA2"),1,4)%>">
<input type="hidden" name="CERANN" value="<%=mid(Request.form("POLIZA2"),5,4)%>">
<input type="hidden" name="CERSEC" value="<%=mid(Request.form("POLIZA2"),9,6)%>">
<input type="hidden" name="RAMO" value="<%=mvarRamo%>">
<input type="hidden" name="CLIENDES" value="<%=owniif(mvarClienApes<>"", mvarClienApes, Request.form("CLIENDES"))%>">
</form>
<%set mobjXMLDoc = nothing%>
