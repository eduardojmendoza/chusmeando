<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format name="european" decimal-separator="."/>

<xsl:template match='/'>
Apellido / Razón Social&#009;&#009;Nro. Póliza&#009;&#009;Endoso&#009;&#009;Estado de Póliza&#009;&#009;Nro. de Recibo&#009;&#009;Moneda&#009;&#009;Importe&#009;&#009;Fecha de Cobro&#009;&#009;<!--Tipo de Cuenta&#009;&#009;-->Canal&#009;&#009;Judicial&#13;&#10;<xsl:for-each select="//CANAL">
	<xsl:sort order="ascending" select="FECSITUE" data-type="text" />
	<xsl:value-of select="CLIENDES"
		/>&#009;&#009;<xsl:value-of select="RAMOPCOD"
		/>-<xsl:value-of select="POLIZANN"
		/>-<xsl:value-of select="POLIZSEC"
		/><xsl:if test="RAMO='M'">/<xsl:value-of select="CERTIPOL"
		/><xsl:value-of select="CERTIANN"
		/><xsl:value-of select="CERTISEC"/>
		</xsl:if>&#009;&#009;<xsl:value-of select="format-number(SUPLENUM,'0')"
		/>&#009;&#009;<xsl:value-of select="SITUCPOL"
		/>&#009;&#009;<xsl:value-of select="RECIBANN"
		/><xsl:value-of select="RECIBTIP"
		/><xsl:value-of select="RECIBSEC"
		/>&#009;&#009;<xsl:value-of select="MONENDES"
		/>&#009;&#009;<xsl:value-of select="format-number(IMPORTE,'#.00')"
		/>&#009;&#009;<xsl:value-of select="FECSITUE"
		/><!--&#009;&#009;<xsl:value-of select="COBRODESC"
		/>-->&#009;&#009;<xsl:value-of select="COBRODABC"
		/>&#009;&#009;<xsl:value-of select="ORDENJUDICIAL"
		/>&#13;&#10;</xsl:for-each>
</xsl:template>

</xsl:stylesheet>
