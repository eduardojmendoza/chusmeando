<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2007. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: 1000_PerfilUsuario.xsl

Fecha de Creación: 17/03/2008

Desarrollador: Matias Coaker

Descripción: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos 
			1000_PerfilUsuario.xml
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html" encoding="iso-8859-1"/>
	<xsl:template match="/">
		<USER_NIVEL>
			<xsl:value-of select="//USUARTIP"/>
		</USER_NIVEL>
		<USUARNOM>
			<xsl:value-of select="//USUARNOM"/>
		</USUARNOM>
		<CLIENTIP>
			<xsl:value-of select="//CLIENTIP"/>
		</CLIENTIP>
		<BANCOCOD>
			<xsl:value-of select="//BANCOCOD"/>
		</BANCOCOD>
		<BANCONOM>
			<xsl:value-of select="//BANCONOM"/>
		</BANCONOM>
		<GRUPOHSBC>
			<xsl:value-of select="//GRUPOHSBC"/>
		</GRUPOHSBC>		
		<ACCESO>
			<xsl:for-each select="//OPCIONMENU">
				<MMENU>
					<xsl:value-of select="OPCIONNUM"/>
				</MMENU>
			</xsl:for-each>
		</ACCESO>
		<EMISIONES>
			<xsl:for-each select="//EMISION">
				<EMISION>
					<RAMOPCOD>
						<xsl:value-of select="RAMOPCOD"/>
					</RAMOPCOD>
				</EMISION>
			</xsl:for-each>
		</EMISIONES>
	</xsl:template>
</xsl:stylesheet>
