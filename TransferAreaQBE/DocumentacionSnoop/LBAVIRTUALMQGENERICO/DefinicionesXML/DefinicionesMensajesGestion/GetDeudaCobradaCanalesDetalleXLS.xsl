<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format name="european" decimal-separator="," grouping-separator="."/>

	<xsl:template name="replace">
		<xsl:param name="str"/>
		<xsl:param name="source"/>
		<xsl:param name="target"/>
		<xsl:choose>
			<xsl:when test="contains($str,$source)">
				<xsl:call-template name="replace">
					<xsl:with-param name="str" select="concat(substring-before($str,$source), $target, substring-after($str,$source))"/>
					<xsl:with-param name="source" select="$source"/>
					<xsl:with-param name="target" select="$target"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$str"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

<xsl:template match="/">

Apellido / Razón Social,Nro. Póliza,Endoso,Estado de Póliza,Nro. de Recibo,Moneda,Importe,Moneda,Importe,Fecha de Cobro,<!--Tipo de Cuenta,--> Canal,Judicial&#13;&#10;<xsl:for-each select="//CANAL">
			<xsl:sort order="ascending" select="FECSITUE" data-type="text"/>
			<xsl:call-template name="replace">
				<xsl:with-param name="str" select="CLIENDES"/>
				<xsl:with-param name="source">,</xsl:with-param>
				<xsl:with-param name="target">;</xsl:with-param>
			</xsl:call-template>,<xsl:value-of select="RAMOPCOD"/>-<xsl:value-of select="POLIZANN"/>-<xsl:value-of select="POLIZSEC"/>
			<xsl:if test="RAMO='M'">/<xsl:value-of select="CERTIPOL"/>
				<xsl:value-of select="CERTIANN"/>
				<xsl:value-of select="CERTISEC"/>
			</xsl:if>,<xsl:value-of select="format-number(SUPLENUM,'0')"/>,<xsl:value-of select="SITUCPOL"/>,<xsl:value-of select="RECIBANN"/>
			<xsl:value-of select="RECIBTIP"/>
			<xsl:value-of select="RECIBSEC"/>,<xsl:choose>
				<xsl:when test="MONENDES='$'"><xsl:value-of select="MONENDES"/>,<xsl:value-of select="format-number(IMPORTE,'#.00')"/>,,,</xsl:when>
				<xsl:when test="MONENDES='U$S'">,,<xsl:value-of select="MONENDES"/>,<xsl:value-of select="format-number(IMPORTE,'#.00')"/>,</xsl:when>
				<xsl:otherwise><xsl:value-of select="MONENDES"/>,<xsl:value-of select="format-number(IMPORTE,'#.00')"/>,,,</xsl:otherwise>
				</xsl:choose><xsl:value-of select="FECSITUE"/><!--,<xsl:value-of select="COBRODESC"/>-->,<xsl:value-of select="COBRODABC"/>,<xsl:value-of select="ORDENJUDICIAL"/>&#13;&#10;</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
