<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/">
	
		<xsl:for-each select="//REGS/REG">
		
			<xsl:if test="ESTADO='N'">
			
				<tr>
				
					<xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="position() mod 2=0">form_campname_bcoR</xsl:when>
							<xsl:otherwise>form_campname_colorR</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					
					<td align="center">
						<xsl:value-of select="./RECNUM"/>
					</td>
					
				</tr>
				
			</xsl:if>
			
		</xsl:for-each>
		
	</xsl:template>
	
</xsl:stylesheet>
