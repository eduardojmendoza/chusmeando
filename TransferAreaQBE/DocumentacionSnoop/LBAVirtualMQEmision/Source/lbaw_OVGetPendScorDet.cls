VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVGetPendScorDet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQEmision.lbaw_OVGetPendScorDet"
Const mcteOpID              As String = "1521"

'Parametros XML de Entrada
Const mcteParam_Usuario     As String = "//USUARIO"
Const mcteParam_FechaDesde  As String = "//FECHADESDE"
Const mcteParam_FechaHasta  As String = "//FECHAHASTA"
Const mcteParam_RAMOPCOD    As String = "//RAMOPCOD"
Const mcteParam_POLIZANN    As String = "//POLIZANN"
Const mcteParam_POLIZSEC    As String = "//POLIZSEC"
Const mcteParam_CERTIPOL    As String = "//CERTIPOL"
Const mcteParam_CERTIANN    As String = "//CERTIANN"
Const mcteParam_CERTISEC    As String = "//CERTISEC"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarFechaDesde      As String
    Dim wvarFechaHasta      As String
    Dim wvarFDesdeVal       As String
    Dim wvarFHastaVal       As String
    Dim wvarRamo            As String
    Dim wvarPolizaAnn       As String
    Dim wvarPolizaSec       As String
    Dim wvarCertiPol        As String
    Dim wvarCertiAnn        As String
    Dim wvarCertiSec        As String
    '
    Dim wvarPos             As Long
    Dim wvarParseString     As String
    Dim wvarStringLen       As Long
    Dim wvarCantLin         As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los par�metros que llegan desde la p�gina
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Par�metros:
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarFechaDesde = Format(.selectSingleNode(mcteParam_FechaDesde).Text, "YYYYMMDD")
        wvarFechaHasta = Format(.selectSingleNode(mcteParam_FechaHasta).Text, "YYYYMMDD")
        wvarRamo = Left(.selectSingleNode(mcteParam_RAMOPCOD).Text & Space(4), 4)
        wvarPolizaAnn = Right("00" & .selectSingleNode(mcteParam_POLIZANN).Text, 2)
        wvarPolizaSec = Right("000000" & .selectSingleNode(mcteParam_POLIZSEC).Text, 6)
        wvarCertiPol = Right("0000" & .selectSingleNode(mcteParam_CERTIPOL).Text, 4)
        wvarCertiAnn = Right("0000" & .selectSingleNode(mcteParam_CERTIANN).Text, 4)
        wvarCertiSec = Right("000000" & .selectSingleNode(mcteParam_CERTISEC).Text, 6)
    End With
    '
    'Valido las fechas recibidas, en formato mm/dd/aaaa
    wvarFDesdeVal = Mid(wvarFechaDesde, 5, 2) & "/" & Mid(wvarFechaDesde, 7, 2) & "/" & Mid(wvarFechaDesde, 1, 4)
    If Not IsDate(wvarFDesdeVal) Then
        Err.Raise -1, mcteClassName & "." & wcteFnName, "Par�metro 'Fecha Desde' No V�lido."
    End If
    wvarFHastaVal = Mid(wvarFechaHasta, 5, 2) & "/" & Mid(wvarFechaHasta, 7, 2) & "/" & Mid(wvarFechaHasta, 1, 4)
    If Not IsDate(wvarFHastaVal) Then
        Err.Raise -1, mcteClassName & "." & wcteFnName, "Par�metro 'Fecha Hasta' No v�lido."
    End If
    '
    wvarStep = 60
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 70
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los par�metros generales (ParametrosMQ.xml)
    wvarStep = 100
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        If .childNodes.length > 0 Then
            wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
        End If
    End With
    '
    wvarStep = 110
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 120
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & _
                wvarFechaDesde & wvarFechaHasta & wvarRamo & wvarPolizaAnn & _
                wvarPolizaSec & wvarCertiPol & wvarCertiAnn & wvarCertiSec
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, wvarParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & wvarParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    wvarStep = 240
    wvarResult = ""
    wvarPos = 108  'cantidad de caracteres ocupados por par�metros de entrada
    '
    wvarStringLen = Len(wvarParseString)
    '
    wvarStep = 250
    wvarEstado = Mid(wvarParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 260
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 280
        wvarResult = ParseoMensaje(wvarPos, wvarParseString, wvarStringLen, wvarCantLin)
        '
        wvarStep = 310
        If wvarCantLin > 0 Then
            '
            wvarStep = 340
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
            '
        Else
            '
            wvarStep = 350
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>"
            '
        End If
        '
    End If
    '
    wvarStep = 360
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarFechaDesde & wvarFechaHasta & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(pvarPos As Long, pvarParseString As String, pvarStringLen As Long, pvarCantLin As Long) As String
    Dim wvarResult As String
    Dim wvarCantLin As Long
    Dim wvarCounter As Long
    '
    'NOTA:
    'No se indica el campo CANTLIN del vector.
    'Voy a hacer un ciclo hasta el m�ximo definido en el doc.
    '
    pvarPos = pvarPos + 1
    '
    wvarResult = "<RS>"
    '
    wvarCounter = 0
    Do While (wvarCounter < 185) And (Len(pvarParseString) > pvarPos)
        wvarResult = wvarResult & "<R>"
        wvarResult = wvarResult & "<RAMOPCOD>" & Mid(pvarParseString, pvarPos, 4) & "</RAMOPCOD>"
        wvarResult = wvarResult & "<POLIZANN>" & Mid(pvarParseString, pvarPos + 4, 2) & "</POLIZANN>"
        wvarResult = wvarResult & "<POLIZSEC>" & Mid(pvarParseString, pvarPos + 6, 6) & "</POLIZSEC>"
        wvarResult = wvarResult & "<CERTIPOL>" & Mid(pvarParseString, pvarPos + 12, 4) & "</CERTIPOL>"
        wvarResult = wvarResult & "<CERTIANN>" & Mid(pvarParseString, pvarPos + 16, 4) & "</CERTIANN>"
        wvarResult = wvarResult & "<CERTISEC>" & Mid(pvarParseString, pvarPos + 20, 6) & "</CERTISEC>"
        wvarResult = wvarResult & "<SUPLENUM>" & Mid(pvarParseString, pvarPos + 26, 4) & "</SUPLENUM>"
        wvarResult = wvarResult & "<CLIENSEC>" & Mid(pvarParseString, pvarPos + 30, 9) & "</CLIENSEC>"
        wvarResult = wvarResult & "<CLIENDES>" & Mid(pvarParseString, pvarPos + 39, 30) & "</CLIENDES>"
        wvarResult = wvarResult & "<TOMARIES>" & Mid(pvarParseString, pvarPos + 69, 30) & "</TOMARIES>"
        wvarResult = wvarResult & "<SITUCPOL>" & Mid(pvarParseString, pvarPos + 99, 30) & "</SITUCPOL>"
        wvarResult = wvarResult & "<TIPOOPER>" & Mid(pvarParseString, pvarPos + 129, 30) & "</TIPOOPER>"
        wvarResult = wvarResult & "<FECING>" & Mid(pvarParseString, pvarPos + 159, 10) & "</FECING>"
        wvarResult = wvarResult & "<AGENTCLA>" & Mid(pvarParseString, pvarPos + 169, 2) & "</AGENTCLA>"
        wvarResult = wvarResult & "<AGENTCOD>" & Mid(pvarParseString, pvarPos + 171, 4) & "</AGENTCOD>"
        wvarResult = wvarResult & "<COLECTIP>" & Mid(pvarParseString, pvarPos + 175, 1) & "</COLECTIP>"
        wvarResult = wvarResult & "</R>"
        pvarPos = pvarPos + 176
        wvarCounter = wvarCounter + 1
    Loop
    '
    wvarResult = wvarResult & "</RS>"
    '
    pvarCantLin = wvarCounter
    '
    ParseoMensaje = wvarResult
End Function





