VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetCertMercosur"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQEmision.lbaw_GetCertMercosur"
Const mcteOpID              As String = "1584"
'            04 LNK-PARM-ENTRADA.
'               06 LNK-MENSAJE               PIC 9(4).
'               06 LNK-PARM-GRAL-SELECCION.
'                  08 LNK-CIAASCOD           PIC X(4).
'                  08 LNK-USUARCOD           PIC X(10).
'                  08 LNK-ESTADO             PIC X(2).
'                  08 LNK-ERROR              PIC X(2).
'                  08 LNK-CLIENSECAS         PIC 9(9).
'                  08 LNK-NIVELCLAS          PIC X(2).
'                  08 LNK-NIVELCLA1          PIC X(2).
'                  08 LNK-CLIENSEC1          PIC 9(9).
'                  08 LNK-NIVELCLA2          PIC X(2).
'                  08 LNK-CLIENSEC2          PIC 9(9).
'                  08 LNK-NIVELCLA3          PIC X(2).
'                  08 LNK-CLIENSEC3          PIC 9(9).
'               06 LNK-PARM-ESPE-SELECCION.
'                  08 LNK-POLIZA.
'                     10 LNK-RAMOPCOD        PIC X(04).
'                     10 LNK-POLIZANN        PIC 9(02).
'                     10 LNK-POLIZSEC        PIC 9(06).
'                     10 LNK-CERTIPOL        PIC 9(04).
'                      10 LNK-CERTIANN        PIC 9(04).
'                      10 LNK-CERTISEC        PIC 9(06).
'                   08 LNK-PATENTE            PIC X(10).
'                   08 LNK-MOTOR              PIC X(25). 29/04/5009 25, antes 20
'                06 LNK-PARM-START.
'                   08 LNK-NROQRY-S           PIC 9(08).
'                   08 LNK-RAMOPCOD-S         PIC X(04).
'                   08 LNK-POLIZANN-S         PIC 9(02).
'                   08 LNK-POLIZSEC-S         PIC 9(06).
'                   08 LNK-CERTIPOL-S         PIC 9(04).
'                   08 LNK-CERTIANN-S         PIC 9(04).
'                   08 LNK-CERTISEC-S         PIC 9(06).
'             04 LNK-PARM-SALIDA.
'                06 LNK-RESULTADOS            OCCURS 150. 29/04/5009 150, antes 200
'                   08 LNK-CLAVE-OPERACION.
'                      10 LNK-RAMOPCOD        PIC X(4).
'                      10 LNK-POLIZANN        PIC 9(2).
'                      10 LNK-POLIZSEC        PIC 9(6).
'                      10 LNK-CERTIPOL        PIC 9(4).
'                      10 LNK-CERTIANN        PIC 9(4).
'                      10 LNK-CERTISEC        PIC 9(6).
'                   08 LNK-CLIENTE            PIC X(35).
'                   08 LNK-TOMARIES           PIC X(70). 29/04/5009 70, antes 30
'                   08 LNK-PATENNUM           PIC X(10).
'                   08 LNK-SITUCPOL           PIC X(30).
'                   08 LNK-SWIMPRIME          PIC X(01).

'Parametros XML de Entrada
Const mcteParam_USUARCOD    As String = "//USUARIO"
Const mcteParam_RAMOPCOD    As String = "//RAMOPCOD"
Const mcteParam_POLIZANN    As String = "//POLIZANN"
Const mcteParam_POLIZSEC    As String = "//POLIZSEC"
Const mcteParam_CERTIPOL    As String = "//CERTIPOL"
Const mcteParam_CERTIANN    As String = "//CERTIANN"
Const mcteParam_CERTISEC    As String = "//CERTISEC"
Const mcteParam_PATENTE     As String = "//PATENTE"
Const mcteParam_MOTOR       As String = "//MOTOR"

' ################################################
' Agregado 09-08-2006. FJO. Paginación
'06  LNK-PARM-START. (Longitud: 34)
Const mcteParam_NROQRY_S        As String = "//NROQRY_S"    '08 LNK-NROQRY-S PIC 9(8).
Const mcteParam_RAMOPCOD_S      As String = "//RAMOPCOD_S"  '08 LNK-RAMOPCOD PIC X(4).
Const mcteParam_POLIZANN_S      As String = "//POLIZANN_S"  '08 LNK-POLIZANN PIC 9(2).
Const mcteParam_POLIZSEC_S      As String = "//POLIZSEC_S"  '08 LNK-POLIZSEC PIC 9(6).
Const mcteParam_CERTIPOL_S      As String = "//CERTIPOL_S"  '08 LNK-CERTIPOL PIC 9(4).
Const mcteParam_CERTIANN_S      As String = "//CERTIANN_S"  '08 LNK-CERTIANN PIC 9(4).
Const mcteParam_CERTISEC_S      As String = "//CERTISEC_S"  '08 LNK-CERTISEC PIC 9(6).
' ################################################

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarRamo            As String
    Dim wvarPolizaAnn       As String
    Dim wvarPolizaSec       As String
    Dim wvarCertiPol        As String
    Dim wvarCertiAnn        As String
    Dim wvarCertiSec        As String
    Dim wvarPatente         As String
    Dim wvarMotor           As String
    '
    ' ################################################
    ' Agregado 09-08-2006. FJO. Paginación
    Dim wvarNROQRY_S      As String
    Dim wvarRAMOPCOD_S    As String
    Dim wvarPOLIZANN_S    As String
    Dim wvarPOLIZSEC_S    As String
    Dim wvarCERTIPOL_S    As String
    Dim wvarCERTIANN_S    As String
    Dim wvarCERTISEC_S    As String
    ' ################################################
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        '
        If .selectNodes(mcteParam_USUARCOD).length = 0 Then
            wvarUsuario = String(10, " ")
        Else
            wvarUsuario = Left(.selectSingleNode(mcteParam_USUARCOD).Text & Space(10), 10)
        End If
        '
        If .selectNodes(mcteParam_RAMOPCOD).length = 0 Then
            wvarRamo = String(4, " ")
        Else
            wvarRamo = Left(.selectSingleNode(mcteParam_RAMOPCOD).Text & Space(4), 4)
        End If
        '
        If .selectNodes(mcteParam_POLIZANN).length = 0 Then
            wvarPolizaAnn = String(2, "0")
        Else
            wvarPolizaAnn = Right("00" & .selectSingleNode(mcteParam_POLIZANN).Text, 2)
        End If
        '
        If .selectNodes(mcteParam_POLIZSEC).length = 0 Then
            wvarPolizaSec = String(6, "0")
        Else
            wvarPolizaSec = Right("000000" & .selectSingleNode(mcteParam_POLIZSEC).Text, 6)
        End If
        '
        If .selectNodes(mcteParam_CERTIPOL).length = 0 Then
            wvarCertiPol = String(4, "0")
        Else
            wvarCertiPol = Right("0000" & .selectSingleNode(mcteParam_CERTIPOL).Text, 4)
        End If
        '
        If .selectNodes(mcteParam_CERTIANN).length = 0 Then
            wvarCertiAnn = String(4, "0")
        Else
            wvarCertiAnn = Right("0000" & .selectSingleNode(mcteParam_CERTIANN).Text, 4)
        End If
        '
        If .selectNodes(mcteParam_CERTISEC).length = 0 Then
            wvarCertiSec = String(6, "0")
        Else
            wvarCertiSec = Right("000000" & .selectSingleNode(mcteParam_CERTISEC).Text, 6)
        End If
        '
        If .selectNodes(mcteParam_PATENTE).length = 0 Then
            wvarPatente = String(10, " ")
        Else
            wvarPatente = Left(.selectSingleNode(mcteParam_PATENTE).Text & String(10, " "), 10)
        End If
        '
        If .selectNodes(mcteParam_MOTOR).length = 0 Then
            wvarMotor = String(25, " ")
        Else
            wvarMotor = Left(.selectSingleNode(mcteParam_MOTOR).Text & String(25, " "), 25)
        End If
        '
        ' ################################################
        ' Agregado 04-08-2006. FJO. Paginación
        If .selectSingleNode(mcteParam_NROQRY_S) Is Nothing Then
            wvarNROQRY_S = "00000000"
        Else
            wvarNROQRY_S = .selectSingleNode(mcteParam_NROQRY_S).Text
        End If
        
        If .selectSingleNode(mcteParam_RAMOPCOD_S) Is Nothing Then
            wvarRAMOPCOD_S = "    "
        Else
            wvarRAMOPCOD_S = .selectSingleNode(mcteParam_RAMOPCOD_S).Text
        End If
        
        If .selectSingleNode(mcteParam_POLIZANN_S) Is Nothing Then
            wvarPOLIZANN_S = "00"
        Else
            wvarPOLIZANN_S = .selectSingleNode(mcteParam_POLIZANN_S).Text
        End If
        
        If .selectSingleNode(mcteParam_POLIZSEC_S) Is Nothing Then
            wvarPOLIZSEC_S = "000000"
        Else
            wvarPOLIZSEC_S = .selectSingleNode(mcteParam_POLIZSEC_S).Text
        End If
        
        If .selectSingleNode(mcteParam_CERTIPOL_S) Is Nothing Then
            wvarCERTIPOL_S = "0000"
        Else
            wvarCERTIPOL_S = .selectSingleNode(mcteParam_CERTIPOL_S).Text
        End If
        
        If .selectSingleNode(mcteParam_CERTIANN_S) Is Nothing Then
            wvarCERTIANN_S = "0000"
        Else
            wvarCERTIANN_S = .selectSingleNode(mcteParam_CERTIANN_S).Text
        End If
        
        If .selectSingleNode(mcteParam_CERTISEC_S) Is Nothing Then
            wvarCERTISEC_S = "000000"
        Else
            wvarCERTISEC_S = .selectSingleNode(mcteParam_CERTISEC_S).Text
        End If
        
        ' ################################################
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & _
                "    000000000    000000000  000000000  000000000" & _
                wvarRamo & wvarPolizaAnn & wvarPolizaSec & wvarCertiPol & _
                wvarCertiAnn & wvarCertiSec & wvarPatente & wvarMotor & wvarNROQRY_S & wvarRAMOPCOD_S & wvarPOLIZANN_S & wvarPOLIZSEC_S & wvarCERTIPOL_S & wvarCERTIANN_S & wvarCERTISEC_S
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
        '
    wvarStep = 190
    wvarResult = ""
'        wvarPos = 122 'cantidad de caracteres ocupados por parámetros de entrada
    'wvarPos = 156 'cantidad de caracteres ocupados por parámetros de entrada
    wvarPos = 161 'LR. Se suman 5 porque aumento el MOTOR a 25
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 200
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 210
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 240
        wvarResult = ""
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        '
        wvarStep = 250
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 260
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML (wvarResult)
        '
        If wobjXMLResponse.selectNodes("//CERTIFICADO").length <> 0 Then
            wvarStep = 290
            Set wobjXMLResponse = Nothing
            '
            wvarStep = 300
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
            '
        Else
            '
            wvarStep = 270
            Set wobjXMLResponse = Nothing
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>"
            '
        End If
    End If
    '
    wvarStep = 310
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    Set wobjXMLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarRamo & wvarPolizaAnn & wvarPolizaSec & wvarCertiPol & wvarCertiAnn & wvarCertiSec & wvarPatente & wvarMotor & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(pvarPos As Long, pstrParseString As String, pvarstrLen As Long) As String
'
Dim wobjRegExp          As Object 'RegExp
Dim wobjColMatch        As Object 'MatchCollection
Dim wobjMatch           As Object 'Match
Dim wvarParseEvalString As String
'
Dim wobjXmlDOMResp      As DOMDocument
Dim wobjXmlDOMReq       As DOMDocument
Dim wobjXmlElemenResp   As IXMLDOMElement
Dim wobjXmlElemenRespD  As IXMLDOMElement
'
Dim wvarCantRegistros   As Long
Dim wvarCcurrRegistro   As Long
'
    Set wobjRegExp = CreateObject("VbScript.RegExp")
    wobjRegExp.Global = True
    '
    ' PARSEO DE LA RESPUESTA - Area Resultados
    '
    'Caracter (1) Estado CL Se desestima...
    pvarPos = pvarPos + 1
    wvarParseEvalString = Mid(pstrParseString, pvarPos)
    wvarParseEvalString = Replace(wvarParseEvalString, " ", "_")
    'wvarCantRegistros = 200. LR 29/04/2009 Se cambia a 150
    wvarCantRegistros = 150
    wvarCcurrRegistro = 0
    
    'wobjRegExp.Pattern = "(\S{4})(\S{2})(\S{6})(\S{4})(\S{4})(\S{6})(\S{35})(\S{30})(\S{10})(\S{30})(\S{1})"
    'LR 29/04/2009. TOMARIES cambia a 70
    wobjRegExp.Pattern = "(\S{4})(\S{2})(\S{6})(\S{4})(\S{4})(\S{6})(\S{35})(\S{70})(\S{10})(\S{30})(\S{1})"
    '
    
    Set wobjColMatch = wobjRegExp.Execute(wvarParseEvalString)
    '
    Set wobjXmlDOMResp = CreateObject("msxml2.DomDocument")
    Call wobjXmlDOMResp.loadXML("<CERTIFICADOS></CERTIFICADOS>")
    '
    '
    For Each wobjMatch In wobjColMatch
    '
        If Not wvarCcurrRegistro < wvarCantRegistros Then Exit For
        If Trim(Replace(wobjMatch.SubMatches(0), "_", " ")) = "" Then Exit For
        wvarCcurrRegistro = wvarCcurrRegistro + 1
        '
        Set wobjXmlElemenResp = wobjXmlDOMResp.createElement("CERTIFICADO")
        wobjXmlDOMResp.selectSingleNode("//CERTIFICADOS").appendChild wobjXmlElemenResp
        '
        Set wobjXmlElemenRespD = wobjXmlDOMResp.createElement("RAMOPCOD")
        wobjXmlElemenResp.appendChild wobjXmlElemenRespD
        wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(0), "_", " "))
        '
        Set wobjXmlElemenRespD = wobjXmlDOMResp.createElement("POLIZANN")
        wobjXmlElemenResp.appendChild wobjXmlElemenRespD
        wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(1), "_", " "))
        '
        Set wobjXmlElemenRespD = wobjXmlDOMResp.createElement("POLIZSEC")
        wobjXmlElemenResp.appendChild wobjXmlElemenRespD
        wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(2), "_", " "))
        '
        Set wobjXmlElemenRespD = wobjXmlDOMResp.createElement("CERTIPOL")
        wobjXmlElemenResp.appendChild wobjXmlElemenRespD
        wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(3), "_", " "))
        '
        Set wobjXmlElemenRespD = wobjXmlDOMResp.createElement("CERTIANN")
        wobjXmlElemenResp.appendChild wobjXmlElemenRespD
        wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(4), "_", " "))
        '
        Set wobjXmlElemenRespD = wobjXmlDOMResp.createElement("CERTISEC")
        wobjXmlElemenResp.appendChild wobjXmlElemenRespD
        wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(5), "_", " "))
        '
        Set wobjXmlElemenRespD = wobjXmlDOMResp.createElement("NOMBRECLIENTE")
        wobjXmlElemenResp.appendChild wobjXmlElemenRespD
        wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(6), "_", " "))
        '
        Set wobjXmlElemenRespD = wobjXmlDOMResp.createElement("TOMARIES")
        wobjXmlElemenResp.appendChild wobjXmlElemenRespD
        wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(7), "_", " "))
        '
        Set wobjXmlElemenRespD = wobjXmlDOMResp.createElement("PATENTE")
        wobjXmlElemenResp.appendChild wobjXmlElemenRespD
        wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(8), "_", " "))
        '
        Set wobjXmlElemenRespD = wobjXmlDOMResp.createElement("SITUCPOL")
        wobjXmlElemenResp.appendChild wobjXmlElemenRespD
        wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(9), "_", " "))
        '
        Set wobjXmlElemenRespD = wobjXmlDOMResp.createElement("IMPRIME_CM")
        wobjXmlElemenResp.appendChild wobjXmlElemenRespD
        wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(10), "_", " "))
        
    Next wobjMatch
    '
    Set wobjMatch = Nothing
    'Set wobjRegExp = Nothing
    '
    ' PARSEO DE LA RESPUESTA - Area Request
    '
    'pvarPos = 156
    pvarPos = 161 'LR 29/04/2009. Se suman 5 porque aumento el MOTOR a 25
    
    wvarParseEvalString = Mid(pstrParseString, 1, pvarPos)
    wvarParseEvalString = Replace(wvarParseEvalString, " ", "_")
    '
    'Set wobjRegExp = CreateObject("VbScript.RegExp")
    'wobjRegExp.Global = True
    'wobjRegExp.Pattern = "(\S{4})(\S{4})(\S{10})(\S{2})(\S{2})(\S{9})(\S{2})(\S{2})(\S{9})(\S{2})(\S{9})(\S{2})(\S{9})(\S{4})(\S{2})(\S{6})(\S{4})(\S{4})(\S{6})(\S{10})(\S{20})(\S{8})(\S{4})(\S{2})(\S{6})(\S{4})(\S{4})(\S{6})"
    'LR 29/04/2009. El motor cambia a 25
    wobjRegExp.Pattern = "(\S{4})(\S{4})(\S{10})(\S{2})(\S{2})(\S{9})(\S{2})(\S{2})(\S{9})(\S{2})(\S{9})(\S{2})(\S{9})(\S{4})(\S{2})(\S{6})(\S{4})(\S{4})(\S{6})(\S{10})(\S{25})(\S{8})(\S{4})(\S{2})(\S{6})(\S{4})(\S{4})(\S{6})"
    
    '
    Set wobjColMatch = wobjRegExp.Execute(wvarParseEvalString)
    Set wobjMatch = wobjColMatch(0)
    '
    Set wobjXmlDOMReq = CreateObject("msxml2.DomDocument")
    Call wobjXmlDOMReq.loadXML("<RespuestaMQ></RespuestaMQ>")
    '
    'Agrego el resultado del parseo de la respuesta
    wobjXmlDOMReq.selectSingleNode("//RespuestaMQ").appendChild wobjXmlDOMResp.selectSingleNode("//CERTIFICADOS")
    '
    'Continuo con el parseo del request recibido.
    Set wobjXmlElemenResp = wobjXmlDOMReq.createElement("Request")
    wobjXmlDOMReq.selectSingleNode("//RespuestaMQ").appendChild wobjXmlElemenResp
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("USUARIO")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(2), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("STATUS")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(3), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("ERR_DESC")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(4), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("RAMOPCOD")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(13), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("POLIZANN")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(14), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("POLIZSEC")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(15), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("CERTIPOL")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(16), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("CERTIANN")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(17), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("CERTISEC")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(18), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("PATENTE")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(19), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("MOTOR")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(20), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("NROQRY_S")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(21), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("RAMOPCOD_S")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(22), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("POLIZANN_S")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(23), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("POLIZSEC_S")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(24), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("CERTIPOL_S")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(25), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("CERTIANN_S")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(26), "_", " "))
    '
    Set wobjXmlElemenRespD = wobjXmlDOMReq.createElement("CERTISEC_S")
    wobjXmlElemenResp.appendChild wobjXmlElemenRespD
    wobjXmlElemenRespD.Text = Trim(Replace(wobjMatch.SubMatches(27), "_", " "))
    '
    ParseoMensaje = wobjXmlDOMReq.xml
    '
    Set wobjXmlDOMResp = Nothing
    Set wobjXmlDOMReq = Nothing
    Set wobjXmlElemenResp = Nothing
    Set wobjXmlElemenRespD = Nothing
    '
    Set wobjRegExp = Nothing
    
End Function

