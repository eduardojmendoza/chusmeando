VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVEmisPropuesta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQEmision.lbaw_OVEmisPropuesta"
Const mcteOpID              As String = "1526"

'Parametros XML de Entrada
Const mcteParam_USUARCOD    As String = "//USUARIO"
Const mcteParam_RAMOPCOD    As String = "//RAMOPCOD"
Const mcteParam_POLIZANN    As String = "//POLIZANN"
Const mcteParam_POLIZSEC    As String = "//POLIZSEC"
Const mcteParam_CERTIPOL    As String = "//CERTIPOL"
Const mcteParam_CERTIANN    As String = "//CERTIANN"
Const mcteParam_CERTISEC    As String = "//CERTISEC"
Const mcteParam_Suplenum    As String = "//SUPLENUM"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarRamo            As String
    Dim wvarPolizaAnn       As String
    Dim wvarPolizaSec       As String
    Dim wvarCertiPol        As String
    Dim wvarCertiAnn        As String
    Dim wvarCertiSec        As String
    Dim wvarSuplenum        As String
    Dim wvarAction          As IAction
    Dim wvarRespFiles       As String
    '
    Dim wvarPos             As Long
    Dim wvarParseString     As String
    Dim wvarStringLen       As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los par�metros que llegan desde la p�gina
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUsuario = Left(.selectSingleNode(mcteParam_USUARCOD).Text & Space(10), 10)
        wvarRamo = Left(.selectSingleNode(mcteParam_RAMOPCOD).Text & Space(4), 4)
        wvarPolizaAnn = Right("00" & .selectSingleNode(mcteParam_POLIZANN).Text, 2)
        wvarPolizaSec = Right("000000" & .selectSingleNode(mcteParam_POLIZSEC).Text, 6)
        wvarCertiPol = Right("0000" & .selectSingleNode(mcteParam_CERTIPOL).Text, 4)
        wvarCertiAnn = Right("0000" & .selectSingleNode(mcteParam_CERTIANN).Text, 4)
        wvarCertiSec = Right("000000" & .selectSingleNode(mcteParam_CERTISEC).Text, 6)
        wvarSuplenum = Right("0000" & .selectSingleNode(mcteParam_Suplenum).Text, 4)
    End With
    '
    wvarStep = 60
    
    'DUMMY!!!!!!!!!!!
        If Not wobjXMLRequest.selectSingleNode("//DUMMY") Is Nothing Then
            If wobjXMLRequest.selectSingleNode("//DUMMY").Text = "NOOK" Then
                Response = "<Response><Estado resultado='true' mensaje=''/>"
                Response = Response & "<TEXTO>" & "No se pudo realizar la emisi�n de la propuesta" & "</TEXTO>"
                Response = Response & "<ERRORES><ERROR>La propuesta posee Retenciones</ERROR></ERRORES>"
                Response = Response & "<CLIENTE>"
                Response = Response & "<CLIENSEC>" & "000000001" & "</CLIENSEC>"
                Response = Response & "<CLIENDES>" & "JUAN ERNESTO PEREZ" & "</CLIENDES>"
                Response = Response & "</CLIENTE>"
                Response = Response & "<OPERASEC>" & "AUS10123456789" & "</OPERASEC>"
                Response = Response & "<RETENCIONES>"
                Response = Response & "<RETENCION>"
                Response = Response & "<SITUACOD>" & "0001" & "</SITUACOD>"
                Response = Response & "<SITUASEC>" & "003" & "</SITUASEC>"
                Response = Response & "<SITUADES>" & "FALTA INSTALACION EQUIPO DE RASTREO" & "</SITUADES>"
                Response = Response & "<FECRETEN>" & Format(Now, "DD/MM/YYYY") & "</FECRETEN>"
                Response = Response & "<USUARORI>" & wvarUsuario & "</USUARORI>"
                Response = Response & "<ORIGECOD>" & "2" & "</ORIGECOD>"
                Response = Response & "<FECRESOL>" & "</FECRESOL>"
                Response = Response & "<USUARRES>" & "</USUARRES>"
                Response = Response & "</RETENCION>"
                Response = Response & "<RETENCION>"
                Response = Response & "<SITUACOD>" & "0002" & "</SITUACOD>"
                Response = Response & "<SITUASEC>" & "005" & "</SITUASEC>"
                Response = Response & "<SITUADES>" & "FALTA VERIFICACION" & "</SITUADES>"
                Response = Response & "<FECRETEN>" & Format(Now, "DD/MM/YYYY") & "</FECRETEN>"
                Response = Response & "<USUARORI>" & wvarUsuario & "</USUARORI>"
                Response = Response & "<ORIGECOD>" & "2" & "</ORIGECOD>"
                Response = Response & "<FECRESOL>" & "</FECRESOL>"
                Response = Response & "<USUARRES>" & "</USUARRES>"
                Response = Response & "</RETENCION>"
                Response = Response & "</RETENCIONES>"
                Response = Response & "</Response>"
            Else
                Response = "<Response><Estado resultado='true' mensaje=''/>"
                Response = Response & "<TEXTO>" & "Emisi�n de propuesta realizada correctamente" & "</TEXTO>"
                Response = Response & "<OPERASEC>" & "AUS10123456789" & "</OPERASEC>"
                'Response = Response & "<FILES>"
                'Response = Response & "<FILE><DESCRIPCION>PRUEBA</DESCRIPCION><RUTA>D:\COMP\LBAOVFaseII\MERCOSUR.PDF</RUTA></FILE>"
                'Response = Response & "<FILE><DESCRIPCION>PRUEBA</DESCRIPCION><RUTA>D:\COMP\LBAOVFaseII\FRENPOLI.pdf</RUTA></FILE>"
                'Response = Response & "</FILES>"
                
                Set wvarAction = CreateObject("lbawA_OVMQEmision.lbaw_OVImprimirPoliza")
                wvarAction.Execute Request, wvarRespFiles, ContextInfo
                Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
                wobjXMLResponse.loadXML wvarRespFiles
                If Not wobjXMLResponse.selectSingleNode("//FILES") Is Nothing Then
                    Response = Response & wobjXMLResponse.selectSingleNode("//FILES").xml
                End If
                Response = Response & "</Response>"
            End If
            Set wobjXMLResponse = Nothing
            Set wobjXMLRequest = Nothing
            Set wvarAction = Nothing
            GoTo ClearObjects:
        End If
    
    Set wobjXMLResponse = Nothing
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 70
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los par�metros generales (ParametrosMQ.xml)
    wvarStep = 100
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        If .childNodes.length > 0 Then
            wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
        End If
    End With
    '
    wvarStep = 110
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 120
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & _
                "    000000000    000000000  000000000  000000000" & _
                wvarRamo & wvarPolizaAnn & wvarPolizaSec & wvarCertiPol & _
                wvarCertiAnn & wvarCertiSec & wvarSuplenum
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, wvarParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & wvarParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    wvarStep = 240
    wvarPos = 96  'cantidad de caracteres ocupados por par�metros de entrada
    wvarStringLen = Len(wvarParseString)
    '
    wvarStep = 250
    wvarEstado = Mid(wvarParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 260
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 280
        wvarResult = ParseoMensaje(wvarPos, wvarParseString, wvarStringLen)
        '
        If wvarStringLen > wvarPos Then
            'Devolvemos el Response
            Response = "<Response><Estado resultado='true' mensaje=''/>" & wvarResult & "</Response>"
            '
        Else
            'No hay datos para devolver
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos'/></Response>"
            '
        End If
        '
    End If
    '
    wvarStep = 290
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & _
                        " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & _
                        wvarRamo & wvarPolizaAnn & wvarPolizaSec & wvarCertiPol & _
                        wvarCertiAnn & wvarCertiSec & wvarSuplenum & " Hora:" & Now(), _
                        vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(pvarPos As Long, pvarParseString As String, pvarStringLen As Long) As String
    Dim wvarResult As String
    Dim wvarCounter As Long
    '
    'NOTA: Los campos LNK-CLAVE-SALIDA y LNK-TEXTO-SAL del documento no est�n
    'completamente definidos, les falta la descripci�n y el tipo.
    'Por ahora no los incluyo en el XML de salida.
    'Tampoco hay CANTLIN.
    'Hago un LOOP hasta el m�ximo definido en el doc.
    '
    pvarPos = pvarPos + 1
    '
    'Datos de la P�liza
    wvarResult = wvarResult & "<TEXTO>" & Mid(pvarParseString, pvarPos, 30) & "</TEXTO>"
    wvarResult = wvarResult & "<POLIZA>"
    wvarResult = wvarResult & "<RAMOPCOD>" & Mid(pvarParseString, pvarPos + 30, 4) & "</RAMOPCOD>"
    wvarResult = wvarResult & "<POLIZANN>" & Mid(pvarParseString, pvarPos + 34, 2) & "</POLIZANN>"
    wvarResult = wvarResult & "<POLIZSEC>" & Mid(pvarParseString, pvarPos + 36, 6) & "</POLIZSEC>"
    wvarResult = wvarResult & "<CERTIPOL>" & Mid(pvarParseString, pvarPos + 42, 4) & "</CERTIPOL>"
    wvarResult = wvarResult & "<CERTIANN>" & Mid(pvarParseString, pvarPos + 46, 4) & "</CERTIANN>"
    wvarResult = wvarResult & "<CERTISEC>" & Mid(pvarParseString, pvarPos + 50, 6) & "</CERTISEC>"
    wvarResult = wvarResult & "<SUPLENUM>" & Mid(pvarParseString, pvarPos + 56, 4) & "</SUPLENUM>"
    wvarResult = wvarResult & "<OPERAPOL>" & Mid(pvarParseString, pvarPos + 60, 6) & "</OPERAPOL>"
    wvarResult = wvarResult & "</POLIZA>"
    '
    'Vector de Mensajes de Error
    pvarPos = pvarPos + 66
    wvarCounter = 0
    wvarResult = wvarResult & "<ERRORES>"
    Do While (wvarCounter < 30) And (Len(pvarParseString) > pvarPos) And (Trim(Mid(pvarParseString, pvarPos, 60)) <> "")
        wvarResult = wvarResult & "<ERROR>" & Trim(Mid(pvarParseString, pvarPos, 60)) & "</ERROR>"
        pvarPos = pvarPos + 60
        wvarCounter = wvarCounter + 1
    Loop
    wvarResult = wvarResult & "</ERRORES>"
    '
    ParseoMensaje = wvarResult
End Function
