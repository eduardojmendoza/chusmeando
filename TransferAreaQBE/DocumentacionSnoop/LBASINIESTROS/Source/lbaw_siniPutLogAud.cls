VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_siniPutLogAud"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context As ObjectContext
Private mobjEventLog    As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction


'Datos de la accion
Const mcteClassName             As String = "lbawA_SiniDenuncia.lbaw_siniPutLogAud"
Const mcteStoreProc             As String = "P_SINI_INSERT_LOGAUD"
'
Const mcteParam_RAMOPCOD                   As String = "//RAMOPCOD"
Const mcteParam_SINIEANNAIS                As String = "//SINIEANNAIS"
Const mcteParam_SINIENUMAIS                As String = "//SINIENUMAIS"
Const mcteParam_SINIENUMSQL                As String = "//SINIENUMSQL"
Const mcteParam_NROFORM                    As String = "//NROFORM"
Const mcteParam_GRABAUSU                   As String = "//GRABAUSU"
Const mcteParam_CLIENSEC                   As String = "//CLIENSEC"
Const mcteParam_GRABASVIA                  As String = "//GRABASVIA"
Const mcteParam_ESTADODAT                  As String = "//ESTADODAT"
Const mcteParam_RECARGA                    As String = "//RECARGA"
Const mcteParam_ESTADOAIS                  As String = "//ESTADOAIS"
Const mcteParam_ESTADOMID                  As String = "//ESTADOMID"
Const mcteParam_SINFECHA                   As String = "//SINFECHA"
Const mcteParam_DENFECHA                   As String = "//DENFECHA"
Const mcteParam_GRABASFECHA                As String = "//GRABASFECHA"
Const mcteParam_GRABAAFECHA                As String = "//GRABAAFECHA"
Const mcteParam_NROSAC                     As String = "//NROSAC"
Const mcteParam_ORDNRO1                    As String = "//ORDNRO1"
Const mcteParam_ORDNRO2                    As String = "//ORDNRO2"
Const mcteParam_ORDNRO3                    As String = "//ORDNRO3"
Const mcteParam_XMLErrores                 As String = "//XMLErrores"

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    
    'Par�metros de entrada
    Dim mvarRAMOPCOD        As String
    Dim mvarSINIEANNAIS     As String
    Dim mvarSINIENUMAIS     As String
    Dim mvarSINIENUMSQL     As String
    Dim mvarNROFORM         As String
    Dim mvarGRABAUSU        As String
    Dim mvarCLIENSEC        As String
    Dim mvarGRABASVIA       As String
    Dim mvarESTADODAT       As String
    Dim mvarRECARGA         As String
    Dim mvarESTADOAIS       As String
    Dim mvarESTADOMID       As String
    Dim mvarSINFECHA        As String
    Dim mvarDENFECHA        As String
    Dim mvarGRABASFECHA     As String
    Dim mvarGRABAAFECHA     As String
    Dim mvarNROSAC          As String
    Dim mvarORDNRO1         As String
    Dim mvarORDNRO2         As String
    Dim mvarORDNRO3         As String
    Dim mvarXMLErrores      As String
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
        mvarSINIEANNAIS = .selectSingleNode(mcteParam_SINIEANNAIS).Text
    End With
    
    'Levanto los datos del XML de entrada.
    wvarStep = 20
    With wobjXMLRequest

        wvarStep = 21
        mvarRAMOPCOD = .selectSingleNode(mcteParam_RAMOPCOD).Text
        
        wvarStep = 22
        mvarSINIEANNAIS = .selectSingleNode(mcteParam_SINIEANNAIS).Text
        
        wvarStep = 23
        mvarSINIENUMAIS = .selectSingleNode(mcteParam_SINIENUMAIS).Text
        
        wvarStep = 24
        mvarSINIENUMSQL = .selectSingleNode(mcteParam_SINIENUMSQL).Text
        
        wvarStep = 25
        mvarNROFORM = .selectSingleNode(mcteParam_NROFORM).Text
        
        wvarStep = 26
        mvarGRABAUSU = .selectSingleNode(mcteParam_GRABAUSU).Text
        
        wvarStep = 27
        mvarCLIENSEC = .selectSingleNode(mcteParam_CLIENSEC).Text
        
        wvarStep = 28
        mvarGRABASVIA = .selectSingleNode(mcteParam_GRABASVIA).Text
        
        wvarStep = 29
        mvarESTADODAT = .selectSingleNode(mcteParam_ESTADODAT).Text
        
        wvarStep = 30
        mvarRECARGA = .selectSingleNode(mcteParam_RECARGA).Text
        
        wvarStep = 31
        mvarESTADOAIS = .selectSingleNode(mcteParam_ESTADOAIS).Text
        
        wvarStep = 32
        mvarESTADOMID = .selectSingleNode(mcteParam_ESTADOMID).Text
        
        wvarStep = 33
        'Dato obligatorio de entrada, no puede venir vac�o.
        mvarSINFECHA = .selectSingleNode(mcteParam_SINFECHA).Text
                
        wvarStep = 34
        'Dato obligatorio de entrada, no puede venir vac�o.
        mvarDENFECHA = .selectSingleNode(mcteParam_DENFECHA).Text
        
        wvarStep = 35
        mvarGRABASFECHA = .selectSingleNode(mcteParam_GRABASFECHA).Text
        
        wvarStep = 36
        mvarGRABAAFECHA = .selectSingleNode(mcteParam_GRABAAFECHA).Text
        
        wvarStep = 37
        mvarNROSAC = .selectSingleNode(mcteParam_NROSAC).Text
        
        wvarStep = 38
        mvarORDNRO1 = .selectSingleNode(mcteParam_ORDNRO1).Text
        
        wvarStep = 39
        mvarORDNRO2 = .selectSingleNode(mcteParam_ORDNRO2).Text
        
        wvarStep = 40
        mvarORDNRO3 = .selectSingleNode(mcteParam_ORDNRO3).Text
        
        wvarStep = 41
        mvarXMLErrores = .selectSingleNode(mcteParam_XMLErrores).Text
        
    End With
    
    'Si hay datos num�ricos en blanco mando como default el cero (solo para los campos que no permiten NULL)
    If Trim(mvarSINIEANNAIS) = "" Then mvarSINIEANNAIS = 0
    If Trim(mvarSINIENUMAIS) = "" Then mvarSINIENUMAIS = 0
    If Trim(mvarSINIENUMSQL) = "" Then mvarSINIENUMSQL = 0
        
    wvarStep = 42
    'TODO: OJOOOOOOOOOOOO En mi PC no funciona con Trx. Para debuggear cambiar. Luego volver a su forma con Trx
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 50
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , "0")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
       
    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RAMOPCOD", adChar, adParamInput, 4, mvarRAMOPCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SINIEANNAIS", adInteger, adParamInput, , mvarSINIEANNAIS)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 90
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SINIENUMAIS", adInteger, adParamInput, , mvarSINIENUMAIS)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 100
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SINIENUMSQL", adInteger, adParamInput, , mvarSINIENUMSQL)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 110
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NROFORM", adSmallInt, adParamInput, , mvarNROFORM)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 120
    Set wobjDBParm = wobjDBCmd.CreateParameter("@GRABAUSU", adChar, adParamInput, 10, mvarGRABAUSU)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 130
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEC", adInteger, adParamInput)
    wobjDBCmd.Parameters.Append wobjDBParm
    If mvarCLIENSEC = "" Then wobjDBParm.Value = Null Else wobjDBParm.Value = mvarCLIENSEC
    Set wobjDBParm = Nothing
    
    wvarStep = 140
    Set wobjDBParm = wobjDBCmd.CreateParameter("@GRABASVIA", adChar, adParamInput, 2, mvarGRABASVIA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 150
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTADODAT", adChar, adParamInput, 1, mvarESTADODAT)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 160
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RECARGA", adChar, adParamInput, 1, mvarRECARGA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
   
    wvarStep = 170
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTADOAIS", adChar, adParamInput, 2, mvarESTADOAIS)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 180
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTADOMID", adChar, adParamInput, 3, mvarESTADOMID)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 190
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SINFECHA", adChar, adParamInput, 17, mvarSINFECHA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 200
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DENFECHA", adChar, adParamInput, 17, mvarDENFECHA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 210
    Set wobjDBParm = wobjDBCmd.CreateParameter("@GRABASFECHA", adChar, adParamInput, 17)
    'Si la fecha vino vac�a, env�o NULL
    If mvarGRABASFECHA = "" Then wobjDBParm.Value = Null Else wobjDBParm.Value = mvarGRABASFECHA
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 220
    Set wobjDBParm = wobjDBCmd.CreateParameter("@GRABAAFECHA", adChar, adParamInput, 17)
    'Si la fecha vino vac�a, env�o NULL
    If mvarGRABAAFECHA = "" Then wobjDBParm.Value = Null Else wobjDBParm.Value = mvarGRABAAFECHA
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 230
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NROSAC", adInteger, adParamInput)
    'Si el nro de SAC vino vac�o, env�o NULL
    If mvarNROSAC = "" Then wobjDBParm.Value = Null Else wobjDBParm.Value = mvarNROSAC
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 240
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ORDNRO1", adInteger, adParamInput)
    If mvarORDNRO1 = "" Then wobjDBParm.Value = Null Else wobjDBParm.Value = mvarORDNRO1
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 250
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ORDNRO2", adInteger, adParamInput)
    If mvarORDNRO2 = "" Then wobjDBParm.Value = Null Else wobjDBParm.Value = mvarORDNRO2
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 260
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ORDNRO3", adInteger, adParamInput)
    If mvarORDNRO3 = "" Then wobjDBParm.Value = Null Else wobjDBParm.Value = mvarORDNRO3
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 270
    Set wobjDBParm = wobjDBCmd.CreateParameter("@XMLErrores", adVarChar, adParamInput, 4000, mvarXMLErrores)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 280
    wobjDBCmd.Execute adExecuteNoRecords
    
    wvarStep = 290
    'Controlamos la respuesta del SQL
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 Then
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /></Response>"
    Else
        wvarMensaje = "Error al insertar en la tabla SQL SINI_LOGAUD (log de auditor�a)"
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje='" & wvarMensaje & "' /></Response>"
    End If

    wvarStep = 300
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 310
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub








