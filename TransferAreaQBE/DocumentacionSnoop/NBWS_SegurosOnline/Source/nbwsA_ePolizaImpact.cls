VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_ePolizaImpact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'ENTRADA
'Listado de p�lizas
    'RAMOPCOD
    'POLIZANN
    'POLIZSEC
    'CERTIPOL
    'CERTIANN
    'CERTISEC
    'SUPLENUM
    'email
    'pwd
    'CONFORMIDAD
'PROCESO
'llamar al mensaje de AIS que har� el impacto de los datos en el Sistema Central.

'SALIDA
'Resultado de la operaci�n

'-----------------------------------------------------------------------------------------------------------------------------------
'TODO: poner COPYRIGHT
' *****************************************************************
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "nbwsA_Transacciones.nbwsA_ePolizaImpact"

'Parametros XML de Entrada
Const mcteParam_CIAASCOD      As String = "//CIAASCOD"
Const mcteParam_RAMOPCOD      As String = "//RAMOPCOD"
Const mcteParam_POLIZANN      As String = "//POLIZANN"
Const mcteParam_POLIZSEC      As String = "//POLIZSEC"
Const mcteParam_CERTIPOL      As String = "//CERTIPOL"
Const mcteParam_CERTIANN      As String = "//CERTIANN"
Const mcteParam_CERTISEC      As String = "//CERTISEC"
Const mcteParam_SWSUSCRI      As String = "//SWSUSCRI"
Const mcteParam_MAIL          As String = "//MAIL"
Const mcteParam_CLAVE         As String = "//CLAVE"
Const mcteParam_SWCLAVE       As String = "//SW-CLAVE"

'Parametro XML de Salida
Const mcteParam_SWCONFIR      As String = "//SWCONFIR"
'Fin constantes





Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    
    Dim wobjClass           As HSBCInterfaces.IAction
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLRespuestas   As MSXML2.DOMDocument
    Dim wobjXSLSalida       As MSXML2.DOMDocument
    Dim wobjXMLEpolizas     As MSXML2.DOMDocument
    
    Dim wobjXML_POLIZAS_List    As MSXML2.IXMLDOMNodeList
    Dim wobjXML_POLIZAS_Node    As MSXML2.IXMLDOMNode
    Dim wobjElemento            As MSXML2.IXMLDOMElement
    
    
    Dim wvarStep            As Long
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wvarResponse_XML    As String
    Dim wvarResponse_HTML   As String
    Dim wvarFalseAIS        As String
    
    'Par�metros de entrada
    Dim wvarDocumtip        As String
    Dim wvarDocumdat        As String
    Dim wvarCLIENNOM        As String
    Dim wvarCLIENAPE        As String
    
    
    Dim wvarCIAASCOD        As String
    Dim wvarRAMOPCOD        As String
    Dim wvarPOLIZANN        As String
    Dim wvarPOLIZSEC        As String
    Dim wvarCERTIPOL        As String
    Dim wvarCERTIANN        As String
    Dim wvarCERTISEC        As String
    Dim wvarSWSUSCRI        As String
    Dim wvarSWCONFIR        As String
    Dim wvarMail            As String
    Dim wvarCLAVE           As String
    Dim wvarSWCLAVE         As String
    Dim wvarCONFORME        As String
    Dim wvarSWTIPOSUS       As String
    
    
    Dim wvarContador        As Integer
    
     '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Carga XML de entrada
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        '
        wvarStep = 20
       
        '
    End With
    '
    wvarStep = 30
    'Arma XML de entrada al COM+ Multithreading
    '
    
      Set wobjXML_POLIZAS_List = wobjXMLRequest.selectNodes("//Request/EPOLIZAS/EPOLIZA[CIAASCOD='0001']")
    
    
    'Recorre cada p�liza
   
     wvarRequest = ""
     wvarContador = 1
     
     If wobjXML_POLIZAS_List.length = 0 Then
     
        wvarContador = 0
    
    Else
   
   
        wvarRequest = wvarRequest & "<Request id=""" & wvarContador & """  actionCode=""nbwsA_MQGenericoAIS"" ciaascod=""0001"">" & _
                            "<DEFINICION>LBA_1185_SusyDesus_epoliza.xml</DEFINICION>" & _
                            "<EPOLIZAS>"
                            
        For Each wobjXML_POLIZAS_Node In wobjXML_POLIZAS_List
                
            wvarRAMOPCOD = wobjXML_POLIZAS_Node.selectSingleNode("RAMOPCOD").Text
            wvarPOLIZANN = wobjXML_POLIZAS_Node.selectSingleNode("POLIZANN").Text
            wvarPOLIZSEC = wobjXML_POLIZAS_Node.selectSingleNode("POLIZSEC").Text
            wvarCERTIPOL = wobjXML_POLIZAS_Node.selectSingleNode("CERTIPOL").Text
            wvarCERTIANN = wobjXML_POLIZAS_Node.selectSingleNode("CERTIANN").Text
            wvarCERTISEC = wobjXML_POLIZAS_Node.selectSingleNode("CERTISEC").Text
            wvarSWSUSCRI = wobjXML_POLIZAS_Node.selectSingleNode("SWSUSCRI").Text
            wvarMail = wobjXML_POLIZAS_Node.selectSingleNode("MAIL").Text
            wvarCLAVE = wobjXML_POLIZAS_Node.selectSingleNode("CLAVE").Text
            wvarSWCLAVE = wobjXML_POLIZAS_Node.selectSingleNode("SW-CLAVE").Text
            
            'GED 20-10-2011 - ANEXO I
            wvarSWTIPOSUS = wobjXML_POLIZAS_Node.selectSingleNode("SWTIPOSUS").Text
                        
            wvarRequest = wvarRequest & "<EPOLIZA><CIAASCOD>0001</CIAASCOD>" & _
                            "<RAMOPCOD>" & wvarRAMOPCOD & "</RAMOPCOD>" & _
                            "<POLIZANN>" & wvarPOLIZANN & "</POLIZANN>" & _
                            "<POLIZSEC>" & wvarPOLIZSEC & "</POLIZSEC>" & _
                            "<CERTIPOL>" & wvarCERTIPOL & "</CERTIPOL>" & _
                            "<CERTIANN>" & wvarCERTIANN & "</CERTIANN>" & _
                            "<CERTISEC>" & wvarCERTISEC & "</CERTISEC>" & _
                            "<SWSUSCRI>" & wvarSWSUSCRI & "</SWSUSCRI>" & _
                            "<MAIL>" & wvarMail & "</MAIL>" & _
                            "<CLAVE>" & wvarCLAVE & "</CLAVE>" & _
                            "<SW-CLAVE>" & wvarSWCLAVE & "</SW-CLAVE>" & _
                             "<SWTIPOSUS>" & wvarSWTIPOSUS & "</SWTIPOSUS></EPOLIZA>"


    Next
    
        
            wvarRequest = wvarRequest & "</EPOLIZAS></Request>"
    
    End If
    
   
    Set wobjXML_POLIZAS_List = Nothing
    
    Set wobjXML_POLIZAS_List = wobjXMLRequest.selectNodes("//Request/EPOLIZAS/EPOLIZA[CIAASCOD='0020']")
     
    If wobjXML_POLIZAS_List.length <> 0 Then
    
        wvarContador = wvarContador + 1
       
        
        wvarRequest = wvarRequest & "<Request id=""" & wvarContador & """ actionCode=""nbwsA_MQGenericoAIS"" ciaascod=""0020"">" & _
                            "<DEFINICION>NYL_1185_SusyDesus_epoliza.xml</DEFINICION>" & _
                            "<EPOLIZAS>"
                            
        
        For Each wobjXML_POLIZAS_Node In wobjXML_POLIZAS_List
      
            wvarRAMOPCOD = wobjXML_POLIZAS_Node.selectSingleNode("RAMOPCOD").Text
            wvarPOLIZANN = wobjXML_POLIZAS_Node.selectSingleNode("POLIZANN").Text
            wvarPOLIZSEC = wobjXML_POLIZAS_Node.selectSingleNode("POLIZSEC").Text
            wvarCERTIPOL = wobjXML_POLIZAS_Node.selectSingleNode("CERTIPOL").Text
            wvarCERTIANN = wobjXML_POLIZAS_Node.selectSingleNode("CERTIANN").Text
            wvarCERTISEC = wobjXML_POLIZAS_Node.selectSingleNode("CERTISEC").Text
            wvarSWSUSCRI = wobjXML_POLIZAS_Node.selectSingleNode("SWSUSCRI").Text
            wvarMail = wobjXML_POLIZAS_Node.selectSingleNode("MAIL").Text
            wvarCLAVE = wobjXML_POLIZAS_Node.selectSingleNode("CLAVE").Text
            wvarSWCLAVE = wobjXML_POLIZAS_Node.selectSingleNode("SW-CLAVE").Text
                        
            wvarRequest = wvarRequest & "<EPOLIZA><CIAASCOD>0020</CIAASCOD>" & _
                            "<RAMOPCOD>" & wvarRAMOPCOD & "</RAMOPCOD>" & _
                            "<POLIZANN>" & wvarPOLIZANN & "</POLIZANN>" & _
                            "<POLIZSEC>" & wvarPOLIZSEC & "</POLIZSEC>" & _
                            "<CERTIPOL>" & wvarCERTIPOL & "</CERTIPOL>" & _
                            "<CERTIANN>" & wvarCERTIANN & "</CERTIANN>" & _
                            "<CERTISEC>" & wvarCERTISEC & "</CERTISEC>" & _
                            "<SWSUSCRI>" & wvarSWSUSCRI & "</SWSUSCRI>" & _
                            "<MAIL>" & wvarMail & "</MAIL>" & _
                            "<CLAVE>" & wvarCLAVE & "</CLAVE>" & _
                            "<SW-CLAVE>" & wvarSWCLAVE & "</SW-CLAVE></EPOLIZA>"
                        
    Next
            
            wvarRequest = wvarRequest & "</EPOLIZAS></Request>"
     
     End If
     
        wvarRequest = "<Request>" & wvarRequest & "</Request>"
     
    wvarStep = 40
    'Ejecuta la funci�n Multithreading
    
    Call cmdp_ExecuteTrnMulti("nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse)
 
    '
    wvarStep = 50
    'Carga las dos respuestas en un XML.
    Set wobjXMLRespuestas = CreateObject("MSXML2.DOMDocument")
        wobjXMLRespuestas.async = False
    Call wobjXMLRespuestas.loadXML(wvarResponse)
    '
    wvarStep = 60
    
      'Verifica que no haya pinchado el COM+
    If wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/Estado") Is Nothing Then
        '
        'Fall� Response 1
        Err.Description = "Response 1: fall� COM+ de Request 1"
        GoTo ErrorHandler
        '
    End If
    
    wvarStep = 70
    If wvarContador = 2 Then
        If wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']/Estado") Is Nothing Then
            '
            'Fall� Response 2
            Err.Description = "Response 2: fall� COM+ de Request 2"
            GoTo ErrorHandler
            '
        End If
    End If
     'Anduvieron los dos llamados al AIS.
    'Arma un XML con los productos de las dos compa��as
    wvarResponse = ""
    '
    wvarStep = 80
    If Not wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/CAMPOS/EPOLIZAS") Is Nothing Then
        wvarResponse = wvarResponse & wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/CAMPOS/EPOLIZAS").xml
    Else
        wvarResponse = wvarResponse & ""
        wvarFalseAIS = wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/Estado/@mensaje").Text
    End If
    '
    wvarStep = 90
     If wvarContador = 2 Then
        If Not wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']/CAMPOS/EPOLIZAS") Is Nothing Then
            wvarResponse = wvarResponse & wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']/CAMPOS/EPOLIZAS").xml
        Else
            wvarResponse = wvarResponse & ""
            wvarFalseAIS = wvarFalseAIS & Chr(13) & wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']/Estado/@mensaje").Text
        End If
    End If
    If wvarResponse = "" Then
        GoTo FalseAIS:
    End If
    
       wvarResponse = "<Response>" & Replace(Replace(wvarResponse, "<EPOLIZAS>", ""), "</EPOLIZAS>", "") & "</Response>"
 
    wvarStep = 100
      
        Set wobjXMLEpolizas = CreateObject("MSXML2.DOMDocument")
        wobjXMLEpolizas.async = False
        Call wobjXMLEpolizas.loadXML(wvarResponse)
      
      
         Set wobjXML_POLIZAS_List = wobjXMLEpolizas.selectNodes("Response/EPOLIZA")
    
        wvarResponse_XML = "<EPOLIZAS>"
     wvarStep = 110
        For Each wobjXML_POLIZAS_Node In wobjXML_POLIZAS_List
        
            wvarCIAASCOD = wobjXML_POLIZAS_Node.selectSingleNode("CIAASCOD").Text
            wvarRAMOPCOD = wobjXML_POLIZAS_Node.selectSingleNode("RAMOPCOD").Text
            wvarPOLIZANN = wobjXML_POLIZAS_Node.selectSingleNode("POLIZANN").Text
            wvarPOLIZSEC = wobjXML_POLIZAS_Node.selectSingleNode("POLIZSEC").Text
            wvarCERTIPOL = wobjXML_POLIZAS_Node.selectSingleNode("CERTIPOL").Text
            wvarCERTIANN = wobjXML_POLIZAS_Node.selectSingleNode("CERTIANN").Text
            wvarCERTISEC = wobjXML_POLIZAS_Node.selectSingleNode("CERTISEC").Text
            wvarSWCONFIR = wobjXML_POLIZAS_Node.selectSingleNode("SWCONFIR").Text
            
       wvarStep = 120
          
            If RTrim(LTrim(wvarCIAASCOD)) <> "" And RTrim(LTrim(wvarRAMOPCOD)) <> "" Then
                wvarResponse_XML = wvarResponse_XML & "<EPOLIZA>" & _
                            "<CIAASCOD>" & wvarCIAASCOD & "</CIAASCOD>" & _
                            "<RAMOPCOD>" & wvarRAMOPCOD & "</RAMOPCOD>" & _
                            "<POLIZANN>" & wvarPOLIZANN & "</POLIZANN>" & _
                            "<POLIZSEC>" & wvarPOLIZSEC & "</POLIZSEC>" & _
                            "<CERTIPOL>" & wvarCERTIPOL & "</CERTIPOL>" & _
                            "<CERTIANN>" & wvarCERTIANN & "</CERTIANN>" & _
                            "<CERTISEC>" & wvarCERTISEC & "</CERTISEC>" & _
                            "<SWCONFIR>" & wvarSWCONFIR & "</SWCONFIR>" & _
                            "</EPOLIZA>"
            
            
             'Si se confirma la recepci�n del AIS grabo en el SQL
        
                 wvarCONFORME = wobjXMLRequest.selectSingleNode("//EPOLIZA[RAMOPCOD='" & wvarRAMOPCOD & "' and POLIZANN='" & wvarPOLIZANN & "' and POLIZSEC='" & wvarPOLIZSEC & "' and CERTIPOL='" & wvarCERTIPOL & "' and CERTIANN='" & wvarCERTIANN & "' and CERTISEC='" & wvarCERTISEC & "']/CONFORME").Text
                
                
                
                If wvarSWCONFIR = "S" And wvarCONFORME = "S" Then
                    wvarDocumtip = wobjXMLRequest.selectSingleNode("//EPOLIZA[RAMOPCOD='" & wvarRAMOPCOD & "' and POLIZANN='" & wvarPOLIZANN & "' and POLIZSEC='" & wvarPOLIZSEC & "' and CERTIPOL='" & wvarCERTIPOL & "' and CERTIANN='" & wvarCERTIANN & "' and CERTISEC='" & wvarCERTISEC & "']/DOCUMTIP").Text
                    wvarDocumdat = wobjXMLRequest.selectSingleNode("//EPOLIZA[RAMOPCOD='" & wvarRAMOPCOD & "' and POLIZANN='" & wvarPOLIZANN & "' and POLIZSEC='" & wvarPOLIZSEC & "' and CERTIPOL='" & wvarCERTIPOL & "' and CERTIANN='" & wvarCERTIANN & "' and CERTISEC='" & wvarCERTISEC & "']/DOCUMDAT").Text
                    wvarMail = wobjXMLRequest.selectSingleNode("//EPOLIZA[RAMOPCOD='" & wvarRAMOPCOD & "' and POLIZANN='" & wvarPOLIZANN & "' and POLIZSEC='" & wvarPOLIZSEC & "' and CERTIPOL='" & wvarCERTIPOL & "' and CERTIANN='" & wvarCERTIANN & "' and CERTISEC='" & wvarCERTISEC & "']/MAIL").Text
                    wvarCLIENNOM = wobjXMLRequest.selectSingleNode("//EPOLIZA[RAMOPCOD='" & wvarRAMOPCOD & "' and POLIZANN='" & wvarPOLIZANN & "' and POLIZSEC='" & wvarPOLIZSEC & "' and CERTIPOL='" & wvarCERTIPOL & "' and CERTIANN='" & wvarCERTIANN & "' and CERTISEC='" & wvarCERTISEC & "']/CLIENNOM").Text
                    wvarCLIENAPE = wobjXMLRequest.selectSingleNode("//EPOLIZA[RAMOPCOD='" & wvarRAMOPCOD & "' and POLIZANN='" & wvarPOLIZANN & "' and POLIZSEC='" & wvarPOLIZSEC & "' and CERTIPOL='" & wvarCERTIPOL & "' and CERTIANN='" & wvarCERTIANN & "' and CERTISEC='" & wvarCERTISEC & "']/CLIENAPE").Text
                    
                    wvarStep = 130
                    
                    wvarRequest = "<Request>" & _
                    "<DEFINICION>P_NBWS_Suscripciones_Insert.xml</DEFINICION>" & _
                        "<RAMOPCOD>" & wvarRAMOPCOD & "</RAMOPCOD>" & _
                        "<POLIZANN>" & wvarPOLIZANN & "</POLIZANN>" & _
                        "<POLIZSEC>" & wvarPOLIZSEC & "</POLIZSEC>" & _
                        "<CERTIPOL>" & wvarCERTIPOL & "</CERTIPOL>" & _
                        "<CERTIANN>" & wvarCERTIANN & "</CERTIANN>" & _
                        "<CERTISEC>" & wvarCERTISEC & "</CERTISEC>" & _
                        "<SUPLENUM>0</SUPLENUM>" & _
                        "<TIPO>EPOL</TIPO>" & _
                        "<DOCUMTIP>" & wvarDocumtip & "</DOCUMTIP>" & _
                        "<DOCUMDAT>" & wvarDocumdat & "</DOCUMDAT>" & _
                        "<EMAIL>" & wvarMail & "</EMAIL>" & _
                        "<VIA_SUSCRIPC>NBWS</VIA_SUSCRIPC>" & _
                        "<NOMBRE>" & wvarCLIENNOM & "</NOMBRE>" & _
                        "<APELLIDO>" & wvarCLIENAPE & "</APELLIDO>" & _
                    "</Request>"
                    
          wvarStep = 140
                    Set wobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
                    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
                    Set wobjClass = Nothing
                
                
            End If
        End If
                        
        Next
        wvarStep = 150
            
        wvarResponse_XML = wvarResponse_XML & "</EPOLIZAS>"
      
      
      
      
      
      Response = "<Response><Estado resultado=""true"" mensaje=""""></Estado><Response_XML>" & wvarResponse_XML & "</Response_XML><Response_HTML><![CDATA[" & wvarResponse_HTML & "]]></Response_HTML></Response>"
  
    
FalseAIS:
    If wvarResponse = "" Then
        Response = "<Response><Estado resultado=""false"" mensaje=""" & wvarFalseAIS & """></Estado><Response_XML></Response_XML><Response_HTML>" & wvarFalseAIS & "</Response_HTML></Response>"
    End If
    
   Set wobjXMLRequest = Nothing
   Set wobjXMLRespuestas = Nothing
   Set wobjXSLSalida = Nothing
   Set wobjXMLEpolizas = Nothing
   Set wobjXML_POLIZAS_Node = Nothing
  Set wobjElemento = Nothing


On Error Resume Next
 mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
      
   Set wobjXMLRequest = Nothing
   Set wobjXMLRespuestas = Nothing
   Set wobjXSLSalida = Nothing
   Set wobjXMLEpolizas = Nothing
   Set wobjXML_POLIZAS_Node = Nothing
  Set wobjElemento = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " - " & wvarRequest, _
                     vbLogEventTypeError
    '
    mobjCOM_Context.SetAbort
    IAction_Execute = 1
    '
End Function



   
'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
