VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_sImpresos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-----------------------------------------------------------------------------------------------------------------------------------
'TODO: poner COPYRIGHT
' *****************************************************************
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "nbwsA_Transacciones.nbwsA_sImpresos"
'
Const wcteXSL_sImpresos         As String = "XSLs\sImpresos.xsl"


' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLRespuestas   As MSXML2.DOMDocument
    Dim wobjXSLSalida       As MSXML2.DOMDocument
    '
    Dim wobjXML_PRODUCTOS_List  As MSXML2.IXMLDOMNodeList
    Dim wobjXML_PRODUCTOS_Node  As MSXML2.IXMLDOMNode
    Dim wobjElemento            As MSXML2.IXMLDOMElement
    '
    Dim wvarCount As Integer
    '
    Dim wvarStep            As Long
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wvarResponse_HTML   As String
    Dim wvarResponse_XML    As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanta las p�lizas a recorrer
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
        wobjXMLRequest.async = False
    Call wobjXMLRequest.loadXML(Request)
    '
    wvarStep = 20
    'Solo p�lizas est�n habilitadas para NBWS y si est�n habilitadas para ser navegadas en el sitio
    Set wobjXML_PRODUCTOS_List = wobjXMLRequest.selectNodes("Request/PRODUCTOS/PRODUCTO[./HABILITADO_NBWS = 'S' and ./HABILITADO_NAVEGACION = 'S' and ./POLIZA_EXCLUIDA = 'N']")
    wvarRequest = ""
    wvarCount = 1
    'Recorre cada p�liza
    For Each wobjXML_PRODUCTOS_Node In wobjXML_PRODUCTOS_List
        '
        wvarStep = 30
        'Se fija de qu� compa��a es la p�liza
        If (wobjXML_PRODUCTOS_Node.selectSingleNode("CIAASCOD").Text = "0020") Then
            '
            wvarStep = 40
            'P�liza de NYL: Arma XML de entrada a la funci�n Multithreading (muchos request)
            wvarRequest = wvarRequest & "<Request id=""" & wvarCount & """ actionCode=""nbwsA_getResuList"" ciaascod=""0020"">" & _
                                        "   <RAMOPCOD>" & wobjXML_PRODUCTOS_Node.selectSingleNode("RAMOPCOD").Text & "</RAMOPCOD>" & _
                                        "   <POLIZANN>" & wobjXML_PRODUCTOS_Node.selectSingleNode("POLIZANN").Text & "</POLIZANN>" & _
                                        "   <POLIZSEC>" & wobjXML_PRODUCTOS_Node.selectSingleNode("POLIZSEC").Text & "</POLIZSEC>" & _
                                        "   <CERTIPOL>" & wobjXML_PRODUCTOS_Node.selectSingleNode("CERTIPOL").Text & "</CERTIPOL>" & _
                                        "   <CERTIANN>" & wobjXML_PRODUCTOS_Node.selectSingleNode("CERTIANN").Text & "</CERTIANN>" & _
                                        "   <CERTISEC>" & wobjXML_PRODUCTOS_Node.selectSingleNode("CERTISEC").Text & "</CERTISEC>" & _
                                        "   <SUPLENUM>" & wobjXML_PRODUCTOS_Node.selectSingleNode("SUPLENUM").Text & "</SUPLENUM>" & _
                                        "</Request>"
        Else
            '
            wvarStep = 50
            'P�liza de LBA: Arma XML de entrada a la funci�n Multithreading (muchos request)
            wvarRequest = wvarRequest & "<Request id=""" & wvarCount & """ actionCode=""lbaw_OVVerifReimpresion"" ciaascod=""0001"">" & _
                                        "   <USUARIO></USUARIO>" & _
                                        "   <RAMOPCOD>" & wobjXML_PRODUCTOS_Node.selectSingleNode("RAMOPCOD").Text & "</RAMOPCOD>" & _
                                        "   <POLIZANN>" & wobjXML_PRODUCTOS_Node.selectSingleNode("POLIZANN").Text & "</POLIZANN>" & _
                                        "   <POLIZSEC>" & wobjXML_PRODUCTOS_Node.selectSingleNode("POLIZSEC").Text & "</POLIZSEC>" & _
                                        "   <CERTIPOL>" & wobjXML_PRODUCTOS_Node.selectSingleNode("CERTIPOL").Text & "</CERTIPOL>" & _
                                        "   <CERTIANN>" & wobjXML_PRODUCTOS_Node.selectSingleNode("CERTIANN").Text & "</CERTIANN>" & _
                                        "   <CERTISEC>" & wobjXML_PRODUCTOS_Node.selectSingleNode("CERTISEC").Text & "</CERTISEC>" & _
                                        "   <SUPLENUM>" & wobjXML_PRODUCTOS_Node.selectSingleNode("SUPLENUM").Text & "</SUPLENUM>" & _
                                        "</Request>"
        End If
        wvarCount = wvarCount + 1
        wobjXML_PRODUCTOS_List.nextNode
        '
    Next
    '
    wvarStep = 60
    'Ejecuta la funci�n Multithreading
    wvarRequest = "<Request>" & wvarRequest & "</Request>"
    Call cmdp_ExecuteTrnMulti("lbaw_OVVerifReimpresion", "lbaw_OVVerifReimpresion.biz", wvarRequest, wvarResponse)
    '
    wvarStep = 70
    'Carga las respuestas
    Set wobjXMLRespuestas = CreateObject("MSXML2.DOMDocument")
        wobjXMLRespuestas.async = False
    Call wobjXMLRespuestas.loadXML(wvarResponse)
    '
    wvarStep = 80
    'Verifica si pinch� el COM+
    If wobjXMLRespuestas.selectSingleNode("//Response") Is Nothing Then
        '
        wvarStep = 90
        'Pinch� el COM+
        Err.Description = "Fall� la ejecuci�n de la funci�n Multithreading."
        GoTo ErrorHandler
    Else
        '
        'Analiza las respuestas en base al request
        wobjXML_PRODUCTOS_List.Reset
        wvarCount = 1
        wvarResponse_HTML = ""
        For Each wobjXML_PRODUCTOS_Node In wobjXML_PRODUCTOS_List
            '
            wvarStep = 100
            If wobjXMLRespuestas.selectSingleNode("Response/Response[@id='" & wvarCount & "']") Is Nothing Then
                '
                wvarStep = 110
                'Pinch� el COM+ para ese PRODUCTO.
                'En este caso el XSL no va a tener el response, con lo cual no dibujar� ninguna impresorita.
            Else
                '
                wvarStep = 120
                Set wobjElemento = wobjXMLRespuestas.selectSingleNode("Response/Response[@id='" & wvarCount & "']").cloneNode(True)
                wobjXML_PRODUCTOS_Node.appendChild wobjElemento
                '
            End If
            '
            'Al nodo original le agrega la respuesta
            wvarStep = 130
            wvarResponse_XML = wvarResponse_XML & wobjXML_PRODUCTOS_Node.xml
            wvarCount = wvarCount + 1
            wobjXML_PRODUCTOS_List.nextNode
        Next
        '
    End If
    '
    wvarStep = 140
    'Levanta XSL para armar XML de salida
    Set wobjXSLSalida = CreateObject("MSXML2.DOMDocument")
        wobjXSLSalida.async = False
    Call wobjXSLSalida.Load(App.Path & "\" & wcteXSL_sImpresos)
    '
    wvarStep = 150
    'Devuelve respuesta en formato HTML
    wobjXMLRespuestas.loadXML ("<PRODUCTOS>" & wvarResponse_XML & "</PRODUCTOS>")
    wvarResponse_HTML = wobjXMLRespuestas.transformNode(wobjXSLSalida)
    '
    Response = "<Response><Estado resultado=""true"" mensaje=""""></Estado><Response_HTML><![CDATA[" & wvarResponse_HTML & "]]></Response_HTML></Response>"
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLRespuestas = Nothing
    Set wobjXML_PRODUCTOS_List = Nothing
    Set wobjXML_PRODUCTOS_Node = Nothing
    Set wobjElemento = Nothing
    Set wobjXSLSalida = Nothing
    '
    wvarStep = 190
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLRequest = Nothing
    Set wobjXMLRespuestas = Nothing
    Set wobjXML_PRODUCTOS_List = Nothing
    Set wobjXML_PRODUCTOS_Node = Nothing
    Set wobjElemento = Nothing
    Set wobjXSLSalida = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    mobjCOM_Context.SetAbort
    IAction_Execute = 1
    '
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

