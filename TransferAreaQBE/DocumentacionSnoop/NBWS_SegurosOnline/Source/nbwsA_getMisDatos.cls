VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_getMisDatos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

'-----------------------------------------------------------------------------------------------------------------------------------
'TODO: poner COPYRIGHT
' *****************************************************************
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName              As String = "nbwsA_Transacciones.nbwsA_getMisDatos"
Const mcteParam_EMAIL            As String = "//EMAIL"


'
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLDoc          As MSXML2.DOMDocument
    Dim oNodo               As MSXML2.IXMLDOMElement
    '
    Dim wvarStep            As Long
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarEMAIL           As String
    Dim wvarPREGUNTA        As String
    Dim wvarRespuesta       As String
    
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Carga XML de entrada
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
        wobjXMLRequest.async = False
        wobjXMLRequest.loadXML (Request)
    '
    wvarStep = 20
    wvarEMAIL = Trim(wobjXMLRequest.selectSingleNode(mcteParam_EMAIL).Text)
    '
    wvarStep = 30
    wvarRequest = "<Request><DEFINICION>P_NBWS_ObtenerPregResp.xml</DEFINICION><MAIL>" & wvarEMAIL & "</MAIL></Request>"
    '
    wvarStep = 40
    Set wobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    '
    wvarStep = 50
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        wobjXMLResponse.async = False
    Call wobjXMLResponse.loadXML(wvarResponse)
    '
    If Not wobjXMLResponse.selectSingleNode("//PREGUNTA") Is Nothing And Not wobjXMLResponse.selectSingleNode("//RESPUESTA") Is Nothing Then
        wvarStep = 60
        wvarPREGUNTA = CapicomDecrypt(wobjXMLResponse.selectSingleNode("//PREGUNTA").Text)
        wvarRespuesta = CapicomDecrypt(Trim(wobjXMLResponse.selectSingleNode("//RESPUESTA").Text))
    Else
        wvarStep = 70
        Err.Description = "No se encontraron datos en el SQL o pinch� el COM+ de SQL Generico"
        GoTo ErrorHandler
    End If
    '
    wvarStep = 80
    '
    Response = "<Response><Estado resultado=""true"" mensaje=""""></Estado>" & "<PREGUNTA>" & wvarPREGUNTA & "</PREGUNTA><RESPUESTA>" & wvarRespuesta & "</RESPUESTA></Response>"
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    '
    wvarStep = 90
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    mobjCOM_Context.SetAbort
    IAction_Execute = 1
    '
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
