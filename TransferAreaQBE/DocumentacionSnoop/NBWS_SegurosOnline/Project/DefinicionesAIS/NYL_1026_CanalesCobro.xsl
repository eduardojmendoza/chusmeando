<!--COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
			LIMITED 2009. ALL RIGHTS RESERVED
			
			This software is only to be used for the purpose for which it has been provided.
			No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
			system or translated in any human or computer language in any way or for any other
			purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
			Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
			offence, which can result in heavy fines and payment of substantial damages.
			
			Nombre del Fuente: NYL_1026_CanalesCobro.xsl
			Fecha de Creación: 13/07/2009
			
			PPcR:  50055/6010662
			
			Desarrollador: Leonardo Ruiz
			
			Descripción: Este archivo XSL se crea para obtener los canales de cobro.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template match="/">
		<OPTIONS>
			<xsl:for-each select="//REG">
				<xsl:sort select="./COBRODAB" order="ascending" data-type="text"/>
				<OPTION>
					<xsl:attribute name="value"><xsl:value-of select="COBROCOD"/></xsl:attribute>
					<xsl:value-of select="COBRODAB"/>
				</OPTION>
			</xsl:for-each>
		</OPTIONS>
	</xsl:template>
</xsl:stylesheet>
