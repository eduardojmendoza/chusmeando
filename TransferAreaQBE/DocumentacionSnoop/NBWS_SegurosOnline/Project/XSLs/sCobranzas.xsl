﻿<?xml version="1.0" encoding="UTF-8"?>
<!--
	COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
	LIMITED 2009. ALL RIGHTS RESERVED
	
	This software is only to be used for the purpose for which it has been provided.
	No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
	system or translated in any human or computer language in any way or for any other
	purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
	Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
	offence, which can result in heavy fines and payment of substantial damages.
	
	Nombre del Fuente: sCobranzas.xsl
	
	Fecha de Creación: 31/07/2009
	
	PPcR: 50055/6010662
	
	Desarrollador: Leonardo Ruiz
	
	Descripción: XSL para armar la solapa Cobranzas/Pagos.-
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:decimal-format name="europeo" decimal-separator="," grouping-separator="."/>
	<xsl:output method="html" encoding="iso-8859-1"/>
	<!-- ******************************************************************** -->
	<!-- TEMPLATE que dibuja tabla de solapa cobranzas -->
	<!-- ******************************************************************** -->
	<xsl:template match="/">
		<table cellspacing="0" cellpadding="0" style="width:800px">
			<tr>
				<th class="impresos">Detalle</th>
				<th class="producto">Libre Deuda</th>
				<th class="producto">Producto</th>
				<th class="poliza">Riesgo / Vida Asegurada</th>
			</tr>
			<xsl:for-each select="//PRODUCTO[COBRANZA = 'S']">
				<tr>
					<td class="impresos">
						<img id="imgMasMenos{position()}" src="../images/Mas.gif" border="0" onclick="javascript:fncMasMenos('{position()}');" style="cursor:hand;"/>
					</td>
					<td class="producto">
						<xsl:choose>
							<xsl:when test="(CIAASCOD = '0001') and (//PRODUCTO[RAMOPCOD=./RAMOPCOD and POLIZANN=./POLIZANN and POLIZSEC=./POLIZSEC and CERTIPOL=./CERTIPOL and CERTIANN=./CERTIANN and CERTISEC=./CERTISEC]//SWCONSTAN = 'S')">
								<img src="../images/boton_impresora.gif" alt="Libre Deuda" style="Cursor:hand">
									<xsl:attribute name="onclick">javascript:aImprimeDocumentos('<xsl:value-of select="RAMOPCOD"/>','<xsl:value-of select="POLIZANN"/>','<xsl:value-of select="POLIZSEC"/>','<xsl:value-of select="CERTIPOL"/>','<xsl:value-of select="CERTIANN"/>','<xsl:value-of select="CERTISEC"/>','CP')</xsl:attribute>
								</img>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="producto">
						<strong>
							<a>
								<xsl:attribute name="href">javascript:fncMasMenos('<xsl:value-of select="position()"/>');</xsl:attribute>
								<xsl:attribute name="style">cursor:hand;</xsl:attribute>
								<xsl:value-of select="RAMOPDES"/>
							</a>
						</strong>
					</td>
					<td class="poliza">
						<xsl:value-of select="TOMARIES"/>
						<!-- DA - 11/11/2009: LG pidió que se agregue el ID de póliza para los Generales de LBA -->
						<xsl:if test="TIPOPROD = 'G'">
							<xsl:if test="TOMARIES !=''">
								<br/>
							</xsl:if>
							<xsl:value-of select="RAMOPCOD"/>-<xsl:value-of select="format-number(POLIZANN, '00', 'europeo')"/>-<xsl:value-of select="format-number(POLIZSEC, '000000', 'europeo')"/>
						</xsl:if>
						<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
						<!-- DA - 10/11/2009: LG pidió por teléfono que esto aparezca solo para productos de NYL Masivos -->
						<xsl:if test="CIAASCOD = 20 and TIPOPROD = 'M'">
							<br/>(Cert <xsl:value-of select="CERTISEC"/>)
						</xsl:if>
						<!-- DA - 30/11/2009: defect 14 punto 22 a pedido de CV-->
						<xsl:if test="CIAASCOD = 20 and TIPOPROD = 'C'">
							<br/>(Póliza <xsl:value-of select="RAMOPCOD"/>-<xsl:value-of select="POLIZANN"/>-<xsl:value-of select="POLIZSEC"/>)
						</xsl:if>
					</td>
				</tr>
				<tr style="display:none;" id="trSubTable{position()}">
					<td>
						<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
					</td>
					<td colspan="3">
						<table id="subtable" style="width:720px">
							<xsl:choose>
								<xsl:when test="count(Response/CAMPOS/RECIBOS/RECIBO) = 0">
									<tr>
										<td style="background-color:#EEEEEE;font-family: Arial; font-size: 11px;">
											<xsl:choose>
												<xsl:when test="Response/Estado/@mensaje != ''">
													<em>
														<xsl:value-of select="Response/Estado/@mensaje"/>
													</em>
												</xsl:when>
												<xsl:otherwise>
													<em>La póliza no posee esta información disponible en HSBC Seguros On Line. </em>
												</xsl:otherwise>
											</xsl:choose>
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="count(Response/CAMPOS/RECIBOS/RECIBO) > 0">
									<tr>
										<xsl:choose>
											<!-- NYL -->
											<xsl:when test="CIAASCOD = '0020'">
												<th>Vigencia</th>
											</xsl:when>
										</xsl:choose>
										<th>Nro de <br/>Recibo</th>
										<th>Fecha de<br/> Vencimiento</th>
										<th>Canal <br/> Cobro</th>
										<th>Total <br/>Recibo</th>
										<th>Estado</th>
										<th>Fecha <br/>Cobro</th>
										<th>Cupón</th>
									</tr>
									<xsl:for-each select="Response/CAMPOS/RECIBOS/RECIBO">
										<tr>
											<!-- DA - 02/11/2009: -->
											<!-- DA - 12/11/2009: se quita esta validación. Defect 14 de LG -->
											<!--
											<xsl:if test="//FECHA_ACTUAL > concat(substring(FECHAVTO,7,4), substring(FECHAVTO,4,2), substring(FECHAVTO,1,2)) and ((SITUCREC = 3) or (SITUCREC = 4))">
												<xsl:attribute name="style">color: red</xsl:attribute>
											</xsl:if>
											-->
											<xsl:choose>
												<!-- NYL -->
												<xsl:when test="../../../../CIAASCOD = '0020'">
													<td>
														<xsl:value-of select="substring(FECHADDE,7,2)"/>/<xsl:value-of select="substring(FECHADDE,5,2)"/>/<xsl:value-of select="substring(FECHADDE,1,4)"/> al
														<xsl:value-of select="substring(FECHAHTA,7,2)"/>/<xsl:value-of select="substring(FECHAHTA,5,2)"/>/<xsl:value-of select="substring(FECHAHTA,1,4)"/>
													</td>
												</xsl:when>
											</xsl:choose>
											<td>
												<xsl:value-of select="RECIBANN"/>
												<xsl:value-of select="RECIBTIP"/>
												<xsl:value-of select="RECIBSEC"/>
											</td>
											<td>
												<xsl:value-of select="FECHAVTO"/>
											</td>
											<td>
												<xsl:value-of select="CANALCOBR"/>
											</td>
											<td>
												<xsl:value-of select="MONEDA"/>
												<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
												<xsl:value-of select="translate(RECTOIMP,'.',',')"/>
											</td>
											<td>
												<xsl:choose>
													<xsl:when test="SITUCREC = 1">COBRADO</xsl:when>
													<xsl:when test="SITUCREC = 2">EN PROCESO DE COBRO</xsl:when>
													<xsl:when test="SITUCREC = 3">COBRO RECHAZADO</xsl:when>
													<xsl:when test="SITUCREC = 4"/>
												</xsl:choose>
												<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
												<xsl:choose>
													<xsl:when test="SITUCREC = 2">
														<img src="../images/ico_faqs.gif" style="cursor: hand;">
															<xsl:choose>
																<xsl:when test="CANALCOBR = 'TARJ'">
																	<xsl:attribute name="onclick">javascript:alert_customizado('El recibo seleccionado se encuentra en proceso de envío a la Tarjeta de Crédito para luego proceder a su cobro.');</xsl:attribute>
																</xsl:when>
																<xsl:when test="CANALCOBR = 'DEBI'">
																	<xsl:attribute name="onclick">javascript:alert_customizado('El recibo seleccionado se encuentra en proceso de envío a la entidad bancaria correspondiente para luego proceder a su cobro.');</xsl:attribute>
																</xsl:when>
															</xsl:choose>
														</img>
													</xsl:when>
													<xsl:when test="SITUCREC = 3">
														<img src="../images/ico_faqs.gif" style="cursor: hand;">
															<xsl:attribute name="onclick">javascript:alert_customizado('No se ha podido efectuar el cobro del recibo seleccionado, debido al siguiente motivo: <xsl:value-of select="MOTIVDAB"/>');</xsl:attribute>
														</img>
													</xsl:when>
													<xsl:when test="SITUCREC = 4">
														<img src="../images/ico_faqs.gif" style="cursor: hand;">
															<xsl:attribute name="onclick">javascript:alert_customizado('Aún no se ha recibido el pago del recibo seleccionado.');</xsl:attribute>
														</img>
													</xsl:when>
												</xsl:choose>
											</td>
											<td>
												<xsl:value-of select="FECHACOB"/>
											</td>
											<td>
												<xsl:if test="SWIMPRES = 1">
													<img src="../images/boton_impresora.gif" alt="Cupon de Pago" style="Cursor:hand">
														<xsl:attribute name="onclick">javascript:aImprimeCupon('<xsl:value-of select="RECIBANN"/><xsl:value-of select="RECIBTIP"/><xsl:value-of select="RECIBSEC"/>', '<xsl:value-of select="../../../../CIAASCOD"/>')</xsl:attribute>
													</img>
												</xsl:if>
											</td>
										</tr>
									</xsl:for-each>
								</xsl:when>
							</xsl:choose>
						</table>
					</td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
</xsl:stylesheet>
