<?xml version="1.0" encoding="UTF-8"?>
<!--
	Fecha de Modificación: 18/12/2009
	PPCR: 
	Desarrollador: Daniel Armentano
	Descripción: Se habilita la columna ORDEN DE PAGO a pedido de LG.


	COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
	LIMITED 2009. ALL RIGHTS RESERVED
	
	This software is only to be used for the purpose for which it has been provided.
	No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
	system or translated in any human or computer language in any way or for any other
	purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
	Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
	offence, which can result in heavy fines and payment of substantial damages.
	
	Nombre del Fuente: sSiniestros.xsl
	
	Fecha de Creación: 31/07/2009
	
	PPcR: 50055/6010662
	
	Desarrollador: Leonardo Ruiz
	
	Descripción: XSL para armar la solapa de Siniestros.-
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:decimal-format name="europeo" decimal-separator="," grouping-separator="."/>
	<xsl:output method="html" encoding="iso-8859-1"/>
	<!-- ******************************************************************** -->
	<!-- TEMPLATE que dibuja tabla de solapa cobranzas -->
	<!-- ******************************************************************** -->
	<xsl:template match="/">
		<table cellspacing="0" cellpadding="0" style="width:800px">
			<xsl:choose>
				<xsl:when test="count(//PRODUCTO[SINIESTRO = 'S']) = 0">
					<tr>
						<th align="center">Tus pólizas no registran siniestros.</th>
					</tr>
				</xsl:when>
				<xsl:otherwise>
					<tr>
						<th class="impresos">Detalle</th>
						<th class="producto">Producto</th>
						<th class="poliza">Riesgo / Vida Asegurada</th>
					</tr>
					<xsl:for-each select="//PRODUCTO[SINIESTRO = 'S']">
						<tr>
							<td class="impresos">
								<img id="imgMasMenos{position()}" src="../images/Mas.gif" border="0" onclick="javascript:fncMasMenos('{position()}');" style="cursor:hand;"/>
							</td>
							<td class="producto">
								<strong>
									<a>
										<xsl:attribute name="href">javascript:fncMasMenos('<xsl:value-of select="position()"/>');</xsl:attribute>
										<xsl:attribute name="style">cursor:hand;</xsl:attribute>
										<xsl:value-of select="RAMOPDES"/>
									</a>
								</strong>
							</td>
							<td class="poliza">
								<xsl:value-of select="TOMARIES"/>
								<!-- DA - 11/11/2009: LG pidió que se agregue el ID de póliza para los Generales de LBA -->
								<xsl:if test="TIPOPROD = 'G'">
									<xsl:if test="TOMARIES !=''">
										<br/>
									</xsl:if>
									<xsl:value-of select="RAMOPCOD"/>-<xsl:value-of select="format-number(POLIZANN, '00', 'europeo')"/>-<xsl:value-of select="format-number(POLIZSEC, '000000', 'europeo')"/>
								</xsl:if>
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
								<!-- DA - 10/11/2009: LG pidió por teléfono que esto aparezca solo para productos de NYL Masivos -->
								<xsl:if test="CIAASCOD = 20 and TIPOPROD = 'M'">
									<br/>(Cert <xsl:value-of select="CERTISEC"/>)
								</xsl:if>
								<!-- DA - 30/11/2009: defect 14 punto 22 a pedido de CV-->
								<xsl:if test="CIAASCOD = 20 and TIPOPROD = 'C'">
									<br/>(Póliza <xsl:value-of select="RAMOPCOD"/>-<xsl:value-of select="POLIZANN"/>-<xsl:value-of select="POLIZSEC"/>)
								</xsl:if>
							</td>
						</tr>
						<tr style="display:none;" id="trSubTable{position()}">
							<td>
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							</td>
							<td colspan="3">
								<table id="subtable" style="width:720px">
									<xsl:choose>
										<xsl:when test="count(Response/CAMPOS/SINIESTROS/SINIESTRO) = 0">
											<tr>
												<td style="background-color:#EEEEEE;font-family: Arial;	font-size: 11px;">
													<em>La póliza no tiene siniestros registrados.</em>
												</td>
											</tr>
										</xsl:when>
										<xsl:when test="count(Response/CAMPOS/SINIESTROS/SINIESTRO) > 0">
											<tr>
												<xsl:choose>
													<!-- LBA -->
													<xsl:when test="CIAASCOD = '0001'">
														<th>Imprimir <br/>Denuncia</th>
													</xsl:when>
												</xsl:choose>
												<th>Fecha del <br/>Siniestro</th>
												<th>Fecha de <br/>Denuncia</th>
												<th>Nro de <br/>Siniestro</th>
												<th>Estado</th>
												<th>Orden de <br/>Pago</th>
											</tr>
											<xsl:for-each select="Response/CAMPOS/SINIESTROS/SINIESTRO">
												<xsl:sort select="FECSINIE" order="descending" data-type="number"/>
												<tr>
													<xsl:choose>
														<!-- LBA -->
														<xsl:when test="../../../../CIAASCOD = '0001'">
															<td>
																<img src="../images/boton_impresora.gif" style="cursor:hand;">
																	<xsl:attribute name="onclick">javascript:aImprimeDenuncia('<xsl:value-of select="concat(RAMOPCOD,'-',SINIEANN,'-',SINIENUM)"/>')</xsl:attribute>
																</img>
															</td>
														</xsl:when>
													</xsl:choose>
													<td>
														<xsl:value-of select="substring(FECSINIE,7,2)"/>-<xsl:value-of select="substring(FECSINIE,5,2)"/>-<xsl:value-of select="substring(FECSINIE,1,4)"/>
													</td>
													<td>
														<xsl:value-of select="substring(FECDENUN,7,2)"/>-<xsl:value-of select="substring(FECDENUN,5,2)"/>-<xsl:value-of select="substring(FECDENUN,1,4)"/>
													</td>
													<td>
														<xsl:value-of select="RAMOPCOD"/>-<xsl:value-of select="SINIEANN"/>-<xsl:value-of select="SINIENUM"/>
													</td>
													<td>
														<xsl:choose>
															<xsl:when test="ESTADSIN ='A'">ABIERTO</xsl:when>
															<xsl:when test="ESTADSIN ='C'">CERRADO</xsl:when>
															<xsl:otherwise>DEFINIR OTRO ESTADO</xsl:otherwise>
														</xsl:choose>
													</td>
													<td>
														<xsl:if test="CANTORD > 0">
															<img src="../images/boton_imprimir.gif" onmouseover="this.style.cursor='hand';">
																<xsl:attribute name="onclick">fncVerOrdenes(this,'<xsl:value-of select="ORDENES"/>', '<xsl:value-of select="CANTORD "/>','<xsl:value-of select="../../../../CIAASCOD"/>')</xsl:attribute>
															</img>
														</xsl:if>
													</td>
												</tr>
											</xsl:for-each>
										</xsl:when>
									</xsl:choose>
								</table>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</table>
	</xsl:template>
</xsl:stylesheet>
