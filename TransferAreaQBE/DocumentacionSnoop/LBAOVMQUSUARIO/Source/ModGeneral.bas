Attribute VB_Name = "ModGeneral"
Option Explicit

' Parametros XML de Configuracion
Public Const gcteConfFileName       As String = "LBAVirtualMQConfig.xml"
Public Const gcteQueueManager       As String = "//QUEUEMANAGER"
Public Const gctePutQueue           As String = "//PUTQUEUE"
Public Const gcteGetQueue           As String = "//GETQUEUE"
Public Const gcteGMOWaitInterval    As String = "//GMO_WAITINTERVAL"

Public Const gcteClassMQConnection  As String = "WD.Frame2MQ"

' Parametros XML del Cotizador
Public Const gcteParamFileName      As String = "ParametrosMQ.xml"
Public Const gcteNodosHogar         As String = "//HOGAR"
Public Const gcteNodosAutoScoring   As String = "//AUTOSCORING"
Public Const gcteNodosGenerales     As String = "//GENERALES"
Public Const gcteRAMOPCOD           As String = "/RAMOPCOD"
Public Const gctePLANNCOD           As String = "/PLANNCOD"
Public Const gctePOLIZSEC           As String = "/POLIZSEC"
Public Const gctePOLIZANN           As String = "/POLIZANN"
Public Const gcteBANCOCOD           As String = "/BANCOCOD"
Public Const gcteSUCURCOD           As String = "/SUCURCOD"
Public Const gcteCIAASCOD           As String = "/CIAASCOD"

Function FormatearNumero(ByVal pvarNumero) As String
    Dim wobjXML As MSXML2.DOMDocument
    Dim wobjXSL As MSXML2.DOMDocument
    
    Set wobjXML = New MSXML2.DOMDocument
    wobjXML.async = False
    wobjXML.loadXML "<PARAM>" & pvarNumero & "</PARAM>"
    
    Set wobjXSL = New MSXML2.DOMDocument
    wobjXSL.async = False
    wobjXSL.loadXML p_GetXSLNumero
    
    FormatearNumero = Replace(wobjXML.transformNode(wobjXSL), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
End Function

Private Function p_GetXSLNumero() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select=""format-number(number(PARAM), '###.###,00', 'european')""/>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLNumero = wvarStrXSL
End Function

