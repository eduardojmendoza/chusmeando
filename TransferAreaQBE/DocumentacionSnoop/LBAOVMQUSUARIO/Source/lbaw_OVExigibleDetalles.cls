VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVExigibleDet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQUsuario.lbaw_OVExigibleDetalles"
Const mcteOpID              As String = "1401"

'Parametros XML de Entrada
Const mcteParam_Usuario      As String = "//USUARIO"
Const mcteParam_NivelAs      As String = "//NIVELAS"
Const mcteParam_ClienSecAs   As String = "//CLIENSECAS"
Const mcteParam_Nivel1       As String = "//NIVEL1"
Const mcteParam_ClienSec1    As String = "//CLIENSEC1"
Const mcteParam_Nivel2       As String = "//NIVEL2"
Const mcteParam_ClienSec2    As String = "//CLIENSEC2"
Const mcteParam_Nivel3       As String = "//NIVEL3"
Const mcteParam_ClienSec3    As String = "//CLIENSEC3"
Const mcteParam_Producto     As String = "//PRODUCTO"
Const mcteParam_Poliza       As String = "//POLIZA"
Const mcteParam_NroConsu     As String = "//NROCONS"
Const mcteParam_NroOrden     As String = "//NROORDEN"
Const mcteParam_DirOrden     As String = "//DIRORDEN"
Const mcteParam_TFiltro      As String = "//TFILTRO"
Const mcteParam_VFiltro      As String = "//VFILTRO"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarResultTR        As String
    Dim wvarNivelAs         As String
    Dim wvarClienSecAs      As String
    Dim wvarNivel1          As String
    Dim wvarClienSec1       As String
    Dim wvarNivel2          As String
    Dim wvarClienSec2       As String
    Dim wvarNivel3          As String
    Dim wvarClienSec3       As String
    Dim wvarCiaAsCod        As String
    Dim wvarProducto        As String
    Dim wvarPoliza          As String
    Dim wvarUsuario         As String
    Dim wvarNroConsu        As String
    Dim wvarNroOrden        As String
    Dim wvarDirOrden        As String
    Dim wvarTFiltro         As String
    Dim wvarVFiltro         As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarNivelAs = Left(.selectSingleNode(mcteParam_NivelAs).Text & Space(2), 2)
        wvarClienSecAs = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSecAs).Text, 9)
        wvarNivel1 = Left(.selectSingleNode(mcteParam_Nivel1).Text & Space(2), 2)
        wvarClienSec1 = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSec1).Text, 9)
        wvarNivel2 = Left(.selectSingleNode(mcteParam_Nivel2).Text & Space(2), 2)
        wvarClienSec2 = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSec2).Text, 9)
        wvarNivel3 = Left(.selectSingleNode(mcteParam_Nivel3).Text & Space(2), 2)
        wvarClienSec3 = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSec3).Text, 9)
        wvarProducto = Left(.selectSingleNode(mcteParam_Producto).Text & Space(4), 4)
        wvarPoliza = Right(String(22, "0") & .selectSingleNode(mcteParam_Poliza).Text, 22)
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarNroConsu = Right(String(4, "0") & .selectSingleNode(mcteParam_NroConsu).Text, 4)
        wvarNroOrden = Right(String(2, "0") & .selectSingleNode(mcteParam_NroOrden).Text, 2)
        If .selectNodes(mcteParam_DirOrden).length <> 0 Then
            wvarDirOrden = Left(.selectSingleNode(mcteParam_DirOrden).Text & "A", 1)
        Else
            wvarDirOrden = "A"
        End If
        wvarTFiltro = Right("0" & .selectSingleNode(mcteParam_TFiltro).Text, 1)
        wvarVFiltro = Left(.selectSingleNode(mcteParam_VFiltro).Text & Space(4), 4)
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los Parametros de la cola de MQ del archivo de configuración
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.Load (App.Path & "\" & gcteParamFileName)
    wvarCiaAsCod = wobjXMLParametros.selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    '
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & Space(4) & wvarClienSecAs & wvarNivelAs & wvarNivel1 & wvarClienSec1 & wvarNivel2 & wvarClienSec2 & wvarNivel3 & wvarClienSec3 & "1" & wvarNroConsu & wvarNroOrden & wvarDirOrden & wvarProducto & wvarPoliza & wvarTFiltro & wvarVFiltro
    wvarStep = 100
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 50
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
        
    wvarStep = 190
    wvarResult = ""
    wvarResultTR = ""
    wvarPos = 106  'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 200
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 210
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 230
        wvarResultTR = wvarResultTR & "<MSGEST>" & wvarEstado & "</MSGEST>"
        wvarResultTR = wvarResultTR & "<PRODUCTO>" & Mid(strParseString, 75, 4) & "</PRODUCTO>"
        wvarResultTR = wvarResultTR & "<POLIZA>" & Mid(strParseString, 79, 22) & "</POLIZA>"
        wvarResultTR = wvarResultTR & "<NROCONS>" & Mid(strParseString, 68, 4) & "</NROCONS>"
        wvarResultTR = wvarResultTR & "<NROORDEN>" & Mid(strParseString, 72, 2) & "</NROORDEN>"
        wvarResultTR = wvarResultTR & "<DIRORDEN>" & Mid(strParseString, 74, 1) & "</DIRORDEN>"
        wvarResultTR = wvarResultTR & "<TFILTRO>" & Mid(strParseString, 101, 1) & "</TFILTRO>"
        wvarResultTR = wvarResultTR & "<VFILTRO>" & Mid(strParseString, 102, 4) & "</VFILTRO>"
        '
        If CLng(wvarNroConsu) <> 0 Then
            wvarResultTR = wvarResultTR & "<PAGINADO>1</PAGINADO>"
        Else
            wvarResultTR = wvarResultTR & "<PAGINADO>0</PAGINADO>"
        End If
        '
        wvarStep = 240
        wvarResult = ""
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        '
        wvarStep = 250
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 260
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML (wvarResult)
        '
        If wobjXMLResponse.selectNodes("//R").length <> 0 Then
            wvarStep = 270
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSL())
            '
            wvarStep = 280
            wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
            '
            wvarStep = 290
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
            '
            wvarStep = 300
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResultTR & wvarResult & "</Response>"
            '
        Else
            '
            wvarStep = 310
            Set wobjXMLResponse = Nothing
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>"
            '
        End If
    End If
    '
    wvarStep = 320
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & Space(4) & wvarClienSecAs & wvarNivelAs & wvarNivel1 & wvarClienSec1 & wvarNivel2 & wvarClienSec2 & wvarNivel3 & wvarClienSec3 & "1" & wvarNroConsu & wvarNroOrden & wvarProducto & wvarPoliza & wvarTFiltro & wvarVFiltro & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarstrLen As Long) As String
Dim wvarResult As String
Dim wvarCantLin As Long
Dim wvarCounter As Long
    '
    wvarResult = wvarResult & "<RS>"
    '
    If Trim(Mid(strParseString, wvarPos, 4)) <> "" Then
        wvarCantLin = Mid(strParseString, wvarPos, 4)
        wvarPos = wvarPos + 4
        '
        For wvarCounter = 0 To wvarCantLin - 1
            wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 149) & "]]></R>"
            wvarPos = wvarPos + 149
        Next
    '
    End If
    wvarResult = wvarResult & "</RS>"
    '
    ParseoMensaje = wvarResult
    '
End Function
Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/'> "
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REGS'>"
    wvarStrXSL = wvarStrXSL & "              <xsl:apply-templates select='/RS/R[substring(.,1,6) != ""      ""]'/>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='TOTS'>"
    wvarStrXSL = wvarStrXSL & "              <xsl:apply-templates select='/RS/R[substring(.,1,6) = ""      ""]'/>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    ' DETALLE
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RS/R[substring(.,1,6) != ""      ""]'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REG'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='AGE'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,1,2)' />-<xsl:value-of select='substring(.,3,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='SINI'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,7,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='RAMO'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,8,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CLIDES'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,9,30))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='PROD'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,39,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='POL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,43,8)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERPOL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,51,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERANN'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,55,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERSEC'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,59,6)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='MON'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,65,3))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
'    wvarStrXSL = wvarStrXSL & "             <xsl:element name='IMPTOS'>"
'    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,68,1) ='-'"">"
'    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,68,1)' />"
'    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
'    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='IMPTO'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,68,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,68,1)' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,69,14)) div 100), ""########0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
'    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_30S'>"
'    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,83,1) ='-'"">"
'    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,83,1)' />"
'    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
'    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_30'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,83,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,83,1)' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,84,14)) div 100), ""########0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
'    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_60S'>"
'    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,98,1) ='-'"">"
'    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,98,1)' />"
'    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
'    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_60'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,98,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,98,1)' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,99,14)) div 100), ""########0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
'    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_90S'>"
'    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,113,1) ='-'"">"
'    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,113,1)' />"
'    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
'    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_90'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,113,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,113,1)' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,114,14)) div 100), ""########0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
'    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_M90S'>"
'    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,128,1) ='-'"">"
'    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,128,1)' />"
'    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
'    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_M90'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,128,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,128,1)' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,129,14)) div 100), ""########0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='EST'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,143,3))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='COB'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,146,3))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    ' TOTALES
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RS/R[substring(.,1,6) = ""      ""]'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='TOT'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='TMON'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,65,3)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='T_IMPTOS'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,68,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,68,1)' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='T_IMPTO'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,69,14)) div 100), ""###.###.##0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='T_30S'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,83,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,83,1)' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='T_30'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,84,14)) div 100), ""###.###.##0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='T_60S'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,98,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,98,1)' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='T_60'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,99,14)) div 100), ""###.###.##0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='T_90S'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,113,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,113,1)' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='T_90'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,114,14)) div 100), ""###.###.##0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='T_M90S'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,128,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,128,1)' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='T_M90'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,129,14)) div 100), ""###.###.##0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIDES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='MON'/>"
    '
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
    '
End Function



