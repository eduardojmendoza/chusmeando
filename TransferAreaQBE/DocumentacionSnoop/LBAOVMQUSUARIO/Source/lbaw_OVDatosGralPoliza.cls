VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVDatosGralPol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQUsuario.lbaw_OVDatosGralPol"
Const mcteOpID              As String = "1101"

'Parametros XML de Entrada
Const mcteParam_Usuario    As String = "//USUARIO"
Const mcteParam_Poliza     As String = "//POLIZA"
Const mcteParam_Producto   As String = "//PRODUCTO"
Const mcteParam_Suplenum   As String = "//SUPLENUM"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarProducto        As String
    Dim wvarPoliza          As String
    Dim wvarSuplenum        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarProducto = Left(.selectSingleNode(mcteParam_Producto).Text & Space(4), 4)
        wvarPoliza = Right(String(22, "0") & .selectSingleNode(mcteParam_Poliza).Text, 22)
        If .selectNodes(mcteParam_Suplenum).length <> 0 Then
            wvarSuplenum = Right(String(4, "0") & .selectSingleNode(mcteParam_Suplenum).Text, 4)
        Else
            wvarSuplenum = String(4, "0")
        End If
        '
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing

    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los Parametros de la cola de MQ del archivo de configuración
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.Load (App.Path & "\" & gcteParamFileName)
    wvarCiaAsCod = wobjXMLParametros.selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarProducto & wvarPoliza & wvarSuplenum
    wvarStep = 180
    wvarResult = ""
    wvarPos = 97  'cantidad de caracteres ocupados por parámetros de entrada
        
    wvarStep = 100
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 8
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 190
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    If wvarEstado = "OK" Then
        '
        wvarStep = 200
        wvarResult = wvarResult & "<DATOS_POLIZA>"
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        '
        wvarStep = 210
        wvarResult = wvarResult & "</DATOS_POLIZA>"
        '
        wvarStep = 220
        Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
    Else
        wvarStep = 230
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
    End If
    '
    wvarStep = 240
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS MQ
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarProducto & wvarPoliza & wvarSuplenum & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarstrLen As Long) As String
Dim wvarResult As String
        
    'Es un solo dato.
    wvarResult = wvarResult & "<ALERT_OP>" & Mid(strParseString, wvarPos, 1) & "</ALERT_OP>"
    wvarResult = wvarResult & "<CLIENSEC>" & Mid(strParseString, wvarPos + 1, 9) & "</CLIENSEC>"
    wvarResult = wvarResult & "<CLIENDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 10, 30)) & "]]></CLIENDES>"
    wvarResult = wvarResult & "<ESTADO><![CDATA[" & Trim(Mid(strParseString, wvarPos + 46, 30)) & "]]></ESTADO>"
    wvarResult = wvarResult & "<ACREEDOR>" & Mid(strParseString, wvarPos + 76, 2) & "</ACREEDOR>"
    wvarResult = wvarResult & "<FEC_INI><![CDATA[" & Mid(strParseString, wvarPos + 78, 10) & "]]></FEC_INI>"
    wvarResult = wvarResult & "<FEC_RENOV><![CDATA[" & Mid(strParseString, wvarPos + 88, 23) & "]]></FEC_RENOV>"
    wvarResult = wvarResult & "<CANAL_COB><![CDATA[" & Trim(Mid(strParseString, wvarPos + 115, 20)) & "]]></CANAL_COB>"
    wvarResult = wvarResult & "<TIPO_CUE><![CDATA[" & Mid(strParseString, wvarPos + 137, 20) & "]]></TIPO_CUE>"
    wvarResult = wvarResult & "<NRO_CUE>" & Mid(strParseString, wvarPos + 157, 24) & "</NRO_CUE>"
    wvarResult = wvarResult & "<CAMPA_COD>" & Mid(strParseString, wvarPos + 181, 4) & "</CAMPA_COD>"
    wvarResult = wvarResult & "<CAMPA_DES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 185, 60)) & "]]></CAMPA_DES>"
    wvarResult = wvarResult & "<AFINIDAD><![CDATA[" & Trim(Mid(strParseString, wvarPos + 245, 60)) & "]]></AFINIDAD>"

    ParseoMensaje = wvarResult

End Function















