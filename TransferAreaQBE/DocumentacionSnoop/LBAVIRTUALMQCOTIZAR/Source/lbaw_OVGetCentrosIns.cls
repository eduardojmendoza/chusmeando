VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVGetCentrosIns"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQCotizar.lbaw_OVGetCentrosIns"
Const mcteOpID              As String = "1512"

'Parametros XML de Entrada
Const mcteParam_Usuario         As String = "//USUARIO"
Const mcteParam_Agentcod        As String = "//AGENTCOD"
Const mcteParam_AgentCla        As String = "//AGENTCLA"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarAgentcod        As String
    Dim wvarAgentCla        As String
    '
    Dim strParseString      As String
    Dim wvarError           As String
    Dim wvarPos             As Long
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarStep = 20
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarAgentcod = Right(String(4, "0") & .selectSingleNode(mcteParam_Agentcod).Text, 4)
        If .selectNodes(mcteParam_AgentCla).length > 0 Then
            wvarAgentCla = .selectSingleNode(mcteParam_AgentCla).Text
        Else
            If wvarAgentcod <> "0000" Then
                wvarAgentCla = "PR"
            Else
                wvarAgentCla = "  "
            End If
        End If
        '
    End With
    '
    wvarStep = 30
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 40
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 70
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 80
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 90
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarAgentcod & wvarAgentCla
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarPos = 73 'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarStep = 200
    If Mid(strParseString, 19, 2) = "ER" Then
        '
        wvarStep = 210
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 220
        wvarResult = ""
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString)
        '
        wvarStep = 230
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 240
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML (wvarResult)
        '
        If wobjXMLResponse.selectNodes("//R").length <> 0 Then
            wvarStep = 250
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSL())
            '
            wvarStep = 260
            wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
            '
            wvarStep = 270
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
            '
            wvarStep = 280
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
        Else
            wvarStep = 285
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>"
        End If
        '
        '
    End If
    '
    wvarStep = 290
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RS/R'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='OPTION'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='value'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,1,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='domic'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,35,40))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='nro'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,75,5))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='codpos'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,80,8))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='local'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,88,40))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='prov'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,130,20))' />"
    wvarStrXSL = wvarStrXSL & "                </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='tel'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,152,30))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='hora'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,182,30))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:value-of select='normalize-space(substring(.,5,30))' />"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function
Private Function ParseoMensaje(wvarPos As Long, strParseString As String) As String
Dim wvarResult As String
Dim wvarCant As Integer
Dim i As Integer

    '
    wvarResult = wvarResult & "<RS>"
    '
    If Trim(Mid(strParseString, wvarPos, 3)) <> "000" And Trim(Mid(strParseString, wvarPos, 3)) <> "" Then
        wvarCant = CInt(Trim(Mid(strParseString, wvarPos, 3)))
        wvarPos = wvarPos + 3
        For i = 1 To wvarCant
            wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 211) & "]]></R>"
            wvarPos = wvarPos + 211
        Next
    End If

    '
    wvarResult = wvarResult & "</RS>"
    '
    ParseoMensaje = wvarResult
    '
End Function






