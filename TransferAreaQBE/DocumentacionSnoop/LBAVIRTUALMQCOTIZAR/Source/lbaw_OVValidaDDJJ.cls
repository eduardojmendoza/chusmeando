VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVValidaDDJJ"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQCotizar.lbaw_OVValidaDDJJ"
Const mcteOpID              As String = "1515"

'Parametros XML de Entrada
Const mcteParam_Usuario             As String = "//USUARIO"
Const mcteParam_Estado              As String = "//ESTADO"
Const mcteParam_Uso                 As String = "//USO"
Const mcteParam_Abollado            As String = "//ABOLLADO"
Const mcteParam_Picado              As String = "//PICADO"
Const mcteParam_Masillado           As String = "//MASILLADO"
Const mcteParam_Roto                As String = "//ROTO"
Const mcteParam_Faltantes           As String = "//FALTANTES"
Const mcteParam_Luces               As String = "//LUCES"
Const mcteParam_Odometro            As String = "//ODOMETRO"
Const mcteParam_AB_Paragolpe        As String = "//AB_PARAGOLPE"
Const mcteParam_AB_Par_Del          As String = "//AB_PAR_DEL"
Const mcteParam_AB_Par_Tra          As String = "//AB_PAR_TRA"
Const mcteParam_AB_Capot            As String = "//AB_CAPOT"
Const mcteParam_AB_Techo            As String = "//AB_TECHO"
Const mcteParam_AB_TBaul            As String = "//AB_TBAUL"
Const mcteParam_AB_Guardabarro      As String = "//AB_GUARDABARRO"
Const mcteParam_AB_Guar_Del         As String = "//AB_GUAR_DEL"
Const mcteParam_AB_Guar_Del_Der     As String = "//AB_GUAR_DEL_DER"
Const mcteParam_AB_Guar_Del_Izq     As String = "//AB_GUAR_DEL_IZQ"
Const mcteParam_AB_Guar_Tra         As String = "//AB_GUAR_TRA"
Const mcteParam_AB_Guar_Tra_Der     As String = "//AB_GUAR_TRA_DER"
Const mcteParam_AB_Guar_Tra_Izq     As String = "//AB_GUAR_TRA_IZQ"
Const mcteParam_AB_Puerta           As String = "//AB_PUERTA"
Const mcteParam_AB_Puer_Del         As String = "//AB_PUER_DEL"
Const mcteParam_AB_Puer_Del_Der     As String = "//AB_PUER_DEL_DER"
Const mcteParam_AB_Puer_Del_Izq     As String = "//AB_PUER_DEL_IZQ"
Const mcteParam_AB_Puer_Tra         As String = "//AB_PUER_TRA"
Const mcteParam_AB_Puer_Tra_Der     As String = "//AB_PUER_TRA_DER"
Const mcteParam_AB_Puer_Tra_Izq     As String = "//AB_PUER_TRA_IZQ"
Const mcteParam_PIC_Capot           As String = "//PIC_CAPOT"
Const mcteParam_PIC_Techo           As String = "//PIC_TECHO"
Const mcteParam_PIC_TBaul           As String = "//PIC_TBAUL"
Const mcteParam_PIC_Guardabarro     As String = "//PIC_GUARDABARRO"
Const mcteParam_PIC_Guar_Del        As String = "//PIC_GUAR_DEL"
Const mcteParam_PIC_Guar_Del_Der    As String = "//PIC_GUAR_DEL_DER"
Const mcteParam_PIC_Guar_Del_Izq    As String = "//PIC_GUAR_DEL_IZQ"
Const mcteParam_PIC_Guar_Tra        As String = "//PIC_GUAR_TRA"
Const mcteParam_PIC_Guar_Tra_Der    As String = "//PIC_GUAR_TRA_DER"
Const mcteParam_PIC_Guar_Tra_Izq    As String = "//PIC_GUAR_TRA_IZQ"
Const mcteParam_PIC_Puerta          As String = "//PIC_PUERTA"
Const mcteParam_PIC_Puer_Del        As String = "//PIC_PUER_DEL"
Const mcteParam_PIC_Puer_Del_Der    As String = "//PIC_PUER_DEL_DER"
Const mcteParam_PIC_Puer_Del_Izq    As String = "//PIC_PUER_DEL_IZQ"
Const mcteParam_PIC_Puer_Tra        As String = "//PIC_PUER_TRA"
Const mcteParam_PIC_Puer_Tra_Der    As String = "//PIC_PUER_TRA_DER"
Const mcteParam_PIC_Puer_Tra_Izq    As String = "//PIC_PUER_TRA_IZQ"
Const mcteParam_PIC_Zocalo          As String = "//PIC_ZOCALO"
Const mcteParam_PIC_Zoc_Der         As String = "//PIC_ZOC_DER"
Const mcteParam_PIC_Zoc_Izq         As String = "//PIC_ZOC_IZQ"
Const mcteParam_MAS_Capot           As String = "//MAS_CAPOT"
Const mcteParam_MAS_Techo           As String = "//MAS_TECHO"
Const mcteParam_MAS_TBaul           As String = "//MAS_TBAUL"
Const mcteParam_MAS_Guardabarro     As String = "//MAS_GUARDABARRO"
Const mcteParam_MAS_Guar_Del        As String = "//MAS_GUAR_DEL"
Const mcteParam_MAS_Guar_Del_Der    As String = "//MAS_GUAR_DEL_DER"
Const mcteParam_MAS_Guar_Del_Izq    As String = "//MAS_GUAR_DEL_IZQ"
Const mcteParam_MAS_Guar_Tra        As String = "//MAS_GUAR_TRA"
Const mcteParam_MAS_Guar_Tra_Der    As String = "//MAS_GUAR_TRA_DER"
Const mcteParam_MAS_Guar_Tra_Izq    As String = "//MAS_GUAR_TRA_IZQ"
Const mcteParam_MAS_Puerta          As String = "//MAS_PUERTA"
Const mcteParam_MAS_Puer_Del        As String = "//MAS_PUER_DEL"
Const mcteParam_MAS_Puer_Del_Der    As String = "//MAS_PUER_DEL_DER"
Const mcteParam_MAS_Puer_Del_Izq    As String = "//MAS_PUER_DEL_IZQ"
Const mcteParam_MAS_Puer_Tra        As String = "//MAS_PUER_TRA"
Const mcteParam_MAS_Puer_Tra_Der    As String = "//MAS_PUER_TRA_DER"
Const mcteParam_MAS_Puer_Tra_Izq    As String = "//MAS_PUER_TRA_IZQ"
Const mcteParam_MAS_Zocalo          As String = "//MAS_ZOCALO"
Const mcteParam_MAS_Zoc_Der         As String = "//MAS_ZOC_DER"
Const mcteParam_MAS_Zoc_Izq         As String = "//MAS_ZOC_IZQ"
Const mcteParam_ROT_Paragolpe       As String = "//ROT_PARAGOLPE"
Const mcteParam_ROT_Par_Del         As String = "//ROT_PAR_DEL"
Const mcteParam_ROT_Par_Tra         As String = "//ROT_PAR_TRA"
Const mcteParam_ROT_Rejilla         As String = "//ROT_REJILLA"
Const mcteParam_ROT_Optica          As String = "//ROT_OPTICA"
Const mcteParam_ROT_Opt_Der         As String = "//ROT_OPT_DER"
Const mcteParam_ROT_Opt_Izq         As String = "//ROT_OPT_IZQ"
Const mcteParam_ROT_FaroGiro        As String = "//ROT_FAROGIRO"
Const mcteParam_ROT_FG_Del          As String = "//ROT_FG_DEL"
Const mcteParam_ROT_FG_Del_Der      As String = "//ROT_FG_DEL_DER"
Const mcteParam_ROT_FG_Del_Izq      As String = "//ROT_FG_DEL_IZQ"
Const mcteParam_ROT_FG_Tra          As String = "//ROT_FG_TRA"
Const mcteParam_ROT_FG_Tra_Der      As String = "//ROT_FG_TRA_DER"
Const mcteParam_ROT_FG_Tra_Izq      As String = "//ROT_FG_TRA_IZQ"
Const mcteParam_ROT_Luneta          As String = "//ROT_LUNETA"
Const mcteParam_ROT_Parabrisas      As String = "//ROT_PARABRISAS"
Const mcteParam_FAL_Paragolpe       As String = "//FAL_PARAGOLPE"
Const mcteParam_FAL_Par_Del         As String = "//FAL_PAR_DEL"
Const mcteParam_FAL_Par_Tra         As String = "//FAL_PAR_TRA"
Const mcteParam_FAL_Rejilla         As String = "//FAL_REJILLA"
Const mcteParam_FAL_Optica          As String = "//FAL_OPTICA"
Const mcteParam_FAL_Opt_Der         As String = "//FAL_OPT_DER"
Const mcteParam_FAL_Opt_Izq         As String = "//FAL_OPT_IZQ"
Const mcteParam_FAL_FaroGiro        As String = "//FAL_FAROGIRO"
Const mcteParam_FAL_FG_Del          As String = "//FAL_FG_DEL"
Const mcteParam_FAL_FG_Del_Der      As String = "//FAL_FG_DEL_DER"
Const mcteParam_FAL_FG_Del_Izq      As String = "//FAL_FG_DEL_IZQ"
Const mcteParam_FAL_FG_Tra          As String = "//FAL_FG_TRA"
Const mcteParam_FAL_FG_Tra_Der      As String = "//FAL_FG_TRA_DER"
Const mcteParam_FAL_FG_Tra_Izq      As String = "//FAL_FG_TRA_IZQ"
Const mcteParam_LUC_Alta            As String = "//LUC_ALTA"
Const mcteParam_LUC_Baja            As String = "//LUC_BAJA"
Const mcteParam_LUC_Giro            As String = "//LUC_GIRO"
Const mcteParam_LUC_Gir_Del         As String = "//LUC_GIR_DEL"
Const mcteParam_LUC_Gir_Del_Der     As String = "//LUC_GIR_DEL_DER"
Const mcteParam_LUC_Gir_Del_Izq     As String = "//LUC_GIR_DEL_IZQ"
Const mcteParam_LUC_Gir_Tra         As String = "//LUC_GIR_TRA"
Const mcteParam_LUC_Gir_Tra_Der     As String = "//LUC_GIR_TRA_DER"
Const mcteParam_LUC_Gir_Tra_Izq     As String = "//LUC_GIR_TRA_IZQ"
Const mcteParam_LUC_Posicion        As String = "//LUC_POSICION"
Const mcteParam_LUC_Pos_Del         As String = "//LUC_POS_DEL"
Const mcteParam_LUC_Pos_Del_Der     As String = "//LUC_POS_DEL_DER"
Const mcteParam_LUC_Pos_Del_Izq     As String = "//LUC_POS_DEL_IZQ"
Const mcteParam_LUC_Pos_Tra         As String = "//LUC_POS_TRA"
Const mcteParam_LUC_Pos_Tra_Der     As String = "//LUC_POS_TRA_DER"
Const mcteParam_LUC_Pos_Tra_Izq     As String = "//LUC_POS_TRA_IZQ"
Const mcteParam_LUC_Stop            As String = "//LUC_STOP"
Const mcteParam_NEU_Est_Del_Der     As String = "//NEU_EST_DEL_DER"
Const mcteParam_NEU_Est_Del_Izq     As String = "//NEU_EST_DEL_IZQ"
Const mcteParam_NEU_Est_Tra_Der     As String = "//NEU_EST_TRA_DER"
Const mcteParam_NEU_Est_Tra_Izq     As String = "//NEU_EST_TRA_IZQ"
Const mcteParam_FuncKil             As String = "//FUNCKIL"


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarEstado          As String
    Dim wvarUso             As String
    Dim wvarAbollado        As String
    Dim wvarPicado          As String
    Dim wvarMasillado       As String
    Dim wvarRoto            As String
    Dim wvarFaltantes       As String
    Dim wvarLuces           As String
    Dim wvarOdometro        As String
    Dim wvarAB_Paragolpe    As String
    Dim wvarAB_Par_Del      As String
    Dim wvarAB_Par_Tra      As String
    Dim wvarAB_Capot        As String
    Dim wvarAB_Techo        As String
    Dim wvarAB_TBaul        As String
    Dim wvarAB_Guardabarro  As String
    Dim wvarAB_Guar_Del     As String
    Dim wvarAB_Guar_Del_Der As String
    Dim wvarAB_Guar_Del_Izq As String
    Dim wvarAB_Guar_Tra     As String
    Dim wvarAB_Guar_Tra_Der As String
    Dim wvarAB_Guar_Tra_Izq As String
    Dim wvarAB_Puerta       As String
    Dim wvarAB_Puer_Del     As String
    Dim wvarAB_Puer_Del_Der As String
    Dim wvarAB_Puer_Del_Izq As String
    Dim wvarAB_Puer_Tra     As String
    Dim wvarAB_Puer_Tra_Der As String
    Dim wvarAB_Puer_Tra_Izq As String
    Dim wvarPIC_Capot       As String
    Dim wvarPIC_Techo       As String
    Dim wvarPIC_TBaul       As String
    Dim wvarPIC_Guardabarro As String
    Dim wvarPIC_Guar_Del    As String
    Dim wvarPIC_Guar_Del_Der    As String
    Dim wvarPIC_Guar_Del_Izq    As String
    Dim wvarPIC_Guar_Tra        As String
    Dim wvarPIC_Guar_Tra_Der    As String
    Dim wvarPIC_Guar_Tra_Izq    As String
    Dim wvarPIC_Puerta      As String
    Dim wvarPIC_Puer_Del        As String
    Dim wvarPIC_Puer_Del_Der    As String
    Dim wvarPIC_Puer_Del_Izq    As String
    Dim wvarPIC_Puer_Tra        As String
    Dim wvarPIC_Puer_Tra_Der    As String
    Dim wvarPIC_Puer_Tra_Izq    As String
    Dim wvarPIC_Zocalo      As String
    Dim wvarPIC_Zoc_Der     As String
    Dim wvarPIC_Zoc_Izq As String
    Dim wvarMAS_Capot   As String
    Dim wvarMAS_Techo   As String
    Dim wvarMAS_TBaul   As String
    Dim wvarMAS_Guardabarro As String
    Dim wvarMAS_Guar_Del    As String
    Dim wvarMAS_Guar_Del_Der    As String
    Dim wvarMAS_Guar_Del_Izq    As String
    Dim wvarMAS_Guar_Tra    As String
    Dim wvarMAS_Guar_Tra_Der    As String
    Dim wvarMAS_Guar_Tra_Izq    As String
    Dim wvarMAS_Puerta  As String
    Dim wvarMAS_Puer_Del    As String
    Dim wvarMAS_Puer_Del_Der    As String
    Dim wvarMAS_Puer_Del_Izq    As String
    Dim wvarMAS_Puer_Tra    As String
    Dim wvarMAS_Puer_Tra_Der    As String
    Dim wvarMAS_Puer_Tra_Izq    As String
    Dim wvarMAS_Zocalo  As String
    Dim wvarMAS_Zoc_Der As String
    Dim wvarMAS_Zoc_Izq As String
    Dim wvarROT_Paragolpe   As String
    Dim wvarROT_Par_Del As String
    Dim wvarROT_Par_Tra As String
    Dim wvarROT_Rejilla As String
    Dim wvarROT_Optica  As String
    Dim wvarROT_Opt_Der As String
    Dim wvarROT_Opt_Izq As String
    Dim wvarROT_FaroGiro    As String
    Dim wvarROT_FG_Del  As String
    Dim wvarROT_FG_Del_Der  As String
    Dim wvarROT_FG_Del_Izq  As String
    Dim wvarROT_FG_Tra  As String
    Dim wvarROT_FG_Tra_Der  As String
    Dim wvarROT_FG_Tra_Izq  As String
    Dim wvarROT_Luneta  As String
    Dim wvarROT_Parabrisas  As String
    Dim wvarFAL_Paragolpe   As String
    Dim wvarFAL_Par_Del As String
    Dim wvarFAL_Par_Tra As String
    Dim wvarFAL_Rejilla As String
    Dim wvarFAL_Optica  As String
    Dim wvarFAL_Opt_Der As String
    Dim wvarFAL_Opt_Izq As String
    Dim wvarFAL_FaroGiro    As String
    Dim wvarFAL_FG_Del  As String
    Dim wvarFAL_FG_Del_Der  As String
    Dim wvarFAL_FG_Del_Izq  As String
    Dim wvarFAL_FG_Tra  As String
    Dim wvarFAL_FG_Tra_Der  As String
    Dim wvarFAL_FG_Tra_Izq  As String
    Dim wvarLUC_Alta    As String
    Dim wvarLUC_Baja    As String
    Dim wvarLUC_Giro    As String
    Dim wvarLUC_Gir_Del As String
    Dim wvarLUC_Gir_Del_Der As String
    Dim wvarLUC_Gir_Del_Izq As String
    Dim wvarLUC_Gir_Tra As String
    Dim wvarLUC_Gir_Tra_Der As String
    Dim wvarLUC_Gir_Tra_Izq As String
    Dim wvarLUC_Posicion    As String
    Dim wvarLUC_Pos_Del As String
    Dim wvarLUC_Pos_Del_Der As String
    Dim wvarLUC_Pos_Del_Izq As String
    Dim wvarLUC_Pos_Tra As String
    Dim wvarLUC_Pos_Tra_Der As String
    Dim wvarLUC_Pos_Tra_Izq As String
    Dim wvarLUC_Stop    As String
    Dim wvarNEU_Est_Del_Der As String
    Dim wvarNEU_Est_Del_Izq As String
    Dim wvarNEU_Est_Tra_Der As String
    Dim wvarNEU_Est_Tra_Izq As String
    Dim wvarFuncKil     As String
    '
    Dim strParseString      As String
    Dim wvarError           As String
    Dim wvarPos             As Long
    Dim wvarString          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarEstado = .selectSingleNode(mcteParam_Estado).Text
        wvarUso = .selectSingleNode(mcteParam_Uso).Text
        wvarStep = 20
        wvarAbollado = .selectSingleNode(mcteParam_Abollado).Text
        wvarPicado = .selectSingleNode(mcteParam_Picado).Text
        wvarMasillado = .selectSingleNode(mcteParam_Masillado).Text
        wvarRoto = .selectSingleNode(mcteParam_Roto).Text
        wvarFaltantes = .selectSingleNode(mcteParam_Faltantes).Text
        wvarLuces = .selectSingleNode(mcteParam_Luces).Text
        wvarStep = 30
        wvarOdometro = Left(.selectSingleNode(mcteParam_Odometro).Text & Space(10), 10)
        wvarAB_Paragolpe = .selectSingleNode(mcteParam_AB_Paragolpe).Text
        wvarAB_Par_Del = .selectSingleNode(mcteParam_AB_Par_Del).Text
        wvarAB_Par_Tra = .selectSingleNode(mcteParam_AB_Par_Tra).Text
        wvarAB_Capot = .selectSingleNode(mcteParam_AB_Capot).Text
        wvarAB_Techo = .selectSingleNode(mcteParam_AB_Techo).Text
        wvarStep = 40
        wvarAB_TBaul = .selectSingleNode(mcteParam_AB_TBaul).Text
        wvarAB_Guardabarro = .selectSingleNode(mcteParam_AB_Guardabarro).Text
        wvarAB_Guar_Del = .selectSingleNode(mcteParam_AB_Guar_Del).Text
        wvarAB_Guar_Del_Der = .selectSingleNode(mcteParam_AB_Guar_Del_Der).Text
        wvarAB_Guar_Del_Izq = .selectSingleNode(mcteParam_AB_Guar_Del_Izq).Text
        wvarAB_Guar_Tra = .selectSingleNode(mcteParam_AB_Guar_Tra).Text
        wvarStep = 50
        wvarAB_Guar_Tra_Der = .selectSingleNode(mcteParam_AB_Guar_Tra_Der).Text
        wvarAB_Guar_Tra_Izq = .selectSingleNode(mcteParam_AB_Guar_Tra_Izq).Text
        wvarAB_Puerta = .selectSingleNode(mcteParam_AB_Puerta).Text
        wvarAB_Puer_Del = .selectSingleNode(mcteParam_AB_Puer_Del).Text
        wvarAB_Puer_Del_Der = .selectSingleNode(mcteParam_AB_Puer_Del_Der).Text
        wvarAB_Puer_Del_Izq = .selectSingleNode(mcteParam_AB_Puer_Del_Izq).Text
        wvarStep = 60
        wvarAB_Puer_Tra = .selectSingleNode(mcteParam_AB_Puer_Tra).Text
        wvarAB_Puer_Tra_Der = .selectSingleNode(mcteParam_AB_Puer_Tra_Der).Text
        wvarAB_Puer_Tra_Izq = .selectSingleNode(mcteParam_AB_Puer_Tra_Izq).Text
        wvarPIC_Capot = .selectSingleNode(mcteParam_PIC_Capot).Text
        wvarPIC_Techo = .selectSingleNode(mcteParam_PIC_Techo).Text
        wvarPIC_TBaul = .selectSingleNode(mcteParam_PIC_TBaul).Text
        wvarStep = 70
        wvarPIC_Guardabarro = .selectSingleNode(mcteParam_PIC_Guardabarro).Text
        wvarPIC_Guar_Del = .selectSingleNode(mcteParam_PIC_Guar_Del).Text
        wvarPIC_Guar_Del_Der = .selectSingleNode(mcteParam_PIC_Guar_Del_Der).Text
        wvarPIC_Guar_Del_Izq = .selectSingleNode(mcteParam_PIC_Guar_Del_Izq).Text
        wvarPIC_Guar_Tra = .selectSingleNode(mcteParam_PIC_Guar_Tra).Text
        wvarPIC_Guar_Tra_Der = .selectSingleNode(mcteParam_PIC_Guar_Tra_Der).Text
        wvarStep = 80
        wvarPIC_Guar_Tra_Izq = .selectSingleNode(mcteParam_PIC_Guar_Tra_Izq).Text
        wvarPIC_Puerta = .selectSingleNode(mcteParam_PIC_Puerta).Text
        wvarPIC_Puer_Del = .selectSingleNode(mcteParam_PIC_Puer_Del).Text
        wvarPIC_Puer_Del_Der = .selectSingleNode(mcteParam_PIC_Puer_Del_Der).Text
        wvarPIC_Puer_Del_Izq = .selectSingleNode(mcteParam_PIC_Puer_Del_Izq).Text
        wvarPIC_Puer_Tra = .selectSingleNode(mcteParam_PIC_Puer_Tra).Text
        wvarStep = 90
        wvarPIC_Puer_Tra_Der = .selectSingleNode(mcteParam_PIC_Puer_Tra_Der).Text
        wvarPIC_Puer_Tra_Izq = .selectSingleNode(mcteParam_PIC_Puer_Tra_Izq).Text
        wvarPIC_Zocalo = .selectSingleNode(mcteParam_PIC_Zocalo).Text
        wvarPIC_Zoc_Der = .selectSingleNode(mcteParam_PIC_Zoc_Der).Text
        wvarPIC_Zoc_Izq = .selectSingleNode(mcteParam_PIC_Zoc_Izq).Text
        wvarMAS_Capot = .selectSingleNode(mcteParam_MAS_Capot).Text
        wvarStep = 100
        wvarMAS_Techo = .selectSingleNode(mcteParam_MAS_Techo).Text
        wvarMAS_TBaul = .selectSingleNode(mcteParam_MAS_TBaul).Text
        wvarMAS_Guardabarro = .selectSingleNode(mcteParam_MAS_Guardabarro).Text
        wvarMAS_Guar_Del = .selectSingleNode(mcteParam_MAS_Guar_Del).Text
        wvarMAS_Guar_Del_Der = .selectSingleNode(mcteParam_MAS_Guar_Del_Der).Text
        wvarMAS_Guar_Del_Izq = .selectSingleNode(mcteParam_MAS_Guar_Del_Izq).Text
        wvarStep = 110
        wvarMAS_Guar_Tra = .selectSingleNode(mcteParam_MAS_Guar_Tra).Text
        wvarMAS_Guar_Tra_Der = .selectSingleNode(mcteParam_MAS_Guar_Tra_Der).Text
        wvarMAS_Guar_Tra_Izq = .selectSingleNode(mcteParam_MAS_Guar_Tra_Izq).Text
        wvarMAS_Puerta = .selectSingleNode(mcteParam_MAS_Puerta).Text
        wvarMAS_Puer_Del = .selectSingleNode(mcteParam_MAS_Puer_Del).Text
        wvarMAS_Puer_Del_Der = .selectSingleNode(mcteParam_MAS_Puer_Del_Der).Text
        wvarStep = 120
        wvarMAS_Puer_Del_Izq = .selectSingleNode(mcteParam_MAS_Puer_Del_Izq).Text
        wvarMAS_Puer_Tra = .selectSingleNode(mcteParam_MAS_Puer_Tra).Text
        wvarMAS_Puer_Tra_Der = .selectSingleNode(mcteParam_MAS_Puer_Tra_Der).Text
        wvarMAS_Puer_Tra_Izq = .selectSingleNode(mcteParam_MAS_Puer_Tra_Izq).Text
        wvarMAS_Zocalo = .selectSingleNode(mcteParam_MAS_Zocalo).Text
        wvarMAS_Zoc_Der = .selectSingleNode(mcteParam_MAS_Zoc_Der).Text
        wvarStep = 130
        wvarMAS_Zoc_Izq = .selectSingleNode(mcteParam_MAS_Zoc_Izq).Text
        wvarROT_Paragolpe = .selectSingleNode(mcteParam_ROT_Paragolpe).Text
        wvarROT_Par_Del = .selectSingleNode(mcteParam_ROT_Par_Del).Text
        wvarROT_Par_Tra = .selectSingleNode(mcteParam_ROT_Par_Tra).Text
        wvarROT_Rejilla = .selectSingleNode(mcteParam_ROT_Rejilla).Text
        wvarROT_Optica = .selectSingleNode(mcteParam_ROT_Optica).Text
        wvarStep = 140
        wvarROT_Opt_Der = .selectSingleNode(mcteParam_ROT_Opt_Der).Text
        wvarROT_Opt_Izq = .selectSingleNode(mcteParam_ROT_Opt_Izq).Text
        wvarROT_FaroGiro = .selectSingleNode(mcteParam_ROT_FaroGiro).Text
        wvarROT_FG_Del = .selectSingleNode(mcteParam_ROT_FG_Del).Text
        wvarROT_FG_Del_Der = .selectSingleNode(mcteParam_ROT_FG_Del_Der).Text
        wvarROT_FG_Del_Izq = .selectSingleNode(mcteParam_ROT_FG_Del_Izq).Text
        wvarStep = 150
        wvarROT_FG_Tra = .selectSingleNode(mcteParam_ROT_FG_Tra).Text
        wvarROT_FG_Tra_Der = .selectSingleNode(mcteParam_ROT_FG_Tra_Der).Text
        wvarROT_FG_Tra_Izq = .selectSingleNode(mcteParam_ROT_FG_Tra_Izq).Text
        wvarROT_Luneta = .selectSingleNode(mcteParam_ROT_Luneta).Text
        wvarROT_Parabrisas = .selectSingleNode(mcteParam_ROT_Parabrisas).Text
        wvarFAL_Paragolpe = .selectSingleNode(mcteParam_FAL_Paragolpe).Text
        wvarStep = 160
        wvarFAL_Par_Del = .selectSingleNode(mcteParam_FAL_Par_Del).Text
        wvarFAL_Par_Tra = .selectSingleNode(mcteParam_FAL_Par_Tra).Text
        wvarFAL_Rejilla = .selectSingleNode(mcteParam_FAL_Rejilla).Text
        wvarFAL_Optica = .selectSingleNode(mcteParam_FAL_Optica).Text
        wvarFAL_Opt_Der = .selectSingleNode(mcteParam_FAL_Opt_Der).Text
        wvarFAL_Opt_Izq = .selectSingleNode(mcteParam_FAL_Opt_Izq).Text
        wvarStep = 170
        wvarFAL_FaroGiro = .selectSingleNode(mcteParam_FAL_FaroGiro).Text
        wvarFAL_FG_Del = .selectSingleNode(mcteParam_FAL_FG_Del).Text
        wvarFAL_FG_Del_Der = .selectSingleNode(mcteParam_FAL_FG_Del_Der).Text
        wvarFAL_FG_Del_Izq = .selectSingleNode(mcteParam_FAL_FG_Del_Izq).Text
        wvarFAL_FG_Tra = .selectSingleNode(mcteParam_FAL_FG_Tra).Text
        wvarFAL_FG_Tra_Der = .selectSingleNode(mcteParam_FAL_FG_Tra_Der).Text
        wvarStep = 180
        wvarFAL_FG_Tra_Izq = .selectSingleNode(mcteParam_FAL_FG_Tra_Izq).Text
        wvarLUC_Alta = .selectSingleNode(mcteParam_LUC_Alta).Text
        wvarLUC_Baja = .selectSingleNode(mcteParam_LUC_Baja).Text
        wvarLUC_Giro = .selectSingleNode(mcteParam_LUC_Giro).Text
        wvarLUC_Gir_Del = .selectSingleNode(mcteParam_LUC_Gir_Del).Text
        wvarLUC_Gir_Del_Der = .selectSingleNode(mcteParam_LUC_Gir_Del_Der).Text
        wvarStep = 190
        wvarLUC_Gir_Del_Izq = .selectSingleNode(mcteParam_LUC_Gir_Del_Izq).Text
        wvarLUC_Gir_Tra = .selectSingleNode(mcteParam_LUC_Gir_Tra).Text
        wvarLUC_Gir_Tra_Der = .selectSingleNode(mcteParam_LUC_Gir_Tra_Der).Text
        wvarLUC_Gir_Tra_Izq = .selectSingleNode(mcteParam_LUC_Gir_Tra_Izq).Text
        wvarLUC_Posicion = .selectSingleNode(mcteParam_LUC_Posicion).Text
        wvarLUC_Pos_Del = .selectSingleNode(mcteParam_LUC_Pos_Del).Text
        wvarStep = 200
        wvarLUC_Pos_Del_Der = .selectSingleNode(mcteParam_LUC_Pos_Del_Der).Text
        wvarLUC_Pos_Del_Izq = .selectSingleNode(mcteParam_LUC_Pos_Del_Izq).Text
        wvarLUC_Pos_Tra = .selectSingleNode(mcteParam_LUC_Pos_Tra).Text
        wvarLUC_Pos_Tra_Der = .selectSingleNode(mcteParam_LUC_Pos_Tra_Der).Text
        wvarLUC_Pos_Tra_Izq = .selectSingleNode(mcteParam_LUC_Pos_Tra_Izq).Text
        wvarLUC_Stop = .selectSingleNode(mcteParam_LUC_Stop).Text
        wvarStep = 210
        wvarNEU_Est_Del_Der = .selectSingleNode(mcteParam_NEU_Est_Del_Der).Text
        wvarNEU_Est_Del_Izq = .selectSingleNode(mcteParam_NEU_Est_Del_Izq).Text
        wvarNEU_Est_Tra_Der = .selectSingleNode(mcteParam_NEU_Est_Tra_Der).Text
        wvarNEU_Est_Tra_Izq = .selectSingleNode(mcteParam_NEU_Est_Tra_Izq).Text
        wvarFuncKil = .selectSingleNode(mcteParam_FuncKil).Text
     '
    End With
    '
    wvarStep = 230
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 240
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 270
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 280
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 290
    wvarString = wvarEstado & wvarUso & wvarAbollado & wvarPicado & wvarMasillado & wvarRoto & wvarFaltantes & wvarLuces & wvarOdometro & wvarAB_Paragolpe
    wvarString = wvarString & wvarAB_Par_Del & wvarAB_Par_Tra & wvarAB_Capot & wvarAB_Techo & wvarAB_TBaul & wvarAB_Guardabarro & wvarAB_Guar_Del & wvarAB_Guar_Del_Der & wvarAB_Guar_Del_Izq
    wvarString = wvarString & wvarAB_Guar_Tra & wvarAB_Guar_Tra_Der & wvarAB_Guar_Tra_Izq & wvarAB_Puerta & wvarAB_Puer_Del & wvarAB_Puer_Del_Der & wvarAB_Puer_Del_Izq & wvarAB_Puer_Tra & wvarAB_Puer_Tra_Der
    wvarString = wvarString & wvarAB_Puer_Tra_Izq & wvarPIC_Capot & wvarPIC_Techo & wvarPIC_TBaul & wvarPIC_Guardabarro & wvarPIC_Guar_Del & wvarPIC_Guar_Del_Der & wvarPIC_Guar_Del_Izq & wvarPIC_Guar_Tra
    wvarString = wvarString & wvarPIC_Guar_Tra_Der & wvarPIC_Guar_Tra_Izq & wvarPIC_Puerta & wvarPIC_Puer_Del & wvarPIC_Puer_Del_Der & wvarPIC_Puer_Del_Izq & wvarPIC_Puer_Tra & wvarPIC_Puer_Tra_Der & wvarPIC_Puer_Tra_Izq
    wvarString = wvarString & wvarPIC_Zocalo & wvarPIC_Zoc_Der & wvarPIC_Zoc_Izq & wvarMAS_Capot & wvarMAS_Techo & wvarMAS_TBaul & wvarMAS_Guardabarro & wvarMAS_Guar_Del & wvarMAS_Guar_Del_Der & wvarMAS_Guar_Del_Izq
    wvarString = wvarString & wvarMAS_Guar_Tra & wvarMAS_Guar_Tra_Der & wvarMAS_Guar_Tra_Izq & wvarMAS_Puerta & wvarMAS_Puer_Del & wvarMAS_Puer_Del_Der & wvarMAS_Puer_Del_Izq & wvarMAS_Puer_Tra & wvarMAS_Puer_Tra_Der
    wvarString = wvarString & wvarMAS_Puer_Tra_Izq & wvarMAS_Zocalo & wvarMAS_Zoc_Der & wvarMAS_Zoc_Izq & wvarROT_Paragolpe & wvarROT_Par_Del & wvarROT_Par_Tra & wvarROT_Rejilla & wvarROT_Optica & wvarROT_Opt_Der & wvarROT_Opt_Izq
    wvarString = wvarString & wvarROT_FaroGiro & wvarROT_FG_Del & wvarROT_FG_Del_Der & wvarROT_FG_Del_Izq & wvarROT_FG_Tra & wvarROT_FG_Tra_Der & wvarROT_FG_Tra_Izq & wvarROT_Luneta & wvarROT_Parabrisas & wvarFAL_Paragolpe & wvarFAL_Par_Del
    wvarString = wvarString & wvarFAL_Par_Tra & wvarFAL_Rejilla & wvarFAL_Optica & wvarFAL_Opt_Der & wvarFAL_Opt_Izq & wvarFAL_FaroGiro & wvarFAL_FG_Del & wvarFAL_FG_Del_Der & wvarFAL_FG_Del_Izq & wvarFAL_FG_Tra & wvarFAL_FG_Tra_Der
    wvarString = wvarString & wvarFAL_FG_Tra_Izq & wvarLUC_Alta & wvarLUC_Baja & wvarLUC_Giro & wvarLUC_Gir_Del & wvarLUC_Gir_Del_Der & wvarLUC_Gir_Del_Izq & wvarLUC_Gir_Tra & wvarLUC_Gir_Tra_Der & wvarLUC_Gir_Tra_Izq & wvarLUC_Posicion
    wvarString = wvarString & wvarLUC_Pos_Del & wvarLUC_Pos_Del_Der & wvarLUC_Pos_Del_Izq & wvarLUC_Pos_Tra & wvarLUC_Pos_Tra_Der & wvarLUC_Pos_Tra_Izq & wvarLUC_Stop & wvarNEU_Est_Del_Der & wvarNEU_Est_Del_Izq & wvarNEU_Est_Tra_Der & wvarNEU_Est_Tra_Izq & wvarFuncKil
    
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarString
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    wvarPos = 197 'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarStep = 420
    If Mid(strParseString, 19, 2) = "ER" Then
        '
        wvarStep = 430
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 440
        wvarResult = ""
        wvarResult = "<RTA>" & Mid(strParseString, wvarPos, 1) & "</RTA>"
        wvarResult = wvarResult & "<MSGERR><![CDATA[" & Trim(Mid(strParseString, wvarPos + 1, 50)) & "]]></MSGERR>"
        wvarResult = wvarResult & "<DDJJSTRING>" & wvarString & "</DDJJSTRING>"
        Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
        '
    End If
    '
    wvarStep = 450
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function p_GetXSLEstados() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RS/R'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='OPTION'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='value'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,1,2))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='flag'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,33,1))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:value-of select='normalize-space(substring(.,3,30))' />"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLEstados = wvarStrXSL
End Function
Private Function ParseoMensajeEstado(wvarPos As Long, strParseString As String) As String
Dim wvarResult As String
Dim wvarCant As Integer
Dim i As Integer
    '
    wvarResult = wvarResult & "<RS>"
    '
    wvarPos = wvarPos + 1
    If Trim(Mid(strParseString, wvarPos, 3)) <> "000" And Trim(Mid(strParseString, wvarPos, 3)) <> "" Then
        wvarCant = CInt(Trim(Mid(strParseString, wvarPos, 3)))
        wvarPos = wvarPos + 3
        For i = 1 To wvarCant
            wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 33) & "]]></R>"
            wvarPos = wvarPos + 33
        Next
    End If
    '
    wvarResult = wvarResult & "</RS>"
    '
    ParseoMensajeEstado = wvarResult
    '
End Function














