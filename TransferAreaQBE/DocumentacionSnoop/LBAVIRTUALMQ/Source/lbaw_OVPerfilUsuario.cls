VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVPerfilUsuario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQ.lbaw_OVPerfilUsuario"
Const mcteOpID              As String = "1000"

'Parametros XML de Entrada
Const mcteParam_Usuario    As String = "//USUARIO"


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarResultUnic      As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        '
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los Parametros de la cola de MQ del archivo de configuración
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.Load (App.Path & "\" & gcteParamFileName)
    wvarCiaAsCod = wobjXMLParametros.selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarUsuario
    '
    wvarStep = 100
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        IAction_Execute = 1
        mobjCOM_Context.SetAbort
        GoTo ClearObjects:
    End If
    '
    wvarStep = 180
    wvarResult = ""
    wvarPos = 77  'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 190
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 200
        Response = "<Response><Estado resultado='false' mensaje='El usuario no se encuentra habilitado en el sistema.' /></Response>"
        '
    Else
        '
        wvarStep = 230
        wvarResultUnic = "<USER_NIVEL>" & Trim(Mid(strParseString, wvarPos, 1)) & "</USER_NIVEL>"
        wvarResultUnic = wvarResultUnic & "<USER_NOM><![CDATA[" & Trim(Mid(strParseString, wvarPos + 1, 60)) & "]]></USER_NOM>"
        '
        wvarResult = ""
        wvarResult = wvarResult & ParseoMensaje(wvarPos + 61, strParseString, wvarstrLen)
        '
        If Trim(wvarResult) <> "" Then
            wvarStep = 290
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResultUnic & wvarResult & "</Response>"
        Else
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>"
        End If
        '
    End If
    '
    wvarStep = 300
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarstrLen As Long) As String
Dim wvarResult As String
Dim wvarCount As Long
Dim wvarCantEmisiones As Long
    '
    wvarResult = wvarResult & "<ACCESO>"
    wvarCount = 1
    '
    While wvarPos < wvarstrLen And Trim(Mid(strParseString, wvarPos, 6)) <> "" And wvarCount <= 200
        wvarResult = wvarResult & "<MMENU><![CDATA[" & Mid(strParseString, wvarPos, 6) & "]]></MMENU>"
        wvarPos = wvarPos + 66
        wvarCount = wvarCount + 1
    Wend
    '
    wvarResult = wvarResult & "</ACCESO>"
    
    'Cargo los Ramos que puede emitir Online
    wvarPos = 13338
    If Len(strParseString) > wvarPos + 3 Then
        'El area incluye valores para emisiones Onlines permitidas
        If IsNumeric(Mid(strParseString, wvarPos, 3)) Then
            wvarResult = wvarResult & "<EMISIONESONLINE>"
            wvarCantEmisiones = Mid(strParseString, wvarPos, 3)
            wvarPos = wvarPos + 3
            For wvarCount = 1 To wvarCantEmisiones
                If wvarPos > wvarstrLen Then Exit For
                If Trim(Mid(strParseString, wvarPos, 4)) = "" Then Exit For
                wvarResult = wvarResult & "<EMISION>" & Mid(strParseString, wvarPos, 4) & "</EMISION>"
                wvarPos = wvarPos + 4
            Next
            wvarResult = wvarResult & "</EMISIONESONLINE>"
        End If
    End If
    '
    ParseoMensaje = wvarResult
    '
End Function

