VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVDatosGralCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQ.lbaw_OVDatosGralCliente"
Const mcteOpID              As String = "1102"

'Parametros XML de Entrada
Const mcteParam_Usuario    As String = "//USUARIO"
Const mcteParam_Producto   As String = "//PRODUCTO"
Const mcteParam_Poliza     As String = "//POLIZA"
Const mcteParam_Certi      As String = "//CERTI"
Const mcteParam_Suplenum   As String = "//SUPLENUM"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarProducto        As String
    Dim wvarPoliza          As String
    Dim wvarCerti           As String
    Dim wvarSuplenum        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarProducto = Left(.selectSingleNode(mcteParam_Producto).Text & Space(4), 4)
        wvarPoliza = Right(String(8, "0") & .selectSingleNode(mcteParam_Poliza).Text, 8)
        wvarCerti = Right(String(14, "0") & .selectSingleNode(mcteParam_Certi).Text, 14)
        If .selectNodes(mcteParam_Suplenum).length <> 0 Then
            wvarSuplenum = Right(String(4, "0") & .selectSingleNode(mcteParam_Suplenum).Text, 4)
        Else
            wvarSuplenum = String(4, "0")
        End If
        '
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los Parametros de la cola de MQ del archivo de configuración
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.Load (App.Path & "\" & gcteParamFileName)
    wvarCiaAsCod = wobjXMLParametros.selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarProducto & wvarPoliza & wvarCerti & wvarSuplenum
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 36
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        IAction_Execute = 1
        mobjCOM_Context.SetAbort
        GoTo ClearObjects:
    End If
    wvarStep = 180
    wvarResult = ""
    wvarPos = 97  'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 190
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    If wvarEstado = "OK" Then
        '
        wvarStep = 200
        wvarResult = wvarResult & "<DATOS>"
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        '
        wvarStep = 210
        wvarResult = wvarResult & "</DATOS>"
        '
        wvarStep = 220
        Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
    Else
        wvarStep = 230
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
    End If
    '
    wvarStep = 240
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing

Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarProducto & wvarPoliza & wvarCerti & wvarSuplenum & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarstrLen As Long) As String
Dim wvarResult As String
        
    'Es un solo dato.
    wvarResult = wvarResult & "<CLIENTE>"
    wvarResult = wvarResult & "<CLIENSEC>" & Mid(strParseString, wvarPos, 9) & "</CLIENSEC>"
    wvarResult = wvarResult & "<CLIENDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 9, 60)) & "]]></CLIENDES>"
    wvarResult = wvarResult & "<TIPODOCU><![CDATA[" & Trim(Mid(strParseString, wvarPos + 75, 8)) & "]]></TIPODOCU>"
    wvarResult = wvarResult & "<NUMEDOCU>" & Trim(Mid(strParseString, wvarPos + 83, 11)) & "</NUMEDOCU>"
    wvarResult = wvarResult & "<NACIONALI><![CDATA[" & Trim(Mid(strParseString, wvarPos + 94, 40)) & "]]></NACIONALI>"
    wvarResult = wvarResult & "<SEXO><![CDATA[" & Trim(Mid(strParseString, wvarPos + 134, 9)) & "]]></SEXO>"
    wvarResult = wvarResult & "<FECNAC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 143, 10)) & "]]></FECNAC>"
    wvarResult = wvarResult & "<ESTCIVIL><![CDATA[" & Trim(Mid(strParseString, wvarPos + 153, 10)) & "]]></ESTCIVIL>"
    wvarResult = wvarResult & "<FUMADOR><![CDATA[" & Trim(Mid(strParseString, wvarPos + 163, 2)) & "]]></FUMADOR>"
    wvarResult = wvarResult & "<CALLEPART><![CDATA[" & Trim(Mid(strParseString, wvarPos + 182, 60)) & "]]></CALLEPART>"
    wvarResult = wvarResult & "<CPOPART><![CDATA[" & Trim(Mid(strParseString, wvarPos + 242, 8)) & "]]></CPOPART>"
    wvarResult = wvarResult & "<LOCALPART><![CDATA[" & Trim(Mid(strParseString, wvarPos + 250, 40)) & "]]></LOCALPART>"
    wvarResult = wvarResult & "<PROVPART><![CDATA[" & Trim(Mid(strParseString, wvarPos + 290, 20)) & "]]></PROVPART>"
    wvarResult = wvarResult & "</CLIENTE>"
    
    wvarPos = wvarPos + 310
    wvarResult = wvarResult & "<CONTACTOS>"
    While wvarPos < wvarstrLen And Trim(Mid(strParseString, wvarPos + 3, 30)) <> ""
        '
        wvarResult = wvarResult & "<CONTACTO>"
        wvarResult = wvarResult & "<DESCRIPCION><![CDATA[" & Trim(Mid(strParseString, wvarPos + 3, 30)) & "]]></DESCRIPCION>"
        wvarResult = wvarResult & "<DATOCONT><![CDATA[" & Trim(Mid(strParseString, wvarPos + 33, 50)) & "]]></DATOCONT>"
        wvarResult = wvarResult & "</CONTACTO>"
        '
        wvarPos = wvarPos + 83
    Wend
    wvarResult = wvarResult & "</CONTACTOS>"
    ParseoMensaje = wvarResult

End Function

















