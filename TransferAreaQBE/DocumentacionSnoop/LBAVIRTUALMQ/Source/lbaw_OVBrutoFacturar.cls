VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVBrutoFacturar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQ.lbaw_OVBrutoFacturar"
Const mcteOpID              As String = "1803"

'Parametros XML de Entrada
Const mcteParam_Usuario     As String = "//USUARIO"
Const mcteParam_ClienSec    As String = "//CLIENSEC"
Const mcteParam_EfectAnn    As String = "//EFECTANN"
Const mcteParam_EfectMes    As String = "//EFECTMES"
Const mcteParam_EfectDia    As String = "//EFECTDIA"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarClienSec        As String
    Dim wvarEfectAnn        As String
    Dim wvarEfectMes        As String
    Dim wvarEfectDia        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarClienSec = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSec).Text, 9)
        wvarEfectAnn = .selectSingleNode(mcteParam_EfectAnn).Text
        wvarEfectMes = Right(String(2, "0") & .selectSingleNode(mcteParam_EfectMes).Text, 2)
        wvarEfectDia = Right(String(2, "0") & .selectSingleNode(mcteParam_EfectDia).Text, 2)
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los Parametros de la cola de MQ del archivo de configuración
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.Load (App.Path & "\" & gcteParamFileName)
    wvarCiaAsCod = wobjXMLParametros.selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & Space(4) & wvarClienSec & "    000000000  000000000  000000000" & wvarEfectAnn & wvarEfectMes & wvarEfectDia
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 400
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        IAction_Execute = 1
        mobjCOM_Context.SetAbort
        GoTo ClearObjects:
    End If
        
    wvarStep = 190
    wvarResult = ""
    wvarPos = 75 'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 200
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 210
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        If Trim(Mid(strParseString, wvarPos + 47, 100)) = "" Then
            '
            wvarStep = 220
            If Trim(Mid(strParseString, wvarPos + 28, 13)) <> "" And Trim(Mid(strParseString, wvarPos + 28, 13)) <> "0000000000000" Then
                wvarResult = wvarResult & "<IMPORTEPESOS>" & CStr(CDbl(Mid(strParseString, wvarPos, 13))) & "</IMPORTEPESOS>"
                wvarResult = wvarResult & "<SIGNOIMPPESOS>" & Mid(strParseString, wvarPos + 13, 1) & "</SIGNOIMPPESOS>"
                wvarResult = wvarResult & "<IMPORTEDOLAR>" & CStr(CDbl(Mid(strParseString, wvarPos + 14, 13))) & "</IMPORTEDOLAR>"
                wvarResult = wvarResult & "<SIGNOIMPDOLAR>" & Mid(strParseString, wvarPos + 27, 1) & "</SIGNOIMPDOLAR>"
                wvarResult = wvarResult & "<IMPORTETOTAL>" & CStr(CDbl(Mid(strParseString, wvarPos + 28, 13))) & "</IMPORTETOTAL>"
                wvarResult = wvarResult & "<SIGNOIMPTOTAL>" & Mid(strParseString, wvarPos + 41, 1) & "</SIGNOIMPTOTAL>"
                wvarResult = wvarResult & "<COTIZACION>" & CStr(CDbl(Mid(strParseString, wvarPos + 42, 5))) & "</COTIZACION>"
                '
                wvarStep = 230
                Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
            Else
                Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje='No se encontraron datos.' /></Response>"
            End If
        Else
            Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje='" & Trim(Mid(strParseString, wvarPos + 44, 100)) & "' /></Response>"
        End If
        '
    End If
    '
    wvarStep = 240
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing

Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarClienSec & wvarEfectAnn & wvarEfectMes & wvarEfectDia & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarstrLen As Long) As String
Dim wvarResult As String
    '
    wvarResult = wvarResult & "<RS>"
    '
    While wvarPos < wvarstrLen And Trim(Mid(strParseString, wvarPos, 4)) <> ""
        wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 138) & "]]></R>"
        wvarPos = wvarPos + 138
    Wend
    '
    wvarResult = wvarResult & "</RS>"
    '
    ParseoMensaje = wvarResult
    '
End Function
Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/'> "
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REGS'>"
    wvarStrXSL = wvarStrXSL & "              <xsl:apply-templates select='/RS/R'/>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RS/R'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REG'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='PROD'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,1,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='POL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,5,8)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERPOL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,13,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERANN'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,17,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERSEC'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,21,6)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CLIDES'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,40,30))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='TOMA'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,70,30))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='EST'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,100,30))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='SINI'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,130,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='ALERTEXI'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,131,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='AGE'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,132,2)' />-<xsl:value-of select='substring(.,134,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='RAMO'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,138,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIDES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='TOMA'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='EST'/>"
    '
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
    '
End Function




















