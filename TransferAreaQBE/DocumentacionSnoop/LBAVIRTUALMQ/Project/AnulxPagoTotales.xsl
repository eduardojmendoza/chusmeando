<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:key name="contacts-by-CL1CL2" match="REG" use="concat(CL1,'_',CL2)"/>
	<xsl:key name="contacts-by-CL1" match="//REGS/REG" use="CL1"/>
	<xsl:template match="/">
		<EST>
			<xsl:for-each select="//REGS/REG[count(. | key('contacts-by-CL1', CL1)[1]) = 1]">
				<xsl:if test="NI1 !=''">
					<GO>
						<xsl:attribute name="CLI"><xsl:value-of select="CL1"/></xsl:attribute>
						<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
						<xsl:attribute name="NIV"><xsl:value-of select="NI1"/></xsl:attribute>
						<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
						<xsl:attribute name="TOTPOL"><xsl:value-of select="TOTPOL"/></xsl:attribute>
						<xsl:attribute name="MON1"><xsl:value-of select="MON1"/></xsl:attribute>
						<xsl:attribute name="SIG1"><xsl:value-of select="SIG1"/></xsl:attribute>
						<xsl:attribute name="TOTIMP1"><xsl:value-of select="TOTIMP1"/></xsl:attribute>
						<xsl:attribute name="MON2"><xsl:value-of select="MON2"/></xsl:attribute>
						<xsl:attribute name="SIG2"><xsl:value-of select="SIG2"/></xsl:attribute>
						<xsl:attribute name="TOTIMP2"><xsl:value-of select="TOTIMP2"/></xsl:attribute>
						<xsl:for-each select="key('contacts-by-CL1',CL1)">
							<xsl:for-each select="(current())[count(. | key('contacts-by-CL1CL2', concat(CL1,'_',CL2))[1]) = 1]">
								<xsl:if test="NI2 !=''">
									<OR>
										<xsl:attribute name="CLI"><xsl:value-of select="CL2"/></xsl:attribute>
										<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
										<xsl:attribute name="NIV"><xsl:value-of select="NI2"/></xsl:attribute>
										<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
										<xsl:attribute name="TOTPOL"><xsl:value-of select="TOTPOL"/></xsl:attribute>
										<xsl:attribute name="MON1"><xsl:value-of select="MON1"/></xsl:attribute>
										<xsl:attribute name="SIG1"><xsl:value-of select="SIG1"/></xsl:attribute>
										<xsl:attribute name="TOTIMP1"><xsl:value-of select="TOTIMP1"/></xsl:attribute>
										<xsl:attribute name="MON2"><xsl:value-of select="MON2"/></xsl:attribute>
										<xsl:attribute name="SIG2"><xsl:value-of select="SIG2"/></xsl:attribute>
										<xsl:attribute name="TOTIMP2"><xsl:value-of select="TOTIMP2"/></xsl:attribute>
										<xsl:for-each select="key('contacts-by-CL1CL2',concat(CL1,'_',CL2))">
											<xsl:if test="NI3 !=''">
												<PR>
													<xsl:attribute name="CLI"><xsl:value-of select="CL3"/></xsl:attribute>
													<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
													<xsl:attribute name="NIV"><xsl:value-of select="NI3"/></xsl:attribute>
													<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
													<xsl:attribute name="TOTPOL"><xsl:value-of select="TOTPOL"/></xsl:attribute>
													<xsl:attribute name="MON1"><xsl:value-of select="MON1"/></xsl:attribute>
													<xsl:attribute name="SIG1"><xsl:value-of select="SIG1"/></xsl:attribute>
													<xsl:attribute name="MON2"><xsl:value-of select="MON2"/></xsl:attribute>
													<xsl:attribute name="SIG2"><xsl:value-of select="SIG2"/></xsl:attribute>
													<xsl:attribute name="TOTIMP2"><xsl:value-of select="TOTIMP2"/></xsl:attribute>
												</PR>
											</xsl:if>
										</xsl:for-each>
									</OR>
								</xsl:if>
								<xsl:if test="NI2 =''">
									<xsl:for-each select="key('contacts-by-CL1CL2',concat(CL1,'_',CL2))">
										<xsl:if test="NI3 !=''">
											<PR>
												<xsl:attribute name="CLI"><xsl:value-of select="CL3"/></xsl:attribute>
												<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
												<xsl:attribute name="NIV"><xsl:value-of select="NI3"/></xsl:attribute>
												<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
												<xsl:attribute name="TOTPOL"><xsl:value-of select="TOTPOL"/></xsl:attribute>												
												<xsl:attribute name="MON1"><xsl:value-of select="MON1"/></xsl:attribute>
												<xsl:attribute name="SIG1"><xsl:value-of select="SIG1"/></xsl:attribute>
												<xsl:attribute name="TOTIMP1"><xsl:value-of select="TOTIMP1"/></xsl:attribute>
												<xsl:attribute name="MON2"><xsl:value-of select="MON2"/></xsl:attribute>
												<xsl:attribute name="SIG2"><xsl:value-of select="SIG2"/></xsl:attribute>
												<xsl:attribute name="TOTIMP2"><xsl:value-of select="TOTIMP2"/></xsl:attribute>
											</PR>
										</xsl:if>
									</xsl:for-each>
								</xsl:if>
							</xsl:for-each>
						</xsl:for-each>
					</GO>
				</xsl:if>
				<xsl:if test="(NI1 ='')">
					<xsl:for-each select="key('contacts-by-CL1',CL1)">
						<xsl:for-each select="(current())[count(. | key('contacts-by-CL1CL2', concat(CL1,'_',CL2))[1]) = 1]">
							<xsl:if test="NI2 !=''">
								<OR>
									<xsl:attribute name="CLI"><xsl:value-of select="CL2"/></xsl:attribute>
									<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
									<xsl:attribute name="NIV"><xsl:value-of select="NI2"/></xsl:attribute>
									<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
									<xsl:attribute name="TOTPOL"><xsl:value-of select="TOTPOL"/></xsl:attribute>
									<xsl:attribute name="MON1"><xsl:value-of select="MON1"/></xsl:attribute>
									<xsl:attribute name="SIG1"><xsl:value-of select="SIG1"/></xsl:attribute>
									<xsl:attribute name="TOTIMP1"><xsl:value-of select="TOTIMP1"/></xsl:attribute>
									<xsl:attribute name="MON2"><xsl:value-of select="MON2"/></xsl:attribute>
									<xsl:attribute name="SIG2"><xsl:value-of select="SIG2"/></xsl:attribute>
									<xsl:attribute name="TOTIMP2"><xsl:value-of select="TOTIMP2"/></xsl:attribute>
									<xsl:for-each select="key('contacts-by-CL1CL2',concat(CL1,'_',CL2))">
										<xsl:if test="NI3 !=''">
											<PR>
												<xsl:attribute name="CLI"><xsl:value-of select="CL3"/></xsl:attribute>
												<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
												<xsl:attribute name="NIV"><xsl:value-of select="NI3"/></xsl:attribute>
												<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
												<xsl:attribute name="TOTPOL"><xsl:value-of select="TOTPOL"/></xsl:attribute>
												<xsl:attribute name="MON1"><xsl:value-of select="MON1"/></xsl:attribute>
												<xsl:attribute name="SIG1"><xsl:value-of select="SIG1"/></xsl:attribute>
												<xsl:attribute name="TOTIMP1"><xsl:value-of select="TOTIMP1"/></xsl:attribute>
												<xsl:attribute name="MON2"><xsl:value-of select="MON2"/></xsl:attribute>
												<xsl:attribute name="SIG2"><xsl:value-of select="SIG2"/></xsl:attribute>
												<xsl:attribute name="TOTIMP2"><xsl:value-of select="TOTIMP2"/></xsl:attribute>
											</PR>
										</xsl:if>
									</xsl:for-each>
								</OR>
							</xsl:if>
							<xsl:if test="NI2 =''">
								<xsl:for-each select="key('contacts-by-CL1CL2',concat(CL1,'_',CL2))">
									<xsl:if test="NI3 !=''">
										<PR>
											<xsl:attribute name="CLI"><xsl:value-of select="CL3"/></xsl:attribute>
											<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
											<xsl:attribute name="NIV"><xsl:value-of select="NI3"/></xsl:attribute>
											<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
											<xsl:attribute name="TOTPOL"><xsl:value-of select="TOTPOL"/></xsl:attribute>
											<xsl:attribute name="MON1"><xsl:value-of select="MON1"/></xsl:attribute>
											<xsl:attribute name="SIG1"><xsl:value-of select="SIG1"/></xsl:attribute>
											<xsl:attribute name="TOTIMP1"><xsl:value-of select="TOTIMP1"/></xsl:attribute>
											<xsl:attribute name="MON2"><xsl:value-of select="MON2"/></xsl:attribute>
											<xsl:attribute name="SIG2"><xsl:value-of select="SIG2"/></xsl:attribute>
											<xsl:attribute name="TOTIMP2"><xsl:value-of select="TOTIMP2"/></xsl:attribute>
										</PR>
									</xsl:if>
								</xsl:for-each>
							</xsl:if>
						</xsl:for-each>
					</xsl:for-each>
				</xsl:if>
			</xsl:for-each>
		</EST>
	</xsl:template>
</xsl:stylesheet>
