migracioncomplus
================

Servicios migrados desde COM+ a Java

Al 2016, estamos trabajando sobre el proyecto https://github.com/snoopconsulting/migracioncomplus/tree/master/dev/NOVServices
que se despliega como un war en Weblogic

Este proyecto hace referencia a distintos proyectos maven que contienen los componentes migrados, todos los CMS-* por ejemplo CMS-mqgen

Dentro de este repo hay código que quedó de la época del proyecto Insis; no lo borramos porque puede que lo necesitemos como referencia para las futuras migraciones. Sí borramos el código que se había desarrollado para integrar Insis via OSB ( quedó en la historia del repo )

También están dentro de este repo:

* Dispatcher: COM+ que hace de gateway al Weblogic; exporta los servicios que tenemos en Weblogic a COM+, proveyendo una interface que llaman los componentes en VB. https://github.com/snoopconsulting/migracioncomplus/tree/master/FrontEnd
* Converter: proyecto usado para procesar los .java que genera el conversor de VB a Java, acomodando diversos aspectos como packages, expresiones que quedan mal traducidas, etc. 


