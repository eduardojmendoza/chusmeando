package com.qbe.services.ovmqemision.impl;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ovmqemision.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVBusqVehic implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQEmision.lbaw_OVBusqVehic";
  static final String mcteOpID = "1502";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_PATENTE = "//PATENBUS";
  static final String mcteParam_MOTOR = "//MOTORBUS";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQConnectionConnector wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarPatente = "";
    String wvarMotor = "";
    Variant wvarPos = new Variant();
    String wvarParseString = "";
    int wvarStringLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //Par�metros:
      //- Usuario PIC X(10)
      //- Patente PIC X(10)
      //- Motor   PIC X(25)
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarPatente = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PATENTE )  ) + Strings.space( 10 ), 10 );
      wvarMotor = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MOTOR )  ) + Strings.space( 25 ), 25 );
      //
      //DUMMYYYYYYY
      if( ! (wobjXMLRequest.selectSingleNode( "//DUMMY" )  == (org.w3c.dom.Node) null) )
      {
        Response.set( "<Response><Estado resultado='true' mensaje=''/>" );
        Response.set( Response + "<VEHICULO>" );
        Response.set( Response + "<VEHICSEC>" + "123456789" + "</VEHICSEC>" );
        Response.set( Response + "<PATENNUM>" + "RSS122" + "</PATENNUM>" );
        Response.set( Response + "<MOTORNUM>" + "1234567890123456789012345" + "</MOTORNUM>" );
        Response.set( Response + "<AUVEHDES>" + "FORD FALCON" + "</AUVEHDES>" );
        Response.set( Response + "<FABRIANN>" + "1981" + "</FABRIANN>" );
        Response.set( Response + "<AUMODORI>" + "A" + "</AUMODORI>" );
        Response.set( Response + "</VEHICULO>" );
        Response.set( Response + "</Response>" );
        wobjXMLRequest = null;
        /*TBD mobjCOM_Context.SetComplete() ;*/
        IAction_Execute = 0;
        return IAction_Execute;
      }

      wvarStep = 60;
      wobjXMLRequest = null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      if( wobjXMLParametros.getDocument().getChildNodes().getLength() > 0 )
      {
        wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      }
      //
      wvarStep = 110;
      wobjXMLParametros = null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarPatente + wvarMotor;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQConnectionConnector.newInstance();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, wvarParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG"))
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + wvarParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 240;
      wvarResult = "";
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos.set( 102 );
      //
      wvarStringLen = Strings.len( wvarParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( wvarParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = ParseoMensaje(wvarPos, wvarParseString, wvarStringLen);
        //
        wvarStep = 290;
        wobjXMLResponse = new XmlDomExtended();
        //
        wvarStep = 300;
        wobjXMLResponse.loadXML( wvarResult );
        //
        wvarStep = 310;
        if( wobjXMLResponse.selectNodes( "//VEHICULO" ) .getLength() > 0 )
        {
          //Devolvemos el Response
          Response.set( "<Response><Estado resultado='true' mensaje=''/>" + wvarResult + "</Response>" );
          //
        }
        else
        {
          //No hay datos para devolver
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos'/></Response>" );
          //
        }
        wobjXMLResponse = null;
      }
      //
      wvarStep = 320;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarPatente + wvarMotor + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant pvarPos, String pvarParseString, int pvarStringLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCantLin = 0;
    int wvarCounter = 0;
    //
    wvarResult = wvarResult + "<VEHICULOS>";
    //
    if( !Strings.trim( Strings.mid( pvarParseString, pvarPos.toInt(), 3 ) ).equals( "" ) )
    {
      wvarCantLin = Obj.toInt( Strings.mid( pvarParseString, pvarPos.toInt(), 3 ) );
      pvarPos.set( pvarPos.add( new Variant( 3 ) ) );
      //
      for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )
      {
        wvarResult = wvarResult + "<VEHICULO>";
        wvarResult = wvarResult + "<VEHICSEC>" + Strings.mid( pvarParseString, pvarPos.toInt(), 9 ) + "</VEHICSEC>";
        wvarResult = wvarResult + "<PATENNUM>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 9 ) ).toInt(), 10 ) + "</PATENNUM>";
        wvarResult = wvarResult + "<MOTORNUM>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 19 ) ).toInt(), 25 ) + "</MOTORNUM>";
        wvarResult = wvarResult + "<AUVEHDES>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 44 ) ).toInt(), 30 ) + "</AUVEHDES>";
        wvarResult = wvarResult + "<FABRIANN>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 74 ) ).toInt(), 4 ) + "</FABRIANN>";
        wvarResult = wvarResult + "<AUMODORI>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 78 ) ).toInt(), 1 ) + "</AUMODORI>";
        wvarResult = wvarResult + "</VEHICULO>";
        pvarPos.set( pvarPos.add( new Variant( 79 ) ) );
      }
      //
    }
    wvarResult = wvarResult + "</VEHICULOS>";
    //
    ParseoMensaje = wvarResult;
    return ParseoMensaje;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
