package com.qbe.services.ovmqemision.impl;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ovmqemision.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVGetPendScorDet implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQEmision.lbaw_OVGetPendScorDet";
  static final String mcteOpID = "1521";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_FechaDesde = "//FECHADESDE";
  static final String mcteParam_FechaHasta = "//FECHAHASTA";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQConnectionConnector wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarFechaDesde = "";
    String wvarFechaHasta = "";
    String wvarFDesdeVal = "";
    String wvarFHastaVal = "";
    String wvarRamo = "";
    String wvarPolizaAnn = "";
    String wvarPolizaSec = "";
    String wvarCertiPol = "";
    String wvarCertiAnn = "";
    String wvarCertiSec = "";
    Variant wvarPos = new Variant();
    String wvarParseString = "";
    int wvarStringLen = 0;
    Variant wvarCantLin = new Variant();
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //Par�metros:
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarFechaDesde = Strings.format( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FechaDesde )  ), "YYYYMMDD" );
      wvarFechaHasta = Strings.format( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FechaHasta )  ), "YYYYMMDD" );
      wvarRamo = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  ) + Strings.space( 4 ), 4 );
      wvarPolizaAnn = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN )  ), 2 );
      wvarPolizaSec = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC )  ), 6 );
      wvarCertiPol = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL )  ), 4 );
      wvarCertiAnn = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN )  ), 4 );
      wvarCertiSec = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC )  ), 6 );
      //
      //Valido las fechas recibidas, en formato mm/dd/aaaa
      wvarFDesdeVal = Strings.mid( wvarFechaDesde, 5, 2 ) + "/" + Strings.mid( wvarFechaDesde, 7, 2 ) + "/" + Strings.mid( wvarFechaDesde, 1, 4 );
      if( ! (new Variant( wvarFDesdeVal ).isDate()) )
      {
        Err.raise( -1, mcteClassName + "." + wcteFnName, "Par�metro 'Fecha Desde' No V�lido." );
      }
      wvarFHastaVal = Strings.mid( wvarFechaHasta, 5, 2 ) + "/" + Strings.mid( wvarFechaHasta, 7, 2 ) + "/" + Strings.mid( wvarFechaHasta, 1, 4 );
      if( ! (new Variant( wvarFHastaVal ).isDate()) )
      {
        Err.raise( -1, mcteClassName + "." + wcteFnName, "Par�metro 'Fecha Hasta' No v�lido." );
      }
      //
      wvarStep = 60;
      wobjXMLRequest = null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      if( wobjXMLParametros.getDocument().getChildNodes().getLength() > 0 )
      {
        wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      }
      //
      wvarStep = 110;
      wobjXMLParametros = null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarFechaDesde + wvarFechaHasta + wvarRamo + wvarPolizaAnn + wvarPolizaSec + wvarCertiPol + wvarCertiAnn + wvarCertiSec;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQConnectionConnector.newInstance();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, wvarParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG"))
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + wvarParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 240;
      wvarResult = "";
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos.set( 108 );
      //
      wvarStringLen = Strings.len( wvarParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( wvarParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = ParseoMensaje(wvarPos, wvarParseString, wvarStringLen, wvarCantLin);
        //
        wvarStep = 310;
        if( wvarCantLin.toInt() > 0 )
        {
          //
          wvarStep = 340;
          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
          //
        }
        else
        {
          //
          wvarStep = 350;
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          //
        }
        //
      }
      //
      wvarStep = 360;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarFechaDesde + wvarFechaHasta + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant pvarPos, String pvarParseString, int pvarStringLen, Variant pvarCantLin ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCantLin = 0;
    int wvarCounter = 0;
    //
    //NOTA:
    //No se indica el campo CANTLIN del vector.
    //Voy a hacer un ciclo hasta el m�ximo definido en el doc.
    //
    pvarPos.set( pvarPos.add( new Variant( 1 ) ) );
    //
    wvarResult = "<RS>";
    //
    wvarCounter = 0;
    while( (wvarCounter < 185) && (Strings.len( pvarParseString ) > pvarPos.toInt()) )
    {
      wvarResult = wvarResult + "<R>";
      wvarResult = wvarResult + "<RAMOPCOD>" + Strings.mid( pvarParseString, pvarPos.toInt(), 4 ) + "</RAMOPCOD>";
      wvarResult = wvarResult + "<POLIZANN>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 4 ) ).toInt(), 2 ) + "</POLIZANN>";
      wvarResult = wvarResult + "<POLIZSEC>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 6 ) ).toInt(), 6 ) + "</POLIZSEC>";
      wvarResult = wvarResult + "<CERTIPOL>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 12 ) ).toInt(), 4 ) + "</CERTIPOL>";
      wvarResult = wvarResult + "<CERTIANN>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 16 ) ).toInt(), 4 ) + "</CERTIANN>";
      wvarResult = wvarResult + "<CERTISEC>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 20 ) ).toInt(), 6 ) + "</CERTISEC>";
      wvarResult = wvarResult + "<SUPLENUM>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 26 ) ).toInt(), 4 ) + "</SUPLENUM>";
      wvarResult = wvarResult + "<CLIENSEC>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 30 ) ).toInt(), 9 ) + "</CLIENSEC>";
      wvarResult = wvarResult + "<CLIENDES>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 39 ) ).toInt(), 30 ) + "</CLIENDES>";
      wvarResult = wvarResult + "<TOMARIES>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 69 ) ).toInt(), 30 ) + "</TOMARIES>";
      wvarResult = wvarResult + "<SITUCPOL>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 99 ) ).toInt(), 30 ) + "</SITUCPOL>";
      wvarResult = wvarResult + "<TIPOOPER>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 129 ) ).toInt(), 30 ) + "</TIPOOPER>";
      wvarResult = wvarResult + "<FECING>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 159 ) ).toInt(), 10 ) + "</FECING>";
      wvarResult = wvarResult + "<AGENTCLA>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 169 ) ).toInt(), 2 ) + "</AGENTCLA>";
      wvarResult = wvarResult + "<AGENTCOD>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 171 ) ).toInt(), 4 ) + "</AGENTCOD>";
      wvarResult = wvarResult + "<COLECTIP>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 175 ) ).toInt(), 1 ) + "</COLECTIP>";
      wvarResult = wvarResult + "</R>";
      pvarPos.set( pvarPos.add( new Variant( 176 ) ) );
      wvarCounter = wvarCounter + 1;
    }
    //
    wvarResult = wvarResult + "</RS>";
    //
    pvarCantLin.set( wvarCounter );
    //
    ParseoMensaje = wvarResult;
    return ParseoMensaje;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
