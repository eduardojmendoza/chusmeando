package com.qbe.services.ovmqemision.impl;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ovmqemision.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVEmisPropuesta implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQEmision.lbaw_OVEmisPropuesta";
  static final String mcteOpID = "1526";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARCOD = "//USUARIO";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_Suplenum = "//SUPLENUM";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQConnectionConnector wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarRamo = "";
    String wvarPolizaAnn = "";
    String wvarPolizaSec = "";
    String wvarCertiPol = "";
    String wvarCertiAnn = "";
    String wvarCertiSec = "";
    String wvarSuplenum = "";
    com.qbe.services.IAction wvarAction = null;
    String wvarRespFiles = "";
    Variant wvarPos = new Variant();
    String wvarParseString = "";
    int wvarStringLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD )  ) + Strings.space( 10 ), 10 );
      wvarRamo = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  ) + Strings.space( 4 ), 4 );
      wvarPolizaAnn = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN )  ), 2 );
      wvarPolizaSec = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC )  ), 6 );
      wvarCertiPol = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL )  ), 4 );
      wvarCertiAnn = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN )  ), 4 );
      wvarCertiSec = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC )  ), 6 );
      wvarSuplenum = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Suplenum )  ), 4 );
      //
      wvarStep = 60;

      //DUMMY!!!!!!!!!!!
      if( ! (wobjXMLRequest.selectSingleNode( "//DUMMY" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DUMMY" )  ).equals( "NOOK" ) )
        {
          Response.set( "<Response><Estado resultado='true' mensaje=''/>" );
          Response.set( Response + "<TEXTO>" + "No se pudo realizar la emisi�n de la propuesta" + "</TEXTO>" );
          Response.set( Response + "<ERRORES><ERROR>La propuesta posee Retenciones</ERROR></ERRORES>" );
          Response.set( Response + "<CLIENTE>" );
          Response.set( Response + "<CLIENSEC>" + "000000001" + "</CLIENSEC>" );
          Response.set( Response + "<CLIENDES>" + "JUAN ERNESTO PEREZ" + "</CLIENDES>" );
          Response.set( Response + "</CLIENTE>" );
          Response.set( Response + "<OPERASEC>" + "AUS10123456789" + "</OPERASEC>" );
          Response.set( Response + "<RETENCIONES>" );
          Response.set( Response + "<RETENCION>" );
          Response.set( Response + "<SITUACOD>" + "0001" + "</SITUACOD>" );
          Response.set( Response + "<SITUASEC>" + "003" + "</SITUASEC>" );
          Response.set( Response + "<SITUADES>" + "FALTA INSTALACION EQUIPO DE RASTREO" + "</SITUADES>" );
          Response.set( Response + "<FECRETEN>" + DateTime.format( DateTime.now(), "dd/MM/yyyy" ) + "</FECRETEN>" );
          Response.set( Response + "<USUARORI>" + wvarUsuario + "</USUARORI>" );
          Response.set( Response + "<ORIGECOD>" + "2" + "</ORIGECOD>" );
          Response.set( Response + "<FECRESOL>" + "</FECRESOL>" );
          Response.set( Response + "<USUARRES>" + "</USUARRES>" );
          Response.set( Response + "</RETENCION>" );
          Response.set( Response + "<RETENCION>" );
          Response.set( Response + "<SITUACOD>" + "0002" + "</SITUACOD>" );
          Response.set( Response + "<SITUASEC>" + "005" + "</SITUASEC>" );
          Response.set( Response + "<SITUADES>" + "FALTA VERIFICACION" + "</SITUADES>" );
          Response.set( Response + "<FECRETEN>" + DateTime.format( DateTime.now(), "dd/MM/yyyy" ) + "</FECRETEN>" );
          Response.set( Response + "<USUARORI>" + wvarUsuario + "</USUARORI>" );
          Response.set( Response + "<ORIGECOD>" + "2" + "</ORIGECOD>" );
          Response.set( Response + "<FECRESOL>" + "</FECRESOL>" );
          Response.set( Response + "<USUARRES>" + "</USUARRES>" );
          Response.set( Response + "</RETENCION>" );
          Response.set( Response + "</RETENCIONES>" );
          Response.set( Response + "</Response>" );
        }
        else
        {
          Response.set( "<Response><Estado resultado='true' mensaje=''/>" );
          Response.set( Response + "<TEXTO>" + "Emisi�n de propuesta realizada correctamente" + "</TEXTO>" );
          Response.set( Response + "<OPERASEC>" + "AUS10123456789" + "</OPERASEC>" );
          //Response = Response & "<FILES>"
          //Response = Response & "<FILE><DESCRIPCION>PRUEBA</DESCRIPCION><RUTA>D:\COMP\LBAOVFaseII\MERCOSUR.PDF</RUTA></FILE>"
          //Response = Response & "<FILE><DESCRIPCION>PRUEBA</DESCRIPCION><RUTA>D:\COMP\LBAOVFaseII\FRENPOLI.pdf</RUTA></FILE>"
          //Response = Response & "</FILES>"
          wvarAction = new Variant( new com.qbe.services.lbaw_OVImprimirPoliza() )((com.qbe.services.IAction) new com.qbe.services.lbaw_OVImprimirPoliza().toObject());
          wvarAction.Execute( Request, wvarRespFiles, ContextInfo );
          wobjXMLResponse = new XmlDomExtended();
          wobjXMLResponse.loadXML( wvarRespFiles );
          if( ! (wobjXMLResponse.selectSingleNode( "//FILES" )  == (org.w3c.dom.Node) null) )
          {
            Response.set( Response + XmlDomExtended.marshal(wobjXMLResponse.selectSingleNode( "//FILES" )) );
          }
          Response.set( Response + "</Response>" );
        }
        wobjXMLResponse = null;
        wobjXMLRequest = null;
        wvarAction = (com.qbe.services.IAction) null;
        return IAction_Execute;
      }

      wobjXMLResponse = null;
      wobjXMLRequest = null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      if( wobjXMLParametros.getDocument().getChildNodes().getLength() > 0 )
      {
        wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      }
      //
      wvarStep = 110;
      wobjXMLParametros = null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarRamo + wvarPolizaAnn + wvarPolizaSec + wvarCertiPol + wvarCertiAnn + wvarCertiSec + wvarSuplenum;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQConnectionConnector.newInstance();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, wvarParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG"))
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + wvarParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 240;
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos.set( 96 );
      wvarStringLen = Strings.len( wvarParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( wvarParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = ParseoMensaje(wvarPos, wvarParseString, wvarStringLen);
        //
        if( wvarStringLen > wvarPos.toInt() )
        {
          //Devolvemos el Response
          Response.set( "<Response><Estado resultado='true' mensaje=''/>" + wvarResult + "</Response>" );
          //
        }
        else
        {
          //No hay datos para devolver
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos'/></Response>" );
          //
        }
        //
      }
      //
      wvarStep = 290;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarRamo + wvarPolizaAnn + wvarPolizaSec + wvarCertiPol + wvarCertiAnn + wvarCertiSec + wvarSuplenum + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant pvarPos, String pvarParseString, int pvarStringLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCounter = 0;
    //
    //NOTA: Los campos LNK-CLAVE-SALIDA y LNK-TEXTO-SAL del documento no est�n
    //completamente definidos, les falta la descripci�n y el tipo.
    //Por ahora no los incluyo en el XML de salida.
    //Tampoco hay CANTLIN.
    //Hago un LOOP hasta el m�ximo definido en el doc.
    //
    pvarPos.set( pvarPos.add( new Variant( 1 ) ) );
    //
    //Datos de la P�liza
    wvarResult = wvarResult + "<TEXTO>" + Strings.mid( pvarParseString, pvarPos.toInt(), 30 ) + "</TEXTO>";
    wvarResult = wvarResult + "<POLIZA>";
    wvarResult = wvarResult + "<RAMOPCOD>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 30 ) ).toInt(), 4 ) + "</RAMOPCOD>";
    wvarResult = wvarResult + "<POLIZANN>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 34 ) ).toInt(), 2 ) + "</POLIZANN>";
    wvarResult = wvarResult + "<POLIZSEC>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 36 ) ).toInt(), 6 ) + "</POLIZSEC>";
    wvarResult = wvarResult + "<CERTIPOL>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 42 ) ).toInt(), 4 ) + "</CERTIPOL>";
    wvarResult = wvarResult + "<CERTIANN>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 46 ) ).toInt(), 4 ) + "</CERTIANN>";
    wvarResult = wvarResult + "<CERTISEC>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 50 ) ).toInt(), 6 ) + "</CERTISEC>";
    wvarResult = wvarResult + "<SUPLENUM>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 56 ) ).toInt(), 4 ) + "</SUPLENUM>";
    wvarResult = wvarResult + "<OPERAPOL>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 60 ) ).toInt(), 6 ) + "</OPERAPOL>";
    wvarResult = wvarResult + "</POLIZA>";
    //
    //Vector de Mensajes de Error
    pvarPos.set( pvarPos.add( new Variant( 66 ) ) );
    wvarCounter = 0;
    wvarResult = wvarResult + "<ERRORES>";
    while( (wvarCounter < 30) && (Strings.len( pvarParseString ) > pvarPos.toInt()) && (!Strings.trim( Strings.mid( pvarParseString, pvarPos.toInt(), 60 ) ).equals( "" )) )
    {
      wvarResult = wvarResult + "<ERROR>" + Strings.trim( Strings.mid( pvarParseString, pvarPos.toInt(), 60 ) ) + "</ERROR>";
      pvarPos.set( pvarPos.add( new Variant( 60 ) ) );
      wvarCounter = wvarCounter + 1;
    }
    wvarResult = wvarResult + "</ERRORES>";
    //
    ParseoMensaje = wvarResult;
    return ParseoMensaje;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
