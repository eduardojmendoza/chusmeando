package com.qbe.services.ovmqemision.impl;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ovmqemision.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVAisGetSolicAUS implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQEmision.lbaw_OVAisGetSolicAUS";
  static final String mcteOpID = "1528";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARCOD = "//USUARIO";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_Suplenum = "//SUPLENUM";
  /**
   * Definicion XML de Salida
   * Datos del Cliente
   */
  static final String mcteParam_NUMEDOCU = "//NUMEDOCU";
  static final String mcteParam_TIPODOCU = "//TIPODOCU";
  static final String mcteParam_CliCLIENAP1 = "//CLICLIENAP1";
  static final String mcteParam_CliCLIENAP2 = "//CLICLIENAP2";
  static final String mcteParam_CliCLIENNOM = "//CLICLIENNOM";
  static final String mcteParam_CliNACIMANN = "//NACIMANN";
  static final String mcteParam_CliNACIMMES = "//NACIMMES";
  static final String mcteParam_CliNACIMDIA = "//NACIMDIA";
  static final String mcteParam_Sexo = "//SEXO";
  static final String mcteParam_ESTCIV = "//ESTCIV";
  static final String mcteParam_CliPAISSCOD = "//CLIPAISSCOD";
  static final String mcteParam_CliIDIOMCOD = "//CLIIDIOMCOD";
  static final String mcteParam_CliEFECTANN = "//CLIEFECTANN";
  static final String mcteParam_CliEFECTMES = "//CLIEFECTMES";
  static final String mcteParam_CliEFECTDIA = "//CLIEFECTDIA";
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_CliCLIENTIP = "//CLICLIENTIP";
  static final String mcteParam_CliCLIENFUM = "//CLICLIENFUM";
  static final String mcteParam_CliCLIENDOZ = "//CLICLIENDOZ";
  static final String mcteParam_CliABRIDTIP = "//CLIABRIDTIP";
  static final String mcteParam_CliABRIDNUM = "//CLIABRIDNUM";
  static final String mcteParam_CliCLIENORG = "//CLICLIENORG";
  /**
   * Domicilio Cliente
   */
  static final String mcteParam_DOMICSEC = "//DOMICSEC";
  static final String mcteParam_CliDOMICCAL = "//CLIDOMICCAL";
  static final String mcteParam_CliDOMICDOM = "//CLIDOMICDOM";
  static final String mcteParam_CliDOMICDNU = "//CLIDOMICDNU";
  static final String mcteParam_CliDOMICESC = "//CLIDOMICESC";
  static final String mcteParam_CliDOMICPIS = "//CLIDOMICPIS";
  static final String mcteParam_CliDOMICPTA = "//CLIDOMICPTA";
  static final String mcteParam_CliDOMICPOB = "//CLIDOMICPOB";
  static final String mcteParam_CliDOMICCPO = "//CLIDOMICCPO";
  static final String mcteParam_CliDOMPROVICOD = "//CLIDOMPROVICOD";
  static final String mcteParam_CliDOMPAISSCOD = "//CLIDOMPAISSCOD";
  /**
   * Const mcteParam_CliDOMICPRE             As String = "//CLIDOMICPRE"
   */
  static final String mcteParam_CliDOMCPACODPO = "//CLIDOMCPACODPO";
  static final String mcteParam_CliDOMICTLF = "//CLIDOMICTLF";
  /**
   * Cuenta Bancaria
   */
  static final String mcteParam_CUENTSEC = "//CUENTSEC";
  static final String mcteParam_TIPOCUEN = "//TIPOCUEN";
  static final String mcteParam_BANCOCOD_CUENTA = "//BANCOCODCUE";
  static final String mcteParam_SUCURCOD_CUENTA = "//SUCURCODCUE";
  static final String mcteParam_CUENTDC = "//CUENTDC";
  static final String mcteParam_CUENNUME = "//CUENNUME";
  static final String mcteParam_TARJECOD = "//TARJECOD";
  static final String mcteParam_VENCIANN = "//VENCIANN";
  static final String mcteParam_VENCIMES = "//VENCIMES";
  static final String mcteParam_VENCIDIA = "//VENCIDIA";
  /**
   * Impuestos Vector de 10
   */
  static final String mcteParam_IMPUESTO = "//IMPUESTOS/IMPUESTO";
  static final String mcteParam_IMPUECOD = "//IMPUECOD";
  static final String mcteParam_IMPUEPAR = "//IMPUEPAR";
  static final String mcteParam_IMPUEDAT = "//IMPUEDAT";
  /**
   * Vector 1208 de 10 posiciones
   */
  static final String mcteParam_NODOCLA = "//CLAIS/CLAI";
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_CLAIDENT = "//CLAIDENT";
  /**
   * Situacion Ganancias Cliente
   */
  static final String mcteParam_CLIENIVA = "//CLIENIVA";
  static final String mcteParam_CLIESEEX = "//CLIESEEX";
  static final String mcteParam_CLIEIBTP = "//CLIEIBTP";
  static final String mcteParam_CLIEIBEX = "//CLIEIBEX";
  static final String mcteParam_CLIEIiTP = "//CLIEIiTP";
  static final String mcteParam_CLIEIiEX = "//CLIEIiEX";
  static final String mcteParam_NROIIBB = "//NROIIBB";
  static final String mcteParam_CLIEGNTP = "//CLIEGNTP";
  static final String mcteParam_DOCUMTIPIVA = "//TIPODOCU";
  static final String mcteParam_DOCUMDATCLI = "//NUMEDOCU";
  /**
   * Vector de 10 posiciones de medios de contacto
   */
  static final String mcteParam_NODOMEDIOCONTACTO = "//MEDIOSCONTACTO/MEDIO";
  static final String mcteParam_MEDCOSEC = "//MEDCOSEC";
  static final String mcteParam_MEDCOCOD = "//MEDCOCOD";
  static final String mcteParam_MEDCODAT = "//MEDCODAT";
  static final String mcteParam_MEDCOSYS = "//MEDCOSYS";
  /**
   * Profesion Cliente
   */
  static final String mcteParam_PROFESEC = "//PROFESEC";
  static final String mcteParam_PROFECOD = "//PROFECOD";
  static final String mcteParam_TIPOCODI = "//TIPOCODI";
  /**
   * Datos del vehiculo
   */
  static final String mcteParam_VEHICSEC = "//VEHICSEC";
  static final String mcteParam_AUMARCOD = "//AUMARCOD";
  static final String mcteParam_AUMODCOD = "//AUMODCOD";
  static final String mcteParam_AUSUBCOD = "//AUSUBCOD";
  static final String mcteParam_AUADICOD = "//AUADICOD";
  static final String mcteParam_AUMODORI = "//AUMODORI";
  static final String mcteParam_SIFMVEHI_DES = "//SIFMVEHI_DES";
  static final String mcteParam_AUTIPCOD = "//AUTIPCOD";
  static final String mcteParam_FABRICAN = "//FABRICAN";
  static final String mcteParam_FABRICMES = "//FABRICMES";
  static final String mcteParam_MOTORNUM = "//MOTORNUM";
  static final String mcteParam_CHASINUM = "//CHASINUM";
  static final String mcteParam_PATENNUM = "//PATENNUM";
  static final String mcteParam_VEHCLRCOD = "//VEHCLRCOD";
  static final String mcteParam_AUCATCOD = "//AUCATCOD";
  static final String mcteParam_AUUSOCOD = "//AUUSOCOD";
  static final String mcteParam_AUVTVCOD = "//AUVTVCOD";
  static final String mcteParam_AUVTVANN = "//AUVTVANN";
  static final String mcteParam_AUVTVMES = "//AUVTVMES";
  static final String mcteParam_AUVTVDIA = "//AUVTVDIA";
  static final String mcteParam_AUKLMNUM = "//AUKLMNUM";
  static final String mcteParam_AUPAISCOD = "//AUPAISCOD";
  static final String mcteParam_AUPROVICOD = "//AUPROVICOD";
  /**
   * Const mcteParam_AUDOMCPACODPO           As String = "//AUDOMCPACODPO"
   * mcteParam_CliDOMCPACODPO
   */
  static final String mcteParam_GUGARAGE = "//GUGARAGE";
  static final String mcteParam_GUDOMICI = "//GUDOMICI";
  /**
   *  Asegurados Adicionales ...son 10 iteraciones 'ojo se estan poniendo los hijos
   */
  static final String mcteParam_CONDUCTORES = "//CONDUCTORES/CONDUCTOR";
  static final String mcteParam_CONDUSEC = "//CONDUSEC";
  static final String mcteParam_CONDUTDO = "//CONDUTDO";
  static final String mcteParam_DOCUMDAT = "//CONDUDOC";
  static final String mcteParam_CONDUAPE = "//APELLIDOHIJO";
  static final String mcteParam_CONDUNOM = "//NOMBREHIJO";
  static final String mcteParam_CONDUVIN = "//CONDUVIN";
  static final String mcteParam_CONDUFEC = "//NACIMHIJO";
  static final String mcteParam_CONDUSEX = "//SEXOHIJO";
  static final String mcteParam_CONDUEST = "//ESTADOHIJO";
  static final String mcteParam_CONDUEXC = "//EXCLUIDO";
  /**
   *  Accesorios ...son 10 iteraciones
   */
  static final String mcteParam_ACCESORIOS = "//ACCESORIOS/ACCESORIO";
  static final String mcteParam_VEACCSEC = "//VEACCSEC";
  static final String mcteParam_AUACCCOD = "//CODIGOACC";
  static final String mcteParam_AUVEASUM = "//PRECIOACC";
  static final String mcteParam_AUVEADES = "//DESCRIPCIONACC";
  static final String mcteParam_AUVEADEP = "//DEPRECIABLEACC";
  /**
   * 
   * Mas datos del cliente
   * mcteParam_CLIENSEC
   * mcteParam_CUENTSEC
   * mcteParam_DOMICSEC
   */
  static final String mcteParam_COBROFOR = "//COBROFOR";
  /**
   * mcteParam_CLIENIVA
   * Campa�as vector de 10
   */
  static final String mcteParam_CAMP_CODIGO = "//CAMP_CODIGO";
  /**
   * Coberturas, vector de 10
   */
  static final String mcteParam_COBERTURAS = "//COBERTURAS/COBERTURA";
  static final String mcteParam_COBERCOD = "//COBERCOD";
  static final String mcteParam_COBERORD = "//COBERORD";
  static final String mcteParam_CAPITASG = "//CAPITASG";
  static final String mcteParam_CAPITIMP = "//CAPITIMP";
  /**
   * Datos de la Poliza
   */
  static final String mcteParam_EXPEDNUM = "//EXPEDNUM";
  static final String mcteParam_CENTROCOD = "//CENTRCOD";
  static final String mcteParam_POLEFECTANN = "//POLEFECTANN";
  static final String mcteParam_POLEFECTMES = "//POLEFECTMES";
  static final String mcteParam_POLEFECTDIA = "//POLEFECTDIA";
  static final String mcteParam_POLVENCIANN = "//POLVENCIANN";
  static final String mcteParam_POLVENCIMES = "//POLVENCIMES";
  static final String mcteParam_POLVENCIDIA = "//POLVENVIDIA";
  static final String mcteParam_COBROCOD = "//COBROCOD";
  static final String mcteParam_COASETIP = "//COASETIP";
  static final String mcteParam_EXESEPOR = "//EXESEPOR";
  static final String mcteParam_FESOLANN = "//FESOLANN";
  static final String mcteParam_FESOLMES = "//FESOLMES";
  static final String mcteParam_FESOLDIA = "//FESOLDIA";
  static final String mcteParam_FEINGANN = "//FEINGANN";
  static final String mcteParam_FEINGMES = "//FEINGMES";
  static final String mcteParam_FEINGDIA = "//FEINGDIA";
  static final String mcteParam_RECARPOR = "//RECARPOR";
  /**
   * ESTO ES PLAN DE PAGO
   */
  static final String mcteParam_PLANCOD = "//PLANCOD";
  /**
   * SI TIENE O NO ACREEDOR PRENDARIO
   */
  static final String mcteParam_SWACREPR = "//SWACREPR";
  /**
   *  Acreedor Prendario
   */
  static final String mcteParam_NUMEDOCU_ACRE = "//NUMEDOCU_ACRE";
  static final String mcteParam_TIPODOCU_ACRE = "//TIPODOCU_ACRE";
  static final String mcteParam_APELLIDO_ACRE = "//APELLIDO";
  static final String mcteParam_APELLIDO_ACRE2 = "//APELLIDO2";
  static final String mcteParam_NOMBRES = "//NOMBRES";
  static final String mcteParam_CLIENTIP_ACRE = "//CLIENTIP_ACRE";
  static final String mcteParam_DOMICCAL = "//DOMICCAL";
  static final String mcteParam_DOMICDOM = "//DOMICDOM";
  static final String mcteParam_DOMICDNU = "//DOMICDNU";
  static final String mcteParam_DOMICESC = "//DOMICESC";
  static final String mcteParam_DOMICPIS = "//DOMICPIS";
  static final String mcteParam_DOMICPTA = "//DOMICPTA";
  static final String mcteParam_DOMICPOB = "//DOMICPOB";
  /**
   * Const mcteParam_DOMICCPO                As String = "//DOMICCPO"
   */
  static final String mcteParam_PROVICOD = "//PROVICOD";
  static final String mcteParam_PAISSCOD = "//PAISSCOD";
  static final String mcteParam_DOMICPRE = "//DOMICPRE";
  static final String mcteParam_DOMICTLF = "//DOMICTLF";
  static final String mcteParam_CPACODPO = "//DOMICCPO";
  static final String mcteParam_FRANQCOD = "//FRANQCOD";
  /**
   * PLAN DE LA POLIZA (TERCEROS, TODO RIESGO, ETC)
   */
  static final String mcteParam_PLANPILICOD = "//PLANPOLICOD";
  /**
   * 
   */
  static final String mcteParam_PRECIOFT = "//PRECIOFT";
  static final String mcteParam_ZONA = "//ZONA";
  static final String mcteParam_CODPROV = "//CODPROV";
  /**
   * VALOR DEL VEHICULO
   */
  static final String mcteParam_SUMALBA = "//SUMALBA";
  /**
   * VALOR DEL VEHICULO
   */
  static final String mcteParam_SUMASEG = "//SUMASEG";
  static final String mcteParam_CLUB_LBA = "//CLUB_LBA";
  static final String mcteParam_CTAKMS = "//CTAKMS";
  static final String mcteParam_ESCERO = "//ESCERO";
  /**
   *  Inspeccion
   */
  static final String mcteParam_EFECTANN = "//EFECTANN2";
  static final String mcteParam_EFECTMES = "//EFECTMES";
  static final String mcteParam_EFECTDIA = "//EFECTDIA";
  static final String mcteParam_ESTAINSP = "//ESTAINSP";
  static final String mcteParam_INSPECOD = "//INSPECOD";
  static final String mcteParam_INSPEDES = "//INSPECTOR";
  static final String mcteParam_CENSICOD = "//CENTRCOD_INS";
  static final String mcteParam_CENSIDES = "//CENTRDES";
  static final String mcteParam_INSPEKMS = "//INSPEKMS";
  static final String mcteParam_INSPEOBS = "//OBSERTXT";
  static final String mcteParam_INSPADOM = "//INSPEADOM";
  static final String mcteParam_INS_DOMICDOM = "//INS_DOMICDOM";
  static final String mcteParam_INS_CPACODPO = "//INS_CPACODPO";
  static final String mcteParam_INS_DOMICPOB = "//INS_DOMICPOB";
  static final String mcteParam_INS_PROVICOD = "//INS_PROVICOD";
  static final String mcteParam_INS_DOMICTLF = "//INS_DOMICTLF";
  static final String mcteParam_INS_RANGOHOR = "//INS_RANGOHOR";
  static final String mcteParam_DISPOCOD = "//RASTREO";
  static final String mcteParam_PRESTCOD = "//ALARMCOD";
  /**
   * mcteParam_ESCERO
   */
  static final String mcteParam_NROFACTU = "//NROFACTU";
  /**
   * Datos propios del estado del auto
   */
  static final String mcteParam_DDJJ = "//DDJJ_STRING";
  /**
   * mcteParam_AUMARCOD
   * mcteParam_AUMODCOD
   * mcteParam_AUSUBCOD
   * mcteParam_AUADICOD
   * mcteParam_AUMODORI
   * mcteParam_SIFMVEHI_DES
   * mcteParam_AUTIPCOD
   * mcteParam_FABRICAN
   * mcteParam_FABRICMES
   * mcteParam_MOTORNUM
   * mcteParam_CHASINUM
   * mcteParam_PATENNUM
   * mcteParam_VEHCLRCOD
   * mcteParam_AUCATCOD
   * mcteParam_AUUSOCOD
   * mcteParam_AUVTVDIA
   * mcteParam_AUVTVMES
   * mcteParam_AUVTVANN
   * mcteParam_AUKLMNUM
   * mcteParam_AUPAISCOD
   * mcteParam_AUPROVICOD
   * mcteParam_CliDOMCPACODPO
   * mcteParam_GUGARAGE
   * mcteParam_GUDOMICI
   */
  static final String mcteParam_AUCIASAN = "//AUCIASAN";
  static final String mcteParam_AUNUMSIN = "//AUNUMSIN";
  static final String mcteParam_AUNUMKMT = "//AUNUMKMT";
  static final String mcteParam_AUUSOGNC = "//AUUSOGNC";
  static final String mcteParam_AUANTANN = "//AUANTANN";
  /**
   * mcteParam_ESCERO
   * mcteParam_NROFACTU
   */
  static final String mcteParam_BANCOCOD = "//BANCOCOD";
  /**
   * mcteParam_DOCUMTIP
   * mcteParam_CLAIDENT
   */
  static final String mcteParam_CONCECOD = "//CONCECOD";
  static final String mcteParam_AGENTCOD = "//AGENTCOD";
  static final String mcteParam_AGENTCLA = "//AGENTCLA";
  static final String mcteParam_NUMCREDI = "//NUMCREDI";
  static final String mcteParam_FECCOPRE = "//FECCOPRE";
  static final String mcteParam_PLAZOMES = "//PLAZOMES";
  static final String mcteParam_PRECIOSO = "//PRECIOSO";
  static final String mcteParam_VIENEDMS = "//VIENEDMS";
  static final String mcteParam_SUCURCOD = "//SUCURCOD";
  static final String mcteParam_LEGAJO_VEND = "//LEGAJO_VEND";
  static final String mcteParam_LEGAJO_GTE = "//LEGAJO_GTE";
  static final String mcteParam_POLIZANNR = "//POLIZANNREFERIDO";
  static final String mcteParam_POLIZSECR = "//POLIZSECREFERIDO";
  static final String mcteParam_CERTIPOLR = "//CERTIPOLREFERIDO";
  static final String mcteParam_CERTIANNR = "//CERTIANNREFERIDO";
  static final String mcteParam_CERTISECR = "//CERTISECREFERIDO";
  static final String mcteParam_SUPLENUMR = "//SUPLENUMREFERIDO";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQConnectionConnector wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarRamo = "";
    String wvarPolizaAnn = "";
    String wvarPolizaSec = "";
    String wvarCertiPol = "";
    String wvarCertiAnn = "";
    String wvarCertiSec = "";
    String wvarSuplenum = "";
    Variant wvarPos = new Variant();
    String wvarParseString = "";
    int wvarStringLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 15;
      //
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD )  ) + Strings.space( 10 ), 10 );
      wvarRamo = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  ) + Strings.space( 4 ), 4 );
      wvarPolizaAnn = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN )  ), 2 );
      wvarPolizaSec = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC )  ), 6 );
      wvarCertiPol = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL )  ), 4 );
      wvarCertiAnn = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN )  ), 4 );
      wvarCertiSec = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC )  ), 6 );
      wvarSuplenum = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Suplenum )  ), 4 );
      //
      wvarStep = 20;
      //
      //
      wobjXMLRequest = null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      if( wobjXMLParametros.getDocument().getChildNodes().getLength() > 0 )
      {
        wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      }
      //
      wvarStep = 110;
      wobjXMLParametros = null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarRamo + wvarPolizaAnn + wvarPolizaSec + wvarCertiPol + wvarCertiAnn + wvarCertiSec + wvarSuplenum;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQConnectionConnector.newInstance();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, wvarParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG"))
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + wvarParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 240;
      wvarResult = "";
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos.set( 97 );
      //
      wvarStringLen = Strings.len( wvarParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( wvarParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = ParseoMensaje(wvarPos, wvarParseString, wvarStringLen);
        //
        wvarStep = 290;
        wobjXMLResponse = new XmlDomExtended();
        //
        wvarStep = 300;
        wobjXMLResponse.loadXML( wvarResult );
        //
        wvarStep = 310;
        if( wobjXMLResponse.selectNodes( "//VEHICULO" ) .getLength() > 0 )
        {
          //Devolvemos el Response
          Response.set( "<Response><Estado resultado='true' mensaje=''/>" + wvarResult + "</Response>" );
          //
        }
        else
        {
          //No hay datos para devolver
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos'/></Response>" );
          //
        }
        wobjXMLResponse = null;
      }
      //
      wvarStep = 320;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarRamo + wvarPolizaAnn + wvarPolizaSec + wvarCertiPol + wvarCertiAnn + wvarCertiSec + wvarSuplenum + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant pvarPos, String pvarParseString, int pvarStringLen ) throws Exception
  {
    String ParseoMensaje = "";
    XmlDomExtended wobjXmlDOMResp = null;
    org.w3c.dom.Element wobjNodoRoot = null;

    wobjXmlDOMResp = new XmlDomExtended();
    wobjXmlDOMResp.loadXML( "<SOLICITUD></SOLICITUD>" );
    wobjNodoRoot = (org.w3c.dom.Element) wobjXmlDOMResp.selectSingleNode( "//SOLICITUD" ) ;
    pvarPos.set( 5234 - 2 );
    //Datos del Cliente
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_TIPODOCU), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_NUMEDOCU), new Variant(0), new Variant(11), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliCLIENAP1), new Variant(0), new Variant(20), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliCLIENAP2), new Variant(0), new Variant(20), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliCLIENNOM), new Variant(0), new Variant(40), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliNACIMANN), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliNACIMMES), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliNACIMDIA), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_Sexo), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_ESTCIV), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliPAISSCOD), new Variant(0), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliIDIOMCOD), new Variant(0), new Variant(3), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FESOLANN), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FESOLMES), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FESOLDIA), new Variant(1), new Variant(2), new Variant(0) } );

    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CLIENSEC), new Variant(1), new Variant(9), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliCLIENTIP), new Variant(0), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliCLIENFUM), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliCLIENDOZ), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliABRIDTIP), new Variant(0), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliABRIDNUM), new Variant(0), new Variant(10), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliCLIENORG), new Variant(0), new Variant(3), new Variant(0) } );

    //Domicilio Cliente
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DOMICSEC), new Variant(0), new Variant(3), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMICCAL), new Variant(0), new Variant(5), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMICDOM), new Variant(0), new Variant(30), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMICDNU), new Variant(0), new Variant(5), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMICESC), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMICPIS), new Variant(0), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMICPTA), new Variant(0), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMICPOB), new Variant(0), new Variant(20), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMICCPO), new Variant(1), new Variant(5), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMPROVICOD), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMPAISSCOD), new Variant(0), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMCPACODPO), new Variant(0), new Variant(8), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMICTLF), new Variant(0), new Variant(30), new Variant(0) } );
    //
    //Cuenta Bancaria
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CUENTSEC), new Variant(0), new Variant(3), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_TIPOCUEN), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_BANCOCOD_CUENTA), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_SUCURCOD_CUENTA), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CUENTDC), new Variant(0), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CUENNUME), new Variant(0), new Variant(16), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_TARJECOD), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_VENCIANN), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_VENCIMES), new Variant(1), new Variant(2), new Variant(0) } );
    //
    //Impuestos Vector de 10
    invoke( "ParseVectorDatos", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_IMPUESTO), new Variant(mcteParam_IMPUECOD + ",0,3,0|" + mcteParam_IMPUEPAR + ",0,15,0|" + mcteParam_IMPUEDAT + ",0,60,0"), new Variant(10) } );
    //
    //Vector 1208 de 10 posiciones
    invoke( "ParseVectorDatos", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_NODOCLA), new Variant(mcteParam_DOCUMTIP + ",1,2,0|" + mcteParam_CLAIDENT + ",0,30,0"), new Variant(10) } );
    //
    //Situacion Ganancias Cliente
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CLIENIVA), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CLIESEEX), new Variant(2), new Variant(3), new Variant(2) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CLIEIBTP), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CLIEIBEX), new Variant(2), new Variant(3), new Variant(2) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CLIEIiTP), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CLIEIiEX), new Variant(2), new Variant(3), new Variant(2) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_NROIIBB), new Variant(0), new Variant(15), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CLIEGNTP), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DOCUMTIPIVA), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DOCUMDATCLI), new Variant(0), new Variant(11), new Variant(0) } );
    //
    //Vector de 10 posiciones de medios de contacto
    invoke( "ParseVectorDatos", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_NODOMEDIOCONTACTO), new Variant(mcteParam_MEDCOSEC + ",1,3,0|" + mcteParam_MEDCOCOD + ",0,3,0|" + mcteParam_MEDCODAT + ",0,50,0|" + mcteParam_MEDCOSYS + ",0,3,0"), new Variant(10) } );
    //
    //Profesion Cliente
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_PROFESEC), new Variant(0), new Variant(3), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_PROFECOD), new Variant(0), new Variant(6), new Variant(0) } );

    //Datos del vehiculo
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_VEHICSEC), new Variant(1), new Variant(9), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUMARCOD), new Variant(1), new Variant(5), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUMODCOD), new Variant(1), new Variant(5), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUSUBCOD), new Variant(1), new Variant(5), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUADICOD), new Variant(1), new Variant(5), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUMODORI), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_SIFMVEHI_DES), new Variant(0), new Variant(35), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUTIPCOD), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FABRICAN), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FABRICMES), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_MOTORNUM), new Variant(0), new Variant(25), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CHASINUM), new Variant(0), new Variant(30), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_PATENNUM), new Variant(0), new Variant(10), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_VEHCLRCOD), new Variant(1), new Variant(3), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUCATCOD), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUUSOCOD), new Variant(1), new Variant(3), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUVTVCOD), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUVTVANN), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUVTVMES), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUVTVDIA), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUKLMNUM), new Variant(1), new Variant(7), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUPAISCOD), new Variant(0), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUPROVICOD), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMCPACODPO), new Variant(0), new Variant(8), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_GUGARAGE), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_GUDOMICI), new Variant(0), new Variant(1), new Variant(0) } );
    //
    //Asegurados Adicionales ...son 10 iteraciones 'ojo se estan poniendo los hijos
    invoke( "ParseVectorDatos", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CONDUCTORES), new Variant(mcteParam_CONDUSEC + ",1,2,0|" + mcteParam_CONDUTDO + ",1,2,0|" + mcteParam_DOCUMDAT + ",0,11,0|" + mcteParam_CONDUAPE + ",0,20,0|" + mcteParam_CONDUNOM + ",0,30,0|" + mcteParam_CONDUVIN + ",0,2,0|" + mcteParam_CONDUFEC + ",3,8,0|" + mcteParam_CONDUSEX + ",0,1,0|" + mcteParam_CONDUEST + ",0,1,0|" + mcteParam_CONDUEXC + ",0,1,0"), new Variant(10) } );
    //
    //Accesorios ...son 10 iteraciones
    invoke( "ParseVectorDatos", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_ACCESORIOS), new Variant(mcteParam_VEACCSEC + ",1,2,0|" + mcteParam_AUACCCOD + ",1,4,0|" + mcteParam_AUVEASUM + ",1,14,0|" + mcteParam_AUVEADES + ",0,30,0|" + mcteParam_AUVEADEP + ",0,1,0"), new Variant(10) } );
    //
    //Mas datos del cliente
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CLIENSEC), new Variant(1), new Variant(9), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CUENTSEC), new Variant(0), new Variant(3), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DOMICSEC), new Variant(0), new Variant(3), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_COBROFOR), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CLIENIVA), new Variant(0), new Variant(1), new Variant(0) } );
    //
    //Campa�as vector de 10
    invoke( "ParseVectorDatos", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CAMP_CODIGO + ",1,4,0"), new Variant("10"), new Variant(0) } );
    //
    //Coberturas, vector de 10
    invoke( "ParseVectorDatos", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_COBERTURAS), new Variant(mcteParam_COBERCOD + ",1,3,0|" + mcteParam_COBERORD + ",1,2,0|" + mcteParam_CAPITASG + ",2,13,2|" + mcteParam_CAPITIMP + ",2,13,2"), new Variant(10) } );
    //
    //Datos de la Poliza
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_EXPEDNUM), new Variant(0), new Variant(10), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CENTROCOD), new Variant(0), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_POLEFECTANN), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_POLEFECTMES), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_POLEFECTDIA), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_POLVENCIANN), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_POLVENCIMES), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_POLVENCIDIA), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_COBROCOD), new Variant(1), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_COASETIP), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_EXESEPOR), new Variant(2), new Variant(3), new Variant(2) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FESOLANN), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FESOLMES), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FESOLDIA), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FEINGANN), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FEINGMES), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FEINGDIA), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_RECARPOR), new Variant(2), new Variant(3), new Variant(2) } );
    //ESTE ES SI 998
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_PLANCOD), new Variant(1), new Variant(3), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_SWACREPR), new Variant(1), new Variant(1), new Variant(0) } );
    //
    // Acreedor Prendario
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_NUMEDOCU_ACRE), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_TIPODOCU_ACRE), new Variant(0), new Variant(11), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_APELLIDO_ACRE), new Variant(0), new Variant(20), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_APELLIDO_ACRE2), new Variant(0), new Variant(20), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_NOMBRES), new Variant(0), new Variant(30), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CLIENTIP_ACRE), new Variant(0), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DOMICCAL), new Variant(0), new Variant(5), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DOMICDOM), new Variant(0), new Variant(40), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DOMICDNU), new Variant(0), new Variant(5), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DOMICESC), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DOMICPIS), new Variant(0), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DOMICPTA), new Variant(0), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DOMICPOB), new Variant(0), new Variant(40), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_PROVICOD), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_PAISSCOD), new Variant(0), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DOMICPRE), new Variant(0), new Variant(5), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DOMICTLF), new Variant(0), new Variant(30), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CPACODPO), new Variant(0), new Variant(8), new Variant(0) } );
    //
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FRANQCOD), new Variant(0), new Variant(2), new Variant(0) } );
    //ESTE 5
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_PLANPILICOD), new Variant(1), new Variant(3), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_PRECIOFT), new Variant(2), new Variant(9), new Variant(3) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_ZONA), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMPROVICOD), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_SUMALBA), new Variant(2), new Variant(7), new Variant(4) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_SUMASEG), new Variant(2), new Variant(7), new Variant(4) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CLUB_LBA), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CTAKMS), new Variant(1), new Variant(6), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_ESCERO), new Variant(0), new Variant(1), new Variant(0) } );
    //
    // Inspeccion
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_EFECTANN), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_EFECTMES), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_EFECTDIA), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_ESTAINSP), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_INSPECOD), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_INSPEDES), new Variant(0), new Variant(30), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CENSICOD), new Variant(0), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CENSIDES), new Variant(0), new Variant(30), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_INSPEKMS), new Variant(1), new Variant(8), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_INSPEOBS), new Variant(0), new Variant(50), new Variant(0) } );
    //
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_INSPADOM), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_INS_DOMICDOM), new Variant(0), new Variant(40), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_INS_CPACODPO), new Variant(0), new Variant(8), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_INS_DOMICPOB), new Variant(0), new Variant(40), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_INS_PROVICOD), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_INS_DOMICTLF), new Variant(0), new Variant(30), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_INS_RANGOHOR), new Variant(1), new Variant(8), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DISPOCOD), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_PRESTCOD), new Variant(0), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_ESCERO), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_NROFACTU), new Variant(0), new Variant(20), new Variant(0) } );
    //
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DDJJ), new Variant(0), new Variant(130), new Variant(0) } );
    //
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUMARCOD), new Variant(1), new Variant(5), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUMODCOD), new Variant(1), new Variant(5), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUSUBCOD), new Variant(1), new Variant(5), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUADICOD), new Variant(1), new Variant(5), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUMODORI), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_SIFMVEHI_DES), new Variant(0), new Variant(35), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUTIPCOD), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FABRICAN), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FABRICMES), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_MOTORNUM), new Variant(0), new Variant(25), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CHASINUM), new Variant(0), new Variant(30), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_PATENNUM), new Variant(0), new Variant(10), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_VEHCLRCOD), new Variant(1), new Variant(3), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUCATCOD), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUUSOCOD), new Variant(1), new Variant(3), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUVTVDIA), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUVTVMES), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUVTVANN), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUKLMNUM), new Variant(1), new Variant(7), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUPAISCOD), new Variant(0), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUPROVICOD), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CliDOMCPACODPO), new Variant(0), new Variant(8), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_GUGARAGE), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_GUDOMICI), new Variant(0), new Variant(1), new Variant(0) } );
    //
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUCIASAN), new Variant(0), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUNUMSIN), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUNUMKMT), new Variant(1), new Variant(6), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUUSOGNC), new Variant(0), new Variant(1), new Variant(0) } );
    //ANTIGUEDAD DEL REGISTRO
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AUANTANN), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_ESCERO), new Variant(0), new Variant(1), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_NROFACTU), new Variant(0), new Variant(20), new Variant(0) } );
    //
    //MANDAR CERO
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_BANCOCOD), new Variant(1), new Variant(4), new Variant(0) } );
    //BLANCO
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_DOCUMTIP), new Variant(0), new Variant(2), new Variant(0) } );
    //BLACNO
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CLAIDENT), new Variant(0), new Variant(30), new Variant(0) } );
    //
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CONCECOD), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AGENTCOD), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_AGENTCLA), new Variant(0), new Variant(2), new Variant(0) } );
    //
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_NUMCREDI), new Variant(0), new Variant(16), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_FECCOPRE), new Variant(3), new Variant(8), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_PLAZOMES), new Variant(1), new Variant(3), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_PRECIOSO), new Variant(2), new Variant(9), new Variant(2) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_VIENEDMS), new Variant(0), new Variant(1), new Variant(0) } );
    //
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_BANCOCOD), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_SUCURCOD), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_LEGAJO_VEND), new Variant(1), new Variant(10), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_LEGAJO_GTE), new Variant(1), new Variant(10), new Variant(0) } );
    //
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_POLIZANNR), new Variant(1), new Variant(2), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_POLIZSECR), new Variant(1), new Variant(6), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CERTIPOLR), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CERTIANNR), new Variant(1), new Variant(4), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_CERTISECR), new Variant(1), new Variant(6), new Variant(0) } );
    invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarParseString), new Variant(pvarPos), new Variant(wobjXmlDOMResp), new Variant(wobjNodoRoot), new Variant(mcteParam_SUPLENUMR), new Variant(1), new Variant(4), new Variant(0) } );

    ParseoMensaje = XmlDomExtended.marshal(wobjXmlDOMResp.getDocument().getDocumentElement());
    wobjXmlDOMResp = null;
    wobjNodoRoot = (org.w3c.dom.Element) null;
    return ParseoMensaje;
  }

  private String ParseVectorDatos( String pvarAreaRecibida, Variant pvarPos, diamondedge.util.XmlDom pobjXMLDOMResp, org.w3c.dom.Node pVarNodos, String pVarPathVector, String pVarDefinicionSubNodos, int pVarCantElementos ) throws Exception
  {
    String ParseVectorDatos = "";
    int wvarActualCounter = 0;
    int wvarActualNodoSolicitado = 0;
    Variant wvarNodoNombre = new Variant();
    Variant wvarNodoTipoDato = new Variant();
    Variant wvarNodoLargoDato = new Variant();
    Variant wvarNodoCantDecimales = new Variant();
    VbCollection wvarColNodosSolicitados = null;
    org.w3c.dom.Element wvarNewElement = null;
    org.w3c.dom.Element wvarNewGrupo = null;
    String wvarNombreNodoGeneral = "";
    String wvarNombreNodoCiclico = "";
    String[] wvarArray = null;
    org.w3c.dom.NodeList wvarSelectedNodos = null;

    wvarArray = Strings.split( Strings.mid( pVarPathVector, 3 ), "/", -1 );
    wvarNombreNodoGeneral = wvarArray[0];
    wvarNombreNodoCiclico = wvarArray[1];

    wvarNewGrupo = pobjXMLDOMResp.getDocument().createElement( wvarNombreNodoGeneral );
    pVarNodos.appendChild( wvarNewGrupo );

    wvarColNodosSolicitados = GetColFromString(pVarDefinicionSubNodos);

    for( wvarActualCounter = 1; wvarActualCounter <= pVarCantElementos; wvarActualCounter++ )
    {

      wvarNewElement = pobjXMLDOMResp.getDocument().createElement( wvarNombreNodoCiclico );
      wvarNewGrupo.appendChild( wvarNewElement );

      //Recorro todos los nodos que formaran el vector
      for( wvarActualNodoSolicitado = 1; wvarActualNodoSolicitado <= wvarColNodosSolicitados.getCount(); wvarActualNodoSolicitado++ )
      {
        //Recorro todos los nodos que se solicitan para cada uno
       GetDefinicionElemento(wvarColNodosSolicitados.getItem(wvarActualNodoSolicitado).toString(), wvarNodoNombre, wvarNodoTipoDato, wvarNodoLargoDato, wvarNodoCantDecimales);
        invoke( "AppendDatoInXML", new Variant[] { new Variant(pvarAreaRecibida), new Variant(pvarPos), new Variant(pobjXMLDOMResp), new Variant(wvarNewElement), new Variant(wvarNodoNombre.toString()), new Variant(wvarNodoTipoDato.toInt()), new Variant(wvarNodoLargoDato.toInt()), new Variant(wvarNodoCantDecimales.toInt()) } );
      }
    }

    return ParseVectorDatos;
  }

  private void AppendDatoInXML( String pvarAreaRecibida, Variant pvarPos, diamondedge.util.XmlDom pobjXMLDOMResp, org.w3c.dom.Node pobjXMLContenedor, String pvarPathDato, int pvarTipoDato, int pvarLongitud, int pvarDecimales ) throws Exception
  {
    org.w3c.dom.Element wvarNewElement = null;
    org.w3c.dom.Node wvarNodoValor = null;
    String wvarDatoValue = "";
    int wvarCounter = 0;
    double wvarCampoNumerico = 0.0;

    wvarNewElement = pobjXMLDOMResp.getDocument().createElement( Strings.mid( pvarPathDato, 3 ) );
    pobjXMLContenedor.appendChild( wvarNewElement );

    
    if( pvarTipoDato == 0 )
    {
      //Dato del Tipo String
      XmlDomExtended.setText( wvarNewElement, Strings.trim( ModGeneral.MidAsString( pvarAreaRecibida, pvarPos, pvarLongitud ) ) );
    }
    else if( pvarTipoDato == 1 )
    {
      //Dato del Tipo Entero
      XmlDomExtended.setText( wvarNewElement, String.valueOf( VB.val( ModGeneral.MidAsString( pvarAreaRecibida, pvarPos, pvarLongitud ) ) ) );
    }
    else if( pvarTipoDato == 2 )
    {
      //Dato del Tipo "Con Decimales"
      wvarDatoValue = ModGeneral.MidAsString( pvarAreaRecibida, pvarPos, pvarLongitud );
      if( new Variant( wvarDatoValue ).isNumeric() )
      {
        wvarCampoNumerico = Obj.toDouble( wvarDatoValue );
      }
      for( wvarCounter = 1; wvarCounter <= pvarDecimales; wvarCounter++ )
      {
        wvarCampoNumerico = wvarCampoNumerico / 10;
      }
      XmlDomExtended.setText( wvarNewElement, String.valueOf( wvarCampoNumerico ) );
    }
    else if( pvarTipoDato == 3 )
    {
      //Dato del Tipo "Fecha" Debe venir del tipo YYYYMMDD devuelve dd/mm/yyyy
      wvarDatoValue = ModGeneral.MidAsString( pvarAreaRecibida, pvarPos, pvarLongitud );
      if( Strings.len( wvarDatoValue ) == 8 )
      {
        XmlDomExtended.setText( wvarNewElement, Strings.right( wvarDatoValue, 2 ) + "/" + Strings.mid( wvarDatoValue, 5, 2 ) + "/" + Strings.left( wvarDatoValue, 4 ) );
      }
    }

  }

  private VbCollection GetColFromString( String pvarString ) throws Exception
  {
    VbCollection GetColFromString = null;
    int wvarPos = 0;
    int wVarposAnt = 0;
    GetColFromString = new VbCollection();
    wvarPos = 1;
    wVarposAnt = 1;
    while( Strings.find( wvarPos, pvarString, "|" ) != 0 )
    {
      wvarPos = Strings.find( wvarPos, pvarString, "|" );
      GetColFromString.add( new Variant( Strings.mid( pvarString, wVarposAnt, wvarPos - wVarposAnt ) ) );
      wVarposAnt = wvarPos + 1;
      wvarPos = wvarPos + 1;
    }
    if( !Strings.mid( pvarString, wVarposAnt ).equals( "" ) )
    {
      GetColFromString.add( new Variant( Strings.mid( pvarString, wVarposAnt ) ) );
    }
    return GetColFromString;
  }

  private String CompleteZero( String pvarString, int pvarLongitud ) throws Exception
  {
    String CompleteZero = "";
    int wvarCounter = 0;
    String wvarstrTemp = "";
    for( wvarCounter = 1; wvarCounter <= pvarLongitud; wvarCounter++ )
    {
      wvarstrTemp = wvarstrTemp + "0";
    }
    CompleteZero = Strings.right( wvarstrTemp + pvarString, pvarLongitud );
    return CompleteZero;
  }

  private void GetDefinicionElemento( String pvarStringElemento, Variant pvarElemento, Variant pvarTipoDato, Variant pvarLongitud, Variant pvarDecimales ) throws Exception
  {
    String[] wvarArray = null;
    wvarArray = Strings.split( pvarStringElemento, ",", -1 );
    pvarElemento.set( wvarArray[0] );
    pvarTipoDato.set( wvarArray[1] );
    pvarLongitud.set( wvarArray[2] );
    pvarDecimales.set( wvarArray[3] );
  }

  private String GetVectorDatos( org.w3c.dom.Node pobjNodo, String pVarPathVector, String pVarDefinicionSubNodos, int pVarCantElementos ) throws Exception
  {
    String GetVectorDatos = "";
    int wvarCounterRelleno = 0;
    int wvarActualCounter = 0;
    int wvarActualNodoSolicitado = 0;
    Variant wvarNodoNombre = new Variant();
    Variant wvarNodoTipoDato = new Variant();
    Variant wvarNodoLargoDato = new Variant();
    Variant wvarNodoCantDecimales = new Variant();
    VbCollection wobjColNodosSolicitados = null;
    org.w3c.dom.NodeList wvarSelectedNodos = null;

    wobjColNodosSolicitados = GetColFromString(pVarDefinicionSubNodos);
    wvarActualCounter = 1;
    if( ! (pobjNodo == (org.w3c.dom.Node) null) )
    {
      wvarSelectedNodos = pobjNodo.selectNodes( pVarPathVector ) ;

      for( wvarActualCounter = 0; wvarActualCounter <= (wvarSelectedNodos.getLength() - 1); wvarActualCounter++ )
      {
        //Recorro todos los nodos que formaran el vector
        for( wvarActualNodoSolicitado = 1; wvarActualNodoSolicitado <= wobjColNodosSolicitados.getCount(); wvarActualNodoSolicitado++ )
        {
          //Recorro todos los nodos que se solicitan para cada uno
         GetDefinicionElemento(wobjColNodosSolicitados.getItem(wvarActualNodoSolicitado).toString(), wvarNodoNombre, wvarNodoTipoDato, wvarNodoLargoDato, wvarNodoCantDecimales);
          GetVectorDatos = GetVectorDatos + invoke( "GetDatoFormateado", new Variant[] { new Variant(wvarSelectedNodos.item( wvarActualCounter )), new Variant(wvarNodoNombre.toString()), new Variant(wvarNodoTipoDato.toInt()), new Variant(wvarNodoLargoDato.toInt()), new Variant(wvarNodoCantDecimales.toInt()) } );
        }
        if( wvarActualCounter == (pVarCantElementos - 1) )
        {
          return IAction_Execute;
        }
      }
    }
    for( wvarCounterRelleno = wvarActualCounter; wvarCounterRelleno <= (pVarCantElementos - 1); wvarCounterRelleno++ )
    {
      for( wvarActualNodoSolicitado = 1; wvarActualNodoSolicitado <= wobjColNodosSolicitados.getCount(); wvarActualNodoSolicitado++ )
      {
        //Recorro todos los nodos que se solicitan para cada uno
       GetDefinicionElemento(wobjColNodosSolicitados.getItem(wvarActualNodoSolicitado).toString(), wvarNodoNombre, wvarNodoTipoDato, wvarNodoLargoDato, wvarNodoCantDecimales);
        GetVectorDatos = GetVectorDatos + invoke( "GetDatoFormateado", new Variant[] { new Variant(null), new Variant(wvarNodoNombre.toString()), new Variant(wvarNodoTipoDato.toInt()), new Variant(wvarNodoLargoDato.toInt()), new Variant(wvarNodoCantDecimales.toInt()) } );
      }
    }
    ClearObjects: 
    wobjColNodosSolicitados = (VbCollection) null;
    return GetVectorDatos;
  }

  private String GetDatoFormateado( org.w3c.dom.Node pobjXMLContenedor, String pvarPathDato, int pvarTipoDato, int pvarLongitud, int pvarDecimales ) throws Exception
  {
    String GetDatoFormateado = "";
    org.w3c.dom.Node wobjNodoValor = null;
    String wvarDatoValue = "";
    int wvarCounter = 0;
    double wvarCampoNumerico = 0.0;

    if( ! (pobjXMLContenedor == (org.w3c.dom.Node) null) )
    {
      wobjNodoValor = pobjXMLContenedor.selectSingleNode( "./" + Strings.mid( pvarPathDato, 3 ) ) ;
      if( ! (wobjNodoValor == (org.w3c.dom.Node) null) )
      {
        wvarDatoValue = XmlDomExtended.getText( wobjNodoValor );
      }
    }

    
    if( pvarTipoDato == 0 )
    {
      //Dato del Tipo String
      GetDatoFormateado = Strings.left( wvarDatoValue + Strings.space( pvarLongitud ), pvarLongitud );
    }
    else if( pvarTipoDato == 1 )
    {
      //Dato del Tipo Entero
      GetDatoFormateado = CompleteZero(wvarDatoValue, pvarLongitud);
    }
    else if( pvarTipoDato == 2 )
    {
      //Dato del Tipo "Con Decimales"
      if( new Variant( wvarDatoValue ).isNumeric() )
      {
        wvarCampoNumerico = Obj.toDouble( wvarDatoValue );
      }
      for( wvarCounter = 1; wvarCounter <= pvarDecimales; wvarCounter++ )
      {
        wvarCampoNumerico = wvarCampoNumerico * 10;
      }
      GetDatoFormateado = invoke( "CompleteZero", new Variant[] { new Variant(String.valueOf( (int)Math.rint( new Variant(wvarCampoNumerico) ) )), new Variant(pvarLongitud) } );
    }
    else if( pvarTipoDato == 3 )
    {
      //Dato del Tipo "Fecha" Debe venir del tipo dd/mm/yyyy devuelve YYYYMMDD
      if( Strings.len( wvarDatoValue ) == 10 )
      {
        GetDatoFormateado = Strings.right( wvarDatoValue, 4 ) + Strings.mid( wvarDatoValue, 4, 2 ) + Strings.left( wvarDatoValue, 2 );
      }
      else
      {
        GetDatoFormateado = Strings.space( 8 );
      }
    }

    return GetDatoFormateado;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
