package com.qbe.services.ovmqemision.impl;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ovmqemision.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetProdUsuario implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQEmision.lbaw_GetProdUsuario";
  static final String mcteOpID = "1553";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_ClienSec_AS = "//CLIENSEC_AS";
  static final String mcteParam_Verif_Convenio = "//VERIFICARCONVENIO";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXMLRespConvenio = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    org.w3c.dom.NodeList wobjNodosProductor = null;
    org.w3c.dom.Node wobjNodoProductor = null;
    lbawA_OVMQIII.lbaw_OVIIIConvProdOrg wobjClass = new lbawA_OVMQIII.lbaw_OVIIIConvProdOrg();
    int wvarMQError = 0;
    String wvarArea = "";
    MQConnectionConnector wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarClienSec = "";
    String wvarClienSecAS = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    boolean wvarVerifConvenio = false;
    String wvarRamoPCod = "";
    String wvarResponse = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //Deber� venir desde la p�gina
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      if( wobjXMLRequest.selectNodes( mcteParam_CLIENSEC ) .getLength() != 0 )
      {
        wvarClienSec = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC )  );
      }
      if( wobjXMLRequest.selectNodes( mcteParam_ClienSec_AS ) .getLength() != 0 )
      {
        wvarClienSecAS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec_AS )  );
      }
      wvarClienSec = Strings.right( "000000000" + wvarClienSec, 9 );
      wvarClienSecAS = Strings.right( "000000000" + wvarClienSecAS, 9 );
      //Esta consulta filtra por convenio
      wvarVerifConvenio = wobjXMLRequest.selectNodes( (mcteParam_Verif_Convenio + "[.='SI']") ) .getLength() != 0;
      if( wobjXMLRequest.selectNodes( mcteParam_RAMOPCOD ) .getLength() != 0 )
      {
        wvarRamoPCod = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  );
      }
      //
      //
      wvarStep = 60;
      wobjXMLRequest = null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      //
      wvarStep = 110;
      wobjXMLParametros = null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    " + wvarClienSecAS + "PR  " + wvarClienSec + "  000000000  000000000";
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQConnectionConnector.newInstance();
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder, wobjXMLConfig.selectSingleNode("//MQCONFIG"));
	strParseString = strParseStringHolder.getValue();

      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 240;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = "";
        //cantidad de caracteres ocupados por par�metros de entrada
        wvarPos = 67;
        wvarResult = ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        //
        //Verifico por convenio
        if( wvarVerifConvenio && (!wvarRamoPCod.equals( "" )) )
        {
          //
          wobjXMLResponse = new XmlDomExtended();
          wobjXMLRequest = new XmlDomExtended();
          wobjXMLRespConvenio = new XmlDomExtended();
          //
          wvarStep = 290;
          wobjXMLResponse.loadXML( wvarResult );
          wobjNodosProductor = wobjXMLResponse.selectNodes( "//PRODUCTOR" ) ;
          //
          for( int nwobjNodoProductor = 0; nwobjNodoProductor < wobjNodosProductor.getLength(); nwobjNodoProductor++ )
          {
            wobjNodoProductor = wobjNodosProductor.item( nwobjNodoProductor );
            //
            wvarStep = 300;
            wobjXMLRequest.loadXML( "<Request>" + "<USUARIO>" + wvarUsuario + "</USUARIO>" + "<RAMOPCOD>" + wvarRamoPCod + "</RAMOPCOD>" + "<AGENTECODIGO>" + XmlDomExtended.getText( /*unsup wobjNodoProductor.selectSingleNode( "./CODIGO" ) */ ) + "</AGENTECODIGO>" + "<AGENTECLASE>" + XmlDomExtended.getText( wobjNodoProductor.selectSingleNode( "./CLASE" )  ) + "</AGENTECLASE>" + "</Request>" );
            //
            wobjClass = new lbawA_OVMQIII.lbaw_OVIIIConvProdOrg();
            wobjClass = new lbawA_OVMQIII.lbaw_OVIIIConvProdOrg();
            wobjClass.Execute( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarResponse, "" );
            wobjClass = (lbawA_OVMQIII.lbaw_OVIIIConvProdOrg) null;
            //
            wvarStep = 310;
            wobjXMLRespConvenio.loadXML( wvarResponse );
            if( wobjXMLRespConvenio.selectNodes( "//OPTION" ) .getLength() == 0 )
            {
              //Si no trajo opciones de convenio, elimino el nodo del productor
              wobjNodoProductor.getParentNode().removeChild( wobjNodoProductor );
            }
          }
          //
          wvarResult = XmlDomExtended.marshal(wobjXMLResponse.getDocument().getDocumentElement());
        }
        //
        wvarStep = 340;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
        //
      }
      //
      wvarStep = 360;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int pvarPos, String pstrParseString, int pvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    VbScript_RegExp wobjRegExp = new VbScript_RegExp();
    MatchCollection wobjColMatch;
    Match wobjMatch = null;
    String wvarParseEvalString = "";
    XmlDomExtended wobjXmlDOMResp = null;
    org.w3c.dom.Element wobjXmlElemenResp = null;
    org.w3c.dom.Element wobjXmlElemenRespD = null;
    int wvarCantRegistros = 0;
    int wvarCcurrRegistro = 0;
    //
    //RegExp
    //MatchCollection
    //Match
    //
    //
    //
    wobjRegExp = new VbScript_RegExp();
    wobjRegExp.setGlobal( true ); 
    //
    wvarParseEvalString = Strings.mid( pstrParseString, pvarPos + 3 );
    wvarParseEvalString = Strings.replace( wvarParseEvalString, " ", "_" );
    wvarCantRegistros = (int)Math.rint( VB.val( Strings.mid( pstrParseString, pvarPos, 3 ) ) );
    wvarCcurrRegistro = 0;

    wobjRegExp.setPattern( "(\\S{4})(\\S{2})(\\S{40})" );
    //
    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    wobjXmlDOMResp = new XmlDomExtended();
    wobjXmlDOMResp.loadXML( "<RS></RS>" );
    //
    //
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //
      if( ! (wvarCcurrRegistro < wvarCantRegistros) )
      {
        break;
      }
      wvarCcurrRegistro = wvarCcurrRegistro + 1;
      //
      wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "PRODUCTOR" );
      /*unsup wobjXmlDOMResp.selectSingleNode( "//RS" ) */.appendChild( wobjXmlElemenResp );
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "CODIGO" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(0), "_", " ").trim());
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "CLASE" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(1), "_", " ").trim());
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "NOMBRE" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(2), "_", " ").trim());
      //
    }
    //
    ParseoMensaje = XmlDomExtended.marshal(wobjXmlDOMResp.getDocument().getDocumentElement());
    //
    wobjXmlDOMResp = null;
    wobjXmlElemenResp = (org.w3c.dom.Element) null;
    wobjXmlElemenRespD = (org.w3c.dom.Element) null;
    //
    wobjRegExp = (VbScript_RegExp) null;

    return ParseoMensaje;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
