package com.qbe.services.ovmqemision.impl;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ovmqemision.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVRiesgosImpres implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQEmision.lbaw_OVRiesgosImpres";
  static final String mcteOpID = "1582";
  /**
   * LINKAGE MQ MSG 1582 -- 29/04/2009
   * 04 LNK-PARM-ENTRADA.
   *     06 LNK-MENSAJE               PIC 9(4).
   *     06 LNK-PARM-GRAL-SELECCION.
   *        08 LNK-CIAASCOD           PIC X(4).
   *        08 LNK-USUARCOD           PIC X(10).
   *        08 LNK-ESTADO             PIC X(2).
   *        08 LNK-ERROR              PIC X(2).
   *        08 LNK-CLIENSECAS         PIC 9(9).
   *        08 LNK-NIVELCLAS          PIC X(2).
   *        08 LNK-NIVELCLA1          PIC X(2).
   *        08 LNK-CLIENSEC1          PIC 9(9).
   *        08 LNK-NIVELCLA2          PIC X(2).
   *        08 LNK-CLIENSEC2          PIC 9(9).
   *        08 LNK-NIVELCLA3          PIC X(2).
   *        08 LNK-CLIENSEC3          PIC 9(9).
   *     06 LNK-PARM-ESPE-SELECCION.
   *        08 LNK-RAMOPCOD           PIC X(4).
   *        08 LNK-POLIZANN           PIC 9(2).
   *        08 LNK-POLIZSEC           PIC 9(6).
   *        08 LNK-CERTIPOL           PIC 9(4).
   *        08 LNK-CERTIANN           PIC 9(4).
   *        08 LNK-CERTISEC           PIC 9(6).
   *     06 LNK-PARM-START.
   *        08 LNK-NROQRY-S           PIC 9(8).
   *        08 LNK-RAMOPCOD-S         PIC X(4).
   *        08 LNK-POLIZANN-S         PIC 9(2).
   *        08 LNK-POLIZSEC-S         PIC 9(6).
   *        08 LNK-CERTIPOL-S         PIC 9(4).
   *        08 LNK-CERTIANN-S         PIC 9(4).
   *        08 LNK-CERTISEC-S         PIC 9(6).
   * 
   * 04 LNK-PARM-SALIDA.
   *     06 LNK-RESULTADOS            OCCURS 150. LR 29/04/2009 Se cambia a 150, antes 200
   *        08 LNK-CLAVE-OPERACION.
   *           10 LNK-RAMOPCOD        PIC X(4).
   *           10 LNK-POLIZANN        PIC 9(2).
   *           10 LNK-POLIZSEC        PIC 9(6).
   *           10 LNK-CERTIPOL        PIC 9(4).
   *           10 LNK-CERTIANN        PIC 9(4).
   *           10 LNK-CERTISEC        PIC 9(6).
   *        08 LNK-TOMARIES           PIC X(70). LR 29/04/2009 Se cambia a 70, antes 30
   *        08 LNK-SITUCPOL           PIC X(30).
   *        08 LNK-SINIESTRO          PIC X.
   *        08 LNK-SWCOBER            PIC X.
   *        08 LNK-SWIMPCOB           PIC X.
   *        08 LNK-SWIMPTAR           PIC X.
   *        08 LNK-SWIMPMER           PIC X.
   *        08 LNK-PATENNUM           PIC X(10).
   *        08 LNK-SWTIPSEG           PIC X.
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARCOD = "//USUARIO";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_Suplenum = "//SUPLENUM";
  /**
   *  ################################################
   *  Agregado 08-08-2006. FJO. Paginaci�n
   * 06  LNK-PARM-START. (Longitud: 34)
   * 08 LNK-NROQRY-S PIC 9(8).
   */
  static final String mcteParam_NROQRY_S = "//NROQRY_S";
  /**
   * 08 LNK-RAMOPCOD PIC X(4).
   */
  static final String mcteParam_RAMOPCOD_S = "//RAMOPCOD_S";
  /**
   * 08 LNK-POLIZANN PIC 9(2).
   */
  static final String mcteParam_POLIZANN_S = "//POLIZANN_S";
  /**
   * 08 LNK-POLIZSEC PIC 9(6).
   */
  static final String mcteParam_POLIZSEC_S = "//POLIZSEC_S";
  /**
   * 08 LNK-CERTIPOL PIC 9(4).
   */
  static final String mcteParam_CERTIPOL_S = "//CERTIPOL_S";
  /**
   * 08 LNK-CERTIANN PIC 9(4).
   */
  static final String mcteParam_CERTIANN_S = "//CERTIANN_S";
  /**
   * 08 LNK-CERTISEC PIC 9(6).
   */
  static final String mcteParam_CERTISEC_S = "//CERTISEC_S";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";
  /**
   * static variable for method: ParseoMensaje
   */
  private final int wvarCantidadRegistrosAProcesar = 150;

  /**
   *  ################################################
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQConnectionConnector wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarRamo = "";
    String wvarPolizaAnn = "";
    String wvarPolizaSec = "";
    String wvarCertiPol = "";
    String wvarCertiAnn = "";
    String wvarCertiSec = "";
    String wvarSuplenum = "";
    String wvarNROQRY_S = "";
    String wvarRAMOPCOD_S = "";
    String wvarPOLIZANN_S = "";
    String wvarPOLIZSEC_S = "";
    String wvarCERTIPOL_S = "";
    String wvarCERTIANN_S = "";
    String wvarCERTISEC_S = "";
    Variant wvarPos = new Variant();
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    // ################################################
    // Agregado 08-08-2006. FJO. Paginaci�n
    // ################################################
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD )  ) + Strings.space( 10 ), 10 );
      wvarRamo = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  ) + Strings.space( 4 ), 4 );
      wvarPolizaAnn = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN )  ), 2 );
      wvarPolizaSec = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC )  ), 6 );
      wvarCertiPol = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL )  ), 4 );
      wvarCertiAnn = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN )  ), 4 );
      wvarCertiSec = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC )  ), 6 );
      //wvarSuplenum = Right("0000" & .selectSingleNode(mcteParam_Suplenum).Text, 4)
      // ################################################
      // Agregado 08-08-2006. FJO. Paginaci�n
      if( wobjXMLRequest.selectSingleNode( mcteParam_NROQRY_S )  == (org.w3c.dom.Node) null )
      {
        wvarNROQRY_S = "00000000";
      }
      else
      {
        wvarNROQRY_S = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NROQRY_S )  );
      }

      if( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD_S )  == (org.w3c.dom.Node) null )
      {
        wvarRAMOPCOD_S = "    ";
      }
      else
      {
        wvarRAMOPCOD_S = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD_S )  );
      }

      if( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN_S )  == (org.w3c.dom.Node) null )
      {
        wvarPOLIZANN_S = "00";
      }
      else
      {
        wvarPOLIZANN_S = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN_S )  );
      }

      if( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC_S )  == (org.w3c.dom.Node) null )
      {
        wvarPOLIZSEC_S = "000000";
      }
      else
      {
        wvarPOLIZSEC_S = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC_S )  );
      }

      if( wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL_S )  == (org.w3c.dom.Node) null )
      {
        wvarCERTIPOL_S = "0000";
      }
      else
      {
        wvarCERTIPOL_S = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL_S )  );
      }

      if( wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN_S )  == (org.w3c.dom.Node) null )
      {
        wvarCERTIANN_S = "0000";
      }
      else
      {
        wvarCERTIANN_S = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN_S )  );
      }

      if( wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC_S )  == (org.w3c.dom.Node) null )
      {
        wvarCERTISEC_S = "000000";
      }
      else
      {
        wvarCERTISEC_S = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC_S )  );
      }
      // ################################################
      //
      wvarStep = 20;
      wobjXMLRequest = null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 60;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      //
      wvarStep = 70;
      wobjXMLParametros = null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarRamo + wvarPolizaAnn + wvarPolizaSec + wvarCertiPol + wvarCertiAnn + wvarCertiSec + wvarSuplenum + wvarNROQRY_S + wvarRAMOPCOD_S + wvarPOLIZANN_S + wvarPOLIZSEC_S + wvarCERTIPOL_S + wvarCERTIANN_S + wvarCERTISEC_S;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQConnectionConnector.newInstance();
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder, wobjXMLConfig.selectSingleNode("//MQCONFIG"));
	strParseString = strParseStringHolder.getValue();

      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 190;
      wvarResult = "";
      //wvarPos = 93 'cantidad de caracteres ocupados por par�metros de entrada
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos.set( 127 );
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 200;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 210;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 240;
        wvarResult = "";
        wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        //
        wvarStep = 250;
        wobjXMLResponse = new XmlDomExtended();
        wobjXSLResponse = new XmlDomExtended();
        //
        wvarStep = 260;
        wobjXMLResponse.loadXML( wvarResult );
        //
        if( wobjXMLResponse.selectNodes( "//R" ) .getLength() != 0 )
        {
          wvarStep = 270;
          wobjXSLResponse.loadXML( p_GetXSL());
          //
          wvarStep = 280;
          wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
          //
          wvarStep = 290;
          wobjXMLResponse = null;
          wobjXSLResponse = null;
          //
          wvarStep = 300;
          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
          //
        }
        else
        {
          //
          wvarStep = 270;
          wobjXMLResponse = null;
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          //
        }
      }
      //
      wvarStep = 310;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarRamo + wvarPolizaAnn + wvarPolizaSec + wvarCertiPol + wvarCertiAnn + wvarCertiSec + wvarSuplenum + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCantidadRegistrosProcesados = 0;
    //
    //Const wvarCantidadRegistrosAProcesar As Long = 200 LR 29/04/2009 Pasa a 150
    //
    wvarCantidadRegistrosProcesados = 1;
    //
    wvarResult = wvarResult + "<RS>";
    //
    while( (wvarPos.toInt() < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ).equals( "" )) && (wvarCantidadRegistrosProcesados <= wvarCantidadRegistrosAProcesar) )
    {
      //wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 101) & "]]></R>"
      //wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 102) & "]]></R>"
      // LR 29/04/2009 Se suma 40 de TOMARIES
      wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 142 ) + "]]></R>";

      //wvarPos = wvarPos + 88
      //wvarPos = wvarPos + 101
      //wvarPos = wvarPos + 102
      // LR 29/04/2009 Se suma 40 de TOMARIES
      wvarPos.set( wvarPos.add( new Variant( 142 ) ) );

      wvarCantidadRegistrosProcesados = wvarCantidadRegistrosProcesados + 1;
    }
    //
    wvarResult = wvarResult + "</RS>";
    //
    wvarResult = wvarResult + "<AreaINRespuesta>" + Strings.left( strParseString, 126 ) + "</AreaINRespuesta>";
    //
    ParseoMensaje = "<RespuestaMQ>" + wvarResult + "</RespuestaMQ>";
    //
    return ParseoMensaje;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REGS'>";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='//RS/R'/>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='Request'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:apply-templates select='//AreaINRespuesta'/>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='//RS/R'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REG'>";
    //wvarStrXSL = wvarStrXSL & "             <xsl:element name='RAMO'>"
    //wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,1,1)' />"
    //wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='PROD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,1,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,5,8)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERPOL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,13,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERANN'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,17,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERSEC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,21,6)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='TOMA'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,27,70))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='EST'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,97,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SINI'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,127,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='LUPA'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,128,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMPRCERTIFICADOCOBERTURA'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,129,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMPRTARJETACIRCULACION'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,130,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMPRCERTIFICADOMERCOSUR'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,131,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='PATENTENUM'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,132,10)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RAMO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,142,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "         <xsl:template match='//AreaINRespuesta'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='USUARIO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,9,10)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='STATUS'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,19,2)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='ERR_DESC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,21,2))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RAMOPCOD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,67,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POLIZANN'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,71,2)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POLIZSEC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,73,6)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERTIPOL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,79,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERTIANN'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,83,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERTISEC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,87,6)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='NROQRY_S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,93,8)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RAMOPCOD_S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,101,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POLIZANN_S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,105,2)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POLIZSEC_S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,107,6)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERTIPOL_S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,113,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERTIANN_S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,117,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERTISEC_S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,121,6)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "         </xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='TOMA'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='EST'/>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    //
    return p_GetXSL;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
