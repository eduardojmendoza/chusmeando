package com.qbe.services.ovmqemision.impl;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ovmqemision.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVGetBinaryFile implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQEmision.lbaw_OVGetBinaryFile";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_FileName = "//RUTA";
  static final String mcteParam_MantenerArchivo = "//MANTENERARCHIVO";
  static final String mcteParam_SoloBuscarArchivo = "//SOLOBUSCARARCHIVO";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarResult = "";
    boolean wvarMarcaMantenerArch = false;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    org.w3c.dom.Element wobjEle = null;
    XmlDomExtended wobjXMLServer = null;
    int wvarFileGet = 0;
    String wvarFileName = "";
    byte[] wvarArrBytes = null;
    int wvarCantidadIntentos = 0;
    int wvarCurrIntento = 0;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLResponse = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarFileName = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FileName )  );
      wvarMarcaMantenerArch = ! (wobjXMLRequest.selectSingleNode( mcteParam_MantenerArchivo )  == (org.w3c.dom.Node) null);
      //
      wvarStep = 20;
      wobjXMLServer = new XmlDomExtended();
      wobjXMLServer.load( System.getProperty("user.dir") + "\\LBAVirtualServerImpresion.xml" );
      wvarCantidadIntentos = (int)Math.rint( (VB.val( XmlDomExtended.getText( wobjXMLServer.selectSingleNode( "//TIMEOUT" )  ) ) * 1000) / 250 );
      wobjXMLServer = null;
      //
      wvarStep = 21;
      //
      //Verifico que exista el archivo que se solicita por par�metro
      for( wvarCurrIntento = 1; wvarCurrIntento <= wvarCantidadIntentos; wvarCurrIntento++ )
      {
        //
        if( !"" /*unsup this.Dir( wvarFileName, 0 ) */.equals( "" ) )
        {
          break;
        }
        //Espero de a 250ms para ver si lo termina de generar
        Thread.sleep( 250 );
        wvarCurrIntento = wvarCurrIntento + 1;
        //
      }
      //
      wvarStep = 22;
      //-------------------------------------------------------------------------------------------------------------------
      //// Metodos del Framework
      //-------------------------------------------------------------------------------------------------------------------
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, "", wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
