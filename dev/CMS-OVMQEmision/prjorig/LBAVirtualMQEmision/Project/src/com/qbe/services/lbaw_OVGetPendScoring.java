package com.qbe.services.ovmqemision.impl;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ovmqemision.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVGetPendScoring implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQEmision.lbaw_OVGetPendScoring";
  static final String mcteOpID = "1520";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_FechaDesde = "//FECHADESDE";
  static final String mcteParam_FechaHasta = "//FECHAHASTA";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQConnectionConnector wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarFechaDesde = "";
    String wvarFechaHasta = "";
    String wvarFDesdeVal = "";
    String wvarFHastaVal = "";
    Variant wvarPos = new Variant();
    String wvarParseString = "";
    int wvarStringLen = 0;
    Variant wvarCantLin = new Variant();
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //Par�metros:
      //- Usuario      PIC X(10)
      //- Fecha Desde  PIC 9(8)
      //- Fecha Hasta  PIC 9(8)
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarFechaDesde = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FechaDesde )  );
      wvarFechaHasta = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FechaHasta )  );
      //
      //Valido las fechas recibidas, en formato mm/dd/aaaa
      wvarFDesdeVal = Strings.mid( wvarFechaDesde, 5, 2 ) + "/" + Strings.mid( wvarFechaDesde, 7, 2 ) + "/" + Strings.mid( wvarFechaDesde, 1, 4 );
      if( ! (new Variant( wvarFDesdeVal ).isDate()) )
      {
        Err.raise( -1, mcteClassName + "." + wcteFnName, "Par�metro 'Fecha Desde' No V�lido." );
      }
      wvarFHastaVal = Strings.mid( wvarFechaHasta, 5, 2 ) + "/" + Strings.mid( wvarFechaHasta, 7, 2 ) + "/" + Strings.mid( wvarFechaHasta, 1, 4 );
      if( ! (new Variant( wvarFHastaVal ).isDate()) )
      {
        Err.raise( -1, mcteClassName + "." + wcteFnName, "Par�metro 'Fecha Hasta' No v�lido." );
      }
      //
      wvarStep = 60;
      wobjXMLRequest = null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      if( wobjXMLParametros.getDocument().getChildNodes().getLength() > 0 )
      {
        wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      }
      //
      wvarStep = 110;
      wobjXMLParametros = null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarFechaDesde + wvarFechaHasta;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQConnectionConnector.newInstance();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, wvarParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG"))
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + wvarParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 240;
      wvarResult = "";
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos.set( 102 );
      //
      wvarStringLen = Strings.len( wvarParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( wvarParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = ParseoMensaje(wvarPos, wvarParseString, wvarStringLen, wvarCantLin);
        //
        wvarStep = 310;
        if( wvarCantLin.toInt() > 0 )
        {
          wvarStep = 340;
          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
          //
        }
        else
        {
          //
          wvarStep = 350;
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          //
        }
        //
      }
      //
      wvarStep = 360;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarFechaDesde + wvarFechaHasta + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant pvarPos, String pvarParseString, int pvarStringLen, Variant pvarCantLin ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCantLin = 0;
    int wvarCounter = 0;
    //
    pvarPos.set( pvarPos.add( new Variant( 1 ) ) );
    wvarResult = wvarResult + "<RS>";
    //
    //NOTA:
    //En el doc del mensaje se indica que la salida incluye los campos NIVELCLA y CLIENSEC
    //que son parte de la entrada.
    //Como no es as� en el resto de los documentos, entiendo que es un error del doc y
    //solo parseo los 3 ultimos campos particulares de la salida de este mensaje.
    //Tampoco se indica el campo CANTLIN, que supongo tambien vendr� en la salidad
    //ya que se recibe un VECTOR.
    //
    if( Strings.trim( Strings.mid( pvarParseString, pvarPos.toInt(), 3 ) ).equals( "" ) )
    {
      pvarCantLin.set( 0 );
    }
    else
    {
      wvarCantLin = Obj.toInt( Strings.mid( pvarParseString, pvarPos.toInt(), 3 ) );
      pvarCantLin.set( wvarCantLin );
      pvarPos.set( pvarPos.add( new Variant( 3 ) ) );
      //
      for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )
      {
        wvarResult = wvarResult + "<R>";
        wvarResult = wvarResult + "<NOMBRE>" + Strings.mid( pvarParseString, pvarPos.toInt(), 30 ) + "</NOMBRE>";
        wvarResult = wvarResult + "<CANTIDAD>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 30 ) ).toInt(), 6 ) + "</CANTIDAD>";
        wvarResult = wvarResult + "<VIGENTE>" + Strings.mid( pvarParseString, pvarPos.add( new Variant( 36 ) ).toInt(), 1 ) + "</VIGENTE>";
        wvarResult = wvarResult + "</R>";
        pvarPos.set( pvarPos.add( new Variant( 37 ) ) );
      }
      //
    }
    wvarResult = wvarResult + "</RS>";
    //
    ParseoMensaje = wvarResult;
    return ParseoMensaje;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
