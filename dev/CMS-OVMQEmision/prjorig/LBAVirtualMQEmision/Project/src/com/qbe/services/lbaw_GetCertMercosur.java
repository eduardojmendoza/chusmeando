package com.qbe.services.ovmqemision.impl;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ovmqemision.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetCertMercosur implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQEmision.lbaw_GetCertMercosur";
  static final String mcteOpID = "1584";
  /**
   *             04 LNK-PARM-ENTRADA.
   *                06 LNK-MENSAJE               PIC 9(4).
   *                06 LNK-PARM-GRAL-SELECCION.
   *                   08 LNK-CIAASCOD           PIC X(4).
   *                   08 LNK-USUARCOD           PIC X(10).
   *                   08 LNK-ESTADO             PIC X(2).
   *                   08 LNK-ERROR              PIC X(2).
   *                   08 LNK-CLIENSECAS         PIC 9(9).
   *                   08 LNK-NIVELCLAS          PIC X(2).
   *                   08 LNK-NIVELCLA1          PIC X(2).
   *                   08 LNK-CLIENSEC1          PIC 9(9).
   *                   08 LNK-NIVELCLA2          PIC X(2).
   *                   08 LNK-CLIENSEC2          PIC 9(9).
   *                   08 LNK-NIVELCLA3          PIC X(2).
   *                   08 LNK-CLIENSEC3          PIC 9(9).
   *                06 LNK-PARM-ESPE-SELECCION.
   *                   08 LNK-POLIZA.
   *                      10 LNK-RAMOPCOD        PIC X(04).
   *                      10 LNK-POLIZANN        PIC 9(02).
   *                      10 LNK-POLIZSEC        PIC 9(06).
   *                      10 LNK-CERTIPOL        PIC 9(04).
   *                       10 LNK-CERTIANN        PIC 9(04).
   *                       10 LNK-CERTISEC        PIC 9(06).
   *                    08 LNK-PATENTE            PIC X(10).
   *                    08 LNK-MOTOR              PIC X(25). 29/04/5009 25, antes 20
   *                 06 LNK-PARM-START.
   *                    08 LNK-NROQRY-S           PIC 9(08).
   *                    08 LNK-RAMOPCOD-S         PIC X(04).
   *                    08 LNK-POLIZANN-S         PIC 9(02).
   *                    08 LNK-POLIZSEC-S         PIC 9(06).
   *                    08 LNK-CERTIPOL-S         PIC 9(04).
   *                    08 LNK-CERTIANN-S         PIC 9(04).
   *                    08 LNK-CERTISEC-S         PIC 9(06).
   *              04 LNK-PARM-SALIDA.
   *                 06 LNK-RESULTADOS            OCCURS 150. 29/04/5009 150, antes 200
   *                    08 LNK-CLAVE-OPERACION.
   *                       10 LNK-RAMOPCOD        PIC X(4).
   *                       10 LNK-POLIZANN        PIC 9(2).
   *                       10 LNK-POLIZSEC        PIC 9(6).
   *                       10 LNK-CERTIPOL        PIC 9(4).
   *                       10 LNK-CERTIANN        PIC 9(4).
   *                       10 LNK-CERTISEC        PIC 9(6).
   *                    08 LNK-CLIENTE            PIC X(35).
   *                    08 LNK-TOMARIES           PIC X(70). 29/04/5009 70, antes 30
   *                    08 LNK-PATENNUM           PIC X(10).
   *                    08 LNK-SITUCPOL           PIC X(30).
   *                    08 LNK-SWIMPRIME          PIC X(01).
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARCOD = "//USUARIO";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_PATENTE = "//PATENTE";
  static final String mcteParam_MOTOR = "//MOTOR";
  /**
   *  ################################################
   *  Agregado 09-08-2006. FJO. Paginaci�n
   * 06  LNK-PARM-START. (Longitud: 34)
   * 08 LNK-NROQRY-S PIC 9(8).
   */
  static final String mcteParam_NROQRY_S = "//NROQRY_S";
  /**
   * 08 LNK-RAMOPCOD PIC X(4).
   */
  static final String mcteParam_RAMOPCOD_S = "//RAMOPCOD_S";
  /**
   * 08 LNK-POLIZANN PIC 9(2).
   */
  static final String mcteParam_POLIZANN_S = "//POLIZANN_S";
  /**
   * 08 LNK-POLIZSEC PIC 9(6).
   */
  static final String mcteParam_POLIZSEC_S = "//POLIZSEC_S";
  /**
   * 08 LNK-CERTIPOL PIC 9(4).
   */
  static final String mcteParam_CERTIPOL_S = "//CERTIPOL_S";
  /**
   * 08 LNK-CERTIANN PIC 9(4).
   */
  static final String mcteParam_CERTIANN_S = "//CERTIANN_S";
  /**
   * 08 LNK-CERTISEC PIC 9(6).
   */
  static final String mcteParam_CERTISEC_S = "//CERTISEC_S";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  ################################################
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQConnectionConnector wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarRamo = "";
    String wvarPolizaAnn = "";
    String wvarPolizaSec = "";
    String wvarCertiPol = "";
    String wvarCertiAnn = "";
    String wvarCertiSec = "";
    String wvarPatente = "";
    String wvarMotor = "";
    String wvarNROQRY_S = "";
    String wvarRAMOPCOD_S = "";
    String wvarPOLIZANN_S = "";
    String wvarPOLIZSEC_S = "";
    String wvarCERTIPOL_S = "";
    String wvarCERTIANN_S = "";
    String wvarCERTISEC_S = "";
    Variant wvarPos = new Variant();
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    // ################################################
    // Agregado 09-08-2006. FJO. Paginaci�n
    // ################################################
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      if( wobjXMLRequest.selectNodes( mcteParam_USUARCOD ) .getLength() == 0 )
      {
        wvarUsuario = Strings.fill( 10, " " );
      }
      else
      {
        wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD )  ) + Strings.space( 10 ), 10 );
      }
      //
      if( wobjXMLRequest.selectNodes( mcteParam_RAMOPCOD ) .getLength() == 0 )
      {
        wvarRamo = Strings.fill( 4, " " );
      }
      else
      {
        wvarRamo = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  ) + Strings.space( 4 ), 4 );
      }
      //
      if( wobjXMLRequest.selectNodes( mcteParam_POLIZANN ) .getLength() == 0 )
      {
        wvarPolizaAnn = Strings.fill( 2, "0" );
      }
      else
      {
        wvarPolizaAnn = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN )  ), 2 );
      }
      //
      if( wobjXMLRequest.selectNodes( mcteParam_POLIZSEC ) .getLength() == 0 )
      {
        wvarPolizaSec = Strings.fill( 6, "0" );
      }
      else
      {
        wvarPolizaSec = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC )  ), 6 );
      }
      //
      if( wobjXMLRequest.selectNodes( mcteParam_CERTIPOL ) .getLength() == 0 )
      {
        wvarCertiPol = Strings.fill( 4, "0" );
      }
      else
      {
        wvarCertiPol = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL )  ), 4 );
      }
      //
      if( wobjXMLRequest.selectNodes( mcteParam_CERTIANN ) .getLength() == 0 )
      {
        wvarCertiAnn = Strings.fill( 4, "0" );
      }
      else
      {
        wvarCertiAnn = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN )  ), 4 );
      }
      //
      if( wobjXMLRequest.selectNodes( mcteParam_CERTISEC ) .getLength() == 0 )
      {
        wvarCertiSec = Strings.fill( 6, "0" );
      }
      else
      {
        wvarCertiSec = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC )  ), 6 );
      }
      //
      if( wobjXMLRequest.selectNodes( mcteParam_PATENTE ) .getLength() == 0 )
      {
        wvarPatente = Strings.fill( 10, " " );
      }
      else
      {
        wvarPatente = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PATENTE )  ) + Strings.fill( 10, " " ), 10 );
      }
      //
      if( wobjXMLRequest.selectNodes( mcteParam_MOTOR ) .getLength() == 0 )
      {
        wvarMotor = Strings.fill( 25, " " );
      }
      else
      {
        wvarMotor = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MOTOR )  ) + Strings.fill( 25, " " ), 25 );
      }
      //
      // ################################################
      // Agregado 04-08-2006. FJO. Paginaci�n
      if( wobjXMLRequest.selectSingleNode( mcteParam_NROQRY_S )  == (org.w3c.dom.Node) null )
      {
        wvarNROQRY_S = "00000000";
      }
      else
      {
        wvarNROQRY_S = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NROQRY_S )  );
      }

      if( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD_S )  == (org.w3c.dom.Node) null )
      {
        wvarRAMOPCOD_S = "    ";
      }
      else
      {
        wvarRAMOPCOD_S = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD_S )  );
      }

      if( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN_S )  == (org.w3c.dom.Node) null )
      {
        wvarPOLIZANN_S = "00";
      }
      else
      {
        wvarPOLIZANN_S = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN_S )  );
      }

      if( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC_S )  == (org.w3c.dom.Node) null )
      {
        wvarPOLIZSEC_S = "000000";
      }
      else
      {
        wvarPOLIZSEC_S = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC_S )  );
      }

      if( wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL_S )  == (org.w3c.dom.Node) null )
      {
        wvarCERTIPOL_S = "0000";
      }
      else
      {
        wvarCERTIPOL_S = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL_S )  );
      }

      if( wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN_S )  == (org.w3c.dom.Node) null )
      {
        wvarCERTIANN_S = "0000";
      }
      else
      {
        wvarCERTIANN_S = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN_S )  );
      }

      if( wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC_S )  == (org.w3c.dom.Node) null )
      {
        wvarCERTISEC_S = "000000";
      }
      else
      {
        wvarCERTISEC_S = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC_S )  );
      }

      // ################################################
      //
      wvarStep = 20;
      wobjXMLRequest = null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 60;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      //
      wvarStep = 70;
      wobjXMLParametros = null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarRamo + wvarPolizaAnn + wvarPolizaSec + wvarCertiPol + wvarCertiAnn + wvarCertiSec + wvarPatente + wvarMotor + wvarNROQRY_S + wvarRAMOPCOD_S + wvarPOLIZANN_S + wvarPOLIZSEC_S + wvarCERTIPOL_S + wvarCERTIANN_S + wvarCERTISEC_S;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQConnectionConnector.newInstance();
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder, wobjXMLConfig.selectSingleNode("//MQCONFIG"));
	strParseString = strParseStringHolder.getValue();

      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 190;
      wvarResult = "";
      //        wvarPos = 122 'cantidad de caracteres ocupados por par�metros de entrada
      //wvarPos = 156 'cantidad de caracteres ocupados por par�metros de entrada
      //LR. Se suman 5 porque aumento el MOTOR a 25
      wvarPos.set( 161 );
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 200;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 210;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 240;
        wvarResult = "";
        wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        //
        wvarStep = 250;
        wobjXMLResponse = new XmlDomExtended();
        //
        wvarStep = 260;
        wobjXMLResponse.loadXML( wvarResult );
        //
        if( wobjXMLResponse.selectNodes( "//CERTIFICADO" ) .getLength() != 0 )
        {
          wvarStep = 290;
          wobjXMLResponse = null;
          //
          wvarStep = 300;
          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
          //
        }
        else
        {
          //
          wvarStep = 270;
          wobjXMLResponse = null;
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          //
        }
      }
      //
      wvarStep = 310;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarRamo + wvarPolizaAnn + wvarPolizaSec + wvarCertiPol + wvarCertiAnn + wvarCertiSec + wvarPatente + wvarMotor + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant pvarPos, String pstrParseString, int pvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    VbScript_RegExp wobjRegExp = new VbScript_RegExp();
    MatchCollection wobjColMatch;
    Match wobjMatch = null;
    String wvarParseEvalString = "";
    XmlDomExtended wobjXmlDOMResp = null;
    XmlDomExtended wobjXmlDOMReq = null;
    org.w3c.dom.Element wobjXmlElemenResp = null;
    org.w3c.dom.Element wobjXmlElemenRespD = null;
    int wvarCantRegistros = 0;
    int wvarCcurrRegistro = 0;
    //
    //RegExp
    //MatchCollection
    //Match
    //
    //
    //
    wobjRegExp = new VbScript_RegExp();
    wobjRegExp.setGlobal( true ); 
    //
    // PARSEO DE LA RESPUESTA - Area Resultados
    //
    //Caracter (1) Estado CL Se desestima...
    pvarPos.set( pvarPos.add( new Variant( 1 ) ) );
    wvarParseEvalString = Strings.mid( pstrParseString, pvarPos.toInt() );
    wvarParseEvalString = Strings.replace( wvarParseEvalString, " ", "_" );
    //wvarCantRegistros = 200. LR 29/04/2009 Se cambia a 150
    wvarCantRegistros = 150;
    wvarCcurrRegistro = 0;

    //wobjRegExp.Pattern = "(\S{4})(\S{2})(\S{6})(\S{4})(\S{4})(\S{6})(\S{35})(\S{30})(\S{10})(\S{30})(\S{1})"
    //LR 29/04/2009. TOMARIES cambia a 70
    wobjRegExp.setPattern( "(\\S{4})(\\S{2})(\\S{6})(\\S{4})(\\S{4})(\\S{6})(\\S{35})(\\S{70})(\\S{10})(\\S{30})(\\S{1})" );
    //
    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    wobjXmlDOMResp = new XmlDomExtended();
    wobjXmlDOMResp.loadXML( "<CERTIFICADOS></CERTIFICADOS>" );
    //
    //
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //
      if( ! (wvarCcurrRegistro < wvarCantRegistros) )
      {
        break;
      }
      //error: function 'SubMatches' was not found.
      if (Strings.replace(wobjMatch.SubMatches(0), "_", " ").trim().equals("")) { 
      		 break;
}
      wvarCcurrRegistro = wvarCcurrRegistro + 1;
      //
      wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "CERTIFICADO" );
      /*unsup wobjXmlDOMResp.selectSingleNode( "//CERTIFICADOS" ) */.appendChild( wobjXmlElemenResp );
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "RAMOPCOD" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(0), "_", " ").trim());
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "POLIZANN" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(1), "_", " ").trim());
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "POLIZSEC" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(2), "_", " ").trim());
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "CERTIPOL" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(3), "_", " ").trim());
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "CERTIANN" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(4), "_", " ").trim());
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "CERTISEC" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(5), "_", " ").trim());
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "NOMBRECLIENTE" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(6), "_", " ").trim());
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "TOMARIES" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(7), "_", " ").trim());
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "PATENTE" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(8), "_", " ").trim());
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "SITUCPOL" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(9), "_", " ").trim());
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "IMPRIME_CM" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(10), "_", " ").trim());
    }
    //
    wobjMatch = null;
    //Set wobjRegExp = Nothing
    //
    // PARSEO DE LA RESPUESTA - Area Request
    //
    //pvarPos = 156
    //LR 29/04/2009. Se suman 5 porque aumento el MOTOR a 25
    pvarPos.set( 161 );

    wvarParseEvalString = Strings.mid( pstrParseString, 1, pvarPos.toInt() );
    wvarParseEvalString = Strings.replace( wvarParseEvalString, " ", "_" );
    //
    //Set wobjRegExp = CreateObject("VbScript_RegExp")
     wobjRegExp.setGlobal(true);
    //wobjRegExp.Pattern = "(\S{4})(\S{4})(\S{10})(\S{2})(\S{2})(\S{9})(\S{2})(\S{2})(\S{9})(\S{2})(\S{9})(\S{2})(\S{9})(\S{4})(\S{2})(\S{6})(\S{4})(\S{4})(\S{6})(\S{10})(\S{20})(\S{8})(\S{4})(\S{2})(\S{6})(\S{4})(\S{4})(\S{6})"
    //LR 29/04/2009. El motor cambia a 25
    wobjRegExp.setPattern( "(\\S{4})(\\S{4})(\\S{10})(\\S{2})(\\S{2})(\\S{9})(\\S{2})(\\S{2})(\\S{9})(\\S{2})(\\S{9})(\\S{2})(\\S{9})(\\S{4})(\\S{2})(\\S{6})(\\S{4})(\\S{4})(\\S{6})(\\S{10})(\\S{25})(\\S{8})(\\S{4})(\\S{2})(\\S{6})(\\S{4})(\\S{4})(\\S{6})" );

    //
    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    wobjMatch = wobjColMatch.next();
    //
    wobjXmlDOMReq = new XmlDomExtended();
    wobjXmlDOMReq.loadXML( "<RespuestaMQ></RespuestaMQ>" );
    //
    //Agrego el resultado del parseo de la respuesta
    /*unsup wobjXmlDOMReq.selectSingleNode( "//RespuestaMQ" ) */.appendChild( wobjXmlDOMResp.selectSingleNode( "//CERTIFICADOS" )  );
    //
    //Continuo con el parseo del request recibido.
    wobjXmlElemenResp = wobjXmlDOMReq.getDocument().createElement( "Request" );
    /*unsup wobjXmlDOMReq.selectSingleNode( "//RespuestaMQ" ) */.appendChild( wobjXmlElemenResp );
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "USUARIO" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(2), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "STATUS" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(3), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "ERR_DESC" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(4), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "RAMOPCOD" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(13), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "POLIZANN" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(14), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "POLIZSEC" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(15), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "CERTIPOL" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(16), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "CERTIANN" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(17), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "CERTISEC" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(18), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "PATENTE" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(19), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "MOTOR" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(20), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "NROQRY_S" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(21), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "RAMOPCOD_S" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(22), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "POLIZANN_S" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(23), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "POLIZSEC_S" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(24), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "CERTIPOL_S" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(25), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "CERTIANN_S" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(26), "_", " ").trim());
    //
    wobjXmlElemenRespD = wobjXmlDOMReq.getDocument().createElement( "CERTISEC_S" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
    //error: function 'SubMatches' was not found.
    wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(27), "_", " ").trim());
    //
    ParseoMensaje = XmlDomExtended.marshal(wobjXmlDOMReq.getDocument().getDocumentElement());
    //
    wobjXmlDOMResp = null;
    wobjXmlDOMReq = null;
    wobjXmlElemenResp = (org.w3c.dom.Element) null;
    wobjXmlElemenRespD = (org.w3c.dom.Element) null;
    //
    wobjRegExp = (VbScript_RegExp) null;

    return ParseoMensaje;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
