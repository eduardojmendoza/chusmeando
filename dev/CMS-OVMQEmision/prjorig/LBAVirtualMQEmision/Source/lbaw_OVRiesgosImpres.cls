VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVRiesgosImpres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQEmision.lbaw_OVRiesgosImpres"
Const mcteOpID              As String = "1582"

'LINKAGE MQ MSG 1582 -- 29/04/2009
'04 LNK-PARM-ENTRADA.
'    06 LNK-MENSAJE               PIC 9(4).
'    06 LNK-PARM-GRAL-SELECCION.
'       08 LNK-CIAASCOD           PIC X(4).
'       08 LNK-USUARCOD           PIC X(10).
'       08 LNK-ESTADO             PIC X(2).
'       08 LNK-ERROR              PIC X(2).
'       08 LNK-CLIENSECAS         PIC 9(9).
'       08 LNK-NIVELCLAS          PIC X(2).
'       08 LNK-NIVELCLA1          PIC X(2).
'       08 LNK-CLIENSEC1          PIC 9(9).
'       08 LNK-NIVELCLA2          PIC X(2).
'       08 LNK-CLIENSEC2          PIC 9(9).
'       08 LNK-NIVELCLA3          PIC X(2).
'       08 LNK-CLIENSEC3          PIC 9(9).
'    06 LNK-PARM-ESPE-SELECCION.
'       08 LNK-RAMOPCOD           PIC X(4).
'       08 LNK-POLIZANN           PIC 9(2).
'       08 LNK-POLIZSEC           PIC 9(6).
'       08 LNK-CERTIPOL           PIC 9(4).
'       08 LNK-CERTIANN           PIC 9(4).
'       08 LNK-CERTISEC           PIC 9(6).
'    06 LNK-PARM-START.
'       08 LNK-NROQRY-S           PIC 9(8).
'       08 LNK-RAMOPCOD-S         PIC X(4).
'       08 LNK-POLIZANN-S         PIC 9(2).
'       08 LNK-POLIZSEC-S         PIC 9(6).
'       08 LNK-CERTIPOL-S         PIC 9(4).
'       08 LNK-CERTIANN-S         PIC 9(4).
'       08 LNK-CERTISEC-S         PIC 9(6).
'
'04 LNK-PARM-SALIDA.
'    06 LNK-RESULTADOS            OCCURS 150. LR 29/04/2009 Se cambia a 150, antes 200
'       08 LNK-CLAVE-OPERACION.
'          10 LNK-RAMOPCOD        PIC X(4).
'          10 LNK-POLIZANN        PIC 9(2).
'          10 LNK-POLIZSEC        PIC 9(6).
'          10 LNK-CERTIPOL        PIC 9(4).
'          10 LNK-CERTIANN        PIC 9(4).
'          10 LNK-CERTISEC        PIC 9(6).
'       08 LNK-TOMARIES           PIC X(70). LR 29/04/2009 Se cambia a 70, antes 30
'       08 LNK-SITUCPOL           PIC X(30).
'       08 LNK-SINIESTRO          PIC X.
'       08 LNK-SWCOBER            PIC X.
'       08 LNK-SWIMPCOB           PIC X.
'       08 LNK-SWIMPTAR           PIC X.
'       08 LNK-SWIMPMER           PIC X.
'       08 LNK-PATENNUM           PIC X(10).
'       08 LNK-SWTIPSEG           PIC X.

'Parametros XML de Entrada
Const mcteParam_USUARCOD    As String = "//USUARIO"
Const mcteParam_RAMOPCOD    As String = "//RAMOPCOD"
Const mcteParam_POLIZANN    As String = "//POLIZANN"
Const mcteParam_POLIZSEC    As String = "//POLIZSEC"
Const mcteParam_CERTIPOL    As String = "//CERTIPOL"
Const mcteParam_CERTIANN    As String = "//CERTIANN"
Const mcteParam_CERTISEC    As String = "//CERTISEC"
Const mcteParam_Suplenum    As String = "//SUPLENUM"

' ################################################
' Agregado 08-08-2006. FJO. Paginación
'06  LNK-PARM-START. (Longitud: 34)
Const mcteParam_NROQRY_S        As String = "//NROQRY_S"    '08 LNK-NROQRY-S PIC 9(8).
Const mcteParam_RAMOPCOD_S      As String = "//RAMOPCOD_S"  '08 LNK-RAMOPCOD PIC X(4).
Const mcteParam_POLIZANN_S      As String = "//POLIZANN_S"  '08 LNK-POLIZANN PIC 9(2).
Const mcteParam_POLIZSEC_S      As String = "//POLIZSEC_S"  '08 LNK-POLIZSEC PIC 9(6).
Const mcteParam_CERTIPOL_S      As String = "//CERTIPOL_S"  '08 LNK-CERTIPOL PIC 9(4).
Const mcteParam_CERTIANN_S      As String = "//CERTIANN_S"  '08 LNK-CERTIANN PIC 9(4).
Const mcteParam_CERTISEC_S      As String = "//CERTISEC_S"  '08 LNK-CERTISEC PIC 9(6).
' ################################################


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarRamo            As String
    Dim wvarPolizaAnn       As String
    Dim wvarPolizaSec       As String
    Dim wvarCertiPol        As String
    Dim wvarCertiAnn        As String
    Dim wvarCertiSec        As String
    Dim wvarSuplenum        As String
    '
    ' ################################################
    ' Agregado 08-08-2006. FJO. Paginación
    Dim wvarNROQRY_S      As String
    Dim wvarRAMOPCOD_S    As String
    Dim wvarPOLIZANN_S    As String
    Dim wvarPOLIZSEC_S    As String
    Dim wvarCERTIPOL_S    As String
    Dim wvarCERTIANN_S    As String
    Dim wvarCERTISEC_S    As String
    ' ################################################
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUsuario = Left(.selectSingleNode(mcteParam_USUARCOD).Text & Space(10), 10)
        wvarRamo = Left(.selectSingleNode(mcteParam_RAMOPCOD).Text & Space(4), 4)
        wvarPolizaAnn = Right("00" & .selectSingleNode(mcteParam_POLIZANN).Text, 2)
        wvarPolizaSec = Right("000000" & .selectSingleNode(mcteParam_POLIZSEC).Text, 6)
        wvarCertiPol = Right("0000" & .selectSingleNode(mcteParam_CERTIPOL).Text, 4)
        wvarCertiAnn = Right("0000" & .selectSingleNode(mcteParam_CERTIANN).Text, 4)
        wvarCertiSec = Right("000000" & .selectSingleNode(mcteParam_CERTISEC).Text, 6)
        'wvarSuplenum = Right("0000" & .selectSingleNode(mcteParam_Suplenum).Text, 4)
        
        ' ################################################
        ' Agregado 08-08-2006. FJO. Paginación
        If .selectSingleNode(mcteParam_NROQRY_S) Is Nothing Then
            wvarNROQRY_S = "00000000"
        Else
            wvarNROQRY_S = .selectSingleNode(mcteParam_NROQRY_S).Text
        End If
        
        If .selectSingleNode(mcteParam_RAMOPCOD_S) Is Nothing Then
            wvarRAMOPCOD_S = "    "
        Else
            wvarRAMOPCOD_S = .selectSingleNode(mcteParam_RAMOPCOD_S).Text
        End If
        
        If .selectSingleNode(mcteParam_POLIZANN_S) Is Nothing Then
            wvarPOLIZANN_S = "00"
        Else
            wvarPOLIZANN_S = .selectSingleNode(mcteParam_POLIZANN_S).Text
        End If
        
        If .selectSingleNode(mcteParam_POLIZSEC_S) Is Nothing Then
            wvarPOLIZSEC_S = "000000"
        Else
            wvarPOLIZSEC_S = .selectSingleNode(mcteParam_POLIZSEC_S).Text
        End If
        
        If .selectSingleNode(mcteParam_CERTIPOL_S) Is Nothing Then
            wvarCERTIPOL_S = "0000"
        Else
            wvarCERTIPOL_S = .selectSingleNode(mcteParam_CERTIPOL_S).Text
        End If
        
        If .selectSingleNode(mcteParam_CERTIANN_S) Is Nothing Then
            wvarCERTIANN_S = "0000"
        Else
            wvarCERTIANN_S = .selectSingleNode(mcteParam_CERTIANN_S).Text
        End If
        
        If .selectSingleNode(mcteParam_CERTISEC_S) Is Nothing Then
            wvarCERTISEC_S = "000000"
        Else
            wvarCERTISEC_S = .selectSingleNode(mcteParam_CERTISEC_S).Text
        End If
        ' ################################################
        
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & _
                "    000000000    000000000  000000000  000000000" & _
                wvarRamo & wvarPolizaAnn & wvarPolizaSec & wvarCertiPol & _
                wvarCertiAnn & wvarCertiSec & wvarSuplenum & wvarNROQRY_S & wvarRAMOPCOD_S & wvarPOLIZANN_S & wvarPOLIZSEC_S & wvarCERTIPOL_S & wvarCERTIANN_S & wvarCERTISEC_S
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    wvarStep = 190
    wvarResult = ""
    'wvarPos = 93 'cantidad de caracteres ocupados por parámetros de entrada
    wvarPos = 127 'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 200
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 210
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 240
        wvarResult = ""
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        '
        wvarStep = 250
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 260
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML (wvarResult)
        '
        If wobjXMLResponse.selectNodes("//R").length <> 0 Then
            wvarStep = 270
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSL())
            '
            wvarStep = 280
            wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
            '
            wvarStep = 290
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
            '
            wvarStep = 300
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
            '
        Else
            '
            wvarStep = 270
            Set wobjXMLResponse = Nothing
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>"
            '
        End If
    End If
    '
    wvarStep = 310
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarRamo & wvarPolizaAnn & wvarPolizaSec & wvarCertiPol & wvarCertiAnn & wvarCertiSec & wvarSuplenum & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarstrLen As Long) As String
Dim wvarResult As String
    '
    'Const wvarCantidadRegistrosAProcesar As Long = 200 LR 29/04/2009 Pasa a 150
    Const wvarCantidadRegistrosAProcesar As Long = 150
    
    Dim wvarCantidadRegistrosProcesados As Long
    '
    wvarCantidadRegistrosProcesados = 1
    '
    wvarResult = wvarResult & "<RS>"
    '
    While wvarPos < wvarstrLen And Trim(Mid(strParseString, wvarPos, 4)) <> "" And wvarCantidadRegistrosProcesados <= wvarCantidadRegistrosAProcesar
        'wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 101) & "]]></R>"
        'wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 102) & "]]></R>"
        ' LR 29/04/2009 Se suma 40 de TOMARIES
        wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 142) & "]]></R>"
        
        'wvarPos = wvarPos + 88
        'wvarPos = wvarPos + 101
        'wvarPos = wvarPos + 102
        wvarPos = wvarPos + 142 ' LR 29/04/2009 Se suma 40 de TOMARIES
        
        wvarCantidadRegistrosProcesados = wvarCantidadRegistrosProcesados + 1
    Wend
    '
    wvarResult = wvarResult & "</RS>"
    '
    wvarResult = wvarResult & "<AreaINRespuesta>" & Left(strParseString, 126) & "</AreaINRespuesta>"
    '
    ParseoMensaje = "<RespuestaMQ>" & wvarResult & "</RespuestaMQ>"
    '
End Function
Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/'> "
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REGS'>"
    wvarStrXSL = wvarStrXSL & "              <xsl:apply-templates select='//RS/R'/>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='Request'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:apply-templates select='//AreaINRespuesta'/>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='//RS/R'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REG'>"
    'wvarStrXSL = wvarStrXSL & "             <xsl:element name='RAMO'>"
    'wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,1,1)' />"
    'wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='PROD'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,1,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='POL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,5,8)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERPOL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,13,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERANN'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,17,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERSEC'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,21,6)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='TOMA'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,27,70))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='EST'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,97,30))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='SINI'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,127,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='LUPA'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,128,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='IMPRCERTIFICADOCOBERTURA'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,129,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='IMPRTARJETACIRCULACION'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,130,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='IMPRCERTIFICADOMERCOSUR'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,131,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='PATENTENUM'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,132,10)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='RAMO'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,142,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "         <xsl:template match='//AreaINRespuesta'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='USUARIO'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,9,10)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='STATUS'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,19,2)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='ERR_DESC'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,21,2))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='RAMOPCOD'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,67,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='POLIZANN'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,71,2)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='POLIZSEC'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,73,6)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERTIPOL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,79,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERTIANN'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,83,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERTISEC'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,87,6)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='NROQRY_S'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,93,8)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='RAMOPCOD_S'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,101,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='POLIZANN_S'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,105,2)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='POLIZSEC_S'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,107,6)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERTIPOL_S'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,113,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERTIANN_S'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,117,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERTISEC_S'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,121,6)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "         </xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='TOMA'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='EST'/>"
    '
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
    '
End Function
















