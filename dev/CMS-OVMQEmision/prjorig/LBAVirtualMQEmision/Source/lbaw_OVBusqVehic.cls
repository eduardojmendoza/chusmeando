VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVBusqVehic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQEmision.lbaw_OVBusqVehic"
Const mcteOpID              As String = "1502"

'Parametros XML de Entrada
Const mcteParam_Usuario    As String = "//USUARIO"
Const mcteParam_PATENTE    As String = "//PATENBUS"
Const mcteParam_MOTOR      As String = "//MOTORBUS"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarPatente        As String
    Dim wvarMotor          As String
    '
    Dim wvarPos             As Long
    Dim wvarParseString      As String
    Dim wvarStringLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Parámetros:
        '- Usuario PIC X(10)
        '- Patente PIC X(10)
        '- Motor   PIC X(25)
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarPatente = Left(.selectSingleNode(mcteParam_PATENTE).Text & Space(10), 10)
        wvarMotor = Left(.selectSingleNode(mcteParam_MOTOR).Text & Space(25), 25)
    End With
    '
    
    'DUMMYYYYYYY
    If Not wobjXMLRequest.selectSingleNode("//DUMMY") Is Nothing Then
        Response = "<Response><Estado resultado='true' mensaje=''/>"
        Response = Response & "<VEHICULO>"
        Response = Response & "<VEHICSEC>" & "123456789" & "</VEHICSEC>"
        Response = Response & "<PATENNUM>" & "RSS122" & "</PATENNUM>"
        Response = Response & "<MOTORNUM>" & "1234567890123456789012345" & "</MOTORNUM>"
        Response = Response & "<AUVEHDES>" & "FORD FALCON" & "</AUVEHDES>"
        Response = Response & "<FABRIANN>" & "1981" & "</FABRIANN>"
        Response = Response & "<AUMODORI>" & "A" & "</AUMODORI>"
        Response = Response & "</VEHICULO>"
        Response = Response & "</Response>"
        Set wobjXMLRequest = Nothing
        mobjCOM_Context.SetComplete
        IAction_Execute = 0
        GoTo ClearObjects
    End If
    
    wvarStep = 60
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 70
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 100
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        If .childNodes.length > 0 Then
            wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
        End If
    End With
    '
    wvarStep = 110
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 120
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarPatente & wvarMotor
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, wvarParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & wvarParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    wvarStep = 240
    wvarResult = ""
    wvarPos = 102  'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarStringLen = Len(wvarParseString)
    '
    wvarStep = 250
    wvarEstado = Mid(wvarParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 260
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 280
        wvarResult = ParseoMensaje(wvarPos, wvarParseString, wvarStringLen)
        '
        wvarStep = 290
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 300
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML wvarResult
        '
        wvarStep = 310
        If wobjXMLResponse.selectNodes("//VEHICULO").length > 0 Then
            'Devolvemos el Response
            Response = "<Response><Estado resultado='true' mensaje=''/>" & wvarResult & "</Response>"
            '
        Else
            'No hay datos para devolver
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos'/></Response>"
            '
        End If
        Set wobjXMLResponse = Nothing
    End If
    '
    wvarStep = 320
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarPatente & wvarMotor & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(pvarPos As Long, pvarParseString As String, pvarStringLen As Long) As String
    Dim wvarResult As String
    Dim wvarCantLin As Long
    Dim wvarCounter As Long
    '
    wvarResult = wvarResult & "<VEHICULOS>"
    '
    If Trim(Mid(pvarParseString, pvarPos, 3)) <> "" Then
        wvarCantLin = Mid(pvarParseString, pvarPos, 3)
        pvarPos = pvarPos + 3
        '
        For wvarCounter = 0 To wvarCantLin - 1
            wvarResult = wvarResult & "<VEHICULO>"
            wvarResult = wvarResult & "<VEHICSEC>" & Mid(pvarParseString, pvarPos, 9) & "</VEHICSEC>"
            wvarResult = wvarResult & "<PATENNUM>" & Mid(pvarParseString, pvarPos + 9, 10) & "</PATENNUM>"
            wvarResult = wvarResult & "<MOTORNUM>" & Mid(pvarParseString, pvarPos + 19, 25) & "</MOTORNUM>"
            wvarResult = wvarResult & "<AUVEHDES>" & Mid(pvarParseString, pvarPos + 44, 30) & "</AUVEHDES>"
            wvarResult = wvarResult & "<FABRIANN>" & Mid(pvarParseString, pvarPos + 74, 4) & "</FABRIANN>"
            wvarResult = wvarResult & "<AUMODORI>" & Mid(pvarParseString, pvarPos + 78, 1) & "</AUMODORI>"
            wvarResult = wvarResult & "</VEHICULO>"
            pvarPos = pvarPos + 79
        Next
    '
    End If
    wvarResult = wvarResult & "</VEHICULOS>"
    '
    ParseoMensaje = wvarResult
End Function











