package com.qbe.services.ovmqemision.impl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.testutils.TestUtils;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;


public class OVMQEmisionMessageTest {

		public OVMQEmisionMessageTest() {
			super();
		}		
		
		/**
		 * Testea la ejecución del mensaje, y que no vuelva un código de error. Valida contra la respuesta esperada
		 * 
		 * @param msgCode
		 * @throws Exception 
		 */
		protected void testMessageFullRequest (String msgCode, VBObjectClass comp) throws Exception {
			String responseText = this.testMessageJustExecution(msgCode, comp);

			Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
			XMLUnit.setIgnoreWhitespace(true);
			String expectedResponse = TestUtils.loadResponse(msgCode);
			if ( expectedResponse == null || expectedResponse.length()==0) throw new Exception("No se pudo cargar el request");
			DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(expectedResponse, responseText));
			List allDifferences = myDiff.getAllDifferences();
			assertEquals(myDiff.toString(), 0, allDifferences.size());
			//Eliminar los &lt; y &gt;
			XMLAssert.assertXMLEqual("No volvió la respuesta esperada", expectedResponse, responseText);

		}
		
		/**
		 * Testea la ejecución del mensaje, y que no vuelva un código de error. No valida contra la respuesta esperada, la devuelve solamente
		 * 
		 * @param msgCode
		 * @throws Exception 
		 */
		protected String testMessageJustExecution (String msgCode, VBObjectClass comp) throws Exception {
			String request = TestUtils.loadRequest(msgCode);
			if ( request == null || request.length()==0 || request.trim().equals("")) throw new Exception("No se pudo cargar el request");
			StringHolder responseHolder = new StringHolder();
			int result = comp.IAction_Execute(request, responseHolder, null);
			if ( result == 1) Assert.fail ("Abortó la ejecución, ver el log");
			Assert.assertTrue("Result no es 1 ni 0, es un error",result == 0);
			try {
				TestUtils.parseResultado(responseHolder.getValue());
				TestUtils.parseMensaje(responseHolder.getValue());
			} catch (Exception e){
				assertTrue("No es posible parsear la respuesta", false);
			}
			String responseText = responseHolder.getValue();
			if (( responseText == null) || ( responseText.length()==0)) {
				fail("responseText vacío");
			}
			return responseText;
		}
//------------------------------ T E S T ------------------------------------------	
		
		@Test
		public void testMSG_1522() throws Exception {
			String msgCode = "1522";
			testMessageFullRequest(msgCode,new lbaw_OVGetRetenc());
		}
		
		@Test
		public void testMSG_1525() throws Exception {
			String msgCode = "1525";
			testMessageFullRequest(msgCode, new lbaw_OVBajaPropuestas());
		}
		
		@Test
		public void testMSG_1526() throws Exception {
			String msgCode = "1526";
			testMessageFullRequest(msgCode,new lbaw_OVEmisPropuesta());
		}
		
		@Test
		@Ignore
		public void testMSG_1540() throws Exception {
			String msgCode = "1540";
			testMessageFullRequest(msgCode, new lbaw_OVAisPutSolicAUS());
		}
		
		@Test
		public void testMSG_1540_2() throws Exception {
			Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
			String msgCode = "1540_2";
			testMessageFullRequest(msgCode, new lbaw_OVAisPutSolicAUS());
		}
		
		@Test
		public void testMSG_1581() throws Exception {
			String msgCode = "1581";
			testMessageFullRequest(msgCode,new lbaw_OVVerifImpresion());
		}
		
		@Test
		public void testMSG_1582() throws Exception {
			String msgCode = "1582";
			testMessageFullRequest(msgCode,new lbaw_OVRiesgosImpres());
		}
		
		@Test
		public void testMSG_1583() throws Exception {
			String msgCode = "1583";
			testMessageFullRequest(msgCode, new lbaw_OVEndososImpres());
		}
		
		@Test
		public void testMSG_1584() throws Exception {
			String msgCode = "1584";
			testMessageFullRequest(msgCode,new lbaw_GetCertMercosur());
		}
		
		@Test
		public void testMSG_1701() throws Exception {
			String msgCode = "1701";
			String response = testMessageJustExecution(msgCode,new lbaw_OVImprimirPoliza());
			XMLAssert.assertXpathEvaluatesTo("5", "count(//Response/FILES/FILE)", response);
		}

		@Test
		@Ignore
		public void testGetBinaryFile() throws Exception {
			String msgCode = "GetBinaryFile";
			String response = testMessageJustExecution(msgCode,new lbaw_OVGetBinaryFile());
			XMLAssert.assertXpathEvaluatesTo("1", "count(//Response/BinData)", response);
		}
		
		@Test
		@Ignore
		public void testGetBinaryFileNotExists() throws Exception {
			String msgCode = "GetBinaryFileNotExists";
			String response = testMessageJustExecution(msgCode,new lbaw_OVGetBinaryFile());
			XMLAssert.assertXpathEvaluatesTo(lbaw_OVGetBinaryFile.MENSAJE_ERROR_405, "//Response/Estado/@mensaje", response);
		}
		
		@Test
		@Ignore //No resuelve hostplanetpress en el ard844
		public void testGetBinaryFilePlanetPress() throws Exception {
			Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
			String msgCode = "GetBinaryFilePlanetPress";
			String response = testMessageJustExecution(msgCode,new lbaw_OVGetBinaryFile());
			//No testeo nada porque no sé si el archivo va a existir. Este test valida que no haya exceptions inesperadas
		}
		
		@Test
		@Ignore //No resuelve hostplanetpress en el ard844
		public void testGetBinaryFilePlanetPressExplicito() throws Exception {
			Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
			String msgCode = "GetBinaryFilePlanetPressExplicito";
			String response = testMessageJustExecution(msgCode,new lbaw_OVGetBinaryFile());
		}
		
		
}
