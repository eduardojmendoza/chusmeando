package com.qbe.services.ovmqemision.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.context.support.GenericXmlApplicationContext;

import com.qbe.services.fileutils.CMSFileUtils;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.ovmqemision.Constants;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;

/**
 * Objetos del FrameWork
 */

public class lbaw_GenerarZIP implements VBObjectClass {

	protected static Logger logger = Logger.getLogger(lbaw_GenerarZIP.class.getName());

	static final String mcteClassName = "lbawA_OfVUtilsFiles.lbaw_OVGenerarZIP";

	static final String mcteOpID = "";
	/**
	 * Parametros XML de Entrada
	 */
	static final String mcteParam_SERVER = "//SERVER";
	static final String mcteParam_FILENAME = "//FILENAME";
	static final String mcteParam_MANTENERARCHIVO = "//MANTENERARCHIVO";

	public static final String PROPS = "classpath:getBinaryFileProps.xml";
	public static final String SERVERPATHPROPS = "serverpaths.xml";

	private static GenericXmlApplicationContext ctx = loadContext();

	private EventLog mobjEventLog = new EventLog();
	/**
	 * static variable for method: IAction_Execute
	 */
	private final String wcteFnName = "IAction_Execute";

	@Override
	public int IAction_Execute(String request, StringHolder response, String contextInfo) {
		int IAction_Execute = 0;
		Variant vbLogEventTypeError = new Variant();
		XmlDomExtended wobjXMLRequest = null;
		XmlDomExtended wobjXMLParametros = null;
		XmlDomExtended serverpathsXML = new XmlDomExtended();
		int wvarMQError = 0;
		int wvarStep = 0;
		String wvarParseString = "";
		int wvarStringLen = 0;
		String filename = "";
		String hostdir = "";
		String hostname = "";
		String wvarEstado = "";
		String wvarErrCod = "";

		byte[] fileZip;
		//
		//
		//
		//
		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			// Levanto los parámetros que llegan desde la página
			wobjXMLRequest = new XmlDomExtended();
			XmlDomExtended wobjXMLResponse = new XmlDomExtended();

			wobjXMLRequest.loadXML(request);
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//
			wvarStep = 10;
			if (wobjXMLRequest.selectSingleNode(mcteParam_FILENAME) == (org.w3c.dom.Node) null) {
				throw new ComponentExecutionException("No existe el nodo con el nombre del archivo");
			}

			if (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_FILENAME)).equals("")) {
				throw new ComponentExecutionException("No contiene ningun nombre de archivo");
			}
			if (wobjXMLRequest.selectSingleNode(mcteParam_SERVER) == (org.w3c.dom.Node) null) {
				throw new ComponentExecutionException("No existe el nodo con el nombre del host");
			}

			if (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_SERVER)).equals("")) {
				throw new ComponentExecutionException("No contiene ningun nombre del host server");
			}
			filename = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_FILENAME));
			hostdir = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_SERVER));
			try {
				if (ctx.getBeanFactory().resolveEmbeddedValue("${zip.fold." + hostdir.toLowerCase() + "}").equals(""))
					throw new ComponentExecutionException("ruta vacia");
			} catch (IllegalArgumentException ne) {
				throw new ComponentExecutionException("No existe esa clave de ese directorio en el properties");
			}

			hostname = Constants.LOCAL_PREFIX
					+ ctx.getBeanFactory().resolveEmbeddedValue("${zip.fold." + hostdir.toLowerCase() + "}");
			// hostname = Constants.LOCAL_PREFIX + "////GT-PC//Users//Public";

			boolean wvarMarcaMantenerArch = !(wobjXMLRequest
					.selectSingleNode(mcteParam_MANTENERARCHIVO) == (org.w3c.dom.Node) null);

			String resourceURL = "";
			if (hostname.startsWith("http://") || hostname.startsWith("file://")) {
				resourceURL = hostname + File.separator + filename;
				resourceURL = FilenameUtils.separatorsToSystem(resourceURL);
			} else if (hostname.startsWith("\\")) {
				resourceURL = "http:" + hostname + File.separator + filename;
				resourceURL = FilenameUtils.separatorsToSystem(resourceURL);
			} else {
				resourceURL = "http://" + hostname + File.separator + filename;
				resourceURL = FilenameUtils.separatorsToSystem(resourceURL);
			}

			logger.log(Level.INFO, "RUTA FINAL:  " + resourceURL);
			String[] nameFiles = CMSFileUtils.getFiles(hostname, filename);

			// if (!CMSFileUtils.resourceExists(new URL(resourceURL))){
			if (nameFiles.length < 1) {
				response.set(
						"<Response><Estado resultado='false' mensaje='NO EXISTE EL ARCHIVO EN EL SERVIDOR CONSULTADO' /><CODIGOERROR>"
								+ wvarErrCod + "</CODIGOERROR></Response>");
				// }else if (CMSFileUtils.loadResource(new
				// URL(resourceURL)).length<1) {
				// //
				// //No hay datos para devolver
				// response.set( "<Response><Estado resultado='false'
				// mensaje='No se ha recibido el mensaje de
				// respuesta'/></Response>" );
				//
			} else {

				String zipPath = hostname;
				File f = new File((new URL(zipPath + File.separator
						+ FilenameUtils.getBaseName(filename) + ".zip")).toURI());
				ZipOutputStream out = new ZipOutputStream(new FileOutputStream(f));

				for (String nameFile : nameFiles) {

					// fileZip = CMSFileUtils.loadResource(new
					// URL(resourceURL));
					fileZip = CMSFileUtils.loadResource(new URL(FilenameUtils.separatorsToSystem(
							hostname + File.separator + nameFile)));

					ZipEntry e = new ZipEntry(nameFile);
					out.putNextEntry(e);

					out.write(fileZip, 0, fileZip.length);
					out.closeEntry();
				}
				out.close();
				wvarParseString = f.getPath();
				// Armamos la respuesta con el mensaje recibido

				response.set("<Response>");
				response.set(response + "<Estado resultado='true' mensaje=''/>");
				response.set(response + ParseoMensaje(wvarParseString));
				response.set(response + "</Response>");
				//

			}
			//
			//
			IAction_Execute = 0;
			wvarStep = 290;
			/* TBD mobjCOM_Context.SetComplete() ; */

			//
			// ~~~~~~~~~~~~~~~
			ClearObjects:
			// ~~~~~~~~~~~~~~~
			// LIBERO LOS OBJETOS
			return IAction_Execute;
			//
			// ~~~~~~~~~~~~~~~
		} catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
			response.set(
					"<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>");
			return 3;
		} catch (Exception _e_) {
			Err.set(_e_);
			logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);

			mobjEventLog
					.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName,
							wcteFnName, wvarStep, Err.getError().getNumber(),
							"Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription()
									+ " Mensaje:" + hostname + "/" + filename + " Hora:" + DateTime.now(),
							vbLogEventTypeError);
			IAction_Execute = 1;
			throw new ComponentExecutionException(_e_);
		}
	}

	private void ObjectControl_Activate() throws Exception {
	}

	private boolean ObjectControl_CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		return ObjectControl_CanBePooled;
	}

	private void ObjectControl_Deactivate() throws Exception {
	}

	private String ParseoMensaje(String pvarParseString) throws Exception {
		String parseoMensaje = "";
		String wvarResult = "";
		int wvarCantLin = 0;
		int wvarCounter = 0;

		parseoMensaje = ((pvarParseString.length() > 0) ? pvarParseString.substring(1) : "");

		wvarResult = wvarResult + "<ZIP>";
		wvarResult = wvarResult + Constants.ZIP_PREFIX + parseoMensaje;
		wvarResult = wvarResult + "</ZIP>";
		//

		return wvarResult;
	}

	/**
	 * -------------------------------------------------------------------------
	 * ------------------------------------------ // Metodos del Framework
	 * -------------------------------------------------------------------------
	 * ------------------------------------------
	 */
	public void Activate() throws Exception {
	}

	public boolean CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		//
		ObjectControl_CanBePooled = true;
		//
		return ObjectControl_CanBePooled;
	}

	public void Deactivate() throws Exception {
	}

	private static GenericXmlApplicationContext loadContext() {
		GenericXmlApplicationContext context = new GenericXmlApplicationContext();
		context.load(PROPS);
		context.refresh();
		context.start();
		return context;
	}
}
