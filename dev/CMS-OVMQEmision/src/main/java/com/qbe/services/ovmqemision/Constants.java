package com.qbe.services.ovmqemision;


public interface Constants {

	public static final String PLANETPRESS_PREFIX = "planetpress://";

	public static final String INSIS_PREFIX = "insis://";
	
	public static final String CMS_PREFIX = "cms://";
	
	public static final String ZIP_PREFIX = "zip://";
	
	public static final String LOCAL_PREFIX = "file:///";

	//Simeon says el: Domingo, 25 de Agosto de 2013 11:24 a.m.
	// "In the response you will receive the absolute path where the file is located. 
	// You should try to get the file from this path.
	//Por lo tanto si viene el full path, no necesitamos prefijarle un dir
	public static final CharSequence INSIS_PRINTOUT_DIR = "";

	/**
	 * Para los archivos generados localmente por CMS, también mandamos el full path
	 */
	public static final CharSequence CMS_PRINTOUT_DIR = "";


}
