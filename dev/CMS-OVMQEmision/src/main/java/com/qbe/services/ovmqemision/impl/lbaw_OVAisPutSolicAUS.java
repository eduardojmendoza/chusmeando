package com.qbe.services.ovmqemision.impl;
import java.text.DecimalFormat;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVGetCotiAUS;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import diamondedge.util.VbCollection;
import com.qbe.vbcompat.format.VBFixesUtil;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVAisPutSolicAUS implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQEmision.lbaw_OVAisPutSolicAUS";
  static final String mcteOpID = "1540";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARCOD = "//USUARCOD";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_Suplenum = "//SUPLENUM";
  static final String mcteParam_TIPOOPERACION = "//TIPOOPERACION";
  static final String mcteParam_OPCION = "//OPCION";
  static final String mcteParam_FUNCION = "//FUNCION";
  /**
   * Datos del Cliente
   */
  static final String mcteParam_RAZONSOCIAL = "//RAZON_SOCIAL";
  static final String mcteParam_NUMEDOCU = "//NUMEDOCU";
  static final String mcteParam_TIPODOCU = "//TIPODOCU";
  static final String mcteParam_CliCLIENAP1 = "//CLICLIENAP1";
  static final String mcteParam_CliCLIENAP2 = "//CLICLIENAP2";
  static final String mcteParam_CliCLIENNOM = "//AISCLICLIENNOM";
  static final String mcteParam_CliNACIMANN = "//NACIMANN";
  static final String mcteParam_CliNACIMMES = "//NACIMMES";
  static final String mcteParam_CliNACIMDIA = "//NACIMDIA";
  static final String mcteParam_Sexo = "//SEXO";
  static final String mcteParam_ESTCIV = "//ESTCIV";
  static final String mcteParam_CliPAISSCOD = "//CLIPAISSCOD";
  static final String mcteParam_CliIDIOMCOD = "//CLIIDIOMCOD";
  static final String mcteParam_CliEFECTANN = "//CLIEFECTANN";
  static final String mcteParam_CliEFECTMES = "//CLIEFECTMES";
  static final String mcteParam_CliEFECTDIA = "//CLIEFECTDIA";
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_CliCLIENTIP = "//CLICLIENTIP";
  static final String mcteParam_CliCLIENFUM = "//CLICLIENFUM";
  static final String mcteParam_CliCLIENDOZ = "//CLICLIENDOZ";
  static final String mcteParam_CliABRIDTIP = "//CLIABRIDTIP";
  static final String mcteParam_CliABRIDNUM = "//CLIABRIDNUM";
  static final String mcteParam_CliCLIENORG = "//CLICLIENORG";
  /**
   * Domicilio Cliente
   */
  static final String mcteParam_DOMICSEC = "//DOMICSEC";
  static final String mcteParam_CliDOMICCAL = "//CLIDOMICCAL";
  static final String mcteParam_CliDOMICDOM = "//CLIDOMICDOM";
  static final String mcteParam_CliDOMICDNU = "//CLIDOMICDNU";
  static final String mcteParam_CliDOMICESC = "//CLIDOMICESC";
  static final String mcteParam_CliDOMICPIS = "//CLIDOMICPIS";
  static final String mcteParam_CliDOMICPTA = "//CLIDOMICPTA";
  static final String mcteParam_CliDOMICPOB = "//CLIDOMICPOB";
  static final String mcteParam_CliDOMICCPO = "//CLIDOMICCPO";
  static final String mcteParam_CliDOMPROVICOD = "//CODPROV";
  static final String mcteParam_CliDOMPAISSCOD = "//CLIDOMPAISSCOD";
  /**
   * Const mcteParam_CliDOMICPRE            As String = "//CLIDOMICPRE"
   */
  static final String mcteParam_CliDOMCPACODPO = "//CLIDOMCPACODPO";
  static final String mcteParam_CliDOMICTLF = "//CLIDOMICTLF";
  /**
   * Cuenta Bancaria
   */
  static final String mcteParam_CUENTSEC = "//CUENTSEC";
  static final String mcteParam_TIPOCUEN = "//TIPOCUEN";
  static final String mcteParam_BANCOCOD_CUENTA = "//BANCOCODCUE";
  static final String mcteParam_SUCURCOD_CUENTA = "//SUCURCODCUE";
  /**
   * Efectivo ES
   */
  static final String mcteParam_CUENTDC = "//COBROTIP";
  static final String mcteParam_CUENNUME = "//CUENNUME";
  /**
   * "//TARJECOD"
   */
  static final String mcteParam_TARJECOD = "//COBROCOD";
  static final String mcteParam_VENCIANN = "//VENCIANN";
  static final String mcteParam_VENCIMES = "//VENCIMES";
  static final String mcteParam_VENCIDIA = "//VENCIDIA";
  /**
   * Impuestos Vector de 10
   */
  static final String mcteParam_IMPUESTO = "//IMPUESTOS/IMPUESTO";
  static final String mcteParam_IMPUECOD = "//IMPUECOD";
  static final String mcteParam_IMPUEPAR = "//IMPUEPAR";
  static final String mcteParam_IMPUEDAT = "//IMPUEDAT";
  /**
   * Vector 1208 de 10 posiciones
   */
  static final String mcteParam_NODOCLA = "//CLAIS/CLAI";
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_CLAIDENT = "//CLAIDENT";
  /**
   * Situacion Ganancias Cliente
   */
  static final String mcteParam_CLIENIVA = "//CLIENIVA";
  static final String mcteParam_CLIESEEX = "//CLIESEEX";
  static final String mcteParam_CLIEIBTP = "//CLIEIBTP";
  static final String mcteParam_CLIEIBEX = "//CLIEIBEX";
  static final String mcteParam_CLIEIiTP = "//CLIEIiTP";
  static final String mcteParam_CLIEIiEX = "//CLIEIiEX";
  static final String mcteParam_NROIIBB = "//NROIIBB";
  static final String mcteParam_CLIEGNTP = "//CLIEGNTP";
  static final String mcteParam_DOCUMTIPIVA = "//TIPODOCU";
  static final String mcteParam_DOCUMDATCLI = "//NUMEDOCU";
  /**
   * Vector de 10 posiciones de medios de contacto
   */
  static final String mcteParam_NODOMEDIOCONTACTO = "//MEDIOSCONTACTO/MEDIO";
  static final String mcteParam_MEDCOSEC = "//MEDCOSEC";
  static final String mcteParam_MEDCOCOD = "//MEDCOCOD";
  static final String mcteParam_MEDCODAT = "//MEDCODAT";
  static final String mcteParam_MEDCOSYS = "//MEDCOSYS";
  /**
   * Profesion Cliente
   */
  static final String mcteParam_PROFESEC = "//PROFESEC";
  static final String mcteParam_PROFECOD = "//PROFECOD";
  static final String mcteParam_TIPOCODI = "//TIPOCODI";
  /**
   * Datos del vehiculo
   */
  static final String mcteParam_VEHICSEC = "//VEHICSEC";
  static final String mcteParam_AUMARCOD = "//AUMARCOD";
  static final String mcteParam_AUMODCOD = "//AUMODCOD";
  static final String mcteParam_AUSUBCOD = "//AUSUBCOD";
  static final String mcteParam_AUADICOD = "//AUADICOD";
  static final String mcteParam_AUMODORI = "//AUMODORI";
  static final String mcteParam_SIFMVEHI_DES = "//SIFMVEHI_DES";
  static final String mcteParam_AUTIPCOD = "//AUTIPCOD";
  static final String mcteParam_FABRICAN = "//FABRICAN";
  static final String mcteParam_FABRICMES = "//FABRICMES";
  static final String mcteParam_MOTORNUM = "//MOTORNUM";
  static final String mcteParam_CHASINUM = "//CHASINUM";
  static final String mcteParam_PATENNUM = "//PATENNUM";
  static final String mcteParam_VEHCLRCOD = "//VEHCLRCOD";
  static final String mcteParam_AUCATCOD = "//AUCATCOD";
  static final String mcteParam_AUUSOCOD = "//AUUSOCOD";
  static final String mcteParam_AUVTVCOD = "//AUVTVCOD";
  static final String mcteParam_AUVTVANN = "//AUVTVANN";
  static final String mcteParam_AUVTVMES = "//AUVTVMES";
  static final String mcteParam_AUVTVDIA = "//AUVTVDIA";
  static final String mcteParam_AUKLMNUM = "//AUKLMNUM";
  static final String mcteParam_AUPAISCOD = "//AUPAISCOD";
  static final String mcteParam_AUPROVICOD = "//AUPROVICOD";
  static final String mcteParam_AUDOMCPACODPO = "//AUDOMCPACODPO";
  static final String mcteParam_GUGARAGE = "//GUGARAGE";
  static final String mcteParam_GUDOMICI = "//GUDOMICI";
  /**
   *  Asegurados Adicionales ...son 10 iteraciones 'ojo se estan poniendo los hijos
   */
  static final String mcteParam_CONDUCTORES = "//CONDUCTORES/CONDUCTOR";
  static final String mcteParam_CONDUSEC = "//CONDUSEC";
  static final String mcteParam_CONDUTDO = "//CONDUTDO";
  static final String mcteParam_DOCUMDAT = "//CONDUDOC";
  static final String mcteParam_CONDUAPE = "//APELLIDOHIJO";
  static final String mcteParam_CONDUNOM = "//NOMBREHIJO";
  static final String mcteParam_CONDUVIN = "//CONDUVIN";
  static final String mcteParam_CONDUFEC = "//NACIMHIJO";
  static final String mcteParam_CONDUSEX = "//SEXOHIJO";
  static final String mcteParam_CONDUEST = "//ESTADOHIJO";
  static final String mcteParam_CONDUEXC = "//EXCLUIDO";
  /**
   *  Accesorios ...son 10 iteraciones
   */
  static final String mcteParam_ACCESORIOS = "//ACCESORIOS/ACCESORIO";
  static final String mcteParam_VEACCSEC = "//VEACCSEC";
  static final String mcteParam_AUACCCOD = "//CODIGOACC";
  static final String mcteParam_AUVEASUM = "//PRECIOACC";
  static final String mcteParam_AUVEADES = "//DESCRIPCIONACC";
  static final String mcteParam_AUVEADEP = "//DEPRECIA";
  /**
   * 
   * Mas datos del cliente
   * mcteParam_CLIENSEC
   * mcteParam_CUENTSEC
   * mcteParam_DOMICSEC
   */
  static final String mcteParam_COBROFOR = "//COBROFOR";
  /**
   * mcteParam_CLIENIVA
   * Campa�as vector de 10
   */
  static final String mcteParam_CAMP_CODIGO = "//CAMP_CODIGO";
  /**
   * Coberturas, vector de 10
   */
  static final String mcteParam_COBERTURAS = "//COBERTURAS/COBERTURA";
  static final String mcteParam_COBERCOD = "//COBERCOD";
  static final String mcteParam_COBERORD = "//COBERORD";
  static final String mcteParam_CAPITASG = "//CAPITASG";
  static final String mcteParam_CAPITIMP = "//CAPITIMP";
  /**
   * Datos de la Poliza
   */
  static final String mcteParam_EXPEDNUM = "//EXPEDNUM";
  static final String mcteParam_CENTROCOD = "//CENTRCOD";
  static final String mcteParam_POLEFECTANN = "//EFECTANN2";
  static final String mcteParam_POLEFECTMES = "//EFECTMES";
  static final String mcteParam_POLEFECTDIA = "//EFECTDIA";
  static final String mcteParam_POLVENCIANN = "//POLVENCIANN";
  static final String mcteParam_POLVENCIMES = "//POLVENCIMES";
  static final String mcteParam_POLVENCIDIA = "//POLVENVIDIA";
  /**
   * efectivo tarjeta, etc.
   */
  static final String mcteParam_COBROCOD = "//COBROCOD";
  static final String mcteParam_COASETIP = "//COASETIP";
  static final String mcteParam_EXESEPOR = "//EXESEPOR";
  static final String mcteParam_FESOLANN = "//FESOLANN";
  static final String mcteParam_FESOLMES = "//FESOLMES";
  static final String mcteParam_FESOLDIA = "//FESOLDIA";
  static final String mcteParam_FEINGANN = "//FEINGANN";
  static final String mcteParam_FEINGMES = "//FEINGMES";
  static final String mcteParam_FEINGDIA = "//FEINGDIA";
  static final String mcteParam_RECARPOR = "//RECARPOR";
  /**
   * ESTO ES PLAN DE PAGO
   */
  static final String mcteParam_PLANCOD = "//PLANPAGOCOD";
  /**
   * SI TIENE O NO ACREEDOR PRENDARIO
   */
  static final String mcteParam_SWACREPR = "//SWACREPR";
  /**
   *  Acreedor Prendario
   */
  static final String mcteParam_NUMEDOCU_ACRE = "//NUMEDOCU_ACRE";
  static final String mcteParam_TIPODOCU_ACRE = "//TIPODOCU_ACRE";
  static final String mcteParam_APELLIDO_ACRE = "//APELLIDO";
  static final String mcteParam_APELLIDO_ACRE2 = "//APELLIDO2";
  static final String mcteParam_NOMBRES = "//NOMBRES";
  static final String mcteParam_CLIENTIP_ACRE = "//CLIENTIP_ACRE";
  static final String mcteParam_DOMICCAL = "//DOMICCAL";
  static final String mcteParam_DOMICDOM = "//DOMICDOM";
  static final String mcteParam_DOMICDNU = "//DOMICDNU";
  static final String mcteParam_DOMICESC = "//DOMICESC";
  static final String mcteParam_DOMICPIS = "//DOMICPIS";
  static final String mcteParam_DOMICPTA = "//DOMICPTA";
  static final String mcteParam_DOMICPOB = "//DOMICPOB";
  /**
   * Const mcteParam_DOMICCPO                As String = "//DOMICCPO"
   */
  static final String mcteParam_PROVICOD = "//PROVICOD";
  static final String mcteParam_PAISSCOD = "//PAISSCOD";
  static final String mcteParam_DOMICPRE = "//DOMICPRE";
  static final String mcteParam_DOMICTLF = "//DOMICTLF";
  static final String mcteParam_CPACODPO = "//DOMICCPO";
  static final String mcteParam_FRANQCOD = "//FRANQCOD";
  /**
   * PLAN DE LA POLIZA (TERCEROS, RIESGO, ETC)
   */
  static final String mcteParam_PLANPILICOD = "//PLANPOLICOD";
  /**
   * 
   */
  static final String mcteParam_PRECIOFT = "//PRECIO_MENSUAL";
  static final String mcteParam_ZONA = "//ZONA";
  static final String mcteParam_CODPROV = "//CODPROV";
  /**
   * VALOR DEL VEHICULO
   */
  static final String mcteParam_SUMALBA = "//SUMALBA";
  /**
   * VALOR DEL VEHICULO
   */
  static final String mcteParam_SUMASEG = "//SUMASEG";
  static final String mcteParam_CLUB_LBA = "//CLUB_LBA";
  static final String mcteParam_CTAKMS = "//CTAKMS";
  static final String mcteParam_ESCERO = "//ESCERO";
  /**
   *  Inspeccion
   */
  static final String mcteParam_EFECTANN = "//INSPEANN";
  static final String mcteParam_EFECTMES = "//INSPEMES";
  static final String mcteParam_EFECTDIA = "//INSPEDIA";
  static final String mcteParam_ESTAINSP = "//ESTAINSP";
  static final String mcteParam_INSPECOD = "//INSPECOD";
  static final String mcteParam_INSPEDES = "//INSPECTOR";
  static final String mcteParam_CENSICOD = "//CENTRCOD_INS";
  static final String mcteParam_CENSIDES = "//CENTRDES";
  static final String mcteParam_INSPEKMS = "//INSPEKMS";
  static final String mcteParam_INSPEOBLEA = "//INSPEOBLEA";
  static final String mcteParam_INSPEOBS = "//OBSERTXT";
  static final String mcteParam_INSPADOM = "//INSPEADOM";
  static final String mcteParam_INS_DOMICDOM = "//INS_DOMICDOM";
  static final String mcteParam_INS_CPACODPO = "//INS_CPACODPO";
  static final String mcteParam_INS_DOMICPOB = "//INS_DOMICPOB";
  static final String mcteParam_INS_PROVICOD = "//INS_PROVICOD";
  static final String mcteParam_INS_DOMICTLF = "//INS_DOMICTLF";
  static final String mcteParam_INS_RANGOHOR = "//INS_RANGOHOR";
  static final String mcteParam_DISPOCOD = "//RASTREO";
  static final String mcteParam_PRESTCOD = "//ALARMCOD";
  /**
   * mcteParam_ESCERO
   */
  static final String mcteParam_NROFACTU = "//NROFACTU";
  /**
   * Datos propios del estado del auto
   */
  static final String mcteParam_DDJJ = "//DDJJ_STRING";
  /**
   * mcteParam_AUMARCOD
   * mcteParam_AUMODCOD
   * mcteParam_AUSUBCOD
   * mcteParam_AUADICOD
   * mcteParam_AUMODORI
   * mcteParam_SIFMVEHI_DES
   * mcteParam_AUTIPCOD
   * mcteParam_FABRICAN
   * mcteParam_FABRICMES
   * mcteParam_MOTORNUM
   * mcteParam_CHASINUM
   * mcteParam_PATENNUM
   * mcteParam_VEHCLRCOD
   * mcteParam_AUCATCOD
   * mcteParam_AUUSOCOD
   * mcteParam_AUVTVDIA
   * mcteParam_AUVTVMES
   * mcteParam_AUVTVANN
   * mcteParam_AUKLMNUM
   * mcteParam_AUPAISCOD
   * mcteParam_AUPROVICOD
   * mcteParam_AUDOMCPACODPO
   * mcteParam_GUGARAGE
   * mcteParam_GUDOMICI
   */
  static final String mcteParam_AUCIASAN = "//AUCIASAN";
  static final String mcteParam_AUNUMSIN = "//AUNUMSIN";
  static final String mcteParam_AUNUMKMT = "//AUNUMKMT";
  /**
   * Const mcteParam_AUNUMKMT                As String = "//KMSRNGCOD"
   */
  static final String mcteParam_AUUSOGNC = "//AUUSOGNC";
  static final String mcteParam_AUANTANN = "//AUANTANN";
  /**
   * mcteParam_ESCERO
   * mcteParam_NROFACTU
   */
  static final String mcteParam_BANCOCOD = "//BANCOCOD";
  /**
   * mcteParam_DOCUMTIP
   * mcteParam_CLAIDENT
   */
  static final String mcteParam_CONCECOD = "//CONCECOD";
  static final String mcteParam_AGENTCOD = "//AGECOD";
  static final String mcteParam_AGENTCLA = "//AGECLA";
  static final String mcteParam_NUMCREDI = "//NUMCREDI";
  static final String mcteParam_FECCOPRE = "//FECCOPRE";
  static final String mcteParam_PLAZOMES = "//PLAZOMES";
  /**
   * "//PRECIOSO"
   */
  static final String mcteParam_PRECIOSO = "//PRECIO_MENSUAL";
  static final String mcteParam_VIENEDMS = "//VIENEDMS";
  /**
   * DA - 02/2007 (UIF): datos del apoderado (DERI)
   * Const mcteParam_APOD_CLIENSEC           As String = "//APOD/CLIENSEC" est� mas abajo
   */
  static final String mcteParam_APOD_DOCUMDAT = "//APOD/DOCUMDAT";
  static final String mcteParam_APOD_DOCUMTIP = "//APOD/DOCUMTIP";
  /**
   * Const mcteParam_APOD_DOMICSEC           As String = "//APOD/DOMICSEC" est� mas abajo
   */
  static final String mcteParam_APOD_CUENTSEC = "//APOD/CUENTSEC";
  static final String mcteParam_APOD_EMPRESEC = "//APOD/EMPRESEC";
  static final String mcteParam_APOD_DOCUMSEC = "//APOD/DOCUMSEC";
  static final String mcteParam_APOD_RELACION = "//APOD/RELACION";
  /**
   * DA: fin (DERI)
   */
  static final String mcteParam_SUCURCOD = "//SUCURSAL_CODIGO";
  static final String mcteParam_LEGAJO_VEND = "//LEGAJO_VEND";
  static final String mcteParam_LEGAJO_GTE = "//LEGAJO_GTE";
  static final String mcteParam_POLIZANNR = "//POLIZANNREFERIDO";
  static final String mcteParam_POLIZSECR = "//POLIZSECREFERIDO";
  static final String mcteParam_CERTIPOLR = "//CERTIPOLREFERIDO";
  static final String mcteParam_CERTIANNR = "//CERTIANNREFERIDO";
  static final String mcteParam_CERTISECR = "//CERTISECREFERIDO";
  static final String mcteParam_SUPLENUMR = "//SUPLENUMREFERIDO";
  /**
   * DA - 02/2007 (UIF): Datos del apoderado ingresados desde la p�gina (PERS)
   */
  static final String mcteParam_APOD_NUMEDOCU = "//APOD/NUMEDOCU";
  static final String mcteParam_APOD_TIPODOCU = "//APOD/TIPODOCU";
  static final String mcteParam_APOD_CLIENAP1 = "//APOD/CLIENAP1";
  static final String mcteParam_APOD_CLIENAP2 = "//APOD/CLIENAP2";
  static final String mcteParam_APOD_CLIENNOM = "//APOD/CLIENNOM";
  static final String mcteParam_APOD_NACIMANN = "//APOD/NACIMANN";
  static final String mcteParam_APOD_NACIMMES = "//APOD/NACIMMES";
  static final String mcteParam_APOD_NACIMDIA = "//APOD/NACIMDIA";
  static final String mcteParam_APOD_CLIENSEX = "//APOD/CLIENSEX";
  static final String mcteParam_APOD_CLIENEST = "//APOD/CLIENEST";
  /**
   * Const mcteParam_APOD_PAISSCOD           As String = "//APOD/PAISSCOD" est� mas abajo
   */
  static final String mcteParam_APOD_IDIOMCOD = "//APOD/IDIOMCOD";
  static final String mcteParam_APOD_EFECTANN = "//APOD/EFECTANN";
  static final String mcteParam_APOD_EFECTMES = "//APOD/EFECTMES";
  static final String mcteParam_APOD_EFECTDIA = "//APOD/EFECTDIA";
  static final String mcteParam_APOD_CLIENSEC = "//APOD/CLIENSEC";
  static final String mcteParam_APOD_CLIENTIP = "//APOD/CLIENTIP";
  static final String mcteParam_APOD_CLIENFUM = "//APOD/CLIENFUM";
  static final String mcteParam_APOD_CLIENDOZ = "//APOD/CLIENDOZ";
  static final String mcteParam_APOD_ABRIDTIP = "//APOD/ABRIDTIP";
  static final String mcteParam_APOD_ABRIDNUM = "//APOD/ABRIDNUM";
  static final String mcteParam_APOD_CLIENORG = "//APOD/CLIENORG";
  static final String mcteParam_APOD_CLIENIVA = "//APOD/CLIENIVA";
  static final String mcteParam_APOD_TARJECOD = "//APOD/COBROCOD";
  /**
   * DA - 02/2007 (UIF): Datos del apoderado ingresados desde la p�gina (DOMI)
   */
  static final String mcteParam_APOD_DOMICSEC = "//APOD/DOMICSEC";
  static final String mcteParam_APOD_DOMICCAL = "//APOD/DOMICCAL";
  static final String mcteParam_APOD_DOMICDOM = "//APOD/DOMICDOM";
  static final String mcteParam_APOD_DOMICDNU = "//APOD/DOMICDNU";
  static final String mcteParam_APOD_DOMICESC = "//APOD/DOMICESC";
  static final String mcteParam_APOD_DOMICPIS = "//APOD/DOMICPIS";
  static final String mcteParam_APOD_DOMICPTA = "//APOD/DOMICPTA";
  static final String mcteParam_APOD_DOMICPOB = "//APOD/DOMICPOB";
  static final String mcteParam_APOD_DOMICCPO = "//APOD/DOMICCPO";
  static final String mcteParam_APOD_PROVICOD = "//APOD/PROVICOD";
  static final String mcteParam_APOD_PAISSCOD = "//APOD/PAISSCOD";
  static final String mcteParam_APOD_CPACODPO = "//APOD/CPACODPO";
  /**
   * Const mcteParam_APOD_DOMICTLF           As String = "//APOD/DOMICTLF"
   */
  static final String mcteParam_APOD_TELCOD = "//APOD/TELCOD";
  static final String mcteParam_APOD_TELNRO = "//APOD/TELNRO";
  /**
   * DA - 02/2007 (UIF): datos del cliente y apoderado exigidos x la UIF
   */
  static final String mcteParam_APOD_PROFECOD = "//APOD/OCUPACOD";
  static final String mcteParam_APOD_UIFCUIT = "//APOD/UIFCUIT";
  static final String mcteParam_UIFCUIT = "//UIFCUIT";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParametros = null;
    int wvarMQError = 0;
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarArea = "";
    org.w3c.dom.Node wvarNodop = null;
    org.w3c.dom.Node wvarNodoAux = null;
    String wvarUsuario = "";
    Variant wvarPos = new Variant();
    String wvarParseString = "";
    int wvarStringLen = 0;
    String wvarEstado = "";
    String mvarINSPEOBLEA = "";
    org.w3c.dom.Node wobjNodoAuxiliar = null;
    int i = 0;
    //
    //
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new XmlDomExtended();
      wvarArea = "";
      wobjXMLRequest.loadXML( Request );
      wvarNodop = wobjXMLRequest.getDocument().getChildNodes().item( 0 );
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD )  ) + Strings.space( 10 ), 10 );
      //
      wvarStep = 20;
      //
      //Datos del Cliente
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_RAMOPCOD, 0, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_POLIZANN, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_POLIZSEC, 1, 6, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CERTIPOL, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CERTIANN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CERTISEC, 1, 6, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_Suplenum, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_TIPOOPERACION, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_OPCION, 0, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FUNCION, 0, 1, 0);

      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_NUMEDOCU, 0, 11, 0);
      //
      wvarStep = 22;
      //Invertimos los codigos de Libreta Civica y Libreta de Enrrolamiento de los datos del cliente
      if( VBFixesUtil.val(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_TIPODOCU)  ) ) == 2 )
      {
        XmlDomExtended.setText( XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_TIPODOCU ) , "3" );
      }
      else if( VBFixesUtil.val( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_TIPODOCU )  ) ) == 3 )
      {
        XmlDomExtended.setText( XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_TIPODOCU ) , "2" );
      }
      //
      //Invertimos tambien para los datos de los conductores
      for( int nwvarNodoAux = 0; nwvarNodoAux < XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_CONDUTDO) ).length(); nwvarNodoAux++ )
      {
    	  //NOTA: cambié (Element) a (Node) porque puede venir un TextNode entre otros
        wvarNodoAux = /*warning: cast*/ (Node) ( XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_CONDUTDO )).getChildNodes() .item( nwvarNodoAux );
        if( VBFixesUtil.val( XmlDomExtended.getText( wvarNodoAux ) ) == 2 )
        {
          XmlDomExtended.setText( wvarNodoAux, "3" );
        }
        else if( VBFixesUtil.val( XmlDomExtended.getText( wvarNodoAux ) ) == 3 )
        {
          XmlDomExtended.setText( wvarNodoAux, "2" );
        }
      }
      //
      //Inverto el acreedor prendario
      if( VBFixesUtil.val( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_TIPODOCU_ACRE )  ) ) == 2 )
      {
        XmlDomExtended.setText(XmlDomExtended.Node_selectSingleNode( wvarNodop, mcteParam_TIPODOCU_ACRE ) , "3" );
      }
      else if( VBFixesUtil.val( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_TIPODOCU_ACRE )  ) ) == 3 )
      {
        XmlDomExtended.setText( XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_TIPODOCU_ACRE ) , "2" );
      }
      //
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_TIPODOCU, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliCLIENAP1, 0, 20, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliCLIENAP2, 0, 20, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliCLIENNOM, 0, 40, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliNACIMANN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliNACIMMES, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliNACIMDIA, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_Sexo, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_ESTCIV, 0, 1, 0);
      //DA - 22/03/2007: todos los PAISSCOD deben ir en 00 (ARGENTINA) ya que no se captura desde el ASP
      if ((wobjXMLRequest.selectSingleNode(mcteParam_CliPAISSCOD) == null)||(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CliPAISSCOD)).equals(""))){
    	  wvarArea = wvarArea + "00";
      }else{
    	  wvarArea = wvarArea + GetDatoFormateado(wvarNodop, mcteParam_CliPAISSCOD, 0, 2,0);
      }
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliIDIOMCOD, 0, 3, 0);
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_CliEFECTANN, 1, 4)
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_CliEFECTMES, 1, 2)
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_CliEFECTDIA, 1, 2)
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FESOLANN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FESOLMES, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FESOLDIA, 1, 2, 0);

      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CLIENSEC, 1, 9, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliCLIENTIP, 0, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliCLIENFUM, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliCLIENDOZ, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliABRIDTIP, 0, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliABRIDNUM, 0, 10, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliCLIENORG, 0, 3, 0);
      //
      wvarStep = 25;
      //
      //Domicilio Cliente
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DOMICSEC, 0, 3, 0);
      //wvarArea = wvarArea & "001" '04/05/2007 - A pedido de Fito
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliDOMICCAL, 0, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliDOMICDOM, 0, 30, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliDOMICDNU, 0, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliDOMICESC, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliDOMICPIS, 0, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliDOMICPTA, 0, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliDOMICPOB, 0, 20, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliDOMICCPO, 1, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliDOMPROVICOD, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliDOMPAISSCOD, 0, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliDOMCPACODPO, 0, 8, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliDOMICTLF, 0, 30, 0);
      //
      wvarStep = 30;
      //
      //Cuenta Bancaria
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CUENTSEC, 0, 3, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_TIPOCUEN, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_BANCOCOD_CUENTA, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_SUCURCOD_CUENTA, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CUENTDC, 0, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CUENNUME, 0, 16, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_TARJECOD, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_VENCIANN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_VENCIMES, 1, 2, 0);
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_VENCIDIA, 1, 2)
      //
      wvarStep = 35;
      //
      //Impuestos Vector de 10
      wvarArea = wvarArea +GetVectorDatos(new XmlDomExtended(wvarNodop), mcteParam_IMPUESTO, mcteParam_IMPUECOD + ",0,3,0|" + mcteParam_IMPUEPAR + ",0,15,0|" + mcteParam_IMPUEDAT + ",0,60,0", 10);
      //
      wvarStep = 40;
      //
      //Vector 1208 de 10 posiciones
      wvarArea = wvarArea + GetVectorDatos(new XmlDomExtended(wvarNodop), mcteParam_NODOCLA, mcteParam_DOCUMTIP + ",1,2,0|" + mcteParam_CLAIDENT + ",0,30,0", 10);
      //
      wvarStep = 45;
      //
      //Situacion Ganancias Cliente
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CLIENIVA, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CLIESEEX, 2, 3, 2);
      //
      wvarStep = 47;
      if(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_CLIEIBTP ) ).length() != 0 )
      {
        //Si el tipo de ingresos brutos lo mandamos en cero, debemos blanquearlo sino puede ser que difiera de lo que se guarda en AIS
        if( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_CLIEIBTP)  ).equals( "0" ) )
        {
          XmlDomExtended.setText( XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_CLIEIBTP) , "" );
        }
      }

      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CLIEIBTP, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CLIEIBEX, 2, 3, 2);
      //
      wvarStep = 48;
      if( XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_CLIEIiTP)).length() != 0)
      {
        //Si el tipo de ingresos brutos lo mandamos en cero, debemos blanquearlo sino puede ser que difiera de lo que se guarda en AIS
        if( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_CLIEIiTP )  ).equals( "0" ) )
        {
          XmlDomExtended.setText( XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_CLIEIiTP) , "" );
        }
      }

      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CLIEIiTP, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CLIEIiEX, 2, 3, 2);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_NROIIBB, 0, 15, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CLIEGNTP, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DOCUMTIPIVA, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DOCUMDATCLI, 0, 11, 0);
      //
      wvarStep = 50;
      //Vector de 10 posiciones de medios de contacto
      wvarArea = wvarArea + GetVectorDatos(new XmlDomExtended(wvarNodop), mcteParam_NODOMEDIOCONTACTO,mcteParam_MEDCOSEC + ",1,3,0|" + mcteParam_MEDCOCOD + ",0,3,0|" + mcteParam_MEDCODAT + ",0,50,0|" + mcteParam_MEDCOSYS + ",0,3,0", 10);
      //
      wvarStep = 51;
      //
      //Profesion Cliente
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_PROFESEC, 0, 3, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_PROFECOD, 0, 6, 0);
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_TIPOCODI, 0, 1)
      //
      wvarStep = 52;
      //
      //Datos del vehiculo
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_VEHICSEC, 1, 9, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUMARCOD, 1, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUMODCOD, 1, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUSUBCOD, 1, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUADICOD, 1, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUMODORI, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_SIFMVEHI_DES, 0, 35, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUTIPCOD, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FABRICAN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FABRICMES, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_MOTORNUM, 0, 25, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CHASINUM, 0, 30, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_PATENNUM, 0, 10, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_VEHCLRCOD, 1, 3, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUCATCOD, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUUSOCOD, 1, 3, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUVTVCOD, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUVTVANN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUVTVMES, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUVTVDIA, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUKLMNUM, 1, 7, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUPAISCOD, 0, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUPROVICOD, 1, 2, 0);
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_CliDOMCPACODPO, 0, 8)
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUDOMCPACODPO, 0, 8, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_GUGARAGE, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_GUDOMICI, 0, 1, 0);
      //
      wvarStep = 53;
      //
      //Asegurados Adicionales ...son 10 iteraciones 'ojo se estan poniendo los hijos
      wvarArea = wvarArea + GetVectorDatos(new XmlDomExtended(wvarNodop), mcteParam_CONDUCTORES, mcteParam_CONDUSEC + ",1,2,0|" + mcteParam_CONDUTDO + ",1,2,0|" + mcteParam_DOCUMDAT + ",0,11,0|" + mcteParam_CONDUAPE + ",0,20,0|" + mcteParam_CONDUNOM + ",0,30,0|" + mcteParam_CONDUVIN + ",0,2,0|" + mcteParam_CONDUFEC + ",3,8,0|" + mcteParam_CONDUSEX + ",0,1,0|" + mcteParam_CONDUEST + ",0,1,0|" + mcteParam_CONDUEXC + ",0,1,0", 10);
      //
      wvarStep = 54;
      //
      //Accesorios ...son 10 iteraciones
      wvarArea = wvarArea + GetVectorDatos(new XmlDomExtended(wvarNodop), mcteParam_ACCESORIOS, mcteParam_VEACCSEC + ",1,2,0|" + mcteParam_AUACCCOD + ",1,4,0|" + mcteParam_AUVEASUM + ",2,12,2|" + mcteParam_AUVEADES + ",0,30,0|" + mcteParam_AUVEADEP + ",0,1,0", 10);
      //
      wvarStep = 55;
      //
      //Mas datos del cliente
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CLIENSEC, 1, 9, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CUENTSEC, 0, 3, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DOMICSEC, 0, 3, 0);
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_COBROFOR, 1, 4)
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_COBROCOD, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CLIENIVA, 0, 1, 0);
      //
      wvarStep = 56;
      //
      //Campa�as vector de 10
      wvarArea = wvarArea + GetVectorDatos(new XmlDomExtended(wvarNodop), "//*", mcteParam_CAMP_CODIGO + ",0,4,0", 10); //TODO CONVERT Convertí la expresión de // a //*, porque no es Xpath sino XMLPattern! Ver site
      //
      wvarStep = 57;
      //
      if( wobjXMLRequest.selectSingleNode( mcteParam_COBERTURAS )  == (org.w3c.dom.Node) null )
      {
        //Vuelvo a Cotizar y agrego las coberturas devueltas al XML Request
        if( ! (AddCoberturas(wobjXMLRequest)) )
        {
          Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='No se pudo realizar la cotización correspondiente' /></Response>" );
          return IAction_Execute;
        }
        wvarNodoAux = wobjXMLRequest.getDocument().createElement( "COBERTURASCONSULTADAS" );
        XmlDomExtended.setText( wvarNodoAux, DateTime.format( DateTime.now() ) );
        wobjXMLRequest.selectSingleNode( "//Request" ).appendChild( wvarNodoAux );
        wvarNodoAux = (org.w3c.dom.Element) null;
      }

      //LOS IMPORTES DE LAS COBERTURAS DEVUELTAS POR LA COTIZACION SON INCORRECTOS. SE DEBEN MULTIPLICAR POR 100
      //TODAS LAS COBERTURAS CUYOS CODIGOS NO SEAN NI 1 NI 2
      //AjustarCoberturas wobjXMLRequest

      
	  //Todos los request se loguean a nivel de CMS ( en fewebservices ). No logueo explícitamente en un archivo
//      if( ! (wobjXMLRequest.selectSingleNode( "//GENERARLOG" )  == (org.w3c.dom.Node) null) )
//      {
//        wobjXMLRequest.save( System.getProperty("user.dir") + "\\" + mcteClassName + ".xml" );
//      }

      //Coberturas, vector de 10
      //wvarArea = wvarArea & GetVectorDatos(wvarNodop, mcteParam_COBERTURAS, mcteParam_COBERCOD & ",1,3,0|" & mcteParam_COBERORD & ",1,2,0|" & mcteParam_CAPITASG & ",2,13,2|" & mcteParam_CAPITIMP & ",2,13,2", 10) 'FJO - 2008-05-12
      //2008-05-12 - Se cambia la cantidad de ocurrencias del vector, de 10 a 20. - FJO
      //FJO - 2008-05-12
      wvarArea = wvarArea + GetVectorDatos(new XmlDomExtended(wvarNodop), mcteParam_COBERTURAS, mcteParam_COBERCOD + ",1,3,0|" + mcteParam_COBERORD + ",1,2,0|" + mcteParam_CAPITASG + ",2,13,2|" + mcteParam_CAPITIMP + ",2,13,2", 20);
      //
      wvarStep = 58;
      //
      //Datos de la Poliza
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_EXPEDNUM, 0, 10, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CENTROCOD, 0, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_POLEFECTANN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_POLEFECTMES, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_POLEFECTDIA, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_POLVENCIANN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_POLVENCIMES, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_POLVENCIDIA, 1, 2, 0);
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_COBROCOD, 1, 1)
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_COBROFOR, 1, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_COASETIP, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_EXESEPOR, 2, 3, 2);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FESOLANN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FESOLMES, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FESOLDIA, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FEINGANN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FEINGMES, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FEINGDIA, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_RECARPOR, 2, 3, 2);
      //ESTE ES SI 998
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_PLANCOD, 1, 3, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_SWACREPR, 1, 1, 0);
      //
      wvarStep = 59;
      //
      // Acreedor Prendario
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_TIPODOCU_ACRE, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_NUMEDOCU_ACRE, 0, 11, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APELLIDO_ACRE, 0, 20, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APELLIDO_ACRE2, 0, 20, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_NOMBRES, 0, 30, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CLIENTIP_ACRE, 0, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DOMICCAL, 0, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DOMICDOM, 0, 40, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DOMICDNU, 0, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DOMICESC, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DOMICPIS, 0, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DOMICPTA, 0, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DOMICPOB, 0, 40, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_PROVICOD, 1, 2, 0);
      //
      //DA - 22/03/2007: todos los PAISSCOD deben ir en 00 (ARGENTINA) ya que no se captura desde el ASP
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_PAISSCOD, 0, 2)
      wvarArea = wvarArea + "00";
      //
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DOMICPRE, 0, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DOMICTLF, 0, 30, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CPACODPO, 0, 8, 0);
      //
      wvarStep = 60;
      //
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FRANQCOD, 0, 2, 0);
      //ESTE 5
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_PLANPILICOD, 1, 3, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_PRECIOFT, 2, 9, 3);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_ZONA, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CliDOMPROVICOD, 1, 2, 0);
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_CODPROV, 1, 2)
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_SUMALBA, 2, 9, 2)
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_SUMASEG, 2, 9, 2)
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_SUMALBA, 2, 7, 4);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_SUMASEG, 2, 7, 4);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CLUB_LBA, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CTAKMS, 1, 6, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_ESCERO, 0, 1, 0);
      //
      wvarStep = 61;
      //
      // Inspeccion
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_EFECTANN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_EFECTMES, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_EFECTDIA, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_ESTAINSP, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_INSPECOD, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_INSPEDES, 0, 30, 0);
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_CENSICOD, 0, 4)
      //Ale Se cambio porque el Centro ' 222' tenia problemas
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CENSICOD, 4, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CENSIDES, 0, 30, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_INSPEKMS, 1, 8, 0);

      //La oblea se guarda como parte de la descripci�n de la inspecci�n.
      // S�lo se guardan los primeros 50 caracteres.
      mvarINSPEOBLEA = Strings.trim(GetDatoFormateado(wvarNodop, mcteParam_INSPEOBLEA, 0, 50, 0) );
      if( Strings.len( mvarINSPEOBLEA ) > 0 )
      {
        wvarArea = wvarArea + Strings.left( "Nro Oblea: " + mvarINSPEOBLEA + ". " + GetDatoFormateado(wvarNodop, mcteParam_INSPEOBS, 0, 50, 0), 50 );
      }
      else
      {
        wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_INSPEOBS, 0, 50, 0);
      }
      //
      wvarStep = 62;
      //
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_INSPADOM, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_INS_DOMICDOM, 0, 40, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_INS_CPACODPO, 0, 8, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_INS_DOMICPOB, 0, 40, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_INS_PROVICOD, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_INS_DOMICTLF, 0, 30, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_INS_RANGOHOR, 1, 8, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DISPOCOD, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_PRESTCOD, 0, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_ESCERO, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_NROFACTU, 0, 20, 0);
      //
      wvarStep = 63;
      //
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DDJJ, 0, 130, 0);
      //
      wvarStep = 64;
      //
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUMARCOD, 1, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUMODCOD, 1, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUSUBCOD, 1, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUADICOD, 1, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUMODORI, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_SIFMVEHI_DES, 0, 35, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUTIPCOD, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FABRICAN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FABRICMES, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_MOTORNUM, 0, 25, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CHASINUM, 0, 30, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_PATENNUM, 0, 10, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_VEHCLRCOD, 1, 3, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUCATCOD, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUUSOCOD, 1, 3, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUVTVDIA, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUVTVMES, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUVTVANN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUKLMNUM, 1, 7, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUPAISCOD, 0, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUPROVICOD, 1, 2, 0);
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_CliDOMCPACODPO, 0, 8)
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUDOMCPACODPO, 0, 8, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_GUGARAGE, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_GUDOMICI, 0, 1, 0);
      //
      wvarStep = 65;
      //
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUCIASAN, 0, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUNUMSIN, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUNUMKMT, 1, 6, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUUSOGNC, 0, 1, 0);
      //ANTIGUEDAD DEL REGISTRO
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AUANTANN, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_ESCERO, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_NROFACTU, 0, 20, 0);
      //
      wvarStep = 66;
      //
      //MANDAR CERO
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_BANCOCOD, 1, 4, 0);
      //BLANCO
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_DOCUMTIP, 0, 2, 0);
      //BLACNO
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CLAIDENT, 0, 30, 0);
      //
      wvarStep = 67;
      //
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CONCECOD, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AGENTCOD, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_AGENTCLA, 0, 2, 0);
      //
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_NUMCREDI, 0, 16, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_FECCOPRE, 3, 8, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_PLAZOMES, 1, 3, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_PRECIOSO, 2, 9, 2);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_VIENEDMS, 0, 1, 0);
      //
      //DA - 02/2007 (UIF): (DERI) Datos del apoderado
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_CLIENSEC, 1, 9, 0);
      //es el DOCUMDAT de la linkage
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_NUMEDOCU, 0, 11, 0);
      //es el DOCUMTIP de la linkage
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_TIPODOCU, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_DOMICSEC, 0, 3, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_CUENTSEC, 0, 3, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_EMPRESEC, 1, 3, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_DOCUMSEC, 1, 2, 0);
      //27/04/2007 - APOD_RELACION: El Back end nos dice que este campo tiene que ir en AP para Apoderado y ES para conyuge
      if(GetDatoFormateado(wvarNodop, mcteParam_APOD_CLIENSEC, 1, 9, 0).equals( "000000000" ) )
      {
        wvarArea = wvarArea + "  ";
      }
      else
      {
        wvarArea = wvarArea + "AP";
      }
      //FIN: (DERI) Datos del apoderado
      //
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_BANCOCOD, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_SUCURCOD, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_LEGAJO_VEND, 1, 10, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_LEGAJO_GTE, 1, 10, 0);
      //
      wvarStep = 68;
      //
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_POLIZANNR, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_POLIZSECR, 1, 6, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CERTIPOLR, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CERTIANNR, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CERTISECR, 1, 6, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_SUPLENUMR, 1, 4, 0);
      //
      //DA - 02/2007 (UIF): datos del apoderado (PERS)
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_NUMEDOCU, 0, 11, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_TIPODOCU, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_CLIENAP1, 0, 20, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_CLIENAP2, 0, 20, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_CLIENNOM, 0, 40, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_NACIMANN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_NACIMMES, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_NACIMDIA, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_CLIENSEX, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_CLIENEST, 0, 1, 0);
      //
      //DA - 22/03/2007: todos los PAISSCOD deben ir en 00 (ARGENTINA) ya que no se captura desde el ASP
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_APOD_PAISSCOD, 0, 2)
      wvarArea = wvarArea + "00";
      //
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_IDIOMCOD, 0, 3, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_EFECTANN, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_EFECTMES, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_EFECTDIA, 1, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_CLIENSEC, 1, 9, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_CLIENTIP, 0, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_CLIENFUM, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_CLIENDOZ, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_ABRIDTIP, 0, 2, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_ABRIDNUM, 0, 10, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_CLIENORG, 0, 3, 0);
      //
      //DA - 02/2007 (UIF): datos del apoderado (DOMI)
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_DOMICSEC, 0, 3, 0);
      //wvarArea = wvarArea & "001" '04/05/2007 - A pedido de Fito
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_DOMICCAL, 0, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_DOMICDOM, 0, 30, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_DOMICDNU, 0, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_DOMICESC, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_DOMICPIS, 0, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_DOMICPTA, 0, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_DOMICPOB, 0, 20, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_DOMICCPO, 1, 5, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_PROVICOD, 1, 2, 0);
      //
      //DA - 22/03/2007: todos los PAISSCOD deben ir en 00 (ARGENTINA) ya que no se captura desde el ASP
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_APOD_PAISSCOD, 0, 2)
      wvarArea = wvarArea + "00";
      //
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_APOD_CPACODPO, 0, 8)
      //04/05/2007 - A pedido de Fito
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_DOMICCPO, 0, 8, 0);

      //Antes no llegaba el telefono del apod.
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParam_APOD_DOMICTLF, 0, 30)
      wobjNodoAuxiliar = XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_APOD_TELCOD );
      if( ! (wobjNodoAuxiliar == (org.w3c.dom.Node) null) )
      {
        wobjNodoAuxiliar = XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_APOD_TELNRO) ;
        if( ! (wobjNodoAuxiliar == (org.w3c.dom.Node) null) )
        {
          wvarArea = wvarArea + Strings.left( XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_APOD_TELCOD )  ) + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wvarNodop, mcteParam_APOD_TELNRO )  ) + Strings.space( 30 ), 30 );
        }
        else
        {
          wvarArea = wvarArea + Strings.space( 30 );
        }
      }
      else
      {
        wvarArea = wvarArea + Strings.space( 30 );
      }

      wobjNodoAuxiliar = (org.w3c.dom.Node) null;

      //
      //DA - 02/2007 (UIF): datos del apoderado (GRFMCUEN y GRFMPEDF)
      //El 3 es el CLIENIVA del Apod
      wvarArea = wvarArea + "    00000000  " + Strings.space( 16 ) + "00" + "000000" + "3" + "00000 00000 00000" + Strings.space( 15 ) + " " + GetDatoFormateado(wvarNodop, mcteParam_APOD_TIPODOCU, 1, 2, 0) + GetDatoFormateado(wvarNodop, mcteParam_APOD_NUMEDOCU, 0, 11, 0);
      //
      //DA - 02/2007 (UIF): datos del apoderado (GRFMPEMC)
      // *08 LNK-GRFMPEMC      OCCURS 10 TIMES.********************************************
      // *[10 MEDCOSEC PIC 9(03)] & [10 MEDCOCOD PIC  X(03)] & [10 MEDCODAT PIC X(50)] & [10 MEDCOSYS PIC X(03)]
      wvarArea = wvarArea + "001TEP" + Strings.left( (GetDatoFormateado(wvarNodop, mcteParam_APOD_TELCOD, 0, 5, 0) + GetDatoFormateado(wvarNodop, mcteParam_APOD_TELNRO, 0, 30, 0 ) + Strings.space( 50 )), 50 ) + "TEP";
      // *[10 MEDCOSEC PIC 9(03)] & [10 MEDCOCOD PIC  X(03)] & [10 MEDCODAT PIC X(50)] & [10 MEDCOSYS PIC X(03)]
      wvarArea = wvarArea + "002EMA" + GetDatoFormateado(wvarNodop, "//APOD/EMAIL", 0, 50, 0 ) + "EMA";
      // *[10 MEDCOSEC PIC 9(03)] & [10 MEDCOCOD PIC  X(03)] & [10 MEDCODAT PIC X(50)] & [10 MEDCOSYS PIC X(03)]
      wvarArea = wvarArea + "003   " + Strings.space( 50 ) + "   ";
      // *[10 MEDCOSEC PIC 9(03)] & [10 MEDCOCOD PIC  X(03)] & [10 MEDCODAT PIC X(50)] & [10 MEDCOSYS PIC X(03)]
      wvarArea = wvarArea + "004   " + Strings.space( 50 ) + "   ";
      // *[10 MEDCOSEC PIC 9(03)] & [10 MEDCOCOD PIC  X(03)] & [10 MEDCODAT PIC X(50)] & [10 MEDCOSYS PIC X(03)]
      wvarArea = wvarArea + "005   " + Strings.space( 50 ) + "   ";
      // *[10 MEDCOSEC PIC 9(03)] & [10 MEDCOCOD PIC  X(03)] & [10 MEDCODAT PIC X(50)] & [10 MEDCOSYS PIC X(03)]
      wvarArea = wvarArea + "006   " + Strings.space( 50 ) + "   ";
      // *[10 MEDCOSEC PIC 9(03)] & [10 MEDCOCOD PIC  X(03)] & [10 MEDCODAT PIC X(50)] & [10 MEDCOSYS PIC X(03)]
      wvarArea = wvarArea + "007   " + Strings.space( 50 ) + "   ";
      // *[10 MEDCOSEC PIC 9(03)] & [10 MEDCOCOD PIC  X(03)] & [10 MEDCODAT PIC X(50)] & [10 MEDCOSYS PIC X(03)]
      wvarArea = wvarArea + "008   " + Strings.space( 50 ) + "   ";
      // *[10 MEDCOSEC PIC 9(03)] & [10 MEDCOCOD PIC  X(03)] & [10 MEDCODAT PIC X(50)] & [10 MEDCOSYS PIC X(03)]
      wvarArea = wvarArea + "009   " + Strings.space( 50 ) + "   ";
      // *[10 MEDCOSEC PIC 9(03)] & [10 MEDCOCOD PIC  X(03)] & [10 MEDCODAT PIC X(50)] & [10 MEDCOSYS PIC X(03)]
      wvarArea = wvarArea + "010   " + Strings.space( 50 ) + "   ";

      //For i = 1 To 10
      //    wvarArea = wvarArea & "000   " & Space(50) & "   "
      //Next i
      //
      // *08 LNK-GRFMPRCC.*****************************************************************
      // * 10 PROFESEC            PIC  X(03).
      wvarArea = wvarArea + Strings.space( 3 );
      // * 10 PROFECOD            PIC  X(06).
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_PROFECOD, 0, 6, 0);
      // *08 LNK-DOCCLIAPO.****************************************************************
      // * 10 LNK-CLIENTE.
      // *  12 TIPDOCCLI        PIC  9(02).
      wvarArea = wvarArea + "04";
      // *  12 NRODOCCLI        PIC  X(11).
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_UIFCUIT, 0, 11, 0);
      // * 10 LNK-APODERA.
      // *  12 TIPDOCAPO        PIC  9(02).
      wvarArea = wvarArea + "04";
      // *  12 NRODOCAPO        PIC  X(11).
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_APOD_UIFCUIT, 0, 11, 0);
      // **********************************************************************************
      //
      //
      wobjXMLRequest = null;
      wvarNodop = (org.w3c.dom.Node) null;
      wvarNodoAux = (org.w3c.dom.Element) null;
      //
      wvarStep = 70;

      //
      //Levanto los parámetros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      if( wobjXMLParametros.getDocument().getChildNodes().getLength() > 0 )
      {
        wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      }
      //
      wvarStep = 110;
      wobjXMLParametros = null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarArea;

      wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);
      StringHolder strParseStringHolder = new StringHolder(wvarParseString);
      wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
      wvarParseString = strParseStringHolder.getValue();
      
      java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());
      logger.log(java.util.logging.Level.FINE, "wvarArea: " + wvarArea);
      logger.log(java.util.logging.Level.FINE, "wvarParseString: " + wvarParseString);
      
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + wvarParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 240;
      wvarResult = "";
      //cantidad de caracteres ocupados por parámetros de entrada
      wvarPos.set( 102 );
      //
      wvarStringLen = Strings.len( wvarParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( wvarParseString, 19, 2 );
      logger.log(java.util.logging.Level.FINE, "wvarEstado: " + wvarEstado);
      
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        //wvarPos = 5138 Antes de AML
        //wvarPos = 6146 'FJO - 2008-05-14 - Cambio del vector de coberturas de 10 a 20 ocurrencias.
        //FJO - 2008-05-14 - Cambio del vector de coberturas de 10 a 20 ocurrencias.
        wvarPos.set( 6146 + 350 );
        wvarResult = ParseoMensaje(wvarPos, wvarParseString, wvarStringLen, wvarEstado);
        Response.set( "<Response><Estado resultado='true' mensaje=''/>" + wvarResult + "</Response>" );
      }
      //
      wvarStep = 280;
      if( !wvarEstado.equals( "OK" ) )
      {
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= Error en Grabacion AIS: " + Response, vbLogEventTypeError );
      }
      //
      wvarStep = 320;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());
      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
      try 
       {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        if( Err.getError().getNumber() != 0 )
        {
          //unsup Resume ClearObjects
        }
        else
        {
          return IAction_Execute;
        }
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant pvarPos, String pvarParseString, int pvarStringLen, String pvarEstado ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCantLin = 0;
    int wvarCounter = 0;
    //
    wvarResult = wvarResult + "<RESULTADO>";
    wvarResult = wvarResult + "<SOLICITUDGRABADA>";
    wvarResult = wvarResult + ((pvarEstado.equals( "E1" )) ? "NO" : "SI");
    wvarResult = wvarResult + "</SOLICITUDGRABADA>";

    wvarResult = wvarResult + "<SOLICITUDOK>";
    wvarResult = wvarResult + ((pvarEstado.equals( "OK" )) ? "SI" : "NO");
    wvarResult = wvarResult + "</SOLICITUDOK>";
    //
    wvarResult = wvarResult + "<MENSAJE>" + Strings.trim( ModGeneral.MidAsString( pvarParseString, pvarPos, 60 ) ) + "</MENSAJE>";
    wvarResult = wvarResult + "<RAMOPCOD>" + ModGeneral.MidAsString( pvarParseString, pvarPos, 4 ) + "</RAMOPCOD>";
    wvarResult = wvarResult + "<POLIZANN>" + ModGeneral.MidAsString( pvarParseString, pvarPos, 2 ) + "</POLIZANN>";
    wvarResult = wvarResult + "<POLIZSEC>" + ModGeneral.MidAsString( pvarParseString, pvarPos, 6 ) + "</POLIZSEC>";
    wvarResult = wvarResult + "<CERTIPOL>" + ModGeneral.MidAsString( pvarParseString, pvarPos, 4 ) + "</CERTIPOL>";
    wvarResult = wvarResult + "<CERTIANN>" + ModGeneral.MidAsString( pvarParseString, pvarPos, 4 ) + "</CERTIANN>";
    wvarResult = wvarResult + "<CERTISEC>" + ModGeneral.MidAsString( pvarParseString, pvarPos, 6 ) + "</CERTISEC>";
    wvarResult = wvarResult + "<SUPLENUM>" + ModGeneral.MidAsString( pvarParseString, pvarPos, 4 ) + "</SUPLENUM>";
    wvarCantLin = (int)Math.rint( VBFixesUtil.val( ModGeneral.MidAsString( pvarParseString, pvarPos, 2 ) ) );
    wvarResult = wvarResult + "<TEXTOS>";
    if( wvarCantLin == 0 )
    {
      wvarResult = wvarResult + "<TEXTO>" + Strings.trim( ModGeneral.MidAsString( pvarParseString, pvarPos, 1000 ) ) + "</TEXTO>";
    }
    for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )
    {
      wvarResult = wvarResult + "<TEXTO>" + Strings.trim( ModGeneral.MidAsString( pvarParseString, pvarPos, 60 ) ) + "</TEXTO>";
    }
    //
    wvarResult = wvarResult + "</TEXTOS>";
    wvarResult = wvarResult + "</RESULTADO>";
    //
    ParseoMensaje = wvarResult;
    return ParseoMensaje;
  }

  private VbCollection GetColFromString( String pvarString ) throws Exception
  {
    VbCollection GetColFromString = null;
    int wvarPos = 0;
    int wVarposAnt = 0;
    GetColFromString = new VbCollection();
    wvarPos = 1;
    wVarposAnt = 1;
    while( Strings.find( wvarPos, pvarString, "|" ) != 0 )
    {
      wvarPos = Strings.find( wvarPos, pvarString, "|" );
      GetColFromString.add( new Variant( Strings.mid( pvarString, wVarposAnt, wvarPos - wVarposAnt ) ) );
      wVarposAnt = wvarPos + 1;
      wvarPos = wvarPos + 1;
    }
    if( !Strings.mid( pvarString, wVarposAnt ).equals( "" ) )
    {
      GetColFromString.add( new Variant( Strings.mid( pvarString, wVarposAnt ) ) );
    }
    return GetColFromString;
  }

  private String GetVectorDatos( XmlDomExtended pobjNodo, String pVarPathVector, String pVarDefinicionSubNodos, int pVarCantElementos ) throws Exception
  {
    String GetVectorDatos = "";
    int wvarCounterRelleno = 0;
    int wvarActualCounter = 0;
    int wvarActualNodoSolicitado = 0;
    Variant wvarNodoNombre = new Variant();
    Variant wvarNodoTipoDato = new Variant();
    Variant wvarNodoLargoDato = new Variant();
    Variant wvarNodoCantDecimales = new Variant();
    VbCollection wobjColNodosSolicitados = null;
    org.w3c.dom.NodeList wvarSelectedNodos = null;

    wobjColNodosSolicitados = GetColFromString(pVarDefinicionSubNodos);
    wvarActualCounter = 1;
    if( ! (pobjNodo == (org.w3c.dom.Node) null) )
    {
      wvarSelectedNodos = pobjNodo.selectNodes( pVarPathVector ) ;

      for( wvarActualCounter = 0; wvarActualCounter <= (wvarSelectedNodos.getLength() - 1); wvarActualCounter++ )
      {
        //Recorro todos los nodos que formaran el vector
        for( wvarActualNodoSolicitado = 1; wvarActualNodoSolicitado <= wobjColNodosSolicitados.getCount(); wvarActualNodoSolicitado++ )
        {
          //Recorro todos los nodos que se solicitan para cada uno
         GetDefinicionElemento(wobjColNodosSolicitados.getItem(wvarActualNodoSolicitado).toString(), wvarNodoNombre, wvarNodoTipoDato, wvarNodoLargoDato, wvarNodoCantDecimales);
          GetVectorDatos = GetVectorDatos + GetDatoFormateado (wvarSelectedNodos.item( wvarActualCounter ), wvarNodoNombre.toString(), wvarNodoTipoDato.toInt(), wvarNodoLargoDato.toInt(), wvarNodoCantDecimales.toInt() );
        }
        if( wvarActualCounter == (pVarCantElementos - 1) )
        {
          return GetVectorDatos;
        }
      }
    }
    for( wvarCounterRelleno = wvarActualCounter; wvarCounterRelleno <= (pVarCantElementos - 1); wvarCounterRelleno++ )
    {
      for( wvarActualNodoSolicitado = 1; wvarActualNodoSolicitado <= wobjColNodosSolicitados.getCount(); wvarActualNodoSolicitado++ )
      {
        //Recorro todos los nodos que se solicitan para cada uno
       GetDefinicionElemento(wobjColNodosSolicitados.getItem(wvarActualNodoSolicitado).toString(), wvarNodoNombre, wvarNodoTipoDato, wvarNodoLargoDato, wvarNodoCantDecimales);
        GetVectorDatos = GetVectorDatos + GetDatoFormateado(null, wvarNodoNombre.toString(), wvarNodoTipoDato.toInt(), wvarNodoLargoDato.toInt(), wvarNodoCantDecimales.toInt());
      }
    }
    ClearObjects: 
    wobjColNodosSolicitados = (VbCollection) null;
    return GetVectorDatos;
  }

  private String GetDatoFormateado( org.w3c.dom.Node pobjXMLContenedor, String pvarPathDato, int pvarTipoDato, int pvarLongitud, int pvarDecimales ) throws Exception
  {
    String GetDatoFormateado = "";
    org.w3c.dom.Node wobjNodoValor = null;
    String wvarDatoValue = "";
    int wvarCounter = 0;
    double wvarCampoNumerico = 0.0;

    if( ! (pobjXMLContenedor == (org.w3c.dom.Node) null) )
    {
      wobjNodoValor = XmlDomExtended.Node_selectSingleNode(pobjXMLContenedor, "./" + Strings.mid( pvarPathDato, 3 ) ) ;
      if( ! (wobjNodoValor == (org.w3c.dom.Node) null) )
      {
        wvarDatoValue = XmlDomExtended.getText( wobjNodoValor );
      }
    }

    
    if( pvarTipoDato == 0 )
    {
      //Dato del Tipo String
      GetDatoFormateado = Strings.left( wvarDatoValue + Strings.space( pvarLongitud ), pvarLongitud );
    }
    else if( pvarTipoDato == 1 )
    {
      //Dato del Tipo Entero
      GetDatoFormateado = CompleteZero(wvarDatoValue, pvarLongitud);
    }
    else if( pvarTipoDato == 2 )
    {
      //Dato del Tipo "Con Decimales"
      if( new Variant( wvarDatoValue ).isNumeric() )
      {
        wvarCampoNumerico = Obj.toDouble( wvarDatoValue );
      }
      for( wvarCounter = 1; wvarCounter <= pvarDecimales; wvarCounter++ )
      {
        wvarCampoNumerico = wvarCampoNumerico * 10;
      }
  	//Implementar el conversor de este parche. El original genera ".0" para los enteros
      // El problema está en que String.valueOf(double) si es un entero me manda .0 al final...
      // Además puede ser un double 3.0E7, y el String.valueOf lo genera así.. Lo cambio para que use NumberFormat
	  // Linea original:
//      GetDatoFormateado = CompleteZero(String.valueOf( wvarCampoNumerico), pvarLongitud + pvarDecimales );
      //Parche:
		//El numero se formatea SIN separador decimal.
		DecimalFormat formatter = new DecimalFormat("#");
		String doubleString = formatter.format(wvarCampoNumerico);
      GetDatoFormateado = CompleteZero(doubleString, pvarLongitud + pvarDecimales );
      //Fin parche
      //GetDatoFormateado = Left(GetDatoFormateado, pvarLongitud) & "" & Right(GetDatoFormateado, pvarDecimales)
    }
    else if( pvarTipoDato == 3 )
    {
      //Dato del Tipo "Fecha" Debe venir del tipo dd/mm/yyyy devuelve YYYYMMDD
      if( Strings.len( wvarDatoValue ) == 10 )
      {
        GetDatoFormateado = Strings.right( wvarDatoValue, 4 ) + Strings.mid( wvarDatoValue, 4, 2 ) + Strings.left( wvarDatoValue, 2 );
      }
      else
      {
        GetDatoFormateado = "00000000";
      }
    }
    else if( pvarTipoDato == 4 )
    {
      //Dato del Tipo String alineado a la izquierda
      GetDatoFormateado = Strings.right( Strings.space( pvarLongitud ) + wvarDatoValue, pvarLongitud );
    }

    return GetDatoFormateado;
  }

  private String CompleteZero( String pvarString, int pvarLongitud ) throws Exception
  {
    String CompleteZero = "";
    int wvarCounter = 0;
    String wvarstrTemp = "";
    for( wvarCounter = 1; wvarCounter <= pvarLongitud; wvarCounter++ )
    {
      wvarstrTemp = wvarstrTemp + "0";
    }
    CompleteZero = Strings.right( wvarstrTemp + pvarString, pvarLongitud );
    return CompleteZero;
  }

  private void GetDefinicionElemento( String pvarStringElemento, Variant pvarElemento, Variant pvarTipoDato, Variant pvarLongitud, Variant pvarDecimales ) throws Exception
  {
    String[] wvarArray = null;
    wvarArray = Strings.split( pvarStringElemento, ",", -1 );
    pvarElemento.set( wvarArray[0] );
    pvarTipoDato.set( wvarArray[1] );
    pvarLongitud.set( wvarArray[2] );
    pvarDecimales.set( wvarArray[3] );
  }

  private boolean AddCoberturas(XmlDomExtended pobjXMLRequest ) throws Exception
  {
    boolean AddCoberturas = false;
    lbaw_OVGetCotiAUS wobjGetCotiAUS = null;
    XmlDomExtended wobjXMLCoberturas = null;
    org.w3c.dom.Element wobjNodoCoberturas = null;
    org.w3c.dom.Element wobjNodoCobertura = null;
    org.w3c.dom.Element wobjCOBERCOD = null;
    org.w3c.dom.Element wobjCOBERORD = null;
    org.w3c.dom.Element wobjCAPITASG = null;
    org.w3c.dom.Element wobjCAPITIMP = null;
    int wvarSumaAdicional = 0;
    String wvarResponse = "";
    int wvarCounter = 0;
    wobjGetCotiAUS = new lbaw_OVGetCotiAUS();
    StringHolder strHoldGetCotiAUS = new StringHolder(wvarResponse);
    //NOTA: Ver esto: antes estaba así: wobjClass.execute(pobjXMLRequest.xml, wvarResponse, "");
    wobjGetCotiAUS.IAction_Execute(pobjXMLRequest.toString(), strHoldGetCotiAUS, "");
    wvarResponse = strHoldGetCotiAUS.getValue();
    wobjGetCotiAUS = null;

    wobjXMLCoberturas = new XmlDomExtended();
    wobjXMLCoberturas.loadXML( wvarResponse );
    if( pobjXMLRequest.selectSingleNode( "//HIJOS/HIJO[INCLUIDO='S']" )  == (org.w3c.dom.Node) null )
    {
      wvarSumaAdicional = 0;
    }
    else
    {
      //Si la solicitud incluye algun hijo, entonces tomo las segundas 10 coberturas
      wvarSumaAdicional = 0;
    }

    if( XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
    {
      AddCoberturas = false;
      return AddCoberturas;
    }
    else
    {
      wobjNodoCoberturas = (org.w3c.dom.Element) pobjXMLRequest.selectSingleNode( "//COBERTURAS" ) ;

      if( wobjNodoCoberturas == (org.w3c.dom.Element) null )
      {
        wobjNodoCoberturas = pobjXMLRequest.getDocument().createElement( "COBERTURAS" );
        pobjXMLRequest.getDocument().getChildNodes().item( 0 ).appendChild( wobjNodoCoberturas );
      }

      //10 + wvarSumaAdicional
      for( wvarCounter = 1 + wvarSumaAdicional; wvarCounter <= 20; wvarCounter++ )
      {
        //If Val(Strings.replace(wobjXMLCoberturas.selectSingleNode(mcteParam_CAPITIMP & wvarCounter).Text, ",", ".")) <> 0 Then
        wobjNodoCobertura = pobjXMLRequest.getDocument().createElement( "COBERTURA" );
        wobjNodoCoberturas.appendChild( wobjNodoCobertura );
        //
        wobjCOBERCOD = pobjXMLRequest.getDocument().createElement( Strings.mid( mcteParam_COBERCOD, 3 ) );
        wobjNodoCobertura.appendChild( wobjCOBERCOD );
        XmlDomExtended.setText( wobjCOBERCOD, XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_COBERCOD + wvarCounter )  ) );
        //
        wobjCOBERORD = pobjXMLRequest.getDocument().createElement( Strings.mid( mcteParam_COBERORD, 3 ) );
        wobjNodoCobertura.appendChild( wobjCOBERORD );
        XmlDomExtended.setText( wobjCOBERORD, XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_COBERORD + wvarCounter )  ) );
        //
        wobjCAPITASG = pobjXMLRequest.getDocument().createElement( Strings.mid( mcteParam_CAPITASG, 3 ) );
        wobjNodoCobertura.appendChild( wobjCAPITASG );
        XmlDomExtended.setText( wobjCAPITASG, Strings.replace( XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_CAPITASG + wvarCounter )  ), ",", "." ) );
        //
        wobjCAPITIMP = pobjXMLRequest.getDocument().createElement( Strings.mid( mcteParam_CAPITIMP, 3 ) );
        wobjNodoCobertura.appendChild( wobjCAPITIMP );
        XmlDomExtended.setText( wobjCAPITIMP, Strings.replace( XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_CAPITIMP + wvarCounter )  ), ",", "." ) );
        //End If
      }
      AddCoberturas = true;
    }

    ClearObjects: 
    wobjCOBERCOD = (org.w3c.dom.Element) null;
    wobjCOBERORD = (org.w3c.dom.Element) null;
    wobjCAPITASG = (org.w3c.dom.Element) null;
    wobjCAPITIMP = (org.w3c.dom.Element) null;
    wobjXMLCoberturas = null;
    wobjNodoCoberturas = (org.w3c.dom.Element) null;
    wobjNodoCobertura = (org.w3c.dom.Element) null;
    return AddCoberturas;
  }

  private boolean AjustarCoberturas(XmlDomExtended pobjXMLRequest ) throws Exception
  {
	  boolean AjustarCoberturas = false;
	  NodeList wobjNodoCoberturas = null;
	  Node wobjNodoCobertura;
	  double wvarSumaImporte = 0.0;
	  int wvarCodigoCobertura = 0;

	  wobjNodoCoberturas = pobjXMLRequest.selectNodes( "//COBERTURA" ) ;
	  for( int nwobjNodoCobertura = 0; nwobjNodoCobertura < wobjNodoCoberturas.getLength(); nwobjNodoCobertura++ )
	  {
		  wobjNodoCobertura = wobjNodoCoberturas.item(nwobjNodoCobertura);
		  wvarCodigoCobertura = (int)Math.rint( VBFixesUtil.val( XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodoCobertura, "." + Strings.mid( mcteParam_COBERCOD, 2 ) )  ) ) );
		  if( ! ((wvarCodigoCobertura == 1) || (wvarCodigoCobertura == 2)) )
		  {
			  wvarSumaImporte = 100 * Obj.toDouble( XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodoCobertura, "." + Strings.mid( mcteParam_CAPITIMP, 2 ))) );
			  XmlDomExtended.setText(XmlDomExtended.Node_selectSingleNode(wobjNodoCobertura, "." + Strings.mid( mcteParam_CAPITIMP, 2 ) ) , String.valueOf( wvarSumaImporte ) );
		  }
	  }

	  ClearObjects: 
		  wobjNodoCoberturas = null;
	  wobjNodoCobertura = (org.w3c.dom.Element) null;
	  return AjustarCoberturas;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
