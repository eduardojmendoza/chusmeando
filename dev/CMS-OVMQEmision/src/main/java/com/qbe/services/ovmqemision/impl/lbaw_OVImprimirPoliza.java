package com.qbe.services.ovmqemision.impl;

import java.util.logging.Logger;

import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.ovmqemision.Constants;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import com.qbe.vbcompat.format.VBFixesUtil;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVImprimirPoliza implements VBObjectClass
{

	protected static Logger logger = Logger.getLogger(lbaw_OVImprimirPoliza.class.getName());

	static final String mcteClassName = "lbawA_OVMQEmision.lbaw_OVImprimirPoliza";

  static final String mcteOpID = "1701";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARCOD = "//USUARIO";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_OPERAPOL = "//OPERAPOL";
  static final String mcteParam_TIPODOCU = "//TIPODOCU";
  static final String mcteParam_TIPOIMPR = "//TIPOIMPR";

  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarRamo = "";
    String wvarPolizaAnn = "";
    String wvarPolizaSec = "";
    String wvarCertiPol = "";
    String wvarCertiAnn = "";
    String wvarCertiSec = "";
    String wvarOperaPol = "";
    String wvarTipoDocu = "";
    String wvarTipoImpre = "";
    Variant wvarPos = new Variant();
    String wvarParseString = "";
    int wvarStringLen = 0;
    String wvarEstado = "";
    String wvarErrCod = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD )  ) + Strings.space( 10 ), 10 );
      wvarRamo = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  ) + Strings.space( 4 ), 4 );
      wvarPolizaAnn = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN )  ), 2 );
      wvarPolizaSec = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC )  ), 6 );
      wvarCertiPol = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL )  ), 4 );
      wvarCertiAnn = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN )  ), 4 );
      wvarCertiSec = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC )  ), 6 );
      wvarOperaPol = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_OPERAPOL )  ), 6 );
      wvarTipoDocu = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU )  ) + Strings.space( 2 ), 2 );
      wvarTipoImpre = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIPOIMPR )  ) + Strings.space( 2 ), 2 );
      //
      wvarStep = 60;
      wobjXMLRequest = null;
      //
      wvarStep = 70;

      //
      //Levanto los parámetros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      if( wobjXMLParametros.getDocument().getChildNodes().getLength() > 0 )
      {
        wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      }
      //
      wvarStep = 110;
      wobjXMLParametros = null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarRamo + wvarPolizaAnn + wvarPolizaSec + wvarCertiPol + wvarCertiAnn + wvarCertiSec + wvarOperaPol + wvarTipoDocu + wvarTipoImpre;
      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
      StringHolder strParseStringHolder = new StringHolder(wvarParseString);
      wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
      wvarParseString = strParseStringHolder.getValue();
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + wvarParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 240;
      //cantidad de caracteres ocupados por parámetros de entrada
      wvarPos.set( 103 );
      wvarStringLen = Strings.len( wvarParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( wvarParseString, 19, 2 );
      //Corto el codigo de error
      wvarErrCod = Strings.mid( wvarParseString, 21, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION' /><CODIGOERROR>" + wvarErrCod + "</CODIGOERROR></Response>" );
        //
      }
      else if( wvarEstado.equals( "EP" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='ERROR EN LOS PARAMETROS DE ENTRADA' /></Response>" );
        //
      }
      else
      {
        //
        if( wvarStringLen > wvarPos.toInt() )
        {
          //
          //Armamos la respuesta con el mensaje recibido
          wvarStep = 280;
          Response.set( "<Response>" );
          Response.set( Response + "<Estado resultado='true' mensaje=''/>" );
          Response.set( Response + ParseoMensaje(wvarPos, wvarParseString, wvarStringLen));
          Response.set( Response + "</Response>" );
          //
        }
        else
        {
          //No hay datos para devolver
          Response.set( "<Response><Estado resultado='false' mensaje='No se ha recibido el mensaje de respuesta'/></Response>" );
          //
        }
        //
      }
      //
      wvarStep = 290;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
        logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarRamo + wvarPolizaAnn + wvarPolizaSec + wvarCertiPol + wvarCertiAnn + wvarCertiSec + wvarOperaPol + wvarTipoDocu + wvarTipoImpre + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        throw new ComponentExecutionException(_e_);
    }
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant pvarPos, String pvarParseString, int pvarStringLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCantLin = 0;
    int wvarCounter = 0;
//    String wvarServerPath = "";
//    XmlDomExtended wobjXMLServerPath = null;
    //
    //Comentado porque no usamos más el path completo del server windows
//    wobjXMLServerPath = new XmlDomExtended();
//    wobjXMLServerPath.load( Thread.currentThread().getContextClassLoader().getResourceAsStream("LBAVirtualServerImpresion.xml"));
//    wvarServerPath = XmlDomExtended.getText( wobjXMLServerPath.selectSingleNode( "//PATH" )  );
//    wobjXMLServerPath = null;
    //
    
    String prefix = Constants.PLANETPRESS_PREFIX;
    
    wvarCantLin = (int)Math.rint( VBFixesUtil.val( ModGeneral.MidAsString( pvarParseString, pvarPos, 6 ) ) );
    wvarResult = wvarResult + "<FILES>";
    for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )
    {
      wvarResult = wvarResult + "<FILE>";
      wvarResult = wvarResult + "<TIPOHOJA>" + Strings.trim( ModGeneral.MidAsString( pvarParseString, pvarPos, 2 ) ) + "</TIPOHOJA>";
      wvarResult = wvarResult + "<DESCRIPCION>" + Strings.trim( ModGeneral.MidAsString( pvarParseString, pvarPos, 58 ) ) + "</DESCRIPCION>";
//      wvarResult = wvarResult + "<RUTA>" + wvarServerPath + "\\" + Strings.trim( ModGeneral.MidAsString( pvarParseString, pvarPos, 90 ) ) + "</RUTA>";
      wvarResult = wvarResult + "<RUTA>" + prefix + Strings.trim( ModGeneral.MidAsString( pvarParseString, pvarPos, 90 ) ) + "</RUTA>";
      wvarResult = wvarResult + "</FILE>";
    }
    wvarResult = wvarResult + "</FILES>";
    //
    ParseoMensaje = wvarResult;
    return ParseoMensaje;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
