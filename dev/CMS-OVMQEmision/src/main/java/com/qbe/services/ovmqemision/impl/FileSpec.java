package com.qbe.services.ovmqemision.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.fileutils.CMSFileUtils;
import com.qbe.services.fileutils.UnsupportedProtocolException;
import com.qbe.services.ovmqemision.Constants;
import com.qbe.vbcompat.xml.XmlDomExtended;

public class FileSpec {

	public static Logger logger = Logger.getLogger(FileSpec.class.getName());

	protected URL fileURL;
	protected String fullPath = "";
	protected boolean deleteableFile = false;
	protected boolean planetPressFile = false;

	public static FileSpec initFileSpecWaitUpTo(String wvarFileName, int timeoutSegundos, String planetPressRepoUrl)
			throws UnsupportedFileSpecException, IOException {
		FileSpec fs = new FileSpec();
		fs.parse(wvarFileName, planetPressRepoUrl);
		logger.log(Level.FINE, String.format("fileURL a recuperar es: %s", fs.fileURL));

		boolean exists = fs.fileExistsWaitUpTo(timeoutSegundos);
		if (!exists) {
			throw new FileNotFoundException();
		}
		return fs;
	}

	public FileSpec() {
	}

	public void parse(String encodedFilename, String planetPressRepoUrl) throws UnsupportedFileSpecException {
		try {
			String filePathSearch = null;
			List<String> pathSearch = loadListPath();
			for (String path : pathSearch) {
				if ((encodedFilename.toLowerCase()).startsWith(path)) {
					filePathSearch = encodedFilename.toLowerCase().replace(path, "/");
					break;
				}
			}

			if (filePathSearch != null) {
				//String filePath = (encodedFilename.toLowerCase()).replace(Constants.PLANETPRESS_PREFIX, "/");
				fileURL = new URL(planetPressRepoUrl + filePathSearch);
				planetPressFile = true; 
			} else if ((encodedFilename.toLowerCase()).startsWith(Constants.ZIP_PREFIX)) {
				String filePath = encodedFilename.replace(Constants.ZIP_PREFIX, "/");
				fileURL = new URL(Constants.LOCAL_PREFIX + filePath);
				planetPressFile = true;
			} else if (!encodedFilename.contains("://")) {
				fileURL = new URL(
						planetPressRepoUrl + File.separator + encodedFilename);
				planetPressFile = true;
			} else if (encodedFilename.startsWith(Constants.INSIS_PREFIX)
					|| encodedFilename.startsWith(Constants.CMS_PREFIX)) {
				// Lo recupero de un directorio local. Uno xor el otro aplican
				fullPath = encodedFilename.replace(Constants.INSIS_PREFIX, Constants.INSIS_PRINTOUT_DIR)
						.replace(Constants.CMS_PREFIX, Constants.CMS_PRINTOUT_DIR);
				fileURL = new File(fullPath).toURI().toURL();
				deleteableFile = true;
			} else {
				throw new UnsupportedFileSpecException(encodedFilename);
			}
		} catch (MalformedURLException e) {
			throw new UnsupportedFileSpecException(encodedFilename, e);
		}

	}

	

	public boolean fileExistsWaitUpTo(int timeoutSegundos) throws IOException, UnsupportedFileSpecException {
		try {
			boolean exists = CMSFileUtils.resourceExists(this.fileURL);
			if (!exists) {
				// Reintenta. Reintento periódicamente, hasta llegar a
				// timeoutSegundos
				logger.log(Level.FINEST, String.format("fileURL [%s] no encontrado. Reintentando.", this.fileURL));
				int delay = 1000;
				int maxRetries = timeoutSegundos * 1000 / delay;
				for (int i = 0; i < maxRetries && !exists; i++) {
					try {
						Thread.sleep(delay);
					} catch (InterruptedException e) {
					}
					exists = CMSFileUtils.resourceExists(this.fileURL);
				}
			}
			return exists;
		} catch (UnsupportedProtocolException e) {
			throw new UnsupportedFileSpecException(e);
		}
	}
	
	/**
	 * @return
	 */
	private List<String> loadListPath() {
		List<String> pathValues = new ArrayList<String>();

		pathValues.add("planetpress://");
		pathValues.add("txtinterfacestablas://");
		pathValues.add("txtcartera://");
		pathValues.add("txtinterfaces://");
		
		
		return pathValues;
	}

	public byte[] getBytes() throws MalformedURLException, IOException {
		return CMSFileUtils.loadResource(this.fileURL);
	}

	public URL getFileURL() {
		return fileURL;
	}

	public void setFileURL(URL fileURL) {
		this.fileURL = fileURL;
	}

	public String getFullPath() {
		return fullPath;
	}

	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}

	public boolean isDeleteableFile() {
		return deleteableFile;
	}

	public void setDeleteableFile(boolean deleteableFile) {
		this.deleteableFile = deleteableFile;
	}

	public boolean isPlanetPressFile() {
		return planetPressFile;
	}

	public void setPlanetPressFile(boolean planetPressFile) {
		this.planetPressFile = planetPressFile;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
