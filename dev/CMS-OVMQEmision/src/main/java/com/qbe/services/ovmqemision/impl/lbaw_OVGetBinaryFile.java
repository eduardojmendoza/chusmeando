package com.qbe.services.ovmqemision.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.context.support.GenericXmlApplicationContext;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.ovmqemision.Constants;
import com.qbe.vbcompat.base64.Base64Encoding;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import com.qbe.vbcompat.format.VBFixesUtil;

/**
 * Objetos del FrameWork
 */

public class lbaw_OVGetBinaryFile implements VBObjectClass {
	public static final String SERVERPATHPROPS = "classpath:getBinaryFileProps.xml";
	public static final String MENSAJE_ERROR_405 = "El Documento solicitado no se encuentra disponible en este momento. ( #405 )";
	public static final String ERROR_405 = "<Response><Estado resultado='false' mensaje='" + MENSAJE_ERROR_405
			+ "' /></Response>";

	public static Logger logger = Logger.getLogger(lbaw_OVGetBinaryFile.class.getName());

	private static GenericXmlApplicationContext ctx = loadContext();
	/**
	 * Implementacion de los objetos Datos de la accion
	 */
	static final String mcteClassName = "lbawA_OVMQEmision.lbaw_OVGetBinaryFile";
	/**
	 * Parametros XML de Entrada
	 */
	static final String mcteParam_FileName = "//RUTA";
	static final String mcteParam_MantenerArchivo = "//MANTENERARCHIVO";
	static final String mcteParam_SoloBuscarArchivo = "//SOLOBUSCARARCHIVO";
	static final String FTP_USER = "ftp.oracle.user";
	static final String FTP_HOST = "ftp.host";
	static final int FTP_PORT = 22;
	static final String FTP_PASS = "ftp.oracle.pass";
	static final String CHANNEL_SFTP = "sftp";
	private static String propsFilename = System.getProperty("cms.config.properties");
	//private static String propsFilename = "local.properties";

	private EventLog mobjEventLog = new EventLog();

	@Override
	public int IAction_Execute(String Request, StringHolder Response, String ContextInfo) {
		int IAction_Execute = 0;
		Variant vbLogEventTypeError = new Variant();
		int wvarStep = 0;
		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//
			wvarStep = 10;
			// Levanto los parámetros que llegan desde la página
			XmlDomExtended wobjXMLRequest = new XmlDomExtended();
			XmlDomExtended wobjXMLResponse = new XmlDomExtended();
			wobjXMLRequest.loadXML(Request);
			String wvarFileName = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_FileName));
			boolean wvarMarcaMantenerArch = !(wobjXMLRequest
					.selectSingleNode(mcteParam_MantenerArchivo) == (org.w3c.dom.Node) null);
			boolean devolverArchivo = wobjXMLRequest.selectSingleNode(mcteParam_SoloBuscarArchivo) == null;
			String hostdir = ""; 
			wvarStep = 20;
			int timeoutSegundos = getConfigTimeoutSeconds();
			//String planetPressRepoUrl = Constants.LOCAL_PREFIX + getConfigPlanetPressURL();
			String filePath = ctx.getBeanFactory().resolveEmbeddedValue("${get.fold." + getFilePath(wvarFileName) + "}");
			String planetPressRepoUrl = Constants.LOCAL_PREFIX + filePath ;

			wvarStep = 21;

			FileSpec fs = FileSpec.initFileSpecWaitUpTo(wvarFileName, timeoutSegundos, planetPressRepoUrl);

			Response.set("<Response><Estado resultado='true' mensaje='' /></Response>");
			if (devolverArchivo) {
				byte[] wvarArrBytes = fs.getBytes();
				wvarStep = 30;
				wobjXMLResponse = new XmlDomExtended();
				wobjXMLResponse.loadXML(Response.getValue());

				org.w3c.dom.Element wobjEle = wobjXMLResponse.getDocument().createElement("BinData");
				wobjXMLResponse.selectSingleNode("//Response").appendChild(wobjEle);
				wobjEle.setAttribute("xmlns:dt", "urn:schemas-microsoft-com:datatypes");
				wobjEle.setAttribute("dt:dt", "bin.base64");
				XmlDomExtended.setText(wobjEle, Base64Encoding.encode(wvarArrBytes));

				Response.set(XmlDomExtended.prettyPrint(wobjXMLResponse.getDocument().getDocumentElement()));

				if (!wvarMarcaMantenerArch) {
					if (fs.planetPressFile) {
						fs.setDeleteableFile(true);
						/*logger.log(Level.WARNING, String.format("¿Se puede borrar? %s", fs.deleteableFile));
						File localFile = new File(fs.fullPath);
						
						logger.info("getNAme" + localFile.getName());
						logger.info("absolutePath " + localFile.getAbsolutePath());
						fs.

						logger.log(Level.WARNING, "DIRECCION DEL ARCHIVOo " + wvarFileName);
						logger.log(Level.WARNING, "URL DEL ARCHIVO " + localFile.getPath());
						localFile.setExecutable(true);
						localFile.setReadable(true);
						localFile.setWritable(true);
						
						logger.log(Level.WARNING, fs.fileURL.toString());
						boolean deleteSuccessful = localFile.delete();
						// Deshabilitado, no puedo borrar aún
						logger.log(Level.WARNING, String.format("Función implementada Snoop %s", fs.fileURL));
						logger.log(Level.WARNING, String.format("¿Se puede borrar? %s", fs.deleteableFile));
						if (!deleteSuccessful) {
							logger.log(Level.WARNING, String.format("No se pudo borrar el archivo %s", fs.fileURL));
						}*/
						try {
						     JSch ssh = new JSch();
						     //Session session = ssh.getSession("oracle", "10.1.10.30", 22);
								Session session = ssh.getSession(loadConfig(FTP_USER), loadConfig(FTP_HOST), FTP_PORT);
						     // Remember that this is just for testing and we need a quick access, you can add an identity and known_hosts file to prevent
						     // Man In the Middle attacks
						     java.util.Properties config = new java.util.Properties();
						     config.put("StrictHostKeyChecking", "no");
						     session.setConfig(config);
						     session.setPassword(loadConfig(FTP_PASS));
						     
						     session.connect();
						     Channel channel = session.openChannel(CHANNEL_SFTP);
						     channel.connect();

						     ChannelSftp sftp = (ChannelSftp) channel;

						     // use the get method , if you are using android remember to remove "file://" and use only the relative path
//						     sftp.rm("/PlanetPress/GR17200B_483923_000001.pdf");
						     // Planetpress://GR17210C_482226_000001.pdf
						     //<RUTA>zip://TXTCARTERA/OV_PR5992_20170210.zip</RUTA>
						     String fileName = wvarFileName.substring(wvarFileName.lastIndexOf("/")+1);
						     logger.log(Level.INFO, "Archivo " + filePath +"/"+ fileName);
						     sftp.rm(filePath + File.separator +  fileName);
						     logger.log(Level.INFO, "Archivo " + fileName + "borrado");
						     channel.disconnect();
						     session.disconnect();
						} catch (JSchException e) {
							logger.log(Level.WARNING, "Al buscar un archivo", e);
							Response.set(ERROR_405);
							IAction_Execute = 0;
							return IAction_Execute;
						} catch (SftpException e) {
							logger.log(Level.WARNING, "Al buscar un archivo", e);
							Response.set(ERROR_405);
							IAction_Execute = 0;
							return IAction_Execute;
						}
					}
				}
			}

			IAction_Execute = 0;
			return IAction_Execute;

		} catch (FileNotFoundException e) {
			logger.log(Level.WARNING, "Al buscar un archivo", e);
			Response.set(ERROR_405);
			IAction_Execute = 0;
			return IAction_Execute;
		} catch (UnsupportedFileSpecException e) {
			logger.log(Level.WARNING, "Al buscar un archivo", e);
			Response.set(ERROR_405);
			IAction_Execute = 0;
			return IAction_Execute;
		} catch (Exception _e_) {
			Err.set(_e_);
			mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"),
					mcteClassName, "", wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber()
							+ "] - " + Err.getError().getDescription() + " Hora:" + DateTime.now(),
					vbLogEventTypeError);
			IAction_Execute = 1;
			throw new ComponentExecutionException(_e_);
		}
	}

	

	public static String getConfigPlanetPressURL() throws XmlDomExtendedException, IOException {
		logger.log(Level.INFO, "CARGO RUTA:" + ctx.getBeanFactory().resolveEmbeddedValue("${impresiones.path}"));
		return ctx.getBeanFactory().resolveEmbeddedValue("${impresiones.path}");
	}

	public static int getConfigTimeoutSeconds() throws XmlDomExtendedException, IOException {
		logger.log(Level.INFO, "CARGO TIMEOUT:" + ctx.getBeanFactory().resolveEmbeddedValue("${impresiones.timeout}"));
		return (int) Math.rint(VBFixesUtil.val(ctx.getBeanFactory().resolveEmbeddedValue("${impresiones.timeout}")));
	}

	private void ObjectControl_Activate() throws Exception {
	}

	private boolean ObjectControl_CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		return ObjectControl_CanBePooled;
	}

	private void ObjectControl_Deactivate() throws Exception {
	}

	public void Activate() throws Exception {
	}

	public boolean CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		//
		ObjectControl_CanBePooled = true;
		//
		return ObjectControl_CanBePooled;
	}

	public void Deactivate() throws Exception {
	}

	private static GenericXmlApplicationContext loadContext() {
		GenericXmlApplicationContext context = new GenericXmlApplicationContext();
		context.load(SERVERPATHPROPS);
		context.refresh();
		context.start();
		return context;
	}
	
	/**
	 * Método que retorna el valor de la key que esta dentro del archivo de
	 * propiedades
	 * 
	 * @param key
	 * @return
	 * @throws IOException
	 */
	private static String loadConfig(String key) throws IOException {
		Properties fileProper = new Properties();
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(propsFilename);
		fileProper.load(in);
		in.close();
		return fileProper.getProperty(key);

	}
	
	/**
	 * @param wvarFileName
	 * @return
	 */
	private String getFilePath(String wvarFileName) {
		String path ="txtinterfaces";
		if ((wvarFileName.toLowerCase()).contains("planetpress")){
			path = "planetpress";
		} else if ((wvarFileName.toLowerCase()).contains("txtcartera")){
			path = "txtcartera";
		}else if ((wvarFileName.toLowerCase()).contains("txtinterfacestabla")){
			path = "txtinterfacestabla";
		}
		return path;
	}

}
