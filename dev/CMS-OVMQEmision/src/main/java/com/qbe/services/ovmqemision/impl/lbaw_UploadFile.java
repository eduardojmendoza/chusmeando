package com.qbe.services.ovmqemision.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Logger;

import org.apache.commons.io.FilenameUtils;
import org.springframework.context.support.GenericXmlApplicationContext;

import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.ovmqemision.Constants;
import com.qbe.vbcompat.base64.Base64Encoding;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Variant;

/**
 * Objetos del FrameWork
 */

/**
 * @author Mauro Plaquin
 *
 */
public class lbaw_UploadFile implements VBObjectClass {
	public static final String SERVERPATHPROPS = "classpath:getBinaryFileProps.xml";


	public static Logger logger = Logger.getLogger(lbaw_UploadFile.class.getName());

	private static GenericXmlApplicationContext ctx = loadContext();
	/**
	 * Implementacion de los objetos Datos de la accion
	 */
	static final String mcteClassName = "lbawA_OfVUtilsFiles.lbaw_UploadFile";
	/**
	 * Parametros XML de Entrada
	 */

	static final String mcteParam_XMLDatosMID = "//xmlDataSource";
	static final String mcteParam_Path = "//Path";
	static final String mcteParam_Content = "//Contenido";
	static final String mcteParam_ReportId = "//ReportId";

	static final String mcteParam_FileName = "//FILENAME";
	static final String mcteParam_MailBox = "//MAILBOX";


	private EventLog mobjEventLog = new EventLog();

	/* (non-Javadoc)
	 * @see com.qbe.vbcompat.framework.VBObjectClass#IAction_Execute(java.lang.String, com.qbe.vbcompat.string.StringHolder, java.lang.String)
	 */
	@Override
	public int IAction_Execute(String Request, StringHolder response, String ContextInfo) {
		int IAction_Execute = 0;
		Variant vbLogEventTypeError = new Variant();
		String wvarRutaPDF;
		String pathFileCreate;

		int wvarStep = 0;
		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//
			wvarStep = 10;
			// Levanto los parámetros que llegan desde la página
			XmlDomExtended wobjXMLRequest = new XmlDomExtended();

			wobjXMLRequest.loadXML(Request);
			wvarStep = 20;
			wvarStep = 21;
			String wvarPath = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Path));
			wvarStep = 22;
			String wvarContenido = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Content));
			String fileName = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_FileName));
			String mailBox = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_MailBox));
			wvarStep = 30;
			wvarRutaPDF = Constants.LOCAL_PREFIX
					+ ctx.getBeanFactory().resolveEmbeddedValue("${upload.fold." + wvarPath.toLowerCase() + "}");


			wvarStep = 40;
			pathFileCreate = crearFile(wvarRutaPDF, fileName, mailBox, wvarContenido);


			wvarStep = 60;
			response.set("<Response>");
			response.set(response + "<Estado resultado='true' mensaje=''/>");
			response.set(response + pathFileCreate);
			response.set(response + "</Response>");
			IAction_Execute = 0;
			return IAction_Execute;

		
		} catch (Exception _e_) {
			Err.set(_e_);
			mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"),
					mcteClassName, "", wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber()
							+ "] - " + Err.getError().getDescription() + " Hora:" + DateTime.now(),
					vbLogEventTypeError);
			IAction_Execute = 1;
			throw new ComponentExecutionException(_e_);
		}
	}





	public void Activate() throws Exception {
	}

	public boolean CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		//
		ObjectControl_CanBePooled = true;
		//
		return ObjectControl_CanBePooled;
	}

	public void Deactivate() throws Exception {
	}

	/**
	 * @return
	 */
	private static GenericXmlApplicationContext loadContext() {
		GenericXmlApplicationContext context = new GenericXmlApplicationContext();
		context.load(SERVERPATHPROPS);
		context.refresh();
		context.start();
		return context;
	}

	/**
	 * Método que crea el archivo en la ruta que indica el path
	 * 
	 * @param wvarRutaPDF
	 * @param fileName
	 * @param contentBase64
	 * @return
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	private static String crearFile(String wvarRutaPDF, String fileName, String mailBox, String contentBase64) throws IOException, URISyntaxException {

		byte[] data = Base64Encoding.decode(contentBase64);
		logger.info("se prepara ruta para crear archivo en :" + wvarRutaPDF + fileName);
		File archivo;

		URL archivoURL = new URL(wvarRutaPDF + File.separator + mailBox + "/"
				+ FilenameUtils.getBaseName(fileName) + "." + FilenameUtils.getExtension(fileName));
		archivo = new File(archivoURL.toURI());

		logger.info("Archivo creado " + archivo.getPath() + " " + archivo.getName());
		BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(archivo));
		writer.write(data);
		writer.flush();
		writer.close();
		logger.info("EL archivo se encuentra en: " + archivo.getPath());
		return archivo.getPath();

	}

	
}
