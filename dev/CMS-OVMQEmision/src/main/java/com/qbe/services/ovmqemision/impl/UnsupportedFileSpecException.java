package com.qbe.services.ovmqemision.impl;

public class UnsupportedFileSpecException extends Exception {

	public UnsupportedFileSpecException() {
	}

	public UnsupportedFileSpecException(String message) {
		super(message);
	}

	public UnsupportedFileSpecException(Throwable cause) {
		super(cause);
	}

	public UnsupportedFileSpecException(String message, Throwable cause) {
		super(message, cause);
	}

}
