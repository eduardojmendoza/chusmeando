package com.qbe.connector.mq;

/**
 * Una implementación del http://www.eaipatterns.com/RequestReplyJmsExample.html usando MQ
 * Un MQConnector tiene una Queue para enviar mensajes, otra para recibirlos.
 * Usa un matchCriteria para saber como correlacionar los mensajes de la cola de respuesta con los que envió. @see http://www.eaipatterns.com/CorrelationIdentifier.html
 * Se puede configurar un timeout máximo para esperar una respuesta
 *  
 *
 * @author ramiro
 *
 */
public interface MQConnector {

	public static final int MSG_ID_2_MSG_ID = 1;

	public static final int MSG_ID_2_CORRELATION_ID = 2;

	public static final int CORRELATION_ID_2_CORRELATION_ID = 3;
	
	public static final int CORRELATION_ID_2_MSG_ID = 4;


	/**
	 * Envía y recibe un mensaje via MQ
	 *  
	 * @param value
	 * @return
	 */
	public String sendMessage(String value) throws MQConnectorException;

	/**
	 * Envía y recibe un mensaje via MQ, esperando hasta timeout milisegundos para obtener la respuesta antes de devolver null
	 *  
	 * @param value
	 * @return
	 */
	public String sendMessage(final String value, final long timeout) throws MQConnectorException;

	public void setReceiveTimeout(long waitInterval);

	public long getReceiveTimeout();

	public void setQueueManagerName(String queueManagerName);

	public String getQueueManagerName();

	public void setPutQueueName(String putQueueName);

	public String getPutQueueName();

	public void setGetQueueName(String getQueueName);

	public String getGetQueueName();

	/**
	 * Especifica como matchear los mensajes recibidos con los enviados
	 * 
	 * @param matchCriteria una de las constantes definidas en esta clase
	 */
	public void setMatchCriteria(int matchCriteria);

	public int getMatchCriteria();

}