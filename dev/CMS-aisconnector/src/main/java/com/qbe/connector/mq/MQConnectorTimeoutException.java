package com.qbe.connector.mq;


public class MQConnectorTimeoutException extends MQConnectorException {

	
	public MQConnectorTimeoutException() {
		super();
	}

	public MQConnectorTimeoutException(String s) {
		super(s);
	}

	public MQConnectorTimeoutException(Throwable e) {
		super(e);
	}

	public MQConnectorTimeoutException(String message, Throwable e) {
		super(message,e);
	}

}
