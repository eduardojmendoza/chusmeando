package com.qbe.connector.mq;


public class MQConnectorException extends Exception {

	
	public MQConnectorException() {
		super();
	}

	public MQConnectorException(String s) {
		super(s);
	}

	public MQConnectorException(Throwable e) {
		super(e);
	}

	public MQConnectorException(String message, Throwable e) {
		super(message,e);
	}

}
