package com.qbe.services.mwGenerico.impl;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Variant;
/**
 * Esta clase está migrada pero no terminada, no hay mapeo definido en ActionCodesRegistry
 * 
 * FIXME: terminar de emprolijar la migración. Falta al menos testearla y emprolijar logging
 */
public class lbaw_Frame2MIDD implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_MWGenerico.lbaw_Frame2MIDD";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   */
  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeInformation = new Variant();
    Variant vbLogEventTypeError = new Variant();
    String wvarRequest = "";
    String wvarResponse = "";
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    Object wobjFrame2MIDD = null;
    int wvarStep = 0;
    int wvarError = 0;
    String wvarOrigenError = "";
    String wvarRtaCOM = "";
    String v1 = "";
    String v2 = "";
    String v3 = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Carga el request
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //Ejecuta el Frame2MIDD
      wvarStep = 20;
      wobjFrame2MIDD = ModGeneral.gcteClassFrame2MIDD;
      //error: function 'Execute' was not found.
      //unsup: wvarError = wobjFrame2MIDD.Execute(wobjXMLRequest.xml, wvarResponse, "")
      wobjFrame2MIDD = null;
      //
      //Carga la respuesta para anlizarla
      wvarStep = 30;
      wobjXMLResponse = new XmlDomExtended();
      wobjXMLResponse.loadXML( wvarResponse );
      //
      wvarStep = 40;
      //Error (ya sea de la Frame2Midd o de Middleware)
      if( wvarError != 0 )
      {
        //
        wvarStep = 50;
        wvarOrigenError = XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@SOURCE" )  );
        //
        wvarStep = 60;
        if( ! (wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@MENSAJE" )  == (org.w3c.dom.Node) null) )
        {
          wvarRtaCOM = XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@MENSAJE" )  );
        }
        else
        {
          wvarRtaCOM = "El COM+ " + ModGeneral.gcteClassFrame2MIDD + " pinch�.";
        }
        //
        
        if( wvarOrigenError.equals( "WDFRAME2MIDD" ) )
        {
          //Error de la Frame2midd
          //
          //
          wvarStep = 70;
          mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, wvarError, "Error= [" + wvarError + "] - MENSAJE: " + wvarRtaCOM, vbLogEventTypeError );
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "WD.Frame2MIDD devolvi� un error." + String.valueOf( (char)(34) ) + " />" + wvarResponse + "</Response>" );
          //
        }
        else if( wvarOrigenError.equals( "MIDD" ) )
        {
          //Error de Middleware
          //
          wvarStep = 80;
          if( ! (wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@op" )  == (org.w3c.dom.Node) null) )
          {
            v1 = "@op: " + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@op" )  );
          }
          else
          {
            v1 = "@op: no vino";
          }
          //
          wvarStep = 90;
          if( ! (wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@RspCde" )  == (org.w3c.dom.Node) null) )
          {
            v2 = "@RspCde: " + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@RspCde" )  );
          }
          else
          {
            v2 = "@RspCde: no vino";
          }
          //
          wvarStep = 100;
          if( ! (wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@Result" )  == (org.w3c.dom.Node) null) )
          {
            v3 = "@Result: " + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@Result" )  );
          }
          else
          {
            v3 = "@Result: no vino";
          }
          //
          wvarStep = 110;
          mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, 0, "Info= " + v1 + " | " + v2 + " | " + v3 + " MENSAJE: " + wvarResponse, vbLogEventTypeInformation );
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "Middleware contest� con un error controlado." + String.valueOf( (char)(34) ) + " />" + wvarResponse + "</Response>" );
          //
        }
        //
      }
      else
      {
        //Sin problemas
        wvarStep = 120;
        if( wvarError == 0 )
        {
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResponse + "</Response>" );
        }
        //
      }
      //
      wvarStep = 130;
      //
      wobjXMLRequest = null;
      wobjXMLResponse = null;
      //
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        wobjXMLRequest = null;
        wobjXMLResponse = null;
        //
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
