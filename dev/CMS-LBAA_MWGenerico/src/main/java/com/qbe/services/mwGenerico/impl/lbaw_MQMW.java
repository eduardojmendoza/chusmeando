package com.qbe.services.mwGenerico.impl;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.base64.Base64Encoding;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.framework.jaxb.Estado;
import com.qbe.vbcompat.framework.jaxb.Response;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.MQMWNamespaceResolver;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

import diamondedge.util.DateTime;
import diamondedge.util.Obj;
import diamondedge.util.Strings;

/**
 * Envía un mensaje al servicio 4 ( coldview ) de MW via MQ, y devuelve un PDF
 * 
 * @author ramiro
 *
 */
public class lbaw_MQMW implements VBObjectClass
{
	private static final String USERID = "compusrdes";

	private static Logger logger = Logger.getLogger(lbaw_MQMW.class.getName());

  static final String gcteQueueManager = "//QUEUEMANAGER";
  static final String gctePutQueue = "//PUTQUEUE";
  static final String gcteGetQueue = "//GETQUEUE";
  static final String gcteGMOWaitInterval = "//GMO_WAITINTERVAL";
  static final String gcteClassMQConnection = "WD.Frame2MQ";
  static final String mcteClassName = "lbawA_MWGenerico.lbaw_MQMW";
  static final String mcteSubDirName = "Definiciones";

  /**
   * 
   */
  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    String RespuestaMQ = "";
    int wvarMQError = 0;
    MQProxy wobjFrame2MQ = null;

    try {

        String wvarHoraInicio = DateTime.format( DateTime.now() );
        long start = System.currentTimeMillis();

        XmlDomExtended wobjXMLRequest = new XmlDomExtended();
        wobjXMLRequest.loadXML( pvarRequest );

        XmlDomExtended wobjXMLDefinition = getDefinition(wobjXMLRequest);

        String configFileName = getConfigFileName(wobjXMLDefinition);
        
        StringHolder nodoRaizName = new StringHolder();
        String	requestMQ = createMQRequest(wobjXMLRequest, wobjXMLDefinition, nodoRaizName);
    		
          wobjFrame2MQ = MQProxy.getInstance(configFileName);
          StringHolder shRespuestaMQ = new StringHolder();
          
          //Para levantar el timeout del default de 30 a 60
//          wvarMQError = wobjFrame2MQ.executePrim(request, shRespuestaMQ, 60000);
          //También podría usar el valor de timeout que levanto del archivo de configuración
          int timeout = getTimeout(wobjXMLDefinition);
          wvarMQError = wobjFrame2MQ.executePrim(requestMQ, shRespuestaMQ);
          
          RespuestaMQ = shRespuestaMQ.getValue();

      if( wvarMQError != 0 )
      {
        pvarResponse.set(Response.marshaled(Estado.FALSE, "El servicio de consulta no se encuentra disponible", "Codigo Error:" + wvarMQError));
      }
      else
      {
        if( Strings.asc( Strings.right( RespuestaMQ, 1 ) ) == 0 )
        {
        	RespuestaMQ = Strings.left( RespuestaMQ, Strings.len( RespuestaMQ ) - 1 );
        }
        XmlDomExtended wvarXMLPDF = new XmlDomExtended();
        wvarXMLPDF.loadXML( RespuestaMQ );
        String operationReturnValue = "";
        
        /*
RespuestaMQ es por ejemplo:
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
	<SOAP-ENV:Header/>
	<SOAP-ENV:Body>
		<ns2:retrieveReportListResponse xmlns:ns2="http://ejbs.mdw.hbar.hsbc.com">
			<retrieveReportListReturn>      <---- Este es [nombreDeOperacion]Return
				<item>1</item>
				<item>2</item>
			</retrieveReportListReturn>
		</ns2:retrieveReportListResponse>
	</SOAP-ENV:Body>
</SOAP-ENV:Envelope>

o

<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header/>
    <SOAP-ENV:Body>
        <ns2:retrieveReportResponse xmlns:ns2="http://ejbs.mdw.hbar.hsbc.com">
            <retrieveReportReturn>
                <report>JVBERi0xLjQKJeLj...etc
                
                
                
También puede venir una respeusta con Body vacío, como pasa en sendPdfReport                
         */
        //Serializa solamente el nodo *Return
        String returnNodeXPath = "//" + Strings.replace( nodoRaizName.getValue(), "share:", "" ) + "Return";
        if( ! (wvarXMLPDF.selectSingleNode(returnNodeXPath)  == null) )
        {
          operationReturnValue = XmlDomExtended.marshal(wvarXMLPDF.selectSingleNode( "//" + Strings.replace( nodoRaizName.getValue(), "share:", "" ) + "Return" ));
        }
        else
        {
          operationReturnValue = "";
        }
        String wvarFaultCode = "";
        if( wvarXMLPDF.selectSingleNode( "//faultstring" )  != null ) {
          wvarFaultCode = "<faultstring>" + XmlDomExtended.getText( wvarXMLPDF.selectSingleNode( "//faultstring" )  ) + "</faultstring>";
        }
        pvarResponse.set(String.format("<Response><Estado resultado='true' mensaje='%s'/>%s</Response>",wvarFaultCode, operationReturnValue));
      }

      if ( getLoguear(wobjXMLDefinition))
      {
    	  //FIXME Loguear con logger
//        wvarNroArch = FileSystem.getFreeFile();
//        wvarExisteArchivo = "";
//        wvarExisteArchivo = "" /*unsup this.Dir( System.getProperty("user.dir") + "\\LOG\\" + wvarNomArchivo, 0 ) */;
//
//        if( !wvarExisteArchivo.equals( "" ) )
//        {
//          FileSystem.kill( System.getProperty("user.dir") + "\\LOG\\" + wvarNomArchivo );
//        }

        //error: Opening files for 'Binary' access is not supported.
        //unsup: Open App.Path & "\LOG\" & wvarNomArchivo For Binary As wvarNroArch
        String wvarTextoALoguear = "<LOGs><LOG InicioConsulta=\"" + wvarHoraInicio + "\" FinConsulta=\"" + DateTime.now() + "\" TiempoIncurrido=\"" + (System.currentTimeMillis() - start) + " mseg\"" + ">" + 
        		"<AreaIn>" + requestMQ + "</AreaIn>" + 
        		"<AreaOut>" + RespuestaMQ + "</AreaOut>" + "</LOG></LOGs>";
        logger.log(Level.FINE, wvarTextoALoguear);

        String logFileName = getLogFileName(wobjXMLDefinition);
        //TODO Grabar el archivo
      }
      return 0;
    }
    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
    	logger.log(Level.SEVERE, "", toe);
        pvarResponse.set(Response.marshaled(Estado.FALSE, "El servicio de consulta no se encuentra disponible", "Codigo Error:3. " + toe.getClass().getName()));
		return 3;
	}
    catch (com.qbe.connector.mq.MQProxyException toe) {
    	logger.log(Level.SEVERE, "", toe);
        pvarResponse.set(Response.marshaled(Estado.FALSE, "El servicio de consulta no se encuentra disponible", "Codigo Error:3. " + toe.getClass().getName()));
		return 3;
	}
    catch( Exception _e_ )
    {
    	logger.log(Level.SEVERE, "Exception. Retornando Estado/@resultado='false'", _e_);
        pvarResponse.set(Response.marshaledResultadoFalse(String.format("Exception %s en %s", _e_.getClass().getName(), this.getClass().getName()), _e_.getMessage()));
    }
    return 0;
  }

  protected String getConfigFileName(XmlDomExtended wobjXMLDefinition) throws XmlDomExtendedException, MQMWException, IOException {
        return XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/MQCONFIGFILE" )  );
        
}

  protected int getTimeout(XmlDomExtended wobjXMLDefinition) throws XmlDomExtendedException, MQMWException, IOException {
      return Obj.toInt( XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/TIMEOUT" )  ) );
      
}

  protected boolean getLoguear(XmlDomExtended wobjXMLDefinition) throws XmlDomExtendedException, MQMWException, IOException {
      return wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" ) != null;
  }
  
  
  protected String getLogFileName(XmlDomExtended wobjXMLDefinition) throws XmlDomExtendedException, MQMWException, IOException {
  
	  if( wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" ).getAttributes().getNamedItem( "LogFile" ) != null ) {
		  return XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) );
	  } else {
		  return null;
	  }
}

protected XmlDomExtended getDefinition(XmlDomExtended wobjXMLRequest) throws XmlDomExtendedException, MQMWException,
		IOException {
	if (wobjXMLRequest.selectSingleNode( "//DEFINICION" )  == null ) {
          throw new MQMWException("No se encontró nodo DEFINICION en Request o XML mal formado");
        }
        
        String wvarDefinitionFile = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DEFINICION" )  );

        XmlDomExtended wobjXMLDefinition = new XmlDomExtended();
        wobjXMLDefinition.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteSubDirName + File.separator + wvarDefinitionFile ));
	return wobjXMLDefinition;
}

protected String createMQRequest(XmlDomExtended wobjXMLRequest, XmlDomExtended wobjXMLDefinition, StringHolder wvarNodoRaiz) throws MQMWException, XmlDomExtendedException, IOException {


      Node wobjXMLSobre = wobjXMLDefinition.selectSingleNode( "//XMLDEFINICION" ) .getFirstChild();

//    Uso este método prettyPrintComplete porque genera los namespaces en los nodos, en este caso en el share:sendPdfReport
      String wvarInputMQ = XmlDomExtended.prettyPrintComplete(wobjXMLSobre);

      //De acá en adelante uso:
      //wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz, new MQMWNamespaceResolver() )
      // para que pueda resolver el caso de las expresiones xpath con forma share:sendPdfReport.
      // el MQMWNamespaceResolver tiene cableados los nombres de los NSs.
      // Para no cablearlo tanto podríamos usar un NamaespaceResolver que tomara la definición del doc, pero en este caso está fijo por config,
      // así que no es necesario.
      // Las alternativas están descriptas en http://www.ibm.com/developerworks/library/x-nmspccontext/
      
      XmlDomExtended wobjXMLInputMQ = new XmlDomExtended();
      wobjXMLInputMQ.loadXML( wvarInputMQ );

      NodeList wobjNodosEntrada = wobjXMLDefinition.selectNodes( "//ENTRADA/PARAMETRO" ) ;

      String wvarCDATA = "NO";
      
      for( int nwobjNodoEntrada = 0; nwobjNodoEntrada < wobjNodosEntrada.getLength(); nwobjNodoEntrada++ )
      {
        Node wobjNodoEntrada = wobjNodosEntrada.item( nwobjNodoEntrada );

        //Que existan todos los nodos definidos, en el Request
        String attributeName = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ));
        if( (wobjXMLRequest.selectSingleNode( "//" + attributeName ) == null) && (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == null) )
        {
          throw new MQMWException("La definición especifica el nodo sin default " +  attributeName + ", pero no se encontró en el Request");
        }
        else
        {
          //Que Exista Nodo Raiz
          if( Strings.toUpperCase( XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "RAIZ" ) )
          {
            if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == null) )
            {
              wvarNodoRaiz.set(XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Default" )));
            }
            else
            {
              wvarNodoRaiz.set(XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) )));
            }
            if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "CDATA" ) == null) )
            {
              wvarCDATA = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "CDATA" ) );
            }
          }
        }
      }

      if( !wvarNodoRaiz.getValue().equals( "" ) )
      {
        for( int nwobjNodoEntrada = 0; nwobjNodoEntrada < wobjNodosEntrada.getLength(); nwobjNodoEntrada++ )
        {
          Node wobjNodoEntrada = wobjNodosEntrada.item( nwobjNodoEntrada );
          String nodeType = Strings.toUpperCase( XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" )));
          String nodeName = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) );
          String nodeContent;

          if( nodeType.equals( "RAIZ" ) )
          {
        	  // No procesamos nada en este caso
        	  continue;
          } else if( nodeType.equals( "BASE64" ) )
          {


            nodeContent = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" + 
            XmlDomExtended.marshal(wobjXMLRequest.selectSingleNode( "//" + nodeName ) .getFirstChild());

            //FIXME Esto de convertir bytes es muy probable que NO ande. revisar
            // Redimensiona Array de Bytes para alojar datos a convertir a BASE64
            byte[] warrayDatos = new byte[Strings.len( nodeContent )+1];

            for( int wvarx = 1; wvarx <= Strings.len( nodeContent ); wvarx++ )
            {

              warrayDatos[wvarx - 1] = (byte) Strings.asc( Strings.mid( nodeContent, wvarx, 1 ) );

            }

            Element wobjNodoBase64 = wobjXMLInputMQ.getDocument().createElement( nodeName );
            wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz, new MQMWNamespaceResolver() ).appendChild( wobjNodoBase64 );
            wobjNodoBase64.setAttribute( "xmlns:dt", "urn:schemas-microsoft-com:datatypes" );
            wobjNodoBase64.setTextContent(Base64Encoding.encode(warrayDatos));

          }
          else if( nodeType.equals( "TEXTO" ) )
          {

            if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == null) )
            {
              nodeContent = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) );
            }
            else
            {
            	nodeContent = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + nodeName )  );
            }

            Node wobjNodo = wobjXMLInputMQ.getDocument().createElement( nodeName );
            wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz, new MQMWNamespaceResolver() ).appendChild( wobjNodo );
            wobjNodo.setTextContent(nodeContent);
            
          }
          else if( nodeType.equals( "XML" ) )
          {

            if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == null) )
            {
              nodeContent = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) );
            }
            else
            {
            	nodeContent = XmlDomExtended.marshal(wobjXMLRequest.selectSingleNode( "//" + nodeName ) .getFirstChild());
            }
            //MHC: 16/01/2009 - Se corrige el agregado de XML al InputMQ
            XmlDomExtended wobjXMLAux = new XmlDomExtended();
            wobjXMLAux.loadXML( "<ROOT>" + nodeContent + "</ROOT>" );
            Node wobjNodo = wobjXMLInputMQ.getDocument().createElement( nodeName );
            wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz, new MQMWNamespaceResolver() ).appendChild( wobjNodo );
            wobjNodo.appendChild(wobjXMLAux.selectSingleNode( "//ROOT" ).getFirstChild() );

          }
          else
          {
        	  throw new MQMWException(String.format("Nodo desconocido en la definición. Nombre: %s, Tipo = %s", nodeName, nodeType));
          }
        }
      } else {
    	  throw new MQMWException("Nodo raiz no encontrado");
      }

      if( Strings.toUpperCase( wvarCDATA ).equals( "SI" ) )
      {
          // Reemplaza nodo Raiz por un CDATA
    	  Node rootNode = wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz, new MQMWNamespaceResolver() );
        String rootNodeXml = XmlDomExtended.marshal(rootNode);
        CDATASection rootNodeCDATA = wobjXMLInputMQ.getDocument().createCDATASection( rootNodeXml ); 
        rootNode.getParentNode().replaceChild( rootNodeCDATA, rootNode );
      }
      XmlDomExtended.setText( wobjXMLInputMQ.selectSingleNode( "//UserId" ) , USERID );
      String wvarTimeStamp = DateTime.year( DateTime.now() ) + "-" + Strings.right( ("00" + DateTime.month( DateTime.now() )), 2 ) + "-" + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) + "T" + Strings.right( ("00" + DateTime.hour( DateTime.now() )), 2 ) + ":" + Strings.right( ("00" + DateTime.minute( DateTime.now() )), 2 ) + ":" + Strings.right( ("00" + DateTime.second( DateTime.now() )), 2 ) + ".000000-03:00";
      XmlDomExtended.setText( wobjXMLInputMQ.selectSingleNode( "//MsgCreatTmsp" ) , wvarTimeStamp );
      return wobjXMLInputMQ.marshal();
}

public void Activate() throws Exception
  {
  }

  public boolean CanBePooled() throws Exception
  {
	  return true;
  }

  public void Deactivate() throws Exception
  {
  }
}
