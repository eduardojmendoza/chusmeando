package com.qbe.services.mwGenerico.impl;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetBinPDF implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_MWGenerico.lbaw_GetBinPDF";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_FileName = "//RUTA";
  static final String mcteParam_MantenerArchivo = "//MANTENERARCHIVO";
  static final String mcteParam_SoloBuscarArchivo = "//SOLOBUSCARARCHIVO";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarResult = "";
    boolean wvarMarcaMantenerArch = false;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    org.w3c.dom.Element wobjEle = null;
    XmlDomExtended wobjXMLServer = null;
    int wvarFileGet = 0;
    String wvarFileName = "";
    byte[] wvarArrBytes = null;
    int wvarCantidadIntentos = 0;
    int wvarCurrIntento = 0;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLResponse = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarFileName = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FileName )  );
      wvarMarcaMantenerArch = ! (wobjXMLRequest.selectSingleNode( mcteParam_MantenerArchivo )  == (org.w3c.dom.Node) null);
      //
      wvarStep = 20;
      //Set wobjXMLServer = CreateObject("MSXML2.DOMDocument")
      //wobjXMLServer.Load App.Path & "\LBAVirtualServerImpresion.xml"
      //wvarCantidadIntentos = Val(wobjXMLServer.selectSingleNode("//TIMEOUT").Text) * 1000 / 250
      //60 * 1000 / 250
      wvarCantidadIntentos = 240;
      //Set wobjXMLServer = Nothing
      //
      wvarStep = 21;
      //
      //Verifico que exista el archivo que se solicita por par�metro
      for( wvarCurrIntento = 1; wvarCurrIntento <= wvarCantidadIntentos; wvarCurrIntento++ )
      {
        //
        if( !"" /*unsup this.Dir( wvarFileName, 0 ) */.equals( "" ) )
        {
          break;
        }
        //Sleep (250) 'Espero de a 250ms para ver si lo termina de generar
        wvarCurrIntento = wvarCurrIntento + 1;
        //
      }
      //
      wvarStep = 22;
      //-------------------------------------------------------------------------------------------------------------------
      //// Metodos del Framework
      //-------------------------------------------------------------------------------------------------------------------
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, "", wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
