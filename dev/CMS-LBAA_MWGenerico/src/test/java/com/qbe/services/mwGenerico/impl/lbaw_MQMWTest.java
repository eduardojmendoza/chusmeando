package com.qbe.services.mwGenerico.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.MessageFormat;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.StreamUtils;
import org.w3c.dom.Node;

import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

public class lbaw_MQMWTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testlbaw_MQMW_retrieveReportList() throws CurrentProfileException, IOException, XmlDomExtendedException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		
		String nroSiniestro = "360149";
		String mqmwRequestTemplate = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("mqmw_retrieveReportList_1.xml"), Charset.forName("UTF-8"));
		String mqmwRequest = MessageFormat.format(mqmwRequestTemplate, nroSiniestro);
		
		lbaw_MQMW mqmw = new lbaw_MQMW();
		StringHolder respSH = new StringHolder();
		int result = 
			mqmw.IAction_Execute(mqmwRequest, respSH, null);
		if ( result == 3 ) {
			//timeout. No es error, puede haber fallado algo temporal de MW
			return;
		}
		assertNotNull(respSH.getValue());
		
		XmlDomExtended reportListXml = new XmlDomExtended();
		reportListXml.loadXML(respSH.getValue());
		
		Node itemNode = reportListXml.selectSingleNode("//Response/retrieveReportListReturn/item");
		assertNotNull("item es null!", itemNode);
		String item = XmlDomExtended.getText(itemNode);
		assertTrue("item es vació", item.length() > 0);
	}

	@Test
	public void testlbaw_MQMW_retrieveReport() throws Exception {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		
		String nroSiniestro = "286157";
		String item = "1";
		String mqmwRequestTemplate = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("mqmw_retrieveReport_1.xml"), Charset.forName("UTF-8"));
		String mqmwRequest = MessageFormat.format(mqmwRequestTemplate, nroSiniestro, item);
		
		lbaw_MQMW mqmw = new lbaw_MQMW();
		StringHolder respSH = new StringHolder();
		int result = 
				mqmw.IAction_Execute(mqmwRequest, respSH, null);
			if ( result == 3 ) {
				//timeout. No es error, puede haber fallado algo temporal de MW
				return;
			}
		assertNotNull(respSH.getValue());
				
		XmlDomExtended reportListXml = new XmlDomExtended();
		reportListXml.loadXML(respSH.getValue());
		Node reportNode = reportListXml.selectSingleNode("//Response/retrieveReportReturn/report");
		assertNotNull("pdf es null!", reportNode);
		String pdf = XmlDomExtended.getText(reportNode);
		assertTrue("El PDF no parece ser un pdf. Empieza con: " + pdf.substring(0, 10), pdf.startsWith("JVB"));
		FileUtils.writeStringToFile(new File("/tmp/testlbaw_MQMW_retrieveReport" + System.currentTimeMillis() + ".base64"), pdf);

	}
	
	@Test
	public void testlbaw_MQMW_badDefinitionFile() throws Exception {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		
		String mqmwRequest = "<Request><DEFINICION>fruta.xml</DEFINICION></Request>";
		
		lbaw_MQMW mqmw = new lbaw_MQMW();
		StringHolder respSH = new StringHolder();
		mqmw.IAction_Execute(mqmwRequest, respSH, null);
		assertNotNull(respSH.getValue());
		XmlDomExtended responseXml = new XmlDomExtended();
		responseXml.loadXML(respSH.getValue());
		Node resultNode = responseXml.selectSingleNode("//Response/Estado/@resultado");
		assertNotNull("resultado es null!", resultNode);
		String resultado = XmlDomExtended.getText(resultNode);
		assertEquals(resultado,"false");
		
	}	

	@Test
	public void testlbaw_MQMW_badRequest() throws Exception {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		
		String mqmwRequest = "<Request><FRUTA/></Request>";
		
		lbaw_MQMW mqmw = new lbaw_MQMW();
		StringHolder respSH = new StringHolder();
		mqmw.IAction_Execute(mqmwRequest, respSH, null);
		assertNotNull(respSH.getValue());
		XmlDomExtended responseXml = new XmlDomExtended();
		responseXml.loadXML(respSH.getValue());
		Node resultNode = responseXml.selectSingleNode("//Response/Estado/@resultado");
		assertNotNull("resultado es null!", resultNode);
		String resultado = XmlDomExtended.getText(resultNode);
		assertEquals(resultado,"false");
	}	

	@Test
	public void testlbaw_MQMW_sendPdfReport() throws Exception {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		
		String mqmwRequestTemplate = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("mqmw_sendPdfReport_1.xml"), Charset.forName("UTF-8"));
		String mqmwRequest = mqmwRequestTemplate;
		
		lbaw_MQMW mqmw = new lbaw_MQMW();
		StringHolder respSH = new StringHolder();
		int result = 
				mqmw.IAction_Execute(mqmwRequest, respSH, null);
			if ( result == 3 ) {
				//timeout. No es error, puede haber fallado algo temporal de MW
				return;
			}
		assertNotNull(respSH.getValue());
		
		System.out.println(respSH.getValue());
		
		XmlDomExtended reportListXml = new XmlDomExtended();
		reportListXml.loadXML(respSH.getValue());
		Node resultNode = reportListXml.selectSingleNode("//Response/Estado/@resultado");
		assertNotNull("resultNode es null!", resultNode);
		Node messageNode = reportListXml.selectSingleNode("//Response/Estado/@mensaje");
		String message = XmlDomExtended.getText(messageNode);
		assertTrue("Vino un mensaje inesperado como respuesta: " + message, message.length() == 0 );
	}


}
