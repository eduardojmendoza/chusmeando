VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_SOAPMW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context             As ObjectContext
Private mobjEventLog                As HSBCInterfaces.IEventLog
Private mvarConsultaRealizada       As String
Private mvarCancelacionManual       As Boolean

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

Const mcteClassName                 As String = "lbawA_MWGenerico.lbaw_SOAPMW"
Const mcteSubDirName                As String = "Definiciones"
'
Public Function IAction_Execute(ByVal pvarRequest As String, pvarResponse As String, ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName       As String = "IAction_Execute"
    '
    Dim wvarStep                    As Long
    Dim wvarHoraInicio              As String
    Dim wvarTiempo                  As Single
    Dim wvarDefinitionFile          As String
    Dim wvarTextoError              As String
    Dim wvarURLWS                   As String
    Dim wvarSoapActionURL           As String
    Dim wvarLoguear                 As Boolean
    Dim wvarNomArchivo              As String
    Dim wvarCDATA                   As String
    Dim wvarNodo                    As String
    Dim wvarDatos                   As String
    Dim wvarTextoALoguear           As String
    Dim wvarx                       As Long
    Dim wvarTimeStamp               As String
    Dim wobjXMLOutputSOAP           As String
    Dim wvarNroArch                 As Long
    Dim wvarExisteArchivo           As String
    Dim wvarConfFileName            As String
    '
    Dim wobjXMLDefinition           As MSXML2.DOMDocument
    Dim wobjXMLRequest              As MSXML2.DOMDocument
    Dim wobjXMLInputSOAP            As MSXML2.DOMDocument
    Dim wobjXMLConfig               As MSXML2.DOMDocument
    Dim wobjXMLAux                  As MSXML2.DOMDocument
    Dim wobjNodoBase64              As MSXML2.IXMLDOMElement
    Dim wobjNodosEntrada            As MSXML2.IXMLDOMNodeList
    Dim wobjNodoEntrada             As MSXML2.IXMLDOMElement
    Dim wobjFrame2MQ                As HSBCInterfaces.IAction
    Dim wobjNodo                    As MSXML2.IXMLDOMElement
    '
    On Error GoTo ErrorHandler
    wvarHoraInicio = Now()
    wvarTiempo = Timer
    '
    wvarStep = 10
    ' Crea Objetos XMLDom -----------------------------
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    Set wobjXMLDefinition = CreateObject("MSXML2.DOMDocument")
    Set wobjXMLInputSOAP = CreateObject("MSXML2.DOMDocument")
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    '-----------------------------------------------------
    '
    wvarStep = 20
    ' Carga XML del Request ASP---------------------------
    wobjXMLRequest.async = False
    wobjXMLRequest.loadXML pvarRequest
    '-----------------------------------------------------
    '
    ' Carga Nombre de Archivo de Definici�n------------
    If Not wobjXMLRequest.selectSingleNode("//DEFINICION") Is Nothing Then
        wvarDefinitionFile = wobjXMLRequest.selectSingleNode("//DEFINICION").Text
    Else
        wvarTextoError = " No se encontr� nodo DEFINICION en Request o XML mal formado"
        Err.Raise -1, , wvarTextoError
    End If
    '----------------------------------------------------
    '
    ' Carga Archivo de Definici�n----------------------------
    wvarStep = 30
    wobjXMLDefinition.async = False
    wobjXMLDefinition.Load App.Path & "\" & mcteSubDirName & "\" & wvarDefinitionFile
    If wobjXMLDefinition.parseError.errorCode <> 0 Then
         Err.Raise wobjXMLDefinition.parseError.errorCode, , wobjXMLDefinition.parseError.reason
    End If
    wvarStep = 35
    '
    ' Carga Nombre de Archivo de Definici�n SOAP --------------------
    wvarConfFileName = wobjXMLDefinition.selectSingleNode("//DEFINICION/SOAPCONFIGFILE").Text
    wvarStep = 40
    '
    ' Carga Archivo de Configuraci�n SOAP --------------------------
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & wvarConfFileName
    wvarStep = 45
    '
    ' Carga la URL del WebService y le agrega el server --------------------
    wvarURLWS = wobjXMLConfig.selectSingleNode("//SERVER").Text & wobjXMLDefinition.selectSingleNode("//DEFINICION/URL").Text
    wvarStep = 50
    '
    ' Carga el valor de SOAP ACTION ------------------------------
    wvarSoapActionURL = wobjXMLDefinition.selectSingleNode("//DEFINICION/SOAPACTION").Text
    wvarStep = 55
    '
    ' Carga nombre Archivo de Logueo
    wvarLoguear = False
    '
    If Not wobjXMLDefinition.selectSingleNode("//DEFINICION/GENERARLOG") Is Nothing Then
        wvarLoguear = True
        If Not wobjXMLDefinition.selectSingleNode("//DEFINICION/GENERARLOG").Attributes.getNamedItem("LogFile") Is Nothing Then
            wvarNomArchivo = wobjXMLDefinition.selectSingleNode("//DEFINICION/GENERARLOG").Attributes.getNamedItem("LogFile").Text
        End If
    End If
    '
    wvarStep = 80
    ' carga Envelope de xml definicion
    wobjXMLInputSOAP.async = False
    wobjXMLInputSOAP.loadXML wobjXMLDefinition.selectSingleNode("//XMLDEFINICION").firstChild.xml
    wobjXMLInputSOAP.resolveExternals = True
    '
    ' Carga los Parametos de Entrada desde la Definicion-------------
    '
    Set wobjNodosEntrada = wobjXMLDefinition.selectNodes("//ENTRADA/PARAMETRO")
    wvarStep = 90
    '
    ' Recorre los Parametros----------------------------------------
    wvarCDATA = "NO"
    For Each wobjNodoEntrada In wobjNodosEntrada
        'Que existan todos los nodos definidos, en el Request
        If (wobjXMLRequest.selectSingleNode("//" & wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text) Is Nothing) And (wobjNodoEntrada.Attributes.getNamedItem("Default") Is Nothing) Then
            wvarTextoError = " No se encontr� nodo " & wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text & " en Request"
            Err.Raise -1, , wvarTextoError
        End If
    Next
    '
    For Each wobjNodoEntrada In wobjNodosEntrada
      Select Case UCase(wobjNodoEntrada.Attributes.getNamedItem("Tipo").Text)
          Case "BASE64"
              wvarNodo = wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text
              wvarDatos = "<?xml version=""1.0"" encoding=""ISO-8859-1""?>" & wobjXMLRequest.selectSingleNode("//" & wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text).firstChild.xml
              ' Redimensiona Array de Bytes para alojar datos a convertir a BASE64
              ReDim warrayDatos(Len(wvarDatos))
              For wvarx = 1 To Len(wvarDatos)
                  warrayDatos(wvarx - 1) = CByte(Asc(Mid(wvarDatos, wvarx, 1)))
              Next wvarx
              Set wobjNodoBase64 = wobjXMLInputSOAP.selectSingleNode("//" & wvarNodo)
              wobjNodoBase64.setAttribute "xmlns:dt", "urn:schemas-microsoft-com:datatypes"
              wobjNodoBase64.dataType = "bin.base64"
              wobjNodoBase64.nodeTypedValue = warrayDatos
              Set wobjNodoBase64 = Nothing
          Case "TEXTO"
              wvarNodo = wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text
              If Not wobjNodoEntrada.Attributes.getNamedItem("Default") Is Nothing Then
                  wvarDatos = wobjNodoEntrada.Attributes.getNamedItem("Default").Text
              Else
                  wvarDatos = wobjXMLRequest.selectSingleNode("//" & wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text).Text
              End If
              Set wobjNodo = wobjXMLInputSOAP.selectSingleNode("//" & wvarNodo)
              wobjNodo.nodeTypedValue = wvarDatos
              Set wobjNodo = Nothing
          Case "XML"
              wvarNodo = wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text
              If Not wobjNodoEntrada.Attributes.getNamedItem("Default") Is Nothing Then
                  wvarDatos = wobjNodoEntrada.Attributes.getNamedItem("Default").Text
              Else
                  wvarDatos = wobjXMLRequest.selectSingleNode("//" & wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text).firstChild.xml
              End If
              Set wobjXMLAux = CreateObject("MSXML2.DOMDocument")
              wobjXMLAux.loadXML ("<ROOT>" & wvarDatos & "</ROOT>")
              Set wobjNodo = wobjXMLInputSOAP.selectSingleNode("//" & wvarNodo)
              wobjNodo.appendChild wobjXMLAux.selectSingleNode("//ROOT").firstChild
              Set wobjNodo = Nothing
              Set wobjXMLAux = Nothing
          Case Else
        End Select
    Next wobjNodoEntrada
    '
    wvarStep = 100
    'wvarTimeStamp = Year(Now()) & "-" & Right("00" & Month(Now()), 2) & "-" & Right("00" & Day(Now()), 2) & "T" & Right("00" & Hour(Now()), 2) & ":" & Right("00" & Minute(Now()), 2) & ":" & Right("00" & Second(Now()), 2) & ".000000-03:00"
    'wobjXMLInputSOAP.selectSingleNode("//MsgCreatTmsp").Text = wvarTimeStamp
    '
    wvarStep = 130
    'Env�a el mensaje por SOAP
    wobjXMLOutputSOAP = PostWebService(wvarURLWS, wvarSoapActionURL, wobjXMLInputSOAP.xml)
    '
    wvarStep = 140
    ' Genera log de la ejecuci�n
    If wvarLoguear Then
            wvarNroArch = FreeFile()
            wvarExisteArchivo = ""
            wvarExisteArchivo = Dir(App.Path & "\LOG\" & wvarNomArchivo)
            
            If wvarExisteArchivo <> "" Then Kill App.Path & "\LOG\" & wvarNomArchivo
    
            Open App.Path & "\LOG\" & wvarNomArchivo For Binary As wvarNroArch
            
            wvarTextoALoguear = "<LOGs><LOG InicioConsulta=""" & wvarHoraInicio & """ FinConsulta=""" & Now() & """ TiempoIncurrido=""" & Timer - wvarTiempo & " seg""" & ">" & _
                                "<AreaIn>" & wobjXMLInputSOAP.xml & "</AreaIn>" & _
                                "<AreaOut>" & wobjXMLOutputSOAP & "</AreaOut>" & _
                                "</LOG></LOGs>"
                                
            Put wvarNroArch, , wvarTextoALoguear
            Close
    End If
    '
    pvarResponse = "<Response><Estado resultado='true' />" & wobjXMLOutputSOAP & "</Response>"
    '
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLInputSOAP = Nothing
    Set wobjNodoBase64 = Nothing
    Set wobjXMLDefinition = Nothing
    Set wobjXMLConfig = Nothing
    Set wobjNodosEntrada = Nothing
    Set wobjNodoEntrada = Nothing
    Set wobjNodo = Nothing
    '
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                  mcteClassName, _
                  wcteFnName, _
                  wvarStep, _
                  Err.Number, _
                  "Error= [" & Err.Number & "] - " & _
                          Err.Description, _
                  vbLogEventTypeError
    '
    pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & Err.Description & Chr(34) & " /></Response>"
    '
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    '
End Function


Private Sub ObjectControl_Activate()

    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")


End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

Private Function PostWebService(ByVal pvarURL As String, ByVal pvarSoapActionUrl As String, ByVal pvarXmlBody As String) As String
    Dim mobjDom As Object
    Dim mobjXmlHttp As Object
    Dim mvarStrRet As String
    Dim mvarintPos1 As Integer
    Dim mvarintPos2 As Integer
    '
    ' Crea objetos DOMDocument y XMLHTTP
    Set mobjDom = CreateObject("MSXML2.DOMDocument")
    Set mobjXmlHttp = CreateObject("MSXML2.XMLHTTP")
    '
    ' carga XML
    mobjDom.async = False
    mobjDom.loadXML pvarXmlBody
    '
    ' Abre el webservice
    mobjXmlHttp.Open "POST", pvarURL, False
    '
    ' Crea encabezados
    mobjXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    mobjXmlHttp.setRequestHeader "SOAPAction", pvarSoapActionUrl
    '
    ' Envia XML
    mobjXmlHttp.send mobjDom.xml
    '
    ' Obtiene la respuesta del envio
    mvarStrRet = mobjXmlHttp.responseText
    '
    ' Cierra el objeto
    Set mobjXmlHttp = Nothing
    '
    ' Extrae el resultado
    mvarintPos1 = InStr(mvarStrRet, "Result>") + 7
    mvarintPos2 = InStr(mvarStrRet, "</")
    If mvarintPos1 > 7 And mvarintPos2 > 0 Then
        mvarStrRet = Mid(mvarStrRet, mvarintPos1, mvarintPos2 - mvarintPos1)
    End If
    '
    ' Devuelve el resultado
    PostWebService = mvarStrRet
End Function


