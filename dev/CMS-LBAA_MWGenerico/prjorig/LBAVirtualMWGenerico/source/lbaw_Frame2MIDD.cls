VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_Frame2MIDD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context As ObjectContext
Private mobjEventLog    As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements HSBCInterfaces.IAction
Implements ObjectControl

'Datos de la accion
Const mcteClassName             As String = "lbawA_MWGenerico.lbaw_Frame2MIDD"
'
Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName         As String = "IAction_Execute"
    '
    Dim wvarRequest          As String
    Dim wvarResponse         As String
    Dim wobjXMLRequest       As MSXML2.DOMDocument
    Dim wobjXMLResponse      As MSXML2.DOMDocument
    Dim wobjFrame2MIDD       As HSBCInterfaces.IAction
    Dim wvarStep             As Long
    Dim wvarError            As Long
    Dim wvarOrigenError      As String
    Dim wvarRtaCOM           As String
    Dim v1, v2, v3           As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'Carga el request
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    Call wobjXMLRequest.loadXML(pvarRequest)
    '
    'Ejecuta el Frame2MIDD
    wvarStep = 20
    Set wobjFrame2MIDD = mobjCOM_Context.CreateInstance(gcteClassFrame2MIDD)
    wvarError = wobjFrame2MIDD.Execute(wobjXMLRequest.xml, wvarResponse, "")
    Set wobjFrame2MIDD = Nothing
    '
    'Carga la respuesta para anlizarla
    wvarStep = 30
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    wobjXMLResponse.async = False
    Call wobjXMLResponse.loadXML(wvarResponse)
    '
    wvarStep = 40
    'Error (ya sea de la Frame2Midd o de Middleware)
    If (wvarError <> 0) Then
        '
        wvarStep = 50
        wvarOrigenError = wobjXMLResponse.selectSingleNode("GENERAL/ESTADO/@SOURCE").Text
        '
        wvarStep = 60
        If Not wobjXMLResponse.selectSingleNode("GENERAL/ESTADO/@MENSAJE") Is Nothing Then
            wvarRtaCOM = wobjXMLResponse.selectSingleNode("GENERAL/ESTADO/@MENSAJE").Text
        Else
            wvarRtaCOM = "El COM+ " & gcteClassFrame2MIDD & " pinch�."
        End If
        '
        Select Case wvarOrigenError
            '
            Case "WDFRAME2MIDD" 'Error de la Frame2midd
                '
                wvarStep = 70
                mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                                 mcteClassName, _
                                 wcteFnName, _
                                 wvarStep, _
                                 wvarError, _
                                 "Error= [" & wvarError & "] - MENSAJE: " & wvarRtaCOM, _
                                 vbLogEventTypeError
                pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "WD.Frame2MIDD devolvi� un error." & Chr(34) & " />" & wvarResponse & "</Response>"
                '
            Case "MIDD" 'Error de Middleware
                '
                wvarStep = 80
                If Not wobjXMLResponse.selectSingleNode("GENERAL/ESTADO/@op") Is Nothing Then
                    v1 = "@op: " & wobjXMLResponse.selectSingleNode("GENERAL/ESTADO/@op").Text
                Else
                    v1 = "@op: no vino"
                End If
                '
                wvarStep = 90
                If Not wobjXMLResponse.selectSingleNode("GENERAL/ESTADO/@RspCde") Is Nothing Then
                    v2 = "@RspCde: " & wobjXMLResponse.selectSingleNode("GENERAL/ESTADO/@RspCde").Text
                Else
                    v2 = "@RspCde: no vino"
                End If
                '
                wvarStep = 100
                If Not wobjXMLResponse.selectSingleNode("GENERAL/ESTADO/@Result") Is Nothing Then
                    v3 = "@Result: " & wobjXMLResponse.selectSingleNode("GENERAL/ESTADO/@Result").Text
                Else
                    v3 = "@Result: no vino"
                End If
                '
                wvarStep = 110
                mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                                 mcteClassName, _
                                 wcteFnName, _
                                 wvarStep, _
                                 0, _
                                 "Info= " & v1 & " | " & v2 & " | " & v3 & " MENSAJE: " & wvarResponse, _
                                 vbLogEventTypeInformation
                pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "Middleware contest� con un error controlado." & Chr(34) & " />" & wvarResponse & "</Response>"
                '
        End Select
        '
    Else
        'TODO OK
        wvarStep = 120
        If (wvarError = 0) Then
            pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResponse & "</Response>"
        End If
        '
    End If
    '
    wvarStep = 130
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    '
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    Exit Function
                     
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    '
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    '
End Function

Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub


