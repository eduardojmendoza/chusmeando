package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_SOAPMW implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   */
  static final String mcteClassName = "lbawA_MWGenerico.lbaw_SOAPMW";
  static final String mcteSubDirName = "Definiciones";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private String mvarConsultaRealizada = "";
  private boolean mvarCancelacionManual = false;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   */
  public int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarHoraInicio = "";
    float wvarTiempo = 0;
    String wvarDefinitionFile = "";
    String wvarTextoError = "";
    String wvarURLWS = "";
    String wvarSoapActionURL = "";
    boolean wvarLoguear = false;
    String wvarNomArchivo = "";
    String wvarCDATA = "";
    String wvarNodo = "";
    String wvarDatos = "";
    String wvarTextoALoguear = "";
    int wvarx = 0;
    String wvarTimeStamp = "";
    String wobjXMLOutputSOAP = "";
    int wvarNroArch = 0;
    String wvarExisteArchivo = "";
    String wvarConfFileName = "";
    diamondedge.util.XmlDom wobjXMLDefinition = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLInputSOAP = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLAux = null;
    org.w3c.dom.Element wobjNodoBase64 = null;
    org.w3c.dom.NodeList wobjNodosEntrada = null;
    org.w3c.dom.Node wobjNodoEntrada = null;
    com.qbe.services.HSBCInterfaces.IAction wobjFrame2MQ = null;
    org.w3c.dom.Element wobjNodo = null;
    com.qbe.services.Variant[] warrayDatos = (com.qbe.services.Variant[]) VB.initArray( new com.qbe.services.Variant[Strings.len( wvarDatos )+1], com.qbe.services.Variant.class );
    //
    //
    //
    //
    try 
    {
      wvarHoraInicio = DateTime.format( DateTime.now() );
      wvarTiempo = DateTime.timer();
      //
      wvarStep = 10;
      // Crea Objetos XMLDom -----------------------------
      wobjXMLRequest = new diamondedge.util.XmlDom();
      wobjXMLDefinition = new diamondedge.util.XmlDom();
      wobjXMLInputSOAP = new diamondedge.util.XmlDom();
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //-----------------------------------------------------
      //
      wvarStep = 20;
      // Carga XML del Request ASP---------------------------
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //-----------------------------------------------------
      //
      // Carga Nombre de Archivo de Definici�n------------
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//DEFINICION" ) */ == (org.w3c.dom.Node) null) )
      {
        wvarDefinitionFile = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//DEFINICION" ) */ );
      }
      else
      {
        wvarTextoError = " No se encontr� nodo DEFINICION en Request o XML mal formado";
        Err.raise( -1, "", wvarTextoError );
      }
      //----------------------------------------------------
      //
      // Carga Archivo de Definici�n----------------------------
      wvarStep = 30;
      //unsup wobjXMLDefinition.async = false;
      wobjXMLDefinition.load( System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + wvarDefinitionFile );
      if( wobjXMLDefinition.getParseError().getErrorCode() != 0 )
      {
        Err.raise( wobjXMLDefinition.getParseError().getErrorCode(), "", wobjXMLDefinition.getParseError().getMessage() );
      }
      wvarStep = 35;
      //
      // Carga Nombre de Archivo de Definici�n SOAP --------------------
      wvarConfFileName = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/SOAPCONFIGFILE" ) */ );
      wvarStep = 40;
      //
      // Carga Archivo de Configuraci�n SOAP --------------------------
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + wvarConfFileName );
      wvarStep = 45;
      //
      // Carga la URL del WebService y le agrega el server --------------------
      wvarURLWS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//SERVER" ) */ ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/URL" ) */ );
      wvarStep = 50;
      //
      // Carga el valor de SOAP ACTION ------------------------------
      wvarSoapActionURL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/SOAPACTION" ) */ );
      wvarStep = 55;
      //
      // Carga nombre Archivo de Logueo
      wvarLoguear = false;
      //
      if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" ) */ == (org.w3c.dom.Node) null) )
      {
        wvarLoguear = true;
        if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" ) */.getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
        {
          wvarNomArchivo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" ) */.getAttributes().getNamedItem( "LogFile" ) );
        }
      }
      //
      wvarStep = 80;
      // carga Envelope de xml definicion
      //unsup wobjXMLInputSOAP.async = false;
      wobjXMLInputSOAP.loadXML( /*unsup wobjXMLDefinition.selectSingleNode( "//XMLDEFINICION" ) */.getFirstChild().toString() );
      //unsup wobjXMLInputSOAP.resolveExternals = true;
      //
      // Carga los Parametos de Entrada desde la Definicion-------------
      //
      wobjNodosEntrada = null /*unsup wobjXMLDefinition.selectNodes( "//ENTRADA/PARAMETRO" ) */;
      wvarStep = 90;
      //
      // Recorre los Parametros----------------------------------------
      wvarCDATA = "NO";
      for( int nwobjNodoEntrada = 0; nwobjNodoEntrada < wobjNodosEntrada.getLength(); nwobjNodoEntrada++ )
      {
        wobjNodoEntrada = wobjNodosEntrada.item( nwobjNodoEntrada );
        //Que existan todos los nodos definidos, en el Request
        if( (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) )) ) */ == (org.w3c.dom.Node) null) && (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == (org.w3c.dom.Node) null) )
        {
          wvarTextoError = " No se encontr� nodo " + diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) + " en Request";
          Err.raise( -1, "", wvarTextoError );
        }
      }
      //
      for( int nwobjNodoEntrada = 0; nwobjNodoEntrada < wobjNodosEntrada.getLength(); nwobjNodoEntrada++ )
      {
        wobjNodoEntrada = wobjNodosEntrada.item( nwobjNodoEntrada );
        
        if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "BASE64" ) )
        {
          wvarNodo = diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) );
          wvarDatos = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" + null /*unsup wobjXMLRequest.selectSingleNode( "//" + diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) ) */.getFirstChild().toString();
          // Redimensiona Array de Bytes para alojar datos a convertir a BASE64
          for( wvarx = 1; wvarx <= Strings.len( wvarDatos ); wvarx++ )
          {
            warrayDatos[wvarx - 1].set( (byte) Strings.asc( Strings.mid( wvarDatos, wvarx, 1 ) ) );
          }
          wobjNodoBase64 = (org.w3c.dom.Element) null /*unsup wobjXMLInputSOAP.selectSingleNode( "//" + wvarNodo ) */;
          wobjNodoBase64.setAttribute( "xmlns:dt", "urn:schemas-microsoft-com:datatypes" );
          //unsup wobjNodoBase64.dataType = "bin.base64";
          //unsup wobjNodoBase64.nodeTypedValue = warrayDatos;
          wobjNodoBase64 = (org.w3c.dom.Element) null;
        }
        else if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "TEXTO" ) )
        {
          wvarNodo = diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) );
          if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == (org.w3c.dom.Node) null) )
          {
            wvarDatos = diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) );
          }
          else
          {
            wvarDatos = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) ) */ );
          }
          wobjNodo = (org.w3c.dom.Element) null /*unsup wobjXMLInputSOAP.selectSingleNode( "//" + wvarNodo ) */;
          //unsup wobjNodo.nodeTypedValue = wvarDatos;
          wobjNodo = (org.w3c.dom.Element) null;
        }
        else if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "XML" ) )
        {
          wvarNodo = diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) );
          if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == (org.w3c.dom.Node) null) )
          {
            wvarDatos = diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) );
          }
          else
          {
            wvarDatos = null /*unsup wobjXMLRequest.selectSingleNode( "//" + diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) ) */.getFirstChild().toString();
          }
          wobjXMLAux = new diamondedge.util.XmlDom();
          wobjXMLAux.loadXML( "<ROOT>" + wvarDatos + "</ROOT>" );
          wobjNodo = (org.w3c.dom.Element) null /*unsup wobjXMLInputSOAP.selectSingleNode( "//" + wvarNodo ) */;
          wobjNodo.appendChild( /*unsup wobjXMLAux.selectSingleNode( "//ROOT" ) */.getFirstChild() );
          wobjNodo = (org.w3c.dom.Element) null;
          wobjXMLAux = (diamondedge.util.XmlDom) null;
        }
        else
        {
        }
      }
      //
      wvarStep = 100;
      //wvarTimeStamp = Year(Now()) & "-" & Right("00" & Month(Now()), 2) & "-" & Right("00" & Day(Now()), 2) & "T" & Right("00" & Hour(Now()), 2) & ":" & Right("00" & Minute(Now()), 2) & ":" & Right("00" & Second(Now()), 2) & ".000000-03:00"
      //wobjXMLInputSOAP.selectSingleNode("//MsgCreatTmsp").Text = wvarTimeStamp
      //
      wvarStep = 130;
      //Env�a el mensaje por SOAP
      wobjXMLOutputSOAP = invoke( "PostWebService", new Variant[] { new Variant(wvarURLWS), new Variant(wvarSoapActionURL), new Variant(wobjXMLInputSOAP.getDocument().getDocumentElement().toString()) } );
      //
      wvarStep = 140;
      // Genera log de la ejecuci�n
      if( wvarLoguear )
      {
        wvarNroArch = FileSystem.getFreeFile();
        wvarExisteArchivo = "";
        wvarExisteArchivo = "" /*unsup this.Dir( System.getProperty("user.dir") + "\\LOG\\" + wvarNomArchivo, 0 ) */;

        if( !wvarExisteArchivo.equals( "" ) )
        {
          FileSystem.kill( System.getProperty("user.dir") + "\\LOG\\" + wvarNomArchivo );
        }

        //error: Opening files for 'Binary' access is not supported.
        //unsup: Open App.Path & "\LOG\" & wvarNomArchivo For Binary As wvarNroArch
        wvarTextoALoguear = "<LOGs><LOG InicioConsulta=\"" + wvarHoraInicio + "\" FinConsulta=\"" + DateTime.now() + "\" TiempoIncurrido=\"" + (DateTime.timer() - wvarTiempo) + " seg\"" + ">" + "<AreaIn>" + wobjXMLInputSOAP.getDocument().getDocumentElement().toString() + "</AreaIn>" + "<AreaOut>" + wobjXMLOutputSOAP + "</AreaOut>" + "</LOG></LOGs>";

        /*unsup this.Put( wvarNroArch, 0, new Variant( wvarTextoALoguear ) ) */;
        FileSystem.close();
      }
      //
      pvarResponse.set( "<Response><Estado resultado='true' />" + wobjXMLOutputSOAP + "</Response>" );
      //
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLInputSOAP = (diamondedge.util.XmlDom) null;
      wobjNodoBase64 = (org.w3c.dom.Element) null;
      wobjXMLDefinition = (diamondedge.util.XmlDom) null;
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      wobjNodosEntrada = (org.w3c.dom.NodeList) null;
      wobjNodoEntrada = (org.w3c.dom.Node) null;
      wobjNodo = (org.w3c.dom.Element) null;
      //
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + Err.getError().getDescription() + String.valueOf( (char)(34) ) + " /></Response>" );
        //
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String PostWebService( String pvarURL, String pvarSoapActionUrl, String pvarXmlBody ) throws Exception
  {
    String PostWebService = "";
    diamondedge.util.XmlDom mobjDom = null;
    MSXML2.XMLHTTP mobjXmlHttp = new MSXML2.XMLHTTP();
    String mvarStrRet = "";
    int mvarintPos1 = 0;
    int mvarintPos2 = 0;
    //
    // Crea objetos DOMDocument y XMLHTTP
    mobjDom = new diamondedge.util.XmlDom();
    mobjXmlHttp = new MSXML2.XMLHTTP();
    //
    // carga XML
    //unsup mobjDom.async = false;
    mobjDom.loadXML( pvarXmlBody );
    //
    // Abre el webservice
    mobjXmlHttp.Open( "POST", pvarURL, false );
    //
    // Crea encabezados
    mobjXmlHttp.setRequestHeader( "Content-Type", "text/xml; charset=utf-8" );
    mobjXmlHttp.setRequestHeader( "SOAPAction", pvarSoapActionUrl );
    //
    // Envia XML
    mobjXmlHttp.send( mobjDom.getDocument().getDocumentElement().toString() );
    //
    // Obtiene la respuesta del envio
    mvarStrRet = mobjXmlHttp.responseText.toString();
    //
    // Cierra el objeto
    mobjXmlHttp = (MSXML2.XMLHTTP) null;
    //
    // Extrae el resultado
    mvarintPos1 = Strings.find( mvarStrRet, "Result>" ) + 7;
    mvarintPos2 = Strings.find( mvarStrRet, "</" );
    if( (mvarintPos1 > 7) && (mvarintPos2 > 0) )
    {
      mvarStrRet = Strings.mid( mvarStrRet, mvarintPos1, mvarintPos2 - mvarintPos1 );
    }
    //
    // Devuelve el resultado
    PostWebService = mvarStrRet;
    return PostWebService;
  }

  public void Activate() throws Exception
  {

    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();


  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
