package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * ******************************************************************************
 * COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
 * LIMITED 2007. ALL RIGHTS RESERVED
 * This software is only to be used for the purpose for which it has been provided.
 * No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
 * system or translated in any human or computer language in any way or for any other
 * purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
 * Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
 * offence, which can result in heavy fines and payment of substantial damages
 * ******************************************************************************
 * Nombre del Modulo: lbaw_MQMW
 * Fecha de Creaci�n: 23/07/2007
 * PPcR: xxxxxxx -x
 * Desarrollador: Gabriel E. D'Agnone
 * Descripci�n:  Envia xml soap hacia Middleware y retorna PDF via MQ 
 * Objetos del FrameWork
 */

public class lbaw_MQMW implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   */
  static final String gcteQueueManager = "//QUEUEMANAGER";
  static final String gctePutQueue = "//PUTQUEUE";
  static final String gcteGetQueue = "//GETQUEUE";
  static final String gcteGMOWaitInterval = "//GMO_WAITINTERVAL";
  static final String gcteClassMQConnection = "WD.Frame2MQ";
  static final String mcteClassName = "lbawA_MWGenerico.lbaw_MQMW";
  static final String mcteSubDirName = "Definiciones";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private String mvarConsultaRealizada = "";
  private boolean mvarCancelacionManual = false;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   */
  public int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarGMOWaitInterval = 0;
    int wvarStep = 0;
    int wvarx = 0;
    int wvarNroArch = 0;
    byte[] warrayDatos = null;
    String wvarDatosPDF = "";
    String wvarRaiz = "";
    String wvarInputMQ = "";
    String wvarError = "";
    String wvarTextoError = "";
    String wvarUsuario = "";
    String wvarDefinitionFile = "";
    String wvarConfFileName = "";
    String wvarTimeStamp = "";
    String wvarCDATA = "";
    String wvarLoguear = "";
    String wvarNomArchivo = "";
    String wvarTextoALoguear = "";
    String wvarExisteArchivo = "";
    String wvarHoraInicio = "";
    String wvarRespuestaFiltrada = "";
    String wvarPasada1 = "";
    String wvarPasada2 = "";
    float wvarTiempo = 0;
    diamondedge.util.XmlDom wobjXMLDefinition = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLInputMQ = null;
    diamondedge.util.XmlDom wobjXMLOutputMQ = null;
    diamondedge.util.XmlDom wvarXMLPDF = null;
    diamondedge.util.XmlDom wobjXMLAux = null;
    org.w3c.dom.Element wobjNodoBase64 = null;
    org.w3c.dom.Element wobjNodo = null;
    org.w3c.dom.Element wobjNodoAux = null;
    org.w3c.dom.CDATASection wobjCdataNodo = null;
    org.w3c.dom.NodeList wobjNodosEntrada = null;
    org.w3c.dom.Element wobjXMLSobre = null;
    org.w3c.dom.Node wobjNodoEntrada = null;
    String wvarFaultCode = "";
    String wvarNodoPDF = "";
    String wvarNodoRaiz = "";
    String RespuestaMQ = "";
    int wvarMQError = 0;
    Object wobjFrame2MQ = null;














    try 
    {
      wvarHoraInicio = DateTime.format( DateTime.now() );
      wvarTiempo = DateTime.timer();

      wvarStep = 10;

      // Crea Objetos XMLDom -----------------------------
      wobjXMLRequest = new diamondedge.util.XmlDom();
      wobjXMLDefinition = new diamondedge.util.XmlDom();
      wobjXMLInputMQ = new diamondedge.util.XmlDom();
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //-----------------------------------------------------
      wvarStep = 20;
      // Carga XML del Request ASP---------------------------
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //-----------------------------------------------------
      // Carga Nombre de Archivo de Definici�n------------
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//DEFINICION" ) */ == (org.w3c.dom.Node) null) )
      {
        wvarDefinitionFile = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//DEFINICION" ) */ );
      }
      else
      {
        wvarTextoError = " No se encontr� nodo DEFINICION en Request o XML mal formado";
        //unsup GoTo ManejoError
      }
      //----------------------------------------------------
      // Carga Archivo de Definici�n----------------------------
      //unsup wobjXMLDefinition.async = false;
      wobjXMLDefinition.load( System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + wvarDefinitionFile );
      wvarStep = 30;

      // Carga Nombre de Archivo de Definici�n MQ --------------------
      wvarConfFileName = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/MQCONFIGFILE" ) */ );
      wvarStep = 40;

      // Carga Tiempo de time-out MQ------------------------------
      wvarGMOWaitInterval = Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/TIMEOUT" ) */ ) );
      wvarStep = 50;

      // Carga Archivo de Configuraci�n MQ --------------------------
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + wvarConfFileName );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( gcteGMOWaitInterval ) */, String.valueOf( wvarGMOWaitInterval ) );

      // Carga nombre Archivo de Logueo
      wvarLoguear = "NO";

      if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" ) */ == (org.w3c.dom.Node) null) )
      {
        wvarLoguear = "SI";
        if( ! (null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" ) */.getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
        {
          wvarNomArchivo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" ) */.getAttributes().getNamedItem( "LogFile" ) );
        }
      }

      // Carga Nodo Sobre de la Definici�n--------------------------
      wvarStep = 60;
      wobjXMLSobre = (org.w3c.dom.Element) null /*unsup wobjXMLDefinition.selectSingleNode( "//XMLDEFINICION" ) */.getFirstChild();

      wvarStep = 70;
      // Carga el Sobre en la variable de Input a MQ---------------------
      wvarInputMQ = "<" + wobjXMLSobre.getTagName() + ">" + wobjXMLSobre.getNodeValue() + "</" + wobjXMLSobre.getTagName() + ">";

      wvarStep = 80;
      // carga Envelope de xml definicion
      //unsup wobjXMLInputMQ.async = false;
      wobjXMLInputMQ.loadXML( wvarInputMQ );
      //unsup wobjXMLInputMQ.resolveExternals = true;

      // Carga los Parametos de Entrada desde la Definicion-------------
      wobjNodosEntrada = null /*unsup wobjXMLDefinition.selectNodes( "//ENTRADA/PARAMETRO" ) */;

      wvarStep = 90;

      // Recorre los Parametros----------------------------------------
      wvarCDATA = "NO";

      for( int nwobjNodoEntrada = 0; nwobjNodoEntrada < wobjNodosEntrada.getLength(); nwobjNodoEntrada++ )
      {
        wobjNodoEntrada = wobjNodosEntrada.item( nwobjNodoEntrada );

        //Que existan todos los nodos definidos, en el Request
        if( (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) )) ) */ == (org.w3c.dom.Node) null) && (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == (org.w3c.dom.Node) null) )
        {

          wvarTextoError = " No se encontr� nodo " + diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) + " en Request";

          //unsup GoTo ManejoError
        }
        else
        {

          //Que Exista Nodo Raiz
          if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "RAIZ" ) )
          {

            //MC - Agregado ----------------------------------------------------------
            if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == (org.w3c.dom.Node) null) )
            {

              wvarNodoRaiz = diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) );

            }
            else
            {

              wvarNodoRaiz = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) ) */ );

            }
            //MC - Fin Agregado ------------------------------------------------------
            if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "CDATA" ) == (org.w3c.dom.Node) null) )
            {

              wvarCDATA = diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "CDATA" ) );

            }

          }

        }

      }

      if( !wvarNodoRaiz.equals( "" ) )
      {

        for( int nwobjNodoEntrada = 0; nwobjNodoEntrada < wobjNodosEntrada.getLength(); nwobjNodoEntrada++ )
        {
          wobjNodoEntrada = wobjNodosEntrada.item( nwobjNodoEntrada );

          
          if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "BASE64" ) )
          {

            wvarNodoPDF = diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) );

            wvarDatosPDF = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" + null /*unsup wobjXMLRequest.selectSingleNode( "//" + diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) ) */.getFirstChild().toString();

            // Redimensiona Array de Bytes para alojar datos a convertir a BASE64
            warrayDatos = new byte[Strings.len( wvarDatosPDF )+1];

            for( wvarx = 1; wvarx <= Strings.len( wvarDatosPDF ); wvarx++ )
            {

              warrayDatos[wvarx - 1] = (byte) Strings.asc( Strings.mid( wvarDatosPDF, wvarx, 1 ) );

            }

            wobjNodoBase64 = wobjXMLInputMQ.getDocument().createElement( wvarNodoPDF );

            /*unsup wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz ) */.appendChild( wobjNodoBase64 );

            wobjNodoBase64.setAttribute( "xmlns:dt", "urn:schemas-microsoft-com:datatypes" );

            //unsup wobjNodoBase64.dataType = "bin.base64";

            //unsup wobjNodoBase64.nodeTypedValue = warrayDatos;

            wobjNodoBase64 = (org.w3c.dom.Element) null;

          }
          else if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "TEXTO" ) )
          {

            wvarNodoPDF = diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) );

            if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == (org.w3c.dom.Node) null) )
            {
              wvarDatosPDF = diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) );
            }
            else
            {
              wvarDatosPDF = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) ) */ );
            }

            wobjNodo = wobjXMLInputMQ.getDocument().createElement( wvarNodoPDF );
            /*unsup wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz ) */.appendChild( wobjNodo );
            //unsup wobjNodo.nodeTypedValue = wvarDatosPDF;
            wobjNodo = (org.w3c.dom.Element) null;

          }
          else if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "XML" ) )
          {

            wvarNodoPDF = diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) );

            if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == (org.w3c.dom.Node) null) )
            {
              wvarDatosPDF = diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) );
            }
            else
            {
              wvarDatosPDF = null /*unsup wobjXMLRequest.selectSingleNode( "//" + diamondedge.util.XmlDom.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) ) */.getFirstChild().toString();
            }
            //MHC: 16/01/2009 - Se corrige el agregado de XML al InputMQ
            wobjXMLAux = new diamondedge.util.XmlDom();
            wobjXMLAux.loadXML( "<ROOT>" + wvarDatosPDF + "</ROOT>" );
            wobjNodo = wobjXMLInputMQ.getDocument().createElement( wvarNodoPDF );
            /*unsup wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz ) */.appendChild( wobjNodo );
            wobjNodo.appendChild( /*unsup wobjXMLAux.selectSingleNode( "//ROOT" ) */.getFirstChild() );

            wobjNodo = (org.w3c.dom.Element) null;
            wobjXMLAux = (diamondedge.util.XmlDom) null;
          }
          else
          {

          }
        }
      }

      wvarStep = 100;

      // Reemplaza nodo Raiz por un CDATA
      if( Strings.toUpperCase( wvarCDATA ).equals( "SI" ) )
      {
        wvarRaiz = null /*unsup wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz ) */.toString();
        wobjCdataNodo = wobjXMLInputMQ.getDocument().createCDATASection( wvarRaiz );
        wobjNodo = (org.w3c.dom.Element) null /*unsup wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz ) */;
        /*unsup wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz ) */.getParentNode().replaceChild( wobjCdataNodo, wobjNodo );
      }

      wvarStep = 110;
      //---------------------------------------
      //Carga usuario ComPlus
      wvarUsuario = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//USERID" ) */ );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLInputMQ.selectSingleNode( "//UserId" ) */, wvarUsuario );
      //---------------------------------------
      wvarStep = 120;
      wvarTimeStamp = DateTime.year( DateTime.now() ) + "-" + Strings.right( ("00" + DateTime.month( DateTime.now() )), 2 ) + "-" + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) + "T" + Strings.right( ("00" + DateTime.hour( DateTime.now() )), 2 ) + ":" + Strings.right( ("00" + DateTime.minute( DateTime.now() )), 2 ) + ":" + Strings.right( ("00" + DateTime.second( DateTime.now() )), 2 ) + ".000000-03:00";
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLInputMQ.selectSingleNode( "//MsgCreatTmsp" ) */, wvarTimeStamp );

      wvarStep = 130;
      //Instancia y llama a MQ ---------------------------------
      wobjFrame2MQ = new gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wobjXMLInputMQ.xml, RespuestaMQ)
      wobjFrame2MQ = null;

      if( wvarMQError != 0 )
      {
        wvarStep = 131;

        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );

        //DA este c�digo es para que aparezca un log en el APP en caso de haber problemas al conectar con MQ de Middleware
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Logical" ), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + RespuestaMQ + " Area:" + " Hora:" + DateTime.now(), vbLogEventTypeError );

        //DA - 26/09/2007: se quita este c�digo ya que es un error controlado.
        //mobjCOM_Context.SetAbort
        //IAction_Execute = 1
      }
      else
      {
        wvarStep = 132;
        if( Strings.asc( Strings.right( RespuestaMQ, 1 ) ) == 0 )
        {
          wvarRespuestaFiltrada = Strings.left( RespuestaMQ, Strings.len( RespuestaMQ ) - 1 );
        }
        else
        {
          wvarRespuestaFiltrada = RespuestaMQ;
        }

        wvarStep = 133;
        wvarXMLPDF = new diamondedge.util.XmlDom();
        //unsup wvarXMLPDF.async = false;
        wvarXMLPDF.loadXML( wvarRespuestaFiltrada );

        wvarStep = 134;
        if( ! (null /*unsup wvarXMLPDF.selectSingleNode( ("//" + Strings.replace( wvarNodoRaiz, "share:", "" ) + "Return") ) */ == (org.w3c.dom.Node) null) )
        {
          wvarPasada1 = null /*unsup wvarXMLPDF.selectSingleNode( "//" + Strings.replace( wvarNodoRaiz, "share:", "" ) + "Return" ) */.toString();
        }
        else
        {
          wvarPasada1 = "";
        }

        wvarStep = 135;
        if( ! (null /*unsup wvarXMLPDF.selectSingleNode( "//faultstring" ) */ == (org.w3c.dom.Node) null) )
        {
          wvarFaultCode = "<faultstring>" + diamondedge.util.XmlDom.getText( null /*unsup wvarXMLPDF.selectSingleNode( "//faultstring" ) */ ) + "</faultstring>";
        }
        else
        {
          wvarFaultCode = "";
        }

        wvarStep = 136;
        pvarResponse.set( "<Response><Estado resultado='true' />" );
        pvarResponse.set( pvarResponse + wvarPasada1 );
        pvarResponse.set( pvarResponse + wvarFaultCode );
        pvarResponse.set( pvarResponse + "</Response>" );

        wvarStep = 137;

        //DA - 26/09/2007: Se manda mas abajo este c�digo
        //mobjCOM_Context.SetComplete
        //IAction_Execute = 0
      }

      wvarStep = 140;

      if( wvarLoguear.equals( "SI" ) )
      {
        wvarNroArch = FileSystem.getFreeFile();
        wvarExisteArchivo = "";
        wvarExisteArchivo = "" /*unsup this.Dir( System.getProperty("user.dir") + "\\LOG\\" + wvarNomArchivo, 0 ) */;

        if( !wvarExisteArchivo.equals( "" ) )
        {
          FileSystem.kill( System.getProperty("user.dir") + "\\LOG\\" + wvarNomArchivo );
        }

        //error: Opening files for 'Binary' access is not supported.
        //unsup: Open App.Path & "\LOG\" & wvarNomArchivo For Binary As wvarNroArch
        wvarTextoALoguear = "<LOGs><LOG InicioConsulta=\"" + wvarHoraInicio + "\" FinConsulta=\"" + DateTime.now() + "\" TiempoIncurrido=\"" + (DateTime.timer() - wvarTiempo) + " seg\"" + ">" + "<AreaIn>" + wobjXMLInputMQ.getDocument().getDocumentElement().toString() + "</AreaIn>" + "<AreaOut>" + RespuestaMQ + "</AreaOut>" + "</LOG></LOGs>";

        /*unsup this.Put( wvarNroArch, 0, new Variant( wvarTextoALoguear ) ) */;
        FileSystem.close();
      }

      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLInputMQ = (diamondedge.util.XmlDom) null;
      wobjNodoBase64 = (org.w3c.dom.Element) null;
      wobjXMLDefinition = (diamondedge.util.XmlDom) null;
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      wobjXMLSobre = (org.w3c.dom.Element) null;
      wobjNodosEntrada = (org.w3c.dom.NodeList) null;
      wobjNodoEntrada = (org.w3c.dom.Node) null;
      wobjNodo = (org.w3c.dom.Element) null;
      wobjXMLSobre = (org.w3c.dom.Element) null;


      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;

        //unsup Resume ClearObjects
        ManejoError: 

        wvarError = Err.getError().getDescription();

        pvarResponse.set( "<Response><Estado resultado='false' mensaje=' Linea: " + wvarStep + "-" + wvarError + wvarTextoError + " ' /></Response>" );

        //unsup GoTo ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   */
  public String DecodificaBase64( String pDatos ) throws Exception
  {
    String DecodificaBase64 = "";
    diamondedge.util.XmlDom wobjXMLPdf = null;
    org.w3c.dom.Node wobjoNode = null;
    ADODB.Stream wobjStream = new ADODB.Stream();
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    wobjXMLPdf = new diamondedge.util.XmlDom();
    //unsup wobjXMLPdf.async = false;
    wobjXMLPdf.loadXML( "<PDF xmlns:dt=\"urn:schemas-microsoft-com:datatypes\" dt:dt=\"bin.base64\">" + pDatos + "</PDF>" );

    wobjoNode = null /*unsup wobjXMLPdf.getDocument().getDocumentElement().selectSingleNode( "//PDF" ) */;

    wobjStream = new ADODB.Stream();
    wobjStream.Charset.set( "Windows-1252" );
    wobjStream.Mode.set( 0 );
    wobjStream.Type.set( 1 );
    wobjStream.Open();
    wobjStream.Write( new Variant() /*unsup wobjoNode.nodeTypedValue */ );
    wobjStream.Position.set( 0 );
    wobjStream.Type.set( 2 );
    DecodificaBase64 = wobjStream.ReadText.toString();
    wobjStream.Close();

    wobjXMLPdf = (diamondedge.util.XmlDom) null;
    wobjoNode = (org.w3c.dom.Node) null;
    wobjStream = (ADODB.Stream) null;

    return DecodificaBase64;
  }

  public void Activate() throws Exception
  {

    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();


  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
