package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_Frame2MIDD implements com.qbe.services.Variant, com.qbe.services.HSBCInterfaces.IAction, com.qbe.services.ObjectControl
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_MWGenerico.lbaw_Frame2MIDD";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   */
  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeInformation = new Variant();
    Variant vbLogEventTypeError = new Variant();
    String wvarRequest = "";
    String wvarResponse = "";
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    Object wobjFrame2MIDD = null;
    int wvarStep = 0;
    int wvarError = 0;
    String wvarOrigenError = "";
    String wvarRtaCOM = "";
    String v1 = "";
    String v2 = "";
    String v3 = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Carga el request
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //Ejecuta el Frame2MIDD
      wvarStep = 20;
      wobjFrame2MIDD = new ModGeneral.gcteClassFrame2MIDD();
      //error: function 'Execute' was not found.
      //unsup: wvarError = wobjFrame2MIDD.Execute(wobjXMLRequest.xml, wvarResponse, "")
      wobjFrame2MIDD = null;
      //
      //Carga la respuesta para anlizarla
      wvarStep = 30;
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( wvarResponse );
      //
      wvarStep = 40;
      //Error (ya sea de la Frame2Midd o de Middleware)
      if( wvarError != 0 )
      {
        //
        wvarStep = 50;
        wvarOrigenError = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@SOURCE" ) */ );
        //
        wvarStep = 60;
        if( ! (null /*unsup wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@MENSAJE" ) */ == (org.w3c.dom.Node) null) )
        {
          wvarRtaCOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@MENSAJE" ) */ );
        }
        else
        {
          wvarRtaCOM = "El COM+ " + ModGeneral.gcteClassFrame2MIDD + " pinch�.";
        }
        //
        
        if( wvarOrigenError.equals( "WDFRAME2MIDD" ) )
        {
          //Error de la Frame2midd
          //
          //
          wvarStep = 70;
          mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, wvarError, "Error= [" + wvarError + "] - MENSAJE: " + wvarRtaCOM, vbLogEventTypeError );
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "WD.Frame2MIDD devolvi� un error." + String.valueOf( (char)(34) ) + " />" + wvarResponse + "</Response>" );
          //
        }
        else if( wvarOrigenError.equals( "MIDD" ) )
        {
          //Error de Middleware
          //
          wvarStep = 80;
          if( ! (null /*unsup wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@op" ) */ == (org.w3c.dom.Node) null) )
          {
            v1 = "@op: " + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@op" ) */ );
          }
          else
          {
            v1 = "@op: no vino";
          }
          //
          wvarStep = 90;
          if( ! (null /*unsup wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@RspCde" ) */ == (org.w3c.dom.Node) null) )
          {
            v2 = "@RspCde: " + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@RspCde" ) */ );
          }
          else
          {
            v2 = "@RspCde: no vino";
          }
          //
          wvarStep = 100;
          if( ! (null /*unsup wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@Result" ) */ == (org.w3c.dom.Node) null) )
          {
            v3 = "@Result: " + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "GENERAL/ESTADO/@Result" ) */ );
          }
          else
          {
            v3 = "@Result: no vino";
          }
          //
          wvarStep = 110;
          mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, 0, "Info= " + v1 + " | " + v2 + " | " + v3 + " MENSAJE: " + wvarResponse, vbLogEventTypeInformation );
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "Middleware contest� con un error controlado." + String.valueOf( (char)(34) ) + " />" + wvarResponse + "</Response>" );
          //
        }
        //
      }
      else
      {
        //TODO OK
        wvarStep = 120;
        if( wvarError == 0 )
        {
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResponse + "</Response>" );
        }
        //
      }
      //
      wvarStep = 130;
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLResponse = (diamondedge.util.XmlDom) null;
      //
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        wobjXMLRequest = (diamondedge.util.XmlDom) null;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        //
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
