package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetBinPDF implements com.qbe.services.Variant, com.qbe.services.HSBCInterfaces.IAction, com.qbe.services.ObjectControl
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_MWGenerico.lbaw_GetBinPDF";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_FileName = "//RUTA";
  static final String mcteParam_MantenerArchivo = "//MANTENERARCHIVO";
  static final String mcteParam_SoloBuscarArchivo = "//SOLOBUSCARARCHIVO";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarResult = "";
    boolean wvarMarcaMantenerArch = false;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    org.w3c.dom.Element wobjEle = null;
    diamondedge.util.XmlDom wobjXMLServer = null;
    int wvarFileGet = 0;
    String wvarFileName = "";
    byte[] wvarArrBytes = null;
    int wvarCantidadIntentos = 0;
    int wvarCurrIntento = 0;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      wobjXMLResponse = new diamondedge.util.XmlDom();

      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarFileName = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FileName ) */ );
      wvarMarcaMantenerArch = ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MantenerArchivo ) */ == (org.w3c.dom.Node) null);
      //
      wvarStep = 20;
      //Set wobjXMLServer = CreateObject("MSXML2.DOMDocument")
      //wobjXMLServer.Load App.Path & "\LBAVirtualServerImpresion.xml"
      //wvarCantidadIntentos = Val(wobjXMLServer.selectSingleNode("//TIMEOUT").Text) * 1000 / 250
      //60 * 1000 / 250
      wvarCantidadIntentos = 240;
      //Set wobjXMLServer = Nothing
      //
      wvarStep = 21;
      //
      //Verifico que exista el archivo que se solicita por parámetro
      for( wvarCurrIntento = 1; wvarCurrIntento <= wvarCantidadIntentos; wvarCurrIntento++ )
      {
        //
        if( !"" /*unsup this.Dir( wvarFileName, 0 ) */.equals( "" ) )
        {
          break;
        }
        //Sleep (250) 'Espero de a 250ms para ver si lo termina de generar
        wvarCurrIntento = wvarCurrIntento + 1;
        //
      }
      //
      wvarStep = 22;
      //-------------------------------------------------------------------------------------------------------------------
      //// Metodos del Framework
      //-------------------------------------------------------------------------------------------------------------------
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, "", wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
