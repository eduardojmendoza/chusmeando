package com.qbe.services.lbawA_OVSQLCotizar.impl;
import java.sql.CallableStatement;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVGetCotiAUS;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import com.qbe.vbcompat.format.VBFixesUtil;
/**
 * 
 * Es igual al OVPutSOlicAUS salvo un par de parámetros. Refactorizar 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 15/09/2009 - LR
 * PPCR 50055/6010662 - Agregado de campos para suscribirse a NBWS y EPOLIZA
 * Campos usados(SN_NBWS, EMAIL_NBWS, SN_EPOLIZA, EMAIL_EPOLIZA, SN_PRESTAMAIL)
 *    13/10/2009 Por nueva definicion se agega PASSEPOL (Password de EPoliza)
 * 01/04/2008 - MC
 * PPCR 50016/6010344 - Agregado del campo EMPL para identificar si el vendedor es empleado del grupo.
 * 01/06/2007 - FO
 * PPCR 20060393-1 - Cambio de mensaje de descripcion de vehiculo. cambia de 35 a 80 posiciones.
 * 15/11/2006 - DA
 * AML (Lavado de Dinero) Datos exigidos x la UIF: Se agregan los siguientes campos
 * 
 * SWAPODER
 * OCUPACODCLIE
 * OCUPACODAPOD
 * CLIENSECAPOD
 * DOMISECAPOD
 * Objetos del FrameWork
 */

public class lbaw_OVUpdSolicAUS implements VBObjectClass
{
	
	private static Logger logger = Logger.getLogger(lbaw_OVUpdSolicAUS.class.getName());
	
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
	static final String mcteClassName = "lbawA_OVSQLCotizar.lbaw_OVUpdSolicAUS";
	static final String mcteStoreProc = "SPSNCV_PROD_SOLI_UPDATE_V1";
  /**
   *  Parametros XML de Entrada
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_SUPLENUM = "//SUPLENUM";
  static final String mcteParam_FRANQCOD = "//FRANQCOD";
  static final String mcteParam_PQTDES = "//PQTDES";
  static final String mcteParam_PLANCOD = "//PLANCOD";
  static final String mcteParam_HIJOS1416 = "//HIJOS1416";
  static final String mcteParam_HIJOS1729 = "//HIJOS1729";
  static final String mcteParam_ZONA = "//ZONA";
  static final String mcteParam_CODPROV = "//CODPROV";
  static final String mcteParam_SUMALBA = "//SUMALBA";
  static final String mcteParam_CLIENIVA = "//CLIENIVA";
  static final String mcteParam_SUMASEG = "//SUMASEG";
  static final String mcteParam_CLUB_LBA = "//CLUB_LBA";
  static final String mcteParam_DEST80 = "//DESTRUCCION_80";
  static final String mcteParam_CPAANO = "//CPAANO";
  static final String mcteParam_CTAKMS = "//CTAKMS";
  static final String mcteParam_ESCERO = "//ESCERO";
  static final String mcteParam_TIENEPLAN = "//TIENEPLAN";
  static final String mcteParam_COND_ADIC = "//COND_ADIC";
  static final String mcteParam_ASEG_ADIC = "//ASEG_ADIC";
  static final String mcteParam_FH_NAC = "//FH_NAC";
  static final String mcteParam_SEXO = "//SEXO";
  static final String mcteParam_ESTCIV = "//ESTCIV";
  static final String mcteParam_SIFMVEHI_DES = "//SIFMVEHI_DES";
  static final String mcteParam_PROFECOD = "//PROFECOD";
  static final String mcteParam_SIACCESORIOS = "//SIACCESORIOS";
  static final String mcteParam_REFERIDO = "//REFERIDO";
  static final String mcteParam_MotorNum = "//MOTORNUM";
  static final String mcteParam_CHASINUM = "//CHASINUM";
  static final String mcteParam_PatenNum = "//PATENNUM";
  static final String mcteParam_AUMARCOD = "//AUMARCOD";
  static final String mcteParam_AUMODCOD = "//AUMODCOD";
  static final String mcteParam_AUSUBCOD = "//AUSUBCOD";
  static final String mcteParam_AUADICOD = "//AUADICOD";
  static final String mcteParam_AUMODORI = "//AUMODORI";
  static final String mcteParam_AUUSOCOD = "//AUUSOCOD";
  static final String mcteParam_AUVTVCOD = "//AUVTVCOD";
  static final String mcteParam_AUVTVDIA = "//AUVTVDIA";
  static final String mcteParam_AUVTVMES = "//AUVTVMES";
  static final String mcteParam_AUVTVANN = "//AUVTVANN";
  static final String mcteParam_VEHCLRCOD = "//VEHCLRCOD";
  static final String mcteParam_AUKLMNUM = "//AUKLMNUM";
  static final String mcteParam_FABRICAN = "//FABRICAN";
  static final String mcteParam_FABRICMES = "//FABRICMES";
  static final String mcteParam_GUGARAGE = "//GUGARAGE";
  static final String mcteParam_GUDOMICI = "//GUDOMICI";
  static final String mcteParam_AUCATCOD = "//AUCATCOD";
  static final String mcteParam_AUTIPCOD = "//AUTIPCOD";
  static final String mcteParam_AUCIASAN = "//AUCIASAN";
  static final String mcteParam_AUANTANN = "//AUANTANN";
  static final String mcteParam_AUNUMSIN = "//AUNUMSIN";
  static final String mcteParam_AUNUMKMT = "//AUNUMKMT";
  static final String mcteParam_AUUSOGNC = "//AUUSOGNC";
  static final String mcteParam_USUARCOD = "//USUARCOD";
  static final String mcteParam_SITUCPOL = "//SITUCPOL";
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_NUMEDOCU = "//NUMEDOCU";
  static final String mcteParam_TIPODOCU = "//TIPODOCU";
  static final String mcteParam_DOMICSEC = "//DOMICSEC";
  static final String mcteParam_CUENTSEC = "//CUENTSEC";
  static final String mcteParam_COBROFOR = "//COBROFOR";
  static final String mcteParam_EDADACTU = "//EDADACTU";
  /**
   *  Coberturas los devuelve el mensaje de Cotizacion y son 20 iteraciones
   */
  static final String mcteParam_COT_NRO = "//COT_NRO";
  static final String mcteParam_COBERCOD = "//COBERCOD";
  static final String mcteParam_COBERORD = "//COBERORD";
  static final String mcteParam_CAPITASG = "//CAPITASG";
  static final String mcteParam_CAPITIMP = "//CAPITIMP";
  /**
   *  Asegurados Adicionales ...son 10 iteraciones
   */
  static final String mcteParam_ASEGURADOS = "//ASEGURADOS/ASEGURADO";
  static final String mcteParam_DOCUMDAT = "DOCUMDAT";
  static final String mcteParam_NOMBREAS = "NOMBREAS";
  /**
   *  Datos Comunes
   */
  static final String mcteParam_CAMP_CODIGO = "//CAMP_CODIGO";
  static final String mcteParam_CAMP_DESC = "//CAMP_DESC";
  static final String mcteParam_LEGAJO_GTE = "//LEGAJO_GTE";
  static final String mcteParam_NRO_PROD = "//NRO_PROD";
  static final String mcteParam_EMPRESA_CODIGO = "//EMPRESA_CODIGO";
  static final String mcteParam_SUCURSAL_CODIGO = "//SUCURSAL_CODIGO";
  static final String mcteParam_LEGAJO_VEND = "//LEGAJO_VEND";
  static final String mcteParam_EMPL = "//EMPL";
  static final String mcteParam_TIPO_PROD = "//AGENTCLA";
  /**
   *  Hijos ...son 10 iteraciones
   */
  static final String mcteParam_CONDUCTORES = "//HIJOS/HIJO";
  static final String mcteParam_CONDUAPE = "APELLIDOHIJO";
  static final String mcteParam_CONDUNOM = "NOMBREHIJO";
  static final String mcteParam_CONDUFEC = "NACIMHIJO";
  static final String mcteParam_CONDUSEX = "SEXOHIJO";
  static final String mcteParam_CONDUEST = "ESTADOHIJO";
  static final String mcteParam_CONDUEXC = "INCLUIDO";
  /**
   *  Accesorios ...son 10 iteraciones
   */
  static final String mcteParam_ACCESORIOS = "//ACCESORIOS/ACCESORIO";
  static final String mcteParam_AUACCCOD = "CODIGOACC";
  static final String mcteParam_AUVEASUM = "PRECIOACC";
  static final String mcteParam_AUVEADES = "DESCRIPCIONACC";
  /**
   *  Inspeccion
   * "//SITUCEST"
   */
  static final String mcteParam_ESTAINSP = "//ESTAINSP";
  static final String mcteParam_INSPECOD = "//INSPECOD";
  static final String mcteParam_INSPEDES = "//INSPECTOR";
  static final String mcteParam_INSPADOM = "//INSPEADOM";
  static final String mcteParam_INSPEOBS = "//OBSERTXT";
  /**
   * "//INSPENUM"
   */
  static final String mcteParam_INSPEKMS = "//INSPEKMS";
  static final String mcteParam_CENSICOD = "//CENTRCOD_INS";
  static final String mcteParam_CENSIDES = "//CENTRDES";
  /**
   * Const mcteParam_INSPECC               As String = "//INSPECC"
   */
  static final String mcteParam_DISPOCOD = "//RASTREO";
  static final String mcteParam_PRESTCOD = "//ALARMCOD";
  static final String mcteParam_INSPEANN = "//INSPEANN";
  static final String mcteParam_INSPEMES = "//INSPEMES";
  static final String mcteParam_INSPEDIA = "//INSPEDIA";
  /**
   *  Acreedor Prendario
   */
  static final String mcteParam_NUMEDOCU_ACRE = "//NUMEDOCU_ACRE";
  static final String mcteParam_TIPODOCU_ACRE = "//TIPODOCU_ACRE";
  static final String mcteParam_APELLIDO = "//APELLIDO";
  static final String mcteParam_NOMBRES = "//NOMBRES";
  static final String mcteParam_DOMICDOM = "//DOMICDOM";
  static final String mcteParam_DOMICDNU = "//DOMICDNU";
  static final String mcteParam_DOMICESC = "//DOMICESC";
  static final String mcteParam_DOMICPIS = "//DOMICPIS";
  static final String mcteParam_DOMICPTA = "//DOMICPTA";
  static final String mcteParam_DOMICPOB = "//DOMICPOB";
  static final String mcteParam_DOMICCPO = "//DOMICCPO";
  static final String mcteParam_PROVICOD = "//PROVICOD";
  static final String mcteParam_PAISSCOD = "//PAISSCOD";
  static final String mcteParam_BANCOCOD = "//BANCOCOD";
  static final String mcteParam_COBROCOD = "//COBROCOD";
  static final String mcteParam_EFECTANN = "//EFECTANN2";
  static final String mcteParam_EFECTMES = "//EFECTMES";
  static final String mcteParam_EFECTDIA = "//EFECTDIA";
  /**
   * 
   */
  static final String mcteParam_ENTDOMSC = "//ENTDOMSC";
  static final String mcteParam_ANOTACIO = "//ANOTACIO";
  static final String mcteParam_PRECIO_MENSUAL = "//PRECIO_MENSUAL";
  static final String mcteParam_TIPO_ACTUA = "//TIPO_ACTU";
  /**
   * 
   */
  static final String mcteParam_CUITNUME = "//CUITNUME";
  static final String mcteParam_RAZONSOC = "//RAZONSOC";
  static final String mcteParam_CLIEIBTP = "//CLIEIBTP";
  static final String mcteParam_NROIIBB = "//NROIIBB";
  static final String mcteParam_LUNETA = "//LUNETA";
  static final String mcteParam_HIJOS1729_EXC = "//HIJOS1729ND";
  /**
   *  Nuevos Campos Cicle Time
   */
  static final String mcteParam_NROFACTU = "//NROFACTU";
  static final String mcteParam_INS_DOMICDOM = "//INS_DOMICDOM";
  static final String mcteParam_INS_CPACODPO = "//INS_CPACODPO";
  static final String mcteParam_INS_DOMICPOB = "//INS_DOMICPOB";
  static final String mcteParam_INS_PROVICOD = "//INS_PROVICOD";
  static final String mcteParam_INS_DOMICTLF = "//INS_DOMICTLF";
  static final String mcteParam_INS_RANGOHOR = "//INS_RANGOHOR";
  static final String mcteParam_DDJJ = "//DDJJ_STRING";
  static final String mcteParam_DERIVACI = "//DERIVACI";
  static final String mcteParam_ALEATORIO = "//ALEATORIO";
  static final String mcteParam_ESTAIDES = "//ESTAIDES";
  static final String mcteParam_DISPODES = "//DISPODES";
  static final String mcteParam_INSTALADP = "//INSTALADP";
  static final String mcteParam_POSEEDISP = "//POSEEDISP";
  static final String mcteParam_AUTIPGAMA = "//AUTIPGAMA";
  /**
   * Datos exigidos por la UIF
   */
  static final String mcteParam_SWAPODER = "//SWAPODER";
  static final String mcteParam_OCUPACODCLIE = "//OCUPACODCLIE";
  static final String mcteParam_OCUPACODAPOD = "//OCUPACODAPOD";
  static final String mcteParam_CLIENSECAPOD = "//CLIENSECAPOD";
  static final String mcteParam_DOMISECAPOD = "//DOMISECAPOD";
  /**
   * Datos de Green Products
   */
  static final String mcteParam_CLUBECO = "//CLUBECO";
  static final String mcteParam_GRANIZO = "//GRANIZO";
  static final String mcteParam_ROBOCONT = "//ROBOCONT";
  /**
   * ***********************************
   *  Definiciones NEW SAS
   * ***********************************
   */
  static final String mcteParam_ASEGNOM = "//CLIENTE_NOMYAPE";
  static final String mcteParam_EMPL_NOMYAPE = "//EMPL_NOMYAPE";
  static final String mcteParam_SICTUPOL = "SICTUPOL";
  static final String mcteParam_RAMO = "AUS1";
  static final String mcteParam_RAMOCOD = "44";
  static final String gcteDB_NEWSAS = "DMS_ControlDuplicidad.udl";
  static final String mcteStoreProcGrabaCabecera_NEWSAS = "P_DMS_AltaCabeCtrolAutos";
  static final String mcteStoreProcNumeracion_NEWSAS = "P_DMS_Numeracion";
  static final String mctePrefixVendor_NEWSAS = "(OV) ";
  /**
   * 
   * ***********************************
   *  Definiciones SUSCRIPCION A NBWS
   * ***********************************
   */
  static final String mcteParam_SN_NBWS = "//SN_NBWS";
  static final String mcteParam_EMAIL_NBWS = "//EMAIL_NBWS";
  static final String mcteParam_SN_EPOLIZA = "//SN_EPOLIZA";
  static final String mcteParam_EMAIL_EPOLIZA = "//EMAIL_EPOLIZA";
  static final String mcteParam_SN_PRESTAMAIL = "//SN_PRESTAMAIL";
  static final String mcteParam_PASSEPOL = "//PASSEPOL";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();

  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   */
  @Override
  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLCoberturas = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    Object wobjClass = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String wvarRequest = "";
    String wvarResponse = "";
    String mvarRAMOPCOD = "";
    String mvarPOLIZANN = "";
    String mvarPOLIZSEC = "";
    String mvarCERTIPOL = "";
    String mvarCERTIANN = "";
    String mvarCERTISEC = "";
    String mvarSUPLENUM = "";
    String mvarFRANQCOD = "";
    String mvarPQTDES = "";
    String mvarPLANCOD = "";
    String mvarHIJOS1416 = "";
    String mvarHIJOS1729 = "";
    String mvarZONA = "";
    String mvarCODPROV = "";
    String mvarSUMALBA = "";
    String mvarCLIENIVA = "";
    String mvarSUMASEG = "";
    String mvarCLUB_LBA = "";
    String mvarDEST80 = "";
    String mvarCPAANO = "";
    String mvarCTAKMS = "";
    String mvarESCERO = "";
    String mvarTIENEPLAN = "";
    String mvarCOND_ADIC = "";
    String mvarASEG_ADIC = "";
    String mvarFH_NAC = "";
    String mvarSEXO = "";
    String mvarESTCIV = "";
    String mvarSIFMVEHI_DES = "";
    String mvarPROFECOD = "";
    String mvarSIACCESORIOS = "";
    String mvarREFERIDO = "";
    String mvarMotorNum = "";
    String mvarCHASINUM = "";
    String mvarPatenNum = "";
    String mvarAUMARCOD = "";
    String mvarAUMODCOD = "";
    String mvarAUSUBCOD = "";
    String mvarAUADICOD = "";
    String mvarAUMODORI = "";
    String mvarAUUSOCOD = "";
    String mvarAUVTVCOD = "";
    String mvarAUVTVDIA = "";
    String mvarAUVTVMES = "";
    String mvarAUVTVANN = "";
    String mvarVEHCLRCOD = "";
    String mvarAUKLMNUM = "";
    String mvarFABRICAN = "";
    String mvarFABRICMES = "";
    String mvarGUGARAGE = "";
    String mvarGUDOMICI = "";
    String mvarAUCATCOD = "";
    String mvarAUTIPCOD = "";
    String mvarAUCIASAN = "";
    String mvarAUANTANN = "";
    String mvarAUNUMSIN = "";
    String mvarAUNUMKMT = "";
    String mvarAUUSOGNC = "";
    String mvarUSUARCOD = "";
    String mvarSITUCPOL = "";
    String mvarCLIENSEC = "";
    String mvarNUMEDOCU = "";
    String mvarTIPODOCU = "";
    String mvarDOMICSEC = "";
    String mvarCUENTSEC = "";
    String mvarCOBROFOR = "";
    String mvarEDADACTU = "";
    String wvarCOT_NRO = "";
    String mvarCOBERCOD = "";
    String mvarCOBERORD = "";
    String mvarCAPITASG = "";
    String mvarCAPITIMP = "";
    String mvarDOCUMDAT = "";
    String mvarNOMBREAS = "";
    String mvarCAMP_CODIGO = "";
    String wvarTIPOPROD = "";
    String mvarCAMP_DESC = "";
    String mvarLEGAJO_GTE = "";
    String mvarNRO_PROD = "";
    String mvarEMPRESA_CODIGO = "";
    String mvarSUCURSAL_CODIGO = "";
    String mvarLEGAJO_VEND = "";
    String mvarEMPL = "";
    String mvarCONDUAPE = "";
    String mvarCONDUNOM = "";
    String mvarCONDUFEC = "";
    String mvarCONDUSEX = "";
    String mvarCONDUEST = "";
    String mvarCONDUEXC = "";
    String mvarAUACCCOD = "";
    String mvarAUVEASUM = "";
    String mvarAUVEADES = "";
    String mvarAUVEADEP = "";
    String mvarESTAINSP = "";
    String mvarINSPECOD = "";
    String mvarINSPEDES = "";
    String mvarINSPADOM = "";
    String mvarINSPEOBS = "";
    String mvarINSPEKMS = "";
    String mvarCENSICOD = "";
    String mvarCENSIDES = "";
    String mvarDISPOCOD = "";
    String mvarPRESTCOD = "";
    String mvarINSPEANN = "";
    String mvarINSPEMES = "";
    String mvarINSPEDIA = "";
    String mvarNUMEDOCU_ACRE = "";
    String mvarTIPODOCU_ACRE = "";
    String mvarAPELLIDO = "";
    String mvarNOMBRES = "";
    String mvarDOMICDOM = "";
    String mvarDOMICDNU = "";
    String mvarDOMICESC = "";
    String mvarDOMICPIS = "";
    String mvarDOMICPTA = "";
    String mvarDOMICPOB = "";
    String mvarDOMICCPO = "";
    String mvarPROVICOD = "";
    String mvarPAISSCOD = "";
    String mvarBANCOCOD = "";
    String mvarCOBROCOD = "";
    String mvarEfectAnn = "";
    String mvarEFECTMES = "";
    String mvarEFECTDIA = "";
    String mvarENTDOMSC = "";
    String mvarANOTACIO = "";
    String mvarPRECIO_MENSUAL = "";
    String mvarTIPO_ACTUA = "";
    String mvarCUITNUME = "";
    String mvarRAZONSOC = "";
    String mvarCLIEIBTP = "";
    String mvarNROIIBB = "";
    String mvarLUNETA = "";
    String mvarHIJOS1729_EXC = "";
    String mvarNROFACTU = "";
    String mvarINS_DOMICDOM = "";
    String mvarINS_CPACODPO = "";
    String mvarINS_DOMICPOB = "";
    String mvarINS_PROVICOD = "";
    String mvarINS_DOMICTLF = "";
    String mvarINS_RANGOHOR = "";
    String mvarDDJJ = "";
    String mvarDERIVACI = "";
    String mvarALEATORIO = "";
    String mvarESTAIDES = "";
    String mvarDISPODES = "";
    String mvarINSTALADP = "";
    String mvarPOSEEDISP = "";
    String mvarAUTIPGAMA = "";
    String wvarSWAPODER = "";
    String wvarOCUPACODCLIE = "";
    String wvarOCUPACODAPOD = "";
    String wvarCLIENSECAPOD = "";
    String wvarDOMISECAPOD = "";
    String wvarCLUBECO = "";
    String wvarGRANIZO = "";
    String wvarROBOCONT = "";
    String mvarXMLCoberturas = "";
    String mvarSucu = "";
    String mvarRAMO = "";
    String mvarNRO_CERTISEC = "";
    String mvarNRO_SOLICITUD = "";
    String mvarDOCU_TIPO = "";
    String mvarDOCU_NUMERO = "";
    String mvarNOM_ASEG = "";
    String mvarLEG_VEND = "";
    String mvarNOM_VEND = "";
    String mvarPATENTE = "";
    String mvarCHASIS = "";
    String mvarSN_NBWS = "";
    String mvarEMAIL_NBWS = "";
    String mvarSN_EPOLIZA = "";
    String mvarEMAIL_EPOLIZA = "";
    String mvarSN_PRESTAMAIL = "";
    String mvarPASSEPOL = "";
    //
    //
    //
    // DATOS GENERALES
    //23/01/2006
    //
    // Coberturas los devuelve el mensaje de Cotizacion y son 20 iteraciones
    // Asegurados Adicionales ...son 10 iteraciones
    // Datos Comunes
    // Hijos ...son 10 iteraciones
    // Accesorios ...son 10 iteraciones
    //Deprecia SI/NO, se le genera siempre en si.
    // Inspeccion
    //Dim mvarINSPECC                 As String
    // Acreedor Prendario
    //
    //
    // Nuevos Campos Cicle Time
    // ******************************
    // COMIENZO VARIABLES NEWSAS
    // ******************************
    // ******************************
    // FIN VARIABLES NEWSAS
    // ******************************
    //
    //Datos exigidos por la UIF
    //
    //Datos de Green Products
    //
    // *************************************************
    // COMIENZO VARIABLES SUSCRIPCIONES NBWS Y EPOLIZA
    // *************************************************
    // ********************************************
    // FIN VARIABLES SUSCRIPCIONES NBWS Y EPOLIZA
    // ********************************************
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // Cargo el xml de entrada
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      // Vuelvo a cotizar para el plan seleccionado
      wvarStep = 20;
//      wobjClass = new lbawA_OVMQCotizar.lbaw_OVGetCotiAUS();
      //error: function 'Execute' was not found.
      //Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
      //Begin convert
      
      lbaw_OVGetCotiAUS getCotiAus = new lbaw_OVGetCotiAUS();
      StringHolder sh = new StringHolder();
      int resultCode = getCotiAus.IAction_Execute(XmlDomExtended.marshal(wobjXMLRequest.getDocument().getDocumentElement()), sh, "");
      wvarResponse = sh.getValue();
      //End convert
      wobjClass = null;
      //
      // Guardo las coberturas y las sumas aseguradas
      wvarStep = 30;
      wobjXMLCoberturas = new XmlDomExtended();
      wobjXMLCoberturas.loadXML( wvarResponse );
      //
      wvarStep = 40;
      // Controlo como devolvi� la cotizaci�n
      if( XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='No se pudo realizar la cotizaci�n correspondiente' /></Response>" );
        //unsup GoTo ErrorHandler
      }
      //
      wvarStep = 42;
      mvarXMLCoberturas = GetCoberturas( wobjXMLCoberturas, ! (wobjXMLRequest.selectSingleNode( mcteParam_CONDUCTORES )  == (org.w3c.dom.Node) null));
      wvarStep = 45;
      wvarCOT_NRO = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_COT_NRO )  );
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_SUMASEG ) , XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_SUMASEG )  ) );
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_SUMALBA ) , XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_SUMALBA )  ) );
      //
      wvarStep = 50;
      //
      wvarStep = 60;
      // DATOS GENERALES
      mvarRAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  );
      mvarPOLIZANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN )  );
      mvarPOLIZSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC )  );
      mvarCERTIPOL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL )  );
      mvarCERTIANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN )  );
      mvarCERTISEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC )  );
      wvarStep = 70;
      mvarSUPLENUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SUPLENUM )  );
      mvarFRANQCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FRANQCOD )  );
      mvarPQTDES = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PQTDES )  ) ), 120 );
      mvarPLANCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PLANCOD )  );
      mvarHIJOS1416 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1416 )  );
      wvarStep = 80;
      mvarHIJOS1729 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1729 )  );
      mvarZONA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ZONA )  );
      mvarCODPROV = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CODPROV )  );
      // Inicio parche
      String sumaAlba = String.valueOf(Obj.toDouble(XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SUMALBA ) ) )/ 100) ;

      String sumaAlbaoConvertido;
      if (sumaAlba.indexOf(".") != -1) {
    	  sumaAlbaoConvertido = sumaAlba.substring(0, sumaAlba.indexOf("."));
      } else {
    	  sumaAlbaoConvertido = sumaAlba;
      }
      // Fin parche
      mvarSUMALBA = Strings.replace(sumaAlbaoConvertido, ",", "." );
      mvarCLIENIVA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENIVA )  );
      wvarStep = 90;
      // Inicio parche
      String sumaSeg = String.valueOf( Obj.toDouble( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SUMASEG )  ) ) / 100 );

      String sumaSegConvertido;
      if (sumaSeg.indexOf(".") != -1) {
    	  sumaSegConvertido = sumaSeg.substring(0, sumaSeg.indexOf("."));
      } else {
    	  sumaSegConvertido = sumaSeg;
      }
      // Fin parche
      mvarSUMASEG = Strings.replace(sumaSegConvertido, ",", "." );
      mvarCLUB_LBA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLUB_LBA )  );

      if( wobjXMLRequest.selectNodes( mcteParam_DEST80 ) .getLength() > 0 )
      {
        //23/01/2006
        mvarDEST80 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DEST80 )  );
      }
      if( mvarDEST80.equals( "" ) )
      {
        mvarDEST80 = "N";
      }

      mvarCPAANO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CPAANO )  );
      mvarCTAKMS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CTAKMS )  );
      mvarESCERO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ESCERO )  );
      wvarStep = 100;
      mvarTIENEPLAN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIENEPLAN )  );
      mvarCOND_ADIC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_COND_ADIC )  );
      mvarASEG_ADIC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ASEG_ADIC )  );
      mvarFH_NAC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FH_NAC )  );
      mvarSEXO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SEXO )  );
      wvarStep = 110;
      mvarESTCIV = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ESTCIV )  );
      //mvarSIFMVEHI_DES = Left(Trim(.selectSingleNode(mcteParam_SIFMVEHI_DES).Text), 35) ' MQ 0093
      // MQ 0051
      mvarSIFMVEHI_DES = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SIFMVEHI_DES )  ) ), 80 );
      mvarPROFECOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PROFECOD )  );
      mvarSIACCESORIOS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SIACCESORIOS )  );
      mvarREFERIDO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_REFERIDO )  );
      wvarStep = 120;
      mvarMotorNum = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MotorNum )  );
      mvarCHASINUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CHASINUM )  );
      mvarPatenNum = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PatenNum )  );
      mvarAUMARCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUMARCOD )  );
      mvarAUMODCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUMODCOD )  );
      wvarStep = 130;
      mvarAUSUBCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUSUBCOD )  );
      mvarAUADICOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUADICOD )  );
      mvarAUMODORI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUMODORI )  );
      mvarAUUSOCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUUSOCOD )  );
      mvarAUVTVCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUVTVCOD )  );
      wvarStep = 140;
      mvarAUVTVDIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUVTVDIA )  );
      mvarAUVTVMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUVTVMES )  );
      mvarAUVTVANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUVTVANN )  );
      mvarVEHCLRCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_VEHCLRCOD )  );
      mvarAUKLMNUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUKLMNUM )  );
      wvarStep = 150;
      mvarFABRICAN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FABRICAN )  );
      mvarFABRICMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FABRICMES )  );
      mvarGUGARAGE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_GUGARAGE )  );
      mvarGUDOMICI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_GUDOMICI )  );
      mvarAUCATCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUCATCOD )  );
      wvarStep = 160;
      mvarAUTIPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUTIPCOD )  );
      mvarAUCIASAN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUCIASAN )  );
      mvarAUANTANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUANTANN )  );
      mvarAUNUMSIN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUNUMSIN )  );
      mvarAUNUMKMT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUNUMKMT )  );
      wvarStep = 170;
      mvarAUUSOGNC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUUSOGNC )  );
      mvarUSUARCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD )  );
      mvarSITUCPOL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SITUCPOL )  );
      mvarCLIENSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC )  );
      mvarNUMEDOCU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU )  );
      wvarStep = 180;
      mvarTIPODOCU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU )  );
      mvarDOMICSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICSEC )  );
      mvarCUENTSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CUENTSEC )  );
      mvarCOBROFOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_COBROFOR )  );
      mvarEDADACTU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EDADACTU )  );
      wvarStep = 190;
      mvarCAMP_CODIGO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CAMP_CODIGO )  );
      mvarCAMP_DESC = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CAMP_DESC )  ) ), 30 );
      mvarLEGAJO_GTE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_LEGAJO_GTE )  );
      mvarNRO_PROD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NRO_PROD )  );
      mvarEMPRESA_CODIGO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EMPRESA_CODIGO )  );
      mvarSUCURSAL_CODIGO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SUCURSAL_CODIGO )  );
      mvarLEGAJO_VEND = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_LEGAJO_VEND )  );
      mvarEMPL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EMPL )  );
      wvarStep = 200;
      mvarESTAINSP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ESTAINSP )  );
      mvarINSPECOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPECOD )  );
      mvarINSPEDES = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPEDES )  ) ), 30 );
      mvarINSPADOM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPADOM )  );
      wvarStep = 210;
      mvarINSPEOBS = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPEOBS )  ) ), 50 );
      mvarINSPEKMS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPEKMS )  );

//    mvarCENSICOD = .selectSingleNode(mcteParam_CENSICOD).Text
      mvarCENSICOD = Strings.right( Strings.fill( 4, " " ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CENSICOD )  ), 4 );

      mvarCENSIDES = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CENSIDES )  ) ), 30 );
      wvarStep = 220;
      //        mvarINSPECC = .selectSingleNode(mcteParam_INSPECC).Text
      mvarDISPOCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DISPOCOD )  );
      mvarPRESTCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PRESTCOD )  );
      mvarNUMEDOCU_ACRE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU_ACRE )  );
      wvarStep = 230;
      mvarTIPODOCU_ACRE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU_ACRE )  );
      mvarAPELLIDO = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_APELLIDO )  ) ), 40 );
      mvarNOMBRES = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NOMBRES )  ) ), 40 );
      mvarDOMICDOM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOM )  );
      mvarDOMICDNU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICDNU )  );
      mvarDOMICESC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICESC )  );
      wvarStep = 240;
      mvarDOMICPIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICPIS )  );
      mvarDOMICPTA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICPTA )  );
      mvarDOMICPOB = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOB )  ) ), 20 );
      mvarDOMICCPO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICCPO )  );
      mvarPROVICOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PROVICOD )  );
      mvarPAISSCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PAISSCOD )  );
      wvarStep = 250;
      mvarBANCOCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_BANCOCOD )  );
      mvarCOBROCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_COBROCOD )  );
      mvarEfectAnn = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EFECTANN )  );
      mvarEFECTMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EFECTMES )  );
      mvarEFECTDIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EFECTDIA )  );
      mvarINSPEANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPEANN )  );
      wvarStep = 260;
      mvarINSPEMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPEMES )  );
      mvarINSPEDIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPEDIA )  );
      mvarENTDOMSC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ENTDOMSC )  );
      mvarANOTACIO = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ANOTACIO )  ) ), 100 );
      mvarPRECIO_MENSUAL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PRECIO_MENSUAL )  );
      mvarTIPO_ACTUA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIPO_ACTUA ) );
      //
      wvarStep = 270;
      mvarCUITNUME = "";
      if( wobjXMLRequest.selectNodes( mcteParam_CUITNUME ) .getLength() > 0 )
      {
        mvarCUITNUME = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CUITNUME )  );
      }
      //
      mvarRAZONSOC = "";
      if( wobjXMLRequest.selectNodes( mcteParam_RAZONSOC ) .getLength() > 0 )
      {
        mvarRAZONSOC = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAZONSOC )  ) ), 60 );
      }
      //
      wvarStep = 280;
      mvarCLIEIBTP = "";
      if( wobjXMLRequest.selectNodes( mcteParam_CLIEIBTP ) .getLength() > 0 )
      {
        mvarCLIEIBTP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIEIBTP )  );
      }
      //
      mvarNROIIBB = "";
      if( wobjXMLRequest.selectNodes( mcteParam_NROIIBB ) .getLength() > 0 )
      {
        mvarNROIIBB = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NROIIBB )  );
      }
      //
      wvarStep = 290;
      mvarLUNETA = "";
      if( wobjXMLRequest.selectNodes( mcteParam_LUNETA ) .getLength() > 0 )
      {
        mvarLUNETA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_LUNETA )  );
      }
      //
      wvarStep = 300;
      mvarHIJOS1729_EXC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1729_EXC )  );
      mvarNROFACTU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NROFACTU )  );
      mvarINS_DOMICDOM = "";
      if( wobjXMLRequest.selectNodes( mcteParam_INS_DOMICDOM ) .getLength() > 0 )
      {
        mvarINS_DOMICDOM = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INS_DOMICDOM )  ) ), 20 );
      }
      mvarINS_CPACODPO = "";
      if( wobjXMLRequest.selectNodes( mcteParam_INS_CPACODPO ) .getLength() > 0 )
      {
        mvarINS_CPACODPO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INS_CPACODPO )  );
      }
      wvarStep = 310;
      mvarINS_DOMICPOB = "";
      if( wobjXMLRequest.selectNodes( mcteParam_INS_DOMICPOB ) .getLength() > 0 )
      {
        mvarINS_DOMICPOB = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INS_DOMICPOB )  );
      }
      mvarINS_PROVICOD = "0";
      if( wobjXMLRequest.selectNodes( mcteParam_INS_PROVICOD ) .getLength() > 0 )
      {
        mvarINS_PROVICOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INS_PROVICOD )  );
      }
      wvarStep = 320;
      mvarINS_DOMICTLF = "";
      if( wobjXMLRequest.selectNodes( mcteParam_INS_DOMICTLF ) .getLength() > 0 )
      {
        mvarINS_DOMICTLF = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INS_DOMICTLF )  ) ), 30 );
      }
      mvarINS_RANGOHOR = "0";
      if( wobjXMLRequest.selectNodes( mcteParam_INS_RANGOHOR ) .getLength() > 0 )
      {
        mvarINS_RANGOHOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INS_RANGOHOR )  );
      }
      wvarStep = 325;
      mvarDDJJ = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DDJJ )  ) ), 140 );
      mvarDERIVACI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DERIVACI )  );
      mvarALEATORIO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ALEATORIO )  );
      mvarESTAIDES = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ESTAIDES )  ), 30 );
      mvarDISPODES = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DISPODES )  ), 30 );

      mvarINSTALADP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSTALADP )  );
      mvarPOSEEDISP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POSEEDISP )  );
      mvarAUTIPGAMA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUTIPGAMA )  );

      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_TIPO_PROD )  == (org.w3c.dom.Node) null) )
      {
        wvarTIPOPROD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIPO_PROD )  );
      }

      wvarStep = 326;

      //Datos exigidos por la UIF
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_SWAPODER )  == (org.w3c.dom.Node) null) )
      {
        wvarSWAPODER = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SWAPODER )  );
      }
      else
      {
        wvarSWAPODER = "";
      }
      //
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_OCUPACODCLIE )  == (org.w3c.dom.Node) null) )
      {
        wvarOCUPACODCLIE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_OCUPACODCLIE )  );
      }
      else
      {
        wvarOCUPACODCLIE = "";
      }
      //
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_OCUPACODAPOD )  == (org.w3c.dom.Node) null) )
      {
        wvarOCUPACODAPOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_OCUPACODAPOD )  );
      }
      else
      {
        wvarOCUPACODAPOD = "";
      }
      //
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_CLIENSECAPOD )  == (org.w3c.dom.Node) null) )
      {
        wvarCLIENSECAPOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENSECAPOD )  );
      }
      else
      {
        wvarCLIENSECAPOD = "0";
      }
      //
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_DOMISECAPOD )  == (org.w3c.dom.Node) null) )
      {
        wvarDOMISECAPOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMISECAPOD )  );
      }
      else
      {
        wvarDOMISECAPOD = "";
      }
      //
      //Green Products
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_CLUBECO )  == (org.w3c.dom.Node) null) )
      {
        wvarCLUBECO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLUBECO )  );
      }
      else
      {
        wvarCLUBECO = "";
      }
      //
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_GRANIZO )  == (org.w3c.dom.Node) null) )
      {
        wvarGRANIZO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_GRANIZO )  );
      }
      else
      {
        wvarGRANIZO = "";
      }
      //
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_ROBOCONT )  == (org.w3c.dom.Node) null) )
      {
        wvarROBOCONT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ROBOCONT )  );
      }
      else
      {
        wvarROBOCONT = "";
      }


      // *********************************
      // ASIGNA NODOS DE NEWSAS
      // *********************************
      wvarStep = 327;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_SUCURSAL_CODIGO )  == (org.w3c.dom.Node) null) )
      {
        mvarSucu = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SUCURSAL_CODIGO )  );
      }
      else
      {
        mvarSucu = "";
      }
      wvarStep = 330;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU )  == (org.w3c.dom.Node) null) )
      {
        mvarDOCU_TIPO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU )  );
      }
      else
      {
        mvarDOCU_TIPO = "";
      }
      wvarStep = 340;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU )  == (org.w3c.dom.Node) null) )
      {
        mvarDOCU_NUMERO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU )  );
      }
      else
      {
        mvarDOCU_NUMERO = "";
      }
      wvarStep = 350;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_ASEGNOM )  == (org.w3c.dom.Node) null) )
      {
        mvarNOM_ASEG = Strings.left( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ASEGNOM )  ) ), 80 );
      }
      else
      {
        mvarNOM_ASEG = "";
      }
      wvarStep = 360;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_LEGAJO_VEND )  == (org.w3c.dom.Node) null) )
      {
        mvarLEG_VEND = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_LEGAJO_VEND )  );
      }
      else
      {
        mvarLEG_VEND = "";
      }
      wvarStep = 370;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_EMPL_NOMYAPE )  == (org.w3c.dom.Node) null) )
      {
        mvarNOM_VEND = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EMPL_NOMYAPE )  ), 50 );
      }
      else
      {
        mvarNOM_VEND = "";
      }

      wvarStep = 380;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_PatenNum )  == (org.w3c.dom.Node) null) )
      {
        mvarPATENTE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PatenNum )  );
      }
      else
      {
        mvarPATENTE = "";
      }
      wvarStep = 390;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_CHASINUM )  == (org.w3c.dom.Node) null) )
      {
        mvarCHASIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CHASINUM )  );
      }
      else
      {
        mvarCHASIS = "";
      }


      // **********************************************
      // FIN NODOS NEWSAS
      // **********************************************
      // **********************************************
      // ASIGNA NODOS DE SUSCRIPCIONES NBWS Y EPOLIZA
      // **********************************************
      wvarStep = 395;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_SN_NBWS )  == (org.w3c.dom.Node) null) )
      {
        mvarSN_NBWS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SN_NBWS )  );
      }
      else
      {
        mvarSN_NBWS = "";
      }
      wvarStep = 396;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_EMAIL_NBWS )  == (org.w3c.dom.Node) null) )
      {
        mvarEMAIL_NBWS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EMAIL_NBWS )  );
      }
      else
      {
        mvarEMAIL_NBWS = "";
      }
      wvarStep = 397;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_SN_EPOLIZA )  == (org.w3c.dom.Node) null) )
      {
        mvarSN_EPOLIZA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SN_EPOLIZA )  );
      }
      else
      {
        mvarSN_EPOLIZA = "";
      }
      wvarStep = 398;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_EMAIL_EPOLIZA )  == (org.w3c.dom.Node) null) )
      {
        mvarEMAIL_EPOLIZA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EMAIL_EPOLIZA )  );
      }
      else
      {
        mvarEMAIL_EPOLIZA = "";
      }
      wvarStep = 399;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_SN_PRESTAMAIL )  == (org.w3c.dom.Node) null) )
      {
        mvarSN_PRESTAMAIL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SN_PRESTAMAIL )  );
      }
      else
      {
        mvarSN_PRESTAMAIL = "";
      }
      //13/10/2009 Se agrega password de EPoliza por nueva definici�n.-
      wvarStep = 400;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_PASSEPOL )  == (org.w3c.dom.Node) null) )
      {
        mvarPASSEPOL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PASSEPOL )  );
      }
      else
      {
        mvarPASSEPOL = "";
      }

      // **********************************************
      // FIN NODOS DE SUSCRIPCIONES NBWS Y EPOLIZA
      // **********************************************
      // **************************************************************************************
      //NEW SAS - Si se envia al AIS, se pide numero a NewSas y se le asigna al CERTISEC de OV
      // **************************************************************************************
      // aca tengo que poner las dos situaciones
      if( (mvarCERTIPOL.equals( "0150" )) && ((Strings.toUpperCase( mvarSITUCPOL ).equals( "X" )) || (Strings.toUpperCase( mvarSITUCPOL ).equals( "A" )) || (Strings.toUpperCase( mvarSITUCPOL ).equals( "O" ))) )
      {

        wvarStep = 410;

        // ******************************
        // OBTIENE NUMERACION DE NEWSAS
        // ******************************
        wvarStep = 420;

        java.sql.Connection jdbcConn = null;
        try {
            JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
            jdbcConn = connFactory.getJDBCConnection(ModGeneral.gcteDB);

        String sp = "{? = call " + mcteStoreProcNumeracion_NEWSAS + "(?)}";

        CallableStatement ps = jdbcConn.prepareCall(sp);
        int posiSP = 1;
        
        ps.registerOutParameter(posiSP++, Types.INTEGER);
        wvarStep = 430;
        
        wvarStep = 450;
        ps.setString(posiSP++, String.valueOf(mcteParam_RAMO));


        wvarStep = 460;

        boolean result = ps.execute();
        
        if (result) {
      	  //El primero es un ResultSet
        } else {
      	  //Salteo el "updateCount"
      	  int uc = ps.getUpdateCount();
      	  if (uc  == -1) {
//      		  System.out.println("No hay resultados");
      	  }
      	  result = ps.getMoreResults();
        }


        java.sql.ResultSet cursor = ps.getResultSet();
        if ( cursor == null ) {
        	throw new ComponentExecutionException("No había resultados de la ejecición del SP");
        }
        if( cursor.next())
        {
          wvarStep = 470;
          // Este parametro se debe pasar tambien en el stored de solicitud en el caso de ser necesario
          // es decir, en el caso que cumpla las condiciones de banco
          mvarNRO_CERTISEC = cursor.getString("UltimoNroTot");
        }
        cursor.close();
        ps.close();
        
        // *********************************
        // *      FIN DE NUMERACI�N
        // *********************************
        // *******************************
        // GRABA CABECERA NEWSAS
        // *******************************
        String spGrab = "{? = call " + mcteStoreProcGrabaCabecera_NEWSAS + "(?,?,?,?,?,?,?,?,?,?,?)}";

        CallableStatement psGrab = jdbcConn.prepareCall(spGrab);
        int posiSPGrab = 1;

        // *******************************************
        // PARAMETROS LLAMADA STORE NEWSAS
        // *******************************************
        wvarStep = 500;
        psGrab.registerOutParameter(posiSP++, Types.INTEGER);

        // SUCURSAL
        wvarStep = 510;
        if (mvarSucu.isEmpty()) {
        	psGrab.setNull(posiSPGrab++, Types.NUMERIC);
    		} else {
    		psGrab.setInt(posiSPGrab++, Integer.parseInt(mvarSucu));
    	}

        // RAMO
        wvarStep = 520;
        if (mcteParam_RAMOCOD.isEmpty()) {
    		psGrab.setNull(posiSPGrab++, Types.NUMERIC);
    		} else {
    		psGrab.setInt(posiSPGrab++, Integer.parseInt(mcteParam_RAMOCOD));
    	}

        // NRO_CERTISEC
        wvarStep = 530;
        if (mvarNRO_CERTISEC.isEmpty()) {
    		psGrab.setNull(posiSPGrab++, Types.NUMERIC);
    		} else {
    		psGrab.setInt(posiSPGrab++, Integer.parseInt(mvarNRO_CERTISEC));
    	}

        // Numero de solicitud armado apartir del nuevo certisec
        mvarNRO_SOLICITUD = Strings.right( "00" + mvarPOLIZANN, 2 ) + Strings.right( ("000000" + mvarPOLIZSEC), 6 ) + Strings.right( ("0000" + mvarCERTIPOL), 4 ) + Strings.right( ("0000" + mvarCERTIANN), 4 ) + Strings.right( ("000000" + mvarNRO_CERTISEC), 6 ) + Strings.right( ("0000" + mvarSUPLENUM), 4 );

        wvarStep = 540;
        if (mvarNRO_SOLICITUD.isEmpty()) {
    		psGrab.setNull(posiSPGrab++, Types.NUMERIC);
    		} else {
    		psGrab.setInt(posiSPGrab++, Integer.parseInt(mvarNRO_SOLICITUD));
    	}

        // DOCU_TIPO
        wvarStep = 560;
        if (mvarDOCU_TIPO.isEmpty()) {
    		psGrab.setNull(posiSPGrab++, Types.NUMERIC);
    		} else {
    		psGrab.setInt(posiSPGrab++, Integer.parseInt(mvarDOCU_TIPO));
    	}
        
        // DOCU_NUMERO
        // AD 22/5/09 Se cambio la longitud del numero de documento a 11 digitos
        //wvarStep = 570
        //Set wobjDBParm = wobjDBCmd.CreateParameter("@DOCU_NUMERO", adNumeric, adParamInput, , mvarDOCU_NUMERO)
        //wobjDBCmd.Parameters.Append wobjDBParm
        //Set wobjDBParm = Nothing
        // DOCU_NUMERO
        // AD - 27/05/2009: se cambia de adInteger a adNumeric para que soporte 11 d�gitos
        wvarStep = 570;
        // Add this line
        if (mvarDOCU_NUMERO.isEmpty()) {
    		psGrab.setNull(posiSPGrab++, Types.NUMERIC);
    		} else {
    		psGrab.setInt(posiSPGrab++, Integer.parseInt(mvarDOCU_NUMERO));
    	}
        // NOMBRE_ASEGURADO
        wvarStep = 580;
        psGrab.setString(posiSPGrab++, String.valueOf(mvarNOM_ASEG));
        //LEGAJO_VEND
        wvarStep = 590;
        if (mvarLEG_VEND.isEmpty()) {
    		psGrab.setNull(posiSPGrab++, Types.NUMERIC);
    		} else {
    		psGrab.setInt(posiSPGrab++, Integer.parseInt(mvarLEG_VEND));
    	}
        
        // NOMBRE_VEND
        wvarStep = 600;
        psGrab.setString(posiSPGrab++, String.valueOf( Strings.left( Strings.toUpperCase( mctePrefixVendor_NEWSAS + mvarNOM_VEND ), 50 ) ));

        //PATENTE
        wvarStep = 610;
        psGrab.setString(posiSPGrab++, String.valueOf(mvarPATENTE));


        //CHASIS
        wvarStep = 620;
        psGrab.setString(posiSPGrab++, String.valueOf(mvarCHASIS));
        
        // *******************************************
        // FIN PARAMETROS LLAMADA STORE NEWSAS
        // *******************************************
        // *************************
        // EJECUTA EL STORED
        // *************************
        wvarStep = 630;


        boolean resultGrab = psGrab.execute();
     
      if (resultGrab) {
    	  //El primero es un ResultSet
      } else {
    	  //Salteo el "updateCount"
    	  int uc = psGrab.getUpdateCount();
    	  if (uc  == -1) {
//    		  System.out.println("No hay resultados");
    	  }
    	  resultGrab = psGrab.getMoreResults();
      }
        } finally {
            try {
                if (jdbcConn != null)
                    jdbcConn.close();
            } catch (Exception e) {
                logger.log(Level.WARNING, "Exception al hacer un close", e);
            }
        }

        // **************************************
        // *      FIN DE EJECUCION
        // **************************************
      }

      // **********************************
      // *      FIN NEWSAS
      // **********************************
      //
      wvarStep = 655;
      wobjXMLParametros = null;
      //
      wvarStep = 660;
      java.sql.Connection jdbcConn = null;
      try {
          JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
          jdbcConn = connFactory.getJDBCConnection(ModGeneral.gcteDB);
      String sp = "{? = call " + mcteStoreProc + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
      CallableStatement ps = jdbcConn.prepareCall(sp);
      int posiSP = 1;
      ps.registerOutParameter(posiSP++, Types.INTEGER);
      //Fin cambios

      wvarStep = 670;
      //
      // Parametros

		
		 wvarStep = 680;
	      
	      ps.setString(posiSP++, String.valueOf(mvarRAMOPCOD));
	      wvarStep = 690;
	      
	      if (mvarPOLIZANN.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarPOLIZANN));
	      }
	      wvarStep = 700;
	      
	      if (mvarPOLIZSEC.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarPOLIZSEC));
	      }
	      wvarStep = 710;
	      
	      if (mvarCERTIPOL.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarCERTIPOL));
	      }
	      wvarStep = 720;
	      
	      if (mvarCERTIANN.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarCERTIANN));
	      }
	      
	      
	      
	      // **********************************
	      // *      ASIGNO CERTISEC
	      // **********************************
	      // aca tengo que poner las dos situaciones
	      if( (mvarCERTIPOL.equals( "0150" )) && ((Strings.toUpperCase( mvarSITUCPOL ).equals( "X" )) || (Strings.toUpperCase( mvarSITUCPOL ).equals( "A" )) || (Strings.toUpperCase( mvarSITUCPOL ).equals( "O" ))) )
	      {

	        
	        
	        if (mvarCERTISEC.isEmpty()) {
	      	  ps.setNull(posiSP++, Types.NUMERIC);
	        } else {
	      	  ps.setInt(posiSP++, Integer.parseInt(mvarCERTISEC));
	        }
	        
	        
	      }
	      else
	      {
	        
	        if (mvarCERTISEC.isEmpty()) {
	      	  ps.setNull(posiSP++, Types.NUMERIC);
	        } else {
	      	  ps.setInt(posiSP++, Integer.parseInt(mvarCERTISEC));
	        }
	      }

	      // **********************************
	      // *      FIN ASIGNO CERTISEC
	      // **********************************

	      wvarStep = 730;
	      
	      if (mvarSUPLENUM.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarSUPLENUM));
	      }
	      wvarStep = 740;
	      
	      ps.setString(posiSP++, String.valueOf(mvarFRANQCOD));
	      wvarStep = 750;
	      
	      ps.setString(posiSP++, String.valueOf(mvarPQTDES));
	      wvarStep = 760;
	      
	      if (mvarPLANCOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarPLANCOD));
	      }
	      wvarStep = 770;
	      
	      if (mvarHIJOS1416.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarHIJOS1416));
	      }
	      wvarStep = 780;
	      
	      if (mvarHIJOS1729.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarHIJOS1729));
	      }
	      wvarStep = 790;
	      
	      if (mvarZONA.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarZONA));
	      }
	      wvarStep = 800;
	      
	      if (mvarCODPROV.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarCODPROV));
	      }
	      wvarStep = 810;
	      
	      if (mvarSUMALBA.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarSUMALBA));
	      }
	      wvarStep = 820;
	      
	      ps.setString(posiSP++, String.valueOf(mvarCLIENIVA));
	      wvarStep = 830;
	      
	      if (mvarSUMASEG.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarSUMASEG));
	      }
	      wvarStep = 840;
	      
	      ps.setString(posiSP++, String.valueOf(mvarCLUB_LBA));
	      wvarStep = 850;
	      
	      if (mvarCPAANO.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarCPAANO));
	      }
	      wvarStep = 860;
	      
	      if (mvarCTAKMS.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarCTAKMS));
	      }
	      wvarStep = 870;
	      
	      ps.setString(posiSP++, String.valueOf(mvarESCERO));
	      wvarStep = 880;
	      
	      ps.setString(posiSP++, String.valueOf(mvarTIENEPLAN));
	      wvarStep = 890;
	      
	      ps.setString(posiSP++, String.valueOf(mvarCOND_ADIC));
	      wvarStep = 900;
	      
	      ps.setString(posiSP++, String.valueOf(mvarASEG_ADIC));
	      wvarStep = 910;
	      
	      if (mvarFH_NAC.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarFH_NAC));
	      }
	      wvarStep = 920;
	      
	      ps.setString(posiSP++, String.valueOf(mvarSEXO));
	      wvarStep = 930;
	      
	      ps.setString(posiSP++, String.valueOf(mvarESTCIV));
	      wvarStep = 940;
	      //Set wobjDBParm = wobjDBCmd.CreateParameter("@SIFMVEHI_DES", adChar, adParamInput, 35, mvarSIFMVEHI_DES) ' MQ 0093
	      // MQ 0051
	      
	      ps.setString(posiSP++, String.valueOf(mvarSIFMVEHI_DES));
	      wvarStep = 950;
	      
	      ps.setString(posiSP++, String.valueOf(mvarPROFECOD));
	      wvarStep = 960;
	      
	      ps.setString(posiSP++, String.valueOf(mvarSIACCESORIOS));
	      wvarStep = 970;
	      
	      if (mvarREFERIDO.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarREFERIDO));
	      }
	      wvarStep = 980;
	      
	      ps.setString(posiSP++, String.valueOf(mvarMotorNum));
	      wvarStep = 990;
	      
	      ps.setString(posiSP++, String.valueOf(mvarCHASINUM));
	      wvarStep = 800;
	      
	      ps.setString(posiSP++, String.valueOf(mvarPatenNum));
	      wvarStep = 810;
	      
	      if (mvarAUMARCOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarAUMARCOD));
	      }
	      wvarStep = 820;
	      
	      if (mvarAUMODCOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarAUMODCOD));
	      }
	      wvarStep = 830;
	      
	      if (mvarAUSUBCOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarAUSUBCOD));
	      }
	      wvarStep = 840;
	      
	      if (mvarAUADICOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarAUADICOD));
	      }
	      wvarStep = 850;
	      
	      ps.setString(posiSP++, String.valueOf(mvarAUMODORI));
	      wvarStep = 860;
	      
	      if (mvarAUUSOCOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarAUUSOCOD));
	      }
	      wvarStep = 870;
	      
	      ps.setString(posiSP++, String.valueOf(mvarAUVTVCOD));
	      wvarStep = 880;
	      
	      if (mvarAUVTVDIA.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarAUVTVDIA));
	      }
	      wvarStep = 890;
	      
	      if (mvarAUVTVMES.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarAUVTVMES));
	      }
	      wvarStep = 910;
	      
	      if (mvarAUVTVANN.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarAUVTVANN));
	      }
	      wvarStep = 920;
	      
	      if (mvarVEHCLRCOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarVEHCLRCOD));
	      }
	      wvarStep = 920;
	      
	      if (mvarAUKLMNUM.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarAUKLMNUM));
	      }
	      wvarStep = 930;
	      
	      if (mvarFABRICAN.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarFABRICAN));
	      }
	      wvarStep = 940;
	      
	      if (mvarFABRICMES.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarFABRICMES));
	      }
	      wvarStep = 950;
	      
	      ps.setString(posiSP++, String.valueOf(mvarGUGARAGE));
	      wvarStep = 960;
	      
	      ps.setString(posiSP++, String.valueOf(mvarGUDOMICI));
	      wvarStep = 970;
	      
	      if (mvarAUCATCOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarAUCATCOD));
	      }
	      wvarStep = 980;
	      
	      if (mvarAUTIPCOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarAUTIPCOD));
	      }
	      wvarStep = 990;
	      
	      ps.setString(posiSP++, String.valueOf(mvarAUCIASAN));
	      wvarStep = 1000;
	      
	      if (mvarAUANTANN.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarAUANTANN));
	      }
	      wvarStep = 1010;
	      
	      if (mvarAUNUMSIN.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarAUNUMSIN));
	      }
	      wvarStep = 1020;
	      
	      if (mvarAUNUMKMT.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarAUNUMKMT));
	      }
	      wvarStep = 1030;
	      
	      ps.setString(posiSP++, String.valueOf(mvarAUUSOGNC));
	      wvarStep = 1040;
	      
	      ps.setString(posiSP++, String.valueOf(mvarUSUARCOD));
	      wvarStep = 1050;
	      
	      ps.setString(posiSP++, String.valueOf(mvarSITUCPOL));
	      wvarStep = 1060;
	      
	      if (mvarCLIENSEC.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarCLIENSEC));
	      }
	      wvarStep = 1070;
	      
	      ps.setString(posiSP++, String.valueOf(mvarNUMEDOCU));
	      wvarStep = 1080;
	      
	      if (mvarTIPODOCU.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarTIPODOCU));
	      }
	      wvarStep = 1090;
	      
	      if (mvarDOMICSEC.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarDOMICSEC));
	      }
	      wvarStep = 1100;
	      
	      if (mvarCUENTSEC.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarCUENTSEC));
	      }
	      wvarStep = 1110;
	      
	      if (mvarCOBROFOR.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarCOBROFOR));
	      }
	      wvarStep = 1120;
	      
	      if (mvarEDADACTU.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarEDADACTU));
	      }
	      
	      ps.setString(posiSP++, String.valueOf(" "));
	      
	      //
	      // Coberturas
	      wvarStep = 1130;
	      for( wvarCounter = 1; wvarCounter <= 30; wvarCounter++ )
	      {
	    	  mvarCOBERCOD = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_COBERCOD + wvarCounter )  );
	    	  mvarCOBERORD = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_COBERORD + wvarCounter )  );
	    	  mvarCAPITASG = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_CAPITASG + wvarCounter )  );
	    	  mvarCAPITIMP = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_CAPITIMP + wvarCounter )  );
	    	  //
	    	  wvarStep = 1140;
	    	  
	    	  if (mvarCOBERCOD.isEmpty()) {
	    		  ps.setNull(posiSP++, Types.NUMERIC);
	    	  } else {
	    		  ps.setInt(posiSP++, Integer.parseInt(mvarCOBERCOD));
	    	  }
	    	  
	    	  //
	    	  wvarStep = 1150;
	    	  
	    	  if (mvarCOBERORD.isEmpty()) {
	    		  ps.setNull(posiSP++, Types.NUMERIC);
	    	  } else {
	    		  ps.setInt(posiSP++, Integer.parseInt(mvarCOBERORD));
	    	  }
	    	  
	    	  //
	    	  wvarStep = 1160;
	    	  
	    	  if (mvarCAPITASG.isEmpty()) {
	    		  ps.setNull(posiSP++, Types.DOUBLE);
	    	  } else {
	    		  double capitaSg = Double
	    		  .parseDouble(mvarCAPITASG.replace(",", "."));
	    		  ps.setDouble(posiSP++, capitaSg);
	    	  }
	    	  
	    	  //
	    	  wvarStep = 1170;
	    	  
	    	  if (mvarCAPITASG.isEmpty()) {
	    		  ps.setNull(posiSP++, Types.DOUBLE);
	    	  } else {
	    		  double capitimp = Double.parseDouble(mvarCAPITIMP.replace(",", "."));
	    		  ps.setDouble(posiSP++, capitimp);
	    	  }
	    	  

	    	  
	    	  
	    	  //
	      }
	      // Fin Coberturas
	      // Asegurados
	      wvarStep = 1180;
	      wobjXMLList = wobjXMLRequest.selectNodes( mcteParam_ASEGURADOS ) ;
	      //
	      wvarStep = 1190;
	      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
	      {
	    	  //
	    	  mvarDOCUMDAT = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_DOCUMDAT )  );
	    	  mvarNOMBREAS = Strings.left( Strings.trim( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_NOMBREAS )  ) ), 49 );
	    	  //
	    	  wvarStep = 1200;
	    	  
	    	  if (mvarDOCUMDAT.isEmpty()) {
	    		  ps.setNull(posiSP++, Types.NUMERIC);
	    	  } else {
	    		  ps.setInt(posiSP++, Integer.parseInt(mvarDOCUMDAT));
	    	  }
	    	  
	    	  
	    	  //
	    	  wvarStep = 1210;
	    	  ps.setString(posiSP++, String.valueOf(mvarNOMBREAS ));
	    	  
	    	  //
	      }
	      //
	      wvarStep = 1220;
	      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
	      {
	    	  //
	    	  wvarStep = 1230;
	    	  
	    	  ps.setInt(posiSP++, 0);
	    	  
	    	  
	    	  //
	    	  wvarStep = 1240;
	    	  ps.setString(posiSP++, String.valueOf(" " ));
	    	  
	    	  //
	      }
	      //
	      wobjXMLList = (org.w3c.dom.NodeList) null;
	      // Fin Asegurados
	      //
	      wvarStep = 1250;
	      
	      if (mvarCAMP_CODIGO.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarCAMP_CODIGO));
	      }
	      
	      
	      //
	      wvarStep = 1260;
	      
	      ps.setString(posiSP++, String.valueOf(Strings.left( Strings.trim( mvarCAMP_DESC ), 30 )));
	      
	      //
	      wvarStep = 1270;
	      
	      ps.setString(posiSP++, String.valueOf(mvarLEGAJO_GTE));
	      
	      //
	      wvarStep = 1280;
	      
	      if (mvarNRO_PROD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarNRO_PROD));
	      }
	      wvarStep = 1285;
	      
	      if (mvarEMPRESA_CODIGO.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarEMPRESA_CODIGO));
	      }
	      wvarStep = 1290;
	      
	      ps.setString(posiSP++, String.valueOf(mvarSUCURSAL_CODIGO));
	      //wobjDBParm.Precision = 6
	      //wobjDBParm.NumericScale = 0
	      wvarStep = 1300;
	      
	      ps.setString(posiSP++, String.valueOf(mvarLEGAJO_VEND));
	      wvarStep = 1305;
	      
	      ps.setString(posiSP++, String.valueOf(mvarEMPL));
	      // Hijos
	      wvarStep = 1310;
	      wobjXMLList = wobjXMLRequest.selectNodes( mcteParam_CONDUCTORES ) ;

	      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
	      {
	    	  mvarCONDUAPE = Strings.left( Strings.trim( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_CONDUAPE )  ) ), 20 );
	    	  mvarCONDUNOM = Strings.left( Strings.trim( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_CONDUNOM )  ) ), 30 );

	    	  wvarStep = 1320;
	    	  mvarCONDUFEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_CONDUFEC )  );
	    	  mvarCONDUSEX = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_CONDUSEX )  );
	    	  mvarCONDUEST = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_CONDUEST )  );
	    	  mvarCONDUEXC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_CONDUEXC )  );

	    	  wvarStep = 1330;
	    	  ps.setString(posiSP++, String.valueOf(mvarCONDUAPE ));
	    	  

	    	  wvarStep = 1340;
	    	  ps.setString(posiSP++, String.valueOf(mvarCONDUNOM ));
	    	  

	    	  wvarStep = 1350;
	    	  
	    	  if (mvarCONDUFEC.isEmpty()) {
	    		  ps.setNull(posiSP++, Types.NUMERIC);
	    	  } else {
	    		  ps.setInt(posiSP++, Integer.parseInt(mvarCONDUFEC));
	    	  }

	    	  wvarStep = 1360;
	    	  ps.setString(posiSP++, String.valueOf(mvarCONDUSEX ));
	    	  

	    	  wvarStep = 1370;
	    	  ps.setString(posiSP++, String.valueOf(mvarCONDUEST ));
	    	  

	    	  wvarStep = 1380;
	    	  ps.setString(posiSP++, String.valueOf(mvarCONDUEXC ));
	    	  
	      }

	      wvarStep = 1390;
	      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
	      {

	    	  wvarStep = 1400;
	    	  ps.setString(posiSP++, String.valueOf(" " ));

	    	  wvarStep = 1410;
	    	  ps.setString(posiSP++, String.valueOf(" " ));
	    	  
	    	  wvarStep = 1420;
	    	  ps.setInt(posiSP++, 0);
	    	  wvarStep = 1430;
	    	  ps.setString(posiSP++, String.valueOf(" " ));
	    	  
	    	  wvarStep = 1440;
	    	  ps.setString(posiSP++, String.valueOf(" " ));
	    	  

	    	  wvarStep = 1450;
	    	  ps.setString(posiSP++, String.valueOf(" " ));
	    	  

	      }

	      wvarStep = 1460;
	      wobjXMLList = (org.w3c.dom.NodeList) null;
	      // Conductores
	      // Accesorios
	      wvarStep = 1470;
	      wobjXMLList = wobjXMLRequest.selectNodes( mcteParam_ACCESORIOS ) ;

	      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
	      {
	    	  wvarStep = 1480;
	    	  mvarAUACCCOD = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_AUACCCOD )  );
	    	  mvarAUVEASUM = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_AUVEASUM )  );
	    	  mvarAUVEADES = Strings.left( Strings.trim( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_AUVEADES )  ) ), 30 );
	    	  mvarAUVEADEP = "S";

	    	  wvarStep = 1490;
	    	  
	    	  if (mvarAUACCCOD.isEmpty()) {
	    		  ps.setNull(posiSP++, Types.NUMERIC);
	    	  } else {
	    		  ps.setInt(posiSP++, Integer.parseInt(mvarAUACCCOD));
	    	  }
	    	  wvarStep = 1500;
	    	  if (mvarAUVEASUM.isEmpty()) {
	    		  ps.setInt(posiSP++, 0);
	    	  } else {
	    		  ps.setInt(posiSP++, Integer.parseInt(mvarAUVEASUM));
	    	  }
	    	  wvarStep = 1510;
	    	  ps.setString(posiSP++, String.valueOf(mvarAUVEADES ));
	    	  

	    	  wvarStep = 1520;
	    	  ps.setString(posiSP++, String.valueOf(mvarAUVEADEP ));
	    	  

	      }

	      wvarStep = 1530;
	      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
	      {
	    	  wvarStep = 1540;
	    	  ps.setInt(posiSP++, 0);
	    	  
	    	  wvarStep = 1550;
	    	  ps.setInt(posiSP++, 0);

	    	  wvarStep = 1560;
	    	  ps.setString(posiSP++, String.valueOf(" " ));
	    	  
	    	  wvarStep = 1570;
	    	  ps.setString(posiSP++, String.valueOf(" " ));
	    	  

	      }

	      wvarStep = 1580;
	      wobjXMLList = (org.w3c.dom.NodeList) null;
	      // Fin Accesorios
	      wvarStep = 1590;
	      
	      if (mvarESTAINSP.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarESTAINSP));
	      }
	      wvarStep = 1600;
	      
	      if (mvarINSPECOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarINSPECOD));
	      }
	      wvarStep = 1610;
	      
	      ps.setString(posiSP++, String.valueOf(mvarINSPEDES));
	      wvarStep = 1620;
	      
	      ps.setString(posiSP++, String.valueOf(mvarINSPADOM));
	      wvarStep = 1630;
	      
	      ps.setString(posiSP++, String.valueOf(mvarINSPEOBS));
	      wvarStep = 1640;
	      
	      if (mvarINSPEKMS.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarINSPEKMS));
	      }
	      wvarStep = 1650;
	      
	      ps.setString(posiSP++, String.valueOf(mvarCENSICOD));
	      //wobjDBParm.Precision = 4
	      //wobjDBParm.NumericScale = 0
	      wvarStep = 1660;
	      
	      ps.setString(posiSP++, String.valueOf(mvarCENSIDES));
	      //    wvarStep = 1570
	      //    Set wobjDBParm = wobjDBCmd.CreateParameter("@INSPECC", adNumeric, adParamInput, , mvarINSPECC)
	      //    wobjDBParm.Precision = 2
	      //    wobjDBParm.NumericScale = 0
	      //    wobjDBCmd.Parameters.Append wobjDBParm
	      //    Set wobjDBParm = Nothing
	      wvarStep = 1670;
	      
	      if (mvarDISPOCOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarDISPOCOD));
	      }
	      wvarStep = 1680;
	      
	      ps.setString(posiSP++, String.valueOf(mvarPRESTCOD));
	      //wobjDBParm.Precision = 2
	      //wobjDBParm.NumericScale = 0
	      wvarStep = 1690;
	      
	      ps.setString(posiSP++, String.valueOf(mvarNUMEDOCU_ACRE));
	      wvarStep = 1700;
	      
	      if (mvarTIPODOCU_ACRE.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarTIPODOCU_ACRE));
	      }
	      wvarStep = 1710;
	      
	      ps.setString(posiSP++, String.valueOf(mvarAPELLIDO));
	      wvarStep = 1720;
	      
	      ps.setString(posiSP++, String.valueOf(mvarNOMBRES));
	      wvarStep = 1730;
	      
	      ps.setString(posiSP++, String.valueOf(mvarDOMICDOM));
	      wvarStep = 1740;
	      
	      ps.setString(posiSP++, String.valueOf(mvarDOMICDNU));
	      wvarStep = 1750;
	      
	      ps.setString(posiSP++, String.valueOf(mvarDOMICESC));
	      wvarStep = 1760;
	      
	      ps.setString(posiSP++, String.valueOf(mvarDOMICPIS));
	      wvarStep = 1770;
	      
	      ps.setString(posiSP++, String.valueOf(mvarDOMICPTA));
	      wvarStep = 1780;
	      
	      ps.setString(posiSP++, String.valueOf(mvarDOMICPOB));
	      wvarStep = 1790;
	      
	      if (mvarDOMICCPO.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarDOMICCPO));
	      }
	      wvarStep = 1800;
	      
	      if (mvarPROVICOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarPROVICOD));
	      }
	      wvarStep = 1810;
	      
	      ps.setString(posiSP++, String.valueOf(mvarPAISSCOD));
	      wvarStep = 1820;
	      
	      ps.setString(posiSP++, String.valueOf(mvarBANCOCOD));
	      wvarStep = 1830;
	      
	      if (mvarCOBROCOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarCOBROCOD));
	      }
	      wvarStep = 1840;
	      
	      if (mvarEfectAnn.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarEfectAnn));
	      }
	      wvarStep = 1850;
	      
	      if (mvarEFECTMES.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarEFECTMES));
	      }
	      wvarStep = 1860;
	      
	      if (mvarEFECTDIA.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarEFECTDIA));
	      }
	      wvarStep = 1870;
	      
	      if (mvarINSPEANN.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarINSPEANN));
	      }
	      wvarStep = 1880;
	      
	      if (mvarINSPEMES.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarINSPEMES));
	      }
	      wvarStep = 1890;
	      
	      if (mvarINSPEDIA.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarINSPEDIA));
	      }
	      wvarStep = 1895;
	      
	      if (mvarENTDOMSC.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarENTDOMSC));
	      }
	      wvarStep = 1896;
	      
	      ps.setString(posiSP++, String.valueOf(mvarANOTACIO));
	      wvarStep = 1897;
	      
	      
	      double pcioMensu = Double.parseDouble(mvarPRECIO_MENSUAL.replace(",", "."));
	      ps.setDouble(posiSP++, pcioMensu);
	      
	      
	      ps.setString(posiSP++, String.valueOf(mvarTIPO_ACTUA));
	      
	      
	      wvarStep = 1898;
	      
	      ps.setString(posiSP++, String.valueOf(mvarCUITNUME));
	      wvarStep = 1899;
	      
	      ps.setString(posiSP++, String.valueOf(mvarRAZONSOC));
	      wvarStep = 1900;
	      
	      ps.setString(posiSP++, String.valueOf(mvarCLIEIBTP));
	      wvarStep = 1901;
	      
	      ps.setString(posiSP++, String.valueOf(mvarNROIIBB));
	      wvarStep = 1902;
	      
	      ps.setString(posiSP++, String.valueOf(mvarLUNETA));
	      wvarStep = 1903;
	      
	      if (mvarHIJOS1729_EXC.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarHIJOS1729_EXC));
	      }
	      wvarStep = 1904;
	      
	      ps.setString(posiSP++, String.valueOf(mvarNROFACTU));
	      wvarStep = 1905;
	      
	      ps.setString(posiSP++, String.valueOf(mvarINS_DOMICDOM));
	      wvarStep = 1910;
	      
	      ps.setString(posiSP++, String.valueOf(mvarINS_CPACODPO));
	      wvarStep = 1920;
	      
	      ps.setString(posiSP++, String.valueOf(mvarINS_DOMICPOB));
	      wvarStep = 1930;
	      
	      if (mvarINS_PROVICOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarINS_PROVICOD));
	      }
	      wvarStep = 1940;
	      
	      ps.setString(posiSP++, String.valueOf(mvarINS_DOMICTLF));
	      wvarStep = 1950;
	      
	      if (mvarINS_RANGOHOR.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(mvarINS_RANGOHOR));
	      }
	      wvarStep = 1960;
	      
	      ps.setString(posiSP++, String.valueOf(mvarDDJJ));
	      wvarStep = 1970;
	      
	      ps.setString(posiSP++, String.valueOf(mvarDERIVACI));
	      wvarStep = 1980;
	      
	      ps.setString(posiSP++, String.valueOf(mvarALEATORIO));
	      wvarStep = 1990;
	      
	      ps.setString(posiSP++, String.valueOf(mvarESTAIDES));
	      wvarStep = 2000;
	      
	      ps.setString(posiSP++, String.valueOf(mvarDISPODES));
	      	      wvarStep = 2001;
	      
	      ps.setString(posiSP++, String.valueOf(mvarINSTALADP));
	      wvarStep = 2002;
	      
	      ps.setString(posiSP++, String.valueOf(mvarPOSEEDISP));
	      wvarStep = 2003;
	      
	      ps.setString(posiSP++, String.valueOf(mvarAUTIPGAMA));
	      //23/01/2006
	      wvarStep = 2004;
	      
	      ps.setString(posiSP++, String.valueOf(mvarDEST80));
	      if( ! (wvarTIPOPROD.equals( "" )) )
	      {
	    	  //22/3/2006 Ale
	    	  wvarStep = 2005;
	    	  
	    	  ps.setString(posiSP++, String.valueOf(wvarTIPOPROD));
	    	  
	      }

	      //Comienzo datos exigidos por la UIF
	      wvarStep = 2006;
	      
	      ps.setString(posiSP++, String.valueOf(wvarSWAPODER));
	      wvarStep = 2007;
	      
	      ps.setString(posiSP++, String.valueOf(wvarOCUPACODCLIE));
	      wvarStep = 2008;
	      
	      ps.setString(posiSP++, String.valueOf(wvarOCUPACODAPOD));
	      wvarStep = 2009;
	      
	      if (wvarCLIENSECAPOD.isEmpty()) {
	    	  ps.setNull(posiSP++, Types.NUMERIC);
	      } else {
	    	  ps.setInt(posiSP++, Integer.parseInt(wvarCLIENSECAPOD));
	      }
	      wvarStep = 2010;
	      
	      ps.setString(posiSP++, String.valueOf(wvarDOMISECAPOD));
	      
	      //Fin datos exigidos por la UIF
	      //Green Products
	      wvarStep = 2011;
	      
	      ps.setString(posiSP++, String.valueOf(wvarCLUBECO));
	      wvarStep = 2012;
	      
	      ps.setString(posiSP++, String.valueOf(wvarGRANIZO));
	      wvarStep = 2013;
	      
	      ps.setString(posiSP++, String.valueOf(wvarROBOCONT));
	      
	      //Fin Green Products
	      // **************************************************************************************
	      // EMPIEZA - SUSCRIPCION A NBWS Y EPOLIZA
	      // **************************************************************************************
	      //SN_NBWS
	      wvarStep = 2022;
	      
	      ps.setString(posiSP++, String.valueOf(mvarSN_NBWS));
	      
	      //EMAIL_NBWS
	      wvarStep = 2023;
	      
	      ps.setString(posiSP++, String.valueOf(mvarEMAIL_NBWS));
	      
	      //SN_EPOLIZA
	      wvarStep = 2024;
	      
	      ps.setString(posiSP++, String.valueOf(mvarSN_EPOLIZA));
	      
	      //EMAIL_EPOLIZA
	      wvarStep = 2025;
	      
	      ps.setString(posiSP++, String.valueOf(mvarEMAIL_EPOLIZA));
	      
	      //SN_PRESTAMAIL
	      wvarStep = 2026;
	      
	      ps.setString(posiSP++, String.valueOf(mvarSN_PRESTAMAIL));
	      
	      //13/10/2009 Se agrega Pass de EPoliza
	      //PASSEPOL
	      wvarStep = 2027;
	      
	      ps.setString(posiSP++, String.valueOf(mvarPASSEPOL));
	      
	

      // *******************************************
      // TERMINA - SUSCRIPCION A NBWS Y EPOLIZA
      // *******************************************
      //'''''''''''''''''''''' Ver parametros '''''''''''''''
      //    wvarStep = 2010
      //    Dim aa
      //    Dim i
      //    For i = 1 To wobjDBCmd.Parameters.Count - 1
      //        If wobjDBCmd.Parameters(i).Type = adChar Then
      //            aa = aa & "'" & wobjDBCmd.Parameters(i).Value & "',"
      //        Else
      //            aa = aa & wobjDBCmd.Parameters(i).Value & ","
      //        End If
      //    Next
      //App.LogEvent "@CENSICOD: '" & mvarCENSICOD & "'"
      //App.LogEvent "ParamslENGTH: '" & Len(aa) & "'"
      //App.LogEvent "Params: '" & aa & "'"
      //App.LogEvent "Params: '" & Mid(aa, 967) & "'"
      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      wvarStep = 2029;
      //wobjDBCmd.NamedParameters = True
//      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdStoredProc );
      
      //Se setean en NULL los campos faltantes
      for (int posiNull = 0; posiNull < 2; posiNull++) {
    	  ps.setNull(posiSP++, Types.CHAR);
      }
      
      boolean result = ps.execute();

      if (result) {
    	  //El primero es un ResultSet
      } else {
    	  //Salteo el "updateCount"
    	  int uc = ps.getUpdateCount();
    	  if (uc  == -1) {
//    		  System.out.println("No hay resultados");
    	  }
    	  result = ps.getMoreResults();
      }
      int returnValue = ps.getInt(1);
      
      wvarStep = 2030;
      if(returnValue >= 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><CADENA>" + returnValue + "</CADENA>" + mvarXMLCoberturas + "</Response>" );
      }
      else
      {
        
        if(returnValue == -1 )
        {
          wvarMensaje = "Error en la tabla SIFSPOLI";
        }
        else if(returnValue == -2 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 1";
        }
        else if(returnValue == -3 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 2";
        }
        else if(returnValue == -4 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 3";
        }
        else if(returnValue == -5 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 4";
        }
        else if(returnValue == -6 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 5";
        }
        else if(returnValue == -7 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 6";
        }
        else if(returnValue == -8 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 7";
        }
        else if(returnValue == -9 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 8";
        }
        else if(returnValue == -10 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 9";
        }
        else if(returnValue == -11 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 10";
        }
        else if(returnValue == -12 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 1";
        }
        else if(returnValue == -13 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 2";
        }
        else if(returnValue == -14 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 3";
        }
        else if(returnValue == -15 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 4";
        }
        else if(returnValue == -16 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 5";
        }
        else if(returnValue == -17 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 6";
        }
        else if(returnValue == -18 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 7";
        }
        else if(returnValue == -19 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 8";
        }
        else if(returnValue == -20 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 9";
        }
        else if(returnValue == -21 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 10";
        }
        else if(returnValue == -22 )
        {
          wvarMensaje = "Error en la tabla SIFSMOVI                ";
        }
        else if(returnValue == -23 )
        {
          wvarMensaje = "Error en la tabla SIFSFIMA_TARI               ";
        }
        else if(returnValue == -24 )
        {
          wvarMensaje = "Error en la tabla SIFSFIMA_VEHI               ";
        }
        else if(returnValue == -25 )
        {
          wvarMensaje = "Error en la tabla SIFSPECO                ";
        }
        else if(returnValue == -26 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_CAMP               ";
        }
        else if(returnValue == -27 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_GTE                ";
        }
        else if(returnValue == -28 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_PROD               ";
        }
        else if(returnValue == -29 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_SUCU               ";
        }
        else if(returnValue == -30 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_VEND               ";
        }
        else if(returnValue == -31 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 1         ";
        }
        else if(returnValue == -32 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 2         ";
        }
        else if(returnValue == -33 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 3         ";
        }
        else if(returnValue == -34 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 4         ";
        }
        else if(returnValue == -35 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 5         ";
        }
        else if(returnValue == -36 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 6         ";
        }
        else if(returnValue == -37 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 7         ";
        }
        else if(returnValue == -38 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 8         ";
        }
        else if(returnValue == -39 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 9         ";
        }
        else if(returnValue == -40 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 10        ";
        }
        else if(returnValue == -41 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 1         ";
        }
        else if(returnValue == -42 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 2         ";
        }
        else if(returnValue == -43 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 3         ";
        }
        else if(returnValue == -44 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 4         ";
        }
        else if(returnValue == -45 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 5         ";
        }
        else if(returnValue == -46 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 6         ";
        }
        else if(returnValue == -47 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 7         ";
        }
        else if(returnValue == -48 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 8         ";
        }
        else if(returnValue == -49 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 9         ";
        }
        else if(returnValue == -50 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 10        ";
        }
        else if(returnValue == -51 )
        {
          wvarMensaje = "Error en la tabla SIFWINSV                ";
        }
        else if(returnValue == -52 )
        {
          wvarMensaje = "El cliente no pertenece al usuario";
        }
        else if(returnValue == -60 )
        {
          wvarMensaje = "Error al insertar en la tabla SIFWFIMA_EMPRESA";
        }
        else if(returnValue == -65 )
        {
          wvarMensaje = "Error al insertar en la tabla PROD_SIFW1UIF";
        }
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='" + wvarMensaje + "' /></Response>" );
      }
      } finally {
          try {
              if (jdbcConn != null)
                  jdbcConn.close();
          } catch (Exception e) {
              logger.log(Level.WARNING, "Exception al hacer un close", e);
          }
      }
      //
      wvarStep = 2040;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      wvarStep = 2050;
      wobjXMLRequest = null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
	  } catch( Exception e ) {
	      logger.log(Level.SEVERE, this.getClass().getName(), e);
	      throw new ComponentExecutionException(e);
    }
  }

  private String GetCoberturas(XmlDomExtended pobjXMLCoberturas, boolean pvarConHijos ) throws Exception
  {
    String GetCoberturas = "";
    XmlDomExtended wobjXMLResponse = null;
    org.w3c.dom.Element wobjNodoCoberturas = null;
    org.w3c.dom.Element wobjNodoCobertura = null;
    org.w3c.dom.Element wobjCOBERCOD = null;
    org.w3c.dom.Element wobjCOBERORD = null;
    org.w3c.dom.Element wobjCAPITASG = null;
    org.w3c.dom.Element wobjCAPITIMP = null;
    String wvarResponse = "";
    int wvarCounter = 0;

    wobjXMLResponse = new XmlDomExtended();
    wobjXMLResponse.loadXML( "<COBERTURAS></COBERTURAS>" );
    wobjNodoCoberturas = (org.w3c.dom.Element) wobjXMLResponse.selectSingleNode( "//COBERTURAS" ) ;

    for( wvarCounter = 1; wvarCounter <= 30; wvarCounter++ )
    {
      //For wvarCounter = 1 To 20
      if( VBFixesUtil.val( Strings.replace( XmlDomExtended.getText( pobjXMLCoberturas.selectSingleNode( (mcteParam_CAPITIMP + wvarCounter) )  ), ",", "." ) ) != 0 )
      {
        wobjNodoCobertura = wobjXMLResponse.getDocument().createElement( "COBERTURA" );
        wobjNodoCoberturas.appendChild( wobjNodoCobertura );
        //
        wobjCOBERCOD = wobjXMLResponse.getDocument().createElement( Strings.mid( mcteParam_COBERCOD, 3 ) );
        wobjNodoCobertura.appendChild( wobjCOBERCOD );
        XmlDomExtended.setText( wobjCOBERCOD, XmlDomExtended.getText( pobjXMLCoberturas.selectSingleNode( mcteParam_COBERCOD + wvarCounter )  ) );
        //
        wobjCOBERORD = wobjXMLResponse.getDocument().createElement( Strings.mid( mcteParam_COBERORD, 3 ) );
        wobjNodoCobertura.appendChild( wobjCOBERORD );
        XmlDomExtended.setText( wobjCOBERORD, XmlDomExtended.getText( pobjXMLCoberturas.selectSingleNode( mcteParam_COBERORD + wvarCounter )  ) );
        //
        wobjCAPITASG = wobjXMLResponse.getDocument().createElement( Strings.mid( mcteParam_CAPITASG, 3 ) );
        wobjNodoCobertura.appendChild( wobjCAPITASG );
        XmlDomExtended.setText( wobjCAPITASG, Strings.replace( XmlDomExtended.getText( pobjXMLCoberturas.selectSingleNode( mcteParam_CAPITASG + wvarCounter )  ), ",", "." ) );
        //
        wobjCAPITIMP = wobjXMLResponse.getDocument().createElement( Strings.mid( mcteParam_CAPITIMP, 3 ) );
        wobjNodoCobertura.appendChild( wobjCAPITIMP );
        XmlDomExtended.setText( wobjCAPITIMP, Strings.replace( XmlDomExtended.getText( pobjXMLCoberturas.selectSingleNode( mcteParam_CAPITIMP + wvarCounter )  ), ",", "." ) );
      }
    }
    GetCoberturas = XmlDomExtended.marshal(wobjXMLResponse.getDocument().getDocumentElement());
    ClearObjects: 
    wobjCOBERCOD = (org.w3c.dom.Element) null;
    wobjCOBERORD = (org.w3c.dom.Element) null;
    wobjCAPITASG = (org.w3c.dom.Element) null;
    wobjCAPITIMP = (org.w3c.dom.Element) null;
    wobjXMLResponse = null;
    return GetCoberturas;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
