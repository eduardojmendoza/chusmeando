package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVValidaFecIns implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVSQLCotizar.lbaw_OVValidaFecIns";
  static final String mcteStoreProc = "SPSNCV_PROD_VALIDA_FECHAS";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_EMISIANN = "//EMISIANN";
  static final String mcteParam_EMISIMES = "//EMISIMES";
  static final String mcteParam_EMISIDIA = "//EMISIDIA";
  static final String mcteParam_EFECTANN = "//EFECTANN";
  static final String mcteParam_EFECTMES = "//EFECTMES";
  static final String mcteParam_EFECTDIA = "//EFECTDIA";
  static final String mcteParam_INSPEANN = "//INSPEANN";
  static final String mcteParam_INSPEMES = "//INSPEMES";
  static final String mcteParam_INSPEDIA = "//INSPEDIA";
  static final String mcteParam_ESTAINSP = "//ESTAINSP";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private com.qbe.services.HSBCInterfaces.IDBConnection mobjHSBC_DBCnn = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarEMISIANN = "";
    String wvarEMISIMES = "";
    String wvarEMISIDIA = "";
    String wvarEFECTANN = "";
    String wvarEFECTMES = "";
    String wvarEFECTDIA = "";
    String wvarINSPEANN = "";
    String wvarINSPEMES = "";
    String wvarINSPEDIA = "";
    String wvarESTAINSP = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_EMISIANN ) */.getLength() > 0 )
      {
        wvarEMISIANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMISIANN ) */ );
      }
      else
      {
        wvarEMISIANN = String.valueOf( DateTime.year( DateTime.now() ) );
      }
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_EMISIMES ) */.getLength() > 0 )
      {
        wvarEMISIMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMISIMES ) */ );
      }
      else
      {
        wvarEMISIMES = Strings.right( Strings.fill( 2, "0" ) + DateTime.month( DateTime.now() ), 2 );
      }
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_EMISIDIA ) */.getLength() > 0 )
      {
        wvarEMISIDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMISIDIA ) */ );
      }
      else
      {
        wvarEMISIDIA = Strings.right( Strings.fill( 2, "0" ) + DateTime.day( DateTime.now() ), 2 );
      }
      wvarEFECTANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTANN ) */ );
      wvarEFECTMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTMES ) */ );
      wvarEFECTDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTDIA ) */ );
      wvarINSPEANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEANN ) */ );
      wvarINSPEMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEMES ) */ );
      wvarINSPEDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEDIA ) */ );
      wvarESTAINSP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ESTAINSP ) */ );
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 20;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //
      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      //
      wvarStep = 40;
      wobjDBCmd = new Command();
      //
      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 60;
      wobjDBParm = new Parameter( "@EMISIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarEMISIANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 70;
      wobjDBParm = new Parameter( "@EMISIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarEMISIMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 80;
      wobjDBParm = new Parameter( "@EMISIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarEMISIDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 90;
      wobjDBParm = new Parameter( "@EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarEFECTANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 100;
      wobjDBParm = new Parameter( "@EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarEFECTMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 110;
      wobjDBParm = new Parameter( "@EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarEFECTDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 120;
      wobjDBParm = new Parameter( "@INSPEANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarINSPEANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 130;
      wobjDBParm = new Parameter( "@INSPEMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarINSPEMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 140;
      wobjDBParm = new Parameter( "@INSPEDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarINSPEDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 150;
      wobjDBParm = new Parameter( "@ESTAINSP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarESTAINSP ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 160;
      wobjDBParm = new Parameter( "@ESTADO", AdoConst.adNumeric, AdoConst.adParamOutput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 170;
      wobjDBParm = new Parameter( "@LEYENDA", AdoConst.adChar, AdoConst.adParamOutput, 200, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 120;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );
      //
      wvarStep = 130;
      if( ! (wrstDBResult.isEOF()) )
      {
        //
        if( wrstDBResult.getFields().getField("ESTADO").getValue().toInt() != 0 )
        {
          Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + Strings.trim( wrstDBResult.getFields().getField("LEYENDA").getValue().toString() ) + String.valueOf( (char)(34) ) + " /></Response>" );
        }
        else
        {
          wvarResult = "";
          Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
        }
      }
      wvarStep = 210;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 220;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 230;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 240;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 250;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 260;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 270;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        if( ! (wrstDBResult == (Recordset) null) )
        {
          if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='ROW'>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ESTADO'><xsl:value-of select='@ESTADO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='LEYENDA'><xsl:value-of select='@LEYENDA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='LEYENDA'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
