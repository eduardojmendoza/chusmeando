package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVInspPendXUsu implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVSQLCotizar.lbaw_OVInspPendXUsu";
  static final String mcteStoreProc = "SPSNCV_PROD_VERIF_INSPE";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARCOD = "//USUARIO";
  static final String mcteParam_LSTAGENTCODXML = "//LST_AGENTCOD_XML";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarUSUARCOD = "";
    String wvarLSTAGENTCODXML = "";
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //wvarUSUARCOD = wobjXMLRequest.selectSingleNode(mcteParam_USUARCOD).Text
      //Ale 27/3
      wvarLSTAGENTCODXML = null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LSTAGENTCODXML ) */.getChildNodes().item( 0 ).toString();
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 20;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //
      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      //
      wvarStep = 40;
      wobjDBCmd = new Command();
      //
      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARCOD", adChar, adParamInput, 10, wvarUSUARCOD)
      //wobjDBCmd.Parameters.Append wobjDBParm
      //Set wobjDBParm = Nothing
      //
      wvarStep = 60;
      wobjDBParm = new Parameter( "@LST_AGENTCOD_XML", AdoConst.adVarChar, AdoConst.adParamInput, 1000, new Variant( wvarLSTAGENTCODXML ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 70;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );
      //
      wvarStep = 80;
      if( ! (wrstDBResult.isEOF()) )
      {
        //
        if( wrstDBResult.getFields().getField("ESTADO").getValue().toString().equals( "B" ) )
        {
          Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='Usted tiene inspecciones en estado pendiente, por favor gestione las mismas de lo contrario no podr� continuar cotizando.' /></Response>" );
        }
        else
        {
          wvarResult = "";
          Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
        }
      }
      //
      wvarStep = 90;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 100;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 110;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 120;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 130;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 140;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 150;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        if( ! (wrstDBResult == (Recordset) null) )
        {
          if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
