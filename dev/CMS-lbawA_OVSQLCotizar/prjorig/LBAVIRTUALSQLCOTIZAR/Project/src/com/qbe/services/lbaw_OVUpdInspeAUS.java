package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * 04/09/2007 - FO
 * PPCR 20060393-1 - Se agrega un nuevo elemento en el XML, que es el que tiene la info de la oblea para el caso GNC.
 * Objetos del FrameWork
 */

public class lbaw_OVUpdInspeAUS implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVSQLCotizar.lbaw_OVUpdInspeAUS";
  static final String mcteStoreProc = "SPSNCV_PROD_SOLI_UPDATE_INSPE";
  /**
   *  Parametros XML de Entrada
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_SUPLENUM = "//SUPLENUM";
  static final String mcteParam_MotorNum = "//MOTORNUM";
  static final String mcteParam_CHASINUM = "//CHASINUM";
  static final String mcteParam_PatenNum = "//PATENNUM";
  /**
   * "//SITUCEST"
   */
  static final String mcteParam_ESTAINSP = "//ESTAINSP";
  static final String mcteParam_INSPECOD = "//INSPECOD";
  static final String mcteParam_INSPEDES = "//INSPECTOR";
  static final String mcteParam_INSPADOM = "//INSPEADOM";
  static final String mcteParam_INSPEOBS = "//OBSERTXT";
  /**
   * "//INSPENUM"
   */
  static final String mcteParam_INSPEKMS = "//INSPEKMS";
  static final String mcteParam_INSPEOBLEA = "//INSPEOBLEA";
  static final String mcteParam_CENSICOD = "//CENTRCOD_INS";
  static final String mcteParam_CENSIDES = "//CENTRDES";
  static final String mcteParam_INSPEANN = "//INSPEANN";
  static final String mcteParam_INSPEMES = "//INSPEMES";
  static final String mcteParam_INSPEDIA = "//INSPEDIA";
  static final String mcteParam_EFECTANN = "//EFECTANN";
  static final String mcteParam_EFECTMES = "//EFECTMES";
  static final String mcteParam_EFECTDIA = "//EFECTDIA";
  static final String mcteParam_SITUCPOL = "//SITUCPOL";
  static final String mcteParam_ESTAIDES = "//ESTAIDES";
  static final String mcteParam_DISPODES = "//DISPODES";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private com.qbe.services.HSBCInterfaces.IDBConnection mobjHSBC_DBCnn = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * -
   */
  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLValidacion = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLCoberturas = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    Object wobjHSBC_DBCnn = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String wvarRequest = "";
    String wvarResponse = "";
    String mvarRAMOPCOD = "";
    String mvarPOLIZANN = "";
    String mvarPOLIZSEC = "";
    String mvarCERTIPOL = "";
    String mvarCERTIANN = "";
    String mvarCERTISEC = "";
    String mvarSUPLENUM = "";
    String mvarMotorNum = "";
    String mvarCHASINUM = "";
    String mvarPatenNum = "";
    String mvarESTAINSP = "";
    String mvarINSPECOD = "";
    String mvarINSPEDES = "";
    String mvarINSPADOM = "";
    String mvarINSPEOBS = "";
    String mvarINSPEKMS = "";
    String mvarINSPEOBLEA = "";
    String mvarCENSICOD = "";
    String mvarCENSIDES = "";
    String mvarINSPEANN = "";
    String mvarINSPEMES = "";
    String mvarINSPEDIA = "";
    String mvarEfectAnn = "";
    String mvarEFECTMES = "";
    String mvarEFECTDIA = "";
    String mvarSITUCPOL = "";
    String mvarESTAIDES = "";
    String mvarDISPODES = "";
    //
    //
    //
    // DATOS GENERALES
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // Cargo el xml de entrada
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      wobjClass = new Variant( new com.qbe.services.lbaw_OVValidaFecIns() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.lbaw_OVValidaFecIns().toObject());
      wobjClass.Execute( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarResponse, "" );
      wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
      //
      // Guardo las coberturas y las sumas aseguradas
      wvarStep = 30;
      wobjXMLValidacion = new diamondedge.util.XmlDom();
      //unsup wobjXMLValidacion.async = false;
      wobjXMLValidacion.loadXML( wvarResponse );
      //
      wvarStep = 40;
      // Controlo como devolvi� la cotizaci�n
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLValidacion.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLValidacion.selectSingleNode( "//Response/Estado/@mensaje" ) */ ) + "' /></Response>" );
      }
      else
      {
        //
        wvarStep = 50;
        //
        wvarStep = 60;
        // DATOS GENERALES
        mvarRAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD ) */ );
        mvarPOLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN ) */ );
        mvarPOLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC ) */ );
        mvarCERTIPOL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL ) */ );
        mvarCERTIANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN ) */ );
        mvarCERTISEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC ) */ );
        wvarStep = 70;
        mvarSUPLENUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUPLENUM ) */ );
        mvarMotorNum = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MotorNum ) */ );
        mvarCHASINUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CHASINUM ) */ );
        mvarPatenNum = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PatenNum ) */ );
        mvarESTAINSP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ESTAINSP ) */ );
        mvarINSPECOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPECOD ) */ );
        wvarStep = 80;
        mvarINSPEDES = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEDES ) */ ) ), 30 );
        mvarINSPADOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPADOM ) */ );
        mvarINSPEOBS = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEOBS ) */ ) ), 50 );
        mvarINSPEKMS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEKMS ) */ );
        // ------------------------------------------------------------------
        //La oblea se guarda como parte de la descripci�n de la inspecci�n.
        // S�lo se guardan los primeros 50 caracteres.
        mvarINSPEOBLEA = Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEOBLEA ) */ ) );
        if( Strings.len( mvarINSPEOBLEA ) > 0 )
        {
          mvarINSPEOBS = Strings.left( "Nro Oblea: " + mvarINSPEOBLEA + ". " + mvarINSPEOBS, 50 );
        }
        // ------------------------------------------------------------------
        mvarCENSICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CENSICOD ) */ );
        mvarCENSIDES = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CENSIDES ) */ ) ), 30 );
        wvarStep = 90;
        mvarINSPEANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEANN ) */ );
        mvarINSPEMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEMES ) */ );
        mvarINSPEDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEDIA ) */ );
        mvarEfectAnn = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTANN ) */ );
        mvarEFECTMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTMES ) */ );
        mvarEFECTDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTDIA ) */ );
        wvarStep = 100;
        mvarSITUCPOL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SITUCPOL ) */ );
        mvarESTAIDES = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ESTAIDES ) */ ), 30 );
        mvarDISPODES = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DISPODES ) */ ), 30 );
        //
        wvarStep = 110;
        wobjXMLParametros = (diamondedge.util.XmlDom) null;
        //
        wvarStep = 120;
        wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
        //error: function 'GetDBConnection' was not found.
        //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
        wobjDBCmd = new Command();

        wvarStep = 130;
        wobjDBCmd.setActiveConnection( wobjDBCnn );
        wobjDBCmd.setCommandText( mcteStoreProc );
        wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

        wvarStep = 140;
        wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( "0" ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        // Parametros
        wvarStep = 150;
        wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarRAMOPCOD ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 160;
        wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPOLIZANN ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 170;
        wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPOLIZSEC ) );
        wobjDBParm.setPrecision( (byte)( 6 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 180;
        wobjDBParm = new Parameter( "@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCERTIPOL ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 190;
        wobjDBParm = new Parameter( "@CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCERTIANN ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 200;
        wobjDBParm = new Parameter( "@CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCERTISEC ) );
        wobjDBParm.setPrecision( (byte)( 6 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 210;
        wobjDBParm = new Parameter( "@SUPLENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarSUPLENUM ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 220;
        wobjDBParm = new Parameter( "@MOTORNUM", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarMotorNum ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 230;
        wobjDBParm = new Parameter( "@CHASINUM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarCHASINUM ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 240;
        wobjDBParm = new Parameter( "@PATENNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarPatenNum ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 250;
        wobjDBParm = new Parameter( "@ESTAINSP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarESTAINSP ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 260;
        wobjDBParm = new Parameter( "@INSPECOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPECOD ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 270;
        wobjDBParm = new Parameter( "@INSPEDES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarINSPEDES ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 280;
        wobjDBParm = new Parameter( "@INSPADOM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarINSPADOM ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 290;
        wobjDBParm = new Parameter( "@INSPEOBS", AdoConst.adChar, AdoConst.adParamInput, 50, new Variant( mvarINSPEOBS ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 300;
        wobjDBParm = new Parameter( "@INSPEKMS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPEKMS ) );
        wobjDBParm.setPrecision( (byte)( 8 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 310;
        wobjDBParm = new Parameter( "@CENSICOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarCENSICOD ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 320;
        wobjDBParm = new Parameter( "@CENSIDES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarCENSIDES ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 330;
        wobjDBParm = new Parameter( "@INSPEANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPEANN ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 340;
        wobjDBParm = new Parameter( "@INSPEMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPEMES ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 350;
        wobjDBParm = new Parameter( "@INSPEDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPEDIA ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 360;
        wobjDBParm = new Parameter( "@EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarEfectAnn ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 370;
        wobjDBParm = new Parameter( "@EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarEFECTMES ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 380;
        wobjDBParm = new Parameter( "@EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarEFECTDIA ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 390;
        wobjDBParm = new Parameter( "@SITUCPOL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarSITUCPOL ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 400;
        wobjDBParm = new Parameter( "@ESTAIDES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarESTAIDES ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 410;
        wobjDBParm = new Parameter( "@DISPODES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarDISPODES ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 420;
        wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );

        wvarStep = 430;
        if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
        {
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><CADENA>" + wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue() + "</CADENA></Response>" );
        }
        else
        {
          
          if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -1 )
          {
            wvarMensaje = "Error en la tabla SIFWINSVA";
          }
          else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -2 )
          {
            wvarMensaje = "Error en la tabla SEFWFIMA_VEHI";
          }
          else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -3 )
          {
            wvarMensaje = "Error en la tabla SEFWFIMA_POLI";
          }
          else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -4 )
          {
            wvarMensaje = "Error en la tabla SEFWFIMA_MOVI";
          }
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='" + wvarMensaje + "' /></Response>" );
        }
        //
      }
      //
      wvarStep = 440;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      wvarStep = 450;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLValidacion = (diamondedge.util.XmlDom) null;
      //
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;
        wobjXMLValidacion = (diamondedge.util.XmlDom) null;
        //
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
