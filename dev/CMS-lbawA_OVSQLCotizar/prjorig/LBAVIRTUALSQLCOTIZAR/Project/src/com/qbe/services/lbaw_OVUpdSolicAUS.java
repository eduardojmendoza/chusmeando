package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * 15/09/2009 - LR
 * PPCR 50055/6010662 - Agregado de campos para suscribirse a NBWS y EPOLIZA
 * Campos usados(SN_NBWS, EMAIL_NBWS, SN_EPOLIZA, EMAIL_EPOLIZA, SN_PRESTAMAIL)
 *    13/10/2009 Por nueva definicion se agega PASSEPOL (Password de EPoliza)
 * 01/04/2008 - MC
 * PPCR 50016/6010344 - Agregado del campo EMPL para identificar si el vendedor es empleado del grupo.
 * 01/06/2007 - FO
 * PPCR 20060393-1 - Cambio de mensaje de descripcion de vehiculo. cambia de 35 a 80 posiciones.
 * 15/11/2006 - DA
 * AML (Lavado de Dinero) Datos exigidos x la UIF: Se agregan los siguientes campos
 * 
 * SWAPODER
 * OCUPACODCLIE
 * OCUPACODAPOD
 * CLIENSECAPOD
 * DOMISECAPOD
 * Objetos del FrameWork
 */

public class lbaw_OVUpdSolicAUS implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVSQLCotizar.lbaw_OVUpdSolicAUS";
  static final String mcteStoreProc = "SPSNCV_PROD_SOLI_UPDATE_V1";
  /**
   *  Parametros XML de Entrada
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_SUPLENUM = "//SUPLENUM";
  static final String mcteParam_FRANQCOD = "//FRANQCOD";
  static final String mcteParam_PQTDES = "//PQTDES";
  static final String mcteParam_PLANCOD = "//PLANCOD";
  static final String mcteParam_HIJOS1416 = "//HIJOS1416";
  static final String mcteParam_HIJOS1729 = "//HIJOS1729";
  static final String mcteParam_ZONA = "//ZONA";
  static final String mcteParam_CODPROV = "//CODPROV";
  static final String mcteParam_SUMALBA = "//SUMALBA";
  static final String mcteParam_CLIENIVA = "//CLIENIVA";
  static final String mcteParam_SUMASEG = "//SUMASEG";
  static final String mcteParam_CLUB_LBA = "//CLUB_LBA";
  /**
   * 23/01/2006
   */
  static final String mcteParam_DEST80 = "//DESTRUCCION_80";
  static final String mcteParam_CPAANO = "//CPAANO";
  static final String mcteParam_CTAKMS = "//CTAKMS";
  static final String mcteParam_ESCERO = "//ESCERO";
  static final String mcteParam_TIENEPLAN = "//TIENEPLAN";
  static final String mcteParam_COND_ADIC = "//COND_ADIC";
  static final String mcteParam_ASEG_ADIC = "//ASEG_ADIC";
  static final String mcteParam_FH_NAC = "//FH_NAC";
  static final String mcteParam_SEXO = "//SEXO";
  static final String mcteParam_ESTCIV = "//ESTCIV";
  static final String mcteParam_SIFMVEHI_DES = "//SIFMVEHI_DES";
  static final String mcteParam_PROFECOD = "//PROFECOD";
  static final String mcteParam_SIACCESORIOS = "//SIACCESORIOS";
  static final String mcteParam_REFERIDO = "//REFERIDO";
  static final String mcteParam_MotorNum = "//MOTORNUM";
  static final String mcteParam_CHASINUM = "//CHASINUM";
  static final String mcteParam_PatenNum = "//PATENNUM";
  static final String mcteParam_AUMARCOD = "//AUMARCOD";
  static final String mcteParam_AUMODCOD = "//AUMODCOD";
  static final String mcteParam_AUSUBCOD = "//AUSUBCOD";
  static final String mcteParam_AUADICOD = "//AUADICOD";
  static final String mcteParam_AUMODORI = "//AUMODORI";
  static final String mcteParam_AUUSOCOD = "//AUUSOCOD";
  static final String mcteParam_AUVTVCOD = "//AUVTVCOD";
  static final String mcteParam_AUVTVDIA = "//AUVTVDIA";
  static final String mcteParam_AUVTVMES = "//AUVTVMES";
  static final String mcteParam_AUVTVANN = "//AUVTVANN";
  static final String mcteParam_VEHCLRCOD = "//VEHCLRCOD";
  static final String mcteParam_AUKLMNUM = "//AUKLMNUM";
  static final String mcteParam_FABRICAN = "//FABRICAN";
  static final String mcteParam_FABRICMES = "//FABRICMES";
  static final String mcteParam_GUGARAGE = "//GUGARAGE";
  static final String mcteParam_GUDOMICI = "//GUDOMICI";
  static final String mcteParam_AUCATCOD = "//AUCATCOD";
  static final String mcteParam_AUTIPCOD = "//AUTIPCOD";
  static final String mcteParam_AUCIASAN = "//AUCIASAN";
  static final String mcteParam_AUANTANN = "//AUANTANN";
  static final String mcteParam_AUNUMSIN = "//AUNUMSIN";
  static final String mcteParam_AUNUMKMT = "//AUNUMKMT";
  static final String mcteParam_AUUSOGNC = "//AUUSOGNC";
  static final String mcteParam_USUARCOD = "//USUARCOD";
  static final String mcteParam_SITUCPOL = "//SITUCPOL";
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_NUMEDOCU = "//NUMEDOCU";
  static final String mcteParam_TIPODOCU = "//TIPODOCU";
  static final String mcteParam_DOMICSEC = "//DOMICSEC";
  static final String mcteParam_CUENTSEC = "//CUENTSEC";
  static final String mcteParam_COBROFOR = "//COBROFOR";
  static final String mcteParam_EDADACTU = "//EDADACTU";
  /**
   *  Coberturas los devuelve el mensaje de Cotizacion y son 20 iteraciones
   */
  static final String mcteParam_COT_NRO = "//COT_NRO";
  static final String mcteParam_COBERCOD = "//COBERCOD";
  static final String mcteParam_COBERORD = "//COBERORD";
  static final String mcteParam_CAPITASG = "//CAPITASG";
  static final String mcteParam_CAPITIMP = "//CAPITIMP";
  /**
   *  Asegurados Adicionales ...son 10 iteraciones
   */
  static final String mcteParam_ASEGURADOS = "//ASEGURADOS/ASEGURADO";
  static final String mcteParam_DOCUMDAT = "DOCUMDAT";
  static final String mcteParam_NOMBREAS = "NOMBREAS";
  /**
   *  Datos Comunes
   */
  static final String mcteParam_CAMP_CODIGO = "//CAMP_CODIGO";
  static final String mcteParam_CAMP_DESC = "//CAMP_DESC";
  static final String mcteParam_LEGAJO_GTE = "//LEGAJO_GTE";
  static final String mcteParam_NRO_PROD = "//NRO_PROD";
  static final String mcteParam_EMPRESA_CODIGO = "//EMPRESA_CODIGO";
  static final String mcteParam_SUCURSAL_CODIGO = "//SUCURSAL_CODIGO";
  static final String mcteParam_LEGAJO_VEND = "//LEGAJO_VEND";
  static final String mcteParam_EMPL = "//EMPL";
  static final String mcteParam_TIPO_PROD = "//AGENTCLA";
  /**
   *  Hijos ...son 10 iteraciones
   */
  static final String mcteParam_CONDUCTORES = "//HIJOS/HIJO";
  static final String mcteParam_CONDUAPE = "APELLIDOHIJO";
  static final String mcteParam_CONDUNOM = "NOMBREHIJO";
  static final String mcteParam_CONDUFEC = "NACIMHIJO";
  static final String mcteParam_CONDUSEX = "SEXOHIJO";
  static final String mcteParam_CONDUEST = "ESTADOHIJO";
  static final String mcteParam_CONDUEXC = "INCLUIDO";
  /**
   *  Accesorios ...son 10 iteraciones
   */
  static final String mcteParam_ACCESORIOS = "//ACCESORIOS/ACCESORIO";
  static final String mcteParam_AUACCCOD = "CODIGOACC";
  static final String mcteParam_AUVEASUM = "PRECIOACC";
  static final String mcteParam_AUVEADES = "DESCRIPCIONACC";
  /**
   *  Inspeccion
   * "//SITUCEST"
   */
  static final String mcteParam_ESTAINSP = "//ESTAINSP";
  static final String mcteParam_INSPECOD = "//INSPECOD";
  static final String mcteParam_INSPEDES = "//INSPECTOR";
  static final String mcteParam_INSPADOM = "//INSPEADOM";
  static final String mcteParam_INSPEOBS = "//OBSERTXT";
  /**
   * "//INSPENUM"
   */
  static final String mcteParam_INSPEKMS = "//INSPEKMS";
  static final String mcteParam_CENSICOD = "//CENTRCOD_INS";
  static final String mcteParam_CENSIDES = "//CENTRDES";
  /**
   * Const mcteParam_INSPECC               As String = "//INSPECC"
   */
  static final String mcteParam_DISPOCOD = "//RASTREO";
  static final String mcteParam_PRESTCOD = "//ALARMCOD";
  static final String mcteParam_INSPEANN = "//INSPEANN";
  static final String mcteParam_INSPEMES = "//INSPEMES";
  static final String mcteParam_INSPEDIA = "//INSPEDIA";
  /**
   *  Acreedor Prendario
   */
  static final String mcteParam_NUMEDOCU_ACRE = "//NUMEDOCU_ACRE";
  static final String mcteParam_TIPODOCU_ACRE = "//TIPODOCU_ACRE";
  static final String mcteParam_APELLIDO = "//APELLIDO";
  static final String mcteParam_NOMBRES = "//NOMBRES";
  static final String mcteParam_DOMICDOM = "//DOMICDOM";
  static final String mcteParam_DOMICDNU = "//DOMICDNU";
  static final String mcteParam_DOMICESC = "//DOMICESC";
  static final String mcteParam_DOMICPIS = "//DOMICPIS";
  static final String mcteParam_DOMICPTA = "//DOMICPTA";
  static final String mcteParam_DOMICPOB = "//DOMICPOB";
  static final String mcteParam_DOMICCPO = "//DOMICCPO";
  static final String mcteParam_PROVICOD = "//PROVICOD";
  static final String mcteParam_PAISSCOD = "//PAISSCOD";
  static final String mcteParam_BANCOCOD = "//BANCOCOD";
  static final String mcteParam_COBROCOD = "//COBROCOD";
  static final String mcteParam_EFECTANN = "//EFECTANN2";
  static final String mcteParam_EFECTMES = "//EFECTMES";
  static final String mcteParam_EFECTDIA = "//EFECTDIA";
  /**
   * 
   */
  static final String mcteParam_ENTDOMSC = "//ENTDOMSC";
  static final String mcteParam_ANOTACIO = "//ANOTACIO";
  static final String mcteParam_PRECIO_MENSUAL = "//PRECIO_MENSUAL";
  static final String mcteParam_TIPO_ACTUA = "//TIPO_ACTU";
  /**
   * 
   */
  static final String mcteParam_CUITNUME = "//CUITNUME";
  static final String mcteParam_RAZONSOC = "//RAZONSOC";
  static final String mcteParam_CLIEIBTP = "//CLIEIBTP";
  static final String mcteParam_NROIIBB = "//NROIIBB";
  static final String mcteParam_LUNETA = "//LUNETA";
  static final String mcteParam_HIJOS1729_EXC = "//HIJOS1729ND";
  /**
   *  Nuevos Campos Cicle Time
   */
  static final String mcteParam_NROFACTU = "//NROFACTU";
  static final String mcteParam_INS_DOMICDOM = "//INS_DOMICDOM";
  static final String mcteParam_INS_CPACODPO = "//INS_CPACODPO";
  static final String mcteParam_INS_DOMICPOB = "//INS_DOMICPOB";
  static final String mcteParam_INS_PROVICOD = "//INS_PROVICOD";
  static final String mcteParam_INS_DOMICTLF = "//INS_DOMICTLF";
  static final String mcteParam_INS_RANGOHOR = "//INS_RANGOHOR";
  static final String mcteParam_DDJJ = "//DDJJ_STRING";
  static final String mcteParam_DERIVACI = "//DERIVACI";
  static final String mcteParam_ALEATORIO = "//ALEATORIO";
  static final String mcteParam_ESTAIDES = "//ESTAIDES";
  static final String mcteParam_DISPODES = "//DISPODES";
  static final String mcteParam_INSTALADP = "//INSTALADP";
  static final String mcteParam_POSEEDISP = "//POSEEDISP";
  static final String mcteParam_AUTIPGAMA = "//AUTIPGAMA";
  /**
   * Datos exigidos por la UIF
   */
  static final String mcteParam_SWAPODER = "//SWAPODER";
  static final String mcteParam_OCUPACODCLIE = "//OCUPACODCLIE";
  static final String mcteParam_OCUPACODAPOD = "//OCUPACODAPOD";
  static final String mcteParam_CLIENSECAPOD = "//CLIENSECAPOD";
  static final String mcteParam_DOMISECAPOD = "//DOMISECAPOD";
  /**
   * Datos de Green Products
   */
  static final String mcteParam_CLUBECO = "//CLUBECO";
  static final String mcteParam_GRANIZO = "//GRANIZO";
  static final String mcteParam_ROBOCONT = "//ROBOCONT";
  /**
   * 
   * ***********************************
   *  Definiciones NEW SAS
   * ***********************************
   */
  static final String mcteParam_ASEGNOM = "//CLIENTE_NOMYAPE";
  static final String mcteParam_EMPL_NOMYAPE = "//EMPL_NOMYAPE";
  static final String mcteParam_SICTUPOL = "SICTUPOL";
  static final String mcteParam_RAMO = "AUS1";
  static final String mcteParam_RAMOCOD = "44";
  static final String gcteDB_NEWSAS = "DMS_ControlDuplicidad.udl";
  static final String mcteStoreProcGrabaCabecera_NEWSAS = "P_DMS_AltaCabeCtrolAutos";
  static final String mcteStoreProcNumeracion_NEWSAS = "P_DMS_Numeracion";
  static final String mctePrefixVendor_NEWSAS = "(OV) ";
  /**
   * 
   * ***********************************
   *  Definiciones SUSCRIPCION A NBWS
   * ***********************************
   */
  static final String mcteParam_SN_NBWS = "//SN_NBWS";
  static final String mcteParam_EMAIL_NBWS = "//EMAIL_NBWS";
  static final String mcteParam_SN_EPOLIZA = "//SN_EPOLIZA";
  static final String mcteParam_EMAIL_EPOLIZA = "//EMAIL_EPOLIZA";
  static final String mcteParam_SN_PRESTAMAIL = "//SN_PRESTAMAIL";
  static final String mcteParam_PASSEPOL = "//PASSEPOL";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private com.qbe.services.HSBCInterfaces.IDBConnection mobjHSBC_DBCnn = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   */
  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLCoberturas = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    HSBC.DBConnectionTrx wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
    Object wobjClass = null;
    Connection wobjDBCnn = null;
    Connection wobjDBCnn_NEWSAS = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String wvarRequest = "";
    String wvarResponse = "";
    String mvarRAMOPCOD = "";
    String mvarPOLIZANN = "";
    String mvarPOLIZSEC = "";
    String mvarCERTIPOL = "";
    String mvarCERTIANN = "";
    String mvarCERTISEC = "";
    String mvarSUPLENUM = "";
    String mvarFRANQCOD = "";
    String mvarPQTDES = "";
    String mvarPLANCOD = "";
    String mvarHIJOS1416 = "";
    String mvarHIJOS1729 = "";
    String mvarZONA = "";
    String mvarCODPROV = "";
    String mvarSUMALBA = "";
    String mvarCLIENIVA = "";
    String mvarSUMASEG = "";
    String mvarCLUB_LBA = "";
    String mvarDEST80 = "";
    String mvarCPAANO = "";
    String mvarCTAKMS = "";
    String mvarESCERO = "";
    String mvarTIENEPLAN = "";
    String mvarCOND_ADIC = "";
    String mvarASEG_ADIC = "";
    String mvarFH_NAC = "";
    String mvarSEXO = "";
    String mvarESTCIV = "";
    String mvarSIFMVEHI_DES = "";
    String mvarPROFECOD = "";
    String mvarSIACCESORIOS = "";
    String mvarREFERIDO = "";
    String mvarMotorNum = "";
    String mvarCHASINUM = "";
    String mvarPatenNum = "";
    String mvarAUMARCOD = "";
    String mvarAUMODCOD = "";
    String mvarAUSUBCOD = "";
    String mvarAUADICOD = "";
    String mvarAUMODORI = "";
    String mvarAUUSOCOD = "";
    String mvarAUVTVCOD = "";
    String mvarAUVTVDIA = "";
    String mvarAUVTVMES = "";
    String mvarAUVTVANN = "";
    String mvarVEHCLRCOD = "";
    String mvarAUKLMNUM = "";
    String mvarFABRICAN = "";
    String mvarFABRICMES = "";
    String mvarGUGARAGE = "";
    String mvarGUDOMICI = "";
    String mvarAUCATCOD = "";
    String mvarAUTIPCOD = "";
    String mvarAUCIASAN = "";
    String mvarAUANTANN = "";
    String mvarAUNUMSIN = "";
    String mvarAUNUMKMT = "";
    String mvarAUUSOGNC = "";
    String mvarUSUARCOD = "";
    String mvarSITUCPOL = "";
    String mvarCLIENSEC = "";
    String mvarNUMEDOCU = "";
    String mvarTIPODOCU = "";
    String mvarDOMICSEC = "";
    String mvarCUENTSEC = "";
    String mvarCOBROFOR = "";
    String mvarEDADACTU = "";
    String wvarCOT_NRO = "";
    String mvarCOBERCOD = "";
    String mvarCOBERORD = "";
    String mvarCAPITASG = "";
    String mvarCAPITIMP = "";
    String mvarDOCUMDAT = "";
    String mvarNOMBREAS = "";
    String mvarCAMP_CODIGO = "";
    String wvarTIPOPROD = "";
    String mvarCAMP_DESC = "";
    String mvarLEGAJO_GTE = "";
    String mvarNRO_PROD = "";
    String mvarEMPRESA_CODIGO = "";
    String mvarSUCURSAL_CODIGO = "";
    String mvarLEGAJO_VEND = "";
    String mvarEMPL = "";
    String mvarCONDUAPE = "";
    String mvarCONDUNOM = "";
    String mvarCONDUFEC = "";
    String mvarCONDUSEX = "";
    String mvarCONDUEST = "";
    String mvarCONDUEXC = "";
    String mvarAUACCCOD = "";
    String mvarAUVEASUM = "";
    String mvarAUVEADES = "";
    String mvarAUVEADEP = "";
    String mvarESTAINSP = "";
    String mvarINSPECOD = "";
    String mvarINSPEDES = "";
    String mvarINSPADOM = "";
    String mvarINSPEOBS = "";
    String mvarINSPEKMS = "";
    String mvarCENSICOD = "";
    String mvarCENSIDES = "";
    String mvarDISPOCOD = "";
    String mvarPRESTCOD = "";
    String mvarINSPEANN = "";
    String mvarINSPEMES = "";
    String mvarINSPEDIA = "";
    String mvarNUMEDOCU_ACRE = "";
    String mvarTIPODOCU_ACRE = "";
    String mvarAPELLIDO = "";
    String mvarNOMBRES = "";
    String mvarDOMICDOM = "";
    String mvarDOMICDNU = "";
    String mvarDOMICESC = "";
    String mvarDOMICPIS = "";
    String mvarDOMICPTA = "";
    String mvarDOMICPOB = "";
    String mvarDOMICCPO = "";
    String mvarPROVICOD = "";
    String mvarPAISSCOD = "";
    String mvarBANCOCOD = "";
    String mvarCOBROCOD = "";
    String mvarEfectAnn = "";
    String mvarEFECTMES = "";
    String mvarEFECTDIA = "";
    String mvarENTDOMSC = "";
    String mvarANOTACIO = "";
    String mvarPRECIO_MENSUAL = "";
    String mvarTIPO_ACTUA = "";
    String mvarCUITNUME = "";
    String mvarRAZONSOC = "";
    String mvarCLIEIBTP = "";
    String mvarNROIIBB = "";
    String mvarLUNETA = "";
    String mvarHIJOS1729_EXC = "";
    String mvarNROFACTU = "";
    String mvarINS_DOMICDOM = "";
    String mvarINS_CPACODPO = "";
    String mvarINS_DOMICPOB = "";
    String mvarINS_PROVICOD = "";
    String mvarINS_DOMICTLF = "";
    String mvarINS_RANGOHOR = "";
    String mvarDDJJ = "";
    String mvarDERIVACI = "";
    String mvarALEATORIO = "";
    String mvarESTAIDES = "";
    String mvarDISPODES = "";
    String mvarINSTALADP = "";
    String mvarPOSEEDISP = "";
    String mvarAUTIPGAMA = "";
    String wvarSWAPODER = "";
    String wvarOCUPACODCLIE = "";
    String wvarOCUPACODAPOD = "";
    String wvarCLIENSECAPOD = "";
    String wvarDOMISECAPOD = "";
    String wvarCLUBECO = "";
    String wvarGRANIZO = "";
    String wvarROBOCONT = "";
    String mvarXMLCoberturas = "";
    Recordset wrstDBResult = null;
    String mvarSucu = "";
    String mvarRAMO = "";
    String mvarNRO_CERTISEC = "";
    String mvarNRO_SOLICITUD = "";
    String mvarDOCU_TIPO = "";
    String mvarDOCU_NUMERO = "";
    String mvarNOM_ASEG = "";
    String mvarLEG_VEND = "";
    String mvarNOM_VEND = "";
    String mvarPATENTE = "";
    String mvarCHASIS = "";
    String mvarSN_NBWS = "";
    String mvarEMAIL_NBWS = "";
    String mvarSN_EPOLIZA = "";
    String mvarEMAIL_EPOLIZA = "";
    String mvarSN_PRESTAMAIL = "";
    String mvarPASSEPOL = "";
    //
    //
    //
    // DATOS GENERALES
    //23/01/2006
    // Coberturas los devuelve el mensaje de Cotizacion y son 20 iteraciones
    // Asegurados Adicionales ...son 10 iteraciones
    // Datos Comunes
    // Hijos ...son 10 iteraciones
    // Accesorios ...son 10 iteraciones
    //Deprecia SI/NO, se le genera siempre en si.
    // Inspeccion
    //Dim mvarINSPECC                 As String
    // Acreedor Prendario
    //
    //
    // Nuevos Campos Cicle Time
    //
    //Datos exigidos por la UIF
    //Datos de Green Products
    //
    //
    // ******************************
    // COMIENZO VARIABLES NEWSAS
    // ******************************
    //
    //
    // ******************************
    // FIN VARIABLES NEWSAS
    // ******************************
    // *************************************************
    // COMIENZO VARIABLES SUSCRIPCIONES NBWS Y EPOLIZA
    // *************************************************
    // ********************************************
    // FIN VARIABLES SUSCRIPCIONES NBWS Y EPOLIZA
    // ********************************************
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // Cargo el xml de entrada
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      // Vuelvo a cotizar para el plan seleccionado
      wvarStep = 20;
      wobjClass = new lbawA_OVMQCotizar.lbaw_OVGetCotiAUS();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
      wobjClass = null;
      //
      // Guardo las coberturas y las sumas aseguradas
      wvarStep = 30;
      wobjXMLCoberturas = new diamondedge.util.XmlDom();
      //unsup wobjXMLCoberturas.async = false;
      wobjXMLCoberturas.loadXML( wvarResponse );
      //
      wvarStep = 40;
      // Controlo como devolvió la cotización
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='No se pudo realizar la cotización correspondiente' /></Response>" );
        //unsup GoTo ErrorHandler
      }
      //
      wvarStep = 42;
      mvarXMLCoberturas = invoke( "GetCoberturas", new Variant[] { new Variant(wobjXMLCoberturas), new Variant(! (null /*unsup wobjXMLRequest.selectSingleNode( new Variant(mcteParam_CONDUCTORES) ) */ == (org.w3c.dom.Node) null)) } );
      //
      wvarStep = 45;
      wvarCOT_NRO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_COT_NRO ) */ );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUMASEG ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_SUMASEG ) */ ) );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUMALBA ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_SUMALBA ) */ ) );
      //
      wvarStep = 50;
      //
      wvarStep = 60;
      // DATOS GENERALES
      mvarRAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD ) */ );
      mvarPOLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN ) */ );
      mvarPOLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC ) */ );
      mvarCERTIPOL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL ) */ );
      mvarCERTIANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN ) */ );
      mvarCERTISEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC ) */ );
      wvarStep = 70;
      mvarSUPLENUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUPLENUM ) */ );
      mvarFRANQCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FRANQCOD ) */ );
      mvarPQTDES = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PQTDES ) */ ) ), 120 );
      mvarPLANCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PLANCOD ) */ );
      mvarHIJOS1416 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1416 ) */ );
      wvarStep = 80;
      mvarHIJOS1729 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1729 ) */ );
      mvarZONA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ZONA ) */ );
      mvarCODPROV = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CODPROV ) */ );
      mvarSUMALBA = Strings.replace( String.valueOf( Obj.toDouble( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUMALBA ) */ ) ) / 100 ), ",", "." );
      mvarCLIENIVA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENIVA ) */ );
      wvarStep = 90;
      mvarSUMASEG = Strings.replace( String.valueOf( Obj.toDouble( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUMASEG ) */ ) ) / 100 ), ",", "." );
      mvarCLUB_LBA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLUB_LBA ) */ );

      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_DEST80 ) */.getLength() > 0 )
      {
        //23/01/2006
        mvarDEST80 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DEST80 ) */ );
      }
      if( mvarDEST80.equals( "" ) )
      {
        mvarDEST80 = "N";
      }

      mvarCPAANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CPAANO ) */ );
      mvarCTAKMS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CTAKMS ) */ );
      mvarESCERO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ESCERO ) */ );
      wvarStep = 100;
      mvarTIENEPLAN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIENEPLAN ) */ );
      mvarCOND_ADIC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COND_ADIC ) */ );
      mvarASEG_ADIC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ASEG_ADIC ) */ );
      mvarFH_NAC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FH_NAC ) */ );
      mvarSEXO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SEXO ) */ );
      wvarStep = 110;
      mvarESTCIV = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ESTCIV ) */ );
      //mvarSIFMVEHI_DES = Left(Trim(.selectSingleNode(mcteParam_SIFMVEHI_DES).Text), 35) ' MQ 0093
      // MQ 0051
      mvarSIFMVEHI_DES = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SIFMVEHI_DES ) */ ) ), 80 );
      mvarPROFECOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROFECOD ) */ );
      mvarSIACCESORIOS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SIACCESORIOS ) */ );
      mvarREFERIDO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_REFERIDO ) */ );
      wvarStep = 120;
      mvarMotorNum = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MotorNum ) */ );
      mvarCHASINUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CHASINUM ) */ );
      mvarPatenNum = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PatenNum ) */ );
      mvarAUMARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUMARCOD ) */ );
      mvarAUMODCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUMODCOD ) */ );
      wvarStep = 130;
      mvarAUSUBCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUSUBCOD ) */ );
      mvarAUADICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUADICOD ) */ );
      mvarAUMODORI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUMODORI ) */ );
      mvarAUUSOCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUUSOCOD ) */ );
      mvarAUVTVCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUVTVCOD ) */ );
      wvarStep = 140;
      mvarAUVTVDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUVTVDIA ) */ );
      mvarAUVTVMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUVTVMES ) */ );
      mvarAUVTVANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUVTVANN ) */ );
      mvarVEHCLRCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VEHCLRCOD ) */ );
      mvarAUKLMNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUKLMNUM ) */ );
      wvarStep = 150;
      mvarFABRICAN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FABRICAN ) */ );
      mvarFABRICMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FABRICMES ) */ );
      mvarGUGARAGE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GUGARAGE ) */ );
      mvarGUDOMICI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GUDOMICI ) */ );
      mvarAUCATCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUCATCOD ) */ );
      wvarStep = 160;
      mvarAUTIPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUTIPCOD ) */ );
      mvarAUCIASAN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUCIASAN ) */ );
      mvarAUANTANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUANTANN ) */ );
      mvarAUNUMSIN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUNUMSIN ) */ );
      mvarAUNUMKMT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUNUMKMT ) */ );
      wvarStep = 170;
      mvarAUUSOGNC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUUSOGNC ) */ );
      mvarUSUARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD ) */ );
      mvarSITUCPOL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SITUCPOL ) */ );
      mvarCLIENSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC ) */ );
      mvarNUMEDOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU ) */ );
      wvarStep = 180;
      mvarTIPODOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU ) */ );
      mvarDOMICSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICSEC ) */ );
      mvarCUENTSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CUENTSEC ) */ );
      mvarCOBROFOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COBROFOR ) */ );
      mvarEDADACTU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EDADACTU ) */ );
      wvarStep = 190;
      mvarCAMP_CODIGO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CAMP_CODIGO ) */ );
      mvarCAMP_DESC = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CAMP_DESC ) */ ) ), 30 );
      mvarLEGAJO_GTE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LEGAJO_GTE ) */ );
      mvarNRO_PROD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NRO_PROD ) */ );
      mvarEMPRESA_CODIGO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMPRESA_CODIGO ) */ );
      mvarSUCURSAL_CODIGO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUCURSAL_CODIGO ) */ );
      mvarLEGAJO_VEND = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LEGAJO_VEND ) */ );
      mvarEMPL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMPL ) */ );
      wvarStep = 200;
      mvarESTAINSP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ESTAINSP ) */ );
      mvarINSPECOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPECOD ) */ );
      mvarINSPEDES = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEDES ) */ ) ), 30 );
      mvarINSPADOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPADOM ) */ );
      wvarStep = 210;
      mvarINSPEOBS = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEOBS ) */ ) ), 50 );
      mvarINSPEKMS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEKMS ) */ );
      //mvarCENSICOD = .selectSingleNode(mcteParam_CENSICOD).Text
      mvarCENSICOD = Strings.right( Strings.fill( 4, " " ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CENSICOD ) */ ), 4 );
      mvarCENSIDES = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CENSIDES ) */ ) ), 30 );
      wvarStep = 220;
      //        mvarINSPECC = .selectSingleNode(mcteParam_INSPECC).Text
      mvarDISPOCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DISPOCOD ) */ );
      mvarPRESTCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PRESTCOD ) */ );
      mvarNUMEDOCU_ACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU_ACRE ) */ );
      wvarStep = 230;
      mvarTIPODOCU_ACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU_ACRE ) */ );
      mvarAPELLIDO = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_APELLIDO ) */ ) ), 40 );
      mvarNOMBRES = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NOMBRES ) */ ) ), 40 );
      mvarDOMICDOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOM ) */ );
      mvarDOMICDNU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDNU ) */ );
      mvarDOMICESC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICESC ) */ );
      wvarStep = 240;
      mvarDOMICPIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPIS ) */ );
      mvarDOMICPTA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPTA ) */ );
      mvarDOMICPOB = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOB ) */ ) ), 20 );
      mvarDOMICCPO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICCPO ) */ );
      mvarPROVICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROVICOD ) */ );
      mvarPAISSCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAISSCOD ) */ );
      wvarStep = 250;
      mvarBANCOCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_BANCOCOD ) */ );
      mvarCOBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COBROCOD ) */ );
      mvarEfectAnn = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTANN ) */ );
      mvarEFECTMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTMES ) */ );
      mvarEFECTDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTDIA ) */ );
      mvarINSPEANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEANN ) */ );
      wvarStep = 260;
      mvarINSPEMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEMES ) */ );
      mvarINSPEDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEDIA ) */ );
      mvarENTDOMSC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ENTDOMSC ) */ );
      mvarANOTACIO = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ANOTACIO ) */ ) ), 100 );
      mvarPRECIO_MENSUAL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PRECIO_MENSUAL ) */ );
      mvarTIPO_ACTUA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPO_ACTUA ) */ );
      //
      wvarStep = 270;
      mvarCUITNUME = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_CUITNUME ) */.getLength() > 0 )
      {
        mvarCUITNUME = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CUITNUME ) */ );
      }
      //
      mvarRAZONSOC = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_RAZONSOC ) */.getLength() > 0 )
      {
        mvarRAZONSOC = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAZONSOC ) */ ) ), 60 );
      }
      //
      wvarStep = 280;
      mvarCLIEIBTP = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_CLIEIBTP ) */.getLength() > 0 )
      {
        mvarCLIEIBTP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIEIBTP ) */ );
      }
      //
      mvarNROIIBB = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_NROIIBB ) */.getLength() > 0 )
      {
        mvarNROIIBB = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NROIIBB ) */ );
      }
      //
      wvarStep = 290;
      mvarLUNETA = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_LUNETA ) */.getLength() > 0 )
      {
        mvarLUNETA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUNETA ) */ );
      }
      //
      wvarStep = 300;
      mvarHIJOS1729_EXC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1729_EXC ) */ );
      mvarNROFACTU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NROFACTU ) */ );
      mvarINS_DOMICDOM = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_INS_DOMICDOM ) */.getLength() > 0 )
      {
        mvarINS_DOMICDOM = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INS_DOMICDOM ) */ ) ), 20 );
      }
      mvarINS_CPACODPO = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_INS_CPACODPO ) */.getLength() > 0 )
      {
        mvarINS_CPACODPO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INS_CPACODPO ) */ );
      }
      wvarStep = 310;
      mvarINS_DOMICPOB = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_INS_DOMICPOB ) */.getLength() > 0 )
      {
        mvarINS_DOMICPOB = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INS_DOMICPOB ) */ );
      }
      mvarINS_PROVICOD = "0";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_INS_PROVICOD ) */.getLength() > 0 )
      {
        mvarINS_PROVICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INS_PROVICOD ) */ );
      }
      wvarStep = 320;
      mvarINS_DOMICTLF = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_INS_DOMICTLF ) */.getLength() > 0 )
      {
        mvarINS_DOMICTLF = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INS_DOMICTLF ) */ ) ), 30 );
      }
      mvarINS_RANGOHOR = "0";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_INS_RANGOHOR ) */.getLength() > 0 )
      {
        mvarINS_RANGOHOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INS_RANGOHOR ) */ );
      }
      wvarStep = 325;
      mvarDDJJ = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DDJJ ) */ ) ), 140 );
      mvarDERIVACI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DERIVACI ) */ );
      mvarALEATORIO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ALEATORIO ) */ );
      mvarESTAIDES = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ESTAIDES ) */ ), 30 );
      mvarDISPODES = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DISPODES ) */ ), 30 );
      mvarINSTALADP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSTALADP ) */ );
      mvarPOSEEDISP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POSEEDISP ) */ );
      mvarAUTIPGAMA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUTIPGAMA ) */ );

      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPO_PROD ) */ == (org.w3c.dom.Node) null) )
      {
        wvarTIPOPROD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPO_PROD ) */ );
      }

      wvarStep = 330;

      //Datos exigidos por la UIF
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SWAPODER ) */ == (org.w3c.dom.Node) null) )
      {
        wvarSWAPODER = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SWAPODER ) */ );
      }
      else
      {
        wvarSWAPODER = "";
      }
      //
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_OCUPACODCLIE ) */ == (org.w3c.dom.Node) null) )
      {
        wvarOCUPACODCLIE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_OCUPACODCLIE ) */ );
      }
      else
      {
        wvarOCUPACODCLIE = "";
      }
      //
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_OCUPACODAPOD ) */ == (org.w3c.dom.Node) null) )
      {
        wvarOCUPACODAPOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_OCUPACODAPOD ) */ );
      }
      else
      {
        wvarOCUPACODAPOD = "";
      }
      //
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENSECAPOD ) */ == (org.w3c.dom.Node) null) )
      {
        wvarCLIENSECAPOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENSECAPOD ) */ );
      }
      else
      {
        wvarCLIENSECAPOD = "0";
      }
      //
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMISECAPOD ) */ == (org.w3c.dom.Node) null) )
      {
        wvarDOMISECAPOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMISECAPOD ) */ );
      }
      else
      {
        wvarDOMISECAPOD = "";
      }
      //
      //Green Products
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLUBECO ) */ == (org.w3c.dom.Node) null) )
      {
        wvarCLUBECO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLUBECO ) */ );
      }
      else
      {
        wvarCLUBECO = "";
      }
      //
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GRANIZO ) */ == (org.w3c.dom.Node) null) )
      {
        wvarGRANIZO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GRANIZO ) */ );
      }
      else
      {
        wvarGRANIZO = "";
      }
      //
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROBOCONT ) */ == (org.w3c.dom.Node) null) )
      {
        wvarROBOCONT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROBOCONT ) */ );
      }
      else
      {
        wvarROBOCONT = "";
      }

      // *********************************
      // ASIGNA NODOS DE NEWSAS
      // *********************************
      wvarStep = 327;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUCURSAL_CODIGO ) */ == (org.w3c.dom.Node) null) )
      {
        mvarSucu = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUCURSAL_CODIGO ) */ );
      }
      else
      {
        mvarSucu = "";
      }
      wvarStep = 330;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU ) */ == (org.w3c.dom.Node) null) )
      {
        mvarDOCU_TIPO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU ) */ );
      }
      else
      {
        mvarDOCU_TIPO = "";
      }
      wvarStep = 340;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU ) */ == (org.w3c.dom.Node) null) )
      {
        mvarDOCU_NUMERO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU ) */ );
      }
      else
      {
        mvarDOCU_NUMERO = "";
      }
      wvarStep = 350;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ASEGNOM ) */ == (org.w3c.dom.Node) null) )
      {
        mvarNOM_ASEG = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ASEGNOM ) */ ) ), 80 );
      }
      else
      {
        mvarNOM_ASEG = "";
      }
      wvarStep = 360;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LEGAJO_VEND ) */ == (org.w3c.dom.Node) null) )
      {
        mvarLEG_VEND = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LEGAJO_VEND ) */ );
      }
      else
      {
        mvarLEG_VEND = "";
      }
      wvarStep = 370;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMPL_NOMYAPE ) */ == (org.w3c.dom.Node) null) )
      {
        mvarNOM_VEND = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMPL_NOMYAPE ) */ ), 50 );
      }
      else
      {
        mvarNOM_VEND = "";
      }

      wvarStep = 380;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PatenNum ) */ == (org.w3c.dom.Node) null) )
      {
        mvarPATENTE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PatenNum ) */ );
      }
      else
      {
        mvarPATENTE = "";
      }
      wvarStep = 390;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CHASINUM ) */ == (org.w3c.dom.Node) null) )
      {
        mvarCHASIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CHASINUM ) */ );
      }
      else
      {
        mvarCHASIS = "";
      }


      // **********************************************
      // FIN NODOS NEWSAS
      // **********************************************
      // **********************************************
      // ASIGNA NODOS DE SUSCRIPCIONES NBWS Y EPOLIZA
      // **********************************************
      wvarStep = 395;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SN_NBWS ) */ == (org.w3c.dom.Node) null) )
      {
        mvarSN_NBWS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SN_NBWS ) */ );
      }
      else
      {
        mvarSN_NBWS = "";
      }
      wvarStep = 396;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMAIL_NBWS ) */ == (org.w3c.dom.Node) null) )
      {
        mvarEMAIL_NBWS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMAIL_NBWS ) */ );
      }
      else
      {
        mvarEMAIL_NBWS = "";
      }
      wvarStep = 397;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SN_EPOLIZA ) */ == (org.w3c.dom.Node) null) )
      {
        mvarSN_EPOLIZA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SN_EPOLIZA ) */ );
      }
      else
      {
        mvarSN_EPOLIZA = "";
      }
      wvarStep = 398;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMAIL_EPOLIZA ) */ == (org.w3c.dom.Node) null) )
      {
        mvarEMAIL_EPOLIZA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMAIL_EPOLIZA ) */ );
      }
      else
      {
        mvarEMAIL_EPOLIZA = "";
      }
      wvarStep = 399;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SN_PRESTAMAIL ) */ == (org.w3c.dom.Node) null) )
      {
        mvarSN_PRESTAMAIL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SN_PRESTAMAIL ) */ );
      }
      else
      {
        mvarSN_PRESTAMAIL = "";
      }
      //13/10/2009 Se agrega password de EPoliza por nueva definición.-
      wvarStep = 400;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PASSEPOL ) */ == (org.w3c.dom.Node) null) )
      {
        mvarPASSEPOL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PASSEPOL ) */ );
      }
      else
      {
        mvarPASSEPOL = "";
      }
      // **********************************************
      // FIN NODOS DE SUSCRIPCIONES NBWS Y EPOLIZA
      // **********************************************
      //
      // **************************************************************************************
      //NEW SAS - Si se envia al AIS, se pide numero a NewSas y se le asigna al CERTISEC de OV
      // **************************************************************************************
      // aca tengo que poner las dos situaciones
      if( (mvarCERTIPOL.equals( "0150" )) && ((Strings.toUpperCase( mvarSITUCPOL ).equals( "X" )) || (Strings.toUpperCase( mvarSITUCPOL ).equals( "A" )) || (Strings.toUpperCase( mvarSITUCPOL ).equals( "O" ))) )
      {

        wvarStep = 410;
        wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();


        // ******************************
        // OBTIENE NUMERACION DE NEWSAS
        // ******************************
        wvarStep = 420;

        //error: function 'GetDBConnection' was not found.
        //unsup: Set wobjDBCnn_NEWSAS = wobjHSBC_DBCnn.GetDBConnection(gcteDB_NEWSAS)
        wobjDBCmd = new Command();

        wvarStep = 430;
        wobjDBCmd.setActiveConnection( wobjDBCnn_NEWSAS );
        wobjDBCmd.setCommandText( mcteStoreProcNumeracion_NEWSAS );
        wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

        wvarStep = 440;
        wobjDBParm = new Parameter( "@RETURN", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 450;
        wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mcteParam_RAMO ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 460;

        //wobjDBCmd.Execute adExecute
        // ***********************
        wrstDBResult = wobjDBCmd.execute();
        wrstDBResult.setActiveConnection( (Connection) null );

        if( ! (wrstDBResult.isEOF()) )
        {
          wvarStep = 470;
          // Este parametro se debe pasar tambien en el stored de solicitud en el caso de ser necesario
          // es decir, en el caso que cumpla las condiciones de banco
          mvarNRO_CERTISEC = wrstDBResult.getFields().getField("UltimoNroTot").getValue().toString();
        }

        wobjDBCmd = (Command) null;

        // *********************************
        // *      FIN DE NUMERACIÓN
        // *********************************
        // *******************************
        // GRABA CABECERA NEWSAS
        // *******************************
        wvarStep = 480;

        wobjDBCmd = new Command();

        wvarStep = 490;
        wobjDBCmd.setActiveConnection( wobjDBCnn_NEWSAS );
        wobjDBCmd.setCommandText( mcteStoreProcGrabaCabecera_NEWSAS );
        wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );


        // *******************************************
        // PARAMETROS LLAMADA STORE NEWSAS
        // *******************************************
        wvarStep = 500;
        wobjDBParm = new Parameter( "@RETURN", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        // SUCURSAL
        wvarStep = 510;
        wobjDBParm = new Parameter( "@SUCURSAL", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarSucu ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        // RAMO
        wvarStep = 520;
        wobjDBParm = new Parameter( "@RAMO", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mcteParam_RAMOCOD ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;


        // NRO_CERTISEC
        wvarStep = 530;
        wobjDBParm = new Parameter( "@NRO_CERTISEC", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarNRO_CERTISEC ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;


        // Numero de solicitud armado apartir del nuevo certisec
        mvarNRO_SOLICITUD = Strings.right( "00" + mvarPOLIZANN, 2 ) + Strings.right( ("000000" + mvarPOLIZSEC), 6 ) + Strings.right( ("0000" + mvarCERTIPOL), 4 ) + Strings.right( ("0000" + mvarCERTIANN), 4 ) + Strings.right( ("000000" + mvarNRO_CERTISEC), 6 ) + Strings.right( ("0000" + mvarSUPLENUM), 4 );

        wvarStep = 540;
        wobjDBParm = new Parameter( "@NRO_SOLICITUD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNRO_SOLICITUD ) );
        wobjDBParm.setPrecision( (byte)( 26 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );

        wobjDBParm = (Parameter) null;


        // DOCU_TIPO
        wvarStep = 560;
        wobjDBParm = new Parameter( "@DOCU_TIPO", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarDOCU_TIPO ) );
        wobjDBCmd.getParameters().append( wobjDBParm );

        wobjDBParm = (Parameter) null;

        // DOCU_NUMERO
        wvarStep = 570;
        wobjDBParm = new Parameter( "@DOCU_NUMERO", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarDOCU_NUMERO ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        // NOMBRE_ASEGURADO
        wvarStep = 580;
        wobjDBParm = new Parameter( "@NOMBRE_ASEGURADO", AdoConst.adVarChar, AdoConst.adParamInput, 80, new Variant( Strings.toUpperCase( mvarNOM_ASEG ) ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        //LEGAJO_VEND
        wvarStep = 590;
        wobjDBParm = new Parameter( "@LEGAJO_VEND", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarLEG_VEND ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        // NOMBRE_VEND
        wvarStep = 600;
        wobjDBParm = new Parameter( "@NOMBRE_VEND", AdoConst.adVarChar, AdoConst.adParamInput, 50, new Variant( Strings.left( Strings.toUpperCase( mctePrefixVendor_NEWSAS + mvarNOM_VEND ), 50 ) ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;


        //PATENTE
        wvarStep = 610;
        wobjDBParm = new Parameter( "@PATENTE", AdoConst.adVarChar, AdoConst.adParamInput, 6, new Variant( Strings.toUpperCase( mvarPATENTE ) ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;



        //CHASIS
        wvarStep = 620;
        wobjDBParm = new Parameter( "@CHASIS", AdoConst.adVarChar, AdoConst.adParamInput, 20, new Variant( Strings.toUpperCase( mvarCHASIS ) ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        // *******************************************
        // FIN PARAMETROS LLAMADA STORE NEWSAS
        // *******************************************
        // *************************
        // EJECUTA EL STORED
        // *************************
        wvarStep = 630;

        wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
        wobjDBCnn = (Connection) null;
        wobjDBCnn_NEWSAS = (Connection) null;

        // **************************************
        // *      FIN DE EJECUCION
        // **************************************
      }

      // **********************************
      // *      FIN NEWSAS
      // **********************************
      wvarStep = 340;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 350;
      wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
      wobjDBCnn = (Connection) (Connection)( wobjHSBC_DBCnn.GetDBConnection( ModGeneral.gcteDB ) );
      wobjDBCmd = new Command();

      wvarStep = 360;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 370;
      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( "0" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      // Parametros
      wvarStep = 380;
      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarRAMOPCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 390;
      wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPOLIZANN ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 400;
      wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPOLIZSEC ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 410;
      wobjDBParm = new Parameter( "@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCERTIPOL ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 420;
      wobjDBParm = new Parameter( "@CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCERTIANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 425;
      // **********************************
      // *      ASIGNO CERTISEC
      // **********************************
      // aca tengo que poner las dos situaciones
      if( (mvarCERTIPOL.equals( "0150" )) && ((Strings.toUpperCase( mvarSITUCPOL ).equals( "X" )) || (Strings.toUpperCase( mvarSITUCPOL ).equals( "A" )) || (Strings.toUpperCase( mvarSITUCPOL ).equals( "O" ))) )
      {

        wvarStep = 640;
        wobjDBParm = new Parameter( "@CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNRO_CERTISEC ) );
        wobjDBParm.setPrecision( (byte)( 6 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }
      else
      {
        wobjDBParm = new Parameter( "@CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCERTISEC ) );
        wobjDBParm.setPrecision( (byte)( 6 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      // **********************************
      // *      FIN ASIGNO CERTISEC
      // **********************************
      wvarStep = 430;
      wobjDBParm = new Parameter( "@SUPLENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarSUPLENUM ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 440;
      wobjDBParm = new Parameter( "@FRANQCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarFRANQCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 450;
      wobjDBParm = new Parameter( "@PQTDES", AdoConst.adChar, AdoConst.adParamInput, 120, new Variant( mvarPQTDES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 460;
      wobjDBParm = new Parameter( "@PLANCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPLANCOD ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 470;
      wobjDBParm = new Parameter( "@HIJOS1416", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarHIJOS1416 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 480;
      wobjDBParm = new Parameter( "@HIJOS1729", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarHIJOS1729 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 490;
      wobjDBParm = new Parameter( "@ZONA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarZONA ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 500;
      wobjDBParm = new Parameter( "@CODPROV", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCODPROV ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 510;
      wobjDBParm = new Parameter( "@SUMALBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarSUMALBA ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 520;
      wobjDBParm = new Parameter( "@CLIENIVA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLIENIVA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 530;
      wobjDBParm = new Parameter( "@SUMAASEG", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarSUMASEG ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 540;
      wobjDBParm = new Parameter( "@CLUB_LBA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLUB_LBA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 550;
      wobjDBParm = new Parameter( "@CPAANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCPAANO ) );
      wobjDBParm.setPrecision( (byte)( 8 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 560;
      wobjDBParm = new Parameter( "@CTAKMS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCTAKMS ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 570;
      wobjDBParm = new Parameter( "@ESCERO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarESCERO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 580;
      wobjDBParm = new Parameter( "@TIENEPLAN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarTIENEPLAN ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 590;
      wobjDBParm = new Parameter( "@COND_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCOND_ADIC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 600;
      wobjDBParm = new Parameter( "@ASEG_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarASEG_ADIC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 610;
      wobjDBParm = new Parameter( "@FH_NAC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarFH_NAC ) );
      wobjDBParm.setPrecision( (byte)( 8 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 620;
      wobjDBParm = new Parameter( "@SEXO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarSEXO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 630;
      wobjDBParm = new Parameter( "@ESTCIV", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarESTCIV ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 640;
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@SIFMVEHI_DES", adChar, adParamInput, 35, mvarSIFMVEHI_DES) ' MQ 0093
      // MQ 0051
      wobjDBParm = new Parameter( "@SIFMVEHI_DES", AdoConst.adChar, AdoConst.adParamInput, 80, new Variant( mvarSIFMVEHI_DES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 650;
      wobjDBParm = new Parameter( "@PROFECOD", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( mvarPROFECOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 660;
      wobjDBParm = new Parameter( "@ACCESORIOS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarSIACCESORIOS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 670;
      wobjDBParm = new Parameter( "@REFERIDO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarREFERIDO ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 680;
      wobjDBParm = new Parameter( "@MOTORNUM", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarMotorNum ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 690;
      wobjDBParm = new Parameter( "@CHASINUM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarCHASINUM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 700;
      wobjDBParm = new Parameter( "@PATENNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarPatenNum ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 710;
      wobjDBParm = new Parameter( "@AUMARCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUMARCOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 720;
      wobjDBParm = new Parameter( "@AUMODCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUMODCOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 730;
      wobjDBParm = new Parameter( "@AUSUBCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUSUBCOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 740;
      wobjDBParm = new Parameter( "@AUADICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUADICOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 750;
      wobjDBParm = new Parameter( "@AUMODORI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarAUMODORI ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 760;
      wobjDBParm = new Parameter( "@AUUSOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUUSOCOD ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 770;
      wobjDBParm = new Parameter( "@AUVTVCOD", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarAUVTVCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 780;
      wobjDBParm = new Parameter( "@AUVTVDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUVTVDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 790;
      wobjDBParm = new Parameter( "@AUVTVMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUVTVMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 800;
      wobjDBParm = new Parameter( "@AUVTVANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUVTVANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 810;
      wobjDBParm = new Parameter( "@VEHCLRCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarVEHCLRCOD ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 820;
      wobjDBParm = new Parameter( "@AUKLMNUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUKLMNUM ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 830;
      wobjDBParm = new Parameter( "@FABRICAN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarFABRICAN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 840;
      wobjDBParm = new Parameter( "@FABRICMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarFABRICMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 850;
      wobjDBParm = new Parameter( "@GUGARAGE", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarGUGARAGE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 860;
      wobjDBParm = new Parameter( "@GUDOMICI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarGUDOMICI ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 870;
      wobjDBParm = new Parameter( "@AUCATCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUCATCOD ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 880;
      wobjDBParm = new Parameter( "@AUTIPCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUTIPCOD ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 890;
      wobjDBParm = new Parameter( "@AUCIASAN", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarAUCIASAN ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 900;
      wobjDBParm = new Parameter( "@AUANTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUANTANN ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 910;
      wobjDBParm = new Parameter( "@AUNUMSIN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUNUMSIN ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 920;
      wobjDBParm = new Parameter( "@AUNUMKMT", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUNUMKMT ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 930;
      wobjDBParm = new Parameter( "@AUUSOGNC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarAUUSOGNC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 940;
      wobjDBParm = new Parameter( "@USUARCOD", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarUSUARCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 950;
      wobjDBParm = new Parameter( "@SITUCPOL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarSITUCPOL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 960;
      wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCLIENSEC ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 970;
      wobjDBParm = new Parameter( "@NUMEDOCU", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( mvarNUMEDOCU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 980;
      wobjDBParm = new Parameter( "@TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarTIPODOCU ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 990;
      wobjDBParm = new Parameter( "@DOMICSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarDOMICSEC ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1000;
      wobjDBParm = new Parameter( "@CUENTSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCUENTSEC ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1010;
      wobjDBParm = new Parameter( "@COBROFOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCOBROFOR ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1020;
      wobjDBParm = new Parameter( "@EDADACTU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarEDADACTU ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1025;
      wobjDBParm = new Parameter( "@SWCLIENT", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //
      // Coberturas
      wvarStep = 1030;
      for( wvarCounter = 1; wvarCounter <= 30; wvarCounter++ )
      {
        mvarCOBERCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_COBERCOD + wvarCounter ) */ );
        mvarCOBERORD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_COBERORD + wvarCounter ) */ );
        mvarCAPITASG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_CAPITASG + wvarCounter ) */ );
        mvarCAPITIMP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_CAPITIMP + wvarCounter ) */ );
        //
        wvarStep = 1040;
        wobjDBParm = new Parameter( "@COBERCOD" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCOBERCOD ) );
        wobjDBParm.setPrecision( (byte)( 3 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 1050;
        wobjDBParm = new Parameter( "@COBERORD" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCOBERORD ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 1060;
        wobjDBParm = new Parameter( "@CAPITASG" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCAPITASG ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 1070;
        wobjDBParm = new Parameter( "@CAPITIMP" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCAPITIMP ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
      }
      // Fin Coberturas
      // Asegurados
      wvarStep = 1080;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_ASEGURADOS ) */;
      //
      wvarStep = 1090;
      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        //
        mvarDOCUMDAT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_DOCUMDAT ) */ );
        mvarNOMBREAS = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_NOMBREAS ) */ ) ), 49 );
        //
        wvarStep = 1100;
        wobjDBParm = new Parameter( "@DOCUMDAT" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarDOCUMDAT ) );
        wobjDBParm.setPrecision( (byte)( 11 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 1110;
        wobjDBParm = new Parameter( "@NOMBREAS" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 49, new Variant( mvarNOMBREAS ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
      }
      //
      wvarStep = 1120;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
      {
        //
        wvarStep = 1130;
        wobjDBParm = new Parameter( "@DOCUMDAT" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 9 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 1140;
        wobjDBParm = new Parameter( "@NOMBREAS" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 49, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
      }
      //
      wobjXMLList = (org.w3c.dom.NodeList) null;
      // Fin Asegurados
      //
      wvarStep = 1150;
      wobjDBParm = new Parameter( "@CAMP_CODIGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCAMP_CODIGO ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1160;
      wobjDBParm = new Parameter( "@CAMP_DESC", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( Strings.left( Strings.trim( mvarCAMP_DESC ), 30 ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1170;
      wobjDBParm = new Parameter( "@LEGAJO_GTE", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarLEGAJO_GTE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1180;
      wobjDBParm = new Parameter( "@NRO_PROD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNRO_PROD ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1185;
      wobjDBParm = new Parameter( "@EMPRESA_CODIGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarEMPRESA_CODIGO ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1190;
      wobjDBParm = new Parameter( "@SUCURSAL_CODIGO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarSUCURSAL_CODIGO ) );
      //wobjDBParm.Precision = 6
      //wobjDBParm.NumericScale = 0
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1200;
      wobjDBParm = new Parameter( "@LEGAJO_VEND", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarLEGAJO_VEND ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1205;
      wobjDBParm = new Parameter( "@EMPL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarEMPL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      // Hijos
      wvarStep = 1210;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_CONDUCTORES ) */;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        mvarCONDUAPE = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUAPE ) */ ) ), 20 );
        mvarCONDUNOM = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUNOM ) */ ) ), 30 );

        wvarStep = 1220;
        mvarCONDUFEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUFEC ) */ );
        mvarCONDUSEX = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUSEX ) */ );
        mvarCONDUEST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUEST ) */ );
        mvarCONDUEXC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUEXC ) */ );

        wvarStep = 1230;
        wobjDBParm = new Parameter( "@CONDUAPE" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( mvarCONDUAPE ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1240;
        wobjDBParm = new Parameter( "@CONDUNOM" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarCONDUNOM ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1250;
        wobjDBParm = new Parameter( "@CONDUFEC" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCONDUFEC ) );
        wobjDBParm.setPrecision( (byte)( 8 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1260;
        wobjDBParm = new Parameter( "@CONDUSEX" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCONDUSEX ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1270;
        wobjDBParm = new Parameter( "@CONDUEST" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCONDUEST ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1280;
        wobjDBParm = new Parameter( "@CONDUEXC" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCONDUEXC ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wvarStep = 1290;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
      {

        wvarStep = 1300;
        wobjDBParm = new Parameter( "@CONDUAPE" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1310;
        wobjDBParm = new Parameter( "@CONDUNOM" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1320;
        wobjDBParm = new Parameter( "@CONDUFEC" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 8 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1330;
        wobjDBParm = new Parameter( "@CONDUSEX" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1340;
        wobjDBParm = new Parameter( "@CONDUEST" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1350;
        wobjDBParm = new Parameter( "@CONDUEXC" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wvarStep = 1360;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      // Conductores
      // Accesorios
      wvarStep = 1370;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_ACCESORIOS ) */;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        wvarStep = 1380;
        mvarAUACCCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_AUACCCOD ) */ );
        mvarAUVEASUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_AUVEASUM ) */ );
        mvarAUVEADES = Strings.left( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_AUVEADES ) */ ) ), 30 );
        mvarAUVEADEP = "S";

        wvarStep = 1390;
        wobjDBParm = new Parameter( "@AUACCCOD" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUACCCOD ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1400;
        wobjDBParm = new Parameter( "@AUVEASUM" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarAUVEASUM.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarAUVEASUM)) );
        wobjDBParm.setPrecision( (byte)( 14 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1410;
        wobjDBParm = new Parameter( "@AUVEADES" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarAUVEADES ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1420;
        wobjDBParm = new Parameter( "@AUVEADEP" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarAUVEADEP ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wvarStep = 1430;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
      {

        wvarStep = 1440;
        wobjDBParm = new Parameter( "@AUACCCOD" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1450;
        wobjDBParm = new Parameter( "@AUVEASUM" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 14 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1460;
        wobjDBParm = new Parameter( "@AUVEADES" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1470;
        wobjDBParm = new Parameter( "@AUVEADEP" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wvarStep = 1480;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      // Fin Accesorios
      wvarStep = 1490;
      wobjDBParm = new Parameter( "@ESTAINSP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarESTAINSP ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1500;
      wobjDBParm = new Parameter( "@INSPECOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPECOD ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1510;
      wobjDBParm = new Parameter( "@INSPEDES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarINSPEDES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1520;
      wobjDBParm = new Parameter( "@INSPADOM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarINSPADOM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1530;
      wobjDBParm = new Parameter( "@INSPEOBS", AdoConst.adChar, AdoConst.adParamInput, 50, new Variant( mvarINSPEOBS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1540;
      wobjDBParm = new Parameter( "@INSPEKMS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPEKMS ) );
      wobjDBParm.setPrecision( (byte)( 8 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1550;
      wobjDBParm = new Parameter( "@CENSICOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarCENSICOD ) );
      //wobjDBParm.Precision = 4
      //wobjDBParm.NumericScale = 0
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1560;
      wobjDBParm = new Parameter( "@CENSIDES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarCENSIDES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //    wvarStep = 1570
      //    Set wobjDBParm = wobjDBCmd.CreateParameter("@INSPECC", adNumeric, adParamInput, , mvarINSPECC)
      //    wobjDBParm.Precision = 2
      //    wobjDBParm.NumericScale = 0
      //    wobjDBCmd.Parameters.Append wobjDBParm
      //    Set wobjDBParm = Nothing
      wvarStep = 1570;
      wobjDBParm = new Parameter( "@DISPOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarDISPOCOD ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1580;
      wobjDBParm = new Parameter( "@PRESTCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarPRESTCOD ) );
      //wobjDBParm.Precision = 2
      //wobjDBParm.NumericScale = 0
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1590;
      wobjDBParm = new Parameter( "@NUMEDOCU_ACRE", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( mvarNUMEDOCU_ACRE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1600;
      wobjDBParm = new Parameter( "@TIPODOCU_ACRE", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarTIPODOCU_ACRE ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1610;
      wobjDBParm = new Parameter( "@APELLIDO", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( mvarAPELLIDO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1620;
      wobjDBParm = new Parameter( "@NOMBRES", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( mvarNOMBRES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1630;
      wobjDBParm = new Parameter( "@DOMICDOM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarDOMICDOM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1640;
      wobjDBParm = new Parameter( "@DOMICDNU", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( mvarDOMICDNU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1650;
      wobjDBParm = new Parameter( "@DOMICESC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarDOMICESC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1660;
      wobjDBParm = new Parameter( "@DOMICPIS", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarDOMICPIS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1670;
      wobjDBParm = new Parameter( "@DOMICPTA", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarDOMICPTA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1680;
      wobjDBParm = new Parameter( "@DOMICPOB", AdoConst.adVarChar, AdoConst.adParamInput, 20, new Variant( mvarDOMICPOB ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1690;
      wobjDBParm = new Parameter( "@DOMICCPO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarDOMICCPO ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1700;
      wobjDBParm = new Parameter( "@PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPROVICOD ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1710;
      wobjDBParm = new Parameter( "@PAISSCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarPAISSCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1720;
      wobjDBParm = new Parameter( "@BANCOCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarBANCOCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1730;
      wobjDBParm = new Parameter( "@COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCOBROCOD ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1740;
      wobjDBParm = new Parameter( "@EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarEfectAnn ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1750;
      wobjDBParm = new Parameter( "@EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarEFECTMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1760;
      wobjDBParm = new Parameter( "@EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarEFECTDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1770;
      wobjDBParm = new Parameter( "@INSPEANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPEANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1780;
      wobjDBParm = new Parameter( "@INSPEMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPEMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1790;
      wobjDBParm = new Parameter( "@INSPEDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPEDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1800;
      wobjDBParm = new Parameter( "@ENTDOMSC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarENTDOMSC ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1810;
      wobjDBParm = new Parameter( "@ANOTACIO", AdoConst.adChar, AdoConst.adParamInput, 100, new Variant( mvarANOTACIO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1820;
      wobjDBParm = new Parameter( "@PRECIO_MENSUAL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPRECIO_MENSUAL ) );
      wobjDBParm.setPrecision( (byte)( 13 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1825;
      wobjDBParm = new Parameter( "@TIPO_ACTUA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarTIPO_ACTUA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1830;
      wobjDBParm = new Parameter( "@CUITNUME", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( mvarCUITNUME ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1840;
      wobjDBParm = new Parameter( "@RAZONSOC", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant( mvarRAZONSOC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1850;
      wobjDBParm = new Parameter( "@CLIEIBTP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLIEIBTP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1860;
      wobjDBParm = new Parameter( "@NROIIBB", AdoConst.adChar, AdoConst.adParamInput, 15, new Variant( mvarNROIIBB ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1870;
      wobjDBParm = new Parameter( "@LUNETA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarLUNETA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1880;
      wobjDBParm = new Parameter( "@HIJOS1729_EXC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarHIJOS1729_EXC ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1890;
      wobjDBParm = new Parameter( "@NROFACTU", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( mvarNROFACTU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1900;
      wobjDBParm = new Parameter( "@INS_DOMICDOM", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( mvarINS_DOMICDOM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1910;
      wobjDBParm = new Parameter( "@INS_CPACODPO", AdoConst.adChar, AdoConst.adParamInput, 8, new Variant( mvarINS_CPACODPO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1920;
      wobjDBParm = new Parameter( "@INS_DOMICPOB", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( mvarINS_DOMICPOB ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1930;
      wobjDBParm = new Parameter( "@INS_PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINS_PROVICOD ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1940;
      wobjDBParm = new Parameter( "@INS_DOMICTLF", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarINS_DOMICTLF ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1950;
      wobjDBParm = new Parameter( "@INS_RANGOHOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINS_RANGOHOR ) );
      wobjDBParm.setPrecision( (byte)( 8 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1960;
      wobjDBParm = new Parameter( "@DDJJ", AdoConst.adVarChar, AdoConst.adParamInput, 140, new Variant( mvarDDJJ ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1970;
      wobjDBParm = new Parameter( "@DERIVACI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarDERIVACI ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1980;
      wobjDBParm = new Parameter( "@ALEATORIO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarALEATORIO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1990;
      wobjDBParm = new Parameter( "@ESTAIDES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarESTAIDES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2000;
      wobjDBParm = new Parameter( "@DISPODES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarDISPODES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2001;
      wobjDBParm = new Parameter( "@INSTALADP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarINSTALADP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2002;
      wobjDBParm = new Parameter( "@POSEEDISP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarPOSEEDISP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2003;
      wobjDBParm = new Parameter( "@AUTIPGAMA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarAUTIPGAMA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      // 23/01/2006
      wvarStep = 2004;
      wobjDBParm = new Parameter( "@SWDT80", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarDEST80 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      if( ! (wvarTIPOPROD.equals( "" )) )
      {
        //22/3/2006 Ale
        wvarStep = 2005;
        wobjDBParm = new Parameter( "@TIPO_PROD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarTIPOPROD ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      //Comienzo datos exigidos por la UIF
      wvarStep = 2006;
      wobjDBParm = new Parameter( "@SWAPODER", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarSWAPODER ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2007;
      wobjDBParm = new Parameter( "@OCUPACODCLIE", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( wvarOCUPACODCLIE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2008;
      wobjDBParm = new Parameter( "@OCUPACODAPOD", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( wvarOCUPACODAPOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2009;
      wobjDBParm = new Parameter( "@CLIENSECAPOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCLIENSECAPOD ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2010;
      wobjDBParm = new Parameter( "@DOMISECAPOD", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( wvarDOMISECAPOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //Fin datos exigidos por la UIF
      //Green Products
      wvarStep = 2011;
      wobjDBParm = new Parameter( "@GREEN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarCLUBECO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2012;
      wobjDBParm = new Parameter( "@GRANIZO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarGRANIZO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2013;
      wobjDBParm = new Parameter( "@ROBOCONT", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarROBOCONT ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //Fin GREEN Products
      // **************************************************************************************
      // EMPIEZA - SUSCRIPCION A NBWS Y EPOLIZA
      // **************************************************************************************
      //SN_NBWS
      wvarStep = 2022;
      wobjDBParm = new Parameter( "@SN_NBWS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarSN_NBWS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //EMAIL_NBWS
      wvarStep = 2023;
      wobjDBParm = new Parameter( "@EMAIL_NBWS", AdoConst.adVarChar, AdoConst.adParamInput, 50, new Variant( Strings.toUpperCase( mvarEMAIL_NBWS ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //SN_EPOLIZA
      wvarStep = 2024;
      wobjDBParm = new Parameter( "@SN_EPOLIZA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarSN_EPOLIZA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //EMAIL_EPOLIZA
      wvarStep = 2025;
      wobjDBParm = new Parameter( "@EMAIL_EPOLIZA", AdoConst.adVarChar, AdoConst.adParamInput, 50, new Variant( Strings.toUpperCase( mvarEMAIL_EPOLIZA ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //SN_PRESTAMAIL
      wvarStep = 2026;
      wobjDBParm = new Parameter( "@SN_PRESTAMAIL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarSN_PRESTAMAIL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //13/10/2009 Se agrega Pass de EPoliza
      //PASSEPOL
      wvarStep = 2027;
      wobjDBParm = new Parameter( "@PASSEPOL", AdoConst.adVarChar, AdoConst.adParamInput, 10, new Variant( mvarPASSEPOL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      // *******************************************
      // TERMINA - SUSCRIPCION A NBWS Y EPOLIZA
      // *******************************************
      //'''''''''''''''''''''' Ver parametros '''''''''''''''
      //    wvarStep = 2010
      //    Dim aa
      //    Dim i
      //    For i = 1 To wobjDBCmd.Parameters.Count - 1
      //        If wobjDBCmd.Parameters(i).Type = adChar Then
      //            aa = aa & "'" & wobjDBCmd.Parameters(i).Value & "',"
      //        Else
      //            aa = aa & wobjDBCmd.Parameters(i).Value & ","
      //        End If
      //    Next
      //App.LogEvent "Execute " & mcteStoreProc & " " & aa
      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      wvarStep = 2029;
      //wobjDBCmd.NamedParameters = True
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );

      wvarStep = 2030;
      if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><CADENA>" + wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue() + "</CADENA>" + mvarXMLCoberturas + "</Response>" );
      }
      else
      {
        
        if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -1 )
        {
          wvarMensaje = "Error en la tabla SIFSPOLI";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -2 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -3 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -4 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -5 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -6 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -7 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -8 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -9 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -10 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -11 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 10";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -12 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -13 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -14 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -15 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -16 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -17 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -18 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -19 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -20 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -21 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 10";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -22 )
        {
          wvarMensaje = "Error en la tabla SIFSMOVI                ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -23 )
        {
          wvarMensaje = "Error en la tabla SIFSFIMA_TARI               ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -24 )
        {
          wvarMensaje = "Error en la tabla SIFSFIMA_VEHI               ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -25 )
        {
          wvarMensaje = "Error en la tabla SIFSPECO                ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -26 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_CAMP               ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -27 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_GTE                ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -28 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_PROD               ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -29 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_SUCU               ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -30 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_VEND               ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -31 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 1         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -32 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 2         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -33 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 3         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -34 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 4         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -35 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 5         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -36 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 6         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -37 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 7         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -38 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 8         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -39 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 9         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -40 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 10        ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -41 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 1         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -42 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 2         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -43 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 3         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -44 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 4         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -45 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 5         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -46 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 6         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -47 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 7         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -48 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 8         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -49 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 9         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -50 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 10        ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -51 )
        {
          wvarMensaje = "Error en la tabla SIFWINSV                ";
        }
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='" + wvarMensaje + "' /></Response>" );
      }

      wvarStep = 2040;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      wvarStep = 2050;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = (HSBC.DBConnectionTrx) null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = (HSBC.DBConnectionTrx) null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String GetCoberturas( diamondedge.util.XmlDom pobjXMLCoberturas, boolean pvarConHijos ) throws Exception
  {
    String GetCoberturas = "";
    diamondedge.util.XmlDom wobjXMLResponse = null;
    org.w3c.dom.Element wobjNodoCoberturas = null;
    org.w3c.dom.Element wobjNodoCobertura = null;
    org.w3c.dom.Element wobjCOBERCOD = null;
    org.w3c.dom.Element wobjCOBERORD = null;
    org.w3c.dom.Element wobjCAPITASG = null;
    org.w3c.dom.Element wobjCAPITIMP = null;
    String wvarResponse = "";
    int wvarCounter = 0;

    wobjXMLResponse = new diamondedge.util.XmlDom();
    //unsup wobjXMLResponse.async = false;
    wobjXMLResponse.loadXML( "<COBERTURAS></COBERTURAS>" );
    wobjNodoCoberturas = (org.w3c.dom.Element) null /*unsup wobjXMLResponse.selectSingleNode( "//COBERTURAS" ) */;

    for( wvarCounter = 1; wvarCounter <= 30; wvarCounter++ )
    {
      //For wvarCounter = 1 To 20
      if( VB.val( Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup pobjXMLCoberturas.selectSingleNode( (mcteParam_CAPITIMP + wvarCounter) ) */ ), ",", "." ) ) != 0 )
      {
        wobjNodoCobertura = wobjXMLResponse.getDocument().createElement( "COBERTURA" );
        wobjNodoCoberturas.appendChild( wobjNodoCobertura );
        //
        wobjCOBERCOD = wobjXMLResponse.getDocument().createElement( Strings.mid( mcteParam_COBERCOD, 3 ) );
        wobjNodoCobertura.appendChild( wobjCOBERCOD );
        diamondedge.util.XmlDom.setText( wobjCOBERCOD, diamondedge.util.XmlDom.getText( null /*unsup pobjXMLCoberturas.selectSingleNode( mcteParam_COBERCOD + wvarCounter ) */ ) );
        //
        wobjCOBERORD = wobjXMLResponse.getDocument().createElement( Strings.mid( mcteParam_COBERORD, 3 ) );
        wobjNodoCobertura.appendChild( wobjCOBERORD );
        diamondedge.util.XmlDom.setText( wobjCOBERORD, diamondedge.util.XmlDom.getText( null /*unsup pobjXMLCoberturas.selectSingleNode( mcteParam_COBERORD + wvarCounter ) */ ) );
        //
        wobjCAPITASG = wobjXMLResponse.getDocument().createElement( Strings.mid( mcteParam_CAPITASG, 3 ) );
        wobjNodoCobertura.appendChild( wobjCAPITASG );
        diamondedge.util.XmlDom.setText( wobjCAPITASG, Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup pobjXMLCoberturas.selectSingleNode( mcteParam_CAPITASG + wvarCounter ) */ ), ",", "." ) );
        //
        wobjCAPITIMP = wobjXMLResponse.getDocument().createElement( Strings.mid( mcteParam_CAPITIMP, 3 ) );
        wobjNodoCobertura.appendChild( wobjCAPITIMP );
        diamondedge.util.XmlDom.setText( wobjCAPITIMP, Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup pobjXMLCoberturas.selectSingleNode( mcteParam_CAPITIMP + wvarCounter ) */ ), ",", "." ) );
      }
    }
    GetCoberturas = wobjXMLResponse.getDocument().getDocumentElement().toString();
    ClearObjects: 
    wobjCOBERCOD = (org.w3c.dom.Element) null;
    wobjCOBERORD = (org.w3c.dom.Element) null;
    wobjCAPITASG = (org.w3c.dom.Element) null;
    wobjCAPITIMP = (org.w3c.dom.Element) null;
    wobjXMLResponse = (diamondedge.util.XmlDom) null;
    return GetCoberturas;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
