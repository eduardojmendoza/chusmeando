package com.qbe.services.lbavirtualmqiii;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.format.VBFixesUtil;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVIIIConvProdOrg implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQIII.lbaw_OVIIIConvProdOrg";
  static final String mcteOpID = "1555";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteAgente_Clase = "//AGENTECLASE";
  static final String mcteAgenteCodigo = "//AGENTECODIGO";
  static final String mcteParam_CodRamo = "//RAMOPCOD";
  static final String mcteParam_ConveCop = "//CONVECOP";
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarAgenteClase = "";
    String wvarAgenteCodigo = "";
    String wvarCodigRamo = "";
    String wvarConveCop = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //Deber� venir desde la p�gina
      wvarUsuario = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarAgenteClase = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteAgente_Clase )  ) + Strings.space( 2 ), 2 );
      wvarAgenteCodigo = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteAgenteCodigo )  ), 4 );
      wvarCodigRamo = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_CodRamo )  ) + Strings.space( 4 ), 4 );

      //El par�metro CONVECOP es opcional
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_ConveCop )  == (org.w3c.dom.Node) null) )
      {
        wvarConveCop = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_ConveCop )  ) + Strings.space( 4 ), 4 );
      }
      else
      {
        wvarConveCop = Strings.space( 4 );
      }

      //
      //
      wvarStep = 60;
      wobjXMLRequest = null;
      //
      wvarStep = 70;
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wvarCiaAsCod = "0001";
      //
      wvarStep = 110;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarAgenteCodigo + wvarAgenteClase + wvarCodigRamo + wvarConveCop;
      
	wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);
	StringHolder strParseStringHolder = new StringHolder( strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();
      

      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      //
      wvarStep = 240;
      wvarResult = "";

      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = "";
        //77 cantidad de caracteres ocupados por par�metros de entrada
        wvarPos = 81;
        wvarResult = ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        //
        wvarStep = 340;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
        //
      }
      //
      wvarStep = 360;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarAgenteCodigo + wvarAgenteClase + wvarCodigRamo + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String ParseoMensaje( int pvarPos, String pstrParseString, int pvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    XmlDomExtended wobjXslDOMResp = null;
    VbScript_RegExp wobjRegExp = new VbScript_RegExp();
    MatchCollection wobjColMatch;
    Match wobjMatch = null;
    String wvarParseEvalString = "";
    XmlDomExtended wobjXmlDOMResp = null;
    org.w3c.dom.Element wobjXmlElemenResp = null;
    org.w3c.dom.Element wobjXmlElemenRespP = null;
    int wvarCantRegistros = 0;
    int wvarCcurrRegistro = 0;
    String wvarCodigoConvenio = "";
    //RegExp
    //MatchCollection
    //Match
    //
    wobjRegExp = new VbScript_RegExp();
    wobjRegExp.setGlobal( true ); 
    //
    wvarCantRegistros = (int)Math.rint( VBFixesUtil.val( Strings.mid( pstrParseString, pvarPos, 3 ) ) );
    wvarCcurrRegistro = 0;
    //Elimino el parametro que indica la cantidad de registros devueltos
    wvarParseEvalString = Strings.mid( pstrParseString, pvarPos + 3 );
    wvarParseEvalString = Strings.replace( wvarParseEvalString, " ", "_" );

    wobjRegExp.setPattern( "(\\S{4})(\\S{60})(\\S{4})(\\S{5})(\\S{5})" );

    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    wobjXmlDOMResp = new XmlDomExtended();
    wobjXslDOMResp = new XmlDomExtended();

    wobjXmlDOMResp.loadXML( "<RS></RS>" );
    //
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //
      //error: function 'SubMatches' was not found.
      //unsup: wvarCodigoConvenio = Trim(Strings.replace(wobjMatch.SubMatches(0), "_", " "))
      if( (!Strings.trim( wvarCodigoConvenio ).equals( "" )) && (wvarCcurrRegistro < wvarCantRegistros) )
      {
        wvarCcurrRegistro = wvarCcurrRegistro + 1;
        wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "CONVENIO" );
        wobjXmlDOMResp.selectSingleNode( "//RS" ).appendChild( wobjXmlElemenResp );
        //
        wobjXmlElemenRespP = wobjXmlDOMResp.getDocument().createElement( "CODIGO" );
        wobjXmlElemenResp.appendChild( wobjXmlElemenRespP );
        XmlDomExtended.setText( wobjXmlElemenRespP, wvarCodigoConvenio );
        //
        wobjXmlElemenRespP = wobjXmlDOMResp.getDocument().createElement( "DESCRIPCION" );
        wobjXmlElemenResp.appendChild( wobjXmlElemenRespP );
        //error: function 'SubMatches' was not found.
        //unsup: wobjXmlElemenRespP.Text = UCase(Trim(Strings.replace(wobjMatch.SubMatches(1), "_", " ")))
        //
        wobjXmlElemenRespP = wobjXmlDOMResp.getDocument().createElement( "CUADRO" );
        wobjXmlElemenResp.appendChild( wobjXmlElemenRespP );
        //error: function 'SubMatches' was not found.
        //unsup: wobjXmlElemenRespP.Text = UCase(Trim(Strings.replace(wobjMatch.SubMatches(2), "_", " ")))
        //
        wobjXmlElemenRespP = wobjXmlDOMResp.getDocument().createElement( "RECARGO" );
        wobjXmlElemenResp.appendChild( wobjXmlElemenRespP );
        //error: function 'SubMatches' was not found.
        //unsup: wobjXmlElemenRespP.Text = Val(Strings.replace(wobjMatch.SubMatches(3), "_", " ")) / 100
        //
        wobjXmlElemenRespP = wobjXmlDOMResp.getDocument().createElement( "COMISION" );
        wobjXmlElemenResp.appendChild( wobjXmlElemenRespP );
        //error: function 'SubMatches' was not found.
        //unsup: wobjXmlElemenRespP.Text = Val(Strings.replace(wobjMatch.SubMatches(4), "_", " ")) / 100
      }
      //
    }
    //
    wobjXslDOMResp.loadXML( p_GetXSL());
    ParseoMensaje = wobjXmlDOMResp.transformNode( wobjXslDOMResp ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
    //ParseoMensaje = wobjXmlDOMResp.xml
    //
    wobjXslDOMResp = null;
    wobjXmlDOMResp = null;
    wobjXmlElemenResp = (org.w3c.dom.Element) null;
    wobjXmlElemenRespP = (org.w3c.dom.Element) null;
    //
    wobjRegExp = (VbScript_RegExp) null;

    return ParseoMensaje;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<?xml version='1.0' encoding='UTF-8'?>";
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>";
    wvarStrXSL = wvarStrXSL + "<xsl:output method='html' version='1.0' encoding='UTF-8' indent='yes'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:template match='/' >";
    wvarStrXSL = wvarStrXSL + "<xsl:for-each select='//CONVENIO'>";
    wvarStrXSL = wvarStrXSL + "     <OPTION>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='cuadro'><xsl:value-of select='CUADRO'/></xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='value'><xsl:value-of select='CODIGO'/></xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='recargo'><xsl:value-of select='RECARGO'/></xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='comision'><xsl:value-of select='COMISION'/></xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "     <xsl:value-of select='DESCRIPCION'/>";
    wvarStrXSL = wvarStrXSL + "     </OPTION>";
    wvarStrXSL = wvarStrXSL + "     </xsl:for-each>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template >";
    wvarStrXSL = wvarStrXSL + "     </xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    //
    return p_GetXSL;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
