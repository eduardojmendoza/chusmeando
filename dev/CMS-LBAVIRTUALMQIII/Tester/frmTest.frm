VERSION 5.00
Begin VB.Form frmTest 
   Caption         =   "Tester de Componentes"
   ClientHeight    =   8520
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8685
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   8520
   ScaleWidth      =   8685
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtResp 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   3555
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   5
      Top             =   4860
      Width           =   8415
   End
   Begin VB.TextBox txtReq 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3315
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   3
      Top             =   1260
      Width           =   8415
   End
   Begin VB.ComboBox cmbComp 
      Height          =   315
      Left            =   1260
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   120
      Width           =   7275
   End
   Begin VB.CommandButton cmdEjecutar 
      Caption         =   "Ejecutar Proceso"
      Height          =   435
      Left            =   120
      TabIndex        =   0
      Top             =   540
      Width           =   8415
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Response"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   4620
      Width           =   8415
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Request"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   1020
      Width           =   8415
   End
   Begin VB.Label lblComp 
      AutoSize        =   -1  'True
      Caption         =   "Componente:"
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   180
      Width           =   975
   End
End
Attribute VB_Name = "frmTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private oRef As HSBCInterfaces.IAction

Private Sub cmbComp_Click()
    Set oRef = Nothing
    On Error Resume Next
    Set oRef = CreateObject(Trim(Left(cmbComp.Text, 50)))
    
    Dim oFSO As New FileSystemObject
    Dim oFile As TextStream
    
    If oFSO.FileExists(App.Path & "\XML\" & Trim(Right(cmbComp.Text, 100))) Then
        Set oFile = oFSO.OpenTextFile(App.Path & "\XML\" & Trim(Right(cmbComp.Text, 100)))
        txtReq.Text = oFile.ReadAll
        oFile.Close
    Else
        txtReq.Text = "No existe el archivo Request de Prueba para el componente: " & cmbComp.Text
    End If
    
    Set oFile = Nothing
    Set oFSO = Nothing
End Sub

Private Sub cmdEjecutar_Click()
    Dim wvarReq As String
    Dim wvarResp As String
    On Error Resume Next
    wvarReq = txtReq.Text
    
    cmdEjecutar.FontBold = True
    cmdEjecutar.Caption = "Ejecutando Pedido MQ ..."
    oRef.Execute wvarReq, wvarResp, ""
    cmdEjecutar.Caption = "Ejecutar Proceso"
    cmdEjecutar.FontBold = False
    
    If wvarResp = "" Then
        txtResp.Text = ""
    Else
        txtResp.Text = wvarResp
    End If
End Sub

Private Sub Form_Load()
Dim sFileName As String
Dim sClassName As String
    sFileName = Dir(App.Path & "\XML\*.xml")
    Do While Not sFileName = ""
        sClassName = Left(sFileName, Len(sFileName) - 4)
        cmbComp.AddItem "lbawA_OVMQIII." & sClassName & Space(199) & sFileName
        sFileName = Dir()
    Loop
    cmbComp.ListIndex = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oRef = Nothing
End Sub
