VERSION 5.00
Begin VB.Form Form2 
   Caption         =   "Test lbawA_OVMQIII"
   ClientHeight    =   8610
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   15240
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8610
   ScaleWidth      =   15240
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command5 
      Caption         =   "---"
      Height          =   375
      Left            =   13560
      TabIndex        =   38
      Top             =   6720
      Width           =   1575
   End
   Begin VB.TextBox Text18 
      Height          =   375
      Left            =   7800
      ScrollBars      =   2  'Vertical
      TabIndex        =   37
      Text            =   "---"
      Top             =   6240
      Width           =   7335
   End
   Begin VB.TextBox Text17 
      Height          =   1335
      Left            =   7800
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   36
      Top             =   7200
      Width           =   7455
   End
   Begin VB.TextBox Text13 
      Height          =   285
      Left            =   8520
      TabIndex        =   35
      Text            =   "---"
      Top             =   6840
      Width           =   4935
   End
   Begin VB.TextBox Text12 
      Height          =   285
      Left            =   8520
      TabIndex        =   31
      Text            =   "---"
      Top             =   3960
      Width           =   4935
   End
   Begin VB.TextBox Text11 
      Height          =   1335
      Left            =   7800
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   30
      Top             =   4320
      Width           =   7455
   End
   Begin VB.TextBox Text10 
      Height          =   375
      Left            =   7800
      ScrollBars      =   2  'Vertical
      TabIndex        =   29
      Text            =   "---"
      Top             =   3360
      Width           =   7335
   End
   Begin VB.CommandButton Command4 
      Caption         =   "---"
      Height          =   375
      Left            =   13560
      TabIndex        =   28
      Top             =   3840
      Width           =   1575
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Execute Clausulas"
      Height          =   375
      Left            =   13560
      TabIndex        =   24
      Top             =   840
      Width           =   1575
   End
   Begin VB.TextBox Text8 
      Height          =   375
      Left            =   7800
      ScrollBars      =   2  'Vertical
      TabIndex        =   23
      Text            =   "<Request><USUARIO>EX009000L </USUARIO><RAMOPCOD>ICO1</RAMOPCOD><PERIODO>1</PERIODO></Request>"
      Top             =   360
      Width           =   7335
   End
   Begin VB.TextBox Text7 
      Height          =   1455
      Left            =   7800
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   22
      Top             =   1320
      Width           =   7455
   End
   Begin VB.TextBox Text6 
      Height          =   285
      Left            =   8520
      TabIndex        =   21
      Text            =   "---"
      Top             =   960
      Width           =   4935
   End
   Begin VB.TextBox Text4 
      Height          =   375
      Left            =   0
      ScrollBars      =   2  'Vertical
      TabIndex        =   13
      Text            =   "<Request><USUARIO>EX009000L </USUARIO><ZONA>0001</ZONA><AGRUPTIP>1</AGRUPTIP><AGRUPCOD>28</AGRUPCOD></Request>"
      Top             =   6240
      Width           =   7575
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Execute Provincias"
      Height          =   375
      Left            =   5880
      TabIndex        =   12
      Top             =   6720
      Width           =   1695
   End
   Begin VB.TextBox Text3 
      Height          =   1335
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   11
      Top             =   7200
      Width           =   7575
   End
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   840
      TabIndex        =   10
      Text            =   "---"
      Top             =   6840
      Width           =   4815
   End
   Begin VB.TextBox Text16 
      Height          =   285
      Left            =   840
      TabIndex        =   9
      Text            =   "C:\Documents and Settings\Exramis\Escritorio\Preguntas.xml"
      Top             =   3960
      Width           =   4815
   End
   Begin VB.TextBox Text15 
      Height          =   285
      Left            =   840
      TabIndex        =   8
      Text            =   "C:\WebPublico\coberturas\coberturas.xml"
      Top             =   960
      Width           =   4815
   End
   Begin VB.TextBox Text14 
      Height          =   1335
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   6
      Top             =   4320
      Width           =   7575
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Execute Preguntas"
      Height          =   375
      Left            =   5880
      TabIndex        =   5
      Top             =   3840
      Width           =   1695
   End
   Begin VB.TextBox Text9 
      Height          =   375
      Left            =   0
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Text            =   "<Request><USUARIO>EX009000L </USUARIO><ZONA>0001</ZONA><ACTIVIDAD>050022</ACTIVIDAD><RUBRO>ICO2</RUBRO></Request>"
      Top             =   3360
      Width           =   7575
   End
   Begin VB.TextBox Text5 
      Height          =   1455
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Top             =   1320
      Width           =   7575
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   0
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Text            =   "<Request><USUARIO>EX009000L </USUARIO><ZONA>0001</ZONA><ACTIVIDAD>050022</ACTIVIDAD><RAMOPCOD>ICO2</RAMOPCOD></Request>"
      Top             =   360
      Width           =   7575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Execute Coberturas"
      Height          =   375
      Left            =   5880
      TabIndex        =   0
      Top             =   840
      Width           =   1695
   End
   Begin VB.Label Label18 
      Caption         =   "XML Out"
      Height          =   255
      Left            =   7680
      TabIndex        =   41
      Top             =   6840
      Width           =   1575
   End
   Begin VB.Line Line5 
      BorderWidth     =   4
      X1              =   8520
      X2              =   14760
      Y1              =   9000
      Y2              =   9000
   End
   Begin VB.Label Label17 
      AutoSize        =   -1  'True
      Caption         =   "XML In"
      Height          =   195
      Left            =   7800
      TabIndex        =   40
      Top             =   6000
      Width           =   510
   End
   Begin VB.Label Label16 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "---"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   11070
      TabIndex        =   39
      Top             =   5880
      Width           =   315
   End
   Begin VB.Label Label15 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "---"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   11070
      TabIndex        =   34
      Top             =   3000
      Width           =   315
   End
   Begin VB.Label Label14 
      AutoSize        =   -1  'True
      Caption         =   "XML In"
      Height          =   195
      Left            =   7800
      TabIndex        =   33
      Top             =   3120
      Width           =   510
   End
   Begin VB.Line Line4 
      BorderWidth     =   4
      X1              =   8520
      X2              =   14760
      Y1              =   5760
      Y2              =   5760
   End
   Begin VB.Label Label13 
      Caption         =   "XML Out"
      Height          =   255
      Left            =   7680
      TabIndex        =   32
      Top             =   3960
      Width           =   1575
   End
   Begin VB.Label Label12 
      Caption         =   "XML Out"
      Height          =   255
      Left            =   7680
      TabIndex        =   27
      Top             =   960
      Width           =   1575
   End
   Begin VB.Line Line3 
      BorderWidth     =   4
      X1              =   8520
      X2              =   14760
      Y1              =   2880
      Y2              =   2880
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      Caption         =   "XML In"
      Height          =   195
      Left            =   7800
      TabIndex        =   26
      Top             =   120
      Width           =   510
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Clausulas de Ajuste"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   10020
      TabIndex        =   25
      Top             =   0
      Width           =   2415
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Coberturas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3225
      TabIndex        =   20
      Top             =   0
      Width           =   1365
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "XML In"
      Height          =   195
      Left            =   0
      TabIndex        =   19
      Top             =   120
      Width           =   510
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Preguntas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3240
      TabIndex        =   18
      Top             =   3000
      Width           =   1275
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "XML In"
      Height          =   195
      Left            =   0
      TabIndex        =   17
      Top             =   3120
      Width           =   510
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Provincias"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3285
      TabIndex        =   16
      Top             =   5880
      Width           =   1245
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "XML In"
      Height          =   195
      Left            =   0
      TabIndex        =   15
      Top             =   6000
      Width           =   510
   End
   Begin VB.Line Line2 
      BorderWidth     =   4
      X1              =   720
      X2              =   6960
      Y1              =   5760
      Y2              =   5760
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "XML Out"
      Height          =   195
      Left            =   0
      TabIndex        =   14
      Top             =   6960
      Width           =   630
   End
   Begin VB.Label Label10 
      Caption         =   "XML Out"
      Height          =   495
      Left            =   0
      TabIndex        =   7
      Top             =   4080
      Width           =   1575
   End
   Begin VB.Line Line1 
      BorderWidth     =   4
      X1              =   720
      X2              =   6960
      Y1              =   2880
      Y2              =   2880
   End
   Begin VB.Label Label6 
      Caption         =   "XML Out"
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   960
      Width           =   1575
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()
    On Error Resume Next
    Me.MousePointer = 11
    Dim Exec As IAction
    Dim sResult As String
    Dim ValRes As Long
    Set Exec = CreateObject("lbawA_OVMQIII.lbaw_OVIIIGetCober")
    ValRes = Exec.Execute(Text1.Text, sResult, "")
    Set Exec = Nothing
    Me.Text5.Text = sResult
    Dim Dom As DOMDocument
    Set Dom = New DOMDocument
    Dom.loadXML sResult
    Dom.Save Text15.Text
    Set Dom = Nothing
    Me.MousePointer = 0
End Sub

Private Sub Command2_Click()
    On Error Resume Next
    Me.MousePointer = 11
    Dim Exec As IAction
    Dim sResult As String
    Dim ValRes As Long
    Set Exec = CreateObject("lbawA_OVMQIII.lbaw_OVIIIGetProvincias")
    ValRes = Exec.Execute(Text4.Text, sResult, "")
    Set Exec = Nothing
    Text3.Text = sResult
    Dim Dom As DOMDocument
    Set Dom = New DOMDocument
    Dom.loadXML sResult
    Dom.Save Text2.Text
    Set Dom = Nothing
    Me.MousePointer = 0
End Sub

Private Sub Command4_Click()
    On Error Resume Next
    Me.MousePointer = 11
    Dim Exec As IAction
    Dim sResult As String
    Dim ValRes As Long
    Set Exec = CreateObject("lbawA_OVMQIII.lbaw_OVIIIZonasGeograf")
    ValRes = Exec.Execute(Text10.Text, sResult, "")
    Set Exec = Nothing
    Text11.Text = sResult
    Dim Dom As DOMDocument
    Set Dom = New DOMDocument
    Dom.loadXML sResult
    Dom.Save Text12.Text
    Set Dom = Nothing
    Me.MousePointer = 0
End Sub

Private Sub Command5_Click()
    On Error Resume Next
    Me.MousePointer = 11
    Dim Exec As IAction
    Dim sResult As String
    Dim ValRes As Long
    Set Exec = CreateObject("lbawA_OVMQIII.lbaw_OVIIIConvProdOrg")
    ValRes = Exec.Execute(Text18.Text, sResult, "")
    Set Exec = Nothing
    Text17.Text = sResult
    Dim Dom As DOMDocument
    Set Dom = New DOMDocument
    Dom.loadXML sResult
    Dom.Save Text13.Text
    Set Dom = Nothing
    Me.MousePointer = 0
End Sub

Private Sub Command6_Click()
    On Error Resume Next
    Me.MousePointer = 11
    Dim Exec As IAction
    Dim sResult As String
    Dim ValRes As Long
    Set Exec = CreateObject("lbawA_OVMQIII.lbaw_OVIIIGetPreguntas")
    ValRes = Exec.Execute(Text9.Text, sResult, "")
    Set Exec = Nothing
    Me.Text14.Text = sResult
    Dim Dom As DOMDocument
    Set Dom = New DOMDocument
    Dom.loadXML sResult
    Dom.Save Text15.Text
    Set Dom = Nothing
    Me.MousePointer = 0
End Sub

Private Sub Command3_Click()
    On Error Resume Next
    Me.MousePointer = 11
    Dim Exec As IAction
    Dim sResult As String
    Dim ValRes As Long
    Set Exec = CreateObject("lbawA_OVMQIII.lbaw_OVIIIGetClausajus")
    ValRes = Exec.Execute(Text8.Text, sResult, "")
    Set Exec = Nothing
    Text7.Text = sResult
    Dim Dom As DOMDocument
    Set Dom = New DOMDocument
    Dom.loadXML sResult
    Dom.Save Text6.Text
    Set Dom = Nothing
    Me.MousePointer = 0
End Sub

