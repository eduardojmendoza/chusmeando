VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Test lbawA_OVMQIII"
   ClientHeight    =   8610
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   15240
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8610
   ScaleWidth      =   15240
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command9 
      Caption         =   "frmTest"
      Height          =   375
      Left            =   1680
      TabIndex        =   44
      Top             =   0
      Width           =   1215
   End
   Begin VB.CommandButton Command8 
      Caption         =   "Helper Zonas"
      Height          =   255
      Left            =   13560
      TabIndex        =   43
      Top             =   4080
      Width           =   1575
   End
   Begin VB.CommandButton Command7 
      Caption         =   "Form2"
      Height          =   375
      Left            =   6120
      TabIndex        =   42
      Top             =   0
      Width           =   1215
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Execute Convenio"
      Height          =   375
      Left            =   13560
      TabIndex        =   38
      Top             =   6720
      Width           =   1575
   End
   Begin VB.TextBox Text18 
      Height          =   375
      Left            =   7800
      ScrollBars      =   2  'Vertical
      TabIndex        =   37
      Text            =   $"Form1.frx":0000
      Top             =   6240
      Width           =   7335
   End
   Begin VB.TextBox Text17 
      Height          =   1335
      Left            =   7800
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   36
      Top             =   7200
      Width           =   7455
   End
   Begin VB.TextBox Text13 
      Height          =   285
      Left            =   8520
      TabIndex        =   35
      Text            =   "C:\Documents and Settings\Exramis\Escritorio\Convenios.xml"
      Top             =   6840
      Width           =   4935
   End
   Begin VB.TextBox Text12 
      Height          =   285
      Left            =   8520
      TabIndex        =   31
      Text            =   "C:\Documents and Settings\Exramis\Escritorio\Zonas.xml"
      Top             =   3960
      Width           =   4935
   End
   Begin VB.TextBox Text11 
      Height          =   1335
      Left            =   7800
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   30
      Top             =   4320
      Width           =   7455
   End
   Begin VB.TextBox Text10 
      Height          =   375
      Left            =   7800
      ScrollBars      =   2  'Vertical
      TabIndex        =   29
      Text            =   "<Request><USUARIO>EX009000L </USUARIO><RAMOPCOD>ICO1</RAMOPCOD></Request>"
      Top             =   3360
      Width           =   7335
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Execute Zonas"
      Height          =   255
      Left            =   13560
      TabIndex        =   28
      Top             =   3840
      Width           =   1575
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Execute Alarmas"
      Height          =   375
      Left            =   13560
      TabIndex        =   24
      Top             =   840
      Width           =   1575
   End
   Begin VB.TextBox Text8 
      Height          =   375
      Left            =   7800
      ScrollBars      =   2  'Vertical
      TabIndex        =   23
      Text            =   "<Request><USUARIO>EX009000L </USUARIO></Request>"
      Top             =   360
      Width           =   7335
   End
   Begin VB.TextBox Text7 
      Height          =   1455
      Left            =   7800
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   22
      Top             =   1320
      Width           =   7455
   End
   Begin VB.TextBox Text6 
      Height          =   285
      Left            =   8520
      TabIndex        =   21
      Text            =   "C:\Documents and Settings\Exramis\Escritorio\TiposAlarma.xml"
      Top             =   960
      Width           =   4935
   End
   Begin VB.TextBox Text4 
      Height          =   375
      Left            =   0
      ScrollBars      =   2  'Vertical
      TabIndex        =   13
      Text            =   "<Request><USUARIO>EX009000L </USUARIO><RAMOPCOD>ICO1</RAMOPCOD></Request>"
      Top             =   6240
      Width           =   7575
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Execute Periodos"
      Height          =   375
      Left            =   5880
      TabIndex        =   12
      Top             =   6720
      Width           =   1695
   End
   Begin VB.TextBox Text3 
      Height          =   1335
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   11
      Top             =   7200
      Width           =   7575
   End
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   840
      TabIndex        =   10
      Text            =   "C:\Documents and Settings\Exramis\Escritorio\Periodos.xml"
      Top             =   6840
      Width           =   4815
   End
   Begin VB.TextBox Text16 
      Height          =   285
      Left            =   840
      TabIndex        =   9
      Text            =   "C:\Documents and Settings\Exramis\Escritorio\Condiciones.xml"
      Top             =   3960
      Width           =   4815
   End
   Begin VB.TextBox Text15 
      Height          =   285
      Left            =   840
      TabIndex        =   8
      Text            =   "C:\Documents and Settings\Exramis\Escritorio\Actividades.xml"
      Top             =   960
      Width           =   4815
   End
   Begin VB.TextBox Text14 
      Height          =   1335
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   6
      Top             =   4320
      Width           =   7575
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Execute Condiciones"
      Height          =   375
      Left            =   5880
      TabIndex        =   5
      Top             =   3840
      Width           =   1695
   End
   Begin VB.TextBox Text9 
      Height          =   375
      Left            =   0
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Text            =   "<Request><USUARIO>EX009000L </USUARIO><ACTIVIDAD>050004</ACTIVIDAD><RAMOPCOD>ICO1</RAMOPCOD></Request>"
      Top             =   3360
      Width           =   7575
   End
   Begin VB.TextBox Text5 
      Height          =   1455
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Top             =   1320
      Width           =   7575
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   0
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Text            =   "<Request><USUARIO>EX009000L </USUARIO><FILTROACTIVIDAD>REST</FILTROACTIVIDAD><RAMOPCOD>ICO1</RAMOPCOD></Request>"
      Top             =   360
      Width           =   7575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Execute Actividades"
      Height          =   375
      Left            =   5880
      TabIndex        =   0
      Top             =   840
      Width           =   1695
   End
   Begin VB.Label Label18 
      Caption         =   "XML Out"
      Height          =   255
      Left            =   7680
      TabIndex        =   41
      Top             =   6840
      Width           =   1575
   End
   Begin VB.Line Line5 
      BorderWidth     =   4
      X1              =   8520
      X2              =   14760
      Y1              =   9000
      Y2              =   9000
   End
   Begin VB.Label Label17 
      AutoSize        =   -1  'True
      Caption         =   "XML In"
      Height          =   195
      Left            =   7800
      TabIndex        =   40
      Top             =   6000
      Width           =   510
   End
   Begin VB.Label Label16 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Convenios Organizador Productor"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   9195
      TabIndex        =   39
      Top             =   5880
      Width           =   4065
   End
   Begin VB.Label Label15 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Zonas Geograficas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   10065
      TabIndex        =   34
      Top             =   3000
      Width           =   2325
   End
   Begin VB.Label Label14 
      AutoSize        =   -1  'True
      Caption         =   "XML In"
      Height          =   195
      Left            =   7800
      TabIndex        =   33
      Top             =   3120
      Width           =   510
   End
   Begin VB.Line Line4 
      BorderWidth     =   4
      X1              =   8520
      X2              =   14760
      Y1              =   5760
      Y2              =   5760
   End
   Begin VB.Label Label13 
      Caption         =   "XML Out"
      Height          =   255
      Left            =   7680
      TabIndex        =   32
      Top             =   3960
      Width           =   1575
   End
   Begin VB.Label Label12 
      Caption         =   "XML Out"
      Height          =   255
      Left            =   7680
      TabIndex        =   27
      Top             =   960
      Width           =   1575
   End
   Begin VB.Line Line3 
      BorderWidth     =   4
      X1              =   8520
      X2              =   14760
      Y1              =   2880
      Y2              =   2880
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      Caption         =   "XML In"
      Height          =   195
      Left            =   7800
      TabIndex        =   26
      Top             =   120
      Width           =   510
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Tipos de Alarma / Guardia Ver Codigo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   8940
      TabIndex        =   25
      Top             =   0
      Width           =   4575
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Actividades"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3195
      TabIndex        =   20
      Top             =   0
      Width           =   1425
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "XML In"
      Height          =   195
      Left            =   0
      TabIndex        =   19
      Top             =   120
      Width           =   510
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Condiciones de Cobertura"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   2325
      TabIndex        =   18
      Top             =   3000
      Width           =   3165
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "XML In"
      Height          =   195
      Left            =   0
      TabIndex        =   17
      Top             =   3120
      Width           =   510
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Periodos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3360
      TabIndex        =   16
      Top             =   5880
      Width           =   1095
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "XML In"
      Height          =   195
      Left            =   0
      TabIndex        =   15
      Top             =   6000
      Width           =   510
   End
   Begin VB.Line Line2 
      BorderWidth     =   4
      X1              =   720
      X2              =   6960
      Y1              =   5760
      Y2              =   5760
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "XML Out"
      Height          =   195
      Left            =   0
      TabIndex        =   14
      Top             =   6960
      Width           =   630
   End
   Begin VB.Label Label10 
      Caption         =   "XML Out"
      Height          =   495
      Left            =   0
      TabIndex        =   7
      Top             =   4080
      Width           =   1575
   End
   Begin VB.Line Line1 
      BorderWidth     =   4
      X1              =   720
      X2              =   6960
      Y1              =   2880
      Y2              =   2880
   End
   Begin VB.Label Label6 
      Caption         =   "XML Out"
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   960
      Width           =   1575
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()
    On Error Resume Next
    Me.MousePointer = 11
    Dim Exec As IAction
    Dim sResult As String
    Dim ValRes As Long
    Set Exec = CreateObject("lbawA_OVMQIII.lbaw_OVIIIActividades")
    ValRes = Exec.Execute(Text1.Text, sResult, "")
    Set Exec = Nothing
    Me.Text5.Text = sResult
    Dim Dom As DOMDocument
    Set Dom = New DOMDocument
    Dom.loadXML sResult
    Dom.Save Text15.Text
    Set Dom = Nothing
    Me.MousePointer = 0
End Sub

Private Sub Command2_Click()
    On Error Resume Next
    Me.MousePointer = 11
    Dim Exec As IAction
    Dim sResult As String
    Dim ValRes As Long
    Set Exec = CreateObject("lbawA_OVMQIII.lbaw_OVIIIPeriodCober")
    ValRes = Exec.Execute(Text4.Text, sResult, "")
    Set Exec = Nothing
    Text3.Text = sResult
    Dim Dom As DOMDocument
    Set Dom = New DOMDocument
    Dom.loadXML sResult
    Dom.Save Text2.Text
    Set Dom = Nothing
    Me.MousePointer = 0
End Sub

Private Sub Command4_Click()
    On Error Resume Next
    Me.MousePointer = 11
    Dim Exec As IAction
    Dim sResult As String
    Dim ValRes As Long
    Set Exec = CreateObject("lbawA_OVMQIII.lbaw_OVIIIZonasGeograf")
    ValRes = Exec.Execute(Text10.Text, sResult, "")
    Set Exec = Nothing
    Text11.Text = sResult
    Dim Dom As DOMDocument
    Set Dom = New DOMDocument
    Dom.loadXML sResult
    Dom.Save Text12.Text
    Set Dom = Nothing
    Me.MousePointer = 0
End Sub

Private Sub Command5_Click()
    On Error Resume Next
    Me.MousePointer = 11
    Dim Exec As IAction
    Dim sResult As String
    Dim ValRes As Long
    Set Exec = CreateObject("lbawA_OVMQIII.lbaw_OVIIIConvProdOrg")
    ValRes = Exec.Execute(Text18.Text, sResult, "")
    Set Exec = Nothing
    Text17.Text = sResult
    Dim Dom As DOMDocument
    Set Dom = New DOMDocument
    Dom.loadXML sResult
    Dom.Save Text13.Text
    Set Dom = Nothing
    Me.MousePointer = 0
End Sub

Private Sub Command6_Click()
    On Error Resume Next
    Me.MousePointer = 11
    Dim Exec As IAction
    Dim sResult As String
    Dim ValRes As Long
    Set Exec = CreateObject("lbawA_OVMQIII.lbaw_OVIIICondCober")
    ValRes = Exec.Execute(Text9.Text, sResult, "")
    Set Exec = Nothing
    Me.Text14.Text = sResult
    Dim Dom As DOMDocument
    Set Dom = New DOMDocument
    Dom.loadXML sResult
    Dom.Save Text15.Text
    Set Dom = Nothing
    Me.MousePointer = 0
End Sub

Private Sub Command3_Click()
    On Error Resume Next
    Me.MousePointer = 11
    Dim Exec As IAction
    Dim sResult As String
    Dim ValRes As Long
    Set Exec = CreateObject("lbawA_OVMQIII.lbaw_OVIIITiposGuardia")
    'Set Exec = CreateObject("lbawA_OVMQIII.lbaw_OVIIITiposAlarma")
    ValRes = Exec.Execute(Text8.Text, sResult, "")
    Set Exec = Nothing
    Text7.Text = sResult
    Dim Dom As DOMDocument
    Set Dom = New DOMDocument
    Dom.loadXML sResult
    Dom.Save Text6.Text
    Set Dom = Nothing
    Me.MousePointer = 0
End Sub

Private Sub Command7_Click()
    Form2.Show
End Sub

Private Sub Command8_Click()
    On Error Resume Next
    Me.MousePointer = 11
    Dim Exec As IAction
    Dim sResult As String
    Dim ValRes As Long
    Set Exec = CreateObject("lbawA_OVMQIII.lbaw_OVIIIHelperZonas")
    ValRes = Exec.Execute(Text10.Text, sResult, "")
    Set Exec = Nothing
    Text11.Text = sResult
    Dim Dom As DOMDocument
    Set Dom = New DOMDocument
    Dom.loadXML sResult
    Dom.Save Text12.Text
    Set Dom = Nothing
    Me.MousePointer = 0
End Sub

Private Sub Command9_Click()
    frmTest.Show , Me
End Sub
