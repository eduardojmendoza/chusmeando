VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVIIIGetDeducib"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context             As ObjectContext
Private mobjEventLog                As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbawA_OVMQIII.lbaw_OVIIIGetDeducib"
Const mcteOpID                      As String = "1530"

'Parametros XML de Entrada
Const mcteParam_Usuario             As String = "//USUARIO"
Const mcteParam_Codigo_Ramo         As String = "//RAMOPCOD"
Const mcteParam_Comercio            As String = "//COMERCIO"
Const mcteParam_Codigo_Profesion    As String = "//I_ACTIVIDAD"
Const mcteParam_Zona                As String = "//I_ZONA"
Const mcteParam_ModuloActual        As String = "//I_CODIGOMODULO"
'Const mcteParam_Codigo_Profesion    As String = "//ACTIVIDAD"
'Const mcteParam_Zona                As String = "//ZONA"
'Const mcteParam_ModuloActual        As String = "//MODULO"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarModuloActual    As String
    Dim wvarCodProfesion    As String
    Dim wvarCodRamo         As String
    Dim wvarCodZona         As String
    Dim wvarAreaToSend      As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    wobjXMLRequest.loadXML Request
    wvarUsuario = Left(wobjXMLRequest.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
    wvarCodRamo = Left(wobjXMLRequest.selectSingleNode(mcteParam_Codigo_Ramo).Text & Space(4), 4)
    wvarCodProfesion = Left(wobjXMLRequest.selectSingleNode(mcteParam_Codigo_Profesion).Text & Space(6), 6)
    wvarCodZona = Right("0000" & wobjXMLRequest.selectSingleNode(mcteParam_Zona).Text, 4)
    '
    wvarAreaToSend = wvarCodRamo & wvarCodProfesion & wvarCodZona
    wvarAreaToSend = wvarAreaToSend & GetVectorCoberturas(wobjXMLRequest.selectSingleNode("."), mcteParam_Comercio, 30)
    '
    wvarStep = 60
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 70
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 100
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 110
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 120
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarAreaToSend
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 240
    '
parsear:
        wvarstrLen = Len(strParseString)
        '
        wvarStep = 250
        wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
        '
        If wvarEstado = "ER" Then
            '
            wvarStep = 260
            Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
            '
        Else
            '
            wvarStep = 280
            wvarResult = ""
            wvarPos = 234   'cantidad de caracteres ocupados por parámetros de entrada
            wvarResult = ParseoMensaje(wvarPos, strParseString, wvarstrLen)
            '
            wvarStep = 340
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
            '
        End If
    '
    
    wvarStep = 360
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarAreaToSend & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(pvarPos As Long, pstrParseString As String, pvarstrLen As Long) As String
Dim wvarResult          As String
'
Dim wobjRegExp          As Object 'RegExp
Dim wobjColMatch        As Object 'MatchCollection
Dim wobjMatch           As Object 'Match
Dim wvarParseEvalString As String
'
Dim wobjXmlDOMResp      As DOMDocument
Dim wobjXmlElemenResp   As IXMLDOMElement
Dim wobjXmlElemenRespE  As IXMLDOMElement
Dim wvarCantRegistros   As Long
Dim wvarCcurrRegistro   As Long
'
    Set wobjRegExp = CreateObject("VbScript.RegExp")
    wobjRegExp.Global = True
    '
    
    wobjRegExp.Pattern = "(\S{3})(\S{2})(\S{60})"
    '
    wvarParseEvalString = Mid(pstrParseString, pvarPos + 3)
    wvarParseEvalString = Replace(wvarParseEvalString, " ", "_")
    wvarCantRegistros = Val(Mid(pstrParseString, pvarPos, 3))
    wvarCcurrRegistro = 0
    
    Set wobjColMatch = wobjRegExp.Execute(wvarParseEvalString)
    '
    Set wobjXmlDOMResp = CreateObject("msxml2.DomDocument")
    
    Call wobjXmlDOMResp.loadXML("<DEDUCIBLES></DEDUCIBLES>")
    '
    For Each wobjMatch In wobjColMatch
    '
        If Not wvarCcurrRegistro < wvarCantRegistros Then Exit For
        wvarCcurrRegistro = wvarCcurrRegistro + 1
        '
        Set wobjXmlElemenResp = wobjXmlDOMResp.createElement("DEDUCIBLE")
        wobjXmlDOMResp.selectSingleNode("//DEDUCIBLES").appendChild wobjXmlElemenResp
        '
        Set wobjXmlElemenRespE = wobjXmlDOMResp.createElement("CODIGOCOBERTURA")
        wobjXmlElemenResp.appendChild wobjXmlElemenRespE
        wobjXmlElemenRespE.Text = UCase(Trim(Replace(wobjMatch.SubMatches(0), "_", " ")))
        '
        Set wobjXmlElemenRespE = wobjXmlDOMResp.createElement("CODIGODEDUCIBLE")
        wobjXmlElemenResp.appendChild wobjXmlElemenRespE
        wobjXmlElemenRespE.Text = Trim(Replace(wobjMatch.SubMatches(1), "_", " "))
        '
        Set wobjXmlElemenRespE = wobjXmlDOMResp.createElement("DESCRIPCION")
        wobjXmlElemenResp.appendChild wobjXmlElemenRespE
        wobjXmlElemenRespE.Text = Trim(Replace(wobjMatch.SubMatches(2), "_", " "))
    '
    Next wobjMatch
    '
    ParseoMensaje = wobjXmlDOMResp.xml
    '
    Set wobjXmlDOMResp = Nothing
    Set wobjXmlElemenResp = Nothing
    Set wobjXmlElemenRespE = Nothing
    '
    Set wobjRegExp = Nothing
    '
End Function

Private Function GetVectorCoberturas(pobjNodo As IXMLDOMNode, pVarPathVector As String, pVarCantElementos As Long) As String
    Dim wvarCounterRelleno As Long
    Dim wvarActualCounter As Long
    Dim wobjNodosComercios  As IXMLDOMNodeList
    Dim wobjNodoComercio    As IXMLDOMNode
    Dim wobjNodosCobertura  As IXMLDOMNodeList
    Dim wobjNodoCobertura   As IXMLDOMNode
    
    Set wobjNodosComercios = pobjNodo.selectNodes(pVarPathVector)
    wvarActualCounter = 0
    For Each wobjNodoComercio In wobjNodosComercios
        Set wobjNodosCobertura = wobjNodoComercio.childNodes
        For Each wobjNodoCobertura In wobjNodosCobertura
            If UCase(wobjNodoCobertura.nodeName) Like "C*" Then
                If Val(wobjNodoCobertura.Text) > 0 Then
                    'Cobertura con Valor
                    GetVectorCoberturas = GetVectorCoberturas & Right("000" & Mid(wobjNodoCobertura.nodeName, 2), 3)
                    GetVectorCoberturas = GetVectorCoberturas & Right("00" & pobjNodo.selectSingleNode(mcteParam_ModuloActual).Text, 2)
                    wvarActualCounter = wvarActualCounter + 1
                    If wvarActualCounter = pVarCantElementos - 1 Then GoTo ClearObjects:
                End If
            End If
        Next
    Next
    
    For wvarCounterRelleno = wvarActualCounter To pVarCantElementos - 1
        GetVectorCoberturas = GetVectorCoberturas & "000"
    Next
    GetVectorCoberturas = Format(wvarActualCounter, "#000") & GetVectorCoberturas
    
ClearObjects:
    Set wobjNodosComercios = Nothing
    Set wobjNodoComercio = Nothing
    Set wobjNodosCobertura = Nothing
    Set wobjNodoCobertura = Nothing
End Function
