package com.qbe.services.lbavirtualmqiii;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.lbavirtualmqiii.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVIIIGetPreguntas implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQIII.lbaw_OVIIIGetPreguntas";
  static final String mcteOpID = "1560";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_Codigo_Ramo = "//RAMOPCOD";
  static final String mcteParam_Codigo_Actividad = "//ACTIVIDAD";
  static final String mcteParam_Codigo_Zona = "//ZONA";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarCodRamo = "";
    String wvarCodActividad = "";
    String wvarCodZona = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //Deber� venir desde la p�gina
      wvarUsuario = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarCodRamo = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Ramo )  ) + Strings.space( 4 ), 4 );
      wvarCodActividad = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Actividad )  ) + Strings.space( 6 ), 6 );
      wvarCodZona = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Zona )  ), 4 );
      //
      //
      wvarStep = 60;
      wobjXMLRequest = null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      //
      wvarStep = 110;
      wobjXMLParametros = null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarCodRamo + wvarCodActividad + wvarCodZona;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);
      wvarMQError = wobjFrame2MQ.Execute( wvarArea, strParseString, XmlDomExtended.marshal(wobjXMLConfig.selectSingleNode( "//MQCONFIG" )) ).toInt();
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      //
      wvarStep = 240;
      //
      parsear: 
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = "";
        //cantidad de caracteres ocupados por par�metros de entrada
        wvarPos = 81;
        wvarResult = ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        //
        wvarStep = 340;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
        //
      }
      //
      wvarStep = 360;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCodRamo + wvarCodActividad + wvarCodZona + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int pvarPos, String pstrParseString, int pvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    VbScript_RegExp wobjRegExp = new VbScript_RegExp();
    MatchCollection wobjColMatch;
    Match wobjMatch = null;
    VbScript_RegExp wobjRegSubexp = new VbScript_RegExp();
    Object wobjColSubMatch = null;
    Object wobjsubMatch = null;
    String wvarParseEvalString = "";
    int i = 0;
    XmlDomExtended wobjXmlDOMResp = null;
    org.w3c.dom.Element wobjXmlElemenResp = null;
    org.w3c.dom.Element wobjXmlElemenRespV = null;
    org.w3c.dom.Element wobjXmlElemenRespC = null;
    org.w3c.dom.Element wobjXmlElemenRespCe = null;
    org.w3c.dom.Element wobjXmlElemenRespVe = null;
    org.w3c.dom.Element wobjXmlElemenRespPr = null;
    org.w3c.dom.Element wobjXmlElemenValPos = null;
    org.w3c.dom.Element wobjXmlElemenValor = null;
    org.w3c.dom.Attr wobjXmlAtributo = null;
    int wvarCantRegistros = 0;
    int wvarCcurrRegistro = 0;
    String wvarCobertura = "";
    String wvarCodPregunta = "";
    int wvarCantPreguntas = 0;
    int wvarCcurrPregunta = 0;
    int wvarCantLeyendas = 0;
    //
    //RegExp
    //MatchCollection
    //Match
    //RegExp
    //MatchCollection
    //Match
    //
    //
    wobjRegExp = new VbScript_RegExp();
    wobjRegExp.setGlobal( true ); 

    wobjRegSubexp = new VbScript_RegExp();
    wobjRegSubexp.Global.set( true );

    //
    wobjRegExp.setPattern( "(\\S{3})(\\S{3})(\\S{1080})" );
    wobjRegSubexp.Pattern.set( "(\\S{4})(\\S{4})(\\S{3})(\\S{124})" );

    //
    wvarParseEvalString = Strings.mid( pstrParseString, pvarPos + 3 );
    wvarParseEvalString = Strings.replace( wvarParseEvalString, " ", "_" );
    wvarCantRegistros = (int)Math.rint( VB.val( Strings.mid( pstrParseString, pvarPos, 3 ) ) );
    wvarCcurrRegistro = 0;

    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    wobjXmlDOMResp = new XmlDomExtended();

    wobjXmlDOMResp.loadXML( "<preguntas></preguntas>" );

    //
    Label249: 
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //
      if( ! (wvarCcurrRegistro < wvarCantRegistros) )
      {
        break;
      }
      wvarCcurrRegistro = wvarCcurrRegistro + 1;
      //
      //error: function 'SubMatches' was not found.
      //unsup: wvarCobertura = Val(Strings.replace(wobjMatch.SubMatches(0), "_", " "))
      //error: function 'SubMatches' was not found.
      //unsup: wvarCantPreguntas = Val(Strings.replace(wobjMatch.SubMatches(1), "_", " "))
      wvarCcurrPregunta = 0;

      //error: function 'SubMatches' was not found.
      //unsup: Set wobjColSubMatch = wobjRegSubexp.Execute(wobjMatch.SubMatches(2))
      for( int nwobjsubMatch = 0; nwobjsubMatch < wobjColSubMatch.getCount(); nwobjsubMatch++ )
      {
        //
        if( ! (wvarCcurrPregunta < wvarCantPreguntas) )
        {
          break;
        }
        wvarCcurrPregunta = wvarCcurrPregunta + 1;
        //
        //error: function 'SubMatches' was not found.
        //unsup: wvarCodPregunta = Val(Strings.replace(wobjsubMatch.SubMatches(1), "_", " "))
        wobjXmlElemenRespPr = (org.w3c.dom.Element) wobjXmlDOMResp.selectSingleNode( "//preguntas/pregunta[@Codigo=" + wvarCodPregunta + "]" ) ;
        //
        if( wobjXmlElemenRespPr == (org.w3c.dom.Element) null )
        {
          //Si no existe el Modulo lo genero
          wobjXmlElemenRespPr = wobjXmlDOMResp.getDocument().createElement( "pregunta" );
          /*unsup wobjXmlDOMResp.selectSingleNode( "//preguntas" ) */.appendChild( wobjXmlElemenRespPr );

          wobjXmlElemenValPos = wobjXmlDOMResp.getDocument().createElement( "valoresposibles" );
          wobjXmlElemenRespPr.appendChild( wobjXmlElemenValPos );

          //Estas preguntas son del tipo SI-NO, genero los posibles valores desde aca
          wobjXmlElemenValor = wobjXmlDOMResp.getDocument().createElement( "OPTION" );
          XmlDomExtended.setText( wobjXmlElemenValor, "NO" );
          wobjXmlElemenValPos.appendChild( wobjXmlElemenValor );
          wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "value" );
          wobjXmlElemenValor.setAttributeNode( wobjXmlAtributo );
          XmlDomExtended.setText( wobjXmlAtributo, "N" );

          wobjXmlElemenValor = wobjXmlDOMResp.getDocument().createElement( "OPTION" );
          XmlDomExtended.setText( wobjXmlElemenValor, "SI" );
          wobjXmlElemenValPos.appendChild( wobjXmlElemenValor );
          wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "value" );
          wobjXmlElemenValor.setAttributeNode( wobjXmlAtributo );
          XmlDomExtended.setText( wobjXmlAtributo, "S" );

          wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Codigo" );
          wobjXmlElemenRespPr.setAttributeNode( wobjXmlAtributo );
          XmlDomExtended.setText( wobjXmlAtributo, wvarCodPregunta );

          wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "OrdenEnPantalla" );
          wobjXmlElemenRespPr.setAttributeNode( wobjXmlAtributo );
          //error: function 'SubMatches' was not found.
          //unsup: wobjXmlAtributo.Text = Val(Strings.replace(wobjsubMatch.SubMatches(0), "_", " "))
          wobjXmlElemenRespV = wobjXmlDOMResp.getDocument().createElement( "leyendas" );
          wobjXmlElemenRespPr.appendChild( wobjXmlElemenRespV );
          //
          //error: function 'SubMatches' was not found.
          //unsup: wvarCantLeyendas = Val(Strings.replace(wobjsubMatch.SubMatches(2), "_", " "))
          for( i = 0; i <= (wvarCantLeyendas - 1); i++ )
          {
            wobjXmlElemenRespVe = wobjXmlDOMResp.getDocument().createElement( "leyenda" );
            wobjXmlElemenRespV.appendChild( wobjXmlElemenRespVe );

            wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Item" );
            //error: function 'SubMatches' was not found.
            //unsup: wobjXmlAtributo.Text = Mid(Strings.replace(wobjsubMatch.SubMatches(3), "_", " "), (i * 62) + 1, 2)
            wobjXmlElemenRespVe.setAttributeNode( wobjXmlAtributo );

            wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Descripcion" );
            //error: function 'SubMatches' was not found.
            //unsup: wobjXmlAtributo.Text = Trim(Mid(Strings.replace(wobjsubMatch.SubMatches(3), "_", " "), (i * 62) + 1 + 2, 60))
            wobjXmlElemenRespVe.setAttributeNode( wobjXmlAtributo );

          }

          wobjXmlElemenRespC = wobjXmlDOMResp.getDocument().createElement( "coberturasactivantes" );
          wobjXmlElemenRespPr.appendChild( wobjXmlElemenRespC );

        }

        wobjXmlElemenRespCe = wobjXmlDOMResp.getDocument().createElement( "cobertura" );
        /*unsup wobjXmlDOMResp.selectSingleNode( "//preguntas/pregunta[@Codigo=" + wvarCodPregunta + "]/coberturasactivantes" ) */.appendChild( wobjXmlElemenRespCe );
        XmlDomExtended.setText( wobjXmlElemenRespCe, wvarCobertura );
        //
      }
      //
    }
    //
    ParseoMensaje = XmlDomExtended.marshal(wobjXmlDOMResp.getDocument().getDocumentElement());
    //
    wobjXmlElemenValor = (org.w3c.dom.Element) null;
    wobjXmlElemenValPos = (org.w3c.dom.Element) null;
    wobjXmlElemenResp = (org.w3c.dom.Element) null;
    wobjXmlElemenRespV = (org.w3c.dom.Element) null;
    wobjXmlElemenRespC = (org.w3c.dom.Element) null;
    wobjXmlElemenRespCe = (org.w3c.dom.Element) null;
    wobjXmlElemenRespVe = (org.w3c.dom.Element) null;
    wobjXmlElemenRespPr = (org.w3c.dom.Element) null;
    //
    wobjRegExp = (VbScript_RegExp) null;
    wobjRegSubexp = (VbScript_RegExp) null;
    //
    return ParseoMensaje;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
