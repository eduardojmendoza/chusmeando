VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "lbaw_OVIIIGetIIBZona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context             As ObjectContext
Private mobjEventLog                As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbawA_OVMQIII.lbaw_OVIIIGetIIBZona"

'Parametros XML de Entrada que se necesitan para los mensajes que consulta
Const mcteParam_PAISSCOD         As String = "//PAISSCOD"
Const mcteParam_V_PROVICOD       As String = "//V_PROVICOD"
Const mcteParam_RAMOPCOD         As String = "//RAMOPCOD"
Const mcteParam_CLIENIVA         As String = "//CLIENIVA"
Const mcteParam_LISTA            As String = "//LISTA"
Const mcteParam_CodIB            As String = "//NUMERO"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjClass           As IAction
    Dim wvarStep            As Long
    Dim wvarResponse        As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los par�metros que llegan desde la p�gina
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    '
    '
    wobjXMLRequest.async = False
    Call wobjXMLRequest.loadXML(Request)
    '
    '
    wvarStep = 60
    '
    '*******************************************************
    'Consulto primero si la provincia aplica Ingresos Brutos
    '*******************************************************
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OfVirtualLBA.lbaw_GetZonaIngBrut")
    wobjClass.Execute Request, wvarResponse, ""
    Set wobjClass = Nothing
    '
    wvarStep = 70
    wobjXMLResponse.loadXML wvarResponse
    '
    wvarStep = 80
    If wobjXMLResponse.selectSingleNode("//PROVICOD") Is Nothing Then
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje='No Existen Ingresos Brutos para la provincia seleccionada'/></Response>"
    Else
        '*******************************************************
        'Consulto los Ingresos Brutos
        '*******************************************************
        Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OfVirtualLBA.lbaw_GetTipIngBrut")
        wobjClass.Execute Request, wvarResponse, ""
        '
        wvarStep = 100
        Set wobjClass = Nothing
        Response = wvarResponse
    End If
    '
    wvarStep = 120
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
Set wobjXMLRequest = Nothing
Set wobjClass = Nothing
Set wobjXMLResponse = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
