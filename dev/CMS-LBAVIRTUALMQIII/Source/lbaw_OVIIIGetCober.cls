VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVIIIGetCober"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context             As ObjectContext
Private mobjEventLog                As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbawA_OVMQIII.lbaw_OVIIIGetCober"
Const mcteOpID                      As String = "1559"

'Parametros XML de Entrada
Const mcteParam_Usuario             As String = "//USUARIO"
Const mcteParam_Codigo_Ramo         As String = "//RAMOPCOD"
Const mcteParam_Codigo_Actividad    As String = "//ACTIVIDAD"
Const mcteParam_Codigo_Zona         As String = "//ZONA"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarCodRamo         As String
    Dim wvarCodActividad    As String
    Dim wvarCodZona         As String
    Dim wvarAction          As IAction
    
    Dim wvarNodoPreg        As IXMLDOMNode
    Dim wvarNodoTAlarma     As IXMLDOMNode
    Dim wvarNodoTGuardia    As IXMLDOMNode
    Dim wobjXmlDOMAdic      As DOMDocument
    Dim wvarRespPreguntas   As String
    Dim wvarRespTiposAlarma As String
    Dim wvarRespTipGuardia  As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarCodRamo = Left(.selectSingleNode(mcteParam_Codigo_Ramo).Text & Space(4), 4)
        wvarCodActividad = Left(.selectSingleNode(mcteParam_Codigo_Actividad).Text & Space(6), 6)
        wvarCodZona = Right("0000" & .selectSingleNode(mcteParam_Codigo_Zona).Text, 4)
        '
    End With
    '
    wvarStep = 60
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 70
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 100
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 110
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 120
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarCodRamo & wvarCodActividad & wvarCodZona
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 240
    '
parsear:
        wvarstrLen = Len(strParseString)
        '
        wvarStep = 250
        wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
        '
        If wvarEstado = "ER" Then
            '
            wvarStep = 260
            Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
            '
        Else
            '
            
            wvarResult = ""
            Set wobjXmlDOMAdic = CreateObject("msxml2.DomDocument")
            'Realizo la consulta de preguntas para anexarlas al XML de la cotizacion
            wvarStep = 280
            Set wvarAction = CreateObject("lbawA_OVMQIII.lbaw_OVIIIGetPreguntas")
            '
            wvarStep = 290
            wvarAction.Execute Request, wvarRespPreguntas, ContextInfo
            wobjXmlDOMAdic.loadXML wvarRespPreguntas
            '
            wvarStep = 300
            Set wvarNodoPreg = wobjXmlDOMAdic.selectSingleNode("//preguntas")
            '
            wvarStep = 310
            Set wvarAction = Nothing
            
            'Realizo la consulta de Tipos de alarma para anexarlas al XML de la cotizacion
            Set wvarAction = CreateObject("lbawA_OVMQIII.lbaw_OVIIITiposAlarma")
            '
            wvarStep = 320
            wvarAction.Execute Request, wvarRespTiposAlarma, ContextInfo
            wobjXmlDOMAdic.loadXML wvarRespTiposAlarma
            '
            wvarStep = 330
            Set wvarNodoTAlarma = wobjXmlDOMAdic.selectSingleNode("//tiposalarma")
            Set wvarAction = Nothing
            
            'Realizo la consulta de Tipos de Guardia para anexarlas al XML de la cotizacion
            Set wvarAction = CreateObject("lbawA_OVMQIII.lbaw_OVIIITiposGuardia")
            wvarAction.Execute Request, wvarRespTipGuardia, ContextInfo
            '
            wvarStep = 340
            wobjXmlDOMAdic.loadXML wvarRespTipGuardia
            Set wvarNodoTGuardia = wobjXmlDOMAdic.selectSingleNode("//tiposguardia")
            Set wvarAction = Nothing
            '
            wvarStep = 350
            wvarPos = 81  'cantidad de caracteres ocupados por parámetros de entrada
            wvarResult = ParseoMensaje(wvarPos, strParseString, wvarstrLen, wvarNodoPreg, wvarNodoTAlarma, wvarNodoTGuardia, wvarCodActividad)
            '
            wvarStep = 360
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
            '
        End If
    '
    
    wvarStep = 370
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCodRamo & wvarCodActividad & wvarCodZona & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(pvarPos As Long, pstrParseString As String, pvarstrLen As Long, pvarPreguntas As IXMLDOMNode, pvarTiposAlarma As IXMLDOMNode, pvarTiposGuardia As IXMLDOMNode, pvarCodigoActividad As String) As String
Dim wvarResult          As String
'
Dim wobjRegExp          As Object 'RegExp
Dim wobjColMatch        As Object 'MatchCollection
Dim wobjMatch           As Object 'Match
Dim wobjRegSubexp       As Object 'RegExp
Dim wobjColSubMatch     As Object 'MatchCollection
Dim wobjsubMatch        As Object 'Match
Dim wvarParseEvalString As String
Dim i                   As Long
'
Dim wobjXmlDOMResp      As DOMDocument
Dim wobjXmlElemenResp   As IXMLDOMElement
Dim wobjXmlElemenRespE  As IXMLDOMElement
Dim wobjXmlElemenRespEe As IXMLDOMElement
Dim wobjXmlElemenRespG  As IXMLDOMElement
Dim wobjXmlElemenRespD  As IXMLDOMElement
Dim wobjXmlElemenRespC  As IXMLDOMElement
Dim wobjXmlElemenRespMo As IXMLDOMElement
Dim wobjXmlAtributo     As IXMLDOMAttribute
Dim wvarCantRegistros   As Long
Dim wvarCcurrRegistro   As Long
Dim wvarCantModulos     As Long
Dim wvarCantExclusiones As Long
Dim wvarCcurrModulo     As Long
Dim wvarCoberturaPr     As String
Dim wvarCobertura       As String
Dim wvarCodModulo       As String
Dim wvarRequiereCantidad As String '31/07/2006. Agregado para parametrizar si es o no requerido el campo CANTIDAD de acuerdo a LNK-SWALUMNO
Dim wobjNodosCoberturas As IXMLDOMNodeList
Dim wobjNodoCobertura   As IXMLDOMElement
Dim wvarActivModulariz  As Boolean
'
    Set wobjRegExp = CreateObject("VbScript.RegExp")
    wobjRegExp.Global = True
    
    Set wobjRegSubexp = CreateObject("VbScript.RegExp")
    wobjRegSubexp.Global = True

    '
    
    wobjRegExp.Pattern = "(\S{3})(\S{2})(\S{1})(\S{60})(\S{1})(\S{3})(\S{1})(\S{1})(\S{3})(\S{360})(\S{3})(\S{30})(\S{1})"
    wobjRegSubexp.Pattern = "(\S{2})(\S{11})(\S{11})(\S{9})(\S{1})(\S{2})"
    
    '
    wvarParseEvalString = Mid(pstrParseString, pvarPos + 5) '+6
    wvarParseEvalString = Replace(wvarParseEvalString, " ", "_")
    ' ###########################################################################################
    ' FJO - 31/07/2006
    'Reemplazar la siguiente asignación por la que está comentada cuando modifiquen el mensaje 1559,
    ' para que incluya la información requerida.
    'wvarCantRegistros = Val(Mid(pstrParseString, pvarPos + 1, 3))
    wvarCantRegistros = Val(Mid(pstrParseString, pvarPos + 2, 3)) '31/07/2006
    ' ###########################################################################################
    wvarCcurrRegistro = 0
    
    Set wobjColMatch = wobjRegExp.Execute(wvarParseEvalString)
    '
    Set wobjXmlDOMResp = CreateObject("msxml2.DomDocument")
    
    Call wobjXmlDOMResp.loadXML("<cotizacion></cotizacion>")
    
    'Genero el Nodo GENERAL
    Set wobjXmlElemenRespG = wobjXmlDOMResp.createElement("general")
    wobjXmlDOMResp.selectSingleNode("//cotizacion").appendChild wobjXmlElemenRespG
    
    Set wobjXmlElemenResp = wobjXmlDOMResp.createElement("coberturasdeterminantes")
    wobjXmlDOMResp.selectSingleNode("//general").appendChild wobjXmlElemenResp
    
    'Creo el elemento Cantidad Minima de Coberturas
    Set wobjXmlElemenResp = wobjXmlDOMResp.createElement("CantidadMinimaCoberturas")
    wobjXmlDOMResp.selectSingleNode("//general").appendChild wobjXmlElemenResp
    wobjXmlElemenResp.Text = Val(Mid(pstrParseString, pvarPos, 1)) '+3
    
    'Creo el elemento Codigo de Actividad
    Set wobjXmlElemenResp = wobjXmlDOMResp.createElement("CodigoActividad")
    wobjXmlDOMResp.selectSingleNode("//general").appendChild wobjXmlElemenResp
    wobjXmlElemenResp.Text = pvarCodigoActividad
    
    'Creo el elemento Requiere Campo Cantidad - '31/07/2006
    Set wobjXmlElemenResp = wobjXmlDOMResp.createElement("RequiereCampoCantidad") '31/07/2006
    wobjXmlDOMResp.selectSingleNode("//general").appendChild wobjXmlElemenResp '31/07/2006
    ' ###########################################################################################
    ' FJO - 31/07/2006
    'Reemplazar la siguiente asignación por la que está comentada cuando modifiquen el mensaje 1559,
    ' para que incluya la información requerida.
    'wobjXmlElemenResp.Text = "N"
    wobjXmlElemenResp.Text = Mid(pstrParseString, pvarPos + 1, 1) '31/07/2006
    ' ###########################################################################################
        
    Set wobjXmlElemenResp = wobjXmlDOMResp.createElement("coberturas")
    wobjXmlDOMResp.selectSingleNode("//cotizacion").appendChild wobjXmlElemenResp
    
    Set wobjXmlElemenResp = wobjXmlDOMResp.createElement("modulos")
    wobjXmlDOMResp.selectSingleNode("//cotizacion").appendChild wobjXmlElemenResp
    
    Set wobjXmlElemenResp = wobjXmlDOMResp.createElement("exclusiones")
    wobjXmlDOMResp.selectSingleNode("//cotizacion").appendChild wobjXmlElemenResp
    '
    For Each wobjMatch In wobjColMatch
    '
        If Not wvarCcurrRegistro < wvarCantRegistros Then Exit For
        wvarCcurrRegistro = wvarCcurrRegistro + 1
        '
        Set wobjXmlElemenResp = wobjXmlDOMResp.createElement("cobertura")
        wobjXmlDOMResp.selectSingleNode("//coberturas").appendChild wobjXmlElemenResp
    
        wvarCobertura = Val(Replace(wobjMatch.SubMatches(0), "_", " "))
        Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Codigo")
        wobjXmlElemenResp.setAttributeNode wobjXmlAtributo
        wobjXmlAtributo.Text = wvarCobertura
        
        Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("OrdenEnPantalla")
        wobjXmlElemenResp.setAttributeNode wobjXmlAtributo
        wobjXmlAtributo.Text = Val(Replace(wobjMatch.SubMatches(1), "_", " "))
        
        Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Adicional")
        wobjXmlElemenResp.setAttributeNode wobjXmlAtributo
        wobjXmlAtributo.Text = Replace(wobjMatch.SubMatches(12), "_", " ")
                
        If wobjMatch.SubMatches(2) = "S" Then 'Este campo indica que esta cobertura determina Modulo
            wvarCoberturaPr = Val(Replace(wobjMatch.SubMatches(0), "_", " ")) 'Este campo indica el codigo de cobertura
            Set wobjXmlElemenRespD = wobjXmlDOMResp.createElement("codigocobertura")
            wobjXmlDOMResp.selectSingleNode("//general/coberturasdeterminantes").appendChild wobjXmlElemenRespD
            wobjXmlElemenRespD.Text = wvarCoberturaPr
        End If
        
        Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Independiente")
        wobjXmlElemenResp.setAttributeNode wobjXmlAtributo
        wobjXmlAtributo.Text = IIf((wobjMatch.SubMatches(2) = "I"), "S", "N")
        
        Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Descripcion")
        wobjXmlElemenResp.setAttributeNode wobjXmlAtributo
        wobjXmlAtributo.Text = Trim(Replace(wobjMatch.SubMatches(3), "_", " "))
        
        Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Obligatoria")
        wobjXmlElemenResp.setAttributeNode wobjXmlAtributo
        wobjXmlAtributo.Text = Trim(Replace(wobjMatch.SubMatches(4), "_", " "))
        
        Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("CodigoCoberturaPrincipal")
        wobjXmlElemenResp.setAttributeNode wobjXmlAtributo
        wobjXmlAtributo.Text = Val(Replace(wobjMatch.SubMatches(5), "_", " "))
        
        Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("ObligatoriaAnteValorPrincipal")
        wobjXmlElemenResp.setAttributeNode wobjXmlAtributo
        wobjXmlAtributo.Text = Trim(Replace(wobjMatch.SubMatches(6), "_", " "))
        
        Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Editable")
        wobjXmlElemenResp.setAttributeNode wobjXmlAtributo
        wobjXmlAtributo.Text = Trim(Replace(wobjMatch.SubMatches(7), "_", " "))
        
        Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("TopeMinimo")
        wobjXmlElemenResp.setAttributeNode wobjXmlAtributo
    
        Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("TopeMaximo")
        wobjXmlElemenResp.setAttributeNode wobjXmlAtributo
    
        wvarCantModulos = Val(Replace(wobjMatch.SubMatches(8), "_", " "))
        wvarCcurrModulo = 0
        
        Set wobjColSubMatch = wobjRegSubexp.Execute(wobjMatch.SubMatches(9)) 'Determinacion del Modulo
        
        For Each wobjsubMatch In wobjColSubMatch
        '
            If Not wvarCcurrModulo < wvarCantModulos Then Exit For
            wvarCcurrModulo = wvarCcurrModulo + 1
            '
            wvarCodModulo = Val(Replace(wobjsubMatch.SubMatches(0), "_", " "))
            Set wobjXmlElemenRespMo = wobjXmlDOMResp.selectSingleNode("//modulos/modulo[@Codigo=" & wvarCodModulo & "]")
            '
            If wobjXmlElemenRespMo Is Nothing Then
                'Si no existe el Modulo lo genero
                Set wobjXmlElemenRespMo = wobjXmlDOMResp.createElement("modulo")
                wobjXmlDOMResp.selectSingleNode("//modulos").appendChild wobjXmlElemenRespMo
                Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Codigo")
                wobjXmlElemenRespMo.setAttributeNode wobjXmlAtributo
                wobjXmlAtributo.Text = wvarCodModulo
            End If
            
            'If wvarCoberturaPr = wvarCobertura Then
                'Esta cobertura determina el Modulo, coloco los valores generales
                'Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("ValorCampoDeterminanteMinimo")
                'wobjXmlElemenRespMo.setAttributeNode wobjXmlAtributo
                'wobjXmlAtributo.Text = Val(Replace(wobjSubMatch.SubMatches(1), "_", " "))
                ''
                'Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("ValorCampoDeterminanteMaximo")
                'wobjXmlElemenRespMo.setAttributeNode wobjXmlAtributo
                'wobjXmlAtributo.Text = Val(Replace(wobjSubMatch.SubMatches(2), "_", " "))
            'End If
            
            'Genero el Nodo Cobertura
            Set wobjXmlElemenRespC = wobjXmlDOMResp.createElement("cobertura")
            wobjXmlElemenRespMo.appendChild wobjXmlElemenRespC
            
            Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Determinante")
            wobjXmlElemenRespC.setAttributeNode wobjXmlAtributo
            wobjXmlAtributo.Text = wobjMatch.SubMatches(2)
            
            Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Codigo")
            wobjXmlElemenRespC.setAttributeNode wobjXmlAtributo
            wobjXmlAtributo.Text = wvarCobertura
            
            Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Min")
            wobjXmlElemenRespC.setAttributeNode wobjXmlAtributo
            wobjXmlAtributo.Text = Val(Left(wobjsubMatch.SubMatches(1), Len(wobjsubMatch.SubMatches(1)) - 2)) & "," & Right(wobjsubMatch.SubMatches(1), 2)
            
            Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Max")
            wobjXmlElemenRespC.setAttributeNode wobjXmlAtributo
            wobjXmlAtributo.Text = Val(Left(wobjsubMatch.SubMatches(2), Len(wobjsubMatch.SubMatches(2)) - 2)) & "," & Right(wobjsubMatch.SubMatches(2), 2)

            Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("PorcentajeCoberturaPrincipal")
            wobjXmlElemenRespC.setAttributeNode wobjXmlAtributo
            wobjXmlAtributo.Text = Val(Left(wobjsubMatch.SubMatches(3), Len(wobjsubMatch.SubMatches(3)) - 6)) & "," & Right(wobjsubMatch.SubMatches(3), 6)

            Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("PorcentajeFijoOTope")
            wobjXmlElemenRespC.setAttributeNode wobjXmlAtributo
            wobjXmlAtributo.Text = Trim(Replace(wobjsubMatch.SubMatches(4), "_", " "))

            Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("CodigoFranquiciaAsociada")
            wobjXmlElemenRespC.setAttributeNode wobjXmlAtributo
            wobjXmlAtributo.Text = Trim(Replace(wobjsubMatch.SubMatches(5), "_", " "))
            '
        Next wobjsubMatch
        '
        wvarCantExclusiones = Val(Replace(wobjMatch.SubMatches(10), "_", " "))
        '
        Set wobjXmlElemenRespE = wobjXmlDOMResp.createElement("exclusion")
        wobjXmlDOMResp.selectSingleNode("//cotizacion/exclusiones").appendChild wobjXmlElemenRespE
        
        Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Cobertura")
        wobjXmlElemenRespE.setAttributeNode wobjXmlAtributo
        wobjXmlAtributo.Text = wvarCobertura
        '
        For i = 0 To wvarCantExclusiones - 1
            Set wobjXmlElemenRespEe = wobjXmlDOMResp.createElement("coberturaexcluyente")
            wobjXmlElemenRespE.appendChild wobjXmlElemenRespEe
            wobjXmlElemenRespEe.Text = Mid((Replace(wobjMatch.SubMatches(11), "_", " ")), i * 3 + 1, 3)
        Next i
        '
    '
    Next wobjMatch
    '
    If Not pvarPreguntas Is Nothing Then wobjXmlDOMResp.selectSingleNode("//cotizacion").appendChild pvarPreguntas
    If Not pvarTiposAlarma Is Nothing Then wobjXmlDOMResp.selectSingleNode("//cotizacion").appendChild pvarTiposAlarma
    If Not pvarTiposGuardia Is Nothing Then wobjXmlDOMResp.selectSingleNode("//cotizacion").appendChild pvarTiposGuardia

    'Recorro las coberturas independientes para colocarles los Topes Minimos y maximos que se van a validar
    Set wobjNodosCoberturas = wobjXmlDOMResp.selectNodes("//cobertura[@Independiente='S']")
    For Each wobjNodoCobertura In wobjNodosCoberturas
        wobjNodoCobertura.Attributes.getNamedItem("TopeMinimo").Text = GetTopeMinimo(wobjNodoCobertura.Attributes.getNamedItem("Codigo").Text, wobjXmlDOMResp)
        wobjNodoCobertura.Attributes.getNamedItem("TopeMaximo").Text = GetTopeMaximo(wobjNodoCobertura.Attributes.getNamedItem("Codigo").Text, wobjXmlDOMResp)
    Next wobjNodoCobertura
    '
    wvarActivModulariz = False
    'Verifico si es una actividad NO modulada para sacarle los campos de coberturas determinantes
    Set wobjNodosCoberturas = wobjXmlDOMResp.selectNodes("//coberturasdeterminantes/codigocobertura")
    For Each wobjNodoCobertura In wobjNodosCoberturas
        If wobjXmlDOMResp.selectNodes("//modulos/modulo/cobertura[@Codigo='" & wobjNodoCobertura.Text & "']").length > 1 Then
            wvarActivModulariz = True
            Exit For
        End If
    Next wobjNodoCobertura
    
    If Not wvarActivModulariz Then
        'Si no es una acitvidad Modularizada Elimino los nodos de las coberturas determinantes
        Set wobjXmlElemenResp = wobjXmlDOMResp.selectSingleNode("//coberturasdeterminantes")
        wobjXmlElemenResp.parentNode.removeChild wobjXmlElemenResp
    End If
    
    ParseoMensaje = wobjXmlDOMResp.xml
    '
    Set wobjNodoCobertura = Nothing
    Set wobjNodosCoberturas = Nothing
    Set wobjXmlDOMResp = Nothing
    Set wobjXmlElemenResp = Nothing
    Set wobjXmlElemenRespE = Nothing
    Set wobjXmlElemenRespEe = Nothing
    Set wobjXmlElemenRespG = Nothing
    Set wobjXmlElemenRespD = Nothing
    Set wobjXmlElemenRespC = Nothing
    Set wobjXmlElemenRespMo = Nothing
    '
    Set wobjRegExp = Nothing
    Set wobjRegSubexp = Nothing
    '
End Function

Private Function GetTopeMaximo(pvarCodigoCobertura As String, pobjXMLDefinition As MSXML2.DOMDocument) As Double
Dim wobjNodoCobertura   As MSXML2.IXMLDOMElement
Dim wobjNodosCobertura  As MSXML2.IXMLDOMNodeList
    GetTopeMaximo = 0
    Set wobjNodosCobertura = pobjXMLDefinition.selectNodes("//modulos/modulo/cobertura[@Codigo='" & pvarCodigoCobertura & "']")
    For Each wobjNodoCobertura In wobjNodosCobertura
        If GetTopeMaximo <= CDbl(Replace(wobjNodoCobertura.Attributes.getNamedItem("Max").Text, ",", ".")) Then
            GetTopeMaximo = CDbl(Replace(wobjNodoCobertura.Attributes.getNamedItem("Max").Text, ",", "."))
        End If
    Next wobjNodoCobertura
    '
    Set wobjNodoCobertura = Nothing
    Set wobjNodosCobertura = Nothing
End Function

Private Function GetTopeMinimo(pvarCodigoCobertura As String, pobjXMLDefinition As MSXML2.DOMDocument) As Double
Dim wobjNodoCobertura   As MSXML2.IXMLDOMElement
Dim wobjNodosCobertura  As MSXML2.IXMLDOMNodeList
    GetTopeMinimo = 99999999
    Set wobjNodosCobertura = pobjXMLDefinition.selectNodes("//modulos/modulo/cobertura[@Codigo='" & pvarCodigoCobertura & "']")
    For Each wobjNodoCobertura In wobjNodosCobertura
        If GetTopeMinimo >= CDbl(Replace(wobjNodoCobertura.Attributes.getNamedItem("Min").Text, ",", ".")) Then
            GetTopeMinimo = CDbl(Replace(wobjNodoCobertura.Attributes.getNamedItem("Min").Text, ",", "."))
        End If
    Next wobjNodoCobertura
    '
    Set wobjNodoCobertura = Nothing
    Set wobjNodosCobertura = Nothing
End Function


