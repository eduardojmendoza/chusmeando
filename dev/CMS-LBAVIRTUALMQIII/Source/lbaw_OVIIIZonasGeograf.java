package com.qbe.services.lbavirtualmqiii;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.lbavirtualmqiii.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVIIIZonasGeograf implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQIII.lbaw_OVIIIZonasGeograf";
  static final String mcteOpID = "1556";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_CodRamo = "//RAMOPCOD";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLResponseProv = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    org.w3c.dom.NodeList wobjNodosZonas = null;
    org.w3c.dom.Node wobjNodoZona = null;
    org.w3c.dom.Element wobjZonaAppend = null;
    IAction wobjClass = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarCodigRamo = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    String wvarRequest = "";
    String wvarResponse = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //Deber� venir desde la p�gina
      wvarUsuario = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarCodigRamo = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_CodRamo )  ) + Strings.space( 4 ), 4 );
      //
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      //
      wvarStep = 110;
      wobjXMLParametros = null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarCodigRamo;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);
      wvarMQError = wobjFrame2MQ.Execute( wvarArea, strParseString, XmlDomExtended.marshal(wobjXMLConfig.selectSingleNode( "//MQCONFIG" )) ).toInt();
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      //
      wvarStep = 240;
      wvarResult = "";

      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = "";
        //cantidad de caracteres ocupados por par�metros de entrada
        wvarPos = 71;
        wvarResult = ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        wobjXMLResponse = new XmlDomExtended();
        wobjXMLResponse.loadXML( "<RS><ZONAS>" + wvarResult + "</ZONAS><PROVINCIAS></PROVINCIAS></RS>" );

        //BUSCO LAS PROVINCIAS DE CADA ZONA
        wobjXMLResponseProv = new XmlDomExtended();
        wobjNodosZonas = wobjXMLResponse.selectNodes( "//ZONAS/option" ) ;
        for( int nwobjNodoZona = 0; nwobjNodoZona < wobjNodosZonas.getLength(); nwobjNodoZona++ )
        {
          wobjNodoZona = wobjNodosZonas.item( nwobjNodoZona );
          //
          wobjZonaAppend = (org.w3c.dom.Element) wobjXMLRequest.selectSingleNode( "//ZONA" ) ;
          if( wobjZonaAppend == (org.w3c.dom.Element) null )
          {
            wobjZonaAppend = wobjXMLRequest.getDocument().createElement( "ZONA" );
            wobjXMLRequest.getDocument().getChildNodes().item( 0 ).appendChild( wobjZonaAppend );
          }

          XmlDomExtended.setText( wobjZonaAppend, XmlDomExtended.getText( wobjNodoZona.getAttributes().getNamedItem( "value" ) ) );
          wobjClass = new Variant( new lbaw_OVIIIGetProvincias() )((IAction) new lbaw_OVIIIGetProvincias().toObject());
          wobjClass.Execute( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarResponse, "" );
          wobjClass = (IAction) null;
          wobjXMLResponseProv.loadXML( wvarResponse );
          //
          wobjZonaAppend = wobjXMLResponse.getDocument().createElement( "ZONA" );
          /*unsup wobjXMLResponse.selectSingleNode( "//PROVINCIAS" ) */.appendChild( wobjZonaAppend );
          wobjZonaAppend.setAttribute( "Codigo", XmlDomExtended.getText( wobjNodoZona.getAttributes().getNamedItem( "value" ) ) );
          /*unsup wobjXMLResponseProv.selectSingleNode( "//Estado" ) */.getParentNode().removeChild( wobjXMLResponseProv.selectSingleNode( "//Estado" )  );
          wobjZonaAppend.appendChild( wobjXMLResponseProv.getDocument().getChildNodes().item( 0 ) );
          //
        }
        //
        wvarStep = 340;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wobjXMLResponse.getDocument().getDocumentElement().toString() + "</Response>" );
        //
      }
      //
      wvarStep = 360;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCodigRamo + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int pvarPos, String pstrParseString, int pvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    VbScript_RegExp wobjRegExp = new VbScript_RegExp();
    MatchCollection wobjColMatch;
    Match wobjMatch = null;
    String wvarParseEvalString = "";
    XmlDomExtended wobjXmlDOMResp = null;
    XmlDomExtended wobjXslDOMResp = null;
    org.w3c.dom.Element wobjXmlElemenResp = null;
    org.w3c.dom.Element wobjXmlElemenRespP = null;
    int wvarCantRegistros = 0;
    int wvarCcurrRegistro = 0;
    String wvarCodZona = "";
    //RegExp
    //MatchCollection
    //Match
    //
    wobjRegExp = new VbScript_RegExp();
    wobjRegExp.setGlobal( true ); 
    //
    wvarCantRegistros = (int)Math.rint( VB.val( Strings.mid( pstrParseString, pvarPos, 3 ) ) );
    wvarCcurrRegistro = 0;
    //Elimino el parametro que indica la cantidad de registros devueltos
    wvarParseEvalString = Strings.mid( pstrParseString, pvarPos + 3 );
    wvarParseEvalString = Strings.replace( wvarParseEvalString, " ", "_" );

    wobjRegExp.setPattern( "(\\S{4})(\\S{30})" );

    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    wobjXmlDOMResp = new XmlDomExtended();
    wobjXslDOMResp = new XmlDomExtended();

    wobjXmlDOMResp.loadXML( "<RS></RS>" );
    //
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //
      //error: function 'SubMatches' was not found.
      //unsup: wvarCodZona = Trim(Strings.replace(wobjMatch.SubMatches(0), "_", " "))
      if( (!Strings.trim( wvarCodZona ).equals( "" )) && (wvarCcurrRegistro < wvarCantRegistros) )
      {
        wvarCcurrRegistro = wvarCcurrRegistro + 1;
        wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "ZONA" );
        /*unsup wobjXmlDOMResp.selectSingleNode( "//RS" ) */.appendChild( wobjXmlElemenResp );

        wobjXmlElemenRespP = wobjXmlDOMResp.getDocument().createElement( "CODIGO" );
        wobjXmlElemenResp.appendChild( wobjXmlElemenRespP );
        if( new Variant( wvarCodZona ).isNumeric() )
        {
          wvarCodZona = String.valueOf( VB.val( wvarCodZona ) );
        }
        XmlDomExtended.setText( wobjXmlElemenRespP, wvarCodZona );

        wobjXmlElemenRespP = wobjXmlDOMResp.getDocument().createElement( "DESCRIPCION" );
        wobjXmlElemenResp.appendChild( wobjXmlElemenRespP );
        //error: function 'SubMatches' was not found.
        //unsup: wobjXmlElemenRespP.Text = UCase(Trim(Strings.replace(wobjMatch.SubMatches(1), "_", " ")))
      }
      //
    }
    //
    wobjXslDOMResp.loadXML( p_GetXSL());
    ParseoMensaje = wobjXmlDOMResp.transformNode( wobjXslDOMResp ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
    //ParseoMensaje = wobjXmlDOMResp.xml
    //
    wobjXmlDOMResp = null;
    wobjXmlElemenResp = (org.w3c.dom.Element) null;
    wobjXmlElemenRespP = (org.w3c.dom.Element) null;
    wobjXslDOMResp = null;
    //
    wobjRegExp = (VbScript_RegExp) null;

    return ParseoMensaje;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<?xml version='1.0' encoding='UTF-8'?>";
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>";
    wvarStrXSL = wvarStrXSL + "<xsl:output method='html' version='1.0' encoding='UTF-8' indent='yes'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:template match='/' >";
    wvarStrXSL = wvarStrXSL + "<xsl:for-each select='//ZONA'>";
    wvarStrXSL = wvarStrXSL + "<xsl:sort select='./DESCRIPCION'/>";
    wvarStrXSL = wvarStrXSL + "     <option>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='value'><xsl:value-of select='CODIGO'/></xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='descripcion'><xsl:value-of select='DESCRIPCION'/></xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "     <xsl:value-of select='DESCRIPCION'/>";
    wvarStrXSL = wvarStrXSL + "     </option>";
    wvarStrXSL = wvarStrXSL + "     </xsl:for-each>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template >";
    wvarStrXSL = wvarStrXSL + "     </xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    //
    return p_GetXSL;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
