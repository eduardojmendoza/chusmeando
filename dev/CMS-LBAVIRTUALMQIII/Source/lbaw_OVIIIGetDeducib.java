package com.qbe.services.lbavirtualmqiii;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.lbavirtualmqiii.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVIIIGetDeducib implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQIII.lbaw_OVIIIGetDeducib";
  static final String mcteOpID = "1530";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_Codigo_Ramo = "//RAMOPCOD";
  static final String mcteParam_Comercio = "//COMERCIO";
  static final String mcteParam_Codigo_Profesion = "//I_ACTIVIDAD";
  static final String mcteParam_Zona = "//I_ZONA";
  static final String mcteParam_ModuloActual = "//I_CODIGOMODULO";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * Const mcteParam_Codigo_Profesion    As String = "//ACTIVIDAD"
   * Const mcteParam_Zona                As String = "//ZONA"
   * Const mcteParam_ModuloActual        As String = "//MODULO"
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarModuloActual = "";
    String wvarCodProfesion = "";
    String wvarCodRamo = "";
    String wvarCodZona = "";
    String wvarAreaToSend = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUsuario = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarCodRamo = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Ramo )  ) + Strings.space( 4 ), 4 );
      wvarCodProfesion = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Profesion )  ) + Strings.space( 6 ), 6 );
      wvarCodZona = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Zona )  ), 4 );
      //
      wvarAreaToSend = wvarCodRamo + wvarCodProfesion + wvarCodZona;
      wvarAreaToSend = wvarAreaToSend + invoke( "GetVectorCoberturas", new Variant[] { new Variant(wobjXMLRequest.selectSingleNode( new Variant(".") ) ), new Variant(mcteParam_Comercio), new Variant(30) } );
      //
      wvarStep = 60;
      wobjXMLRequest = null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      //
      wvarStep = 110;
      wobjXMLParametros = null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarAreaToSend;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);
      wvarMQError = wobjFrame2MQ.Execute( wvarArea, strParseString, XmlDomExtended.marshal(wobjXMLConfig.selectSingleNode( "//MQCONFIG" )) ).toInt();
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      //
      wvarStep = 240;
      //
      parsear: 
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = "";
        //cantidad de caracteres ocupados por par�metros de entrada
        wvarPos = 234;
        wvarResult = ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        //
        wvarStep = 340;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
        //
      }
      //
      wvarStep = 360;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarAreaToSend + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int pvarPos, String pstrParseString, int pvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    VbScript_RegExp wobjRegExp = new VbScript_RegExp();
    MatchCollection wobjColMatch;
    Match wobjMatch = null;
    String wvarParseEvalString = "";
    XmlDomExtended wobjXmlDOMResp = null;
    org.w3c.dom.Element wobjXmlElemenResp = null;
    org.w3c.dom.Element wobjXmlElemenRespE = null;
    int wvarCantRegistros = 0;
    int wvarCcurrRegistro = 0;
    //
    //RegExp
    //MatchCollection
    //Match
    //
    //
    wobjRegExp = new VbScript_RegExp();
    wobjRegExp.setGlobal( true ); 
    //
    wobjRegExp.setPattern( "(\\S{3})(\\S{2})(\\S{60})" );
    //
    wvarParseEvalString = Strings.mid( pstrParseString, pvarPos + 3 );
    wvarParseEvalString = Strings.replace( wvarParseEvalString, " ", "_" );
    wvarCantRegistros = (int)Math.rint( VB.val( Strings.mid( pstrParseString, pvarPos, 3 ) ) );
    wvarCcurrRegistro = 0;

    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    wobjXmlDOMResp = new XmlDomExtended();

    wobjXmlDOMResp.loadXML( "<DEDUCIBLES></DEDUCIBLES>" );
    //
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //
      if( ! (wvarCcurrRegistro < wvarCantRegistros) )
      {
        break;
      }
      wvarCcurrRegistro = wvarCcurrRegistro + 1;
      //
      wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "DEDUCIBLE" );
      /*unsup wobjXmlDOMResp.selectSingleNode( "//DEDUCIBLES" ) */.appendChild( wobjXmlElemenResp );
      //
      wobjXmlElemenRespE = wobjXmlDOMResp.getDocument().createElement( "CODIGOCOBERTURA" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespE );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlElemenRespE.Text = UCase(Trim(Strings.replace(wobjMatch.SubMatches(0), "_", " ")))
      //
      wobjXmlElemenRespE = wobjXmlDOMResp.getDocument().createElement( "CODIGODEDUCIBLE" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespE );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlElemenRespE.Text = Trim(Strings.replace(wobjMatch.SubMatches(1), "_", " "))
      //
      wobjXmlElemenRespE = wobjXmlDOMResp.getDocument().createElement( "DESCRIPCION" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespE );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlElemenRespE.Text = Trim(Strings.replace(wobjMatch.SubMatches(2), "_", " "))
      //
    }
    //
    ParseoMensaje = XmlDomExtended.marshal(wobjXmlDOMResp.getDocument().getDocumentElement());
    //
    wobjXmlDOMResp = null;
    wobjXmlElemenResp = (org.w3c.dom.Element) null;
    wobjXmlElemenRespE = (org.w3c.dom.Element) null;
    //
    wobjRegExp = (VbScript_RegExp) null;
    //
    return ParseoMensaje;
  }

  private String GetVectorCoberturas( org.w3c.dom.Node pobjNodo, String pVarPathVector, int pVarCantElementos ) throws Exception
  {
    String GetVectorCoberturas = "";
    int wvarCounterRelleno = 0;
    int wvarActualCounter = 0;
    org.w3c.dom.NodeList wobjNodosComercios = null;
    org.w3c.dom.Node wobjNodoComercio = null;
    org.w3c.dom.NodeList wobjNodosCobertura = null;
    org.w3c.dom.Node wobjNodoCobertura = null;

    wobjNodosComercios = pobjNodo.selectNodes( pVarPathVector ) ;
    wvarActualCounter = 0;
    for( int nwobjNodoComercio = 0; nwobjNodoComercio < wobjNodosComercios.getLength(); nwobjNodoComercio++ )
    {
      wobjNodoComercio = wobjNodosComercios.item( nwobjNodoComercio );
      wobjNodosCobertura = wobjNodoComercio.getChildNodes();
      for( int nwobjNodoCobertura = 0; nwobjNodoCobertura < wobjNodosCobertura.getLength(); nwobjNodoCobertura++ )
      {
        wobjNodoCobertura = wobjNodosCobertura.item( nwobjNodoCobertura );
        if( Strings.toUpperCase( wobjNodoCobertura.getNodeName() ).matches( "C*" ) )
        {
          if( VB.val( XmlDomExtended .getText( wobjNodoCobertura ) ) > 0 )
          {
            //Cobertura con Valor
            GetVectorCoberturas = GetVectorCoberturas + Strings.right( "000" + Strings.mid( wobjNodoCobertura.getNodeName(), 2 ), 3 );
            GetVectorCoberturas = GetVectorCoberturas + Strings.right( "00" + XmlDomExtended.getText( pobjNodo.selectSingleNode( mcteParam_ModuloActual )  ), 2 );
            wvarActualCounter = wvarActualCounter + 1;
            if( wvarActualCounter == (pVarCantElementos - 1) )
            {
              return IAction_Execute;
            }
          }
        }
      }
    }

    for( wvarCounterRelleno = wvarActualCounter; wvarCounterRelleno <= (pVarCantElementos - 1); wvarCounterRelleno++ )
    {
      GetVectorCoberturas = GetVectorCoberturas + "000";
    }
    GetVectorCoberturas = Strings.format( wvarActualCounter, "#000" ) + GetVectorCoberturas;

    ClearObjects: 
    wobjNodosComercios = (org.w3c.dom.NodeList) null;
    wobjNodoComercio = (org.w3c.dom.Node) null;
    wobjNodosCobertura = (org.w3c.dom.NodeList) null;
    wobjNodoCobertura = (org.w3c.dom.Node) null;
    return GetVectorCoberturas;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
