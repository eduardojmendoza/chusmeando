package com.qbe.services.lbavirtualmqiii;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.lbavirtualmqiii.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVIIICondCober implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQIII.lbaw_OVIIICondCober";
  static final String mcteOpID = "1507";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_Codigo_Ramo = "//RAMOPCOD";
  /**
   * Const mcteParam_Codigo_Profesion    As String = "//ACTIVIDAD"
   */
  static final String mcteParam_Codigo_Profesion = "//I_ACTIVIDAD";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarCodProfesion = "";
    String wvarCodRamo = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //Deber� venir desde la p�gina
      wvarUsuario = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarCodRamo = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Ramo )  ) + Strings.space( 4 ), 4 );
      wvarCodProfesion = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Profesion )  ) + Strings.space( 6 ), 6 );
      //
      //
      wvarStep = 60;
      wobjXMLRequest = null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      //
      wvarStep = 110;
      wobjXMLParametros = null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarCodRamo + wvarCodProfesion;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);
      wvarMQError = wobjFrame2MQ.Execute( wvarArea, strParseString, XmlDomExtended.marshal(wobjXMLConfig.selectSingleNode( "//MQCONFIG" )) ).toInt();
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      //
      wvarStep = 240;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = "";
        //cantidad de caracteres ocupados por par�metros de entrada
        wvarPos = 77;
        wvarResult = ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        //
        wvarStep = 340;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
        //
      }
      //
      wvarStep = 360;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCodRamo + wvarCodProfesion + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int pvarPos, String pstrParseString, int pvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    VbScript_RegExp wobjRegExp = new VbScript_RegExp();
    MatchCollection wobjColMatch;
    Match wobjMatch = null;
    String wvarParseEvalString = "";
    XmlDomExtended wobjXmlDOMResp = null;
    org.w3c.dom.Element wobjXmlElemenResp = null;
    org.w3c.dom.Element wobjXmlElemenRespD = null;
    int wvarCantRegistros = 0;
    int wvarCcurrRegistro = 0;
    //
    //RegExp
    //MatchCollection
    //Match
    //
    //
    //
    wobjRegExp = new VbScript_RegExp();
    wobjRegExp.setGlobal( true ); 
    //
    wvarParseEvalString = Strings.mid( pstrParseString, pvarPos + 4 );
    wvarParseEvalString = Strings.replace( wvarParseEvalString, " ", "_" );
    wvarCantRegistros = (int)Math.rint( VB.val( Strings.mid( pstrParseString, pvarPos, 4 ) ) );
    wvarCcurrRegistro = 0;

    //LNK-CODCONDI PIC X(02) 10 LNK-COBDES  OCCURS 10 TIMES.
    wobjRegExp.setPattern( "(\\S{2})(\\S{600})" );
    //
    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    wobjXmlDOMResp = new XmlDomExtended();
    wobjXmlDOMResp.loadXML( "<CONDICIONESESPECIALES></CONDICIONESESPECIALES>" );
    //
    //
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //
      if( ! (wvarCcurrRegistro < wvarCantRegistros) )
      {
        break;
      }
      wvarCcurrRegistro = wvarCcurrRegistro + 1;
      //
      wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "DATO" );
      /*unsup wobjXmlDOMResp.selectSingleNode( "//CONDICIONESESPECIALES" ) */.appendChild( wobjXmlElemenResp );
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "CODIGO" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(0), "_", " ").trim());
      //
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "DESCRIPCION" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespD );
      //error: function 'SubMatches' was not found.
      wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches(1), "_", " ").trim());
      //
    }
    //
    ParseoMensaje = XmlDomExtended.marshal(wobjXmlDOMResp.getDocument().getDocumentElement());
    //
    wobjXmlDOMResp = null;
    wobjXmlElemenResp = (org.w3c.dom.Element) null;
    wobjXmlElemenRespD = (org.w3c.dom.Element) null;
    //
    wobjRegExp = (VbScript_RegExp) null;

    return ParseoMensaje;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
