Attribute VB_Name = "ModGeneral"
Option Explicit

' Parametros XML de Configuracion
Public Const gcteConfFileName       As String = "LBAVirtualMQConfig.xml"
Public Const gcteQueueManager       As String = "//QUEUEMANAGER"
Public Const gctePutQueue           As String = "//PUTQUEUE"
Public Const gcteGetQueue           As String = "//GETQUEUE"
Public Const gcteGMOWaitInterval    As String = "//GMO_WAITINTERVAL"

' Parametros XML del Cotizador
Public Const gcteParamFileName      As String = "ParametrosMQ.xml"
Public Const gcteNodosGenerales     As String = "//GENERALES"
Public Const gcteCIAASCOD           As String = "/CIAASCOD"
Public Const gcteClassMQConnection  As String = "WD.Frame2MQ"

Public Function MidAsString(pvarStringCompleto As String, ByRef pvarActualCounter As Long, pvarLongitud As Long) As String
    MidAsString = Mid(pvarStringCompleto, pvarActualCounter, pvarLongitud)
    pvarActualCounter = pvarActualCounter + pvarLongitud
End Function

