package com.qbe.services.lbavirtualmqiii;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.lbavirtualmqiii.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVIIIGetCotizICO implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQIII.lbaw_OVIIIGetCotizICO";
  static final String mcteOpID = "1533";
  /**
   * 'Parametros XML de Entrada
   * 'Datos Generales
   * Const mcteParam_ValoresIngresados       As String = "//valoresingresados"
   * Const mcteParam_Usuario                 As String = "//USUARIO"
   * Const mcteParam_CodRamo                 As String = "//RAMOPCOD"
   * Const mcteParamIVACLIENTE               As String = "//IVACLIENTE"
   * Const mcteParamESTABILIZACION           As String = "//ESTABILIZACION"
   * Const mcteParamFORMAPAGO                As String = "//FORMAPAGO"
   * Const mcteParamPERIODO                  As String = "//PERIODO"
   * 'Cantidad de Comercios
   * Const mcteParamPLANPAGO                 As String = "//PLANPAGO"
   * Const mcteParamRECARGOADMINISTRATIVO    As String = "//RECARGOADMINISTRATIVO"
   * Const mcteParamRECARGOFINANCIERO        As String = "//RECARGOFINANCIERO"
   * Const mcteParamCOMISIONPRODUCTOR        As String = "//COMISIONPRODUCTOR"
   * Const mcteParamACTIVIDAD                As String = "//ACTIVIDAD"
   * Const mcteParamVIGENCIAPOLANN           As String = "//VIGENCIAPOLANN"
   * Const mcteParamVIGENCIAPOLMES           As String = "//VIGENCIAPOLMES"
   * Const mcteParamVIGENCIAPOLDIA           As String = "//VIGENCIAPOLDIA"
   * 
   * 'Datos del Comercio
   * Const mcteParamPAIS                     As String = "./PAIS"
   * Const mcteParamPROVINCIA                As String = "./PROVINCIA"
   * Const mcteParamZONA                     As String = "./ZONA"
   * Const mcteParamINGRESOSBRUTOS           As String = "./INGRESOSBRUTOS"
   * Const mcteParamTIPOALARMA               As String = "./TIPOALARMA"
   * Const mcteParamTIPOGUARDIA              As String = "./TIPOGUARDIA"
   * Const mcteParamALUMNOSCARTELES          As String = "./ALUMNOSCARTELES"
   * Const mcteParamCODIGOMODULO             As String = "./CODIGOMODULO"
   */
  static final String mcteParam_Conf_ConvertirXML = "//APLICARCONVERSION";
  static final String mcteParam_Conf_ValidarImportes = "//VALIDARIMPORTES";
  static final String mcteParam_Conf_GuardarSQL = "//GUARDARCOTIZACION";
  static final String mcteParam_Conf_GuardarSQLComerc = "//GUARDARCOMERCIO";
  static final String mcteParam_Conf_BuscDatosAdicional = "//BUSCARDATOSADICIONALES";
  /**
   * 
   */
  static final String mcteParam_ACTUALIZARCLIENTE = "//ACTUALIZARCLIENTE";
  static final String mcteParam_ValoresIngresados = "//valoresingresados";
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_CodRamo = "//RAMOPCOD";
  static final String mcteParamIVACLIENTE = "//I_IVA";
  static final String mcteParamCLIENTIP = "//CLIENTIP";
  static final String mcteParamESTABILIZACION = "//I_ESTABILIZACION";
  static final String mcteParamFORMAPAGO = "//I_FORMAPAGO";
  static final String mcteParamPERIODO = "//I_PERIODO";
  /**
   * Cantidad de Comercios
   */
  static final String mcteParamPLANPAGO = "//I_PLANPAGO";
  static final String mcteParamRECARGOADMINISTRATIVO = "//I_RECARGOADMINISTRATIVO";
  static final String mcteParamRECARGOFINANCIERO = "//I_RECARGOFINANCIERO";
  static final String mcteParamCOMISIONPRODUCTOR = "//I_COMISIONPRODUCTOR";
  static final String mcteParamACTIVIDAD = "//I_ACTIVIDAD";
  /**
   * ***
   */
  static final String mcteParam_FECHAVIGENCIA = "//I_FECHA";
  /**
   * Datos del Comercio
   */
  static final String mcteParamPAIS = "./I_PAIS";
  static final String mcteParamPROVINCIA = "./I_PROVINCIA";
  static final String mcteParamZONA = "./I_ZONA";
  static final String mcteParamINGRESOSBRUTOS = "./I_IIBB";
  static final String mcteParamTIPOALARMA = "./TIPOALARMA";
  static final String mcteParamTIPOGUARDIA = "./TIPOGUARDIA";
  static final String mcteParamALUMNOSCARTELES = "./ALUMNOSCARTELES";
  static final String mcteParamCODIGOMODULO = "./I_CODIGOMODULO";
  static final String mcteParamBAJACOMERCIO = "BAJA";
  /**
   * Modulos
   */
  static final String mcteParam_XML_Modulos = "//modulos";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    String wvarResponse = "";
    String wvarRequest = "";
    Object wobjClass = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended testXML = null;
    XmlDomExtended wvarXMLSQLArea = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarCodigoModulo = "";
    String wvarCodigoModGral = "";
    org.w3c.dom.Node wvarNodop = null;
    org.w3c.dom.Element wvarNewNodo = null;
    Variant wvarPos = new Variant();
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    org.w3c.dom.NodeList wobjNodosComercios = null;
    org.w3c.dom.NodeList wobjNodosCoberturas = null;
    org.w3c.dom.NodeList wobjNodosPreguntas = null;
    org.w3c.dom.Node wobjNodoComercio = null;
    org.w3c.dom.Node wobjNodoCobertura = null;
    org.w3c.dom.Node wobjNodoPregunta = null;
    int wvarCantCoberturas = 0;
    int wvarCantPreguntas = 0;
    int wvarCounter = 0;
    String wvarParseString = "";
    int wvarStringLen = 0;
    boolean wvarValidarImportes = false;
    boolean wvarGuardarCotiz = false;
    boolean wvarGuardarComercio = false;
    boolean wvarActualizarCli = false;
    boolean wvarNuevoCliente = false;
    String wvarValoresIngres = "";
    boolean wvarObtenerDatosAdicionales = false;
    String wvarFormaPago = "";
    String wvarPlanPago = "";
    //
    //
    //
    //
    //
    //
    //
    //DA - 16/03/2007: ver mas abajo el control que se hace con estas variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarNodop = wobjXMLRequest.getDocument().getChildNodes().item( 0 );
      //
      if( wobjXMLRequest.selectNodes( mcteParam_ValoresIngresados ) .getLength() != 0 )
      {
        wvarValoresIngres = wobjXMLRequest.selectSingleNode( mcteParam_ValoresIngresados ) .toString();
        invoke( "ConvertirXML", new Variant[] { new Variant(wobjXMLRequest) } );
      }

      //Si la fecha de vigencia viene mal informado la corrijo
      if( wobjXMLRequest.selectNodes( mcteParam_FECHAVIGENCIA ) .getLength() != 0 )
      {
        if( ! (new Variant( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_FECHAVIGENCIA )  ) ).isDate()) )
        {
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_FECHAVIGENCIA ) , DateTime.format( DateTime.now(), "dd/MM/yyyy" ) );
        }
      }
      //
      wvarStep = 15;
      wvarUsuario = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      //
      wobjNodosComercios = wvarNodop.selectNodes( "//COMERCIO" ) ;
      wvarNewNodo = /*warning: cast*/ ((org.w3c.dom.Element) wobjXMLRequest.selectSingleNode( "//CANTCOMER" ) );
      if( wvarNewNodo == (org.w3c.dom.Element) null )
      {
        wvarNewNodo = wobjXMLRequest.getDocument().createElement( "CANTCOMER" );
        /*unsup wobjXMLRequest.selectSingleNode( "//ENCABEZADO" ) */.appendChild( wvarNewNodo );
      }
      XmlDomExtended.setText( wvarNewNodo, String.valueOf( wobjNodosComercios.getLength() - wobjXMLRequest.selectNodes( ("//" + mcteParamBAJACOMERCIO) ) .getLength() ) );
      //
      if( ! (wobjXMLRequest.selectSingleNode( "//GENERARLOG" )  == (org.w3c.dom.Node) null) )
      {
        wobjXMLRequest.save( System.getProperty("user.dir") + "\\" + mcteClassName + ".xml" );
      }
      //
      wvarStep = 20;
      //
      //Datos Generales de la Cotizacion
      wvarArea = "";
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParam_CodRamo, 0, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParamIVACLIENTE, 0, 1, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParamESTABILIZACION, 1, 4, 0);

      //DA - 16/03/2007
      //A partir de ahora a la forma de pago EFECTIVO(1) se agregan las formas DEBITO(5) y TARJETA(4)
      //El AIS determina si hay que aplicar recargo financiero en base al plan de pago (cantidad de cuotas) sin tener en cuenta la forma de pago.
      //Si tiene mas de 5 cuotas, se le aplica un recargo financiero, indistintamente de la forma de pago.
      //Dado que para forma de pago DEBITO y TARJETA NO debe aplicarse recargo financiero
      //para no modificar el back se fuerza el plan de pago a 1 cuando se trate de estas formas de pago.
      //De este modo, para DEBITO y TARJETA NUNCA se va a aplicar recargo financiero.
      wvarFormaPago = Strings.right( invoke( "GetDatoFormateado", new Variant[] { new Variant(wvarNodop), new Variant(mcteParamFORMAPAGO), new Variant(1), new Variant(4), new Variant(0) } ), 1 );

      if( Obj.toInt( wvarFormaPago ) != 1 )
      {
        wvarPlanPago = "001";
      }
      else
      {
        wvarPlanPago = invoke( "GetDatoFormateado", new Variant[] { new Variant(wvarNodop), new Variant(mcteParamPLANPAGO), new Variant(1), new Variant(3), new Variant(0) } );
      }

      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParamFORMAPAGO, 1, 4, 0);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParamPERIODO, 1, 4, 0);
      wvarArea = wvarArea + invoke( "CompleteZero", new Variant[] { new Variant(String.valueOf( (wobjNodosComercios.getLength() - wobjXMLRequest.selectNodes( new Variant(("//" + mcteParamBAJACOMERCIO)) ) .getLength()) )), new Variant(2) } );
      //wvarArea = wvarArea & GetDatoFormateado(wvarNodop, mcteParamPLANPAGO, 1, 3)
      wvarArea = wvarArea + wvarPlanPago;
      //
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParamRECARGOADMINISTRATIVO, 2, 3, 2);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParamRECARGOFINANCIERO, 2, 3, 2);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParamCOMISIONPRODUCTOR, 2, 3, 2);
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParamACTIVIDAD, 0, 6, 0);
      wvarArea = wvarArea + DateTime.format( DateTime.now(), "yyyyMMdd" );
      //MHC: PPCR 2009-00013 : Sellados
      wvarArea = wvarArea + GetDatoFormateado( wvarNodop, mcteParamCLIENTIP, 0, 2, 0);
      //
      wvarStep = 25;
      for( int nwobjNodoComercio = 0; nwobjNodoComercio < wobjNodosComercios.getLength(); nwobjNodoComercio++ )
      {
        wobjNodoComercio = wobjNodosComercios.item( nwobjNodoComercio );
        if( wobjNodoComercio.selectSingleNode( (".[./" + mcteParamBAJACOMERCIO + "='S']") )  == (org.w3c.dom.Node) null )
        {
          //
          wvarStep = 26;
          wvarCodigoModGral = XmlDomExtended.getText( wobjNodoComercio.selectSingleNode( mcteParamCODIGOMODULO )  );
          //
          wvarArea = wvarArea + GetDatoFormateado( wobjNodoComercio, mcteParamPAIS, 1, 2, 0);
          wvarArea = wvarArea + GetDatoFormateado( wobjNodoComercio, mcteParamPROVINCIA, 1, 2, 0);
          wvarArea = wvarArea + GetDatoFormateado( wobjNodoComercio, mcteParamZONA, 1, 4, 0);
          wvarArea = wvarArea + GetDatoFormateado( wobjNodoComercio, mcteParamINGRESOSBRUTOS, 0, 1, 0);
          wvarArea = wvarArea + GetDatoFormateado( wobjNodoComercio, mcteParamTIPOALARMA, 1, 2, 0);
          wvarArea = wvarArea + GetDatoFormateado( wobjNodoComercio, mcteParamTIPOGUARDIA, 1, 2, 0);
          wvarArea = wvarArea + GetDatoFormateado( wobjNodoComercio, mcteParamALUMNOSCARTELES, 1, 5, 0);
          //
          wvarStep = 27;
          //Recorro todos los Nodos del Comercio Buscando las Coberturas
          wobjNodosCoberturas = wobjNodoComercio.getChildNodes();
          wvarCantCoberturas = 0;
          //
          for( int nwobjNodoCobertura = 0; nwobjNodoCobertura < wobjNodosCoberturas.getLength(); nwobjNodoCobertura++ )
          {
            wobjNodoCobertura = wobjNodosCoberturas.item( nwobjNodoCobertura );
            if( Strings.toUpperCase( wobjNodoCobertura.getNodeName() ).matches( "C#*" ) )
            {
              wvarCantCoberturas = wvarCantCoberturas + 1;
              //Solo se informan 30 coberturas
              if( wvarCantCoberturas <= 30 )
              {
                wvarCodigoModulo = invoke( "GetModulo", new Variant[] { new Variant(Obj.toInt( new Variant(wvarCodigoModGral) )), new Variant(Strings.mid( new Variant(wobjNodoCobertura.getNodeName()), new Variant(2) )), new Variant(Obj.toDouble( new Variant( XmlDomExtended .getText( wobjNodoCobertura )) )), new Variant(wobjXMLRequest.selectSingleNode( new Variant(mcteParam_XML_Modulos) ) ) } ).toString();
                //wvarCodigoModulo = wvarCodigoModGral
                wvarArea = wvarArea + invoke( "CompleteZero", new Variant[] { new Variant(Strings.mid( new Variant(wobjNodoCobertura.getNodeName()), new Variant(2) )), new Variant(3) } );
                wvarArea = wvarArea + CompleteZero(wvarCodigoModulo, 2);
                wvarArea = wvarArea + invoke( "CompleteZero", new Variant[] { new Variant(String.valueOf( (Obj.toDouble( new Variant( XmlDomExtended .getText( wobjNodoCobertura )) ) * 100) )), new Variant(15) } );
              }
            }
          }
          //
          wvarStep = 30;
          //Completo el area hasta llegar a las 30 Coberturas
          for( wvarCounter = wvarCantCoberturas + 1; wvarCounter <= 30; wvarCounter++ )
          {
            wvarArea = wvarArea + invoke( "CompleteZero", new Variant[] { new Variant("0"), new Variant(3) } );
            wvarArea = wvarArea + invoke( "CompleteZero", new Variant[] { new Variant("0"), new Variant(2) } );
            wvarArea = wvarArea + invoke( "CompleteZero", new Variant[] { new Variant("0"), new Variant(15) } );
          }
          //
          wobjNodosCoberturas = (org.w3c.dom.NodeList) null;
          wobjNodoCobertura = (org.w3c.dom.Node) null;
          //
          wvarStep = 32;
          //Recorro todos los Nodos del Comercio Buscando las Preguntas
          wobjNodosPreguntas = wobjNodoComercio.getChildNodes();
          wvarCantPreguntas = 0;
          //
          for( int nwobjNodoPregunta = 0; nwobjNodoPregunta < wobjNodosPreguntas.getLength(); nwobjNodoPregunta++ )
          {
            wobjNodoPregunta = wobjNodosPreguntas.item( nwobjNodoPregunta );
            if( Strings.toUpperCase( wobjNodoPregunta.getNodeName() ).matches( "P#*" ) )
            {
              wvarCantPreguntas = wvarCantPreguntas + 1;
              //Solo se informan 30 preguntas
              if( wvarCantPreguntas <= 30 )
              {
                wvarArea = wvarArea + invoke( "CompleteZero", new Variant[] { new Variant(Strings.mid( new Variant(wobjNodoPregunta.getNodeName()), new Variant(2) )), new Variant(6) } );
                wvarArea = wvarArea + Strings.left( XmlDomExtended .getText( wobjNodoPregunta ), 1 );
              }
            }
          }
          //
          wvarStep = 34;
          //Completo el area hasta llegar a las 30 Preguntas
          for( wvarCounter = wvarCantPreguntas + 1; wvarCounter <= 30; wvarCounter++ )
          {
            wvarArea = wvarArea + invoke( "CompleteZero", new Variant[] { new Variant("0"), new Variant(6) } );
            wvarArea = wvarArea + " ";
          }
          //
          wobjNodosPreguntas = (org.w3c.dom.NodeList) null;
          wobjNodoPregunta = (org.w3c.dom.Node) null;
          //
        }
      }
      //
      //
      wvarNewNodo = (org.w3c.dom.Element) null;
      wvarNodop = (org.w3c.dom.Node) null;
      //
      wvarStep = 36;
      //
      // ***************************************
      //VALIDACION DE IMPORTES ANTES DE COTIZAR
      // ***************************************
      if( wobjXMLRequest.selectSingleNode( mcteParam_Conf_ValidarImportes )  == (org.w3c.dom.Node) null )
      {
        wvarValidarImportes = true;
      }
      else
      {
        wvarValidarImportes = !XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Conf_ValidarImportes )  ).equals( "N" );
      }
      //
      //
      wvarStep = 37;
      if( wvarValidarImportes )
      {
        //Recorro nuevamente los Comercios para validar los importes de cada uno
        wobjXMLResponse = new XmlDomExtended();
        for( int nwobjNodoComercio = 0; nwobjNodoComercio < wobjNodosComercios.getLength(); nwobjNodoComercio++ )
        {
          wobjNodoComercio = wobjNodosComercios.item( nwobjNodoComercio );
          if( wobjNodoComercio.selectSingleNode( (".[./" + mcteParamBAJACOMERCIO + "='S']") )  == (org.w3c.dom.Node) null )
          {
            //
            wvarStep = 40;
            //Primero valido los importes ingresados para el comercio
            wvarRequest = "<Request>" + XmlDomExtended.marshal(wobjXMLRequest.selectSingleNode( "//ENCABEZADO" )) + wobjNodoComercio.toString() + "</Request>";
            wobjClass = new Variant( new lbaw_OVIIIValidImporte() )new lbaw_OVIIIValidImporte().toObject();
            wobjClass.Execute( wvarRequest, wvarResponse, "" );
            wobjClass = (IAction) null;
            //
            wobjXMLResponse.loadXML( wvarResponse );
            //
            if( ! (wobjXMLResponse.selectSingleNode( "//ERROR" )  == (org.w3c.dom.Node) null) )
            {
              //
              wvarStep = 50;
              Response.set( wobjXMLResponse.getDocument().getDocumentElement().toString() );
              /*TBD mobjCOM_Context.SetComplete() ;*/
              IAction_Execute = 0;
              //unsup GoTo ClearObjects
              //
            }
            //
          }
        }
      }
      //
      wobjXMLResponse = null;
      wobjNodoComercio = (org.w3c.dom.Node) null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      if( wobjXMLParametros.getDocument().getChildNodes().getLength() > 0 )
      {
        wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      }
      //
      wvarStep = 110;
      wobjXMLParametros = null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarArea;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, wvarParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG"))
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + wvarParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      //
      wvarStep = 240;
      wvarResult = "";
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos.set( 104 );
      //
      wvarStringLen = Strings.len( wvarParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( wvarParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 285;
        wvarPos.set( 12540 );
        wvarResult = ParseoMensaje(wvarPos, wvarParseString, wvarstrLen, wobjXMLRequest);
        wvarXMLSQLArea = new XmlDomExtended();
        wvarXMLSQLArea.loadXML( wvarResult );
        //
        wvarStep = 286;
        if( wobjXMLRequest.selectSingleNode( mcteParam_Conf_BuscDatosAdicional )  == (org.w3c.dom.Node) null )
        {
          wvarObtenerDatosAdicionales = true;
        }
        else
        {
          wvarObtenerDatosAdicionales = !XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Conf_BuscDatosAdicional )  ).equals( "N" );
        }

        if( wvarObtenerDatosAdicionales )
        {
          // **************************************
          //OBTENCION DATOS ADICIONALES PARA LA COTIZACION REALIZADA
          // **************************************
          wobjXMLResponse = new XmlDomExtended();
          //
          //Obtencion de las condiciones especiales de coberturas
          //
          wvarRequest = "<Request>" + XmlDomExtended.marshal(wvarXMLSQLArea.selectSingleNode( "//ENCABEZADO" )) + "</Request>";
          wobjClass = new Variant( new lbaw_OVIIICondCober() )new lbaw_OVIIICondCober().toObject();
          wobjClass.Execute( wvarRequest, wvarResponse, "" );
          wobjClass = (IAction) null;
          wobjXMLResponse.loadXML( wvarResponse );
          //
          wvarStep = 287;
          if( ! (wobjXMLResponse.selectSingleNode( "//Estado[@resultado='true']" )  == (org.w3c.dom.Node) null) )
          {
            wvarNodop = wobjXMLResponse.selectSingleNode( "//CONDICIONESESPECIALES" ) ;
            if( ! (wvarNodop == (org.w3c.dom.Node) null) )
            {
              /*unsup wvarXMLSQLArea.selectSingleNode( "//ENCABEZADO" ) */.appendChild( wvarNodop );
            }
          }
          //
          //Obtencion de Deducibles para todos los comercios
          //
          wobjNodosComercios = wvarXMLSQLArea.selectNodes( "//COMERCIO" ) ;
          for( int nwobjNodoComercio = 0; nwobjNodoComercio < wobjNodosComercios.getLength(); nwobjNodoComercio++ )
          {
            wobjNodoComercio = wobjNodosComercios.item( nwobjNodoComercio );
            if( wobjNodoComercio.selectSingleNode( (".[./" + mcteParamBAJACOMERCIO + "='S']") )  == (org.w3c.dom.Node) null )
            {
              wvarRequest = "<Request>" + XmlDomExtended.marshal(wvarXMLSQLArea.selectSingleNode( "//ENCABEZADO" )) + wobjNodoComercio.toString() + "</Request>";
              wobjClass = new Variant( new lbaw_OVIIIGetDeducib() )new lbaw_OVIIIGetDeducib().toObject();
              wobjClass.Execute( wvarRequest, wvarResponse, "" );
              wobjClass = (IAction) null;
              wobjXMLResponse.loadXML( wvarResponse );
              //
              wvarStep = 288;
              if( ! (wobjXMLResponse.selectSingleNode( "//Estado[@resultado='true']" )  == (org.w3c.dom.Node) null) )
              {
                wvarNodop = wobjXMLResponse.selectSingleNode( "//DEDUCIBLES" ) ;
                if( ! (wvarNodop == (org.w3c.dom.Node) null) )
                {
                  wobjNodoComercio.appendChild( wvarNodop );
                }
              }
            }
          }
        }

        // **************************************
        //GUARDADO DE DATOS EN SQL
        // **************************************
        //
        if( ! (wobjXMLRequest.selectSingleNode( "//GENERARLOG" )  == (org.w3c.dom.Node) null) )
        {
          wvarXMLSQLArea.save( System.getProperty("user.dir") + "\\" + mcteClassName + "_Resp.xml" );
        }

        //
        wvarStep = 290;
        //
        wvarActualizarCli = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ACTUALIZARCLIENTE )  ).equals( "S" );
        wvarNuevoCliente = wvarXMLSQLArea.selectNodes( "//ENCABEZADO[IDCLIENTE!='']" ) .getLength() == 0;

        if( wobjXMLRequest.selectSingleNode( mcteParam_Conf_GuardarSQLComerc )  == (org.w3c.dom.Node) null )
        {
          wvarGuardarComercio = true;
        }
        else
        {
          wvarGuardarComercio = !XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Conf_GuardarSQLComerc )  ).equals( "N" );
        }
        //
        if( wobjXMLRequest.selectSingleNode( mcteParam_Conf_GuardarSQL )  == (org.w3c.dom.Node) null )
        {
          wvarGuardarCotiz = true;
        }
        else
        {
          wvarGuardarCotiz = !XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Conf_GuardarSQL )  ).equals( "N" );
        }
        //
        if( wvarGuardarCotiz )
        {
          //
          wvarStep = 291;
          wobjXMLResponse = new XmlDomExtended();
          //Guardo primero el Encabezado si no esta informado el IDEncabezado
          if( wvarXMLSQLArea.selectNodes( "//ENCABEZADO[IDENCABEZADO!=0]" ) .getLength() == 0 )
          {
            wvarRequest = "<Request>" + wvarXMLSQLArea.getDocument().getDocumentElement().toString() + wvarValoresIngres + "</Request>";
            //
            wobjClass = new lbawA_OVSQLICO.lbaw_OVICOPutEncab();
            //error: function 'Execute' was not found.
            //unsup: wobjClass.Execute wvarRequest, wvarResponse, ""
            wobjClass = null;
            //
            wvarStep = 292;
            wobjXMLResponse.loadXML( wvarResponse );
            //
            wvarStep = 300;
            if( wobjXMLResponse.selectSingleNode( "//Estado[@resultado='true']" )  == (org.w3c.dom.Node) null )
            {
              Response.set( wobjXMLResponse.getDocument().getDocumentElement().toString() );
              /*TBD mobjCOM_Context.SetComplete() ;*/
              IAction_Execute = 0;
              return IAction_Execute;
            }
            else
            {
              wvarNodop = wvarXMLSQLArea.selectSingleNode( "//ENCABEZADO/IDENCABEZADO" ) ;
              if( wvarNodop == (org.w3c.dom.Node) null )
              {
                wvarNodop = wvarXMLSQLArea.getDocument().createElement( "IDENCABEZADO" );
                /*unsup wvarXMLSQLArea.selectSingleNode( "//ENCABEZADO" ) */.appendChild( wvarNodop );
              }
              XmlDomExtended.setText( wvarNodop, XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//CADENA" )  ) );
            }
          }
          //
          wvarStep = 305;
          //Recorro nuevamente los Comercios y los voy guardando o actualizando segun este o no informado el TAG IDCOMERCIO
          if( wvarGuardarComercio )
          {
            wobjNodosComercios = wvarXMLSQLArea.selectNodes( "//COMERCIO" ) ;
            for( int nwobjNodoComercio = 0; nwobjNodoComercio < wobjNodosComercios.getLength(); nwobjNodoComercio++ )
            {
              wobjNodoComercio = wobjNodosComercios.item( nwobjNodoComercio );
              wvarRequest = "<Request>" + XmlDomExtended.marshal(wvarXMLSQLArea.selectSingleNode( "//ENCABEZADO" )) + wobjNodoComercio.toString() + "</Request>";
              wobjClass = new lbawA_OVSQLICO.lbaw_OVICOPutComercio();
              wobjClass.Execute( wvarRequest, wvarResponse, "" );
              wobjClass = (Object) null;
              wobjXMLResponse.loadXML( wvarResponse );
              //
              wvarStep = 310;
              if( wobjXMLResponse.selectSingleNode( "//Estado[@resultado='true']" )  == (org.w3c.dom.Node) null )
              {
                Response.set( wobjXMLResponse.getDocument().getDocumentElement().toString() );
                /*TBD mobjCOM_Context.SetComplete() ;*/
                IAction_Execute = 0;
                return IAction_Execute;
              }
              else
              {
                wvarNodop = wobjNodoComercio.selectSingleNode( "./IDCOMERCIO" ) ;
                if( wvarNodop == (org.w3c.dom.Node) null )
                {
                  wvarNodop = wvarXMLSQLArea.getDocument().createElement( "IDCOMERCIO" );
                  wobjNodoComercio.appendChild( wvarNodop );
                }
                XmlDomExtended.setText( wvarNodop, XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//CADENA" )  ) );
              }
            }
          }
          //
          if( wvarActualizarCli || wvarNuevoCliente )
          {
            //
            wvarStep = 340;
            wobjXMLResponse = new XmlDomExtended();
            //Actualizo el Cliente o Guardo el Cliente
            if( wvarNuevoCliente )
            {
              //Nuevo Cliente
              wvarRequest = "<Request>" + XmlDomExtended.marshal(wvarXMLSQLArea.selectSingleNode( "//ENCABEZADO" )) + "</Request>";
              wobjClass = new lbawA_ProductorLBA.lbaw_PutPersona();
            }
            else
            {
              //Actualizacion de Datos
              wvarRequest = "<Request><CLIENSEC>" + XmlDomExtended.getText( XmlDomExtended.marshal(wvarXMLSQLArea.selectSingleNode( "//ENCABEZADO/IDCLIENTE" )  ) + "</CLIENSEC>" + wvarXMLSQLArea.selectSingleNode( "//ENCABEZADO" )) + "</Request>";
              wobjClass = new lbawA_ProductorLBA.lbaw_UpdPersona();
            }
            //
            wobjClass.Execute( wvarRequest, wvarResponse, "" );
            wobjClass = (Object) null;
            //
            wvarStep = 350;
            wobjXMLResponse.loadXML( wvarResponse );
            //
            wvarStep = 360;
            if( wobjXMLResponse.selectSingleNode( "//Estado[@resultado='true']" )  == (org.w3c.dom.Node) null )
            {
              Response.set( wobjXMLResponse.getDocument().getDocumentElement().toString() );
              /*TBD mobjCOM_Context.SetComplete() ;*/
              IAction_Execute = 0;
              return IAction_Execute;
            }
            else
            {
              if( wvarNuevoCliente )
              {
                wvarNodop = wvarXMLSQLArea.selectSingleNode( "//IDCLIENTE" ) ;
                if( wvarNodop == (org.w3c.dom.Node) null )
                {
                  wvarNodop = wvarXMLSQLArea.getDocument().createElement( "IDCLIENTE" );
                  /*unsup wvarXMLSQLArea.selectSingleNode( "//ENCABEZADO" ) */.appendChild( wvarNodop );
                }
                XmlDomExtended.setText( wvarNodop, XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//PERSONAID" )  ) );
              }
            }
          }
          wvarStep = 370;

          //ACTUALIZO EL ENCABEZADO CON LOS NUEVOS DATOS
          wvarRequest = "<Request>" + wvarXMLSQLArea.getDocument().getDocumentElement().toString() + wvarValoresIngres + "</Request>";

          wobjClass = new lbawA_OVSQLICO.lbaw_OVICOPutEncab();
          wobjClass.Execute( wvarRequest, wvarResponse, "" );
          wobjClass = (Object) null;

          wobjXMLResponse.loadXML( wvarResponse );

          wvarStep = 380;
          if( wobjXMLResponse.selectSingleNode( "//Estado[@resultado='true']" )  == (org.w3c.dom.Node) null )
          {
            Response.set( wobjXMLResponse.getDocument().getDocumentElement().toString() );
            /*TBD mobjCOM_Context.SetComplete() ;*/
            IAction_Execute = 0;
            return IAction_Execute;
          }
        }
        //
        wvarStep = 400;
        Response.set( "<Response><Estado resultado='true' mensaje=''/>" + wvarXMLSQLArea.getDocument().getDocumentElement().toString() + "</Response>" );
      }
      //
      wvarStep = 320;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private VbCollection GetColFromString( String pvarString ) throws Exception
  {
    VbCollection GetColFromString = null;
    int wvarPos = 0;
    int wVarposAnt = 0;
    GetColFromString = new VbCollection();
    wvarPos = 1;
    wVarposAnt = 1;
    while( Strings.find( wvarPos, pvarString, "|" ) != 0 )
    {
      wvarPos = Strings.find( wvarPos, pvarString, "|" );
      GetColFromString.add( new Variant( Strings.mid( pvarString, wVarposAnt, wvarPos - wVarposAnt ) ) );
      wVarposAnt = wvarPos + 1;
      wvarPos = wvarPos + 1;
    }
    if( !Strings.mid( pvarString, wVarposAnt ).equals( "" ) )
    {
      GetColFromString.add( new Variant( Strings.mid( pvarString, wVarposAnt ) ) );
    }
    return GetColFromString;
  }

  private String GetVectorDatos( org.w3c.dom.Node pobjNodo, String pVarPathVector, String pVarDefinicionSubNodos, int pVarCantElementos ) throws Exception
  {
    String GetVectorDatos = "";
    int wvarCounterRelleno = 0;
    int wvarActualCounter = 0;
    int wvarActualNodoSolicitado = 0;
    Variant wvarNodoNombre = new Variant();
    Variant wvarNodoTipoDato = new Variant();
    Variant wvarNodoLargoDato = new Variant();
    Variant wvarNodoCantDecimales = new Variant();
    VbCollection wobjColNodosSolicitados = null;
    org.w3c.dom.NodeList wvarSelectedNodos = null;

    wobjColNodosSolicitados = GetColFromString(pVarDefinicionSubNodos);
    wvarActualCounter = 1;
    if( ! (pobjNodo == (org.w3c.dom.Node) null) )
    {
      wvarSelectedNodos = pobjNodo.selectNodes( pVarPathVector ) ;

      for( wvarActualCounter = 0; wvarActualCounter <= (wvarSelectedNodos.getLength() - 1); wvarActualCounter++ )
      {
        //Recorro todos los nodos que formaran el vector
        for( wvarActualNodoSolicitado = 1; wvarActualNodoSolicitado <= wobjColNodosSolicitados.getCount(); wvarActualNodoSolicitado++ )
        {
          //Recorro todos los nodos que se solicitan para cada uno
         GetDefinicionElemento(wobjColNodosSolicitados.getItem(wvarActualNodoSolicitado).toString(), wvarNodoNombre, wvarNodoTipoDato, wvarNodoLargoDato, wvarNodoCantDecimales);
          GetVectorDatos = GetVectorDatos + invoke( "GetDatoFormateado", new Variant[] { new Variant(wvarSelectedNodos.item( wvarActualCounter )), new Variant(wvarNodoNombre.toString()), new Variant(wvarNodoTipoDato.toInt()), new Variant(wvarNodoLargoDato.toInt()), new Variant(wvarNodoCantDecimales.toInt()) } );
        }
        if( wvarActualCounter == (pVarCantElementos - 1) )
        {
          return IAction_Execute;
        }
      }
    }
    for( wvarCounterRelleno = wvarActualCounter; wvarCounterRelleno <= (pVarCantElementos - 1); wvarCounterRelleno++ )
    {
      for( wvarActualNodoSolicitado = 1; wvarActualNodoSolicitado <= wobjColNodosSolicitados.getCount(); wvarActualNodoSolicitado++ )
      {
        //Recorro todos los nodos que se solicitan para cada uno
       GetDefinicionElemento(wobjColNodosSolicitados.getItem(wvarActualNodoSolicitado).toString(), wvarNodoNombre, wvarNodoTipoDato, wvarNodoLargoDato, wvarNodoCantDecimales);
        GetVectorDatos = GetVectorDatos + invoke( "GetDatoFormateado", new Variant[] { new Variant(null), new Variant(wvarNodoNombre.toString()), new Variant(wvarNodoTipoDato.toInt()), new Variant(wvarNodoLargoDato.toInt()), new Variant(wvarNodoCantDecimales.toInt()) } );
      }
    }
    ClearObjects: 
    wobjColNodosSolicitados = (VbCollection) null;
    return GetVectorDatos;
  }

  private String GetDatoFormateado( org.w3c.dom.Node pobjXMLContenedor, String pvarPathDato, int pvarTipoDato, int pvarLongitud, int pvarDecimales ) throws Exception
  {
    String GetDatoFormateado = "";
    org.w3c.dom.Node wobjNodoValor = null;
    String wvarDatoValue = "";
    int wvarCounter = 0;
    double wvarCampoNumerico = 0.0;
    String[] wvarArrFecha = null;

    if( ! (pobjXMLContenedor == (org.w3c.dom.Node) null) )
    {
      wobjNodoValor = pobjXMLContenedor.selectSingleNode( pvarPathDato ) ;
      if( ! (wobjNodoValor == (org.w3c.dom.Node) null) )
      {
        wvarDatoValue = XmlDomExtended.getText( wobjNodoValor );
      }
    }

    
    if( pvarTipoDato == 0 )
    {
      //Dato del Tipo String
      GetDatoFormateado = Strings.left( wvarDatoValue + Strings.space( pvarLongitud ), pvarLongitud );
    }
    else if( pvarTipoDato == 1 )
    {
      //Dato del Tipo Entero
      GetDatoFormateado = CompleteZero(wvarDatoValue, pvarLongitud);
    }
    else if( pvarTipoDato == 2 )
    {
      //Dato del Tipo "Con Decimales"
      if( new Variant( wvarDatoValue ).isNumeric() )
      {
        wvarCampoNumerico = Obj.toDouble( wvarDatoValue );
      }
      for( wvarCounter = 1; wvarCounter <= pvarDecimales; wvarCounter++ )
      {
        wvarCampoNumerico = wvarCampoNumerico * 10;
      }
      GetDatoFormateado = invoke( "CompleteZero", new Variant[] { new Variant(String.valueOf( new Variant(wvarCampoNumerico) )), new Variant(pvarLongitud + pvarDecimales) } );
    }
    else if( pvarTipoDato == 3 )
    {
      //Dato del Tipo "Fecha" Debe venir del tipo dd/mm/yyyy devuelve YYYYMMDD
      if( wvarDatoValue.matches( "*/*/*" ) )
      {
        wvarArrFecha = Strings.split( wvarDatoValue, "/", -1 );
        GetDatoFormateado = invoke( "CompleteZero", new Variant[] { new Variant(wvarArrFecha[2]), new Variant(4) } ) + invoke( "CompleteZero", new Variant[] { new Variant(wvarArrFecha[1]), new Variant(2) } ) + invoke( "CompleteZero", new Variant[] { new Variant(wvarArrFecha[0]), new Variant(2) } );
      }
      else
      {
        GetDatoFormateado = "00000000";
      }
    }

    return GetDatoFormateado;
  }

  private String CompleteZero( String pvarString, int pvarLongitud ) throws Exception
  {
    String CompleteZero = "";
    int wvarCounter = 0;
    String wvarstrTemp = "";
    for( wvarCounter = 1; wvarCounter <= pvarLongitud; wvarCounter++ )
    {
      wvarstrTemp = wvarstrTemp + "0";
    }
    CompleteZero = Strings.right( wvarstrTemp + pvarString, pvarLongitud );
    return CompleteZero;
  }

  private void GetDefinicionElemento( String pvarStringElemento, Variant pvarElemento, Variant pvarTipoDato, Variant pvarLongitud, Variant pvarDecimales ) throws Exception
  {
    String[] wvarArray = null;
    wvarArray = Strings.split( pvarStringElemento, ",", -1 );
    pvarElemento.set( wvarArray[0] );
    pvarTipoDato.set( wvarArray[1] );
    pvarLongitud.set( wvarArray[2] );
    pvarDecimales.set( wvarArray[3] );
  }

  private String ParseoMensaje( Variant pvarPos, String pstrParseString, int pvarstrLen, diamondedge.util.XmlDom pobjXMLRequest ) throws Exception
  {
    String ParseoMensaje = "";
    org.w3c.dom.NodeList wobjNodosComercios = null;
    org.w3c.dom.Node wobjNodoComercio = null;
    org.w3c.dom.Element wobjXmlElemenResp = null;
    org.w3c.dom.Element wobjXmlElemenRespE = null;
    org.w3c.dom.Element wobjXmlElemenRespG = null;
    org.w3c.dom.Element wobjXmlElemenRespC = null;
    int wvarCcurrRegistro = 0;
    String wvarPrimaTotal = "";
    String wvarComisionProduc = "";
    String wvarCodigoCobertura = "";
    String wvarPrecioTotal = "";
    String wvarCapitalAsegurado = "";
    String wvarPorcentajeTarifa = "";
    String wvarPrimaCobertura = "";
    //
    //
    //
    wobjXmlElemenResp = pobjXMLRequest.getDocument().createElement( "GENERAL" );
    /*unsup pobjXMLRequest.selectSingleNode( "//COTIZACION" ) */.appendChild( wobjXmlElemenResp );
    //
    wvarPrimaTotal = invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } );
    wobjXmlElemenRespG = pobjXMLRequest.getDocument().createElement( "PRIMATOTAL" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespG );
    XmlDomExtended.setText( wobjXmlElemenRespG, wvarPrimaTotal );
    //
    wobjXmlElemenRespC = pobjXMLRequest.getDocument().createElement( "COMPOSICIONPRECIO" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespC );
    //
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "PRIMAIMP" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, wvarPrimaTotal );
    //
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "DEREMIMP" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } ) );
    //
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "RECAIMPO" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } ) );
    //
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "RECAFIMPO" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } ) );
    //
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "TASUIMPO" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } ) );
    //
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "IVAIMPOR" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } ) );
    //
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "IVAIBASE" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } ) );
    //
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "IVAIMPOA" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } ) );
    //
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "IVAABASE" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } ) );
    //
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "IVARETEN" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } ) );
    //
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "IVARBASE" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } ) );
    //
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "IMPUEIMP" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } ) );
    //
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "SELLAIMP" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } ) );
    //
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "INGBRIMP" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } ) );
    //
    wvarPrecioTotal = invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } );
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "RECTOIMP" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, wvarPrecioTotal );
    //
    wvarComisionProduc = invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } );
    wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "COMISIMP" );
    wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
    XmlDomExtended.setText( wobjXmlElemenRespE, wvarComisionProduc );
    //
    wobjXmlElemenRespG = pobjXMLRequest.getDocument().createElement( "PRECIOTOTAL" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespG );
    XmlDomExtended.setText( wobjXmlElemenRespG, wvarPrecioTotal );
    //
    wobjXmlElemenRespG = pobjXMLRequest.getDocument().createElement( "COMISIONPRODUCTOR" );
    wobjXmlElemenResp.appendChild( wobjXmlElemenRespG );
    XmlDomExtended.setText( wobjXmlElemenRespG, wvarComisionProduc );

    //Recorro los comercios y les agrego lo correspondiente a la cotizacion de cada uno
    wobjNodosComercios = pobjXMLRequest.selectNodes( "//COMERCIO" ) ;
    for( int nwobjNodoComercio = 0; nwobjNodoComercio < wobjNodosComercios.getLength(); nwobjNodoComercio++ )
    {
      wobjNodoComercio = wobjNodosComercios.item( nwobjNodoComercio );
      if( wobjNodoComercio.selectSingleNode( (".[./" + mcteParamBAJACOMERCIO + "='S']") )  == (org.w3c.dom.Node) null )
      {
        //Devuelven 30 Coberturas por Comercio
        for( wvarCcurrRegistro = 1; wvarCcurrRegistro <= 30; wvarCcurrRegistro++ )
        {
          wvarCodigoCobertura = ModGeneral.MidAsString( pstrParseString, pvarPos, 3 );
          wvarCapitalAsegurado = invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } );
          wvarPorcentajeTarifa = invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(5) )), new Variant(2) } );
          wvarPrimaCobertura = invoke( "FormatearDecimales", new Variant[] { new Variant(ModGeneral.MidAsString( new Variant(pstrParseString), new Variant(pvarPos), new Variant(15) )), new Variant(2) } );
          if( VB.val( wvarCodigoCobertura ) != 0 )
          {
            wobjXmlElemenRespC = (org.w3c.dom.Element) wobjNodoComercio.selectSingleNode( "./C" + wvarCodigoCobertura ) ;
            if( wobjXmlElemenRespC == (org.w3c.dom.Element) null )
            {
              wobjXmlElemenRespC = (org.w3c.dom.Element) wobjNodoComercio.selectSingleNode( "./c" + wvarCodigoCobertura ) ;
            }
            if( wobjXmlElemenRespC == (org.w3c.dom.Element) null )
            {
              wobjXmlElemenRespC = pobjXMLRequest.getDocument().createElement( "C" + wvarCodigoCobertura );
              wobjNodoComercio.appendChild( wobjXmlElemenRespC );
            }
            wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "CODIGO" );
            wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
            XmlDomExtended.setText( wobjXmlElemenRespE, wvarCodigoCobertura );
            //
            wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "SUMAASEGURADA" );
            wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
            XmlDomExtended.setText( wobjXmlElemenRespE, wvarCapitalAsegurado );
            //
            wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "TASA" );
            wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
            XmlDomExtended.setText( wobjXmlElemenRespE, wvarPorcentajeTarifa );
            //
            wobjXmlElemenRespE = pobjXMLRequest.getDocument().createElement( "PRIMA" );
            wobjXmlElemenRespC.appendChild( wobjXmlElemenRespE );
            XmlDomExtended.setText( wobjXmlElemenRespE, wvarPrimaCobertura );
          }
        }
      }
    }

    ParseoMensaje = XmlDomExtended.marshal(pobjXMLRequest.selectSingleNode( "//COTIZACION" ));
    wobjNodosComercios = (org.w3c.dom.NodeList) null;
    wobjNodoComercio = (org.w3c.dom.Node) null;
    wobjXmlElemenResp = (org.w3c.dom.Element) null;
    wobjXmlElemenRespE = (org.w3c.dom.Element) null;
    wobjXmlElemenRespG = (org.w3c.dom.Element) null;
    wobjXmlElemenRespC = (org.w3c.dom.Element) null;
    //
    return ParseoMensaje;
  }

  private String FormatearDecimales( String pvarValor, int pvarCantDecimales ) throws Exception
  {
    String FormatearDecimales = "";
    double wvarValorNumerico = 0.0;
    wvarValorNumerico = VB.val( pvarValor );
    FormatearDecimales = String.valueOf( wvarValorNumerico / Math.pow( 10, pvarCantDecimales ) );
    return FormatearDecimales;
  }

  private void ConvertirXML( XmlDomExtended pobjXMLEntrada ) throws Exception
  {
    org.w3c.dom.NodeList wobjNodosAEvaluar = null;
    org.w3c.dom.Node wobjSubNodo = null;
    org.w3c.dom.Element wobjNodoLocal = null;
    org.w3c.dom.Element wobjCotizacion = null;
    org.w3c.dom.Element wobjEncabezado = null;
    org.w3c.dom.Element wobjComercios = null;
    org.w3c.dom.Node wobjComercio = null;
    org.w3c.dom.Element wobjNewNodo = null;
    org.w3c.dom.Element wobjNroComercio = null;
    org.w3c.dom.Attr wobjXmlAtributo = null;
    String wvarModuloGeneral = "";

    //Genero los elementos de la cotizacion
    wobjCotizacion = pobjXMLEntrada.getDocument().createElement( "COTIZACION" );
    /*unsup pobjXMLEntrada.selectSingleNode( "//Request" ) */.appendChild( wobjCotizacion );
    //
    wobjEncabezado = pobjXMLEntrada.getDocument().createElement( "ENCABEZADO" );
    wobjCotizacion.appendChild( wobjEncabezado );
    //
    wobjComercios = pobjXMLEntrada.getDocument().createElement( "COMERCIOS" );
    wobjCotizacion.appendChild( wobjComercios );
    //
    /*unsup pobjXMLEntrada.selectNodes( mcteParam_ValoresIngresados + "/dato" ) */;
    //
    //Copio los parametros sueltos en el Tag ENCABEZADO
    wobjNodosAEvaluar = pobjXMLEntrada.getDocument().getChildNodes().item( 0 ).getChildNodes();
    for( int nwobjSubNodo = 0; nwobjSubNodo < wobjNodosAEvaluar.getLength(); nwobjSubNodo++ )
    {
      wobjSubNodo = wobjNodosAEvaluar.item( nwobjSubNodo );
      if( (!Strings.toUpperCase( wobjSubNodo.getNodeName() ).equals( "VALORES" )) && (!Strings.toUpperCase( wobjSubNodo.getNodeName() ).equals( "COTIZACION" )) )
      {
        wobjNewNodo = pobjXMLEntrada.getDocument().createElement( wobjSubNodo.getNodeName() );
        XmlDomExtended.setText( wobjNewNodo, XmlDomExtended.getText( wobjSubNodo ) );
        wobjEncabezado.appendChild( wobjNewNodo );
      }
    }
    //
    wobjNodosAEvaluar = pobjXMLEntrada.selectNodes( mcteParam_ValoresIngresados + "/dato" ) ;
    for( int nwobjSubNodo = 0; nwobjSubNodo < wobjNodosAEvaluar.getLength(); nwobjSubNodo++ )
    {
      wobjSubNodo = wobjNodosAEvaluar.item( nwobjSubNodo );
      //Dato del Encabezado
      wobjNewNodo = pobjXMLEntrada.getDocument().createElement( XmlDomExtended .getText( wobjSubNodo.selectSingleNode( "./nombre" )  ) );
      //wobjNewNodo.Text = wobjSubNodo.selectSingleNode(".//valor").Text
      XmlDomExtended.setText( wobjNewNodo, Strings.toUpperCase( XmlDomExtended .getText( wobjSubNodo.selectSingleNode( ".//valor" )  ) ) );
      wobjNodoLocal = (org.w3c.dom.Element) wobjSubNodo.selectSingleNode( "./local" ) ;
      if( wobjNodoLocal == (org.w3c.dom.Element) null )
      {
        wobjEncabezado.appendChild( wobjNewNodo );
      }
      else
      {
        wobjComercio = (org.w3c.dom.Element) wobjComercios.selectSingleNode( "./COMERCIO[NROCOMERCIO='" + XmlDomExtended.getText( wobjNodoLocal ) + "']" ) ;
        if( wobjComercio == (org.w3c.dom.Node) null )
        {
          wobjComercio = pobjXMLEntrada.getDocument().createElement( "COMERCIO" );
          wobjComercios.appendChild( wobjComercio );
          wobjNroComercio = pobjXMLEntrada.getDocument().createElement( "NROCOMERCIO" );
          XmlDomExtended.setText( wobjNroComercio, XmlDomExtended.getText( wobjNodoLocal ) );
          wobjComercio.appendChild( wobjNroComercio );
        }
        wobjComercio.appendChild( wobjNewNodo );
        if( Strings.toUpperCase( wobjNewNodo.getNodeName() ).matches( "C#*" ) && (VB.val( XmlDomExtended .getText( wobjNewNodo ) ) == 0) )
        {
          wobjComercio.removeChild( wobjNewNodo );
        }
        else
        {
          if( Strings.toUpperCase( wobjNewNodo.getNodeName() ).matches( "C#*" ) )
          {
            wvarModuloGeneral = XmlDomExtended.getText( null /*unsup null (*unsup pobjXMLEntrada.selectSingleNode( mcteParam_ValoresIngresados + "/dato[./nombre='I_CODIGOMODULO' and ./local='" + XmlDomExtended.getText( wobjNodoLocal ) + "']" ) *).selectSingleNode( "./valor" ) */ );
            wobjXmlAtributo = pobjXMLEntrada.getDocument().createAttribute( "CodModulo" );
            wobjNewNodo.setAttributeNode( wobjXmlAtributo );
            XmlDomExtended.setText( wobjXmlAtributo, invoke( "GetModulo", new Variant[] { new Variant(Obj.toInt( new Variant(wvarModuloGeneral) )), new Variant(Strings.mid( new Variant(wobjNewNodo.getNodeName()), new Variant(2) )), new Variant(Obj.toDouble( new Variant( XmlDomExtended .getText( wobjNewNodo )) )), new Variant(pobjXMLEntrada.selectSingleNode( new Variant(mcteParam_XML_Modulos) ) ) } ).toString() );
          }
        }
      }
    }
    //
    //Verifico que todos los comercios informados tengan la estructura correcta.
    //Si no viene informado el Nro de Modulo ni la marca que se quiere dar de baja, lo elimino directamente
    for( int nwobjComercio = 0; nwobjComercio < pobjXMLEntrada.selectNodes( "//COMERCIO" ) .getLength(); nwobjComercio++ )
    {
      wobjComercio = pobjXMLEntrada.selectNodes( "//COMERCIO" ) .item( nwobjComercio );
      if( (wobjComercio.selectSingleNode( "./I_CODIGOMODULO" )  == (org.w3c.dom.Node) null) && (wobjComercio.selectSingleNode( "./BAJA" )  == (org.w3c.dom.Node) null) )
      {
        wobjComercio.getParentNode().removeChild( wobjComercio );
      }
    }
    //
    wobjXmlAtributo = (org.w3c.dom.Attr) null;
    wobjNodosAEvaluar = (org.w3c.dom.NodeList) null;
    wobjSubNodo = (org.w3c.dom.Node) null;
    wobjNodoLocal = (org.w3c.dom.Element) null;
    wobjCotizacion = (org.w3c.dom.Element) null;
    wobjEncabezado = (org.w3c.dom.Element) null;
    wobjComercios = (org.w3c.dom.Element) null;
    wobjComercio = (org.w3c.dom.Node) null;
    wobjNewNodo = (org.w3c.dom.Element) null;
    wobjNroComercio = (org.w3c.dom.Element) null;
  }

  private int GetModulo( int pvarModuloGeneral, String pvarCodigoCobertura, double pvarImporteIngresado, org.w3c.dom.Element pobjNodoModulos ) throws Exception
  {
    int GetModulo = 0;
    org.w3c.dom.Node wobjNodoCobertura = null;
    org.w3c.dom.NodeList wobjNodosCobertura = null;
    GetModulo = 0;
    wobjNodosCobertura = pobjNodoModulos.selectNodes( "//cobertura[@Codigo='" + pvarCodigoCobertura + "']" ) ;
    for( int nwobjNodoCobertura = 0; nwobjNodoCobertura < wobjNodosCobertura.getLength(); nwobjNodoCobertura++ )
    {
      wobjNodoCobertura = wobjNodosCobertura.item( nwobjNodoCobertura );
      if( XmlDomExtended .getText( wobjNodoCobertura.getAttributes().getNamedItem( "Determinante" ) ).equals( "I" ) )
      {
        //Solo se busca el Modulo para las coberturas que son independientes, sino se devuelve el modulo principal
        if( pvarImporteIngresado >= Obj.toDouble( Strings.replace( XmlDomExtended .getText( wobjNodoCobertura.getAttributes().getNamedItem( "Min" ) ), ",", "." ) ) )
        {
          if( pvarImporteIngresado <= Obj.toDouble( Strings.replace( XmlDomExtended .getText( wobjNodoCobertura.getAttributes().getNamedItem( "Max" ) ), ",", "." ) ) )
          {
            GetModulo = Obj.toInt( XmlDomExtended .getText( wobjNodoCobertura.getParentNode().getAttributes().getNamedItem( "Codigo" ) ) );
          }
        }
      }
      else
      {
        GetModulo = pvarModuloGeneral;
      }
    }
    //
    wobjNodoCobertura = (org.w3c.dom.Node) null;
    wobjNodosCobertura = (org.w3c.dom.NodeList) null;
    return GetModulo;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
