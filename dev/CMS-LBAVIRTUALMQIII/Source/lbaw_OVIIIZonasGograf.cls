VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVIIIZonasGeograf"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context             As ObjectContext
Private mobjEventLog                As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbawA_OVMQIII.lbaw_OVIIIZonasGeograf"
Const mcteOpID                      As String = "1556"

'Parametros XML de Entrada
Const mcteParam_Usuario             As String = "//USUARIO"
Const mcteParam_CodRamo             As String = "//RAMOPCOD"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLResponseProv As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjNodosZonas      As MSXML2.IXMLDOMNodeList
    Dim wobjNodoZona        As MSXML2.IXMLDOMElement
    Dim wobjZonaAppend      As MSXML2.IXMLDOMElement
    Dim wobjClass           As IAction
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarCodigRamo       As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarCodigRamo = Left(.selectSingleNode(mcteParam_CodRamo).Text & Space(4), 4)
        '
    End With
    '
    wvarStep = 70
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 100
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 110
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 120
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarCodigRamo
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 240
    wvarResult = ""
    
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 250
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 260
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 280
        wvarResult = ""
        wvarPos = 71 'cantidad de caracteres ocupados por parámetros de entrada
        wvarResult = ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        wobjXMLResponse.loadXML "<RS><ZONAS>" & wvarResult & "</ZONAS><PROVINCIAS></PROVINCIAS></RS>"
        
        'BUSCO LAS PROVINCIAS DE CADA ZONA
        Set wobjXMLResponseProv = CreateObject("MSXML2.DOMDocument")
        Set wobjNodosZonas = wobjXMLResponse.selectNodes("//ZONAS/option")
        For Each wobjNodoZona In wobjNodosZonas
            '
            Set wobjZonaAppend = wobjXMLRequest.selectSingleNode("//ZONA")
            If wobjZonaAppend Is Nothing Then
                Set wobjZonaAppend = wobjXMLRequest.createElement("ZONA")
                wobjXMLRequest.childNodes(0).appendChild wobjZonaAppend
            End If
            
            wobjZonaAppend.Text = wobjNodoZona.Attributes.getNamedItem("value").Text
            Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVMQIII.lbaw_OVIIIGetProvincias")
            wobjClass.Execute wobjXMLRequest.xml, wvarResponse, ""
            Set wobjClass = Nothing
            wobjXMLResponseProv.loadXML wvarResponse
            '
            Set wobjZonaAppend = wobjXMLResponse.createElement("ZONA")
            wobjXMLResponse.selectSingleNode("//PROVINCIAS").appendChild wobjZonaAppend
            wobjZonaAppend.setAttribute "Codigo", wobjNodoZona.Attributes.getNamedItem("value").Text
            wobjXMLResponseProv.selectSingleNode("//Estado").parentNode.removeChild wobjXMLResponseProv.selectSingleNode("//Estado")
            wobjZonaAppend.appendChild wobjXMLResponseProv.childNodes(0)
            '
        Next wobjNodoZona
        '
        wvarStep = 340
        Response = "<Response><Estado resultado='true' mensaje='' />" & wobjXMLResponse.xml & "</Response>"
        '
    End If
    '
    wvarStep = 360
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCodigRamo & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(pvarPos As Long, pstrParseString As String, pvarstrLen As Long) As String
Dim wobjRegExp          As Object 'RegExp
Dim wobjColMatch        As Object 'MatchCollection
Dim wobjMatch           As Object 'Match
Dim wvarParseEvalString As String
Dim wobjXmlDOMResp      As DOMDocument
Dim wobjXslDOMResp      As DOMDocument
Dim wobjXmlElemenResp   As IXMLDOMElement
Dim wobjXmlElemenRespP  As IXMLDOMElement
Dim wvarCantRegistros   As Long
Dim wvarCcurrRegistro   As Long
Dim wvarCodZona         As String
'
    Set wobjRegExp = CreateObject("VbScript.RegExp")
    wobjRegExp.Global = True
    '
    wvarCantRegistros = Val(Mid(pstrParseString, pvarPos, 3))
    wvarCcurrRegistro = 0
    wvarParseEvalString = Mid(pstrParseString, pvarPos + 3) 'Elimino el parametro que indica la cantidad de registros devueltos
    wvarParseEvalString = Replace(wvarParseEvalString, " ", "_")
    
    wobjRegExp.Pattern = "(\S{4})(\S{30})"
    
    Set wobjColMatch = wobjRegExp.Execute(wvarParseEvalString)
    '
    Set wobjXmlDOMResp = CreateObject("msxml2.DomDocument")
    Set wobjXslDOMResp = CreateObject("msxml2.DomDocument")
    
    Call wobjXmlDOMResp.loadXML("<RS></RS>")
    '
    
    For Each wobjMatch In wobjColMatch
    '
        wvarCodZona = Trim(Replace(wobjMatch.SubMatches(0), "_", " "))
        If Trim(wvarCodZona) <> "" And wvarCcurrRegistro < wvarCantRegistros Then
            wvarCcurrRegistro = wvarCcurrRegistro + 1
            Set wobjXmlElemenResp = wobjXmlDOMResp.createElement("ZONA")
            wobjXmlDOMResp.selectSingleNode("//RS").appendChild wobjXmlElemenResp
            
            Set wobjXmlElemenRespP = wobjXmlDOMResp.createElement("CODIGO")
            wobjXmlElemenResp.appendChild wobjXmlElemenRespP
            If IsNumeric(wvarCodZona) Then wvarCodZona = Val(wvarCodZona)
            wobjXmlElemenRespP.Text = wvarCodZona
            
            Set wobjXmlElemenRespP = wobjXmlDOMResp.createElement("DESCRIPCION")
            wobjXmlElemenResp.appendChild wobjXmlElemenRespP
            wobjXmlElemenRespP.Text = UCase(Trim(Replace(wobjMatch.SubMatches(1), "_", " ")))
        End If
    '
    Next wobjMatch
    '
    wobjXslDOMResp.async = False
    wobjXslDOMResp.loadXML p_GetXSL()
    ParseoMensaje = Replace(wobjXmlDOMResp.transformNode(wobjXslDOMResp), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
    'ParseoMensaje = wobjXmlDOMResp.xml
    '
    Set wobjXmlDOMResp = Nothing
    Set wobjXmlElemenResp = Nothing
    Set wobjXmlElemenRespP = Nothing
    Set wobjXslDOMResp = Nothing
    '
    Set wobjRegExp = Nothing
    
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<?xml version='1.0' encoding='UTF-8'?>"
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>"
    wvarStrXSL = wvarStrXSL & "<xsl:output method='html' version='1.0' encoding='UTF-8' indent='yes'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:template match='/' >"
    wvarStrXSL = wvarStrXSL & "<xsl:for-each select='//ZONA'>"
    wvarStrXSL = wvarStrXSL & "<xsl:sort select='./DESCRIPCION'/>"
    wvarStrXSL = wvarStrXSL & "     <option>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='value'><xsl:value-of select='CODIGO'/></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='descripcion'><xsl:value-of select='DESCRIPCION'/></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "     <xsl:value-of select='DESCRIPCION'/>"
    wvarStrXSL = wvarStrXSL & "     </option>"
    wvarStrXSL = wvarStrXSL & "     </xsl:for-each>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template >"
    wvarStrXSL = wvarStrXSL & "     </xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
    '
End Function

