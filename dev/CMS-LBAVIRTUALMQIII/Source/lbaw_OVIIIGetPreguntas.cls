VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVIIIGetPreguntas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context             As ObjectContext
Private mobjEventLog                As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbawA_OVMQIII.lbaw_OVIIIGetPreguntas"
Const mcteOpID                      As String = "1560"

'Parametros XML de Entrada
Const mcteParam_Usuario             As String = "//USUARIO"
Const mcteParam_Codigo_Ramo         As String = "//RAMOPCOD"
Const mcteParam_Codigo_Actividad    As String = "//ACTIVIDAD"
Const mcteParam_Codigo_Zona         As String = "//ZONA"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarCodRamo         As String
    Dim wvarCodActividad    As String
    Dim wvarCodZona         As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarCodRamo = Left(.selectSingleNode(mcteParam_Codigo_Ramo).Text & Space(4), 4)
        wvarCodActividad = Left(.selectSingleNode(mcteParam_Codigo_Actividad).Text & Space(6), 6)
        wvarCodZona = Right("0000" & .selectSingleNode(mcteParam_Codigo_Zona).Text, 4)
        '
    End With
    '
    wvarStep = 60
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 70
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 100
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 110
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 120
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarCodRamo & wvarCodActividad & wvarCodZona
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 240
    '
parsear:
        wvarstrLen = Len(strParseString)
        '
        wvarStep = 250
        wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
        '
        If wvarEstado = "ER" Then
            '
            wvarStep = 260
            Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
            '
        Else
            '
            wvarStep = 280
            wvarResult = ""
            wvarPos = 81  'cantidad de caracteres ocupados por parámetros de entrada
            wvarResult = ParseoMensaje(wvarPos, strParseString, wvarstrLen)
            '
            wvarStep = 340
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
            '
        End If
    '
    
    wvarStep = 360
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCodRamo & wvarCodActividad & wvarCodZona & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(pvarPos As Long, pstrParseString As String, pvarstrLen As Long) As String
Dim wvarResult          As String
'
Dim wobjRegExp          As Object 'RegExp
Dim wobjColMatch        As Object 'MatchCollection
Dim wobjMatch           As Object 'Match
Dim wobjRegSubexp       As Object 'RegExp
Dim wobjColSubMatch     As Object 'MatchCollection
Dim wobjsubMatch        As Object 'Match
Dim wvarParseEvalString As String
Dim i                   As Long
'
Dim wobjXmlDOMResp      As DOMDocument
Dim wobjXmlElemenResp   As IXMLDOMElement
Dim wobjXmlElemenRespV  As IXMLDOMElement
Dim wobjXmlElemenRespC  As IXMLDOMElement
Dim wobjXmlElemenRespCe As IXMLDOMElement
Dim wobjXmlElemenRespVe As IXMLDOMElement
Dim wobjXmlElemenRespPr As IXMLDOMElement
Dim wobjXmlElemenValPos As IXMLDOMElement
Dim wobjXmlElemenValor  As IXMLDOMElement
Dim wobjXmlAtributo     As IXMLDOMAttribute
Dim wvarCantRegistros   As Long
Dim wvarCcurrRegistro   As Long
Dim wvarCobertura       As String
Dim wvarCodPregunta     As String
Dim wvarCantPreguntas   As Long
Dim wvarCcurrPregunta   As Long
Dim wvarCantLeyendas    As Long

'
    Set wobjRegExp = CreateObject("VbScript.RegExp")
    wobjRegExp.Global = True
    
    Set wobjRegSubexp = CreateObject("VbScript.RegExp")
    wobjRegSubexp.Global = True

    '
    
    wobjRegExp.Pattern = "(\S{3})(\S{3})(\S{1080})"
    wobjRegSubexp.Pattern = "(\S{4})(\S{4})(\S{3})(\S{124})"
    
    '
    wvarParseEvalString = Mid(pstrParseString, pvarPos + 3)
    wvarParseEvalString = Replace(wvarParseEvalString, " ", "_")
    wvarCantRegistros = Val(Mid(pstrParseString, pvarPos, 3))
    wvarCcurrRegistro = 0
    
    Set wobjColMatch = wobjRegExp.Execute(wvarParseEvalString)
    '
    Set wobjXmlDOMResp = CreateObject("msxml2.DomDocument")
    
    Call wobjXmlDOMResp.loadXML("<preguntas></preguntas>")
    
    '
    For Each wobjMatch In wobjColMatch
    '
        If Not wvarCcurrRegistro < wvarCantRegistros Then Exit For
        wvarCcurrRegistro = wvarCcurrRegistro + 1
        '
        wvarCobertura = Val(Replace(wobjMatch.SubMatches(0), "_", " "))
        wvarCantPreguntas = Val(Replace(wobjMatch.SubMatches(1), "_", " "))
        wvarCcurrPregunta = 0
        
        Set wobjColSubMatch = wobjRegSubexp.Execute(wobjMatch.SubMatches(2))
        For Each wobjsubMatch In wobjColSubMatch
        '
            If Not wvarCcurrPregunta < wvarCantPreguntas Then Exit For
            wvarCcurrPregunta = wvarCcurrPregunta + 1
            '
            wvarCodPregunta = Val(Replace(wobjsubMatch.SubMatches(1), "_", " "))
            
            Set wobjXmlElemenRespPr = wobjXmlDOMResp.selectSingleNode("//preguntas/pregunta[@Codigo=" & wvarCodPregunta & "]")
            '
            If wobjXmlElemenRespPr Is Nothing Then
                'Si no existe el Modulo lo genero
                Set wobjXmlElemenRespPr = wobjXmlDOMResp.createElement("pregunta")
                wobjXmlDOMResp.selectSingleNode("//preguntas").appendChild wobjXmlElemenRespPr
                
                Set wobjXmlElemenValPos = wobjXmlDOMResp.createElement("valoresposibles")
                wobjXmlElemenRespPr.appendChild wobjXmlElemenValPos
                
                'Estas preguntas son del tipo SI-NO, genero los posibles valores desde aca
                
                Set wobjXmlElemenValor = wobjXmlDOMResp.createElement("OPTION")
                wobjXmlElemenValor.Text = "NO"
                wobjXmlElemenValPos.appendChild wobjXmlElemenValor
                Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("value")
                wobjXmlElemenValor.setAttributeNode wobjXmlAtributo
                wobjXmlAtributo.Text = "N"
                
                Set wobjXmlElemenValor = wobjXmlDOMResp.createElement("OPTION")
                wobjXmlElemenValor.Text = "SI"
                wobjXmlElemenValPos.appendChild wobjXmlElemenValor
                Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("value")
                wobjXmlElemenValor.setAttributeNode wobjXmlAtributo
                wobjXmlAtributo.Text = "S"
                
                Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Codigo")
                wobjXmlElemenRespPr.setAttributeNode wobjXmlAtributo
                wobjXmlAtributo.Text = wvarCodPregunta
                
                Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("OrdenEnPantalla")
                wobjXmlElemenRespPr.setAttributeNode wobjXmlAtributo
                wobjXmlAtributo.Text = Val(Replace(wobjsubMatch.SubMatches(0), "_", " "))
                               
                Set wobjXmlElemenRespV = wobjXmlDOMResp.createElement("leyendas")
                wobjXmlElemenRespPr.appendChild wobjXmlElemenRespV
                '
                wvarCantLeyendas = Val(Replace(wobjsubMatch.SubMatches(2), "_", " "))
                
                For i = 0 To wvarCantLeyendas - 1
                    Set wobjXmlElemenRespVe = wobjXmlDOMResp.createElement("leyenda")
                    wobjXmlElemenRespV.appendChild wobjXmlElemenRespVe
                    
                    Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Item")
                    wobjXmlAtributo.Text = Mid(Replace(wobjsubMatch.SubMatches(3), "_", " "), (i * 62) + 1, 2)
                    wobjXmlElemenRespVe.setAttributeNode wobjXmlAtributo
                    
                    Set wobjXmlAtributo = wobjXmlDOMResp.createAttribute("Descripcion")
                    wobjXmlAtributo.Text = Trim(Mid(Replace(wobjsubMatch.SubMatches(3), "_", " "), (i * 62) + 1 + 2, 60))
                    wobjXmlElemenRespVe.setAttributeNode wobjXmlAtributo
                    
                Next i
                
                Set wobjXmlElemenRespC = wobjXmlDOMResp.createElement("coberturasactivantes")
                wobjXmlElemenRespPr.appendChild wobjXmlElemenRespC
                
            End If
            
            Set wobjXmlElemenRespCe = wobjXmlDOMResp.createElement("cobertura")
            wobjXmlDOMResp.selectSingleNode("//preguntas/pregunta[@Codigo=" & wvarCodPregunta & "]/coberturasactivantes").appendChild wobjXmlElemenRespCe
            wobjXmlElemenRespCe.Text = wvarCobertura
        '
        Next wobjsubMatch
    '
    Next wobjMatch
    '
    ParseoMensaje = wobjXmlDOMResp.xml
    '
    Set wobjXmlElemenValor = Nothing
    Set wobjXmlElemenValPos = Nothing
    Set wobjXmlElemenResp = Nothing
    Set wobjXmlElemenRespV = Nothing
    Set wobjXmlElemenRespC = Nothing
    Set wobjXmlElemenRespCe = Nothing
    Set wobjXmlElemenRespVe = Nothing
    Set wobjXmlElemenRespPr = Nothing
    '
    Set wobjRegExp = Nothing
    Set wobjRegSubexp = Nothing
    '
End Function

