VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVIIIActividades"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context             As ObjectContext
Private mobjEventLog                As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbawA_OVMQIII.lbaw_OVIIIActividades"
Const mcteOpID                      As String = "1506"

'Parametros XML de Entrada
Const mcteParam_Usuario             As String = "//USUARIO"
Const mcteParam_Actividad_A_Buscar  As String = "//FILTROACTIVIDAD"
Const mcteParam_Codigo_Rubro        As String = "//RAMOPCOD"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wobjNodoActividad   As MSXML2.IXMLDOMNode
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarActividadBuscar As String
    Dim wvarCodRubro        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarCodRubro = Left(.selectSingleNode(mcteParam_Codigo_Rubro).Text & Space(4), 4)
        For Each wobjNodoActividad In .selectNodes(mcteParam_Actividad_A_Buscar)
            If Len(wvarActividadBuscar) <= 60 * 60 Then
                'Solo se permite ingresar 60 descripciones de 60 caracteres cada una
                wvarActividadBuscar = wvarActividadBuscar & Left(wobjNodoActividad.Text & Space(60), 60)
            End If
        Next wobjNodoActividad
        '
    End With
    '
    wvarStep = 60
    Set wobjXMLRequest = Nothing
    Set wobjNodoActividad = Nothing
    '
    wvarStep = 70
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 100
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 110
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 120
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarActividadBuscar
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 240
    wvarResult = ""
    
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 250
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 260
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 280
        wvarResult = ""
        wvarPos = 127 'cantidad de caracteres ocupados por parámetros de entrada
        wvarResult = ParseoMensaje(wvarPos, strParseString, wvarstrLen, wvarCodRubro)
        '
        wvarStep = 340
        Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
        '
    End If
    '
    wvarStep = 360
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarActividadBuscar & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(pvarPos As Long, pstrParseString As String, pvarstrLen As Long, pvarRubroFiltro As String) As String
Dim wvarCondicion       As String
Dim wvarCantLin         As Long
Dim wvarCounter         As Long
Dim wobjRegExp          As Object 'RegExp
Dim wobjColMatch        As Object 'MatchCollection
Dim wobjMatch           As Object 'Match
Dim wobjRegSubexp       As Object 'RegExp
Dim wobjColSubMatch     As Object 'MatchCollection
Dim wobjsubMatch        As Object 'Match
Dim wvarParseEvalString As String
Dim wobjXmlDOMResp      As DOMDocument
Dim wobjXslDOMResp      As DOMDocument
Dim wobjXmlElemenResp   As IXMLDOMElement
Dim wobjXmlElemenRespP  As IXMLDOMElement
Dim wobjXmlElemenRespC  As IXMLDOMElement
Dim wobjXmlElemenRespCe As IXMLDOMElement
Dim wvarCodigoProfesion As String
Dim wvarCantRegistros   As Long
Dim wvarCcurrRegistro   As Long
Dim wvarCantCondi       As Long
Dim wvarCodigoRubro     As String
'
    Set wobjRegExp = CreateObject("VbScript.RegExp")
    Set wobjRegSubexp = CreateObject("VbScript.RegExp")
    wobjRegSubexp.Global = True
    wobjRegExp.Global = True
    '
    wvarCantRegistros = Val(Mid(pstrParseString, pvarPos, 4))
    wvarCcurrRegistro = 0
    wvarParseEvalString = Mid(pstrParseString, pvarPos + 4) 'Elimino el parametro que indica la cantidad de registros devueltos
    wvarParseEvalString = Replace(wvarParseEvalString, " ", "_")
    
    wobjRegExp.Pattern = "(\S{4})(\S{6})(\S{60})(\S{20})" 'RAMOPCOD PIC X(04),LNK-PROFECOD PIC X(06), LNK-PROFEDES PIC X(60)
    
    wobjRegSubexp.Pattern = "(\S{2})" 'LNK-COBERTURAS OCCURS 10 TIMES. LNK-CODCONDI PIC X(02).
    '
    
    Set wobjColMatch = wobjRegExp.Execute(wvarParseEvalString)
    '
    Set wobjXmlDOMResp = CreateObject("msxml2.DomDocument")
    Set wobjXslDOMResp = CreateObject("msxml2.DomDocument")
    
    Call wobjXmlDOMResp.loadXML("<RS></RS>")
    '
    For Each wobjMatch In wobjColMatch
    '
        wvarCodigoProfesion = Replace(wobjMatch.SubMatches(1), "_", " ")
        wvarCodigoRubro = Trim(Replace(wobjMatch.SubMatches(0), "_", " "))
        If Trim(wvarCodigoProfesion) <> "" And wvarCcurrRegistro < wvarCantRegistros Then
            If UCase(pvarRubroFiltro) = UCase(wvarCodigoRubro) Then
                wvarCcurrRegistro = wvarCcurrRegistro + 1
                Set wobjXmlElemenResp = wobjXmlDOMResp.createElement("PROFESION")
                wobjXmlDOMResp.selectSingleNode("//RS").appendChild wobjXmlElemenResp
                
                Set wobjXmlElemenRespP = wobjXmlDOMResp.createElement("RAMOPCOD")
                wobjXmlElemenResp.appendChild wobjXmlElemenRespP
                wobjXmlElemenRespP.Text = wvarCodigoRubro
                
                Set wobjXmlElemenRespP = wobjXmlDOMResp.createElement("PROFECOD")
                wobjXmlElemenResp.appendChild wobjXmlElemenRespP
                wobjXmlElemenRespP.Text = Trim(wvarCodigoProfesion)
                
                Set wobjXmlElemenRespP = wobjXmlDOMResp.createElement("PROFEDES")
                wobjXmlElemenResp.appendChild wobjXmlElemenRespP
                wobjXmlElemenRespP.Text = Trim(Replace(wobjMatch.SubMatches(2), "_", " "))
                
                Set wobjColSubMatch = wobjRegSubexp.Execute(wobjMatch.SubMatches(3))
                
                Set wobjXmlElemenRespC = wobjXmlDOMResp.createElement("COBERCONDI")
                wobjXmlElemenResp.appendChild wobjXmlElemenRespC
                wvarCantCondi = 0
                '
                For Each wobjsubMatch In wobjColSubMatch
                    '
                    wvarCondicion = Replace(wobjsubMatch.SubMatches(0), "_", " ")
                    If Trim(wvarCondicion) <> "" Then
                        wvarCantCondi = wvarCantCondi + 1
                        Set wobjXmlElemenRespCe = wobjXmlDOMResp.createElement("CODCONDI")
                        wobjXmlElemenRespC.appendChild wobjXmlElemenRespCe
                        wobjXmlElemenRespCe.Text = Trim(Replace(wobjsubMatch.SubMatches(0), "_", " "))
                    End If
                    '
                Next
                wobjXmlElemenRespC.setAttribute "CANTCONDI", wvarCantCondi
            End If
        End If
    '
    Next wobjMatch
    '
    wobjXslDOMResp.async = False
    wobjXslDOMResp.loadXML p_GetXSL()
    ParseoMensaje = Replace(wobjXmlDOMResp.transformNode(wobjXslDOMResp), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
    'ParseoMensaje = wobjXmlDOMResp.xml
    '
    Set wobjXmlDOMResp = Nothing
    Set wobjXmlElemenResp = Nothing
    Set wobjXmlElemenRespP = Nothing
    Set wobjXmlElemenRespC = Nothing
    Set wobjXmlElemenRespCe = Nothing
    Set wobjXslDOMResp = Nothing
    '
    Set wobjRegExp = Nothing
    Set wobjRegSubexp = Nothing
    
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<?xml version='1.0' encoding='UTF-8'?>"
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>"
    wvarStrXSL = wvarStrXSL & "<xsl:output method='html' version='1.0' encoding='UTF-8' indent='yes'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:template match='/' >"
    wvarStrXSL = wvarStrXSL & "<xsl:for-each select='//PROFESION'>"
    wvarStrXSL = wvarStrXSL & "<xsl:sort select='./PROFEDES'/>"
    wvarStrXSL = wvarStrXSL & "     <option>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='value'><xsl:value-of select='PROFECOD'/></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='Rubro'><xsl:value-of select='RAMOPCOD'/></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='CantCondiciones'><xsl:value-of select='./COBERCONDI/@CANTCONDI'/></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "     <xsl:value-of select='PROFECOD'/> <xsl:text> </xsl:text> <xsl:value-of select='PROFEDES'/>"
    'wvarStrXSL = wvarStrXSL & "     <xsl:for-each select='./COBERCONDI/CODCONDI'>"
    'wvarStrXSL = wvarStrXSL & "     <xsl:text>   </xsl:text><xsl:value-of select='.'/>"
    'wvarStrXSL = wvarStrXSL & "     </xsl:for-each>"
    wvarStrXSL = wvarStrXSL & "     </option>"
    wvarStrXSL = wvarStrXSL & "     </xsl:for-each>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template >"
    wvarStrXSL = wvarStrXSL & "     </xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
    '
End Function

