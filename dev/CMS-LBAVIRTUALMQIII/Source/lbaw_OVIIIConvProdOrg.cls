VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVIIIConvProdOrg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context             As ObjectContext
Private mobjEventLog                As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbawA_OVMQIII.lbaw_OVIIIConvProdOrg"
Const mcteOpID                      As String = "1555"

'Parametros XML de Entrada
Const mcteParam_Usuario             As String = "//USUARIO"
Const mcteAgente_Clase              As String = "//AGENTECLASE"
Const mcteAgenteCodigo              As String = "//AGENTECODIGO"
Const mcteParam_CodRamo             As String = "//RAMOPCOD"
Const mcteParam_ConveCop            As String = "//CONVECOP"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarAgenteClase     As String
    Dim wvarAgenteCodigo    As String
    Dim wvarCodigRamo       As String
    Dim wvarConveCop        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarAgenteClase = Left(.selectSingleNode(mcteAgente_Clase).Text & Space(2), 2)
        wvarAgenteCodigo = Right("0000" & .selectSingleNode(mcteAgenteCodigo).Text, 4)
        wvarCodigRamo = Left(.selectSingleNode(mcteParam_CodRamo).Text & Space(4), 4)
        
        'El parámetro CONVECOP es opcional
        If Not .selectSingleNode(mcteParam_ConveCop) Is Nothing Then
            wvarConveCop = Left(.selectSingleNode(mcteParam_ConveCop).Text & Space(4), 4)
        Else
            wvarConveCop = Space(4)
        End If
        
        '
    End With
    '
    wvarStep = 60
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 70
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 100
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 110
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 120
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarAgenteCodigo & wvarAgenteClase & wvarCodigRamo & wvarConveCop
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 240
    wvarResult = ""
    
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 250
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 260
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 280
        wvarResult = ""
        wvarPos = 81 '77 cantidad de caracteres ocupados por parámetros de entrada
        wvarResult = ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        '
        wvarStep = 340
        Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
        '
    End If
    '
    wvarStep = 360
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarAgenteCodigo & wvarAgenteClase & wvarCodigRamo & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(pvarPos As Long, pstrParseString As String, pvarstrLen As Long) As String
Dim wobjXslDOMResp      As DOMDocument
Dim wobjRegExp          As Object 'RegExp
Dim wobjColMatch        As Object 'MatchCollection
Dim wobjMatch           As Object 'Match
Dim wvarParseEvalString As String
Dim wobjXmlDOMResp      As DOMDocument
Dim wobjXmlElemenResp   As IXMLDOMElement
Dim wobjXmlElemenRespP  As IXMLDOMElement
Dim wvarCantRegistros   As Long
Dim wvarCcurrRegistro   As Long
Dim wvarCodigoConvenio  As String
'
    Set wobjRegExp = CreateObject("VbScript.RegExp")
    wobjRegExp.Global = True
    '
    wvarCantRegistros = Val(Mid(pstrParseString, pvarPos, 3))
    wvarCcurrRegistro = 0
    wvarParseEvalString = Mid(pstrParseString, pvarPos + 3) 'Elimino el parametro que indica la cantidad de registros devueltos
    wvarParseEvalString = Replace(wvarParseEvalString, " ", "_")
    
    wobjRegExp.Pattern = "(\S{4})(\S{60})(\S{4})(\S{5})(\S{5})"
    
    Set wobjColMatch = wobjRegExp.Execute(wvarParseEvalString)
    '
    Set wobjXmlDOMResp = CreateObject("msxml2.DomDocument")
    Set wobjXslDOMResp = CreateObject("msxml2.DomDocument")
    
    Call wobjXmlDOMResp.loadXML("<RS></RS>")
    '
    
    For Each wobjMatch In wobjColMatch
    '
        wvarCodigoConvenio = Trim(Replace(wobjMatch.SubMatches(0), "_", " "))
        If Trim(wvarCodigoConvenio) <> "" And wvarCcurrRegistro < wvarCantRegistros Then
            wvarCcurrRegistro = wvarCcurrRegistro + 1
            Set wobjXmlElemenResp = wobjXmlDOMResp.createElement("CONVENIO")
            wobjXmlDOMResp.selectSingleNode("//RS").appendChild wobjXmlElemenResp
            '
            Set wobjXmlElemenRespP = wobjXmlDOMResp.createElement("CODIGO")
            wobjXmlElemenResp.appendChild wobjXmlElemenRespP
            wobjXmlElemenRespP.Text = wvarCodigoConvenio
            '
            Set wobjXmlElemenRespP = wobjXmlDOMResp.createElement("DESCRIPCION")
            wobjXmlElemenResp.appendChild wobjXmlElemenRespP
            wobjXmlElemenRespP.Text = UCase(Trim(Replace(wobjMatch.SubMatches(1), "_", " ")))
            '
            Set wobjXmlElemenRespP = wobjXmlDOMResp.createElement("CUADRO")
            wobjXmlElemenResp.appendChild wobjXmlElemenRespP
            wobjXmlElemenRespP.Text = UCase(Trim(Replace(wobjMatch.SubMatches(2), "_", " ")))
            '
            Set wobjXmlElemenRespP = wobjXmlDOMResp.createElement("RECARGO")
            wobjXmlElemenResp.appendChild wobjXmlElemenRespP
            wobjXmlElemenRespP.Text = Val(Replace(wobjMatch.SubMatches(3), "_", " ")) / 100
            '
            Set wobjXmlElemenRespP = wobjXmlDOMResp.createElement("COMISION")
            wobjXmlElemenResp.appendChild wobjXmlElemenRespP
            wobjXmlElemenRespP.Text = Val(Replace(wobjMatch.SubMatches(4), "_", " ")) / 100
        End If
    '
    Next wobjMatch
    '
    wobjXslDOMResp.async = False
    wobjXslDOMResp.loadXML p_GetXSL()
    ParseoMensaje = Replace(wobjXmlDOMResp.transformNode(wobjXslDOMResp), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
    'ParseoMensaje = wobjXmlDOMResp.xml
    '
    Set wobjXslDOMResp = Nothing
    Set wobjXmlDOMResp = Nothing
    Set wobjXmlElemenResp = Nothing
    Set wobjXmlElemenRespP = Nothing
    '
    Set wobjRegExp = Nothing
    
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<?xml version='1.0' encoding='UTF-8'?>"
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>"
    wvarStrXSL = wvarStrXSL & "<xsl:output method='html' version='1.0' encoding='UTF-8' indent='yes'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:template match='/' >"
    wvarStrXSL = wvarStrXSL & "<xsl:for-each select='//CONVENIO'>"
    wvarStrXSL = wvarStrXSL & "     <OPTION>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='cuadro'><xsl:value-of select='CUADRO'/></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='value'><xsl:value-of select='CODIGO'/></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='recargo'><xsl:value-of select='RECARGO'/></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='comision'><xsl:value-of select='COMISION'/></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "     <xsl:value-of select='DESCRIPCION'/>"
    wvarStrXSL = wvarStrXSL & "     </OPTION>"
    wvarStrXSL = wvarStrXSL & "     </xsl:for-each>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template >"
    wvarStrXSL = wvarStrXSL & "     </xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
    '
End Function
