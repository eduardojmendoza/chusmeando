package com.qbe.services.lbavirtualmqiii;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.lbavirtualmqiii.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVIIIGetIIBZona implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQIII.lbaw_OVIIIGetIIBZona";
  /**
   * Parametros XML de Entrada que se necesitan para los mensajes que consulta
   */
  static final String mcteParam_PAISSCOD = "//PAISSCOD";
  static final String mcteParam_V_PROVICOD = "//V_PROVICOD";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_CLIENIVA = "//CLIENIVA";
  static final String mcteParam_LISTA = "//LISTA";
  static final String mcteParam_CodIB = "//NUMERO";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    lbawA_OfVirtualLBA.lbaw_GetTipIngBrut wobjClass = new lbawA_OfVirtualLBA.lbaw_GetTipIngBrut();
    int wvarStep = 0;
    String wvarResponse = "";
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLResponse = new XmlDomExtended();
      //
      //
      wobjXMLRequest.loadXML( Request );
      //
      //
      wvarStep = 60;
      //
      // *******************************************************
      //Consulto primero si la provincia aplica Ingresos Brutos
      // *******************************************************
      wobjClass = new lbawA_OfVirtualLBA.lbaw_GetZonaIngBrut();
      wobjClass.Execute( Request, wvarResponse, "" );
      wobjClass = (lbawA_OfVirtualLBA.lbaw_GetTipIngBrut) null;
      //
      wvarStep = 70;
      wobjXMLResponse.loadXML( wvarResponse );
      //
      wvarStep = 80;
      if( wobjXMLResponse.selectSingleNode( "//PROVICOD" )  == (org.w3c.dom.Node) null )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje='No Existen Ingresos Brutos para la provincia seleccionada'/></Response>" );
      }
      else
      {
        // *******************************************************
        //Consulto los Ingresos Brutos
        // *******************************************************
        wobjClass = new lbawA_OfVirtualLBA.lbaw_GetTipIngBrut();
        wobjClass.Execute( Request, wvarResponse, "" );
        //
        wvarStep = 100;
        wobjClass = (lbawA_OfVirtualLBA.lbaw_GetTipIngBrut) null;
        Response.set( wvarResponse );
      }
      //
      wvarStep = 120;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      wobjXMLRequest = null;
      wobjClass = (lbawA_OfVirtualLBA.lbaw_GetTipIngBrut) null;
      wobjXMLResponse = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
