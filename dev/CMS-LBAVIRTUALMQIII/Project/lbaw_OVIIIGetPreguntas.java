import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVIIIGetPreguntas implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQIII.lbaw_OVIIIGetPreguntas";
  static final String mcteOpID = "1560";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_Codigo_Ramo = "//RAMOPCOD";
  static final String mcteParam_Codigo_Actividad = "//ACTIVIDAD";
  static final String mcteParam_Codigo_Zona = "//ZONA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarCodRamo = "";
    String wvarCodActividad = "";
    String wvarCodZona = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //Deberá venir desde la página
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarCodRamo = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Ramo ) */ ) + Strings.space( 4 ), 4 );
      wvarCodActividad = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Actividad ) */ ) + Strings.space( 6 ), 6 );
      wvarCodZona = Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Zona ) */ ), 4 );
      //
      //
      wvarStep = 60;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los parámetros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 110;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarCodRamo + wvarCodActividad + wvarCodZona;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      wvarMQError = wobjFrame2MQ.Execute( wvarArea, strParseString, null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG" ) */.toString() ).toInt();
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 240;
      //
      parsear: 
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = "";
        //cantidad de caracteres ocupados por parámetros de entrada
        wvarPos = 81;
        wvarResult = invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseString), new Variant(wvarstrLen) } );
        //
        wvarStep = 340;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
        //
      }
      //
      wvarStep = 360;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCodRamo + wvarCodActividad + wvarCodZona + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int pvarPos, String pstrParseString, int pvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    VbScript.RegExp wobjRegExp = new VbScript.RegExp();
    void wobjColMatch;
    Object wobjMatch = null;
    VbScript.RegExp wobjRegSubexp = new VbScript.RegExp();
    Object wobjColSubMatch = null;
    Object wobjsubMatch = null;
    String wvarParseEvalString = "";
    int i = 0;
    diamondedge.util.XmlDom wobjXmlDOMResp = null;
    org.w3c.dom.Element wobjXmlElemenResp = null;
    org.w3c.dom.Element wobjXmlElemenRespV = null;
    org.w3c.dom.Element wobjXmlElemenRespC = null;
    org.w3c.dom.Element wobjXmlElemenRespCe = null;
    org.w3c.dom.Element wobjXmlElemenRespVe = null;
    org.w3c.dom.Element wobjXmlElemenRespPr = null;
    org.w3c.dom.Element wobjXmlElemenValPos = null;
    org.w3c.dom.Element wobjXmlElemenValor = null;
    org.w3c.dom.Attr wobjXmlAtributo = null;
    int wvarCantRegistros = 0;
    int wvarCcurrRegistro = 0;
    String wvarCobertura = "";
    String wvarCodPregunta = "";
    int wvarCantPreguntas = 0;
    int wvarCcurrPregunta = 0;
    int wvarCantLeyendas = 0;
    //
    //RegExp
    //MatchCollection
    //Match
    //RegExp
    //MatchCollection
    //Match
    //
    //
    wobjRegExp = new VbScript.RegExp();
    wobjRegExp.Global.set( true );

    wobjRegSubexp = new VbScript.RegExp();
    wobjRegSubexp.Global.set( true );

    //
    wobjRegExp.Pattern.set( "(\\S{3})(\\S{3})(\\S{1080})" );
    wobjRegSubexp.Pattern.set( "(\\S{4})(\\S{4})(\\S{3})(\\S{124})" );

    //
    wvarParseEvalString = Strings.mid( pstrParseString, pvarPos + 3 );
    wvarParseEvalString = Strings.replace( wvarParseEvalString, " ", "_" );
    wvarCantRegistros = (int)Math.rint( VB.val( Strings.mid( pstrParseString, pvarPos, 3 ) ) );
    wvarCcurrRegistro = 0;

    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    wobjXmlDOMResp = new diamondedge.util.XmlDom();

    wobjXmlDOMResp.loadXML( "<preguntas></preguntas>" );

    //
    Label249: 
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //
      if( ! (wvarCcurrRegistro < wvarCantRegistros) )
      {
        break;
      }
      wvarCcurrRegistro = wvarCcurrRegistro + 1;
      //
      //error: function 'SubMatches' was not found.
      //unsup: wvarCobertura = Val(Replace(wobjMatch.SubMatches(0), "_", " "))
      //error: function 'SubMatches' was not found.
      //unsup: wvarCantPreguntas = Val(Replace(wobjMatch.SubMatches(1), "_", " "))
      wvarCcurrPregunta = 0;

      //error: function 'SubMatches' was not found.
      //unsup: Set wobjColSubMatch = wobjRegSubexp.Execute(wobjMatch.SubMatches(2))
      for( int nwobjsubMatch = 0; nwobjsubMatch < wobjColSubMatch.getCount(); nwobjsubMatch++ )
      {
        //
        if( ! (wvarCcurrPregunta < wvarCantPreguntas) )
        {
          break;
        }
        wvarCcurrPregunta = wvarCcurrPregunta + 1;
        //
        //error: function 'SubMatches' was not found.
        //unsup: wvarCodPregunta = Val(Replace(wobjsubMatch.SubMatches(1), "_", " "))
        wobjXmlElemenRespPr = (org.w3c.dom.Element) null /*unsup wobjXmlDOMResp.selectSingleNode( "//preguntas/pregunta[@Codigo=" + wvarCodPregunta + "]" ) */;
        //
        if( wobjXmlElemenRespPr == (org.w3c.dom.Element) null )
        {
          //Si no existe el Modulo lo genero
          wobjXmlElemenRespPr = wobjXmlDOMResp.getDocument().createElement( "pregunta" );
          /*unsup wobjXmlDOMResp.selectSingleNode( "//preguntas" ) */.appendChild( wobjXmlElemenRespPr );

          wobjXmlElemenValPos = wobjXmlDOMResp.getDocument().createElement( "valoresposibles" );
          wobjXmlElemenRespPr.appendChild( wobjXmlElemenValPos );

          //Estas preguntas son del tipo SI-NO, genero los posibles valores desde aca
          wobjXmlElemenValor = wobjXmlDOMResp.getDocument().createElement( "OPTION" );
          diamondedge.util.XmlDom.setText( wobjXmlElemenValor, "NO" );
          wobjXmlElemenValPos.appendChild( wobjXmlElemenValor );
          wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "value" );
          wobjXmlElemenValor.setAttributeNode( wobjXmlAtributo );
          diamondedge.util.XmlDom.setText( wobjXmlAtributo, "N" );

          wobjXmlElemenValor = wobjXmlDOMResp.getDocument().createElement( "OPTION" );
          diamondedge.util.XmlDom.setText( wobjXmlElemenValor, "SI" );
          wobjXmlElemenValPos.appendChild( wobjXmlElemenValor );
          wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "value" );
          wobjXmlElemenValor.setAttributeNode( wobjXmlAtributo );
          diamondedge.util.XmlDom.setText( wobjXmlAtributo, "S" );

          wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Codigo" );
          wobjXmlElemenRespPr.setAttributeNode( wobjXmlAtributo );
          diamondedge.util.XmlDom.setText( wobjXmlAtributo, wvarCodPregunta );

          wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "OrdenEnPantalla" );
          wobjXmlElemenRespPr.setAttributeNode( wobjXmlAtributo );
          //error: function 'SubMatches' was not found.
          //unsup: wobjXmlAtributo.Text = Val(Replace(wobjsubMatch.SubMatches(0), "_", " "))
          wobjXmlElemenRespV = wobjXmlDOMResp.getDocument().createElement( "leyendas" );
          wobjXmlElemenRespPr.appendChild( wobjXmlElemenRespV );
          //
          //error: function 'SubMatches' was not found.
          //unsup: wvarCantLeyendas = Val(Replace(wobjsubMatch.SubMatches(2), "_", " "))
          for( i = 0; i <= (wvarCantLeyendas - 1); i++ )
          {
            wobjXmlElemenRespVe = wobjXmlDOMResp.getDocument().createElement( "leyenda" );
            wobjXmlElemenRespV.appendChild( wobjXmlElemenRespVe );

            wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Item" );
            //error: function 'SubMatches' was not found.
            //unsup: wobjXmlAtributo.Text = Mid(Replace(wobjsubMatch.SubMatches(3), "_", " "), (i * 62) + 1, 2)
            wobjXmlElemenRespVe.setAttributeNode( wobjXmlAtributo );

            wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Descripcion" );
            //error: function 'SubMatches' was not found.
            //unsup: wobjXmlAtributo.Text = Trim(Mid(Replace(wobjsubMatch.SubMatches(3), "_", " "), (i * 62) + 1 + 2, 60))
            wobjXmlElemenRespVe.setAttributeNode( wobjXmlAtributo );

          }

          wobjXmlElemenRespC = wobjXmlDOMResp.getDocument().createElement( "coberturasactivantes" );
          wobjXmlElemenRespPr.appendChild( wobjXmlElemenRespC );

        }

        wobjXmlElemenRespCe = wobjXmlDOMResp.getDocument().createElement( "cobertura" );
        /*unsup wobjXmlDOMResp.selectSingleNode( "//preguntas/pregunta[@Codigo=" + wvarCodPregunta + "]/coberturasactivantes" ) */.appendChild( wobjXmlElemenRespCe );
        diamondedge.util.XmlDom.setText( wobjXmlElemenRespCe, wvarCobertura );
        //
      }
      //
    }
    //
    ParseoMensaje = wobjXmlDOMResp.getDocument().getDocumentElement().toString();
    //
    wobjXmlElemenValor = (org.w3c.dom.Element) null;
    wobjXmlElemenValPos = (org.w3c.dom.Element) null;
    wobjXmlElemenResp = (org.w3c.dom.Element) null;
    wobjXmlElemenRespV = (org.w3c.dom.Element) null;
    wobjXmlElemenRespC = (org.w3c.dom.Element) null;
    wobjXmlElemenRespCe = (org.w3c.dom.Element) null;
    wobjXmlElemenRespVe = (org.w3c.dom.Element) null;
    wobjXmlElemenRespPr = (org.w3c.dom.Element) null;
    //
    wobjRegExp = (VbScript.RegExp) null;
    wobjRegSubexp = (VbScript.RegExp) null;
    //
    return ParseoMensaje;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
