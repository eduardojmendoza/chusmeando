import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVIIIGetCoberModul implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQIII.lbaw_OVIIIGetCoberModul";
  static final String mcteOpID = "1538";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_Codigo_Ramo = "//RAMOPCOD";
  static final String mcteParam_Codigo_Actividad = "//ACTIVIDAD";
  static final String mcteParam_Codigo_Zona = "//ZONA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarCodRamo = "";
    String wvarCodActividad = "";
    String wvarCodZona = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //Deberá venir desde la página
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarCodRamo = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Ramo ) */ ) + Strings.space( 4 ), 4 );
      wvarCodActividad = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Actividad ) */ ) + Strings.space( 6 ), 6 );
      wvarCodZona = Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Zona ) */ ), 4 );
      //
      //
      wvarStep = 60;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los parámetros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 110;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarCodRamo + wvarCodActividad + wvarCodZona;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      wvarMQError = wobjFrame2MQ.Execute( wvarArea, strParseString, null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG" ) */.toString() ).toInt();
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 240;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 350;
        //cantidad de caracteres ocupados por parámetros de entrada
        wvarPos = 81;
        wvarResult = invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseString), new Variant(wvarstrLen) } );
        //
        wvarStep = 360;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
        //
      }
      //
      wvarStep = 370;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCodRamo + wvarCodActividad + wvarCodZona + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int pvarPos, String pstrParseString, int pvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    VbScript.RegExp wobjRegExp = new VbScript.RegExp();
    void wobjColMatch;
    Object wobjMatch = null;
    VbScript.RegExp wobjRegSubexp = new VbScript.RegExp();
    Object wobjColSubMatch = null;
    Object wobjsubMatch = null;
    String wvarParseEvalString = "";
    int i = 0;
    diamondedge.util.XmlDom wobjXmlDOMResp = null;
    org.w3c.dom.Element wobjXmlElemenResp = null;
    org.w3c.dom.Element wobjXmlElemenRespE = null;
    org.w3c.dom.Element wobjXmlElemenRespEe = null;
    org.w3c.dom.Element wobjXmlElemenRespG = null;
    org.w3c.dom.Element wobjXmlElemenRespD = null;
    org.w3c.dom.Element wobjXmlElemenRespC = null;
    org.w3c.dom.Element wobjXmlElemenRespMo = null;
    org.w3c.dom.Attr wobjXmlAtributo = null;
    int wvarCantRegistros = 0;
    int wvarCcurrRegistro = 0;
    int wvarCantModulos = 0;
    int wvarCantExclusiones = 0;
    int wvarCcurrModulo = 0;
    String wvarCoberturaPr = "";
    String wvarCobertura = "";
    String wvarCodModulo = "";
    org.w3c.dom.NodeList wobjNodosCoberturas = null;
    org.w3c.dom.Node wobjNodoCobertura = null;
    boolean wvarActivModulariz = false;
    //
    //RegExp
    //MatchCollection
    //Match
    //RegExp
    //MatchCollection
    //Match
    //
    //
    wobjRegExp = new VbScript.RegExp();
    wobjRegExp.Global.set( true );

    wobjRegSubexp = new VbScript.RegExp();
    wobjRegSubexp.Global.set( true );

    //
    wobjRegExp.Pattern.set( "(\\S{3})(\\S{2})(\\S{1})(\\S{60})(\\S{1})(\\S{3})(\\S{1})(\\S{1})(\\S{3})(\\S{1080})(\\S{3})(\\S{30})" );
    wobjRegSubexp.Pattern.set( "(\\S{2})(\\S{11})(\\S{11})(\\S{9})(\\S{1})(\\S{2})" );

    //
    //+6
    wvarParseEvalString = Strings.mid( pstrParseString, pvarPos + 4 );
    wvarParseEvalString = Strings.replace( wvarParseEvalString, " ", "_" );
    wvarCantRegistros = (int)Math.rint( VB.val( Strings.mid( pstrParseString, pvarPos + 1, 3 ) ) );
    wvarCcurrRegistro = 0;

    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    wobjXmlDOMResp = new diamondedge.util.XmlDom();

    wobjXmlDOMResp.loadXML( "<cotizacion></cotizacion>" );

    //Genero el Nodo GENERAL
    wobjXmlElemenRespG = wobjXmlDOMResp.getDocument().createElement( "general" );
    /*unsup wobjXmlDOMResp.selectSingleNode( "//cotizacion" ) */.appendChild( wobjXmlElemenRespG );

    wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "coberturasdeterminantes" );
    /*unsup wobjXmlDOMResp.selectSingleNode( "//general" ) */.appendChild( wobjXmlElemenResp );

    //Creo el elemento Cantidad Minima de Coberturas
    wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "CantidadMinimaCoberturas" );
    /*unsup wobjXmlDOMResp.selectSingleNode( "//general" ) */.appendChild( wobjXmlElemenResp );
    //+3
    diamondedge.util.XmlDom.setText( wobjXmlElemenResp, String.valueOf( VB.val( Strings.mid( pstrParseString, pvarPos, 1 ) ) ) );

    wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "coberturas" );
    /*unsup wobjXmlDOMResp.selectSingleNode( "//cotizacion" ) */.appendChild( wobjXmlElemenResp );

    wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "modulos" );
    /*unsup wobjXmlDOMResp.selectSingleNode( "//cotizacion" ) */.appendChild( wobjXmlElemenResp );

    wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "exclusiones" );
    /*unsup wobjXmlDOMResp.selectSingleNode( "//cotizacion" ) */.appendChild( wobjXmlElemenResp );
    //
    Label269: 
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //
      if( ! (wvarCcurrRegistro < wvarCantRegistros) )
      {
        break;
      }
      wvarCcurrRegistro = wvarCcurrRegistro + 1;
      //
      wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "cobertura" );
      /*unsup wobjXmlDOMResp.selectSingleNode( "//coberturas" ) */.appendChild( wobjXmlElemenResp );

      //error: function 'SubMatches' was not found.
      //unsup: wvarCobertura = Val(Replace(wobjMatch.SubMatches(0), "_", " "))
      wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Codigo" );
      wobjXmlElemenResp.setAttributeNode( wobjXmlAtributo );
      diamondedge.util.XmlDom.setText( wobjXmlAtributo, wvarCobertura );

      wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "OrdenEnPantalla" );
      wobjXmlElemenResp.setAttributeNode( wobjXmlAtributo );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlAtributo.Text = Val(Replace(wobjMatch.SubMatches(1), "_", " "))
      //error: function 'SubMatches' was not found.
      //unsup: If wobjMatch.SubMatches(2) = "S" Then 'Este campo indica que esta cobertura determina Modulo
      //Este campo indica que esta cobertura determina Modulo
      //error: function 'SubMatches' was not found.
      //unsup: wvarCoberturaPr = Val(Replace(wobjMatch.SubMatches(0), "_", " ")) 'Este campo indica el codigo de cobertura
      //Este campo indica el codigo de cobertura
      wobjXmlElemenRespD = wobjXmlDOMResp.getDocument().createElement( "codigocobertura" );
      /*unsup wobjXmlDOMResp.selectSingleNode( "//general/coberturasdeterminantes" ) */.appendChild( wobjXmlElemenRespD );
      diamondedge.util.XmlDom.setText( wobjXmlElemenRespD, wvarCoberturaPr );
      //error: syntax error: near "End If":
      //unsup: End If
      wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Determinante" );
      wobjXmlElemenResp.setAttributeNode( wobjXmlAtributo );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlAtributo.Text = wobjMatch.SubMatches(2)
      wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Descripcion" );
      wobjXmlElemenResp.setAttributeNode( wobjXmlAtributo );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlAtributo.Text = Trim(Replace(wobjMatch.SubMatches(3), "_", " "))
      wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Obligatoria" );
      wobjXmlElemenResp.setAttributeNode( wobjXmlAtributo );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlAtributo.Text = Trim(Replace(wobjMatch.SubMatches(4), "_", " "))
      wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "CodigoCoberturaPrincipal" );
      wobjXmlElemenResp.setAttributeNode( wobjXmlAtributo );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlAtributo.Text = Val(Replace(wobjMatch.SubMatches(5), "_", " "))
      wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "ObligatoriaAnteValorPrincipal" );
      wobjXmlElemenResp.setAttributeNode( wobjXmlAtributo );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlAtributo.Text = Trim(Replace(wobjMatch.SubMatches(6), "_", " "))
      wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Editable" );
      wobjXmlElemenResp.setAttributeNode( wobjXmlAtributo );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlAtributo.Text = Trim(Replace(wobjMatch.SubMatches(7), "_", " "))
      wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "TopeMinimo" );
      wobjXmlElemenResp.setAttributeNode( wobjXmlAtributo );

      wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "TopeMaximo" );
      wobjXmlElemenResp.setAttributeNode( wobjXmlAtributo );

      //error: function 'SubMatches' was not found.
      //unsup: wvarCantModulos = Val(Replace(wobjMatch.SubMatches(8), "_", " "))
      wvarCcurrModulo = 0;

      //error: function 'SubMatches' was not found.
      //unsup: Set wobjColSubMatch = wobjRegSubexp.Execute(wobjMatch.SubMatches(9)) 'Determinacion del Modulo
      //Determinacion del Modulo
      for( int nwobjsubMatch = 0; nwobjsubMatch < wobjColSubMatch.getCount(); nwobjsubMatch++ )
      {
        //
        if( ! (wvarCcurrModulo < wvarCantModulos) )
        {
          break;
        }
        wvarCcurrModulo = wvarCcurrModulo + 1;
        //
        //error: function 'SubMatches' was not found.
        //unsup: wvarCodModulo = Val(Replace(wobjsubMatch.SubMatches(0), "_", " "))
        wobjXmlElemenRespMo = (org.w3c.dom.Element) null /*unsup wobjXmlDOMResp.selectSingleNode( "//modulos/modulo[@Codigo=" + wvarCodModulo + "]" ) */;
        //
        if( wobjXmlElemenRespMo == (org.w3c.dom.Element) null )
        {
          //Si no existe el Modulo lo genero
          wobjXmlElemenRespMo = wobjXmlDOMResp.getDocument().createElement( "modulo" );
          /*unsup wobjXmlDOMResp.selectSingleNode( "//modulos" ) */.appendChild( wobjXmlElemenRespMo );
          wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Codigo" );
          wobjXmlElemenRespMo.setAttributeNode( wobjXmlAtributo );
          diamondedge.util.XmlDom.setText( wobjXmlAtributo, wvarCodModulo );
        }

        //Genero el Nodo Cobertura
        wobjXmlElemenRespC = wobjXmlDOMResp.getDocument().createElement( "cobertura" );
        wobjXmlElemenRespMo.appendChild( wobjXmlElemenRespC );

        wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Determinante" );
        wobjXmlElemenRespC.setAttributeNode( wobjXmlAtributo );
        //error: function 'SubMatches' was not found.
        //unsup: wobjXmlAtributo.Text = wobjMatch.SubMatches(2)
        wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Codigo" );
        wobjXmlElemenRespC.setAttributeNode( wobjXmlAtributo );
        diamondedge.util.XmlDom.setText( wobjXmlAtributo, wvarCobertura );

        wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Min" );
        wobjXmlElemenRespC.setAttributeNode( wobjXmlAtributo );
        //error: function 'SubMatches' was not found.
        //unsup: wobjXmlAtributo.Text = Val(Left(wobjsubMatch.SubMatches(1), Len(wobjsubMatch.SubMatches(1)) - 2)) & "," & Right(wobjsubMatch.SubMatches(1), 2)
        wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Max" );
        wobjXmlElemenRespC.setAttributeNode( wobjXmlAtributo );
        //error: function 'SubMatches' was not found.
        //unsup: wobjXmlAtributo.Text = Val(Left(wobjsubMatch.SubMatches(2), Len(wobjsubMatch.SubMatches(2)) - 2)) & "," & Right(wobjsubMatch.SubMatches(2), 2)
        wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "PorcentajeCoberturaPrincipal" );
        wobjXmlElemenRespC.setAttributeNode( wobjXmlAtributo );
        //error: function 'SubMatches' was not found.
        //unsup: wobjXmlAtributo.Text = Val(Left(wobjsubMatch.SubMatches(3), Len(wobjsubMatch.SubMatches(3)) - 6)) & "," & Right(wobjsubMatch.SubMatches(3), 6)
        wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "PorcentajeFijoOTope" );
        wobjXmlElemenRespC.setAttributeNode( wobjXmlAtributo );
        //error: function 'SubMatches' was not found.
        //unsup: wobjXmlAtributo.Text = Trim(Replace(wobjsubMatch.SubMatches(4), "_", " "))
        wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "CodigoFranquiciaAsociada" );
        wobjXmlElemenRespC.setAttributeNode( wobjXmlAtributo );
        //error: function 'SubMatches' was not found.
        //unsup: wobjXmlAtributo.Text = Trim(Replace(wobjsubMatch.SubMatches(5), "_", " "))
        //
      }
      //
      //error: function 'SubMatches' was not found.
      //unsup: wvarCantExclusiones = Val(Replace(wobjMatch.SubMatches(10), "_", " "))
      //
      wobjXmlElemenRespE = wobjXmlDOMResp.getDocument().createElement( "exclusion" );
      /*unsup wobjXmlDOMResp.selectSingleNode( "//cotizacion/exclusiones" ) */.appendChild( wobjXmlElemenRespE );

      wobjXmlAtributo = wobjXmlDOMResp.getDocument().createAttribute( "Cobertura" );
      wobjXmlElemenRespE.setAttributeNode( wobjXmlAtributo );
      diamondedge.util.XmlDom.setText( wobjXmlAtributo, wvarCobertura );
      //
      for( i = 0; i <= (wvarCantExclusiones - 1); i++ )
      {
        wobjXmlElemenRespEe = wobjXmlDOMResp.getDocument().createElement( "coberturaexcluyente" );
        wobjXmlElemenRespE.appendChild( wobjXmlElemenRespEe );
        //error: function 'SubMatches' was not found.
        //unsup: wobjXmlElemenRespEe.Text = Mid((Replace(wobjMatch.SubMatches(11), "_", " ")), i * 3 + 1, 3)
      }
      //
      //
    }
    //
    //Recorro las coberturas independientes para colocarles los Topes Minimos y maximos que se van a validar
    wobjNodosCoberturas = null /*unsup wobjXmlDOMResp.selectNodes( "//cobertura[@Independiente='S']" ) */;
    for( int nwobjNodoCobertura = 0; nwobjNodoCobertura < wobjNodosCoberturas.getLength(); nwobjNodoCobertura++ )
    {
      wobjNodoCobertura = wobjNodosCoberturas.item( nwobjNodoCobertura );
      diamondedge.util.XmlDom.setText( wobjNodoCobertura.getAttributes().getNamedItem( "TopeMinimo" ), String.valueOf( invoke( "GetTopeMinimo", new Variant[] { new Variant(diamondedge.util.XmlDom.getText( wobjNodoCobertura.getAttributes().getNamedItem( new Variant("Codigo") ) )), new Variant(wobjXmlDOMResp) } ) ) );
      diamondedge.util.XmlDom.setText( wobjNodoCobertura.getAttributes().getNamedItem( "TopeMaximo" ), String.valueOf( invoke( "GetTopeMaximo", new Variant[] { new Variant(diamondedge.util.XmlDom.getText( wobjNodoCobertura.getAttributes().getNamedItem( new Variant("Codigo") ) )), new Variant(wobjXmlDOMResp) } ) ) );
    }
    //
    wvarActivModulariz = false;
    //Verifico si es una actividad NO modulada para sacarle los campos de coberturas determinantes
    wobjNodosCoberturas = null /*unsup wobjXmlDOMResp.selectNodes( "//coberturasdeterminantes/codigocobertura" ) */;
    for( int nwobjNodoCobertura = 0; nwobjNodoCobertura < wobjNodosCoberturas.getLength(); nwobjNodoCobertura++ )
    {
      wobjNodoCobertura = wobjNodosCoberturas.item( nwobjNodoCobertura );
      if( null /*unsup wobjXmlDOMResp.selectNodes( ("//modulos/modulo/cobertura[@Codigo='" + diamondedge.util.XmlDom.getText( wobjNodoCobertura ) + "']") ) */.getLength() > 1 )
      {
        wvarActivModulariz = true;
        break;
      }
    }

    if( ! (wvarActivModulariz) )
    {
      //Si no es una acitvidad Modularizada Elimino los nodos de las coberturas determinantes
      wobjXmlElemenResp = (org.w3c.dom.Element) null /*unsup wobjXmlDOMResp.selectSingleNode( "//coberturasdeterminantes" ) */;
      wobjXmlElemenResp.getParentNode().removeChild( wobjXmlElemenResp );
    }

    ParseoMensaje = wobjXmlDOMResp.getDocument().getDocumentElement().toString();
    //
    wobjNodoCobertura = (org.w3c.dom.Node) null;
    wobjNodosCoberturas = (org.w3c.dom.NodeList) null;
    wobjXmlDOMResp = (diamondedge.util.XmlDom) null;
    wobjXmlElemenResp = (org.w3c.dom.Element) null;
    wobjXmlElemenRespE = (org.w3c.dom.Element) null;
    wobjXmlElemenRespEe = (org.w3c.dom.Element) null;
    wobjXmlElemenRespG = (org.w3c.dom.Element) null;
    wobjXmlElemenRespD = (org.w3c.dom.Element) null;
    wobjXmlElemenRespC = (org.w3c.dom.Element) null;
    wobjXmlElemenRespMo = (org.w3c.dom.Element) null;
    //
    wobjRegExp = (VbScript.RegExp) null;
    wobjRegSubexp = (VbScript.RegExp) null;
    //
    return ParseoMensaje;
  }

  private double GetTopeMaximo( String pvarCodigoCobertura, diamondedge.util.XmlDom pobjXMLDefinition ) throws Exception
  {
    double GetTopeMaximo = 0.0;
    org.w3c.dom.Node wobjNodoCobertura = null;
    org.w3c.dom.NodeList wobjNodosCobertura = null;
    GetTopeMaximo = 0;
    wobjNodosCobertura = null /*unsup pobjXMLDefinition.selectNodes( "//modulos/modulo/cobertura[@Codigo='" + pvarCodigoCobertura + "']" ) */;
    for( int nwobjNodoCobertura = 0; nwobjNodoCobertura < wobjNodosCobertura.getLength(); nwobjNodoCobertura++ )
    {
      wobjNodoCobertura = wobjNodosCobertura.item( nwobjNodoCobertura );
      if( GetTopeMaximo <= Obj.toDouble( Strings.replace( diamondedge.util.XmlDom.getText( wobjNodoCobertura.getAttributes().getNamedItem( "Max" ) ), ",", "." ) ) )
      {
        GetTopeMaximo = Obj.toDouble( Strings.replace( diamondedge.util.XmlDom.getText( wobjNodoCobertura.getAttributes().getNamedItem( "Max" ) ), ",", "." ) );
      }
    }
    //
    wobjNodoCobertura = (org.w3c.dom.Node) null;
    wobjNodosCobertura = (org.w3c.dom.NodeList) null;
    return GetTopeMaximo;
  }

  private double GetTopeMinimo( String pvarCodigoCobertura, diamondedge.util.XmlDom pobjXMLDefinition ) throws Exception
  {
    double GetTopeMinimo = 0.0;
    org.w3c.dom.Node wobjNodoCobertura = null;
    org.w3c.dom.NodeList wobjNodosCobertura = null;
    GetTopeMinimo = 99999999;
    wobjNodosCobertura = null /*unsup pobjXMLDefinition.selectNodes( "//modulos/modulo/cobertura[@Codigo='" + pvarCodigoCobertura + "']" ) */;
    for( int nwobjNodoCobertura = 0; nwobjNodoCobertura < wobjNodosCobertura.getLength(); nwobjNodoCobertura++ )
    {
      wobjNodoCobertura = wobjNodosCobertura.item( nwobjNodoCobertura );
      if( GetTopeMinimo >= Obj.toDouble( Strings.replace( diamondedge.util.XmlDom.getText( wobjNodoCobertura.getAttributes().getNamedItem( "Min" ) ), ",", "." ) ) )
      {
        GetTopeMinimo = Obj.toDouble( Strings.replace( diamondedge.util.XmlDom.getText( wobjNodoCobertura.getAttributes().getNamedItem( "Min" ) ), ",", "." ) );
      }
    }
    //
    wobjNodoCobertura = (org.w3c.dom.Node) null;
    wobjNodosCobertura = (org.w3c.dom.NodeList) null;
    return GetTopeMinimo;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
