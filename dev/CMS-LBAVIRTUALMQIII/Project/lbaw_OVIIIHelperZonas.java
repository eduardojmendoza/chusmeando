import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVIIIHelperZonas implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQIII.lbaw_OVIIIHelperZonas";
  static final String mcteOpID = "0000";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //Deberá venir desde la página
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      //
      //
      wvarStep = 60;

      //DUMYYYYYYYYYYYYYYYYYY
      wobjXMLRequest.load( System.getProperty("user.dir") + "\\zonas.xml" );
      wvarResult = null /*unsup wobjXMLRequest.selectSingleNode( "//zonas" ) */.toString();
      Response.set( "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //unsup GoTo ClearObjects
      //FIN DUMYYY
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los parámetros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 110;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000";
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      wvarMQError = wobjFrame2MQ.Execute( wvarArea, strParseString, null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG" ) */.toString() ).toInt();
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 240;
      wvarResult = "";
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = "";
        //cantidad de caracteres ocupados por parámetros de entrada
        wvarPos = 67;
        wvarResult = invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseString), new Variant(wvarstrLen) } );
        //
        wvarStep = 340;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
        //
      }
      //
      wvarStep = 360;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int pvarPos, String pstrParseString, int pvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    VbScript.RegExp wobjRegExp = new VbScript.RegExp();
    void wobjColMatch;
    Object wobjMatch = null;
    String wvarParseEvalString = "";
    diamondedge.util.XmlDom wobjXmlDOMResp = null;
    diamondedge.util.XmlDom wobjXslDOMResp = null;
    org.w3c.dom.Element wobjXmlElemenResp = null;
    org.w3c.dom.Element wobjXmlElemenRespP = null;
    int wvarCantRegistros = 0;
    int wvarCcurrRegistro = 0;
    String wvarTipoAlarma = "";
    //RegExp
    //MatchCollection
    //Match
    //
    wobjRegExp = new VbScript.RegExp();
    wobjRegExp.Global.set( true );
    //
    wvarCantRegistros = (int)Math.rint( VB.val( Strings.mid( pstrParseString, pvarPos, 3 ) ) );
    wvarCcurrRegistro = 0;
    //Elimino el parametro que indica la cantidad de registros devueltos
    wvarParseEvalString = Strings.mid( pstrParseString, pvarPos + 3 );
    wvarParseEvalString = Strings.replace( wvarParseEvalString, " ", "_" );

    wobjRegExp.Pattern.set( "(\\S{2})(\\S{40})" );

    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    wobjXmlDOMResp = new diamondedge.util.XmlDom();
    wobjXslDOMResp = new diamondedge.util.XmlDom();

    wobjXmlDOMResp.loadXML( "<RS></RS>" );
    //
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //
      //error: function 'SubMatches' was not found.
      //unsup: wvarTipoAlarma = Trim(Replace(wobjMatch.SubMatches(0), "_", " "))
      if( (!Strings.trim( wvarTipoAlarma ).equals( "" )) && (wvarCcurrRegistro < wvarCantRegistros) )
      {
        wvarCcurrRegistro = wvarCcurrRegistro + 1;
        wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "ALARMA" );
        /*unsup wobjXmlDOMResp.selectSingleNode( "//RS" ) */.appendChild( wobjXmlElemenResp );

        wobjXmlElemenRespP = wobjXmlDOMResp.getDocument().createElement( "TIPO" );
        wobjXmlElemenResp.appendChild( wobjXmlElemenRespP );
        diamondedge.util.XmlDom.setText( wobjXmlElemenRespP, wvarTipoAlarma );

        wobjXmlElemenRespP = wobjXmlDOMResp.getDocument().createElement( "DESCRIPCION" );
        wobjXmlElemenResp.appendChild( wobjXmlElemenRespP );
        //error: function 'SubMatches' was not found.
        //unsup: wobjXmlElemenRespP.Text = UCase(Trim(Replace(wobjMatch.SubMatches(1), "_", " ")))
      }
      //
    }
    //
    //unsup wobjXslDOMResp.async = false;
    wobjXslDOMResp.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
    ParseoMensaje = Strings.replace( new Variant() /*unsup wobjXmlDOMResp.transformNode( wobjXslDOMResp ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
    //ParseoMensaje = wobjXmlDOMResp.xml
    //
    wobjXmlDOMResp = (diamondedge.util.XmlDom) null;
    wobjXmlElemenResp = (org.w3c.dom.Element) null;
    wobjXmlElemenRespP = (org.w3c.dom.Element) null;
    wobjXslDOMResp = (diamondedge.util.XmlDom) null;
    //
    wobjRegExp = (VbScript.RegExp) null;

    return ParseoMensaje;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<?xml version='1.0' encoding='UTF-8'?>";
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>";
    wvarStrXSL = wvarStrXSL + "<xsl:output method='html' version='1.0' encoding='UTF-8' indent='yes'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:template match='/' >";
    wvarStrXSL = wvarStrXSL + "<tiposalarma>";
    wvarStrXSL = wvarStrXSL + "<xsl:for-each select='//ALARMA'>";
    wvarStrXSL = wvarStrXSL + "<xsl:sort select='./DESCRIPCION'/>";
    wvarStrXSL = wvarStrXSL + "     <option>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='value'><xsl:value-of select='TIPO'/></xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='descripcion'><xsl:value-of select='DESCRIPCION'/></xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "     <xsl:value-of select='DESCRIPCION'/>";
    wvarStrXSL = wvarStrXSL + "     </option>";
    wvarStrXSL = wvarStrXSL + "     </xsl:for-each>";
    wvarStrXSL = wvarStrXSL + "</tiposalarma>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template >";
    wvarStrXSL = wvarStrXSL + "     </xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    //
    return p_GetXSL;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
