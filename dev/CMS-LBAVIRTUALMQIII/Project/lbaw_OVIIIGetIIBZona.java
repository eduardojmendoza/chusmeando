import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVIIIGetIIBZona implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQIII.lbaw_OVIIIGetIIBZona";
  /**
   * Parametros XML de Entrada que se necesitan para los mensajes que consulta
   */
  static final String mcteParam_PAISSCOD = "//PAISSCOD";
  static final String mcteParam_V_PROVICOD = "//V_PROVICOD";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_CLIENIVA = "//CLIENIVA";
  static final String mcteParam_LISTA = "//LISTA";
  static final String mcteParam_CodIB = "//NUMERO";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    lbawA_OfVirtualLBA.lbaw_GetTipIngBrut wobjClass = new lbawA_OfVirtualLBA.lbaw_GetTipIngBrut();
    int wvarStep = 0;
    String wvarResponse = "";
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new diamondedge.util.XmlDom();
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //
      //
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //
      //
      wvarStep = 60;
      //
      // *******************************************************
      //Consulto primero si la provincia aplica Ingresos Brutos
      // *******************************************************
      wobjClass = new lbawA_OfVirtualLBA.lbaw_GetZonaIngBrut();
      wobjClass.Execute( Request, wvarResponse, "" );
      wobjClass = (lbawA_OfVirtualLBA.lbaw_GetTipIngBrut) null;
      //
      wvarStep = 70;
      wobjXMLResponse.loadXML( wvarResponse );
      //
      wvarStep = 80;
      if( null /*unsup wobjXMLResponse.selectSingleNode( "//PROVICOD" ) */ == (org.w3c.dom.Node) null )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje='No Existen Ingresos Brutos para la provincia seleccionada'/></Response>" );
      }
      else
      {
        // *******************************************************
        //Consulto los Ingresos Brutos
        // *******************************************************
        wobjClass = new lbawA_OfVirtualLBA.lbaw_GetTipIngBrut();
        wobjClass.Execute( Request, wvarResponse, "" );
        //
        wvarStep = 100;
        wobjClass = (lbawA_OfVirtualLBA.lbaw_GetTipIngBrut) null;
        Response.set( wvarResponse );
      }
      //
      wvarStep = 120;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjClass = (lbawA_OfVirtualLBA.lbaw_GetTipIngBrut) null;
      wobjXMLResponse = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
