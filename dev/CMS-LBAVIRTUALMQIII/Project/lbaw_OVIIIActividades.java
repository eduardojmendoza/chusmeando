import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVIIIActividades implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQIII.lbaw_OVIIIActividades";
  static final String mcteOpID = "1506";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_Actividad_A_Buscar = "//FILTROACTIVIDAD";
  static final String mcteParam_Codigo_Rubro = "//RAMOPCOD";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    org.w3c.dom.Node wobjNodoActividad = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarActividadBuscar = "";
    String wvarCodRubro = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //Deberá venir desde la página
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarCodRubro = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Rubro ) */ ) + Strings.space( 4 ), 4 );
      for( int nwobjNodoActividad = 0; nwobjNodoActividad < null /*unsup wobjXMLRequest.selectNodes( mcteParam_Actividad_A_Buscar ) */.getLength(); nwobjNodoActividad++ )
      {
        wobjNodoActividad = null /*unsup wobjXMLRequest.selectNodes( mcteParam_Actividad_A_Buscar ) */.item( nwobjNodoActividad );
        if( Strings.len( wvarActividadBuscar ) <= (60 * 60) )
        {
          //Solo se permite ingresar 60 descripciones de 60 caracteres cada una
          wvarActividadBuscar = wvarActividadBuscar + Strings.left( diamondedge.util.XmlDom.getText( wobjNodoActividad ) + Strings.space( 60 ), 60 );
        }
      }
      //
      //
      wvarStep = 60;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjNodoActividad = (org.w3c.dom.Node) null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los parámetros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 110;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarActividadBuscar;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      wvarMQError = wobjFrame2MQ.Execute( wvarArea, strParseString, null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG" ) */.toString() ).toInt();
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 240;
      wvarResult = "";

      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = "";
        //cantidad de caracteres ocupados por parámetros de entrada
        wvarPos = 127;
        wvarResult = invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseString), new Variant(wvarstrLen), new Variant(wvarCodRubro) } );
        //
        wvarStep = 340;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
        //
      }
      //
      wvarStep = 360;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarActividadBuscar + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int pvarPos, String pstrParseString, int pvarstrLen, String pvarRubroFiltro ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarCondicion = "";
    int wvarCantLin = 0;
    int wvarCounter = 0;
    VbScript.RegExp wobjRegExp = new VbScript.RegExp();
    void wobjColMatch;
    Object wobjMatch = null;
    VbScript.RegExp wobjRegSubexp = new VbScript.RegExp();
    Object wobjColSubMatch = null;
    Object wobjsubMatch = null;
    String wvarParseEvalString = "";
    diamondedge.util.XmlDom wobjXmlDOMResp = null;
    diamondedge.util.XmlDom wobjXslDOMResp = null;
    org.w3c.dom.Element wobjXmlElemenResp = null;
    org.w3c.dom.Element wobjXmlElemenRespP = null;
    org.w3c.dom.Element wobjXmlElemenRespC = null;
    org.w3c.dom.Element wobjXmlElemenRespCe = null;
    String wvarCodigoProfesion = "";
    int wvarCantRegistros = 0;
    int wvarCcurrRegistro = 0;
    int wvarCantCondi = 0;
    String wvarCodigoRubro = "";
    //RegExp
    //MatchCollection
    //Match
    //RegExp
    //MatchCollection
    //Match
    //
    wobjRegExp = new VbScript.RegExp();
    wobjRegSubexp = new VbScript.RegExp();
    wobjRegSubexp.Global.set( true );
    wobjRegExp.Global.set( true );
    //
    wvarCantRegistros = (int)Math.rint( VB.val( Strings.mid( pstrParseString, pvarPos, 4 ) ) );
    wvarCcurrRegistro = 0;
    //Elimino el parametro que indica la cantidad de registros devueltos
    wvarParseEvalString = Strings.mid( pstrParseString, pvarPos + 4 );
    wvarParseEvalString = Strings.replace( wvarParseEvalString, " ", "_" );

    //RAMOPCOD PIC X(04),LNK-PROFECOD PIC X(06), LNK-PROFEDES PIC X(60)
    wobjRegExp.Pattern.set( "(\\S{4})(\\S{6})(\\S{60})(\\S{20})" );

    //LNK-COBERTURAS OCCURS 10 TIMES. LNK-CODCONDI PIC X(02).
    wobjRegSubexp.Pattern.set( "(\\S{2})" );
    //
    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    wobjXmlDOMResp = new diamondedge.util.XmlDom();
    wobjXslDOMResp = new diamondedge.util.XmlDom();

    wobjXmlDOMResp.loadXML( "<RS></RS>" );
    //
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //
      //error: function 'SubMatches' was not found.
      //unsup: wvarCodigoProfesion = Replace(wobjMatch.SubMatches(1), "_", " ")
      //error: function 'SubMatches' was not found.
      //unsup: wvarCodigoRubro = Trim(Replace(wobjMatch.SubMatches(0), "_", " "))
      if( (!Strings.trim( wvarCodigoProfesion ).equals( "" )) && (wvarCcurrRegistro < wvarCantRegistros) )
      {
        if( Strings.toUpperCase( pvarRubroFiltro ).equals( Strings.toUpperCase( wvarCodigoRubro ) ) )
        {
          wvarCcurrRegistro = wvarCcurrRegistro + 1;
          wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "PROFESION" );
          /*unsup wobjXmlDOMResp.selectSingleNode( "//RS" ) */.appendChild( wobjXmlElemenResp );

          wobjXmlElemenRespP = wobjXmlDOMResp.getDocument().createElement( "RAMOPCOD" );
          wobjXmlElemenResp.appendChild( wobjXmlElemenRespP );
          diamondedge.util.XmlDom.setText( wobjXmlElemenRespP, wvarCodigoRubro );

          wobjXmlElemenRespP = wobjXmlDOMResp.getDocument().createElement( "PROFECOD" );
          wobjXmlElemenResp.appendChild( wobjXmlElemenRespP );
          diamondedge.util.XmlDom.setText( wobjXmlElemenRespP, Strings.trim( wvarCodigoProfesion ) );

          wobjXmlElemenRespP = wobjXmlDOMResp.getDocument().createElement( "PROFEDES" );
          wobjXmlElemenResp.appendChild( wobjXmlElemenRespP );
          //error: function 'SubMatches' was not found.
          //unsup: wobjXmlElemenRespP.Text = Trim(Replace(wobjMatch.SubMatches(2), "_", " "))
          //error: function 'SubMatches' was not found.
          //unsup: Set wobjColSubMatch = wobjRegSubexp.Execute(wobjMatch.SubMatches(3))
          wobjXmlElemenRespC = wobjXmlDOMResp.getDocument().createElement( "COBERCONDI" );
          wobjXmlElemenResp.appendChild( wobjXmlElemenRespC );
          wvarCantCondi = 0;
          //
          for( int nwobjsubMatch = 0; nwobjsubMatch < wobjColSubMatch.getCount(); nwobjsubMatch++ )
          {
            //
            //error: function 'SubMatches' was not found.
            //unsup: wvarCondicion = Replace(wobjsubMatch.SubMatches(0), "_", " ")
            if( !Strings.trim( wvarCondicion ).equals( "" ) )
            {
              wvarCantCondi = wvarCantCondi + 1;
              wobjXmlElemenRespCe = wobjXmlDOMResp.getDocument().createElement( "CODCONDI" );
              wobjXmlElemenRespC.appendChild( wobjXmlElemenRespCe );
              //error: function 'SubMatches' was not found.
              //unsup: wobjXmlElemenRespCe.Text = Trim(Replace(wobjsubMatch.SubMatches(0), "_", " "))
            }
            //
          }
          wobjXmlElemenRespC.setAttribute( "CANTCONDI", String.valueOf( wvarCantCondi ) );
        }
      }
      //
    }
    //
    //unsup wobjXslDOMResp.async = false;
    wobjXslDOMResp.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
    ParseoMensaje = Strings.replace( new Variant() /*unsup wobjXmlDOMResp.transformNode( wobjXslDOMResp ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
    //ParseoMensaje = wobjXmlDOMResp.xml
    //
    wobjXmlDOMResp = (diamondedge.util.XmlDom) null;
    wobjXmlElemenResp = (org.w3c.dom.Element) null;
    wobjXmlElemenRespP = (org.w3c.dom.Element) null;
    wobjXmlElemenRespC = (org.w3c.dom.Element) null;
    wobjXmlElemenRespCe = (org.w3c.dom.Element) null;
    wobjXslDOMResp = (diamondedge.util.XmlDom) null;
    //
    wobjRegExp = (VbScript.RegExp) null;
    wobjRegSubexp = (VbScript.RegExp) null;

    return ParseoMensaje;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<?xml version='1.0' encoding='UTF-8'?>";
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>";
    wvarStrXSL = wvarStrXSL + "<xsl:output method='html' version='1.0' encoding='UTF-8' indent='yes'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:template match='/' >";
    wvarStrXSL = wvarStrXSL + "<xsl:for-each select='//PROFESION'>";
    wvarStrXSL = wvarStrXSL + "<xsl:sort select='./PROFEDES'/>";
    wvarStrXSL = wvarStrXSL + "     <option>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='value'><xsl:value-of select='PROFECOD'/></xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='Rubro'><xsl:value-of select='RAMOPCOD'/></xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='CantCondiciones'><xsl:value-of select='./COBERCONDI/@CANTCONDI'/></xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "     <xsl:value-of select='PROFECOD'/> <xsl:text> </xsl:text> <xsl:value-of select='PROFEDES'/>";
    //wvarStrXSL = wvarStrXSL & "     <xsl:for-each select='./COBERCONDI/CODCONDI'>"
    //wvarStrXSL = wvarStrXSL & "     <xsl:text>   </xsl:text><xsl:value-of select='.'/>"
    //wvarStrXSL = wvarStrXSL & "     </xsl:for-each>"
    wvarStrXSL = wvarStrXSL + "     </option>";
    wvarStrXSL = wvarStrXSL + "     </xsl:for-each>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template >";
    wvarStrXSL = wvarStrXSL + "     </xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    //
    return p_GetXSL;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
