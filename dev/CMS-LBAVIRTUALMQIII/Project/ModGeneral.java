import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 *  Parametros XML de Configuracion
 */

public class ModGeneral
{
  public static final String gcteConfFileName = "LBAVirtualMQConfig.xml";
  public static final String gcteQueueManager = "//QUEUEMANAGER";
  public static final String gctePutQueue = "//PUTQUEUE";
  public static final String gcteGetQueue = "//GETQUEUE";
  public static final String gcteGMOWaitInterval = "//GMO_WAITINTERVAL";
  /**
   *  Parametros XML del Cotizador
   */
  public static final String gcteParamFileName = "ParametrosMQ.xml";
  public static final String gcteNodosGenerales = "//GENERALES";
  public static final String gcteCIAASCOD = "/CIAASCOD";
  public static final String gcteClassMQConnection = "WD.Frame2MQ";

  public static String MidAsString( String pvarStringCompleto, Variant pvarActualCounter, int pvarLongitud ) throws Exception
  {
    String MidAsString = "";
    MidAsString = Strings.mid( pvarStringCompleto, pvarActualCounter.toInt(), pvarLongitud );
    pvarActualCounter.set( pvarActualCounter.add( new Variant( pvarLongitud ) ) );
    return MidAsString;
  }
}
