import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVIIIGetDeducib implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQIII.lbaw_OVIIIGetDeducib";
  static final String mcteOpID = "1530";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_Codigo_Ramo = "//RAMOPCOD";
  static final String mcteParam_Comercio = "//COMERCIO";
  static final String mcteParam_Codigo_Profesion = "//I_ACTIVIDAD";
  static final String mcteParam_Zona = "//I_ZONA";
  static final String mcteParam_ModuloActual = "//I_CODIGOMODULO";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * Const mcteParam_Codigo_Profesion    As String = "//ACTIVIDAD"
   * Const mcteParam_Zona                As String = "//ZONA"
   * Const mcteParam_ModuloActual        As String = "//MODULO"
   */
  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarModuloActual = "";
    String wvarCodProfesion = "";
    String wvarCodRamo = "";
    String wvarCodZona = "";
    String wvarAreaToSend = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarCodRamo = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Ramo ) */ ) + Strings.space( 4 ), 4 );
      wvarCodProfesion = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Codigo_Profesion ) */ ) + Strings.space( 6 ), 6 );
      wvarCodZona = Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Zona ) */ ), 4 );
      //
      wvarAreaToSend = wvarCodRamo + wvarCodProfesion + wvarCodZona;
      wvarAreaToSend = wvarAreaToSend + invoke( "GetVectorCoberturas", new Variant[] { new Variant(null /*unsup wobjXMLRequest.selectSingleNode( new Variant(".") ) */), new Variant(mcteParam_Comercio), new Variant(30) } );
      //
      wvarStep = 60;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 70;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los parámetros generales (ParametrosMQ.xml)
      wvarStep = 100;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 110;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarAreaToSend;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      wvarMQError = wobjFrame2MQ.Execute( wvarArea, strParseString, null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG" ) */.toString() ).toInt();
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 240;
      //
      parsear: 
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = "";
        //cantidad de caracteres ocupados por parámetros de entrada
        wvarPos = 234;
        wvarResult = invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseString), new Variant(wvarstrLen) } );
        //
        wvarStep = 340;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
        //
      }
      //
      wvarStep = 360;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarAreaToSend + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int pvarPos, String pstrParseString, int pvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    VbScript.RegExp wobjRegExp = new VbScript.RegExp();
    void wobjColMatch;
    Object wobjMatch = null;
    String wvarParseEvalString = "";
    diamondedge.util.XmlDom wobjXmlDOMResp = null;
    org.w3c.dom.Element wobjXmlElemenResp = null;
    org.w3c.dom.Element wobjXmlElemenRespE = null;
    int wvarCantRegistros = 0;
    int wvarCcurrRegistro = 0;
    //
    //RegExp
    //MatchCollection
    //Match
    //
    //
    wobjRegExp = new VbScript.RegExp();
    wobjRegExp.Global.set( true );
    //
    wobjRegExp.Pattern.set( "(\\S{3})(\\S{2})(\\S{60})" );
    //
    wvarParseEvalString = Strings.mid( pstrParseString, pvarPos + 3 );
    wvarParseEvalString = Strings.replace( wvarParseEvalString, " ", "_" );
    wvarCantRegistros = (int)Math.rint( VB.val( Strings.mid( pstrParseString, pvarPos, 3 ) ) );
    wvarCcurrRegistro = 0;

    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    wobjXmlDOMResp = new diamondedge.util.XmlDom();

    wobjXmlDOMResp.loadXML( "<DEDUCIBLES></DEDUCIBLES>" );
    //
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //
      if( ! (wvarCcurrRegistro < wvarCantRegistros) )
      {
        break;
      }
      wvarCcurrRegistro = wvarCcurrRegistro + 1;
      //
      wobjXmlElemenResp = wobjXmlDOMResp.getDocument().createElement( "DEDUCIBLE" );
      /*unsup wobjXmlDOMResp.selectSingleNode( "//DEDUCIBLES" ) */.appendChild( wobjXmlElemenResp );
      //
      wobjXmlElemenRespE = wobjXmlDOMResp.getDocument().createElement( "CODIGOCOBERTURA" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespE );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlElemenRespE.Text = UCase(Trim(Replace(wobjMatch.SubMatches(0), "_", " ")))
      //
      wobjXmlElemenRespE = wobjXmlDOMResp.getDocument().createElement( "CODIGODEDUCIBLE" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespE );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlElemenRespE.Text = Trim(Replace(wobjMatch.SubMatches(1), "_", " "))
      //
      wobjXmlElemenRespE = wobjXmlDOMResp.getDocument().createElement( "DESCRIPCION" );
      wobjXmlElemenResp.appendChild( wobjXmlElemenRespE );
      //error: function 'SubMatches' was not found.
      //unsup: wobjXmlElemenRespE.Text = Trim(Replace(wobjMatch.SubMatches(2), "_", " "))
      //
    }
    //
    ParseoMensaje = wobjXmlDOMResp.getDocument().getDocumentElement().toString();
    //
    wobjXmlDOMResp = (diamondedge.util.XmlDom) null;
    wobjXmlElemenResp = (org.w3c.dom.Element) null;
    wobjXmlElemenRespE = (org.w3c.dom.Element) null;
    //
    wobjRegExp = (VbScript.RegExp) null;
    //
    return ParseoMensaje;
  }

  private String GetVectorCoberturas( org.w3c.dom.Node pobjNodo, String pVarPathVector, int pVarCantElementos ) throws Exception
  {
    String GetVectorCoberturas = "";
    int wvarCounterRelleno = 0;
    int wvarActualCounter = 0;
    org.w3c.dom.NodeList wobjNodosComercios = null;
    org.w3c.dom.Node wobjNodoComercio = null;
    org.w3c.dom.NodeList wobjNodosCobertura = null;
    org.w3c.dom.Node wobjNodoCobertura = null;

    wobjNodosComercios = null /*unsup pobjNodo.selectNodes( pVarPathVector ) */;
    wvarActualCounter = 0;
    for( int nwobjNodoComercio = 0; nwobjNodoComercio < wobjNodosComercios.getLength(); nwobjNodoComercio++ )
    {
      wobjNodoComercio = wobjNodosComercios.item( nwobjNodoComercio );
      wobjNodosCobertura = wobjNodoComercio.getChildNodes();
      for( int nwobjNodoCobertura = 0; nwobjNodoCobertura < wobjNodosCobertura.getLength(); nwobjNodoCobertura++ )
      {
        wobjNodoCobertura = wobjNodosCobertura.item( nwobjNodoCobertura );
        if( Strings.toUpperCase( wobjNodoCobertura.getNodeName() ).matches( "C*" ) )
        {
          if( VB.val( diamondedge.util.XmlDom.getText( wobjNodoCobertura ) ) > 0 )
          {
            //Cobertura con Valor
            GetVectorCoberturas = GetVectorCoberturas + Strings.right( "000" + Strings.mid( wobjNodoCobertura.getNodeName(), 2 ), 3 );
            GetVectorCoberturas = GetVectorCoberturas + Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup pobjNodo.selectSingleNode( mcteParam_ModuloActual ) */ ), 2 );
            wvarActualCounter = wvarActualCounter + 1;
            if( wvarActualCounter == (pVarCantElementos - 1) )
            {
              //unsup GoTo ClearObjects
            }
          }
        }
      }
    }

    for( wvarCounterRelleno = wvarActualCounter; wvarCounterRelleno <= (pVarCantElementos - 1); wvarCounterRelleno++ )
    {
      GetVectorCoberturas = GetVectorCoberturas + "000";
    }
    GetVectorCoberturas = Strings.format( wvarActualCounter, "#000" ) + GetVectorCoberturas;

    ClearObjects: 
    wobjNodosComercios = (org.w3c.dom.NodeList) null;
    wobjNodoComercio = (org.w3c.dom.Node) null;
    wobjNodosCobertura = (org.w3c.dom.NodeList) null;
    wobjNodoCobertura = (org.w3c.dom.Node) null;
    return GetVectorCoberturas;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
