#!/bin/bash
mvn install:install-file -Dfile=saxon9.jar -DgroupId=saxon -DartifactId=saxon9 -Dversion=9 -Dpackaging=jar
mvn install:install-file -Dfile=saxon9-dom.jar -DgroupId=saxon -DartifactId=saxon9-dom -Dversion=9 -Dpackaging=jar
mvn install:install-file -DgroupId=com.ibm.mq -DartifactId=mq -Dversion=7.5 -Dpackaging=jar -Dfile=com.ibm.mq-7.5.jar
mvn install:install-file -DgroupId=com.ibm.mq -DartifactId=mqjms -Dversion=7.5 -Dpackaging=jar -Dfile=com.ibm.mqjms-7.5.jar
mvn install:install-file -DgroupId=com.ibm.mq -DartifactId=jmqi -Dversion=7.5 -Dpackaging=jar -Dfile=com.ibm.mq.jmqi-7.5.jar
mvn install:install-file -DgroupId=com.ibm.mq -DartifactId=headers -Dversion=7.5 -Dpackaging=jar -Dfile=com.ibm.mq.headers-7.5.jar
mvn install:install-file -Dfile=dhbcore.jar -DgroupId=com.ibm -DartifactId=dhbcore -Dversion=7.5 -Dpackaging=jar
mvn install:install-file -Dfile=servingxml.jar -DgroupId=com.servingxml -DartifactId=servingxml -Dversion=1.1.2 -Dpackaging=jar


