// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Color;
import java.awt.Font;
import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

// Referenced classes of package diamondedge.vb:
//            OptionButton, Screen, VbPanel

public class TitledPanel extends javax.swing.JPanel
{

    public TitledPanel()
    {
        _fldif = null;
        _flddo = true;
        _fldfor = -1;
        buttonGroup = null;
        setLayout(null);
        setFont(diamondedge.vb.Screen.getDefaultFont());
        _fldif = javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder());
        setBorder(_fldif);
        setEnabled(true);
    }

    public java.lang.String getTitle()
    {
        return _fldif.getTitle();
    }

    public void setTitle(java.lang.String s)
    {
        _fldif.setTitle(s);
        repaint();
    }

    public java.lang.String getTag()
    {
        return a;
    }

    public void setTag(java.lang.String s)
    {
        a = s;
    }

    public void setFont(java.awt.Font font)
    {
        if(_fldif != null)
            _fldif.setTitleFont(font);
        super.setFont(font);
    }

    public void setForeground(java.awt.Color color)
    {
        if(_fldif != null)
            _fldif.setTitleColor(color);
        super.setForeground(color);
    }

    public void setBackground(java.awt.Color color)
    {
        if(java.lang.Math.min(java.lang.Math.min(color.getRed(), color.getGreen()), color.getBlue()) < 65 && _fldif != null)
            _fldif.setBorder(new EtchedBorder(1, java.awt.Color.white, java.awt.Color.lightGray));
        super.setBackground(color);
    }

    public boolean isBordered()
    {
        return _flddo;
    }

    public void setBordered(boolean flag)
    {
        _flddo = flag;
        if(flag)
            setBorder(_fldif);
        else
            setBorder(null);
    }

    public int getAlignment()
    {
        return _fldif.getTitleJustification();
    }

    public void setAlignment(int i)
    {
        _fldif.setTitleJustification(i);
    }

    public void addExclusiveButton(javax.swing.AbstractButton abstractbutton)
    {
        add(abstractbutton);
        if(buttonGroup == null)
            buttonGroup = new ButtonGroup();
        buttonGroup.add(abstractbutton);
        if((abstractbutton instanceof diamondedge.vb.OptionButton) && _fldfor >= 0 && ((diamondedge.vb.OptionButton)abstractbutton).getOptionValue() == _fldfor)
            ((diamondedge.vb.OptionButton)abstractbutton).doClick();
    }

    public int getOptionValue()
    {
        java.util.Enumeration enumeration = null;
        if(buttonGroup != null)
            enumeration = buttonGroup.getElements();
        if(enumeration != null)
            while(enumeration.hasMoreElements()) 
            {
                java.lang.Object obj = enumeration.nextElement();
                if((obj instanceof diamondedge.vb.OptionButton) && ((diamondedge.vb.OptionButton)obj).isSelected())
                {
                    _fldfor = ((diamondedge.vb.OptionButton)obj).getOptionValue();
                    return _fldfor;
                }
            }
        return _fldfor;
    }

    public void setOptionValue(int i)
    {
        _fldfor = i;
        java.util.Enumeration enumeration = null;
        if(buttonGroup != null)
            enumeration = buttonGroup.getElements();
        java.lang.Object obj;
        if(enumeration != null)
            do
            {
                if(!enumeration.hasMoreElements())
                    break;
                obj = enumeration.nextElement();
                if(!(obj instanceof diamondedge.vb.OptionButton) || ((diamondedge.vb.OptionButton)obj).getOptionValue() != i)
                    continue;
                ((diamondedge.vb.OptionButton)obj).doClick();
                break;
            } while(true);
    }

    public void setBounds(int i, int j, int k, int l)
    {
        super.setBounds(i, j, k, l);
        if(!isEnabled())
            diamondedge.vb.VbPanel.a(this, i, j, k, l);
    }

    public void setEnabled(boolean flag)
    {
        diamondedge.vb.VbPanel.a(this, flag);
        super.setEnabled(flag);
        if(flag)
            _fldif.setTitleColor(null);
        else
        if(getBackground() == null)
            _fldif.setTitleColor(java.awt.Color.gray);
        else
            _fldif.setTitleColor(getBackground().darker());
    }

    private java.lang.String a = null;
    private javax.swing.border.TitledBorder _fldif = null;
    private boolean _flddo = false;
    private int _fldfor = 0;
    protected javax.swing.ButtonGroup buttonGroup = null;
    public static final int LEFT = 1;
    public static final int CENTER = 2;
    public static final int RIGHT = 3;
}
