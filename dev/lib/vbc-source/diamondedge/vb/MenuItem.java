// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import javax.swing.JMenuItem;

// Referenced classes of package diamondedge.vb:
//            Screen, JForm

public class MenuItem extends javax.swing.JMenuItem
{

    public MenuItem()
    {
        setFont(diamondedge.vb.Screen.getDefaultFont());
    }

    public void setText(java.lang.String s)
    {
        super.setText(diamondedge.vb.JForm.processMnemonic(this, s));
    }

    public java.lang.String getTag()
    {
        return a != null ? a.toString() : null;
    }

    public void setTag(java.lang.Object obj)
    {
        a = obj;
    }

    public java.lang.Object getTagObject()
    {
        return a;
    }

    private java.lang.Object a = null;
}
