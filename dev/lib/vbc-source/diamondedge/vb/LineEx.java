// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.swing.DsLine;

// Referenced classes of package diamondedge.vb:
//            VbPanel, Screen

public class LineEx extends diamondedge.swing.DsLine
{

    public LineEx()
    {
        x = -1F;
        y = -1F;
        x2 = -1F;
        y2 = -1F;
    }

    public synchronized void setupBounds()
    {
        int i = _mthif(x);
        int j = a(y);
        int k = _mthif(x2);
        int l = a(y2);
        int i1 = java.lang.Math.min(i, k);
        int j1 = java.lang.Math.min(j, l);
        if(i1 < 0 || j1 < 0)
            return;
        int k1 = java.lang.Math.abs(k - i);
        int l1 = java.lang.Math.abs(l - j);
        double d = (double)(l - j) / (double)((k - i) + (k1 != 0 ? 0 : 1));
        int i2 = (int)java.lang.Math.round(getLineWidth());
        if(k1 < i2)
        {
            i1 -= i2 / 2;
            k1 = i2;
        }
        if(l1 < i2)
        {
            j1 -= i2 / 2;
            l1 = i2;
        }
        if(d > 0.0D)
            setLineSlantingUp(false);
        else
            setLineSlantingUp(true);
        setBounds(i1, j1, k1, l1);
    }

    public float getX1()
    {
        return x;
    }

    public void setX1(float f)
    {
        x = f;
        setupBounds();
    }

    public float getX2()
    {
        return x2;
    }

    public void setX2(float f)
    {
        x2 = f;
        setupBounds();
    }

    public float getY1()
    {
        return y;
    }

    public void setY1(float f)
    {
        y = f;
        setupBounds();
    }

    public float getY2()
    {
        return y2;
    }

    public void setY2(float f)
    {
        y2 = f;
        setupBounds();
    }

    private int _mthif(float f)
    {
        if(getParent() instanceof diamondedge.vb.VbPanel)
        {
            diamondedge.vb.VbPanel vbpanel = (diamondedge.vb.VbPanel)getParent();
            return (int)vbpanel.scaleX(f, vbpanel.getScaleMode(), 3);
        } else
        {
            return (int)((double)f * diamondedge.vb.Screen.pixelsPerTwip);
        }
    }

    private int a(float f)
    {
        if(getParent() instanceof diamondedge.vb.VbPanel)
        {
            diamondedge.vb.VbPanel vbpanel = (diamondedge.vb.VbPanel)getParent();
            return (int)vbpanel.scaleY(f, vbpanel.getScaleMode(), 3);
        } else
        {
            return (int)((double)f * diamondedge.vb.Screen.pixelsPerTwip);
        }
    }

    public java.lang.String getTag()
    {
        return a;
    }

    public void setTag(java.lang.String s)
    {
        a = s;
    }

    private java.lang.String a = null;
    public float x = 0;
    public float y = 0;
    public float x2 = 0;
    public float y2 = 0;
}
