// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Component;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.LayoutFocusTraversalPolicy;

// Referenced classes of package diamondedge.vb:
//            VbTabOrderComparator

final class VbFocusPolicy extends javax.swing.LayoutFocusTraversalPolicy
{

    VbFocusPolicy()
    {
        setComparator(new VbTabOrderComparator(getComparator()));
    }

    protected boolean accept(java.awt.Component component)
    {
        for(java.lang.Object obj = component; (obj = ((java.awt.Component) (obj)).getParent()) != null;)
            if(!((java.awt.Component) (obj)).isEnabled())
                return false;

        if((component instanceof javax.swing.JScrollBar) && (component.getParent() instanceof javax.swing.JScrollPane))
            return false;
        else
            return super.accept(component);
    }
}
