// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;


public class CtrlConst
{

    public CtrlConst()
    {
    }

    public static final int grdSingle = 0;
    public static final int grdRepeat = 1;
    public static final int ssDarkGrey = 0;
    public static final int ssBlack = 1;
    public static final int ssPictureToButton = 1;
    public static final int ssButtonToPicture = 2;
    public static final int ssLeftToRight = 1;
    public static final int ssRightToLeft = 2;
    public static final int ssTopToBottom = 3;
    public static final int ssBottomToTop = 4;
    public static final int ssWideningCircle = 5;
    public static final int ssNoChange = 0;
    public static final int ssDither = 1;
    public static final int ssInvert = 2;
    public static final int vbFlat = 0;
    public static final int vb3D = 1;
    public static final int ccScrollingStandard = 0;
    public static final int ccScrollingSmooth = 1;
    public static final int ccDragManual = 0;
    public static final int ccDragAutomatic = 1;
    public static final int ccDropNone = 0;
    public static final int ccDropManual = 1;
    public static final int ccDropAutomatic = 2;
    public static final int ccEnter = 0;
    public static final int ccLeave = 1;
    public static final int ccOver = 2;
    public static final int ccDropEffectNone = 0;
    public static final int ccDropEffectCopy = 1;
    public static final int ccDropEffectMove = 2;
    public static final int ccDropEffectScroll = 0x80000000;
    public static final int ccTransparent = 0;
    public static final int ccOpaque = 1;
    public static final int tbrDefault = 0;
    public static final int tbrCheck = 1;
    public static final int tbrButtonGroup = 2;
    public static final int tbrSeparator = 3;
    public static final int tbrPlaceholder = 4;
    public static final int tbrDropdown = 5;
    public static final int tbrUnpressed = 0;
    public static final int tbrPressed = 1;
    public static final int sbrNoBevel = 0;
    public static final int sbrInset = 1;
    public static final int sbrRaised = 2;
    public static final int sbrText = 0;
    public static final int sbrCaps = 1;
    public static final int sbrNum = 2;
    public static final int sbrIns = 3;
    public static final int sbrScrl = 4;
    public static final int sbrTime = 5;
    public static final int sbrDate = 6;
    public static final int sbrKana = 7;
    public static final int lvwNone = 0;
    public static final int lvwAutoLeft = 1;
    public static final int lvwAutoTop = 2;
    public static final int lvwTopLeft = 0;
    public static final int lvwTopRight = 1;
    public static final int lvwBottomLeft = 2;
    public static final int lvwBottomRight = 3;
    public static final int lvwCenter = 4;
    public static final int lvwTile = 5;
    public static final int imlNormal = 0;
    public static final int imlTransparent = 1;
    public static final int imlSelected = 2;
    public static final int imlFocus = 3;
    public static final int sldBottomRight = 0;
    public static final int sldTopLeft = 1;
    public static final int sldBoth = 2;
    public static final int sldNoTicks = 3;
    public static final int sldAboveLeft = 0;
    public static final int sldBelowRight = 1;
    public static final int ImgCboDropdownCombo = 0;
    public static final int ImgCboSimpleCombo = 1;
    public static final int ImgCboDropdownList = 2;
    public static final int adDoMoveFirst = 0;
    public static final int adStayBOF = 1;
    public static final int adDoMoveLast = 0;
    public static final int adStayEOF = 1;
    public static final int adDoAddNew = 2;
}
