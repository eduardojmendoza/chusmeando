// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.UI;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JComponent;
import javax.swing.JScrollPane;

public class ScrolledComponent extends javax.swing.JScrollPane
    implements java.awt.event.FocusListener, java.awt.event.KeyListener, java.awt.event.MouseListener, java.awt.event.MouseMotionListener
{

    ScrolledComponent(javax.swing.JComponent jcomponent)
    {
        super(jcomponent);
        component = null;
        setComponent(jcomponent);
    }

    public java.awt.Component getComponent()
    {
        return component;
    }

    public void setComponent(javax.swing.JComponent jcomponent)
    {
        if(component != null)
        {
            component.removeMouseListener(this);
            component.removeMouseMotionListener(this);
            component.removeKeyListener(this);
            component.removeFocusListener(this);
        }
        component = jcomponent;
        if(component != null)
        {
            component.addMouseListener(this);
            component.addKeyListener(this);
            component.addFocusListener(this);
        }
    }

    static int a(javax.swing.JScrollPane jscrollpane)
    {
        boolean flag = jscrollpane.getHorizontalScrollBarPolicy() != 31;
        boolean flag1 = jscrollpane.getVerticalScrollBarPolicy() != 21;
        if(flag1 && flag)
            return 3;
        if(flag1)
            return 2;
        return !flag ? 0 : 1;
    }

    static void a(javax.swing.JScrollPane jscrollpane, int i)
    {
        if(jscrollpane == null)
            return;
        switch(i)
        {
        case 0: // '\0'
            jscrollpane.setHorizontalScrollBarPolicy(31);
            jscrollpane.setVerticalScrollBarPolicy(21);
            break;

        case 1: // '\001'
            jscrollpane.setHorizontalScrollBarPolicy(32);
            jscrollpane.setVerticalScrollBarPolicy(21);
            break;

        case 2: // '\002'
            jscrollpane.setHorizontalScrollBarPolicy(31);
            jscrollpane.setVerticalScrollBarPolicy(22);
            break;

        case 3: // '\003'
        default:
            jscrollpane.setHorizontalScrollBarPolicy(30);
            jscrollpane.setVerticalScrollBarPolicy(20);
            break;
        }
    }

    public int getScrollBarPolicy()
    {
        return diamondedge.vb.ScrolledComponent.a(this);
    }

    public void setScrollBarPolicy(int i)
    {
        diamondedge.vb.ScrolledComponent.a(this, i);
    }

    public boolean hasFocus()
    {
        if(component != null)
            return component.hasFocus();
        else
            return super.hasFocus();
    }

    public boolean requestFocus(boolean flag)
    {
        if(component != null)
            return component.requestFocus(flag);
        else
            return super.requestFocus(flag);
    }

    public void requestFocus()
    {
        requestFocus(false);
    }

    public boolean requestFocusInWindow()
    {
        return requestFocus(false);
    }

    public void setBackground(java.awt.Color color)
    {
        if(component != null)
            component.setBackground(color);
        super.setBackground(color);
    }

    public void setCursor(java.awt.Cursor cursor)
    {
        if(component != null)
            component.setCursor(cursor);
        super.setCursor(cursor);
    }

    public void setEnabled(boolean flag)
    {
        if(component != null)
            component.setEnabled(flag);
        super.setEnabled(flag);
    }

    public void setFont(java.awt.Font font)
    {
        if(component != null)
            component.setFont(font);
        super.setFont(font);
    }

    public void setForeground(java.awt.Color color)
    {
        if(component != null)
            component.setForeground(color);
        super.setForeground(color);
    }

    public void setSize(java.awt.Dimension dimension)
    {
        if(component != null)
            component.setSize(dimension);
        super.setSize(dimension);
    }

    public void setSize(int i, int j)
    {
        if(component != null)
            component.setSize(i, j);
        super.setSize(i, j);
    }

    public void setVisible(boolean flag)
    {
        if(component != null)
            component.setVisible(flag);
        super.setVisible(flag);
    }

    public void addMouseMotionListener(java.awt.event.MouseMotionListener mousemotionlistener)
    {
        if(component != null)
        {
            java.util.EventListener aeventlistener[] = component.getListeners(java.awt.event.MouseMotionListener.class);
            boolean flag = false;
            for(int i = 0; i < aeventlistener.length; i++)
                if(aeventlistener[i] == this)
                    flag = true;

            if(!flag)
                component.addMouseMotionListener(this);
        }
        super.addMouseMotionListener(mousemotionlistener);
    }

    public void removeMouseMotionListener(java.awt.event.MouseMotionListener mousemotionlistener)
    {
        if(component != null && getListeners(java.awt.event.MouseMotionListener.class).length == 0)
            component.removeMouseMotionListener(this);
        super.removeMouseMotionListener(mousemotionlistener);
    }

    public void mouseClicked(java.awt.event.MouseEvent mouseevent)
    {
        diamondedge.util.UI.forwardMouseClicked(mouseevent, this);
    }

    public void mousePressed(java.awt.event.MouseEvent mouseevent)
    {
        diamondedge.util.UI.forwardMousePressed(mouseevent, this);
    }

    public void mouseReleased(java.awt.event.MouseEvent mouseevent)
    {
        diamondedge.util.UI.forwardMouseReleased(mouseevent, this);
    }

    public void mouseEntered(java.awt.event.MouseEvent mouseevent)
    {
        diamondedge.util.UI.forwardMouseEntered(mouseevent, this);
    }

    public void mouseExited(java.awt.event.MouseEvent mouseevent)
    {
        diamondedge.util.UI.forwardMouseExited(mouseevent, this);
    }

    public void mouseDragged(java.awt.event.MouseEvent mouseevent)
    {
        diamondedge.util.UI.forwardMouseDragged(mouseevent, this);
    }

    public void mouseMoved(java.awt.event.MouseEvent mouseevent)
    {
        diamondedge.util.UI.forwardMouseMoved(mouseevent, this);
    }

    public void keyPressed(java.awt.event.KeyEvent keyevent)
    {
        diamondedge.util.UI.forwardKeyPressed(keyevent, this);
    }

    public void keyReleased(java.awt.event.KeyEvent keyevent)
    {
        diamondedge.util.UI.forwardKeyReleased(keyevent, this);
    }

    public void keyTyped(java.awt.event.KeyEvent keyevent)
    {
        diamondedge.util.UI.forwardKeyTyped(keyevent, this);
    }

    public void focusGained(java.awt.event.FocusEvent focusevent)
    {
        diamondedge.util.UI.forwardFocusGained(focusevent, this);
    }

    public void focusLost(java.awt.event.FocusEvent focusevent)
    {
        diamondedge.util.UI.forwardFocusLost(focusevent, this);
    }

    private javax.swing.JComponent component = null;
}
