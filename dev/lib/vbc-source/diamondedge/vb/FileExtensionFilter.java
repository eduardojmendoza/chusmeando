// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.io.File;
import java.io.FileFilter;
import java.util.Enumeration;
import java.util.Hashtable;

class FileExtensionFilter
    implements java.io.FileFilter
{

    public FileExtensionFilter(boolean flag, java.lang.String s)
    {
        _fldif = null;
        a = null;
        _fldfor = null;
        _flddo = true;
        _flddo = flag;
        _fldif = new Hashtable();
        if(s != null)
            setDescription(s);
    }

    public boolean accept(java.io.File file)
    {
        if(file != null)
        {
            if(file.isDirectory() && _flddo)
                return true;
            java.lang.String s = getExtension(file);
            if(s != null && _fldif.get(getExtension(file)) != null)
                return true;
        }
        return false;
    }

    public java.lang.String getExtension(java.io.File file)
    {
        if(file != null)
        {
            java.lang.String s = file.getName();
            int i = s.lastIndexOf('.');
            if(i > 0 && i < s.length() - 1)
                return s.substring(i + 1).toLowerCase();
        }
        return null;
    }

    public void addExtension(java.lang.String s)
    {
        if(_fldif == null)
            _fldif = new Hashtable(5);
        _fldif.put(s.toLowerCase(), this);
        _fldfor = null;
    }

    public java.lang.String getDescription()
    {
        if(_fldfor == null)
            if(a == null)
            {
                _fldfor = "(";
                java.util.Enumeration enumeration = _fldif.keys();
                if(enumeration != null)
                    for(_fldfor += "*." + (java.lang.String)enumeration.nextElement(); enumeration.hasMoreElements(); _fldfor += ", *." + (java.lang.String)enumeration.nextElement());
                _fldfor += ")";
            } else
            {
                _fldfor = a;
            }
        return _fldfor;
    }

    public void setDescription(java.lang.String s)
    {
        a = s;
        _fldfor = null;
    }

    private java.util.Hashtable _fldif = null;
    private java.lang.String a = null;
    private java.lang.String _fldfor = null;
    private boolean _flddo = false;
}
