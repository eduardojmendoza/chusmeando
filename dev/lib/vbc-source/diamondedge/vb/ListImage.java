// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.HashList;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

// Referenced classes of package diamondedge.vb:
//            Screen, ListImages

public class ListImage extends javax.swing.JPanel
{

    public ListImage(diamondedge.vb.ListImages listimages, java.lang.String s)
    {
        _flddo = null;
        _fldif = null;
        _fldint = null;
        _fldfor = null;
        a = null;
        a = listimages;
        _fldif = s;
    }

    public javax.swing.ImageIcon getIcon()
    {
        if(_fldfor != null)
            return _fldfor;
        if(_fldint != null)
            _fldfor = new ImageIcon(_fldint);
        return _fldfor;
    }

    public javax.swing.ImageIcon getScaledIcon(int i, int j)
    {
        if(_fldfor != null)
            return _fldfor;
        if(_fldint != null)
            _fldfor = new ImageIcon(getScaledImage(i, j));
        return _fldfor;
    }

    public java.awt.Image getImage()
    {
        return _fldint;
    }

    public java.awt.Image getScaledImage(int i, int j)
    {
        return _fldint.getScaledInstance(i, j, 1);
    }

    public void setImage(java.awt.Image image)
    {
        _fldint = image;
    }

    public void setImage(java.lang.String s)
    {
        _fldint = diamondedge.vb.Screen.loadImage(s);
    }

    public int getIndex()
    {
        return a.getCollection().getIndex(_fldif);
    }

    public void setIndex(int i)
        throws java.lang.IndexOutOfBoundsException
    {
        a.remove(_fldif);
        a.getCollection().add(this, _fldif, i, 0);
    }

    public java.lang.String getKey()
    {
        return _fldif;
    }

    public void setKey(java.lang.String s)
    {
        if(s == null || s.equals(""))
        {
            return;
        } else
        {
            a.getCollection().changeKey(_fldif, s);
            _fldif = s;
            return;
        }
    }

    public java.lang.Object getData()
    {
        return _flddo;
    }

    public void setData(java.lang.Object obj)
    {
        _flddo = obj;
    }

    private java.lang.Object _flddo = null;
    private java.lang.String _fldif = null;
    private java.awt.Image _fldint = null;
    private javax.swing.ImageIcon _fldfor = null;
    private diamondedge.vb.ListImages a = null;
}
