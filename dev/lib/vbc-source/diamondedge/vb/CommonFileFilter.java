// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.swing.filechooser.FileFilter;

class CommonFileFilter extends javax.swing.filechooser.FileFilter
{

    public CommonFileFilter(java.lang.String s)
    {
        _fldif = null;
        a = null;
        _flddo = null;
        _fldif = new Hashtable();
        if(s != null)
            setDescription(s);
    }

    public boolean accept(java.io.File file)
    {
        if(file != null)
        {
            if(file.isDirectory())
                return true;
            java.lang.String s = getExtension(file);
            if(s != null && _fldif.get(getExtension(file)) != null)
                return true;
        }
        return false;
    }

    public java.lang.String getExtension(java.io.File file)
    {
        if(file != null)
        {
            java.lang.String s = file.getName();
            int i = s.lastIndexOf('.');
            if(i > 0 && i < s.length() - 1)
                return s.substring(i + 1).toLowerCase();
        }
        return null;
    }

    public void addExtension(java.lang.String s)
    {
        if(_fldif == null)
            _fldif = new Hashtable(5);
        _fldif.put(s.toLowerCase(), this);
        _flddo = null;
    }

    public java.lang.String getDescription()
    {
        if(_flddo == null)
            if(a == null)
            {
                _flddo = "(";
                java.util.Enumeration enumeration = _fldif.keys();
                if(enumeration != null)
                    for(_flddo += "*." + (java.lang.String)enumeration.nextElement(); enumeration.hasMoreElements(); _flddo += ", *." + (java.lang.String)enumeration.nextElement());
                _flddo += ")";
            } else
            {
                _flddo = a;
            }
        return _flddo;
    }

    public void setDescription(java.lang.String s)
    {
        a = s;
        _flddo = null;
    }

    private java.util.Hashtable _fldif = null;
    private java.lang.String a = null;
    private java.lang.String _flddo = null;
}
