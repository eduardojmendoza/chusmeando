// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.ado.AdoError;
import diamondedge.ado.Field;
import diamondedge.ado.Recordset;
import diamondedge.swing.grid.AbstractGridRowModel;
import diamondedge.swing.grid.GridRowModel;
import diamondedge.util.Variant;
import java.io.PrintStream;
import java.util.Vector;

// Referenced classes of package diamondedge.vb:
//            ColumnInfo, DataSource

class RecordsetTableModel extends diamondedge.swing.grid.AbstractGridRowModel
    implements diamondedge.swing.grid.GridRowModel
{

    public RecordsetTableModel(diamondedge.vb.DataSource datasource)
    {
        _fldif = true;
        _fldfor = false;
        a = datasource;
    }

    public void cancelChanges()
    {
        try
        {
            _fldfor = false;
            _flddo.cancelUpdate();
        }
        catch(diamondedge.ado.AdoError adoerror) { }
        fireRowChanged(3);
    }

    public void saveChanges()
    {
        if(_fldfor)
            try
            {
                _flddo.update();
                _fldfor = false;
                fireRowChanged(2);
            }
            catch(diamondedge.ado.AdoError adoerror)
            {
                java.lang.System.out.println("RecordsetTableModel.saveChanges: " + adoerror);
            }
    }

    public boolean isChanged()
    {
        return _fldfor;
    }

    public void setChanged(boolean flag)
    {
        if(!flag && _fldfor)
            cancelChanges();
        else
            _fldfor = flag;
    }

    public diamondedge.ado.Recordset getRecordset()
    {
        if(_flddo == null && a != null)
        {
            _flddo = a.getRecordset();
            _fldint = null;
            if(_flddo != null)
                fireTableStructureChanged();
        }
        return _flddo;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        a = datasource;
        _flddo = null;
        _fldint = null;
        fireTableDataChanged();
    }

    public int getRowCount()
    {
        if(_flddo == null)
            getRecordset();
        if(_flddo == null)
        {
            return 0;
        } else
        {
            int i = _flddo.getRecordCount(true);
            return i;
        }
    }

    void a()
    {
        if(_flddo == null)
            getRecordset();
        if(_fldint == null)
            _fldint = new Vector();
        else
            _fldint.removeAllElements();
        if(_flddo == null)
            return;
        int i = _flddo.getFieldCount();
        Object obj = null;
        try
        {
            for(int j = 0; j < i; j++)
            {
                diamondedge.ado.Field field = _flddo.getField(j);
                _fldint.addElement(new ColumnInfo(field.getName(), j));
            }

        }
        catch(diamondedge.ado.AdoError adoerror) { }
    }

    public int getColumnCount()
    {
        if(_fldint == null)
            a();
        return _fldint.size();
    }

    public java.lang.String getColumnName(int i)
    {
        if(_fldint == null)
            a();
        if(i < _fldint.size())
        {
            diamondedge.vb.ColumnInfo columninfo = (diamondedge.vb.ColumnInfo)_fldint.elementAt(i);
            return columninfo.a;
        } else
        {
            return java.lang.String.valueOf(i);
        }
    }

    public int getColumn(java.lang.String s)
    {
        if(_fldint == null)
            a();
        int i = _fldint.size();
        for(int j = 0; j < i; j++)
        {
            diamondedge.vb.ColumnInfo columninfo = (diamondedge.vb.ColumnInfo)_fldint.elementAt(j);
            if(columninfo._flddo.equals(s))
                return j;
        }

        return -1;
    }

    public java.lang.Object getValueAt(int i, int j)
    {
        if(_fldint == null)
            a();
        diamondedge.vb.ColumnInfo columninfo = (diamondedge.vb.ColumnInfo)_fldint.elementAt(j);
        if(_flddo == null)
            return "";
        else
            return _flddo.getValueAt(i, columninfo._fldif);
    }

    public void setValueAt(java.lang.Object obj, int i, int j)
    {
        if(_fldint == null)
            a();
        if(_flddo != null)
        {
            diamondedge.vb.ColumnInfo columninfo = (diamondedge.vb.ColumnInfo)_fldint.elementAt(j);
            try
            {
                int k = i + 1;
                if(_flddo.getAbsolutePosition() != k)
                    _flddo.setAbsolutePosition(k);
                diamondedge.ado.Field field = _flddo.getField(columninfo._fldif);
                field.setValue(new Variant(obj));
            }
            catch(diamondedge.ado.AdoError adoerror)
            {
                java.lang.System.out.println("setValueAt: " + adoerror);
            }
            _fldfor = true;
            fireTableCellUpdated(i, j);
            fireRowChanged(1);
        }
    }

    public void setRowPosition(int i)
    {
        if(i != curRow)
            saveChanges();
        super.setRowPosition(i);
    }

    public boolean isCellEditable(int i, int j)
    {
        return _fldif && i == curRow;
    }

    public boolean isEditable()
    {
        return _fldif;
    }

    public void setEditable(boolean flag)
    {
        _fldif = flag;
    }

    private diamondedge.ado.Recordset _flddo = null;
    private diamondedge.vb.DataSource a = null;
    private java.util.Vector _fldint = null;
    private boolean _fldif = false;
    private boolean _fldfor = false;
}
