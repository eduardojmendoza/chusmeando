// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.AWTEventMulticaster;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;

public class VbTimer extends java.awt.Component
{
    private static class a extends java.lang.Thread
    {

        public void a(boolean flag)
        {
            _fldif = flag;
        }

        public void a(int i)
        {
            a = i;
        }

        public void run()
        {
            while(_fldif && a > 0) 
            {
                try
                {
                    diamondedge.vb.a.sleep(a);
                }
                catch(java.lang.Exception exception) { }
                _flddo.fireActionEvent();
            }
        }

        private boolean _fldif = false;
        private int a = 0;
        private diamondedge.vb.VbTimer _flddo = null;

        public a(diamondedge.vb.VbTimer vbtimer, boolean flag, int i)
        {
            _fldif = true;
            a = 0;
            _flddo = vbtimer;
            _fldif = flag;
            a = i;
        }
    }


    public VbTimer()
    {
        a = null;
        _fldfor = null;
        _fldif = 0;
    }

    private void a()
    {
        try
        {
            a = new a(this, isEnabled(), _fldif);
            a.start();
        }
        catch(java.lang.Exception exception)
        {
            java.lang.System.out.println("VbTimer could not start thead: " + exception);
        }
    }

    public java.lang.String getData()
    {
        return _flddo;
    }

    public void setData(java.lang.String s)
    {
        _flddo = s;
    }

    public int getInterval()
    {
        return _fldif;
    }

    public void setInterval(int i)
    {
        if(_fldif != i)
        {
            _fldif = i;
            if(_fldif > 0 && (a == null || !a.isAlive()))
                a();
            else
            if(a != null)
            {
                a.a(i);
                if(_fldif <= 0)
                    a = null;
            }
        }
    }

    public void setEnabled(boolean flag)
    {
        if(isEnabled() != flag)
        {
            super.setEnabled(flag);
            if(flag && (a == null || !a.isAlive()))
                a();
            else
            if(a != null)
            {
                a.a(flag);
                if(!flag)
                    a = null;
            }
        }
    }

    public void addActionListener(java.awt.event.ActionListener actionlistener)
    {
        _fldfor = java.awt.AWTEventMulticaster.add(_fldfor, actionlistener);
    }

    public void removeActionListener(java.awt.event.ActionListener actionlistener)
    {
        _fldfor = java.awt.AWTEventMulticaster.remove(_fldfor, actionlistener);
    }

    protected void fireActionEvent()
    {
        if(_fldfor != null)
            _fldfor.actionPerformed(new ActionEvent(this, 1001, _flddo));
    }

    private java.lang.String _flddo = null;
    private diamondedge.vb.a a = null;
    private java.awt.event.ActionListener _fldfor = null;
    private int _fldif = 0;
}
