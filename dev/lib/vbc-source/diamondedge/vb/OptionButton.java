// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import javax.swing.JRadioButton;

// Referenced classes of package diamondedge.vb:
//            VbControl, Screen, JForm

public class OptionButton extends javax.swing.JRadioButton
    implements diamondedge.vb.VbControl
{

    public OptionButton()
    {
        setFont(diamondedge.vb.Screen.getDefaultFont());
        setHorizontalTextPosition(4);
    }

    public void setValue(boolean flag)
    {
        if(isSelected() != flag)
            if(flag && isEnabled())
                doClick();
            else
                setSelected(flag);
    }

    public int getOptionValue()
    {
        return a9;
    }

    public void setOptionValue(int i)
    {
        a9 = i;
    }

    public int getAlignment()
    {
        return getHorizontalTextPosition() != 4 ? 4 : 2;
    }

    public void setAlignment(int i)
    {
        setHorizontalTextPosition(i != 4 ? 4 : 2);
    }

    public void setAlignGraphical(boolean flag)
    {
        if(flag)
        {
            setHorizontalTextPosition(0);
            setVerticalTextPosition(3);
        } else
        {
            setHorizontalTextPosition(4);
            setVerticalTextPosition(0);
        }
    }

    public void setText(java.lang.String s)
    {
        super.setText(diamondedge.vb.JForm.processMnemonic(this, s));
    }

    public java.lang.String getTag()
    {
        return a7;
    }

    public void setTag(java.lang.String s)
    {
        a7 = s;
    }

    public int getTabOrder()
    {
        return a8;
    }

    public void setTabOrder(int i)
    {
        a8 = i;
    }

    private java.lang.String a7 = null;
    private int a8 = 0;
    private int a9 = 0;
}
