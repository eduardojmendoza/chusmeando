// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.swing.DsListView;
import diamondedge.swing.IconList;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

// Referenced classes of package diamondedge.vb:
//            VbControl

public class ListView extends diamondedge.swing.DsListView
    implements diamondedge.vb.VbControl
{

    public ListView()
    {
    }

    public void initItems(int i)
    {
        initColumns(i);
    }

    public void initColumns(int i)
    {
        setColumnCount(i);
        if(i < 2)
            setColumnAutoResizeMode(2);
    }

    public void setIcons(diamondedge.swing.IconList iconlist)
    {
        if(this.iconlist == null)
            this.iconlist = iconlist;
    }

    public javax.swing.table.TableColumn getColumnAtX(int i)
    {
        int j = getColumnModel().getColumnIndexAtX(i);
        return getColumn(j);
    }

    public java.lang.String getTag()
    {
        return tag;
    }

    public void setTag(java.lang.String s)
    {
        tag = s;
    }

    public int getTabOrder()
    {
        return tabOrder;
    }

    public void setTabOrder(int i)
    {
        tabOrder = i;
    }

    protected java.lang.String tag = null;
    protected int tabOrder = 0;
}
