// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.HashList;
import java.awt.Image;

// Referenced classes of package diamondedge.vb:
//            ListImage

public class ListImages
{

    public ListImages()
    {
        a = new HashList();
    }

    public diamondedge.vb.ListImage getItem(int i)
    {
        if(i < 0 || i >= a.size())
        {
            return null;
        } else
        {
            java.lang.Object obj = a.get(i);
            return (obj instanceof diamondedge.vb.ListImage) ? (diamondedge.vb.ListImage)obj : null;
        }
    }

    public diamondedge.vb.ListImage getItem(java.lang.String s)
    {
        java.lang.Object obj = a.get(s);
        return (obj instanceof diamondedge.vb.ListImage) ? (diamondedge.vb.ListImage)obj : null;
    }

    public int getCount()
    {
        return a.size();
    }

    public diamondedge.vb.ListImage add(int i, java.lang.String s, java.awt.Image image)
        throws java.lang.IndexOutOfBoundsException
    {
        if(s == null)
            s = a.getNextKey();
        diamondedge.vb.ListImage listimage = new ListImage(this, s);
        listimage.setImage(image);
        if(i >= 0)
            a.add(listimage, s, i);
        else
            a.add(listimage, s);
        return listimage;
    }

    public void remove(int i)
    {
        a.remove(i);
    }

    public void remove(java.lang.String s)
    {
        a.removeKey(s);
    }

    public void clear()
    {
        a.clear();
    }

    public diamondedge.util.HashList getCollection()
    {
        return a;
    }

    private diamondedge.util.HashList a = null;
}
