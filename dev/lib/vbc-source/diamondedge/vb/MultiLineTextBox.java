// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

// Referenced classes of package diamondedge.vb:
//            ScrolledComponent, DataBound, TextBoxCtrl, VbControl, 
//            Screen, DataSource, DataBoundRouter, DataChangeEvent

public class MultiLineTextBox extends diamondedge.vb.ScrolledComponent
    implements javax.swing.event.DocumentListener, diamondedge.vb.DataBound, diamondedge.vb.TextBoxCtrl, diamondedge.vb.VbControl
{
    private class a extends javax.swing.text.PlainDocument
    {

        public void insertString(int i, java.lang.String s, javax.swing.text.AttributeSet attributeset)
            throws javax.swing.text.BadLocationException
        {
            if(s == null)
                return;
            if(O == 0 || getLength() + s.length() <= O)
                super.insertString(i, s, attributeset);
            else
            if(getLength() < O)
            {
                s = s.substring(0, O - getLength());
                super.insertString(i, s, attributeset);
            } else
            {
                java.awt.Toolkit.getDefaultToolkit().beep();
            }
        }

        private a()
        {
        }

    }


    public MultiLineTextBox()
    {
        super(new JTextArea());
        O = 0;
        R = null;
        a();
    }

    public MultiLineTextBox(java.lang.String s, int i, int j)
    {
        super(new JTextArea(s, i, j));
        O = 0;
        R = null;
        a();
    }

    private void a()
    {
        P = (javax.swing.JTextArea)getViewport().getView();
        P.setFont(diamondedge.vb.Screen.getDefaultFont());
        P.setWrapStyleWord(true);
        P.setLineWrap(true);
        P.setCursor(java.awt.Cursor.getPredefinedCursor(2));
        P.setDocument(new a());
        P.getDocument().addDocumentListener(this);
        P.setFocusTraversalKeys(0, null);
        P.setFocusTraversalKeys(1, null);
    }

    public javax.swing.JTextArea control()
    {
        return P;
    }

    public java.lang.String getText()
    {
        return P.getText();
    }

    public void setText(java.lang.String s)
    {
        P.setText(s);
        P.setCaretPosition(0);
    }

    public java.lang.String getTag()
    {
        return N;
    }

    public void setTag(java.lang.String s)
    {
        N = s;
    }

    public int getTabOrder()
    {
        return Q;
    }

    public void setTabOrder(int i)
    {
        Q = i;
    }

    public int getSelectionStart()
    {
        return P.getSelectionStart();
    }

    public void setSelectionStart(int i)
    {
        P.setSelectionStart(i);
    }

    public int getSelectionEnd()
    {
        return P.getSelectionEnd();
    }

    public void setSelectionEnd(int i)
    {
        P.setSelectionEnd(i);
    }

    public boolean isEditable()
    {
        return P.isEditable();
    }

    public void setEditable(boolean flag)
    {
        P.setEditable(flag);
    }

    public int getHorizontalAlignment()
    {
        return 0;
    }

    public void setHorizontalAlignment(int i)
    {
    }

    public java.lang.String getSelectedText()
    {
        return P.getSelectedText();
    }

    public void replaceSelection(java.lang.String s)
    {
        P.replaceSelection(s);
    }

    public int getMaxLength()
    {
        return O;
    }

    public void setMaxLength(int i)
    {
        O = i;
    }

    protected void fireDocumentChanged(javax.swing.event.DocumentEvent documentevent)
    {
        java.lang.Object aobj[] = listenerList.getListenerList();
        java.awt.event.TextEvent textevent = null;
        for(int i = aobj.length - 2; i >= 0; i -= 2)
        {
            if(aobj[i] != (java.awt.event.TextListener.class))
                continue;
            if(textevent == null)
                textevent = new TextEvent(this, 900);
            ((java.awt.event.TextListener)aobj[i + 1]).textValueChanged(textevent);
        }

    }

    public void changedUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void insertUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void removeUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void addTextListener(java.awt.event.TextListener textlistener)
    {
        listenerList.add(java.awt.event.TextListener.class, textlistener);
    }

    public void removeTextListener(java.awt.event.TextListener textlistener)
    {
        listenerList.remove(java.awt.event.TextListener.class, textlistener);
    }

    public java.lang.String getDataField()
    {
        return S;
    }

    public void setDataField(java.lang.String s)
    {
        S = s;
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return R;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        if(R != null)
        {
            R.removeDataBound(this);
            removeTextListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        if(datasource != null)
        {
            datasource.addDataBound(this);
            addTextListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        R = datasource;
    }

    public diamondedge.util.Variant getDataValue()
    {
        return new Variant(getText());
    }

    public void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent)
    {
        setText(datachangeevent.getValue() != null ? datachangeevent.getValue().toString() : "");
    }

    private java.lang.String N = null;
    private int Q = 0;
    private javax.swing.JTextArea P = null;
    private int O = 0;
    private java.lang.String S = null;
    private diamondedge.vb.DataSource R = null;

}
