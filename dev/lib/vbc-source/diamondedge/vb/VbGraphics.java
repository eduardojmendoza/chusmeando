// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.image.PixelGrabber;

// Referenced classes of package diamondedge.vb:
//            Screen

public class VbGraphics
{

    public VbGraphics(java.awt.Component component)
    {
        _fldgoto = 0;
        _fldelse = 0;
        _fldfor = java.awt.Color.black;
        _fldint = 1;
        _fldcase = 13;
        _fldnull = -1;
        d = 0.0D;
        _fldbyte = 0.0D;
        c = 0.0D;
        _fldif = 0.0D;
        _fldnew = 0.0D;
        _flddo = 0.0D;
        _fldlong = null;
        _fldtry = component;
        setScaleMode(1);
    }

    public java.awt.Insets getInsets()
    {
        return _fldlong;
    }

    public void setInsets(java.awt.Insets insets)
    {
        _fldlong = insets;
    }

    public double getCurrentX()
    {
        return scalePixelsX(_fldgoto);
    }

    public void setCurrentX(double d1)
    {
        _fldgoto = scaleToPixelsX(d1);
    }

    public double getCurrentY()
    {
        return scalePixelsY(_fldelse);
    }

    public void setCurrentY(double d1)
    {
        _fldelse = scaleToPixelsY(d1);
    }

    public java.awt.Color getFillColor()
    {
        return _fldfor;
    }

    public void setFillColor(java.awt.Color color)
    {
        _fldfor = color;
    }

    public int getFillStyle()
    {
        return _fldint;
    }

    public void setFillStyle(int i)
    {
        _fldint = (byte)i;
    }

    public int getDrawMode()
    {
        return _fldcase;
    }

    public void setDrawMode(int i)
    {
        _fldcase = (byte)i;
    }

    public double getScaleLeft()
    {
        return _fldbyte;
    }

    public void setScaleLeft(double d1)
    {
        _fldbyte = d1;
        _fldnull = 0;
    }

    public double getScaleTop()
    {
        return d;
    }

    public void setScaleTop(double d1)
    {
        d = d1;
        _fldnull = 0;
    }

    public double getScaleWidth()
    {
        return c;
    }

    public void setScaleWidth(double d1)
    {
        if(c == 0.0D && _fldnull != 0)
            _fldnew = d1;
        else
            _fldnull = 0;
        c = d1;
        if(_fldnull == 0)
            b = _mthif(_fldnull);
    }

    public double getScaleHeight()
    {
        return _fldif;
    }

    public void setScaleHeight(double d1)
    {
        if(_fldif == 0.0D && _fldnull != 0)
            _flddo = d1;
        else
            _fldnull = 0;
        _fldif = d1;
        if(_fldnull == 0)
            _fldvoid = a(_fldnull);
    }

    public int getScaleMode()
    {
        return _fldnull;
    }

    public void setScaleMode(int i)
    {
        if(i == _fldnull)
            return;
        if(_fldnew != 0.0D)
            c = _fldnew;
        if(_flddo != 0.0D)
            _fldif = _flddo;
        _fldnew = 0.0D;
        _flddo = 0.0D;
        c = scaleX(c, _fldnull, i);
        _fldif = scaleY(_fldif, _fldnull, i);
        if(i > 0)
            _fldbyte = d = 0.0D;
        _fldnull = (short)i;
        b = _mthif(i);
        _fldvoid = a(i);
    }

    double _mthif(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            if(c != 0.0D && _fldtry.getSize().width != 0)
                return (double)_fldtry.getSize().width / c;
            else
                return 1.0D;

        case 3: // '\003'
            return 1.0D;

        case 1: // '\001'
            return diamondedge.vb.Screen.pixelsPerTwip;

        case 2: // '\002'
            return diamondedge.vb.Screen.pixelsPerPoint;

        case 4: // '\004'
            return diamondedge.vb.Screen.pixelsPerCharacterX;

        case 5: // '\005'
            return diamondedge.vb.Screen.pixelsPerInch;

        case 6: // '\006'
            return diamondedge.vb.Screen.pixelsPerMillimeter;

        case 7: // '\007'
            return diamondedge.vb.Screen.pixelsPerCentimeter;
        }
        return 1.0D;
    }

    double a(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            if(_fldif != 0.0D && _fldtry.getSize().height != 0)
                return (double)_fldtry.getSize().height / _fldif;
            else
                return 1.0D;

        case 3: // '\003'
            return 1.0D;

        case 1: // '\001'
            return diamondedge.vb.Screen.pixelsPerTwip;

        case 2: // '\002'
            return diamondedge.vb.Screen.pixelsPerPoint;

        case 4: // '\004'
            return diamondedge.vb.Screen.pixelsPerCharacterY;

        case 5: // '\005'
            return diamondedge.vb.Screen.pixelsPerInch;

        case 6: // '\006'
            return diamondedge.vb.Screen.pixelsPerMillimeter;

        case 7: // '\007'
            return diamondedge.vb.Screen.pixelsPerCentimeter;
        }
        return 1.0D;
    }

    public void scale(double d1, double d2, double d3, double d4)
    {
        setScaleLeft(d1);
        setScaleTop(d2);
        setScaleWidth(d3 - d1);
        setScaleHeight(d4 - d2);
    }

    public void scale()
    {
        d = 0.0D;
        _fldbyte = 0.0D;
        setScaleMode(1);
    }

    public double scaleX(double d1, int i, int j)
    {
        double d2 = d1 * _mthif(i);
        return d2 / _mthif(j);
    }

    public double scaleY(double d1, int i, int j)
    {
        double d2 = d1 * a(i);
        return d2 / a(j);
    }

    public int scaleToPixelsX(double d1)
    {
        return (int)((d1 - _fldbyte) * b);
    }

    public int scaleToPixelsY(double d1)
    {
        return (int)((d1 - d) * _fldvoid);
    }

    public double scalePixelsX(int i)
    {
        return (double)i / b + _fldbyte;
    }

    public double scalePixelsY(int i)
    {
        return (double)i / _fldvoid + d;
    }

    public java.awt.Color point(double d1, double d2, java.awt.Image image)
    {
        if(image == null)
            return _fldtry.getBackground();
        int ai[];
        ai = new int[1];
        java.awt.image.PixelGrabber pixelgrabber = new PixelGrabber(image, scaleToPixelsX(d1), scaleToPixelsY(d2), 1, 1, ai, 0, 1);
        pixelgrabber.grabPixels();
        return new Color(ai[0]);
        java.lang.InterruptedException interruptedexception;
        interruptedexception;
        return _fldtry.getBackground();
    }

    public void circle(double d1, double d2, boolean flag, double d3, 
            java.awt.Color color, double d4, double d5, double d6)
    {
        int i = scaleToPixelsX(d1);
        int j = scaleToPixelsY(d2);
        int i1 = scaleToPixelsX(d3);
        if(flag)
        {
            i += _fldgoto;
            j += _fldelse;
        }
        _fldgoto = i;
        _fldelse = j;
        d6 = java.lang.Math.abs(d6);
        int l;
        int k = l = 2 * i1;
        if(d6 < 1.0D)
            l = (int)((double)l * d6);
        else
            k = (int)((double)k / d6);
        i -= k / 2;
        j -= l / 2;
        k--;
        l--;
        if(_fldlong != null)
        {
            i += _fldlong.left;
            j += _fldlong.top;
        }
        java.awt.Graphics g = _fldtry.getGraphics();
        if(g == null)
            return;
        java.awt.Color color1 = color;
        if(color == null)
            color1 = _fldtry.getForeground();
        if(color1 == null)
            color1 = java.awt.Color.black;
        if(d5 - d4 < _fldchar)
        {
            g.setColor(_fldfor);
            g.fillArc(i, j, k, l, (int)(d4 * a), (int)((d5 - d4) * a));
        } else
        {
            if(_fldint != 1)
            {
                g.setColor(_fldfor);
                g.fillOval(i, j, k, l);
            }
            g.setColor(color1);
            g.drawOval(i, j, k, l);
        }
    }

    public void line(double d1, double d2, boolean flag, double d3, 
            double d4, boolean flag1, java.awt.Color color, int i)
    {
        int j = scaleToPixelsX(d1);
        int k = scaleToPixelsY(d2);
        if(flag && d1 == 0.0D && d2 == 0.0D)
            j = k = 0;
        int l = scaleToPixelsX(d3);
        int i1 = scaleToPixelsY(d4);
        java.awt.Graphics g = _fldtry.getGraphics();
        if(g == null)
            return;
        java.awt.Color color1 = color;
        if(color == null)
            color1 = _fldtry.getForeground();
        if(color1 == null)
            color1 = java.awt.Color.black;
        if(flag)
        {
            j += _fldgoto;
            k += _fldelse;
        }
        if(flag1)
        {
            l += j;
            i1 += k;
        }
        _fldgoto = l;
        _fldelse = i1;
        if(_fldlong != null)
        {
            j += _fldlong.left;
            k += _fldlong.top;
            l += _fldlong.left;
            i1 += _fldlong.top;
            java.awt.Dimension dimension = _fldtry.getSize();
            if(l > dimension.width - _fldlong.right - 1)
                l = dimension.width - _fldlong.right - 1;
            if(i1 > dimension.height - _fldlong.bottom - 1)
                i1 = dimension.height - _fldlong.bottom - 1;
        }
        if(_fldcase == 10)
            g.setXORMode(_fldtry.getBackground());
        else
            g.setPaintMode();
        if(i == 0)
        {
            g.setColor(color1);
            g.drawLine(j, k, l, i1);
        } else
        {
            int j1 = l - j - 1;
            int k1 = i1 - k - 1;
            if((i & 2) == 2)
            {
                g.setColor(color1);
                g.fillRect(j, k, j1, k1);
            } else
            if(_fldint != 1)
            {
                g.setColor(_fldfor);
                g.fillRect(j, k, j1, k1);
            }
            g.setColor(color1);
            g.drawRect(j, k, j1, k1);
        }
    }

    public void paintPicture(java.awt.Image image, double d1, double d2, double d3, 
            double d4)
    {
        java.awt.Graphics g = _fldtry.getGraphics();
        if(g == null)
            return;
        int i = scaleToPixelsX(d1);
        int j = scaleToPixelsY(d2);
        if(d3 < 0.0D || d4 < 0.0D)
        {
            int k;
            if(d3 < 0.0D)
                k = image.getWidth(_fldtry);
            else
                k = scaleToPixelsX(d3);
            int l;
            if(d4 < 0.0D)
                l = image.getHeight(_fldtry);
            else
                l = scaleToPixelsY(d4);
            g.drawImage(image, i, j, k, l, _fldtry);
        } else
        {
            g.drawImage(image, i, j, _fldtry);
        }
    }

    public void pointSet(double d1, double d2, boolean flag, java.awt.Color color)
    {
        java.awt.Graphics g = _fldtry.getGraphics();
        if(g == null)
            return;
        int i = scaleToPixelsX(d1);
        int j = scaleToPixelsY(d2);
        if(flag)
        {
            i += _fldgoto;
            j += _fldelse;
        }
        if(color != null)
            g.setColor(color);
        g.drawLine(i, j, i + 1, j);
    }

    public void print(java.lang.String s)
    {
        java.awt.Graphics g = _fldtry.getGraphics();
        if(g == null)
            return;
        java.awt.Color color = _fldtry.getForeground();
        if(color == null)
            color = java.awt.Color.black;
        g.setColor(color);
        java.awt.FontMetrics fontmetrics = g.getFontMetrics();
        int i = _fldgoto;
        int j = _fldelse + fontmetrics.getAscent();
        if(_fldlong != null)
        {
            i += _fldlong.left;
            j += _fldlong.top;
        }
        g.drawString(s, i, j);
        _fldgoto = 0;
        _fldelse += fontmetrics.getHeight() + 1;
    }

    public static void drawLine(java.awt.Graphics g, int i, int j, int k, int l, int i1)
    {
        if(i1 <= 1)
        {
            g.drawLine(i, j, k, l);
        } else
        {
            int j1 = -(l - j);
            int k1 = k - i;
            double d1 = java.lang.Math.sqrt(k1 * k1 + j1 * j1);
            double d2 = (double)j1 / d1;
            double d3 = (double)k1 / d1;
            int l1 = i1 / 2;
            int i2;
            if(d2 >= 0.0D)
                i2 = (int)((double)l1 * d2 + 0.5D);
            else
                i2 = (int)((double)l1 * d2 - 0.5D);
            int j2;
            if(d3 >= 0.0D)
                j2 = (int)((double)l1 * d3 + 0.5D);
            else
                j2 = (int)((double)l1 * d3 - 0.5D);
            int ai[] = new int[4];
            int ai1[] = new int[4];
            ai[0] = i - i2;
            ai1[0] = j - j2;
            ai[1] = k - i2;
            ai1[1] = l - j2;
            l1 = (i1 - 1) / 2;
            if(d2 >= 0.0D)
                i2 = (int)((double)l1 * d2 + 0.5D);
            else
                i2 = (int)((double)l1 * d2 - 0.5D);
            if(d3 >= 0.0D)
                j2 = (int)((double)l1 * d3 + 0.5D);
            else
                j2 = (int)((double)l1 * d3 - 0.5D);
            ai[2] = k + i2;
            ai1[2] = l + j2;
            ai[3] = i + i2;
            ai1[3] = j + j2;
            g.fillPolygon(ai, ai1, 4);
        }
    }

    public static void drawShape(java.awt.Graphics g, int i, int j, int k, int l, int i1, java.awt.Color color, byte byte0, 
            java.awt.Color color1, boolean flag)
    {
        switch(i1)
        {
        default:
            break;

        case 0: // '\0'
            if(byte0 != 1)
            {
                g.setColor(color);
                g.fillRect(i, j, k, l);
            }
            if(flag)
            {
                g.setColor(color1);
                g.drawRect(i, j, k, l);
            }
            break;

        case 1: // '\001'
            int l1 = java.lang.Math.min(k, l);
            i += (k - l1) / 2;
            j += (l - l1) / 2;
            if(byte0 != 1)
            {
                g.setColor(color);
                g.fillRect(i, j, l1, l1);
            }
            if(flag)
            {
                g.setColor(color1);
                g.drawRect(i, j, l1, l1);
            }
            break;

        case 2: // '\002'
            if(byte0 != 1)
            {
                g.setColor(color);
                g.fillOval(i, j, k, l);
            }
            if(flag)
            {
                g.setColor(color1);
                g.drawOval(i, j, k, l);
            }
            break;

        case 3: // '\003'
            int i2 = java.lang.Math.min(k, l);
            i += (k - i2) / 2;
            j += (l - i2) / 2;
            if(byte0 != 1)
            {
                g.setColor(color);
                g.fillOval(i, j, i2, i2);
            }
            if(flag)
            {
                g.setColor(color1);
                g.drawOval(i, j, i2, i2);
            }
            break;

        case 4: // '\004'
            int j1 = java.lang.Math.min(k, l) / 5;
            if(byte0 != 1)
            {
                g.setColor(color);
                g.fillRoundRect(i, j, k, l, j1, j1);
            }
            if(flag)
            {
                g.setColor(color1);
                g.drawRoundRect(i, j, k, l, j1, j1);
            }
            break;

        case 5: // '\005'
            int j2 = java.lang.Math.min(k, l);
            i += (k - j2) / 2;
            j += (l - j2) / 2;
            int k1 = j2 / 5;
            if(byte0 != 1)
            {
                g.setColor(color);
                g.fillRoundRect(i, j, j2, j2, k1, k1);
            }
            if(flag)
            {
                g.setColor(color1);
                g.drawRoundRect(i, j, j2, j2, k1, k1);
            }
            break;
        }
    }

    private int _fldgoto = 0;
    private int _fldelse = 0;
    private java.awt.Color _fldfor = null;
    private short _fldint = 0;
    private byte _fldcase = 0;
    private short _fldnull = 0;
    private double d = 0;
    private double _fldbyte = 0;
    private double c = 0;
    private double _fldif = 0;
    private double _fldnew = 0;
    private double _flddo = 0;
    private double b = 0;
    private double _fldvoid = 0;
    private static double _fldchar = 0;
    private static double a = 0;
    java.awt.Insets _fldlong = null;
    java.awt.Component _fldtry = null;

    static 
    {
        _fldchar = 6.2831853071795862D;
        a = 360D / _fldchar;
    }
}
