// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

// Referenced classes of package diamondedge.vb:
//            VbPanel, VbGraphics, Screen

public class Line extends java.awt.Component
{

    public Line()
    {
        x = -1F;
        y = -1F;
        x2 = -1F;
        y2 = -1F;
        lineColor = java.awt.Color.black;
        lineWidth = 1;
    }

    public void paint(java.awt.Graphics g)
    {
        g.setColor(lineColor);
        diamondedge.vb.VbGraphics.drawLine(g, a(x), _mthif(y), a(x2), _mthif(y2), lineWidth);
    }

    public synchronized void setupBounds()
    {
        int i = a(x);
        int j = _mthif(y);
        int k = a(x2);
        int l = _mthif(y2);
        int i1 = java.lang.Math.min(i, k);
        int j1 = java.lang.Math.min(j, l);
        if(i1 < 0 || j1 < 0)
        {
            return;
        } else
        {
            int k1 = java.lang.Math.abs(k - i) + lineWidth + 1;
            int l1 = java.lang.Math.abs(l - j) + lineWidth + 1;
            setBounds(0, 0, i1 + k1, j1 + l1);
            return;
        }
    }

    public java.awt.Color getLineColor()
    {
        return lineColor;
    }

    public void setLineColor(java.awt.Color color)
    {
        lineColor = color;
        repaint();
    }

    public int getLineWidth()
    {
        return lineWidth;
    }

    public void setLineWidth(int i)
    {
        lineWidth = (byte)i;
        repaint();
    }

    public float getX1()
    {
        return x;
    }

    public void setX1(float f)
    {
        x = f;
        setupBounds();
    }

    public float getX2()
    {
        return x2;
    }

    public void setX2(float f)
    {
        x2 = f;
        setupBounds();
    }

    public float getY1()
    {
        return y;
    }

    public void setY1(float f)
    {
        y = f;
        setupBounds();
    }

    public float getY2()
    {
        return y2;
    }

    public void setY2(float f)
    {
        y2 = f;
        setupBounds();
    }

    private int a(float f)
    {
        if(getParent() instanceof diamondedge.vb.VbPanel)
        {
            diamondedge.vb.VbPanel vbpanel = (diamondedge.vb.VbPanel)getParent();
            return (int)vbpanel.scaleX(f, vbpanel.getScaleMode(), 3);
        } else
        {
            return (int)((double)f * diamondedge.vb.Screen.pixelsPerTwip);
        }
    }

    private int _mthif(float f)
    {
        if(getParent() instanceof diamondedge.vb.VbPanel)
        {
            diamondedge.vb.VbPanel vbpanel = (diamondedge.vb.VbPanel)getParent();
            return (int)vbpanel.scaleY(f, vbpanel.getScaleMode(), 3);
        } else
        {
            return (int)((double)f * diamondedge.vb.Screen.pixelsPerTwip);
        }
    }

    public java.lang.String getTag()
    {
        return a;
    }

    public void setTag(java.lang.String s)
    {
        a = s;
    }

    private java.lang.String a = null;
    public float x = 0;
    public float y = 0;
    public float x2 = 0;
    public float y2 = 0;
    public java.awt.Color lineColor = null;
    public byte lineWidth = 0;
}
