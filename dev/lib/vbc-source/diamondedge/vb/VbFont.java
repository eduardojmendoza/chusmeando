// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Component;
import java.awt.Font;

public class VbFont
{

    public VbFont()
    {
        a = "Dialog";
        _fldfor = 10;
    }

    public java.awt.Font getFont()
    {
        int i = 0;
        if(_flddo)
            i |= 1;
        if(_fldif)
            i |= 2;
        return new Font(a, i, _fldfor);
    }

    public java.lang.String getName()
    {
        return a;
    }

    public void setName(java.lang.String s)
    {
        a = s;
    }

    public int getSize()
    {
        return _fldfor;
    }

    public void setSize(int i)
    {
        _fldfor = i;
    }

    public boolean isBold()
    {
        return _flddo;
    }

    public void setBold(boolean flag)
    {
        _flddo = flag;
    }

    public boolean isItalic()
    {
        return _fldif;
    }

    public void setItalic(boolean flag)
    {
        _fldif = flag;
    }

    public int getWeight()
    {
        return _flddo ? 700 : '\u0190';
    }

    public void setWeight(int i)
    {
        _flddo = i >= 700;
    }

    public boolean equals(java.lang.Object obj)
    {
        return getFont().equals(obj);
    }

    public static void setFont(java.awt.Component component, java.awt.Font font)
    {
        if(component == null)
        {
            return;
        } else
        {
            component.setFont(font);
            return;
        }
    }

    public static void setFont(java.awt.Component component, diamondedge.vb.VbFont vbfont)
    {
        if(component == null)
        {
            return;
        } else
        {
            component.setFont(vbfont.getFont());
            return;
        }
    }

    private static java.awt.Font a(java.awt.Font font, int i, boolean flag)
    {
        int j = font.getStyle() & ~i;
        if(flag)
            j |= i;
        return new Font(font.getName(), j, font.getSize());
    }

    public static void setFontBold(java.awt.Component component, boolean flag)
    {
        if(component == null)
        {
            return;
        } else
        {
            component.setFont(diamondedge.vb.VbFont.a(component.getFont(), 1, flag));
            return;
        }
    }

    public static void setFontItalic(java.awt.Component component, boolean flag)
    {
        if(component == null)
        {
            return;
        } else
        {
            component.setFont(diamondedge.vb.VbFont.a(component.getFont(), 2, flag));
            return;
        }
    }

    java.lang.String a = null;
    int _fldfor = 0;
    boolean _flddo = false;
    boolean _fldif = false;
}
