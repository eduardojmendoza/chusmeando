// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Err;
import diamondedge.util.UI;
import diamondedge.util.Variant;
import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.PrintJob;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.InputEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.URL;
import java.util.Properties;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.JApplet;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutFocusTraversalPolicy;
import javax.swing.MenuElement;
import javax.swing.RootPaneContainer;
import javax.swing.UIManager;

// Referenced classes of package diamondedge.vb:
//            VbPanel, VbControl, TitledPanel, MDIForm, 
//            VbFocusPolicy, Form, Screen, Application, 
//            Menu, DataSource

public class JForm extends diamondedge.vb.VbPanel
    implements diamondedge.vb.Form, java.awt.event.WindowListener
{

    public JForm()
    {
        initialized = false;
        unloaded = false;
        initInProgress = false;
        frame = null;
        dialog = null;
        window = null;
        startUpPosition = 0;
        windowState = 0;
        sizable = true;
        aC = 88;
        aI = false;
        aB = false;
        aD = false;
        aF = null;
        aG = null;
        aE = null;
        setFont(diamondedge.vb.Screen.getDefaultFont());
    }

    protected void finalize()
    {
        Form_Terminate();
    }

    public void init()
    {
        initialize();
    }

    public void initialize()
    {
        if(initInProgress)
        {
            return;
        } else
        {
            initInProgress = true;
            Form_Initialize();
            Form_Load();
            unloaded = false;
            initInProgress = false;
            setVisible(true);
            _mthdo();
            return;
        }
    }

    public void unload()
    {
        unloaded = true;
        initialized = false;
    }

    public boolean isInitialized()
    {
        return initialized;
    }

    protected void initializeForm(int i)
    {
        aC = i;
        initialized = true;
    }

    public void dispose()
    {
        if(window != null)
            window.dispose();
        frame = null;
        dialog = null;
        window = null;
    }

    public void setWindowListMenu(javax.swing.JMenu jmenu)
    {
    }

    private void _mthdo()
    {
        try
        {
            if(aE != null)
                if(window != null)
                {
                    window.setFocusTraversalPolicy(aE);
                } else
                {
                    java.applet.Applet applet = diamondedge.vb.Application.getApplet(this);
                    if(applet != null)
                        applet.setFocusTraversalPolicy(aE);
                }
            aI = _mthfor();
        }
        catch(java.lang.Throwable throwable)
        {
            java.lang.String s = "Requires version Java v1.4 or higher.";
            java.lang.String s1 = "Please upgrade your version of Java.";
            javax.swing.JOptionPane.showMessageDialog(null, s, s1, 1);
        }
    }

    public java.awt.FocusTraversalPolicy getFocusTraversalPolicy()
    {
        return aE != null ? aE : LAYOUT_FOCUS_TRAVERSAL_POLICY;
    }

    public void setFocusTraversalPolicy(java.awt.FocusTraversalPolicy focustraversalpolicy)
    {
        aE = focustraversalpolicy;
        _mthdo();
        super.setFocusTraversalPolicy(focustraversalpolicy);
    }

    public boolean isFocusable()
    {
        return aI;
    }

    private boolean _mthfor()
        throws java.lang.Throwable
    {
        int i = getComponentCount();
        for(int j = 0; j < i; j++)
        {
            java.awt.Component component = getComponent(j);
            if(component.isVisible() && component.isEnabled() && component.isFocusable())
                return false;
        }

        return true;
    }

    public static java.awt.Component getComponent(java.awt.Container container, java.lang.String s)
    {
        if(container == null || s == null)
            return null;
        int i = container.getComponentCount();
        Object obj = null;
        for(int j = 0; j < i; j++)
        {
            java.awt.Component component = container.getComponent(j);
            if(s.equals(component.getName()))
                return component;
            if(!(component instanceof java.awt.Container))
                continue;
            component = diamondedge.vb.JForm.getComponent((java.awt.Container)component, s);
            if(component != null)
                return component;
        }

        return null;
    }

    public java.awt.Component getComponent(java.lang.String s)
    {
        return diamondedge.vb.JForm.getComponent(((java.awt.Container) (this)), s);
    }

    private void a(java.util.Vector vector, java.awt.Component component)
    {
        if(component == null)
            return;
        java.awt.Component acomponent[] = (component instanceof java.awt.Container) ? ((java.awt.Container)component).getComponents() : null;
        if(acomponent == null || acomponent.length == 0)
            return;
        for(int i = 0; i < acomponent.length; i++)
        {
            component = acomponent[i];
            if((component instanceof diamondedge.vb.VbControl) || (component instanceof diamondedge.vb.TitledPanel))
                vector.add(component);
            a(vector, component);
        }

    }

    public java.awt.Component[] getAllComponents()
    {
        java.util.Vector vector = new Vector();
        a(vector, this);
        java.awt.Component acomponent[] = new java.awt.Component[vector.size()];
        for(int i = 0; i < acomponent.length; i++)
            acomponent[i] = (java.awt.Component)vector.get(i);

        return acomponent;
    }

    private void a(java.util.Vector vector, javax.swing.MenuElement menuelement)
    {
        if(menuelement == null)
            return;
        javax.swing.MenuElement amenuelement[] = menuelement.getSubElements();
        if(amenuelement == null || amenuelement.length == 0)
            return;
        for(int i = 0; i < amenuelement.length; i++)
        {
            menuelement = amenuelement[i];
            if(menuelement instanceof javax.swing.JMenuItem)
                vector.add(menuelement);
            a(vector, menuelement);
        }

    }

    public javax.swing.JMenuItem[] getAllMenus()
    {
        javax.swing.JFrame jframe = getFrame();
        if(jframe == null)
            return new javax.swing.JMenuItem[0];
        java.util.Vector vector = new Vector();
        a(vector, getFrame().getJMenuBar());
        javax.swing.JMenuItem ajmenuitem[] = new javax.swing.JMenuItem[vector.size()];
        for(int i = 0; i < ajmenuitem.length; i++)
            ajmenuitem[i] = (javax.swing.JMenuItem)vector.get(i);

        return ajmenuitem;
    }

    public static diamondedge.vb.JForm getForm(java.awt.Component component)
    {
        for(; component != null; component = component.getParent())
            if(component instanceof diamondedge.vb.JForm)
                return (diamondedge.vb.JForm)component;

        return null;
    }

    protected boolean isPictureTiled()
    {
        return aB;
    }

    public void setPictureTiled(boolean flag)
    {
        aB = flag;
    }

    protected boolean isScrollable()
    {
        return aD;
    }

    public void setScrollable(boolean flag)
    {
        aD = flag;
        if(flag && window != null && window.isVisible())
            addScrollPane();
    }

    public void paintComponent(java.awt.Graphics g)
    {
        super.paintComponent(g);
        Form_Paint();
        if(aC != 88)
        {
            java.awt.Color color = getBackground();
            color = new Color(java.lang.Math.min((int)((double)color.getRed() * 1.1000000000000001D), 240), java.lang.Math.min((int)((double)color.getGreen() * 1.1000000000000001D), 240), java.lang.Math.min((int)((double)color.getBlue() * 1.1000000000000001D), 240));
            g.setColor(color);
            java.awt.Dimension dimension = getSize();
            int i = dimension.width;
            int j = dimension.height;
            int k = 50;
            int l = 12;
            java.awt.Font font = new Font("Serif", 2, 22);
            g.setFont(font);
            java.awt.FontMetrics fontmetrics = getFontMetrics(font);
            if(fontmetrics != null)
            {
                k = fontmetrics.stringWidth("Diamond Edge Demo") + 20;
                l = fontmetrics.getHeight() + 5;
            }
            for(int i1 = 5; i1 < i; i1 += k)
            {
                for(int j1 = 20; j1 < j; j1 += l)
                    g.drawString("Diamond Edge Demo", i1, j1);

            }

        }
    }

    public void align(javax.swing.JComponent jcomponent, int i)
    {
        if(aF != null)
        {
            int j = aF.indexOf(jcomponent);
            if(j >= 0)
            {
                aF.remove(j);
                aG.remove(j);
            }
        }
        if(i == 0)
            return;
        if(aF == null)
        {
            aF = new Vector();
            aG = new Vector();
        }
        if(i == 1 || i == 2)
        {
            aF.add(0, jcomponent);
            aG.add(0, new Integer(i));
        } else
        {
            aF.add(jcomponent);
            aG.add(new Integer(i));
        }
    }

    public void setBounds(int i, int j, int k, int l)
    {
        super.setBounds(i, j, k, l);
        if(!aH)
        {
            aH = true;
            Form_Resize();
            aH = false;
        }
        if(aF == null)
            return;
        int i1 = 0;
        int j1 = 0;
        int k1 = 0;
        int l1 = 0;
        for(int i2 = 0; i2 < aF.size(); i2++)
        {
            javax.swing.JComponent jcomponent = (javax.swing.JComponent)aF.get(i2);
            int j2 = ((java.lang.Integer)aG.get(i2)).intValue();
            int k2 = jcomponent.getHeight();
            int l2 = jcomponent.getWidth();
            if(j2 == 1)
            {
                jcomponent.setBounds(i1, k1, k - i1 - j1, k2);
                k1 += k2;
                continue;
            }
            if(j2 == 2)
            {
                l1 += k2;
                jcomponent.setBounds(i1, l - l1, k - i1 - j1, k2);
                continue;
            }
            if(j2 == 3)
            {
                jcomponent.setBounds(i1, k1, l2, l - k1 - l1);
                i1 += l2;
                continue;
            }
            if(j2 == 4)
            {
                j1 += l2;
                jcomponent.setBounds(k - j1, k1, l2, l - k1 - l1);
            }
        }

    }

    public void setFormSize(int i, int j)
    {
        java.awt.Dimension dimension = new Dimension(i, j);
        setPreferredSize(dimension);
        setMinimumSize(dimension);
        if(window != null)
            window.pack();
    }

    public int getFormX()
    {
        if(window != null)
            return window.getX();
        else
            return locationX;
    }

    public int getFormY()
    {
        if(window != null)
            return window.getY();
        else
            return locationY;
    }

    public void setFormLocation(int i, int j)
    {
        locationX = i;
        locationY = j;
        if(window != null)
            window.setLocation(locationX, locationY);
    }

    public int getStartUpPosition()
    {
        return startUpPosition;
    }

    public void setStartUpPosition(int i)
    {
        startUpPosition = i;
    }

    public synchronized void setCursor(java.awt.Cursor cursor)
    {
        if(window != null)
            window.setCursor(cursor);
        else
            super.setCursor(cursor);
    }

    public void add(javax.swing.JMenu jmenu)
    {
        if(frame != null)
        {
            javax.swing.JMenuBar jmenubar = frame.getJMenuBar();
            if(jmenubar == null)
            {
                jmenubar = new JMenuBar();
                frame.setJMenuBar(jmenubar);
            }
            jmenubar.add(jmenu);
        } else
        if(dialog != null)
        {
            javax.swing.JMenuBar jmenubar1 = dialog.getJMenuBar();
            if(jmenubar1 == null)
            {
                jmenubar1 = new JMenuBar();
                dialog.setJMenuBar(jmenubar1);
            }
            jmenubar1.add(jmenu);
        } else
        {
            java.applet.Applet applet = diamondedge.vb.Application.getApplet(this);
            if(applet instanceof javax.swing.JApplet)
            {
                javax.swing.JMenuBar jmenubar2 = null;
                javax.swing.JApplet japplet = (javax.swing.JApplet)applet;
                int i = japplet.getContentPane().getComponentCount();
                int j = 0;
                do
                {
                    if(j >= i)
                        break;
                    java.awt.Component component = japplet.getContentPane().getComponent(j);
                    if(component instanceof javax.swing.JMenuBar)
                    {
                        jmenubar2 = (javax.swing.JMenuBar)component;
                        break;
                    }
                    j++;
                } while(true);
                if(jmenubar2 == null)
                {
                    jmenubar2 = new JMenuBar();
                    japplet.getContentPane().add(jmenubar2, "North");
                }
                jmenubar2.add(jmenu);
            }
        }
    }

    public java.lang.String getTitle()
    {
        if(frame != null)
            return frame.getTitle();
        if(dialog != null)
            return dialog.getTitle();
        else
            return "";
    }

    public void setTitle(java.lang.String s)
    {
        if(frame != null)
            frame.setTitle(s);
        else
        if(dialog != null)
            dialog.setTitle(s);
    }

    public java.awt.Image getIcon()
    {
        if(frame != null)
            return frame.getIconImage();
        else
            return null;
    }

    public void setIcon(java.lang.String s)
    {
        if(frame != null)
            frame.setIconImage(diamondedge.vb.Screen.loadImage(s));
    }

    public void setIcon(java.awt.Image image)
    {
        if(frame != null)
            frame.setIconImage(image);
    }

    public int getBorderStyle()
    {
        if(frame != null)
            return !frame.isResizable() ? 1 : 2;
        if(dialog != null)
            return !dialog.isResizable() ? 1 : 2;
        return !sizable ? 1 : 2;
    }

    public void setBorderStyle(int i)
    {
        if(i == 2 || i == 5)
            sizable = true;
        else
            sizable = false;
        if(frame != null)
            frame.setResizable(sizable);
        else
        if(dialog != null)
            dialog.setResizable(sizable);
    }

    public int getWindowState()
    {
        if(frame != null)
        {
            if(frame.getState() == 1)
                return 1;
            if((frame.getState() & 6) != 0)
                return 2;
        }
        return windowState;
    }

    public void setWindowState(int i)
    {
        windowState = i;
        if(frame == null)
            return;
        switch(i)
        {
        case 0: // '\0'
            frame.setExtendedState(0);
            break;

        case 1: // '\001'
            frame.setExtendedState(1);
            break;

        case 2: // '\002'
            frame.setExtendedState(6);
            break;
        }
    }

    public java.awt.Component getActiveControl()
    {
        if(window != null)
            return window.getFocusOwner();
        else
            return null;
    }

    public javax.swing.JDialog getDialog()
    {
        return dialog;
    }

    public javax.swing.JFrame getFrame()
    {
        if(frame == null)
            return diamondedge.util.UI.getJFrame(this);
        else
            return frame;
    }

    public static short buttonFlags(java.awt.event.InputEvent inputevent)
    {
        int i = inputevent.getModifiers();
        short word0 = 0;
        if((i & 0x10) != 0)
            word0 |= 1;
        if((i & 8) != 0)
            word0 |= 4;
        if((i & 4) != 0)
            word0 |= 2;
        return word0;
    }

    public static short shiftFlags(java.awt.event.InputEvent inputevent)
    {
        int i = inputevent.getModifiers();
        short word0 = 0;
        if((i & 1) != 0)
            word0 |= 1;
        if((i & 2) != 0)
            word0 |= 2;
        if((i & 4) != 0)
            word0 |= 4;
        return word0;
    }

    public void windowClosing(java.awt.event.WindowEvent windowevent)
    {
        diamondedge.vb.Application.a(this);
    }

    public void windowActivated(java.awt.event.WindowEvent windowevent)
    {
        diamondedge.vb.Screen.setActiveForm(this);
    }

    public void windowDeactivated(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowOpened(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowClosed(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowIconified(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowDeiconified(java.awt.event.WindowEvent windowevent)
    {
    }

    public void showForm()
    {
        showForm(0, null);
    }

    public void showForm(int i, diamondedge.vb.Form form)
    {
        if(initInProgress)
            return;
        setVisible(false);
        if(!initialized || frame == null && dialog == null)
        {
            if(diamondedge.vb.Application.getApplet(this) != null)
            {
                if(!initialized)
                {
                    init();
                    validate();
                    diamondedge.vb.Screen.setActiveForm(this);
                }
            } else
            if(frame == null && dialog == null)
            {
                if(i == 1)
                {
                    if(form == null)
                        form = diamondedge.vb.Screen.getActiveForm();
                    if(form != null && (form instanceof diamondedge.vb.JForm))
                    {
                        diamondedge.vb.JForm jform = (diamondedge.vb.JForm)form;
                        javax.swing.JDialog jdialog = jform.getDialog();
                        if(jdialog != null)
                        {
                            dialog = new JDialog(jdialog, true);
                        } else
                        {
                            javax.swing.JFrame jframe1 = jform.getFrame();
                            dialog = new JDialog(jframe1, true);
                        }
                    }
                    if(form != null && (form instanceof diamondedge.vb.MDIForm))
                    {
                        diamondedge.vb.MDIForm mdiform = (diamondedge.vb.MDIForm)form;
                        javax.swing.JFrame jframe = mdiform.getFrame();
                        dialog = new JDialog(jframe, true);
                    }
                    if(dialog == null)
                        dialog = new JDialog((java.awt.Frame)null, true);
                    dialog.setDefaultCloseOperation(0);
                    window = dialog;
                    dialog.getContentPane().add(this, "Center");
                } else
                {
                    frame = new JFrame();
                    frame.setDefaultCloseOperation(0);
                    window = frame;
                    frame.getContentPane().add(this, "Center");
                }
                window.addWindowListener(this);
                init();
                if(window != null)
                    window.pack();
            } else
            {
                init();
                if(window != null)
                    window.pack();
            }
            if(aD)
                addScrollPane();
            diamondedge.vb.Application.a(null, this);
        }
        if(!isVisible())
        {
            setVisible(true);
            if(diamondedge.vb.Application.getApplet(this) != null)
                diamondedge.vb.Screen.setActiveForm(this);
        }
        if(window != null)
        {
            if(startUpPosition == 3)
                window.setLocation(0, 0);
            else
            if(startUpPosition == 2)
            {
                int j = (diamondedge.vb.Screen.getWidth() - window.getWidth()) / 2;
                int k = (diamondedge.vb.Screen.getHeight() - window.getHeight()) / 2;
                window.setLocation(j, k);
            } else
            {
                window.setLocation(locationX, locationY);
            }
            window.setVisible(true);
            if(window != null)
                window.toFront();
            if(frame != null)
                frame.setResizable(sizable);
            else
            if(dialog != null)
                dialog.setResizable(sizable);
            setWindowState(windowState);
        }
    }

    public void hideForm()
    {
        if(window != null)
            window.setVisible(false);
        else
            setVisible(false);
    }

    public void addScrollPane()
    {
        if(!(window instanceof javax.swing.RootPaneContainer))
            return;
        javax.swing.RootPaneContainer rootpanecontainer = (javax.swing.RootPaneContainer)window;
        rootpanecontainer.getContentPane().remove(this);
        rootpanecontainer.getContentPane().add(new JScrollPane(this), "Center");
        window.pack();
        java.awt.Component acomponent[] = getComponents();
        int i = 0;
        int j = 0;
        for(int k = 0; k < acomponent.length; k++)
        {
            java.awt.Component component = acomponent[k];
            if(component.getX() + component.getWidth() > i)
                i = component.getX() + component.getWidth();
            if(component.getY() + component.getHeight() > j)
                j = component.getY() + component.getHeight();
        }

        if(getWidth() > i)
        {
            i = getWidth();
        } else
        {
            i += 3;
            window.setSize(window.getWidth(), window.getHeight() + 16);
        }
        if(getHeight() > j)
        {
            j = getHeight();
        } else
        {
            j += 3;
            window.setSize(window.getWidth() + 16, window.getHeight());
        }
        setPreferredSize(new Dimension(i, j));
    }

    public final void printForm()
    {
        java.awt.PrintJob printjob = getToolkit().getPrintJob(new Frame(), "Form Print", new Properties());
        if(printjob != null)
        {
            java.awt.Graphics g = printjob.getGraphics();
            g.setClip(getBounds());
            printAll(g);
            g.dispose();
            printjob.end();
        }
    }

    public void zOrder(int i)
    {
        if(frame == null)
            return;
        if(i == 0)
            frame.toFront();
        else
            frame.toBack();
    }

    public void Form_Load()
    {
    }

    public void Form_DragDrop(java.awt.Component component, float f, float f1)
    {
    }

    public void Form_DragOver(java.awt.Component component, float f, float f1, int i)
    {
    }

    public void Form_Resize()
    {
    }

    public void Form_Unload(int i)
    {
    }

    public void Form_Unload(diamondedge.util.Variant variant)
    {
    }

    public void Form_QueryUnload(diamondedge.util.Variant variant, int i)
    {
    }

    public void Form_QueryUnload(int i, int j)
    {
    }

    public void Form_Activate()
    {
    }

    public void Form_Deactivate()
    {
    }

    public void Form_Paint()
    {
    }

    public void Form_Initialize()
    {
    }

    public void Form_Terminate()
    {
    }

    public static void showURL(java.lang.String s, diamondedge.vb.Application application)
    {
        if(s == null)
            return;
        try
        {
            if(application.isAppletRunning())
            {
                java.applet.Applet applet = application.getApplet();
                java.net.URL url = new URL(s);
                java.applet.AppletContext appletcontext = applet.getAppletContext();
                appletcontext.showDocument(url);
            } else
            {
                javax.swing.JEditorPane jeditorpane = new JEditorPane(s);
                jeditorpane.setEditable(false);
                javax.swing.JFrame jframe = new JFrame("Browse - " + s);
                jframe.setSize(600, 600);
                jframe.getContentPane().add(new JScrollPane(jeditorpane), "Center");
                jframe.setVisible(true);
            }
        }
        catch(java.lang.Exception exception)
        {
            diamondedge.util.Err.set(exception, "fullstack");
        }
    }

    public static java.lang.String processMnemonic(javax.swing.AbstractButton abstractbutton, java.lang.String s)
    {
        if(s == null)
            return null;
        int i = s.indexOf('&');
        char c = '\0';
        if(i >= 0)
        {
            c = s.charAt(i + 1);
            s = s.substring(0, i) + s.substring(i + 1);
        }
        if(i >= 0)
            abstractbutton.setMnemonic(c);
        return s;
    }

    public static void showPopupMenu(diamondedge.vb.Menu menu, java.awt.Component component, int i, int j)
    {
        if(menu == null || component == null)
        {
            return;
        } else
        {
            menu.getPopupMenu().show(component, i, j);
            return;
        }
    }

    public diamondedge.util.Variant getDataValue()
    {
        return null;
    }

    public java.lang.String getDataField()
    {
        return null;
    }

    public void setDataField(java.lang.String s)
    {
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return null;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
    }

    public static final java.lang.String copyright = "Copyright 1996-2006 Diamond Edge, Inc.";
    protected boolean initialized = false;
    protected boolean unloaded = false;
    protected boolean initInProgress = false;
    protected javax.swing.JFrame frame = null;
    protected javax.swing.JDialog dialog = null;
    protected java.awt.Window window = null;
    protected int startUpPosition = 0;
    protected int locationX = 0;
    protected int locationY = 0;
    protected int windowState = 0;
    protected boolean sizable = false;
    private int aC = 0;
    private boolean aI = false;
    private boolean aB = false;
    private boolean aD = false;
    private java.util.Vector aF = null;
    private java.util.Vector aG = null;
    private java.awt.FocusTraversalPolicy aE = null;
    public static java.awt.FocusTraversalPolicy TAB_ORDER_FOCUS_TRAVERSAL_POLICY = null;
    public static java.awt.FocusTraversalPolicy LAYOUT_FOCUS_TRAVERSAL_POLICY = null;
    static boolean aH = false;

    static 
    {
        javax.swing.UIManager.put("Button.showMnemonics", java.lang.Boolean.TRUE);
        try
        {
            TAB_ORDER_FOCUS_TRAVERSAL_POLICY = new VbFocusPolicy();
            LAYOUT_FOCUS_TRAVERSAL_POLICY = new LayoutFocusTraversalPolicy();
        }
        catch(java.lang.Throwable throwable)
        {
            java.lang.String s = "Requires version Java v1.4 or higher.";
            java.lang.String s1 = "Please upgrade your version of Java.";
            javax.swing.JOptionPane.showMessageDialog(null, s, s1, 1);
        }
    }
}
