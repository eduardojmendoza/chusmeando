// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.swing.IconList;
import diamondedge.util.Variant;
import diamondedge.util.VbCollection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

// Referenced classes of package diamondedge.vb:
//            TbButton

public class Toolbar extends javax.swing.JToolBar
    implements java.awt.event.ActionListener, javax.swing.SwingConstants
{

    public Toolbar()
    {
        a = new VbCollection();
        buttonGroup = null;
        imagelist = null;
    }

    public void initButtons(int i)
    {
        while(i-- > 0) 
            a(-1);
    }

    private diamondedge.vb.TbButton a(int i)
    {
        diamondedge.vb.TbButton tbbutton = new TbButton(this);
        if(i >= 0)
            add(tbbutton, i);
        else
            add(tbbutton);
        a.add(new Variant(tbbutton));
        tbbutton.addActionListener(this);
        return tbbutton;
    }

    public diamondedge.vb.TbButton addButton(int i, java.lang.String s, java.lang.String s1, int j, diamondedge.util.Variant variant)
    {
        diamondedge.vb.TbButton tbbutton = a(i);
        tbbutton.setText(s1);
        tbbutton.setKey(s);
        tbbutton.setStyle(j);
        tbbutton.setImage(variant);
        return tbbutton;
    }

    public static void initToolbar(diamondedge.vb.Toolbar toolbar)
    {
        toolbar.initToolbar();
    }

    public void initToolbar()
    {
        int i = getComponentCount();
        for(int j = 0; j < i; j++)
        {
            java.awt.Component component = getComponent(j);
            if(!(component instanceof diamondedge.vb.TbButton))
                continue;
            diamondedge.vb.TbButton tbbutton = (diamondedge.vb.TbButton)component;
            if(tbbutton.getKey() == null || tbbutton.getKey().equals(""))
                tbbutton.setKey(a.getKey(j + 1));
            else
                a.setKey(j + 1, tbbutton.getKey());
            a(tbbutton, _flddo);
        }

    }

    public void makeSeparator(int i)
    {
        addSeparator();
        int j = getComponentCount() - 1;
        java.awt.Component component = getComponentAtIndex(j);
        remove(j);
        i--;
        remove(i);
        add(component, i);
    }

    public void actionPerformed(java.awt.event.ActionEvent actionevent)
    {
        if(actionevent.getSource() instanceof diamondedge.vb.TbButton)
        {
            diamondedge.vb.TbButton tbbutton = (diamondedge.vb.TbButton)actionevent.getSource();
            if(_fldif != null)
                _fldif.actionPerformed(new ActionEvent(this, actionevent.getID(), tbbutton.getKey(), actionevent.getModifiers()));
        }
    }

    public void addActionListener(java.awt.event.ActionListener actionlistener)
    {
        _fldif = actionlistener;
    }

    public diamondedge.vb.TbButton getButton(int i)
    {
        java.lang.Object obj = null;
        diamondedge.util.Variant variant = a.getItem(i);
        if(variant != null)
            obj = variant.toObject();
        return (obj instanceof diamondedge.vb.TbButton) ? (diamondedge.vb.TbButton)obj : null;
    }

    public diamondedge.vb.TbButton getButton(java.lang.String s)
    {
        java.lang.Object obj = null;
        diamondedge.util.Variant variant = a.getItem(s);
        if(variant != null)
            obj = variant.toObject();
        return (obj instanceof diamondedge.vb.TbButton) ? (diamondedge.vb.TbButton)obj : null;
    }

    public diamondedge.util.VbCollection getButtons()
    {
        return a;
    }

    public diamondedge.swing.IconList getImageList()
    {
        return imagelist;
    }

    public void setImageList(diamondedge.swing.IconList iconlist)
    {
        imagelist = iconlist;
    }

    public boolean isBordered()
    {
        return getBorder() != null;
    }

    public void setBordered(boolean flag)
    {
        if(flag)
            setBorder(javax.swing.BorderFactory.createRaisedBevelBorder());
        else
            setBorder(null);
    }

    public int getTextPosition()
    {
        return _flddo;
    }

    public void setTextPosition(int i)
    {
        _flddo = i;
        int j = a.getCount();
        for(int k = 1; k <= j; k++)
            a(getButton(k), _flddo);

    }

    private void a(diamondedge.vb.TbButton tbbutton, int i)
    {
        if(tbbutton == null)
            return;
        if(i == 4)
        {
            tbbutton.setHorizontalTextPosition(4);
            tbbutton.setVerticalTextPosition(0);
        } else
        {
            tbbutton.setHorizontalTextPosition(0);
            tbbutton.setVerticalTextPosition(3);
        }
    }

    public void addExclusiveButton(javax.swing.AbstractButton abstractbutton)
    {
        add(abstractbutton);
        if(buttonGroup == null)
            buttonGroup = new ButtonGroup();
        buttonGroup.add(abstractbutton);
    }

    private diamondedge.util.VbCollection a = null;
    protected javax.swing.ButtonGroup buttonGroup = null;
    protected diamondedge.swing.IconList imagelist = null;
    private java.awt.event.ActionListener _fldif = null;
    private int _flddo = 0;
    public static final int RAISED = 9;
    public static final int ETCHED = 10;
}
