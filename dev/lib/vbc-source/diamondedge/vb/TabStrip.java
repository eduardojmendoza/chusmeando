// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.swing.IconList;
import javax.swing.JTabbedPane;

// Referenced classes of package diamondedge.vb:
//            Tab, VbControl

public class TabStrip extends javax.swing.JTabbedPane
    implements diamondedge.vb.VbControl
{

    public TabStrip()
    {
        imagelist = null;
    }

    public void initTabs(int i)
    {
        for(int j = getTabCount(); j < i; j++)
            addTab("", new Tab(this, j));

    }

    public diamondedge.vb.Tab getTab(int i)
    {
        return (diamondedge.vb.Tab)getComponentAt(i);
    }

    public diamondedge.vb.Tab getTab(java.lang.String s)
    {
        if(s == null)
            return null;
        int i = getComponentCount();
        for(int j = 0; j < i; j++)
            if(s.equals(getTab(j).getKey()))
                return getTab(j);

        return null;
    }

    public int getTabOrder()
    {
        return W;
    }

    public void setTabOrder(int i)
    {
        W = i;
    }

    public diamondedge.swing.IconList getImageList()
    {
        return imagelist;
    }

    public void setImageList(diamondedge.swing.IconList iconlist)
    {
        imagelist = iconlist;
    }

    protected diamondedge.swing.IconList imagelist = null;
    private int W = 0;
}
