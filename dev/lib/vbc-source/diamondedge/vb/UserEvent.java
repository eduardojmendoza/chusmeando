// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.util.EventObject;

public class UserEvent extends java.util.EventObject
{

    public UserEvent(java.lang.Object obj, int i, diamondedge.util.Variant variant, diamondedge.util.Variant variant1, diamondedge.util.Variant variant2)
    {
        super(obj);
        _fldfor = 0;
        _flddo = null;
        _fldif = null;
        a = null;
        _fldfor = i;
        _flddo = variant;
        _fldif = variant1;
        a = variant2;
    }

    public int getType()
    {
        return _fldfor;
    }

    public diamondedge.util.Variant getArg1()
    {
        return _flddo;
    }

    public diamondedge.util.Variant getArg2()
    {
        return _fldif;
    }

    public diamondedge.util.Variant getArg3()
    {
        return a;
    }

    int _fldfor = 0;
    diamondedge.util.Variant _flddo = null;
    diamondedge.util.Variant _fldif = null;
    diamondedge.util.Variant a = null;
}
