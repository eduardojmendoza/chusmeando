// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.ado.AdoError;
import diamondedge.ado.Command;
import diamondedge.ado.Field;
import diamondedge.ado.Fields;
import diamondedge.ado.Recordset;
import diamondedge.util.Variant;
import java.io.PrintStream;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

// Referenced classes of package diamondedge.vb:
//            ScrolledComponent, Item, Data, DataBound, 
//            VbControl, Screen, DataSource, DataBoundRouter, 
//            DataChangeEvent

public class ListBox extends diamondedge.vb.ScrolledComponent
    implements javax.swing.event.ListSelectionListener, diamondedge.vb.DataBound, diamondedge.vb.VbControl
{

    public ListBox()
    {
        super(new JList(new DefaultListModel()));
        l = -1;
        i = false;
        d = null;
        e = null;
        n = false;
        k = (javax.swing.JList)getViewport().getView();
        h = (javax.swing.DefaultListModel)k.getModel();
        k.setFont(diamondedge.vb.Screen.getDefaultFont());
        k.getSelectionModel().addListSelectionListener(this);
        k.setSelectionMode(0);
    }

    public ListBox(javax.swing.DefaultListModel defaultlistmodel)
    {
        super(new JList(defaultlistmodel));
        l = -1;
        i = false;
        d = null;
        e = null;
        n = false;
        k = (javax.swing.JList)getViewport().getView();
        h = defaultlistmodel;
        k.setFont(diamondedge.vb.Screen.getDefaultFont());
        k.getSelectionModel().addListSelectionListener(this);
        k.setSelectionMode(0);
    }

    public javax.swing.JList getList()
    {
        return k;
    }

    public javax.swing.DefaultListModel getModel()
    {
        return h;
    }

    public void setModel(javax.swing.DefaultListModel defaultlistmodel)
    {
        k.setModel(defaultlistmodel);
        h = defaultlistmodel;
    }

    public int getNewIndex()
    {
        return l;
    }

    public void addItem(java.lang.Object obj)
    {
        h.addElement(obj);
        l = h.size() - 1;
    }

    public void addItem(java.lang.Object obj, int i1)
    {
        addItem(new Item(obj, i1));
    }

    public void insertItemAt(java.lang.Object obj, int i1)
    {
        h.add(i1, obj);
        l = i1;
    }

    public void removeItemAt(int i1)
    {
        h.remove(i1);
    }

    public void clear()
    {
        diamondedge.vb.DataSource datasource = d;
        d = null;
        java.lang.String s = f;
        f = null;
        i = true;
        h.clear();
        i = false;
        d = datasource;
        f = s;
    }

    public void removeAllItems()
    {
        clear();
    }

    public int getItemCount()
    {
        return h.size();
    }

    public java.lang.Object getItemAt(int i1)
    {
        return h.get(i1);
    }

    public java.lang.String getItem(int i1)
    {
        java.lang.Object obj = h.get(i1);
        return obj != null ? obj.toString() : "";
    }

    public void setItem(int i1, java.lang.String s)
    {
        int j1 = h.size();
        if(i1 < 0 || i1 >= j1)
        {
            h.addElement(s);
        } else
        {
            java.lang.Object obj = getItemAt(i1);
            if(obj instanceof diamondedge.vb.Item)
                ((diamondedge.vb.Item)obj).setString(s);
            else
                h.set(i1, s);
        }
    }

    public int getItemData(int i1)
    {
        java.lang.Object obj = h.get(i1);
        if(obj instanceof diamondedge.vb.Item)
            return ((diamondedge.vb.Item)obj).getData();
        else
            return 0;
    }

    public void setItemData(int i1, int j1)
    {
        int k1 = h.size();
        if(i1 < 0 || i1 >= k1)
        {
            h.addElement(new Item("", j1));
        } else
        {
            java.lang.Object obj = h.get(i1);
            if(obj instanceof diamondedge.vb.Item)
                ((diamondedge.vb.Item)obj).setData(j1);
            else
                h.set(i1, new Item(obj, j1));
        }
    }

    public java.lang.Object getItemObject(int i1)
    {
        java.lang.Object obj = h.get(i1);
        if(obj instanceof diamondedge.vb.Item)
            return ((diamondedge.vb.Item)obj).getItem();
        else
            return obj;
    }

    public int findItem(java.lang.Object obj)
    {
        int i1 = h.getSize();
        for(int j1 = 0; j1 < i1; j1++)
            if(h.getElementAt(j1).equals(obj))
                return j1;

        return -1;
    }

    public int getSelectedIndex()
    {
        return k.getSelectedIndex();
    }

    public void setSelectedIndex(int i1)
    {
        if(i1 < 0)
            k.clearSelection();
        else
            k.setSelectedIndex(i1);
    }

    public java.lang.String getSelectedItem()
    {
        java.lang.Object obj = k.getSelectedValue();
        return obj != null ? obj.toString() : "";
    }

    public void setSelectedItem(java.lang.String s)
    {
        int i1 = findItem(s);
        k.setSelectedIndex(i1);
        k.ensureIndexIsVisible(i1);
    }

    public int getSelectionMode()
    {
        return k.getSelectionMode();
    }

    public void setSelectionMode(int i1)
    {
        k.setSelectionMode(i1);
    }

    public int getSelectedCount()
    {
        return k.getSelectedIndices().length;
    }

    public int getTopIndex()
    {
        return k.getFirstVisibleIndex();
    }

    public void setTopIndex(int i1)
    {
        k.ensureIndexIsVisible(i1);
    }

    public int getVisibleCount()
    {
        return k.getLastVisibleIndex() - k.getFirstVisibleIndex();
    }

    public boolean isSelected(int i1)
    {
        return k.isSelectedIndex(i1);
    }

    public void setSelected(int i1, boolean flag)
    {
        if(flag)
            k.addSelectionInterval(i1, i1);
        else
            k.removeSelectionInterval(i1, i1);
    }

    public void initializeList(java.lang.String as[])
    {
        if(as == null)
            return;
        for(int i1 = 0; i1 < as.length; i1++)
            setItem(i1, as[i1]);

    }

    public void initializeItemData(int ai[])
    {
        if(ai == null)
            return;
        for(int i1 = 0; i1 < ai.length; i1++)
            setItemData(i1, ai[i1]);

    }

    public java.lang.String getTag()
    {
        return p;
    }

    public void setTag(java.lang.String s)
    {
        p = s;
    }

    public int getTabOrder()
    {
        return m;
    }

    public void setTabOrder(int i1)
    {
        m = i1;
    }

    protected void fireSelectionValueChanged(int i1, int j1, boolean flag)
    {
        java.lang.Object aobj[] = listenerList.getListenerList();
        javax.swing.event.ListSelectionEvent listselectionevent = null;
        for(int k1 = aobj.length - 2; k1 >= 0; k1 -= 2)
        {
            if(aobj[k1] != (javax.swing.event.ListSelectionListener.class))
                continue;
            if(listselectionevent == null)
                listselectionevent = new ListSelectionEvent(this, i1, j1, flag);
            ((javax.swing.event.ListSelectionListener)aobj[k1 + 1]).valueChanged(listselectionevent);
        }

    }

    public void valueChanged(javax.swing.event.ListSelectionEvent listselectionevent)
    {
        if(!i)
            fireSelectionValueChanged(listselectionevent.getFirstIndex(), listselectionevent.getLastIndex(), listselectionevent.getValueIsAdjusting());
    }

    public void addListSelectionListener(javax.swing.event.ListSelectionListener listselectionlistener)
    {
        listenerList.add(javax.swing.event.ListSelectionListener.class, listselectionlistener);
    }

    public void removeListSelectionListener(javax.swing.event.ListSelectionListener listselectionlistener)
    {
        listenerList.remove(javax.swing.event.ListSelectionListener.class, listselectionlistener);
    }

    public java.lang.String getBoundField()
    {
        return f;
    }

    public void setBoundField(java.lang.String s)
    {
        if(s != null && !s.equals(""))
            f = s;
        else
            f = null;
    }

    public diamondedge.util.Variant getBoundValue()
    {
        diamondedge.ado.Recordset recordset;
        if(f == null || e == null)
            break MISSING_BLOCK_LABEL_85;
        recordset = e.getRecordset();
        if(recordset == null)
            break MISSING_BLOCK_LABEL_85;
        diamondedge.ado.Field field;
        int i1 = getSelectedIndex() + 1;
        if(recordset.getCursorType() == 0 && i1 < recordset.getAbsolutePosition())
            recordset.moveFirst();
        recordset.setAbsolutePosition(i1);
        field = recordset.getField(f);
        if(field != null)
            return field.getValue();
        break MISSING_BLOCK_LABEL_85;
        java.lang.Exception exception;
        exception;
        exception.printStackTrace();
        return null;
    }

    public java.lang.String getBoundText()
    {
        diamondedge.util.Variant variant = getBoundValue();
        if(variant != null)
            return variant.toString();
        else
            return "";
    }

    public void setBoundText(java.lang.String s)
    {
        if(f == null || d == null)
            break MISSING_BLOCK_LABEL_52;
        diamondedge.ado.Recordset recordset;
        recordset = d.getRecordset();
        if(recordset == null)
            return;
        try
        {
            recordset.getField(f).setValue(new Variant(s));
        }
        catch(java.lang.Exception exception) { }
    }

    public java.lang.String getListField()
    {
        return g;
    }

    public void setListField(java.lang.String s)
    {
        g = s;
    }

    public diamondedge.vb.DataSource getRowSource()
    {
        return e;
    }

    public void setRowSource(diamondedge.vb.DataSource datasource)
    {
        if(e != null)
            e.removeDataBound(this);
        if(datasource != null)
        {
            datasource.addDataBound(this);
            n = false;
        }
        e = datasource;
    }

    public void setRowSource(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        if(e instanceof diamondedge.vb.Data)
            ((diamondedge.vb.Data)e).setRecordSource(s);
        else
        if(e != null)
        {
            diamondedge.ado.Recordset recordset = e.getRecordset();
            if(recordset != null)
            {
                recordset.close();
                recordset.open(s);
            }
        }
    }

    public java.lang.String getDataField()
    {
        return o;
    }

    public void setDataField(java.lang.String s)
    {
        o = s;
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return d;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        if(d != null)
        {
            d.removeDataBound(this);
            removeListSelectionListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        if(datasource != null)
        {
            datasource.addDataBound(this);
            addListSelectionListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        d = datasource;
    }

    public diamondedge.util.Variant getDataValue()
    {
        if(f == null)
            return new Variant(getSelectedItem());
        else
            return getBoundValue();
    }

    public void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent)
    {
        java.lang.String s;
        s = datachangeevent.getValue() != null ? datachangeevent.getValue().toString() : "";
        if(e == null)
            break MISSING_BLOCK_LABEL_230;
        if(d == null || datachangeevent.getRecordset() != d.getRecordset())
            break MISSING_BLOCK_LABEL_206;
        if(s.length() == 0)
        {
            setSelectedIndex(-1);
            break MISSING_BLOCK_LABEL_235;
        }
        if(g == null || f == null || g.equals(f))
            break MISSING_BLOCK_LABEL_198;
        diamondedge.ado.Recordset recordset;
        recordset = e.getRecordset();
        if(recordset == null)
            return;
        try
        {
            java.lang.String s1 = recordset.getSource().getSqlWithNewFilter(recordset, f, s);
            recordset = new Recordset(recordset, false);
            recordset.open(s1);
            diamondedge.ado.Field field = recordset.getField(g);
            if(field != null)
                setSelectedItem(field.getValue().toString());
        }
        catch(java.lang.Exception exception)
        {
            java.lang.System.out.println("ComboBox.dataChanged(): " + exception);
        }
        break MISSING_BLOCK_LABEL_235;
        setSelectedItem(s);
        break MISSING_BLOCK_LABEL_235;
        if(datachangeevent.getChangeType() == 7 || datachangeevent.getChangeType() == 1)
            fillList();
        break MISSING_BLOCK_LABEL_235;
        setSelectedItem(s);
    }

    public void fillList()
    {
        if(!n && e != null && g != null)
        {
            diamondedge.ado.Recordset recordset = e.getRecordset();
            if(recordset == null)
                return;
            try
            {
                i = true;
                h.removeAllElements();
                int i1 = recordset.getFields().getFieldIndex(g, null);
                int j1 = recordset.getAbsolutePosition();
                int k1 = recordset.getRecordCount();
                if(k1 == -1)
                    k1 = recordset.getRecordCount(true);
                for(int l1 = 0; l1 < k1; l1++)
                {
                    diamondedge.util.Variant variant = recordset.getValueAt(l1, i1);
                    if(variant != null)
                        addItem(variant.toString(), l1 + 1);
                }

                if(k1 > 0)
                    k.setSelectedIndex(0);
                if(j1 != recordset.getAbsolutePosition())
                    recordset.setAbsolutePosition(j1);
                n = true;
            }
            catch(java.lang.Exception exception)
            {
                java.lang.System.out.println("ListBox.fillList:" + exception);
            }
            i = false;
        }
    }

    private java.lang.String p = null;
    private int m = 0;
    private int l = 0;
    private javax.swing.JList k = null;
    private javax.swing.DefaultListModel h = null;
    private javax.swing.event.ListSelectionListener j = null;
    private boolean i = false;
    private java.lang.String o = null;
    private diamondedge.vb.DataSource d = null;
    private java.lang.String g = null;
    private java.lang.String f = null;
    private diamondedge.vb.DataSource e = null;
    private boolean n = false;
}
