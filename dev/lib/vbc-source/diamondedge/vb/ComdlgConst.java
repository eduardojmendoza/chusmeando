// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;


public class ComdlgConst
{

    public ComdlgConst()
    {
    }

    public static final int cdlPDAllPages = 0;
    public static final int cdlPDCollate = 16;
    public static final int cdlPDDisablePrintToFile = 0x80000;
    public static final int cdlPDHidePrintToFile = 0x100000;
    public static final int cdlPDNoPageNums = 8;
    public static final int cdlPDNoSelection = 4;
    public static final int cdlPDNoWarning = 128;
    public static final int cdlPDPageNums = 2;
    public static final int cdlPDPrintSetup = 64;
    public static final int cdlPDPrintToFile = 32;
    public static final int cdlPDReturnDC = 256;
    public static final int cdlPDReturnDefault = 1024;
    public static final int cdlPDReturnIC = 512;
    public static final int cdlPDSelection = 1;
    public static final int cdlPDHelpButton = 2048;
    public static final int cdlPDUseDevModeCopies = 0x40000;
    public static final int cdlCCRGBInit = 1;
    public static final int cdlCCFullOpen = 2;
    public static final int cdlCCPreventFullOpen = 4;
    public static final int cdlCCHelpButton = 8;
    public static final int cdlOFNReadOnly = 1;
    public static final int cdlOFNOverwritePrompt = 2;
    public static final int cdlOFNHideReadOnly = 4;
    public static final int cdlOFNNoChangeDir = 8;
    public static final int cdlOFNHelpButton = 16;
    public static final int cdlOFNNoValidate = 256;
    public static final int cdlOFNAllowMultiselect = 512;
    public static final int cdlOFNExtensionDifferent = 1024;
    public static final int cdlOFNPathMustExist = 2048;
    public static final int cdlOFNFileMustExist = 4096;
    public static final int cdlOFNCreatePrompt = 8192;
    public static final int cdlOFNShareAware = 16384;
    public static final int cdlOFNNoReadOnlyReturn = 32768;
    public static final int cdlOFNNoLongNames = 0x40000;
    public static final int cdlOFNExplorer = 0x80000;
    public static final int cdlOFNNoDereferenceLinks = 0x100000;
    public static final int cdlOFNLongNames = 0x200000;
    public static final int cdlCFScreenFonts = 1;
    public static final int cdlCFPrinterFonts = 2;
    public static final int cdlCFBoth = 3;
    public static final int cdlCFHelpButton = 4;
    public static final int cdlCFEffects = 256;
    public static final int cdlCFApply = 512;
    public static final int cdlCFANSIOnly = 1024;
    public static final int cdlCFNoVectorFonts = 2048;
    public static final int cdlCFNoSimulations = 4096;
    public static final int cdlCFLimitSize = 8192;
    public static final int cdlCFFixedPitchOnly = 16384;
    public static final int cdlCFWYSIWYG = 32768;
    public static final int cdlCFForceFontExist = 0x10000;
    public static final int cdlCFScalableOnly = 0x20000;
    public static final int cdlCFTTOnly = 0x40000;
    public static final int cdlCFNoFaceSel = 0x80000;
    public static final int cdlCFNoStyleSel = 0x100000;
    public static final int cdlCFNoSizeSel = 0x200000;
    public static final int cdlInvalidPropertyValue = 380;
    public static final int cdlGetNotSupported = 394;
    public static final int cdlSetNotSupported = 383;
    public static final int cdlAlloc = 32752;
    public static final int cdlCancel = 32755;
    public static final int cdlDialogFailure = -32768;
    public static final int cdlFindResFailure = 32761;
    public static final int cdlHelp = 32751;
    public static final int cdlInitialization = 32765;
    public static final int cdlLoadResFailure = 32760;
    public static final int cdlLoadStrFailure = 32762;
    public static final int cdlLockResFailure = 32759;
    public static final int cdlMemAllocFailure = 32758;
    public static final int cdlMemLockFailure = 32757;
    public static final int cdlNoFonts = 24574;
    public static final int cdlBufferTooSmall = 20476;
    public static final int cdlInvalidFileName = 20477;
    public static final int cdlSubclassFailure = 20478;
    public static final int cdlCreateICFailure = 28661;
    public static final int cdlDndmMismatch = 28662;
    public static final int cdlGetDevModeFail = 28666;
    public static final int cdlInitFailure = 28665;
    public static final int cdlLoadDrvFailure = 28667;
    public static final int cdlNoDefaultPrn = 28663;
    public static final int cdlNoDevices = 28664;
    public static final int cdlParseFailure = 28669;
    public static final int cdlPrinterCodes = 28671;
    public static final int cdlPrinterNotFound = 28660;
    public static final int cdlRetDefFailure = 28668;
    public static final int cdlSetupFailure = 28670;
    public static final int cdlNoTemplate = 32764;
    public static final int cdlNoInstance = 32763;
    public static final int cdlInvalidSafeModeProcCall = 680;
    public static final int cdlHelpContext = 1;
    public static final int cdlHelpQuit = 2;
    public static final int cdlHelpIndex = 3;
    public static final int cdlHelpContents = 3;
    public static final int cdlHelpHelpOnHelp = 4;
    public static final int cdlHelpSetIndex = 5;
    public static final int cdlHelpSetContents = 5;
    public static final int cdlHelpContextPopup = 8;
    public static final int cdlHelpForceFile = 9;
    public static final int cdlHelpKey = 257;
    public static final int cdlHelpCommandHelp = 258;
    public static final int cdlHelpPartialKey = 261;
    public static final int cdlPortrait = 1;
    public static final int cdlLandscape = 2;
}
