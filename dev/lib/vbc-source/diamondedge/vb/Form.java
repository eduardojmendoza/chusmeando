// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;

public interface Form
{

    public abstract void init();

    public abstract java.awt.Component getActiveControl();

    public abstract boolean isInitialized();

    public abstract void unload();

    public abstract void setVisible(boolean flag);

    public abstract java.awt.Cursor getCursor();

    public abstract void setCursor(java.awt.Cursor cursor);

    public abstract void Form_Activate();

    public abstract void Form_Deactivate();

    public abstract void Form_Unload(int i);

    public abstract void Form_Unload(diamondedge.util.Variant variant);

    public abstract void Form_QueryUnload(diamondedge.util.Variant variant, int i);

    public abstract void Form_QueryUnload(int i, int j);

    public abstract void showForm();

    public abstract void hideForm();

    public abstract java.lang.String getName();

    public abstract void setName(java.lang.String s);

    public abstract void dispose();

    public abstract java.awt.Component getComponent(java.lang.String s);

    public abstract int getFormX();

    public abstract int getFormY();

    public abstract void setFormLocation(int i, int j);

    public abstract void setFormSize(int i, int j);

    public abstract int getStartUpPosition();

    public abstract void setStartUpPosition(int i);

    public abstract java.awt.Image getImage();

    public abstract java.lang.String getTitle();

    public abstract void setTitle(java.lang.String s);

    public abstract java.awt.Image getIcon();

    public abstract void setIcon(java.awt.Image image);

    public abstract double scaleX(double d, int i, int j);

    public abstract double scaleY(double d, int i, int j);

    public abstract void showForm(int i, diamondedge.vb.Form form);

    public abstract void printForm();

    public abstract void zOrder(int i);

    public abstract int getWindowState();

    public abstract void setWindowState(int i);

    public abstract java.awt.Component getComponent(int i);

    public abstract int getComponentCount();

    public abstract java.awt.Component add(java.awt.Component component, int i);

    public abstract void remove(int i);

    public abstract java.awt.Container getParent();

    public abstract boolean isVisible();

    public abstract boolean isShowing();

    public abstract boolean isEnabled();

    public abstract void setEnabled(boolean flag);

    public abstract java.awt.Color getForeground();

    public abstract void setForeground(java.awt.Color color);

    public abstract java.awt.Color getBackground();

    public abstract void setBackground(java.awt.Color color);

    public abstract java.awt.Font getFont();

    public abstract void setFont(java.awt.Font font);

    public abstract java.awt.Point getLocation();

    public abstract void setLocation(int i, int j);

    public abstract java.awt.Dimension getSize();

    public abstract void setSize(int i, int j);

    public abstract void repaint();

    public abstract void requestFocus();

    public abstract int getWidth();

    public abstract int getHeight();
}
