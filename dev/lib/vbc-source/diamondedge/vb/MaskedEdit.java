// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.awt.Cursor;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.text.ParseException;
import javax.swing.JFormattedTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.text.Document;
import javax.swing.text.MaskFormatter;

// Referenced classes of package diamondedge.vb:
//            DataBound, TextBoxCtrl, VbControl, Screen, 
//            DataSource, DataBoundRouter, DataChangeEvent

public class MaskedEdit extends javax.swing.JFormattedTextField
    implements javax.swing.event.DocumentListener, diamondedge.vb.DataBound, diamondedge.vb.TextBoxCtrl, diamondedge.vb.VbControl
{

    public MaskedEdit()
    {
        super(new MaskFormatter());
        D = null;
        E = true;
        G = null;
        D = (javax.swing.text.MaskFormatter)getFormatter();
        setFocusLostBehavior(0);
        setFont(diamondedge.vb.Screen.getDefaultFont());
        setCursor(java.awt.Cursor.getPredefinedCursor(2));
        setHorizontalAlignment(2);
        getDocument().addDocumentListener(this);
    }

    public javax.swing.text.MaskFormatter getMaskFormatter()
    {
        return D;
    }

    public java.lang.String getMask()
    {
        return D.getMask();
    }

    public void setMask(java.lang.String s)
        throws java.text.ParseException
    {
        D.setMask(s);
    }

    public java.lang.String getTextValue()
    {
        if(E)
            break MISSING_BLOCK_LABEL_13;
        return getClipText();
        java.text.ParseException parseexception;
        parseexception;
        return getText();
    }

    public java.lang.String getFormattedText()
    {
        java.lang.String s = getText();
        if(D.getPlaceholderCharacter() != ' ')
            s = s.replace(D.getPlaceholderCharacter(), ' ');
        return s;
    }

    public java.lang.String getClipText()
        throws java.text.ParseException
    {
        boolean flag = D.getValueContainsLiteralCharacters();
        D.setValueContainsLiteralCharacters(false);
        java.lang.String s = (java.lang.String)D.stringToValue(getText());
        D.setValueContainsLiteralCharacters(flag);
        return s;
    }

    public java.lang.String getPromptChar()
    {
        return java.lang.String.valueOf(D.getPlaceholderCharacter());
    }

    public void setPromptChar(java.lang.String s)
    {
        D.setPlaceholderCharacter(s.charAt(0));
    }

    public boolean isLiteralsIncluded()
    {
        return E;
    }

    public void setLiteralsIncluded(boolean flag)
    {
        E = flag;
    }

    public java.lang.String getTag()
    {
        return C;
    }

    public void setTag(java.lang.String s)
    {
        C = s;
    }

    public int getTabOrder()
    {
        return F;
    }

    public void setTabOrder(int i)
    {
        F = i;
    }

    public java.lang.String toString()
    {
        return getText();
    }

    protected void fireDocumentChanged(javax.swing.event.DocumentEvent documentevent)
    {
        java.lang.Object aobj[] = listenerList.getListenerList();
        java.awt.event.TextEvent textevent = null;
        for(int i = aobj.length - 2; i >= 0; i -= 2)
        {
            if(aobj[i] != (java.awt.event.TextListener.class))
                continue;
            if(textevent == null)
                textevent = new TextEvent(this, 900);
            ((java.awt.event.TextListener)aobj[i + 1]).textValueChanged(textevent);
        }

    }

    public void changedUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void insertUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void removeUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void addTextListener(java.awt.event.TextListener textlistener)
    {
        listenerList.add(java.awt.event.TextListener.class, textlistener);
    }

    public void removeTextListener(java.awt.event.TextListener textlistener)
    {
        listenerList.remove(java.awt.event.TextListener.class, textlistener);
    }

    public java.lang.String getDataField()
    {
        return H;
    }

    public void setDataField(java.lang.String s)
    {
        H = s;
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return G;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        if(G != null)
        {
            G.removeDataBound(this);
            removeTextListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        if(datasource != null)
        {
            datasource.addDataBound(this);
            addTextListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        G = datasource;
    }

    public diamondedge.util.Variant getDataValue()
    {
        return new Variant(getTextValue());
    }

    public void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent)
    {
        setValue(datachangeevent.getValue() != null ? ((java.lang.Object) (datachangeevent.getValue().toString())) : "");
    }

    private java.lang.String C = null;
    private int F = 0;
    private javax.swing.text.MaskFormatter D = null;
    private boolean E = false;
    private java.lang.String H = null;
    private diamondedge.vb.DataSource G = null;
}
