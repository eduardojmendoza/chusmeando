// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.applet.Applet;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.InputEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

// Referenced classes of package diamondedge.vb:
//            Form, Application, Screen, DataSource

public class JForm2 extends javax.swing.JPanel
    implements diamondedge.vb.Form, java.awt.event.WindowListener
{

    public JForm2()
    {
        initialized = false;
        unloaded = false;
        initInProgress = false;
        startUpPosition = 0;
        buttonGroup = null;
        setFont(defaultFont);
    }

    public java.awt.Image getImage()
    {
        return null;
    }

    public void printForm()
    {
    }

    protected void finalize()
    {
        Form_Terminate();
    }

    public void init()
    {
        initialize();
    }

    public void initialize()
    {
        if(initInProgress)
        {
            return;
        } else
        {
            initInProgress = true;
            Form_Initialize();
            Form_Load();
            unloaded = false;
            initInProgress = false;
            return;
        }
    }

    public void unload()
    {
        unloaded = true;
        initialized = false;
    }

    public boolean isInitialized()
    {
        return initialized;
    }

    protected void initializeForm(int i)
    {
        initialized = true;
    }

    public void dispose()
    {
        if(window != null)
            window.dispose();
        frame = null;
        dialog = null;
        window = null;
    }

    public boolean isFocusable()
    {
        int i = getComponentCount();
        for(int j = 0; j < i; j++)
        {
            java.awt.Component component = getComponent(j);
            if(component.isVisible() && component.isEnabled() && component.isFocusable())
                return false;
        }

        return true;
    }

    public java.awt.Component getComponent(java.lang.String s)
    {
        int i = getComponentCount();
        for(int j = 0; j < i; j++)
        {
            java.awt.Component component = getComponent(j);
            if(s.equals(component.getName()))
                return component;
        }

        return null;
    }

    public void paintComponent(java.awt.Graphics g)
    {
        super.paintComponent(g);
        Form_Paint();
    }

    public void setBounds(int i, int j, int k, int l)
    {
        super.setBounds(i, j, k, l);
        Form_Resize();
    }

    public void setFormSize(int i, int j)
    {
        java.awt.Dimension dimension = new Dimension(i, j);
        setPreferredSize(dimension);
        setMinimumSize(dimension);
        if(window != null)
            window.setSize(i, j);
    }

    public int getFormX()
    {
        if(window != null)
            return window.getX();
        else
            return locationX;
    }

    public int getFormY()
    {
        if(window != null)
            return window.getY();
        else
            return locationY;
    }

    public void setFormLocation(int i, int j)
    {
        locationX = i;
        locationY = j;
        if(window != null)
            window.setLocation(locationX, locationY);
    }

    public int getStartUpPosition()
    {
        return startUpPosition;
    }

    public void setStartUpPosition(int i)
    {
        startUpPosition = i;
    }

    public synchronized void setCursor(java.awt.Cursor cursor)
    {
        if(window != null)
            window.setCursor(cursor);
        else
            super.setCursor(cursor);
    }

    public void add(javax.swing.JMenu jmenu)
    {
        if(frame != null)
        {
            javax.swing.JMenuBar jmenubar = frame.getJMenuBar();
            if(jmenubar == null)
            {
                jmenubar = new JMenuBar();
                frame.setJMenuBar(jmenubar);
            }
            jmenubar.add(jmenu);
        } else
        {
            java.applet.Applet applet = diamondedge.vb.Application.getApplet(this);
            if(applet instanceof javax.swing.JApplet)
            {
                javax.swing.JMenuBar jmenubar1 = null;
                javax.swing.JApplet japplet = (javax.swing.JApplet)applet;
                int i = japplet.getComponentCount();
                int j = 0;
                do
                {
                    if(j >= i)
                        break;
                    java.awt.Component component = japplet.getComponent(j);
                    if(component instanceof javax.swing.JMenuBar)
                    {
                        jmenubar1 = (javax.swing.JMenuBar)component;
                        break;
                    }
                    j++;
                } while(true);
                if(jmenubar1 == null)
                {
                    jmenubar1 = new JMenuBar();
                    japplet.getContentPane().add(jmenubar1, "North");
                }
                jmenubar1.add(jmenu);
            }
        }
    }

    public java.lang.String getTitle()
    {
        if(frame != null)
            return frame.getTitle();
        if(dialog != null)
            return dialog.getTitle();
        else
            return "";
    }

    public void setTitle(java.lang.String s)
    {
        if(frame != null)
            frame.setTitle(s);
        else
        if(dialog != null)
            dialog.setTitle(s);
    }

    public java.awt.Image getIcon()
    {
        if(frame != null)
            return frame.getIconImage();
        else
            return null;
    }

    public void setIcon(java.lang.String s)
    {
        if(frame != null)
            frame.setIconImage(diamondedge.vb.Screen.loadImage(s));
    }

    public void setIcon(java.awt.Image image)
    {
        if(frame != null)
            frame.setIconImage(image);
    }

    public int getBorderStyle()
    {
        if(frame != null)
            return !frame.isResizable() ? 1 : 2;
        if(dialog != null)
            return !dialog.isResizable() ? 1 : 2;
        else
            return 2;
    }

    public void setBorderStyle(int i)
    {
        if(frame != null)
        {
            if(i == 2)
                frame.setResizable(true);
            else
                frame.setResizable(false);
        } else
        if(dialog != null)
            if(i == 2)
                dialog.setResizable(true);
            else
                dialog.setResizable(false);
    }

    public int getWindowState()
    {
        return frame == null || frame.getState() != 1 ? 0 : 1;
    }

    public void setWindowState(int i)
    {
        if(frame == null)
            return;
        switch(i)
        {
        case 0: // '\0'
            frame.setState(0);
            break;

        case 1: // '\001'
            frame.setState(1);
            break;

        case 2: // '\002'
            frame.setState(0);
            break;
        }
    }

    public java.awt.Component getActiveControl()
    {
        if(window != null)
            return window.getFocusOwner();
        else
            return null;
    }

    public javax.swing.JDialog getDialog()
    {
        return dialog;
    }

    public javax.swing.JFrame getFrame()
    {
        if(frame == null)
            window = frame = diamondedge.vb.JForm2.a(this);
        return frame;
    }

    private static javax.swing.JFrame a(java.awt.Component component)
    {
        for(; component != null; component = component.getParent())
            if(component instanceof javax.swing.JFrame)
                return (javax.swing.JFrame)component;

        return null;
    }

    public static short buttonFlags(java.awt.event.InputEvent inputevent)
    {
        int i = inputevent.getModifiers();
        short word0 = 0;
        if((i & 0x10) != 0)
            word0 |= 1;
        if((i & 8) != 0)
            word0 |= 2;
        if((i & 4) != 0)
            word0 |= 4;
        return word0;
    }

    public static short shiftFlags(java.awt.event.InputEvent inputevent)
    {
        int i = inputevent.getModifiers();
        short word0 = 0;
        if((i & 1) != 0)
            word0 |= 1;
        if((i & 2) != 0)
            word0 |= 2;
        if((i & 4) != 0)
            word0 |= 4;
        return word0;
    }

    public void windowClosing(java.awt.event.WindowEvent windowevent)
    {
        diamondedge.vb.JForm2.unload(this);
    }

    public void windowActivated(java.awt.event.WindowEvent windowevent)
    {
        Form_Activate();
    }

    public void windowDeactivated(java.awt.event.WindowEvent windowevent)
    {
        Form_Deactivate();
    }

    public void windowOpened(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowClosed(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowIconified(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowDeiconified(java.awt.event.WindowEvent windowevent)
    {
    }

    public void showForm()
    {
        showForm(0, null);
    }

    public void showForm(int i, diamondedge.vb.Form form)
    {
        if(initInProgress)
            return;
        setVisible(true);
        if(!initialized || frame == null && dialog == null)
            if(diamondedge.vb.JForm2.getApplet(this) != null)
            {
                if(!initialized)
                {
                    init();
                    validate();
                }
            } else
            if(frame == null && dialog == null)
            {
                if(i == 1)
                {
                    if(form != null && (form instanceof diamondedge.vb.JForm2))
                    {
                        diamondedge.vb.JForm2 jform2 = (diamondedge.vb.JForm2)form;
                        javax.swing.JDialog jdialog = jform2.getDialog();
                        if(jdialog != null)
                        {
                            dialog = new JDialog(jdialog, true);
                        } else
                        {
                            javax.swing.JFrame jframe = jform2.getFrame();
                            dialog = new JDialog(jframe, true);
                        }
                    }
                    if(dialog == null)
                        dialog = new JDialog((java.awt.Frame)null, true);
                    window = dialog;
                    dialog.getContentPane().add(this, "Center");
                } else
                {
                    frame = new JFrame();
                    window = frame;
                    frame.getContentPane().add(this, "Center");
                }
                window.addWindowListener(this);
                init();
                window.pack();
            } else
            {
                init();
                frame.pack();
            }
        if(!isVisible())
            setVisible(true);
        if(window != null)
        {
            if(startUpPosition == 3)
                window.setLocation(0, 0);
            else
            if(startUpPosition == 2)
            {
                int j = (diamondedge.vb.Screen.getWidth() - window.getWidth()) / 2;
                int k = (diamondedge.vb.Screen.getHeight() - window.getHeight()) / 2;
                window.setLocation(j, k);
            } else
            {
                window.setLocation(locationX, locationY);
            }
            window.setVisible(true);
        }
    }

    public void hideForm()
    {
        if(frame != null)
            frame.setVisible(false);
        else
            setVisible(false);
    }

    public static final void load(diamondedge.vb.JForm2 jform2)
    {
        if(jform2 != null)
            jform2.showForm();
    }

    public static final void unload(diamondedge.vb.JForm2 jform2)
    {
        if(jform2 != null && jform2.isInitialized())
        {
            diamondedge.util.Variant variant = new Variant(false);
            jform2.Form_QueryUnload(variant, 1);
            if(variant.toBoolean())
                return;
            jform2.Form_QueryUnload(0, 1);
            jform2.Form_Unload(variant);
            if(variant.toBoolean())
                return;
            jform2.Form_Unload(0);
            jform2.unload();
            java.applet.Applet applet = diamondedge.vb.JForm2.getApplet(jform2);
            if(applet == null)
            {
                jform2.hideForm();
                jform2.dispose();
            }
        }
        diamondedge.vb.JForm2._mthif();
    }

    private static final void _mthif()
    {
        for(java.util.Enumeration enumeration = aA.elements(); enumeration.hasMoreElements();)
        {
            diamondedge.vb.Form form = (diamondedge.vb.Form)enumeration.nextElement();
            if(form != null && form.isInitialized())
                return;
        }

        try
        {
            java.lang.System.exit(0);
        }
        catch(java.lang.Exception exception) { }
    }

    public static final java.applet.Applet getApplet(java.awt.Component component)
    {
        for(; component != null; component = component.getParent())
            if(component instanceof java.applet.Applet)
                return (java.applet.Applet)component;

        return null;
    }

    public void zOrder(int i)
    {
        if(frame == null)
            return;
        if(i == 0)
            frame.toFront();
        else
            frame.toBack();
    }

    public void Form_Load()
    {
    }

    public void Form_Resize()
    {
    }

    public void Form_Unload(int i)
    {
    }

    public void Form_Unload(diamondedge.util.Variant variant)
    {
    }

    public void Form_QueryUnload(diamondedge.util.Variant variant, int i)
    {
    }

    public void Form_QueryUnload(int i, int j)
    {
    }

    public void Form_Activate()
    {
    }

    public void Form_Deactivate()
    {
    }

    public void Form_Paint()
    {
    }

    public void Form_Initialize()
    {
    }

    public void Form_Terminate()
    {
    }

    public double scaleX(double d, int i, int j)
    {
        return d;
    }

    public double scaleY(double d, int i, int j)
    {
        return d;
    }

    public diamondedge.util.Variant getDataValue()
    {
        return null;
    }

    public java.lang.String getDataField()
    {
        return null;
    }

    public void setDataField(java.lang.String s)
    {
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return null;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
    }

    public static final void Load(diamondedge.vb.JForm2 jform2)
    {
        diamondedge.vb.JForm2.load(jform2);
    }

    public final void Unload(diamondedge.vb.JForm2 jform2)
    {
        diamondedge.vb.JForm2.unload(jform2);
    }

    public static final java.lang.String copyright = "Copyright 1996-2004 Diamond Edge, Inc.";
    protected boolean initialized = false;
    protected boolean unloaded = false;
    protected boolean initInProgress = false;
    protected javax.swing.JFrame frame = null;
    protected javax.swing.JDialog dialog = null;
    protected java.awt.Window window = null;
    protected int startUpPosition = 0;
    protected int locationX = 0;
    protected int locationY = 0;
    protected javax.swing.ButtonGroup buttonGroup = null;
    protected static java.awt.Font defaultFont = new Font("sansserif", 0, 10);
    private static java.util.Vector aA = new Vector(12);

}
