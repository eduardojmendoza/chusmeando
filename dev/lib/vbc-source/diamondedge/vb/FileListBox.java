// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.io.File;
import javax.swing.DefaultListModel;

// Referenced classes of package diamondedge.vb:
//            ListBox, FileExtensionFilter

public class FileListBox extends diamondedge.vb.ListBox
{

    public FileListBox()
    {
        q = "*.*";
        r = null;
    }

    public java.lang.String getPattern()
    {
        return q;
    }

    public void setPattern(java.lang.String s)
    {
        q = s;
    }

    public java.lang.String getFileName()
    {
        return getSelectedItem();
    }

    public java.lang.String getPath()
    {
        return r;
    }

    public void setPath(java.lang.String s)
    {
        r = s;
        java.io.File file = new File(s);
        diamondedge.vb.FileExtensionFilter fileextensionfilter = null;
        if(q.startsWith("*.") && !q.equals("*.*"))
        {
            fileextensionfilter = new FileExtensionFilter(false, null);
            fileextensionfilter.addExtension(q.substring(2));
        }
        java.io.File afile[] = file.listFiles(fileextensionfilter);
        if(afile != null)
        {
            javax.swing.DefaultListModel defaultlistmodel = getModel();
            defaultlistmodel.removeAllElements();
            for(int i = 0; i < afile.length; i++)
                defaultlistmodel.addElement(afile[i].getName());

        }
    }

    private java.lang.String q = null;
    private java.lang.String r = null;
}
