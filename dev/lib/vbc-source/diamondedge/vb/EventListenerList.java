// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.util.EventListener;

public class EventListenerList
{

    public EventListenerList()
    {
        listenerList = a;
    }

    public java.lang.Object[] getListenerList()
    {
        return listenerList;
    }

    public int getListenerCount()
    {
        return listenerList.length / 2;
    }

    public int getListenerCount(java.lang.Class class1)
    {
        int i = 0;
        java.lang.Object aobj[] = listenerList;
        for(int j = 0; j < aobj.length; j += 2)
            if(class1 == (java.lang.Class)aobj[j])
                i++;

        return i;
    }

    public synchronized void add(java.lang.Class class1, java.util.EventListener eventlistener)
    {
        if(eventlistener == null)
            return;
        if(!class1.isInstance(eventlistener))
            throw new IllegalArgumentException("Listener " + eventlistener + " is not of type " + class1);
        if(listenerList == a)
        {
            listenerList = (new java.lang.Object[] {
                class1, eventlistener
            });
        } else
        {
            int i = listenerList.length;
            java.lang.Object aobj[] = new java.lang.Object[i + 2];
            java.lang.System.arraycopy(((java.lang.Object) (listenerList)), 0, ((java.lang.Object) (aobj)), 0, i);
            aobj[i] = class1;
            aobj[i + 1] = eventlistener;
            listenerList = aobj;
        }
    }

    public synchronized void remove(java.lang.Class class1, java.util.EventListener eventlistener)
    {
        if(eventlistener == null)
            return;
        if(!class1.isInstance(eventlistener))
            throw new IllegalArgumentException("Listener " + eventlistener + " is not of type " + class1);
        int i = -1;
        int j = listenerList.length - 2;
        do
        {
            if(j < 0)
                break;
            if(listenerList[j] == class1 && listenerList[j + 1].equals(eventlistener))
            {
                i = j;
                break;
            }
            j -= 2;
        } while(true);
        if(i != -1)
        {
            java.lang.Object aobj[] = new java.lang.Object[listenerList.length - 2];
            java.lang.System.arraycopy(((java.lang.Object) (listenerList)), 0, ((java.lang.Object) (aobj)), 0, i);
            if(i < aobj.length)
                java.lang.System.arraycopy(((java.lang.Object) (listenerList)), i + 2, ((java.lang.Object) (aobj)), i, aobj.length - i);
            listenerList = aobj.length != 0 ? aobj : a;
        }
    }

    public java.lang.String toString()
    {
        java.lang.Object aobj[] = listenerList;
        java.lang.String s = "EventListenerList: ";
        s = s + aobj.length / 2 + " listeners: ";
        for(int i = 0; i <= aobj.length - 2; i += 2)
        {
            s = s + " type " + ((java.lang.Class)aobj[i]).getName();
            s = s + " listener " + aobj[i + 1];
        }

        return s;
    }

    private static final java.lang.Object a[] = new java.lang.Object[0];
    protected transient java.lang.Object listenerList[] = null;

}
