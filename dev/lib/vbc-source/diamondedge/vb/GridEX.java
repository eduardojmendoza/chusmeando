// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.ado.Recordset;
import javax.swing.table.JTableHeader;

// Referenced classes of package diamondedge.vb:
//            Grid, Data

public class GridEX extends diamondedge.vb.Grid
{

    public GridEX()
    {
        this(2, 2);
    }

    public GridEX(int i, int j)
    {
        super(i - 1, j);
        aM = null;
        getTableHeader().setReorderingAllowed(false);
        setAutoResizeMode(0);
        setRowHeaderAutoLabel(0);
        leaveFrozenColumns = true;
    }

    public void init()
    {
        countFrozenRow = true;
        removeAll();
        if(aM == null)
        {
            aM = new Data();
            aM.setVisible(false);
        }
    }

    public void initEnd()
    {
        setDataSource(aM);
    }

    public diamondedge.vb.Data getData()
    {
        return aM;
    }

    public void setRecordset(diamondedge.ado.Recordset recordset)
    {
        if(aM != null)
            aM.setRecordset(recordset);
        else
            throw new IllegalArgumentException("Cannot change Recordset.");
    }

    public final boolean isAutoSort()
    {
        return isSorted() && getNumClicksToSort() > 0;
    }

    public void setAutoSort(boolean flag)
    {
        setSorted(flag);
        if(flag)
            setNumClicksToSort(1);
        else
            setNumClicksToSort(0);
    }

    private diamondedge.vb.Data aM = null;
    public static final int GRID_LINES_NONE = 0;
    public static final int GRID_LINES_VERTICAL = 1;
    public static final int GRID_LINES_HORIZONTAL = 2;
    public static final int GRID_LINES_BOTH = 3;
}
