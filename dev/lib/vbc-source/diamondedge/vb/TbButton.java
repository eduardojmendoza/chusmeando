// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JToolBar;

// Referenced classes of package diamondedge.vb:
//            Screen, Toolbar, ImageList, JForm

public class TbButton extends javax.swing.JButton
{

    public TbButton(diamondedge.vb.Toolbar toolbar)
    {
        setFont(diamondedge.vb.Screen.getDefaultFont());
        _fldint = toolbar;
        int i = 1;
        byte byte0 = 2;
        setMargin(new Insets(byte0, i, byte0, i));
        setHorizontalTextPosition(0);
        setVerticalTextPosition(3);
    }

    public java.lang.String getKey()
    {
        return _fldif;
    }

    public void setKey(java.lang.String s)
    {
        _fldif = s;
    }

    public int getStyle()
    {
        return _fldfor;
    }

    public void setStyle(int i)
    {
        _fldfor = i;
        if(_fldfor == 3)
        {
            int j = _fldint.getComponentCount();
            int k = 0;
            do
            {
                if(k >= j)
                    break;
                if(_fldint.getComponent(k) == this)
                {
                    _fldint.remove(k);
                    _fldint.add(new javax.swing.JToolBar.Separator(), k);
                    break;
                }
                k++;
            } while(true);
        }
    }

    public diamondedge.util.Variant getImage()
    {
        return _flddo;
    }

    public void setImage(diamondedge.util.Variant variant)
    {
        _flddo = variant;
        setIcon(diamondedge.vb.ImageList.getIcon(variant, _fldint.getImageList()));
    }

    public void setText(java.lang.String s)
    {
        super.setText(diamondedge.vb.JForm.processMnemonic(this, s));
    }

    public diamondedge.util.Variant getTag()
    {
        if(a == null)
            a = new Variant();
        return a;
    }

    public void setTag(diamondedge.util.Variant variant)
    {
        getTag().set(variant);
    }

    private diamondedge.util.Variant a = null;
    private diamondedge.util.Variant _flddo = null;
    private java.lang.String _fldif = null;
    private int _fldfor = 0;
    private diamondedge.vb.Toolbar _fldint = null;
}
