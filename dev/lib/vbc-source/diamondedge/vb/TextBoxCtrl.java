// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.MenuComponent;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.TextListener;
import java.awt.image.ColorModel;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Locale;
import javax.accessibility.AccessibleContext;
import javax.swing.JRootPane;
import javax.swing.JToolTip;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;

// Referenced classes of package diamondedge.vb:
//            DataSource, DataChangeEvent

public interface TextBoxCtrl
{

    public abstract java.lang.String getTag();

    public abstract void setTag(java.lang.String s);

    public abstract void changedUpdate(javax.swing.event.DocumentEvent documentevent);

    public abstract void insertUpdate(javax.swing.event.DocumentEvent documentevent);

    public abstract void removeUpdate(javax.swing.event.DocumentEvent documentevent);

    public abstract void addTextListener(java.awt.event.TextListener textlistener);

    public abstract void removeTextListener(java.awt.event.TextListener textlistener);

    public abstract java.lang.String getDataField();

    public abstract void setDataField(java.lang.String s);

    public abstract diamondedge.vb.DataSource getDataSource();

    public abstract void setDataSource(diamondedge.vb.DataSource datasource);

    public abstract diamondedge.util.Variant getDataValue();

    public abstract void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent);

    public abstract int getHorizontalAlignment();

    public abstract void setHorizontalAlignment(int i);

    public abstract void setText(java.lang.String s);

    public abstract java.lang.String getText();

    public abstract java.lang.String getSelectedText();

    public abstract boolean isEditable();

    public abstract void setEditable(boolean flag);

    public abstract int getSelectionStart();

    public abstract void setSelectionStart(int i);

    public abstract int getSelectionEnd();

    public abstract void setSelectionEnd(int i);

    public abstract boolean isPaintingTile();

    public abstract boolean isFocusCycleRoot();

    public abstract boolean isManagingFocus();

    public abstract void setNextFocusableComponent(java.awt.Component component);

    public abstract java.awt.Component getNextFocusableComponent();

    public abstract void setRequestFocusEnabled(boolean flag);

    public abstract boolean isRequestFocusEnabled();

    public abstract void grabFocus();

    public abstract void setPreferredSize(java.awt.Dimension dimension);

    public abstract void setMaximumSize(java.awt.Dimension dimension);

    public abstract void setMinimumSize(java.awt.Dimension dimension);

    public abstract void setBorder(javax.swing.border.Border border);

    public abstract javax.swing.border.Border getBorder();

    public abstract java.awt.Insets getInsets();

    public abstract java.awt.Insets getInsets(java.awt.Insets insets);

    public abstract void setAlignmentY(float f);

    public abstract void setAlignmentX(float f);

    public abstract void setDebugGraphicsOptions(int i);

    public abstract int getDebugGraphicsOptions();

    public abstract void registerKeyboardAction(java.awt.event.ActionListener actionlistener, java.lang.String s, javax.swing.KeyStroke keystroke, int i);

    public abstract void registerKeyboardAction(java.awt.event.ActionListener actionlistener, javax.swing.KeyStroke keystroke, int i);

    public abstract void unregisterKeyboardAction(javax.swing.KeyStroke keystroke);

    public abstract javax.swing.KeyStroke[] getRegisteredKeyStrokes();

    public abstract int getConditionForKeyStroke(javax.swing.KeyStroke keystroke);

    public abstract java.awt.event.ActionListener getActionForKeyStroke(javax.swing.KeyStroke keystroke);

    public abstract void resetKeyboardActions();

    public abstract boolean requestDefaultFocus();

    public abstract void setToolTipText(java.lang.String s);

    public abstract java.lang.String getToolTipText();

    public abstract java.lang.String getToolTipText(java.awt.event.MouseEvent mouseevent);

    public abstract java.awt.Point getToolTipLocation(java.awt.event.MouseEvent mouseevent);

    public abstract javax.swing.JToolTip createToolTip();

    public abstract void scrollRectToVisible(java.awt.Rectangle rectangle);

    public abstract void setAutoscrolls(boolean flag);

    public abstract boolean getAutoscrolls();

    public abstract java.lang.Object getClientProperty(java.lang.Object obj);

    public abstract void putClientProperty(java.lang.Object obj, java.lang.Object obj1);

    public abstract void setOpaque(boolean flag);

    public abstract void computeVisibleRect(java.awt.Rectangle rectangle);

    public abstract java.awt.Rectangle getVisibleRect();

    public abstract java.awt.Container getTopLevelAncestor();

    public abstract void addAncestorListener(javax.swing.event.AncestorListener ancestorlistener);

    public abstract void removeAncestorListener(javax.swing.event.AncestorListener ancestorlistener);

    public abstract void repaint(java.awt.Rectangle rectangle);

    public abstract void revalidate();

    public abstract boolean isValidateRoot();

    public abstract boolean isOptimizedDrawingEnabled();

    public abstract void paintImmediately(int i, int j, int k, int l);

    public abstract void paintImmediately(java.awt.Rectangle rectangle);

    public abstract void setDoubleBuffered(boolean flag);

    public abstract javax.swing.JRootPane getRootPane();

    public abstract java.lang.String getName();

    public abstract void setName(java.lang.String s);

    public abstract java.awt.Container getParent();

    public abstract java.awt.Toolkit getToolkit();

    public abstract boolean isValid();

    public abstract boolean isVisible();

    public abstract boolean isShowing();

    public abstract boolean isEnabled();

    public abstract void setEnabled(boolean flag);

    public abstract void enable();

    public abstract void enable(boolean flag);

    public abstract void disable();

    public abstract boolean isDoubleBuffered();

    public abstract void setVisible(boolean flag);

    public abstract void show();

    public abstract void show(boolean flag);

    public abstract void hide();

    public abstract java.awt.Color getForeground();

    public abstract void setForeground(java.awt.Color color);

    public abstract java.awt.Color getBackground();

    public abstract void setBackground(java.awt.Color color);

    public abstract java.awt.Font getFont();

    public abstract void setFont(java.awt.Font font);

    public abstract java.util.Locale getLocale();

    public abstract void setLocale(java.util.Locale locale);

    public abstract java.awt.image.ColorModel getColorModel();

    public abstract java.awt.Point getLocation();

    public abstract java.awt.Point getLocationOnScreen();

    public abstract java.awt.Point location();

    public abstract void setLocation(int i, int j);

    public abstract void move(int i, int j);

    public abstract void setLocation(java.awt.Point point);

    public abstract java.awt.Dimension getSize();

    public abstract java.awt.Dimension size();

    public abstract void setSize(int i, int j);

    public abstract void resize(int i, int j);

    public abstract void setSize(java.awt.Dimension dimension);

    public abstract void resize(java.awt.Dimension dimension);

    public abstract java.awt.Rectangle getBounds();

    public abstract java.awt.Rectangle bounds();

    public abstract void setBounds(int i, int j, int k, int l);

    public abstract void reshape(int i, int j, int k, int l);

    public abstract void setBounds(java.awt.Rectangle rectangle);

    public abstract int getX();

    public abstract int getY();

    public abstract int getWidth();

    public abstract int getHeight();

    public abstract java.awt.Rectangle getBounds(java.awt.Rectangle rectangle);

    public abstract java.awt.Dimension getSize(java.awt.Dimension dimension);

    public abstract java.awt.Point getLocation(java.awt.Point point);

    public abstract boolean isOpaque();

    public abstract java.awt.Dimension getPreferredSize();

    public abstract java.awt.Dimension preferredSize();

    public abstract java.awt.Dimension getMinimumSize();

    public abstract java.awt.Dimension minimumSize();

    public abstract java.awt.Dimension getMaximumSize();

    public abstract float getAlignmentX();

    public abstract float getAlignmentY();

    public abstract void doLayout();

    public abstract void layout();

    public abstract void validate();

    public abstract void invalidate();

    public abstract java.awt.Graphics getGraphics();

    public abstract java.awt.FontMetrics getFontMetrics(java.awt.Font font);

    public abstract void setCursor(java.awt.Cursor cursor);

    public abstract java.awt.Cursor getCursor();

    public abstract void paint(java.awt.Graphics g);

    public abstract void update(java.awt.Graphics g);

    public abstract void paintAll(java.awt.Graphics g);

    public abstract void repaint();

    public abstract void repaint(long l);

    public abstract void repaint(int i, int j, int k, int l);

    public abstract void repaint(long l, int i, int j, int k, int i1);

    public abstract void print(java.awt.Graphics g);

    public abstract void printAll(java.awt.Graphics g);

    public abstract boolean imageUpdate(java.awt.Image image, int i, int j, int k, int l, int i1);

    public abstract java.awt.Image createImage(java.awt.image.ImageProducer imageproducer);

    public abstract java.awt.Image createImage(int i, int j);

    public abstract boolean prepareImage(java.awt.Image image, java.awt.image.ImageObserver imageobserver);

    public abstract boolean prepareImage(java.awt.Image image, int i, int j, java.awt.image.ImageObserver imageobserver);

    public abstract int checkImage(java.awt.Image image, java.awt.image.ImageObserver imageobserver);

    public abstract int checkImage(java.awt.Image image, int i, int j, java.awt.image.ImageObserver imageobserver);

    public abstract boolean contains(int i, int j);

    public abstract boolean inside(int i, int j);

    public abstract boolean contains(java.awt.Point point);

    public abstract java.awt.Component getComponentAt(int i, int j);

    public abstract java.awt.Component locate(int i, int j);

    public abstract java.awt.Component getComponentAt(java.awt.Point point);

    public abstract boolean gotFocus(java.awt.Event event, java.lang.Object obj);

    public abstract boolean lostFocus(java.awt.Event event, java.lang.Object obj);

    public abstract boolean isFocusable();

    public abstract void requestFocus();

    public abstract void transferFocus();

    public abstract void nextFocus();

    public abstract boolean hasFocus();

    public abstract void add(java.awt.PopupMenu popupmenu);

    public abstract void remove(java.awt.MenuComponent menucomponent);

    public abstract java.lang.String toString();

    public abstract void list();

    public abstract void list(java.io.PrintStream printstream);

    public abstract void list(java.io.PrintStream printstream, int i);

    public abstract void list(java.io.PrintWriter printwriter);

    public abstract void list(java.io.PrintWriter printwriter, int i);

    public abstract javax.accessibility.AccessibleContext getAccessibleContext();
}
