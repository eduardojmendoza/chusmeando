// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JScrollBar;

// Referenced classes of package diamondedge.vb:
//            VbControl

public class ScrollBar extends javax.swing.JScrollBar
    implements diamondedge.vb.VbControl
{

    public ScrollBar()
    {
        this(1);
    }

    public ScrollBar(int i)
    {
        super(i);
        setValues(0, getVisibleAmount(), 0, 32767 + getVisibleAmount());
        setUnitIncrement(1);
        setBlockIncrement(1);
        setBorder(javax.swing.BorderFactory.createLineBorder(java.awt.Color.gray));
    }

    public void setMinimum(int i)
    {
        super.setMinimum(i);
        adjustThumbSize();
    }

    public void setMaximum(int i)
    {
        super.setMaximum(i);
        adjustThumbSize();
        super.setMaximum(i + getVisibleAmount());
    }

    protected void adjustThumbSize()
    {
        int i = getMaximum() - getMinimum();
        int j = getOrientation() != 1 ? getWidth() : getHeight();
        if(j == 0)
            j = 100;
        int k = i / j;
        if(k < 1)
            k = 1;
        setVisibleAmount(k);
    }

    public java.lang.String getTag()
    {
        return a;
    }

    public void setTag(java.lang.String s)
    {
        a = s;
    }

    public int getTabOrder()
    {
        return _fldif;
    }

    public void setTabOrder(int i)
    {
        _fldif = i;
    }

    private java.lang.String a = null;
    private int _fldif = 0;
}
