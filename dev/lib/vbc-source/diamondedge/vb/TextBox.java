// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

// Referenced classes of package diamondedge.vb:
//            DataBound, TextBoxCtrl, VbControl, Screen, 
//            DataSource, DataBoundRouter, DataChangeEvent

public class TextBox extends javax.swing.JTextField
    implements javax.swing.event.DocumentListener, diamondedge.vb.DataBound, diamondedge.vb.TextBoxCtrl, diamondedge.vb.VbControl
{
    private class a extends javax.swing.text.PlainDocument
    {

        public void insertString(int i, java.lang.String s, javax.swing.text.AttributeSet attributeset)
            throws javax.swing.text.BadLocationException
        {
            if(s == null)
                return;
            if(w > 0)
            {
                if((w & 1) > 0)
                    s = s.toUpperCase();
                else
                if((w & 2) > 0)
                    s = s.toLowerCase();
                if((w & 0xc) > 0)
                {
                    java.lang.StringBuffer stringbuffer = new StringBuffer(s);
                    for(int j = s.length() - 1; j >= 0; j--)
                    {
                        char c = stringbuffer.charAt(j);
                        if((w & 0xc) == 12)
                        {
                            if(!java.lang.Character.isLetterOrDigit(c))
                                stringbuffer.deleteCharAt(j);
                            continue;
                        }
                        if((w & 8) > 0)
                        {
                            if(!java.lang.Character.isDigit(c))
                                stringbuffer.deleteCharAt(j);
                            continue;
                        }
                        if(!java.lang.Character.isLetter(c))
                            stringbuffer.deleteCharAt(j);
                    }

                    s = stringbuffer.toString();
                }
            }
            if(v == 0 || getLength() + s.length() <= v)
                super.insertString(i, s, attributeset);
            else
            if(getLength() < v)
            {
                s = s.substring(0, v - getLength());
                super.insertString(i, s, attributeset);
            } else
            {
                java.awt.Toolkit.getDefaultToolkit().beep();
            }
        }

        private a()
        {
        }

    }


    public TextBox()
    {
        v = 0;
        w = 0;
        u = null;
        setFont(diamondedge.vb.Screen.getDefaultFont());
        setCursor(java.awt.Cursor.getPredefinedCursor(2));
        setHorizontalAlignment(2);
        getDocument().addDocumentListener(this);
    }

    public java.lang.String getTag()
    {
        return B;
    }

    public void setTag(java.lang.String s)
    {
        B = s;
    }

    public int getTabOrder()
    {
        return z;
    }

    public void setTabOrder(int i)
    {
        z = i;
    }

    public java.lang.String toString()
    {
        return getText();
    }

    public void setText(java.lang.String s)
    {
        super.setText(s);
        setCaretPosition(0);
    }

    public int getMaxLength()
    {
        return v;
    }

    public void setMaxLength(int i)
    {
        v = i;
    }

    public int getContentStyle()
    {
        return w;
    }

    public void setContentStyle(int i)
    {
        w = i;
    }

    protected javax.swing.text.Document createDefaultModel()
    {
        return new a();
    }

    protected void fireDocumentChanged(javax.swing.event.DocumentEvent documentevent)
    {
        java.lang.Object aobj[] = listenerList.getListenerList();
        java.awt.event.TextEvent textevent = null;
        for(int i = aobj.length - 2; i >= 0; i -= 2)
        {
            if(aobj[i] != (java.awt.event.TextListener.class))
                continue;
            if(textevent == null)
                textevent = new TextEvent(this, 900);
            ((java.awt.event.TextListener)aobj[i + 1]).textValueChanged(textevent);
        }

    }

    public void changedUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void insertUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void removeUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void addTextListener(java.awt.event.TextListener textlistener)
    {
        listenerList.add(java.awt.event.TextListener.class, textlistener);
    }

    public void removeTextListener(java.awt.event.TextListener textlistener)
    {
        listenerList.remove(java.awt.event.TextListener.class, textlistener);
    }

    public java.lang.String getDataField()
    {
        return A;
    }

    public void setDataField(java.lang.String s)
    {
        A = s;
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return u;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        if(u != null)
        {
            u.removeDataBound(this);
            removeTextListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        if(datasource != null)
        {
            datasource.addDataBound(this);
            addTextListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        u = datasource;
    }

    public diamondedge.util.Variant getDataValue()
    {
        return new Variant(getText());
    }

    public void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent)
    {
        setText(datachangeevent.getValue() != null ? datachangeevent.getValue().toString() : "");
    }

    private java.lang.String B = null;
    private int z = 0;
    private int v = 0;
    private int w = 0;
    public static final int TEXT_ANY_CHARACTER = 0;
    public static final int TEXT_UPPER_CASE = 1;
    public static final int TEXT_LOWER_CASE = 2;
    public static final int TEXT_ALPHA = 4;
    public static final int TEXT_NUMERIC = 8;
    public static final int TEXT_ALPHA_NUMERIC = 12;
    private java.lang.String A = null;
    private diamondedge.vb.DataSource u = null;


}
