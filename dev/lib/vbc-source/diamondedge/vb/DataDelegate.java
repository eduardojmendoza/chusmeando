// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.ado.Recordset;

// Referenced classes of package diamondedge.vb:
//            DataSource, DataBound

public class DataDelegate
    implements diamondedge.vb.DataSource
{

    public DataDelegate()
    {
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return data;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        data = datasource;
    }

    public void addDataBound(diamondedge.vb.DataBound databound)
    {
        if(data != null)
            data.addDataBound(databound);
    }

    public void removeDataBound(diamondedge.vb.DataBound databound)
    {
        if(data != null)
            data.removeDataBound(databound);
    }

    public diamondedge.ado.Recordset getRecordset()
    {
        if(data != null)
            return data.getRecordset();
        else
            return null;
    }

    protected diamondedge.vb.DataSource data = null;
}
