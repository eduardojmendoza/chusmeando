// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.applet.Applet;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Paint;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.io.PrintStream;
import java.util.Enumeration;

// Referenced classes of package diamondedge.vb:
//            Form, Application

public class Screen
{

    public Screen()
    {
    }

    public static final int convertTwips(int i)
    {
        int j = (int)((double)i * pixelsPerTwip);
        return j;
    }

    public static final int getTwipsPerPixelX()
    {
        return twipsPerPixel;
    }

    public static final int getTwipsPerPixelY()
    {
        return twipsPerPixel;
    }

    public static java.awt.Font getDefaultFont()
    {
        return defaultFont;
    }

    public static void setDefaultFont(java.awt.Font font)
    {
        defaultFont = font;
    }

    public static final void setCursor(java.awt.Cursor cursor)
    {
        if(diamondedge.vb.Application.getApplication() == null)
            return;
        java.util.Enumeration enumeration = diamondedge.vb.Application.getApplication().getForms();
        do
        {
            if(!enumeration.hasMoreElements())
                break;
            diamondedge.vb.Form form = (diamondedge.vb.Form)enumeration.nextElement();
            if(form != null)
                form.setCursor(cursor);
        } while(true);
    }

    public static final java.awt.Cursor getCursor()
    {
        if(activeForm != null)
            return activeForm.getCursor();
        else
            return null;
    }

    public static final diamondedge.vb.Form getActiveForm()
    {
        return activeForm;
    }

    public static final void setActiveForm(diamondedge.vb.Form form)
    {
        if(form != activeForm)
        {
            activeForm = form;
            if(activeForm != null)
                activeForm.Form_Deactivate();
            if(form != null)
            {
                form.Form_Activate();
                form.requestFocus();
            }
        }
    }

    public static final java.awt.Component getActiveControl()
    {
        if(activeForm != null)
            return activeForm.getActiveControl();
        else
            return null;
    }

    public static final int getWidth()
    {
        return java.awt.Toolkit.getDefaultToolkit().getScreenSize().width;
    }

    public static final int getHeight()
    {
        return java.awt.Toolkit.getDefaultToolkit().getScreenSize().height;
    }

    public static int vbColor(java.awt.Color color)
    {
        if(color == null)
            return 0;
        else
            return color.getRed() | color.getGreen() << 8 | color.getBlue() << 16;
    }

    public static int vbColor(java.awt.Paint paint)
    {
        if(paint == null || !(paint instanceof java.awt.Color))
        {
            return 0;
        } else
        {
            java.awt.Color color = (java.awt.Color)paint;
            return color.getRed() | color.getGreen() << 8 | color.getBlue() << 16;
        }
    }

    public static java.awt.Color javaColor(int i)
    {
        if((i & 0xff000000) == 0x80000000)
        {
            switch(i)
            {
            case -2147483648: 
                return java.awt.SystemColor.scrollbar;

            case -2147483647: 
                return java.awt.SystemColor.desktop;

            case -2147483646: 
                return java.awt.SystemColor.activeCaption;

            case -2147483645: 
                return java.awt.SystemColor.inactiveCaption;

            case -2147483644: 
                return java.awt.SystemColor.menu;

            case -2147483643: 
                return java.awt.SystemColor.window;

            case -2147483642: 
                return java.awt.SystemColor.windowBorder;

            case -2147483641: 
                return java.awt.SystemColor.menuText;

            case -2147483640: 
                return java.awt.SystemColor.windowText;

            case -2147483639: 
                return java.awt.SystemColor.activeCaptionText;

            case -2147483638: 
                return java.awt.SystemColor.activeCaptionBorder;

            case -2147483637: 
                return java.awt.SystemColor.inactiveCaptionBorder;

            case -2147483636: 
                return java.awt.SystemColor.menu;

            case -2147483635: 
                return java.awt.SystemColor.textHighlight;

            case -2147483634: 
                return java.awt.SystemColor.textHighlightText;

            case -2147483633: 
                return java.awt.SystemColor.menu;

            case -2147483632: 
                return java.awt.SystemColor.controlShadow;

            case -2147483631: 
                return java.awt.SystemColor.textInactiveText;

            case -2147483630: 
                return java.awt.SystemColor.controlText;

            case -2147483629: 
                return java.awt.SystemColor.inactiveCaptionText;

            case -2147483628: 
                return java.awt.SystemColor.controlHighlight;

            case -2147483627: 
                return java.awt.SystemColor.controlDkShadow;

            case -2147483626: 
                return java.awt.SystemColor.controlLtHighlight;

            case -2147483625: 
                return java.awt.SystemColor.infoText;

            case -2147483624: 
                return java.awt.SystemColor.info;
            }
            return java.awt.Color.gray;
        } else
        {
            return new Color((i & 0xff) << 16 | i & 0xff00 | (i & 0xff0000) >> 16);
        }
    }

    public static java.awt.Image loadImage(java.lang.String s)
    {
        java.awt.Image image;
        java.lang.Object obj;
        if(diamondedge.vb.Application.getApplication() != null && diamondedge.vb.Application.getApplication().isAppletRunning())
        {
            java.applet.Applet applet = diamondedge.vb.Application.getApplication().getApplet();
            image = applet.getImage(applet.getCodeBase(), s);
            obj = applet;
        } else
        {
            java.net.URL url = java.lang.ClassLoader.getSystemResource(s);
            if(url == null)
                return null;
            image = java.awt.Toolkit.getDefaultToolkit().getImage(url);
            obj = (java.awt.Component)diamondedge.vb.Screen.getActiveForm();
        }
        return diamondedge.vb.Screen.a(image, ((java.awt.Component) (obj)), s);
    }

    private static java.awt.Image a(java.awt.Image image, java.awt.Component component, java.lang.Object obj)
    {
        if(component == null)
            component = new Canvas();
        if(image != null)
        {
            java.awt.MediaTracker mediatracker = new MediaTracker(component);
            mediatracker.addImage(image, 0);
            try
            {
                mediatracker.waitForID(0, 5000L);
            }
            catch(java.lang.InterruptedException interruptedexception)
            {
                java.lang.System.out.println("INTERRUPTED while loading image " + obj);
            }
            mediatracker.removeImage(image, 0);
        }
        return image;
    }

    public static java.awt.Image loadImage(diamondedge.vb.Form form, java.lang.String s)
    {
        if(form == null)
            return diamondedge.vb.Screen.loadImage(s);
        java.awt.Component component = (java.awt.Component)form;
        java.net.URL url = component.getClass().getResource(s);
        if(url == null)
            url = java.lang.ClassLoader.getSystemResource(s);
        if(url == null)
        {
            return null;
        } else
        {
            java.awt.Image image = java.awt.Toolkit.getDefaultToolkit().getImage(url);
            return diamondedge.vb.Screen.a(image, component, url);
        }
    }

    public static int screenResolution = 0;
    public static double pixelsPerTwip = 0;
    public static double pixelsPerPoint = 0;
    public static double pixelsPerCharacterX = 0;
    public static double pixelsPerCharacterY = 0;
    public static double pixelsPerInch = 0;
    public static double pixelsPerMillimeter = 0;
    public static double pixelsPerCentimeter = 0;
    public static int twipsPerPixel = 0;
    protected static diamondedge.vb.Form activeForm = null;
    protected static java.awt.Font defaultFont = new Font("Dialog", 0, 11);

    static 
    {
        screenResolution = 120;
        screenResolution = java.awt.Toolkit.getDefaultToolkit().getScreenResolution();
        pixelsPerTwip = (double)screenResolution / 1440D;
        pixelsPerPoint = (double)screenResolution / 72D;
        pixelsPerCharacterX = pixelsPerTwip * 120D;
        pixelsPerCharacterY = pixelsPerTwip * 240D;
        pixelsPerInch = screenResolution;
        pixelsPerMillimeter = (double)screenResolution / 25.399999999999999D;
        pixelsPerCentimeter = (double)screenResolution / 2.54D;
        twipsPerPixel = 1440 / screenResolution;
    }
}
