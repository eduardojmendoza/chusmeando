// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Color;
import java.awt.Paint;

// Referenced classes of package diamondedge.vb:
//            Screen

public class VbColor
{

    public VbColor()
    {
    }

    public static java.awt.Color QBColor(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return java.awt.Color.black;

        case 1: // '\001'
            return new Color(0, 0, 128);

        case 2: // '\002'
            return new Color(0, 128, 0);

        case 3: // '\003'
            return new Color(0, 128, 128);

        case 4: // '\004'
            return new Color(128, 0, 0);

        case 5: // '\005'
            return new Color(128, 0, 128);

        case 6: // '\006'
            return new Color(128, 128, 0);

        case 7: // '\007'
            return java.awt.Color.lightGray;

        case 8: // '\b'
            return java.awt.Color.gray;

        case 9: // '\t'
            return java.awt.Color.blue;

        case 10: // '\n'
            return java.awt.Color.green;

        case 11: // '\013'
            return java.awt.Color.cyan;

        case 12: // '\f'
            return java.awt.Color.red;

        case 13: // '\r'
            return java.awt.Color.magenta;

        case 14: // '\016'
            return java.awt.Color.yellow;

        case 15: // '\017'
            return java.awt.Color.white;
        }
        return java.awt.Color.black;
    }

    public static int RGB(int i, int j, int k)
    {
        return i | j << 8 | k << 16;
    }

    public static int vbColor(java.awt.Color color)
    {
        return diamondedge.vb.Screen.vbColor(color);
    }

    public static int vbColor(java.awt.Paint paint)
    {
        return diamondedge.vb.Screen.vbColor(paint);
    }

    public static java.awt.Color javaColor(int i)
    {
        return diamondedge.vb.Screen.javaColor(i);
    }

    public static final java.awt.Color navy = new Color(0, 0, 128);

}
