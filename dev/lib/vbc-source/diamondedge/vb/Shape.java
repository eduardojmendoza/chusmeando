// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.SystemColor;

// Referenced classes of package diamondedge.vb:
//            VbGraphics

public class Shape extends java.awt.Component
{

    public Shape()
    {
        _fldfor = 0;
        _fldbyte = java.awt.Color.white;
        _flddo = 1;
        _fldint = 0;
        _fldtry = java.awt.Color.black;
        _fldif = 1;
        _fldnew = true;
        setBackground(java.awt.SystemColor.window);
    }

    public void paint(java.awt.Graphics g)
    {
        java.awt.Dimension dimension = getSize();
        int i = 0;
        int j = 0;
        int k = dimension.width - 1;
        int l = dimension.height - 1;
        int i1 = _fldif;
        byte byte0 = (byte)(_fldint != 0 ? 0 : 1);
        java.awt.Color color = getBackground();
        if(byte0 == 1 && _flddo == 0)
        {
            byte0 = _flddo;
            color = _fldbyte;
        }
        if(i1 == 1)
            diamondedge.vb.VbGraphics.drawShape(g, i, j, k, l, _fldfor, color, byte0, _fldtry, _fldnew);
        else
        if(byte0 == 1)
        {
            for(int j1 = 0; j1 < i1; j1++)
            {
                diamondedge.vb.VbGraphics.drawShape(g, i, j, k, l, _fldfor, color, byte0, _fldtry, _fldnew);
                k -= 2;
                l -= 2;
                i++;
                j++;
            }

        } else
        {
            diamondedge.vb.VbGraphics.drawShape(g, i, j, k + 1, l + 1, _fldfor, _fldtry, (byte)0, _fldtry, false);
            i1--;
            diamondedge.vb.VbGraphics.drawShape(g, i + i1, j + i1, k - i1 * 2, l - i1 * 2, _fldfor, color, byte0, _fldtry, false);
        }
    }

    public void setBackground(java.awt.Color color)
    {
        super.setBackground(color);
        repaint();
    }

    public java.awt.Color getBorderColor()
    {
        return _fldtry;
    }

    public void setBorderColor(java.awt.Color color)
    {
        _fldtry = color;
        repaint();
    }

    public int getBorderWidth()
    {
        return _fldif;
    }

    public void setBorderWidth(int i)
    {
        _fldif = (byte)i;
        repaint();
    }

    public boolean isBordered()
    {
        return _fldnew;
    }

    public void setBordered(boolean flag)
    {
        _fldnew = flag;
        repaint();
    }

    public java.awt.Color getFillColor()
    {
        return _fldbyte;
    }

    public void setFillColor(java.awt.Color color)
    {
        _fldbyte = color;
        repaint();
    }

    public int getFillStyle()
    {
        return _flddo;
    }

    public void setFillStyle(int i)
    {
        _flddo = (byte)i;
        repaint();
    }

    public int getBackStyle()
    {
        return _fldint;
    }

    public void setBackStyle(int i)
    {
        _fldint = (byte)i;
        repaint();
    }

    public int getShape()
    {
        return _fldfor;
    }

    public void setShape(int i)
    {
        _fldfor = (byte)i;
        repaint();
    }

    public java.lang.String getTag()
    {
        return a;
    }

    public void setTag(java.lang.String s)
    {
        a = s;
    }

    private java.lang.String a = null;
    private byte _fldfor = 0;
    private java.awt.Color _fldbyte = null;
    private byte _flddo = 0;
    private byte _fldint = 0;
    private java.awt.Color _fldtry = null;
    private byte _fldif = 0;
    private boolean _fldnew = false;
}
