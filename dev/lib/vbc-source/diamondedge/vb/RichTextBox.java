// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import javax.swing.JTextPane;
import javax.swing.JViewport;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.text.AttributeSet;
import javax.swing.text.EditorKit;
import javax.swing.text.Element;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.rtf.RTFEditorKit;

// Referenced classes of package diamondedge.vb:
//            ScrolledComponent, DataBound, VbControl, Screen, 
//            DataSource, DataBoundRouter, DataChangeEvent

public class RichTextBox extends diamondedge.vb.ScrolledComponent
    implements javax.swing.event.DocumentListener, diamondedge.vb.DataBound, diamondedge.vb.VbControl
{

    public RichTextBox()
    {
        super(new JTextPane());
        T = null;
        textpane = (javax.swing.JTextPane)getViewport().getView();
        textpane.setEditorKit(new RTFEditorKit());
        textpane.setFont(diamondedge.vb.Screen.getDefaultFont());
        textpane.setCursor(java.awt.Cursor.getPredefinedCursor(2));
        doc = textpane.getStyledDocument();
        doc.addDocumentListener(this);
        setScrollBarPolicy(0);
    }

    public javax.swing.JTextPane control()
    {
        return textpane;
    }

    public java.lang.String getFileName()
    {
        return fileName;
    }

    public void setFileName(java.lang.String s)
    {
        fileName = s;
        try
        {
            loadFile(fileName);
        }
        catch(java.lang.Exception exception) { }
    }

    public boolean isEditable()
    {
        return textpane.isEditable();
    }

    public void setEditable(boolean flag)
    {
        textpane.setEditable(flag);
    }

    public int getSelAlignment()
    {
        int i = javax.swing.text.StyleConstants.getAlignment(getAttributeSet());
        switch(i)
        {
        case 0: // '\0'
            return 0;

        case 1: // '\001'
            return 2;

        case 2: // '\002'
            return 1;
        }
        return 0;
    }

    public void setSelAlignment(int i)
    {
        javax.swing.text.SimpleAttributeSet simpleattributeset = new SimpleAttributeSet();
        switch(i)
        {
        case 0: // '\0'
            javax.swing.text.StyleConstants.setAlignment(simpleattributeset, 0);
            break;

        case 1: // '\001'
            javax.swing.text.StyleConstants.setAlignment(simpleattributeset, 2);
            break;

        case 2: // '\002'
            javax.swing.text.StyleConstants.setAlignment(simpleattributeset, 1);
            break;

        default:
            javax.swing.text.StyleConstants.setAlignment(simpleattributeset, 0);
            break;
        }
        setAttributeSet(simpleattributeset);
    }

    public boolean isSelBold()
    {
        return javax.swing.text.StyleConstants.isBold(getAttributeSet());
    }

    public void setSelBold(boolean flag)
    {
        javax.swing.text.SimpleAttributeSet simpleattributeset = new SimpleAttributeSet();
        javax.swing.text.StyleConstants.setBold(simpleattributeset, flag);
        setAttributeSet(simpleattributeset);
    }

    public java.awt.Color getSelColor()
    {
        return javax.swing.text.StyleConstants.getForeground(getAttributeSet());
    }

    public void setSelColor(java.awt.Color color)
    {
        javax.swing.text.SimpleAttributeSet simpleattributeset = new SimpleAttributeSet();
        javax.swing.text.StyleConstants.setForeground(simpleattributeset, color);
        setAttributeSet(simpleattributeset);
    }

    public java.lang.String getSelFontName()
    {
        return javax.swing.text.StyleConstants.getFontFamily(getAttributeSet());
    }

    public void setSelFontName(java.lang.String s)
    {
        javax.swing.text.SimpleAttributeSet simpleattributeset = new SimpleAttributeSet();
        javax.swing.text.StyleConstants.setFontFamily(simpleattributeset, s);
        setAttributeSet(simpleattributeset);
    }

    public int getSelFontSize()
    {
        return javax.swing.text.StyleConstants.getFontSize(getAttributeSet());
    }

    public void setSelFontSize(int i)
    {
        javax.swing.text.SimpleAttributeSet simpleattributeset = new SimpleAttributeSet();
        javax.swing.text.StyleConstants.setFontSize(simpleattributeset, i);
        setAttributeSet(simpleattributeset);
    }

    public float getSelHangingIndent()
    {
        return javax.swing.text.StyleConstants.getFirstLineIndent(getAttributeSet());
    }

    public void setSelHangingIndent(float f)
    {
        javax.swing.text.SimpleAttributeSet simpleattributeset = new SimpleAttributeSet();
        javax.swing.text.StyleConstants.setFirstLineIndent(simpleattributeset, f);
        setAttributeSet(simpleattributeset);
    }

    public float getSelIndent()
    {
        return javax.swing.text.StyleConstants.getLeftIndent(getAttributeSet());
    }

    public void setSelIndent(float f)
    {
        javax.swing.text.SimpleAttributeSet simpleattributeset = new SimpleAttributeSet();
        javax.swing.text.StyleConstants.setLeftIndent(simpleattributeset, f);
        setAttributeSet(simpleattributeset);
    }

    public boolean isSelItalic()
    {
        return javax.swing.text.StyleConstants.isItalic(getAttributeSet());
    }

    public void setSelItalic(boolean flag)
    {
        javax.swing.text.SimpleAttributeSet simpleattributeset = new SimpleAttributeSet();
        javax.swing.text.StyleConstants.setItalic(simpleattributeset, flag);
        setAttributeSet(simpleattributeset);
    }

    public int getSelLength()
    {
        return getSelectionEnd() - getSelectionStart();
    }

    public void setSelLength(int i)
    {
        setSelectionEnd(getSelectionStart() + i);
    }

    public float getSelRightIndent()
    {
        return javax.swing.text.StyleConstants.getRightIndent(getAttributeSet());
    }

    public void setSelRightIndent(float f)
    {
        javax.swing.text.SimpleAttributeSet simpleattributeset = new SimpleAttributeSet();
        javax.swing.text.StyleConstants.setRightIndent(simpleattributeset, f);
        setAttributeSet(simpleattributeset);
    }

    public java.lang.String getSelRTF()
        throws java.lang.Exception
    {
        int i = getSelLength();
        java.io.ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream(i);
        textpane.getEditorKit().write(bytearrayoutputstream, doc, getSelectionStart(), i);
        bytearrayoutputstream.close();
        return bytearrayoutputstream.toString();
    }

    public void setSelRTF(java.lang.String s)
    {
        textpane.replaceSelection(s);
    }

    public int getSelectionStart()
    {
        return textpane.getSelectionStart();
    }

    public void setSelectionStart(int i)
    {
        textpane.setSelectionStart(i);
    }

    public int getSelectionEnd()
    {
        return textpane.getSelectionEnd();
    }

    public void setSelectionEnd(int i)
    {
        textpane.setSelectionEnd(i);
    }

    public boolean isSelStrikeThru()
    {
        return javax.swing.text.StyleConstants.isStrikeThrough(getAttributeSet());
    }

    public void setSelStrikeThru(boolean flag)
    {
        javax.swing.text.SimpleAttributeSet simpleattributeset = new SimpleAttributeSet();
        javax.swing.text.StyleConstants.setStrikeThrough(simpleattributeset, flag);
        setAttributeSet(simpleattributeset);
    }

    public java.lang.String getSelectedText()
    {
        return textpane.getSelectedText();
    }

    public void replaceSelection(java.lang.String s)
    {
        textpane.replaceSelection(s);
    }

    public boolean isSelUnderline()
    {
        return javax.swing.text.StyleConstants.isUnderline(getAttributeSet());
    }

    public void setSelUnderline(boolean flag)
    {
        javax.swing.text.SimpleAttributeSet simpleattributeset = new SimpleAttributeSet();
        javax.swing.text.StyleConstants.setUnderline(simpleattributeset, flag);
        setAttributeSet(simpleattributeset);
    }

    public java.lang.String getText()
    {
        return doc.getText(0, doc.getLength());
        java.lang.Exception exception;
        exception;
        java.lang.System.out.println("RichTextBox.getText: " + exception);
        return "";
    }

    public void setText(java.lang.String s)
    {
        textpane.selectAll();
        textpane.replaceSelection(s);
    }

    public java.lang.String getTextRTF()
        throws java.lang.Exception
    {
        int i = doc.getLength();
        java.io.ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream(i >= 40 ? i * 2 : 80 + i);
        textpane.getEditorKit().write(bytearrayoutputstream, doc, 0, i);
        bytearrayoutputstream.close();
        return bytearrayoutputstream.toString();
    }

    public void setTextRTF(java.lang.String s)
    {
        textpane.setText(s);
    }

    public void loadFile(java.lang.String s)
        throws java.io.FileNotFoundException, java.io.IOException
    {
        doc.removeDocumentListener(this);
        fileName = s;
        java.io.FileInputStream fileinputstream = new FileInputStream(s);
        textpane.read(fileinputstream, s);
        doc = textpane.getStyledDocument();
    }

    public void saveFile(java.lang.String s)
        throws java.lang.Exception
    {
        fileName = s;
        java.io.FileOutputStream fileoutputstream = new FileOutputStream(s);
        textpane.getEditorKit().write(fileoutputstream, doc, 0, doc.getLength());
        fileoutputstream.close();
    }

    public void addCaretListener(javax.swing.event.CaretListener caretlistener)
    {
        textpane.addCaretListener(caretlistener);
    }

    public void removeCaretListener(javax.swing.event.CaretListener caretlistener)
    {
        textpane.removeCaretListener(caretlistener);
    }

    public javax.swing.text.AttributeSet getAttributeSet()
    {
        return doc.getCharacterElement(textpane.getCaretPosition()).getAttributes();
    }

    protected void setAttributeSet(javax.swing.text.AttributeSet attributeset)
    {
        setAttributeSet(attributeset, false);
    }

    protected void setAttributeSet(javax.swing.text.AttributeSet attributeset, boolean flag)
    {
        int i = textpane.getSelectionStart();
        int j = textpane.getSelectionEnd();
        if(flag)
            doc.setParagraphAttributes(i, j - i, attributeset, false);
        else
        if(i != j)
        {
            doc.setCharacterAttributes(i, j - i, attributeset, false);
        } else
        {
            javax.swing.text.MutableAttributeSet mutableattributeset = ((javax.swing.text.StyledEditorKit)textpane.getEditorKit()).getInputAttributes();
            mutableattributeset.addAttributes(attributeset);
        }
    }

    public void setSelection(int i, int j, boolean flag)
    {
        if(flag)
        {
            textpane.setCaretPosition(j);
            textpane.moveCaretPosition(i);
        } else
        {
            textpane.select(i, j);
        }
        selStart = textpane.getSelectionStart();
        selFinish = textpane.getSelectionEnd();
    }

    public int getTabOrder()
    {
        return U;
    }

    public void setTabOrder(int i)
    {
        U = i;
    }

    protected void fireDocumentChanged(javax.swing.event.DocumentEvent documentevent)
    {
        java.lang.Object aobj[] = listenerList.getListenerList();
        java.awt.event.TextEvent textevent = null;
        for(int i = aobj.length - 2; i >= 0; i -= 2)
        {
            if(aobj[i] != (java.awt.event.TextListener.class))
                continue;
            if(textevent == null)
                textevent = new TextEvent(this, 900);
            ((java.awt.event.TextListener)aobj[i + 1]).textValueChanged(textevent);
        }

    }

    public void changedUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void insertUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void removeUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void addTextListener(java.awt.event.TextListener textlistener)
    {
        listenerList.add(java.awt.event.TextListener.class, textlistener);
    }

    public void removeTextListener(java.awt.event.TextListener textlistener)
    {
        listenerList.remove(java.awt.event.TextListener.class, textlistener);
    }

    public java.lang.String getDataField()
    {
        return V;
    }

    public void setDataField(java.lang.String s)
    {
        V = s;
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return T;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        if(T != null)
        {
            T.removeDataBound(this);
            removeTextListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        if(datasource != null)
        {
            datasource.addDataBound(this);
            addTextListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        T = datasource;
    }

    public diamondedge.util.Variant getDataValue()
    {
        return new Variant(getText());
    }

    public void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent)
    {
        setText(datachangeevent.getValue() != null ? datachangeevent.getValue().toString() : "");
    }

    private int U = 0;
    protected javax.swing.JTextPane textpane = null;
    protected javax.swing.text.StyledDocument doc = null;
    protected int selStart = 0;
    protected int selFinish = 0;
    protected java.lang.String fileName = null;
    private java.lang.String V = null;
    private diamondedge.vb.DataSource T = null;
    public static final int ALIGN_LEFT = 0;
    public static final int ALIGN_RIGHT = 1;
    public static final int ALIGN_CENTER = 2;
}
