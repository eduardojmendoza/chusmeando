// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.ado.Field;
import diamondedge.ado.Fields;
import diamondedge.ado.Recordset;
import diamondedge.swing.DsComboBox;
import diamondedge.swing.DsListItem;
import diamondedge.util.DateTime;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import java.io.PrintStream;

// Referenced classes of package diamondedge.vb:
//            DataBound, VbControl, Screen, DataSource, 
//            DataBoundRouter, DataChangeEvent

public class MultiColumnComboBox extends diamondedge.swing.DsComboBox
    implements diamondedge.vb.DataBound, diamondedge.vb.VbControl
{

    public MultiColumnComboBox()
    {
        a6 = false;
        a0 = null;
        a4 = null;
        a1 = null;
        a2 = false;
        setFont(diamondedge.vb.Screen.getDefaultFont());
        setEditable(true);
    }

    public java.lang.String getColumnWidths()
    {
        return a0;
    }

    public void setColumnWidths(java.lang.String s)
    {
        a0 = s;
        _mthtry();
    }

    private void _mthtry()
    {
        if(a0 == null || a0.length() == 0)
        {
            if(getListWidth() > 0)
            {
                int i = getListWidth() / getColumnCount();
                for(int k = 0; k < getColumnCount(); k++)
                    setColumnWidth(k, i);

            }
        } else
        {
            int j = 0;
            java.lang.String as[] = diamondedge.util.Strings.split(a0, ";", -1);
            for(int l = 0; l < as.length; l++)
            {
                int i1 = diamondedge.vb.MultiColumnComboBox.a(as[l], false);
                if(i1 < 0 || as[l].equals(""))
                    i1 = 96;
                if(j < getColumnCount())
                    setColumnWidth(j, i1);
                j++;
            }

        }
    }

    public java.lang.Object getValueAt(int i, int j)
    {
        if(i < 0)
            i = getSelectedIndex();
        return super.getValueAt(i, j);
    }

    public java.lang.String getTextAt(int i, int j)
    {
        if(i < 0)
            i = getSelectedIndex();
        return super.getTextAt(i, j);
    }

    public void setIgnoreEvents(boolean flag)
    {
        a6 = flag;
    }

    protected void fireActionEvent()
    {
        if(!a6)
            super.fireActionEvent();
    }

    public java.lang.String getTag()
    {
        return aZ;
    }

    public void setTag(java.lang.String s)
    {
        aZ = s;
    }

    public int getTabOrder()
    {
        return a3;
    }

    public void setTabOrder(int i)
    {
        a3 = i;
    }

    public diamondedge.vb.DataSource getRowSource()
    {
        return a1;
    }

    public void setRowSource(diamondedge.vb.DataSource datasource)
    {
        if(a1 != null)
            a1.removeDataBound(this);
        if(datasource != null)
        {
            datasource.addDataBound(this);
            a2 = false;
        }
        a1 = datasource;
    }

    public java.lang.String getDataField()
    {
        return a5;
    }

    public void setDataField(java.lang.String s)
    {
        a5 = s;
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return a4;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        if(a4 != null)
        {
            a4.removeDataBound(this);
            removeItemListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        if(datasource != null)
        {
            datasource.addDataBound(this);
            addItemListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        a4 = datasource;
    }

    public diamondedge.util.Variant getDataValue()
    {
        return new Variant(getValue());
    }

    public void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent)
    {
        diamondedge.util.Variant variant = datachangeevent.getValue();
        if(a1 != null)
        {
            if(a4 != null && datachangeevent.getRecordset() == a4.getRecordset())
                setValue(variant);
            else
            if(datachangeevent.getChangeType() == 7 || datachangeevent.getChangeType() == 1)
                _mthbyte();
        } else
        {
            setValue(variant);
        }
    }

    private void _mthbyte()
    {
        if(!a2 && a1 != null)
        {
            diamondedge.ado.Recordset recordset = a1.getRecordset();
            _mthif(recordset);
        }
    }

    private void _mthif(diamondedge.ado.Recordset recordset)
    {
        clear();
        if(recordset == (diamondedge.ado.Recordset)null)
            return;
        int i;
        int j;
        int k;
        i = recordset.getFields().getCount();
        setColumnCount(i);
        Object obj = null;
        j = recordset.getAbsolutePosition();
        k = recordset.getRecordCount(true);
        recordset.moveFirst();
        if(k == 0)
            return;
        try
        {
            for(int l = 1; l <= k; l++)
            {
                diamondedge.swing.DsListItem dslistitem = addItem(-1, null, _mthdo(recordset.getField(0).getValue()), null);
                for(int i1 = 1; i1 < i; i1++)
                    dslistitem.setCellValue(i1, _mthdo(recordset.getField(i1).getValue()));

                recordset.moveNext();
            }

            if(j != recordset.getAbsolutePosition())
                try
                {
                    recordset.setAbsolutePosition(j);
                }
                catch(java.lang.Exception exception1) { }
            a2 = true;
        }
        catch(java.lang.Exception exception)
        {
            java.lang.System.out.println("MultiColumnComboBox.fillList:" + exception);
        }
        return;
    }

    private java.lang.String _mthdo(diamondedge.util.Variant variant)
    {
        if(variant.isNull())
            return "";
        if(variant.isDate())
            return diamondedge.util.DateTime.format(variant.toDate());
        else
            return variant.toString().trim();
    }

    static int a(java.lang.String s, boolean flag)
    {
        int i = 0;
        double d = diamondedge.util.VB.val(s);
        if(diamondedge.util.Strings.find(1, s, "cm", true) > 0)
            i = (int)((d * 1440D) / 2.54D);
        else
        if(diamondedge.util.Strings.find(1, s, "in", true) > 0 || diamondedge.util.Strings.find(1, s, "\"", true) > 0)
        {
            i = (int)(d * 1440D);
        } else
        {
            if(flag)
                return (int)d;
            i = (int)d;
        }
        return i / 15;
    }

    public void removeAllItems()
    {
        diamondedge.vb.DataSource datasource = a4;
        a4 = null;
        a6 = true;
        super.removeAllItems();
        a6 = false;
        a4 = datasource;
    }

    private java.lang.String aZ = null;
    private int a3 = 0;
    private boolean a6 = false;
    private java.lang.String a0 = null;
    private java.lang.String a5 = null;
    private diamondedge.vb.DataSource a4 = null;
    private diamondedge.vb.DataSource a1 = null;
    private boolean a2 = false;
}
