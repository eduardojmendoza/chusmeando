// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.swing.DsTree;
import diamondedge.util.UI;
import java.awt.Container;
import javax.swing.JScrollPane;
import javax.swing.tree.TreeSelectionModel;

public class TreeView extends diamondedge.swing.DsTree
{

    public TreeView()
    {
        _fldfor = null;
        setRootVisible(false);
    }

    public boolean isScrollable()
    {
        return diamondedge.util.UI.getScrollPane(this) != null;
    }

    public void setScrollable(boolean flag)
    {
        if(flag == isScrollable())
            return;
        java.awt.Container container = getParent();
        javax.swing.JScrollPane jscrollpane = diamondedge.util.UI.getScrollPane(this);
        if(jscrollpane != null)
            container = jscrollpane.getParent();
        if(flag)
        {
            container.remove(this);
            jscrollpane = new JScrollPane(this);
            jscrollpane.setBounds(getBounds());
            container.add(jscrollpane);
            revalidate();
            jscrollpane.repaint();
        } else
        if(jscrollpane != null)
        {
            container.remove(jscrollpane);
            container.add(this);
            setBounds(jscrollpane.getBounds());
        }
    }

    public boolean isVisible()
    {
        javax.swing.JScrollPane jscrollpane = diamondedge.util.UI.getScrollPane(this);
        if(jscrollpane == null)
            return super.isVisible();
        else
            return jscrollpane.isVisible();
    }

    public void setVisible(boolean flag)
    {
        javax.swing.JScrollPane jscrollpane = diamondedge.util.UI.getScrollPane(this);
        if(jscrollpane == null)
            super.setVisible(flag);
        else
            jscrollpane.setVisible(flag);
    }

    public boolean isSingleSelection()
    {
        return getSelectionModel().getSelectionMode() == 1;
    }

    public void setSingleSelection(boolean flag)
    {
        javax.swing.JScrollPane jscrollpane = diamondedge.util.UI.getScrollPane(this);
        if(flag)
            getSelectionModel().setSelectionMode(1);
        else
            getSelectionModel().setSelectionMode(4);
    }

    public int getStyle()
    {
        int i = 0;
        if(isIconVisible())
            i |= 1;
        if(getShowsRootHandles())
            i |= 2;
        if(!getClientProperty("JTree.lineStyle").equals("None"))
            i |= 4;
        return i;
    }

    public void setStyle(int i)
    {
        if((i & 4) > 0)
            putClientProperty("JTree.lineStyle", "Angled");
        else
            putClientProperty("JTree.lineStyle", "None");
        if((i & 1) > 0)
            setIconVisible(true);
        else
            setIconVisible(false);
        if((i & 2) > 0)
            setShowsRootHandles(true);
        else
            setShowsRootHandles(false);
    }

    public java.lang.String getTag()
    {
        return _fldfor;
    }

    public void setTag(java.lang.String s)
    {
        _fldfor = s;
    }

    private java.lang.String _fldfor = null;
    private static final int _fldif = 1;
    private static final int a = 2;
    private static final int _flddo = 4;
    public static final int TextOnly = 0;
    public static final int PictureText = 1;
    public static final int PlusMinusText = 2;
    public static final int PlusPictureText = 3;
    public static final int TreelinesText = 4;
    public static final int TreelinesPictureText = 5;
    public static final int TreelinesPlusMinusText = 6;
    public static final int TreelinesPlusMinusPictureText = 7;
}
