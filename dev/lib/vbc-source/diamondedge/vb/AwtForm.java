// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.Point;
import java.awt.PrintJob;
import java.awt.Scrollbar;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.InputEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Properties;

// Referenced classes of package diamondedge.vb:
//            AwtPanel, Form, Screen, Application

public class AwtForm extends diamondedge.vb.AwtPanel
    implements java.awt.event.WindowListener, diamondedge.vb.Form
{

    public AwtForm()
    {
        initialized = false;
        unloaded = false;
        initInProgress = false;
        frame = null;
        dialog = null;
        window = null;
        startUpPosition = 0;
        prefSize = new Dimension(150, 250);
        aw = 88;
        setBackground(java.awt.SystemColor.control);
        setFont(diamondedge.vb.Screen.getDefaultFont());
    }

    protected void finalize()
    {
        Form_Terminate();
    }

    public void init()
    {
        initialize();
    }

    public void initialize()
    {
        if(initInProgress)
        {
            return;
        } else
        {
            initInProgress = true;
            Form_Initialize();
            Form_Load();
            unloaded = false;
            initInProgress = false;
            setVisible(true);
            return;
        }
    }

    public void unload()
    {
        unloaded = true;
        initialized = false;
    }

    public boolean isInitialized()
    {
        return initialized;
    }

    protected void initializeForm(int i)
    {
        aw = i;
        initialized = true;
    }

    public void dispose()
    {
        if(window != null)
            window.dispose();
        frame = null;
        dialog = null;
        window = null;
    }

    public void paint(java.awt.Graphics g)
    {
        super.paint(g);
        Form_Paint();
        if(aw != 88)
        {
            java.awt.Color color = getBackground();
            color = new Color(java.lang.Math.min((int)((double)color.getRed() * 1.1000000000000001D), 240), java.lang.Math.min((int)((double)color.getGreen() * 1.1000000000000001D), 240), java.lang.Math.min((int)((double)color.getBlue() * 1.1000000000000001D), 240));
            g.setColor(color);
            java.awt.Dimension dimension = getSize();
            int i = dimension.width;
            int j = dimension.height;
            int k = 50;
            int l = 12;
            java.awt.Font font = new Font("Serif", 2, 22);
            g.setFont(font);
            java.awt.FontMetrics fontmetrics = getFontMetrics(font);
            if(fontmetrics != null)
            {
                k = fontmetrics.stringWidth("Diamond Edge Demo") + 20;
                l = fontmetrics.getHeight() + 5;
            }
            for(int i1 = 5; i1 < i; i1 += k)
            {
                for(int j1 = 20; j1 < j; j1 += l)
                    g.drawString("Diamond Edge Demo", i1, j1);

            }

        }
    }

    public java.awt.Component getComponent(java.lang.String s)
    {
        int i = getComponentCount();
        for(int j = 0; j < i; j++)
        {
            java.awt.Component component = getComponent(j);
            if(component.getName().equals(s))
                return component;
        }

        return null;
    }

    public java.awt.Dimension getPreferredSize()
    {
        return prefSize;
    }

    public void setBounds(int i, int j, int k, int l)
    {
        super.setBounds(i, j, k, l);
        Form_Resize();
    }

    public void setFormSize(int i, int j)
    {
        prefSize = new Dimension(i, j);
        if(window != null)
            window.setSize(i, j);
    }

    public int getFormX()
    {
        if(window != null)
            return window.getLocation().x;
        else
            return locationX;
    }

    public int getFormY()
    {
        if(window != null)
            return window.getLocation().y;
        else
            return locationY;
    }

    public void setFormLocation(int i, int j)
    {
        locationX = i;
        locationY = j;
        if(window != null)
            window.setLocation(locationX, locationY);
    }

    public int getStartUpPosition()
    {
        return startUpPosition;
    }

    public void setStartUpPosition(int i)
    {
        startUpPosition = i;
    }

    public synchronized void setCursor(java.awt.Cursor cursor)
    {
        if(window != null)
            window.setCursor(cursor);
        else
            super.setCursor(cursor);
    }

    public void add(java.awt.Menu menu)
    {
        if(frame != null)
        {
            java.awt.MenuBar menubar = frame.getMenuBar();
            if(menubar == null)
            {
                menubar = new MenuBar();
                frame.setMenuBar(menubar);
            }
            menubar.add(menu);
        }
    }

    public java.lang.String getTag()
    {
        return ax;
    }

    public void setTag(java.lang.String s)
    {
        ax = s;
    }

    public java.lang.String getTitle()
    {
        if(frame != null)
            return frame.getTitle();
        if(dialog != null)
            return dialog.getTitle();
        else
            return "";
    }

    public void setTitle(java.lang.String s)
    {
        if(frame != null)
            frame.setTitle(s);
        else
        if(dialog != null)
            dialog.setTitle(s);
    }

    public java.awt.Image getIcon()
    {
        if(frame != null)
            return frame.getIconImage();
        else
            return null;
    }

    public void setIcon(java.lang.String s)
    {
        if(frame != null)
            frame.setIconImage(diamondedge.vb.Screen.loadImage(s));
    }

    public void setIcon(java.awt.Image image)
    {
        if(frame != null)
            frame.setIconImage(image);
    }

    public int getBorderStyle()
    {
        if(frame != null)
            return !frame.isResizable() ? 1 : 2;
        if(dialog != null)
            return !dialog.isResizable() ? 1 : 2;
        else
            return 2;
    }

    public void setBorderStyle(int i)
    {
        if(frame != null)
        {
            if(i == 2)
                frame.setResizable(true);
            else
                frame.setResizable(false);
        } else
        if(dialog != null)
            if(i == 2)
                dialog.setResizable(true);
            else
                dialog.setResizable(false);
    }

    public java.awt.Component getActiveControl()
    {
        if(frame != null)
            return frame.getFocusOwner();
        else
            return null;
    }

    public java.awt.Dialog getDialog()
    {
        return dialog;
    }

    public java.awt.Frame getFrame()
    {
        if(frame == null)
            frame = diamondedge.vb.AwtForm.getFrame(((java.awt.Component) (this)));
        return frame;
    }

    public static java.awt.Frame getFrame(java.awt.Component component)
    {
        for(; component != null; component = component.getParent())
            if(component instanceof java.awt.Frame)
                return (java.awt.Frame)component;

        return null;
    }

    public static short buttonFlags(java.awt.event.InputEvent inputevent)
    {
        int i = inputevent.getModifiers();
        short word0 = 0;
        if((i & 0x10) != 0)
            word0 |= 1;
        if((i & 8) != 0)
            word0 |= 4;
        if((i & 4) != 0)
            word0 |= 2;
        return word0;
    }

    public static short shiftFlags(java.awt.event.InputEvent inputevent)
    {
        int i = inputevent.getModifiers();
        short word0 = 0;
        if((i & 1) != 0)
            word0 |= 1;
        if((i & 2) != 0)
            word0 |= 2;
        if((i & 4) != 0)
            word0 |= 4;
        return word0;
    }

    public void windowClosing(java.awt.event.WindowEvent windowevent)
    {
        diamondedge.vb.Application.a(this);
    }

    public void windowActivated(java.awt.event.WindowEvent windowevent)
    {
        diamondedge.vb.Screen.setActiveForm(this);
    }

    public void windowDeactivated(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowOpened(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowClosed(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowIconified(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowDeiconified(java.awt.event.WindowEvent windowevent)
    {
    }

    public void showForm()
    {
        showForm(0, null);
    }

    public void showForm(int i, diamondedge.vb.Form form)
    {
        if(initInProgress)
            return;
        setVisible(false);
        if(!initialized || window == null)
            if(diamondedge.vb.Application.getApplet(this) != null)
            {
                if(!initialized)
                {
                    init();
                    validate();
                    diamondedge.vb.Screen.setActiveForm(this);
                }
            } else
            if(window == null)
            {
                if(i == 1)
                {
                    if(form == null)
                        form = diamondedge.vb.Screen.getActiveForm();
                    if(form != null && (form instanceof diamondedge.vb.AwtForm))
                    {
                        diamondedge.vb.AwtForm awtform = (diamondedge.vb.AwtForm)form;
                        java.awt.Dialog dialog1 = awtform.getDialog();
                        if(dialog1 != null)
                        {
                            dialog = new Dialog(dialog1, null, true);
                        } else
                        {
                            java.awt.Frame frame1 = awtform.getFrame();
                            dialog = new Dialog(frame1, true);
                        }
                    }
                    if(dialog == null)
                        dialog = new Dialog((java.awt.Frame)null, true);
                    window = dialog;
                    dialog.add(this, "Center");
                } else
                {
                    frame = new Frame();
                    frame.setLayout(new BorderLayout());
                    window = frame;
                    frame.add(this, "Center");
                }
                window.addWindowListener(this);
                init();
                if(window != null)
                    window.pack();
                java.awt.Insets insets = window.getInsets();
                if(insets.bottom < 0)
                {
                    java.awt.Dimension dimension1 = window.getPreferredSize();
                    dimension1.height += -insets.bottom;
                    window.setSize(dimension1);
                }
            } else
            {
                init();
                if(window != null)
                    window.pack();
            }
        if(!isVisible())
        {
            setVisible(true);
            if(diamondedge.vb.Application.getApplet(this) != null)
                diamondedge.vb.Screen.setActiveForm(this);
        }
        if(window != null)
        {
            if(startUpPosition == 3)
                window.setLocation(0, 0);
            else
            if(startUpPosition == 2)
            {
                java.awt.Dimension dimension = window.getSize();
                int j = (diamondedge.vb.Screen.getWidth() - dimension.width) / 2;
                int k = (diamondedge.vb.Screen.getHeight() - dimension.height) / 2;
                window.setLocation(j, k);
            } else
            {
                window.setLocation(locationX, locationY);
            }
            window.setVisible(true);
            if(window != null)
                window.toFront();
        }
        diamondedge.vb.Application.a(null, this);
    }

    public void hideForm()
    {
        if(window != null)
            window.setVisible(false);
        else
            setVisible(false);
    }

    public final void printForm()
    {
        java.awt.PrintJob printjob = getToolkit().getPrintJob(new Frame(), "Form Print", new Properties());
        if(printjob != null)
        {
            java.awt.Graphics g = printjob.getGraphics();
            g.setClip(getBounds());
            printAll(g);
            g.dispose();
            printjob.end();
        }
    }

    public void zOrder(int i)
    {
        if(window == null)
            return;
        if(i == 0)
            window.toFront();
        else
            window.toBack();
    }

    public int getWindowState()
    {
        return 0;
    }

    public void setWindowState(int i)
    {
    }

    public void Form_Load()
    {
    }

    public void Form_DragDrop(java.awt.Component component, float f, float f1)
    {
    }

    public void Form_DragOver(java.awt.Component component, float f, float f1, int i)
    {
    }

    public void Form_Resize()
    {
    }

    public void Form_Unload(int i)
    {
    }

    public void Form_Unload(diamondedge.util.Variant variant)
    {
    }

    public void Form_QueryUnload(diamondedge.util.Variant variant, int i)
    {
    }

    public void Form_QueryUnload(int i, int j)
    {
    }

    public void Form_Activate()
    {
    }

    public void Form_Deactivate()
    {
    }

    public void Form_Paint()
    {
    }

    public void Form_Initialize()
    {
    }

    public void Form_Terminate()
    {
    }

    public void initializeList(java.awt.List list, java.lang.String as[])
    {
        if(as == null || list == null)
            return;
        for(int i = 0; i < as.length; i++)
            list.add(as[i]);

    }

    public void initializeList(java.awt.Choice choice, java.lang.String as[])
    {
        if(as == null || choice == null)
            return;
        for(int i = 0; i < as.length; i++)
            choice.add(as[i]);

    }

    public static void initScrollbar(java.awt.Scrollbar scrollbar)
    {
        scrollbar.setValues(0, scrollbar.getVisibleAmount(), 0, 32767);
        scrollbar.setUnitIncrement(1);
        scrollbar.setBlockIncrement(1);
    }

    public static final java.lang.String copyright = "Copyright 1996-2004 Diamond Edge, Inc.";
    private java.lang.String ax = null;
    protected boolean initialized = false;
    protected boolean unloaded = false;
    protected boolean initInProgress = false;
    protected java.awt.Frame frame = null;
    protected java.awt.Dialog dialog = null;
    protected java.awt.Window window = null;
    protected int startUpPosition = 0;
    protected int locationX = 0;
    protected int locationY = 0;
    protected java.awt.Dimension prefSize = null;
    private int aw = 0;
}
