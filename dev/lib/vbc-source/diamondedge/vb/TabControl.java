// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Component;
import javax.swing.JTabbedPane;

// Referenced classes of package diamondedge.vb:
//            VbControl

public class TabControl extends javax.swing.JTabbedPane
    implements diamondedge.vb.VbControl
{

    public TabControl()
    {
    }

    public int getTabOrder()
    {
        return _flddo;
    }

    public void setTabOrder(int i)
    {
        _flddo = i;
    }

    public void addTab(java.lang.String s, java.awt.Component component)
    {
        int i;
        if(component != null && (i = indexOfComponent(component)) >= 0)
            removeTabAt(i);
        super.addTab(s, component);
    }

    private int _flddo = 0;
}
