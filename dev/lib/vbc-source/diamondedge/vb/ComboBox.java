// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.ado.Command;
import diamondedge.ado.Field;
import diamondedge.ado.Fields;
import diamondedge.ado.Recordset;
import diamondedge.util.Variant;
import java.io.PrintStream;
import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;

// Referenced classes of package diamondedge.vb:
//            Item, DataBound, VbControl, Screen, 
//            DataSource, DataBoundRouter, DataChangeEvent

public class ComboBox extends javax.swing.JComboBox
    implements diamondedge.vb.DataBound, diamondedge.vb.VbControl
{

    public ComboBox()
    {
        _fldlong = -1;
        _fldbyte = 0;
        _fldgoto = false;
        _fldtry = null;
        _fldcase = null;
        _fldvoid = false;
        setFont(diamondedge.vb.Screen.getDefaultFont());
        setEditable(true);
    }

    public int getNewIndex()
    {
        return _fldlong;
    }

    public java.lang.String getItem(int i)
    {
        java.lang.Object obj = getItemAt(i);
        return obj != null ? obj.toString() : "";
    }

    public void setItem(int i, java.lang.String s)
    {
        int j = getItemCount();
        if(i < 0 || i >= j)
        {
            addItem(s);
        } else
        {
            java.lang.Object obj = getItemAt(i);
            if(obj instanceof diamondedge.vb.Item)
            {
                ((diamondedge.vb.Item)obj).setString(s);
            } else
            {
                removeItemAt(i);
                insertItemAt(s, i);
            }
        }
    }

    public int getItemData(int i)
    {
        java.lang.Object obj = getItemAt(i);
        if(obj instanceof diamondedge.vb.Item)
            return ((diamondedge.vb.Item)obj).getData();
        else
            return 0;
    }

    public void setItemData(int i, int j)
    {
        int k = getItemCount();
        if(i < 0 || i >= k)
        {
            addItem(new Item("", j));
        } else
        {
            java.lang.Object obj = getItemAt(i);
            if(obj instanceof diamondedge.vb.Item)
            {
                ((diamondedge.vb.Item)obj).setData(j);
            } else
            {
                removeItemAt(i);
                insertItemAt(new Item(obj, j), i);
            }
        }
    }

    public synchronized void addItem(java.lang.Object obj)
    {
        super.addItem(obj);
        _fldlong = getItemCount() - 1;
    }

    public synchronized void addItem(java.lang.Object obj, int i)
    {
        addItem(new Item(obj, i));
    }

    public synchronized void insertItemAt(java.lang.Object obj, int i)
    {
        super.insertItemAt(obj, i);
        _fldlong = i;
    }

    public void initializeList(java.lang.String as[])
    {
        if(as == null)
            return;
        for(int i = 0; i < as.length; i++)
            setItem(i, as[i]);

    }

    public void initializeItemData(int ai[])
    {
        if(ai == null)
            return;
        for(int i = 0; i < ai.length; i++)
            setItemData(i, ai[i]);

    }

    public void setIgnoreEvents(boolean flag)
    {
        _fldgoto = flag;
    }

    protected void fireActionEvent()
    {
        if(!_fldgoto)
            super.fireActionEvent();
    }

    public java.lang.String getTag()
    {
        return c;
    }

    public void setTag(java.lang.String s)
    {
        c = s;
    }

    public int getTabOrder()
    {
        return _fldnull;
    }

    public void setTabOrder(int i)
    {
        _fldnull = i;
    }

    public int getStyle()
    {
        return _fldbyte;
    }

    public void setStyle(int i)
    {
        _fldbyte = i;
        setEditable(_fldbyte != 2);
    }

    public java.lang.String getText()
    {
        java.lang.Object obj = getEditor().getItem();
        if(_fldbyte == 2)
            obj = getSelectedItem();
        return obj != null ? obj.toString() : "";
    }

    public void setText(java.lang.String s)
    {
        if(_fldbyte == 2)
            setSelectedItem(s);
        else
            getEditor().setItem(s);
    }

    public java.lang.String getBoundField()
    {
        return _fldchar;
    }

    public void setBoundField(java.lang.String s)
    {
        if(s != null && !s.equals(""))
            _fldchar = s;
        else
            _fldchar = null;
    }

    public diamondedge.util.Variant getBoundValue()
    {
        diamondedge.ado.Recordset recordset;
        if(_fldchar == null || _fldcase == null)
            break MISSING_BLOCK_LABEL_85;
        recordset = _fldcase.getRecordset();
        if(recordset == null)
            break MISSING_BLOCK_LABEL_85;
        diamondedge.ado.Field field;
        int i = getSelectedIndex() + 1;
        if(recordset.getCursorType() == 0 && i < recordset.getAbsolutePosition())
            recordset.moveFirst();
        recordset.setAbsolutePosition(i);
        field = recordset.getField(_fldchar);
        if(field != null)
            return field.getValue();
        break MISSING_BLOCK_LABEL_85;
        java.lang.Exception exception;
        exception;
        exception.printStackTrace();
        return null;
    }

    public java.lang.String getBoundText()
    {
        diamondedge.util.Variant variant = getBoundValue();
        if(variant != null)
            return variant.toString();
        else
            return "";
    }

    public void setBoundText(java.lang.String s)
    {
        if(_fldchar == null || _fldtry == null)
            break MISSING_BLOCK_LABEL_52;
        diamondedge.ado.Recordset recordset;
        recordset = _fldtry.getRecordset();
        if(recordset == null)
            return;
        try
        {
            recordset.getField(_fldchar).setValue(new Variant(s));
        }
        catch(java.lang.Exception exception) { }
    }

    public java.lang.String getListField()
    {
        return _fldelse;
    }

    public void setListField(java.lang.String s)
    {
        _fldelse = s;
    }

    public diamondedge.vb.DataSource getRowSource()
    {
        return _fldcase;
    }

    public void setRowSource(diamondedge.vb.DataSource datasource)
    {
        if(_fldcase != null)
            _fldcase.removeDataBound(this);
        if(datasource != null)
        {
            datasource.addDataBound(this);
            _fldvoid = false;
        }
        _fldcase = datasource;
    }

    public java.lang.String getDataField()
    {
        return b;
    }

    public void setDataField(java.lang.String s)
    {
        b = s;
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return _fldtry;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        if(_fldtry != null)
        {
            _fldtry.removeDataBound(this);
            removeItemListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        if(datasource != null)
        {
            datasource.addDataBound(this);
            addItemListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        _fldtry = datasource;
    }

    public diamondedge.util.Variant getDataValue()
    {
        if(_fldchar == null)
            return new Variant(getText());
        else
            return getBoundValue();
    }

    public void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent)
    {
        java.lang.String s;
        s = datachangeevent.getValue() != null ? datachangeevent.getValue().toString() : "";
        if(_fldcase == null)
            break MISSING_BLOCK_LABEL_230;
        if(_fldtry == null || datachangeevent.getRecordset() != _fldtry.getRecordset())
            break MISSING_BLOCK_LABEL_206;
        if(s.length() == 0)
        {
            setSelectedIndex(-1);
            break MISSING_BLOCK_LABEL_235;
        }
        if(_fldelse == null || _fldchar == null || _fldelse.equals(_fldchar))
            break MISSING_BLOCK_LABEL_198;
        diamondedge.ado.Recordset recordset;
        recordset = _fldcase.getRecordset();
        if(recordset == null)
            return;
        try
        {
            java.lang.String s1 = recordset.getSource().getSqlWithNewFilter(recordset, _fldchar, s);
            recordset = new Recordset(recordset, false);
            recordset.open(s1);
            diamondedge.ado.Field field = recordset.getField(_fldelse);
            if(field != null)
                setText(field.getValue().toString());
        }
        catch(java.lang.Exception exception)
        {
            java.lang.System.out.println("ComboBox.dataChanged(): " + exception);
        }
        break MISSING_BLOCK_LABEL_235;
        setText(s);
        break MISSING_BLOCK_LABEL_235;
        if(datachangeevent.getChangeType() == 7 || datachangeevent.getChangeType() == 1)
            fillList();
        break MISSING_BLOCK_LABEL_235;
        setText(s);
    }

    public void fillList()
    {
        if(!_fldvoid && _fldcase != null && _fldelse != null)
        {
            diamondedge.ado.Recordset recordset = _fldcase.getRecordset();
            if(recordset == null)
                return;
            try
            {
                clear();
                int i = recordset.getFields().getFieldIndex(_fldelse, null);
                int j = recordset.getAbsolutePosition();
                int k = recordset.getRecordCount();
                if(k == -1)
                    k = recordset.getRecordCount(true);
                for(int l = 0; l < k; l++)
                {
                    diamondedge.util.Variant variant = recordset.getValueAt(l, i);
                    if(variant != null)
                        addItem(variant.toString(), l + 1);
                }

                if(j != recordset.getAbsolutePosition())
                    recordset.setAbsolutePosition(j);
                _fldvoid = true;
            }
            catch(java.lang.Exception exception)
            {
                java.lang.System.out.println("ComboBox.fillList:" + exception);
            }
        }
    }

    public void removeAllItems()
    {
        diamondedge.vb.DataSource datasource = _fldtry;
        _fldtry = null;
        java.lang.String s = _fldchar;
        _fldchar = null;
        _fldgoto = true;
        super.removeAllItems();
        _fldgoto = false;
        _fldtry = datasource;
        _fldchar = s;
    }

    public void clear()
    {
        removeAllItems();
    }

    private java.lang.String c = null;
    private int _fldnull = 0;
    private int _fldlong = 0;
    public static final short vbComboDropdown = 0;
    public static final short vbComboSimple = 1;
    public static final short vbComboDropdownList = 2;
    private int _fldbyte = 0;
    private boolean _fldgoto = false;
    private java.lang.String b = null;
    private diamondedge.vb.DataSource _fldtry = null;
    private java.lang.String _fldelse = null;
    private java.lang.String _fldchar = null;
    private diamondedge.vb.DataSource _fldcase = null;
    private boolean _fldvoid = false;
}
