// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.ado.AdoError;
import diamondedge.ado.Field;
import diamondedge.ado.Fields;
import diamondedge.ado.Recordset;
import java.io.PrintStream;

// Referenced classes of package diamondedge.vb:
//            DataBoundRouter, DataBound, DataChangeEvent

public class DataChange
{

    public DataChange()
    {
    }

    public static void initialize(diamondedge.vb.DataBound databound, diamondedge.vb.DataChangeEvent datachangeevent, diamondedge.ado.Recordset recordset)
    {
        diamondedge.vb.DataBoundRouter.dbRouter.setInitializing(true);
        java.lang.String s = databound.getDataField();
        if(s == null || recordset.isEOF())
            datachangeevent.setValue(null);
        else
            try
            {
                diamondedge.ado.Field field = recordset.getFields().getField(s);
                if(field != null)
                    datachangeevent.setValue(field.getValue());
            }
            catch(diamondedge.ado.AdoError adoerror)
            {
                if(adoerror.getNumber() != 30011)
                    java.lang.System.out.println("DataChange.initialize(" + s + "): " + adoerror);
            }
        databound.dataChanged(datachangeevent);
        diamondedge.vb.DataBoundRouter.dbRouter.setInitializing(false);
    }
}
