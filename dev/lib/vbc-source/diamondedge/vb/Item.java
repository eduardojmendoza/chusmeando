// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;


public class Item
{

    public Item(java.lang.Object obj, int i)
    {
        a = null;
        _fldif = 0;
        a = obj;
        _fldif = i;
    }

    public java.lang.String toString()
    {
        return a != null ? a.toString() : "";
    }

    public void setString(java.lang.String s)
    {
        a = s;
    }

    public java.lang.Object getItem()
    {
        return a;
    }

    public int getData()
    {
        return _fldif;
    }

    public void setData(int i)
    {
        _fldif = i;
    }

    public boolean equals(java.lang.Object obj)
    {
        if(obj instanceof diamondedge.vb.Item)
        {
            diamondedge.vb.Item item = (diamondedge.vb.Item)obj;
            if(item.getData() == _fldif && item.toString().equals(toString()))
                return true;
        } else
        if(obj != null && obj.toString().equals(toString()))
            return true;
        return false;
    }

    private java.lang.Object a = null;
    private int _fldif = 0;
}
