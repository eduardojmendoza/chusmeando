// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.swing.DsActionGroup;
import diamondedge.swing.DsSlidingTabBar;
import diamondedge.util.Variant;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.event.EventListenerList;

// Referenced classes of package diamondedge.vb:
//            VbActionEvent, ImageList, ListImages

public class SlidingTabBar extends diamondedge.swing.DsSlidingTabBar
{
    private static class a extends javax.swing.AbstractAction
    {

        public void actionPerformed(java.awt.event.ActionEvent actionevent)
        {
            if(a != null)
                a.actionPerformed(actionevent);
        }

        java.awt.event.ActionListener a = null;

        a(java.lang.String s, javax.swing.Icon icon, java.awt.event.ActionListener actionlistener)
        {
            super(s, icon);
            a = null;
            a = actionlistener;
        }
    }


    public SlidingTabBar()
    {
        smallIconList = null;
        largeIconList = null;
    }

    public void setActionCount(diamondedge.swing.DsActionGroup dsactiongroup, int i)
    {
        if(dsactiongroup != null)
            if(i > dsactiongroup.getActionCount())
            {
                for(int j = dsactiongroup.getActionCount(); j < i; j++)
                    a(dsactiongroup);

            } else
            if(i < dsactiongroup.getActionCount())
            {
                for(int k = dsactiongroup.getActionCount() - 1; k >= i; k--)
                    dsactiongroup.remove(k);

            }
    }

    private javax.swing.Action a(diamondedge.swing.DsActionGroup dsactiongroup)
    {
        diamondedge.vb.a a1 = new a(null, null, this);
        dsactiongroup.add(a1);
        return a1;
    }

    public void actionPerformed(java.awt.event.ActionEvent actionevent)
    {
        if(actionevent.getSource() instanceof javax.swing.AbstractButton)
        {
            javax.swing.AbstractButton abstractbutton = (javax.swing.AbstractButton)actionevent.getSource();
            if(abstractbutton.getAction() instanceof diamondedge.vb.a)
            {
                a(actionevent);
                return;
            }
        }
        super.actionPerformed(actionevent);
    }

    public void addActionListener(java.awt.event.ActionListener actionlistener)
    {
        listenerList.add(java.awt.event.ActionListener.class, actionlistener);
    }

    public void removeActionListener(java.awt.event.ActionListener actionlistener)
    {
        listenerList.remove(java.awt.event.ActionListener.class, actionlistener);
    }

    private void a(java.awt.event.ActionEvent actionevent)
    {
        diamondedge.vb.VbActionEvent vbactionevent = null;
        javax.swing.Action action = ((javax.swing.AbstractButton)actionevent.getSource()).getAction();
        java.lang.Object aobj[] = listenerList.getListenerList();
        for(int i = aobj.length - 2; i >= 0; i -= 2)
        {
            if(aobj[i] != (java.awt.event.ActionListener.class))
                continue;
            if(vbactionevent == null)
                vbactionevent = new VbActionEvent(this, 1001, actionevent.getActionCommand(), action);
            ((java.awt.event.ActionListener)aobj[i + 1]).actionPerformed(vbactionevent);
        }

    }

    public void initSmallIcons(int i)
    {
        smallIconList = new ImageList(i);
    }

    public void initLargeIcons(int i)
    {
        largeIconList = new ImageList(i);
    }

    public diamondedge.vb.ListImages getSmallIcons()
    {
        return smallIconList.getListImages();
    }

    public diamondedge.vb.ListImages getLargeIcons()
    {
        return smallIconList.getListImages();
    }

    public javax.swing.Icon getLargeIcon(java.lang.Object obj)
    {
        return diamondedge.vb.ImageList.getIcon(obj, largeIconList);
    }

    public javax.swing.Icon getSmallIcon(java.lang.Object obj)
    {
        return diamondedge.vb.ImageList.getIcon(obj, smallIconList);
    }

    public diamondedge.util.Variant getTag()
    {
        if(a == null)
            a = new Variant();
        return a;
    }

    public void setTag(diamondedge.util.Variant variant)
    {
        getTag().set(variant);
    }

    private diamondedge.util.Variant a = null;
    protected diamondedge.vb.ImageList smallIconList = null;
    protected diamondedge.vb.ImageList largeIconList = null;
}
