// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.io.File;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

// Referenced classes of package diamondedge.vb:
//            VbControl

public class DriveComboBox extends javax.swing.JComboBox
    implements diamondedge.vb.VbControl
{

    public DriveComboBox()
    {
        super(new DefaultComboBoxModel(java.io.File.listRoots()));
    }

    public java.lang.String getDrive()
    {
        java.lang.Object obj = getSelectedItem();
        return obj != null ? obj.toString() : "";
    }

    public void setDrive(java.lang.String s)
    {
        char c = s.toUpperCase().charAt(0);
        int i = getItemCount();
        for(int j = 0; j < i; j++)
        {
            java.lang.Object obj = getItemAt(j);
            java.lang.String s1 = obj != null ? obj.toString().toUpperCase() : "";
            if(s1.charAt(0) == c)
            {
                setSelectedIndex(j);
                return;
            }
        }

    }

    public java.io.File getDriveObject()
    {
        java.lang.Object obj = getSelectedItem();
        if(obj instanceof java.io.File)
            return (java.io.File)obj;
        else
            return null;
    }

    public java.lang.String getTag()
    {
        return aN;
    }

    public void setTag(java.lang.String s)
    {
        aN = s;
    }

    public int getTabOrder()
    {
        return aO;
    }

    public void setTabOrder(int i)
    {
        aO = i;
    }

    private java.lang.String aN = null;
    private int aO = 0;
}
