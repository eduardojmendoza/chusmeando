// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JSlider;

// Referenced classes of package diamondedge.vb:
//            VbControl, Screen

public class Slider extends javax.swing.JSlider
    implements diamondedge.vb.VbControl
{

    public Slider()
    {
        super(0, 10, 0);
        _fldint = false;
        setFont(diamondedge.vb.Screen.getDefaultFont());
        setPaintTicks(true);
        setMinorTickSpacing(1);
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
    }

    public java.lang.String getTag()
    {
        return _fldfor;
    }

    public void setTag(java.lang.String s)
    {
        _fldfor = s;
    }

    public int getTabOrder()
    {
        return _fldnew;
    }

    public void setTabOrder(int i)
    {
        _fldnew = i;
    }

    public void setValue(int i)
    {
        if(i != getValue())
        {
            super.setValue(i);
            paintImmediately(0, 0, getWidth(), getHeight());
        }
    }

    public int getNumTicks()
    {
        return (getMaximum() - getMinimum()) / getMinorTickSpacing();
    }

    public boolean isBordered()
    {
        return _fldint;
    }

    public void setBordered(boolean flag)
    {
        _fldint = flag;
        javax.swing.border.Border border = null;
        if(_fldint)
            border = javax.swing.BorderFactory.createLineBorder(java.awt.Color.black, 1);
        else
            border = javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1);
        setBorder(border);
    }

    public void setBorderVisible(boolean flag)
    {
        setBordered(flag);
    }

    private java.lang.String _fldfor = null;
    private boolean _fldint = false;
    private int _fldnew = 0;
}
