// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.awt.Color;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.border.Border;

// Referenced classes of package diamondedge.vb:
//            DataBound, VbControl, Screen, DataSource, 
//            DataChangeEvent

public class VbLabel extends javax.swing.JLabel
    implements diamondedge.vb.DataBound, diamondedge.vb.VbControl
{

    public VbLabel()
    {
        bj = '\0';
        labelFor = null;
        bh = 0;
        bi = true;
        bl = false;
        bg = null;
        setFont(diamondedge.vb.Screen.getDefaultFont());
        setOpaque(true);
    }

    public static void initSSPanel(diamondedge.vb.VbLabel vblabel)
    {
        vblabel.setOuterBorder(9);
        vblabel.setHorizontalAlignment(0);
        vblabel.setVerticalAlignment(0);
        vblabel.setOutline3D(false);
    }

    public java.lang.String getTag()
    {
        return bm;
    }

    public void setTag(java.lang.String s)
    {
        bm = s;
    }

    public int getTabOrder()
    {
        return bk;
    }

    public void setTabOrder(int i)
    {
        bk = i;
    }

    public int getOuterBorder()
    {
        return bh;
    }

    public void setOuterBorder(int i)
    {
        bh = i;
        setBorders();
    }

    public boolean isOutlined()
    {
        return bl;
    }

    public void setOutlined(boolean flag)
    {
        bl = flag;
        setBorders();
    }

    public boolean isOutline3D()
    {
        return bi;
    }

    public void setOutline3D(boolean flag)
    {
        if(bi != flag)
        {
            bi = flag;
            setBorders();
        }
    }

    private javax.swing.border.Border _mthint(int i)
    {
        if(i == 8)
            return javax.swing.BorderFactory.createLoweredBevelBorder();
        if(i == 9)
            return javax.swing.BorderFactory.createRaisedBevelBorder();
        if(i == 10)
            return javax.swing.BorderFactory.createEtchedBorder();
        if(i == 11)
            return javax.swing.BorderFactory.createEtchedBorder(0);
        if(i == 2)
        {
            java.awt.Color color = getBackground();
            if(color == null)
                color = java.awt.Color.black;
            return javax.swing.BorderFactory.createLineBorder(color, 1);
        } else
        {
            return null;
        }
    }

    protected javax.swing.border.Border createBorder()
    {
        java.lang.Object obj = null;
        if(bh != 0)
            if(obj == null)
                obj = _mthint(bh);
            else
                obj = javax.swing.BorderFactory.createCompoundBorder(((javax.swing.border.Border) (obj)), _mthint(bh));
        if(bl)
        {
            javax.swing.border.Border border = null;
            if(bi)
                border = javax.swing.BorderFactory.createLoweredBevelBorder();
            else
                border = javax.swing.BorderFactory.createLineBorder(java.awt.Color.black, 1);
            if(border != null && obj != null)
                obj = javax.swing.BorderFactory.createCompoundBorder(border, ((javax.swing.border.Border) (obj)));
            else
                obj = border;
        }
        return ((javax.swing.border.Border) (obj));
    }

    protected void setBorders()
    {
        setBorder(createBorder());
        repaint();
    }

    public java.lang.String getDataField()
    {
        return bn;
    }

    public void setDataField(java.lang.String s)
    {
        bn = s;
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return bg;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        if(bg != null)
            bg.removeDataBound(this);
        if(datasource != null)
            datasource.addDataBound(this);
        bg = datasource;
    }

    public diamondedge.util.Variant getDataValue()
    {
        return new Variant(getText());
    }

    public void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent)
    {
        setText(datachangeevent.getValue() != null ? datachangeevent.getValue().toString() : "");
    }

    private int bk = 0;
    private char bj = 0;
    private java.awt.Component labelFor = null;
    private int bh = 0;
    private boolean bi = false;
    private boolean bl = false;
    public static final int NONE = 1;
    public static final int SINGLE_LINE = 2;
    public static final int LOWERED = 8;
    public static final int RAISED = 9;
    public static final int ETCHED = 10;
    public static final int ETCHED_RAISED = 11;
    private java.lang.String bn = null;
    private diamondedge.vb.DataSource bg = null;
    private java.lang.String bm = null;
}
