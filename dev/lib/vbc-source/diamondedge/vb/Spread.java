// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import javax.swing.table.JTableHeader;

// Referenced classes of package diamondedge.vb:
//            Grid

public class Spread extends diamondedge.vb.Grid
{

    public Spread()
    {
        this(2, 2);
    }

    public Spread(int i, int j)
    {
        super(i - 1, j);
        getTableHeader().setReorderingAllowed(false);
        setAutoResizeMode(0);
        setRowHeaderAutoLabel(0);
        leaveFrozenColumns = true;
    }

    public void init()
    {
        countFrozenRow = true;
        removeAll();
    }

    public int getCellType()
    {
        return 0;
    }

    public void setCellType(int i)
    {
    }

    public void setOperationMode(int i)
    {
        setSelectionPolicy(i & 0xf0);
        setSelectionMode(i & 0xf);
    }
}
