// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import diamondedge.util.VbCollection;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;

// Referenced classes of package diamondedge.vb:
//            StatusPanel

public class StatusBar extends javax.swing.JPanel
    implements java.awt.event.ActionListener
{

    public StatusBar()
    {
        super(new FlowLayout(0, 1, 1));
        a = new VbCollection();
    }

    public void initPanels(int i)
    {
        if(i > 1 && _fldfor == null)
        {
            char c = '\u03E8';
            _fldfor = new Timer(c, this);
            _fldfor.start();
        }
        for(int j = a.getCount(); i-- > j;)
        {
            diamondedge.vb.StatusPanel statuspanel = new StatusPanel();
            add(statuspanel);
            a.add(new Variant(statuspanel));
        }

    }

    public void actionPerformed(java.awt.event.ActionEvent actionevent)
    {
        int i = a.getCount();
        for(int j = 1; j <= i; j++)
        {
            diamondedge.vb.StatusPanel statuspanel = getPanel(j);
            statuspanel.a();
        }

    }

    public void setDelay(int i)
    {
        if(i <= 0)
        {
            if(_fldfor != null)
            {
                _fldfor.stop();
                _fldfor.removeActionListener(this);
            }
            _fldfor = null;
        } else
        {
            if(_fldfor == null)
            {
                _fldfor = new Timer(i, this);
                _fldfor.start();
            }
            _fldfor.setDelay(i);
            if(!_fldfor.isRunning())
                _fldfor.restart();
        }
    }

    public void setBounds(int i, int j, int k, int l)
    {
        super.setBounds(i, j, k, l);
        if(k == 0 || l == 0)
            return;
        int i1 = a.getCount();
        double d = 1.0D;
        for(int j1 = 1; j1 <= i1; j1++)
        {
            diamondedge.vb.StatusPanel statuspanel = getPanel(j1);
            d += statuspanel.getPreferredSize().width;
        }

        d += 1 * (i1 - 1);
        double d1 = (double)k / d;
        for(int k1 = 1; k1 <= i1; k1++)
        {
            diamondedge.vb.StatusPanel statuspanel1 = getPanel(k1);
            statuspanel1.setPreferredSize(new Dimension((int)((double)statuspanel1.getPreferredSize().width * d1), l));
        }

    }

    public diamondedge.vb.StatusPanel getPanel(int i)
    {
        return (diamondedge.vb.StatusPanel)a.getItem(i).toObject();
    }

    public diamondedge.vb.StatusPanel getPanel(java.lang.String s)
    {
        return (diamondedge.vb.StatusPanel)a.getItem(s).toObject();
    }

    public java.lang.String getText()
    {
        if(a.getCount() == 1)
        {
            diamondedge.vb.StatusPanel statuspanel = getPanel(1);
            return statuspanel.getText();
        } else
        {
            return null;
        }
    }

    public void setText(java.lang.String s)
    {
        if(a.getCount() == 1)
        {
            diamondedge.vb.StatusPanel statuspanel = getPanel(1);
            statuspanel.setText(s);
        }
    }

    public diamondedge.util.VbCollection getPanels()
    {
        return a;
    }

    private diamondedge.util.VbCollection a = null;
    private int _fldif = 0;
    private static final int _flddo = 1;
    private javax.swing.Timer _fldfor = null;
}
