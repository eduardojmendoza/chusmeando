// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.swing.DsTree;
import diamondedge.swing.DsTreeModel;
import diamondedge.swing.DsTreeNode;

// Referenced classes of package diamondedge.vb:
//            VbControl

public class Outline extends diamondedge.swing.DsTree
    implements diamondedge.vb.VbControl
{

    public Outline()
    {
    }

    public int getItemCount()
    {
        return getNodes().getCount();
    }

    public java.lang.String getItem(int i)
    {
        diamondedge.swing.DsTreeNode dstreenode = getNodes().getNode(i);
        return dstreenode != null ? dstreenode.toString() : "";
    }

    public java.lang.Object getItemData(int i)
    {
        return getNodes().getNode(i).getData();
    }

    public void setItemData(int i, java.lang.Object obj)
    {
        getNodes().getNode(i).setData(obj);
    }

    public java.lang.String getTag()
    {
        return X;
    }

    public void setTag(java.lang.String s)
    {
        X = s;
    }

    public int getTabOrder()
    {
        return Y;
    }

    public void setTabOrder(int i)
    {
        Y = i;
    }

    private java.lang.String X = null;
    private int Y = 0;
}
