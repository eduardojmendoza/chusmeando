// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Button;
import java.awt.Canvas;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// Referenced classes of package diamondedge.vb:
//            AwtLabel, Application, Screen, AwtForm

public class VbDialog extends java.awt.Dialog
    implements java.awt.event.ActionListener
{

    public static java.lang.String InputBox(java.lang.String s, java.lang.String s1, java.lang.String s2, int i, int j)
    {
        if(s1 == null)
            s1 = diamondedge.vb.Application.getApplication() != null ? diamondedge.vb.Application.getApplication().getName() : "Input";
        diamondedge.vb.VbDialog vbdialog = new VbDialog(s, s1, s2, i, j);
        int k = vbdialog.display();
        if(k == 1)
            return vbdialog.getText();
        else
            return "";
    }

    public static java.lang.String InputBox(java.lang.String s, java.lang.String s1)
    {
        return diamondedge.vb.VbDialog.InputBox(s, s1, "", -1, -1);
    }

    public static java.lang.String InputBox(java.lang.String s)
    {
        return diamondedge.vb.VbDialog.InputBox(s, null, "", -1, -1);
    }

    public static int MsgBox(java.lang.String s, int i, java.lang.String s1)
    {
        if(s1 == null)
            s1 = diamondedge.vb.Application.getApplication() != null ? diamondedge.vb.Application.getApplication().getName() : "Message";
        diamondedge.vb.VbDialog vbdialog = new VbDialog(s, i, s1);
        return vbdialog.display();
    }

    public static int MsgBox(java.lang.String s)
    {
        java.lang.String s1 = diamondedge.vb.Application.getApplication() != null ? diamondedge.vb.Application.getApplication().getName() : "Message";
        diamondedge.vb.VbDialog vbdialog = new VbDialog(s, 0, s1);
        return vbdialog.display();
    }

    public VbDialog(java.lang.String s, int i, java.lang.String s1)
    {
        super(diamondedge.vb.VbDialog.a(), s1, true);
        x = -1;
        y = -1;
        setFont(diamondedge.vb.Screen.getDefaultFont());
        setResizable(false);
        diamondedge.vb.AwtLabel awtlabel = new AwtLabel(s, 10, 10, 0, 65);
        add("Center", awtlabel);
        addButtons(i, true);
    }

    public VbDialog(java.lang.String s, java.lang.String s1, java.lang.String s2, int i, int j)
    {
        super(diamondedge.vb.VbDialog.a(), s1, true);
        x = -1;
        y = -1;
        setFont(diamondedge.vb.Screen.getDefaultFont());
        setResizable(false);
        x = i;
        y = j;
        diamondedge.vb.AwtLabel awtlabel = new AwtLabel(s, 10, 10, 0, 65);
        if(s2 == null)
            s2 = "";
        input = new TextField(s2);
        add("Center", awtlabel);
        add("South", input);
        addButtons(1, false);
    }

    public int display()
    {
        pack();
        java.awt.Dimension dimension = getSize();
        java.lang.String s = getTitle();
        if(s != null)
        {
            java.awt.FontMetrics fontmetrics = getFontMetrics(getFont());
            int i = (int)((double)fontmetrics.stringWidth(s) * 1.3D) + 25;
            if(fontmetrics != null && i >= dimension.width)
            {
                dimension.width = i;
                setSize(dimension);
            }
        }
        if(x < 0)
            x = (java.awt.Toolkit.getDefaultToolkit().getScreenSize().width - dimension.width) / 2;
        if(y < 0)
            y = (java.awt.Toolkit.getDefaultToolkit().getScreenSize().height - dimension.height) / 2;
        setLocation(x, y);
        setVisible(true);
        return action;
    }

    public java.awt.Insets getInsets()
    {
        return new Insets(30, 10, 10, 10);
    }

    public java.lang.String getText()
    {
        if(input != null)
            return input.getText();
        else
            return "";
    }

    protected java.awt.Container addButtons(int i, boolean flag)
    {
        button = new java.awt.Button[3];
        buttonId = new int[3];
        switch(i & 0xf)
        {
        case 1: // '\001'
            button[1] = new Button("Cancel");
            buttonId[1] = 2;
            // fall through

        case 0: // '\0'
            button[0] = new Button(" OK ");
            buttonId[0] = 1;
            break;

        case 3: // '\003'
            button[2] = new Button("Cancel");
            buttonId[2] = 2;
            // fall through

        case 4: // '\004'
            button[0] = new Button(" Yes ");
            buttonId[0] = 6;
            button[1] = new Button(" No ");
            buttonId[1] = 7;
            break;

        case 2: // '\002'
            button[0] = new Button("Abort");
            buttonId[0] = 3;
            button[1] = new Button("Retry");
            buttonId[1] = 4;
            button[2] = new Button("Ignore");
            buttonId[2] = 5;
            break;

        case 5: // '\005'
            button[0] = new Button("Retry");
            buttonId[0] = 4;
            button[1] = new Button("Cancel");
            buttonId[1] = 2;
            break;
        }
        java.awt.Panel panel = new Panel();
        for(int j = 0; j < button.length; j++)
            if(button[j] != null)
            {
                button[j].addActionListener(this);
                panel.add(button[j]);
            }

        if(flag)
        {
            panel.setLayout(new FlowLayout(1, 7, 15));
            add("South", panel);
        } else
        {
            panel.setLayout(new GridLayout(0, 1, 7, 7));
            panel.add(new Canvas());
            add("East", panel);
        }
        return panel;
    }

    public void actionPerformed(java.awt.event.ActionEvent actionevent)
    {
        int i = 0;
        do
        {
            if(i >= button.length)
                break;
            if(actionevent.getSource() == button[i])
            {
                action = buttonId[i];
                break;
            }
            i++;
        } while(true);
        dispose();
    }

    private static java.awt.Frame a()
    {
        diamondedge.vb.Form form = diamondedge.vb.Screen.getActiveForm();
        if(form != null)
        {
            java.awt.Frame frame = diamondedge.vb.AwtForm.getFrame((java.awt.Component)form);
            if(frame != null)
                return frame;
        }
        return new Frame();
    }

    protected int action = 0;
    protected java.awt.Button button[] = null;
    protected int buttonId[] = null;
    protected java.awt.TextField input = null;
    protected int x = 0;
    protected int y = 0;
    public static final int vbOKOnly = 0;
    public static final int vbOKCancel = 1;
    public static final int vbAbortRetryIgnore = 2;
    public static final int vbYesNoCancel = 3;
    public static final int vbYesNo = 4;
    public static final int vbRetryCancel = 5;
    public static final int vbCritical = 16;
    public static final int vbQuestion = 32;
    public static final int vbExclamation = 48;
    public static final int vbInformation = 64;
    public static final int vbDefaultButton1 = 0;
    public static final int vbDefaultButton2 = 256;
    public static final int vbDefaultButton3 = 512;
    public static final int vbDefaultButton4 = 768;
    public static final int vbApplicationModal = 0;
    public static final int vbSystemModal = 4096;
    public static final int vbMsgBoxHelpButton = 16384;
    public static final int vbMsgBoxRight = 0x80000;
    public static final int vbMsgBoxRtlReading = 0x100000;
    public static final int vbMsgBoxSetForeground = 0x10000;
    public static final short vbOK = 1;
    public static final short vbCancel = 2;
    public static final short vbAbort = 3;
    public static final short vbRetry = 4;
    public static final short vbIgnore = 5;
    public static final short vbYes = 6;
    public static final short vbNo = 7;
}
