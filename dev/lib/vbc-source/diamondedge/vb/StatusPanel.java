// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.text.DateFormat;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class StatusPanel extends javax.swing.JLabel
{

    public StatusPanel()
    {
        _fldfor = 0;
        _fldif = null;
        _flddo = 0;
        setBevel(1);
        setPreferredSize(new Dimension(96, 18));
        setSize(96, 18);
    }

    public int getBevel()
    {
        return _fldfor;
    }

    public void setBevel(int i)
    {
        _fldfor = i;
        javax.swing.border.Border border = null;
        if(i == 1)
            border = javax.swing.BorderFactory.createLoweredBevelBorder();
        else
        if(i == 2)
            border = javax.swing.BorderFactory.createRaisedBevelBorder();
        javax.swing.border.Border border1 = javax.swing.BorderFactory.createEmptyBorder(0, 4, 0, 0);
        if(border == null)
            setBorder(border1);
        else
            setBorder(javax.swing.BorderFactory.createCompoundBorder(border, border1));
    }

    public java.lang.String getKey()
    {
        return _fldif;
    }

    public void setKey(java.lang.String s)
    {
        _fldif = s;
    }

    public void setWidth(int i)
    {
        setPreferredSize(new Dimension(i, getHeight()));
    }

    public java.awt.Image getPicture()
    {
        return null;
    }

    public void setPicture(java.awt.Image image)
    {
        setIcon(new ImageIcon(image));
    }

    public int getStyle()
    {
        return _flddo;
    }

    public void setStyle(int i)
    {
        _flddo = i;
        a();
    }

    public diamondedge.util.Variant getTag()
    {
        if(a == null)
            a = new Variant();
        return a;
    }

    public void setTag(diamondedge.util.Variant variant)
    {
        getTag().set(variant);
    }

    void a()
    {
        try
        {
            switch(_flddo)
            {
            case 1: // '\001'
                setEnabled(java.awt.Toolkit.getDefaultToolkit().getLockingKeyState(20));
                break;

            case 2: // '\002'
                setEnabled(java.awt.Toolkit.getDefaultToolkit().getLockingKeyState(144));
                break;

            case 4: // '\004'
                setEnabled(java.awt.Toolkit.getDefaultToolkit().getLockingKeyState(145));
                break;

            case 5: // '\005'
                setText(java.text.DateFormat.getTimeInstance(3).format(new Date()));
                break;

            case 6: // '\006'
                setText(java.text.DateFormat.getDateInstance().format(new Date()));
                break;

            case 7: // '\007'
                setEnabled(java.awt.Toolkit.getDefaultToolkit().getLockingKeyState(262));
                break;
            }
        }
        catch(java.lang.UnsupportedOperationException unsupportedoperationexception)
        {
            setEnabled(false);
        }
    }

    private diamondedge.util.Variant a = null;
    private int _fldfor = 0;
    private java.lang.String _fldif = null;
    private int _flddo = 0;
}
