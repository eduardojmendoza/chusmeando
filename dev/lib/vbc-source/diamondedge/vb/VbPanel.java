// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.util.EventObject;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.event.EventListenerList;

// Referenced classes of package diamondedge.vb:
//            VbGraphics, RepaintListener, Screen

public class VbPanel extends javax.swing.JPanel
{

    public VbPanel()
    {
        buttonGroup = null;
        an = false;
        am = true;
        ao = null;
        ignoreRepaint = false;
        al = new VbGraphics(this);
        setLayout(null);
        setFont(diamondedge.vb.Screen.getDefaultFont());
        al.setInsets(getInsets());
    }

    public java.awt.Image getImage()
    {
        return ao;
    }

    public void setImage(java.lang.String s)
    {
        ao = diamondedge.vb.Screen.loadImage(s);
    }

    public void setImage(java.awt.Image image)
    {
        ao = image;
    }

    public void paintComponent(java.awt.Graphics g)
    {
        super.paintComponent(g);
        if(ao != null)
        {
            java.awt.Insets insets = getInsets();
            if(isPictureTiled())
            {
                int i = ao.getWidth(this);
                int j = ao.getHeight(this);
                int k = getWidth() - insets.left - insets.right;
                int l = getHeight() - insets.top - insets.bottom;
                for(int i1 = insets.top; i1 < l; i1 += j)
                {
                    for(int j1 = insets.left; j1 < k; j1 += i)
                        g.drawImage(ao, j1, i1, this);

                }

            } else
            {
                g.drawImage(ao, insets.left, insets.top, this);
            }
        }
        firePaint();
    }

    public void setBounds(int i, int j, int k, int l)
    {
        super.setBounds(i, j, k, l);
        int i1 = al.getScaleMode();
        al.setScaleMode(3);
        al.setScaleLeft(i);
        al.setScaleTop(j);
        al.setScaleWidth(k);
        al.setScaleHeight(l);
        al.setScaleMode(i1);
        if(!isEnabled())
            diamondedge.vb.VbPanel.a(this, i, j, k, l);
    }

    public diamondedge.util.Variant getTag()
    {
        if(ak == null)
            ak = new Variant();
        return ak;
    }

    public void setTag(diamondedge.util.Variant variant)
    {
        getTag().set(variant);
    }

    static void a(java.awt.Container container, int i, int j, int k, int l)
    {
        int i1 = container.getComponentCount();
        int j1 = 0;
        do
        {
            if(j1 >= i1)
                break;
            java.awt.Component component = container.getComponent(j1);
            if(component instanceof javax.swing.JLayeredPane)
            {
                component.setBounds(0, 0, k, l);
                break;
            }
            j1++;
        } while(true);
    }

    public void setEnabled(boolean flag)
    {
        diamondedge.vb.VbPanel.a(this, flag);
        super.setEnabled(flag);
    }

    static void a(java.awt.Container container, boolean flag)
    {
        if(flag)
        {
            int i = container.getComponentCount();
            for(int j = 0; j < i; j++)
            {
                java.awt.Component component = container.getComponent(j);
                if(component instanceof javax.swing.JLayeredPane)
                {
                    container.remove(component);
                    i--;
                    j--;
                }
            }

        } else
        {
            javax.swing.JLayeredPane jlayeredpane = new JLayeredPane();
            jlayeredpane.setBounds(0, 0, container.getWidth(), container.getHeight());
            jlayeredpane.addMouseListener(new java.awt.event.MouseAdapter() {

            }
);
            container.add(jlayeredpane, 0);
            if(!container.requestFocusInWindow())
                container.transferFocusUpCycle();
        }
    }

    protected boolean isPictureTiled()
    {
        return false;
    }

    public boolean isBorder3D()
    {
        return am;
    }

    public void setBorder3D(boolean flag)
    {
        if(am != flag)
        {
            am = flag;
            setBordered(an);
        }
    }

    public boolean isBordered()
    {
        return an;
    }

    public void setBordered(boolean flag)
    {
        an = flag;
        if(flag)
        {
            if(am)
                setBorder(javax.swing.BorderFactory.createLoweredBevelBorder());
            else
                setBorder(javax.swing.BorderFactory.createLineBorder(java.awt.Color.black));
        } else
        {
            setBorder(null);
        }
        al.setInsets(getInsets());
    }

    public double getCurrentX()
    {
        return al.getCurrentX();
    }

    public void setCurrentX(double d)
    {
        al.setCurrentX(d);
    }

    public double getCurrentY()
    {
        return al.getCurrentY();
    }

    public void setCurrentY(double d)
    {
        al.setCurrentY(d);
    }

    public int getDrawMode()
    {
        return al.getDrawMode();
    }

    public void setDrawMode(int i)
    {
        al.setDrawMode(i);
    }

    public int getDrawStyle()
    {
        return 0;
    }

    public void setDrawStyle(int i)
    {
    }

    public int getDrawWidth()
    {
        return 0;
    }

    public void setDrawWidth(int i)
    {
    }

    public void repaint()
    {
        if(!ignoreRepaint)
            super.repaint();
    }

    public void setFont(java.awt.Font font)
    {
        ignoreRepaint = true;
        super.setFont(font);
        ignoreRepaint = false;
    }

    public void setForeground(java.awt.Color color)
    {
        ignoreRepaint = true;
        super.setForeground(color);
        ignoreRepaint = false;
    }

    public void setBackground(java.awt.Color color)
    {
        ignoreRepaint = true;
        super.setBackground(color);
        ignoreRepaint = false;
    }

    public java.awt.Color getFillColor()
    {
        return al.getFillColor();
    }

    public void setFillColor(java.awt.Color color)
    {
        al.setFillColor(color);
    }

    public int getFillStyle()
    {
        return al.getFillStyle();
    }

    public void setFillStyle(int i)
    {
        al.setFillStyle(i);
    }

    public double getScaleLeft()
    {
        return al.getScaleLeft();
    }

    public void setScaleLeft(double d)
    {
        al.setScaleLeft(d);
    }

    public double getScaleTop()
    {
        return al.getScaleTop();
    }

    public void setScaleTop(double d)
    {
        al.setScaleTop(d);
    }

    public double getScaleWidth()
    {
        return al.getScaleWidth();
    }

    public void setScaleWidth(double d)
    {
        al.setScaleWidth(d);
    }

    public double getScaleHeight()
    {
        return al.getScaleHeight();
    }

    public void setScaleHeight(double d)
    {
        al.setScaleHeight(d);
    }

    public int getScaleMode()
    {
        return al.getScaleMode();
    }

    public void setScaleMode(int i)
    {
        al.setScaleMode(i);
    }

    public void scale(double d, double d1, double d2, double d3)
    {
        al.scale(d, d1, d2, d3);
    }

    public void scale()
    {
        al.scale();
    }

    public double scaleX(double d, int i, int j)
    {
        return al.scaleX(d, i, j);
    }

    public double scaleY(double d, int i, int j)
    {
        return al.scaleY(d, i, j);
    }

    public void addExclusiveButton(javax.swing.AbstractButton abstractbutton)
    {
        add(abstractbutton);
        if(buttonGroup == null)
            buttonGroup = new ButtonGroup();
        buttonGroup.add(abstractbutton);
    }

    public void circle(double d, double d1, boolean flag, double d2, 
            java.awt.Color color, double d3, double d4, double d5)
    {
        al.circle(d, d1, flag, d2, color, d3, d4, d5);
    }

    public void cls()
    {
        java.awt.Insets insets = getInsets();
        java.awt.Graphics g = getGraphics();
        g.setColor(getBackground());
        java.awt.Dimension dimension = getSize();
        dimension.width -= insets.left + insets.right;
        dimension.height -= insets.top + insets.bottom;
        g.fillRect(insets.left, insets.top, dimension.width, dimension.height);
        setCurrentX(0.0D);
        setCurrentY(0.0D);
    }

    public void line(double d, double d1, boolean flag, double d2, 
            double d3, boolean flag1, java.awt.Color color, int i)
    {
        al.line(d, d1, flag, d2, d3, flag1, color, i);
    }

    public void paintPicture(java.awt.Image image, double d, double d1, double d2, 
            double d3)
    {
        al.paintPicture(image, d, d1, d2, d3);
    }

    public java.awt.Color point(double d, double d1)
    {
        return al.point(d, d1, ao);
    }

    public void print(java.lang.String s)
    {
        al.print(s);
    }

    public void pointSet(double d, double d1, boolean flag, java.awt.Color color)
    {
        al.pointSet(d, d1, flag, color);
    }

    public double textWidth(java.lang.String s)
    {
        java.awt.FontMetrics fontmetrics = getFontMetrics(getFont());
        return (double)fontmetrics.stringWidth(s);
    }

    public double textHeight(java.lang.String s)
    {
        java.awt.FontMetrics fontmetrics = getFontMetrics(getFont());
        return (double)fontmetrics.getHeight();
    }

    protected void firePaint()
    {
        java.lang.Object aobj[] = listenerList.getListenerList();
        java.util.EventObject eventobject = null;
        for(int i = aobj.length - 2; i >= 0; i -= 2)
        {
            if(aobj[i] != (diamondedge.vb.RepaintListener.class))
                continue;
            if(eventobject == null)
                eventobject = new EventObject(this);
            ((diamondedge.vb.RepaintListener)aobj[i + 1]).repaintRequested(eventobject);
        }

    }

    public void addRepaintListener(diamondedge.vb.RepaintListener repaintlistener)
    {
        listenerList.add(diamondedge.vb.RepaintListener.class, repaintlistener);
    }

    public void removeRepaintListener(diamondedge.vb.RepaintListener repaintlistener)
    {
        listenerList.remove(diamondedge.vb.RepaintListener.class, repaintlistener);
    }

    protected javax.swing.ButtonGroup buttonGroup = null;
    private boolean an = false;
    private boolean am = false;
    private diamondedge.util.Variant ak = null;
    private java.awt.Image ao = null;
    private diamondedge.vb.VbGraphics al = null;
    private boolean ignoreRepaint = false;
}
