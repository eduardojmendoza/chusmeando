// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.io.PrintStream;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

// Referenced classes of package diamondedge.vb:
//            JForm, Screen, Application, MDIForm, 
//            Form

public class MDIChildForm extends diamondedge.vb.JForm
    implements javax.swing.event.InternalFrameListener
{

    public MDIChildForm()
    {
        showInProgress = false;
        frame = null;
        minButton = true;
        maxButton = true;
    }

    public void dispose()
    {
        super.dispose();
        if(frame != null)
            frame.dispose();
        frame = null;
    }

    public void setFormSize(int i, int j)
    {
        java.awt.Dimension dimension = new Dimension(i, j);
        setPreferredSize(dimension);
        setMinimumSize(dimension);
        if(frame != null)
            if(isVisible())
                frame.pack();
            else
                frame.setSize(i, j);
    }

    public int getFormX()
    {
        if(frame != null)
            return frame.getX();
        else
            return 0;
    }

    public int getFormY()
    {
        if(frame != null)
            return frame.getY();
        else
            return 0;
    }

    public void setFormLocation(int i, int j)
    {
        if(frame != null)
            frame.setLocation(i, j);
    }

    public void add(javax.swing.JMenu jmenu)
    {
        if(frame != null)
        {
            javax.swing.JMenuBar jmenubar = frame.getJMenuBar();
            if(jmenubar == null)
            {
                jmenubar = new JMenuBar();
                frame.setJMenuBar(jmenubar);
            }
            jmenubar.add(jmenu);
        }
    }

    public java.lang.String getTitle()
    {
        if(frame != null)
            return frame.getTitle();
        else
            return "";
    }

    public void setTitle(java.lang.String s)
    {
        if(frame != null)
            frame.setTitle(s);
    }

    public java.awt.Image getIcon()
    {
        if(frame != null && (frame.getFrameIcon() instanceof javax.swing.ImageIcon))
            return ((javax.swing.ImageIcon)frame.getFrameIcon()).getImage();
        else
            return null;
    }

    public void setIcon(java.lang.String s)
    {
        if(frame != null)
            frame.setFrameIcon(new ImageIcon(diamondedge.vb.Screen.loadImage(s)));
    }

    public void setIcon(java.awt.Image image)
    {
        if(frame != null)
            frame.setFrameIcon(new ImageIcon(image));
    }

    public int getBorderStyle()
    {
        if(frame != null)
            return !sizable ? 1 : 2;
        else
            return 2;
    }

    public void setBorderStyle(int i)
    {
        if(i == 2 || i == 5)
            sizable = true;
        else
            sizable = false;
        if(frame != null)
            frame.setResizable(sizable);
    }

    public int getWindowState()
    {
        if(frame != null)
        {
            if(frame.isIcon())
                return 1;
            if(frame.isMaximum())
                return 2;
        }
        return windowState;
    }

    public void setWindowState(int i)
    {
        windowState = i;
        if(frame == null || showInProgress)
            return;
        try
        {
            switch(i)
            {
            case 0: // '\0'
                frame.setMaximum(false);
                frame.setIcon(false);
                break;

            case 1: // '\001'
                frame.setIcon(true);
                break;

            case 2: // '\002'
                frame.setMaximum(true);
                break;
            }
        }
        catch(java.lang.Exception exception) { }
    }

    public boolean getMaxButton()
    {
        return maxButton;
    }

    public void setMaxButton(boolean flag)
    {
        maxButton = flag;
        if(frame != null)
            frame.setMaximizable(flag);
    }

    public boolean getMinButton()
    {
        return minButton;
    }

    public void setMinButton(boolean flag)
    {
        minButton = flag;
        if(frame != null)
            frame.setIconifiable(flag);
    }

    public javax.swing.JInternalFrame getInternalFrame()
    {
        return frame;
    }

    void a(java.lang.String s)
    {
        java.lang.System.out.println(s + " " + getName() + " " + frame.getBounds());
    }

    public void internalFrameClosing(javax.swing.event.InternalFrameEvent internalframeevent)
    {
        diamondedge.vb.Application.a(this);
    }

    public void internalFrameActivated(javax.swing.event.InternalFrameEvent internalframeevent)
    {
        diamondedge.vb.Screen.setActiveForm(this);
    }

    public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent internalframeevent)
    {
    }

    public void internalFrameOpened(javax.swing.event.InternalFrameEvent internalframeevent)
    {
    }

    public void internalFrameClosed(javax.swing.event.InternalFrameEvent internalframeevent)
    {
    }

    public void internalFrameIconified(javax.swing.event.InternalFrameEvent internalframeevent)
    {
    }

    public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent internalframeevent)
    {
    }

    public void showForm()
    {
        showForm(0, null);
    }

    public void showForm(int i, diamondedge.vb.Form form)
    {
        if(i == 1)
        {
            frame = null;
            super.showForm(i, form);
            return;
        }
        if(showInProgress)
            return;
        showInProgress = true;
        setVisible(false);
        if(!initialized)
            if(frame == null)
            {
                if(diamondedge.vb.MDIForm.mdiForm != null)
                {
                    javax.swing.JInternalFrame jinternalframe = new JInternalFrame(getTitle(), sizable, true, maxButton, minButton);
                    frame = jinternalframe;
                    jinternalframe.setDefaultCloseOperation(0);
                    jinternalframe.getContentPane().add(this, "Center");
                    jinternalframe.addInternalFrameListener(this);
                    init();
                    diamondedge.vb.MDIForm.mdiForm.add(jinternalframe);
                    setVisible(true);
                    jinternalframe.pack();
                    if(diamondedge.vb.MDIForm.mdiForm.getFrame() == null)
                        diamondedge.vb.MDIForm.mdiForm.showForm();
                    if(!diamondedge.vb.MDIForm.mdiForm.isVisible())
                        diamondedge.vb.MDIForm.mdiForm.setVisible(true);
                } else
                {
                    javax.swing.JFrame jframe = new JFrame();
                    jframe.getContentPane().add(this, "Center");
                    jframe.addWindowListener(this);
                    init();
                    jframe.pack();
                    jframe.setVisible(true);
                }
            } else
            {
                init();
            }
        if(!isVisible())
            setVisible(true);
        if(frame != null)
            frame.setVisible(true);
        showInProgress = false;
        setWindowState(windowState);
    }

    public void hideForm()
    {
        if(frame != null)
            frame.setVisible(false);
        else
            setVisible(false);
    }

    public void zOrder(int i)
    {
        if(frame == null)
            return;
        if(i == 0)
            frame.toFront();
        else
            frame.toBack();
    }

    public static final java.lang.String copyright = "Copyright 1996-2006 Diamond Edge, Inc.";
    protected boolean showInProgress = false;
    protected javax.swing.JInternalFrame frame = null;
    protected boolean minButton = false;
    protected boolean maxButton = false;
}
