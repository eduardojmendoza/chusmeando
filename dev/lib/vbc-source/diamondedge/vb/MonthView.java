// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.swing.DsDateView;
import java.util.Calendar;

public class MonthView extends diamondedge.swing.DsDateView
{

    public MonthView()
    {
    }

    public int getDay()
    {
        return getCalendar().get(5);
    }

    public void setDay(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(5, i);
        setCalendar(calendar);
    }

    public int getDayOfWeek()
    {
        return getCalendar().get(7);
    }

    public void setDayOfWeek(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(7, i);
        setCalendar(calendar);
    }

    public int getHour()
    {
        return getCalendar().get(11);
    }

    public void setHour(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(11, i);
        setCalendar(calendar);
    }

    public int getMinute()
    {
        return getCalendar().get(12);
    }

    public void setMinute(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(12, i);
        setCalendar(calendar);
    }

    public int getMonth()
    {
        return getCalendar().get(2) + 1;
    }

    public void setMonth(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(2, i - 1);
        setCalendar(calendar);
    }

    public int getSecond()
    {
        return getCalendar().get(13);
    }

    public void setSecond(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(13, i);
        setCalendar(calendar);
    }

    public int getWeek()
    {
        return getCalendar().get(3);
    }

    public void setWeek(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(3, i);
        setCalendar(calendar);
    }

    public int getYear()
    {
        return getCalendar().get(1);
    }

    public void setYear(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(1, i);
        setCalendar(calendar);
    }
}
