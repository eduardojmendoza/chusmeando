// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.PrintJob;
import java.awt.Toolkit;
import java.util.StringTokenizer;

public class Printer
{

    public Printer()
    {
    }

    private static boolean a()
    {
        if(_fldfor == null)
        {
            _fldfor = java.awt.Toolkit.getDefaultToolkit().getPrintJob(new Frame(), "VB Printer", null);
            if(_fldfor == null)
                _flddo = null;
            else
                _flddo = _fldfor.getGraphics();
        }
        return _flddo != null;
    }

    public static void print(java.lang.String s)
    {
        if(!diamondedge.vb.Printer.a())
            return;
        java.awt.Color color = _fldint;
        if(color == null)
            color = java.awt.Color.black;
        _flddo.setColor(color);
        java.awt.FontMetrics fontmetrics = _flddo.getFontMetrics();
        byte byte0 = 20;
        int i = _fldif + byte0;
        int j = a + fontmetrics.getAscent() + byte0;
        for(java.util.StringTokenizer stringtokenizer = new StringTokenizer(s, "\n\r"); stringtokenizer.hasMoreTokens();)
        {
            _flddo.drawString(stringtokenizer.nextToken(), i, j);
            _fldif = 0;
            a += fontmetrics.getHeight() + 1;
            i = byte0;
            j += fontmetrics.getHeight() + 1;
        }

    }

    public static void endDoc()
    {
        if(_fldfor != null)
        {
            _fldfor.end();
            _fldfor = null;
        }
        if(_flddo != null)
        {
            _flddo.dispose();
            _flddo = null;
        }
    }

    private static int _fldif = 0;
    private static int a = 0;
    private static java.awt.Color _fldint = null;
    private static java.awt.PrintJob _fldfor = null;
    private static java.awt.Graphics _flddo = null;

    static 
    {
        _fldint = java.awt.Color.black;
    }
}
