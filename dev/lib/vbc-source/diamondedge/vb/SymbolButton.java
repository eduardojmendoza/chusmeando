// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.UIManager;

public class SymbolButton extends javax.swing.JButton
{

    public SymbolButton()
    {
        this(2);
    }

    public SymbolButton(int i)
    {
        _fldnew = java.awt.Color.black;
        _fldfor = i;
        setText(null);
        setMinimumSize(_fldint);
        setMaximumSize(_fldtry);
        setPreferredSize(_fldif);
        setRequestFocusEnabled(false);
        setDefaultCapable(false);
        updateUI();
    }

    public void updateUI()
    {
        super.updateUI();
        _flddo = javax.swing.UIManager.getColor("controlShadow");
        a = javax.swing.UIManager.getColor("controlLtHighlight");
    }

    public int getSymbol()
    {
        return _fldfor;
    }

    public void setSymbol(int i)
    {
        _fldfor = i;
    }

    public java.awt.Paint getSymbolColor()
    {
        return _fldnew;
    }

    public void setSymbolColor(java.awt.Paint paint)
    {
        _fldnew = paint;
    }

    public void setForeground(java.awt.Color color)
    {
        _fldnew = color;
        super.setForeground(color);
    }

    public void setText(java.lang.String s)
    {
    }

    public void paintComponent(java.awt.Graphics g)
    {
        super.paintComponent(g);
        boolean flag = getModel().isPressed();
        boolean flag1 = isEnabled();
        int i = getWidth();
        int j = getHeight();
        if(flag)
            g.translate(1, 1);
        ((java.awt.Graphics2D)g).setPaint(_fldnew);
        if(flag1)
        {
            diamondedge.vb.SymbolButton.a(g, _fldfor, i, j);
        } else
        {
            g.translate(1, 1);
            g.setColor(a);
            diamondedge.vb.SymbolButton.a(g, _fldfor, i, j);
            g.translate(-1, -1);
            g.setColor(_flddo);
            diamondedge.vb.SymbolButton.a(g, _fldfor, i, j);
        }
        if(flag)
            g.translate(-1, -1);
    }

    private static void a(java.awt.Graphics g, int i, int j, int k)
    {
        int l = java.lang.Math.min((j / 3) * 2, (k / 3) * 2);
        l--;
        if(i == 11 || i == 10)
        {
            diamondedge.vb.SymbolButton.a(g, (j - l) / 2, (k - l / 2) / 2, l, i);
            return;
        }
        if(i == 6)
        {
            l--;
            g.fillRect((j - l) / 2, (k - l) / 2, l, l);
            return;
        }
        if(i == 7)
        {
            l--;
            g.fillOval((j - l) / 2, (k - l) / 2, l, l);
            return;
        }
        if(i == 12 || i == 13)
        {
            if(i == 12)
                g.fillRect(j / 2 - 2, (k - l) / 2, 3, l);
            g.fillRect((j - l) / 2, k / 2 - 2, l, 3);
            return;
        }
        int i1 = (l + 1) / 2;
        int j1 = (j - i1) / 2;
        int k1 = (k - l) / 2;
        int l1 = (j1 + i1) - 1;
        int i2 = (k1 + l) - 1;
        if(i == 1 || i == 5)
        {
            j1--;
            if(i != 5)
                j1--;
            g.drawLine(j1, k1, j1++, i2);
            g.drawLine(j1, k1, j1++, i2);
            j1++;
            l1++;
        }
        if(i == 9)
        {
            j1 = (j - i1 * 2) / 2 - 1;
            diamondedge.vb.SymbolButton.a(g, j1, k1, l, 0);
            j1 += i1;
        }
        if(i == 0 || i == 1 || i == 9)
            diamondedge.vb.SymbolButton.a(g, j1, k1, l, 0);
        if(i == 3 || i == 5)
        {
            if(i != 5)
                l1++;
            g.drawLine(l1, k1, l1, i2);
            l1--;
            g.drawLine(l1, k1, l1, i2);
            j1 -= 2;
            l1 -= 2;
        }
        if(i == 4)
        {
            g.setFont(new Font("SanSerif", 0, java.lang.Math.min(j, k)));
            java.awt.FontMetrics fontmetrics = g.getFontMetrics();
            j1 = (j - i1 - fontmetrics.stringWidth("*")) / 2;
            g.drawString("*", j1 + i1 + 1, k);
        }
        if(i == 8)
        {
            j1 = (j - 2 * i1) / 2;
            diamondedge.vb.SymbolButton.a(g, j1, k1, l, 2);
            j1 += i1;
        }
        if(i == 2 || i == 3 || i == 4 || i == 8)
            diamondedge.vb.SymbolButton.a(g, j1, k1, l, 2);
    }

    private static void a(java.awt.Graphics g, int i, int j, int k, int l)
    {
        boolean flag = false;
        int i1 = ++k / 2;
        i1 += i1 % 2 - 1;
        k = java.lang.Math.max(k / 2, 2);
        g.translate(i, j);
        switch(l)
        {
        default:
            break;

        case 10: // '\n'
            for(int j1 = 0; j1 < k; j1++)
                g.drawLine(i1 - j1, j1, i1 + j1, j1);

            break;

        case 11: // '\013'
            int j2 = 0;
            for(int k1 = k - 1; k1 >= 0; k1--)
            {
                g.drawLine(i1 - k1, j2, i1 + k1, j2);
                j2++;
            }

            break;

        case 0: // '\0'
            for(int l1 = 0; l1 < k; l1++)
                g.drawLine(l1, i1 - l1, l1, i1 + l1);

            break;

        case 2: // '\002'
            int k2 = 0;
            for(int i2 = k - 1; i2 >= 0; i2--)
            {
                g.drawLine(k2, i1 - i2, k2, i1 + i2);
                k2++;
            }

            break;
        }
        g.translate(-i, -j);
    }

    public static final int LEFT_ARROW = 0;
    public static final int LEFT_ARROW_LINE = 1;
    public static final int RIGHT_ARROW = 2;
    public static final int RIGHT_ARROW_LINE = 3;
    public static final int RIGHT_ARROW_ASTERISK = 4;
    public static final int DOUBLE_LINE = 5;
    public static final int SQUARE = 6;
    public static final int CIRCLE = 7;
    public static final int DOUBLE_RIGHT_ARROW = 8;
    public static final int DOUBLE_LEFT_ARROW = 9;
    public static final int UP_ARROW = 10;
    public static final int DOWN_ARROW = 11;
    public static final int PLUS = 12;
    public static final int MINUS = 13;
    private int _fldfor = 0;
    private java.awt.Paint _fldnew = null;
    private static java.awt.Color _flddo = null;
    private static java.awt.Color a = null;
    private static java.awt.Insets _fldbyte = new Insets(0, 0, 0, 0);
    private static java.awt.Dimension _fldif = new Dimension(18, 20);
    private static java.awt.Dimension _fldint = new Dimension(6, 6);
    private static java.awt.Dimension _fldtry = new Dimension(4681, 4681);

}
