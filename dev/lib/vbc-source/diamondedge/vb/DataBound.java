// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.util.EventListener;

// Referenced classes of package diamondedge.vb:
//            DataChangeEvent, DataSource

public interface DataBound
    extends java.util.EventListener
{

    public abstract void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent);

    public abstract diamondedge.util.Variant getDataValue();

    public abstract java.lang.String getDataField();

    public abstract void setDataField(java.lang.String s);

    public abstract diamondedge.vb.DataSource getDataSource();

    public abstract void setDataSource(diamondedge.vb.DataSource datasource);
}
