// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import javax.swing.JPasswordField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

// Referenced classes of package diamondedge.vb:
//            DataBound, TextBoxCtrl, VbControl, Screen, 
//            DataSource, DataBoundRouter, DataChangeEvent

public class PasswordTextBox extends javax.swing.JPasswordField
    implements javax.swing.event.DocumentListener, diamondedge.vb.DataBound, diamondedge.vb.TextBoxCtrl, diamondedge.vb.VbControl
{
    private class a extends javax.swing.text.PlainDocument
    {

        public void insertString(int i, java.lang.String s, javax.swing.text.AttributeSet attributeset)
            throws javax.swing.text.BadLocationException
        {
            if(s == null)
                return;
            if(J == 0 || getLength() + s.length() <= J)
                super.insertString(i, s, attributeset);
            else
            if(getLength() < J)
            {
                s = s.substring(0, J - getLength());
                super.insertString(i, s, attributeset);
            } else
            {
                java.awt.Toolkit.getDefaultToolkit().beep();
            }
        }

        private a()
        {
        }

    }


    public PasswordTextBox()
    {
        J = 0;
        L = null;
        setFont(diamondedge.vb.Screen.getDefaultFont());
        setCursor(java.awt.Cursor.getPredefinedCursor(2));
        setHorizontalAlignment(2);
        getDocument().addDocumentListener(this);
    }

    public java.lang.String getTag()
    {
        return I;
    }

    public void setTag(java.lang.String s)
    {
        I = s;
    }

    public int getTabOrder()
    {
        return K;
    }

    public void setTabOrder(int i)
    {
        K = i;
    }

    public java.lang.String toString()
    {
        javax.swing.text.Document document = getDocument();
        java.lang.String s;
        try
        {
            s = document.getText(0, document.getLength());
        }
        catch(javax.swing.text.BadLocationException badlocationexception)
        {
            s = null;
        }
        return s;
    }

    public void setMaxLength(int i)
    {
        J = i;
    }

    protected javax.swing.text.Document createDefaultModel()
    {
        return new a();
    }

    protected void fireDocumentChanged(javax.swing.event.DocumentEvent documentevent)
    {
        java.lang.Object aobj[] = listenerList.getListenerList();
        java.awt.event.TextEvent textevent = null;
        for(int i = aobj.length - 2; i >= 0; i -= 2)
        {
            if(aobj[i] != (java.awt.event.TextListener.class))
                continue;
            if(textevent == null)
                textevent = new TextEvent(this, 900);
            ((java.awt.event.TextListener)aobj[i + 1]).textValueChanged(textevent);
        }

    }

    public void changedUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void insertUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void removeUpdate(javax.swing.event.DocumentEvent documentevent)
    {
        fireDocumentChanged(documentevent);
    }

    public void addTextListener(java.awt.event.TextListener textlistener)
    {
        listenerList.add(java.awt.event.TextListener.class, textlistener);
    }

    public void removeTextListener(java.awt.event.TextListener textlistener)
    {
        listenerList.remove(java.awt.event.TextListener.class, textlistener);
    }

    public java.lang.String getDataField()
    {
        return M;
    }

    public void setDataField(java.lang.String s)
    {
        M = s;
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return L;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        if(L != null)
        {
            L.removeDataBound(this);
            removeTextListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        if(datasource != null)
        {
            datasource.addDataBound(this);
            addTextListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        L = datasource;
    }

    public diamondedge.util.Variant getDataValue()
    {
        return new Variant(toString());
    }

    public void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent)
    {
        setText(datachangeevent.getValue() != null ? datachangeevent.getValue().toString() : "");
    }

    private java.lang.String I = null;
    private int K = 0;
    private int J = 0;
    private java.lang.String M = null;
    private diamondedge.vb.DataSource L = null;

}
