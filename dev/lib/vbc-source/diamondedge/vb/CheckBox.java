// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import javax.swing.JCheckBox;

// Referenced classes of package diamondedge.vb:
//            DataBound, VbControl, Screen, JForm, 
//            DataSource, DataBoundRouter, DataChangeEvent

public class CheckBox extends javax.swing.JCheckBox
    implements diamondedge.vb.DataBound, diamondedge.vb.VbControl
{

    public CheckBox()
    {
        bc = null;
        setFont(diamondedge.vb.Screen.getDefaultFont());
        setHorizontalTextPosition(4);
    }

    public void setValue(boolean flag)
    {
        if(isSelected() != flag)
            if(isEnabled())
                doClick();
            else
                setSelected(flag);
    }

    public void setName(java.lang.String s)
    {
        super.setName(s);
        setSelected(false);
    }

    public int getAlignment()
    {
        return getHorizontalTextPosition() != 4 ? 4 : 2;
    }

    public void setAlignment(int i)
    {
        setHorizontalTextPosition(i != 4 ? 4 : 2);
    }

    public void setAlignGraphical(boolean flag)
    {
        if(flag)
        {
            setHorizontalTextPosition(0);
            setVerticalTextPosition(3);
        } else
        {
            setHorizontalTextPosition(4);
            setVerticalTextPosition(0);
        }
    }

    public void setText(java.lang.String s)
    {
        super.setText(diamondedge.vb.JForm.processMnemonic(this, s));
    }

    public java.lang.String getTag()
    {
        return ba;
    }

    public void setTag(java.lang.String s)
    {
        ba = s;
    }

    public int getTabOrder()
    {
        return bb;
    }

    public void setTabOrder(int i)
    {
        bb = i;
    }

    public java.lang.String getDataField()
    {
        return bd;
    }

    public void setDataField(java.lang.String s)
    {
        bd = s;
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return bc;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        if(bc != null)
        {
            bc.removeDataBound(this);
            removeItemListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        if(datasource != null)
        {
            datasource.addDataBound(this);
            addItemListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        bc = datasource;
    }

    public diamondedge.util.Variant getDataValue()
    {
        return new Variant(isSelected());
    }

    public void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent)
    {
        setValue(datachangeevent.getValue() != null ? datachangeevent.getValue().toBoolean() : false);
    }

    private java.lang.String ba = null;
    private int bb = 0;
    private java.lang.String bd = null;
    private diamondedge.vb.DataSource bc = null;
}
