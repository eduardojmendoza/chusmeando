// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.text.ParseException;
import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.LookAndFeel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.JTextComponent;

// Referenced classes of package diamondedge.vb:
//            MultiLineTextBox, VbControl

public class UpDown extends javax.swing.JSpinner
    implements diamondedge.vb.VbControl, javax.swing.event.ChangeListener
{

    public UpDown()
    {
        ab = 0;
        ac = (javax.swing.SpinnerNumberModel)getModel();
        setMin(0);
        setMax(10);
    }

    public boolean hasChangedUp()
    {
        return getVal() > ab;
    }

    public int getVal()
    {
        return ac.getNumber().intValue();
    }

    public void setVal(int i)
    {
        ab = getVal();
        ac.setValue(new Integer(i));
    }

    public void setValue(java.lang.Object obj)
    {
        ab = getVal();
        ac.setValue(new Integer((new Variant(obj)).toInt()));
    }

    public int getIncrement()
    {
        return ac.getStepSize().intValue();
    }

    public void setIncrement(int i)
    {
        ac.setStepSize(new Integer(i));
    }

    public int getMax()
    {
        return ((java.lang.Integer)ac.getMaximum()).intValue();
    }

    public void setMax(int i)
    {
        ac.setMaximum(new Integer(i));
    }

    public int getMin()
    {
        return ((java.lang.Integer)ac.getMinimum()).intValue();
    }

    public void setMin(int i)
    {
        ac.setMinimum(new Integer(i));
    }

    public int getTabOrder()
    {
        return ad;
    }

    public void setTabOrder(int i)
    {
        ad = i;
    }

    public void setEditor(javax.swing.JComponent jcomponent)
    {
        addChangeListener(this);
        super.setEditor(jcomponent);
    }

    public void commitEdit()
        throws java.text.ParseException
    {
        javax.swing.JComponent jcomponent = getEditor();
        if(jcomponent != null && aa != null)
            try
            {
                setVal(diamondedge.util.Variant.getValue(jcomponent, aa).toInt());
            }
            catch(java.lang.Exception exception)
            {
                throw new ParseException(exception.getMessage(), 0);
            }
        else
        if(jcomponent instanceof javax.swing.text.JTextComponent)
        {
            java.lang.String s = ((javax.swing.text.JTextComponent)jcomponent).getText();
            setVal(java.lang.Integer.parseInt(s));
        } else
        if(jcomponent instanceof diamondedge.vb.MultiLineTextBox)
        {
            java.lang.String s1 = ((diamondedge.vb.MultiLineTextBox)jcomponent).getText();
            setVal(java.lang.Integer.parseInt(s1));
        }
    }

    public java.lang.String getEditorProperty()
    {
        return aa;
    }

    public void setEditorProperty(java.lang.String s)
    {
        aa = s;
    }

    public void stateChanged(javax.swing.event.ChangeEvent changeevent)
    {
        javax.swing.JSpinner jspinner = (javax.swing.JSpinner)changeevent.getSource();
        javax.swing.JComponent jcomponent = getEditor();
        java.lang.Object obj = jspinner.getValue();
        if(jcomponent != null && aa != null)
            try
            {
                diamondedge.util.Variant.setValue(jcomponent, aa, new Variant(obj));
            }
            catch(java.lang.Exception exception)
            {
                javax.swing.UIManager.getLookAndFeel().provideErrorFeedback(this);
            }
        else
        if(jcomponent instanceof javax.swing.text.JTextComponent)
            ((javax.swing.text.JTextComponent)jcomponent).setText(obj.toString());
        else
        if(jcomponent instanceof diamondedge.vb.MultiLineTextBox)
            ((diamondedge.vb.MultiLineTextBox)jcomponent).setText(obj.toString());
    }

    private javax.swing.SpinnerNumberModel ac = null;
    private java.lang.String aa = null;
    private int ab = 0;
    private int ad = 0;
}
