// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Strings;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.Vector;
import javax.swing.DefaultListModel;
import javax.swing.JList;

// Referenced classes of package diamondedge.vb:
//            ListBox, DirectoryFilter

public class DirListBox extends diamondedge.vb.ListBox
    implements java.awt.event.MouseListener
{

    public DirListBox()
    {
        t = null;
        s = new Vector();
        setPath(java.lang.System.getProperty("user.dir"));
        getList().addMouseListener(this);
    }

    public java.lang.Object getItemAt(int i)
    {
        if(getSelectedIndex() < 0)
            return null;
        else
            return s.get(i);
    }

    public java.lang.String getItem(int i)
    {
        if(getSelectedIndex() < 0)
        {
            return t;
        } else
        {
            java.lang.Object obj = s.get(i);
            return obj != null ? obj.toString() : "";
        }
    }

    public java.lang.String getPath()
    {
        return t;
    }

    public void setPath(java.lang.String s1)
    {
        t = s1;
        javax.swing.DefaultListModel defaultlistmodel = getModel();
        defaultlistmodel.removeAllElements();
        s.removeAllElements();
        java.io.File file = new File(s1);
        for(java.io.File file1 = file; (file1 = file1.getParentFile()) != null;)
            s.add(0, file1);

        s.add(file);
        for(int i = 0; i < s.size(); i++)
        {
            java.io.File file2 = (java.io.File)s.get(i);
            defaultlistmodel.addElement(diamondedge.util.Strings.space(i * 2) + (i != 0 ? file2.getName() : file2.getAbsolutePath()));
        }

        int j = s.size() * 2;
        java.io.File afile[] = file.listFiles(new DirectoryFilter());
        for(int k = 0; k < afile.length; k++)
        {
            defaultlistmodel.addElement(diamondedge.util.Strings.space(j) + afile[k].getName());
            s.add(afile[k]);
        }

    }

    public void mouseClicked(java.awt.event.MouseEvent mouseevent)
    {
        if(mouseevent.getSource() == getList() && mouseevent.getClickCount() > 1)
            setPath(getItem(getSelectedIndex()));
    }

    public void mousePressed(java.awt.event.MouseEvent mouseevent)
    {
    }

    public void mouseReleased(java.awt.event.MouseEvent mouseevent)
    {
    }

    public void mouseEntered(java.awt.event.MouseEvent mouseevent)
    {
    }

    public void mouseExited(java.awt.event.MouseEvent mouseevent)
    {
    }

    private java.lang.String t = null;
    java.util.Vector s = null;
}
