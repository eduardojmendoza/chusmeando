// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.ado.AdoError;
import diamondedge.ado.Field;
import diamondedge.ado.Fields;
import diamondedge.ado.Recordset;
import diamondedge.swing.DsListItem;
import diamondedge.swing.DsListView;
import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
import java.io.PrintStream;
import javax.swing.table.TableColumn;

// Referenced classes of package diamondedge.vb:
//            Data, DataBound, VbControl, Screen, 
//            MultiColumnComboBox, DataSource, DataBoundRouter, DataChangeEvent

public class MultiColumnListBox extends diamondedge.swing.DsListView
    implements diamondedge.vb.DataBound, diamondedge.vb.VbControl
{

    public MultiColumnListBox()
    {
        super(3);
        aU = false;
        aS = null;
        aP = null;
        aQ = null;
        aR = null;
        aW = false;
        setFont(diamondedge.vb.Screen.getDefaultFont());
        setHighlightPolicy(1);
    }

    public int getBoundColumn()
    {
        return aT;
    }

    public void setBoundColumn(int i)
    {
        aT = i;
    }

    public void setColumnReorderingAllowed(boolean flag)
    {
    }

    public java.lang.Object getValue()
    {
        if(getBoundColumn() < 0)
            return new Integer(getSelectedIndex());
        if(getSelectedIndex() >= 0)
            return getValueAt(getSelectedIndex(), getBoundColumn());
        else
            return null;
    }

    public void setValue(java.lang.Object obj)
    {
        if(getBoundColumn() < 0)
        {
            int i = diamondedge.util.Obj.toInt(obj);
            if(i < 0 || i >= getItemCount())
                i = -1;
            setSelectedIndex(i);
        } else
        {
            java.lang.String s = obj != null ? obj.toString() : "";
            int j = findItem(s, getBoundColumn(), 0, false);
            setSelectedIndex(j);
        }
    }

    public java.lang.Object getValueAt(int i, int j)
    {
        if(i < 0)
            i = getSelectedIndex();
        return super.getValueAt(i, j);
    }

    public java.lang.String getTextAt(int i, int j)
    {
        if(i < 0)
            i = getSelectedIndex();
        return super.getTextAt(i, j);
    }

    private diamondedge.swing.DsListItem a(java.lang.String s, int i)
    {
        int j = findItem(s, i, 0, false);
        if(j >= 0)
            return getItem(j);
        else
            return null;
    }

    public java.lang.String getColumnWidths()
    {
        return aS;
    }

    public void setColumnWidths(java.lang.String s)
    {
        aS = s;
        _mthint();
    }

    private void _mthint()
    {
        if(aS == null || aS.length() == 0)
        {
            if(getWidth() > 0)
            {
                int i = getWidth() / getColumnCount();
                for(int k = 0; k < getColumnCount(); k++)
                    setColumnWidth(k, i);

            }
        } else
        {
            int j = 0;
            java.lang.String as[] = diamondedge.util.Strings.split(aS, ";", -1);
            for(int l = 0; l < as.length; l++)
            {
                int i1 = diamondedge.vb.MultiColumnComboBox.a(as[l], false);
                if(i1 < 0 || as[l].equals(""))
                    i1 = 96;
                if(j < getColumnCount())
                    setColumnWidth(j, i1);
                j++;
            }

        }
    }

    public java.lang.String getRowValueList()
    {
        return aP;
    }

    public void setRowValueList(java.lang.String s)
    {
        try
        {
            aP = s;
            clear();
            java.lang.String as[] = diamondedge.util.Strings.split(aP, ";", -1);
            addValues(as);
            setSelectedIndex(-1);
        }
        catch(java.lang.Exception exception)
        {
            diamondedge.util.Err.set(exception, "setRowValueList");
        }
    }

    public void addValues(java.lang.Object aobj[])
    {
        int i = getColumnCount();
        if(i < 1)
            i = 1;
        int j = 0;
        if(isColumnHeaderVisible())
            j = -1;
        for(int k = 0; k < aobj.length;)
        {
            for(int l = 0; l < i; l++)
            {
                if(j == -1)
                    getColumn(l).setHeaderValue(aobj[k]);
                else
                if(l == 0)
                    addItem(-1, null, aobj[k], null, null);
                else
                    setValueAt(aobj[k], j, l);
                k++;
            }

            j++;
        }

        repaint();
    }

    public java.lang.String getTag()
    {
        return aY;
    }

    public void setTag(java.lang.String s)
    {
        aY = s;
    }

    public int getTabOrder()
    {
        return aV;
    }

    public void setTabOrder(int i)
    {
        aV = i;
    }

    public diamondedge.vb.DataSource getRowSource()
    {
        return aR;
    }

    public void setRowSource(diamondedge.vb.DataSource datasource)
    {
        if(aR != null)
            aR.removeDataBound(this);
        if(datasource != null)
        {
            datasource.addDataBound(this);
            aW = false;
        }
        aR = datasource;
    }

    public void setRowSource(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        if(aR instanceof diamondedge.vb.Data)
            ((diamondedge.vb.Data)aR).setRecordSource(s);
        else
        if(aR != null)
        {
            diamondedge.ado.Recordset recordset = aR.getRecordset();
            if(recordset != null)
            {
                recordset.close();
                recordset.open(s);
            }
        }
    }

    public java.lang.String getDataField()
    {
        return aX;
    }

    public void setDataField(java.lang.String s)
    {
        aX = s;
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return aQ;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        if(aQ != null)
        {
            aQ.removeDataBound(this);
            removeItemListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        if(datasource != null)
        {
            datasource.addDataBound(this);
            addItemListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        aQ = datasource;
    }

    public diamondedge.util.Variant getDataValue()
    {
        return new Variant(getValue());
    }

    public void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent)
    {
        diamondedge.util.Variant variant = datachangeevent.getValue();
        if(aR != null)
        {
            if(aQ != null && datachangeevent.getRecordset() == aQ.getRecordset())
                setValue(variant);
            else
            if(datachangeevent.getChangeType() == 7 || datachangeevent.getChangeType() == 1)
                _mthnew();
        } else
        {
            setValue(variant);
        }
    }

    private void _mthnew()
    {
        if(!aW && aR != null)
        {
            diamondedge.ado.Recordset recordset = aR.getRecordset();
            a(recordset);
        }
    }

    private void a(diamondedge.ado.Recordset recordset)
    {
        clear();
        if(recordset == (diamondedge.ado.Recordset)null)
            return;
        int i;
        int k;
        int l;
        i = recordset.getFields().getCount();
        setColumnCount(i);
        for(int j = 0; j < i; j++)
        {
            java.lang.String s = recordset.getField(j).getName();
            setColumnName(j, s);
        }

        Object obj = null;
        k = recordset.getAbsolutePosition();
        l = recordset.getRecordCount(true);
        recordset.moveFirst();
        if(l == 0)
            return;
        try
        {
            for(int i1 = 1; i1 <= l; i1++)
            {
                diamondedge.swing.DsListViewItem dslistviewitem = addItem(-1, null, _mthif(recordset.getField(0).getValue()), null, null);
                for(int j1 = 1; j1 < i; j1++)
                    dslistviewitem.setCellValue(j1, _mthif(recordset.getField(j1).getValue()));

                recordset.moveNext();
            }

            if(k != recordset.getAbsolutePosition())
                try
                {
                    recordset.setAbsolutePosition(k);
                }
                catch(java.lang.Exception exception1) { }
            aW = true;
        }
        catch(java.lang.Exception exception)
        {
            java.lang.System.out.println("MultiColumnListBox.fillList:" + exception);
            exception.printStackTrace();
        }
        return;
    }

    private java.lang.String _mthif(diamondedge.util.Variant variant)
    {
        if(variant.isNull())
            return "";
        if(variant.isDate())
            return diamondedge.util.DateTime.format(variant.toDate());
        else
            return variant.toString().trim();
    }

    public void clear()
    {
        diamondedge.vb.DataSource datasource = aQ;
        aQ = null;
        aU = true;
        super.clear();
        aU = false;
        aQ = datasource;
    }

    private java.lang.String aY = null;
    private int aV = 0;
    private int aT = 0;
    private boolean aU = false;
    private java.lang.String aS = null;
    private java.lang.String aP = null;
    private java.lang.String aX = null;
    private diamondedge.vb.DataSource aQ = null;
    private diamondedge.vb.DataSource aR = null;
    private boolean aW = false;
}
