// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Component;
import java.awt.Container;
import java.util.Comparator;
import javax.swing.JViewport;

// Referenced classes of package diamondedge.vb:
//            VbControl, ScrolledComponent

final class VbTabOrderComparator
    implements java.util.Comparator
{

    VbTabOrderComparator(java.util.Comparator comparator)
    {
        a = null;
        a = comparator;
    }

    public int compare(java.lang.Object obj, java.lang.Object obj1)
    {
        if(obj == obj1)
            return 0;
        if(obj == null)
            return -1;
        if(obj1 == null)
            return 1;
        if(!(obj instanceof diamondedge.vb.VbControl) && (((java.awt.Component)obj).getParent() instanceof diamondedge.vb.VbControl))
            obj = ((java.awt.Component)obj).getParent();
        if(!(obj1 instanceof diamondedge.vb.VbControl) && (((java.awt.Component)obj1).getParent() instanceof diamondedge.vb.VbControl))
            obj1 = ((java.awt.Component)obj1).getParent();
        if((((java.awt.Component)obj).getParent() instanceof javax.swing.JViewport) && (((java.awt.Component)obj).getParent().getParent() instanceof diamondedge.vb.ScrolledComponent))
            obj = ((java.awt.Component)obj).getParent().getParent();
        if((((java.awt.Component)obj1).getParent() instanceof javax.swing.JViewport) && (((java.awt.Component)obj1).getParent().getParent() instanceof diamondedge.vb.ScrolledComponent))
            obj1 = ((java.awt.Component)obj1).getParent().getParent();
        if((obj instanceof diamondedge.vb.VbControl) && (obj1 instanceof diamondedge.vb.VbControl))
            return ((diamondedge.vb.VbControl)obj).getTabOrder() >= ((diamondedge.vb.VbControl)obj1).getTabOrder() ? 1 : -1;
        if((obj instanceof java.awt.Component) && (obj1 instanceof java.awt.Component))
        {
            int i = 0;
            java.awt.Component component = (java.awt.Component)obj;
            java.awt.Component component1 = (java.awt.Component)obj1;
            int j = 0;
            int k = 0;
            java.awt.Container container = component.getParent();
            if(container != null)
            {
                int l = container.getComponentCount();
                for(int j1 = 0; j1 < l; j1++)
                {
                    java.awt.Component component2 = container.getComponent(j1);
                    if(component == component2)
                        j = j1;
                    if((component2 instanceof diamondedge.vb.VbControl) && ((diamondedge.vb.VbControl)component2).getTabOrder() > i)
                        i = ((diamondedge.vb.VbControl)component2).getTabOrder();
                }

            }
            java.awt.Container container1 = component1.getParent();
            if(container1 != null)
            {
                int i1 = container1.getComponentCount();
                for(int k1 = 0; k1 < i1; k1++)
                {
                    java.awt.Component component3 = container1.getComponent(k1);
                    if(component1 == component3)
                        k = k1;
                    if((component3 instanceof diamondedge.vb.VbControl) && ((diamondedge.vb.VbControl)component3).getTabOrder() > i)
                        i = ((diamondedge.vb.VbControl)component3).getTabOrder();
                }

            }
            i++;
            if(!(component instanceof diamondedge.vb.VbControl))
                j += i;
            if(!(component1 instanceof diamondedge.vb.VbControl))
                k += i;
            return j >= k ? 1 : -1;
        } else
        {
            return obj != null ? 1 : -1;
        }
    }

    java.util.Comparator a = null;
}
