// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import com.sun.java.swing.plaf.windows.WindowsProgressBarUI;
import javax.swing.JProgressBar;

// Referenced classes of package diamondedge.vb:
//            SmoothProgressBarUI, Screen

public class ProgressBar extends javax.swing.JProgressBar
{

    public ProgressBar()
    {
        setFont(diamondedge.vb.Screen.getDefaultFont());
    }

    public java.lang.String getTag()
    {
        return a;
    }

    public void setTag(java.lang.String s)
    {
        a = s;
    }

    public void setValue(int i)
    {
        if(i != getValue())
        {
            super.setValue(i);
            paintImmediately(0, 0, getWidth(), getHeight());
        }
    }

    public boolean isSmoothScrolling()
    {
        if(getUI() instanceof diamondedge.vb.SmoothProgressBarUI)
        {
            diamondedge.vb.SmoothProgressBarUI smoothprogressbarui = (diamondedge.vb.SmoothProgressBarUI)getUI();
            return smoothprogressbarui.isSmoothScrolling();
        } else
        {
            return false;
        }
    }

    public void setSmoothScrolling(boolean flag)
    {
        if(flag && (getUI() instanceof com.sun.java.swing.plaf.windows.WindowsProgressBarUI))
        {
            diamondedge.vb.SmoothProgressBarUI smoothprogressbarui = new SmoothProgressBarUI();
            setUI(smoothprogressbarui);
            smoothprogressbarui.setSmoothScrolling(true);
        } else
        if(getUI() instanceof diamondedge.vb.SmoothProgressBarUI)
        {
            diamondedge.vb.SmoothProgressBarUI smoothprogressbarui1 = (diamondedge.vb.SmoothProgressBarUI)getUI();
            smoothprogressbarui1.setSmoothScrolling(flag);
        }
    }

    private java.lang.String a = null;
}
