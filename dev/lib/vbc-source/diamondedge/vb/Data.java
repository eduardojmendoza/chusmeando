// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.ado.AdoError;
import diamondedge.ado.Connection;
import diamondedge.ado.Recordset;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.event.EventListenerList;

// Referenced classes of package diamondedge.vb:
//            SymbolButton, DataBound, DataSource

public class Data extends javax.swing.JComponent
    implements diamondedge.vb.DataSource, java.awt.event.ActionListener
{

    public Data()
    {
        label = new JLabel();
        MoveFirstBtn = new SymbolButton(1);
        MovePrevBtn = new SymbolButton(0);
        MoveNextBtn = new SymbolButton(2);
        MoveLastBtn = new SymbolButton(3);
        btnSize = new Dimension(20, 23);
        noMargin = new Insets(0, 0, 0, 0);
        con = null;
        rs = null;
        cursorType = 3;
        lockType = 3;
        commandType = -1;
        maxRecords = 0;
        BOFAction = 0;
        EOFAction = 0;
        label.setBackground(java.awt.Color.white);
        label.setOpaque(true);
        label.setHorizontalAlignment(0);
        label.setText("");
        MoveFirstBtn.setMaximumSize(btnSize);
        MoveFirstBtn.setMinimumSize(btnSize);
        MoveFirstBtn.setPreferredSize(btnSize);
        MovePrevBtn.setMaximumSize(btnSize);
        MovePrevBtn.setMinimumSize(btnSize);
        MovePrevBtn.setPreferredSize(btnSize);
        MoveNextBtn.setMaximumSize(btnSize);
        MoveNextBtn.setMinimumSize(btnSize);
        MoveNextBtn.setPreferredSize(btnSize);
        MoveLastBtn.setMaximumSize(btnSize);
        MoveLastBtn.setMinimumSize(btnSize);
        MoveLastBtn.setPreferredSize(btnSize);
        setLayout(new GridBagLayout());
        setBorder(javax.swing.BorderFactory.createLoweredBevelBorder());
        add(MoveFirstBtn, new GridBagConstraints(0, 0, 1, 1, 0.0D, 0.0D, 10, 3, noMargin, 0, 0));
        add(MovePrevBtn, new GridBagConstraints(1, 0, 1, 1, 0.0D, 0.0D, 10, 3, noMargin, 0, 0));
        add(label, new GridBagConstraints(2, 0, 1, 1, 22D, 22D, 10, 1, noMargin, 21, 12));
        add(MoveNextBtn, new GridBagConstraints(3, 0, 1, 1, 0.0D, 0.0D, 10, 3, noMargin, 0, 0));
        add(MoveLastBtn, new GridBagConstraints(4, 0, 1, 1, 0.0D, 0.0D, 10, 3, noMargin, 0, 0));
        MoveFirstBtn.addActionListener(this);
        MovePrevBtn.addActionListener(this);
        MoveNextBtn.addActionListener(this);
        MoveLastBtn.addActionListener(this);
    }

    public static void initRDC(diamondedge.vb.Data data)
    {
        data.commandType = 1;
    }

    public void showAddNewButton(boolean flag)
    {
        if(flag && AddNewBtn == null)
        {
            AddNewBtn = new SymbolButton(4);
            AddNewBtn.setMaximumSize(btnSize);
            AddNewBtn.setMinimumSize(btnSize);
            AddNewBtn.setPreferredSize(btnSize);
            add(AddNewBtn, new GridBagConstraints(5, 0, 1, 1, 0.0D, 0.0D, 10, 3, noMargin, 0, 0));
            AddNewBtn.addActionListener(this);
        } else
        if(!flag && AddNewBtn != null)
        {
            remove(AddNewBtn);
            AddNewBtn.removeActionListener(this);
            AddNewBtn = null;
        }
    }

    public void addDataBound(diamondedge.vb.DataBound databound)
    {
        listenerList.add(diamondedge.vb.DataBound.class, databound);
        if(rs != null)
            rs.addDataBound(databound);
    }

    public void removeDataBound(diamondedge.vb.DataBound databound)
    {
        listenerList.remove(diamondedge.vb.DataBound.class, databound);
        if(rs != null)
            rs.removeDataBound(databound);
    }

    public java.lang.String getConnectionString()
    {
        if(con != null)
            return con.getConnectionString();
        else
            return url;
    }

    public void setConnectionString(java.lang.String s, java.lang.String s1)
    {
        if(s != null && !s.equals(""))
            url = s;
        if(s1 != null && !s1.equals(""))
            driver = s1;
    }

    public void setConnectionString(java.lang.String s, java.lang.String s1, java.lang.String s2, java.lang.String s3)
    {
        if(s1 != null)
            userName = s1;
        if(s2 != null)
            password = s2;
        setConnectionString(s, s3);
    }

    public java.lang.String getUserName()
    {
        return userName;
    }

    public void setUserName(java.lang.String s)
    {
        if(!s.equals(""))
            userName = s;
    }

    public java.lang.String getPassword()
    {
        return password;
    }

    public void setPassword(java.lang.String s)
    {
        if(!s.equals(""))
            password = s;
    }

    public int getConnectionTimeout()
    {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int i)
        throws diamondedge.ado.AdoError
    {
        connectionTimeout = i;
        if(con != null && !con.isConnected())
            con.setConnectionTimeout(i);
    }

    public int getCommandTimeout()
    {
        return commandTimeout;
    }

    public void setCommandTimeout(int i)
    {
        commandTimeout = i;
        if(con != null && !con.isConnected())
            con.setCommandTimeout(i);
    }

    public java.lang.String getRecordSource()
    {
        return recordSource;
    }

    public void setRecordSource(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        recordSource = s;
        if(s != null && !s.equals(""))
            if(rs != null)
            {
                rs.close();
                rs.open(recordSource);
            } else
            {
                getRecordset();
            }
    }

    public int getCursorType()
    {
        return cursorType;
    }

    public void setCursorType(int i)
        throws diamondedge.ado.AdoError
    {
        cursorType = i;
        if(rs != null && !rs.isOpen())
            rs.setCursorType(i);
    }

    public int getLockType()
    {
        return lockType;
    }

    public void setLockType(int i)
        throws diamondedge.ado.AdoError
    {
        lockType = i;
        if(rs != null && !rs.isOpen())
            rs.setLockType(i);
    }

    public int getCommandType()
    {
        return commandType;
    }

    public void setCommandType(int i)
    {
        commandType = i;
    }

    public int getMaxRecords()
    {
        return maxRecords;
    }

    public void setMaxRecords(int i)
        throws diamondedge.ado.AdoError
    {
        maxRecords = i;
        if(rs != null && !rs.isOpen())
            rs.setMaxRecords(i);
    }

    public int getBOFAction()
    {
        return BOFAction;
    }

    public void setBOFAction(int i)
    {
        BOFAction = i;
    }

    public int getEOFAction()
    {
        return EOFAction;
    }

    public void setEOFAction(int i)
    {
        EOFAction = i;
    }

    public java.lang.String getCaption()
    {
        return label.getText();
    }

    public void setCaption(java.lang.String s)
    {
        label.setText(s);
    }

    public diamondedge.ado.Recordset getRecordset()
    {
        if(rs == null && recordSource != null)
            try
            {
                if(con == null)
                    connect();
                recordSource = recordSource.trim();
                if(commandType == -1 || commandType == 8)
                    if(diamondedge.ado.Recordset.isSqlStatement(recordSource))
                        commandType = 1;
                    else
                        commandType = 2;
                rs = new Recordset();
                rs.setMaxRecords(maxRecords);
                rs.open(recordSource, con, cursorType, lockType, commandType);
                if(!rs.isEOF())
                    rs.moveFirst();
                addListeners(rs);
            }
            catch(diamondedge.ado.AdoError adoerror)
            {
                java.lang.String s = getName();
                if(getParent() != null && getParent().getName() != null)
                    s = getParent().getName() + "." + s;
                if(s == null)
                    s = "Data";
                java.lang.System.out.println(s + ".getRecordset: " + adoerror);
            }
        return rs;
    }

    public void setRecordset(diamondedge.ado.Recordset recordset)
    {
        if(recordset != rs)
        {
            addListeners(recordset);
            rs = recordset;
        }
    }

    public diamondedge.ado.Connection getConnection()
    {
        return con;
    }

    public void setConnection(diamondedge.ado.Connection connection)
    {
        con = connection;
        rs = null;
    }

    public void updateRecord()
    {
        if(rs != null)
            try
            {
                rs.update();
            }
            catch(diamondedge.ado.AdoError adoerror)
            {
                java.lang.System.out.println("Data.updateRecord: " + adoerror);
            }
    }

    public java.lang.String getTag()
    {
        return tag;
    }

    public void setTag(java.lang.String s)
    {
        tag = s;
    }

    public void requery()
        throws diamondedge.ado.AdoError
    {
        if(rs != null)
            rs.requery();
    }

    protected void connect()
    {
        if(con == null && url != null && driver != null)
            try
            {
                con = diamondedge.ado.Connection.openConnection(url, userName, password, driver);
            }
            catch(diamondedge.ado.AdoError adoerror)
            {
                java.lang.System.out.println("Data.connect(): " + adoerror);
            }
    }

    protected void addListeners(diamondedge.ado.Recordset recordset)
    {
        java.lang.Object aobj[] = listenerList.getListenerList();
        for(int i = aobj.length - 2; i >= 0; i -= 2)
        {
            if(aobj[i] != (diamondedge.vb.DataBound.class))
                continue;
            if(recordset != null)
                recordset.addDataBound((diamondedge.vb.DataBound)aobj[i + 1]);
            if(recordset != rs && rs != null)
                rs.removeDataBound((diamondedge.vb.DataBound)aobj[i + 1]);
        }

    }

    protected void removeListeners(diamondedge.ado.Recordset recordset)
    {
        java.lang.Object aobj[] = listenerList.getListenerList();
        for(int i = aobj.length - 2; i >= 0; i -= 2)
            if(aobj[i] == (diamondedge.vb.DataBound.class) && recordset != null)
                rs.removeDataBound((diamondedge.vb.DataBound)aobj[i + 1]);

    }

    public static void initControls(diamondedge.vb.Data data)
    {
        diamondedge.ado.Recordset recordset = data.getRecordset();
    }

    public void actionPerformed(java.awt.event.ActionEvent actionevent)
    {
        diamondedge.ado.Recordset recordset = getRecordset();
        if(recordset == null)
            return;
        try
        {
            if(actionevent.getSource() == MoveFirstBtn)
                recordset.moveFirst();
            else
            if(actionevent.getSource() == MovePrevBtn)
            {
                if(!recordset.isBOF())
                    recordset.movePrevious();
                if(recordset.isBOF() && BOFAction == 0)
                    recordset.moveFirst();
            } else
            if(actionevent.getSource() == MoveNextBtn)
            {
                if(!recordset.isEOF())
                    recordset.moveNext();
                if(recordset.isEOF())
                    if(EOFAction == 0)
                        recordset.moveLast();
                    else
                    if(EOFAction == 2)
                        recordset.addNew();
            } else
            if(actionevent.getSource() == MoveLastBtn)
                recordset.moveLast();
            else
            if(actionevent.getSource() == AddNewBtn)
                recordset.addNew();
        }
        catch(diamondedge.ado.AdoError adoerror)
        {
            java.lang.System.out.println("Data.actionPerformed: " + adoerror);
            java.lang.System.out.println(recordset);
        }
    }

    protected java.lang.String tag = null;
    protected javax.swing.JLabel label = null;
    protected diamondedge.vb.SymbolButton MoveFirstBtn = null;
    protected diamondedge.vb.SymbolButton MovePrevBtn = null;
    protected diamondedge.vb.SymbolButton MoveNextBtn = null;
    protected diamondedge.vb.SymbolButton MoveLastBtn = null;
    protected diamondedge.vb.SymbolButton AddNewBtn = null;
    protected java.awt.Dimension btnSize = null;
    protected java.awt.Insets noMargin = null;
    protected diamondedge.ado.Connection con = null;
    protected diamondedge.ado.Recordset rs = null;
    protected java.lang.String recordSource = null;
    protected java.lang.String userName = null;
    protected java.lang.String password = null;
    protected java.lang.String url = null;
    protected java.lang.String driver = null;
    protected int connectionTimeout = 0;
    protected int commandTimeout = 0;
    protected int cursorType = 0;
    protected int lockType = 0;
    protected int commandType = 0;
    protected int maxRecords = 0;
    protected int BOFAction = 0;
    protected int EOFAction = 0;
}
