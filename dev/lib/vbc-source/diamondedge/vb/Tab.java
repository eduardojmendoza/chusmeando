// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.swing.IconList;
import diamondedge.util.Variant;
import javax.swing.JTabbedPane;

// Referenced classes of package diamondedge.vb:
//            VbPanel, TabStrip, ImageList

public class Tab extends diamondedge.vb.VbPanel
{

    public Tab(javax.swing.JTabbedPane jtabbedpane, int i)
    {
        aq = null;
        ap = i;
        as = jtabbedpane;
    }

    public java.lang.String getCaption()
    {
        return as.getTitleAt(ap);
    }

    public void setCaption(java.lang.String s)
    {
        as.setTitleAt(ap, s);
    }

    public int getIndex()
    {
        return ap + 1;
    }

    public void setIndex(int i)
    {
        ap = i - 1;
    }

    public java.lang.String getKey()
    {
        return ar;
    }

    public void setKey(java.lang.String s)
    {
        ar = s;
    }

    public boolean isSelected()
    {
        return ap == as.getSelectedIndex();
    }

    public void setSelected(boolean flag)
    {
        if(flag)
            as.setSelectedIndex(ap);
    }

    public diamondedge.util.Variant getImageVal()
    {
        return aq;
    }

    public void setImage(int i)
    {
        aq = new Variant(i);
        if(as instanceof diamondedge.vb.TabStrip)
        {
            diamondedge.swing.IconList iconlist = ((diamondedge.vb.TabStrip)as).getImageList();
            if(iconlist != null)
                as.setIconAt(i, iconlist.getIcon(i));
        }
    }

    public void setImage(java.lang.String s)
    {
        aq = new Variant(s);
        if(as instanceof diamondedge.vb.TabStrip)
        {
            diamondedge.swing.IconList iconlist = ((diamondedge.vb.TabStrip)as).getImageList();
            if(iconlist != null)
                as.setIconAt(ap, diamondedge.vb.ImageList.getIcon(s, iconlist));
        }
    }

    private int ap = 0;
    private java.lang.String ar = null;
    private javax.swing.JTabbedPane as = null;
    private diamondedge.util.Variant aq = null;
}
