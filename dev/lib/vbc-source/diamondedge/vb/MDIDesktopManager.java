// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Container;
import java.beans.PropertyVetoException;
import javax.swing.DefaultDesktopManager;
import javax.swing.JInternalFrame;

// Referenced classes of package diamondedge.vb:
//            MDIForm

class MDIDesktopManager extends javax.swing.DefaultDesktopManager
{

    public MDIDesktopManager(diamondedge.vb.MDIForm mdiform)
    {
        _fldif = mdiform;
    }

    public void closeFrame(javax.swing.JInternalFrame jinternalframe)
    {
        a(jinternalframe);
        super.closeFrame(jinternalframe);
    }

    public void activateFrame(javax.swing.JInternalFrame jinternalframe)
    {
        try
        {
            super.activateFrame(jinternalframe);
            if(a != null && jinternalframe != a && a.isMaximum() && jinternalframe.getClientProperty("JInternalFrame.frameType") != "optionDialog")
            {
                a.setMaximum(false);
                jinternalframe.setMaximum(true);
            }
            a = jinternalframe;
        }
        catch(java.beans.PropertyVetoException propertyvetoexception) { }
    }

    void a(javax.swing.JInternalFrame jinternalframe)
    {
        java.awt.Container container = jinternalframe.getParent();
        if(container == null)
            return;
        javax.swing.JInternalFrame jinternalframe1 = null;
        Object obj = null;
        int i = 0;
        do
        {
            if(i >= container.getComponentCount())
                break;
            java.awt.Component component = container.getComponent(i);
            if((component instanceof javax.swing.JInternalFrame) && component != jinternalframe)
            {
                jinternalframe1 = (javax.swing.JInternalFrame)component;
                break;
            }
            i++;
        } while(true);
        if(jinternalframe1 != null)
        {
            try
            {
                jinternalframe1.setSelected(true);
            }
            catch(java.beans.PropertyVetoException propertyvetoexception) { }
            if(jinternalframe1 != a)
                activateFrame(jinternalframe1);
            jinternalframe1.moveToFront();
        }
    }

    private diamondedge.vb.MDIForm _fldif = null;
    javax.swing.JInternalFrame a = null;
}
