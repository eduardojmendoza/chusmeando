// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Toolkit;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

// Referenced classes of package diamondedge.vb:
//            Application

public class Clipboard
    implements java.awt.datatransfer.ClipboardOwner
{

    private Clipboard()
    {
    }

    public void lostOwnership(java.awt.datatransfer.Clipboard clipboard, java.awt.datatransfer.Transferable transferable)
    {
    }

    public static final java.awt.datatransfer.Clipboard getClipboard()
    {
        if(_fldif == null)
            if(diamondedge.vb.Application.getApplication().isAppletRunning())
                _fldif = new java.awt.datatransfer.Clipboard(diamondedge.vb.Application.getApplication().getName());
            else
                _fldif = java.awt.Toolkit.getDefaultToolkit().getSystemClipboard();
        return _fldif;
    }

    public static final void clear()
    {
        diamondedge.vb.Clipboard.getClipboard().setContents(new StringSelection(""), a);
    }

    public static final java.lang.String getText()
    {
        java.awt.datatransfer.Transferable transferable = diamondedge.vb.Clipboard.getClipboard().getContents(a);
        java.lang.String s = null;
        try
        {
            s = (java.lang.String)transferable.getTransferData(java.awt.datatransfer.DataFlavor.stringFlavor);
        }
        catch(java.lang.Exception exception) { }
        if(s == null)
            return "";
        else
            return s;
    }

    public static final java.lang.String getText(int i)
    {
        return diamondedge.vb.Clipboard.getText();
    }

    public static final void setText(java.lang.String s)
    {
        java.awt.datatransfer.StringSelection stringselection = new StringSelection(s);
        diamondedge.vb.Clipboard.getClipboard().setContents(stringselection, a);
    }

    public static final void setText(java.lang.String s, int i)
    {
        diamondedge.vb.Clipboard.setText(s);
    }

    public static final boolean getFormat(int i)
    {
        java.awt.datatransfer.Transferable transferable = diamondedge.vb.Clipboard.getClipboard().getContents(a);
        switch(i)
        {
        case 1: // '\001'
            return transferable.isDataFlavorSupported(java.awt.datatransfer.DataFlavor.stringFlavor);
        }
        return false;
    }

    private static java.awt.datatransfer.Clipboard _fldif = null;
    private static diamondedge.vb.Clipboard a = new Clipboard();
    public static final short CFLink = -16640;
    public static final short CFText = 1;
    public static final short CFBitmap = 2;
    public static final short CFMetafile = 3;
    public static final short CFDIB = 8;
    public static final short CFPalette = 9;
    public static final short CFEMetafile = 14;
    public static final short CFFiles = 15;
    public static final short CFRTF = -16639;

}
