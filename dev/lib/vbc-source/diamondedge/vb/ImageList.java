// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.swing.IconList;
import diamondedge.util.Variant;
import java.awt.Component;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;

// Referenced classes of package diamondedge.vb:
//            ListImages, ListImage, Screen

public class ImageList extends java.awt.Component
    implements diamondedge.swing.IconList
{

    public ImageList()
    {
        _fldif = new ListImages();
        _flddo = 0;
        a = 0;
    }

    public ImageList(int i)
    {
        _fldif = new ListImages();
        _flddo = 0;
        a = 0;
        for(int j = 0; j < i; j++)
            try
            {
                _fldif.add(-1, null, null);
            }
            catch(java.lang.Exception exception) { }

    }

    public int getImageHeight()
    {
        return a;
    }

    public void setImageHeight(int i)
    {
        a = i;
    }

    public int getImageWidth()
    {
        return _flddo;
    }

    public void setImageWidth(int i)
    {
        _flddo = i;
    }

    public diamondedge.vb.ListImages getListImages()
    {
        return _fldif;
    }

    public java.awt.Image getImage(diamondedge.vb.ListImage listimage)
    {
        if(listimage == null)
            return null;
        if(_flddo == 0 || a == 0)
            return listimage.getImage();
        else
            return listimage.getScaledImage(_flddo, a);
    }

    public javax.swing.ImageIcon getIcon(diamondedge.vb.ListImage listimage)
    {
        if(listimage == null)
            return null;
        if(_flddo == 0 || a == 0)
            return listimage.getIcon();
        else
            return listimage.getScaledIcon(_flddo, a);
    }

    public javax.swing.Icon getIcon(int i)
    {
        return getIcon(_fldif.getItem(i));
    }

    public javax.swing.Icon getIcon(java.lang.String s)
    {
        return getIcon(_fldif.getItem(s));
    }

    public javax.swing.Icon getIcon(diamondedge.util.Variant variant)
    {
        if(variant.getVarType() != 15 && variant.isNumeric())
            return getIcon(variant.toInt());
        else
            return getIcon(variant.toString());
    }

    public java.awt.Image getImage(int i)
    {
        return getImage(_fldif.getItem(i));
    }

    public java.awt.Image getImage(java.lang.String s)
    {
        return getImage(_fldif.getItem(s));
    }

    public java.awt.Image getImage(diamondedge.util.Variant variant)
    {
        if(variant.getVarType() != 15 && variant.isNumeric())
            return getImage(variant.toInt());
        else
            return getImage(variant.toString());
    }

    public void setImage(int i, java.lang.String s)
    {
        java.awt.Image image = diamondedge.vb.Screen.loadImage(s);
        setImage(i, image);
    }

    public void setImage(int i, java.awt.Image image)
    {
        try
        {
            if(i == _fldif.getCount())
            {
                _fldif.add(i, null, image);
            } else
            {
                diamondedge.vb.ListImage listimage = _fldif.getItem(i);
                if(listimage == null)
                    _fldif.add(i, null, image);
                else
                    listimage.setImage(image);
            }
        }
        catch(java.lang.Exception exception) { }
    }

    public static javax.swing.Icon getIcon(java.lang.Object obj, diamondedge.swing.IconList iconlist)
    {
        if(obj == null)
            return null;
        if(obj instanceof javax.swing.Icon)
            return (javax.swing.Icon)obj;
        if(obj instanceof java.awt.Image)
            return new ImageIcon((java.awt.Image)obj);
        if(iconlist != null)
        {
            if(obj instanceof java.lang.Number)
                return iconlist.getIcon(((java.lang.Number)obj).intValue());
            if(obj instanceof diamondedge.util.Variant)
            {
                diamondedge.util.Variant variant = (diamondedge.util.Variant)obj;
                if(variant.getVarType() != 15 && variant.isNumeric())
                    return iconlist.getIcon(variant.toInt());
                else
                    return iconlist.getIcon(variant.toString());
            } else
            {
                return iconlist.getIcon(obj.toString());
            }
        } else
        {
            return null;
        }
    }

    private diamondedge.vb.ListImages _fldif = null;
    private int _flddo = 0;
    private int a = 0;
}
