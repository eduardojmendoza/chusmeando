// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import com.sun.java.swing.plaf.windows.WindowsProgressBarUI;
import javax.swing.UIManager;

public class SmoothProgressBarUI extends com.sun.java.swing.plaf.windows.WindowsProgressBarUI
{

    public SmoothProgressBarUI()
    {
        setCellSpacing(0);
    }

    public void setSmoothScrolling(boolean flag)
    {
        if(flag)
            setCellSpacing(0);
        else
            setCellSpacing(javax.swing.UIManager.getInt("ProgressBar.cellSpacing"));
    }

    public boolean isSmoothScrolling()
    {
        return getCellSpacing() == 0;
    }
}
