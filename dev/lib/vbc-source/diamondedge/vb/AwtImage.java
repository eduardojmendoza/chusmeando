// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

// Referenced classes of package diamondedge.vb:
//            Screen

public class AwtImage extends java.awt.Component
{

    public AwtImage()
    {
        _flddo = null;
        a = false;
    }

    public java.awt.Image getImage()
    {
        return _flddo;
    }

    public void setImage(java.lang.String s)
    {
        _flddo = diamondedge.vb.Screen.loadImage(s);
        repaint();
    }

    public void setImage(java.awt.Image image)
    {
        _flddo = image;
        repaint();
    }

    public void paint(java.awt.Graphics g)
    {
        if(_flddo != null)
            if(a)
            {
                java.awt.Dimension dimension = getSize();
                g.drawImage(_flddo, 0, 0, dimension.width, dimension.height, this);
            } else
            {
                g.drawImage(_flddo, 0, 0, this);
            }
    }

    public boolean isStretchToFit()
    {
        return a;
    }

    public void setStretchToFit(boolean flag)
    {
        a = flag;
    }

    public java.lang.String getTag()
    {
        return _fldif;
    }

    public void setTag(java.lang.String s)
    {
        _fldif = s;
    }

    private java.lang.String _fldif = null;
    private java.awt.Image _flddo = null;
    private boolean a = false;
}
