// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Color;
import java.awt.Component;
import java.io.File;
import java.util.StringTokenizer;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JFileChooser;

// Referenced classes of package diamondedge.vb:
//            CommonFileFilter

public class CommonDialog extends javax.swing.JComponent
{

    public CommonDialog()
    {
        _fldbyte = "";
        _fldint = null;
        _fldnew = null;
        _fldfor = null;
        _fldtry = null;
        _fldif = null;
    }

    public java.lang.String getFileName()
    {
        return _fldbyte;
    }

    public void setFileName(java.lang.String s)
    {
        _fldbyte = s;
    }

    public java.lang.String getDialogTitle()
    {
        return a;
    }

    public void setDialogTitle(java.lang.String s)
    {
        a = s;
    }

    public java.lang.String getFilter()
    {
        return _flddo;
    }

    public void setFilter(java.lang.String s)
    {
        getFileChooser();
        _flddo = s;
        _fldtry.resetChoosableFileFilters();
        if(_flddo == null || _flddo.equals(""))
            return;
        java.util.StringTokenizer stringtokenizer = new StringTokenizer(_flddo, "|");
        boolean flag = false;
        do
        {
            if(!stringtokenizer.hasMoreTokens())
                break;
            java.lang.String s1 = stringtokenizer.nextToken();
            java.lang.String s3 = stringtokenizer.nextToken();
            if(!s3.equals("*.*"))
            {
                diamondedge.vb.CommonFileFilter commonfilefilter = new CommonFileFilter(s1);
                java.lang.String s2;
                for(java.util.StringTokenizer stringtokenizer1 = new StringTokenizer(s3, ";"); stringtokenizer1.hasMoreTokens(); commonfilefilter.addExtension(s2))
                {
                    s2 = stringtokenizer1.nextToken();
                    if(s2.startsWith("*."))
                        s2 = s2.substring(2);
                }

                _fldtry.addChoosableFileFilter(commonfilefilter);
                boolean flag1 = true;
            }
        } while(true);
    }

    public java.lang.String getDefaultExt()
    {
        return _fldint;
    }

    public void setDefaultExt(java.lang.String s)
    {
        _fldint = s;
    }

    public java.lang.String getInitDir()
    {
        return _fldnew != null ? _fldnew : "";
    }

    public void setInitDir(java.lang.String s)
    {
        _fldnew = s;
    }

    public java.awt.Color getColor()
    {
        return _fldif;
    }

    public void setColor(java.awt.Color color)
    {
        _fldif = color;
    }

    public javax.swing.JFileChooser getFileChooser()
    {
        if(_fldtry == null)
            _fldtry = new JFileChooser();
        return _fldtry;
    }

    private void _mthif()
    {
        getFileChooser();
        if(a != null && !a.equals(""))
            _fldtry.setDialogTitle(a);
        if(_fldnew != null && !_fldnew.equals(""))
            _fldtry.setCurrentDirectory(new File(_fldnew));
        else
            _fldtry.setCurrentDirectory(new File(_fldbyte));
        _fldtry.setSelectedFile(new File(_fldbyte));
    }

    private void a()
    {
        java.io.File file = _fldtry.getSelectedFile();
        if(file == null)
        {
            _fldbyte = "";
        } else
        {
            _fldbyte = file.getPath();
            if(_fldbyte.indexOf(".") < 0 && _fldint != null)
                _fldbyte += "." + _fldint;
        }
    }

    public void showOpen()
    {
        showOpen(null);
    }

    public void showOpen(java.awt.Component component)
    {
        _mthif();
        int i = _fldtry.showOpenDialog(component);
        a();
    }

    public void showSave()
    {
        showSave(null);
    }

    public void showSave(java.awt.Component component)
    {
        _mthif();
        int i = _fldtry.showSaveDialog(component);
        a();
    }

    public void showColor()
    {
        showColor(null);
    }

    public void showColor(java.awt.Component component)
    {
        _fldif = javax.swing.JColorChooser.showDialog(component, a, _fldif);
    }

    private java.lang.String a = null;
    private java.lang.String _flddo = null;
    private java.lang.String _fldbyte = null;
    private java.lang.String _fldint = null;
    private java.lang.String _fldnew = null;
    private diamondedge.vb.CommonFileFilter _fldfor = null;
    private javax.swing.JFileChooser _fldtry = null;
    private java.awt.Color _fldif = null;
}
