// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;

public class AwtFrame extends java.awt.Container
{

    public AwtFrame()
    {
        optionButtonGroup = null;
        setLayout(null);
    }

    public void paint(java.awt.Graphics g)
    {
        g.setColor(getBackground());
        java.awt.Dimension dimension = getSize();
        g.fillRect(0, 0, dimension.width - 1, dimension.height - 1);
        int i = 1;
        java.lang.String s = " " + _fldif + " ";
        java.awt.FontMetrics fontmetrics = g.getFontMetrics();
        int j = fontmetrics.getHeight();
        int k = fontmetrics.stringWidth(s);
        byte byte0 = 8;
        int l = fontmetrics.getAscent();
        int i1 = j / 2;
        int j1 = getSize().width - 2 * i - 1;
        int k1 = getSize().height - i - i1 - 1;
        g.setColor(java.awt.Color.white);
        g.drawRect(i + 1, i1 + 1, j1, k1);
        g.setColor(java.awt.Color.gray);
        g.drawRect(i, i1, j1, k1);
        g.setColor(getBackground());
        g.fillRect(byte0 - 1, 0, k + 2, j);
        g.setColor(getForeground());
        g.drawString(s, byte0, l);
        super.paint(g);
    }

    public void addExclusiveButton(java.awt.Checkbox checkbox)
    {
        add(checkbox);
        if(optionButtonGroup == null)
            optionButtonGroup = new CheckboxGroup();
        checkbox.setCheckboxGroup(optionButtonGroup);
    }

    public java.lang.String getCaption()
    {
        return _fldif;
    }

    public void setCaption(java.lang.String s)
    {
        _fldif = s;
    }

    public java.lang.String getTag()
    {
        return a;
    }

    public void setTag(java.lang.String s)
    {
        a = s;
    }

    private java.lang.String a = null;
    private java.lang.String _fldif = null;
    protected java.awt.CheckboxGroup optionButtonGroup = null;
}
