// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

// Referenced classes of package diamondedge.vb:
//            Screen

public class VbImage extends javax.swing.JPanel
{

    public VbImage()
    {
        _flddo = false;
        a = false;
        setOpaque(false);
    }

    public java.awt.Image getImage()
    {
        return _fldfor;
    }

    public void setImage(java.lang.String s)
    {
        _fldfor = diamondedge.vb.Screen.loadImage(s);
        repaint();
    }

    public void setImage(java.awt.Image image)
    {
        _fldfor = image;
        repaint();
    }

    public void paintComponent(java.awt.Graphics g)
    {
        super.paintComponent(g);
        if(_fldfor != null)
        {
            java.awt.Insets insets = getInsets();
            if(a)
            {
                java.awt.Dimension dimension = getSize();
                int i = dimension.width - insets.left - insets.right;
                int j = dimension.height - insets.top - insets.bottom;
                g.drawImage(_fldfor, insets.left, insets.top, i, j, this);
            } else
            {
                g.drawImage(_fldfor, insets.left, insets.top, this);
            }
        }
    }

    public java.awt.Dimension getPreferredSize()
    {
        if(_fldfor == null)
        {
            return super.getPreferredSize();
        } else
        {
            java.awt.Insets insets = getInsets();
            return new Dimension(_fldfor.getWidth(this) + insets.left + insets.right, _fldfor.getHeight(this) + insets.top + insets.bottom);
        }
    }

    public java.lang.String getTag()
    {
        return _fldif;
    }

    public void setTag(java.lang.String s)
    {
        _fldif = s;
    }

    public boolean isStretchToFit()
    {
        return a;
    }

    public void setStretchToFit(boolean flag)
    {
        a = flag;
    }

    public boolean isOutlined()
    {
        return _flddo;
    }

    public void setOutlined(boolean flag)
    {
        _flddo = flag;
        a();
    }

    private void a()
    {
        javax.swing.border.Border border = null;
        if(_flddo)
            border = javax.swing.BorderFactory.createLineBorder(java.awt.Color.black, 1);
        setBorder(border);
    }

    private java.lang.String _fldif = null;
    private java.awt.Image _fldfor = null;
    private boolean _flddo = false;
    private boolean a = false;
}
