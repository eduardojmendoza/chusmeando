// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import javax.swing.JScrollBar;

public class SpinButton extends javax.swing.JScrollBar
{

    public SpinButton()
    {
        this(1);
    }

    public SpinButton(int i)
    {
        super(i);
        _fldif = 0;
        setValues(0, 0, 0xffe17b80, 0x1e8480);
        setUnitIncrement(1);
        setBlockIncrement(1);
        setVisibleAmount(0);
    }

    public boolean hasChangedUp()
    {
        return getValue() < _fldif;
    }

    public void setValue(int i)
    {
        _fldif = getValue();
        super.setValue(i);
    }

    public java.lang.String getTag()
    {
        return a;
    }

    public void setTag(java.lang.String s)
    {
        a = s;
    }

    private java.lang.String a = null;
    private int _fldif = 0;
}
