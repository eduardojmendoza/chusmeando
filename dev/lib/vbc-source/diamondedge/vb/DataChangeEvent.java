// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.ado.Recordset;
import diamondedge.util.Variant;

public class DataChangeEvent
{

    public DataChangeEvent(diamondedge.ado.Recordset recordset, int i)
    {
        _fldif = recordset;
        a = i;
    }

    public diamondedge.util.Variant getValue()
    {
        return _flddo;
    }

    public void setValue(diamondedge.util.Variant variant)
    {
        _flddo = variant;
    }

    public diamondedge.ado.Recordset getRecordset()
    {
        return _fldif;
    }

    public diamondedge.ado.Recordset getSource()
    {
        return _fldif;
    }

    public int getChangeType()
    {
        return a;
    }

    public java.lang.String toString()
    {
        return "DataChangeEvent[val:" + _flddo + " type: " + a + " " + _fldif + "]";
    }

    private diamondedge.ado.Recordset _fldif = null;
    private diamondedge.util.Variant _flddo = null;
    private int a = 0;
}
