// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.ado.AdoError;
import diamondedge.ado.Field;
import diamondedge.ado.Recordset;
import diamondedge.swing.DsCell;
import diamondedge.swing.DsCellProperties;
import diamondedge.swing.DsTableCell;
import diamondedge.swing.DsTableColumnHeader;
import diamondedge.swing.grid.DefaultGridModel;
import diamondedge.swing.grid.DelegateGridModel;
import diamondedge.swing.grid.DsCellRange;
import diamondedge.swing.grid.DsGrid;
import diamondedge.swing.grid.GridModel;
import diamondedge.swing.grid.GridRowHeader;
import diamondedge.swing.grid.GridRowModel;
import diamondedge.util.ListFormat;
import diamondedge.util.Variant;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import javax.swing.Icon;
import javax.swing.JScrollPane;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

// Referenced classes of package diamondedge.vb:
//            RecordsetTableModel, DataBound, VbControl, VbColor, 
//            ScrolledComponent, DataSource, DataChangeEvent

public class Grid extends diamondedge.swing.grid.DsGrid
    implements diamondedge.vb.DataBound, diamondedge.vb.VbControl
{

    public Grid()
    {
        this(2, 2);
    }

    public Grid(int i, int j)
    {
        super(i - 1, j);
        countFrozenRow = false;
        aK = null;
        curRecord = 1;
        getTableHeader().setReorderingAllowed(false);
        setAutoResizeMode(0);
        setRowHeaderAutoLabel(0);
        leaveFrozenColumns = true;
    }

    public void initDBGrid()
    {
        countFrozenRow = false;
        setRowStatusIconShowing(true);
        removeAll();
    }

    public void initDataGrid()
    {
        countFrozenRow = false;
        setRowStatusIconShowing(true);
        removeAll();
    }

    public void initFlexGrid()
    {
        countFrozenRow = true;
        int i = getRowCount();
        setRowCount(0);
        setRowCount(i);
        addNotify();
        setFrozenColumnCount(1);
    }

    public void initGrid()
    {
        initFlexGrid();
    }

    public void setModel(javax.swing.table.TableModel tablemodel)
    {
        if(tablemodel == getModel())
            return;
        if((tablemodel instanceof diamondedge.swing.grid.GridModel) && getModel() != null)
        {
            autoCreateColumnsFromModel = false;
            if(tablemodel instanceof diamondedge.swing.grid.DefaultGridModel)
            {
                int i = tablemodel.getColumnCount();
                int j = 0;
                do
                {
                    if(j >= i)
                        break;
                    if(tablemodel.getColumnName(j) != null)
                    {
                        autoCreateColumnsFromModel = true;
                        break;
                    }
                    j++;
                } while(true);
            }
        }
        super.setModel(tablemodel);
    }

    public final boolean getCountFrozenRow()
    {
        return countFrozenRow;
    }

    public void setCountFrozenRow(boolean flag)
    {
        countFrozenRow = flag;
    }

    public final int getRows()
    {
        return getRowCount() + getFrozenRowCount();
    }

    public void setRows(int i)
    {
        setRowCount(i - getFrozenRowCount());
    }

    public final int getCols()
    {
        return getColumnCount();
    }

    public void setCols(int i)
    {
        setColumnCount(i);
    }

    public void setColumnCount(int i)
    {
        if(i < getFrozenColumnCount())
            setFrozenColumnCount(i);
        super.setColumnCount(i);
    }

    public int getFrozenRowCount()
    {
        return !countFrozenRow || !isColumnHeaderVisible() ? 0 : 1;
    }

    public void setFrozenRowCount(int i)
    {
        if(i == getFrozenRowCount())
            return;
        if(i == 0)
        {
            if(isColumnHeaderVisible())
            {
                setRowCount(getRowCount() + 1);
                setColumnHeaderVisible(false);
            }
        } else
        if(!isColumnHeaderVisible())
        {
            setRowCount(getRowCount() - 1);
            setColumnHeaderVisible(true);
        }
    }

    private void a(int i)
    {
        setColumnWidthToFit(i, true, false);
    }

    private void a(javax.swing.table.TableColumn tablecolumn)
    {
        tablecolumn.setMinWidth(0);
        tablecolumn.setMaxWidth(0);
        tablecolumn.setPreferredWidth(0);
        tablecolumn.setWidth(0);
    }

    public void hideColumn(int i)
    {
        if(i == 0 && getFrozenColumnCount() == 1)
            setRowHeaderVisible(false);
        else
        if(i < getFrozenColumnCount())
        {
            a(getRowHeader().getColumn(i));
            if(isRowHeaderVisible() && getRowHeader().getTableHeader().getColumnModel().getTotalColumnWidth() == 0)
                setRowHeaderVisible(false);
        } else
        {
            a(getColumn(i));
        }
    }

    public int getFixedCols()
    {
        return getFrozenColumnCount();
    }

    public void setFixedCols(int i)
    {
        setFrozenColumnCount(i);
    }

    public int getFixedRows()
    {
        return getFrozenRowCount();
    }

    public void setFixedRows(int i)
    {
        setFrozenRowCount(i);
    }

    public int getRow2()
    {
        int i = super.getRow2();
        if(i >= 0)
            return i + getFrozenRowCount();
        else
            return i;
    }

    public void setRow2(int i)
    {
        super.setRow2(i - getFrozenRowCount());
    }

    public int getRow()
    {
        int i = super.getRow();
        if(i >= 0)
            return i + getFrozenRowCount();
        else
            return i;
    }

    public void setRow(int i)
    {
        super.setRow(i - getFrozenRowCount());
    }

    protected boolean fireGridEditorEvent(int i, int j, int k, java.awt.Component component)
    {
        return super.fireGridEditorEvent(i, j + getFrozenRowCount(), k, component);
    }

    public boolean select(int i, int j, int k, int l)
    {
        return super.select(i - getFrozenRowCount(), j, k - getFrozenRowCount(), l);
    }

    public void sortSelectedColumns(int i)
    {
        diamondedge.swing.grid.GridModel gridmodel = getGridModel();
        if(gridmodel != null)
        {
            for(int j = getCol2(); j >= 0 && j >= getCol(); j--)
                gridmodel.sort(j, i);

        }
    }

    public diamondedge.swing.DsCell getCellAt(int i, int j)
    {
        if(i >= getRowCount() || j >= getColumnCount())
            return null;
        if(i < 0)
        {
            if(j < getFrozenColumnCount())
                return getRowHeader().getColumnHeader().getCellAt(j);
            else
                return getColumnHeader().getCellAt(j);
        } else
        {
            return super.getCellAt(i, j);
        }
    }

    public java.lang.Object getCellValueAt(int i, int j)
    {
        if(i >= getRowCount() || j >= getColumnCount())
            return null;
        if(i < 0)
        {
            if(j < getFrozenColumnCount())
                return getRowHeader().getColumnName(j);
            else
                return getColumnName(j);
        } else
        {
            return super.getCellValueAt(i, j);
        }
    }

    public java.lang.Object getValueAt(int i, int j)
    {
        if(i >= getRowCount() || j >= getColumnCount())
            return null;
        if(i < 0)
        {
            if(j < getFrozenColumnCount())
                return getRowHeader().getColumnHeader().getValueAt(0, j);
            else
                return getColumnHeader().getValueAt(0, j);
        } else
        {
            return super.getValueAt(i, j);
        }
    }

    public void setValueAt(java.lang.Object obj, int i, int j)
    {
        if(i < 0)
        {
            if(j < getFrozenColumnCount())
                getRowHeader().getColumnHeader().setValueAt(obj, 0, j);
            else
                getColumnHeader().setValueAt(obj, 0, j);
        } else
        {
            super.setValueAt(obj, i, j);
        }
    }

    public java.lang.String getText()
    {
        return getTextAt(getRow(), getCol());
    }

    public void setText(java.lang.String s)
    {
        setTextAt(s, getRow(), getCol());
    }

    public java.lang.String getTextAt(int i, int j)
    {
        i -= getFrozenRowCount();
        if(i >= getRowCount() || j >= getColumnCount())
            return "";
        if(i < 0)
        {
            if(j < getFrozenColumnCount())
                return getRowHeader().getColumnName(j);
            else
                return getColumnName(j);
        } else
        {
            return super.getTextAt(i, j);
        }
    }

    public void setTextAt(java.lang.String s, int i, int j)
    {
        i -= getFrozenRowCount();
        if(i < 0)
        {
            if(j < getFrozenColumnCount())
                getRowHeader().setColumnName(j, s);
            else
                setColumnName(j, s);
        } else
        {
            java.lang.Object obj = s;
            try
            {
                java.lang.Class class1 = getColumnClass(j);
                if(class1 != null)
                {
                    diamondedge.util.Variant variant = new Variant(s);
                    variant.changeType(diamondedge.util.Variant.getVarType(class1));
                    obj = variant.toObject();
                }
            }
            catch(java.lang.Exception exception) { }
            super.setValueAt(obj, i, j);
        }
    }

    public java.lang.String getTextAt(int i)
    {
        int j = getColumnCount();
        int k = i / j;
        int l = i % j;
        return getTextAt(k, l);
    }

    public void setTextAt(java.lang.String s, int i)
    {
        int j = getColumnCount();
        int k = i / j;
        int l = i % j;
        setTextAt(s, k, l);
    }

    public void setRowHeight(int i, int j)
    {
        i -= getFrozenRowCount();
        if(i >= getRowCount())
            super.setRowCount(i + 1);
        if(i < 0)
        {
            getTableHeader().setSize(getTableHeader().getWidth(), j);
            getTableHeader().setPreferredSize(new Dimension(getTableHeader().getWidth(), j));
        } else
        {
            super.setRowHeight(i, j);
        }
    }

    public void insertRow(int i, java.lang.String s)
    {
        if(i == 0 && getFrozenRowCount() > 0)
            setTextAt(s, i, 0);
        else
            super.insertRow(i - getFrozenRowCount(), s);
    }

    public void removeRow(int i)
    {
        super.removeRow(i - getFrozenRowCount());
    }

    public void setRowColorEven(java.awt.Color color)
    {
        autoColor2 = color;
    }

    public void setRowColorOdd(java.awt.Color color)
    {
        autoColor4 = color;
    }

    public void setRowColorAlternating(boolean flag)
    {
        if(flag)
        {
            if(autoColor2 == null)
                autoColor2 = getBackground();
            setAutoStyle(2, null, autoColor2, null, autoColor4);
        } else
        {
            setAutoStyle(1, null, null, null, null);
        }
    }

    public void setHeaderBackground(java.awt.Color color)
    {
        setColumnHeaderBackground(color);
        if(getRowHeader() != null)
        {
            getRowHeader().setColumnHeaderBackground(color);
            getRowHeader().setBackground(color);
            getRowHeader().repaint();
        }
    }

    public void setHeaderForeground(java.awt.Color color)
    {
        setColumnHeaderForeground(color);
        if(getRowHeader() != null)
        {
            getRowHeader().setColumnHeaderForeground(color);
            getRowHeader().setForeground(color);
            getRowHeader().repaint();
        }
    }

    public void setHeaderFont(java.awt.Font font)
    {
        setColumnHeaderFont(font);
        if(getRowHeader() != null)
        {
            getRowHeader().setColumnHeaderFont(font);
            getRowHeader().setFont(font);
            getRowHeader().repaint();
        }
    }

    public void setHeaderTextStyle(int i)
    {
        getColumnHeader().getProperties().setTextStyle(i);
        getColumnHeader().repaint();
        if(getRowHeader() != null)
        {
            getRowHeader().getColumnHeader().getProperties().setTextStyle(i);
            getRowHeader().getProperties().setTextStyle(i);
            getRowHeader().repaint();
        }
    }

    public java.awt.Font getCellFont()
    {
        diamondedge.swing.DsCell dscell = getCell();
        java.awt.Font font = dscell.getFont();
        if(font == null)
            return getFont();
        else
            return font;
    }

    private static int _mthfor(int i)
    {
        int j = i & 0xf0;
        j >>= 4;
        return j;
    }

    private static int _mthif(int i)
    {
        int j = i & 0xf;
        return j;
    }

    private static int _mthif(int i, int j)
    {
        return j << 4 | i;
    }

    private static int a(diamondedge.swing.DsCell dscell)
    {
        int i = 18;
        if(dscell != null)
        {
            int j = dscell.getHorizontalAlignment();
            int k = dscell.getVerticalAlignment();
            i = diamondedge.vb.Grid._mthif(j, k);
        }
        return i;
    }

    private diamondedge.swing.DsCell _mthdo(int i)
    {
        if(getRowHeader() != null && i < getFrozenColumnCount())
            return getRowHeader().getColumnHeader().getCellAt(i);
        else
            return getColumnHeader().getCellAt(i);
    }

    public static diamondedge.swing.DsCell getColumnHeaderCell(javax.swing.table.TableColumn tablecolumn)
    {
        return diamondedge.swing.DsTableColumnHeader.getCellAt(tablecolumn);
    }

    public static diamondedge.swing.DsCell getColumnProperties(javax.swing.table.TableColumn tablecolumn)
    {
        return diamondedge.swing.DsTableColumnHeader.getCellAt(tablecolumn);
    }

    public void setColumnAlignment(int i, int j)
    {
        int k = diamondedge.vb.Grid._mthif(j);
        int l = diamondedge.vb.Grid._mthfor(j);
        _mthdo(i).setHorizontalAlignment(k);
        _mthdo(i).setVerticalAlignment(l);
        diamondedge.swing.DsCellProperties dscellproperties = getColumnProperties(i);
        if(dscellproperties != null)
        {
            dscellproperties.setHorizontalAlignment(k);
            dscellproperties.setVerticalAlignment(l);
        }
    }

    public void setColumnHeaderAlignment(int i, int j)
    {
        int k = diamondedge.vb.Grid._mthif(j);
        int l = diamondedge.vb.Grid._mthfor(j);
        _mthdo(i).setHorizontalAlignment(k);
        _mthdo(i).setVerticalAlignment(l);
    }

    public int getCellAlignment()
    {
        return diamondedge.vb.Grid.a(getCell());
    }

    public void setCellAlignment(int i)
    {
        diamondedge.swing.DsCell dscell = getCell();
        if(dscell != null)
        {
            dscell.setHorizontalAlignment(diamondedge.vb.Grid._mthif(i));
            dscell.setVerticalAlignment(diamondedge.vb.Grid._mthfor(i));
        }
    }

    private static void a(diamondedge.swing.DsCell dscell, int i)
    {
        if(dscell != null)
        {
            int j = diamondedge.vb.Grid._mthif(i);
            int k = diamondedge.vb.Grid._mthfor(i);
            int l = dscell.getHorizontalAlignment();
            int i1 = dscell.getVerticalAlignment();
            int j1 = dscell.getHorizontalAlignment();
            int k1 = dscell.getVerticalAlignment();
            if(j == l)
                j1 = 0;
            else
            if(j == 2)
                j1 = 4;
            else
            if(j == 4)
                j1 = 2;
            if(k == i1)
                k1 = 0;
            else
            if(k == 1)
                k1 = 3;
            else
            if(k == 3)
                k1 = 1;
            dscell.setHorizontalTextPosition(j1);
            dscell.setVerticalTextPosition(k1);
        }
    }

    public void setCellIconAlignment(int i)
    {
        diamondedge.swing.DsCell dscell = getCell();
        if(dscell != null)
        {
            int j = diamondedge.vb.Grid._mthif(i);
            int k = diamondedge.vb.Grid._mthfor(i);
            int l = dscell.getHorizontalAlignment();
            int i1 = dscell.getVerticalAlignment();
            int j1 = dscell.getHorizontalAlignment();
            int k1 = dscell.getVerticalAlignment();
            if(j == l)
                j1 = 0;
            else
            if(j == 2)
                j1 = 4;
            else
            if(j == 4)
                j1 = 2;
            if(k == i1)
                k1 = 0;
            else
            if(k == 1)
                k1 = 3;
            else
            if(k == 3)
                k1 = 1;
            dscell.setHorizontalTextPosition(j1);
            dscell.setVerticalTextPosition(k1);
        }
    }

    private diamondedge.util.Variant a(int i, int j)
    {
        java.lang.Object obj = super.getCellValueAt(i, j);
        diamondedge.util.Variant variant = new Variant(obj);
        if(variant.toBoolean())
            variant.set(1);
        else
        if(obj == null)
            variant.set(0);
        else
            variant.set(2);
        return variant;
    }

    public int getCellChecked()
    {
        return a(super.getRow(), super.getCol()).toInt();
    }

    public void setCellChecked(int i)
    {
        setCellProp(5, super.getRow(), super.getCol(), new Variant(i));
    }

    private java.lang.String a(diamondedge.swing.DsCell dscell, int i, int j)
    {
        java.lang.Object obj = dscell.getValue();
        java.lang.String s = null;
        if(getColumnProperties(j).getFormat() != null)
            s = diamondedge.swing.DsTableCell.formatValue(getColumnProperties(j), obj);
        if(getRowProperties(i).getFormat() != null)
            s = diamondedge.swing.DsTableCell.formatValue(getRowProperties(i), obj);
        if(s == null || dscell.getFormat() != null)
            s = diamondedge.swing.DsTableCell.formatValue(dscell, obj);
        return s;
    }

    public diamondedge.util.Variant getCellProp(int i, int j, int k)
    {
        if(i == 0)
            return new Variant(getTextAt(j, k));
        j -= getFrozenRowCount();
        if(i == 5)
            return a(j, k);
        diamondedge.swing.DsCell dscell = getCellAt(j, k);
        switch(i)
        {
        case 1: // '\001'
            return new Variant(dscell.getTextStyle());

        case 2: // '\002'
            return new Variant(diamondedge.vb.Grid.a(dscell));

        case 3: // '\003'
            return new Variant(dscell.getIcon());

        case 4: // '\004'
            return new Variant(2);

        case 6: // '\006'
            return new Variant(dscell.getBackground());

        case 7: // '\007'
            return new Variant(dscell.getForeground());

        case 10: // '\n'
            return new Variant(dscell.getFont());

        case 11: // '\013'
            return new Variant(dscell.getFontName());

        case 12: // '\f'
            return new Variant(dscell.getFontSize());

        case 13: // '\r'
            return new Variant(dscell.isFontBold());

        case 14: // '\016'
            return new Variant(dscell.isFontItalic());

        case 15: // '\017'
            return new Variant(dscell.getFontUnderline());

        case 16: // '\020'
            return new Variant(dscell.isFontStrikeThrough());

        case 18: // '\022'
            return new Variant(dscell.getValue());

        case 19: // '\023'
            return new Variant(a(dscell, j, k));

        case 20: // '\024'
            return new Variant(dscell.getData());

        case 21: // '\025'
            return new Variant(true);

        case 27: // '\033'
            repaint();
            return new Variant();

        case 5: // '\005'
        case 8: // '\b'
        case 9: // '\t'
        case 17: // '\021'
        case 22: // '\026'
        case 23: // '\027'
        case 24: // '\030'
        case 25: // '\031'
        case 26: // '\032'
        default:
            return new Variant();
        }
    }

    public void setCellProp(int i, int j, int k, diamondedge.util.Variant variant)
    {
        setCellProp(i, j, k, j, k, variant);
    }

    public void setCellProp(int i, int j, int k, int l, int i1, diamondedge.util.Variant variant)
    {
        j -= getFrozenRowCount();
        l -= getFrozenRowCount();
        diamondedge.swing.grid.DsCellRange dscellrange = getCells(j, k, l, i1);
        if(i == 5)
        {
            switch(variant.toInt())
            {
            case 0: // '\0'
                dscellrange.setValue(null);
                break;

            case 2: // '\002'
            case 5: // '\005'
                dscellrange.setValue(java.lang.Boolean.FALSE);
                break;

            default:
                dscellrange.setValue(java.lang.Boolean.TRUE);
                break;
            }
            return;
        }
        try
        {
            switch(i)
            {
            case 0: // '\0'
                dscellrange.setValue(variant.toObject());
                break;

            case 1: // '\001'
                dscellrange.setTextStyle(variant.toInt());
                break;

            case 3: // '\003'
                dscellrange.setIcon((javax.swing.Icon)variant.toObject());
                break;

            case 6: // '\006'
                dscellrange.setBackground(a(variant));
                break;

            case 7: // '\007'
                dscellrange.setForeground(a(variant));
                break;

            case 10: // '\n'
                dscellrange.setFont((java.awt.Font)variant.toObject());
                break;

            case 11: // '\013'
                dscellrange.setFontName(variant.toString());
                break;

            case 12: // '\f'
                dscellrange.setFontSize(variant.toInt());
                break;

            case 13: // '\r'
                dscellrange.setFontBold(variant.toBoolean());
                break;

            case 14: // '\016'
                dscellrange.setFontItalic(variant.toBoolean());
                break;

            case 15: // '\017'
                dscellrange.setFontUnderline(variant.toInt());
                break;

            case 16: // '\020'
                dscellrange.setFontStrikeThrough(variant.toBoolean());
                break;

            case 18: // '\022'
                dscellrange.setValue(variant.toObject());
                break;

            case 19: // '\023'
                dscellrange.setValue(variant.toString());
                break;

            case 20: // '\024'
                dscellrange.setData(variant.toObject());
                break;

            case 21: // '\025'
                dscellrange.clear(true);
                break;

            case 27: // '\033'
                repaint();
                break;

            case 2: // '\002'
                dscellrange.setHorizontalAlignment(diamondedge.vb.Grid._mthif(variant.toInt()));
                dscellrange.setVerticalAlignment(diamondedge.vb.Grid._mthfor(variant.toInt()));
                break;

            case 4: // '\004'
                diamondedge.swing.DsCell dscell = getCellAt(j, k);
                diamondedge.vb.Grid.a(dscell, variant.toInt());
                dscellrange.setHorizontalTextPosition(dscell.getHorizontalTextPosition());
                dscellrange.setVerticalTextPosition(dscell.getVerticalTextPosition());
                break;
            }
        }
        catch(java.lang.Exception exception)
        {
            throw new IllegalArgumentException("Value not valid: " + variant);
        }
    }

    private java.awt.Color a(diamondedge.util.Variant variant)
    {
        java.lang.Object obj = variant.toObject();
        if(obj instanceof java.awt.Color)
            return (java.awt.Color)obj;
        else
            return diamondedge.vb.VbColor.javaColor(variant.toInt());
    }

    public int getScrollBarPolicy()
    {
        javax.swing.JScrollPane jscrollpane = diamondedge.vb.Grid.getScrollPane(this);
        if(jscrollpane == null)
            return 0;
        else
            return diamondedge.vb.ScrolledComponent.a(jscrollpane);
    }

    public void setScrollBarPolicy(int i)
    {
        javax.swing.JScrollPane jscrollpane = diamondedge.vb.Grid.getScrollPane(this);
        java.awt.Container container = getParent();
        if(jscrollpane != null)
            container = jscrollpane.getParent();
        if(i == 0)
        {
            if(jscrollpane != null && !isColumnHeaderVisible() && !isRowHeaderVisible())
            {
                container.remove(jscrollpane);
                container.add(this);
                setBounds(jscrollpane.getBounds());
            }
        } else
        if(jscrollpane == null)
        {
            boolean flag = isColumnHeaderVisible();
            container.remove(this);
            jscrollpane = new JScrollPane(this);
            jscrollpane.setBounds(getBounds());
            container.add(jscrollpane);
            configureEnclosingScrollPane();
            setColumnHeaderVisible(flag);
        }
        diamondedge.vb.ScrolledComponent.a(jscrollpane, i);
    }

    public boolean isVisible()
    {
        javax.swing.JScrollPane jscrollpane = diamondedge.vb.Grid.getScrollPane(this);
        if(jscrollpane == null)
            return super.isVisible();
        else
            return jscrollpane.isVisible();
    }

    public void setVisible(boolean flag)
    {
        javax.swing.JScrollPane jscrollpane = diamondedge.vb.Grid.getScrollPane(this);
        if(jscrollpane == null)
            super.setVisible(flag);
        else
            jscrollpane.setVisible(flag);
    }

    public diamondedge.util.ListFormat buildComboList(diamondedge.ado.Recordset recordset, java.lang.String s, java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        diamondedge.util.ListFormat listformat = new ListFormat();
        if(!recordset.isEOF())
        {
            java.lang.Object obj = recordset.getField(s1).getValue().toObject();
            listformat.setDataValueType(obj != null ? obj.getClass() : null);
        }
        for(; !recordset.isEOF(); recordset.moveNext())
            listformat.add(recordset.getField(s1).getValue().toObject(), recordset.getField(s).getValue().toString());

        return listformat;
    }

    public java.lang.String getTag()
    {
        return tag;
    }

    public void setTag(java.lang.String s)
    {
        tag = s;
    }

    public int getTabOrder()
    {
        return tabOrder;
    }

    public void setTabOrder(int i)
    {
        tabOrder = i;
    }

    public java.lang.String getDataField()
    {
        return aL;
    }

    public void setDataField(java.lang.String s)
    {
        aL = s;
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return aK;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        if(aK != null)
            aK.removeDataBound(this);
        if(datasource != null)
        {
            datasource.addDataBound(this);
            if(getGridModel() instanceof diamondedge.vb.RecordsetTableModel)
            {
                diamondedge.vb.RecordsetTableModel recordsettablemodel = (diamondedge.vb.RecordsetTableModel)getGridModel();
                recordsettablemodel.setDataSource(datasource);
                recordsettablemodel.fireTableStructureChanged();
            } else
            {
                diamondedge.vb.RecordsetTableModel recordsettablemodel1 = new RecordsetTableModel(datasource);
                diamondedge.ado.Recordset recordset = recordsettablemodel1.getRecordset();
                setModel(recordsettablemodel1);
            }
            curRecord = 1;
            revalidate();
            repaint();
        }
        aK = datasource;
    }

    public diamondedge.util.Variant getDataValue()
    {
        return new Variant(getText());
    }

    public void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent)
    {
        if(isEditing())
        {
            javax.swing.table.TableCellEditor tablecelleditor = getCellEditor();
            if(tablecelleditor != null)
                tablecelleditor.stopCellEditing();
        }
        int i = 1;
        try
        {
            i = datachangeevent.getRecordset().getAbsolutePosition();
        }
        catch(diamondedge.ado.AdoError adoerror) { }
        if(datachangeevent.getChangeType() == 7 || datachangeevent.getChangeType() == 1)
        {
            diamondedge.vb.RecordsetTableModel recordsettablemodel = null;
            if(getGridModel() instanceof diamondedge.vb.RecordsetTableModel)
                recordsettablemodel = (diamondedge.vb.RecordsetTableModel)getGridModel();
            else
            if((getGridModel() instanceof diamondedge.swing.grid.DelegateGridModel) && (((diamondedge.swing.grid.DelegateGridModel)getGridModel()).getModel() instanceof diamondedge.vb.RecordsetTableModel))
                recordsettablemodel = (diamondedge.vb.RecordsetTableModel)((diamondedge.swing.grid.DelegateGridModel)getGridModel()).getModel();
            if(recordsettablemodel != null)
            {
                recordsettablemodel.a();
                recordsettablemodel.fireTableDataChanged();
            }
            curRecord = i;
            repaint();
        } else
        if(i != curRecord)
        {
            int j = getRowHeight() + getRowMargin();
            java.awt.Rectangle rectangle = new Rectangle(0, (curRecord - 1) * j, getColumnModel().getTotalColumnWidth(), curRecord * j);
            repaint(rectangle);
            curRecord = i;
        }
        if(--i >= 0 && i < getRowCount())
            setRow(i);
    }

    public void setGridLines(int i)
    {
        setShowGrid(false);
        if((i & 1) > 0)
            setShowVerticalLines(true);
        if((i & 2) > 0)
            setShowHorizontalLines(true);
    }

    public void setScrollAreaBackground(java.awt.Color color)
    {
        javax.swing.JScrollPane jscrollpane = diamondedge.vb.Grid.getScrollPane(this);
        if(jscrollpane != null)
            jscrollpane.setBackground(color);
    }

    public boolean isDataChanged()
    {
        if(getGridModel() instanceof diamondedge.swing.grid.GridRowModel)
        {
            diamondedge.swing.grid.GridRowModel gridrowmodel = (diamondedge.swing.grid.GridRowModel)getGridModel();
            return gridrowmodel.isChanged();
        } else
        {
            throw new IllegalArgumentException("Not bound to a Recordset.");
        }
    }

    public void setDataChanged(boolean flag)
    {
        if(getGridModel() instanceof diamondedge.swing.grid.GridRowModel)
        {
            diamondedge.swing.grid.GridRowModel gridrowmodel = (diamondedge.swing.grid.GridRowModel)getGridModel();
            gridrowmodel.setChanged(flag);
        } else
        {
            throw new IllegalArgumentException("Not bound to a Recordset.");
        }
    }

    public diamondedge.ado.Recordset getRecordset()
    {
        if(getGridModel() instanceof diamondedge.vb.RecordsetTableModel)
        {
            diamondedge.vb.RecordsetTableModel recordsettablemodel = (diamondedge.vb.RecordsetTableModel)getGridModel();
            return recordsettablemodel.getRecordset();
        } else
        {
            throw new IllegalArgumentException("Not bound to a Recordset.");
        }
    }

    public void setRecordset(diamondedge.ado.Recordset recordset)
    {
        if(getGridModel() instanceof diamondedge.vb.RecordsetTableModel)
        {
            diamondedge.vb.RecordsetTableModel recordsettablemodel = (diamondedge.vb.RecordsetTableModel)getGridModel();
            recordsettablemodel.setDataSource(recordset);
        } else
        {
            throw new IllegalArgumentException("Cannot change Recordset.");
        }
    }

    public static final int ALIGN_LEFT_TOP = 18;
    public static final int ALIGN_LEFT_CENTER = 2;
    public static final int ALIGN_LEFT_BOTTOM = 50;
    public static final int ALIGN_CENTER_TOP = 16;
    public static final int ALIGN_CENTER_CENTER = 0;
    public static final int ALIGN_CENTER_BOTTOM = 48;
    public static final int ALIGN_RIGHT_TOP = 20;
    public static final int ALIGN_RIGHT_CENTER = 4;
    public static final int ALIGN_RIGHT_BOTTOM = 52;
    public static final int TEXT_PROP = 0;
    public static final int TEXT_STYLE_PROP = 1;
    public static final int ALIGNMENT_PROP = 2;
    public static final int PICTURE_PROP = 3;
    public static final int PICTURE_ALIGNMENT_PROP = 4;
    public static final int CHECKED_PROP = 5;
    public static final int BACKGROUND_PROP = 6;
    public static final int FOREGROUND_PROP = 7;
    public static final int FONT_PROP = 10;
    public static final int FONT_NAME_PROP = 11;
    public static final int FONT_SIZE_PROP = 12;
    public static final int FONT_BOLD_PROP = 13;
    public static final int FONT_ITALIC_PROP = 14;
    public static final int FONT_UNDERLINE_PROP = 15;
    public static final int FONT_STRIKETHRU_PROP = 16;
    public static final int VALUE_PROP = 18;
    public static final int TEXT_DISPLAY_PROP = 19;
    public static final int DATA_PROP = 20;
    public static final int CUSTOM_FORMAT_PROP = 21;
    public static final int REFRESH_PROP = 27;
    public static final int UNSUP_PROP = -1;
    public static final int NO_CHECKBOX = 0;
    public static final int CHECKED = 1;
    public static final int UNCHECKED = 2;
    public static final int GRID_LINES_NONE = 0;
    public static final int GRID_LINES_VERTICAL = 1;
    public static final int GRID_LINES_HORIZONTAL = 2;
    public static final int GRID_LINES_BOTH = 3;
    protected boolean countFrozenRow = false;
    protected java.lang.String tag = null;
    protected int tabOrder = 0;
    private java.lang.String aL = null;
    private diamondedge.vb.DataSource aK = null;
    protected int curRecord = 0;
}
