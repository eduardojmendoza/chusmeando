// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.AWTEventMulticaster;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.util.Vector;

// Referenced classes of package diamondedge.vb:
//            Screen

public class AwtLabel extends java.awt.Component
{

    public AwtLabel()
    {
        margin_width = 1;
        margin_height = 0;
        maxChars = -1;
        alignment = 0;
        opaque = true;
        labelChangedListener = null;
        setBackground(java.awt.SystemColor.control);
        setForeground(java.awt.SystemColor.menuText);
        setFont(diamondedge.vb.Screen.getDefaultFont());
    }

    public AwtLabel(java.lang.String s, int i, int j, int k, int l)
    {
        margin_width = 1;
        margin_height = 0;
        maxChars = -1;
        alignment = 0;
        opaque = true;
        labelChangedListener = null;
        margin_width = i;
        margin_height = j;
        alignment = k;
        autoSize = true;
        maxChars = l;
        setFont(diamondedge.vb.Screen.getDefaultFont());
        setBackground(java.awt.SystemColor.control);
        setForeground(java.awt.SystemColor.menuText);
        newLabel(s);
    }

    public int getBackStyle()
    {
        return opaque ? 1 : 0;
    }

    public void setBackStyle(int i)
    {
        if(i == 0)
            opaque = false;
        else
            opaque = true;
    }

    public boolean isAutoSize()
    {
        return autoSize;
    }

    public void setAutoSize(boolean flag)
    {
        autoSize = flag;
    }

    public boolean isWordWrap()
    {
        return wordWrap;
    }

    public void setWordWrap(boolean flag)
    {
        wordWrap = flag;
    }

    public int getAlignment()
    {
        return alignment;
    }

    public void setAlignment(int i)
    {
        alignment = i;
        repaint();
    }

    public java.lang.String getTag()
    {
        return a;
    }

    public void setTag(java.lang.String s)
    {
        a = s;
    }

    protected void newLabel(java.lang.String s)
    {
        if(s == null)
            s = "";
        java.lang.String s1 = label;
        label = s.replace('\t', ' ');
        if(wordWrap || !autoSize)
        {
            java.awt.FontMetrics fontmetrics;
            if(getFont() == null)
                fontmetrics = getFontMetrics(diamondedge.vb.Screen.getDefaultFont());
            else
                fontmetrics = getFontMetrics(getFont());
            if(fontmetrics != null)
                if(fontmetrics.stringWidth(s) <= getSize().width)
                    maxChars = s.length();
                else
                    maxChars = getSize().width / fontmetrics.stringWidth("x");
        }
        lines = new Vector();
        int i = s.length();
        for(int j = 0; j != -1 && j < i;)
        {
            int k = s.indexOf("\n", j);
            java.lang.String s2;
            if(k == -1)
            {
                s2 = s.substring(j);
                j = -1;
            } else
            {
                s2 = s.substring(j, k);
                j = k + 1;
            }
            if(maxChars == -1 || s2.length() <= maxChars)
            {
                lines.addElement(s2);
            } else
            {
                int l;
                for(; s2.length() > maxChars; s2 = s2.substring(l + 1))
                {
                    l = s2.lastIndexOf(' ', maxChars);
                    if(l == -1)
                    {
                        l = s2.indexOf(' ');
                        if(l == -1)
                            break;
                    }
                    lines.addElement(s2.substring(0, l));
                }

                lines.addElement(s2);
            }
        }

        num_lines = lines.size();
        line_widths = new int[num_lines];
        if(s1 != s)
            fireLabelChangedEvent();
    }

    protected void measure()
    {
        if(getFont() == null)
            return;
        java.awt.FontMetrics fontmetrics = getFontMetrics(getFont());
        if(fontmetrics == null)
            return;
        line_height = fontmetrics.getHeight();
        line_ascent = fontmetrics.getAscent();
        max_width = 0;
        for(int i = 0; i < num_lines; i++)
        {
            line_widths[i] = fontmetrics.stringWidth((java.lang.String)lines.elementAt(i));
            if(line_widths[i] > max_width)
                max_width = line_widths[i];
        }

    }

    public void setText(java.lang.String s)
    {
        maxChars = -1;
        newLabel(s);
        measure();
        setSize(getPreferredSize());
        repaint();
    }

    public java.lang.String getText()
    {
        return label;
    }

    public void setFont(java.awt.Font font)
    {
        super.setFont(font);
        newLabel(label);
        measure();
        setSize(getPreferredSize());
        repaint();
    }

    public void setSize(int i, int j)
    {
        super.setSize(i, j);
        newLabel(label);
        measure();
        repaint();
    }

    public void setBounds(java.awt.Rectangle rectangle)
    {
        super.setBounds(rectangle);
        newLabel(label);
        measure();
        repaint();
    }

    public void setMarginWidth(int i)
    {
        margin_width = i;
        repaint();
    }

    public void setMarginHeight(int i)
    {
        margin_height = i;
        repaint();
    }

    public int getMarginWidth()
    {
        return margin_width;
    }

    public int getMarginHeight()
    {
        return margin_height;
    }

    public void addNotify()
    {
        super.addNotify();
        measure();
        setSize(getPreferredSize());
    }

    public void addTextListener(java.awt.event.TextListener textlistener)
    {
        labelChangedListener = java.awt.AWTEventMulticaster.add(labelChangedListener, textlistener);
    }

    public void removeTextListener(java.awt.event.TextListener textlistener)
    {
        labelChangedListener = java.awt.AWTEventMulticaster.remove(labelChangedListener, textlistener);
    }

    protected void fireLabelChangedEvent()
    {
        if(labelChangedListener != null)
            labelChangedListener.textValueChanged(new TextEvent(this, 900));
    }

    public java.awt.Dimension getPreferredSize()
    {
        if(autoSize)
            return new Dimension(max_width + 2 * margin_width, num_lines * line_height + 2 * margin_height);
        else
            return getSize();
    }

    public java.awt.Dimension getMinimumSize()
    {
        if(autoSize)
            return new Dimension(max_width, num_lines * line_height);
        else
            return getSize();
    }

    public void paint(java.awt.Graphics g)
    {
        java.awt.Dimension dimension = getSize();
        if(opaque)
        {
            g.setColor(getBackground());
            g.fillRect(0, 0, dimension.width - 1, dimension.height - 1);
        }
        g.setColor(getForeground());
        g.setFont(getFont());
        int j = line_ascent + (dimension.height - num_lines * line_height) / 2;
        for(int k = 0; k < num_lines;)
        {
            int i;
            switch(alignment)
            {
            case 0: // '\0'
                i = 1;
                break;

            case 2: // '\002'
            default:
                i = (dimension.width - line_widths[k]) / 2;
                break;

            case 1: // '\001'
                i = dimension.width - line_widths[k];
                break;
            }
            g.drawString(((java.lang.String)lines.elementAt(k)).trim(), i, j);
            k++;
            j += line_height;
        }

    }

    public static final int LEFT = 0;
    public static final int RIGHT = 1;
    public static final int CENTER = 2;
    private java.lang.String a = null;
    protected java.lang.String label = null;
    protected java.util.Vector lines = null;
    protected int num_lines = 0;
    protected int margin_width = 0;
    protected int margin_height = 0;
    protected int maxChars = 0;
    protected int line_height = 0;
    protected int line_ascent = 0;
    protected int line_widths[] = null;
    protected int max_width = 0;
    protected int alignment = 0;
    protected boolean opaque = false;
    protected boolean autoSize = false;
    protected boolean wordWrap = false;
    protected java.awt.event.TextListener labelChangedListener = null;
}
