// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.swing.DsLabel;
import diamondedge.util.Variant;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.border.Border;

// Referenced classes of package diamondedge.vb:
//            DataBound, VbControl, Screen, DataSource, 
//            DataBoundRouter, DataChangeEvent

public class VbLabelEx extends diamondedge.swing.DsLabel
    implements diamondedge.vb.DataBound, diamondedge.vb.VbControl
{

    public VbLabelEx()
    {
        ag = true;
        af = false;
        ai = null;
        setFont(diamondedge.vb.Screen.getDefaultFont());
        setWordWrap(true);
    }

    public static void initSSPanel(diamondedge.vb.VbLabelEx vblabelex)
    {
        vblabelex.setOuterBorder(9);
        vblabelex.setHorizontalAlignment(0);
        vblabelex.setVerticalAlignment(0);
        vblabelex.setOutline3D(false);
    }

    public java.lang.String getTag()
    {
        return ae;
    }

    public void setTag(java.lang.String s)
    {
        ae = s;
    }

    public int getTabOrder()
    {
        return ah;
    }

    public void setTabOrder(int i)
    {
        ah = i;
    }

    public boolean isOutlined()
    {
        return af;
    }

    public void setOutlined(boolean flag)
    {
        af = flag;
        setBorders();
    }

    public boolean isOutline3D()
    {
        return ag;
    }

    public void setOutline3D(boolean flag)
    {
        if(ag != flag)
        {
            ag = flag;
            setBorders();
        }
    }

    protected javax.swing.border.Border createBorder()
    {
        javax.swing.border.Border border = super.createBorder();
        java.lang.Object obj = null;
        if(af)
        {
            if(ag)
                obj = javax.swing.BorderFactory.createLoweredBevelBorder();
            else
                obj = javax.swing.BorderFactory.createLineBorder(java.awt.Color.black, 1);
            if(obj != null && border != null)
                obj = javax.swing.BorderFactory.createCompoundBorder(((javax.swing.border.Border) (obj)), border);
        }
        return ((javax.swing.border.Border) (obj != null ? obj : border));
    }

    public java.lang.String getDataField()
    {
        return aj;
    }

    public void setDataField(java.lang.String s)
    {
        aj = s;
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return ai;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
        if(ai != null)
        {
            ai.removeDataBound(this);
            removeTextListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        if(datasource != null)
        {
            datasource.addDataBound(this);
            addTextListener(diamondedge.vb.DataBoundRouter.dbRouter);
        }
        ai = datasource;
    }

    public diamondedge.util.Variant getDataValue()
    {
        return new Variant(getText());
    }

    public void dataChanged(diamondedge.vb.DataChangeEvent datachangeevent)
    {
        setText(datachangeevent.getValue() != null ? datachangeevent.getValue().toString() : "");
    }

    private int ah = 0;
    private boolean ag = false;
    private boolean af = false;
    private java.lang.String aj = null;
    private diamondedge.vb.DataSource ai = null;
    private java.lang.String ae = null;
}
