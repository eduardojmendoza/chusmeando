// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.UI;
import diamondedge.util.Variant;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

// Referenced classes of package diamondedge.vb:
//            MDIDesktopManager, MenuItem, Form, Screen, 
//            Application, DataSource

public class MDIForm extends javax.swing.JDesktopPane
    implements diamondedge.vb.Form, java.awt.event.WindowListener, javax.swing.event.InternalFrameListener
{

    public MDIForm()
    {
        initialized = false;
        unloaded = false;
        startUpPosition = 0;
        windowListMenu = null;
        ay = null;
        setFont(diamondedge.vb.Screen.getDefaultFont());
        mdiForm = this;
        ay = new MDIDesktopManager(this);
        setDesktopManager(ay);
    }

    protected void finalize()
    {
        Form_Terminate();
    }

    public void init()
    {
        initialize();
    }

    public void initialize()
    {
        Form_Initialize();
        Form_Load();
        unloaded = false;
    }

    public void unload()
    {
        unloaded = true;
        initialized = false;
    }

    public boolean isInitialized()
    {
        return initialized;
    }

    protected void initializeForm(int i)
    {
        initialized = true;
    }

    public void dispose()
    {
        if(frame != null)
            frame.dispose();
        frame = null;
    }

    public java.awt.Component getComponent(java.lang.String s)
    {
        int i = getComponentCount();
        for(int j = 0; j < i; j++)
        {
            java.awt.Component component = getComponent(j);
            if(component.getName().equals(s))
                return component;
        }

        return null;
    }

    public void setSize(int i, int j)
    {
        super.setSize(i, j);
        Form_Resize();
    }

    public void setFormSize(int i, int j)
    {
        java.awt.Dimension dimension = new Dimension(i, j);
        setPreferredSize(dimension);
        setMinimumSize(dimension);
        if(frame != null)
            if(isVisible())
                frame.pack();
            else
                frame.setSize(i, j);
    }

    public int getFormX()
    {
        if(frame != null)
            return frame.getX();
        else
            return locationX;
    }

    public int getFormY()
    {
        if(frame != null)
            return frame.getY();
        else
            return locationY;
    }

    public void setFormLocation(int i, int j)
    {
        locationX = i;
        locationY = j;
        if(frame != null)
            frame.setLocation(locationX, locationY);
    }

    public int getStartUpPosition()
    {
        return startUpPosition;
    }

    public void setStartUpPosition(int i)
    {
        startUpPosition = i;
    }

    public void add(javax.swing.JMenu jmenu)
    {
        if(frame != null)
        {
            javax.swing.JMenuBar jmenubar = frame.getJMenuBar();
            if(jmenubar == null)
            {
                jmenubar = new JMenuBar();
                frame.setJMenuBar(jmenubar);
            }
            jmenubar.add(jmenu);
        }
    }

    public void add(javax.swing.JInternalFrame jinternalframe)
    {
        super.add(jinternalframe);
        if(windowListMenu != null)
        {
            jinternalframe.addInternalFrameListener(this);
            diamondedge.vb.MenuItem menuitem = new MenuItem();
            java.lang.String s = jinternalframe.getTitle();
            if(s == null || s.equals(""))
                s = jinternalframe.getName();
            menuitem.setText(s);
            menuitem.setTag(jinternalframe);
            menuitem.setName(s);
            windowListMenu.add(menuitem);
            menuitem.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent actionevent)
                {
                    if(actionevent.getSource() instanceof diamondedge.vb.MenuItem)
                    {
                        diamondedge.vb.MenuItem menuitem1 = (diamondedge.vb.MenuItem)actionevent.getSource();
                        java.lang.Object obj = menuitem1.getTagObject();
                        if(obj instanceof javax.swing.JInternalFrame)
                        {
                            javax.swing.JInternalFrame jinternalframe1 = (javax.swing.JInternalFrame)obj;
                            if(jinternalframe1.isIcon())
                                try
                                {
                                    jinternalframe1.setIcon(false);
                                }
                                catch(java.lang.Exception exception) { }
                            jinternalframe1.setVisible(true);
                        }
                    }
                }

            }
);
        }
    }

    public void setWindowListMenu(javax.swing.JMenu jmenu)
    {
        windowListMenu = jmenu;
    }

    public java.lang.String getTitle()
    {
        if(frame != null)
            return frame.getTitle();
        else
            return "";
    }

    public void setTitle(java.lang.String s)
    {
        if(frame != null)
            frame.setTitle(s);
    }

    public java.awt.Image getIcon()
    {
        if(frame != null)
            return frame.getIconImage();
        else
            return null;
    }

    public void setIcon(java.lang.String s)
    {
        if(frame != null)
            frame.setIconImage(diamondedge.vb.Screen.loadImage(s));
    }

    public void setIcon(java.awt.Image image)
    {
        if(frame != null)
            frame.setIconImage(image);
    }

    public java.awt.Image getImage()
    {
        return null;
    }

    public int getWindowState()
    {
        return frame == null || frame.getState() != 1 ? 0 : 1;
    }

    public void setWindowState(int i)
    {
        if(frame == null)
            return;
        switch(i)
        {
        case 0: // '\0'
            frame.setState(0);
            break;

        case 1: // '\001'
            frame.setState(1);
            break;

        case 2: // '\002'
            frame.setState(0);
            break;
        }
    }

    public java.awt.Component getActiveControl()
    {
        if(frame != null)
            return frame.getFocusOwner();
        else
            return null;
    }

    public javax.swing.JFrame getFrame()
    {
        if(frame == null)
            frame = diamondedge.util.UI.getJFrame(this);
        return frame;
    }

    public static short buttonFlags(java.awt.event.InputEvent inputevent)
    {
        int i = inputevent.getModifiers();
        short word0 = 0;
        if((i & 0x10) != 0)
            word0 |= 1;
        if((i & 8) != 0)
            word0 |= 2;
        if((i & 4) != 0)
            word0 |= 4;
        return word0;
    }

    public static short shiftFlags(java.awt.event.InputEvent inputevent)
    {
        int i = inputevent.getModifiers();
        short word0 = 0;
        if((i & 1) != 0)
            word0 |= 1;
        if((i & 2) != 0)
            word0 |= 2;
        if((i & 4) != 0)
            word0 |= 4;
        return word0;
    }

    public void windowClosing(java.awt.event.WindowEvent windowevent)
    {
        diamondedge.vb.Application.a(this);
    }

    public void windowActivated(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowDeactivated(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowOpened(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowClosed(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowIconified(java.awt.event.WindowEvent windowevent)
    {
    }

    public void windowDeiconified(java.awt.event.WindowEvent windowevent)
    {
    }

    public void internalFrameClosing(javax.swing.event.InternalFrameEvent internalframeevent)
    {
    }

    public void internalFrameActivated(javax.swing.event.InternalFrameEvent internalframeevent)
    {
    }

    public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent internalframeevent)
    {
    }

    public void internalFrameOpened(javax.swing.event.InternalFrameEvent internalframeevent)
    {
    }

    public void internalFrameClosed(javax.swing.event.InternalFrameEvent internalframeevent)
    {
        if(windowListMenu != null)
        {
            int i = windowListMenu.getMenuComponentCount();
            for(int j = 0; j < i; j++)
            {
                java.awt.Component component = windowListMenu.getMenuComponent(j);
                if(!(component instanceof diamondedge.vb.MenuItem))
                    continue;
                diamondedge.vb.MenuItem menuitem = (diamondedge.vb.MenuItem)component;
                java.lang.Object obj = menuitem.getTagObject();
                if(obj == internalframeevent.getSource())
                {
                    windowListMenu.remove(menuitem);
                    return;
                }
            }

        }
    }

    public void internalFrameIconified(javax.swing.event.InternalFrameEvent internalframeevent)
    {
    }

    public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent internalframeevent)
    {
    }

    public void showForm()
    {
        showForm(0, null);
    }

    public void showForm(int i, diamondedge.vb.Form form)
    {
        setVisible(false);
        if(!initialized || frame == null)
            if(frame == null)
            {
                frame = new JFrame();
                javax.swing.JScrollPane jscrollpane = new JScrollPane();
                jscrollpane.getViewport().add(this);
                frame.getContentPane().add(jscrollpane, "Center");
                frame.addWindowListener(this);
                frame.setVisible(true);
                init();
                setVisible(true);
                frame.pack();
            } else
            {
                init();
                setVisible(true);
            }
        if(frame != null)
        {
            if(startUpPosition == 3)
                frame.setLocation(0, 0);
            else
            if(startUpPosition == 2)
            {
                int j = (diamondedge.vb.Screen.getWidth() - frame.getWidth()) / 2;
                int k = (diamondedge.vb.Screen.getHeight() - frame.getHeight()) / 2;
                frame.setLocation(j, k);
            } else
            {
                frame.setLocation(locationX, locationY);
            }
            frame.setVisible(true);
        }
        diamondedge.vb.Application.a(null, this);
    }

    public void hideForm()
    {
        if(frame != null)
            frame.setVisible(false);
        else
            setVisible(false);
    }

    public void printForm()
    {
    }

    public void zOrder(int i)
    {
        if(frame == null)
            return;
        if(i == 0)
            frame.toFront();
        else
            frame.toBack();
    }

    public void Form_Load()
    {
    }

    public void Form_DragDrop(java.awt.Component component, float f, float f1)
    {
    }

    public void Form_DragOver(java.awt.Component component, float f, float f1, int i)
    {
    }

    public void Form_Resize()
    {
    }

    public void Form_Unload(int i)
    {
    }

    public void Form_Unload(diamondedge.util.Variant variant)
    {
    }

    public void Form_QueryUnload(diamondedge.util.Variant variant, int i)
    {
    }

    public void Form_QueryUnload(int i, int j)
    {
    }

    public void Form_Activate()
    {
    }

    public void Form_Deactivate()
    {
    }

    public void Form_Initialize()
    {
    }

    public void Form_Terminate()
    {
    }

    public diamondedge.util.Variant getDataValue()
    {
        return null;
    }

    public java.lang.String getDataField()
    {
        return null;
    }

    public void setDataField(java.lang.String s)
    {
    }

    public diamondedge.vb.DataSource getDataSource()
    {
        return null;
    }

    public void setDataSource(diamondedge.vb.DataSource datasource)
    {
    }

    public void arrange(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            cascadeFrames();
            break;

        case 1: // '\001'
            tileFramesHorizontal();
            break;

        case 2: // '\002'
            tileFramesVertical();
            break;
        }
    }

    private int a(javax.swing.JInternalFrame ajinternalframe[])
    {
        int i = 0;
        for(int j = 0; j < ajinternalframe.length; j++)
            if(!ajinternalframe[j].isIcon())
                i++;

        return i;
    }

    public void cascadeFrames()
    {
        int i = 0;
        int j = 0;
        javax.swing.JInternalFrame ajinternalframe[] = getAllFrames();
        int k = a(ajinternalframe);
        int l = getBounds().height - 5 - k * 20;
        int i1 = getBounds().width - 5 - k * 20;
        for(int j1 = ajinternalframe.length - 1; j1 >= 0; j1--)
            if(!ajinternalframe[j1].isIcon())
            {
                ajinternalframe[j1].setSize(i1, l);
                ajinternalframe[j1].setLocation(i, j);
                i += 20;
                j += 20;
            }

    }

    public void tileFramesHorizontal()
    {
        javax.swing.JInternalFrame ajinternalframe[] = getAllFrames();
        int i = a(ajinternalframe);
        if(i > 3)
        {
            tileFramesRowCol(false);
            return;
        }
        int j = getBounds().width;
        int k = getBounds().height / i;
        int l = 0;
        int i1 = 0;
        for(int j1 = 0; j1 < ajinternalframe.length; j1++)
            if(!ajinternalframe[j1].isIcon())
            {
                ajinternalframe[j1].setSize(j, k);
                ajinternalframe[j1].setLocation(l, i1);
                i1 += k;
            }

    }

    public void tileFramesVertical()
    {
        javax.swing.JInternalFrame ajinternalframe[] = getAllFrames();
        int i = a(ajinternalframe);
        if(i > 3)
        {
            tileFramesRowCol(true);
            return;
        }
        int j = getBounds().width / i;
        int k = getBounds().height;
        int l = 0;
        int i1 = 0;
        for(int j1 = 0; j1 < ajinternalframe.length; j1++)
            if(!ajinternalframe[j1].isIcon())
            {
                ajinternalframe[j1].setSize(j, k);
                ajinternalframe[j1].setLocation(l, i1);
                l += j;
            }

    }

    public void tileFramesRowCol(boolean flag)
    {
        javax.swing.JInternalFrame ajinternalframe[] = getAllFrames();
        int i = a(ajinternalframe);
        int j = (int)java.lang.Math.sqrt(i);
        int k = j;
        int l = j;
        if(flag)
            l = ((i + j) - 1) / k;
        else
            k = ((i + j) - 1) / l;
        int i1 = getBounds().width / l;
        int j1 = getBounds().height / k;
        boolean flag1 = false;
        boolean flag2 = false;
        for(int i2 = 0; i2 < i; i2++)
            if(!ajinternalframe[i2].isIcon())
            {
                ajinternalframe[i2].setSize(i1, j1);
                int k1 = (i2 % l) * i1;
                int l1 = (i2 / l) * j1;
                ajinternalframe[i2].setLocation(k1, l1);
            }

    }

    public void setAllSize(java.awt.Dimension dimension)
    {
        setMinimumSize(dimension);
        setMaximumSize(dimension);
        setPreferredSize(dimension);
    }

    public void setAllSize(int i, int j)
    {
        setAllSize(new Dimension(i, j));
    }

    public double scaleX(double d, int i, int j)
    {
        return d;
    }

    public double scaleY(double d, int i, int j)
    {
        return d;
    }

    public static final java.lang.String copyright = "Copyright 1996-2005 Diamond Edge, Inc.";
    protected boolean initialized = false;
    protected boolean unloaded = false;
    protected javax.swing.JFrame frame = null;
    protected static diamondedge.vb.MDIForm mdiForm = null;
    protected int startUpPosition = 0;
    protected int locationX = 0;
    protected int locationY = 0;
    protected javax.swing.JMenu windowListMenu = null;
    private diamondedge.vb.MDIDesktopManager ay = null;
    private static final int az = 20;

}
