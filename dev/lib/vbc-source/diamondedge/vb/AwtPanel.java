// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Panel;
import java.util.EventObject;

// Referenced classes of package diamondedge.vb:
//            VbGraphics, Screen, RepaintListener

public class AwtPanel extends java.awt.Panel
{

    public AwtPanel()
    {
        optionButtonGroup = null;
        av = null;
        repaintListener = null;
        font = null;
        au = new VbGraphics(this);
        setLayout(null);
    }

    public java.awt.Image getImage()
    {
        return av;
    }

    public void setImage(java.lang.String s)
    {
        av = diamondedge.vb.Screen.loadImage(s);
        repaint();
    }

    public void setImage(java.awt.Image image)
    {
        av = image;
        repaint();
    }

    public void paint(java.awt.Graphics g)
    {
        super.paint(g);
        if(av != null)
        {
            java.awt.Insets insets = getInsets();
            g.drawImage(av, insets.left, insets.top, this);
        }
        firePaint();
    }

    public java.lang.String getTag()
    {
        return at;
    }

    public void setTag(java.lang.String s)
    {
        at = s;
    }

    public java.awt.Font getFont()
    {
        if(font == null)
            return super.getFont();
        else
            return font;
    }

    public void setFont(java.awt.Font font1)
    {
        font = font1;
    }

    public int getWidth()
    {
        return getSize().width;
    }

    public int getHeight()
    {
        return getSize().height;
    }

    public double getCurrentX()
    {
        return au.getCurrentX();
    }

    public void setCurrentX(double d)
    {
        au.setCurrentX(d);
    }

    public double getCurrentY()
    {
        return au.getCurrentY();
    }

    public void setCurrentY(double d)
    {
        au.setCurrentY(d);
    }

    public int getDrawMode()
    {
        return au.getDrawMode();
    }

    public void setDrawMode(int i)
    {
        au.setDrawMode(i);
    }

    public java.awt.Color getFillColor()
    {
        return au.getFillColor();
    }

    public void setFillColor(java.awt.Color color)
    {
        au.setFillColor(color);
    }

    public int getFillStyle()
    {
        return au.getFillStyle();
    }

    public void setFillStyle(int i)
    {
        au.setFillStyle(i);
    }

    public double getScaleLeft()
    {
        return au.getScaleLeft();
    }

    public void setScaleLeft(double d)
    {
        au.setScaleLeft(d);
    }

    public double getScaleTop()
    {
        return au.getScaleTop();
    }

    public void setScaleTop(double d)
    {
        au.setScaleTop(d);
    }

    public double getScaleWidth()
    {
        return au.getScaleWidth();
    }

    public void setScaleWidth(double d)
    {
        au.setScaleWidth(d);
    }

    public double getScaleHeight()
    {
        return au.getScaleHeight();
    }

    public void setScaleHeight(double d)
    {
        au.setScaleHeight(d);
    }

    public int getScaleMode()
    {
        return au.getScaleMode();
    }

    public void setScaleMode(int i)
    {
        au.setScaleMode(i);
    }

    public void scale(double d, double d1, double d2, double d3)
    {
        au.scale(d, d1, d2, d3);
    }

    public void scale()
    {
        au.scale();
    }

    public double scaleX(double d, int i, int j)
    {
        return au.scaleX(d, i, j);
    }

    public double scaleY(double d, int i, int j)
    {
        return au.scaleY(d, i, j);
    }

    public void addExclusiveButton(java.awt.Checkbox checkbox)
    {
        add(checkbox);
        if(optionButtonGroup == null)
            optionButtonGroup = new CheckboxGroup();
        checkbox.setCheckboxGroup(optionButtonGroup);
    }

    public void circle(double d, double d1, boolean flag, double d2, 
            java.awt.Color color, double d3, double d4, double d5)
    {
        au.circle(d, d1, flag, d2, color, d3, d4, d5);
    }

    public void cls()
    {
        java.awt.Graphics g = getGraphics();
        g.setColor(getBackground());
        java.awt.Dimension dimension = getSize();
        g.fillRect(0, 0, dimension.width, dimension.height);
        setCurrentX(0.0D);
        setCurrentY(0.0D);
    }

    public void line(double d, double d1, boolean flag, double d2, 
            double d3, boolean flag1, java.awt.Color color, int i)
    {
        au.line(d, d1, flag, d2, d3, flag1, color, i);
    }

    public void paintPicture(java.awt.Image image, double d, double d1, double d2, 
            double d3)
    {
        au.paintPicture(image, d, d1, d2, d3);
    }

    public java.awt.Color point(double d, double d1)
    {
        return au.point(d, d1, av);
    }

    public void print(java.lang.String s)
    {
        au.print(s);
    }

    public void pointSet(double d, double d1, boolean flag, java.awt.Color color)
    {
        au.pointSet(d, d1, flag, color);
    }

    public double textWidth(java.lang.String s)
    {
        java.awt.FontMetrics fontmetrics = getFontMetrics(getFont());
        return (double)fontmetrics.stringWidth(s);
    }

    public double textHeight(java.lang.String s)
    {
        java.awt.FontMetrics fontmetrics = getFontMetrics(getFont());
        return (double)fontmetrics.getHeight();
    }

    protected void firePaint()
    {
        if(repaintListener != null)
            repaintListener.repaintRequested(new EventObject(this));
    }

    public void addRepaintListener(diamondedge.vb.RepaintListener repaintlistener)
    {
        repaintListener = repaintlistener;
    }

    public void removeRepaintListener(diamondedge.vb.RepaintListener repaintlistener)
    {
        repaintListener = null;
    }

    protected java.awt.CheckboxGroup optionButtonGroup = null;
    private java.lang.String at = null;
    private java.awt.Image av = null;
    private diamondedge.vb.VbGraphics au = null;
    protected diamondedge.vb.RepaintListener repaintListener = null;
    private java.awt.Font font = null;
}
