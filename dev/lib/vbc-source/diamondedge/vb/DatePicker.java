// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.swing.DsDatePicker;
import java.util.Calendar;
import javax.swing.JFormattedTextField;

// Referenced classes of package diamondedge.vb:
//            VbControl

public class DatePicker extends diamondedge.swing.DsDatePicker
    implements diamondedge.vb.VbControl
{

    public DatePicker()
    {
        setNullAllowed(false);
    }

    public int getDay()
    {
        return getCalendar().get(5);
    }

    public void setDay(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(5, i);
        field.setValue(calendar.getTime());
    }

    public int getDayOfWeek()
    {
        return getCalendar().get(7);
    }

    public void setDayOfWeek(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(7, i);
        field.setValue(calendar.getTime());
    }

    public int getHour()
    {
        return getCalendar().get(11);
    }

    public void setHour(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(11, i);
        field.setValue(calendar.getTime());
    }

    public int getMinute()
    {
        return getCalendar().get(12);
    }

    public void setMinute(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(12, i);
        field.setValue(calendar.getTime());
    }

    public int getMonth()
    {
        return getCalendar().get(2) + 1;
    }

    public void setMonth(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(2, i - 1);
        field.setValue(calendar.getTime());
    }

    public int getSecond()
    {
        return getCalendar().get(13);
    }

    public void setSecond(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(13, i);
        field.setValue(calendar.getTime());
    }

    public java.lang.String getText()
    {
        return field.getText();
    }

    public int getYear()
    {
        return getCalendar().get(1);
    }

    public void setYear(int i)
    {
        java.util.Calendar calendar = getCalendar();
        calendar.set(1, i);
        field.setValue(calendar.getTime());
    }

    public int getTabOrder()
    {
        return Z;
    }

    public void setTabOrder(int i)
    {
        Z = i;
    }

    private int Z = 0;
}
