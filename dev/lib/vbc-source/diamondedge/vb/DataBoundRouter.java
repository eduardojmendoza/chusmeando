// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.ado.AdoError;
import diamondedge.ado.Field;
import diamondedge.ado.Fields;
import diamondedge.ado.Recordset;
import diamondedge.util.Err;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.io.PrintStream;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

// Referenced classes of package diamondedge.vb:
//            DataBound, DataSource

public class DataBoundRouter
    implements java.awt.event.TextListener, java.awt.event.ItemListener, javax.swing.event.ListSelectionListener
{

    public DataBoundRouter()
    {
        a = 0;
    }

    public boolean isInitializing()
    {
        return a > 0;
    }

    public void setInitializing(boolean flag)
    {
        if(flag)
            a++;
        else
            a--;
        if(a < 0)
        {
            java.lang.System.out.print("error: DataBoundRouter.initializing < 0: ");
            diamondedge.util.Err.printStackTrace();
        }
    }

    private void a(diamondedge.vb.DataBound databound)
    {
        if(a > 0)
            return;
        if(databound != null)
            try
            {
                diamondedge.ado.Recordset recordset = databound.getDataSource() == null ? null : databound.getDataSource().getRecordset();
                if(recordset != null && databound.getDataField() != null)
                {
                    diamondedge.ado.Field field = recordset.getFields().getField(databound.getDataField());
                    if(field != null)
                        field.setValue(databound.getDataValue());
                }
            }
            catch(diamondedge.ado.AdoError adoerror)
            {
                java.lang.System.out.println("DataBoundRouter.changeValue " + adoerror);
            }
    }

    private void a(diamondedge.vb.DataBound databound, int i)
    {
    }

    public void textValueChanged(java.awt.event.TextEvent textevent)
    {
        a((diamondedge.vb.DataBound)textevent.getSource());
    }

    public void itemStateChanged(java.awt.event.ItemEvent itemevent)
    {
        a((diamondedge.vb.DataBound)itemevent.getSource());
    }

    public void valueChanged(javax.swing.event.ListSelectionEvent listselectionevent)
    {
        a((diamondedge.vb.DataBound)listselectionevent.getSource());
    }

    public static diamondedge.vb.DataBoundRouter dbRouter = new DataBoundRouter();
    private int a = 0;

}
