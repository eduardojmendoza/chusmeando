// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.util.Variant;
import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.Component;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package diamondedge.vb:
//            Form

public class Application
{

    public Application(java.lang.String s)
    {
        a = new Vector(12);
        _fldfor = null;
        _flddo = "";
        _fldint = false;
        _fldif = s;
        diamondedge.vb.Application.setApplication(this);
    }

    public final java.lang.String getName()
    {
        return _fldif;
    }

    public final java.lang.String getCommandLine()
    {
        return _flddo;
    }

    public static diamondedge.vb.Application getCurrent()
    {
        return (diamondedge.vb.Application)_fldnew.get();
    }

    public static final diamondedge.vb.Application getApplication()
    {
        return diamondedge.vb.Application.getCurrent();
    }

    public static final void setApplication(diamondedge.vb.Application application)
    {
        _fldnew.set(application);
    }

    public static final java.applet.Applet getApplet(java.awt.Component component)
    {
        for(; component != null; component = component.getParent())
            if(component instanceof java.applet.Applet)
                return (java.applet.Applet)component;

        return null;
    }

    public final boolean isAppletRunning()
    {
        return getApplet() != null;
    }

    public final java.applet.Applet getApplet()
    {
        return _fldfor;
    }

    public final void setApplet(java.applet.Applet applet)
    {
        _fldfor = applet;
        java.lang.String s = applet.getParameter("cmd");
        if(s != null)
            _flddo = s;
    }

    public final void setApplication(java.lang.Object obj, java.lang.String as[])
    {
        _fldint = true;
        for(int i = 0; i < as.length; i++)
        {
            if(i != 0)
                _flddo += " ";
            _flddo += as[i];
        }

    }

    public final void addForm(java.lang.String s, diamondedge.vb.Form form)
    {
        if(a.indexOf(form) == -1)
            a.addElement(form);
        if(s != null)
            form.setName(s);
    }

    public final diamondedge.vb.Form getForm(int i)
    {
        return (diamondedge.vb.Form)a.elementAt(i);
    }

    public final diamondedge.vb.Form getForm(java.lang.String s)
    {
        s = s.replace(' ', '_');
        s = s.replace('-', '_');
        s = s.replace('/', '_');
        for(java.util.Enumeration enumeration = getForms(); enumeration.hasMoreElements();)
        {
            diamondedge.vb.Form form = (diamondedge.vb.Form)enumeration.nextElement();
            if(form.getName().equals(s))
                return form;
        }

        return null;
    }

    public final diamondedge.vb.Form getForm(diamondedge.util.Variant variant)
    {
        if(variant.getVarType() != 15 && variant.isNumeric())
            return getForm(variant.toInt());
        else
            return getForm(variant.toString());
    }

    public final int getFormCount()
    {
        return a.size();
    }

    public final java.util.Enumeration getForms()
    {
        return a.elements();
    }

    public static final void load(diamondedge.vb.Form form)
    {
        if(form != null)
            form.showForm();
    }

    public final boolean unload(diamondedge.vb.Form form)
    {
        if(form != null && form.isInitialized())
        {
            diamondedge.util.Variant variant = new Variant(false);
            form.Form_QueryUnload(variant, 1);
            if(variant.toBoolean())
                return false;
            form.Form_QueryUnload(0, 1);
            form.Form_Unload(variant);
            if(variant.toBoolean())
                return false;
            form.Form_Unload(0);
            form.unload();
            java.applet.Applet applet = diamondedge.vb.Application.getApplet((java.awt.Component)form);
            if(applet == null)
            {
                form.hideForm();
                form.dispose();
            }
        }
        a();
        return true;
    }

    static final boolean a(diamondedge.vb.Form form)
    {
        if(diamondedge.vb.Application.getApplication() != null)
            return diamondedge.vb.Application.getApplication().unload(form);
        else
            return true;
    }

    static final void a(java.lang.String s, diamondedge.vb.Form form)
    {
        if(diamondedge.vb.Application.getApplication() != null)
            diamondedge.vb.Application.getApplication().addForm(s, form);
    }

    public final void endApplication()
    {
        _fldint = false;
        a();
    }

    private final void a()
    {
        if(_fldint)
            return;
        java.applet.Applet applet = diamondedge.vb.Application.getApplication().getApplet();
        if(applet != null)
            return;
        for(java.util.Enumeration enumeration = diamondedge.vb.Application.getApplication().getForms(); enumeration.hasMoreElements();)
        {
            diamondedge.vb.Form form = (diamondedge.vb.Form)enumeration.nextElement();
            if(form != null && form.isInitialized())
                return;
        }

        java.lang.System.exit(0);
    }

    public final void showURL(java.lang.String s)
    {
        try
        {
            java.applet.Applet applet = getApplet();
            if(applet != null && s != null)
            {
                java.net.URL url = new URL(s);
                java.applet.AppletContext appletcontext = applet.getAppletContext();
                appletcontext.showDocument(url);
            }
        }
        catch(java.lang.Exception exception)
        {
            exception.printStackTrace();
        }
    }

    public final void playSound(java.lang.String s)
    {
        try
        {
            java.applet.Applet applet = getApplet();
            if(applet != null && s != null)
                applet.play(new URL(s));
        }
        catch(java.lang.Exception exception)
        {
            exception.printStackTrace();
        }
    }

    public static final void Load(diamondedge.vb.Form form)
    {
        diamondedge.vb.Application.load(form);
    }

    public final boolean Unload(diamondedge.vb.Form form)
    {
        return unload(form);
    }

    private static java.lang.ThreadLocal _fldnew = new InheritableThreadLocal();
    private java.util.Vector a = null;
    private java.applet.Applet _fldfor = null;
    private java.lang.String _fldif = null;
    private java.lang.String _flddo = null;
    private boolean _fldint = false;

}
