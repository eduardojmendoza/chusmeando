// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import diamondedge.ado.Recordset;

// Referenced classes of package diamondedge.vb:
//            DataBound

public interface DataSource
{

    public abstract void addDataBound(diamondedge.vb.DataBound databound);

    public abstract void removeDataBound(diamondedge.vb.DataBound databound);

    public abstract diamondedge.ado.Recordset getRecordset();
}
