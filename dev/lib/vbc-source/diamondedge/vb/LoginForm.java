// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Point;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class LoginForm extends java.awt.Dialog
    implements java.awt.event.ActionListener
{

    public LoginForm()
    {
        super(new Frame(), true);
        a = false;
        _fldif = null;
        _fldint = new Label();
        _fldfor = new Label();
        _fldcase = new TextField();
        _fldchar = new TextField();
        _fldtry = new Button();
        _fldnew = new Button();
        _flddo = new Label();
        _fldbyte = new TextField();
        setLayout(null);
        setBackground(java.awt.Color.lightGray);
        setSize(361, 155);
        setVisible(false);
        _fldint.setText("User ID:");
        add(_fldint);
        _fldint.setBounds(12, 18, 96, 18);
        _fldfor.setText("Password:");
        add(_fldfor);
        _fldfor.setBounds(12, 48, 90, 18);
        add(_fldcase);
        _fldcase.setBounds(108, 18, 240, 20);
        _fldchar.setEchoChar('*');
        add(_fldchar);
        _fldchar.setBounds(108, 48, 240, 20);
        _fldtry.setLabel("Login");
        add(_fldtry);
        _fldtry.setBounds(66, 114, 84, 24);
        _fldnew.setLabel("Cancel");
        add(_fldnew);
        _fldnew.setBounds(204, 114, 84, 24);
        _flddo.setText("Database URL:");
        add(_flddo);
        _flddo.setBounds(12, 78, 96, 18);
        _fldbyte.setEditable(false);
        add(_fldbyte);
        _fldbyte.setBackground(java.awt.Color.lightGray);
        _fldbyte.setBounds(108, 78, 240, 20);
        setTitle("Please Login to the Database");
        _fldtry.addActionListener(this);
        _fldnew.addActionListener(this);
    }

    public static java.sql.Connection getConnection(java.lang.String s, java.lang.String s1)
    {
        diamondedge.vb.LoginForm loginform = new LoginForm();
        loginform._fldbyte.setText(s);
        loginform._fldcase.setText(s1);
        loginform.setVisible(true);
        return loginform._fldif;
    }

    public void setVisible(boolean flag)
    {
        if(flag)
            setLocation(300, 300);
        super.setVisible(flag);
    }

    public void actionPerformed(java.awt.event.ActionEvent actionevent)
    {
        if(actionevent.getSource() == _fldtry)
        {
            java.lang.String s = "";
            try
            {
                _fldif = java.sql.DriverManager.getConnection(_fldbyte.getText(), _fldcase.getText(), _fldchar.getText());
                dispose();
            }
            catch(java.sql.SQLException sqlexception)
            {
                setTitle("Login error " + sqlexception.getErrorCode() + ": " + sqlexception.toString());
                java.lang.String s1 = "Error: " + sqlexception.toString() + "\n" + "Error code: " + sqlexception.getErrorCode() + " DatabaseURL: " + _fldbyte.getText();
                java.lang.System.out.println(s1);
            }
        } else
        {
            dispose();
        }
    }

    public void addNotify()
    {
        java.awt.Dimension dimension = getSize();
        super.addNotify();
        if(a)
            return;
        setSize(getInsets().left + getInsets().right + dimension.width, getInsets().top + getInsets().bottom + dimension.height);
        java.awt.Component acomponent[] = getComponents();
        for(int i = 0; i < acomponent.length; i++)
        {
            java.awt.Point point = acomponent[i].getLocation();
            point.translate(getInsets().left, getInsets().top);
            acomponent[i].setLocation(point);
        }

        a = true;
    }

    boolean a = false;
    java.sql.Connection _fldif = null;
    java.awt.Label _fldint = null;
    java.awt.Label _fldfor = null;
    java.awt.TextField _fldcase = null;
    java.awt.TextField _fldchar = null;
    java.awt.Button _fldtry = null;
    java.awt.Button _fldnew = null;
    java.awt.Label _flddo = null;
    java.awt.TextField _fldbyte = null;
}
