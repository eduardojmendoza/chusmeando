// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.vb;

import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;

// Referenced classes of package diamondedge.vb:
//            VbControl, Screen, JForm

public class CommandButton extends javax.swing.JButton
    implements diamondedge.vb.VbControl
{

    public CommandButton()
    {
        setFont(diamondedge.vb.Screen.getDefaultFont());
        int i = 1;
        byte byte0 = 2;
        setMargin(new Insets(byte0, i, byte0, i));
        setHorizontalTextPosition(0);
        setVerticalTextPosition(3);
        setDefaultCapable(false);
    }

    public void setDefaultButton(boolean flag)
    {
        javax.swing.JRootPane jrootpane = javax.swing.SwingUtilities.getRootPane(this);
        if(flag && jrootpane != null)
        {
            setDefaultCapable(true);
            jrootpane.setDefaultButton(this);
        }
    }

    public void setText(java.lang.String s)
    {
        super.setText(diamondedge.vb.JForm.processMnemonic(this, s));
    }

    public java.lang.String getTag()
    {
        return be;
    }

    public void setTag(java.lang.String s)
    {
        be = s;
    }

    public int getTabOrder()
    {
        return bf;
    }

    public void setTabOrder(int i)
    {
        bf = i;
    }

    private java.lang.String be = null;
    private int bf = 0;
}
