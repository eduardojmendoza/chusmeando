// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.ado;

import diamondedge.util.Variant;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

// Referenced classes of package diamondedge.ado:
//            AdoError, Recordset, Connection, AdoUtil, 
//            TypeInfo

public class Field
{

    private Field(diamondedge.ado.Recordset recordset)
    {
        _fldelse = false;
        _fldcase = false;
        _fldvoid = 0;
        f = 0;
        _fldif = 0;
        _fldbyte = 0;
        _fldtry = -1;
        _fldint = 0;
        _fldgoto = recordset;
        e = new Variant();
        d = new Variant();
        e.setNull();
        d.setNull();
    }

    public Field(diamondedge.ado.Recordset recordset, java.sql.ResultSetMetaData resultsetmetadata, int i)
        throws diamondedge.ado.AdoError
    {
        this(recordset);
        a(resultsetmetadata, i);
    }

    public Field(diamondedge.ado.Recordset recordset, diamondedge.ado.Field field)
        throws diamondedge.ado.AdoError
    {
        this(recordset);
        a(field);
    }

    public boolean equals(java.lang.Object obj)
    {
        if(this == obj)
            return true;
        if(obj instanceof diamondedge.ado.Field)
        {
            diamondedge.util.Variant variant = ((diamondedge.ado.Field)obj).e;
            if(e == variant)
                return true;
            if(e != null && e.equals(variant))
                return true;
        }
        return false;
    }

    public int getAttributes()
    {
        return _fldnew;
    }

    public int getActualSize()
    {
        if(e == null)
            return -1;
        java.lang.Object obj = e.toObject();
        if(obj instanceof java.lang.String)
            return ((java.lang.String)obj).length();
        if(obj instanceof byte[])
            return ((byte[])obj).length;
        if(obj instanceof java.io.InputStream)
            return ((java.io.InputStream)obj).available();
        if(obj instanceof java.io.Reader)
            return e.toString().length();
        break MISSING_BLOCK_LABEL_82;
        java.lang.Exception exception;
        exception;
        return _fldint;
    }

    public java.lang.String getName()
    {
        return g;
    }

    public int getDefinedSize()
        throws diamondedge.ado.AdoError
    {
        return _fldint;
    }

    public int getPrecision()
        throws diamondedge.ado.AdoError
    {
        if(_fldvoid <= 0)
            throw new AdoError("error getting column information for: " + getName(), 30007);
        else
            return _fldvoid;
    }

    public int getScale()
    {
        return f;
    }

    public int getType()
    {
        return _fldif;
    }

    public int getSqlType()
    {
        return _fldbyte;
    }

    public boolean isNumericType()
    {
        boolean flag = false;
        switch(_fldbyte)
        {
        case -6: 
        case -5: 
        case 2: // '\002'
        case 4: // '\004'
        case 5: // '\005'
        case 6: // '\006'
        case 7: // '\007'
        case 8: // '\b'
            flag = true;
            // fall through

        case -4: 
        case -3: 
        case -2: 
        case -1: 
        case 0: // '\0'
        case 1: // '\001'
        case 3: // '\003'
        default:
            return flag;
        }
    }

    public java.lang.String getSqlTypeName()
    {
        return _fldlong;
    }

    public synchronized diamondedge.util.Variant getValue()
        throws diamondedge.ado.AdoError
    {
        return e;
    }

    public synchronized void setValue(diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        if(isReadOnly())
        {
            diamondedge.ado.Connection connection = _fldgoto.getActiveConnection();
            if(connection == null || (connection.getWorkArounds() & 4) != 4)
                throw new AdoError("Cannot update a read only column: " + getName(), 30012);
        }
        if(isAutoInc())
            throw new AdoError("Cannot update an auto increment column: " + getName(), 30013);
        _mthcase();
        if(variant == null)
            e = (new Variant()).setNull();
        else
            e = variant;
        _fldcase = true;
    }

    public synchronized void setValue(diamondedge.ado.Field field)
        throws diamondedge.ado.AdoError
    {
        if(field == null)
            setValue((diamondedge.util.Variant)null);
        else
            setValue(field.getValue());
    }

    public boolean isSearchable()
    {
        return _fldfor;
    }

    public boolean isAutoInc()
    {
        return c;
    }

    public boolean isReadOnly()
    {
        return _fldnull;
    }

    public boolean isCurrency()
    {
        return _flddo;
    }

    public int getNullable()
    {
        return _fldchar;
    }

    public boolean isNull()
    {
        boolean flag = e.getVarType() == 1 || e.toObject() == null;
        return flag;
    }

    public java.lang.String getTableName()
    {
        return a;
    }

    public synchronized void appendChunk(diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError, java.io.IOException
    {
        if(!_mthbyte())
            _mthint();
        if(variant == null)
        {
            e = new Variant();
            e.setNull();
            _mthcase();
            _fldcase = true;
        } else
        {
            java.io.InputStream inputstream = null;
            if(_fldcase)
            {
                inputstream = diamondedge.ado.AdoUtil.a(e);
                if(inputstream == null)
                    return;
            } else
            {
                _mthcase();
                e = new Variant();
                e.setNull();
                _fldcase = true;
            }
            diamondedge.ado.AdoUtil.a(e, variant, inputstream);
        }
    }

    public synchronized diamondedge.util.Variant getChunk(int i, int j)
        throws diamondedge.ado.AdoError, java.io.IOException
    {
        if(!_mthbyte())
            _mthint();
        diamondedge.util.Variant variant = new Variant();
        variant.setNull();
        java.io.InputStream inputstream = diamondedge.ado.AdoUtil.a(e);
        if(inputstream != null)
            try
            {
                if(i > 0)
                    inputstream.skip(i);
                int k = inputstream.available();
                int l = j >= k ? k : j;
                if(l > 0)
                {
                    byte abyte0[] = new byte[l];
                    int i1 = inputstream.read(abyte0);
                    variant.set(abyte0);
                }
            }
            catch(java.io.IOException ioexception)
            {
                throw new AdoError(ioexception, "Error retrieving data.", 30003);
            }
        return variant;
    }

    public synchronized diamondedge.util.Variant getChunk(int i)
        throws diamondedge.ado.AdoError, java.io.IOException
    {
        return getChunk(0, i);
    }

    synchronized void a(diamondedge.util.Variant variant)
    {
        if(variant == null)
            e.setNull();
        else
            e = variant;
        d = new Variant();
        d.setNull();
    }

    boolean _mthdo()
    {
        return _fldcase;
    }

    synchronized void _mthif(diamondedge.ado.Field field)
    {
        e = field.e;
    }

    synchronized void _mthnew()
        throws diamondedge.ado.AdoError
    {
        diamondedge.util.Variant variant = new Variant();
        variant.setNull();
        setValue(variant);
    }

    synchronized diamondedge.util.Variant _mthtry()
    {
        return d;
    }

    synchronized void a()
        throws diamondedge.ado.AdoError
    {
        d = new Variant();
        d.setNull();
        _fldcase = false;
    }

    synchronized void _mthif()
        throws diamondedge.ado.AdoError
    {
        e = d;
        d = new Variant();
        d.setNull();
        _fldcase = false;
    }

    synchronized void _mthcase()
        throws diamondedge.ado.AdoError
    {
        if(!_fldcase || d.isNull())
            d = e;
    }

    void a(java.lang.String s)
    {
        a = s;
    }

    private boolean _mthbyte()
    {
        boolean flag = false;
        if(_fldgoto != null)
            flag = _fldgoto.Y();
        return flag;
    }

    private void _mthint()
        throws diamondedge.ado.AdoError
    {
        throw new AdoError("The cursor must be positioned on a valid record to access the field value.", 30006);
    }

    private void a(java.sql.ResultSetMetaData resultsetmetadata, int i)
        throws diamondedge.ado.AdoError
    {
        try
        {
            g = resultsetmetadata.getColumnName(i);
            _fldlong = resultsetmetadata.getColumnTypeName(i);
            _fldbyte = resultsetmetadata.getColumnType(i);
            f = resultsetmetadata.getScale(i);
            try
            {
                _fldfor = resultsetmetadata.isSearchable(i);
            }
            catch(java.sql.SQLException sqlexception)
            {
                if(_fldbyte == -1 || _fldbyte == -4)
                    _fldfor = false;
                else
                    _fldfor = true;
            }
            _fldnull = resultsetmetadata.isReadOnly(i);
            b = resultsetmetadata.isWritable(i);
            _fldchar = resultsetmetadata.isNullable(i);
            c = resultsetmetadata.isAutoIncrement(i);
            _flddo = resultsetmetadata.isCurrency(i);
            _fldint = resultsetmetadata.getColumnDisplaySize(i);
            _fldvoid = -1;
            try
            {
                _fldvoid = resultsetmetadata.getPrecision(i);
            }
            catch(java.lang.Exception exception) { }
            if(_fldvoid <= 0)
                switch(_fldbyte)
                {
                default:
                    break;

                case 1: // '\001'
                case 12: // '\f'
                    _fldvoid = _fldint;
                    break;

                case 91: // '['
                case 92: // '\\'
                case 93: // ']'
                    diamondedge.ado.Connection connection = _fldgoto.getActiveConnection();
                    diamondedge.ado.TypeInfo typeinfo = connection.a(_fldbyte);
                    if(typeinfo != null)
                        _fldvoid = typeinfo._fldvoid;
                    break;
                }
            _fldnew = 0;
            if(b)
                _fldnew |= 4;
            if(_fldchar == 1)
                _fldnew |= 0x20;
            if(_fldbyte == -1 || _fldbyte == -4)
                _fldnew |= 0x80;
            _fldif = diamondedge.ado.AdoUtil.getAdoType(_fldbyte);
            try
            {
                a = resultsetmetadata.getTableName(i);
            }
            catch(java.sql.SQLException sqlexception1) { }
        }
        catch(java.sql.SQLException sqlexception2)
        {
            throw new AdoError(sqlexception2, "error getting column information for: " + getName(), 30007);
        }
    }

    private void a(diamondedge.ado.Field field)
        throws diamondedge.ado.AdoError
    {
        g = field.g;
        _fldlong = field._fldlong;
        _fldbyte = field._fldbyte;
        _fldvoid = field._fldvoid;
        f = field.f;
        _fldfor = field._fldfor;
        _fldnull = field._fldnull;
        b = field.b;
        _fldchar = field._fldchar;
        c = field.c;
        _flddo = field._flddo;
        _fldint = field._fldint;
        _fldtry = field._fldtry;
        _fldnew = field._fldnew;
        _fldif = diamondedge.ado.AdoUtil.getAdoType(_fldbyte);
        a = field.a;
    }

    private void _mthfor()
        throws diamondedge.ado.AdoError
    {
        throw new AdoError("Field value is null.", 30008);
    }

    public java.lang.String toString()
    {
        return "Field[name=" + g + ",value=" + e + (_fldcase ? ",dirty" : "") + "]";
    }

    private boolean _fldelse = false;
    private diamondedge.util.Variant e = null;
    private diamondedge.util.Variant d = null;
    private java.lang.String g = null;
    private java.lang.String _fldlong = null;
    private boolean _fldcase = false;
    private int _fldvoid = 0;
    private int f = 0;
    private int _fldif = 0;
    private int _fldbyte = 0;
    private int _fldtry = 0;
    private int _fldint = 0;
    private diamondedge.ado.Recordset _fldgoto = null;
    private boolean _fldfor = false;
    private boolean c = false;
    private boolean _flddo = false;
    private boolean _fldnull = false;
    private boolean b = false;
    private int _fldchar = 0;
    private int _fldnew = 0;
    private java.lang.String a = null;
}
