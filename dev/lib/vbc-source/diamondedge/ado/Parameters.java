// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.ado;

import diamondedge.util.Variant;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

// Referenced classes of package diamondedge.ado:
//            Parameter, AdoError, Command, Connection

public class Parameters
{

    public Parameters()
    {
        this(null);
    }

    public Parameters(diamondedge.ado.Command command)
    {
        this(3, command);
    }

    public Parameters(int i, diamondedge.ado.Command command)
    {
        _fldif = new Vector(i);
        a = command;
    }

    public void append(diamondedge.ado.Parameter parameter)
    {
        _fldif.addElement(parameter);
    }

    public int getCount()
    {
        return _fldif.size();
    }

    public void remove(diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        remove(getParameter(variant));
    }

    public diamondedge.ado.Parameter getParameter(diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.Parameter parameter = null;
        if(variant.getVarType() == 15)
            parameter = getParameter(variant.toString());
        else
        if(variant.isNumeric())
            parameter = getParameter(variant.toInt());
        else
        if(variant.getVarType() == 12)
            parameter = (diamondedge.ado.Parameter)variant.toObject();
        else
            throw new AdoError("Unknown/Unhandled variant type for this operation: getItem", 0x15f91);
        if(parameter == null)
            throw new AdoError("Parameter not found in the collection. ", 40005);
        else
            return parameter;
    }

    public void refresh()
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.Connection connection = a.getActiveConnection();
        if(connection == null || !connection.isConnected())
            throw new AdoError("You must have an open connection before calling: Refresh", 10009);
        java.lang.String s = a._mthchar();
        java.sql.Connection connection1 = connection.getConnection();
        try
        {
            java.sql.DatabaseMetaData databasemetadata = connection1.getMetaData();
            java.lang.String s1 = connection1.getCatalog();
            java.sql.ResultSet resultset = databasemetadata.getProcedureColumns(s1, null, s, null);
            boolean flag = true;
            boolean flag1 = true;
            do
            {
                if(!resultset.next())
                    break;
                byte byte0;
                switch(resultset.getInt("COLUMN_TYPE"))
                {
                case 1: // '\001'
                    byte0 = 1;
                    break;

                case 3: // '\003'
                    continue;

                case 2: // '\002'
                    byte0 = 3;
                    break;

                case 4: // '\004'
                    byte0 = 2;
                    break;

                case 5: // '\005'
                    byte0 = 4;
                    break;

                case 0: // '\0'
                default:
                    throw new AdoError("Error extracting parameter information", 40006);
                }
                diamondedge.ado.Parameter parameter = new Parameter();
                parameter.setSqlType(resultset.getInt(6));
                parameter.setDirection(byte0);
                append(parameter);
            } while(true);
            resultset.close();
        }
        catch(java.sql.SQLException sqlexception)
        {
            throw new AdoError(sqlexception, "Error extracting parameter information", 40006);
        }
    }

    public void remove(diamondedge.ado.Parameter parameter)
        throws diamondedge.ado.AdoError
    {
        if(!_fldif.removeElement(parameter))
            throw new AdoError("Parameter not found in the collection. ", 40005);
        else
            return;
    }

    public diamondedge.ado.Parameter getParameter(int i)
        throws diamondedge.ado.AdoError
    {
        if(i >= _fldif.size())
            throw new AdoError("Parameter array index out of bounds, array size: " + _fldif.size(), 40007);
        else
            return (diamondedge.ado.Parameter)_fldif.elementAt(i);
    }

    public diamondedge.ado.Parameter getParameter(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        int i = _fldif.size();
        for(int j = 0; j < i; j++)
        {
            diamondedge.ado.Parameter parameter = (diamondedge.ado.Parameter)_fldif.elementAt(j);
            if(s.equalsIgnoreCase(parameter.getName()))
                return parameter;
        }

        throw new AdoError("Parameter not found in the collection. " + s, 40005);
    }

    void a(diamondedge.ado.Command command)
    {
        a = command;
    }

    private java.util.Vector _fldif = null;
    private diamondedge.ado.Command a = null;
}
