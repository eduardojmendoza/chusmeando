// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.ado;

import diamondedge.util.Variant;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

// Referenced classes of package diamondedge.ado:
//            AdoError, Fields, Field, AdoUtil, 
//            Recordset

class AdoCursor
    implements java.lang.Runnable
{
    private static class a
    {

        boolean a()
        {
            return (a & _fldfor) > 0;
        }

        void _mthif(boolean flag)
        {
            a &= ~_fldfor;
            if(flag)
                a |= _fldfor;
        }

        boolean _mthif()
        {
            return (a & _flddo) > 0;
        }

        void a(boolean flag)
        {
            a &= ~_flddo;
            if(flag)
                a |= _flddo;
        }

        diamondedge.util.Variant a(int i)
        {
            return (diamondedge.util.Variant)_fldif.elementAt(i);
        }

        void a(diamondedge.util.Variant variant)
        {
            _fldif.addElement(variant);
        }

        private byte a = 0;
        private static byte _flddo = 1;
        private static byte _fldfor = 2;
        java.util.Vector _fldif = null;


        a(int i)
        {
            a = 0;
            _fldif = null;
            _fldif = new Vector(i);
        }
    }


    AdoCursor(diamondedge.ado.Recordset recordset, diamondedge.ado.Fields fields)
    {
        _fldnew = recordset;
        _fldtry = fields;
        _flddo = new Vector(50);
        _fldfor = 0x7fffffff;
        _fldbyte = 2;
        _fldcase = false;
        _mthif(false);
    }

    diamondedge.util.Variant a(int i, int j, boolean flag)
        throws diamondedge.ado.AdoError
    {
        int k = _flddo.size();
        if(i >= k)
            return (new Variant()).setNull();
        if(flag)
        {
            i++;
            int l = 0;
            for(int i1 = 0; i1 < k; i1++)
            {
                diamondedge.ado.a a1 = _mthtry(i1);
                if(!a1._mthif() && ++l == i)
                    return a1.a(j);
            }

            return new Variant();
        } else
        {
            return _mthtry(i).a(j);
        }
    }

    void _mthif(diamondedge.ado.Fields fields)
    {
        _fldtry = fields;
    }

    void a(java.sql.ResultSet resultset)
    {
        _fldint = resultset;
    }

    void _mthnew(int i)
    {
        a = i;
    }

    void a(boolean flag)
    {
        a("Cursor.setAllRecsFetched " + flag);
        _fldcase = flag;
    }

    boolean _mthdo()
    {
        return _fldcase;
    }

    int _mthif()
    {
        int i = 0;
        synchronized(_flddo)
        {
            i = _flddo.size();
        }
        return i;
    }

    boolean _mthfor()
        throws diamondedge.ado.AdoError
    {
        boolean flag = false;
        try
        {
            if(a > 0 && _mthif() == a || _fldint == null)
                flag = true;
            else
                flag = !_fldint.next();
        }
        catch(java.sql.SQLException sqlexception)
        {
            flag = true;
            throw new AdoError(sqlexception, "error moving to next record.", 0x10d89);
        }
        return flag;
    }

    void _mthfor(diamondedge.ado.Fields fields)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.a a1 = _mthdo(fields);
        a(a1);
    }

    int a(diamondedge.ado.Fields fields)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.Fields fields1 = _fldtry;
        if(fields != null)
            fields1 = fields;
        diamondedge.ado.a a1 = _mthdo(fields1);
        a(a1, fields);
        a(a1);
        return _flddo.size();
    }

    void a(int i, diamondedge.ado.Fields fields)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.a a1 = _mthtry(i);
        a(a1, fields);
    }

    void _mthint(int i)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.a a1 = _mthtry(i);
        a1._mthif(true);
        a1.a(true);
    }

    void _mthdo(int i)
        throws diamondedge.ado.AdoError
    {
        _flddo.removeElementAt(i);
    }

    boolean _mthfor(int i)
    {
        diamondedge.ado.a a1 = _mthtry(i);
        return a1.a();
        java.lang.Exception exception;
        exception;
        return false;
    }

    boolean a(int i)
        throws diamondedge.ado.AdoError
    {
        return _mthtry(i)._mthif();
    }

    void a(int i, boolean flag)
        throws diamondedge.ado.AdoError
    {
        _mthtry(i).a(flag);
    }

    void _mthdo(boolean flag)
        throws diamondedge.ado.AdoError
    {
        int i = _mthif();
        for(int j = 0; j < i; j++)
            _mthtry(j).a(flag);

    }

    private void a(diamondedge.ado.a a1, diamondedge.ado.Fields fields)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.Fields fields1 = _fldtry;
        if(fields != null)
            fields1 = fields;
        int i = fields1.getCount();
        for(int j = 0; j < i; j++)
        {
            diamondedge.ado.Field field = fields1.getField(j);
            int k = field.getSqlType();
            diamondedge.util.Variant variant = a1.a(j);
            if(fields != null)
                diamondedge.ado.AdoUtil.a(k, field, variant);
            else
                diamondedge.ado.AdoUtil.a(_fldint, k, j + 1, variant);
        }

    }

    void a(int i, diamondedge.ado.Field field, int j)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.a a1 = _mthtry(i);
        diamondedge.util.Variant variant = a1.a(j);
        variant.set(field.getValue());
    }

    private void a(diamondedge.ado.a a1)
    {
        synchronized(_flddo)
        {
            _flddo.add(a1);
        }
    }

    private diamondedge.ado.a _mthtry(int i)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.a a1 = null;
        synchronized(_flddo)
        {
            a1 = (diamondedge.ado.a)_flddo.elementAt(i);
        }
        if(a1 == null)
            throw new AdoError(null, "Internal error fetching row. row not found, rowNum: " + i, 0x10d8a);
        else
            return a1;
    }

    void a()
    {
        _flddo.removeAllElements();
        _fldfor = 0x7fffffff;
        _fldcase = false;
    }

    private final void a(java.lang.String s)
    {
        if(_fldnew != null)
            _fldnew._mthnew(s);
    }

    void _mthif(int i, boolean flag)
        throws diamondedge.ado.AdoError
    {
        int j = _fldtry.getCount();
        diamondedge.ado.a a1 = null;
        if(flag && i >= _flddo.size())
        {
            for(int k = 0; k < j; k++)
            {
                diamondedge.ado.Field field = _fldtry.getField(k);
                field.a((new Variant()).setNull());
            }

            return;
        }
        if(flag)
            a1 = _mthtry(i);
        for(int l = 0; l < j; l++)
        {
            diamondedge.ado.Field field1 = _fldtry.getField(l);
            int i1 = field1.getSqlType();
            if(flag)
            {
                diamondedge.util.Variant variant = a1.a(l);
                field1.a(variant);
            } else
            {
                diamondedge.util.Variant variant1 = new Variant();
                variant1.setNull();
                diamondedge.ado.AdoUtil.a(_fldint, i1, l + 1, variant1);
                field1.a(variant1);
            }
        }

    }

    private diamondedge.ado.a _mthdo(diamondedge.ado.Fields fields)
        throws diamondedge.ado.AdoError
    {
        int i = fields.getCount();
        diamondedge.ado.a a1 = new a(i);
        for(int j = 0; j < i; j++)
        {
            diamondedge.ado.Field field = fields.getField(j);
            diamondedge.util.Variant variant = new Variant();
            variant.setNull();
            a1.a(variant);
        }

        return a1;
    }

    synchronized boolean _mthif(int i)
    {
        boolean flag;
        flag = false;
        if(i <= 0)
            break MISSING_BLOCK_LABEL_103;
        if(i <= _mthif())
            break MISSING_BLOCK_LABEL_101;
        _fldfor = i;
_L2:
        if(_fldfor != i || _fldcase)
            break; /* Loop/switch isn't completed */
        if(_fldnew._fldfor)
            break; /* Loop/switch isn't completed */
        try
        {
            a("waiting for record: " + _fldfor);
            wait();
        }
        catch(java.lang.InterruptedException interruptedexception) { }
        if(true) goto _L2; else goto _L1
_L1:
        flag = i <= _mthif();
        break MISSING_BLOCK_LABEL_103;
        flag = true;
        return flag;
    }

    private synchronized void _mthint()
        throws diamondedge.ado.AdoError
    {
        try
        {
            for(int i = 0; i < _fldbyte && !_fldcase && !_fldif; i++)
                if(!_mthfor())
                {
                    a(((diamondedge.ado.Fields) (null)));
                    if(_fldfor != 0x7fffffff && _fldfor == _mthif())
                    {
                        a("notify that we fetched record: " + _fldfor);
                        _fldfor = 0x7fffffff;
                        notify();
                    }
                } else
                {
                    _fldcase = true;
                    a("notify that all recs have been fetched.");
                    _fldfor = 0x7fffffff;
                    notify();
                    _fldint = null;
                    _fldnew.af();
                    _fldnew._mthchar(_mthif());
                }

        }
        catch(java.lang.Exception exception)
        {
            a("caught exception in getData; setting AllRecordsFetched to true.");
            a(exception.toString());
            _fldcase = true;
            notify();
            throw new AdoError(exception, "Caught Exception getting data.", 0x10d8b);
        }
    }

    public void run()
    {
        try
        {
            while(!_fldcase && !_fldif) 
                _mthint();
            _mthif(false);
        }
        catch(diamondedge.ado.AdoError adoerror)
        {
            _fldnew.a(adoerror);
        }
    }

    void _mthif(boolean flag)
    {
        _fldif = flag;
    }

    public java.lang.String toString()
    {
        java.lang.String s = "AdoCursor[";
        s = s + "maxRecords=" + a + ",allRecordsFetched=" + _fldcase + ",recToFetch=" + _fldfor + ",numToFetch=" + _fldbyte + "]";
        return s;
    }

    private diamondedge.ado.Recordset _fldnew = null;
    private diamondedge.ado.Fields _fldtry = null;
    private java.util.Vector _flddo = null;
    private java.sql.ResultSet _fldint = null;
    private int a = 0;
    private boolean _fldcase = false;
    boolean _fldif = false;
    private int _fldfor = 0;
    private int _fldbyte = 0;
}
