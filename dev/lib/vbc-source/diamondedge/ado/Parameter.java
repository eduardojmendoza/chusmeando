// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.ado;

import diamondedge.util.Variant;
import java.io.InputStream;
import java.util.Properties;

// Referenced classes of package diamondedge.ado:
//            AdoError, AdoUtil

public class Parameter
{

    public Parameter()
    {
        a = "";
        _fldfor = new Variant();
        _fldfor.setNull();
        _fldbyte = 1;
        _fldtry = 4;
        _fldcase = 4;
        _fldnew = new Properties();
    }

    public Parameter(java.lang.String s, int i, int j, int k, diamondedge.util.Variant variant)
    {
        a = s;
        _fldfor = variant;
        if(variant == null)
        {
            _fldfor = new Variant();
            _fldfor.setNull();
        }
        _fldbyte = j;
        _fldchar = k;
        _fldtry = i;
        try
        {
            _fldcase = diamondedge.ado.AdoUtil.getSqlType(i);
        }
        catch(java.lang.Exception exception) { }
        _fldnew = new Properties();
    }

    public int getDirection()
    {
        return _fldbyte;
    }

    public void setDirection(int i)
    {
        _fldbyte = i;
    }

    public java.lang.String getName()
    {
        return a;
    }

    public void setName(java.lang.String s)
    {
        a = s;
    }

    public int getPrecision()
    {
        return _fldint;
    }

    public void setPrecision(int i)
        throws diamondedge.ado.AdoError
    {
        if(i < 0)
        {
            throw new AdoError("The precision for the parameter should be >= 0", 40001);
        } else
        {
            _fldint = i;
            return;
        }
    }

    public java.util.Properties getProperties()
    {
        return _fldnew;
    }

    public int getScale()
    {
        return _fldif;
    }

    public void setScale(int i)
        throws diamondedge.ado.AdoError
    {
        if(i < 0)
        {
            throw new AdoError("The scale for the parameter should be >= 0", 40002);
        } else
        {
            _fldif = i;
            return;
        }
    }

    public int getSize()
    {
        return _fldchar;
    }

    public void setSize(int i)
        throws diamondedge.ado.AdoError
    {
        if(i < 0)
        {
            throw new AdoError("The size for the parameter should be >= 0", 40003);
        } else
        {
            _fldchar = i;
            return;
        }
    }

    public int getType()
    {
        return _fldtry;
    }

    public void setType(int i)
        throws diamondedge.ado.AdoError
    {
        _fldtry = i;
        _fldcase = diamondedge.ado.AdoUtil.getSqlType(i);
    }

    public int getSqlType()
    {
        return _fldcase;
    }

    public void setSqlType(int i)
        throws diamondedge.ado.AdoError
    {
        _fldcase = i;
        _fldtry = diamondedge.ado.AdoUtil.getAdoType(i);
    }

    public diamondedge.util.Variant getValue()
    {
        return _fldfor;
    }

    public void setValue(diamondedge.util.Variant variant)
    {
        _fldfor = variant;
        _flddo = true;
    }

    public void appendChunk(diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        if(variant == null)
        {
            _fldfor.setNull();
            _flddo = true;
        } else
        {
            java.io.InputStream inputstream = null;
            if(_flddo)
            {
                inputstream = (java.io.InputStream)_fldfor.toObject();
                if(inputstream == null)
                    throw new AdoError("Error retrieving old data from variant.", 40004);
            } else
            {
                _fldfor.setNull();
                _flddo = true;
            }
            diamondedge.ado.AdoUtil.a(_fldfor, variant, inputstream);
        }
    }

    public java.lang.String toString()
    {
        return "Parameter[name=" + a + ",data=" + _fldfor + ",direction=" + _fldbyte + ",type=" + _fldtry + "]";
    }

    private boolean _flddo = false;
    private diamondedge.util.Variant _fldfor = null;
    private java.lang.String a = null;
    private int _fldint = 0;
    private int _fldif = 0;
    private int _fldchar = 0;
    private int _fldtry = 0;
    private int _fldcase = 0;
    private int _fldbyte = 0;
    private java.util.Properties _fldnew = null;
}
