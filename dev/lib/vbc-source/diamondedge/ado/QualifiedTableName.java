// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.ado;

import java.util.StringTokenizer;

// Referenced classes of package diamondedge.ado:
//            AdoError

public class QualifiedTableName
{

    public QualifiedTableName(java.lang.String s, java.lang.String s1, java.lang.String s2, int i, boolean flag, boolean flag1)
        throws diamondedge.ado.AdoError
    {
        _fldnew = null;
        _fldbyte = null;
        _fldtry = null;
        _fldif = null;
        _fldfor = null;
        _fldint = null;
        _flddo = null;
        _fldcase = 0;
        a = false;
        _fldint = s1;
        _flddo = s2;
        a = flag;
        _fldcase = i;
        if(s.indexOf("[") >= 0)
        {
            char c = '"';
            if(s1 != null && s1.length() > 0)
                c = s1.charAt(0);
            s = s.replace('[', c);
            s = s.replace(']', c);
        }
        java.lang.String s3 = s;
        if(flag1)
            s3 = getStringWithoutAlias(s);
        if(s3.length() == 0)
            throw new AdoError("Missing table name in select statement.", 0x11190);
        int j = s3.indexOf(s2);
        if(j != -1)
        {
            int k = s3.indexOf(s2, j + 1);
            if(k != -1)
            {
                _fldnew = a(s3.substring(0, j));
                if(j + 1 < k)
                    _fldbyte = a(s3.substring(j + 1, k));
                else
                    _fldbyte = null;
                if(s3.length() > k + 1)
                    _fldtry = a(s3.substring(k + 1));
                else
                    _fldtry = "";
            } else
            {
                _fldbyte = a(s3.substring(0, j));
                _fldtry = a(s3.substring(j + 1));
            }
        } else
        {
            _fldtry = a(s3);
        }
        a();
    }

    public java.lang.String getCatalogName()
    {
        return _fldnew;
    }

    public java.lang.String getOwnerName()
    {
        return _fldbyte;
    }

    public java.lang.String getTableName()
    {
        return _fldtry;
    }

    public java.lang.String getExtendedTableName()
    {
        return _fldfor;
    }

    public java.lang.String getStringWithoutAlias(java.lang.String s)
    {
        java.lang.String s1 = _fldint + " \r\n" +
"\t"
;
        s = s.trim();
        java.util.StringTokenizer stringtokenizer = new StringTokenizer(s, s1, true);
        boolean flag = false;
        boolean flag1 = false;
        java.lang.String s3 = "";
        while(stringtokenizer.hasMoreTokens()) 
        {
            java.lang.String s2 = stringtokenizer.nextToken();
            char c = s2.charAt(0);
            if(!flag && (c == ' ' || c == '\n' || c == '\r' || c == '\t'))
                flag1 = true;
            else
            if(flag1)
            {
                _fldif = s2;
            } else
            {
                if(s2.equals(_fldint))
                    flag = !flag;
                s3 = s3 + s2;
            }
        }
        return s3;
    }

    private java.lang.String a(java.lang.String s, int i)
    {
        switch(i)
        {
        case 2: // '\002'
            s = s.toLowerCase();
            break;

        case 1: // '\001'
            s = s.toUpperCase();
            break;
        }
        return s;
    }

    java.lang.String a(java.lang.String s)
    {
        java.lang.String s1 = s;
        if(_fldint != null && _fldint.length() > 0)
        {
            boolean flag = false;
            boolean flag1 = false;
            int i = s.length();
            char c = _fldint.charAt(0);
            char c1 = s.charAt(0);
            if(c1 == c)
                flag = true;
            c1 = s.charAt(i - 1);
            if(c1 == c)
                flag1 = true;
            if(flag && flag1)
                s1 = s.substring(1, i - 1);
            else
                s1 = a(s, _fldcase);
        }
        return s1;
    }

    void a()
    {
        _fldfor = "";
        if(_fldnew != null && _fldnew.length() > 0)
        {
            _fldfor += _fldint;
            _fldfor += _fldnew;
            _fldfor += _fldint;
            _fldfor += _flddo;
        }
        if(_fldbyte != null && _fldbyte.length() > 0)
        {
            _fldfor += _fldint;
            _fldfor += _fldbyte;
            _fldfor += _fldint;
            _fldfor += _flddo;
        } else
        if(_fldnew != null && _fldnew.length() > 0)
            _fldfor += _flddo;
        _fldfor += _fldint;
        _fldfor += _fldtry;
        _fldfor += _fldint;
    }

    java.lang.String _fldnew = null;
    java.lang.String _fldbyte = null;
    java.lang.String _fldtry = null;
    java.lang.String _fldif = null;
    java.lang.String _fldfor = null;
    java.lang.String _fldint = null;
    java.lang.String _flddo = null;
    int _fldcase = 0;
    boolean a = false;
}
