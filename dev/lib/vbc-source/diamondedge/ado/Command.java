// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.ado;

import diamondedge.util.Variant;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.jdbc.driver.OracleConnection;
import oracle.sql.BLOB;
import oracle.sql.CLOB;

// Referenced classes of package diamondedge.ado:
//            Connection, AdoError, Parameters, QualifiedTableName, 
//            Recordset, AdoUtil, Parameter, Field

public class Command
{

    public Command()
    {
        _fldtry = "";
        a = 8;
        _fldnull = null;
        _fldchar = null;
        _fldif = null;
        _fldvoid = 30;
        c = false;
        _fldbyte = false;
        b = false;
        _fldnew = false;
        _fldfor = "";
        _fldcase = 2;
        _fldelse = null;
        _mthlong();
    }

    public diamondedge.ado.Connection getActiveConnection()
    {
        return _fldnull;
    }

    public void setActiveConnection(diamondedge.ado.Connection connection)
        throws diamondedge.ado.AdoError
    {
        _mthvoid();
        _fldnull = connection;
        _fldnew = false;
    }

    public void setActiveConnection(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        _mthvoid();
        try
        {
            _fldnull = new Connection();
            _fldnull.setConnectionString(s);
            _fldnull.open();
            _fldnew = true;
        }
        catch(java.lang.Exception exception)
        {
            throw new AdoError(exception, "Error creating connection.", 10001);
        }
    }

    public java.lang.String getCommandText()
    {
        return _flddo;
    }

    public void setCommandText(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        if(c)
            _mthif("setCommandText");
        _flddo = s;
        _fldlong = null;
    }

    public int getCommandTimeout()
    {
        return _fldvoid;
    }

    public void setCommandTimeout(int i)
        throws diamondedge.ado.AdoError
    {
        if(i < 0)
        {
            throw new AdoError(null, "Command timeout value should be greater than or equal to 0", 20001);
        } else
        {
            _fldvoid = i;
            return;
        }
    }

    public int getCommandType()
    {
        return a;
    }

    public void setCommandType(int i)
        throws diamondedge.ado.AdoError
    {
        if(i == 0)
            return;
        if(c)
            _mthif("setCommandType");
        switch(i)
        {
        case 4: // '\004'
            _fldbyte = true;
            // fall through

        case 1: // '\001'
        case 2: // '\002'
        case 1024: 
        case 2048: 
        case 4096: 
        case 8192: 
        case 16384: 
        case 32768: 
        case 65536: 
        case 131072: 
            a = i;
            break;

        case 8: // '\b'
        default:
            throw new AdoError(null, "unknown/unhandled command type", 20002);
        }
    }

    public diamondedge.ado.Parameters getParameters()
    {
        if(_fldint == null)
            _fldint = new Parameters(this);
        return _fldint;
    }

    public void setParameters(diamondedge.ado.Parameters parameters)
    {
        _fldint = parameters;
        if(parameters != null)
            _fldint.a(this);
    }

    public boolean getPrepared()
    {
        return _fldbyte;
    }

    public void setPrepared(boolean flag)
    {
        _fldbyte = flag;
    }

    public java.util.Properties getProperties()
    {
        return _fldgoto;
    }

    public diamondedge.ado.Recordset execute(diamondedge.util.Variant variant, java.util.Vector vector, int i)
        throws diamondedge.ado.AdoError
    {
        setCommandType(i);
        if(vector != null)
            _fldelse = vector;
        diamondedge.ado.Recordset recordset = execute();
        _fldelse = null;
        if(variant != null && recordset != null)
            variant.set(recordset.getRecordsAffected());
        return recordset;
    }

    public diamondedge.ado.Recordset execute(diamondedge.util.Variant variant, int i)
        throws diamondedge.ado.AdoError
    {
        setCommandType(i);
        diamondedge.ado.Recordset recordset = execute();
        if(variant != null && recordset != null)
            variant.set(recordset.getRecordsAffected());
        return recordset;
    }

    public diamondedge.ado.Recordset execute()
        throws diamondedge.ado.AdoError
    {
        if(_fldnull == null)
            throw new AdoError("You need to set the connection before calling: Execute", 20003);
        if(!_fldnull.isConnected())
        {
            throw new AdoError(null, "You must have an open connection before calling: Execute", 10009);
        } else
        {
            _mthbyte();
            c();
            return _mthtry();
        }
    }

    public void setSortOrder(java.lang.String s)
    {
        _fldfor = s;
    }

    public java.lang.String getSQLStatement()
        throws diamondedge.ado.AdoError
    {
        if(_fldlong == null || _fldlong.length() == 0)
            _mthbyte();
        return _fldlong;
    }

    void a(java.sql.PreparedStatement preparedstatement, int i, diamondedge.util.Variant variant, int j)
        throws diamondedge.ado.AdoError
    {
        i++;
        try
        {
            if(variant.getVarType() == 1)
                preparedstatement.setNull(i, j);
            else
                switch(j)
                {
                case -5: 
                    preparedstatement.setLong(i, variant.toLong());
                    break;

                case -3: 
                case -2: 
                    preparedstatement.setBytes(i, (byte[])variant.toObject());
                    break;

                case -7: 
                    preparedstatement.setBoolean(i, variant.toBoolean());
                    break;

                case 1: // '\001'
                case 12: // '\f'
                    preparedstatement.setString(i, variant.toString());
                    break;

                case 2: // '\002'
                case 3: // '\003'
                    if((_fldnull.getWorkArounds() & 2) == 2)
                        preparedstatement.setDouble(i, variant.toDouble());
                    else
                        preparedstatement.setBigDecimal(i, variant.toDecimal());
                    break;

                case 6: // '\006'
                case 8: // '\b'
                    preparedstatement.setDouble(i, variant.toDouble());
                    break;

                case 4: // '\004'
                    preparedstatement.setInt(i, variant.toInt());
                    break;

                case -4: 
                    java.io.InputStream inputstream = diamondedge.ado.AdoUtil.a(variant);
                    if(inputstream == null)
                        preparedstatement.setNull(i, j);
                    else
                        preparedstatement.setBinaryStream(i, inputstream, inputstream.available());
                    break;

                case -1: 
                    diamondedge.util.Variant variant1 = new Variant(0);
                    java.io.Reader reader = diamondedge.ado.Command.a(variant, variant1);
                    if(reader == null)
                        preparedstatement.setNull(i, j);
                    else
                        preparedstatement.setCharacterStream(i, reader, variant1.toInt());
                    break;

                case 2004: 
                    _mthif(preparedstatement, i, variant, j);
                    break;

                case 2005: 
                    _mthdo(preparedstatement, i, variant, j);
                    break;

                case 0: // '\0'
                    preparedstatement.setObject(i, (java.lang.Object)null);
                    break;

                case 1111: 
                    preparedstatement.setObject(i, variant.toObject());
                    break;

                case 7: // '\007'
                    preparedstatement.setFloat(i, variant.toFloat());
                    break;

                case 5: // '\005'
                    preparedstatement.setShort(i, variant.toShort());
                    break;

                case 9: // '\t'
                case 91: // '['
                    preparedstatement.setDate(i, diamondedge.ado.AdoUtil.getDate(variant));
                    break;

                case 10: // '\n'
                case 92: // '\\'
                    preparedstatement.setTime(i, diamondedge.ado.AdoUtil.getTime(variant));
                    break;

                case 11: // '\013'
                case 93: // ']'
                    preparedstatement.setTimestamp(i, diamondedge.ado.AdoUtil.getTimestamp(variant));
                    break;

                case -6: 
                    preparedstatement.setByte(i, variant.toByte());
                    break;

                default:
                    throw new AdoError(null, "Unknown SQL Type : " + j, 9001);
                }
        }
        catch(diamondedge.ado.AdoError adoerror)
        {
            if(adoerror.getNumber() == 30009)
            {
                java.lang.String s = adoerror.getMessage();
                s = s + " Parameter number is: " + i;
                adoerror.setErrorMsg(s);
            }
            throw adoerror;
        }
        catch(java.sql.SQLException sqlexception)
        {
            throw new AdoError(sqlexception, "Error setting value for parameter: " + i, 20004);
        }
        catch(java.lang.Exception exception)
        {
            throw new AdoError(exception, "Error during data type conversion.", 30002);
        }
    }

    private void _mthdo(java.sql.PreparedStatement preparedstatement, int i, diamondedge.util.Variant variant, int j)
        throws java.io.IOException, java.sql.SQLException
    {
        diamondedge.util.Variant variant1;
        try
        {
            oracle.sql.CLOB clob = oracle.sql.CLOB.createTemporary((oracle.jdbc.driver.OracleConnection)_fldnull.getConnection(), false, 10);
            java.io.Writer writer = clob.setCharacterStream(0L);
            writer.write(variant.toString());
            writer.close();
            preparedstatement.setClob(i, clob);
            return;
        }
        catch(java.lang.Exception exception)
        {
            variant1 = new Variant(0);
        }
        java.io.Reader reader = diamondedge.ado.Command.a(variant, variant1);
        if(reader == null)
            preparedstatement.setNull(i, j);
        else
            preparedstatement.setCharacterStream(i, reader, variant1.toInt());
    }

    private void _mthif(java.sql.PreparedStatement preparedstatement, int i, diamondedge.util.Variant variant, int j)
        throws java.io.IOException, java.sql.SQLException
    {
        try
        {
            oracle.sql.BLOB blob = oracle.sql.BLOB.createTemporary((oracle.jdbc.driver.OracleConnection)_fldnull.getConnection(), false, 10);
            java.io.OutputStream outputstream = blob.setBinaryStream(0L);
            outputstream.write(variant.toBytes());
            outputstream.close();
            preparedstatement.setBlob(i, blob);
            return;
        }
        catch(java.lang.Exception exception)
        {
            java.lang.System.out.println(exception);
        }
        java.io.InputStream inputstream = diamondedge.ado.AdoUtil.a(variant);
        if(inputstream == null)
            preparedstatement.setNull(i, j);
        else
            preparedstatement.setBinaryStream(i, inputstream, inputstream.available());
    }

    private static java.io.Reader a(diamondedge.util.Variant variant, diamondedge.util.Variant variant1)
        throws java.io.IOException
    {
        java.lang.Object obj = null;
        java.lang.Object obj1 = variant.toObject();
        variant1.set(0);
        if(obj1 == null)
            return null;
        if(obj1 instanceof java.io.Reader)
            obj = (java.io.Reader)obj1;
        else
        if(obj1 instanceof java.io.InputStream)
            obj = new InputStreamReader((java.io.InputStream)obj1);
        if(obj == null)
        {
            java.lang.String s = variant.toString();
            variant1.set(s.length());
            obj = new StringReader(s);
        } else
        {
            ((java.io.Reader) (obj)).reset();
            long l;
            for(l = 0L; ((java.io.Reader) (obj)).read() >= 0; l++);
            variant1.set(l);
            ((java.io.Reader) (obj)).reset();
        }
        return ((java.io.Reader) (obj));
    }

    void _mthfor()
        throws diamondedge.ado.AdoError
    {
        int i = 0;
        if(_fldelse != null)
            i = _fldelse.size();
        if(i > 0 && getPrepared() && b)
        {
            if(_fldint.getCount() < i)
                throw new AdoError("You need to set the parameters and their types before calling Execute", 20009);
            java.lang.Object obj = a != 4 ? ((java.lang.Object) (_fldchar)) : ((java.lang.Object) (_fldif));
            for(int j = 0; j < i; j++)
            {
                diamondedge.util.Variant variant = (diamondedge.util.Variant)_fldelse.elementAt(j);
                int k = _fldint.getParameter(j).getSqlType();
                if(k == 1111)
                    throw new AdoError("Parameter type not set.", 40008);
                a(((java.sql.PreparedStatement) (obj)), j, variant, _fldint.getParameter(j).getSqlType());
            }

        }
    }

    void f()
        throws diamondedge.ado.AdoError
    {
        if(_fldelse != null)
            _mthfor();
        else
        if(_fldint != null)
        {
            int i = _fldint.getCount();
            if(i > 0 && getPrepared() && b)
            {
                java.lang.Object obj = a != 4 ? ((java.lang.Object) (_fldchar)) : ((java.lang.Object) (_fldif));
                for(int j = 0; j < i; j++)
                {
                    diamondedge.ado.Parameter parameter = _fldint.getParameter(j);
                    int k = parameter.getDirection();
                    if(k == 1 || k == 3)
                    {
                        diamondedge.util.Variant variant = parameter.getValue();
                        a(((java.sql.PreparedStatement) (obj)), j, variant, parameter.getSqlType());
                    }
                    if(k != 2 && k != 4 && k != 3)
                        continue;
                    if(a != 4)
                        throw new AdoError("Cannot set direction to return value unless the command type is adCmdStoredProc.", 20005);
                    try
                    {
                        _fldif.registerOutParameter(j + 1, parameter.getSqlType());
                    }
                    catch(java.sql.SQLException sqlexception)
                    {
                        throw new AdoError("Error registering outParam for parameter: " + (j + 1), 20006);
                    }
                }

            }
        }
    }

    public int getIndexOf(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        int i = 0;
        if(_fldlong == null || _fldlong.length() == 0)
            _mthbyte();
        if(_fldnull == null || !_fldnull.isConnected())
            throw new AdoError("You must have an open connection before calling: buildSqlStatement", 10009);
        java.lang.String s1 = _fldnull.getIdentifierQuoteString();
        java.lang.String s2 = " \t\n\r" + s1;
        boolean flag = false;
        boolean flag1 = false;
        java.lang.String s3 = "";
        for(java.util.StringTokenizer stringtokenizer = new StringTokenizer(_fldlong, s2, true); stringtokenizer.hasMoreTokens() && !flag;)
        {
            java.lang.String s4 = stringtokenizer.nextToken();
            if(!flag1 && s4.equalsIgnoreCase(s))
            {
                flag = true;
            } else
            {
                if(s4.equals(s1))
                    flag1 = !flag1;
                i += s4.length();
            }
        }

        if(!flag)
            i = -1;
        return i;
    }

    int _mthnew()
        throws diamondedge.ado.AdoError
    {
        return getIndexOf("FROM");
    }

    int _mthif()
        throws diamondedge.ado.AdoError
    {
        return getIndexOf("WHERE");
    }

    int _mthint()
        throws diamondedge.ado.AdoError
    {
        return getIndexOf("ORDER");
    }

    int _mthnull()
        throws diamondedge.ado.AdoError
    {
        return getIndexOf("GROUP");
    }

    java.lang.String _mthelse()
        throws diamondedge.ado.AdoError
    {
        int i = _mthif();
        java.lang.String s = "";
        if(i != -1)
            s = _fldlong.substring(i);
        return s;
    }

    java.lang.String e()
        throws diamondedge.ado.AdoError
    {
        int i = _mthint();
        java.lang.String s = "";
        if(i != -1)
            s = _fldlong.substring(i);
        return s;
    }

    public java.lang.String getTableName()
        throws diamondedge.ado.AdoError
    {
        int i = _mthnew();
        if(i != -1)
        {
            java.lang.String s = _fldlong.substring(i + 4).trim();
            java.util.StringTokenizer stringtokenizer = new StringTokenizer(s);
            if(s.charAt(0) == '[')
                return stringtokenizer.nextToken("[]");
            else
                return stringtokenizer.nextToken();
        } else
        {
            return null;
        }
    }

    public java.lang.String getSqlWithNewFilter(diamondedge.ado.Recordset recordset, java.lang.String s, java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.Field field = recordset.getField(s);
        if(!field.isNumericType())
            s1 = "'" + s1 + "'";
        int i = getIndexOf("where");
        java.lang.String s2 = _fldlong.trim();
        if(i == -1)
        {
            java.lang.String s3 = getTableName();
            int j = getIndexOf("FROM");
            if(j != -1)
                s2 = s2.substring(0, j) + "From " + s3;
        } else
        {
            s2 = s2.substring(0, i);
        }
        if(s2.endsWith(";"))
            s2 = s2.substring(0, s2.length() - 1);
        java.lang.String s4 = _fldnull.getIdentifierQuoteString();
        s2 = s2 + " Where " + s4 + s + s4 + "=" + s1;
        return s2;
    }

    java.lang.String _mthchar()
        throws diamondedge.ado.AdoError
    {
        java.lang.String s = "";
        if(a != 4)
            throw new AdoError("Command type should be adCmdStoredProc for invoking this method.getProcName", 20007);
        if(_flddo.length() == 0)
            throw new AdoError("Command text not set yet.", 20011);
        java.lang.String s1 = _fldnull.getIdentifierQuoteString();
        java.lang.String s2 = " \t\n\r" + s1 + "{}";
        java.util.StringTokenizer stringtokenizer = new StringTokenizer(_flddo, s2, true);
        java.lang.String s3 = stringtokenizer.nextToken();
        boolean flag = false;
        do
            if(s3.equalsIgnoreCase("Call"))
            {
                s = stringtokenizer.nextToken("(}");
                flag = true;
            } else
            {
                s3 = stringtokenizer.nextToken();
            }
        while(stringtokenizer.hasMoreTokens() && !flag);
        if(!flag)
            throw new AdoError("Missing call keyword in stored procedure call: " + _flddo, 20010);
        s = s.trim();
        int i = s.length();
        if(s1.length() > 0)
        {
            char c1 = s1.charAt(0);
            char c2 = s.charAt(0);
            char c3 = s.charAt(i - 1);
            if(c2 == c1 && c3 == c1)
                s = s.substring(1, i - 1);
        }
        return s;
    }

    java.sql.PreparedStatement _mthdo()
        throws diamondedge.ado.AdoError
    {
        c();
        return _fldchar;
    }

    java.sql.CallableStatement _mthgoto()
        throws diamondedge.ado.AdoError
    {
        c();
        return _fldif;
    }

    void b()
        throws diamondedge.ado.AdoError
    {
        if(b)
        {
            if(a == 4)
            {
                _fldnull.a(_fldif);
                _fldif = null;
            } else
            {
                _fldnull.a(_fldchar);
                _fldchar = null;
            }
            b = false;
        }
    }

    int d()
        throws diamondedge.ado.AdoError
    {
        if(_fldlong == null || _fldlong.length() == 0)
            _mthbyte();
        return _fldcase;
    }

    synchronized void a(boolean flag)
    {
        c = flag;
    }

    void _mthvoid()
        throws diamondedge.ado.AdoError
    {
        b();
        a(false);
        if(_fldnew)
        {
            _fldnull.close();
            _fldnew = false;
        }
    }

    private void _mthbyte()
        throws diamondedge.ado.AdoError
    {
        if(_flddo == null || _flddo.length() == 0)
            throw new AdoError("Command text not set yet.", 20011);
        switch(a)
        {
        case 8: // '\b'
            diamondedge.ado.AdoError.a("Command Type: " + a);
            break;

        case 1: // '\001'
        case 4: // '\004'
        case 1024: 
        case 2048: 
        case 4096: 
        case 8192: 
        case 16384: 
        case 32768: 
        case 65536: 
        case 131072: 
            _fldlong = _flddo;
            break;

        case 2: // '\002'
            if(_fldnull == null || !_fldnull.isConnected())
                throw new AdoError("You must have an open connection before calling: buildSqlStatement", 10009);
            _fldlong = "select * from ";
            java.util.Properties properties = _fldnull.getProperties();
            boolean flag = ((java.lang.Boolean)properties.get("IsCatalogAtStart")).booleanValue();
            int i = ((java.lang.Integer)properties.get("IdentifierCase")).intValue();
            java.lang.String s = _fldnull.getCatalogSeparator();
            java.lang.String s1 = _fldnull.getIdentifierQuoteString();
            boolean flag1 = false;
            diamondedge.ado.QualifiedTableName qualifiedtablename = new QualifiedTableName(_flddo, s1, s, i, flag, flag1);
            _fldlong += qualifiedtablename.getExtendedTableName();
            a();
            break;

        default:
            throw new AdoError("unknown/unhandled command type", 20002);
        }
        _mthcase();
    }

    private void _mthcase()
        throws diamondedge.ado.AdoError
    {
        java.util.StringTokenizer stringtokenizer = new StringTokenizer(_fldlong, " \r\n" +
"\t"
);
        java.lang.String s = stringtokenizer.nextToken();
        if(s.equalsIgnoreCase("SELECT"))
        {
            _fldcase = 2;
            int i = _mthif();
            if(i != -1)
                _fldcase |= 4;
            i = _mthnull();
            if(i != -1)
                _fldcase |= 8;
            i = _mthint();
            if(i != -1)
                _fldcase |= 0x10;
        } else
        {
            _fldcase = 1;
            if(s.equalsIgnoreCase("EXEC") || s.equalsIgnoreCase("EXECUTE"))
            {
                a = 4;
                _fldcase = 32;
            } else
            if(s.equalsIgnoreCase("CALL"))
            {
                a = 4;
                _fldcase = 32;
            }
        }
    }

    private void c()
        throws diamondedge.ado.AdoError
    {
        if(getPrepared() && !b)
            if(getCommandType() == 4)
            {
                if(!_fldlong.startsWith("{call"))
                {
                    java.lang.StringBuffer stringbuffer = new StringBuffer();
                    stringbuffer.append("{");
                    boolean flag = false;
                    int i = 0;
                    if(_fldint != null)
                    {
                        int j = _fldint.getCount();
                        for(int l = 0; l < j; l++)
                        {
                            diamondedge.ado.Parameter parameter = _fldint.getParameter(l);
                            if(parameter == null)
                                continue;
                            if(parameter.getDirection() == 4)
                                flag = true;
                            else
                                i++;
                        }

                    }
                    if(flag)
                        stringbuffer.append("? = ");
                    stringbuffer.append("call ");
                    stringbuffer.append(_fldlong);
                    if(i > 0)
                        stringbuffer.append("(");
                    for(int k = 0; k < i; k++)
                    {
                        if(k > 0)
                            stringbuffer.append(",");
                        stringbuffer.append("?");
                    }

                    if(i > 0)
                        stringbuffer.append(")");
                    stringbuffer.append("}");
                    _fldlong = stringbuffer.toString();
                }
                a("preparing call: " + _fldlong);
                _fldif = _fldnull._mthif(_fldlong);
                b = true;
            } else
            if(getCommandType() == 1 || getCommandType() == 2)
            {
                a("preparing statement: " + _fldlong);
                _fldchar = _fldnull.a(_fldlong);
                b = true;
            }
    }

    private diamondedge.ado.Recordset _mthtry()
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.Recordset recordset = new Recordset();
        recordset.setSource(this);
        recordset.open();
        if((_fldcase & 2) != 2 && a != 4 && a != 1024 && a != 2048 && a != 4096 && a != 8192 && a != 0x10000 && a != 0x20000 && a != 32768 && a != 16384)
            recordset.close();
        return recordset;
    }

    private void a()
    {
        if(_fldfor.length() > 0)
            _fldlong += " order by " + _fldfor;
    }

    private void _mthif(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        throw new AdoError("The cursor is already open.  You need to close the cursor before calling: " + s, 0x11171);
    }

    private void a(java.lang.String s)
    {
        _fldnull._mthfor(s);
    }

    private void _mthlong()
    {
        a = 8;
        _fldvoid = 30;
        _fldcase = 2;
    }

    public void finalize()
    {
        try
        {
            _mthvoid();
        }
        catch(diamondedge.ado.AdoError adoerror) { }
    }

    public java.lang.String toString()
    {
        return "Command[sql=" + _fldlong + ",cmdType=" + a + ",con=" + _fldnull + "]";
    }

    private java.lang.String _fldlong = null;
    private java.lang.String _fldtry = null;
    private java.lang.String _flddo = null;
    int a = 0;
    private diamondedge.ado.Connection _fldnull = null;
    private java.sql.PreparedStatement _fldchar = null;
    private java.sql.CallableStatement _fldif = null;
    private int _fldvoid = 0;
    private boolean c = false;
    private boolean _fldbyte = false;
    private boolean b = false;
    private boolean _fldnew = false;
    private java.lang.String _fldfor = null;
    private int _fldcase = 0;
    private diamondedge.ado.Parameters _fldint = null;
    private java.util.Vector _fldelse = null;
    private static java.util.Properties _fldgoto = null;

    static 
    {
        _fldgoto = new Properties();
        _fldgoto.put("ParametersSupported", new Boolean(true));
        _fldgoto.put("PrepareSupported", new Boolean(true));
    }
}
