// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.ado;

import diamondedge.util.Variant;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Vector;

// Referenced classes of package diamondedge.ado:
//            AdoError, Field

public class Fields
{

    public Fields()
    {
        this(5);
    }

    public Fields(int i)
    {
        a = null;
        a = new Vector(i);
    }

    public int getCount()
    {
        return a.size();
    }

    void a(diamondedge.ado.Field field)
    {
        a.addElement(field);
    }

    public diamondedge.ado.Field getField(diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.Field field = null;
        if(variant == null)
            return null;
        if(variant.getVarType() == 15)
            field = getField(variant.toString());
        else
        if(variant.isNumeric())
            field = getField(variant.toInt());
        else
            throw new AdoError("Unknown/Unhandled object type for this operation: ", 0x15f92);
        return field;
    }

    public diamondedge.ado.Field getField(java.lang.Object obj)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.Field field = null;
        if(obj instanceof java.lang.String)
            field = getField((java.lang.String)obj);
        else
        if(obj instanceof diamondedge.util.Variant)
            field = getField((diamondedge.util.Variant)obj);
        else
        if(obj instanceof java.lang.Integer)
        {
            java.lang.Integer integer = (java.lang.Integer)obj;
            int i = integer.intValue();
            field = getField(i);
        } else
        {
            throw new AdoError("Unknown/Unhandled object type for this operation: ", 0x15f92);
        }
        return field;
    }

    public diamondedge.ado.Field getField(int i)
        throws diamondedge.ado.AdoError
    {
        if(i < 0 || i >= a.size())
            throw new AdoError("Field array index out of bounds, array size: " + a.size(), 30010);
        else
            return (diamondedge.ado.Field)a.elementAt(i);
    }

    public diamondedge.ado.Field getField(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        return getField(s, null);
    }

    public diamondedge.ado.Field getField(java.lang.String s, java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        int i = getFieldIndex(s, s1);
        return getField(i);
    }

    public int getFieldIndex(java.lang.String s, java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        int i = a.size();
        for(int j = 0; j < i; j++)
        {
            diamondedge.ado.Field field = (diamondedge.ado.Field)a.elementAt(j);
            if(s.equalsIgnoreCase(field.getName()) && (s1 == null || s1.equalsIgnoreCase(field.getTableName())))
                return j;
        }

        throw new AdoError("Field not found: " + s, 30011);
    }

    public void printFields(java.io.PrintStream printstream)
        throws diamondedge.ado.AdoError, java.io.IOException
    {
        if(printstream != null)
        {
            for(int i = 0; i < getCount(); i++)
            {
                diamondedge.ado.Field field = (diamondedge.ado.Field)a.elementAt(i);
                int j = field.getSqlType();
                switch(j)
                {
                case 4: // '\004'
                    printstream.println(field.getValue().toInt());
                    break;

                case -4: 
                case -1: 
                    java.io.InputStream inputstream = (java.io.InputStream)field.getValue().toObject();
                    byte abyte0[] = new byte[1024];
                    boolean flag = false;
                    do
                    {
                        int k = inputstream.read(abyte0);
                        if(k <= 0)
                            break;
                        if(j == -1)
                            a(printstream, abyte0);
                        else
                            _mthif(printstream, abyte0);
                    } while(true);
                    printstream.println("");
                    break;

                default:
                    printstream.println(field.getValue().toString());
                    break;
                }
            }

        }
    }

    void _mthif(java.io.PrintStream printstream, byte abyte0[])
    {
        int i = abyte0.length;
        for(int j = 0; j < i; j++)
            printstream.print(abyte0[j]);

    }

    void a(java.io.PrintStream printstream, byte abyte0[])
    {
        int i = abyte0.length;
        for(int j = 0; j < i; j++)
            printstream.print((char)abyte0[j]);

    }

    void a(diamondedge.ado.Fields fields)
        throws diamondedge.ado.AdoError
    {
        for(int i = 0; i < getCount(); i++)
        {
            diamondedge.ado.Field field = (diamondedge.ado.Field)a.elementAt(i);
            field._mthif(fields.getField(i));
        }

    }

    java.util.Vector a = null;
}
