// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.ado;

import java.io.PrintStream;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

class TypeInfo
{

    TypeInfo(java.sql.ResultSet resultset)
        throws java.lang.Exception
    {
        _fldtry = null;
        _fldvoid = 0;
        try
        {
            _fldtry = resultset.getString(1);
        }
        catch(java.lang.Exception exception)
        {
            a(exception, "typeName");
        }
        try
        {
            _fldnull = resultset.getShort(2);
        }
        catch(java.lang.Exception exception1)
        {
            a(exception1, "dataType");
        }
        try
        {
            _fldvoid = resultset.getInt(3);
        }
        catch(java.lang.Exception exception2)
        {
            a(exception2, "precision");
        }
        try
        {
            _fldlong = resultset.getString(4);
        }
        catch(java.lang.Exception exception3)
        {
            a(exception3, "literalPrefix");
        }
        try
        {
            a = resultset.getString(5);
        }
        catch(java.lang.Exception exception4)
        {
            a(exception4, "literalSuffix");
        }
        try
        {
            _fldgoto = resultset.getString(6);
        }
        catch(java.lang.Exception exception5)
        {
            a(exception5, "createParams");
        }
        try
        {
            _fldchar = resultset.getShort(7);
        }
        catch(java.lang.Exception exception6)
        {
            a(exception6, "nullable");
        }
        try
        {
            _fldif = resultset.getBoolean(8);
        }
        catch(java.lang.Exception exception7)
        {
            a(exception7, "caseSensitive");
        }
        try
        {
            _flddo = resultset.getShort(9);
        }
        catch(java.lang.Exception exception8)
        {
            a(exception8, "searchable");
        }
        try
        {
            _fldcase = resultset.getBoolean(10);
        }
        catch(java.lang.Exception exception9)
        {
            a(exception9, "unsignedAttribute");
        }
        try
        {
            c = resultset.getBoolean(11);
        }
        catch(java.lang.Exception exception10)
        {
            a(exception10, "fixedPrecScale");
        }
        try
        {
            _fldbyte = resultset.getBoolean(12);
        }
        catch(java.lang.Exception exception11)
        {
            a(exception11, "autoIncrement");
        }
        try
        {
            _fldfor = resultset.getString(13);
        }
        catch(java.lang.Exception exception12)
        {
            a(exception12, "localTypeName");
        }
        try
        {
            _fldnew = resultset.getShort(14);
        }
        catch(java.lang.Exception exception13)
        {
            a(exception13, "minimumScale");
        }
        try
        {
            d = resultset.getShort(15);
        }
        catch(java.lang.Exception exception14)
        {
            a(exception14, "maximumScale");
        }
        if(resultset.getMetaData().getColumnCount() == 18)
        {
            try
            {
                b = resultset.getInt(16);
            }
            catch(java.lang.Exception exception15)
            {
                a(exception15, "sqlDataType");
            }
            try
            {
                _fldelse = resultset.getInt(17);
            }
            catch(java.lang.Exception exception16)
            {
                a(exception16, "sqlDateTimeSub");
            }
            try
            {
                _fldint = resultset.getInt(18);
            }
            catch(java.lang.Exception exception17)
            {
                a(exception17, "numPrecRadix");
            }
        }
    }

    void a(java.lang.Exception exception, java.lang.String s)
        throws java.lang.Exception
    {
        java.lang.System.out.print("TypeInfo(");
        if(_fldtry != null)
            java.lang.System.out.print(_fldtry + ",");
        java.lang.System.out.println(s + ") unavailable: " + exception);
        throw exception;
    }

    java.lang.String _fldtry = null;
    short _fldnull = 0;
    int _fldvoid = 0;
    java.lang.String _fldlong = null;
    java.lang.String a = null;
    java.lang.String _fldgoto = null;
    short _fldchar = 0;
    boolean _fldif = false;
    short _flddo = 0;
    boolean _fldcase = false;
    boolean c = false;
    boolean _fldbyte = false;
    java.lang.String _fldfor = null;
    short _fldnew = 0;
    short d = 0;
    int b = 0;
    int _fldelse = 0;
    int _fldint = 0;
}
