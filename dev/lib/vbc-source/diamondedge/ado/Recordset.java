// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.ado;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
import diamondedge.vb.DataBound;
import diamondedge.vb.DataChange;
import diamondedge.vb.DataChangeEvent;
import diamondedge.vb.DataSource;
import diamondedge.vb.EventListenerList;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.BitSet;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

// Referenced classes of package diamondedge.ado:
//            AdoError, Connection, Command, Fields, 
//            Field, AdoCursor, QualifiedTableName, Parameter, 
//            Parameters, AdoUtil

public class Recordset
    implements java.lang.Runnable, diamondedge.vb.DataSource
{
    private static class a
    {

        int _mthdo()
        {
            return _fldnew;
        }

        diamondedge.util.Variant a(int i1)
        {
            return (diamondedge.util.Variant)a.elementAt(i1);
        }

        void a(int i1, diamondedge.util.Variant variant)
        {
            variant = new Variant(variant);
            if(i1 < a.size())
                a.setElementAt(variant, i1);
            else
                a.addElement(variant);
        }

        boolean a()
        {
            return (_fldif & _flddo) > 0;
        }

        void a(boolean flag)
        {
            _fldif &= ~_flddo;
            if(flag)
                _fldif |= _flddo;
        }

        boolean _mthif()
        {
            return (_fldif & _fldfor) > 0;
        }

        void _mthif(boolean flag)
        {
            _fldif &= ~_fldfor;
            if(flag)
                _fldif |= _fldfor;
        }

        boolean _mthif(int i1)
        {
            return _fldint.get(i1);
        }

        void a(int i1, boolean flag)
        {
            _fldint.set(i1, flag);
        }

        private static byte _fldfor = 1;
        private static byte _flddo = 2;
        private byte _fldif = 0;
        int _fldnew = 0;
        java.util.BitSet _fldint = null;
        java.util.Vector a = null;


        a(int i1, int j1)
        {
            _fldif = 0;
            _fldnew = i1;
            _fldint = new BitSet(j1);
            a = new Vector(j1);
        }
    }

    private static class b
    {

        boolean _flddo = false;
        java.lang.String _fldint = null;
        diamondedge.ado.Field _fldfor = null;
        int _fldif = 0;
        diamondedge.util.Variant a = null;

        private b()
        {
            _flddo = true;
            _fldint = null;
            _fldfor = null;
            _fldif = 2;
            a = new Variant();
        }

    }


    public Recordset()
    {
        _fldint = null;
        s = false;
        k = true;
        h = false;
        o = -1;
        v = null;
        M = null;
        H = new Object();
        V();
        w = new Properties();
        w.put("ReadOnly", new Boolean(true));
        w.put("BookmarkSupport", new Boolean(true));
        w.put("RequerySupport", new Boolean(true));
    }

    public Recordset(diamondedge.ado.Recordset recordset, boolean flag)
    {
        _fldint = null;
        s = false;
        k = true;
        h = false;
        o = -1;
        v = null;
        M = null;
        H = new Object();
        a(recordset, flag);
    }

    public diamondedge.ado.Recordset cloneRecordset()
    {
        Q = new Recordset(this, true);
        return Q;
    }

    public diamondedge.ado.Recordset getLastClone()
    {
        if(Q == null)
            Q = cloneRecordset();
        return Q;
    }

    public int getAbsolutePosition()
        throws diamondedge.ado.AdoError
    {
        aa();
        if(!_fldnew && !_fldfor)
            diamondedge.ado.AdoError._mthdo("getAbsolutePosition");
        g();
        int i1 = -1;
        int j1 = getRecordsFetched();
        if(j1 == 0)
            i1 = -1;
        else
        if(i || Y)
            i1 = _fldlong;
        else
            i1 = f;
        return i1;
    }

    public void setAbsolutePosition(int i1)
        throws diamondedge.ado.AdoError
    {
        aa();
        if(_fldnew)
        {
            g();
            if(C == 0 && i1 < f)
                throw new AdoError("Invalid operation for cursor type: Forward Only Cursor", 0x11172);
            if(i1 < 1 || i1 == -2 || i1 == -3 || i1 == -1)
                throw new AdoError("Invalid row value :" + i1, 0x11173);
            synchronized(H)
            {
                A();
                _mthint(i1);
            }
            _mthfor(10);
        } else
        {
            diamondedge.ado.AdoError._mthif("setAbsolutePosition");
        }
    }

    private void _mthint(int i1)
        throws diamondedge.ado.AdoError
    {
        f = 0;
        i = false;
        Y = false;
        a(i1, true);
    }

    public diamondedge.ado.Connection getActiveConnection()
    {
        return P;
    }

    public void setActiveConnection(diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        if(variant.getVarType() == 15)
        {
            setActiveConnection(variant.toString());
        } else
        {
            java.lang.Object obj = variant.toObject();
            if(obj != null && !(obj instanceof diamondedge.ado.Connection))
                throw new AdoError("Unknown/Unhandled variant type for this operation: setActiveConnection", 0x15f91);
            setActiveConnection((diamondedge.ado.Connection)obj);
        }
    }

    public void setActiveConnection(diamondedge.ado.Connection connection)
        throws diamondedge.ado.AdoError
    {
        if(_fldnew && connection == null)
        {
            int i1 = N.d();
            if((i1 & 2) == 2 || i1 == 32)
                getRecordCount(true);
            O();
            _fldfor = true;
        }
        if(P != null)
            P.a(this);
        if(_fldif)
        {
            n();
            _fldif = false;
        }
        P = connection;
        if(P != null)
        {
            P._mthif(this);
            _fldfor = false;
        }
        if(N != null && N.getActiveConnection() != connection)
            N.setActiveConnection(connection);
    }

    public void setActiveConnection(java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        setActiveConnection(s1, null, null);
    }

    public void setActiveConnection(java.lang.String s1, java.lang.String s2, java.lang.String s3)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.Connection connection = null;
        try
        {
            connection = new diamondedge.ado.Connection();
            connection.setConnectionString(s1, s2, s3);
            connection.open();
            setActiveConnection(connection);
            _fldif = true;
        }
        catch(diamondedge.ado.AdoError adoerror)
        {
            if(connection != null)
            {
                if(connection.isConnected())
                    connection.close();
                connection = null;
            }
            throw adoerror;
        }
    }

    public boolean getBOF()
    {
        return i;
    }

    public boolean isBOF()
    {
        return i;
    }

    public diamondedge.util.Variant getBookmark()
        throws diamondedge.ado.AdoError
    {
        aa();
        if(!_fldnew && !_fldfor)
            diamondedge.ado.AdoError._mthdo("setBookmark");
        g();
        diamondedge.util.Variant variant = new Variant(f);
        return variant;
    }

    public void setBookmark(diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        int i1 = variant.toInt();
        if(i1 < 1)
            return;
        aa();
        g();
        if(C == 0 && i1 < f)
            throw new AdoError("Invalid operation for cursor type: Forward Only Cursor", 0x11172);
        if(_mthnew(i1))
        {
            throw new AdoError("The requested row to go has since been deleted.", 0x11175);
        } else
        {
            setAbsolutePosition(i1);
            return;
        }
    }

    public int getCursorType()
    {
        return C;
    }

    public void setCursorType(int i1)
        throws diamondedge.ado.AdoError
    {
        if(_fldnew)
            throw new AdoError("The cursor is already open.  You need to close the cursor before calling: setCursorType", 0x11171);
        switch(i1)
        {
        case 0: // '\0'
        case 1: // '\001'
        case 3: // '\003'
            C = i1;
            break;

        case 2: // '\002'
        default:
            C = 1;
            break;
        }
    }

    public boolean getEOF()
    {
        return Y;
    }

    public boolean isEOF()
    {
        return Y;
    }

    public diamondedge.ado.Fields getFields()
        throws diamondedge.ado.AdoError
    {
        aa();
        return a;
    }

    public int getFieldCount()
    {
        if(a != null)
            return a.getCount();
        else
            return 0;
    }

    public diamondedge.ado.Field getField(diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        aa();
        return a == null ? null : a.getField(variant);
    }

    public diamondedge.ado.Field getField(int i1)
        throws diamondedge.ado.AdoError
    {
        aa();
        return a == null ? null : a.getField(i1);
    }

    public diamondedge.ado.Field getField(java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        aa();
        return a == null ? null : a.getField(s1, null);
    }

    public void setFieldValue(diamondedge.util.Variant variant, diamondedge.ado.Field field)
        throws diamondedge.ado.AdoError
    {
        getField(variant).setValue(field);
    }

    public void setFieldValue(int i1, diamondedge.ado.Field field)
        throws diamondedge.ado.AdoError
    {
        getField(i1).setValue(field);
    }

    public void setFieldValue(java.lang.String s1, diamondedge.ado.Field field)
        throws diamondedge.ado.AdoError
    {
        getField(s1).setValue(field);
    }

    public int getLockType()
    {
        return m;
    }

    public void setLockType(int i1)
        throws diamondedge.ado.AdoError
    {
        if(_fldnew)
            throw new AdoError("The cursor is already open.  You need to close the cursor before calling: setLockType", 0x11171);
        if(i1 == 2)
            diamondedge.ado.AdoError.a("Lock Pessimistic");
        m = i1;
        if(i1 == 4)
            s = true;
        else
            s = false;
        if(i1 == 1)
            w.put("ReadOnly", new Boolean(true));
        else
            w.put("ReadOnly", new Boolean(false));
    }

    public int getMaxRecords()
    {
        return _fldgoto;
    }

    public void setMaxRecords(int i1)
        throws diamondedge.ado.AdoError
    {
        if(_fldnew)
            throw new AdoError("The cursor is already open.  You need to close the cursor before calling: setMaxRecords", 0x11171);
        if(i1 < 0)
            throw new AdoError("MaxRecords must be greater than or equal to 0.", 0x11195);
        _fldgoto = i1;
        if(_fldnull != null)
            _fldnull._mthnew(i1);
    }

    public java.util.Properties getProperties()
    {
        return w;
    }

    public int getRecordCount()
    {
        if(I != null)
            return o;
        if(C == 0)
            return getRecordCount(false);
        else
            return getRecordCount(true);
    }

    public int getRecordCount(boolean flag)
    {
        if(I != null)
            return o;
        if(!_fldnew)
            return -1;
        if(!N())
            if(flag)
                try
                {
                    _mthcase(0x7fffffff);
                }
                catch(java.lang.Exception exception)
                {
                    return -1;
                }
            else
                return -1;
        return getRecordsFetched();
    }

    public int getRecordsAffected()
    {
        return o;
    }

    public diamondedge.ado.Command getSource()
    {
        return N;
    }

    public void setSource(diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        if(_fldnew)
            throw new AdoError("The cursor is already open.  You need to close the cursor before calling: setSource", 0x11171);
        if(variant.getVarType() == 15)
        {
            setSource(variant.toString());
        } else
        {
            java.lang.Object obj = variant.toObject();
            if(obj != null && !(obj instanceof diamondedge.ado.Command))
                throw new AdoError("Unknown/Unhandled variant type for this operation: ", 0x15f91);
            setSource((diamondedge.ado.Command)obj);
        }
    }

    public void setSource(diamondedge.ado.Command command)
        throws diamondedge.ado.AdoError
    {
        if(_fldnew)
            throw new AdoError("The cursor is already open.  You need to close the cursor before calling: setSource", 0x11171);
        if(command instanceof diamondedge.ado.Command)
        {
            if(L)
            {
                f();
                L = false;
            }
            N = command;
            setActiveConnection(command.getActiveConnection());
        } else
        {
            throw new AdoError("Unknown/Unhandled object type for this operation: ", 0x15f92);
        }
    }

    public void setSource(java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        if(_fldnew)
        {
            throw new AdoError("The cursor is already open.  You need to close the cursor before calling: setSource", 0x11171);
        } else
        {
            N = new Command();
            s1 = s1.trim();
            N.setCommandText(s1);
            N.setCommandType(1);
            N.setActiveConnection(P);
            L = true;
            return;
        }
    }

    public void addNew()
        throws diamondedge.ado.AdoError
    {
        aa();
        if(!_fldnew)
            diamondedge.ado.AdoError._mthdo("AddNew");
        g();
        if(m == 1)
            ag();
        A();
        _mthif(1);
        _mthgoto();
        r();
        _mthfor(1);
    }

    public void addNew(java.util.Vector vector, java.util.Vector vector1)
        throws diamondedge.ado.AdoError
    {
        aa();
        if(!_fldnew)
            diamondedge.ado.AdoError._mthdo("AddNew");
        if(m == 1)
            ag();
        g();
        _mthgoto();
        r();
        a(vector, vector1);
        _mthif(1);
        update();
    }

    private void h()
        throws diamondedge.ado.AdoError
    {
        if(s)
            _mthtry();
        _mthfor(((java.util.Vector) (null)));
    }

    private void A()
        throws diamondedge.ado.AdoError
    {
        if(E == 1 || ae())
            if(P == null || s)
                h();
            else
                update();
    }

    public void update()
        throws diamondedge.ado.AdoError
    {
        aa();
        if(!_fldnew)
            diamondedge.ado.AdoError._mthdo("Update");
        if(m == 1)
            ag();
        g();
        if(E == 8 && !ae())
            throw new AdoError("there's nothing to update.", 0x11178);
        if(P == null || s || _fldfor)
            h();
        else
            _mthelse();
        _mthfor(3);
    }

    private void _mthelse()
        throws diamondedge.ado.AdoError
    {
        synchronized(H)
        {
            if(E == 1)
            {
                Q();
                Y = i = false;
            } else
            {
                _mthif(false);
            }
        }
        _mthif(8);
        _mthgoto();
    }

    public void update(java.util.Vector vector, java.util.Vector vector1)
        throws diamondedge.ado.AdoError
    {
        aa();
        if(!_fldnew)
            diamondedge.ado.AdoError._mthdo("AddNew");
        if(m == 1)
            ag();
        g();
        a(vector, vector1);
        update();
    }

    public void cancelUpdate()
        throws diamondedge.ado.AdoError
    {
        aa();
        if(E == 2 || E == 1 || ae())
        {
            for(int i1 = 0; i1 < a.getCount(); i1++)
            {
                diamondedge.ado.Field field = a.getField(i1);
                if(field._mthdo())
                    field._mthif();
            }

        }
        int j1 = E;
        _mthif(8);
        _mthgoto();
        if(j1 == 1 && f > 0)
            _fldnull._mthdo(f - 1);
        if(j1 == 1 && C != 0 && f > 0)
            synchronized(H)
            {
                _mthdo(true);
            }
    }

    public void delete()
        throws diamondedge.ado.AdoError
    {
        aa();
        if(!_fldnew)
            diamondedge.ado.AdoError._mthdo("Delete");
        if(m == 1)
            ag();
        g();
        _mthif(4);
        if(s)
        {
            _mthtry();
            _mthbyte();
        } else
        {
            _mthif(true);
        }
    }

    public diamondedge.util.Variant[][] getRows(diamondedge.util.Variant variant, java.util.Vector vector)
        throws diamondedge.ado.AdoError
    {
        return getRows(-1, variant, vector);
    }

    public diamondedge.util.Variant[][] getRows(int i1, diamondedge.util.Variant variant, java.util.Vector vector)
        throws diamondedge.ado.AdoError
    {
        aa();
        g();
        return a(i1, variant, vector);
    }

    public void move(int i1, diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        aa();
        g();
        if(variant != null)
            setBookmark(variant);
        _mthdo(i1);
        _mthfor(10);
    }

    public void move(int i1)
        throws diamondedge.ado.AdoError
    {
        aa();
        if(!_fldnew && !_fldfor)
            diamondedge.ado.AdoError._mthdo("Move");
        _mthdo(i1);
        _mthfor(10);
    }

    public void moveFirst()
        throws diamondedge.ado.AdoError
    {
        aa();
        X();
        _mthfor(12);
    }

    public void moveLast()
        throws diamondedge.ado.AdoError
    {
        aa();
        K();
        _mthfor(15);
    }

    public void movePrevious()
        throws diamondedge.ado.AdoError
    {
        aa();
        _mthif();
        _mthfor(14);
    }

    public void moveNext()
        throws diamondedge.ado.AdoError
    {
        aa();
        s();
        _mthfor(13);
    }

    public void open()
        throws diamondedge.ado.AdoError
    {
        aa();
        w();
        _mthfor(7);
    }

    public void open(java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        aa();
        if(s1 != null)
            setSource(s1);
        open();
    }

    public void open(diamondedge.ado.Command command, diamondedge.ado.Connection connection, int i1, int j1)
        throws diamondedge.ado.AdoError
    {
        aa();
        if(command != null)
            setSource(command);
        if(connection != null)
            setActiveConnection(connection);
        setCursorType(i1);
        setLockType(j1);
        open();
    }

    public void open(diamondedge.util.Variant variant, diamondedge.util.Variant variant1, int i1, int j1, int k1)
        throws diamondedge.ado.AdoError
    {
        aa();
        if(variant != null)
            setSource(variant);
        if(variant1 != null)
            setActiveConnection(variant1);
        setCursorType(i1);
        setLockType(j1);
        N.setCommandType(k1);
        open();
    }

    public void open(java.lang.String s1, diamondedge.ado.Connection connection, int i1, int j1, int k1)
        throws diamondedge.ado.AdoError
    {
        aa();
        if(s1 != null)
            setSource(s1);
        if(connection != null)
            setActiveConnection(connection);
        setCursorType(i1);
        setLockType(j1);
        N.setCommandType(k1);
        open();
    }

    public void open(java.lang.String s1, java.lang.String s2, int i1, int j1, int k1)
        throws diamondedge.ado.AdoError
    {
        aa();
        if(s1 != null)
            setSource(s1);
        if(s2 != null)
            setActiveConnection(s2);
        setCursorType(i1);
        setLockType(j1);
        N.setCommandType(k1);
        open();
    }

    private static boolean a(diamondedge.ado.b b1, diamondedge.util.Variant variant)
    {
        if(b1._fldif == 6)
            return variant.toString().toUpperCase().matches(b1.a.toString());
        int i1 = diamondedge.util.Obj.compare(variant, b1.a, false);
        switch(b1._fldif)
        {
        case 0: // '\0'
            return i1 < 0;

        case 1: // '\001'
            return i1 <= 0;

        case 2: // '\002'
            return i1 == 0;

        case 3: // '\003'
            return i1 >= 0;

        case 4: // '\004'
            return i1 > 0;

        case 5: // '\005'
            return i1 != 0;
        }
        return false;
    }

    private boolean a(java.util.Vector vector)
        throws diamondedge.ado.AdoError
    {
        int i1 = vector.size();
        boolean flag = true;
        for(int j1 = 0; j1 < i1; j1++)
        {
            diamondedge.ado.b b1 = (diamondedge.ado.b)vector.get(j1);
            boolean flag1 = diamondedge.ado.Recordset.a(b1, b1._fldfor.getValue());
            if(b1._flddo)
            {
                if(flag && flag1)
                    flag = true;
                else
                    flag = false;
                continue;
            }
            if(flag || flag1)
                flag = true;
            else
                flag = false;
        }

        return flag;
    }

    private java.util.Vector _mthdo(java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        s1 = s1.replace('(', ' ');
        s1 = s1.replace(')', ' ');
        java.util.Vector vector = new Vector();
        java.lang.String s2 = s1.toUpperCase();
        int i1 = 0;
        java.lang.String s3 = s1;
        boolean flag = true;
        do
        {
            int j1 = s2.indexOf(" AND ", i1);
            int k1 = s2.indexOf(" OR ", i1);
            if(j1 < 0 && k1 < 0)
            {
                vector.add(a(s3, flag));
                break;
            }
            int l1 = java.lang.Math.min(j1 >= 0 ? j1 : 0xf423f, k1 >= 0 ? k1 : 0xf423f);
            s3 = s1.substring(i1, l1);
            vector.add(a(s3, flag));
            i1 = l1 + 4;
            s3 = s1.substring(i1);
            flag = l1 == j1;
        } while(true);
        return vector;
    }

    private diamondedge.ado.b a(java.lang.String s1, boolean flag)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.b b1 = new b();
        b1._flddo = flag;
        java.lang.String s2 = "";
        java.lang.String s3 = "";
        java.lang.String s4 = s1.toUpperCase();
        int i1 = s4.indexOf(" LIKE ");
        if(i1 > 0)
        {
            s2 = s1.substring(0, i1).trim();
            s3 = s1.substring(i1 + 6).trim();
            s3 = s3.replace('_', '.');
            s3 = diamondedge.util.Strings.replace(s3, "*", ".*");
            s3 = diamondedge.util.Strings.replace(s3, "%", ".*");
            s3 = s3.toUpperCase();
            b1._fldif = 6;
        } else
        {
            java.util.StringTokenizer stringtokenizer = new StringTokenizer(s1, "<=>", true);
            if(stringtokenizer.hasMoreTokens())
            {
                s2 = stringtokenizer.nextToken();
                s2 = s2.replace('[', ' ');
                s2 = s2.replace(']', ' ').trim();
            }
            if(stringtokenizer.hasMoreTokens())
            {
                java.lang.String s6 = stringtokenizer.nextToken().trim();
                if(stringtokenizer.hasMoreTokens())
                {
                    java.lang.String s7 = stringtokenizer.nextToken().trim();
                    if(s7.equals("=") || s7.equals(">"))
                        s6 = s6 + s7;
                    else
                        s3 = s7;
                }
                if(s6.equals("<"))
                    b1._fldif = 0;
                else
                if(s6.equals("<="))
                    b1._fldif = 1;
                else
                if(s6.equals("="))
                    b1._fldif = 2;
                else
                if(s6.equals(">="))
                    b1._fldif = 3;
                else
                if(s6.equals(">"))
                    b1._fldif = 4;
                else
                if(s6.equals("<>"))
                    b1._fldif = 5;
            }
            if(stringtokenizer.hasMoreTokens())
                s3 = s3 + stringtokenizer.nextToken().trim();
        }
        b1._fldfor = a.getField(s2);
        b1.a.set(s3);
        if(s3.length() > 0)
        {
            if(s3.charAt(0) == '#')
            {
                java.lang.String s5 = s3.substring(1, s3.length() - 1);
                try
                {
                    b1.a.set(diamondedge.util.DateTime.toDate(s5));
                }
                catch(java.lang.Exception exception)
                {
                    throw new AdoError(exception, "Error parsing date: " + s5);
                }
            }
            if(s3.charAt(0) == '\'' && s3.charAt(s3.length() - 1) == '\'')
                b1.a.set(s3.substring(1, s3.length() - 1));
        }
        if(b1.a.isNumeric())
            b1.a.set(b1.a.toInt());
        return b1;
    }

    public boolean findFirst(java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        if(Y && i)
        {
            return false;
        } else
        {
            moveFirst();
            return findNext(s1);
        }
    }

    public boolean findNext(java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        if(Y && i)
            return false;
        A();
        java.util.Vector vector = _mthdo(s1);
        while(!Y) 
        {
            if(a(vector))
            {
                _mthfor(13);
                return true;
            }
            H();
        }
        _mthfor(15);
        return false;
    }

    public boolean findPrevious(java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        if(Y && i)
            return false;
        A();
        java.util.Vector vector = _mthdo(s1);
        while(!i) 
        {
            if(a(vector))
            {
                _mthfor(14);
                return true;
            }
            R();
        }
        _mthfor(12);
        return false;
    }

    public boolean findLast(java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        if(Y && i)
        {
            return false;
        } else
        {
            moveLast();
            return findPrevious(s1);
        }
    }

    public boolean find(java.lang.String s1, int i1, int j1, diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        A();
        if(variant != null)
            setBookmark(variant);
        if(i1 != 0)
            if(i1 > 0 && !Y)
                a(i1, false);
            else
            if(i1 < 0 && !i)
                a(i1, false);
        if(j1 >= 0)
            return findNext(s1);
        else
            return findPrevious(s1);
    }

    public diamondedge.util.Variant getFilter()
    {
        return I;
    }

    public void setFilter(diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        I = null;
        o = -1;
        diamondedge.util.Variant variant1 = variant;
        if(variant1 != null && variant1.isNumeric() && variant1.toInt() == 0)
            variant1 = null;
        if(variant1 == null)
        {
            _fldnull._mthdo(false);
        } else
        {
            getRecordCount(true);
            if(variant1.getVarType() == 15)
            {
                o = 0;
                java.lang.String s1 = variant1.toString();
                if(s1 == null || s1.length() == 0)
                    return;
                java.util.Vector vector = _mthdo(variant1.toString());
                X();
                while(!Y) 
                {
                    if(a(vector))
                    {
                        _fldnull.a(f - 1, false);
                        o++;
                    } else
                    {
                        _fldnull.a(f - 1, true);
                    }
                    H();
                }
            }
            if(variant1.isNumeric() && variant1.toInt() == 1)
            {
                o = 0;
                _fldnull._mthdo(true);
                if(_fldint != null)
                {
                    o = _fldint.size();
                    diamondedge.ado.a a1;
                    for(java.util.Enumeration enumeration = _fldint.elements(); enumeration.hasMoreElements(); _fldnull.a(a1._mthdo() - 1, false))
                        a1 = (diamondedge.ado.a)enumeration.nextElement();

                }
            }
            I = new Variant(variant1);
            X();
        }
        _mthfor(7);
    }

    public void requery()
        throws diamondedge.ado.AdoError
    {
        aa();
        p();
        _mthfor(7);
    }

    public static void setAutoIncrementQuery(java.lang.String s1)
    {
        u = s1;
    }

    public void setAllowUpdateOnFirstTableInJoins(boolean flag)
    {
        F = flag;
    }

    public java.lang.String getClipString()
        throws diamondedge.ado.AdoError
    {
        aa();
        return a(-1, null, null, null, null, null);
    }

    public java.lang.String getClipString(int i1)
        throws diamondedge.ado.AdoError
    {
        aa();
        return a(i1, null, null, null, null, null);
    }

    public java.lang.String getClipString(int i1, diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        aa();
        return a(i1, variant, null, null, null, null);
    }

    public java.lang.String getClipString(int i1, diamondedge.util.Variant variant, java.util.Vector vector)
        throws diamondedge.ado.AdoError
    {
        aa();
        return a(i1, variant, vector, null, null, null);
    }

    public java.lang.String getClipString(int i1, diamondedge.util.Variant variant, java.util.Vector vector, java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        aa();
        return a(i1, variant, vector, s1, null, null);
    }

    public java.lang.String getClipString(int i1, diamondedge.util.Variant variant, java.util.Vector vector, java.lang.String s1, java.lang.String s2)
        throws diamondedge.ado.AdoError
    {
        aa();
        return a(i1, variant, vector, s1, s2, null);
    }

    public java.lang.String getClipString(int i1, diamondedge.util.Variant variant, java.util.Vector vector, java.lang.String s1, java.lang.String s2, java.lang.String s3)
        throws diamondedge.ado.AdoError
    {
        aa();
        return a(i1, variant, vector, s1, s2, s3);
    }

    public diamondedge.ado.Recordset nextRecordset()
        throws diamondedge.ado.AdoError
    {
        aa();
        if(!_fldnew)
            diamondedge.ado.AdoError._mthdo("NextRecordset");
        if(N == null || P == null)
            throw new AdoError("Recordset is no longer associated with a connection/query", 0x11179);
        if(!P.isConnected())
            throw new AdoError("You must have an open connection before calling: ", 10009);
        g();
        if(q)
            throw new AdoError("NextRecordset can be called only once per Recordset.", 0x1117a);
        else
            return P();
    }

    public diamondedge.ado.Recordset getRecordset()
    {
        return this;
    }

    public void addDataBound(diamondedge.vb.DataBound databound)
    {
        if(M == null)
            M = new EventListenerList();
        M.remove(diamondedge.vb.DataBound.class, databound);
        M.add(diamondedge.vb.DataBound.class, databound);
        diamondedge.vb.DataChange.initialize(databound, new DataChangeEvent(this, 7), this);
    }

    public void removeDataBound(diamondedge.vb.DataBound databound)
    {
        if(M != null)
            M.remove(diamondedge.vb.DataBound.class, databound);
    }

    protected void _mthfor(int i1)
    {
        if(M == null)
            return;
        if((Y || i) && (!Y || !i))
            return;
        java.lang.Object aobj[] = M.getListenerList();
        diamondedge.vb.DataChangeEvent datachangeevent = null;
        for(int j1 = aobj.length - 2; j1 >= 0; j1 -= 2)
        {
            if(aobj[j1] != (diamondedge.vb.DataBound.class))
                continue;
            if(datachangeevent == null)
                datachangeevent = new DataChangeEvent(this, i1);
            diamondedge.vb.DataBound databound = (diamondedge.vb.DataBound)aobj[j1 + 1];
            diamondedge.vb.DataChange.initialize(databound, datachangeevent, this);
        }

    }

    public diamondedge.util.Variant getValueAt(int i1, int j1)
    {
        if(i1 == f - 1)
            return getField(j1).getValue();
        return _fldnull.a(i1, j1, I != null);
        java.lang.Exception exception;
        exception;
        return null;
    }

    public void reExecuteQuery()
        throws diamondedge.ado.AdoError
    {
        aa();
        g();
        p();
    }

    public boolean isOpen()
    {
        return _fldnew;
    }

    java.sql.ResultSet m()
    {
        return D;
    }

    void _mthchar(int i1)
    {
        _fldbyte = i1;
    }

    boolean Y()
    {
        boolean flag = true;
        if(!_fldnew || E != 1 && (Y || i || B()))
            flag = false;
        return flag;
    }

    void af()
        throws diamondedge.ado.AdoError
    {
        _mthnew("in closeCursor");
        if(z != null)
            _mthchar();
        if(_fldcase != null)
            _mthdo();
        if(g)
        {
            if(D != null)
            {
                try
                {
                    D.close();
                }
                catch(java.lang.Exception exception) { }
                D = null;
            }
            if(aa != null)
            {
                try
                {
                    P.a(aa);
                }
                catch(java.lang.Exception exception1) { }
                aa = null;
            }
            g = false;
        }
    }

    public void setKeepOpenAfterTransaction(boolean flag)
    {
        T = flag;
    }

    void L()
        throws diamondedge.ado.AdoError
    {
        if(P == null || T)
            return;
        java.lang.Integer integer = (java.lang.Integer)P.getProperties().get("PrepareCommitBehavior");
        if(integer.intValue() == 1)
            O();
    }

    void W()
        throws diamondedge.ado.AdoError
    {
        if(P == null || T)
            return;
        java.lang.Integer integer = (java.lang.Integer)P.getProperties().get("PrepareAbortBehavior");
        if(integer.intValue() == 1)
            O();
    }

    private void i()
        throws diamondedge.ado.AdoError
    {
        if(d != null)
        {
            d.close();
            d = null;
            k = true;
        }
        if(V != null)
        {
            diamondedge.ado.Connection connection = V.getActiveConnection();
            if(connection != null && P != connection)
                connection.close();
            V = null;
        }
    }

    private void I()
        throws diamondedge.ado.AdoError
    {
        if(O != null)
        {
            O.close();
            O = null;
        }
        if(_fldelse != null)
        {
            diamondedge.ado.Connection connection = _fldelse.getActiveConnection();
            if(connection != null && P != connection)
                connection.close();
            _fldelse = null;
        }
    }

    private void _mthdo(int i1)
        throws diamondedge.ado.AdoError
    {
        g();
        synchronized(H)
        {
            if(i1 != 0)
            {
                if(C == 0 && i1 < 0)
                    throw new AdoError("Invalid operation for cursor type: Forward Only Cursor", 0x11172);
                A();
                a(i1, false);
            }
        }
    }

    private void X()
        throws diamondedge.ado.AdoError
    {
        synchronized(H)
        {
            if(!_fldnew && !_fldfor)
                diamondedge.ado.AdoError._mthdo("MoveFirst");
            g();
            A();
            G();
        }
    }

    private void K()
        throws diamondedge.ado.AdoError
    {
        synchronized(H)
        {
            if(!_fldnew && !_fldfor)
                diamondedge.ado.AdoError._mthdo("MoveLast");
            g();
            A();
            _mthcase();
        }
    }

    private void _mthif()
        throws diamondedge.ado.AdoError
    {
        synchronized(H)
        {
            if(!_fldnew && !_fldfor)
                diamondedge.ado.AdoError._mthdo("MovePrevious");
            g();
            if(C == 0)
                throw new AdoError("Invalid operation for cursor type: Forward Only Cursor", 0x11172);
            if(i)
                throw new AdoError("You cannot move back when the cursor is on BOF.", 0x1117c);
            A();
            R();
        }
    }

    private void s()
        throws diamondedge.ado.AdoError
    {
        synchronized(H)
        {
            if(!_fldnew && !_fldfor)
                diamondedge.ado.AdoError._mthdo("MoveNext");
            g();
            if(Y)
                throw new AdoError("You cannot move forward when the cursor is on EOF.", 0x1117d);
            A();
            H();
        }
    }

    private java.lang.String a(int i1, diamondedge.util.Variant variant, java.util.Vector vector, java.lang.String s1, java.lang.String s2, java.lang.String s3)
        throws diamondedge.ado.AdoError
    {
        if(!_fldnew && !_fldfor)
            diamondedge.ado.AdoError._mthdo("getClipString");
        g();
        java.lang.String s4 = "";
        synchronized(H)
        {
            if(variant != null)
                setBookmark(variant);
            if(i || Y)
                throw new AdoError("You have to be positioned on a valid record when you call: getClipString", 0x11174);
            int j1 = a.getCount();
            if(vector != null)
                j1 = vector.size();
            char c1 = '\t';
            char c2 = '\r';
            java.lang.String s5 = s1 == null ? java.lang.String.valueOf(c1) : s1;
            java.lang.String s6 = s2 == null ? java.lang.String.valueOf(c2) : s2;
            java.lang.String s7 = s3 == null ? "" : s3;
            for(int k1 = 0; !Y && (i1 == -1 || k1 < i1); k1++)
            {
                if(k1 > 0)
                    s4 = s4 + s6;
                for(int l1 = 0; l1 < j1; l1++)
                {
                    diamondedge.ado.Field field;
                    if(vector != null)
                        field = a.getField(vector.elementAt(l1));
                    else
                        field = a.getField(l1);
                    if(l1 > 0)
                        s4 = s4 + s5;
                    if(field.isNull())
                        s4 = s4 + s7;
                    else
                        s4 = s4 + field.getValue().toString();
                }

                H();
            }

        }
        return s4;
    }

    private diamondedge.util.Variant[][] a(int i1, diamondedge.util.Variant variant, java.util.Vector vector)
        throws diamondedge.ado.AdoError
    {
        if(!_fldnew && !_fldfor)
            diamondedge.ado.AdoError._mthdo("GetRows");
        g();
        diamondedge.util.Variant avariant[][] = (diamondedge.util.Variant[][])null;
        synchronized(H)
        {
            if(i1 != -1 && i1 < 1)
                throw new AdoError("Invalid value passed for numRecords in GetRows.", 0x1117e);
            if(variant != null)
                setBookmark(variant);
            if(i || Y)
                throw new AdoError("You have to be positioned on a valid record when you call: GetRows", 0x11174);
            int j1 = a.getCount();
            if(vector != null)
                j1 = vector.size();
            int k1 = f + i1;
            if(i1 == -1)
                k1 = 0x7fffffff;
            if(!_mthcase(k1) && getRecordsFetched() == 0)
            {
                Y = i = true;
                _fldlong = -3;
                f = 0;
            }
            k1 = getRecordsFetched() - f;
            if(k1 < i1 || i1 == -1)
                k1++;
            else
            if(k1 > i1)
                k1 = i1;
            avariant = new diamondedge.util.Variant[j1][k1];
            for(int l1 = 0; !Y && l1 < k1; l1++)
            {
                for(int i2 = 0; i2 < j1; i2++)
                {
                    diamondedge.ado.Field field;
                    if(vector != null)
                        field = a.getField(vector.elementAt(i2));
                    else
                        field = a.getField(i2);
                    diamondedge.util.Variant variant1 = field.getValue();
                    if(variant1 == null)
                        variant1 = new Variant((java.lang.Object)null, 1);
                    avariant[i2][l1] = variant1;
                }

                H();
            }

        }
        return avariant;
    }

    private void y()
        throws diamondedge.ado.AdoError
    {
        synchronized(H)
        {
            try
            {
                int i1 = N.getCommandType();
                switch(i1)
                {
                case 1024: 
                    c();
                    break;

                case 2048: 
                    S();
                    break;

                case 4096: 
                    d();
                    break;

                case 8192: 
                    a();
                    break;

                case 65536: 
                    _mthnew();
                    break;

                case 131072: 
                    _mthnull();
                    break;

                case 16384: 
                    q();
                    break;

                case 32768: 
                    _mthfor();
                    break;

                default:
                    if(C == 3 || C == 0)
                    {
                        if(!N.getPrepared())
                            _mthint(N.getSQLStatement());
                        else
                            v();
                    } else
                    {
                        ad();
                    }
                    break;
                }
            }
            catch(java.sql.SQLException sqlexception)
            {
                throw new AdoError(sqlexception, "Error executing query: " + N.getSQLStatement(), 0x11180);
            }
            F();
        }
    }

    private diamondedge.ado.Recordset P()
        throws diamondedge.ado.AdoError
    {
        g();
        q = true;
        java.sql.Statement statement = _mthvoid();
        diamondedge.ado.Recordset recordset = null;
        try
        {
            if(statement.getMoreResults() && statement.getUpdateCount() == -1)
            {
                java.sql.ResultSet resultset = statement.getResultSet();
                if(resultset != null)
                {
                    recordset = new Recordset(this, false);
                    recordset.D = resultset;
                    recordset.F();
                }
            }
        }
        catch(java.sql.SQLException sqlexception)
        {
            throw new AdoError(sqlexception, "Error getting next recordset.", 0);
        }
        close();
        return recordset;
    }

    private void p()
        throws diamondedge.ado.AdoError
    {
        synchronized(H)
        {
            af();
            if(O != null)
                O.close();
            if(d != null)
                d.close();
            if(!_fldnew)
                u();
            try
            {
                y();
            }
            catch(diamondedge.ado.AdoError adoerror)
            {
                N.a(false);
                throw adoerror;
            }
        }
    }

    public void resync()
        throws diamondedge.ado.AdoError
    {
        aa();
        g();
        synchronized(H)
        {
            switch(C)
            {
            case 3: // '\003'
                reExecuteQuery();
                break;

            case 1: // '\001'
                if(!_fldnew)
                {
                    requery();
                } else
                {
                    int i1 = S;
                    S = _fldvoid = 0;
                    _mthcase(i1);
                }
                break;
            }
        }
    }

    private java.sql.PreparedStatement x()
        throws diamondedge.ado.AdoError
    {
        java.lang.Object obj = null;
        if(N.getCommandType() == 4)
            obj = N._mthgoto();
        else
            obj = N._mthdo();
        return ((java.sql.PreparedStatement) (obj));
    }

    private java.sql.Statement _mthvoid()
        throws diamondedge.ado.AdoError
    {
        java.lang.Object obj = null;
        if(N.getPrepared())
            obj = x();
        else
            obj = aa;
        return ((java.sql.Statement) (obj));
    }

    private void z()
        throws diamondedge.ado.AdoError
    {
        int i1 = b();
        a = new Fields(i1);
        try
        {
            java.sql.ResultSetMetaData resultsetmetadata = D.getMetaData();
            for(int j1 = 0; j1 < i1; j1++)
            {
                diamondedge.ado.Field field = new Field(this, resultsetmetadata, j1 + 1);
                a.a(field);
            }

        }
        catch(java.lang.Exception exception)
        {
            throw new AdoError(exception, "error getting recordset metadata.", 0x11182);
        }
    }

    private void a(diamondedge.ado.Fields fields)
        throws diamondedge.ado.AdoError
    {
        int i1 = fields.getCount();
        a = new Fields(i1);
        for(int j1 = 0; j1 < i1; j1++)
        {
            diamondedge.ado.Field field = fields.getField(j1);
            diamondedge.ado.Field field1 = new Field(this, field);
            a.a(field1);
        }

    }

    private void ab()
        throws diamondedge.ado.AdoError
    {
        if(a == null)
        {
            z();
            _fldnull = new AdoCursor(this, a);
            _fldnull._mthnew(_fldgoto);
        } else
        {
            _fldnull.a();
        }
        _fldnull.a(D);
        if(z == null && (C != 0 || _fldtry > 0))
        {
            z = new Thread(_fldnull);
            if(z != null)
                z.setPriority(3);
            if(z != null)
                z.start();
            _mthnew("fetching records.");
        }
    }

    public int getRecordsFetched()
    {
        int i1 = 0;
        if(C == 1 && d != null)
            i1 = d.getRecordsFetched();
        else
        if(_fldnull != null)
            i1 = _fldnull._mthif();
        return i1;
    }

    private boolean _mthcase(int i1)
        throws diamondedge.ado.AdoError
    {
        boolean flag = true;
        int j1 = N.d();
        g();
        if(C == 1)
        {
            if((i1 > _fldvoid || i1 < S) && (i1 < 1 || d == null || !a(i1)))
                flag = false;
        } else
        if(_fldnull != null)
            flag = _fldnull._mthif(i1);
        else
            flag = false;
        return flag;
    }

    private void a(int i1, boolean flag)
        throws diamondedge.ado.AdoError
    {
        int j1 = f + i1;
        if(j1 < 1)
        {
            if(i && i1 < 0)
                throw new AdoError("You cannot move back when the cursor is on BOF.", 0x1117c);
            if(!flag)
            {
                i = true;
                _fldlong = -2;
                f = 0;
                return;
            }
            j1 = 1;
        } else
        if(Y && i1 > 0)
            throw new AdoError("You cannot move forward when the cursor is on EOF.", 0x1117d);
        if(!_mthcase(j1))
        {
            if(!flag)
            {
                Y = true;
                _fldlong = -3;
                f = getRecordsFetched() + 1;
                return;
            }
            if(getRecordsFetched() == 0)
            {
                Y = i = true;
                _fldlong = -3;
                f = 0;
                return;
            }
            t();
        } else
        {
            _mthif(j1, i1 >= 0);
        }
        if(B() && !flag)
        {
            if(i1 < 0)
                R();
            else
                H();
        } else
        {
            i = false;
            Y = false;
            _mthdo(true);
        }
    }

    private void H()
        throws diamondedge.ado.AdoError
    {
        if(C == 0 && _fldtry == 0)
        {
            Y = _fldnull._mthfor();
            f++;
            if(!Y)
            {
                _mthdo(false);
            } else
            {
                a(true);
                _fldlong = -3;
                _fldbyte = getRecordsFetched();
            }
        } else
        if(!_mthcase(f + 1))
        {
            f++;
            Y = true;
            _fldlong = -3;
        } else
        {
            Y = false;
            o();
            if(B())
            {
                H();
            } else
            {
                i = false;
                _mthdo(true);
            }
        }
    }

    private void R()
        throws diamondedge.ado.AdoError
    {
        if(!_mthcase(f - 1))
        {
            f = 0;
            i = true;
            _fldlong = -2;
        } else
        {
            i = false;
            E();
            if(B())
            {
                R();
            } else
            {
                Y = false;
                _mthdo(true);
            }
        }
    }

    private void G()
        throws diamondedge.ado.AdoError
    {
        if(!_mthcase(1))
        {
            Y = true;
            i = true;
            _fldlong = -3;
            f = -1;
            return;
        }
        i = false;
        Y = false;
        k();
        if(B())
        {
            H();
            if(Y)
            {
                i = true;
                _fldlong = -2;
                f = 0;
            }
        } else
        {
            _mthdo(true);
        }
    }

    private void _mthcase()
        throws diamondedge.ado.AdoError
    {
        if(!_mthcase(0x7fffffff) && getRecordsFetched() == 0)
        {
            Y = true;
            i = true;
            _fldlong = -3;
            f = 0;
            return;
        }
        i = false;
        Y = false;
        t();
        if(B())
        {
            R();
            if(i)
            {
                f = 0;
                Y = true;
                _fldlong = -3;
            }
        } else
        {
            _mthdo(true);
        }
    }

    private void o()
        throws diamondedge.ado.AdoError
    {
        f++;
        if(I != null)
        {
            int i1;
            for(i1 = _fldnull._mthif(); f <= i1 && _fldnull.a(f - 1); f++);
            if(f > i1)
                Y = true;
        }
    }

    private void E()
        throws diamondedge.ado.AdoError
    {
        f--;
        if(I != null)
        {
            for(; f > 0 && _fldnull.a(f - 1); f--);
            if(f == 0)
                i = true;
        }
    }

    private void k()
        throws diamondedge.ado.AdoError
    {
        f = 1;
        if(I != null && _fldnull.a(f - 1))
            o();
    }

    private void t()
        throws diamondedge.ado.AdoError
    {
        f = getRecordsFetched();
        if(I != null && _fldnull.a(f - 1))
            E();
    }

    private void _mthif(int i1, boolean flag)
        throws diamondedge.ado.AdoError
    {
        f = i1;
        if(I != null && _fldnull.a(f - 1))
            if(flag)
                o();
            else
                E();
    }

    private boolean B()
    {
        boolean flag = false;
        synchronized(H)
        {
            flag = _mthnew(f);
        }
        return flag;
    }

    private boolean _mthnew(int i1)
    {
        boolean flag = false;
        if(C == 1)
        {
            if(d._fldnull != null)
                flag = d._fldnull._mthfor(i1 - 1);
        } else
        {
            flag = _fldnull._mthfor(i1 - 1);
        }
        return flag;
    }

    private boolean ae()
        throws diamondedge.ado.AdoError
    {
        if(a != null)
        {
            for(int i1 = 0; i1 < a.getCount(); i1++)
            {
                diamondedge.ado.Field field = a.getField(i1);
                if(field._mthdo())
                    return true;
            }

        }
        return false;
    }

    private int b()
        throws diamondedge.ado.AdoError
    {
        if(!_fldnew)
            diamondedge.ado.AdoError._mthdo("GetNumColumns");
        if(p == -1)
            try
            {
                java.sql.ResultSetMetaData resultsetmetadata = D.getMetaData();
                p = resultsetmetadata.getColumnCount();
                _mthnew("numColumns: " + p);
            }
            catch(java.sql.SQLException sqlexception)
            {
                throw new AdoError(sqlexception, "error getting recordset metadata.", 0);
            }
        return p;
    }

    private void _mthdo(boolean flag)
        throws diamondedge.ado.AdoError
    {
        int i1 = f - 1;
        if(C == 1)
        {
            i1 = (i1 - S) + 1;
            if(i1 > _fldtry)
            {
                java.lang.String s1 = "internal error, recToFill is: " + i1;
                s1 = s1 + " cache size is: " + _fldtry;
                throw new AdoError(s1);
            }
        }
        _fldnull._mthif(i1, flag);
    }

    private boolean N()
    {
        boolean flag = false;
        if(C == 1)
            flag = d.N();
        else
        if(_fldnull != null)
            flag = _fldnull._mthdo();
        return flag;
    }

    private void a(boolean flag)
    {
        if(_fldnull != null)
            _fldnull.a(flag);
    }

    public int getRecordStatus()
    {
        return E;
    }

    private void _mthif(int i1)
    {
        E = i1;
        if(C == 1 && d != null)
            d._mthif(i1);
    }

    private void w()
        throws diamondedge.ado.AdoError
    {
        synchronized(H)
        {
            u();
            try
            {
                y();
            }
            catch(diamondedge.ado.AdoError adoerror)
            {
                N.a(false);
                throw adoerror;
            }
        }
    }

    private void u()
        throws diamondedge.ado.AdoError
    {
        if(_fldnew)
            throw new AdoError("The cursor is already open.  You need to close the cursor before calling: exeQuery", 0x11171);
        if(N == null || P == null)
        {
            _mthnew("ownerquery or ownercon is null");
            throw new AdoError("Recordset source not set yet.", 0x11185);
        }
        try
        {
            int i1 = N.getCommandType();
            if(i1 == 8 || i1 == -1)
                if(diamondedge.ado.Recordset.isSqlStatement(N.getCommandText()))
                    N.setCommandType(1);
                else
                    N.setCommandType(2);
            N.a(true);
            if(!P.isConnected())
            {
                _mthnew("ownercon is not connected");
                throw new AdoError("You must have an open connection before calling: Open", 10009);
            }
            if(i1 == 1024)
            {
                if(C == 1)
                    throw new AdoError("You cannot set the cursor type to adOpenKeyset for adCmdGetTables/adCmdGetColumns - you should set it to adOpenStatic", 0x1118f);
            } else
            if(i1 == 2048 || i1 == 4096 || i1 == 8192 || i1 == 0x10000 || i1 == 0x20000 || i1 == 16384 || i1 == 32768)
            {
                if(C == 1)
                    throw new AdoError("You cannot set the cursor type to adOpenKeyset for adCmdGetTables/adCmdGetColumns - you should set it to adOpenStatic", 0x1118f);
            } else
            {
                int j1 = N.d();
                if(C == 1)
                {
                    if((j1 & 8) == 8)
                        throw new AdoError("You cannot set the cursor type to adOpenKeyset on a Group By query - you should set it to adOpenStatic", 0x1118c);
                    if((j1 & 2) != 2)
                        throw new AdoError("You cannot set the cursor type to adOpenKeyset on a non select stmt - you should set it to adOpenStatic type: " + j1 + " cmdtype: " + N.getCommandType() + " sql: " + N.getSQLStatement(), 0x11193);
                }
                if((j1 & 2) == 2)
                    _mthint();
                if(C == 1 && i1 != 4 && (j1 & 8) == 0)
                {
                    if(b == null)
                        throw new AdoError("Missing table name in select statement.", 0x11190);
                    T();
                    M();
                }
            }
        }
        catch(diamondedge.ado.AdoError adoerror)
        {
            N.a(false);
            throw adoerror;
        }
    }

    private void a(java.sql.CallableStatement callablestatement)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.Parameters parameters = getSource().getParameters();
        if(parameters != null)
        {
            int i1 = parameters.getCount();
            for(int j1 = 0; j1 < i1; j1++)
            {
                diamondedge.ado.Parameter parameter = parameters.getParameter(j1);
                int k1 = parameter.getDirection();
                if(k1 == 2 || k1 == 3 || k1 == 4)
                {
                    diamondedge.util.Variant variant = new Variant();
                    variant.setNull();
                    diamondedge.ado.AdoUtil.a(callablestatement, parameter.getSqlType(), j1 + 1, variant);
                    parameter.setValue(variant);
                }
            }

        }
    }

    private void F()
        throws diamondedge.ado.AdoError
    {
        _mthnew("in prepareToFetch()");
        g = true;
        _fldnew = true;
        Y = false;
        int i1 = N.d();
        int j1 = N.getCommandType();
        if(j1 == 4 && D == null)
            Y = i = true;
        else
        if((i1 & 2) == 2 || j1 == 4 || j1 == 1024 || j1 == 4096 || j1 == 8192 || j1 == 0x10000 || j1 == 0x20000 || j1 == 16384 || j1 == 32768 || j1 == 2048)
        {
            if(C != 1)
            {
                if(D == null)
                    throw new AdoError("JDBC driver returned null for ResultSet after executing the query.", 0x11194);
                if(C != 0 && _fldtry == 0)
                    _fldtry = 5;
                ab();
            } else
            {
                Z();
            }
            _mthnew("calling getFirst()");
            try
            {
                G();
            }
            catch(diamondedge.ado.AdoError adoerror)
            {
                Y = i = true;
                _fldlong = -3;
                f = -1;
                diamondedge.util.Err.set(null);
            }
        }
    }

    private void _mthint()
        throws diamondedge.ado.AdoError
    {
        b = new Vector(3);
        java.lang.String s1 = N.getSQLStatement();
        int i1 = N._mthnew();
        java.util.Properties properties = P.getProperties();
        boolean flag = ((java.lang.Boolean)properties.get("IsCatalogAtStart")).booleanValue();
        int j1 = ((java.lang.Integer)properties.get("IdentifierCase")).intValue();
        java.lang.String s2 = P.getCatalogSeparator();
        java.lang.String s3 = P.getIdentifierQuoteString();
        java.lang.String s4 = " \r\n" +
"\t,"
 + s3;
        if(i1 == -1)
        {
            diamondedge.ado.QualifiedTableName qualifiedtablename = new QualifiedTableName("dual", s3, s2, j1, flag, false);
            b.addElement(qualifiedtablename);
            return;
        }
        java.util.StringTokenizer stringtokenizer = new StringTokenizer(s1.substring(i1), s4, true);
        stringtokenizer.nextToken();
        java.lang.String s6 = "";
        boolean flag1 = true;
        boolean flag2 = false;
        diamondedge.ado.QualifiedTableName qualifiedtablename1 = null;
        boolean flag3 = true;
        while(stringtokenizer.hasMoreTokens() && flag1) 
        {
            java.lang.String s5 = stringtokenizer.nextToken();
            if(!flag2 && (s5.equalsIgnoreCase("WHERE") || s5.equalsIgnoreCase("GROUP") || s5.equalsIgnoreCase("HAVING") || s5.equalsIgnoreCase("ORDER")))
                flag1 = false;
            else
            if(!flag2 && s5.equals(","))
            {
                qualifiedtablename1 = new QualifiedTableName(s6, s3, s2, j1, flag, flag3);
                b.addElement(qualifiedtablename1);
                s6 = "";
            } else
            {
                if(s5.equals(s3))
                    flag2 = !flag2;
                s6 = s6 + s5;
            }
        }
        qualifiedtablename1 = new QualifiedTableName(s6, s3, s2, j1, flag, flag3);
        b.addElement(qualifiedtablename1);
        stringtokenizer = null;
    }

    private void _mthlong()
        throws java.sql.SQLException
    {
        b = new Vector(3);
        java.sql.ResultSetMetaData resultsetmetadata = D.getMetaData();
        for(int i1 = 1; i1 <= p; i1++)
        {
            java.lang.String s1 = resultsetmetadata.getTableName(i1);
            if(!b.contains(s1))
            {
                java.lang.String s2 = resultsetmetadata.getCatalogName(i1);
                java.lang.String s3 = resultsetmetadata.getSchemaName(i1);
                b.addElement(s1);
            }
        }

    }

    private void a(diamondedge.ado.Command command)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.Connection connection = P;
        int i1 = P.getMaxStatements();
        if(i1 > 0 && i1 <= P._mthdo())
            try
            {
                connection = new diamondedge.ado.Connection(P, true);
            }
            catch(java.lang.Exception exception)
            {
                throw new AdoError(exception, "Error creating a second connection.", 10020);
            }
        command.setActiveConnection(connection);
    }

    private void T()
        throws diamondedge.ado.AdoError
    {
        java.lang.String s1 = j();
        V = new Command();
        a(V);
        V.setCommandText(s1);
        V.setCommandType(1);
        V.setPrepared(N.getPrepared());
        diamondedge.ado.Parameters parameters = N.getParameters();
        for(int i1 = 0; i1 < parameters.getCount(); i1++)
        {
            diamondedge.ado.Parameter parameter = parameters.getParameter(i1);
            V.getParameters().append(parameter);
        }

        d = new Recordset();
        d.setMaxRecords(_fldgoto);
        d.setSource(V);
        d.setCursorType(3);
        d.setLockType(getLockType());
    }

    private void ad()
        throws diamondedge.ado.AdoError
    {
        d.open();
        k = true;
    }

    private void M()
        throws diamondedge.ado.AdoError
    {
        java.lang.String s1 = new String(N.getSQLStatement());
        int i1 = N._mthif();
        if(i1 == -1)
            i1 = N._mthint();
        if(i1 != -1)
            s1 = s1.substring(0, i1);
        s1 = s1 + " where ";
        s1 = s1 + t;
        _fldelse = new Command();
        a(_fldelse);
        _fldelse.setPrepared(true);
        _fldelse.setCommandText(s1);
        _fldelse.setCommandType(1);
        O = new Recordset();
        O.setSource(_fldelse);
        O.setCursorType(3);
        O.setMaxRecords(_fldgoto);
        O.setLockType(getLockType());
        _fldnull = new AdoCursor(this, a);
        _fldnull._mthnew(_fldgoto);
    }

    private void J()
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.Fields fields = d.a;
        _fldelse.setParameters(null);
        diamondedge.ado.Parameters parameters = _fldelse.getParameters();
        for(int i1 = 0; i1 < fields.getCount(); i1++)
        {
            diamondedge.ado.Field field = fields.getField(i1);
            diamondedge.ado.Parameter parameter = new Parameter();
            parameter.setSqlType(field.getSqlType());
            parameters.append(parameter);
        }

    }

    private void Z()
        throws diamondedge.ado.AdoError
    {
        J();
        if(!a(1))
        {
            _fldnull.a(true);
            try
            {
                if(N.getPrepared())
                    v();
                else
                    _mthint(N.getSQLStatement());
                _fldnew = true;
                z();
                _fldnull._mthif(a);
                D.close();
                D = null;
            }
            catch(java.sql.SQLException sqlexception)
            {
                throw new AdoError(sqlexception, "Error executing query: " + N.getSQLStatement(), 0x11180);
            }
        }
    }

    private synchronized boolean _mthbyte(int i1)
    {
        boolean flag = true;
        for(_flddo = i1; _flddo == i1 && !k;)
            try
            {
                wait();
            }
            catch(java.lang.InterruptedException interruptedexception) { }

        if(_fldvoid < i1)
            flag = false;
        return flag;
    }

    private void _mthchar()
    {
        if(!_fldnull._mthdo())
        {
            _fldnull._mthif(true);
            try
            {
                z.join();
            }
            catch(java.lang.InterruptedException interruptedexception) { }
        }
        z = null;
    }

    private void _mthdo()
    {
        if(!k)
        {
            _mthnew("aborting key thread...");
            h = true;
            try
            {
                _fldcase.join();
            }
            catch(java.lang.InterruptedException interruptedexception) { }
            _mthnew("aborted key thread.");
        }
        _fldcase = null;
    }

    private boolean a(int i1)
        throws diamondedge.ado.AdoError
    {
        _mthnew("in fetchKeyRecords, record to fetch is " + i1);
        boolean flag = true;
        if(i1 < S && i1 != 1)
            K = true;
        else
            K = false;
        if(!k && i1 > _fldvoid && i1 < S + _fldtry)
        {
            flag = _mthbyte(i1);
        } else
        {
            _mthdo();
            int j1 = 0;
            _mthnew("calling setAbsolutePostion on the key cursor");
            d.setAbsolutePosition(i1);
            j1 = d.getAbsolutePosition();
            _mthnew("after getAbsolutePostion rowMovedTo is " + j1);
            if(j1 == i1 || i1 == 0x7fffffff && !d.isEOF() && !d.isBOF())
            {
                if(d.B())
                {
                    _fldnull.a();
                    _fldnull._mthfor(a);
                    _fldnull._mthint(0);
                    S = j1;
                    _fldvoid = j1;
                } else
                {
                    S = j1;
                    _fldvoid = j1 - 1;
                    _mthnew("starting worker thread to fetch the key record...");
                    U();
                    aa();
                }
            } else
            {
                flag = false;
            }
        }
        return flag;
    }

    private synchronized void U()
    {
        l = false;
        _fldcase = new Thread(this);
        int i1 = java.lang.Thread.currentThread().getPriority();
        _fldcase.setPriority(i1);
        k = false;
        _fldcase.start();
        while(!l) 
            try
            {
                _mthnew("waiting for notification from the key cursor");
                wait();
            }
            catch(java.lang.InterruptedException interruptedexception) { }
    }

    public void run()
    {
        _fldnull.a();
        D();
        e();
    }

    private synchronized void D()
    {
        try
        {
            _mthtry(1);
        }
        catch(diamondedge.ado.AdoError adoerror)
        {
            _mthnew("AdoError in Recordset run " + adoerror.toString());
            a(adoerror);
        }
        catch(java.lang.Exception exception)
        {
            _mthnew("Exception while fetching the rest of the columns for the query.\r\n" +
" "
 + exception.toString());
            diamondedge.ado.AdoError adoerror1 = new AdoError();
            adoerror1.setErrorMsg("Caught Exception getting data for keyset cursor.");
            adoerror1.a(exception);
            a(adoerror1);
        }
        finally
        {
            if(!l)
            {
                l = true;
                notify();
            }
        }
    }

    private void e()
    {
        if(!_fldnull._mthdo())
            try
            {
                for(int i1 = 0; !d.isEOF() && !d.isBOF() && i1 < _fldtry - 1 && !h && !_fldnull._mthdo(); i1 += _mthtry(2));
            }
            catch(diamondedge.ado.AdoError adoerror)
            {
                _mthnew("AdoError in Recordset run " + adoerror.toString());
                a(adoerror);
            }
            catch(java.lang.Exception exception)
            {
                _mthnew("Exception in Recordset run " + exception.toString());
                diamondedge.ado.AdoError adoerror1 = new AdoError();
                adoerror1.setErrorMsg("Caught Exception getting data for keyset cursor.");
                adoerror1.a(exception);
                a(adoerror1);
            }
        ac();
    }

    private synchronized void ac()
    {
        k = true;
        h = false;
        notify();
    }

    private synchronized int _mthtry(int i1)
        throws diamondedge.ado.AdoError, java.lang.Exception
    {
        _mthnew("in fetchRecords(int) - fetching the user columns in the background");
        int j1 = 0;
        try
        {
            do
            {
                if(d.isEOF() || d.isBOF() || j1 >= i1 || h)
                    break;
                diamondedge.ado.Fields fields = d.getFields();
                diamondedge.ado.Parameters parameters = _fldelse.getParameters();
                for(int k1 = 0; k1 < fields.getCount(); k1++)
                {
                    diamondedge.ado.Field field = fields.getField(k1);
                    diamondedge.ado.Parameter parameter = parameters.getParameter(k1);
                    parameter.setValue(field.getValue());
                }

                if(!O._fldnew)
                {
                    _mthnew("opening cursor for user columns...");
                    O.open();
                    a(O.a);
                    _fldnull._mthif(a);
                } else
                {
                    _mthnew("reexcuting the query to fetch user columns...");
                    O.reExecuteQuery();
                }
                if(!O.isEOF())
                {
                    _fldnull.a(O.a);
                    d.moveNext();
                } else
                {
                    _mthnew("record no longer found in the db.");
                    _fldnull._mthfor(a);
                    _fldnull._mthint(j1);
                    if(K)
                        d.movePrevious();
                    else
                        d.moveNext();
                }
                j1++;
                _fldvoid++;
                if(_flddo != 0x7fffffff)
                {
                    _flddo = 0x7fffffff;
                    notify();
                }
            } while(true);
            if(d.isEOF())
            {
                _fldnull.a(true);
                if(_flddo != 0x7fffffff)
                {
                    _mthnew("notifying fetched key record: " + _flddo);
                    _flddo = 0x7fffffff;
                    notify();
                }
                d.movePrevious();
            } else
            if(d.isBOF())
                d.moveNext();
        }
        catch(diamondedge.ado.AdoError adoerror)
        {
            _fldnull.a(true);
            if(_flddo != 0x7fffffff)
            {
                _flddo = 0x7fffffff;
                notify();
            }
            throw adoerror;
        }
        catch(java.lang.Exception exception)
        {
            _fldnull.a(true);
            if(_flddo != 0x7fffffff)
            {
                _flddo = 0x7fffffff;
                notify();
            }
            throw exception;
        }
        return j1;
    }

    private java.lang.String _mthint(java.util.Vector vector)
    {
        java.lang.String s1 = " from ";
        java.lang.String s2 = P.getIdentifierQuoteString();
        for(int i1 = 0; i1 < vector.size(); i1++)
        {
            if(i1 > 0)
                s1 = s1 + ", ";
            diamondedge.ado.QualifiedTableName qualifiedtablename = (diamondedge.ado.QualifiedTableName)vector.elementAt(i1);
            s1 = s1 + qualifiedtablename.getExtendedTableName();
            if(qualifiedtablename._fldif != null)
                s1 = s1 + " " + qualifiedtablename._fldif;
        }

        return s1;
    }

    private java.lang.String a(java.util.Hashtable hashtable, int i1)
    {
        boolean flag = false;
        boolean flag1 = false;
        if((i1 & 2) > 0)
        {
            flag = true;
            flag1 = true;
            i1 &= -3;
        }
        if((i1 & 4) > 0)
        {
            flag1 = true;
            i1 &= -5;
        }
        java.lang.String s1 = "";
        java.lang.String s2 = P.getIdentifierQuoteString();
        java.lang.String s3 = P.getCatalogSeparator();
        int j1 = 0;
        do
        {
            if(j1 >= b.size())
                break;
            if(j1 > 0)
                if(i1 == 0)
                    s1 = s1 + ", ";
                else
                if(i1 == 1)
                    s1 = s1 + "and ";
            diamondedge.ado.QualifiedTableName qualifiedtablename = (diamondedge.ado.QualifiedTableName)b.elementAt(j1);
            java.lang.String s4 = flag1 ? null : qualifiedtablename._fldif;
            if(s4 == null)
                s4 = s2 + qualifiedtablename._fldtry + s2;
            java.util.Vector vector = (java.util.Vector)hashtable.get(qualifiedtablename);
            if(vector != null)
            {
                for(int k1 = 0; k1 < vector.size(); k1++)
                {
                    if(k1 > 0)
                        if(i1 == 0)
                            s1 = s1 + ", ";
                        else
                        if(i1 == 1)
                            s1 = s1 + "and ";
                    s1 = s1 + s4 + s3;
                    s1 = s1 + s2 + (java.lang.String)vector.elementAt(k1) + s2;
                    if(i1 == 1)
                        s1 = s1 + " = ? ";
                }

            }
            if(flag)
                break;
            j1++;
        } while(true);
        return s1;
    }

    private java.lang.String j()
        throws diamondedge.ado.AdoError
    {
        boolean flag = false;
        java.util.Hashtable hashtable = _mthif(b);
        java.lang.String s1 = "select ";
        t = " ";
        s1 = s1 + a(hashtable, 0);
        s1 = s1 + _mthint(b);
        t += a(hashtable, 1);
        int i1 = N.d();
        if((i1 & 4) == 4)
            s1 = s1 + " " + N._mthelse();
        else
        if((i1 & 0x10) == 16)
            s1 = s1 + " " + N.e();
        _mthnew(s1);
        _mthnew(t);
        return s1;
    }

    private java.util.Hashtable _mthif(java.util.Vector vector)
        throws diamondedge.ado.AdoError
    {
        if(v == null)
        {
            v = new Hashtable(3);
            for(int i1 = 0; i1 < vector.size(); i1++)
            {
                diamondedge.ado.QualifiedTableName qualifiedtablename = (diamondedge.ado.QualifiedTableName)vector.elementAt(i1);
                v.put(qualifiedtablename, a(qualifiedtablename._fldnew, qualifiedtablename._fldbyte, qualifiedtablename._fldtry));
            }

        }
        return v;
    }

    private java.util.Hashtable _mthdo(java.util.Vector vector)
        throws diamondedge.ado.AdoError
    {
        java.util.Hashtable hashtable = new Hashtable(3);
        for(int i1 = 0; i1 < vector.size(); i1++)
        {
            diamondedge.ado.QualifiedTableName qualifiedtablename = (diamondedge.ado.QualifiedTableName)vector.elementAt(i1);
            hashtable.put(qualifiedtablename, _mthif(qualifiedtablename._fldnew, qualifiedtablename._fldbyte, qualifiedtablename._fldtry));
        }

        return hashtable;
    }

    private java.util.Vector a(java.lang.String s1, java.lang.String s2, java.lang.String s3)
        throws diamondedge.ado.AdoError
    {
        java.util.Vector vector = null;
        try
        {
            vector = _mthdo(s1, s2, s3);
        }
        catch(java.lang.Exception exception) { }
        if(vector == null || vector.size() == 0)
            vector = _mthfor(s1, s2, s3);
        if(vector.size() == 0 && C == 1)
            throw new AdoError("Either the table does not exist or there is no unique index in the table: " + s3, 0x11184);
        else
            return vector;
    }

    private java.util.Vector _mthint(java.lang.String s1, java.lang.String s2, java.lang.String s3)
        throws diamondedge.ado.AdoError
    {
        java.util.Vector vector = new Vector(2);
        try
        {
            java.sql.DatabaseMetaData databasemetadata = P.getMetaData();
            java.sql.DatabaseMetaData _tmp = databasemetadata;
            java.sql.ResultSet resultset = databasemetadata.getBestRowIdentifier(s1, s2, s3, 1, false);
            P.a();
            Object obj = null;
            boolean flag = true;
            do
            {
                if(!resultset.next() || !flag)
                    break;
                int j1 = resultset.getInt("SCOPE");
                java.sql.DatabaseMetaData _tmp1 = databasemetadata;
                if(j1 != 1)
                {
                    java.sql.DatabaseMetaData _tmp2 = databasemetadata;
                    if(j1 != 2)
                    {
                        flag = false;
                        continue;
                    }
                }
                java.lang.String s4 = resultset.getString("COLUMN_NAME");
                int i1 = resultset.getInt("DATA_TYPE");
                vector.addElement(s4);
            } while(true);
            resultset.close();
            if((P.getWorkArounds() & 1) != 1)
                P._mthtry();
        }
        catch(java.sql.SQLException sqlexception)
        {
            throw new AdoError(sqlexception, "error getting recordset metadata.", 0x11182);
        }
        return vector;
    }

    private java.util.Vector _mthif(java.lang.String s1, java.lang.String s2, java.lang.String s3)
        throws diamondedge.ado.AdoError
    {
        java.util.Vector vector = new Vector(2);
        try
        {
            java.sql.DatabaseMetaData databasemetadata = P.getMetaData();
            D = databasemetadata.getColumns(s1, s2, s3, null);
            do
            {
                if(!D.next())
                    break;
                java.lang.String s4 = "";
                int i1 = 0;
                s4 = D.getString(4);
                i1 = D.getInt(5);
                diamondedge.ado.Field field = null;
                try
                {
                    field = a.getField(s4);
                }
                catch(diamondedge.ado.AdoError adoerror)
                {
                    if(adoerror.getNumber() != 30011 || C != 1)
                    {
                        adoerror.setErrorNumber(0x11191);
                        adoerror.setErrorMsg("To update the record, you need to have the unique key columns or all the searchable columns in the select list.");
                        throw adoerror;
                    }
                }
                if(field.isSearchable())
                    vector.addElement(s4);
            } while(true);
        }
        catch(java.sql.SQLException sqlexception)
        {
            throw new AdoError(sqlexception, "getSearchableCols");
        }
        return vector;
    }

    private java.util.Vector _mthdo(java.lang.String s1, java.lang.String s2, java.lang.String s3)
        throws diamondedge.ado.AdoError
    {
        java.util.Vector vector = new Vector(2);
        try
        {
            java.sql.DatabaseMetaData databasemetadata = P.getMetaData();
            java.sql.ResultSet resultset = databasemetadata.getPrimaryKeys(s1, s2, s3);
            P.a();
            Object obj = null;
            java.lang.String s4;
            for(; resultset.next(); vector.addElement(s4))
                s4 = resultset.getString(4);

            resultset.close();
            P._mthtry();
        }
        catch(java.sql.SQLException sqlexception)
        {
            throw new AdoError(sqlexception, "error getting recordset metadata.", 0x11182);
        }
        return vector;
    }

    private java.util.Vector _mthfor(java.lang.String s1, java.lang.String s2, java.lang.String s3)
        throws diamondedge.ado.AdoError
    {
        if(P == null)
            return null;
        java.util.Vector vector = new Vector(2);
        try
        {
            java.sql.DatabaseMetaData databasemetadata = P.getMetaData();
            java.sql.ResultSet resultset = databasemetadata.getIndexInfo(s1, s2, s3, true, true);
            P.a();
            java.lang.String s4 = null;
            Object obj = null;
            do
            {
                if(!resultset.next())
                    break;
                java.lang.String s6 = resultset.getString(6);
                int i1 = resultset.getInt(7);
                java.lang.String s5 = resultset.getString(9);
                java.sql.DatabaseMetaData _tmp = databasemetadata;
                if(i1 != 0)
                {
                    if(s4 != null && !s4.equals(s6))
                        break;
                    s4 = s6;
                    vector.addElement(s5);
                } else
                {
                    s4 = null;
                }
            } while(true);
            resultset.close();
            if((P.getWorkArounds() & 1) != 1)
                P._mthtry();
        }
        catch(java.sql.SQLException sqlexception)
        {
            throw new AdoError(sqlexception, "error getting recordset metadata.", 0x11182);
        }
        return vector;
    }

    private void c()
        throws java.sql.SQLException
    {
        java.util.Properties properties = P.getProperties();
        java.lang.String s1 = (java.lang.String)properties.get("CurrentCatalog");
        java.sql.DatabaseMetaData databasemetadata = P.getConnection().getMetaData();
        java.util.StringTokenizer stringtokenizer = new StringTokenizer(N.getCommandText(), ",");
        int i1 = stringtokenizer.countTokens();
        java.lang.String as[] = new java.lang.String[i1];
        int j1 = 0;
        do
        {
            if(!stringtokenizer.hasMoreTokens())
                break;
            try
            {
                as[j1++] = stringtokenizer.nextToken();
                continue;
            }
            catch(java.lang.ArrayIndexOutOfBoundsException arrayindexoutofboundsexception) { }
            break;
        } while(true);
        try
        {
            D = databasemetadata.getTables(s1, null, null, as);
        }
        catch(java.sql.SQLException sqlexception)
        {
            D = databasemetadata.getTables(null, null, null, as);
        }
    }

    private void a(java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        if(s1 == null || s1.length() == 0)
            throw new AdoError("Command text not set yet.", 20011);
        else
            return;
    }

    private void S()
        throws java.sql.SQLException, diamondedge.ado.AdoError
    {
        java.sql.DatabaseMetaData databasemetadata = P.getConnection().getMetaData();
        java.lang.String s1 = N.getCommandText();
        a(s1);
        boolean flag = false;
        java.util.Properties properties = P.getProperties();
        java.lang.String s2 = P.getCatalogSeparator();
        java.lang.String s3 = P.getIdentifierQuoteString();
        boolean flag1 = ((java.lang.Boolean)properties.get("IsCatalogAtStart")).booleanValue();
        int i1 = ((java.lang.Integer)properties.get("IdentifierCase")).intValue();
        diamondedge.ado.QualifiedTableName qualifiedtablename = new QualifiedTableName(s1, s3, s2, i1, flag1, flag);
        try
        {
            D = databasemetadata.getColumns(qualifiedtablename._fldnew, qualifiedtablename._fldbyte, qualifiedtablename._fldtry, null);
        }
        catch(java.sql.SQLException sqlexception)
        {
            D = databasemetadata.getColumns(null, null, qualifiedtablename._fldtry, null);
        }
    }

    private void d()
        throws java.sql.SQLException, diamondedge.ado.AdoError
    {
        java.sql.DatabaseMetaData databasemetadata = P.getConnection().getMetaData();
        java.lang.String s1 = N.getCommandText();
        a(s1);
        java.util.StringTokenizer stringtokenizer = new StringTokenizer(N.getCommandText(), ", ");
        int i1 = stringtokenizer.countTokens();
        java.lang.String as[] = new java.lang.String[i1];
        int j1 = 0;
        do
        {
            if(!stringtokenizer.hasMoreTokens())
                break;
            try
            {
                as[j1++] = stringtokenizer.nextToken();
                continue;
            }
            catch(java.lang.ArrayIndexOutOfBoundsException arrayindexoutofboundsexception)
            {
                break;
            }
        } while(true);
        java.util.Properties properties = P.getProperties();
        java.lang.String s2 = (java.lang.String)properties.get("CurrentCatalog");
        if(as.length == 1)
            D = databasemetadata.getPrimaryKeys(s2, null, as[0]);
        else
        if(as.length == 2)
            D = databasemetadata.getPrimaryKeys(s2, as[0], as[1]);
        else
        if(as.length == 3)
        {
            java.lang.String s3 = null;
            if(as[0].length() > 0)
                s3 = as[0];
            D = databasemetadata.getPrimaryKeys(s3, as[1], as[2]);
        } else
        {
            throw new AdoError("adCmdGetPrimaryKeys call should have a tablename", 0x11196);
        }
    }

    private void q()
        throws java.sql.SQLException, diamondedge.ado.AdoError
    {
        a(true, true);
    }

    private void _mthfor()
        throws java.sql.SQLException, diamondedge.ado.AdoError
    {
        a(false, true);
    }

    private void a(boolean flag, boolean flag1)
        throws java.sql.SQLException, diamondedge.ado.AdoError
    {
        java.sql.DatabaseMetaData databasemetadata = P.getConnection().getMetaData();
        java.lang.String s1 = N.getCommandText();
        a(s1);
        java.util.StringTokenizer stringtokenizer = new StringTokenizer(N.getCommandText(), ", ");
        int i1 = stringtokenizer.countTokens();
        java.lang.String as[] = new java.lang.String[i1];
        int j1 = 0;
        do
        {
            if(!stringtokenizer.hasMoreTokens())
                break;
            try
            {
                as[j1++] = stringtokenizer.nextToken();
                continue;
            }
            catch(java.lang.ArrayIndexOutOfBoundsException arrayindexoutofboundsexception)
            {
                break;
            }
        } while(true);
        java.util.Properties properties = P.getProperties();
        java.lang.String s2 = (java.lang.String)properties.get("CurrentCatalog");
        if(as.length == 1)
            D = databasemetadata.getIndexInfo(s2, null, as[0], flag, flag1);
        else
        if(as.length == 2)
            D = databasemetadata.getIndexInfo(s2, as[0], as[1], flag, flag1);
        else
        if(as.length == 3)
        {
            java.lang.String s3 = null;
            if(as[0].length() > 0)
                s3 = as[0];
            D = databasemetadata.getIndexInfo(s3, as[1], as[2], flag, flag1);
        } else
        {
            throw new AdoError("adCmdGetIndexes call should have a tablename", 0x11198);
        }
    }

    private void a()
        throws java.sql.SQLException, diamondedge.ado.AdoError
    {
        java.util.Properties properties = P.getProperties();
        java.lang.String s1 = (java.lang.String)properties.get("CurrentCatalog");
        java.sql.DatabaseMetaData databasemetadata = P.getConnection().getMetaData();
        java.lang.String s2 = N.getCommandText();
        a(s2);
        java.util.StringTokenizer stringtokenizer = new StringTokenizer(s2, ", ");
        int i1 = stringtokenizer.countTokens();
        java.lang.String as[] = new java.lang.String[i1];
        int j1 = 0;
        do
        {
            if(!stringtokenizer.hasMoreTokens())
                break;
            try
            {
                as[j1++] = stringtokenizer.nextToken();
                continue;
            }
            catch(java.lang.ArrayIndexOutOfBoundsException arrayindexoutofboundsexception) { }
            break;
        } while(true);
        if(as.length == 0)
            D = databasemetadata.getCrossReference(s1, null, null, s1, null, null);
        else
        if(as.length == 1)
            D = databasemetadata.getImportedKeys(s1, null, as[0]);
        else
        if(as.length == 2)
            D = databasemetadata.getImportedKeys(s1, as[0], as[1]);
        else
        if(as.length == 3)
            D = databasemetadata.getImportedKeys(as[0], as[1], as[2]);
        else
            throw new AdoError("adCmdGetForeignKeys call should have a tablename", 0x11197);
    }

    private void _mthnew()
        throws java.sql.SQLException, diamondedge.ado.AdoError
    {
        java.util.Properties properties = P.getProperties();
        java.lang.String s1 = (java.lang.String)properties.get("CurrentCatalog");
        java.sql.DatabaseMetaData databasemetadata = P.getConnection().getMetaData();
        java.lang.String s2 = N.getCommandText();
        a(s2);
        java.util.StringTokenizer stringtokenizer = new StringTokenizer(s2, ", ");
        int i1 = stringtokenizer.countTokens();
        java.lang.String as[] = new java.lang.String[i1];
        int j1 = 0;
        do
        {
            if(!stringtokenizer.hasMoreTokens())
                break;
            try
            {
                as[j1++] = stringtokenizer.nextToken();
                continue;
            }
            catch(java.lang.ArrayIndexOutOfBoundsException arrayindexoutofboundsexception) { }
            break;
        } while(true);
        if(as.length == 1)
            D = databasemetadata.getExportedKeys(s1, null, as[0]);
        else
        if(as.length == 2)
            D = databasemetadata.getExportedKeys(s1, as[0], as[1]);
        else
        if(as.length == 3)
            D = databasemetadata.getExportedKeys(as[0], as[1], as[2]);
        else
            throw new AdoError("adCmdGetForeignKeys call should have a tablename", 0x11197);
    }

    private void _mthnull()
        throws java.sql.SQLException, diamondedge.ado.AdoError
    {
        java.util.Properties properties = P.getProperties();
        java.lang.String s1 = (java.lang.String)properties.get("CurrentCatalog");
        java.sql.DatabaseMetaData databasemetadata = P.getConnection().getMetaData();
        java.lang.String s2 = N.getCommandText();
        a(s2);
        java.util.StringTokenizer stringtokenizer = new StringTokenizer(s2, ", ");
        int i1 = stringtokenizer.countTokens();
        java.lang.String as[] = new java.lang.String[i1];
        int j1 = 0;
        do
        {
            if(!stringtokenizer.hasMoreTokens())
                break;
            try
            {
                as[j1++] = stringtokenizer.nextToken();
                continue;
            }
            catch(java.lang.ArrayIndexOutOfBoundsException arrayindexoutofboundsexception) { }
            break;
        } while(true);
        if(as.length == 0)
            D = databasemetadata.getCrossReference(s1, null, null, s1, null, null);
        else
        if(as.length == 2)
            D = databasemetadata.getCrossReference(s1, null, as[0], s1, null, as[1]);
        else
        if(as.length == 4)
            D = databasemetadata.getCrossReference(s1, as[0], as[1], s1, as[2], as[3]);
        else
            throw new AdoError("adCmdGetForeignKeys call should have a tablename", 0x11197);
    }

    private void v()
        throws diamondedge.ado.AdoError, java.sql.SQLException
    {
        _mthnew("preparing sql statement");
        java.sql.PreparedStatement preparedstatement = x();
        N.f();
        try
        {
            preparedstatement.setMaxRows(_fldgoto);
        }
        catch(java.lang.Exception exception)
        {
            if(_fldgoto > 0)
                exception.printStackTrace();
        }
        D = null;
        _mthnew("executing prepared statement.");
        int i1 = N.d();
        int j1 = N.getCommandType();
        if((i1 & 2) == 2 || j1 == 4)
        {
            boolean flag = preparedstatement.execute();
            if(j1 == 4 && (preparedstatement instanceof java.sql.CallableStatement))
                a((java.sql.CallableStatement)preparedstatement);
            if(flag)
                D = preparedstatement.getResultSet();
            o = -1;
        } else
        {
            o = preparedstatement.executeUpdate();
            D = null;
        }
    }

    private void _mthint(java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        try
        {
            if(aa == null)
            {
                if(P == null)
                    throw new AdoError("You must have a valid connection before accessing the database.", 10009);
                aa = P._mthfor();
            }
            try
            {
                aa.setMaxRows(_fldgoto);
            }
            catch(java.lang.Exception exception)
            {
                if(_fldgoto > 0)
                    exception.printStackTrace();
            }
            _mthnew("executing sql statement: " + s1);
            int i1 = N.d();
            int j1 = N.getCommandType();
            if((i1 & 2) == 2 || j1 == 4)
            {
                D = aa.executeQuery(s1);
                if(j1 == 4 && (aa instanceof java.sql.CallableStatement))
                    a((java.sql.CallableStatement)aa);
                o = -1;
            } else
            {
                D = null;
                o = aa.executeUpdate(s1);
                P.a(aa);
                aa = null;
            }
        }
        catch(java.sql.SQLException sqlexception)
        {
            P.a(aa);
            aa = null;
            throw new AdoError(sqlexception, "Error executing query: " + s1, 0x11180);
        }
    }

    private void f()
        throws diamondedge.ado.AdoError
    {
        N._mthvoid();
    }

    private void n()
        throws diamondedge.ado.AdoError
    {
        P.a(this);
        P.close();
    }

    void O()
        throws diamondedge.ado.AdoError
    {
        if(z != null)
            _mthchar();
        if(_fldcase != null)
            _mthdo();
        if(N != null)
            N.a(false);
        af();
        i();
        I();
        if(!q)
        {
            if(N != null && N.getPrepared())
                N.b();
            if(L)
                f();
            if(_fldif)
                n();
        }
    }

    public void close()
        throws diamondedge.ado.AdoError
    {
        aa();
        O();
        i = true;
        Y = true;
        _fldlong = -3;
        _fldnew = false;
        f = 0;
        p = -1;
        _fldbyte = -1;
        _mthif(8);
        _fldnull = null;
        a = null;
        D = null;
        a(false);
        b = null;
        v = null;
        W = null;
        S = 0;
        _fldvoid = 0;
        l = false;
    }

    private void V()
    {
        _fldbyte = -1;
        _fldnew = false;
        g = false;
        _fldfor = false;
        m = 1;
        C = 0;
        i = true;
        Y = true;
        _fldlong = -3;
        _fldgoto = 0;
        _fldtry = 20;
        P = null;
        N = null;
        p = -1;
        aa = null;
        D = null;
        b = null;
        a = null;
        V = null;
        d = null;
        W = null;
        t = null;
        S = 0;
        _fldvoid = 0;
        _flddo = 0x7fffffff;
        f = 0;
        l = false;
        _fldelse = null;
        O = null;
        v = null;
        z = null;
        _mthif(8);
        I = null;
        _fldif = false;
        L = false;
        _fldnull = null;
        q = false;
        n = false;
        e = null;
    }

    private void a(diamondedge.ado.Recordset recordset, boolean flag)
    {
        _fldbyte = recordset._fldbyte;
        m = recordset.m;
        C = recordset.C;
        _fldgoto = recordset._fldgoto;
        _fldtry = recordset._fldtry;
        P = recordset.P;
        N = recordset.N;
        w = new Properties();
        diamondedge.ado.AdoUtil.a(recordset.w, w);
        _fldif = recordset._fldif;
        L = recordset.L;
        S = 0;
        _fldvoid = 0;
        _flddo = 0x7fffffff;
        f = 0;
        l = false;
        z = null;
        n = false;
        e = null;
        _mthif(8);
        if(flag)
        {
            _fldnew = false;
            g = recordset.g;
            i = recordset.i;
            Y = recordset.Y;
            _fldlong = recordset._fldlong;
            p = recordset.p;
            aa = recordset.aa;
            D = recordset.D;
            b = new Vector();
            for(int i1 = 0; i1 < recordset.b.size(); i1++)
                b.addElement(recordset.b.elementAt(i1));

            a = null;
            V = recordset.V;
            d = recordset.d;
            W = recordset.W;
            t = recordset.t;
            _fldelse = recordset._fldelse;
            O = recordset.O;
            _fldnull = recordset._fldnull;
            q = recordset.q;
            I = recordset.I;
            v = recordset.v == null ? null : (java.util.Hashtable)recordset.v.clone();
            try
            {
                w();
            }
            catch(java.lang.Exception exception) { }
        } else
        {
            i = true;
            Y = true;
            _fldlong = -3;
            p = -1;
            aa = null;
            D = null;
            b = null;
            a = null;
            V = null;
            d = null;
            W = null;
            t = null;
            _fldelse = null;
            O = null;
            _fldnull = null;
            q = false;
            I = null;
        }
    }

    public void finalize()
        throws diamondedge.ado.AdoError
    {
        O();
    }

    private void _mthgoto()
        throws diamondedge.ado.AdoError
    {
        for(int i1 = 0; i1 < a.getCount(); i1++)
        {
            diamondedge.ado.Field field = a.getField(i1);
            field.a();
            field.a((new Variant()).setNull());
        }

    }

    private void a(java.util.Vector vector, java.util.Vector vector1)
        throws diamondedge.ado.AdoError
    {
        for(int i1 = 0; i1 < vector.size(); i1++)
        {
            diamondedge.ado.Field field = a.getField(vector.elementAt(i1));
            java.lang.Object obj = vector1.elementAt(i1);
            if(!(obj instanceof diamondedge.util.Variant))
                throw new AdoError("Unknown/Unhandled object type for this operation: ", 0x15f92);
            field.setValue((diamondedge.util.Variant)obj);
        }

    }

    void Q()
        throws diamondedge.ado.AdoError
    {
        if(b.size() != 1 && !F)
            throw new AdoError("Currently you can do an insert only on queries with one table", 0x11187);
        diamondedge.ado.QualifiedTableName qualifiedtablename = (diamondedge.ado.QualifiedTableName)b.elementAt(0);
        java.lang.String s1 = qualifiedtablename.getExtendedTableName();
        java.lang.String s2 = _mthif(s1, a);
        java.sql.PreparedStatement preparedstatement = null;
        _mthnew(s2);
        try
        {
            preparedstatement = P.a(s2);
            int i1 = a.getCount();
            int j1 = 0;
            int k1 = -1;
            for(int l1 = 0; l1 < i1; l1++)
            {
                diamondedge.ado.Field field = a.getField(l1);
                if(!field.isAutoInc() && field._mthdo())
                {
                    N.a(preparedstatement, j1++, field.getValue(), field.getSqlType());
                    _mthnew("  " + field.getName() + "=" + field.getValue());
                }
                if(field.isAutoInc())
                    k1 = l1;
            }

            preparedstatement.executeUpdate();
            P.a(preparedstatement);
            preparedstatement = null;
            if(k1 >= 0 && u != null)
            {
                java.sql.Statement statement = P._mthfor();
                java.sql.ResultSet resultset = statement.executeQuery(u);
                _mthnew(u);
                _mthnew("  autoInc Field: " + k1);
                if(resultset.next())
                {
                    int i2 = resultset.getInt(1);
                    diamondedge.ado.Field field1 = a.getField(k1);
                    field1.a(new Variant(i2));
                    _fldnull.a(f - 1, field1, k1);
                    _mthnew("  new ID: " + field1.getName() + "=" + field1.getValue());
                }
                statement.close();
            }
        }
        catch(java.sql.SQLException sqlexception)
        {
            if(preparedstatement != null)
                P.a(preparedstatement);
            throw new AdoError(sqlexception, "Error executing query: " + s2, 0x11180);
        }
    }

    java.lang.String _mthif(java.lang.String s1, diamondedge.ado.Fields fields)
        throws diamondedge.ado.AdoError
    {
        java.lang.String s2 = P.getIdentifierQuoteString();
        java.lang.String s3 = "insert into ";
        s3 = s3 + s1;
        s3 = s3 + " ( ";
        java.lang.String s4 = " ) values ( ";
        for(int i1 = 0; i1 < fields.getCount(); i1++)
        {
            diamondedge.ado.Field field = fields.getField(i1);
            if(!field.isAutoInc() && field._mthdo())
            {
                s3 = s3 + s2;
                s3 = s3 + field.getName();
                s3 = s3 + s2;
                s4 = s4 + "?";
                s3 = s3 + ", ";
                s4 = s4 + ", ";
            }
        }

        int j1 = s3.lastIndexOf(',');
        if(j1 == -1)
        {
            throw new AdoError("Either none of the fields were updated or the updated fields are not modifiable.", 0x1118e);
        } else
        {
            s3 = s3.substring(0, j1);
            j1 = s4.lastIndexOf(',');
            s4 = s4.substring(0, j1);
            s3 = s3 + s4;
            s3 = s3 + " ) ";
            return s3;
        }
    }

    synchronized void _mthif(boolean flag)
        throws diamondedge.ado.AdoError
    {
        int i1 = 5;
        if(b.size() > 1)
            if(F)
                i1 |= 2;
            else
                throw new AdoError("Currently you can do an update only on queries with one table", 0x11188);
        java.util.Hashtable hashtable = _mthif(b);
        diamondedge.ado.QualifiedTableName qualifiedtablename = (diamondedge.ado.QualifiedTableName)b.elementAt(0);
        java.util.Vector vector = (java.util.Vector)hashtable.get(qualifiedtablename);
        if(vector == null || vector.size() == 0)
        {
            hashtable = _mthdo(b);
            vector = (java.util.Vector)hashtable.get(qualifiedtablename);
            if(vector == null || vector.size() == 0)
                throw new AdoError("To update the record, you need to have the unique key columns or all the searchable columns in the select list.", 0x11191);
        }
        java.lang.String s1 = a(hashtable, i1);
        java.lang.String s2 = qualifiedtablename.getExtendedTableName();
        java.sql.Statement statement = null;
        java.sql.PreparedStatement preparedstatement = null;
        java.lang.String s3 = null;
        java.lang.String s4 = "";
        if(flag)
            s4 = _mthfor(s2);
        else
            s4 = a(s2, a);
        s4 = s4 + " where ";
        try
        {
            if(statement == null)
            {
                s4 = s4 + s1;
            } else
            {
                s4 = s4 + "current of ";
                s4 = s4 + s3;
            }
            preparedstatement = P.a(s4);
            _mthnew(s4);
            int j1 = a.getCount();
            int k1 = 0;
            if(!flag)
            {
                for(int l1 = 0; l1 < j1; l1++)
                {
                    diamondedge.ado.Field field = a.getField(l1);
                    if(field._mthdo())
                    {
                        _mthnew("  " + field.getName() + "=" + field.getValue());
                        N.a(preparedstatement, k1++, field.getValue(), field.getSqlType());
                    }
                }

            }
            if(statement == null)
                a(preparedstatement, k1, vector);
            int i2 = preparedstatement.executeUpdate();
            P.a(preparedstatement);
            preparedstatement = null;
            if(statement != null)
            {
                P.a(statement);
                statement = null;
            }
            if(i2 == 0)
            {
                l();
                throw new AdoError(flag ? "The record to be deleted is no longer in the database! You should Refresh the query to get the latest state of the db." : "The record to be updated is no longer in the database! You should Refresh the query to get the latest state of the db.", 0x1118a);
            }
            if(flag)
                _mthbyte();
            else
                _mthfor(vector);
        }
        catch(java.sql.SQLException sqlexception)
        {
            if(preparedstatement != null)
                P.a(preparedstatement);
            if(statement != null)
                P.a(statement);
            throw new AdoError(sqlexception, "Error executing query: " + s4, 0x11180);
        }
    }

    private void a(java.sql.PreparedStatement preparedstatement, int i1, java.util.Vector vector)
        throws diamondedge.ado.AdoError
    {
        int j1 = vector.size();
        for(int k1 = 0; k1 < j1; k1++)
        {
            diamondedge.ado.Field field = null;
            try
            {
                field = a.getField((java.lang.String)vector.elementAt(k1));
            }
            catch(diamondedge.ado.AdoError adoerror)
            {
                if(adoerror.getNumber() != 30011 || C != 1)
                {
                    adoerror.setErrorMsg("To update the record, you need to have the unique key columns or all the searchable columns in the select list.");
                    adoerror.setErrorNumber(0x11191);
                    throw adoerror;
                }
            }
            if(field == null)
            {
                d.setAbsolutePosition(f);
                diamondedge.ado.Fields fields = d.getFields();
                field = fields.getField((java.lang.String)vector.elementAt(k1));
            }
            _mthnew("  where value: " + (field._mthdo() ? field._mthtry() : field.getValue()));
            N.a(preparedstatement, k1 + i1, field._mthdo() ? field._mthtry() : field.getValue(), field.getSqlType());
        }

    }

    java.lang.String _mthfor(java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        java.lang.String s2 = P.getIdentifierQuoteString();
        java.lang.String s3 = "delete from ";
        s3 = s3 + s1;
        return s3;
    }

    java.lang.String a(java.lang.String s1, diamondedge.ado.Fields fields)
        throws diamondedge.ado.AdoError
    {
        java.lang.String s2 = P.getIdentifierQuoteString();
        java.lang.String s3 = "update ";
        s3 = s3 + s1;
        s3 = s3 + " set ";
        for(int i1 = 0; i1 < fields.getCount(); i1++)
        {
            diamondedge.ado.Field field = fields.getField(i1);
            if(field._mthdo())
            {
                s3 = s3 + s2;
                s3 = s3 + field.getName();
                s3 = s3 + s2;
                s3 = s3 + " = ?, ";
            }
        }

        int j1 = s3.lastIndexOf(',');
        if(j1 == -1)
        {
            throw new AdoError("Either none of the fields were updated or the updated fields are not modifiable.", 0x1118e);
        } else
        {
            s3 = s3.substring(0, j1);
            return s3;
        }
    }

    void _mthif(java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        java.sql.Statement statement = null;
        try
        {
            if(m != 4)
            {
                statement = P._mthfor();
                o = statement.executeUpdate(s1);
                P.a(statement);
            }
        }
        catch(java.sql.SQLException sqlexception)
        {
            if(statement != null)
                try
                {
                    statement.close();
                }
                catch(java.sql.SQLException sqlexception1) { }
            throw new AdoError(sqlexception, "Error executing query: " + s1, 0x11180);
        }
    }

    private boolean _mthtry(java.util.Vector vector)
        throws diamondedge.ado.AdoError
    {
        java.lang.String s1 = "";
        boolean flag = false;
        for(int i1 = 0; i1 < vector.size(); i1++)
        {
            java.lang.String s2 = (java.lang.String)vector.elementAt(i1);
            Object obj = null;
            try
            {
                diamondedge.ado.Field field = a.getField(s2);
                if(field._mthdo())
                {
                    diamondedge.ado.Field field1 = d.a.getField(s2);
                    diamondedge.util.Variant variant = field.getValue();
                    field1.setValue(variant);
                    flag = true;
                }
            }
            catch(diamondedge.ado.AdoError adoerror) { }
        }

        return flag;
    }

    private boolean _mthnew(java.util.Vector vector)
        throws diamondedge.ado.AdoError
    {
        boolean flag = false;
        java.lang.String s1 = "";
        boolean flag1 = false;
        for(int i1 = 0; i1 < vector.size() && !flag; i1++)
        {
            java.lang.String s2 = (java.lang.String)vector.elementAt(i1);
            diamondedge.ado.Field field = a.getField(s2);
            if(field.isAutoInc())
                flag = true;
        }

        return flag;
    }

    private void r()
        throws diamondedge.ado.AdoError
    {
        if(C == 1)
        {
            java.util.Vector vector = null;
            if(C == 1)
            {
                java.util.Hashtable hashtable = _mthif(b);
                diamondedge.ado.QualifiedTableName qualifiedtablename = (diamondedge.ado.QualifiedTableName)b.elementAt(0);
                vector = (java.util.Vector)hashtable.get(qualifiedtablename);
            }
            if(!_mthnew(vector))
            {
                _mthtry(vector);
                d._fldnull.a(d.a);
                d.l();
                f = _fldnull.a(a);
            } else
            {
                f--;
            }
        } else
        if(C == 3)
            f = _fldnull.a(a);
    }

    private void _mthbyte()
        throws diamondedge.ado.AdoError
    {
        if(C == 1)
        {
            d._fldnull._mthint(f - 1);
            _fldnull._mthint(f - S);
        } else
        if(C == 3)
            _fldnull._mthint(f - 1);
    }

    private void _mthfor(java.util.Vector vector)
        throws diamondedge.ado.AdoError
    {
        if(C == 1)
        {
            if(_mthtry(vector))
                d._fldnull.a(f - 1, d.a);
            d.l();
            _fldnull.a(f - S, a);
        } else
        if(C == 3)
        {
            _fldnull.a(f - 1, a);
            if(I != null && I.isNumeric() && I.toInt() == 1)
                if(_fldfor)
                {
                    _fldnull.a(f - 1, false);
                    o++;
                } else
                {
                    _fldnull.a(f - 1, true);
                    o--;
                }
        }
        l();
    }

    private void l()
        throws diamondedge.ado.AdoError
    {
        for(int i1 = 0; i1 < a.getCount(); i1++)
        {
            diamondedge.ado.Field field = a.getField(i1);
            field.a();
        }

    }

    private final void ag()
        throws diamondedge.ado.AdoError
    {
        throw new AdoError("You cannot insert/update/delete if the locktype is set as read only", 0x1118b);
    }

    private final void aa()
        throws diamondedge.ado.AdoError
    {
        if(n)
        {
            n = false;
            throw e;
        } else
        {
            return;
        }
    }

    private final void g()
        throws diamondedge.ado.AdoError
    {
        if(N == null)
            return;
        int i1 = N.getCommandType();
        int j1 = N.d();
        if((j1 & 2) != 2 && i1 != 4 && i1 != 1024 && i1 != 4096 && i1 != 8192 && i1 != 0x10000 && i1 != 0x20000 && i1 != 16384 && i1 != 32768 && i1 != 2048)
            throw new AdoError("Invalid operation for the command executed.", 0x11192);
        else
            return;
    }

    final void a(diamondedge.ado.AdoError adoerror)
    {
        e = adoerror;
        n = true;
    }

    final void _mthnew(java.lang.String s1)
    {
        if(P != null)
            P._mthfor(s1);
    }

    public java.lang.String toString()
    {
        java.lang.String s1 = "Recordset[";
        if(i)
            s1 = s1 + "BOF,";
        if(Y)
            s1 = s1 + "EOF,";
        if(_fldnew)
            s1 = s1 + "Open,";
        if(_fldnew)
            s1 = s1 + "filter=" + I + ",";
        s1 = s1 + "recCount=" + _fldbyte + ",currentRowNum=" + f + ",cursorType=" + C + ",lockType=" + m + " " + N + "]";
        return s1;
    }

    public static boolean isSqlStatement(java.lang.String s1)
    {
        java.util.StringTokenizer stringtokenizer = new StringTokenizer(s1, " \r\n" +
"\t"
);
        if(stringtokenizer.hasMoreTokens())
        {
            java.lang.String s2 = stringtokenizer.nextToken().trim().toLowerCase();
            return U.containsKey(s2);
        } else
        {
            return false;
        }
    }

    static void C()
    {
        U = new Hashtable();
        java.lang.Boolean boolean1 = new Boolean(true);
        U.put("select", boolean1);
        U.put("insert", boolean1);
        U.put("update", boolean1);
        U.put("delete", boolean1);
        U.put("commit", boolean1);
        U.put("rollback", boolean1);
        U.put("create", boolean1);
        U.put("drop", boolean1);
        U.put("alter", boolean1);
        U.put("grant", boolean1);
        U.put("revoke", boolean1);
        U.put("lock", boolean1);
        U.put("call", boolean1);
        U.put("exec", boolean1);
        U.put("execute", boolean1);
    }

    public void cancelBatch()
        throws diamondedge.ado.AdoError
    {
        if(_fldint != null)
            _fldint.clear();
        if(I != null && I.isNumeric() && I.toInt() == 1)
            _fldnull._mthdo(false);
    }

    public void updateBatch()
        throws diamondedge.ado.AdoError
    {
        if(s)
        {
            int i1 = getAbsolutePosition();
            A();
            s = false;
            diamondedge.util.Variant variant = getFilter();
            if(variant != null)
                setFilter(null);
            int j1 = _fldint != null ? _fldint.size() : 0;
            try
            {
                for(java.util.Enumeration enumeration = _fldint.elements(); enumeration.hasMoreElements();)
                {
                    diamondedge.ado.a a1 = (diamondedge.ado.a)enumeration.nextElement();
                    _mthint(a1._mthdo());
                    _mthdo(true);
                    int k1 = a.getCount();
                    for(int l1 = 0; l1 < k1; l1++)
                    {
                        diamondedge.ado.Field field = a.getField(l1);
                        diamondedge.util.Variant variant1 = field.getValue();
                        field.a();
                        if(a1._mthif(l1))
                        {
                            field.a(a1.a(l1));
                            field.setValue(variant1);
                        }
                    }

                    _fldint.remove(new Integer(a1._mthdo()));
                    if(a1._mthif())
                    {
                        delete();
                    } else
                    {
                        if(a1.a())
                            _mthif(1);
                        else
                            _mthif(2);
                        _mthelse();
                    }
                }

            }
            catch(diamondedge.ado.AdoError adoerror)
            {
                throw adoerror;
            }
            finally
            {
                s = true;
                if(variant != null)
                    setFilter(variant);
                _mthint(i1);
            }
        } else
        {
            update();
        }
    }

    private void _mthtry()
        throws diamondedge.ado.AdoError
    {
        if(_fldint == null)
            _fldint = new Hashtable();
        java.lang.Integer integer = new Integer(f);
        diamondedge.ado.a a1 = (diamondedge.ado.a)_fldint.get(integer);
        if(a1 == null)
        {
            a1 = new a(f, getFieldCount());
            _fldint.put(integer, a1);
        }
        if(E == 1)
            a1.a(true);
        else
        if(E == 4)
            a1._mthif(true);
        for(int i1 = 0; i1 < a.getCount(); i1++)
        {
            diamondedge.ado.Field field = a.getField(i1);
            if(field._mthdo())
            {
                diamondedge.util.Variant variant = field._mthtry();
                field.a();
                a1.a(i1, true);
                a1.a(i1, variant);
                continue;
            }
            if(!a1._mthif(i1))
                a1.a(i1, field.getValue());
        }

        _mthif(8);
    }

    private static final int j = 0;
    private static final int r = 1;
    private static final int G = 2;
    private static final int _fldchar = 3;
    private static final int X = 4;
    private static final int R = 5;
    private static final int A = 6;
    private java.util.Hashtable _fldint = null;
    private boolean s = false;
    private static java.util.Hashtable U = null;
    private boolean T = false;
    private boolean _fldnew = false;
    boolean _fldfor = false;
    private boolean g = false;
    private boolean i = false;
    private boolean Y = false;
    private boolean _fldif = false;
    private boolean L = false;
    private boolean q = false;
    private boolean F = false;
    private int _fldtry = 0;
    private int f = 0;
    private int C = 0;
    private int m = 0;
    private int _fldgoto = 0;
    private int p = 0;
    private int _fldbyte = 0;
    private int E = 0;
    private int _fldlong = 0;
    private java.util.Properties w = null;
    private java.util.Vector b = null;
    private diamondedge.ado.Recordset Q = null;
    private diamondedge.ado.Connection P = null;
    private diamondedge.ado.Command N = null;
    private diamondedge.ado.AdoCursor _fldnull = null;
    private java.sql.Statement aa = null;
    private java.sql.ResultSet D = null;
    private diamondedge.ado.Fields a = null;
    private diamondedge.util.Variant I = null;
    private java.lang.Thread z = null;
    private diamondedge.ado.Command V = null;
    private diamondedge.ado.Recordset d = null;
    private java.lang.String W = null;
    private java.lang.String t = null;
    private diamondedge.ado.Command _fldelse = null;
    private diamondedge.ado.Recordset O = null;
    private boolean k = false;
    private boolean h = false;
    private java.lang.Thread _fldcase = null;
    private int S = 0;
    private int _fldvoid = 0;
    private boolean l = false;
    private boolean K = false;
    private int _flddo = 0;
    static final int c = 0;
    static final int Z = 1;
    static final int B = 2;
    static final int J = 4;
    private static java.lang.String u = null;
    private int o = 0;
    java.util.Hashtable v = null;
    private java.lang.Object H = null;
    private diamondedge.ado.AdoError e = null;
    private boolean n = false;
    protected diamondedge.vb.EventListenerList M = null;

    static 
    {
        diamondedge.ado.Recordset.C();
    }
}
