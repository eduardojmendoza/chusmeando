// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.ado;

import diamondedge.util.HashList;
import diamondedge.util.Variant;
import diamondedge.vb.LoginForm;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Vector;
import javax.swing.JOptionPane;

// Referenced classes of package diamondedge.ado:
//            AdoError, Recordset, Command, TypeInfo, 
//            AdoUtil

public class Connection
{

    public Connection()
    {
        d = 30;
        c = 15;
        k = 4096;
        _fldchar = "";
        _fldfor = null;
        _fldif = null;
        l = false;
        _fldnew = null;
        _fldtry = null;
        j = false;
        f = 0;
        _fldbyte = false;
        _fldelse = 0;
        _fldnull = "cursor";
        i = 0;
        e = null;
        b = null;
        a = 0;
        _flddo = new Properties();
    }

    Connection(diamondedge.ado.Connection connection, boolean flag)
        throws diamondedge.ado.AdoError
    {
        this();
        d = connection.d;
        c = connection.c;
        _fldchar = connection._fldchar;
        _fldfor = connection._fldfor;
        _fldif = connection._fldif;
        if(flag)
        {
            _fldnew = _mthcase();
            h = "con" + ++_fldgoto;
            _mthfor("opening con " + h);
            diamondedge.ado.AdoUtil.a(connection._flddo, _flddo);
            l = true;
        }
    }

    public static diamondedge.ado.Connection openConnection(java.lang.String s, java.lang.String s1, java.lang.String s2, java.lang.String s3)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.AdoUtil.registerDriver(s3);
        diamondedge.ado.Connection connection = diamondedge.ado.Connection.getConnection(s);
        if(connection == null)
        {
            connection = new Connection();
            connection.open(s, s1, s2);
            g.add(connection, s);
        } else
        if(!connection.isConnected())
            connection.open(s, s1, s2);
        return connection;
    }

    public static diamondedge.ado.Connection getConnection(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        java.lang.Object obj = g.get(s);
        if(obj instanceof diamondedge.ado.Connection)
            return (diamondedge.ado.Connection)obj;
        else
            return null;
    }

    public static diamondedge.ado.Connection getConnection(int i1)
    {
        java.lang.Object obj = g.get(i1);
        if(obj instanceof diamondedge.ado.Connection)
            return (diamondedge.ado.Connection)obj;
        else
            return null;
    }

    public static diamondedge.ado.Connection getLastConnection()
    {
        return diamondedge.ado.Connection.getConnection(g.size() - 1);
    }

    public static int getConnectionCount()
    {
        return g.size();
    }

    public int getConnectionTimeout()
    {
        return c;
    }

    public void setConnectionTimeout(int i1)
        throws diamondedge.ado.AdoError
    {
        if(!l)
        {
            if(i1 < 1)
                throw new AdoError("Connection timeout value should be greater than 0", 10019);
            c = i1;
        } else
        {
            throw new AdoError("you already have an open connection for this object.setConnectionTimeout", 10008);
        }
    }

    public int getCommandTimeout()
    {
        return d;
    }

    public void setCommandTimeout(int i1)
    {
        d = i1;
    }

    public java.lang.String getConnectionString()
    {
        return _fldchar;
    }

    public void setConnectionString(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        if(!l)
            a(s, null, null);
        else
            throw new AdoError("you already have an open connection for this object.setConnectionString", 10008);
    }

    public void setConnectionString(java.lang.String s, java.lang.String s1, java.lang.String s2)
        throws diamondedge.ado.AdoError
    {
        if(!l)
            a(s, s1, s2);
        else
            throw new AdoError("you already have an open connection for this object.setConnectionString", 10008);
    }

    public int getIsolationLevel()
    {
        return k;
    }

    public void setIsolationLevel(int i1)
    {
        k = i1;
    }

    public java.util.Properties getProperties()
    {
        return _flddo;
    }

    public java.lang.String getVersion()
    {
        return "1.0";
    }

    public boolean isBusy()
    {
        return _fldbyte;
    }

    public boolean isInTransaction()
    {
        return j;
    }

    public int beginTrans()
        throws diamondedge.ado.AdoError
    {
        if(!l)
            _mthnew("BeginTrans");
        java.lang.Boolean boolean1 = (java.lang.Boolean)_flddo.get("TransactionSupport");
        if(!boolean1.booleanValue())
            throw new AdoError("The backend database does not support transactions.", 10002);
        if(j)
            throw new AdoError("Nested Transactions are not supported.  You need to commit/rollback the previous transaction before beginning a new one.", 10021);
        try
        {
            _fldnew.setTransactionIsolation(diamondedge.ado.AdoUtil.getJDBCIsolationLevel(k));
            _fldnew.setAutoCommit(false);
            j = true;
        }
        catch(java.sql.SQLException sqlexception)
        {
            throw new AdoError(sqlexception, "error starting the transaction.", 10003);
        }
        return ++f;
    }

    public void commitTrans()
        throws diamondedge.ado.AdoError
    {
        if(!l)
            _mthnew("CommitTrans");
        if(!j)
            _mthint("CommitTrans");
        try
        {
            _fldnew.commit();
            f--;
            if(f == 0)
            {
                j = false;
                _fldnew.setAutoCommit(true);
            }
            a(true);
        }
        catch(java.sql.SQLException sqlexception)
        {
            diamondedge.ado.AdoError adoerror = new AdoError();
            adoerror.setErrorMsg("error while committing transaction.");
            adoerror.setErrorNumber(10004);
            adoerror.a(sqlexception);
        }
    }

    public void rollbackTrans()
        throws diamondedge.ado.AdoError
    {
        if(!l)
            _mthnew("RollbackTrans");
        if(!j)
            _mthint("RollbackTrans");
        try
        {
            _fldnew.rollback();
            f--;
            if(f == 0)
            {
                j = false;
                _fldnew.setAutoCommit(true);
            }
            a(false);
        }
        catch(java.sql.SQLException sqlexception)
        {
            diamondedge.ado.AdoError adoerror = new AdoError();
            adoerror.setErrorMsg("error while rolling back transaction.");
            adoerror.setErrorNumber(10005);
            adoerror.a(sqlexception);
        }
    }

    public void close()
        throws diamondedge.ado.AdoError
    {
        _mthfor("number of active statements are : " + _fldelse);
        if(l)
        {
            if(j)
                throw new AdoError("You need to complete the current transaction before closing the connection.", 10006);
            try
            {
                _mthchar();
                if(_fldtry != null)
                {
                    _fldtry.close();
                    _mthfor("closed second con " + _fldvoid);
                    _fldtry = null;
                }
                _fldnew.close();
                _mthfor("closed con " + h);
                _fldnew = null;
                l = false;
            }
            catch(java.sql.SQLException sqlexception)
            {
                throw new AdoError(sqlexception, "cannot close the connection.", 10007);
            }
        }
    }

    public void useConnection(java.sql.Connection connection)
        throws diamondedge.ado.AdoError
    {
        if(l)
        {
            throw new AdoError("you already have an open connection for this object.", 10008);
        } else
        {
            _fldnew = connection;
            h = "pooledcon" + ++_fldgoto;
            _mthfor("using con " + h);
            _mthbyte();
            l = true;
            return;
        }
    }

    public void open()
        throws diamondedge.ado.AdoError
    {
        if(l)
        {
            throw new AdoError("you already have an open connection for this object.", 10008);
        } else
        {
            _fldnew = _mthcase();
            h = "con" + ++_fldgoto;
            _mthfor("opening con " + h);
            _mthbyte();
            l = true;
            return;
        }
    }

    public void open(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        if(l)
        {
            throw new AdoError("you already have an open connection for this object.", 10008);
        } else
        {
            a(s, null, null);
            open();
            return;
        }
    }

    public void open(java.lang.String s, java.lang.String s1)
        throws diamondedge.ado.AdoError
    {
        if(l)
        {
            throw new AdoError("you already have an open connection for this object.", 10008);
        } else
        {
            a(s, s1, null);
            open();
            return;
        }
    }

    public void open(java.lang.String s, java.lang.String s1, java.lang.String s2)
        throws diamondedge.ado.AdoError
    {
        if(l)
        {
            throw new AdoError("you already have an open connection for this object.", 10008);
        } else
        {
            a(s, s1, s2);
            open();
            return;
        }
    }

    public diamondedge.ado.Recordset openRecordset(java.lang.String s, int i1, int j1)
        throws diamondedge.ado.AdoError
    {
        diamondedge.ado.Recordset recordset = new Recordset();
        recordset.open(s, this, i1, j1, 1);
        return recordset;
    }

    public diamondedge.ado.Recordset execute(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        return execute(s, null, 1);
    }

    public diamondedge.ado.Recordset execute(java.lang.String s, diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        return execute(s, variant, 1);
    }

    public diamondedge.ado.Recordset execute(java.lang.String s, diamondedge.util.Variant variant, int i1)
        throws diamondedge.ado.AdoError
    {
        if(!l)
        {
            throw new AdoError("You must have an open connection before calling: Execute", 10009);
        } else
        {
            diamondedge.ado.Command command = new Command();
            command.setCommandText(s);
            command.setCommandType(i1);
            command.setCommandTimeout(d);
            command.setActiveConnection(this);
            return command.execute(variant, i1);
        }
    }

    public int executeSQL(java.lang.String s)
        throws java.sql.SQLException
    {
        if(!l)
        {
            throw new SQLException("You must have an open connection before calling: Execute");
        } else
        {
            java.sql.Statement statement = _fldnew.createStatement();
            statement.execute(s);
            int i1 = statement.getUpdateCount();
            statement.close();
            return i1;
        }
    }

    public boolean isConnected()
    {
        return l;
    }

    public int getMaxStatements()
    {
        java.lang.Integer integer = (java.lang.Integer)_flddo.get("MaxStatements");
        return integer.intValue();
    }

    public java.lang.String getIdentifierQuoteString()
    {
        return (java.lang.String)_flddo.get("IdentifierQuoteString");
    }

    public java.lang.String getCatalogSeparator()
    {
        return (java.lang.String)_flddo.get("CatalogSeparator");
    }

    public boolean isPositionedDeleteSupported()
    {
        return ((java.lang.Boolean)_flddo.get("SupportsPosDelete")).booleanValue();
    }

    public boolean isPositionedUpdateSupported()
    {
        return ((java.lang.Boolean)_flddo.get("SupportsPosUpdate")).booleanValue();
    }

    public java.util.Vector getRecordsets()
    {
        return e;
    }

    public int getWorkArounds()
    {
        return a;
    }

    public void setWorkArounds(int i1)
    {
        a = i1;
    }

    public static void setLogWriter(java.io.PrintWriter printwriter)
    {
        _fldlong = printwriter;
        java.sql.DriverManager.setLogWriter(printwriter);
    }

    public static java.io.PrintWriter getLogWriter()
    {
        return _fldlong;
    }

    protected void finalize()
        throws java.lang.Throwable
    {
        super.finalize();
        g.remove(_fldchar);
        if(l && _fldnew != null)
        {
            if(j)
                try
                {
                    rollbackTrans();
                }
                catch(diamondedge.ado.AdoError adoerror) { }
            try
            {
                close();
            }
            catch(diamondedge.ado.AdoError adoerror1) { }
        }
    }

    public java.sql.Connection getConnection()
    {
        return _fldnew;
    }

    java.sql.Connection _mthelse()
        throws diamondedge.ado.AdoError
    {
        if(_fldtry == null)
        {
            _fldtry = _mthcase();
            _fldvoid = "con" + ++_fldgoto;
            _mthfor("opening second con " + _fldvoid);
        }
        return _fldtry;
    }

    public java.lang.String getUserId()
    {
        return _fldfor;
    }

    public java.lang.String getPassword()
    {
        return _fldif;
    }

    int _mthdo()
    {
        return _fldelse;
    }

    private void a(java.lang.String s, java.lang.String s1, java.lang.String s2)
    {
        _fldchar = s;
        if(s1 != null)
            _fldfor = s1;
        if(s2 != null)
            _fldif = s2;
    }

    private java.sql.Connection _mthcase()
        throws diamondedge.ado.AdoError
    {
        java.sql.Connection connection = null;
        try
        {
            java.sql.DriverManager.setLoginTimeout(c);
            try
            {
                if(_fldfor == null && _fldif == null)
                    connection = java.sql.DriverManager.getConnection(_fldchar);
                else
                    connection = java.sql.DriverManager.getConnection(_fldchar, _fldfor, _fldif);
                if(diamondedge.ado.Connection.getConnection(_fldchar) == null)
                    g.add(this, _fldchar);
            }
            catch(java.sql.SQLException sqlexception)
            {
                if(sqlexception.getErrorCode() == -3810)
                {
                    java.lang.String s = "error connecting to data source '" + _fldchar + "'\n" +
"  "
 + sqlexception.getMessage();
                    if(s.indexOf("Access") > 0)
                    {
                        s = s + "\n\n" +
"Make sure you close the Access database before\n" +
"trying to run the application from Java"
;
                        java.lang.System.out.println(s);
                        try
                        {
                            javax.swing.JOptionPane.showMessageDialog(null, s, "Connection Error", 0);
                        }
                        catch(java.lang.Exception exception) { }
                        java.lang.System.exit(0);
                    }
                    throw sqlexception;
                }
                if(sqlexception.getErrorCode() == 1017 || _fldfor == null || _fldfor.equals(""))
                    try
                    {
                        java.lang.System.out.println("invalid login: " + sqlexception.getErrorCode() + ": " + sqlexception);
                        connection = diamondedge.vb.LoginForm.getConnection(_fldchar, _fldfor);
                    }
                    catch(java.lang.Throwable throwable)
                    {
                        throw sqlexception;
                    }
                if(connection == null)
                    throw sqlexception;
            }
            try
            {
                connection.setAutoCommit(true);
            }
            catch(java.sql.SQLException sqlexception1)
            {
                a("setAutoCommit", sqlexception1);
            }
        }
        catch(java.sql.SQLException sqlexception2)
        {
            throw new AdoError(sqlexception2, "Error connecting to data source: " + _fldchar, 10010);
        }
        return connection;
    }

    private java.lang.String _mthdo(java.lang.String s)
    {
        if(s == null)
            return "";
        else
            return s;
    }

    private void _mthbyte()
        throws diamondedge.ado.AdoError
    {
        a(_fldnew != null, "connection is null in initializeConnectionProps");
        try
        {
            java.sql.DatabaseMetaData databasemetadata = _fldnew.getMetaData();
            java.lang.String s = "";
            try
            {
                if((getWorkArounds() & 8) != 8)
                    s = _fldnew.getCatalog();
            }
            catch(java.sql.SQLException sqlexception1) { }
            _flddo.put("CurrentCatalog", _mthdo(s));
            int i1 = 0;
            try
            {
                i1 = databasemetadata.getMaxConnections();
            }
            catch(java.sql.SQLException sqlexception2)
            {
                a("ActiveSessions", sqlexception2);
            }
            _flddo.put("ActiveSessions", new Integer(i1));
            byte byte0 = 1;
            try
            {
                byte0 = ((byte)(databasemetadata.isCatalogAtStart() ? 1 : 2));
            }
            catch(java.sql.SQLException sqlexception3)
            {
                a("CatalogLocation", sqlexception3);
            }
            _flddo.put("CatalogLocation", new Integer(byte0));
            java.lang.String s1 = "";
            try
            {
                s1 = databasemetadata.getCatalogTerm();
            }
            catch(java.sql.SQLException sqlexception4)
            {
                a("CatalogTerm", sqlexception4);
            }
            _flddo.put("CatalogTerm", _mthdo(s1));
            int j1 = 0;
            try
            {
                if(databasemetadata.supportsCatalogsInDataManipulation())
                    j1 |= 1;
                if(databasemetadata.supportsCatalogsInTableDefinitions())
                    j1 |= 2;
                if(databasemetadata.supportsCatalogsInIndexDefinitions())
                    j1 |= 4;
                if(databasemetadata.supportsCatalogsInPrivilegeDefinitions())
                    j1 |= 8;
            }
            catch(java.sql.SQLException sqlexception5)
            {
                a("CatalogUsage", sqlexception5);
            }
            _flddo.put("CatalogUsage", new Integer(j1));
            byte byte1 = 0;
            try
            {
                byte1 = ((byte)(databasemetadata.nullPlusNonNullIsNull() ? 1 : 2));
            }
            catch(java.sql.SQLException sqlexception6)
            {
                a("ConcatNullBehavior", sqlexception6);
            }
            _flddo.put("ConcatNullBehavior", new Integer(byte1));
            java.lang.String s2 = "";
            try
            {
                s2 = databasemetadata.getURL();
            }
            catch(java.sql.SQLException sqlexception7)
            {
                a("DataSourceName", sqlexception7);
            }
            _flddo.put("DataSourceName", _mthdo(s2));
            boolean flag = false;
            try
            {
                flag = databasemetadata.isReadOnly();
            }
            catch(java.sql.SQLException sqlexception8)
            {
                a("DataSourceReadOnly", sqlexception8);
            }
            _flddo.put("DataSourceReadOnly", new Boolean(flag));
            java.lang.String s3 = "";
            try
            {
                s3 = databasemetadata.getDatabaseProductName();
            }
            catch(java.sql.SQLException sqlexception9)
            {
                a("DBMSName", sqlexception9);
            }
            _flddo.put("DBMSName", _mthdo(s3));
            java.lang.String s4 = "";
            try
            {
                s4 = databasemetadata.getDatabaseProductVersion();
            }
            catch(java.sql.SQLException sqlexception10)
            {
                a("DBMSVer", sqlexception10);
            }
            _flddo.put("DBMSVer", _mthdo(s4));
            byte byte2 = 0;
            try
            {
                if(databasemetadata.supportsGroupByUnrelated())
                    byte2 = 3;
                else
                if(databasemetadata.supportsGroupByBeyondSelect())
                    byte2 = 2;
                else
                if(databasemetadata.supportsGroupBy())
                    byte2 = 1;
            }
            catch(java.sql.SQLException sqlexception11)
            {
                a("GroupBy", sqlexception11);
            }
            _flddo.put("GroupBy", new Integer(byte2));
            _flddo.put("HeterogeneousTables", new Integer(0));
            byte byte3 = 0;
            try
            {
                if(databasemetadata.storesLowerCaseIdentifiers())
                    byte3 = 2;
                else
                if(databasemetadata.storesUpperCaseIdentifiers())
                    byte3 = 1;
                else
                if(databasemetadata.storesMixedCaseIdentifiers())
                    byte3 = 4;
                else
                if(databasemetadata.supportsMixedCaseIdentifiers())
                    byte3 = 3;
            }
            catch(java.sql.SQLException sqlexception12)
            {
                a("IdentifierCase", sqlexception12);
            }
            _flddo.put("IdentifierCase", new Integer(byte3));
            byte byte4 = 0;
            try
            {
                if(databasemetadata.storesLowerCaseQuotedIdentifiers())
                    byte4 = 2;
                else
                if(databasemetadata.storesUpperCaseQuotedIdentifiers())
                    byte4 = 1;
                else
                if(databasemetadata.storesMixedCaseQuotedIdentifiers())
                    byte4 = 4;
                else
                if(databasemetadata.supportsMixedCaseQuotedIdentifiers())
                    byte4 = 3;
            }
            catch(java.sql.SQLException sqlexception13)
            {
                a("QuotedIdentifierCase", sqlexception13);
            }
            _flddo.put("QuotedIdentifierCase", new Integer(byte4));
            int k1 = 1;
            _flddo.put("LockModes", new Integer(k1));
            int l1 = 0;
            try
            {
                l1 = databasemetadata.getMaxIndexLength();
            }
            catch(java.sql.SQLException sqlexception14)
            {
                a("MaxIndexSize", sqlexception14);
            }
            _flddo.put("MaxIndexSize", new Integer(l1));
            int i2 = 0;
            try
            {
                i2 = databasemetadata.getMaxRowSize();
            }
            catch(java.sql.SQLException sqlexception15)
            {
                a("MaxRowSize", sqlexception15);
            }
            _flddo.put("MaxRowSize", new Integer(i2));
            boolean flag1 = false;
            try
            {
                flag1 = databasemetadata.doesMaxRowSizeIncludeBlobs();
            }
            catch(java.sql.SQLException sqlexception16)
            {
                a("MaxRowSizeIncludesBLOB", sqlexception16);
            }
            _flddo.put("MaxRowSizeIncludesBLOB", new Boolean(flag1));
            int j2 = 0;
            try
            {
                j2 = databasemetadata.getMaxTablesInSelect();
            }
            catch(java.sql.SQLException sqlexception17)
            {
                a("MaxTablesInSelect", sqlexception17);
            }
            _flddo.put("MaxTablesInSelect", new Integer(j2));
            _flddo.put("MultipleUpdate", new Boolean(false));
            byte byte5 = 1;
            try
            {
                if(databasemetadata.nullsAreSortedAtStart())
                    byte5 = 4;
                else
                if(databasemetadata.nullsAreSortedHigh())
                    byte5 = 2;
                else
                if(databasemetadata.nullsAreSortedLow())
                    byte5 = 4;
            }
            catch(java.sql.SQLException sqlexception18)
            {
                a("NullCollation", sqlexception18);
            }
            _flddo.put("NullCollation", new Integer(byte5));
            boolean flag2 = false;
            try
            {
                flag2 = databasemetadata.supportsOrderByUnrelated();
            }
            catch(java.sql.SQLException sqlexception19)
            {
                a("OrderByColumnsInSelect", sqlexception19);
            }
            _flddo.put("OrderByColumnsInSelect", new Boolean(flag2));
            byte byte6 = 1;
            try
            {
                if(databasemetadata.supportsOpenStatementsAcrossCommit())
                    byte6 = 2;
            }
            catch(java.sql.SQLException sqlexception20)
            {
                a("PrepareCommitBehavior", sqlexception20);
            }
            _flddo.put("PrepareCommitBehavior", new Integer(byte6));
            byte byte7 = 1;
            boolean flag3 = false;
            if(flag3)
                try
                {
                    if(databasemetadata.supportsOpenStatementsAcrossRollback())
                        byte7 = 2;
                }
                catch(java.sql.SQLException sqlexception21)
                {
                    a("PrepareAbortBehavior", sqlexception21);
                }
            _flddo.put("PrepareAbortBehavior", new Integer(byte7));
            java.lang.String s5 = "";
            try
            {
                s5 = databasemetadata.getProcedureTerm();
            }
            catch(java.sql.SQLException sqlexception22)
            {
                a("ProcedureTerm", sqlexception22);
            }
            _flddo.put("ProcedureTerm", _mthdo(s5));
            java.lang.String s6 = "";
            try
            {
                s6 = databasemetadata.getDriverName();
            }
            catch(java.sql.SQLException sqlexception23)
            {
                a("ProviderName", sqlexception23);
            }
            _flddo.put("ProviderName", _mthdo(s6));
            java.lang.String s7 = "";
            try
            {
                s7 = databasemetadata.getDriverVersion();
            }
            catch(java.sql.SQLException sqlexception24)
            {
                a("ProviderVersion", sqlexception24);
            }
            _flddo.put("ProviderVersion", _mthdo(s7));
            java.lang.String s8 = "";
            try
            {
                s8 = databasemetadata.getSchemaTerm();
            }
            catch(java.sql.SQLException sqlexception25)
            {
                a("SchemaTerm", sqlexception25);
            }
            _flddo.put("SchemaTerm", _mthdo(s8));
            int k2 = 0;
            try
            {
                if(databasemetadata.supportsSchemasInDataManipulation())
                    k2 |= 1;
                if(databasemetadata.supportsSchemasInTableDefinitions())
                    k2 |= 2;
                if(databasemetadata.supportsSchemasInIndexDefinitions())
                    k2 |= 4;
                if(databasemetadata.supportsSchemasInPrivilegeDefinitions())
                    k2 |= 8;
            }
            catch(java.sql.SQLException sqlexception26)
            {
                a("SchemaUsage", sqlexception26);
            }
            _flddo.put("SchemaUsage", new Integer(k2));
            int l2 = 0;
            try
            {
                if(!databasemetadata.supportsANSI92EntryLevelSQL());
                if(!databasemetadata.supportsANSI92IntermediateSQL());
                if(!databasemetadata.supportsANSI92FullSQL());
                if(!databasemetadata.supportsMinimumSQLGrammar());
                if(!databasemetadata.supportsCoreSQLGrammar());
                if(!databasemetadata.supportsExtendedSQLGrammar());
                if(!databasemetadata.supportsLikeEscapeClause());
            }
            catch(java.sql.SQLException sqlexception27) { }
            _flddo.put("SQLSupport", new Integer(l2));
            int i3 = 0;
            try
            {
                if(databasemetadata.supportsCorrelatedSubqueries())
                    i3 |= 1;
                if(databasemetadata.supportsSubqueriesInComparisons())
                    i3 |= 2;
                if(databasemetadata.supportsSubqueriesInExists())
                    i3 |= 4;
                if(databasemetadata.supportsSubqueriesInIns())
                    i3 |= 8;
                if(databasemetadata.supportsSubqueriesInQuantifieds())
                    i3 |= 0x10;
            }
            catch(java.sql.SQLException sqlexception28)
            {
                a("SubQueries", sqlexception28);
            }
            _flddo.put("SubQueries", new Integer(i3));
            k = -1;
            try
            {
                int j3 = _fldnew.getTransactionIsolation();
                k = diamondedge.ado.AdoUtil.getAdoIsolationLevel(j3);
            }
            catch(java.sql.SQLException sqlexception29) { }
            _flddo.put("TableTerm", "TABLE");
            java.lang.String s9 = "";
            try
            {
                s9 = databasemetadata.getUserName();
            }
            catch(java.sql.SQLException sqlexception30)
            {
                a("UserName", sqlexception30);
            }
            _flddo.put("UserName", _mthdo(s9));
            boolean flag4 = false;
            try
            {
                flag4 = databasemetadata.supportsTransactions();
            }
            catch(java.sql.SQLException sqlexception31)
            {
                a("TransactionSupport", sqlexception31);
            }
            _flddo.put("TransactionSupport", new Boolean(flag4));
            _flddo.put("CommandSupport", new Boolean(true));
            int k3 = 1;
            try
            {
                k3 = databasemetadata.getMaxStatements();
            }
            catch(java.sql.SQLException sqlexception32)
            {
                a("MaxStatements", sqlexception32);
            }
            _flddo.put("MaxStatements", new Integer(k3));
            java.lang.String s10 = "\"";
            try
            {
                s10 = databasemetadata.getIdentifierQuoteString();
                if(s10 == null)
                    s10 = "\"";
            }
            catch(java.sql.SQLException sqlexception33)
            {
                a("IdentifierQuoteString", sqlexception33);
            }
            _flddo.put("IdentifierQuoteString", s10);
            java.lang.String s11 = ".";
            _flddo.put("CatalogSeparator", s11);
            boolean flag5 = false;
            try
            {
                flag5 = databasemetadata.isCatalogAtStart();
            }
            catch(java.sql.SQLException sqlexception34)
            {
                a("IsCatalogAtStart", sqlexception34);
            }
            _flddo.put("IsCatalogAtStart", new Boolean(flag5));
            boolean flag6 = false;
            try
            {
                flag6 = databasemetadata.supportsPositionedUpdate();
            }
            catch(java.sql.SQLException sqlexception35)
            {
                a("SupportsPosUpdate", sqlexception35);
            }
            _flddo.put("SupportsPosUpdate", new Boolean(flag6));
            boolean flag7 = false;
            try
            {
                flag7 = databasemetadata.supportsPositionedDelete();
            }
            catch(java.sql.SQLException sqlexception36)
            {
                a("SupportsPosDelete", sqlexception36);
            }
            _flddo.put("SupportsPosDelete", new Boolean(flag7));
        }
        catch(java.sql.SQLException sqlexception)
        {
            throw new AdoError(sqlexception, "Error getting capabilities for this connection: ", 10011);
        }
    }

    private void _mthint()
        throws java.sql.SQLException
    {
        java.sql.DatabaseMetaData databasemetadata = _fldnew.getMetaData();
        java.sql.ResultSet resultset = databasemetadata.getTypeInfo();
        b = new Vector(resultset.getMetaData().getColumnCount());
        while(resultset.next()) 
            try
            {
                diamondedge.ado.TypeInfo typeinfo = new TypeInfo(resultset);
                b.addElement(typeinfo);
            }
            catch(java.lang.Exception exception) { }
        resultset.close();
    }

    diamondedge.ado.TypeInfo a(int i1)
        throws java.sql.SQLException
    {
        diamondedge.ado.TypeInfo typeinfo = null;
        if(b == null)
            _mthint();
        boolean flag = false;
        for(int j1 = 0; j1 < b.size() && !flag; j1++)
        {
            if(b.elementAt(j1) == null)
                continue;
            typeinfo = (diamondedge.ado.TypeInfo)b.elementAt(j1);
            if(typeinfo._fldnull == i1)
                flag = true;
        }

        if(!flag)
            typeinfo = null;
        return typeinfo;
    }

    void _mthif(diamondedge.ado.Recordset recordset)
    {
        if(e == null)
            e = new Vector(5);
        e.addElement(recordset);
    }

    void a(diamondedge.ado.Recordset recordset)
    {
        e.removeElement(recordset);
    }

    java.sql.Connection _mthnew()
        throws diamondedge.ado.AdoError
    {
        int i1 = getMaxStatements();
        java.sql.Connection connection;
        if(i1 == 0 || i1 > _fldelse)
            connection = getConnection();
        else
            connection = _mthelse();
        return connection;
    }

    synchronized void a()
    {
        _fldelse++;
    }

    synchronized void _mthtry()
    {
        _fldelse--;
    }

    java.sql.Statement _mthfor()
        throws diamondedge.ado.AdoError
    {
        java.sql.Statement statement = null;
        try
        {
            java.sql.Connection connection = _mthnew();
            statement = connection.createStatement();
            a();
        }
        catch(java.sql.SQLException sqlexception)
        {
            throw new AdoError(sqlexception, "error creating new statement.", 10012);
        }
        return statement;
    }

    java.sql.PreparedStatement a(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        java.sql.PreparedStatement preparedstatement = null;
        try
        {
            java.sql.Connection connection = _mthnew();
            preparedstatement = connection.prepareStatement(s);
            a();
        }
        catch(java.sql.SQLException sqlexception)
        {
            throw new AdoError(sqlexception, "error preparing statement: " + s, 10013);
        }
        return preparedstatement;
    }

    java.sql.CallableStatement _mthif(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        java.sql.CallableStatement callablestatement = null;
        try
        {
            java.sql.Connection connection = _mthnew();
            callablestatement = connection.prepareCall(s);
            a();
        }
        catch(java.sql.SQLException sqlexception)
        {
            throw new AdoError(sqlexception, "error creating call statement: " + s, 10014);
        }
        return callablestatement;
    }

    public java.sql.DatabaseMetaData getMetaData()
        throws diamondedge.ado.AdoError
    {
        java.sql.DatabaseMetaData databasemetadata = null;
        try
        {
            java.sql.Connection connection = _mthnew();
            databasemetadata = connection.getMetaData();
        }
        catch(java.sql.SQLException sqlexception)
        {
            throw new AdoError(sqlexception, "error creating database metadata.", 10015);
        }
        return databasemetadata;
    }

    void a(java.sql.Statement statement)
        throws diamondedge.ado.AdoError
    {
        try
        {
            statement.close();
            _mthtry();
            statement = null;
        }
        catch(java.sql.SQLException sqlexception)
        {
            throw new AdoError(sqlexception, "error closing statement.", 10016);
        }
    }

    java.lang.String _mthif()
    {
        i++;
        return _fldnull + i;
    }

    void _mthfor(java.lang.String s)
    {
        if(_fldlong != null)
            _fldlong.println(s);
    }

    private void a(boolean flag)
        throws diamondedge.ado.AdoError
    {
        if(e != null)
        {
            int i1 = e.size();
            diamondedge.ado.AdoError adoerror = null;
            for(int j1 = 0; j1 < i1; j1++)
            {
                diamondedge.ado.Recordset recordset = (diamondedge.ado.Recordset)e.elementAt(j1);
                try
                {
                    if(flag)
                        recordset.L();
                    else
                        recordset.W();
                }
                catch(diamondedge.ado.AdoError adoerror1)
                {
                    adoerror = adoerror1;
                }
            }

            if(adoerror != null)
                throw adoerror;
        }
    }

    private void _mthchar()
        throws diamondedge.ado.AdoError
    {
        if(e != null)
        {
            int i1 = e.size();
            diamondedge.ado.AdoError adoerror = null;
            for(int j1 = 0; j1 < i1; j1++)
            {
                diamondedge.ado.Recordset recordset = (diamondedge.ado.Recordset)e.elementAt(j1);
                try
                {
                    recordset.close();
                }
                catch(diamondedge.ado.AdoError adoerror1)
                {
                    adoerror = adoerror1;
                }
            }

            if(adoerror != null)
                throw adoerror;
        }
    }

    private void _mthnew(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        throw new AdoError("You must have an open connection before calling: " + s, 10009);
    }

    private void _mthint(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        throw new AdoError("You need to be in a transaction in order to call: " + s, 10017);
    }

    private void a(java.lang.String s, java.sql.SQLException sqlexception)
        throws diamondedge.ado.AdoError
    {
        throw new AdoError(sqlexception, "Error getting capabilities for this connection: " + s, 10011);
    }

    private void a(boolean flag, java.lang.String s)
    {
        if(!flag)
            _mthfor(s);
    }

    public java.lang.String toString()
    {
        return "Connection[url=" + _fldchar + ",userId=" + _fldfor + (l ? ",connected" : "") + "]";
    }

    private int d = 0;
    private int c = 0;
    private int k = 0;
    private java.lang.String _fldchar = null;
    private java.lang.String _fldfor = null;
    private java.lang.String _fldif = null;
    private static final java.lang.String _fldint = "1.0";
    private java.util.Properties _flddo = null;
    private boolean l = false;
    private java.sql.Connection _fldnew = null;
    private java.sql.Connection _fldtry = null;
    private boolean j = false;
    private int f = 0;
    private boolean _fldbyte = false;
    private static java.io.PrintWriter _fldlong = null;
    int _fldelse = 0;
    private java.lang.String _fldnull = null;
    private int i = 0;
    private java.util.Vector e = null;
    private java.util.Vector b = null;
    static final java.lang.String _fldcase = "con";
    static int _fldgoto = 0;
    java.lang.String h = null;
    java.lang.String _fldvoid = null;
    private int a = 0;
    private static diamondedge.util.HashList g = new HashList();

    static 
    {
        try
        {
            _fldlong = java.sql.DriverManager.getLogWriter();
        }
        catch(java.lang.Exception exception) { }
    }
}
