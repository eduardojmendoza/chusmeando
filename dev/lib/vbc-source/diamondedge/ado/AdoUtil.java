// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.ado;

import diamondedge.util.Variant;
import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Enumeration;
import java.util.Properties;

// Referenced classes of package diamondedge.ado:
//            AdoError, Connection, Field

public class AdoUtil
{

    public AdoUtil()
    {
    }

    public static void registerDriver(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        if(s == null || s.equals(""))
            return;
        Object obj = null;
        try
        {
            java.sql.Driver driver = (java.sql.Driver)java.lang.Class.forName(s).newInstance();
            java.sql.DriverManager.registerDriver(driver);
        }
        catch(java.lang.Exception exception)
        {
            throw new AdoError(exception, "error registering driver: " + s, 0);
        }
    }

    public static void createLogFile(java.lang.String s)
    {
        try
        {
            diamondedge.ado.Connection.setLogWriter(new PrintWriter(new FileWriter(s), true));
        }
        catch(java.lang.Exception exception)
        {
            java.lang.System.out.println("AdoUtil.createLogFile: " + exception);
        }
    }

    static void a(diamondedge.util.Variant variant, diamondedge.util.Variant variant1, java.io.InputStream inputstream)
        throws diamondedge.ado.AdoError
    {
        java.io.InputStream inputstream1 = null;
        java.lang.Object obj = variant1.toObject();
        if(obj == null)
            throw new AdoError(null, "Field value is null.", 30008);
        if(obj instanceof byte[])
            inputstream1 = diamondedge.ado.AdoUtil.a(inputstream, (byte[])obj);
        else
        if(obj instanceof java.lang.String)
            inputstream1 = diamondedge.ado.AdoUtil.a(inputstream, ((java.lang.String)obj).getBytes());
        else
            diamondedge.ado.AdoError.a("appendToData data: " + obj);
        variant.set(inputstream1);
    }

    static java.io.InputStream a(java.io.InputStream inputstream, byte abyte0[])
        throws diamondedge.ado.AdoError
    {
        Object obj = null;
        byte abyte2[] = null;
        int i = ((byte[])abyte0).length;
        boolean flag = false;
        int k = 0;
        java.io.ByteArrayInputStream bytearrayinputstream = null;
        try
        {
            if(inputstream != null)
            {
                k = inputstream.available();
                abyte2 = new byte[k];
                inputstream.reset();
                inputstream.read(abyte2, 0, k);
            }
            int j = i + k;
            byte abyte1[] = new byte[j];
            if(abyte2 != null)
                java.lang.System.arraycopy(abyte2, 0, abyte1, 0, k);
            java.lang.System.arraycopy((byte[])abyte0, 0, abyte1, k, i);
            bytearrayinputstream = new ByteArrayInputStream(abyte1);
        }
        catch(java.io.IOException ioexception)
        {
            throw new AdoError(ioexception, "Error reading from input stream.", 0);
        }
        return bytearrayinputstream;
    }

    private static java.io.ByteArrayInputStream _mthif(java.io.InputStream inputstream)
        throws java.io.IOException
    {
        java.io.ByteArrayInputStream bytearrayinputstream = null;
        byte abyte0[] = diamondedge.ado.AdoUtil.a(inputstream);
        if(abyte0 != null && abyte0.length > 0)
            bytearrayinputstream = new ByteArrayInputStream(abyte0);
        return bytearrayinputstream;
    }

    static byte[] a(java.io.InputStream inputstream)
        throws java.io.IOException
    {
        byte abyte0[] = null;
        if(inputstream != null)
        {
            byte abyte1[] = new byte[1012];
            byte abyte2[] = null;
            int i = 0;
            do
            {
                int j = inputstream.read(abyte1);
                if(j <= 0)
                    break;
                if(i > 0)
                {
                    abyte2 = new byte[i];
                    java.lang.System.arraycopy(abyte0, 0, abyte2, 0, i);
                }
                i += j;
                abyte0 = new byte[i];
                if(i > j)
                    java.lang.System.arraycopy(abyte2, 0, abyte0, 0, abyte2.length);
                java.lang.System.arraycopy(abyte1, 0, abyte0, i - j, j);
            } while(true);
        }
        return abyte0;
    }

    static void a(java.sql.ResultSet resultset, int i, int j, diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        try
        {
            switch(i)
            {
            case 1: // '\001'
            case 12: // '\f'
                java.lang.String s = resultset.getString(j);
                if(s != null)
                    variant.set(s);
                break;

            case -3: 
            case -2: 
                byte abyte0[] = resultset.getBytes(j);
                if(abyte0 != null)
                    variant.set(abyte0, 8194);
                break;

            case 4: // '\004'
                variant.set(resultset.getInt(j));
                break;

            case 5: // '\005'
                variant.set(resultset.getShort(j));
                break;

            case -6: 
                variant.set(resultset.getByte(j));
                break;

            case -5: 
                variant.set(resultset.getLong(j));
                break;

            case 6: // '\006'
            case 8: // '\b'
                variant.set(resultset.getDouble(j));
                break;

            case 7: // '\007'
                variant.set(resultset.getFloat(j));
                break;

            case 2: // '\002'
            case 3: // '\003'
                java.lang.String s1 = resultset.getString(j);
                if(s1 != null)
                {
                    java.math.BigDecimal bigdecimal = new BigDecimal(s1);
                    variant.set(bigdecimal, 10);
                }
                break;

            case -7: 
                variant.set(resultset.getBoolean(j));
                break;

            case 9: // '\t'
            case 91: // '['
                java.sql.Date date = resultset.getDate(j);
                if(date != null)
                    variant.set(date);
                break;

            case 10: // '\n'
            case 92: // '\\'
                java.sql.Time time = resultset.getTime(j);
                if(time != null)
                    variant.set(time);
                break;

            case 11: // '\013'
            case 93: // ']'
                java.sql.Timestamp timestamp = resultset.getTimestamp(j);
                if(timestamp != null)
                    variant.set(timestamp);
                break;

            case -4: 
            case 2004: 
                java.io.InputStream inputstream = resultset.getBinaryStream(j);
                if(inputstream != null)
                {
                    byte abyte1[] = diamondedge.ado.AdoUtil.a(inputstream);
                    variant.set(abyte1);
                } else
                {
                    variant.setNull();
                }
                break;

            case -1: 
            case 2005: 
                java.io.Reader reader = resultset.getCharacterStream(j);
                if(reader != null)
                {
                    char ac[] = new char[64];
                    java.lang.StringBuffer stringbuffer = new StringBuffer();
                    int k;
                    while((k = reader.read(ac)) > 0) 
                        stringbuffer.append(ac, 0, k);
                    variant.set(stringbuffer.toString());
                } else
                {
                    variant.setNull();
                }
                break;

            default:
                java.lang.String s2 = resultset.getString(j);
                if(s2 != null)
                    variant.set(s2);
                break;
            }
            if(resultset.wasNull())
                variant.setNull();
        }
        catch(java.lang.Exception exception)
        {
            throw new AdoError(exception, "error fetching values. (in AdoUtil.getColValue)", 0x10d8c);
        }
    }

    static void a(java.sql.CallableStatement callablestatement, int i, int j, diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        try
        {
            switch(i)
            {
            case -1: 
            case 1: // '\001'
            case 12: // '\f'
                java.lang.String s = callablestatement.getString(j);
                if(s != null)
                    variant.set(s);
                break;

            case -3: 
            case -2: 
                byte abyte0[] = callablestatement.getBytes(j);
                if(abyte0 != null)
                    variant.set(abyte0, 8194);
                break;

            case 4: // '\004'
                variant.set(callablestatement.getInt(j));
                break;

            case 5: // '\005'
                variant.set(callablestatement.getShort(j));
                break;

            case -6: 
                variant.set(callablestatement.getByte(j));
                break;

            case -5: 
                variant.set(callablestatement.getLong(j));
                break;

            case 6: // '\006'
            case 8: // '\b'
                variant.set(callablestatement.getDouble(j));
                break;

            case 7: // '\007'
                variant.set(callablestatement.getFloat(j));
                break;

            case 2: // '\002'
            case 3: // '\003'
                java.lang.String s1 = callablestatement.getString(j);
                if(s1 != null)
                {
                    java.math.BigDecimal bigdecimal = new BigDecimal(s1);
                    variant.set(bigdecimal, 10);
                }
                break;

            case -7: 
                variant.set(callablestatement.getBoolean(j));
                break;

            case 9: // '\t'
            case 91: // '['
                java.sql.Date date = callablestatement.getDate(j);
                if(date != null)
                    variant.set(date);
                break;

            case 10: // '\n'
            case 92: // '\\'
                java.sql.Time time = callablestatement.getTime(j);
                if(time != null)
                    variant.set(time);
                break;

            case 11: // '\013'
            case 93: // ']'
                java.sql.Timestamp timestamp = callablestatement.getTimestamp(j);
                if(timestamp != null)
                    variant.set(timestamp);
                break;

            case -4: 
                throw new AdoError(null, "Invalid datatype.", 0x11199);

            default:
                java.lang.String s2 = callablestatement.getString(j);
                if(s2 != null)
                    variant.set(s2);
                break;
            }
            if(callablestatement.wasNull())
                variant.setNull();
        }
        catch(java.lang.Exception exception)
        {
            throw new AdoError(exception, "error fetching values.", 0x10d8c);
        }
    }

    static void a(int i, diamondedge.ado.Field field, diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        if(field.isNull())
        {
            variant.setNull();
        } else
        {
            diamondedge.util.Variant variant1 = field.getValue();
            try
            {
                variant.set(variant1.toObject(), variant1.getVarType());
                int j = diamondedge.ado.AdoUtil.getAdoType(i);
                variant.changeType(j);
            }
            catch(java.lang.Exception exception)
            {
                throw new AdoError(exception, "error in AdoUtil.getColValue");
            }
        }
    }

    static void a(java.util.Properties properties, java.util.Properties properties1)
    {
        java.lang.String s;
        for(java.util.Enumeration enumeration = properties.propertyNames(); enumeration.hasMoreElements(); properties1.put(s, properties.get(s)))
            s = (java.lang.String)enumeration.nextElement();

    }

    public static java.sql.Date getDate(diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        java.sql.Date date = null;
        java.lang.Object obj = variant.toObject();
        if(obj instanceof java.sql.Date)
            date = (java.sql.Date)obj;
        else
        if(obj instanceof java.sql.Timestamp)
            date = new Date(((java.sql.Timestamp)obj).getTime());
        else
        if(obj instanceof java.util.Date)
            date = new Date(((java.util.Date)obj).getTime());
        else
        if(variant.getVarType() == 15)
        {
            java.util.Date date1 = variant.toDate();
            if(date1 != null)
                date = new Date(date1.getTime());
            else
                try
                {
                    date = java.sql.Date.valueOf(variant.toString());
                }
                catch(java.lang.Exception exception)
                {
                    throw new AdoError(exception, "Error during data type conversion.", 0);
                }
        } else
        if(variant.getVarType() != 1)
            diamondedge.ado.AdoUtil.a("DATE", variant.getVarType());
        return date;
    }

    public static java.sql.Time getTime(diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        java.sql.Time time = null;
        java.lang.Object obj = variant.toObject();
        if(obj instanceof java.sql.Time)
            time = (java.sql.Time)obj;
        else
        if(obj instanceof java.sql.Timestamp)
            time = new Time(((java.sql.Timestamp)obj).getTime());
        else
        if(obj instanceof java.util.Date)
            time = new Time(((java.util.Date)obj).getTime());
        else
        if(variant.getVarType() == 15)
        {
            java.util.Date date = variant.toDate();
            if(date != null)
                time = new Time(date.getTime());
            else
                try
                {
                    time = java.sql.Time.valueOf(variant.toString());
                }
                catch(java.lang.Exception exception)
                {
                    throw new AdoError(exception, "Error during data type conversion.", 0);
                }
        } else
        if(variant.getVarType() != 1)
            diamondedge.ado.AdoUtil.a("TIME", variant.getVarType());
        return time;
    }

    public static java.sql.Timestamp getTimestamp(diamondedge.util.Variant variant)
        throws diamondedge.ado.AdoError
    {
        java.sql.Timestamp timestamp = null;
        java.lang.Object obj = variant.toObject();
        if(obj instanceof java.sql.Timestamp)
            timestamp = (java.sql.Timestamp)obj;
        else
        if(obj instanceof java.sql.Date)
            timestamp = new Timestamp(((java.sql.Date)obj).getTime());
        else
        if(obj instanceof java.util.Date)
            timestamp = new Timestamp(((java.util.Date)obj).getTime());
        else
        if(variant.getVarType() == 15)
        {
            java.util.Date date = variant.toDate();
            if(date != null)
                timestamp = new Timestamp(date.getTime());
            else
                try
                {
                    timestamp = java.sql.Timestamp.valueOf(variant.toString());
                }
                catch(java.lang.Exception exception)
                {
                    java.lang.System.out.println("error converting: " + variant.toString());
                    throw new AdoError(exception, "Error during data type conversion.", 0);
                }
        } else
        if(variant.getVarType() != 1)
            diamondedge.ado.AdoUtil.a("TIMESTAMP", variant.getVarType());
        return timestamp;
    }

    static java.io.InputStream a(diamondedge.util.Variant variant)
        throws java.io.IOException
    {
        java.lang.Object obj = null;
        java.lang.Object obj1 = variant.toObject();
        if(obj1 == null)
            return null;
        if(obj1 instanceof java.io.ByteArrayInputStream)
        {
            obj = (java.io.InputStream)obj1;
            ((java.io.InputStream) (obj)).reset();
        } else
        if(obj1 instanceof java.io.InputStream)
            obj = diamondedge.ado.AdoUtil._mthif((java.io.InputStream)obj1);
        if(obj == null)
        {
            byte abyte0[] = variant.toBytes();
            obj = new ByteArrayInputStream(abyte0);
        }
        return ((java.io.InputStream) (obj));
    }

    public static int getAdoType(int i)
        throws diamondedge.ado.AdoError
    {
        char c = '\r';
        switch(i)
        {
        case -5: 
            c = '\006';
            break;

        case -2: 
            c = '\200';
            break;

        case -7: 
        case 14: // '\016'
            c = '\013';
            break;

        case 16: // '\020'
            c = '\013';
            break;

        case -8: 
        case 1: // '\001'
            c = '\201';
            break;

        case 3: // '\003'
            c = '\n';
            break;

        case 8: // '\b'
            c = '\b';
            break;

        case 6: // '\006'
            c = '\b';
            break;

        case 4: // '\004'
            c = '\004';
            break;

        case -4: 
        case 2004: 
            c = '\315';
            break;

        case -10: 
        case -1: 
        case 2005: 
            c = '\311';
            break;

        case 0: // '\0'
            c = '\001';
            break;

        case 2: // '\002'
            c = '\n';
            break;

        case 1111: 
            c = '\r';
            break;

        case 7: // '\007'
            c = '\007';
            break;

        case 5: // '\005'
            c = '\003';
            break;

        case 9: // '\t'
        case 91: // '['
            c = '\205';
            break;

        case 10: // '\n'
        case 92: // '\\'
            c = '\206';
            break;

        case 11: // '\013'
        case 93: // ']'
            c = '\207';
            break;

        case -6: 
            c = '\002';
            break;

        case -3: 
            c = '\314';
            break;

        case -9: 
        case 12: // '\f'
            c = '\310';
            break;

        default:
            throw new AdoError("Unknown SQL Type : " + i);
        }
        return c;
    }

    public static int getSqlType(int i)
        throws diamondedge.ado.AdoError
    {
        char c = '\u0457';
        switch(i)
        {
        case 6: // '\006'
            c = '\uFFFB';
            break;

        case 128: 
            c = '\uFFFE';
            break;

        case 11: // '\013'
            c = '\uFFF9';
            break;

        case 129: 
            c = '\001';
            break;

        case 133: 
            c = '[';
            break;

        case 10: // '\n'
            c = '\003';
            break;

        case 8: // '\b'
            c = '\b';
            break;

        case 4: // '\004'
            c = '\004';
            break;

        case 205: 
            c = '\uFFFC';
            break;

        case 201: 
            c = '\uFFFF';
            break;

        case 1: // '\001'
            c = '\0';
            break;

        case 13: // '\r'
            c = '\u0457';
            break;

        case 7: // '\007'
            c = '\007';
            break;

        case 3: // '\003'
            c = '\005';
            break;

        case 134: 
            c = '\\';
            break;

        case 135: 
            c = ']';
            break;

        case 2: // '\002'
            c = '\uFFFA';
            break;

        case 204: 
            c = '\uFFFD';
            break;

        case 15: // '\017'
        case 200: 
            c = '\f';
            break;

        default:
            throw new AdoError("Unknown ADO Type : " + i);
        }
        return c;
    }

    public static int getAdoIsolationLevel(int i)
    {
        int j = -1;
        switch(i)
        {
        case 1: // '\001'
            j = 256;
            break;

        case 2: // '\002'
            j = 4096;
            break;

        case 4: // '\004'
            j = 0x10000;
            break;

        case 8: // '\b'
            j = 0x100000;
            break;
        }
        return j;
    }

    public static int getJDBCIsolationLevel(int i)
    {
        byte byte0 = 2;
        switch(i)
        {
        case 256: 
            byte0 = 1;
            break;

        case 4096: 
            byte0 = 2;
            break;

        case 65536: 
            byte0 = 4;
            break;

        case 1048576: 
            byte0 = 8;
            break;
        }
        return byte0;
    }

    static void a(java.lang.String s, int i)
        throws diamondedge.ado.AdoError
    {
        throw new AdoError(null, "Incompatible data types.  The value is of type: " + diamondedge.util.Variant.typeName(i) + " " + "It cannot be converted to type:  " + s, 30009);
    }
}
