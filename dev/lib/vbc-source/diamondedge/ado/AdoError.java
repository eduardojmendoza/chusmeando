// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.ado;

import diamondedge.util.RuntimeError;
import java.sql.SQLException;

public class AdoError extends diamondedge.util.RuntimeError
{

    public AdoError()
    {
        _fldnew = null;
        _fldint = null;
        a = "ADO";
    }

    public AdoError(java.lang.Exception exception, java.lang.String s, int i)
    {
        super(i, "ADO", s);
        _fldnew = null;
        _fldint = null;
        a(exception);
    }

    public AdoError(java.lang.String s)
    {
        super(0, "ADO", s);
        _fldnew = null;
        _fldint = null;
    }

    public AdoError(java.lang.Exception exception, java.lang.String s)
    {
        super(0, "ADO", s);
        _fldnew = null;
        _fldint = null;
        a(exception);
    }

    public AdoError(java.lang.String s, int i)
    {
        super(i, "ADO", s);
        _fldnew = null;
        _fldint = null;
    }

    public java.lang.String getMessage()
    {
        if(_fldint != null)
            return _fldint;
        else
            return super.getMessage();
    }

    public java.lang.String getSQLState()
    {
        return _fldnew;
    }

    public int getNativeError()
    {
        if(_fldif instanceof java.sql.SQLException)
            return ((java.sql.SQLException)_fldif).getErrorCode();
        else
            return 0;
    }

    public java.lang.Throwable getNativeException()
    {
        return _fldif;
    }

    public java.lang.String toString()
    {
        java.lang.String s = super.toString();
        if(_fldnew != null)
            s = s + "\r\n" +
" SQLState: "
 + _fldnew;
        return s;
    }

    public void setErrorMsg(java.lang.String s)
    {
        _fldint = s;
    }

    public void setErrorNumber(int i)
    {
        _fldfor = i;
    }

    void a(java.lang.Exception exception)
    {
        if(exception instanceof java.sql.SQLException)
        {
            a = "SQL";
            java.sql.SQLException sqlexception = (java.sql.SQLException)exception;
            _fldfor = sqlexception.getErrorCode();
            _fldnew = sqlexception.getSQLState();
        }
        if(_fldint == null || _fldint.length() == 0)
            _fldint = exception.getMessage();
        else
            _fldint += "\r\n" + exception.getMessage();
        _fldif = exception;
    }

    static void _mthif(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        throw new AdoError("You need to have an open cursor in order to call: " + s);
    }

    static void _mthdo(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        throw new AdoError("You need to have an open cursor in order to call: " + s);
    }

    static void a(java.lang.String s)
        throws diamondedge.ado.AdoError
    {
        throw new AdoError("Feature Not Supported: " + s);
    }

    private java.lang.String _fldnew = null;
    private java.lang.String _fldint = null;
}
