// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.text.DecimalFormat;
import java.text.ParseException;

public class PositiveNumberFormat extends java.text.DecimalFormat
{

    public PositiveNumberFormat()
    {
    }

    public PositiveNumberFormat(java.lang.String s)
    {
        super(s);
    }

    public java.lang.Object parseObject(java.lang.String s)
        throws java.text.ParseException
    {
        java.lang.Object obj = super.parseObject(s);
        if((obj instanceof java.lang.Number) && ((java.lang.Number)obj).doubleValue() < 0.0D)
        {
            int i = s.indexOf('-');
            if(i < 0)
                i = 0;
            throw new ParseException("Must be a positive number: " + s, i);
        } else
        {
            return obj;
        }
    }
}
