// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.util.Enumeration;

public class StringSplitter
    implements java.util.Enumeration
{

    public StringSplitter(java.lang.String s, java.lang.String s1)
    {
        _fldint = 0;
        _fldfor = -1;
        _flddo = false;
        _fldnew = s;
        _fldtry = s.length();
        _fldif = s1;
        a = s1.length();
    }

    public StringSplitter(java.lang.String s)
    {
        this(s, " ");
    }

    private int a(int i)
    {
        if(_fldif == null)
            throw new NullPointerException();
        int j = i;
        int k;
        for(k = 0; k < a && j + k < _fldtry && _fldnew.charAt(j + k) == _fldif.charAt(k); k++);
        if(k == a)
            j += a;
        return j;
    }

    private int _mthif(int i)
    {
        int j = i;
        char c = a <= 0 ? '\0' : _fldif.charAt(0);
        for(; j < _fldtry; j++)
        {
            char c1 = _fldnew.charAt(j);
            if(c1 != c)
                continue;
            boolean flag = false;
            int k = 1;
            do
            {
                if(k >= a || j + k >= _fldtry)
                    break;
                if(_fldnew.charAt(j + k) != _fldif.charAt(k))
                {
                    flag = true;
                    break;
                }
                k++;
            } while(true);
            if(!flag)
                break;
        }

        return j;
    }

    public boolean hasMoreTokens()
    {
        _fldfor = a(_fldint);
        return _fldfor < _fldtry || _fldfor > _fldint;
    }

    public java.lang.String nextToken()
    {
        _fldint = _fldfor < 0 || _flddo ? a(_fldint) : _fldfor;
        _flddo = false;
        _fldfor = -1;
        if(_fldint >= _fldtry)
        {
            return "";
        } else
        {
            int i = _fldint;
            _fldint = _mthif(_fldint);
            return _fldnew.substring(i, _fldint);
        }
    }

    public java.lang.String nextToken(java.lang.String s)
    {
        _fldif = s;
        a = s.length();
        _flddo = true;
        return nextToken();
    }

    public boolean hasMoreElements()
    {
        return hasMoreTokens();
    }

    public java.lang.Object nextElement()
    {
        return nextToken();
    }

    public int countTokens()
    {
        int i = 0;
        int j = _fldint;
        do
        {
            if(j >= _fldtry)
                break;
            j = a(j);
            if(j >= _fldtry)
                break;
            j = _mthif(j);
            i++;
        } while(true);
        return i;
    }

    private int _fldint = 0;
    private int _fldfor = 0;
    private int _fldtry = 0;
    private java.lang.String _fldnew = null;
    private java.lang.String _fldif = null;
    private boolean _flddo = false;
    private int a = 0;
}
