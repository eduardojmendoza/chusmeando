// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Date;
import java.util.Vector;

// Referenced classes of package diamondedge.util:
//            InputFile, OutputFile, AppendFile

public class FileSystem
{

    public FileSystem()
    {
    }

    public static int getFreeFile()
    {
        int i = a.size();
        for(int j = 0; j < i; j++)
            if(a.elementAt(j) == null)
            {
                a.setElementAt(_fldif, j);
                return j;
            }

        a.addElement(_fldif);
        return a.size() - 1;
    }

    public static diamondedge.util.InputFile in(int i)
    {
        return (diamondedge.util.InputFile)a.elementAt(i);
    }

    public static diamondedge.util.OutputFile out(int i)
    {
        return (diamondedge.util.OutputFile)a.elementAt(i);
    }

    public static diamondedge.util.InputFile openInput(java.lang.String s, int i)
        throws java.io.IOException
    {
        diamondedge.util.InputFile inputfile = new InputFile(s);
        if(i >= a.size())
            a.setSize(i + 1);
        a.setElementAt(inputfile, i);
        return inputfile;
    }

    public static diamondedge.util.OutputFile openOutput(java.lang.String s, int i)
        throws java.io.IOException
    {
        diamondedge.util.OutputFile outputfile = new OutputFile(s);
        if(i >= a.size())
            a.setSize(i + 1);
        a.setElementAt(outputfile, i);
        return outputfile;
    }

    public static diamondedge.util.AppendFile openAppend(java.lang.String s, int i)
        throws java.io.IOException
    {
        diamondedge.util.AppendFile appendfile = new AppendFile(s);
        if(i >= a.size())
            a.setSize(i + 1);
        a.setElementAt(appendfile, i);
        return appendfile;
    }

    public static void close()
        throws java.io.IOException
    {
        int i = a.size();
        for(int j = 0; j < i; j++)
            diamondedge.util.FileSystem.close(j);

    }

    public static void close(int i)
        throws java.io.IOException
    {
        java.lang.Object obj = a.elementAt(i);
        if(obj == null)
            throw new FileNotFoundException("File Number not found:" + i);
        if(obj instanceof diamondedge.util.InputFile)
            ((diamondedge.util.InputFile)obj).close();
        else
        if(obj instanceof diamondedge.util.OutputFile)
            ((diamondedge.util.OutputFile)obj).close();
        a.setElementAt(null, i);
    }

    public static boolean isEOF(int i)
        throws java.io.IOException
    {
        java.lang.Object obj = a.elementAt(i);
        if(obj == null)
            throw new FileNotFoundException("File Number not found:" + i);
        if(obj instanceof diamondedge.util.InputFile)
        {
            diamondedge.util.InputFile inputfile = (diamondedge.util.InputFile)obj;
            if(inputfile.getFilePointer() >= inputfile.length())
                return true;
        }
        return false;
    }

    public static java.util.Date getFileDateTime(java.lang.String s)
        throws java.io.IOException
    {
        java.io.File file = new File(s);
        if(file.exists())
            return new Date(file.lastModified());
        else
            throw new FileNotFoundException("File not found:" + s);
    }

    public static int getFileLen(java.lang.String s)
        throws java.io.IOException
    {
        java.io.File file = new File(s);
        if(file.exists())
            return (int)file.length();
        else
            throw new FileNotFoundException("File not found:" + s);
    }

    public static void kill(java.lang.String s)
        throws java.io.IOException
    {
        java.io.File file = new File(s);
        if(file.exists() && !file.isDirectory())
            file.delete();
        else
            throw new FileNotFoundException("File not found:" + s);
    }

    public static int getFileLoc(int i)
        throws java.io.IOException
    {
        java.lang.Object obj = a.elementAt(i);
        if(obj instanceof diamondedge.util.InputFile)
            return (int)((diamondedge.util.InputFile)obj).getFilePointer();
        else
            throw new IOException("Loc not implemented for output files");
    }

    public static int getFileLen(int i)
        throws java.io.IOException
    {
        java.lang.Object obj = a.elementAt(i);
        if(obj == null)
            throw new FileNotFoundException("File Number not found:" + i);
        if(obj instanceof diamondedge.util.InputFile)
            return (int)((diamondedge.util.InputFile)obj).length();
        if(obj instanceof diamondedge.util.OutputFile)
            return ((diamondedge.util.OutputFile)obj).length();
        else
            return 0;
    }

    public static java.io.File mkDir(java.lang.String s)
        throws java.io.IOException
    {
        java.io.File file = new File(s);
        if(!file.mkdir())
            throw new IOException("Path/File access error:" + s);
        else
            return file;
    }

    public static void rmDir(java.lang.String s)
        throws java.io.IOException
    {
        java.io.File file = new File(s);
        if(file.isDirectory())
            file.delete();
        else
            throw new FileNotFoundException("Directory not found:" + s);
    }

    public static int seek(int i)
        throws java.io.IOException
    {
        java.lang.Object obj = a.elementAt(i);
        if(obj instanceof diamondedge.util.InputFile)
            return (int)((diamondedge.util.InputFile)obj).getFilePointer() + 1;
        else
            throw new FileNotFoundException("File Number not found:" + i);
    }

    public static void seek(int i, int j)
        throws java.io.IOException
    {
        java.lang.Object obj = a.elementAt(i);
        if(obj instanceof diamondedge.util.InputFile)
        {
            ((diamondedge.util.InputFile)obj).seek(j - 1);
            return;
        } else
        {
            throw new FileNotFoundException("File Number not found:" + i);
        }
    }

    public static java.lang.String getDriveName(java.lang.String s)
    {
        if(s != null)
        {
            int i = s.indexOf(":");
            if(i >= 0)
                return s.substring(0, i + 1);
        }
        return "";
    }

    public static java.lang.String getBaseName(java.lang.String s)
    {
        if(s != null)
        {
            java.lang.String s1 = (new File(s)).getName();
            int i = s1.indexOf(".");
            if(i >= 0)
                return s1.substring(0, i);
        }
        return "";
    }

    public static java.lang.String getExtensionName(java.lang.String s)
    {
        if(s != null)
        {
            java.lang.String s1 = (new File(s)).getName();
            int i = s1.indexOf(".");
            if(i >= 0)
                return s1.substring(i + 1, s1.length());
        }
        return "";
    }

    public static int getAttr(java.io.File file)
        throws java.io.IOException
    {
        if(file == null)
            throw new FileNotFoundException("File not found");
        if(!file.exists())
            throw new FileNotFoundException("File not found:" + file.getPath());
        int i = 0;
        if(file.isDirectory())
            i += 16;
        if(file.isHidden())
            i += 2;
        if(!file.canWrite())
            i++;
        return i;
    }

    public static int getAttr(java.lang.String s)
        throws java.io.IOException
    {
        return diamondedge.util.FileSystem.getAttr(new File(s));
    }

    public static java.lang.String readString(java.io.RandomAccessFile randomaccessfile, int i)
        throws java.io.IOException
    {
        byte abyte0[] = new byte[i];
        randomaccessfile.read(abyte0, 0, i);
        return new String(abyte0);
    }

    public static java.io.RandomAccessFile openFile(java.lang.String s, java.lang.String s1)
        throws java.io.IOException
    {
        return new RandomAccessFile(s, s1);
    }

    public static java.io.RandomAccessFile openFile(java.lang.String s, int i)
        throws java.io.IOException
    {
        java.lang.String s1;
        if(i == 1)
            s1 = "r";
        else
            s1 = "rw";
        java.io.RandomAccessFile randomaccessfile = diamondedge.util.FileSystem.openFile(s, s1);
        if(i == 8)
            randomaccessfile.seek(randomaccessfile.length());
        return randomaccessfile;
    }

    private static java.util.Vector a = null;
    private static java.lang.Integer _fldif = null;

    static 
    {
        _fldif = new Integer(0);
        a = new Vector();
        a.addElement(_fldif);
    }
}
