// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormatSymbols;
import java.util.Locale;

// Referenced classes of package diamondedge.util:
//            HashList, Variant

public class ListFormat
{

    public ListFormat()
    {
        a = new HashList();
        _fldif = 0;
    }

    public int size()
    {
        return a.size();
    }

    private diamondedge.util.Variant a(java.lang.Object obj)
    {
        diamondedge.util.Variant variant = new Variant(obj);
        if(_fldif != 0)
            try
            {
                variant.changeType(_fldif);
            }
            catch(java.lang.Exception exception) { }
        return variant;
    }

    public void setDataValueType(java.lang.Class class1)
    {
        if(class1 == null)
            _fldif = 0;
        else
            _fldif = diamondedge.util.Variant.getVarType(class1);
    }

    public int getIndex(java.lang.Object obj)
    {
        return a.getIndex(obj);
    }

    public java.lang.Object get(int i)
    {
        return a.get(i);
    }

    public java.lang.Object get(java.lang.Object obj)
    {
        if(_fldif != 0)
        {
            diamondedge.util.Variant variant = new Variant(obj);
            try
            {
                variant.changeType(_fldif);
            }
            catch(java.lang.Exception exception) { }
            if(obj == null && variant.isNumeric())
                return null;
            obj = variant.toObject();
        }
        return a.get(obj);
    }

    public java.lang.Object getDataValue(int i)
    {
        java.lang.Object obj = a.getKey(i);
        if(obj instanceof diamondedge.util.Variant)
            return ((diamondedge.util.Variant)obj).toObject();
        else
            return obj;
    }

    public java.lang.Object set(int i, java.lang.Object obj)
    {
        return a.set(i, obj);
    }

    public java.lang.Object set(java.lang.Object obj, java.lang.Object obj1)
    {
        return a.set(a(obj), obj1);
    }

    public void add(java.lang.Object obj, java.lang.Object obj1)
    {
        a.add(obj1, a(obj));
    }

    public void add(int i, java.lang.Object obj, java.lang.Object obj1)
    {
        a.add(obj1, a(obj), i);
    }

    public void add(java.sql.ResultSet resultset)
        throws java.sql.SQLException
    {
        for(; resultset.next(); add(resultset.getObject(1), resultset.getObject(2)));
    }

    public void add(java.sql.Connection connection, java.lang.String s)
        throws java.sql.SQLException
    {
        java.sql.ResultSet resultset = connection.createStatement().executeQuery(s);
        add(resultset);
    }

    public java.lang.Object remove(int i)
    {
        return a.remove(i);
    }

    public java.lang.Object removeDataValue(java.lang.Object obj)
    {
        return a.removeKey(obj);
    }

    public boolean remove(java.lang.Object obj)
    {
        return a.remove(obj);
    }

    public void clear()
    {
        a.clear();
    }

    public boolean containsDataValue(java.lang.Object obj)
    {
        return a.containsKey(obj);
    }

    public boolean containsDisplayValue(java.lang.Object obj)
    {
        return a.containsValue(obj);
    }

    public static diamondedge.util.ListFormat getMonthNames(java.util.Locale locale)
    {
        diamondedge.util.ListFormat listformat = new ListFormat();
        java.text.DateFormatSymbols dateformatsymbols = locale != null ? new DateFormatSymbols(locale) : new DateFormatSymbols();
        java.lang.String as[] = dateformatsymbols.getMonths();
        for(int i = 0; i < as.length; i++)
            listformat.add(new Integer(i), as[i]);

        return listformat;
    }

    public static diamondedge.util.ListFormat getWeekdayNames(java.util.Locale locale)
    {
        diamondedge.util.ListFormat listformat = new ListFormat();
        java.text.DateFormatSymbols dateformatsymbols = locale != null ? new DateFormatSymbols(locale) : new DateFormatSymbols();
        java.lang.String as[] = dateformatsymbols.getWeekdays();
        for(int i = 0; i < as.length; i++)
            listformat.add(new Integer(i), as[i]);

        return listformat;
    }

    private diamondedge.util.HashList a = null;
    private int _fldif = 0;
}
