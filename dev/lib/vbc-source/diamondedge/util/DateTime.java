// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

// Referenced classes of package diamondedge.util:
//            Variant

public class DateTime
{

    public DateTime()
    {
    }

    public static final java.util.Calendar getCalendar(java.util.Date date)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        if(date != null)
            calendar.setTime(date);
        return calendar;
    }

    public static final int compare(java.util.Date date, java.util.Date date1)
    {
        long l1 = date.getTime();
        long l2 = date1.getTime();
        if(l1 == l2)
            return 0;
        return l1 >= l2 ? 1 : -1;
    }

    public static final int toInt(java.util.Date date)
    {
        if(date == null)
            return 0;
        else
            return (int)java.lang.Math.round(diamondedge.util.DateTime.toDouble(date));
    }

    public static final double toDouble(java.util.Date date)
    {
        if(date == null)
            return 0.0D;
        else
            return (double)(date.getTime() + (long)o) / 86400000D + 25569D;
    }

    public static final java.util.Date toDate(double d1)
    {
        return new Date(java.lang.Math.round((d1 - 25569D) * 86400000D - (double)o));
    }

    public static final java.util.Date toDate(java.lang.String s1)
        throws java.text.ParseException
    {
        if(s1 == null || s1.length() == 0)
            return null;
        java.text.DateFormat dateformat = _flddo;
        java.text.ParsePosition parseposition = new ParsePosition(0);
        int i1 = s1.length();
        for(int j1 = 0; j1 < 38; j1++)
        {
            switch(j1)
            {
            case 0: // '\0'
                dateformat = _fldint;
                break;

            case 1: // '\001'
                dateformat = _flddo;
                break;

            case 2: // '\002'
                dateformat = z;
                break;

            case 3: // '\003'
                dateformat = n;
                break;

            case 4: // '\004'
                dateformat = m;
                break;

            case 5: // '\005'
                dateformat = l;
                break;

            case 6: // '\006'
                dateformat = _fldnull;
                break;

            case 7: // '\007'
                dateformat = _fldlong;
                break;

            case 8: // '\b'
                dateformat = d;
                break;

            case 9: // '\t'
                dateformat = c;
                break;

            case 10: // '\n'
                dateformat = b;
                break;

            case 11: // '\013'
                dateformat = j;
                break;

            case 12: // '\f'
                dateformat = h;
                break;

            case 13: // '\r'
                dateformat = _fldchar;
                break;

            case 14: // '\016'
                dateformat = _fldcase;
                break;

            case 15: // '\017'
                dateformat = _fldbyte;
                break;

            case 16: // '\020'
                dateformat = C;
                break;

            case 17: // '\021'
                dateformat = B;
                break;

            case 18: // '\022'
                dateformat = A;
                break;

            case 19: // '\023'
                dateformat = _fldgoto;
                break;

            case 20: // '\024'
                dateformat = _fldelse;
                break;

            case 21: // '\025'
                dateformat = s;
                break;

            case 22: // '\026'
                dateformat = q;
                break;

            case 23: // '\027'
                dateformat = _fldif;
                break;

            case 24: // '\030'
                dateformat = a;
                break;

            case 25: // '\031'
                dateformat = _fldvoid;
                break;

            case 26: // '\032'
                dateformat = _fldtry;
                break;

            case 27: // '\033'
                dateformat = y;
                break;

            case 28: // '\034'
                dateformat = x;
                break;

            case 29: // '\035'
                dateformat = v;
                break;

            case 30: // '\036'
                dateformat = u;
                break;

            case 31: // '\037'
                dateformat = t;
                break;

            case 32: // ' '
                dateformat = p;
                break;

            case 33: // '!'
                dateformat = k;
                break;

            case 34: // '"'
                dateformat = i;
                break;

            case 35: // '#'
                dateformat = g;
                break;

            case 36: // '$'
                dateformat = f;
                break;

            case 37: // '%'
                dateformat = e;
                break;
            }
            parseposition.setIndex(0);
            java.util.Date date = dateformat.parse(s1, parseposition);
            if(date != null && parseposition.getIndex() >= i1)
                return date;
        }

        return diamondedge.util.DateTime.toDate(java.lang.Double.parseDouble(s1));
        java.lang.Exception exception;
        exception;
        throw new ParseException("Unparseable date: " + s1, 0);
    }

    public static final java.util.Date toDate2(java.lang.String s1)
    {
        return diamondedge.util.DateTime.toDate(s1);
        java.lang.Exception exception;
        exception;
        return null;
    }

    private static final boolean _mthif(java.util.Date date)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        if(date != null)
            calendar.setTime(date);
        int i1 = calendar.get(11);
        int j1 = calendar.get(12);
        int k1 = calendar.get(13);
        return i1 != 0 || j1 != 0 || k1 != 0;
    }

    private static final boolean a(java.util.Date date)
    {
        double d1 = diamondedge.util.DateTime.toDouble(date);
        return (int)d1 != 0;
    }

    public static java.util.Date now()
    {
        return new Date();
    }

    private static int a(java.lang.String s1)
    {
        s1 = s1.toLowerCase();
        byte byte0 = 1;
        if(s1.equals("yyyy"))
            byte0 = 1;
        else
        if(s1.equals("q"))
            byte0 = 2;
        else
        if(s1.equals("m"))
            byte0 = 2;
        else
        if(s1.equals("y"))
            byte0 = 6;
        else
        if(s1.equals("d"))
            byte0 = 5;
        else
        if(s1.equals("w"))
            byte0 = 7;
        else
        if(s1.equals("ww"))
            byte0 = 3;
        else
        if(s1.equals("h"))
            byte0 = 11;
        else
        if(s1.equals("n"))
            byte0 = 12;
        else
        if(s1.equals("s"))
            byte0 = 13;
        return byte0;
    }

    public static java.util.Date add(java.lang.String s1, int i1, java.util.Date date)
    {
        int j1 = diamondedge.util.DateTime.a(s1);
        java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
        calendar.add(j1, i1);
        if(j1 == 2 && s1.equals("q"))
            calendar.add(j1, i1 * 2);
        return calendar.getTime();
    }

    public static java.util.Date add(int i1, int j1, java.util.Date date)
    {
        java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
        calendar.add(i1, j1);
        return calendar.getTime();
    }

    public static java.util.Date add(java.util.Date date, java.util.Date date1)
    {
        return diamondedge.util.DateTime.add(date, diamondedge.util.DateTime.toDouble(date1));
    }

    public static java.util.Date add(java.util.Date date, double d1)
    {
        java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
        long l1 = java.lang.Math.round(java.lang.Math.floor(d1));
        double d2 = d1 - (double)l1;
        calendar.add(5, (int)l1);
        calendar.add(14, (int)java.lang.Math.round(d2 * 86400000D));
        return calendar.getTime();
    }

    public static java.util.Date subtract(java.util.Date date, double d1)
    {
        return diamondedge.util.DateTime.add(date, -d1);
    }

    public static int diff(java.lang.String s1, java.util.Date date, java.util.Date date1)
    {
        double d1 = diamondedge.util.DateTime.toDouble(date);
        double d2 = diamondedge.util.DateTime.toDouble(date1);
        int i1 = 0;
        if(s1.equals("yyyy"))
            i1 = diamondedge.util.DateTime.year(date1) - diamondedge.util.DateTime.year(date);
        else
        if(s1.equals("q"))
            i1 = (int)((d2 / 365D) * 4D + 9.9999999999999995E-07D) - (int)((d1 / 365D) * 4D + 9.9999999999999995E-07D);
        else
        if(s1.equals("m"))
            i1 = (int)((d2 / 365D) * 12D + 9.9999999999999995E-07D) - (int)((d1 / 365D) * 12D + 9.9999999999999995E-07D);
        else
        if(s1.equals("y") || s1.equals("d") || s1.equals("w"))
            i1 = (int)(d2 + 9.9999999999999995E-07D) - (int)(d1 + 9.9999999999999995E-07D);
        else
        if(s1.equals("ww"))
            i1 = (int)((d2 * 52D) / 365D + 9.9999999999999995E-07D) - (int)((d1 * 52D) / 365D + 9.9999999999999995E-07D);
        else
        if(s1.equals("h"))
            i1 = (int)(d2 * 24D + 9.9999999999999995E-07D) - (int)(d1 * 24D + 9.9999999999999995E-07D);
        else
        if(s1.equals("n"))
            i1 = (int)(d2 * 24D * 60D + 9.9999999999999995E-07D) - (int)(d1 * 24D * 60D + 9.9999999999999995E-07D);
        else
        if(s1.equals("s"))
            i1 = (int)((long)(d2 * 24D * 60D * 60D) - (long)(d1 * 24D * 60D * 60D));
        return i1;
    }

    public static int diff2(java.util.Date date, java.util.Date date1, java.lang.String s1)
    {
        double d1 = diamondedge.util.DateTime.toDouble(date);
        double d2 = diamondedge.util.DateTime.toDouble(date1);
        int i1 = 0;
        if(s1.equalsIgnoreCase("y"))
            i1 = (int)((d2 - d1) / 365D + 9.9999999999999995E-07D);
        else
        if(s1.equalsIgnoreCase("m"))
            i1 = (int)(((d2 - d1) / 365D) * 12D + 9.9999999999999995E-07D);
        else
        if(s1.equalsIgnoreCase("d"))
            i1 = (int)(d2 + 9.9999999999999995E-07D) - (int)(d1 + 9.9999999999999995E-07D);
        else
        if(s1.equalsIgnoreCase("md"))
            i1 = diamondedge.util.DateTime.day(date1) - diamondedge.util.DateTime.day(date);
        else
        if(s1.equalsIgnoreCase("ym"))
        {
            int j1 = diamondedge.util.DateTime.month(date);
            int l1 = diamondedge.util.DateTime.month(date1);
            if(j1 > l1)
                l1 += 12;
            i1 = l1 - j1;
        } else
        if(s1.equalsIgnoreCase("yd"))
        {
            int k1 = diamondedge.util.DateTime.dayOfYear(date1);
            java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
            calendar.set(1, diamondedge.util.DateTime.year(date1));
            int i2 = calendar.get(6);
            if(i2 > k1)
                k1 += 365;
            i1 = k1 - i2;
        }
        return i1;
    }

    public static double diff(java.util.Date date, java.util.Date date1)
    {
        double d1 = diamondedge.util.DateTime.toDouble(date);
        double d2 = diamondedge.util.DateTime.toDouble(date1);
        return d2 - d1;
    }

    public static int year(java.util.Date date)
    {
        java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
        return calendar.get(1);
    }

    public static int month(java.util.Date date)
    {
        java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
        return calendar.get(2) + 1;
    }

    public static int day(java.util.Date date)
    {
        java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
        return calendar.get(5);
    }

    public static int dayOfYear(java.util.Date date)
    {
        java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
        return calendar.get(6);
    }

    public static int weekday(java.util.Date date, int i1)
    {
        java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
        int j1 = calendar.get(7);
        if(i1 != 0)
        {
            j1 -= i1 - 1;
            if(j1 < 1)
                j1 += 7;
        }
        return j1;
    }

    public static int weekday2(java.util.Date date, int i1)
    {
        java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
        int j1 = calendar.get(7);
        if(i1 >= 2)
        {
            if(--j1 < 1)
                j1 = 7;
            if(i1 == 3)
                j1--;
        }
        return j1;
    }

    public static int weekOfYear(java.util.Date date, int i1)
    {
        java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
        if(i1 == 1)
            calendar.setFirstDayOfWeek(1);
        else
        if(i1 == 2)
            calendar.setFirstDayOfWeek(2);
        return calendar.get(3);
    }

    public static int hour(java.util.Date date)
    {
        java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
        return calendar.get(11);
    }

    public static int minute(java.util.Date date)
    {
        java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
        return calendar.get(12);
    }

    public static int second(java.util.Date date)
    {
        java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
        return calendar.get(13);
    }

    public static float timer()
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        return (float)(calendar.get(11) * 3600 + calendar.get(12) * 60 + calendar.get(13)) + (float)calendar.get(14) / 1000F;
    }

    public static java.util.Date dateSerial(int i1, int j1, int k1)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.set(i1, j1 - 1, k1, 0, 0, 0);
        return calendar.getTime();
    }

    public static java.util.Date timeSerial(int i1, int j1, int k1)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.set(1899, 11, 30, i1, j1, k1);
        return calendar.getTime();
    }

    public static int datePart(java.lang.String s1, java.util.Date date, int i1, int j1)
    {
        int k1 = diamondedge.util.DateTime.a(s1);
        java.util.Calendar calendar = diamondedge.util.DateTime.getCalendar(date);
        if(i1 != 0)
            calendar.setFirstDayOfWeek(i1);
        int l1 = calendar.get(k1);
        if(k1 == 2 && s1.equals("q"))
            l1 /= 4;
        return l1;
    }

    public static int datePart(java.lang.String s1, java.util.Date date)
    {
        return diamondedge.util.DateTime.datePart(s1, date, 0, 0);
    }

    public static java.lang.String formatDateTime(java.util.Date date)
    {
        return diamondedge.util.DateTime.format(date);
    }

    public static java.lang.String format(java.util.Date date)
    {
        if(date == null)
            return "";
        boolean flag = diamondedge.util.DateTime.a(date);
        boolean flag1 = diamondedge.util.DateTime._mthif(date);
        java.text.DateFormat dateformat;
        if(flag && flag1)
            dateformat = r;
        else
        if(flag1)
            dateformat = _fldfor;
        else
            dateformat = w;
        return dateformat.format(date);
    }

    public static void setDefaultDateFormat(java.text.DateFormat dateformat)
    {
        w = dateformat;
    }

    public static void setDefaultTimeFormat(java.text.DateFormat dateformat)
    {
        _fldfor = dateformat;
    }

    public static void setDefaultDateTimeFormat(java.text.DateFormat dateformat)
    {
        r = dateformat;
    }

    public static java.lang.String weekdayName(int i1, boolean flag, int j1)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        if(j1 != 0)
            calendar.setFirstDayOfWeek(j1);
        calendar.set(7, i1);
        java.text.SimpleDateFormat simpledateformat;
        if(flag)
            simpledateformat = new SimpleDateFormat("E");
        else
            simpledateformat = new SimpleDateFormat("EEEE");
        return simpledateformat.format(calendar.getTime());
    }

    public static java.lang.String monthName(int i1, boolean flag)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.set(1970, i1 - 1, 1, 0, 0, 0);
        java.text.SimpleDateFormat simpledateformat;
        if(flag)
            simpledateformat = new SimpleDateFormat("MMM");
        else
            simpledateformat = new SimpleDateFormat("MMMM");
        return simpledateformat.format(calendar.getTime());
    }

    public static java.lang.String format(java.util.Date date, java.lang.String s1, int i1)
    {
        if(s1 == null)
            return date.toString();
        if(date == null)
            return "";
        java.lang.String s2 = s1.toUpperCase();
        if(s2.equals("GENERAL DATE"))
            return diamondedge.util.DateTime.format(date);
        java.lang.Object obj;
        if(s2.equals("LONG DATE"))
            obj = l;
        else
        if(s2.equals("MEDIUM DATE"))
            obj = new SimpleDateFormat("dd-MMM-yy");
        else
        if(s2.equals("SHORT DATE"))
            obj = n;
        else
        if(s2.equals("LONG TIME"))
            obj = _fldlong;
        else
        if(s2.equals("MEDIUM TIME"))
            obj = new SimpleDateFormat("hh:mm a");
        else
        if(s2.equals("SHORT TIME"))
            obj = new SimpleDateFormat("HH:mm");
        else
            obj = new SimpleDateFormat(s1);
        return ((java.text.DateFormat) (obj)).format(date);
    }

    public static java.lang.String format(java.util.Date date, java.lang.String s1)
    {
        return diamondedge.util.DateTime.format(date, s1, 0);
    }

    public static java.lang.String format(java.lang.String s1, java.lang.String s2)
    {
        return diamondedge.util.DateTime.format(diamondedge.util.DateTime.toDate(s1), s2, 0);
        java.lang.Exception exception;
        exception;
        return s1;
    }

    public static java.lang.String format(diamondedge.util.Variant variant, java.lang.String s1)
    {
        if(variant.isNull())
            return "";
        else
            return diamondedge.util.DateTime.format(variant.toDate(), s1, 0);
    }

    public static final int USE_SYSTEM_DAY_OF_WEEK = 0;
    private static final double _fldnew = 86400000D;
    private static java.text.DateFormat _fldint = java.text.DateFormat.getDateTimeInstance(3, 3);
    private static java.text.DateFormat _flddo = java.text.DateFormat.getDateTimeInstance(3, 2);
    private static java.text.DateFormat _fldif = java.text.DateFormat.getDateTimeInstance(3, 1);
    private static java.text.DateFormat a = java.text.DateFormat.getDateTimeInstance(3, 0);
    private static java.text.DateFormat d = java.text.DateFormat.getDateTimeInstance(2, 3);
    private static java.text.DateFormat c = java.text.DateFormat.getDateTimeInstance(2, 2);
    private static java.text.DateFormat b = java.text.DateFormat.getDateTimeInstance(2, 1);
    private static java.text.DateFormat _fldvoid = java.text.DateFormat.getDateTimeInstance(2, 1);
    private static java.text.DateFormat _fldchar = java.text.DateFormat.getDateTimeInstance(1, 3);
    private static java.text.DateFormat _fldcase = java.text.DateFormat.getDateTimeInstance(1, 2);
    private static java.text.DateFormat _fldbyte = java.text.DateFormat.getDateTimeInstance(1, 1);
    private static java.text.DateFormat _fldtry = java.text.DateFormat.getDateTimeInstance(1, 0);
    private static java.text.DateFormat C = java.text.DateFormat.getDateTimeInstance(0, 3);
    private static java.text.DateFormat B = java.text.DateFormat.getDateTimeInstance(0, 2);
    private static java.text.DateFormat A = java.text.DateFormat.getDateTimeInstance(0, 1);
    private static java.text.DateFormat y = java.text.DateFormat.getDateTimeInstance(0, 0);
    private static java.text.DateFormat n = java.text.DateFormat.getDateInstance(3);
    private static java.text.DateFormat m = java.text.DateFormat.getDateInstance(2);
    private static java.text.DateFormat l = java.text.DateFormat.getDateInstance(1);
    private static java.text.DateFormat k = java.text.DateFormat.getDateInstance(0);
    private static java.text.DateFormat _fldnull = java.text.DateFormat.getTimeInstance(3);
    private static java.text.DateFormat _fldlong = java.text.DateFormat.getTimeInstance(2);
    private static java.text.DateFormat _fldgoto = java.text.DateFormat.getTimeInstance(1);
    private static java.text.DateFormat _fldelse = java.text.DateFormat.getTimeInstance(0);
    private static java.text.DateFormat j = null;
    private static java.text.DateFormat h = null;
    private static java.text.DateFormat s = new SimpleDateFormat("yyyy-M-d H:mm:ss");
    private static java.text.DateFormat q = new SimpleDateFormat("yyyy-M-d H:mm:ss.S");
    private static java.text.DateFormat z = new SimpleDateFormat("M/d/yy H:mm");
    private static java.text.DateFormat x = new SimpleDateFormat("M/d/yy H:mm:ss");
    private static java.text.DateFormat v = new SimpleDateFormat("d-MMM-yy H:mm");
    private static java.text.DateFormat u = new SimpleDateFormat("d-MMM-yy h:mm a");
    private static java.text.DateFormat t = new SimpleDateFormat("d-MMM-yy H:mm:ss");
    private static java.text.DateFormat p = new SimpleDateFormat("d-MMM-yy h:mm:ss a");
    private static java.text.DateFormat i = new SimpleDateFormat("M-d-yy");
    private static java.text.DateFormat g = new SimpleDateFormat("d-MMM-yy");
    private static java.text.DateFormat f = new SimpleDateFormat("d MMM yy");
    private static java.text.DateFormat e = new SimpleDateFormat("d.MMM.yy");
    private static java.text.DateFormat w = java.text.DateFormat.getDateInstance(3);
    private static java.text.DateFormat r = java.text.DateFormat.getDateTimeInstance(3, 2);
    private static java.text.DateFormat _fldfor = java.text.DateFormat.getTimeInstance(2);
    public static final java.util.Date EmptyDate = diamondedge.util.DateTime.toDate(0.0D);
    private static int o = 0;

    static 
    {
        j = new SimpleDateFormat("EEE MMM dd H:mm:ss zzz yyyy", java.util.Locale.US);
        h = new SimpleDateFormat("EEE MMM dd H:mm:ss yyyy", java.util.Locale.US);
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        o = calendar.get(15) + calendar.get(16);
    }
}
