// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.StringTokenizer;

// Referenced classes of package diamondedge.util:
//            Variant, DateTime

public class InputFile extends java.io.RandomAccessFile
{

    public InputFile(java.lang.String s)
        throws java.io.FileNotFoundException
    {
        super(s, "r");
        _fldif = null;
    }

    private java.lang.String a()
        throws java.io.IOException
    {
        if(_fldif == null)
            _fldif = new StringTokenizer(readLine(), ",");
        if(_fldif.hasMoreTokens())
        {
            return _fldif.nextToken();
        } else
        {
            _fldif = null;
            return a();
        }
    }

    public diamondedge.util.Variant input()
        throws java.io.IOException
    {
        java.lang.String s;
        s = a().trim();
        if(s.charAt(0) == '"')
            return new Variant(s.substring(1, s.length() - 1));
        if(s.charAt(0) != '#')
            break MISSING_BLOCK_LABEL_173;
        s = s.substring(1, s.length() - 1);
        if(s.toLowerCase().equals("true"))
            return new Variant(true);
        if(s.toLowerCase().equals("false"))
            return new Variant(false);
        if(s.toLowerCase().equals("null"))
            return (new Variant()).setNull();
        return new Variant(diamondedge.util.DateTime.toDate(s));
        java.lang.Exception exception;
        exception;
        throw new IOException(exception + "\n" + "Error: parsing string: " + s);
        return new Variant(s);
    }

    public java.lang.String inputLine()
        throws java.io.IOException
    {
        return readLine();
    }

    public java.lang.String input(int i)
        throws java.io.IOException
    {
        byte abyte0[] = new byte[i];
        read(abyte0, 0, i);
        return new String(abyte0);
    }

    private static final int a = 35;
    private static final int _flddo = 34;
    private java.util.StringTokenizer _fldif = null;
}
