// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Random;

// Referenced classes of package diamondedge.util:
//            Variant, Obj, DateTime

public class VB
{

    public VB()
    {
    }

    public static final diamondedge.util.Variant Choose(int i, diamondedge.util.Variant variant, diamondedge.util.Variant variant1, diamondedge.util.Variant variant2)
    {
        switch(i)
        {
        case 1: // '\001'
            return variant;

        case 2: // '\002'
            return variant1;

        case 3: // '\003'
            return variant2;
        }
        return null;
    }

    public static final diamondedge.util.Variant Choose(int i, diamondedge.util.Variant variant, diamondedge.util.Variant variant1, diamondedge.util.Variant variant2, diamondedge.util.Variant variant3, diamondedge.util.Variant variant4, diamondedge.util.Variant variant5, diamondedge.util.Variant variant6, 
            diamondedge.util.Variant variant7, diamondedge.util.Variant variant8, diamondedge.util.Variant variant9)
    {
        switch(i)
        {
        case 1: // '\001'
            return variant;

        case 2: // '\002'
            return variant1;

        case 3: // '\003'
            return variant2;

        case 4: // '\004'
            return variant3;

        case 5: // '\005'
            return variant4;

        case 6: // '\006'
            return variant5;

        case 7: // '\007'
            return variant6;

        case 8: // '\b'
            return variant7;

        case 9: // '\t'
            return variant8;

        case 10: // '\n'
            return variant9;
        }
        return null;
    }

    public static final diamondedge.util.Variant Switch(boolean flag, diamondedge.util.Variant variant, boolean flag1, diamondedge.util.Variant variant1, boolean flag2, diamondedge.util.Variant variant2)
    {
        if(flag)
            return variant;
        if(flag1)
            return variant1;
        if(flag2)
            return variant2;
        else
            return null;
    }

    public static final diamondedge.util.Variant Switch(boolean flag, diamondedge.util.Variant variant, boolean flag1, diamondedge.util.Variant variant1, boolean flag2, diamondedge.util.Variant variant2, boolean flag3, diamondedge.util.Variant variant3, 
            boolean flag4, diamondedge.util.Variant variant4, boolean flag5, diamondedge.util.Variant variant5, boolean flag6, diamondedge.util.Variant variant6)
    {
        if(flag)
            return variant;
        if(flag1)
            return variant1;
        if(flag2)
            return variant2;
        if(flag3)
            return variant3;
        if(flag4)
            return variant4;
        if(flag5)
            return variant5;
        if(flag6)
            return variant6;
        else
            return null;
    }

    public static final void Assert(boolean flag)
        throws java.lang.RuntimeException
    {
        if(!flag)
            throw new RuntimeException("Assertion failed");
        else
            return;
    }

    public static final void Assert(double d)
        throws java.lang.RuntimeException
    {
        if(d == 0.0D)
            throw new RuntimeException("Assertion failed");
        else
            return;
    }

    public static boolean matches(java.lang.String s, java.lang.String s1)
    {
        return s.matches(s1);
    }

    public static diamondedge.util.Variant nz(diamondedge.util.Variant variant, diamondedge.util.Variant variant1)
    {
        if(variant == null || variant.isNull())
        {
            if(variant1 == null)
            {
                if(variant == null || variant1.isNull() || variant.isNumeric())
                    return new Variant(0);
                else
                    return new Variant("");
            } else
            {
                return variant1;
            }
        } else
        {
            return variant;
        }
    }

    public static diamondedge.util.Variant nz(diamondedge.util.Variant variant)
    {
        return diamondedge.util.VB.nz(variant, null);
    }

    public static synchronized double rnd()
    {
        if(_fldif == null)
            _fldif = new Random();
        return a = _fldif.nextDouble();
    }

    public static synchronized double rnd(int i)
    {
        if(_fldif == null)
            _fldif = new Random();
        if(i == 0)
            return a;
        if(i < 0)
            _fldif.setSeed(java.lang.Math.abs(i));
        return diamondedge.util.VB.rnd();
    }

    public static void randomize()
    {
        _fldif = new Random();
    }

    public static void randomize(int i)
    {
        _fldif = new Random(i);
    }

    public static double fix(double d)
    {
        if(d < 0.0D)
            return -java.lang.Math.floor(-d);
        else
            return java.lang.Math.floor(d);
    }

    public static int upperBound(diamondedge.util.Variant variant, int i)
    {
        java.lang.Object obj = variant.toObject();
        return diamondedge.util.VB.upperBound(obj, i);
    }

    public static int upperBound(java.lang.Object obj, int i)
    {
        java.lang.Object obj1;
        for(obj1 = obj; i > 1 && obj1 != null; i--)
            obj1 = java.lang.reflect.Array.get(obj1, 0);

        int j = java.lang.reflect.Array.getLength(obj1);
        return j - 1;
    }

    private static java.lang.String a(java.lang.String s, char c)
    {
        java.lang.StringBuffer stringbuffer = new StringBuffer(s);
        for(int i = s.length() - 1; i >= 0; i--)
            if(stringbuffer.charAt(i) == c)
                stringbuffer.deleteCharAt(i);

        return stringbuffer.toString();
    }

    public static java.lang.String spc(int i)
    {
        java.lang.StringBuffer stringbuffer = new StringBuffer();
        for(int j = 0; j < i; j++)
            stringbuffer.append(' ');

        return stringbuffer.toString();
    }

    public static java.lang.String tab(int i)
    {
        return "\t";
    }

    public static double val(java.lang.String s)
    {
        java.lang.StringBuffer stringbuffer;
        double d;
        stringbuffer = new StringBuffer();
        int i = s.length();
        for(int j = 0; j < i; j++)
        {
            char c = s.charAt(j);
            if(c != ' ')
                stringbuffer.append(s.charAt(j));
        }

        d = 0.0D;
        java.lang.Number number = diamondedge.util.Obj._fldif.parse(stringbuffer.toString(), new ParsePosition(0));
        if(number != null)
            return number.doubleValue();
        break MISSING_BLOCK_LABEL_91;
        java.lang.Exception exception;
        exception;
        return d;
    }

    public static java.lang.Object copyArray(java.lang.Object obj, java.lang.Object obj1)
    {
        if(obj == null)
        {
            return obj1;
        } else
        {
            int i = java.lang.Math.min(java.lang.reflect.Array.getLength(obj), java.lang.reflect.Array.getLength(obj1));
            java.lang.System.arraycopy(obj, 0, obj1, 0, i);
            return obj1;
        }
    }

    public static java.lang.Object copyArray(java.lang.Object obj, java.lang.Object obj1, int i)
    {
        if(obj == null)
            return obj1;
        if(i == 1)
            return diamondedge.util.VB.copyArray(obj, obj1);
        try
        {
            int j = java.lang.Math.min(java.lang.reflect.Array.getLength(obj), java.lang.reflect.Array.getLength(obj1));
            for(int k = 0; k < j; k++)
                diamondedge.util.VB.copyArray(java.lang.reflect.Array.get(obj, k), java.lang.reflect.Array.get(obj1, k), i - 1);

        }
        catch(java.lang.Exception exception) { }
        return obj1;
    }

    private static java.lang.Object a(java.lang.Class class1)
    {
        if(class1 == (java.math.BigDecimal.class))
            return new BigDecimal(0.0D);
        if(class1 == (java.util.Date.class))
            return diamondedge.util.DateTime.EmptyDate;
        else
            return null;
    }

    public static java.lang.Object[] initArray(java.lang.Object aobj[], java.lang.Class class1)
    {
        java.lang.Object obj = diamondedge.util.VB.a(class1);
        int i = aobj.length;
        for(int j = 0; j < i; j++)
            try
            {
                aobj[j] = obj != null ? obj : class1.newInstance();
            }
            catch(java.lang.Exception exception) { }

        return aobj;
    }

    public static java.lang.Object[][] initArray(java.lang.Object aobj[][], java.lang.Class class1)
    {
        java.lang.Object obj = diamondedge.util.VB.a(class1);
        int i = aobj.length;
        int j = aobj[0].length;
        for(int k = 0; k < i; k++)
        {
            for(int l = 0; l < j; l++)
                try
                {
                    aobj[k][l] = obj != null ? obj : class1.newInstance();
                }
                catch(java.lang.Exception exception) { }

        }

        return aobj;
    }

    public static java.lang.Object[][][] initArray(java.lang.Object aobj[][][], java.lang.Class class1)
    {
        java.lang.Object obj = diamondedge.util.VB.a(class1);
        int i = aobj.length;
        int j = aobj[0].length;
        int k = aobj[0][0].length;
        for(int l = 0; l < i; l++)
        {
            for(int i1 = 0; i1 < j; i1++)
            {
                for(int j1 = 0; j1 < k; j1++)
                    try
                    {
                        aobj[l][i1][j1] = obj != null ? obj : class1.newInstance();
                    }
                    catch(java.lang.Exception exception) { }

            }

        }

        return aobj;
    }

    public static java.lang.Object[][][][] initArray(java.lang.Object aobj[][][][], java.lang.Class class1)
    {
        java.lang.Object obj = diamondedge.util.VB.a(class1);
        int i = aobj.length;
        int j = aobj[0].length;
        int k = aobj[0][0].length;
        int l = aobj[0][0][0].length;
        for(int i1 = 0; i1 < i; i1++)
        {
            for(int j1 = 0; j1 < j; j1++)
            {
                for(int k1 = 0; k1 < k; k1++)
                {
                    for(int l1 = 0; l1 < l; l1++)
                        try
                        {
                            aobj[i1][j1][k1][l1] = obj != null ? obj : class1.newInstance();
                        }
                        catch(java.lang.Exception exception) { }

                }

            }

        }

        return aobj;
    }

    private static java.lang.String a(java.lang.String s)
    {
        if(s.charAt(0) == '"' && s.charAt(s.length() - 1) == '"')
            return s.substring(1, s.length() - 1);
        else
            return s;
    }

    public static int shellExecute(java.lang.String s, java.lang.String s1, java.lang.String s2, java.lang.String s3)
        throws java.lang.Exception
    {
        java.lang.String s5 = java.lang.System.getProperty("os.name");
        if(s5.startsWith("Windows"))
        {
            java.lang.String s4;
            switch(s5.charAt(8))
            {
            case 50: // '2'
            case 78: // 'N'
            case 88: // 'X'
                s4 = "cmd /c start \"ShellExecute\" ";
                break;

            default:
                s4 = "start ";
                break;
            }
            java.lang.String s6 = "";
            if(s3 != null && s3.compareTo("") != 0)
            {
                s6 = s6 + diamondedge.util.VB.a(s3);
                if(!s6.endsWith("\\"))
                    s6 = s6 + "\\";
            }
            s6 = s6 + diamondedge.util.VB.a(s1).trim();
            if(s6.indexOf(' ') > 0)
                s6 = '"' + s6 + '"';
            s4 = s4 + s6;
            if(s2 != null)
                s4 = s4 + " " + s2;
            java.lang.Runtime.getRuntime().exec(s4);
        } else
        {
            throw new Exception("ShellExecute( " + s1 + " ) is only supported in Windows");
        }
        return 0;
    }

    public static double Val(java.lang.String s)
    {
        return diamondedge.util.VB.val(s);
    }

    public static double Fix(double d)
    {
        return diamondedge.util.VB.fix(d);
    }

    public static int UBound(diamondedge.util.Variant variant, int i)
    {
        return diamondedge.util.VB.upperBound(variant, i);
    }

    public static int UBound(java.lang.Object obj, int i)
    {
        return diamondedge.util.VB.upperBound(obj, i);
    }

    public static final java.lang.String copyright = "Copyright 1996-2005 Diamond Edge, Inc.";
    private static java.util.Random _fldif = null;
    private static double a = 0;
}
