// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Vector;

// Referenced classes of package diamondedge.util:
//            StringSplitter, Variant, DateTime

public class Strings
{

    public Strings()
    {
    }

    public static boolean isNull(java.lang.String s)
    {
        return s == null || s.length() == 0;
    }

    public static int asc(java.lang.String s)
    {
        return s.charAt(0);
    }

    public static java.lang.String mid(java.lang.String s, int i, int j)
    {
        if(s == null)
            return "";
        int k = s.length();
        if(i > k)
            return "";
        if(i + j > k)
            j = (k - i) + 1;
        return s.substring(i - 1, (i - 1) + j);
    }

    public static java.lang.String mid(java.lang.String s, int i)
    {
        if(s == null)
            return "";
        int j = s.length();
        if(i > j)
            return "";
        else
            return s.substring(i - 1, j);
    }

    public static java.lang.String mid(java.lang.String s, int i, int j, java.lang.String s1)
    {
        if(s == null)
            return "";
        java.lang.StringBuffer stringbuffer = new StringBuffer(s);
        int k = s.length();
        if(i + j > k || j < 0)
            j = (k - i) + 1;
        i--;
        int l = java.lang.Math.min(j, s1.length());
        int i1 = i;
        for(int j1 = 0; j1 < l; j1++)
        {
            stringbuffer.setCharAt(i1, s1.charAt(j1));
            i1++;
        }

        return stringbuffer.toString();
    }

    public static java.lang.String replaceMid(java.lang.String s, int i, int j, java.lang.String s1)
    {
        if(s == null)
        {
            return "";
        } else
        {
            int k = s.length();
            return diamondedge.util.Strings.left(s, i - 1) + s1 + diamondedge.util.Strings.right(s, (k - i - j) + 1);
        }
    }

    public static java.lang.String replace(java.lang.String s, java.lang.String s1, java.lang.String s2)
    {
        return diamondedge.util.Strings.replace(s, s1, s2, 1, -1);
    }

    public static java.lang.String replace(java.lang.String s, java.lang.String s1, java.lang.String s2, int i, int j)
    {
        if(s == null)
            return "";
        java.lang.StringBuffer stringbuffer = new StringBuffer(s.length());
        int k;
        k = k = s1.length();
        int l = i - 1;
        for(int i1 = s.indexOf(s1, l); i1 >= 0 && j != 0; j--)
        {
            stringbuffer.append(s.substring(l, i1));
            stringbuffer.append(s2);
            l = i1 + k;
            i1 = s.indexOf(s1, l);
        }

        stringbuffer.append(s.substring(l));
        return stringbuffer.toString();
    }

    public static java.lang.String replaceOccurrence(java.lang.String s, java.lang.String s1, java.lang.String s2, int i)
    {
        if(s == null)
            return "";
        int j;
        j = j = s1.length();
        int k = s.indexOf(s1, 0);
        int l;
        for(l = 0; k >= 0 && ++l < i; k = s.indexOf(s1, k + j));
        if(l == i)
            return diamondedge.util.Strings.replaceMid(s, k + 1, j, s2);
        else
            return s;
    }

    public static java.lang.String left(java.lang.String s, int i)
    {
        if(s == null)
            return "";
        if(i > s.length())
            return s;
        else
            return s.substring(0, i);
    }

    public static java.lang.String right(java.lang.String s, int i)
    {
        if(s == null)
            return "";
        int j = s.length();
        if(j - i < 0)
            return s;
        else
            return s.substring(j - i);
    }

    public static java.lang.String toLowerCase(java.lang.String s)
    {
        if(s == null)
            return "";
        else
            return s.toLowerCase();
    }

    public static java.lang.String toUpperCase(java.lang.String s)
    {
        if(s == null)
            return "";
        else
            return s.toUpperCase();
    }

    public static java.lang.String toProperCase(java.lang.String s)
    {
        if(s == null)
            return "";
        java.lang.StringBuffer stringbuffer = new StringBuffer(s.toLowerCase());
        int i = stringbuffer.length();
        boolean flag = true;
        for(int j = 0; j < i; j++)
        {
            char c = stringbuffer.charAt(j);
            if(flag)
            {
                stringbuffer.setCharAt(j, java.lang.Character.toUpperCase(c));
                flag = false;
            }
            if(c == ' ')
                flag = true;
        }

        return stringbuffer.toString();
    }

    public static java.lang.String trimLeft(java.lang.String s)
    {
        if(s == null)
            return "";
        int i = s.length();
        int j;
        for(j = 0; j < i && s.charAt(j) == ' '; j++);
        return j <= 0 ? s : s.substring(j, i);
    }

    public static java.lang.String trimRight(java.lang.String s)
    {
        if(s == null)
            return "";
        int i;
        for(i = s.length(); i > 0 && s.charAt(i - 1) == ' '; i--);
        return i >= s.length() ? s : s.substring(0, i);
    }

    public static java.lang.String trim(java.lang.String s)
    {
        if(s == null)
            return "";
        else
            return diamondedge.util.Strings.trimLeft(diamondedge.util.Strings.trimRight(s));
    }

    public static java.lang.String trimAll(java.lang.String s)
    {
        if(s == null)
            return "";
        s = s.trim();
        return s.replaceAll("\\s+", " ");
        java.lang.Exception exception;
        exception;
        return s;
    }

    public static java.lang.String clean(java.lang.String s)
    {
        if(s == null)
            return "";
        java.lang.StringBuffer stringbuffer = new StringBuffer(s);
        int i = stringbuffer.length();
        for(int j = i - 1; j >= 0; j--)
            if(java.lang.Character.isISOControl(stringbuffer.charAt(j)))
                stringbuffer.deleteCharAt(j);

        return stringbuffer.toString();
    }

    public static int len(java.lang.String s)
    {
        if(s == null)
            return 0;
        else
            return s.length();
    }

    public static int find(int i, java.lang.String s, java.lang.String s1, boolean flag)
    {
        if(s == null || s1 == null)
            return 0;
        if(flag)
        {
            s = s.toLowerCase();
            s1 = s1.toLowerCase();
        }
        return s.indexOf(s1, i - 1) + 1;
    }

    public static int find(int i, java.lang.String s, java.lang.String s1)
    {
        if(s == null || s1 == null)
            return 0;
        else
            return s.indexOf(s1, i - 1) + 1;
    }

    public static int find(java.lang.String s, java.lang.String s1)
    {
        if(s == null || s1 == null)
            return 0;
        else
            return s.indexOf(s1) + 1;
    }

    public static int findReverse(java.lang.String s, java.lang.String s1, int i, boolean flag)
    {
        if(s == null || s1 == null)
            return -1;
        if(i < 0)
            i = s.length();
        if(flag)
        {
            s = s.toLowerCase();
            s1 = s1.toLowerCase();
        }
        return s.lastIndexOf(s1, i - 1) + 1;
    }

    public static int compare(java.lang.String s, java.lang.String s1)
    {
        if(s == s1)
            return 0;
        if(s == null)
            return -1;
        if(s1 == null)
            return 1;
        int i = s.compareTo(s1);
        if(i == 0)
            return 0;
        else
            return i <= 0 ? -1 : 1;
    }

    public static java.lang.String space(int i)
    {
        return diamondedge.util.Strings.fill(i, ' ');
    }

    public static java.lang.String fill(int i, char c)
    {
        char ac[] = new char[i];
        for(int j = 0; j < i; j++)
            ac[j] = c;

        return new String(ac);
    }

    public static java.lang.String fill(int i, java.lang.String s)
    {
        return diamondedge.util.Strings.fill(i, s.charAt(0));
    }

    public static java.lang.String repeat(java.lang.String s, int i)
    {
        java.lang.StringBuffer stringbuffer = new StringBuffer();
        for(int j = 0; j < i; j++)
            stringbuffer.append(s);

        return stringbuffer.toString();
    }

    public static java.lang.String formatNumber(double d, int i, boolean flag, boolean flag1, boolean flag2)
    {
        return diamondedge.util.Strings.a(java.text.NumberFormat.getInstance(), d, i, flag, flag1, flag2);
    }

    public static java.lang.String formatPercent(double d, int i, boolean flag, boolean flag1, boolean flag2)
    {
        return diamondedge.util.Strings.a(java.text.NumberFormat.getPercentInstance(), d, i, flag, flag1, flag2);
    }

    public static java.lang.String formatCurrency(double d, int i, boolean flag, boolean flag1, boolean flag2)
    {
        return diamondedge.util.Strings.a(java.text.NumberFormat.getCurrencyInstance(), d, i, flag, flag1, flag2);
    }

    private static java.lang.String a(java.text.NumberFormat numberformat, double d, int i, boolean flag, boolean flag1, boolean flag2)
    {
        if(i >= 0)
        {
            numberformat.setMinimumFractionDigits(i);
            numberformat.setMaximumFractionDigits(i);
        }
        numberformat.setGroupingUsed(flag2);
        if(!flag);
        numberformat.setMinimumIntegerDigits(1);
        java.lang.String s = numberformat.format(d);
        if(!flag)
        {
            int j = s.indexOf("0.");
            s = s.substring(0, j) + s.substring(j + 1);
        }
        if(flag1)
        {
            int k = s.indexOf("-");
            if(k >= 0)
                s = s.substring(0, k) + "(" + s.substring(k + 1) + ")";
        }
        return s;
    }

    public static java.lang.String join(java.lang.Object aobj[], java.lang.String s)
    {
        java.lang.StringBuffer stringbuffer = new StringBuffer();
        for(int i = 0; i < aobj.length; i++)
        {
            if(i > 0)
                stringbuffer.append(s);
            stringbuffer.append(aobj[i]);
        }

        return stringbuffer.toString();
    }

    public static java.lang.String join(diamondedge.util.Variant variant, java.lang.String s)
    {
        if(variant.isArray())
            return diamondedge.util.Strings.join((java.lang.Object[])variant.toObject(), s);
        else
            return variant.toString();
    }

    public static java.lang.String[] split(java.lang.String s, java.lang.String s1, int i)
    {
        if(i == 0)
            return null;
        diamondedge.util.StringSplitter stringsplitter = new StringSplitter(s, s1);
        java.util.Vector vector = new Vector();
        for(; stringsplitter.hasMoreTokens() && i != 0; i--)
            vector.addElement(stringsplitter.nextToken());

        if(stringsplitter.hasMoreTokens())
        {
            java.lang.String s2;
            for(s2 = (java.lang.String)vector.lastElement(); stringsplitter.hasMoreTokens(); s2 = s2 + "," + stringsplitter.nextToken());
            vector.setElementAt(s2, vector.size() - 1);
        }
        java.lang.String as[] = new java.lang.String[vector.size()];
        vector.copyInto(as);
        return as;
    }

    public static java.lang.String format(double d, java.lang.String s)
    {
        if(s == null)
            return java.lang.String.valueOf(d);
        java.lang.String s1 = s.toUpperCase();
        java.lang.Object obj;
        if(s1.equals("GENERAL NUMBER"))
        {
            obj = java.text.NumberFormat.getNumberInstance();
            ((java.text.NumberFormat) (obj)).setGroupingUsed(false);
        } else
        if(s1.equals("CURRENCY"))
            obj = java.text.NumberFormat.getCurrencyInstance();
        else
        if(s1.equals("FIXED"))
        {
            obj = java.text.NumberFormat.getNumberInstance();
            ((java.text.NumberFormat) (obj)).setMinimumFractionDigits(2);
            ((java.text.NumberFormat) (obj)).setMaximumFractionDigits(2);
            ((java.text.NumberFormat) (obj)).setMinimumIntegerDigits(1);
            ((java.text.NumberFormat) (obj)).setGroupingUsed(false);
        } else
        if(s1.equals("STANDARD"))
        {
            obj = java.text.NumberFormat.getNumberInstance();
            ((java.text.NumberFormat) (obj)).setMinimumFractionDigits(2);
            ((java.text.NumberFormat) (obj)).setMaximumFractionDigits(2);
            ((java.text.NumberFormat) (obj)).setMinimumIntegerDigits(1);
        } else
        if(s1.equals("PERCENT"))
        {
            obj = java.text.NumberFormat.getPercentInstance();
            ((java.text.NumberFormat) (obj)).setMinimumFractionDigits(2);
            ((java.text.NumberFormat) (obj)).setMaximumFractionDigits(2);
        } else
        if(s1.equals("SCIENTIFIC"))
        {
            obj = new DecimalFormat("0.##E00");
        } else
        {
            if(s1.equals("YES/NO"))
                return d != 0.0D ? "Yes" : "No";
            if(s1.equals("TRUE/FALSE"))
                return d != 0.0D ? "True" : "False";
            if(s1.equals("ON/OFF"))
                return d != 0.0D ? "On" : "Off";
            obj = new DecimalFormat(s);
        }
        return ((java.text.NumberFormat) (obj)).format(d);
    }

    private static boolean a(java.lang.String s)
    {
        if(s == null)
            return false;
        java.lang.String s1 = s.toUpperCase();
        return s1.equals("GENERAL NUMBER") || s1.equals("CURRENCY") || s1.equals("FIXED") || s1.equals("STANDARD") || s1.equals("PERCENT") || s1.equals("SCIENTIFIC") || s1.equals("YES/NO") || s1.equals("TRUE/FALSE") || s1.equals("ON/OFF");
    }

    public static java.lang.String format(java.lang.String s, java.lang.String s1)
    {
        if(s1 == null)
            return s;
        boolean flag = false;
        boolean flag1 = false;
        boolean flag2 = true;
        if(s1.indexOf('!') >= 0)
            flag2 = false;
        java.lang.StringBuffer stringbuffer = new StringBuffer();
        java.lang.String s2 = s;
        int i = 0;
        if(flag2)
        {
            int j = 0;
            for(int l = 0; l < s1.length(); l++)
            {
                char c = s1.charAt(l);
                if(c == '@' || c == '&')
                    j++;
            }

            j -= s.length();
            for(i = 0; i < s1.length() && j > 0; i++)
            {
                char c1 = s1.charAt(i);
                if(c1 == '&' || c1 == '@')
                {
                    if(c1 == '@')
                        stringbuffer.append(' ');
                    j--;
                    continue;
                }
                if(c1 != '<' && c1 != '>' && c1 != '!')
                    stringbuffer.append(c1);
            }

        }
        int k = 0;
        int i1 = s.length();
        for(; i < s1.length(); i++)
        {
            char c2 = s1.charAt(i);
            if(c2 == '@')
            {
                stringbuffer.append(k >= i1 ? ' ' : s.charAt(k++));
                continue;
            }
            if(c2 == '&')
            {
                if(k < i1)
                    stringbuffer.append(s.charAt(k++));
                continue;
            }
            if(c2 == '<')
            {
                flag1 = true;
                continue;
            }
            if(c2 == '>')
            {
                flag = true;
                continue;
            }
            if(c2 != '!')
                stringbuffer.append(c2);
        }

        if(s.length() > k)
            stringbuffer.append(s.substring(k));
        s2 = stringbuffer.toString();
        if(flag)
            s2 = s2.toUpperCase();
        if(flag1)
            s2 = s2.toLowerCase();
        return s2;
    }

    public static java.lang.String format(diamondedge.util.Variant variant, java.lang.String s)
    {
        if(variant.isNull())
            return "";
        if(variant.isNumeric() || diamondedge.util.Strings.a(s))
            return diamondedge.util.Strings.format(variant.toDouble(), s);
        if(variant.isDate())
            return diamondedge.util.DateTime.format(variant.toDate(), s, 0);
        else
            return diamondedge.util.Strings.format(variant.toString(), s);
    }
}
