// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.util.Arrays;

public class Statistics
{

    public Statistics()
    {
    }

    public static double sum(double ad[])
    {
        if(ad.length == 0)
            return 0.0D;
        double d = 0.0D;
        for(int i = 0; i < ad.length; i++)
            d += ad[i];

        return d;
    }

    public static double sumSquares(double ad[])
    {
        if(ad.length == 0)
            return 0.0D;
        double d = 0.0D;
        for(int i = 0; i < ad.length; i++)
            d += ad[i] * ad[i];

        return d;
    }

    public static double product(double ad[])
    {
        if(ad.length == 0)
            return 0.0D;
        double d = 1.0D;
        for(int i = 0; i < ad.length; i++)
            d *= ad[i];

        return d;
    }

    public static double sumProduct(double ad[][])
    {
        if(ad.length == 0)
            return 0.0D;
        int i = ad.length;
        int j = ad[0].length;
        double d = 0.0D;
        for(int k = 0; k < j; k++)
        {
            double d1 = 0.0D;
            for(int l = 0; l < i; l++)
                if(l == 0)
                    d1 = ad[l][k];
                else
                    d1 *= ad[l][k];

            d += d1;
        }

        return d;
    }

    public static double min(double ad[])
    {
        if(ad.length == 0)
            return 0.0D;
        double d = 1.7976931348623157E+308D;
        for(int i = 0; i < ad.length; i++)
            if(ad[i] < d)
                d = ad[i];

        return d;
    }

    public static double max(double ad[])
    {
        if(ad.length == 0)
            return 0.0D;
        double d = 4.9406564584124654E-324D;
        for(int i = 0; i < ad.length; i++)
            if(ad[i] > d)
                d = ad[i];

        return d;
    }

    public static double average(double ad[])
    {
        return diamondedge.util.Statistics.sum(ad) / (double)ad.length;
    }

    public static double median(double ad[])
    {
        if(ad.length == 0)
            return 0.0D;
        java.util.Arrays.sort(ad);
        if(ad.length % 2 == 0)
            return (ad[ad.length / 2 - 1] + ad[ad.length / 2]) / 2D;
        else
            return ad[ad.length / 2];
    }

    public static int rank(double d, double ad[], int i)
    {
        if(ad.length == 0)
            return 0;
        java.util.Arrays.sort(ad);
        if(i > 0)
        {
            for(int j = 0; j < ad.length; j++)
                if(d == ad[j])
                    return j + 1;

        } else
        {
            for(int k = ad.length - 1; k >= 0; k--)
                if(d == ad[k])
                    return k + 1;

        }
        return 0;
    }

    public static double stdev(double ad[])
    {
        return java.lang.Math.sqrt(diamondedge.util.Statistics.var(ad));
    }

    public static double stdevp(double ad[])
    {
        return java.lang.Math.sqrt(diamondedge.util.Statistics.varp(ad));
    }

    public static double var(double ad[])
    {
        double d = diamondedge.util.Statistics.sum(ad);
        double d1 = diamondedge.util.Statistics.sumSquares(ad);
        int i = ad.length;
        return ((double)i * d1 - d * d) / (double)(i * (i - 1));
    }

    public static double varp(double ad[])
    {
        double d = diamondedge.util.Statistics.sum(ad);
        double d1 = diamondedge.util.Statistics.sumSquares(ad);
        int i = ad.length;
        return ((double)i * d1 - d * d) / (double)(i * i);
    }
}
