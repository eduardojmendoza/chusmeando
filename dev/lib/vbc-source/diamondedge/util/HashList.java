// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.util.AbstractList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

public class HashList extends java.util.AbstractList
    implements java.util.List
{

    public HashList()
    {
        _flddo = new Vector();
        a = new Hashtable();
        _fldif = 1;
    }

    public int size()
    {
        return a.size();
    }

    public java.lang.Object get(int i)
    {
        if(i < 0)
        {
            return null;
        } else
        {
            java.lang.Object obj = _flddo.elementAt(i);
            return a.get(obj);
        }
    }

    public java.lang.Object get(java.lang.Object obj)
    {
        if(obj == null)
            return null;
        else
            return a.get(obj);
    }

    public java.lang.Object set(int i, java.lang.Object obj)
    {
        java.lang.Object obj1 = _flddo.elementAt(i);
        return a.put(obj1, obj);
    }

    public java.lang.Object set(java.lang.Object obj, java.lang.Object obj1)
    {
        return put(obj, obj1);
    }

    public boolean add(java.lang.Object obj)
    {
        add(obj, null);
        return true;
    }

    public void add(int i, java.lang.Object obj)
    {
        add(obj, null, i, 0);
    }

    public void add(java.lang.Object obj, java.lang.Object obj1)
    {
        if(obj1 == null)
            obj1 = java.lang.String.valueOf(_fldif++);
        _flddo.addElement(obj1);
        a.put(obj1, obj);
    }

    public void add(java.lang.Object obj, java.lang.Object obj1, java.lang.Object obj2, int i)
    {
        add(obj, obj1, getIndex(obj2), i);
    }

    public void add(java.lang.Object obj, java.lang.Object obj1, int i)
    {
        add(obj, obj1, i, 0);
    }

    public void add(java.lang.Object obj, java.lang.Object obj1, int i, int j)
    {
        if(i < 0)
            throw new IndexOutOfBoundsException("Collection: Index " + i + " not valid.");
        if(obj1 == null)
            obj1 = java.lang.String.valueOf(_fldif++);
        if(j == 1)
            i++;
        _flddo.insertElementAt(obj1, i);
        a.put(obj1, obj);
    }

    public java.lang.Object remove(int i)
    {
        java.lang.Object obj = _flddo.elementAt(i);
        _flddo.removeElementAt(i);
        return a.remove(obj);
    }

    public java.lang.Object removeKey(java.lang.Object obj)
    {
        _flddo.removeElement(obj);
        return a.remove(obj);
    }

    public boolean remove(java.lang.Object obj)
    {
        int i = indexOf(obj);
        if(i >= 0)
        {
            java.lang.Object obj1 = getKey(i);
            _flddo.removeElement(obj1);
            a.remove(obj1);
            return true;
        } else
        {
            return false;
        }
    }

    public void clear()
    {
        _flddo.removeAllElements();
        a.clear();
    }

    public int getIndex(java.lang.Object obj)
    {
        int i = _flddo.size();
        for(int j = 0; j < i; j++)
            if(obj.equals(_flddo.elementAt(j)))
                return j;

        return -1;
    }

    public java.lang.Object getKey(int i)
    {
        return _flddo.elementAt(i);
    }

    public void setKey(int i, java.lang.Object obj)
    {
        if(obj == null || obj.equals(""))
        {
            return;
        } else
        {
            java.lang.Object obj1 = a.remove(getKey(i));
            a.put(obj, obj1);
            _flddo.setElementAt(obj, i);
            return;
        }
    }

    public void changeKey(java.lang.Object obj, java.lang.Object obj1)
    {
        int i = getIndex(obj);
        setKey(i, obj1);
    }

    public java.lang.String getNextKey()
    {
        return java.lang.String.valueOf(_fldif++);
    }

    public boolean containsKey(java.lang.Object obj)
    {
        return a.containsKey(obj);
    }

    public boolean containsValue(java.lang.Object obj)
    {
        return a.containsValue(obj);
    }

    public java.lang.Object put(java.lang.Object obj, java.lang.Object obj1)
    {
        if(!a.containsKey(obj))
            _flddo.addElement(obj);
        return a.put(obj, obj1);
    }

    public void putAll(java.util.Map map)
    {
        java.util.Map.Entry entry;
        for(java.util.Iterator iterator = map.entrySet().iterator(); iterator.hasNext(); put(entry.getKey(), entry.getValue()))
            entry = (java.util.Map.Entry)iterator.next();

    }

    public java.util.Set keySet()
    {
        return a.keySet();
    }

    public java.util.Collection values()
    {
        return a.values();
    }

    public java.util.Set entrySet()
    {
        return a.entrySet();
    }

    private java.util.Vector _flddo = null;
    private java.util.Hashtable a = null;
    private int _fldif = 0;
    public static final int BEFORE = 0;
    public static final int AFTER = 1;
}
