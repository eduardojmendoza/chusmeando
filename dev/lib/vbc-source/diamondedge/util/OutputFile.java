// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

// Referenced classes of package diamondedge.util:
//            Variant

public class OutputFile extends java.io.FileOutputStream
{

    public OutputFile(java.lang.String s)
        throws java.io.FileNotFoundException
    {
        super(s);
        _fldtry = new char[256];
        _flddo = 1;
        _fldif = 0;
    }

    public OutputFile(java.lang.String s, boolean flag)
        throws java.io.FileNotFoundException
    {
        super(s, flag);
        _fldtry = new char[256];
        _flddo = 1;
        _fldif = 0;
    }

    public void println()
        throws java.io.IOException
    {
        write(13);
        write(10);
    }

    public void print(int i)
        throws java.io.IOException
    {
        a(java.lang.Integer.toString(i));
    }

    public void print(double d)
        throws java.io.IOException
    {
        a(java.lang.Double.toString(d));
    }

    public void print(java.lang.String s)
        throws java.io.IOException
    {
        a(s);
    }

    public void print(boolean flag)
        throws java.io.IOException
    {
        a(flag ? "True" : "False");
    }

    public void print(diamondedge.util.Variant variant)
        throws java.io.IOException
    {
        a(variant.toString());
    }

    public void writeValue(int i)
        throws java.io.IOException
    {
        a(java.lang.Integer.toString(i), 0);
    }

    public void writeValue(double d)
        throws java.io.IOException
    {
        a(java.lang.Double.toString(d), 0);
    }

    public void writeValue(java.lang.String s)
        throws java.io.IOException
    {
        a(s, 34);
    }

    public void writeValue(boolean flag)
        throws java.io.IOException
    {
        a(flag ? "True" : "False", 35);
    }

    public void writeValue(diamondedge.util.Variant variant)
        throws java.io.IOException
    {
        if(variant.isNumeric())
        {
            a(variant.toString(), 0);
        } else
        {
            int i = variant.getVarType();
            byte byte0 = 35;
            if(i == 15)
                byte0 = 34;
            a(variant.toString(), byte0);
        }
    }

    public void spc(int i)
        throws java.io.IOException
    {
        for(int j = 0; j < i; j++)
            write(32);

    }

    public void tab()
        throws java.io.IOException
    {
        int i = 14 - (_flddo - 1) % 14;
        spc(i);
    }

    public void tab(int i)
        throws java.io.IOException
    {
        if(i < 1)
            i = 1;
        if(i < _flddo)
        {
            println();
            spc(i);
        } else
        {
            spc(i - _flddo);
        }
    }

    private void a(java.lang.String s, int i)
        throws java.io.IOException
    {
        if(_flddo != 1)
            write(44);
        if(i != 0)
            write(i);
        a(s);
        if(i != 0)
            write(i);
    }

    private void a(java.lang.String s)
        throws java.io.IOException
    {
        int i = s.length();
        char ac[];
        if(i < 256)
        {
            ac = _fldtry;
            s.getChars(0, i, ac, 0);
        } else
        {
            ac = s.toCharArray();
        }
        boolean flag = false;
        for(int j = 0; j < i; j++)
            if((ac[j] & 0xff00) != 0)
                flag = true;

        for(int k = 0; k < i; k++)
        {
            char c = ac[k];
            if(flag)
                write(c >>> 8 & 0xff);
            write(c & 0xff);
        }

    }

    public void write(int i)
        throws java.io.IOException
    {
        super.write(i);
        _fldif++;
        if(i != 10)
            _flddo++;
        if(i == 13)
            _flddo = 1;
    }

    public int length()
    {
        return _fldif;
    }

    private static final int a = 35;
    private static final int _fldnew = 34;
    private static final int _fldint = 44;
    private static final int _fldfor = 32;
    private char _fldtry[] = null;
    private int _flddo = 0;
    private int _fldif = 0;
}
