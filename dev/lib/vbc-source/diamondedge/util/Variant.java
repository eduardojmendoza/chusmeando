// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.io.InputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;

// Referenced classes of package diamondedge.util:
//            DateTime, Obj

public class Variant
    implements java.lang.Cloneable, java.io.Serializable
{

    public Variant()
    {
        _fldif = 0;
        _fldint = null;
    }

    public Variant(int i)
    {
        set(i);
    }

    public Variant(double d)
    {
        set(d);
    }

    public Variant(java.lang.Object obj)
    {
        set(obj);
    }

    public Variant(java.lang.Object obj, int i)
    {
        set(obj, i);
    }

    public Variant(boolean flag)
    {
        set(flag);
    }

    public Variant(java.lang.String s)
    {
        set(s);
    }

    public Variant(java.util.Date date)
    {
        set(date);
    }

    public Variant(int i, int j)
    {
        set(i, j);
    }

    public int compareTo(diamondedge.util.Variant variant)
    {
        if(_fldif == 14 || variant._fldif == 14)
            return diamondedge.util.DateTime.compare(toDate(), variant.toDate());
        if(_fldif == 15 || variant._fldif == 15)
            return toString().compareTo(variant.toString());
        double d = toDouble();
        double d1 = variant.toDouble();
        if(d > d1)
            return 1;
        else
            return d >= d1 ? 0 : -1;
    }

    public int compareTo(java.lang.Object obj)
    {
        if(_fldint == obj)
            return 0;
        if(obj == null)
            return 1;
        if(obj instanceof diamondedge.util.Variant)
            return compareTo((diamondedge.util.Variant)obj);
        if((_fldint instanceof java.math.BigDecimal) || (obj instanceof java.math.BigDecimal))
            return toDecimal().compareTo(diamondedge.util.Obj.toDecimal(obj));
        if((_fldint instanceof java.lang.Number) || (obj instanceof java.lang.Number))
        {
            double d = toDouble();
            double d1 = diamondedge.util.Obj.toDouble(obj);
            if(d < d1)
                return -1;
            return d <= d1 ? 0 : 1;
        }
        if((_fldint instanceof java.lang.String) || (obj instanceof java.lang.String))
            return toString().compareTo(diamondedge.util.Obj.toString(obj));
        return !_fldint.equals(obj) ? 1 : 0;
    }

    public boolean equals(java.lang.Object obj)
    {
        if(this == obj)
            return true;
        if(obj == null && _fldint == null)
            return true;
        if(_fldint == null)
            return toString().equals(obj.toString());
        if(obj == null)
            return false;
        if(_fldint.equals(obj))
            return true;
        if(obj instanceof java.util.Date)
            return toDate().equals(obj);
        if(obj instanceof java.util.Calendar)
            return toDate().equals(((java.util.Calendar)obj).getTime());
        if(obj instanceof java.lang.String)
            return toString().equals(obj);
        if((obj instanceof java.lang.Number) && (_fldint instanceof java.lang.Number))
            return toDouble() == ((java.lang.Number)obj).doubleValue();
        if(obj instanceof diamondedge.util.Variant)
        {
            diamondedge.util.Variant variant = (diamondedge.util.Variant)obj;
            if(variant._fldint == null)
                return toString().equals(variant.toString());
            if(_fldint.equals(variant._fldint))
                return true;
            if(_fldif == 14 || variant._fldif == 14)
                return toDate().equals(variant.toDate());
            if((_fldint instanceof java.lang.Number) && (variant._fldint instanceof java.lang.Number))
                return toDouble() == variant.toDouble();
        }
        return false;
    }

    public boolean equals(int i)
    {
        return equals(new Integer(i));
    }

    public boolean equals(double d)
    {
        return equals(new Double(d));
    }

    public int hashCode()
    {
        if(_fldint == null)
            return 0;
        else
            return _fldint.hashCode();
    }

    public final int getVarType()
    {
        return _fldif;
    }

    public static int getVarType(java.lang.Class class1)
    {
        byte byte0 = 13;
        if(class1 == (java.lang.Integer.class))
            byte0 = 4;
        else
        if(class1 == (java.lang.String.class))
            byte0 = 15;
        else
        if(class1 == (java.lang.Double.class))
            byte0 = 8;
        else
        if(class1 == (java.lang.Boolean.class))
            byte0 = 11;
        else
        if(class1 == (diamondedge.util.Variant.class))
            byte0 = 12;
        else
        if(class1 == (java.util.Date.class))
            byte0 = 14;
        else
        if(class1 == (java.util.Calendar.class))
            byte0 = 14;
        else
        if(class1 == (java.lang.Float.class))
            byte0 = 7;
        else
        if(class1 == (java.lang.Byte.class))
            byte0 = 2;
        else
        if(class1 == (java.lang.Short.class))
            byte0 = 3;
        else
        if(class1 == (java.lang.Long.class))
            byte0 = 6;
        else
        if(class1 == (java.math.BigDecimal.class))
            byte0 = 10;
        else
        if(class1 == (java.lang.Throwable.class))
            byte0 = 5;
        return byte0;
    }

    public static int getVarType(java.lang.Object obj)
    {
        short word0 = 13;
        if(obj == null)
            return word0;
        if(obj instanceof java.lang.String)
            word0 = 15;
        else
        if(obj instanceof java.util.Date)
            word0 = 14;
        else
        if(obj instanceof java.util.Calendar)
            word0 = 14;
        else
        if(obj instanceof java.lang.Number)
        {
            if(obj instanceof java.lang.Integer)
                word0 = 4;
            else
            if(obj instanceof java.lang.Double)
                word0 = 8;
            else
            if(obj instanceof java.lang.Float)
                word0 = 7;
            else
            if(obj instanceof java.lang.Byte)
                word0 = 2;
            else
            if(obj instanceof java.lang.Short)
                word0 = 3;
            else
            if(obj instanceof java.math.BigDecimal)
                word0 = 10;
            else
                word0 = 6;
        } else
        if(obj instanceof diamondedge.util.Variant)
        {
            diamondedge.util.Variant variant = (diamondedge.util.Variant)obj;
            word0 = variant._fldif;
        } else
        if(obj instanceof java.lang.Boolean)
            word0 = 11;
        else
        if(obj instanceof byte[])
            word0 = 8194;
        else
        if(obj instanceof java.lang.String[])
            word0 = 8207;
        else
        if(obj instanceof int[])
            word0 = 8196;
        else
        if(obj instanceof double[])
            word0 = 8200;
        else
        if(obj instanceof diamondedge.util.Variant[])
            word0 = 8204;
        else
        if(obj instanceof java.lang.Object[])
            word0 = 8205;
        else
        if(obj instanceof float[])
            word0 = 8199;
        else
        if(obj instanceof short[])
            word0 = 8195;
        else
        if(obj instanceof java.lang.String[][])
            word0 = 8207;
        else
        if(obj instanceof int[][])
            word0 = 8196;
        else
        if(obj instanceof double[][])
            word0 = 8200;
        else
        if(obj instanceof diamondedge.util.Variant[][])
            word0 = 8204;
        return word0;
    }

    public static java.lang.String typeName(int i)
    {
        if((i & 0x2000) == 8192)
            return diamondedge.util.Variant.typeName(i & 0xff) + "()";
        switch(i)
        {
        case 0: // '\0'
            return "Empty";

        case 1: // '\001'
            return "Null";

        case 4: // '\004'
            return "Integer";

        case 6: // '\006'
            return "Long";

        case 7: // '\007'
            return "Float";

        case 8: // '\b'
            return "Double";

        case 14: // '\016'
            return "Date";

        case 15: // '\017'
            return "String";

        case 13: // '\r'
            return "Object";

        case 5: // '\005'
            return "Error";

        case 11: // '\013'
            return "Boolean";

        case 12: // '\f'
            return "Variant";

        case 10: // '\n'
            return "BigDecimal";

        case 2: // '\002'
            return "Byte";

        case 3: // '\003'
            return "Short";

        case 9: // '\t'
        default:
            return "Empty";
        }
    }

    public final java.lang.String getTypeName()
    {
        if(_fldif == 13)
        {
            if(_fldint == null)
                return "Nothing";
            java.lang.String s = _fldint.getClass().getName();
            int i = s.lastIndexOf('.');
            if(i < 0)
                return s;
            else
                return s.substring(i + 1);
        } else
        {
            return diamondedge.util.Variant.typeName(_fldif);
        }
    }

    public void changeType(int i)
        throws java.lang.Exception
    {
        if(i == _fldif)
            return;
        switch(i)
        {
        case 0: // '\0'
            return;

        case 1: // '\001'
            return;

        case 13: // '\r'
            return;

        case 5: // '\005'
            return;

        case 201: 
            return;

        case 205: 
            return;

        case 4: // '\004'
            set(toInt());
            return;

        case 6: // '\006'
            set(toLong());
            return;

        case 7: // '\007'
            set(toFloat());
            return;

        case 8: // '\b'
            set(toDouble());
            return;

        case 14: // '\016'
            set(toDate());
            return;

        case 15: // '\017'
        case 129: 
        case 200: 
            set(toString());
            return;

        case 11: // '\013'
            set(toBoolean());
            return;

        case 10: // '\n'
        case 131: 
            set(toDecimal());
            return;

        case 2: // '\002'
            set(toByte());
            return;

        case 3: // '\003'
            set(toShort());
            return;

        case 128: 
        case 204: 
        case 8194: 
            if(!(_fldint instanceof byte[]))
                set(toString().getBytes(), 8194);
            return;

        case 133: 
            if(!(_fldint instanceof java.sql.Date))
                set(new java.sql.Date(toDateNew().getTime()));
            return;

        case 135: 
            if(!(_fldint instanceof java.sql.Timestamp))
                set(new Timestamp(toDateNew().getTime()));
            return;

        case 134: 
            if(!(_fldint instanceof java.sql.Time))
                set(new Time(toDateNew().getTime()));
            return;
        }
        throw new Exception("Can't changeType from " + diamondedge.util.Variant.typeName(_fldif) + " to a " + diamondedge.util.Variant.typeName(i));
    }

    public final void set(java.lang.Object obj)
    {
        if(obj instanceof diamondedge.util.Variant)
        {
            diamondedge.util.Variant variant = (diamondedge.util.Variant)obj;
            _fldint = variant._fldint;
            _fldif = variant._fldif;
        } else
        {
            _fldint = obj;
            _fldif = (short)diamondedge.util.Variant.getVarType(obj);
        }
    }

    public final void set(java.lang.Object obj, int i)
    {
        _fldint = obj;
        _fldif = (short)i;
    }

    public final void set(java.util.Date date)
    {
        _fldint = date;
        if(date == null)
            _fldif = 1;
        else
            _fldif = 14;
    }

    public final void set(boolean flag)
    {
        _fldint = new Boolean(flag);
        _fldif = 11;
    }

    public final void set(byte byte0)
    {
        _fldint = new Byte(byte0);
        _fldif = 2;
    }

    public final void set(short word0)
    {
        _fldint = new Short(word0);
        _fldif = 3;
    }

    public final void set(int i)
    {
        _fldint = new Integer(i);
        _fldif = 4;
    }

    public final void set(int i, int j)
    {
        _fldint = new Integer(i);
        _fldif = (short)j;
    }

    public final void set(long l)
    {
        _fldint = new Long(l);
        _fldif = 6;
    }

    public final void set(java.math.BigDecimal bigdecimal)
    {
        _fldint = bigdecimal;
        _fldif = 10;
    }

    public final void set(float f)
    {
        _fldint = new Float(f);
        _fldif = 7;
    }

    public final void set(double d)
    {
        _fldint = new Double(d);
        _fldif = 8;
    }

    public final void set(java.lang.String s)
    {
        _fldint = s;
        if(s == null)
            _fldif = 1;
        else
            _fldif = 15;
    }

    public final diamondedge.util.Variant setNull()
    {
        _fldint = null;
        _fldif = 1;
        return this;
    }

    public final boolean toBoolean()
    {
        return diamondedge.util.Obj.toBoolean(_fldint);
    }

    public final int toInt()
    {
        return diamondedge.util.Obj.toInt(_fldint);
    }

    public final long toLong()
    {
        return diamondedge.util.Obj.toLong(_fldint);
    }

    public final double toDouble()
    {
        return diamondedge.util.Obj.toDouble(_fldint);
    }

    public final java.math.BigDecimal toDecimal()
    {
        return diamondedge.util.Obj.toDecimal(_fldint);
    }

    public final java.lang.String toString()
    {
        return diamondedge.util.Obj.toString(_fldint);
    }

    public final byte[] toBytes()
    {
        if(_fldint == null)
            return null;
        if(_fldint instanceof byte[])
            return (byte[])_fldint;
        if(!(_fldint instanceof java.io.InputStream))
            break MISSING_BLOCK_LABEL_86;
        byte abyte0[];
        java.io.InputStream inputstream = (java.io.InputStream)_fldint;
        abyte0 = new byte[inputstream.available()];
        inputstream.read(abyte0);
        return abyte0;
        java.lang.Exception exception;
        exception;
        java.lang.System.out.println("Variant.toBytes(InputStream): " + exception);
        return _fldint.toString().getBytes();
    }

    public final byte toByte()
    {
        return (byte)toInt();
    }

    public final short toShort()
    {
        return (short)toInt();
    }

    public final float toFloat()
    {
        return (float)toDouble();
    }

    public final java.util.Date toDate()
    {
        _fldif;
        JVM INSTR tableswitch 2 15: default 143
    //                   2 76
    //                   3 76
    //                   4 76
    //                   5 76
    //                   6 76
    //                   7 85
    //                   8 85
    //                   9 143
    //                   10 85
    //                   11 143
    //                   12 143
    //                   13 104
    //                   14 104
    //                   15 93;
           goto _L1 _L2 _L2 _L2 _L2 _L2 _L3 _L3 _L1 _L3 _L1 _L1 _L4 _L4 _L5
_L1:
        break MISSING_BLOCK_LABEL_147;
_L2:
        return diamondedge.util.DateTime.toDate(toInt());
_L3:
        return diamondedge.util.DateTime.toDate(toDouble());
_L5:
        return diamondedge.util.DateTime.toDate((java.lang.String)_fldint);
_L4:
        if(_fldint instanceof java.util.Date)
            return (java.util.Date)_fldint;
        if(_fldint instanceof java.util.Calendar)
            return ((java.util.Calendar)_fldint).getTime();
        break MISSING_BLOCK_LABEL_147;
        java.lang.Exception exception;
        exception;
        return null;
    }

    public final java.util.Date toDateNew()
    {
        java.util.Date date = toDate();
        if(date == null)
            return new Date();
        else
            return date;
    }

    public final java.lang.Object toObject()
    {
        return _fldint;
    }

    public final boolean isArray()
    {
        return (_fldif & 0x2000) == 8192;
    }

    public final boolean isDate()
    {
        if(_fldint instanceof java.util.Date)
            return true;
        if(_fldif == 14)
            return true;
        if(_fldint instanceof java.util.Calendar)
            return true;
        if(_fldint instanceof java.lang.String)
        {
            java.util.Date date = toDate();
            if(date != null)
                return true;
        }
        return false;
    }

    public final boolean isEmpty()
    {
        return _fldif == 0;
    }

    public final boolean isError()
    {
        return _fldif == 5;
    }

    public final boolean isNull()
    {
        return _fldif == 1;
    }

    public static boolean isNumeric(java.lang.Class class1)
    {
        return diamondedge.util.Variant.a(diamondedge.util.Variant.getVarType(class1));
    }

    private static boolean a(int i)
    {
        return i >= 2 && i <= 10;
    }

    public final boolean isNumeric()
    {
        java.lang.String s;
        if(_fldint instanceof java.lang.Number)
            return true;
        if(diamondedge.util.Variant.a(_fldif))
            return true;
        if(!(_fldint instanceof java.lang.String))
            break MISSING_BLOCK_LABEL_89;
        s = (java.lang.String)_fldint;
        java.text.ParsePosition parseposition;
        java.lang.Number number;
        s = diamondedge.util.Obj._mthif(s);
        java.text.NumberFormat numberformat = java.text.NumberFormat.getNumberInstance();
        parseposition = new ParsePosition(0);
        number = numberformat.parse(s, parseposition);
        if(number == null || parseposition.getIndex() < s.length())
            return false;
        return true;
        java.lang.Exception exception;
        exception;
        return false;
    }

    public final boolean isObject()
    {
        return _fldif == 13;
    }

    public java.lang.Object clone()
    {
        diamondedge.util.Variant variant = (diamondedge.util.Variant)super.clone();
        return variant;
        java.lang.CloneNotSupportedException clonenotsupportedexception;
        clonenotsupportedexception;
        return null;
    }

    public final diamondedge.util.Variant invoke(java.lang.String s, diamondedge.util.Variant avariant[])
        throws java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException
    {
        return diamondedge.util.Variant.invoke(_fldint, s, avariant);
    }

    public static diamondedge.util.Variant invoke(java.lang.Object obj, java.lang.String s, diamondedge.util.Variant avariant[])
        throws java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException
    {
        if(obj == null)
            return (new Variant()).setNull();
        java.lang.Class class1 = null;
        if(obj instanceof java.lang.Class)
            class1 = (java.lang.Class)obj;
        else
            class1 = obj.getClass();
        java.lang.reflect.Method amethod[] = class1.getMethods();
        if(amethod != null)
        {
            for(int i = 0; i < amethod.length; i++)
            {
                java.lang.reflect.Method method = amethod[i];
                if(method.getName().equals(s))
                {
                    int j = avariant != null ? avariant.length : 0;
                    java.lang.Class aclass[] = method.getParameterTypes();
                    java.lang.Object aobj[] = new java.lang.Object[aclass.length];
                    Object obj1 = null;
                    for(int k = 0; k < aclass.length; k++)
                    {
                        diamondedge.util.Variant variant;
                        if(k >= j)
                            variant = new Variant();
                        else
                            variant = avariant[k];
                        java.lang.Object obj2 = variant.toObject();
                        java.lang.Class class2 = aclass[k];
                        if(obj2 == null || obj2.getClass() != class2)
                            if(class2 == (java.lang.String.class))
                                obj2 = variant.toString();
                            else
                            if(class2 == java.lang.Integer.TYPE)
                                obj2 = new Integer(variant.toInt());
                            else
                            if(class2 == java.lang.Double.TYPE)
                                obj2 = new Double(variant.toDouble());
                            else
                            if(class2 == java.lang.Float.TYPE)
                                obj2 = new Float(variant.toFloat());
                            else
                            if(class2 == java.lang.Boolean.TYPE)
                                obj2 = new Boolean(variant.toBoolean());
                            else
                            if(class2 == java.lang.Byte.TYPE)
                                obj2 = new Byte(variant.toByte());
                            else
                            if(class2 == java.lang.Short.TYPE)
                                obj2 = new Short(variant.toShort());
                            else
                                obj2 = variant.toObject();
                        aobj[k] = obj2;
                    }

                    return new Variant(method.invoke(obj, aobj));
                }
            }

        }
        return null;
    }

    public final void setValue(java.lang.String s, diamondedge.util.Variant variant)
        throws java.lang.IllegalAccessException, java.lang.NoSuchFieldException
    {
        diamondedge.util.Variant.setValue(_fldint, s, variant);
    }

    public static void setValue(java.lang.Object obj, java.lang.String s, diamondedge.util.Variant variant)
        throws java.lang.IllegalAccessException, java.lang.NoSuchFieldException
    {
        try
        {
            java.lang.reflect.Field field = obj.getClass().getField(s);
            if(field.getType() == (diamondedge.util.Variant.class))
                field.set(obj, variant);
            else
                field.set(obj, variant._fldint);
        }
        catch(java.lang.NoSuchFieldException nosuchfieldexception)
        {
            diamondedge.util.Variant avariant[] = new diamondedge.util.Variant[1];
            avariant[0] = variant;
            try
            {
                diamondedge.util.Variant.invoke(obj, "set" + s, avariant);
            }
            catch(java.lang.reflect.InvocationTargetException invocationtargetexception)
            {
                throw nosuchfieldexception;
            }
        }
    }

    public final diamondedge.util.Variant getValue(java.lang.String s)
        throws java.lang.IllegalAccessException, java.lang.NoSuchFieldException
    {
        return diamondedge.util.Variant.getValue(_fldint, s);
    }

    public static diamondedge.util.Variant getValue(java.lang.Object obj, java.lang.String s)
        throws java.lang.IllegalAccessException, java.lang.NoSuchFieldException
    {
        java.lang.reflect.Field field = obj.getClass().getField(s);
        return new Variant(field.get(obj));
        java.lang.NoSuchFieldException nosuchfieldexception;
        nosuchfieldexception;
        return diamondedge.util.Variant.invoke(obj, "get" + s, null);
        java.lang.reflect.InvocationTargetException invocationtargetexception;
        invocationtargetexception;
        throw nosuchfieldexception;
    }

    public final diamondedge.util.Variant getValue(java.lang.String s, int ai[])
        throws java.lang.IllegalAccessException, java.lang.NoSuchFieldException
    {
        boolean flag = false;
        java.lang.reflect.Field field = _fldint.getClass().getField(s);
        java.lang.Object obj = field.get(_fldint);
        for(int i = 0; i < ai.length; i++)
            obj = java.lang.reflect.Array.get(obj, ai[i]);

        if(obj instanceof diamondedge.util.Variant)
            return (diamondedge.util.Variant)obj;
        else
            return new Variant(obj);
    }

    public final diamondedge.util.Variant getValueAt(int i)
    {
        java.lang.Object obj = java.lang.reflect.Array.get(_fldint, i);
        if(obj instanceof diamondedge.util.Variant)
            return (diamondedge.util.Variant)obj;
        else
            return new Variant(obj);
    }

    public final diamondedge.util.Variant getValueAt(int ai[])
    {
        boolean flag = false;
        java.lang.Object obj = _fldint;
        for(int i = 0; i < ai.length; i++)
            obj = java.lang.reflect.Array.get(obj, ai[i]);

        if(obj instanceof diamondedge.util.Variant)
            return (diamondedge.util.Variant)obj;
        else
            return new Variant(obj);
    }

    public final void setValueAt(int i, diamondedge.util.Variant variant)
    {
        java.lang.Class class1 = null;
        class1 = _fldint.getClass().getComponentType();
        if(class1 == (diamondedge.util.Variant.class))
        {
            java.lang.Object obj = java.lang.reflect.Array.get(_fldint, i);
            if(obj instanceof diamondedge.util.Variant)
                ((diamondedge.util.Variant)obj).set(variant);
            else
                java.lang.reflect.Array.set(_fldint, i, new Variant(variant));
        } else
        {
            java.lang.reflect.Array.set(_fldint, i, variant._fldint);
        }
    }

    public final void setValueAt(int ai[], diamondedge.util.Variant variant)
    {
        java.lang.Class class1 = null;
        int i = 0;
        java.lang.Object obj = _fldint;
        for(i = 0; i < ai.length - 1; i++)
            obj = java.lang.reflect.Array.get(obj, ai[i]);

        class1 = obj.getClass().getComponentType();
        if(class1 == (diamondedge.util.Variant.class))
        {
            java.lang.Object obj1 = java.lang.reflect.Array.get(obj, ai[i]);
            if(obj1 instanceof diamondedge.util.Variant)
                ((diamondedge.util.Variant)obj1).set(variant);
            else
                java.lang.reflect.Array.set(obj, ai[i], new Variant(variant));
        } else
        {
            java.lang.reflect.Array.set(obj, ai[i], variant._fldint);
        }
    }

    private static int a(diamondedge.util.Variant variant, diamondedge.util.Variant variant1)
    {
        short word0 = variant._fldif;
        short word1 = variant1._fldif;
        short word2 = word0 <= word1 ? word1 : word0;
        if(word2 == 12)
            if(word0 == 12)
                word2 = word1;
            else
                word2 = word0;
        if(word0 == 15 || word1 == 15)
        {
            word2 = 15;
            if(variant.isNumeric() && variant1.isNumeric())
                if(diamondedge.util.Variant.a(word0))
                    word2 = word0;
                else
                if(diamondedge.util.Variant.a(word1))
                    word2 = word1;
                else
                    word2 = 8;
        }
        if(word2 < 2 || word2 > 10)
            if(word0 == 14 || word1 == 14)
                word2 = 14;
            else
            if(diamondedge.util.Variant.a(word0))
                word2 = word0;
            else
                word2 = word1;
        switch(word2)
        {
        case 2: // '\002'
        case 3: // '\003'
        case 11: // '\013'
            word2 = 4;
            break;

        case 7: // '\007'
            word2 = 8;
            break;
        }
        return word2;
    }

    public final diamondedge.util.Variant add(diamondedge.util.Variant variant)
    {
        int i = diamondedge.util.Variant.a(this, variant);
        if(i == 15)
            return new Variant(toString() + variant.toString());
        switch(i)
        {
        case 4: // '\004'
            return new Variant(toInt() + variant.toInt());

        case 6: // '\006'
            return new Variant(toLong() + variant.toLong());

        case 8: // '\b'
            return new Variant(toDouble() + variant.toDouble());

        case 10: // '\n'
            return new Variant(toDecimal().add(variant.toDecimal()));

        case 14: // '\016'
            return new Variant(_fldif != 14 ? diamondedge.util.DateTime.add(variant.toDate(), toDouble()) : diamondedge.util.DateTime.add(toDate(), variant.toDouble()));

        case 5: // '\005'
        case 7: // '\007'
        case 9: // '\t'
        case 11: // '\013'
        case 12: // '\f'
        case 13: // '\r'
        default:
            return this;
        }
    }

    public final diamondedge.util.Variant subtract(diamondedge.util.Variant variant)
    {
        int i = diamondedge.util.Variant.a(this, variant);
        switch(i)
        {
        case 4: // '\004'
            return new Variant(toInt() - variant.toInt());

        case 6: // '\006'
            return new Variant(toLong() - variant.toLong());

        case 8: // '\b'
            return new Variant(toDouble() - variant.toDouble());

        case 10: // '\n'
            return new Variant(toDecimal().subtract(variant.toDecimal()));

        case 14: // '\016'
            return new Variant(diamondedge.util.DateTime.subtract(toDate(), variant.toDouble()));

        case 5: // '\005'
        case 7: // '\007'
        case 9: // '\t'
        case 11: // '\013'
        case 12: // '\f'
        case 13: // '\r'
        default:
            return this;
        }
    }

    public final diamondedge.util.Variant multiply(diamondedge.util.Variant variant)
    {
        int i = diamondedge.util.Variant.a(this, variant);
        switch(i)
        {
        case 4: // '\004'
            return new Variant(toInt() * variant.toInt());

        case 6: // '\006'
            return new Variant(toLong() * variant.toLong());

        case 8: // '\b'
            return new Variant(toDouble() * variant.toDouble());

        case 10: // '\n'
            return new Variant(toDecimal().multiply(variant.toDecimal()));

        case 5: // '\005'
        case 7: // '\007'
        case 9: // '\t'
        default:
            return this;
        }
    }

    public final diamondedge.util.Variant divide(diamondedge.util.Variant variant)
    {
        int i = diamondedge.util.Variant.a(this, variant);
        switch(i)
        {
        case 4: // '\004'
            return new Variant(toInt() / variant.toInt());

        case 6: // '\006'
            return new Variant(toLong() / variant.toLong());

        case 8: // '\b'
            return new Variant(toDouble() / variant.toDouble());

        case 10: // '\n'
            return new Variant(toDecimal().divide(variant.toDecimal(), 6));

        case 5: // '\005'
        case 7: // '\007'
        case 9: // '\t'
        default:
            return this;
        }
    }

    public final diamondedge.util.Variant negate()
    {
        switch(_fldif)
        {
        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
            return new Variant(-toInt());

        case 6: // '\006'
            return new Variant(-toLong());

        case 7: // '\007'
        case 8: // '\b'
            return new Variant(-toDouble());

        case 10: // '\n'
            return new Variant(toDecimal().negate());

        case 5: // '\005'
        case 9: // '\t'
        default:
            return this;
        }
    }

    private java.lang.Object _fldint = null;
    private short _fldif = 0;
    public static final short EMPTY = 0;
    public static final short NULL = 1;
    public static final short BYTE = 2;
    public static final short SHORT = 3;
    public static final short INTEGER = 4;
    public static final short ERROR = 5;
    public static final short LONG = 6;
    public static final short FLOAT = 7;
    public static final short DOUBLE = 8;
    public static final short DECIMAL = 10;
    public static final short BOOLEAN = 11;
    public static final short VARIANT = 12;
    public static final short OBJECT = 13;
    public static final short DATE = 14;
    public static final short STRING = 15;
    public static final short ARRAY = 8192;
    private static final int _fldgoto = 131;
    private static final int _fldcase = 133;
    private static final int _fldnew = 134;
    private static final int _fldtry = 135;
    private static final int _flddo = 129;
    private static final int _fldelse = 200;
    private static final int _fldchar = 201;
    private static final int _fldbyte = 128;
    private static final int _fldfor = 204;
    private static final int a = 205;
}
