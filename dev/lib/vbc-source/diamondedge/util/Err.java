// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;

// Referenced classes of package diamondedge.util:
//            RuntimeError

public class Err
{

    private Err()
    {
    }

    public static diamondedge.util.RuntimeError getError()
    {
        diamondedge.util.RuntimeError runtimeerror = _fldif;
        if(_flddo)
            runtimeerror = (diamondedge.util.RuntimeError)a.get();
        return runtimeerror != null ? runtimeerror : _fldfor;
    }

    public static void set(java.lang.Exception exception)
    {
        diamondedge.util.RuntimeError runtimeerror = null;
        if(exception instanceof diamondedge.util.RuntimeError)
            runtimeerror = (diamondedge.util.RuntimeError)exception;
        else
        if(exception != null)
            runtimeerror = new RuntimeError(exception);
        if(_flddo)
            a.set(runtimeerror);
        else
            _fldif = runtimeerror;
    }

    public static void set(java.lang.Exception exception, java.lang.String s)
    {
        diamondedge.util.Err.set(exception);
        if(s != null)
        {
            java.io.StringWriter stringwriter = new StringWriter();
            exception.printStackTrace(new PrintWriter(stringwriter));
            java.lang.String s1 = stringwriter.toString();
            int i = s1.indexOf(s);
            if(i < 0 || (i = s1.indexOf('\n', i)) < 0)
                java.lang.System.out.println(s1);
            else
                java.lang.System.out.println(s1.substring(0, i));
        }
    }

    public static java.lang.String getDescription()
    {
        diamondedge.util.RuntimeError runtimeerror = diamondedge.util.Err.getError();
        if(runtimeerror == null)
            return "";
        else
            return runtimeerror.getDescription();
    }

    public static void raise(int i, java.lang.String s, java.lang.String s1, java.lang.String s2, int j)
        throws diamondedge.util.RuntimeError
    {
        throw new RuntimeError(i, s, s1);
    }

    public static void raise(int i, java.lang.String s, java.lang.String s1)
        throws diamondedge.util.RuntimeError
    {
        throw new RuntimeError(i, s, s1);
    }

    public static void clear()
    {
        diamondedge.util.Err.set(null);
    }

    public static void printStackTrace()
    {
        try
        {
            throw new Throwable("stack trace");
        }
        catch(java.lang.Throwable throwable)
        {
            throwable.printStackTrace();
        }
    }

    private static diamondedge.util.RuntimeError _fldif = null;
    private static diamondedge.util.RuntimeError _fldfor = new RuntimeError(0, null, "no error");
    private static java.lang.ThreadLocal a = null;
    private static boolean _flddo = false;

    static 
    {
        try
        {
            a = new ThreadLocal();
            _flddo = true;
        }
        catch(java.lang.Throwable throwable) { }
    }
}
