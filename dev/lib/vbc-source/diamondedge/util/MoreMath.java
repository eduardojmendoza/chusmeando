// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;


public class MoreMath
{

    public MoreMath()
    {
    }

    public static double log(double d, double d1)
    {
        if(d1 <= 0.0D)
            d1 = 10D;
        double d2 = java.lang.Math.log(d) / java.lang.Math.log(d1);
        if(d2 == 0.0D)
            return 0.0D;
        else
            return d2;
    }

    public static double log10(double d)
    {
        return java.lang.Math.log(d) / java.lang.Math.log(10D);
    }

    public static double acosh(double d)
    {
        return java.lang.Math.log(d + java.lang.Math.sqrt(d * d - 1.0D));
    }

    public static double asinh(double d)
    {
        return java.lang.Math.log(d + java.lang.Math.sqrt(d * d + 1.0D));
    }

    public static double atanh(double d)
    {
        return java.lang.Math.log(java.lang.Math.sqrt((1.0D + d) / (1.0D - d)));
    }

    public static double cosh(double d)
    {
        return (java.lang.Math.exp(d) + java.lang.Math.exp(-d)) / 2D;
    }

    public static double sinh(double d)
    {
        return (java.lang.Math.exp(d) - java.lang.Math.exp(-d)) / 2D;
    }

    public static double tanh(double d)
    {
        double d1 = java.lang.Math.exp(d);
        double d2 = java.lang.Math.exp(-d);
        return (d1 - d2) / (d1 + d2);
    }

    public static int factorial(int i)
    {
        int j = 0;
        if(i <= 0)
            j = 1;
        else
            j = i * diamondedge.util.MoreMath.factorial(i - 1);
        return j;
    }

    public static int permutations(int i, int j)
    {
        return diamondedge.util.MoreMath.factorial(i) / diamondedge.util.MoreMath.factorial(i - j);
    }

    public static int combinations(int i, int j)
    {
        return diamondedge.util.MoreMath.factorial(i) / (diamondedge.util.MoreMath.factorial(i - j) * diamondedge.util.MoreMath.factorial(j));
    }

    public static int mod(int i, int j)
    {
        return java.lang.Math.abs(i % j);
    }

    public static int quotient(int i, int j)
    {
        return i % j;
    }

    public static int random(int i, int j)
    {
        return (int)(java.lang.Math.random() * (double)((j - i) + 1)) + i;
    }

    public static double ceil(double d, double d1)
    {
        return java.lang.Math.ceil(d / d1) * d1;
    }

    public static double floor(double d, double d1)
    {
        return java.lang.Math.floor(d / d1) * d1;
    }

    public static double round(double d, double d1)
    {
        if(d < 0.0D)
            if(d1 == 0.0D)
            {
                return java.lang.Math.ceil(d - 0.5D);
            } else
            {
                double d2 = java.lang.Math.pow(10D, d1);
                return java.lang.Math.ceil(d * d2 - 0.5D) / d2;
            }
        if(d1 == 0.0D)
        {
            return java.lang.Math.floor(d + 0.5D);
        } else
        {
            double d3 = java.lang.Math.pow(10D, d1);
            return java.lang.Math.floor(d * d3 + 0.5D) / d3;
        }
    }

    public static double roundUp(double d, double d1)
    {
        if(d < 0.0D)
            if(d1 == 0.0D)
            {
                return java.lang.Math.floor(d);
            } else
            {
                double d2 = java.lang.Math.pow(10D, d1);
                return java.lang.Math.floor(d * d2) / d2;
            }
        if(d1 == 0.0D)
        {
            return java.lang.Math.ceil(d);
        } else
        {
            double d3 = java.lang.Math.pow(10D, d1);
            return java.lang.Math.ceil(d * d3) / d3;
        }
    }

    public static double roundDown(double d, double d1)
    {
        if(d < 0.0D)
            if(d1 == 0.0D)
            {
                return java.lang.Math.ceil(d);
            } else
            {
                double d2 = java.lang.Math.pow(10D, d1);
                return java.lang.Math.ceil(d * d2) / d2;
            }
        if(d1 == 0.0D)
        {
            return java.lang.Math.floor(d);
        } else
        {
            double d3 = java.lang.Math.pow(10D, d1);
            return java.lang.Math.floor(d * d3) / d3;
        }
    }

    public static double odd(double d)
    {
        double d1 = java.lang.Math.ceil(d);
        if(d1 % 2D == 0.0D)
            d1++;
        return d1;
    }

    public static double even(double d)
    {
        double d1 = java.lang.Math.ceil(d);
        d1 += d1 % 2D;
        return d1;
    }

    public static int sign(double d)
    {
        return d != 0.0D ? d <= 0.0D ? -1 : 1 : 0;
    }
}
