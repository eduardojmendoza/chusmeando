// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.io.File;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;

public class XmlDom
{
    public static class a
    {

        void a()
        {
            _fldfor = 0;
            _flddo = null;
            a = 0;
            _fldif = 0;
        }

        void a(java.lang.Exception exception)
        {
            a();
            _flddo = exception;
        }

        void a(org.xml.sax.SAXParseException saxparseexception)
        {
            a(((java.lang.Exception) (saxparseexception)));
            a = saxparseexception.getLineNumber();
            _fldif = saxparseexception.getColumnNumber();
            _fldfor = 1;
        }

        public java.lang.String _mthif()
        {
            return _flddo != null ? _flddo.getMessage() : "";
        }

        public int _mthint()
        {
            return _fldfor;
        }

        public int _mthfor()
        {
            return a;
        }

        public int _mthdo()
        {
            return _fldif;
        }

        private java.lang.Exception _flddo = null;
        private int _fldfor = 0;
        private int a = 0;
        private int _fldif = 0;

        a()
        {
            _flddo = null;
            _fldfor = 0;
            a = 0;
            _fldif = 0;
        }
    }


    public XmlDom()
    {
        _flddo = false;
        _fldif = false;
        _fldfor = new a();
    }

    public org.w3c.dom.Document getDocument()
        throws java.lang.Exception
    {
        if(_fldint == null)
        {
            javax.xml.parsers.DocumentBuilderFactory documentbuilderfactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
            documentbuilderfactory.setValidating(false);
            javax.xml.parsers.DocumentBuilder documentbuilder = documentbuilderfactory.newDocumentBuilder();
            _fldint = documentbuilder.newDocument();
        }
        return _fldint;
    }

    public java.lang.String getUrl()
    {
        return a;
    }

    public boolean isPreservingWhiteSpace()
    {
        return _fldif;
    }

    public void setPreservingWhiteSpace(boolean flag)
    {
        _fldif = flag;
    }

    public boolean isValidating()
    {
        return _flddo;
    }

    public void setValidating(boolean flag)
    {
        _flddo = flag;
    }

    public boolean load(java.lang.String s)
        throws java.lang.Exception
    {
        if(s == null)
        {
            throw new Exception("fileName == null");
        } else
        {
            a = s;
            return load(new File(s));
        }
    }

    public boolean load(java.io.File file)
    {
        try
        {
            javax.xml.parsers.DocumentBuilderFactory documentbuilderfactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
            documentbuilderfactory.setValidating(_flddo);
            documentbuilderfactory.setIgnoringElementContentWhitespace(true);
            javax.xml.parsers.DocumentBuilder documentbuilder = documentbuilderfactory.newDocumentBuilder();
            _fldint = documentbuilder.parse(file);
            if(!_fldif)
                diamondedge.util.XmlDom.removeWhiteSpaceNodes(_fldint);
        }
        catch(org.xml.sax.SAXParseException saxparseexception)
        {
            _fldfor.a(saxparseexception);
            return false;
        }
        catch(java.lang.Exception exception)
        {
            _fldfor.a(exception);
            return false;
        }
        _fldfor.a();
        return true;
    }

    public boolean loadXML(java.lang.String s)
    {
        try
        {
            if(s == null)
                throw new Exception("no data to parse");
            a = null;
            javax.xml.parsers.DocumentBuilderFactory documentbuilderfactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
            documentbuilderfactory.setValidating(false);
            javax.xml.parsers.DocumentBuilder documentbuilder = documentbuilderfactory.newDocumentBuilder();
            _fldint = documentbuilder.parse(new InputSource(new StringReader(s)));
            if(!_fldif)
                diamondedge.util.XmlDom.removeWhiteSpaceNodes(_fldint);
        }
        catch(org.xml.sax.SAXParseException saxparseexception)
        {
            _fldfor.a(saxparseexception);
            return false;
        }
        catch(java.lang.Exception exception)
        {
            _fldfor.a(exception);
            return false;
        }
        _fldfor.a();
        return true;
    }

    public diamondedge.util.a getParseError()
    {
        return _fldfor;
    }

    public void save(java.lang.String s)
        throws java.lang.Exception
    {
        throw new Exception("save() is not yet supported.");
    }

    private static boolean a(org.w3c.dom.Node node)
    {
        if(node == null)
            return false;
        switch(node.getNodeType())
        {
        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
        case 7: // '\007'
        case 8: // '\b'
            return true;

        case 5: // '\005'
        case 6: // '\006'
        default:
            return false;
        }
    }

    public static java.lang.String getText(org.w3c.dom.Node node)
    {
        if(node != null)
        {
            if(diamondedge.util.XmlDom.a(node))
                return node.getNodeValue();
            org.w3c.dom.Node node1;
            for(node1 = node.getFirstChild(); node1 != null && !(node1 instanceof org.w3c.dom.Text); node1 = node1.getNextSibling());
            if(node1 != null)
                return node1.getNodeValue();
        }
        return "";
    }

    public static void setText(org.w3c.dom.Node node, java.lang.String s)
    {
        if(node != null)
        {
            if(diamondedge.util.XmlDom.a(node))
            {
                node.setNodeValue(s);
                return;
            }
            org.w3c.dom.Node node1 = node.getFirstChild();
            if(node1 == null && node.getNodeType() == 1)
            {
                node.appendChild(node.getOwnerDocument().createTextNode(s));
                return;
            }
            for(; node1 != null && !(node1 instanceof org.w3c.dom.Text); node1 = node1.getNextSibling());
            if(node1 != null)
                node1.setNodeValue(s);
        }
    }

    public static void removeWhiteSpaceNodes(org.w3c.dom.Node node)
    {
        if(node == null)
            return;
        org.w3c.dom.NodeList nodelist = node.getChildNodes();
        if(nodelist == null)
            return;
        for(int i = nodelist.getLength() - 1; i >= 0; i--)
        {
            org.w3c.dom.Node node1 = nodelist.item(i);
            if(node1.getNodeType() == 3 && (node1.getNodeValue() == null || node1.getNodeValue().trim().length() == 0))
                node.removeChild(node1);
            else
                diamondedge.util.XmlDom.removeWhiteSpaceNodes(node1);
        }

    }

    private org.w3c.dom.Document _fldint = null;
    private java.lang.String a = null;
    private boolean _flddo = false;
    private boolean _fldif = false;
    private diamondedge.util.a _fldfor = null;
}
