// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

// Referenced classes of package diamondedge.util:
//            Err

public class RuntimeError extends java.lang.Exception
{

    public RuntimeError()
    {
        super("");
        _fldfor = 1;
        a = "";
        _fldif = null;
        _flddo = null;
        diamondedge.util.Err.set(this);
    }

    public RuntimeError(int i, java.lang.String s, java.lang.String s1)
    {
        super(s1);
        _fldfor = 1;
        a = "";
        _fldif = null;
        _flddo = null;
        diamondedge.util.Err.set(this);
        _fldfor = i;
        a = s;
    }

    public RuntimeError(java.lang.Throwable throwable)
    {
        super(throwable.getLocalizedMessage());
        _fldfor = 1;
        a = "";
        _fldif = null;
        _flddo = null;
        _fldif = throwable;
    }

    public java.lang.String getDescription()
    {
        if(_flddo != null)
            return _flddo;
        java.lang.String s = getLocalizedMessage();
        if(s == null)
        {
            if(_fldif == null)
                return getClass().getName();
            else
                return _fldif.getClass().getName();
        } else
        {
            return s;
        }
    }

    public void setDescription(java.lang.String s)
    {
        _flddo = s;
    }

    public java.lang.String toString()
    {
        java.lang.String s = null;
        if(_fldif == null)
            s = super.toString();
        else
            s = _fldif.toString();
        s = s + "\r\n" +
" Error Number: "
 + _fldfor + " Source: " + a;
        if(_flddo != null)
            s = s + "\r\n" +
" Description: "
 + _flddo;
        return s;
    }

    public int getNumber()
    {
        if(_fldfor == 1 && _fldif != null)
            return diamondedge.util.RuntimeError.a(_fldif);
        else
            return _fldfor;
    }

    public void setNumber(int i)
    {
        _fldfor = i;
        if(i == 0)
            diamondedge.util.Err.clear();
    }

    public java.lang.String getSource()
    {
        return a;
    }

    public void setSource(java.lang.String s)
    {
        a = s;
    }

    public void printStackTrace(java.io.PrintStream printstream)
    {
        if(_fldif == null)
            super.printStackTrace(printstream);
        else
            _fldif.printStackTrace(printstream);
    }

    public void printStackTrace(java.io.PrintWriter printwriter)
    {
        if(_fldif == null)
            super.printStackTrace(printwriter);
        else
            _fldif.printStackTrace(printwriter);
    }

    private static int a(java.lang.Throwable throwable)
    {
        byte byte0 = 1;
        if(throwable instanceof java.lang.IllegalArgumentException)
            byte0 = 5;
        else
        if(throwable instanceof java.lang.OutOfMemoryError)
            byte0 = 7;
        else
        if(throwable instanceof java.lang.IndexOutOfBoundsException)
            byte0 = 9;
        else
        if(throwable instanceof java.lang.ArithmeticException)
            byte0 = 11;
        else
        if(throwable instanceof java.lang.ClassCastException)
            byte0 = 13;
        else
        if(throwable instanceof java.lang.NumberFormatException)
            byte0 = 13;
        else
        if(throwable instanceof java.lang.StackOverflowError)
            byte0 = 28;
        else
        if(throwable instanceof java.lang.InternalError)
            byte0 = 51;
        else
        if(throwable instanceof java.io.FileNotFoundException)
            byte0 = 53;
        else
        if(throwable instanceof java.io.IOException)
            byte0 = 57;
        else
        if(throwable instanceof java.lang.NullPointerException)
            byte0 = 91;
        return byte0;
    }

    protected int _fldfor = 0;
    protected java.lang.String a = null;
    protected java.lang.Throwable _fldif = null;
    protected java.lang.String _flddo = null;
}
