// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;


// Referenced classes of package diamondedge.util:
//            MoreMath

public class Financial
{

    public Financial()
    {
    }

    public static double sln(double d, double d1, double d2)
    {
        return (d - d1) / d2;
    }

    public static double syd(double d, double d1, double d2, double d3)
    {
        double d4 = (d2 * (d2 + 1.0D)) / 2D;
        return ((d - d1) * ((d2 - d3) + 1.0D)) / d4;
    }

    public static double ddb(double d, double d1, double d2, double d3, 
            double d4)
    {
        if(d4 <= 0.0D)
            d4 = 2D;
        if(d3 >= d2)
            return 0.0D;
        double d5 = 0.0D;
        for(int i = 1; (double)i <= d3 - 1.0D; i++)
            d5 += ((d - d5) * d4) / d2;

        double d6 = ((d - d5) * d4) / d2;
        if(d6 + d5 > d - d1)
            d6 = d - d1 - d5;
        if(d6 < 0.0D)
            d6 = 0.0D;
        return d6;
    }

    public static double db(double d, double d1, double d2, int i, int j)
    {
        double d3 = 1.0D - java.lang.Math.pow(d1 / d, 1.0D / d2);
        d3 = diamondedge.util.MoreMath.round(d3, 3D);
        if(j <= 0)
            j = 12;
        int k = (int)d2;
        if(j < 12)
            k++;
        if(i > k)
            return 0.0D;
        double d4 = (d * d3 * (double)j) / 12D;
        if(i == 1)
            return d4;
        double d5 = d4;
        for(int l = 2; l <= i - 1; l++)
            d5 += (d - d5) * d3;

        if(i == k)
            return ((d - d5) * d3 * (double)(12 - j)) / 12D;
        double d6 = (d - d5) * d3;
        if(d6 + d5 > d - d1)
            d6 = d - d1 - d5;
        if(d6 < 0.0D)
            d6 = 0.0D;
        return d6;
    }

    public static double pmt(double d, double d1, double d2, double d3, 
            int i)
    {
        double d4 = java.lang.Math.pow(1.0D + d, d1);
        double d5 = (d2 * d * d4) / (d4 - 1.0D);
        d5 += (d3 * d) / (d4 - 1.0D);
        if(i > 0)
            d5 /= 1.0D + d;
        return -d5;
    }

    public static double pv(double d, double d1, double d2, double d3, 
            int i)
    {
        double d4 = java.lang.Math.pow(1.0D + d, d1);
        double d5 = i <= 0 ? 0.0D : d * (d4 - 1.0D);
        double d6 = (d2 * ((d4 - 1.0D) + d5)) / d / d4;
        d6 += d3 / d4;
        return -d6;
    }

    public static double fv(double d, double d1, double d2, double d3, 
            int i)
    {
        double d4 = java.lang.Math.pow(1.0D + d, d1);
        double d5 = (d2 * (d4 - 1.0D)) / d;
        if(i > 0)
            d5 *= 1.0D + d;
        d5 += d3 * d4;
        return -d5;
    }

    public static double nper(double d, double d1, double d2, double d3, 
            int i)
    {
        double d4 = (d1 * (1.0D + d * (double)i)) / d;
        double d5 = java.lang.Math.log((d4 - d3) / (d4 + d2)) / java.lang.Math.log(1.0D + d);
        return d5;
    }
}
