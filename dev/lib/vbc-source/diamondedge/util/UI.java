// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.applet.Applet;
import java.awt.Component;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.PrintStream;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

public class UI
{

    public UI()
    {
    }

    public static void main(java.lang.String args[])
    {
        java.lang.System.out.println(copyright);
        java.lang.System.out.println("No startup class available.");
    }

    public static javax.swing.JScrollPane getScrollPane(java.awt.Component component)
    {
        java.awt.Container container = component.getParent();
        container = container != null ? container.getParent() : null;
        if(container instanceof javax.swing.JScrollPane)
            return (javax.swing.JScrollPane)container;
        else
            return null;
    }

    public static javax.swing.JFrame getJFrame(java.awt.Component component)
    {
        for(; component != null; component = component.getParent())
        {
            if(component instanceof javax.swing.JFrame)
                return (javax.swing.JFrame)component;
            if(component instanceof java.awt.Window)
                return null;
        }

        return null;
    }

    public static java.awt.Frame getFrame(java.awt.Component component)
    {
        for(; component != null; component = component.getParent())
            if(component instanceof java.awt.Frame)
                return (java.awt.Frame)component;

        return null;
    }

    public static java.applet.Applet getApplet(java.awt.Component component)
    {
        for(; component != null; component = component.getParent())
            if(component instanceof java.applet.Applet)
                return (java.applet.Applet)component;

        return null;
    }

    public static java.awt.Component getParent(java.awt.Component component, java.lang.Class class1)
    {
        for(; component != null; component = component.getParent())
            if(class1.isInstance(component))
                return component;

        return null;
    }

    public static void forwardKeyPressed(java.awt.event.KeyEvent keyevent, java.awt.Component component)
    {
        java.util.EventListener aeventlistener[] = component.getListeners(java.awt.event.KeyListener.class);
        java.awt.event.KeyEvent keyevent1 = null;
        if(aeventlistener.length > 0)
            keyevent1 = new KeyEvent(component, keyevent.getID(), keyevent.getWhen(), keyevent.getModifiers(), keyevent.getKeyCode(), keyevent.getKeyChar());
        for(int i = 0; i < aeventlistener.length; i++)
            ((java.awt.event.KeyListener)aeventlistener[i]).keyPressed(keyevent1);

    }

    public static void forwardKeyReleased(java.awt.event.KeyEvent keyevent, java.awt.Component component)
    {
        java.util.EventListener aeventlistener[] = component.getListeners(java.awt.event.KeyListener.class);
        java.awt.event.KeyEvent keyevent1 = null;
        if(aeventlistener.length > 0)
            keyevent1 = new KeyEvent(component, keyevent.getID(), keyevent.getWhen(), keyevent.getModifiers(), keyevent.getKeyCode(), keyevent.getKeyChar());
        for(int i = 0; i < aeventlistener.length; i++)
            ((java.awt.event.KeyListener)aeventlistener[i]).keyReleased(keyevent1);

    }

    public static void forwardKeyTyped(java.awt.event.KeyEvent keyevent, java.awt.Component component)
    {
        java.util.EventListener aeventlistener[] = component.getListeners(java.awt.event.KeyListener.class);
        java.awt.event.KeyEvent keyevent1 = null;
        if(aeventlistener.length > 0)
            keyevent1 = new KeyEvent(component, keyevent.getID(), keyevent.getWhen(), keyevent.getModifiers(), keyevent.getKeyCode(), keyevent.getKeyChar());
        for(int i = 0; i < aeventlistener.length; i++)
            ((java.awt.event.KeyListener)aeventlistener[i]).keyTyped(keyevent1);

    }

    public static void forwardFocusGained(java.awt.event.FocusEvent focusevent, java.awt.Component component)
    {
        java.util.EventListener aeventlistener[] = component.getListeners(java.awt.event.FocusListener.class);
        java.awt.event.FocusEvent focusevent1 = null;
        if(aeventlistener.length > 0)
            focusevent1 = new FocusEvent(component, focusevent.getID(), focusevent.isTemporary());
        for(int i = 0; i < aeventlistener.length; i++)
            ((java.awt.event.FocusListener)aeventlistener[i]).focusGained(focusevent1);

    }

    public static void forwardFocusLost(java.awt.event.FocusEvent focusevent, java.awt.Component component)
    {
        java.util.EventListener aeventlistener[] = component.getListeners(java.awt.event.FocusListener.class);
        java.awt.event.FocusEvent focusevent1 = null;
        if(aeventlistener.length > 0)
            focusevent1 = new FocusEvent(component, focusevent.getID(), focusevent.isTemporary());
        for(int i = 0; i < aeventlistener.length; i++)
            ((java.awt.event.FocusListener)aeventlistener[i]).focusLost(focusevent1);

    }

    public static void forwardMouseClicked(java.awt.event.MouseEvent mouseevent, java.awt.Component component)
    {
        java.util.EventListener aeventlistener[] = component.getListeners(java.awt.event.MouseListener.class);
        java.awt.event.MouseEvent mouseevent1 = null;
        if(aeventlistener.length > 0)
            mouseevent1 = new MouseEvent(component, mouseevent.getID(), mouseevent.getWhen(), mouseevent.getModifiers(), mouseevent.getX(), mouseevent.getY(), mouseevent.getClickCount(), mouseevent.isPopupTrigger());
        for(int i = 0; i < aeventlistener.length; i++)
            ((java.awt.event.MouseListener)aeventlistener[i]).mouseClicked(mouseevent1);

    }

    public static void forwardMousePressed(java.awt.event.MouseEvent mouseevent, java.awt.Component component)
    {
        java.util.EventListener aeventlistener[] = component.getListeners(java.awt.event.MouseListener.class);
        java.awt.event.MouseEvent mouseevent1 = null;
        if(aeventlistener.length > 0)
            mouseevent1 = new MouseEvent(component, mouseevent.getID(), mouseevent.getWhen(), mouseevent.getModifiers(), mouseevent.getX(), mouseevent.getY(), mouseevent.getClickCount(), mouseevent.isPopupTrigger());
        for(int i = 0; i < aeventlistener.length; i++)
            ((java.awt.event.MouseListener)aeventlistener[i]).mousePressed(mouseevent1);

    }

    public static void forwardMouseReleased(java.awt.event.MouseEvent mouseevent, java.awt.Component component)
    {
        java.util.EventListener aeventlistener[] = component.getListeners(java.awt.event.MouseListener.class);
        java.awt.event.MouseEvent mouseevent1 = null;
        if(aeventlistener.length > 0)
            mouseevent1 = new MouseEvent(component, mouseevent.getID(), mouseevent.getWhen(), mouseevent.getModifiers(), mouseevent.getX(), mouseevent.getY(), mouseevent.getClickCount(), mouseevent.isPopupTrigger());
        for(int i = 0; i < aeventlistener.length; i++)
            ((java.awt.event.MouseListener)aeventlistener[i]).mouseReleased(mouseevent1);

    }

    public static void forwardMouseEntered(java.awt.event.MouseEvent mouseevent, java.awt.Component component)
    {
        java.util.EventListener aeventlistener[] = component.getListeners(java.awt.event.MouseListener.class);
        java.awt.event.MouseEvent mouseevent1 = null;
        if(aeventlistener.length > 0)
            mouseevent1 = new MouseEvent(component, mouseevent.getID(), mouseevent.getWhen(), mouseevent.getModifiers(), mouseevent.getX(), mouseevent.getY(), mouseevent.getClickCount(), mouseevent.isPopupTrigger());
        for(int i = 0; i < aeventlistener.length; i++)
            ((java.awt.event.MouseListener)aeventlistener[i]).mouseEntered(mouseevent1);

    }

    public static void forwardMouseExited(java.awt.event.MouseEvent mouseevent, java.awt.Component component)
    {
        java.util.EventListener aeventlistener[] = component.getListeners(java.awt.event.MouseListener.class);
        java.awt.event.MouseEvent mouseevent1 = null;
        if(aeventlistener.length > 0)
            mouseevent1 = new MouseEvent(component, mouseevent.getID(), mouseevent.getWhen(), mouseevent.getModifiers(), mouseevent.getX(), mouseevent.getY(), mouseevent.getClickCount(), mouseevent.isPopupTrigger());
        for(int i = 0; i < aeventlistener.length; i++)
            ((java.awt.event.MouseListener)aeventlistener[i]).mouseExited(mouseevent1);

    }

    public static void forwardMouseDragged(java.awt.event.MouseEvent mouseevent, java.awt.Component component)
    {
        java.util.EventListener aeventlistener[] = component.getListeners(java.awt.event.MouseMotionListener.class);
        java.awt.event.MouseEvent mouseevent1 = null;
        if(aeventlistener.length > 0)
            mouseevent1 = new MouseEvent(component, mouseevent.getID(), mouseevent.getWhen(), mouseevent.getModifiers(), mouseevent.getX(), mouseevent.getY(), mouseevent.getClickCount(), mouseevent.isPopupTrigger());
        for(int i = 0; i < aeventlistener.length; i++)
            ((java.awt.event.MouseMotionListener)aeventlistener[i]).mouseDragged(mouseevent1);

    }

    public static void forwardMouseMoved(java.awt.event.MouseEvent mouseevent, java.awt.Component component)
    {
        java.util.EventListener aeventlistener[] = component.getListeners(java.awt.event.MouseMotionListener.class);
        java.awt.event.MouseEvent mouseevent1 = null;
        if(aeventlistener.length > 0)
            mouseevent1 = new MouseEvent(component, mouseevent.getID(), mouseevent.getWhen(), mouseevent.getModifiers(), mouseevent.getX(), mouseevent.getY(), mouseevent.getClickCount(), mouseevent.isPopupTrigger());
        for(int i = 0; i < aeventlistener.length; i++)
            ((java.awt.event.MouseMotionListener)aeventlistener[i]).mouseMoved(mouseevent1);

    }

    public static java.lang.String copyright = "Copyright 1999-2004 Diamond Edge, Inc. All rights reserved.";
    private static java.lang.String a = "Diamond Edge";

}
