// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package diamondedge.util:
//            Variant

public class VbCollection
{

    public VbCollection()
    {
        _flddo = new Vector();
        a = new Hashtable();
        _fldif = 1;
    }

    public diamondedge.util.Variant getItem(int i)
    {
        if(i < 1)
            throw new ArrayIndexOutOfBoundsException(i);
        java.lang.Object obj = _flddo.elementAt(i - 1);
        java.lang.Object obj1 = a.get(obj);
        if(obj1 == null && !a.containsKey(obj))
            throw new ArrayIndexOutOfBoundsException(i);
        else
            return new Variant(obj1);
    }

    public diamondedge.util.Variant getItem(java.lang.String s)
    {
        java.lang.String s1 = s.toLowerCase();
        java.lang.Object obj = a.get(s1);
        if(obj == null && !a.containsKey(s1))
            throw new IndexOutOfBoundsException("key doesn't exist: " + s);
        else
            return new Variant(obj);
    }

    public diamondedge.util.Variant getItem(diamondedge.util.Variant variant)
    {
        if(variant.getVarType() != 15 && variant.isNumeric())
            return getItem(variant.toInt());
        else
            return getItem(variant.toString());
    }

    public int getCount()
    {
        return a.size();
    }

    private java.lang.Object a(java.lang.Object obj)
    {
        if(obj instanceof diamondedge.util.Variant)
        {
            if(((diamondedge.util.Variant)obj).toObject() == null)
                obj = new Variant(obj);
            else
                obj = ((diamondedge.util.Variant)obj).toObject();
        } else
        if(obj == null)
            obj = new Variant((java.lang.Object)null);
        return obj;
    }

    public void add(java.lang.Object obj)
        throws java.lang.RuntimeException
    {
        add(obj, java.lang.String.valueOf(_fldif++));
    }

    public void add(java.lang.Object obj, java.lang.String s)
        throws java.lang.RuntimeException
    {
        synchronized(_flddo)
        {
            s = s.toLowerCase();
            if(a.containsKey(s))
                throw new RuntimeException("The key: " + s + " is already associated with an element in this collection.");
            _flddo.addElement(s);
            a.put(s, a(obj));
        }
    }

    public void add(java.lang.Object obj, java.lang.String s, java.lang.String s1, int i)
        throws java.lang.IndexOutOfBoundsException
    {
        add(obj, s, getIndex(s1), i);
    }

    public void add(java.lang.Object obj, java.lang.String s, int i, int j)
        throws java.lang.IndexOutOfBoundsException, java.lang.RuntimeException
    {
        if(i < 1)
            throw new IndexOutOfBoundsException("Collection: Index " + i + " not valid.");
        synchronized(_flddo)
        {
            if(s == null)
                s = java.lang.String.valueOf(_fldif++);
            if(j == 1)
                i++;
            s = s.toLowerCase();
            if(a.containsKey(s))
                throw new RuntimeException("The key '" + s + "' is already associated with an element in this collection.");
            _flddo.insertElementAt(s, i - 1);
            a.put(s, a(obj));
        }
    }

    public void remove(int i)
    {
        synchronized(_flddo)
        {
            java.lang.Object obj = _flddo.elementAt(i - 1);
            _flddo.removeElementAt(i - 1);
            a.remove(obj);
        }
    }

    public void remove(java.lang.String s)
    {
        synchronized(_flddo)
        {
            s = s.toLowerCase();
            _flddo.removeElement(s);
            a.remove(s);
        }
    }

    public void remove(diamondedge.util.Variant variant)
    {
        if(variant.getVarType() != 15 && variant.isNumeric())
            remove(variant.toInt());
        else
            remove(variant.toString());
    }

    public void clear()
    {
        synchronized(_flddo)
        {
            _flddo.removeAllElements();
            a.clear();
        }
    }

    public int getIndex(java.lang.String s)
    {
        s = s.toLowerCase();
        int i = _flddo.size();
        for(int j = 0; j < i; j++)
            if(s.equals(_flddo.elementAt(j)))
                return j + 1;

        return -1;
    }

    public java.lang.String getKey(int i)
    {
        java.lang.Object obj = _flddo.elementAt(i - 1);
        return (obj instanceof java.lang.String) ? (java.lang.String)obj : "";
    }

    public void setKey(int i, java.lang.String s)
    {
label0:
        {
            synchronized(_flddo)
            {
                if(s != null && !s.equals(""))
                    break label0;
            }
            return;
        }
        java.lang.Object obj = a.remove(getKey(i));
        s = s.toLowerCase();
        a.put(s, obj);
        _flddo.setElementAt(s, i - 1);
        vector;
        JVM INSTR monitorexit ;
          goto _L1
        exception;
        throw exception;
_L1:
    }

    public void changeKey(java.lang.String s, java.lang.String s1)
    {
        int i = getIndex(s);
        setKey(i, s1);
    }

    public java.lang.String getNextKey()
    {
        return java.lang.String.valueOf(_fldif++);
    }

    public boolean containsKey(java.lang.Object obj)
    {
        return a.containsKey(obj);
    }

    public boolean containsValue(java.lang.Object obj)
    {
        return a.containsValue(obj);
    }

    private java.util.Vector _flddo = null;
    private java.util.Hashtable a = null;
    private int _fldif = 0;
    public static final int BEFORE = 0;
    public static final int AFTER = 1;
}
