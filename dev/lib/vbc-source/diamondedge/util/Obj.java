// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames definits splitstr(nl) 

package diamondedge.util;

import java.io.InputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;

// Referenced classes of package diamondedge.util:
//            Variant, DateTime, Strings

public class Obj
{

    public Obj()
    {
    }

    public static int compare(java.lang.Object obj, java.lang.Object obj1)
    {
        return diamondedge.util.Obj.compare(obj, obj1, false);
    }

    public static int compare(java.lang.Object obj, java.lang.Object obj1, boolean flag)
    {
        if(obj == obj1)
            return 0;
        if(obj == null)
            return -1;
        if(obj1 == null)
            return 1;
        if(obj instanceof diamondedge.util.Variant)
            obj = ((diamondedge.util.Variant)obj).toObject();
        if(obj1 instanceof diamondedge.util.Variant)
            obj1 = ((diamondedge.util.Variant)obj1).toObject();
        if((obj instanceof java.util.Date) || (obj1 instanceof java.util.Date) || (obj instanceof java.util.Calendar) || (obj1 instanceof java.util.Calendar))
        {
            java.util.Date date = diamondedge.util.Obj.toDate(obj);
            java.util.Date date1 = diamondedge.util.Obj.toDate(obj1);
            if(date != null && date1 != null)
            {
                long l = date.getTime();
                long l1 = date1.getTime();
                return l <= l1 ? l >= l1 ? 0 : -1 : 1;
            } else
            {
                return -1;
            }
        }
        if((obj instanceof java.math.BigDecimal) || (obj1 instanceof java.math.BigDecimal))
            return diamondedge.util.Obj.toDecimal(obj).compareTo(diamondedge.util.Obj.toDecimal(obj1));
        if((obj instanceof java.lang.Number) || (obj1 instanceof java.lang.Number))
        {
            double d = diamondedge.util.Obj.toDouble(obj);
            double d1 = diamondedge.util.Obj.toDouble(obj1);
            return d <= d1 ? d >= d1 ? 0 : -1 : 1;
        }
        if((obj instanceof java.lang.Boolean) || (obj1 instanceof java.lang.Boolean))
        {
            boolean flag1 = diamondedge.util.Obj.toBoolean(obj);
            boolean flag2 = diamondedge.util.Obj.toBoolean(obj1);
            return flag1 != flag2 ? flag1 ? 1 : -1 : 0;
        }
        if((obj instanceof java.lang.String) || (obj1 instanceof java.lang.String))
            if(flag)
                return diamondedge.util.Obj.toString(obj).compareTo(diamondedge.util.Obj.toString(obj1));
            else
                return diamondedge.util.Obj.toString(obj).compareToIgnoreCase(diamondedge.util.Obj.toString(obj1));
        if(obj instanceof java.lang.Comparable)
            return ((java.lang.Comparable)obj).compareTo(obj1);
        if(obj1 instanceof java.lang.Comparable)
            return -((java.lang.Comparable)obj1).compareTo(obj);
        break MISSING_BLOCK_LABEL_341;
        java.lang.Exception exception;
        exception;
        return !obj.equals(obj1) ? 1 : 0;
    }

    public static java.lang.Number toNumber(java.lang.Object obj)
    {
        java.lang.String s;
        if(obj instanceof java.lang.Number)
            return (java.lang.Number)obj;
        if(obj instanceof java.lang.Boolean)
            return ((java.lang.Boolean)obj).booleanValue() ? new Integer(1) : new Integer(0);
        if(obj instanceof java.util.Date)
            return new Long(((java.util.Date)obj).getTime());
        if(obj == null)
            return null;
        if(obj instanceof java.lang.String)
            s = ((java.lang.String)obj).trim();
        else
            s = obj.toString();
        return diamondedge.util.Obj.parseNumber(s);
        java.text.ParseException parseexception;
        parseexception;
        return null;
    }

    public static int toInt(java.lang.Object obj)
    {
        java.lang.String s;
        if(obj instanceof java.lang.Number)
            return (int)java.lang.Math.rint(((java.lang.Number)obj).doubleValue());
        if(obj instanceof java.lang.Boolean)
            return ((java.lang.Boolean)obj).booleanValue() ? -1 : 0;
        if(obj instanceof java.util.Date)
            return (int)java.lang.Math.rint(diamondedge.util.DateTime.toDouble((java.util.Date)obj));
        if(obj instanceof java.util.Calendar)
            return (int)java.lang.Math.rint(diamondedge.util.DateTime.toDouble(((java.util.Calendar)obj).getTime()));
        if(obj == null)
            return 0;
        if(obj instanceof java.lang.String)
            s = diamondedge.util.Obj._mthif((java.lang.String)obj);
        else
            s = obj.toString();
        return java.lang.Integer.parseInt(s);
        java.lang.NumberFormatException numberformatexception;
        numberformatexception;
        return (int)diamondedge.util.Obj.a(s);
    }

    public static long toLong(java.lang.Object obj)
    {
        java.lang.String s;
        if(obj instanceof java.lang.Number)
            return ((java.lang.Number)obj).longValue();
        if(obj instanceof java.lang.Boolean)
            return ((java.lang.Boolean)obj).booleanValue() ? -1L : 0L;
        if(obj instanceof java.util.Date)
            return java.lang.Math.round(diamondedge.util.DateTime.toDouble((java.util.Date)obj));
        if(obj instanceof java.util.Calendar)
            return java.lang.Math.round(diamondedge.util.DateTime.toDouble(((java.util.Calendar)obj).getTime()));
        if(obj == null)
            return 0L;
        if(obj instanceof java.lang.String)
            s = diamondedge.util.Obj._mthif((java.lang.String)obj);
        else
            s = obj.toString();
        return java.lang.Long.parseLong(s);
        java.lang.NumberFormatException numberformatexception;
        numberformatexception;
        return (long)diamondedge.util.Obj.a(s);
    }

    public static double toDouble(java.lang.Object obj)
    {
        if(obj instanceof java.lang.Number)
            return ((java.lang.Number)obj).doubleValue();
        if(obj instanceof java.util.Date)
            return diamondedge.util.DateTime.toDouble((java.util.Date)obj);
        if(obj instanceof java.util.Calendar)
            return diamondedge.util.DateTime.toDouble(((java.util.Calendar)obj).getTime());
        if(obj instanceof java.lang.Boolean)
            return ((java.lang.Boolean)obj).booleanValue() ? -1D : 0.0D;
        if(obj == null)
            return 0.0D;
        java.lang.String s;
        if(obj instanceof java.lang.String)
            s = diamondedge.util.Obj._mthif((java.lang.String)obj);
        else
            s = obj.toString();
        return diamondedge.util.Obj.a(s);
    }

    public static java.math.BigDecimal toDecimal(java.lang.Object obj)
    {
        if(obj instanceof java.math.BigDecimal)
            return (java.math.BigDecimal)obj;
        if(!(obj instanceof java.lang.Number))
            break MISSING_BLOCK_LABEL_52;
        if(obj instanceof java.lang.Long)
            return java.math.BigDecimal.valueOf(((java.lang.Number)obj).longValue());
        return new BigDecimal(((java.lang.Number)obj).doubleValue());
        if(obj == null)
            return new BigDecimal(0.0D);
        java.lang.String s;
        if(obj instanceof java.lang.String)
            s = diamondedge.util.Obj._mthif((java.lang.String)obj);
        else
            s = obj.toString();
        if(s.length() > 0)
            return new BigDecimal(s);
        break MISSING_BLOCK_LABEL_108;
        java.lang.NumberFormatException numberformatexception;
        numberformatexception;
        return new BigDecimal(0.0D);
    }

    public static boolean toBoolean(java.lang.Object obj)
    {
        if(obj == null)
            return false;
        if(obj instanceof java.lang.String)
        {
            java.lang.String s = ((java.lang.String)obj).trim();
            if(s.length() == 0)
                return false;
            if(s.equalsIgnoreCase("true"))
                return true;
        }
        double d = diamondedge.util.Obj.toDouble(obj);
        return d != 0.0D;
    }

    public static java.util.Date toDate(java.lang.Object obj)
    {
        if(obj instanceof java.util.Date)
            return (java.util.Date)obj;
        if(obj instanceof java.util.Calendar)
            return ((java.util.Calendar)obj).getTime();
        if(obj instanceof java.lang.Number)
            return diamondedge.util.DateTime.toDate(((java.lang.Number)obj).doubleValue());
        if(obj instanceof java.lang.String)
            return diamondedge.util.DateTime.toDate((java.lang.String)obj);
        break MISSING_BLOCK_LABEL_64;
        java.lang.Exception exception;
        exception;
        return null;
    }

    public static java.util.Date toDateNew(java.lang.Object obj)
    {
        java.util.Date date = diamondedge.util.Obj.toDate(obj);
        if(date == null)
            return new Date();
        else
            return date;
    }

    public static java.lang.String toString(java.lang.Object obj)
    {
        if(obj == null)
            return "";
        if(obj instanceof java.lang.String)
            return (java.lang.String)obj;
        if(obj instanceof java.lang.Boolean)
            return ((java.lang.Boolean)obj).booleanValue() ? "true" : "false";
        if(obj instanceof byte[])
            return new String((byte[])obj);
        if(obj instanceof java.util.Date)
            return diamondedge.util.DateTime.format((java.util.Date)obj);
        if(obj instanceof java.util.Calendar)
            return diamondedge.util.DateTime.format(((java.util.Calendar)obj).getTime());
        if(!(obj instanceof java.io.InputStream))
            break MISSING_BLOCK_LABEL_156;
        byte abyte0[];
        java.io.InputStream inputstream = (java.io.InputStream)obj;
        abyte0 = new byte[inputstream.available()];
        inputstream.read(abyte0);
        return new String(abyte0);
        java.lang.Exception exception;
        exception;
        java.lang.System.out.println("Obj.toString(InputStream): " + exception);
        if(!(obj instanceof java.io.Reader))
            break MISSING_BLOCK_LABEL_236;
        java.lang.StringBuffer stringbuffer;
        java.io.Reader reader = (java.io.Reader)obj;
        char ac[] = new char[64];
        stringbuffer = new StringBuffer();
        int i;
        while((i = reader.read(ac)) > 0) 
            stringbuffer.append(ac, 0, i);
        return stringbuffer.toString();
        reader;
        java.lang.System.out.println("Obj.toString(Reader): " + reader);
        return obj.toString();
    }

    static java.lang.String _mthif(java.lang.String s)
    {
        s = s.trim();
        if(s.length() > 0 && s.charAt(0) == '$')
            s = s.substring(1).trim();
        s = diamondedge.util.Strings.replace(s, ",", "");
        return s;
    }

    private static double a(java.lang.String s)
    {
        return java.lang.Double.parseDouble(s);
        java.lang.Object obj;
        obj;
        return java.lang.Double.valueOf(s).doubleValue();
        obj;
        return 0.0D;
    }

    public static final java.lang.Number parseNumber(java.lang.String s)
        throws java.text.ParseException
    {
        if(s == null || s.length() == 0)
            return null;
        return new Integer(s);
        java.lang.NumberFormatException numberformatexception;
        numberformatexception;
        java.text.NumberFormat numberformat = null;
        java.text.ParsePosition parseposition = new ParsePosition(0);
        int i = s.length();
        for(int j = 0; j < 4; j++)
        {
            switch(j)
            {
            case 0: // '\0'
                numberformat = a;
                break;

            case 1: // '\001'
                numberformat = _fldif;
                break;

            case 2: // '\002'
                numberformat = _fldfor;
                break;

            case 3: // '\003'
                numberformat = _flddo;
                break;
            }
            parseposition.setIndex(0);
            java.lang.Number number = numberformat.parse(s, parseposition);
            if(number != null && parseposition.getIndex() >= i)
                return number;
        }

        throw new ParseException("Unparseable number: \"" + s + "\"", parseposition.getIndex());
    }

    public static final java.lang.Object parseObject(java.lang.Object obj)
    {
        java.lang.String s;
        if(obj == null)
            return null;
        if((obj instanceof java.lang.Number) || (obj instanceof java.lang.Boolean) || (obj instanceof java.util.Date))
            return obj;
        if(obj instanceof java.lang.String)
            s = ((java.lang.String)obj).trim();
        else
            s = obj.toString();
        return diamondedge.util.Obj.parseNumber(s);
        java.text.ParseException parseexception;
        parseexception;
        if(s.equalsIgnoreCase("false"))
            return new Boolean(false);
        if(s.equalsIgnoreCase("true"))
            return new Boolean(true);
        return diamondedge.util.DateTime.toDate(s);
        parseexception;
        return obj;
    }

    static java.text.NumberFormat _fldif = java.text.NumberFormat.getInstance();
    static java.text.NumberFormat _fldfor = java.text.NumberFormat.getPercentInstance();
    static java.text.NumberFormat _flddo = java.text.NumberFormat.getCurrencyInstance();
    static java.text.NumberFormat a = null;

    static 
    {
        try
        {
            a = java.text.NumberFormat.getIntegerInstance();
        }
        catch(java.lang.Throwable throwable)
        {
            a = java.text.NumberFormat.getInstance();
        }
    }
}
