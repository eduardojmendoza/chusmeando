#PerfilUsuario

##Interfaces

Publicamos el servicio de PerfilUsuario ( 1000 ) mediante una interface SOAP y mediante una interface REST, en este último caso soportando payloads en formato JSON y XML.

Para definir estas interfaces seguimos unas definiciones para transformar los mensajes del formato actual de "Action Codes" heredado de las aplicaciones de la época HSBC a nuevas definiciones que soporten los estándares.

El formato del mensaje original es:

Request:

```xml
<ActionCode>lbaw_GetConsultaMQGestion</ActionCode>
<Request>
	<DEFINICION>1000_PerfilUsuario.xml</DEFINICION>
	<AplicarXSL>1000_PerfilUsuario.xsl</AplicarXSL>
	<USUARIO>EX009005L</USUARIO>
</Request>
```

Response:

```xml
<Response>
	<Estado resultado="true" mensaje=""/>
	<USER_NIVEL>A</USER_NIVEL>
	<USUARNOM>FAELLA</USUARNOM>
	<CLIENTIP>P</CLIENTIP>
	<BANCOCOD>9000</BANCOCOD>
	<BANCONOM>CANAL PRODUCTORES</BANCONOM>
	<GRUPOHSBC>N</GRUPOHSBC>
	<ACCESO>
		<MMENU>050000</MMENU>
		<MMENU>551000</MMENU>
	</ACCESO>
	<EMISIONES>
		<EMISION>
			<RAMOPCOD>CON1</RAMOPCOD>
		</EMISION>
		<EMISION>
			<RAMOPCOD>GRN1</RAMOPCOD>
		</EMISION>
	</EMISIONES>
	<MAILUSER>BRENDA.VELASCO@QBE.COM.AR</MAILUSER>
	< PEOPLESOFTEC >12146773</PEOPLESOFTEC>
	< USUARNOMEC >CLAUDIO AMOROSINO</USUARNOMEC>
	<MAILEC>JORGE.ARENA@QBE.COM.AR</MAILEC>
</Response>
```

Este "ActionCode" es un mensaje genérico implementado en el módulo LBAW_MQMensajeGestion


##Interface SOAP

Creamos un servicio para el módulo, una operación en este servicio para el "ActionCode", y pasamos los datos que se envían en el request como parámetros de la operación.


Así el mensaje SOAP de request es:
```xml
<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Header/>
    <soap:Body>
        <ns1:PerfilUsuario_1000 xmlns:ns1="http://nov.services.qbe.com/">
            <AplicarXSL>1000_PerfilUsuario.xsl</AplicarXSL>
            <Usuario>EX009005L</Usuario>
        </ns1:PerfilUsuario_1000>
    </soap:Body>
</soap:Envelope>
```

y uno de respuesta de ejemplo es:

```xml
<?xml version='1.0' encoding='UTF-8'?>
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
	<S:Body>
		<ns0:PerfilUsuario_1000Response xmlns:ns0="http://nov.services.qbe.com/">
			<return>
				<code>0</code>
				<Response>
					<Estado mensaje="" resultado="true"/>
					<USER_NIVEL>A</USER_NIVEL>
					<USUARNOM>COMPERATORE MAURICIO JAVIER</USUARNOM>
					<CLIENTIP>P</CLIENTIP>
					<BANCOCOD>9000</BANCOCOD>
					<BANCONOM>CANAL PRODUCTORES</BANCONOM>
					<GRUPOHSBC>N</GRUPOHSBC>
					<ACCESO>
						<MMENU>050000</MMENU>
						<MMENU>100000</MMENU>
						<MMENU>200000</MMENU>
					</ACCESO>
					<EMISIONES/>
				</Response>
			</return>
		</ns0:PerfilUsuario_1000Response>
	</S:Body>
</S:Envelope>
```

Como se puede ver los cambios solamente se aplican al "envoltorio" y no al contenido del mensaje que respeta el formato original.


###Pruebas SOAP

La versión de prueba está desplegada en el weblogic 10.1.10.30:8080/NOVServices; por ejemplo para hacer una prueba usando SOAP podemos guardar el request soap en un archivo pu.xml y ejecutar:

```bash
 curl -X POST -d @pu.xml -H "Content-Type: text/xml;charset=UTF-8" http://10.1.10.30:8080/NOVServices/ConsultaMQGestion
```


##Interface Rest

Publicamos también una interface Rest. Esta la usamos porque permite que desde el FrontEnd se puedan invocar las operaciones usando JSON como payload, que es más simple de procesar que XML.

Nuestra API fuerza un poco el término Rest, porque no presentamos un conjunto de recursos sino que presentamos operaciones que se invocan al estilo RPC; pero usamos el término porque es descriptivo.


En este caso la forma de mapear los ActionCodes a Rest es: creamos un recurso para el módulo, un subrecurso para el "ActionCode", y pasamos los datos que se envían en el request como parámetros de la operación. Usamos POST que es el método apropiado para operaciones de este estilo tipo RPC.

Esta operación de perfilUsuario está publicada como recurso
/NOVServices/rest/v1/ConsultaMQGestionAPI/perfilUsuario

La API soporta XML y JSON.

**Nota** La implementación que migramos de VisualBasic COM+ a Java usa XML para las respuestas, y genera este XML a mano, sin validarlo. Por otro lado la interface que publicamos convierte automáticamente de XML a JSON, por lo que no estamos seguros de que el JSON que se genere sea válido y tendremos que probar distintos casos para validarlo.



###Pruebas Rest
Un ejemplo de invocación al ambiente de test es:

####JSON
```bash
curl -v -H "Accept: application/json" -X POST "http://10.1.10.30:8080/NOVServices/rest/v1/ConsultaMQGestionAPI/perfilUsuario?AplicarXSL=1000_PerfilUsuario.xsl&Usuario=EX009005L"
```

Respuesta:

```json
{
  "code" : 0,
  "Response" : {
    "Estado" : {
      "mensaje" : "",
      "resultado" : "true"
    },
    "USER_NIVEL" : "A",
    "USUARNOM" : "FAELLA",
    "CLIENTIP" : "B",
    "BANCOCOD" : "4013",
    "BANCONOM" : "HSBC BANK ARGENTINA (PRENDAS)",
    "GRUPOHSBC" : "N",
    "ACCESO" : {
      "MMENU" : "050000",
      "MMENU" : "100000",
      "MMENU" : "200000",
      "MMENU" : "300000",
      "MMENU" : "350000",
      "MMENU" : "400000",
      "MMENU" : "450000",
      "MMENU" : "460000",
      "MMENU" : "470000",
      "MMENU" : "480000",
      "MMENU" : "490000",
      "MMENU" : "500000",
      "MMENU" : "550000",
      "MMENU" : "600000",
      "MMENU" : "051000",
      "MMENU" : "052000",
      "MMENU" : "053000",
      "MMENU" : "054000",
      "MMENU" : "054100",
      "MMENU" : "054500",
      "MMENU" : "101000",
      "MMENU" : "102000",
      "MMENU" : "103000",
      "MMENU" : "104000",
      "MMENU" : "105000",
      "MMENU" : "106000",
      "MMENU" : "107000",
      "MMENU" : "108000",
      "MMENU" : "201000",
      "MMENU" : "202000",
      "MMENU" : "203000",
      "MMENU" : "204000",
      "MMENU" : "205000",
      "MMENU" : "301000",
      "MMENU" : "302000",
      "MMENU" : "302500",
      "MMENU" : "303000",
      "MMENU" : "304000",
      "MMENU" : "305000",
      "MMENU" : "306000",
      "MMENU" : "351000",
      "MMENU" : "352000",
      "MMENU" : "353000",
      "MMENU" : "451000",
      "MMENU" : "452000",
      "MMENU" : "453000",
      "MMENU" : "454000",
      "MMENU" : "455000",
      "MMENU" : "456000",
      "MMENU" : "501000",
      "MMENU" : "502000",
      "MMENU" : "551000"
    },
    "EMISIONES" : {
      "EMISION" : {
        "RAMOPCOD" : "AUS1"
      },
      "EMISION" : {
        "RAMOPCOD" : "AUS1"
      }
    }
  }
 }
```

####XML

```bash
curl -v -H "Accept: application/xml" -X POST "http://10.1.10.30:8080/NOVServices/rest/v1/ConsultaMQGestionAPI/perfilUsuario?AplicarXSL=1000_PerfilUsuario.xsl&Usuario=EX009005L"
```

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<novServiceResponse>
	<code>0</code>
	<Response>
		<Estado mensaje="" resultado="true" />
		<USER_NIVEL>A</USER_NIVEL>
		<USUARNOM>FAELLA</USUARNOM>
		<CLIENTIP>B</CLIENTIP>
		<BANCOCOD>4013</BANCOCOD>
		<BANCONOM>HSBC BANK ARGENTINA (PRENDAS)</BANCONOM>
		<GRUPOHSBC>N</GRUPOHSBC>
		<ACCESO>
			<MMENU>050000</MMENU>
			<MMENU>100000</MMENU>
			<MMENU>200000</MMENU>
			<MMENU>300000</MMENU>
			<MMENU>350000</MMENU>
			<MMENU>400000</MMENU>
			<MMENU>450000</MMENU>
			<MMENU>460000</MMENU>
			<MMENU>470000</MMENU>
			<MMENU>480000</MMENU>
			<MMENU>490000</MMENU>
			<MMENU>500000</MMENU>
			<MMENU>550000</MMENU>
			<MMENU>600000</MMENU>
			<MMENU>051000</MMENU>
			<MMENU>052000</MMENU>
			<MMENU>053000</MMENU>
			<MMENU>054000</MMENU>
			<MMENU>054100</MMENU>
			<MMENU>054500</MMENU>
			<MMENU>101000</MMENU>
			<MMENU>102000</MMENU>
			<MMENU>103000</MMENU>
			<MMENU>104000</MMENU>
			<MMENU>105000</MMENU>
			<MMENU>106000</MMENU>
			<MMENU>107000</MMENU>
			<MMENU>108000</MMENU>
			<MMENU>201000</MMENU>
			<MMENU>202000</MMENU>
			<MMENU>203000</MMENU>
			<MMENU>204000</MMENU>
			<MMENU>205000</MMENU>
			<MMENU>301000</MMENU>
			<MMENU>302000</MMENU>
			<MMENU>302500</MMENU>
			<MMENU>303000</MMENU>
			<MMENU>304000</MMENU>
			<MMENU>305000</MMENU>
			<MMENU>306000</MMENU>
			<MMENU>351000</MMENU>
			<MMENU>352000</MMENU>
			<MMENU>353000</MMENU>
			<MMENU>451000</MMENU>
			<MMENU>452000</MMENU>
			<MMENU>453000</MMENU>
			<MMENU>454000</MMENU>
			<MMENU>455000</MMENU>
			<MMENU>456000</MMENU>
			<MMENU>501000</MMENU>
			<MMENU>502000</MMENU>
			<MMENU>551000</MMENU>
		</ACCESO>
		<EMISIONES>
			<EMISION>
				<RAMOPCOD>AUS1</RAMOPCOD>
			</EMISION>
			<EMISION>
				<RAMOPCOD>AUS1</RAMOPCOD>
			</EMISION>
		</EMISIONES>
	</Response>
</novServiceResponse>
```
