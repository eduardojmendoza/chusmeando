tests locales

perfilUsuario

curl -v -H "Accept: application/json" -X POST "http://localhost:7001/NOVServices/rest/v1/ConsultaMQGestionAPI/perfilUsuario?AplicarXSL=1000_PerfilUsuario.xsl&Usuario=EX009005L"

curl -v -H "Accept: application/xml" -X POST "http://localhost:7001/NOVServices/rest/v1/ConsultaMQGestionAPI/perfilUsuario?AplicarXSL=1000_PerfilUsuario.xsl&Usuario=EX009005L"

getActividadOcupacion

curl -v -H "Accept: application/json" -X GET "http://localhost:7001/NOVServices/rest/v1/ConsultaMQGestionAPI/getActividadOcupacion?Buscar=SISTEMAS"

curl -v -H "Accept: application/xml" -X GET "http://localhost:7001/NOVServices/rest/v1/ConsultaMQGestionAPI/getActividadOcupacion?Buscar=SISTEMAS"


CampanasHabilitadasProductor

curl -v -H "Accept: application/xml" -X GET "http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/getCampanasHabilitadasProductor?usuarcod=EX009005L&ramopcod=AUS1&agentcla=PR&agentcod=3233"


1510_GetDispRastreo

curl -v -H "Accept: application/xml" -X GET "http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/getDispRastreo?usuario=EX9017L&ramopcod=APR1&modeautcod=00001000100004700001N&sumaseg=3850000&provi=13&localidadcod=2000&opcion=S&operacion=C&prescod=&instala"


FormasDePagoxCampana

curl -v -H "Accept: application/xml" -X GET "http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/getFormasDePagoxCampana?usuarcod=EX009000L&ramopcod=AUS1&polizann=00&polizsec=000001&campacod=0225"


ListadoVehiculosxProd

curl -v -H "Accept: application/xml" -X GET "http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/getListadoVehiculosxProd?usuarcod=EX009011L&aumarcod=00002&auvehdes=FOCUS&procann=2012&procmes=12&procdia=10&auprocod=02"


productosPorProductor

curl -v -H "Accept: application/json" -X POST "http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/productosPorProductor?usuarcod=EX009005L"

curl -v -H "Accept: application/xml" -X POST "http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/productosPorProductor?usuarcod=EX009005L"


ProductoresHabilitadosParaCotizar

curl -v -H "Accept: application/json" -X POST "http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/productoresHabilitadosParaCotizar?aplicarXSL=ProductoresHabilitadosParaCotizarCombo.xsl&ramopcod=AUS1&usuarcod=EX009005L&cliensecas=100029438&nivelclas=GO"

curl -v -H "Accept: application/xml" -X POST "http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/productoresHabilitadosParaCotizar?aplicarXSL=ProductoresHabilitadosParaCotizarCombo.xsl&ramopcod=AUS1&usuarcod=EX009005L&cliensecas=100029438&nivelclas=GO"

curl -v -H "Accept: application/json" -X POST "http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/productoresHabilitadosParaCotizar?aplicarXSL=ProductoresHabilitadosParaCotizarXML.xsl&ramopcod=AUS1&usuarcod=EX009005L&cliensecas=100029438&nivelclas=GO"

curl -v -H "Accept: application/xml" -X POST "http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/productoresHabilitadosParaCotizar?aplicarXSL=ProductoresHabilitadosParaCotizarXML.xsl&ramopcod=AUS1&usuarcod=EX009005L&cliensecas=100029438&nivelclas=GO"

getOrganizadores

curl -v -H "Accept: application/json" -X POST "http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/getOrganizadores?usuario=EX009005L&agenteclase=PR&agentecodigo=1670"

curl -v -H "Accept: application/xml" -X POST "http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/getOrganizadores?usuario=EX009005L&agenteclase=PR&agentecodigo=1670"

renovacionPorUsuario

curl -X POST --header "Content-Type: application/xml" --header "Accept: application/xml" "http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/renovacionPorUsuario?USUARCOD=EX009005L"

curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" "http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/renovacionPorUsuario?USUARCOD=EX009005L"



-----------------


En Test QBE

perfilUsuario

curl -v -H "Accept: application/json" -X POST "http://10.1.10.30:8080/NOVServices/rest/v1/ConsultaMQGestionAPI/perfilUsuario?AplicarXSL=1000_PerfilUsuario.xsl&Usuario=EX009005L"

curl -v -H "Accept: application/xml" -X POST "http://10.1.10.30:8080/NOVServices/rest/v1/ConsultaMQGestionAPI/perfilUsuario?AplicarXSL=1000_PerfilUsuario.xsl&Usuario=EX009005L"

productosPorProductor

curl -v -H "Accept: application/json" -X POST "http://10.1.10.30:8080/NOVServices/rest/v1/ConsultaMQAPI/productosPorProductor?usuarcod=EX009005L"

curl -v -H "Accept: application/xml" -X POST "http://10.1.10.30:8080/NOVServices/rest/v1/ConsultaMQAPI/productosPorProductor?usuarcod=EX009005L"

ProductoresHabilitadosParaCotizar

curl -v -H "Accept: application/json" -X POST "http://10.1.10.30:8080/NOVServices/rest/v1/ConsultaMQAPI/productoresHabilitadosParaCotizar?aplicarXSL=ProductoresHabilitadosParaCotizarCombo.xsl&ramopcod=AUS1&usuarcod=EX009005L&cliensecas=100029438&nivelclas=GO"

curl -v -H "Accept: application/xml" -X POST "http://10.1.10.30:8080/NOVServices/rest/v1/ConsultaMQAPI/productoresHabilitadosParaCotizar?aplicarXSL=ProductoresHabilitadosParaCotizarCombo.xsl&ramopcod=AUS1&usuarcod=EX009005L&cliensecas=100029438&nivelclas=GO"

curl -v -H "Accept: application/json" -X POST "http://10.1.10.30:8080/NOVServices/rest/v1/ConsultaMQAPI/productoresHabilitadosParaCotizar?aplicarXSL=ProductoresHabilitadosParaCotizarXML.xsl&ramopcod=AUS1&usuarcod=EX009005L&cliensecas=100029438&nivelclas=GO"

curl -v -H "Accept: application/xml" -X POST "http://10.1.10.30:8080/NOVServices/rest/v1/ConsultaMQAPI/productoresHabilitadosParaCotizar?aplicarXSL=ProductoresHabilitadosParaCotizarXML.xsl&ramopcod=AUS1&usuarcod=EX009005L&cliensecas=100029438&nivelclas=GO"

getOrganizadores

curl -v -H "Accept: application/json" -X POST "http://10.1.10.30:8080/NOVServices/rest/v1/ConsultaMQAPI/getOrganizadores?usuario=EX009005L&agenteclase=PR&agentecodigo=1670"

curl -v -H "Accept: application/xml" -X POST "http://10.1.10.30:8080/NOVServices/rest/v1/ConsultaMQAPI/getOrganizadores?usuario=EX009005L&agenteclase=PR&agentecodigo=1670"



getcotiaus
curl -v --header "Content-Type: application/json" -H "Accept: application/json" -X POST -d @getcotiaus.json "http://10.1.10.30:8080/NOVServices/rest/v1/OVMQCotizarAPI/getCotiAUS"



curl -X POST -v -H "Accept: application/xml" --header "Content-Type: application/xml" -d @2120.xml http://localhost:7001/NOVServices/rest/v1/ConsultaMQAPI/planPagoxProdPolMedCobro


