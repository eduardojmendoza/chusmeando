package com.qbe.services.nov.impl;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.qbe.services.nov.jaxb.mqemision.putsolicaus.PutSolicAUSRequest;
import com.qbe.services.nov.rest.NovRestServicesUtil;

public class PutSolicAUSUtil {

	public static void main(String[] args) throws PropertyException, JAXBException {
		PutSolicAUSUtil util = new PutSolicAUSUtil();
		try {
			util.generateJAXB();
			util.generateJSON();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void generateJSON() throws PropertyException, JAXBException, IOException {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("putSolicAUS_request_1.xml");
		
        PutSolicAUSRequest requestFromXml = (PutSolicAUSRequest) NovRestServicesUtil.unmarshalFromXml(PutSolicAUSRequest.class, IOUtils.toString(is));
		String requestJson = NovRestServicesUtil.marshalAsJson(PutSolicAUSRequest.class, requestFromXml);
		System.out.println(requestJson);
	}

	private void generateJAXB() throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("putSolicAUS_request_1.xml");
		
		Document doc = db.parse(is);
		NodeList requests = doc.getElementsByTagName("Request");
		if ( requests.getLength() != 1) {
			throw new IOException("Invalid Request source");
		}
		
		String fieldPattern = 
				"    @XmlElement(name=\"{0}\")\n" +	
				"    public String {1};\n\n";
		MessageFormat fieldMF = new MessageFormat(fieldPattern);

//		@XmlElementWrapper( name = "ACCESORIOS", required = false)
//	    @XmlElement(name="ACCESORIO")	
//		public List<Accesorio> accesorios;
//		
//
		String listPattern = 
				"    @XmlElementWrapper( name = \"{0}\", required = false)\n" +	
				"    @XmlElement(name=\"{1}\")\n" +
				"    public List<{2}> {3};\n\n";
		MessageFormat listMF = new MessageFormat(listPattern);

		Element request = (Element)requests.item(0);
		NodeList fields = request.getChildNodes();
		for (int i = 0; i < fields.getLength(); i++) {
			if ( fields.item(i).getNodeType() == Element.ELEMENT_NODE) {
				Element field = (Element)fields.item(i);
				if ( field.hasChildNodes() && field.getChildNodes().getLength() == 1 && field.getFirstChild().getNodeType() == Element.TEXT_NODE) {
					String tagName = field.getNodeName();
					String javaField = tagName.toLowerCase();
					String[] arguments = new String[] { tagName, javaField };
					String javaEntry = fieldMF.format(arguments, new StringBuffer(), null).toString();
					System.out.println(javaEntry);
					
				} else if ( field.hasChildNodes() && field.getChildNodes().getLength() > 1  ) {
					String wrapperTag = field.getNodeName();
					NodeList wrapperChildren = field.getChildNodes();
					for (int j = 0; j < wrapperChildren.getLength(); j++) {
						if ( wrapperChildren.item(j).getNodeType() == Element.ELEMENT_NODE) {
							String elementTag = wrapperChildren.item(j).getNodeName();
							String[] arguments = new String[] { wrapperTag, elementTag, StringUtils.capitalize(elementTag.toLowerCase()), elementTag.toLowerCase() };
							String javaEntry = listMF.format(arguments, new StringBuffer(), null).toString();
							System.out.println(javaEntry);
							break;
						}
					}
					
				} else {
					String tagName = field.getNodeName();
					String javaField = tagName.toLowerCase();
					String[] arguments = new String[] { tagName, javaField };
					String javaEntry = fieldMF.format(arguments, new StringBuffer(), null).toString();
					System.out.println(javaEntry);
				}
				
			}
			
		}
			
	}

}
