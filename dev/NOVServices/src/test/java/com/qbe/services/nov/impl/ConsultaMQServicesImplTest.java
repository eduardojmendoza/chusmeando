package com.qbe.services.nov.impl;

import static org.junit.Assert.*;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.nov.GenericServiceResponse;

public class ConsultaMQServicesImplTest {

	final String REQUEST_1101 = "<Request><DEFINICION>1101_OVDatosGralPoliza.xml</DEFINICION><RAMOPCOD>AUT1</RAMOPCOD><POLIZANN>01</POLIZANN><POLIZSEC>086519</POLIZSEC><CERTIPOL>0</CERTIPOL><CERTIANN>0</CERTIANN><CERTISEC>0</CERTISEC><SUPLENUM>0</SUPLENUM></Request>";
	final String REQUEST_1101_RESPONSE = "<Response><Estado resultado='true' mensaje='' /><CAMPOS>\n" + 
			"   <OPERPTE/>\n" + 
			"   <CLIENSEC>943887</CLIENSEC>\n" + 
			"   <CLIENDES>MINA PIRQUITAS, LLC SUCURSAL A</CLIENDES>\n" + 
			"   <DOMICSEC>005</DOMICSEC>\n" + 
			"   <CUENTSEC>001</CUENTSEC>\n" + 
			"   <SITUCPOL>VIGENTE</SITUCPOL>\n" + 
			"   <ACREEDOR>NO</ACREEDOR>\n" + 
			"   <FECINI>21/03/2018</FECINI>\n" + 
			"   <FECULTRE>21/03/2018 - 21/03/2019</FECULTRE>\n" + 
			"   <COBROCOD>1</COBROCOD>\n" + 
			"   <COBRODAB>CAJA COMPAÑIA</COBRODAB>\n" + 
			"   <COBROTIP>EF</COBROTIP>\n" + 
			"   <COBRODES>EFECTIVO</COBRODES>\n" + 
			"   <CUENTNUM/>\n" + 
			"   <CAMPACOD/>\n" + 
			"   <CAMPADES/>\n" + 
			"   <GRUPOASE>MINA PIRQUITAS LLC. SUCURSAL ARGENTINA,</GRUPOASE>\n" + 
			"   <CLUBECO>N</CLUBECO>\n" + 
			"   <TARJECOD>0</TARJECOD>\n" + 
			"   <OPTP6511/>\n" + 
			"   <SWSCOSIM/>\n" + 
			"</CAMPOS></Response>";
	
	
	
	final String REQUEST_1101_SOLORETORNARAREA = "<Request><SOLORETORNARAREA/><DEFINICION>1101_OVDatosGralPoliza.xml</DEFINICION><RAMOPCOD>AUT1</RAMOPCOD><POLIZANN>01</POLIZANN><POLIZSEC>086519</POLIZSEC><CERTIPOL>0</CERTIPOL><CERTIANN>0</CERTIANN><CERTISEC>0</CERTISEC><SUPLENUM>0</SUPLENUM></Request>";
	final String REQUEST_1101_RESPONSE_SOLORETORNARAREA = "<Response><Estado resultado=\"false\" mensaje=\"No se Procesa por la existencia del TAG SOLORETORNARAREA\" />" + 
			"<AreaIN>11010001              000000000    000000000  000000000  000000000AUT101086519000000000000000000</AreaIN>" + 
			"</Response>";
	
	
	protected ConsultaMQServicesImpl serviceImpl = null;
	
	@Before
	public void setup() {
		this.serviceImpl = new ConsultaMQServicesImpl();
	}
	
	@Test
	public void testDatosGralPolizaResp() throws Exception {
		GenericServiceResponse gsr = this.serviceImpl.datosGralPoliza(REQUEST_1101);
		String response = gsr.getResponse();
		
		assertEquals("Difieren", StringUtils.deleteWhitespace(REQUEST_1101_RESPONSE), StringUtils.deleteWhitespace(response));
	}
	
	@Test
	@Ignore
	public void testDatosGralPolizaArea() throws Exception {
		
		class Go1101 implements Runnable {
			Go1101() {
			}

			public void run() {
				ConsultaMQServicesImpl serviceImpl = new ConsultaMQServicesImpl();
				GenericServiceResponse gsr;
				try {
					gsr = serviceImpl.datosGralPoliza(REQUEST_1101_SOLORETORNARAREA);
					String response = gsr.getResponse();
					assertEquals("Difieren", REQUEST_1101_RESPONSE_SOLORETORNARAREA, response);
				} catch (Exception e) {
					fail();					
				}
			}
		}

		for (int j = 0; j < 10; j++) {
			for (int i = 0; i < 200; i++) {
				Go1101 p = new Go1101();
				new Thread(p).start();
			}
			Thread.sleep(1000);
		}
	}

}
