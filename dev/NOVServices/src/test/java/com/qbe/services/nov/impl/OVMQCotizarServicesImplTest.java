package com.qbe.services.nov.impl;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import com.qbe.services.nov.jaxb.mqcotizar.getcotiaus.GetCotiAUS;
import com.qbe.services.nov.jaxb.mqcotizar.getcotiaus.GetCotiAUSRequest;
import com.qbe.services.nov.rest.NovRestServicesUtil;

public class OVMQCotizarServicesImplTest {

	@Test
	public void testGetCotiAUSUnmarshal() throws IOException, JAXBException {
		
		String resultXML = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("getcotiausresponse_1.xml"), "UTF-8");
		
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(GetCotiAUS.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        GetCotiAUS result = (GetCotiAUS) unmarshaller.unmarshal(new StringReader(resultXML));
        Assert.assertEquals(result.ageCod, "3233");
        Assert.assertEquals(result.sumalba, "00020100000");
        
        Assert.assertEquals(result.planes.get(0).planndes, "Responsabilidad Civil/FRANQUICIA NO APLICABLE");
	}

	@Test
	public void testFromXmlToJson() throws IOException, JAXBException {
		
		String requestXML = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("getcotiaus_request_1.xml"), "UTF-8");
		GetCotiAUSRequest requestFromXml = (GetCotiAUSRequest) NovRestServicesUtil.unmarshalFromXml(GetCotiAUSRequest.class, requestXML);
		String json = NovRestServicesUtil.marshalAsJson(GetCotiAUSRequest.class, requestFromXml, false);
		System.out.println(json);
		GetCotiAUSRequest requestFromJson = (GetCotiAUSRequest) NovRestServicesUtil.unmarshalFromJson(GetCotiAUSRequest.class, json);
        Assert.assertEquals(requestFromJson.modeautcod, "00010000270000304338N");
        Assert.assertEquals(requestFromJson.hijos.get(0).nacimhijo, "01011994");
        
	}

	
	
}
