package com.qbe.services.nov.jaxb.mqmensaje.getorganizadores;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;

/**
 * 
 * 
    <Response>
        <Estado resultado="true" mensaje=""/>
        <CAMPOS>
            <RS>
                <ORGANIZADOR>
                    <CODIGO>50</CODIGO>
                    <CLASE>OR</CLASE>
                    <NOMBRE><![CDATA[AON RISK SERVICES  ARGENTINA S(OR-0050)]]></NOMBRE>
                </ORGANIZADOR>
                <ORGANIZADOR>
                    <CODIGO>4810</CODIGO>
                    <CLASE>OR</CLASE>
                    <NOMBRE><![CDATA[HSBC BANK ARGENTINA   S.A     (OR-4810)]]></NOMBRE>
                </ORGANIZADOR>
            </RS>
        </CAMPOS>
    </Response>
    
 * 
 * @author ramiro
 *
 */
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetOrganizadores implements ResponseWithEstado {

	@XmlElement( name = "Estado", required = true)
	public Estado estado;
	
	@XmlElement( name = "CAMPOS", required = true)
	private Campos campos;

	@Override
	public Estado getEstado() {
		return estado;
	}
}
