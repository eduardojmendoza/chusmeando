package com.qbe.services.nov.impl;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.cotizadorEndosos.impl.LBAWA_OVCotEndosos;
import com.qbe.services.cotizadorEndosos.impl.Lbaw_OVDatosRiesgoHogar;
import com.qbe.services.mqgeneric.impl.LBAW_MQMensajeGestion;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.vbcompat.string.StringHolder;

public class LBAWA_OVCotEndososServicesImpl {


	/**
	 * Implementación del servicio
	 * 
	 * @author cbonilla
	 *
	 */

		private static final String EMPTY_STRING = "";

		protected Logger logger = Logger.getLogger(getClass().getName());
		
		public LBAWA_OVCotEndososServicesImpl() {
		}

		
		public GenericServiceResponse cotizadorEndosos(String request) throws Exception {
			LBAWA_OVCotEndosos comp = new LBAWA_OVCotEndosos();
			StringHolder sh = new StringHolder();
			int resultCode = comp.IAction_Execute(request, sh, EMPTY_STRING);
			if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente CotizadorEndosos");
			return new GenericServiceResponse(resultCode,sh.getValue());
		}
		

		public GenericServiceResponse datosGeneralesPolizaHogar(String request) throws Exception {
			LBAWA_OVCotEndosos comp = new LBAWA_OVCotEndosos();			
			StringHolder sh = new StringHolder();
			logger.log(Level.FINE, "Recibido request: " + request);
			int resultCode = comp.IAction_Execute(request, sh, EMPTY_STRING);
			logger.log(Level.FINE, "Resultado de ejecución de datosGeneralesPolizaHogar: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
			if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
			return new GenericServiceResponse(resultCode,sh.getValue());
		}
		
		public GenericServiceResponse precioUltimaFacturacionHogar(String request) throws Exception {
			LBAWA_OVCotEndosos comp = new LBAWA_OVCotEndosos();			
			StringHolder sh = new StringHolder();
			logger.log(Level.FINE, "Recibido request: " + request);
			int resultCode = comp.IAction_Execute(request, sh, EMPTY_STRING);
			logger.log(Level.FINE, "Resultado de ejecución de precioUltimaFacturacionHogar: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
			if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
			return new GenericServiceResponse(resultCode,sh.getValue());
		}
		
		
		public GenericServiceResponse getDatosRiesgoHogar(String request) throws Exception {
			LBAWA_OVCotEndosos comp = new LBAWA_OVCotEndosos();			
			StringHolder sh = new StringHolder();
			logger.log(Level.FINE, "Recibido request: " + request);
			int resultCode = comp.IAction_Execute(request, sh, EMPTY_STRING);
			logger.log(Level.FINE, "Resultado de ejecución de getDatosRiesgoHogar: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
			if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
			return new GenericServiceResponse(resultCode,sh.getValue());
		}
		
		/**
		 * @param request
		 * @return
		 * @throws Exception
		 */
		public GenericServiceResponse getDatosRiesgoHogarManual(String request) throws Exception {
			Lbaw_OVDatosRiesgoHogar comp = new Lbaw_OVDatosRiesgoHogar();			
			StringHolder sh = new StringHolder();
			logger.log(Level.FINE, "Recibido request: " + request);
			int resultCode = comp.IAction_Execute(request, sh, EMPTY_STRING);
			logger.log(Level.FINE, "Resultado de ejecución de getDatosRiesgoHogar: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
			if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
			return new GenericServiceResponse(resultCode,sh.getValue());
		}
		
		

}
