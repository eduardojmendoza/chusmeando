package com.qbe.services.nov.jaxb.lbawebmq.getsumaasegurada;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
            <OPTION value="2012" sumaseg="142000,00">2012</OPTION>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Option {
	
    @XmlAttribute(name="value")	
	public String value;
	
    @XmlAttribute(name="sumaseg")	
	public String sumaseg;
	
    @XmlValue
	// Ignorado para jackson porque no interpreta bien Option referenciado desde GetZonaResponse. Como tiene un atributo value y un @XmlValue, tira: Multiple fields representing property "value":
    @JsonIgnore
	public String texto;
    
}
