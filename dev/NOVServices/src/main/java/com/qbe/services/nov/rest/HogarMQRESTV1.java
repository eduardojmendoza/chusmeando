package com.qbe.services.nov.rest;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qbe.services.exutils.ExceptionUtils;
import com.qbe.services.nov.AisMqService;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.impl.HogarMQServicesImpl;

import io.swagger.annotations.Api;

/**
 * 
 * @author ramiro
 *
 */
@Path("/v1/HogarMQAPI")
@Api(value="HogarMQ")
public class HogarMQRESTV1 extends AisMqService {

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	protected HogarMQServicesImpl servicesImpl;
	
	public HogarMQRESTV1() {
	}
	
	public HogarMQServicesImpl getServicesImpl() {
		if (servicesImpl == null ) {
			this.servicesImpl = (HogarMQServicesImpl)getContext().getBean("HogarMQServices");
		}
		return servicesImpl;
	}
	
	@POST
	@Path("/getPolCamp")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getPolCamp(String request)  {
		
		GenericServiceResponse result;
		try {
			result = getServicesImpl().getPolCamp(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/getPolCob")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getPolCob(String request)  {
		
		GenericServiceResponse result;
		try {
			result = getServicesImpl().getPolCob(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	@POST
	@Path("/getPolPlanes")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getPolPlanes(String request)  {
		
		GenericServiceResponse result;
		try {
			result = getServicesImpl().getPolPlanes(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	@POST
	@Path("/getCotiz")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getCotiz(String request)  {
		
		GenericServiceResponse result;
		try {
			result = getServicesImpl().getCotiz(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
		
	
	
}