package com.qbe.services.nov.jaxb.lbawebmq.getsumaasegurada;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;

/**
*
* Respuesta recibida del MQMensaje
* 
    <Response>
        <Estado resultado="true" mensaje=""/>
        <AUTIPCOD>0148</AUTIPCOD>
        <AUTIPCOM>N</AUTIPCOM>
        <AUTIPDES>SEDAN 5 PUERTAS               </AUTIPDES>
        <AUCOMDES>     NAFTA                                                  </AUCOMDES>
        <AUCATCOD>14</AUCATCOD>
        <COMBO>
            <OPTION value="2012" sumaseg="142000,00">2012</OPTION>
            <OPTION value="2013" sumaseg="148000,00">2013</OPTION>
            <OPTION value="2014" sumaseg="154000,00">2014</OPTION>
            <OPTION value="2015" sumaseg="183000,00">2015</OPTION>
        </COMBO>
    </Response>
* 
* 
* @author ramiro
*
*/
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetSumaAsegurada implements ResponseWithEstado {

	@XmlElement( name = "Estado", required = true)
	public Estado estado;

	@XmlElement( name = "AUTIPCOD", required = true)
	public String autipcod;

	@XmlElement( name = "AUTIPCOM", required = true)
	public String autipcom;

	@XmlElement( name = "AUTIPDES", required = true)
	public String autipdes;

	@XmlElement( name = "AUCOMDES", required = true)
	public String aucomdes;

	@XmlElement( name = "AUCATCOD", required = true)
	public String aucatcod;

    @XmlElementWrapper(name="COMBO")
	@XmlElement(name="OPTION")	
	public List<Option> options;

	
	public GetSumaAsegurada() {
	}

	@Override
	public Estado getEstado() {
		return estado;
	}

}
