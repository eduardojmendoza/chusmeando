package com.qbe.services.nov.jaxb.mqecoupons.getpolaus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
       <PARAMETROS>
         <CLIENSEC>000412335</CLIENSEC>
         <TIPOSEG>AUS1</TIPOSEG>
         <POLIZA>0001856577</POLIZA>
       <PARAMETROS>
 * 
 * @author jose
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Parametro {
	 
	@XmlElement(name="CLIENSEC")	
	public String cliensec;
	
    @XmlElement(name="TIPOSEG")	
	public String tiposeg;
	
    @XmlElement(name="POLIZA")	
	public String poliza;

}
