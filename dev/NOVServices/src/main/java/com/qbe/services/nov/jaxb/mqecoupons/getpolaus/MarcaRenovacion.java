package com.qbe.services.nov.jaxb.mqecoupons.getpolaus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
*
* Respuesta recibida del MQMensaje
* 
 
       <MARCA_RENOVACION desc='Poliza Anterior'>2</MARCA_RENOVACION>

* 
* 
* @author jose
*
*/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "MARCA_RENOVACION")
public class MarcaRenovacion {

	 @XmlAttribute(name="desc")	
	 public String desc;
	 
	 @XmlValue
	 public String texto;
}
