package com.qbe.services.nov.jaxb.mqcotizar.getcotiaus;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
*
* Request para GetCotiAUS
* 
<Request>
	<NACIMANN>1978</NACIMANN>
	<CLUBECO>S</CLUBECO>
	<GRANIZO>N</GRANIZO>
	<ROBOCONT>N</ROBOCONT>
	<NACIMMES>12</NACIMMES>
	<NACIMDIA>12</NACIMDIA>
	<SEXO>F</SEXO>
	<ESTADO>S</ESTADO>
	<MODEAUTCOD>00010000270000304338N</MODEAUTCOD>
	<KMSRNGCOD>30000</KMSRNGCOD>
	<EFECTANN>2015</EFECTANN>
	<SIGARAGE>S</SIGARAGE>
	<SINIESTROS>0</SINIESTROS>
	<GAS>S</GAS>
	<PROVI>1</PROVI>
	<LOCALIDADCOD>1428</LOCALIDADCOD>
	<CLUBLBA>S</CLUBLBA>
	<CAMPACOD>1681</CAMPACOD>
	<AGECOD>3233</AGECOD>
	<AGECLA>PR</AGECLA>
	<COBROCOD>1</COBROCOD>
	<COBROTIP>PF</COBROTIP>
	<LUNETA>N</LUNETA>
	<IVA>3</IVA>
	<IBB>0</IBB>
	<CLIENTIP />
	<DATOSPLAN>0000</DATOSPLAN>
	<ESCERO>S</ESCERO>
	<DESTRUCCION_80>S</DESTRUCCION_80>
	<HIJOS>
		<HIJO>
			<NACIMHIJO>01011994</NACIMHIJO>
			<EDADHIJO>21</EDADHIJO>
			<SEXOHIJO>F</SEXOHIJO>
			<ESTADOHIJO>S</ESTADOHIJO>
			<INCLUIDO>1</INCLUIDO>
		</HIJO>
	</HIJOS>
	<ACCESORIOS>
		<ACCESORIO>
			<CODIGOACC>5</CODIGOACC>
			<DESCRIPCIONACC><![CDATA[EQUIPO GNC: GAS S.A.]]></DESCRIPCIONACC>
			<PRECIOACC>8000</PRECIOACC>
		</ACCESORIO>
	</ACCESORIOS>
</Request>* 
* 
* @author ramiro
*
*/
@XmlRootElement( name = "Request")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetCotiAUSRequest {
	
    @XmlElement(name="NACIMANN")	
	public String nacimann;

    @XmlElement(name="CLUBECO")	
	public String clubeco;

    @XmlElement(name="GRANIZO")	
	public String granizo;

    @XmlElement(name="ROBOCONT")	
	public String robocont;

    @XmlElement(name="NACIMMES")	
	public String nacimmes;

    @XmlElement(name="NACIMDIA")	
	public String nacimdia;

    @XmlElement(name="SEXO")	
	public String sexo;

    @XmlElement(name="ESTADO")	
	public String estado;

    @XmlElement(name="MODEAUTCOD")	
	public String modeautcod;

    @XmlElement(name="KMSRNGCOD")	
	public String kmsrngcod;

    @XmlElement(name="EFECTANN")	
	public String efectann;

    @XmlElement(name="SIGARAGE")	
	public String sigarage;

    @XmlElement(name="SINIESTROS")	
	public String siniestros;

    @XmlElement(name="GAS")	
	public String gas;

    @XmlElement(name="PROVI")	
	public String provi;

    @XmlElement(name="LOCALIDADCOD")	
	public String localidadcod;

    @XmlElement(name="CLUBLBA")	
	public String clublba;

    @XmlElement(name="CAMPACOD")	
	public String campacod;

    @XmlElement(name="AGECOD")	
	public String agecod;

    @XmlElement(name="AGECLA")	
	public String agecla;

    @XmlElement(name="COBROCOD")	
	public String cobrocod;

    @XmlElement(name="COBROTIP")	
	public String cobrotip;

    @XmlElement(name="LUNETA")	
	public String luneta;

    @XmlElement(name="IVA")	
	public String iva;

    @XmlElement(name="IBB")	
	public String iibb;

    @XmlElement(name="CLIENTIP")	
	public String clientip;

    @XmlElement(name="DATOSPLAN")	
	public String datosplan;

    @XmlElement(name="ESCERO")	
	public String escero;

    @XmlElement(name="DESTRUCCION_80")	
	public String destruccion80;
	
	@XmlElementWrapper( name = "ACCESORIOS", required = false)
    @XmlElement(name="ACCESORIO")	
	public List<Accesorio> accesorios;
	
	@XmlElementWrapper( name = "HIJOS", required = false)
    @XmlElement(name="HIJO")	
	public List<Hijo> hijos;
	
	public GetCotiAUSRequest() {
	}

}
