package com.qbe.services.nov.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.Contact;
import io.swagger.annotations.ExternalDocs;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;

@SwaggerDefinition(
        info = @Info(
                description = "Servicios de la Nueva Oficina Virtual migrados de COM+",
                version = "latest",
                title = "NOV Services API",
                contact = @Contact(
                   name = "Snoop Consulting", 
                   email = "ramiro@snoopconsulting.com"
                )
        ),
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"},
        schemes = {SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTPS},
        tags = {
                @Tag(name = "ConsultaMQ", description = "Migradas del COM+ lbaw_MQMensaje")
                ,@Tag(name = "ConsultaMQGestion", description = "Migradas del COM+ lbaw_MQMensajeGestion")
                ,@Tag(name = "OVMQUsuario", description = "Migradas del COM+ OVMQUsuario")
                
                
                
        }
//        ,externalDocs = @ExternalDocs(value = "Repositorio github", url = "https://github.com/snoopconsulting/migracioncomplus")
)
@Api
public interface SwaggerAPIConfig {

}
