package com.qbe.services.nov.rest;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qbe.services.exutils.ExceptionUtils;
import com.qbe.services.nov.AisMqService;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.impl.OVMQRiesgosServicesImpl;
import com.qbe.services.nov.impl.OVMQSoloServicesImpl;


import io.swagger.annotations.Api;

/**
 * 
 * @author cbonilla
 * 
 */

@Path("/v1/OVMQRiesgosAPI")
@Api(value="OVMQRiesgos")
public class OVMQRiesgosRESTV1 extends AisMqService{

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	private OVMQRiesgosServicesImpl getServicesImpl(){
		OVMQRiesgosServicesImpl ovmq = new OVMQRiesgosServicesImpl();
		return ovmq;
	}
	
	@POST
	@Path("/riesComercio")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response riesComercio(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().riesComercio(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		} 
	}
	
	
	
	@POST
	@Path("/riesHogMasivo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response riesHogMasivo(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().riesHogMasivo(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		} 
	}
	
	
	@POST
	@Path("/riesAutos")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response riesAutos(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().riesAutos(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		} 
	}
	
	
	
	@POST
	@Path("/riesEdificio")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response riesEdificio(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().riesEdificio(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		} 
	}
	
	@POST
	@Path("/riesTarjetas")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response riesTarjetas(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().riesTarjetas(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		} 
	}
	
	
	@POST
	@Path("/riesCascos")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response riesCascos(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().riesCascos(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		} 
	}
	
	@POST
	@Path("/riesAgricola")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response riesAgricola(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().riesAgricola(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		} 
	}
	
	
	@POST
	@Path("/riesAereo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response riesAereo(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().riesAereo(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		} 
	}
	
	
	@POST
	@Path("/riesEquiElect")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response riesEquiElect(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().riesEquiElect(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		} 
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/riesViajes")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response riesViajes(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().riesViajes(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		} 
	}
	
	
		@GET
	@Path("/version")
	public Response getVersion() throws Exception {
		try {
			return Response
					.status(Response.Status.OK)
					.header("VERSION", "1")
					.build();
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(Response.Status.PRECONDITION_FAILED)
					.entity(e.getMessage()).build();
		}
	}


}
