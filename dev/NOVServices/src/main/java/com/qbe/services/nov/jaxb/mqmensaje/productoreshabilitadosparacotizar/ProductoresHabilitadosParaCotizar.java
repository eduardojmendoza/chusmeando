package com.qbe.services.nov.jaxb.mqmensaje.productoreshabilitadosparacotizar;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;
import com.qbe.services.nov.jaxb.mqmensaje.productosporproductor.Producto;

/**
 *
 * Respuesta recibida del MQMensaje
 * 
 * Si usamos ProductoresHabilitadosParaCotizarCombo.xsl

    <Response>
        <Estado resultado="true" mensaje=""/>
        <option value="5380|PR|ABELENDA  GUSTAVO DANIEL" agentecodigo="5380">5380|PR|ABELENDA  GUSTAVO DANIEL</option>
        <option value="2383|PR|ANIDO  PABLO ALEJANDRO" agentecodigo="2383">2383|PR|ANIDO  PABLO ALEJANDRO</option>
        <option value="4164|PR|ASESORES SEGUROENLIN EA S.A." agentecodigo="4164">4164|PR|ASESORES SEGUROENLIN EA S.A.</option>
        <option value="4174|PR|ASESORES SEGUROENLIN EA S.A." agentecodigo="4174">4174|PR|ASESORES SEGUROENLIN EA S.A.</option>
        <option value="4175|PR|ASESORES SEGUROENLIN EA S.A." agentecodigo="4175">4175|PR|ASESORES SEGUROENLIN EA S.A.</option>
[....]


Si usamos ProductoresHabilitadosParaCotizarXML.xsl
<Response><Estado resultado='true' mensaje='' />
<ROOT>
   <AGENTE AGENTCLA="PR" AGENTCOD="1670"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="6662"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="0188"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="2000"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="7082"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="8713"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="7916"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="7933"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="8078"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="8165"/>
</ROOT>
</Response>

 * @author ramiro
 *
 */
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductoresHabilitadosParaCotizar implements ResponseWithEstado {
	@XmlElement( name = "Estado", required = true)
	public Estado estado;
	
    @XmlElement(name="option")	
	public List<Option> options;

	@XmlElementWrapper( name = "ROOT")
    @XmlElement(name="AGENTE")	
	public List<Agente> root;

    public ProductoresHabilitadosParaCotizar() {
	}

	@Override
	public Estado getEstado() {
		return estado;
	}
}
