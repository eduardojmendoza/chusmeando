package com.qbe.services.nov.jaxb.mqmensajegestion.actividadocupacion;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;
import com.qbe.services.nov.jaxb.mqmensaje.productosporproductor.Campos;
import com.qbe.services.nov.jaxb.mqmensaje.productosporproductor.Producto;

/**
*
* Respuesta recibida del MQMensaje
* 

<Response>
        <Estado resultado="true" mensaje=""/>
        <PROFESIONES>
            <PROFESION>
                <PROFECOD>000006</PROFECOD>
                <PROFEDES>INGENIERO DE SISTEMAS</PROFEDES>
            </PROFESION>
[..........................................]
            <PROFESION>
                <PROFECOD>005140</PROFECOD>
                <PROFEDES>SISTEMAS DE SEGURIDAD, INSTALACION DE</PROFEDES>
            </PROFESION>
            </Response>

	


* @author cesar
*
*/
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActividadOcupacion implements ResponseWithEstado {
	@XmlElement( name = "Estado", required = true)
	public Estado estado;
	
	@XmlElementWrapper( name = "PROFESIONES", required = true)
    @XmlElement(name="PROFESION")	
	public List<Profesion> profesiones;
		
	public ActividadOcupacion() {
	}

	@Override
	public Estado getEstado() {
		return estado;
	}

}
