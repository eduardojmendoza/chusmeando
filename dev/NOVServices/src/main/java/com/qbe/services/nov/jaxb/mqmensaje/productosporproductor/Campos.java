package com.qbe.services.nov.jaxb.mqmensaje.productosporproductor;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.jaxb.AnyXmlElement;

/**
 * 
 * 
		<PRODUCTOS>
			<PRODUCTO>

 * @author ramiro
 *
 * 
 * 
	<SALIDA>
		<AREA Nombre="CAMPOS" Pattern="(\S{3})(\S{8800})">
			<AREA Nombre="CANTLINPRO" Visible="NO" TipoDato="ENTERO"/>
			<AREA Nombre="PRODUCTOS" NombrePorOcurrencia="PRODUCTO" Pattern="(\S{4})(\S{40})(\S{20})(\S{4})">
				<AREA Nombre="RAMOPCOD" Visible="SI" TipoDato="TEXTO"/>
				<AREA Nombre="RAMOPDES" Visible="SI" TipoDato="TEXTO"/>
				<AREA Nombre="RAMOPDAB" Visible="SI" TipoDato="TEXTO"/>
				<AREA Nombre="MONENCOD" Visible="SI" TipoDato="TEXTO"/>
			</AREA>
		</AREA>
	</SALIDA>

 * 
 */
@XmlRootElement
//@XmlType(name = "NOVServiceResponse", propOrder = {
//	    "code", "response"
//	})
@XmlAccessorType(XmlAccessType.FIELD)
public class Campos {

	@XmlElementWrapper( name = "PRODUCTOS", required = true)
    @XmlElement(name="PRODUCTO")	
	public List<Producto> productos;
}
