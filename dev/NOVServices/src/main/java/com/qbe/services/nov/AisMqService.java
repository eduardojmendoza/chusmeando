package com.qbe.services.nov;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Define métodos comunes a las subclases concretas que implementan operaciones para acceder a los servicios de AIS via MQ
 * @author ramiro
 *
 */
public class AisMqService {

	protected static ApplicationContext context;

	protected static synchronized ApplicationContext getContext() {
	
		if ( context == null) {
			context = new ClassPathXmlApplicationContext("classpath*:spring-beans.xml");
		}
		return context;
	}

	public AisMqService() {
		super();
	}


}