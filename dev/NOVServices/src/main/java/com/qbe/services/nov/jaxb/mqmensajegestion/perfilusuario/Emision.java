package com.qbe.services.nov.jaxb.mqmensajegestion.perfilusuario;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Emision {
	
    @XmlElement(name="RAMOPCOD")	
	public String ramopcod;
	
}
