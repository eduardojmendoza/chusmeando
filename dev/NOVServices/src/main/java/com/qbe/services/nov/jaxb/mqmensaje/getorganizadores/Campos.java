package com.qbe.services.nov.jaxb.mqmensaje.getorganizadores;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

/**
 * 
 * 
        <CAMPOS>
            <RS>
                <ORGANIZADOR>
                    <CODIGO>50</CODIGO>
                    <CLASE>OR</CLASE>
                    <NOMBRE><![CDATA[AON RISK SERVICES  ARGENTINA S(OR-0050)]]></NOMBRE>
                </ORGANIZADOR>
                <ORGANIZADOR>
                    <CODIGO>4810</CODIGO>
                    <CLASE>OR</CLASE>
                    <NOMBRE><![CDATA[HSBC BANK ARGENTINA   S.A     (OR-4810)]]></NOMBRE>
                </ORGANIZADOR>
            </RS>
        </CAMPOS>
 * 
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Campos {

	@XmlElementWrapper( name = "RS", required = true)
    @XmlElement(name="ORGANIZADOR")	
	public List<Organizador> organizadores;
	
}
