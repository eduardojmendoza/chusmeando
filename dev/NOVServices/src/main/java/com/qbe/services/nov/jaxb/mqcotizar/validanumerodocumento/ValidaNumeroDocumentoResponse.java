package com.qbe.services.nov.jaxb.mqcotizar.validanumerodocumento;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.qbe.services.nov.ResponseWithEstado;
import com.qbe.services.nov.ServiceResponse;


/**
 * Wrapper que incluye el resultCode
 * 
 * 
 * 
 * 
 *  
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidaNumeroDocumentoResponse implements ServiceResponse {
	
	@XmlElement( name = "resultCode", required = true)
	public int resultCode;
	
	@XmlElement( name = "response", required = true)
	public ValidaNumeroDocumento response;
	
	public ValidaNumeroDocumentoResponse() {
	}

	@Override
	public ResponseWithEstado getResponseWithEstado() {
		return response;
	}

	@Override
	public int getResultCode() {
		return resultCode;
	}

	public ValidaNumeroDocumento getResponse() {
		return response;
	}
	
	
}
