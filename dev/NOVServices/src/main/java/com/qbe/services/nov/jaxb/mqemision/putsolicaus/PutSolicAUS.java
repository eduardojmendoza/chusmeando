package com.qbe.services.nov.jaxb.mqemision.putsolicaus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;

/**
*
* Respuesta recibida del MQMensaje
* 
 <Response>
        <Estado resultado="true" mensaje=""/>
        <RESULTADO>
            <SOLICITUDGRABADA>SI</SOLICITUDGRABADA>
            <SOLICITUDOK>SI</SOLICITUDOK>
            <MENSAJE>PROPUESTA EMITIDA</MENSAJE>
            <RAMOPCOD>AUS1</RAMOPCOD>
            <POLIZANN>00</POLIZANN>
            <POLIZSEC>000001</POLIZSEC>
            <CERTIPOL>9000</CERTIPOL>
            <CERTIANN>2017</CERTIANN>
            <CERTISEC>973574</CERTISEC>
            <SUPLENUM>0000</SUPLENUM>
            <TEXTOS>
                <TEXTO/>
            </TEXTOS>
        </RESULTADO>
    </Response>
* 
* 
* @author ramiro
*
*/
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class PutSolicAUS implements ResponseWithEstado {
	@XmlElement( name = "Estado", required = true)
	public Estado estado;
	
    @XmlElement(name="RESULTADO")	
	public Resultado resultado;

	
	public PutSolicAUS() {
	}

	@Override
	public Estado getEstado() {
		return estado;
	}

}
