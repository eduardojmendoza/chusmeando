package com.qbe.services.nov.jaxb.mqmensaje.getorganizadores;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * 
 * 
                <ORGANIZADOR>
                    <CODIGO>50</CODIGO>
                    <CLASE>OR</CLASE>
                    <NOMBRE><![CDATA[AON RISK SERVICES  ARGENTINA S(OR-0050)]]></NOMBRE>
                </ORGANIZADOR>
                <ORGANIZADOR>
                    <CODIGO>4810</CODIGO>
                    <CLASE>OR</CLASE>
                    <NOMBRE><![CDATA[HSBC BANK ARGENTINA   S.A     (OR-4810)]]></NOMBRE>
                </ORGANIZADOR>
 * 
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Organizador {
	
    @XmlElement(name="CODIGO")	
	public String codigo;
	
    @XmlElement(name="CLASE")	
	public String clase;
	
    @XmlElement(name="NOMBRE")	
	public String nombre;
	

}
