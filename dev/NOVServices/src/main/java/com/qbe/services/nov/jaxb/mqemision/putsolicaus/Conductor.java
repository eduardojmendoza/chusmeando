package com.qbe.services.nov.jaxb.mqemision.putsolicaus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
            <CONDUCTOR>
                <CONDUSEC>1</CONDUSEC>
                <CONDUTDO>1</CONDUTDO>
                <CONDUDOC>18069158</CONDUDOC>
                <APELLIDOHIJO><![CDATA[ARMATI]]></APELLIDOHIJO>
                <NOMBREHIJO><![CDATA[MARIA ADRIANA]]></NOMBREHIJO>
                <SEXOHIJO>M</SEXOHIJO>
                <ESTADOHIJO>S</ESTADOHIJO>
                <NACIMHIJO>12/01/1976</NACIMHIJO>
                <EXCLUIDO>N</EXCLUIDO>
            </CONDUCTOR>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Conductor {
	
    @XmlElement(name="CONDUSEC")	
	public String condusec;
	
    @XmlElement(name="CONDUTDO")	
	public String condutdo;
	
    @XmlElement(name="CONDUDOC")	
	public String condudoc;
	
    @XmlElement(name="APELLIDOHIJO")	
	public String apellidohijo;
	
    @XmlElement(name="NOMBREHIJO")	
	public String nombrehijo;
	
    @XmlElement(name="SEXOHIJO")	
	public String sexohijo;
	
    @XmlElement(name="ESTADOHIJO")	
	public String estadohijo;
	
    @XmlElement(name="NACIMHIJO")	
	public String nacimhijo;
	
    @XmlElement(name="EXCLUIDO")	
	public String excluido;
	
    
}
