package com.qbe.services.nov.rest;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qbe.services.nov.AisMqService;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.impl.OVMQEmisionServicesImpl;
import com.qbe.services.nov.jaxb.mqemision.putsolicaus.PutSolicAUS;
import com.qbe.services.nov.jaxb.mqemision.putsolicaus.PutSolicAUSRequest;
import com.qbe.services.nov.jaxb.mqemision.putsolicaus.PutSolicAUSResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ramiro
 *
 */
@Path("/v1/OVMQEmisionAPI")
@Api(value="OVMQEmision")
public class OVMQEmisionRESTV1 extends AisMqService {

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	protected OVMQEmisionServicesImpl servicesImpl;
	
	public OVMQEmisionRESTV1() {
	}
	
	public OVMQEmisionServicesImpl getServicesImpl() {
		if (servicesImpl == null ) {
			this.servicesImpl = (OVMQEmisionServicesImpl)getContext().getBean("OVMQEmisionServices");
		}
		return servicesImpl;
	}
	
	@POST
	@Path("/putSolicAUS")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})  
	@ApiOperation(value = "putSolicAUSJson", notes = "...", response = PutSolicAUSResponse.class)
	public Response putSolicAUSJson(String request) throws Exception {
		//Convierto el JSON a JAXB, y lo bajo a XML
        PutSolicAUSRequest requestFromJson = (PutSolicAUSRequest) NovRestServicesUtil.unmarshalFromJson(PutSolicAUSRequest.class, request, false);
		String requestXML = NovRestServicesUtil.marshalAsXml(PutSolicAUSRequest.class, requestFromJson);

		PutSolicAUSResponse result = getServicesImpl().putSolicAUS(requestXML);
        String json = NovRestServicesUtil.marshalAsJson(PutSolicAUS.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(json).build();
	}

	@POST
	@Path("/putSolicAUS")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	@ApiOperation(value = "putSolicAUSXml", notes = "...", response = PutSolicAUSResponse.class)
	public Response putSolicAUSXml(String request) throws Exception {
		
		PutSolicAUSResponse result = getServicesImpl().putSolicAUS(request);
        String xml = NovRestServicesUtil.marshalAsXml(PutSolicAUS.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}

	
	@POST
	@Path("/emitirPropuesta")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	//@ApiOperation(value = "validaNumeroDocumentoXml", notes = "...", response = ValidaNumeroDocumentoResponse.class)
	public Response emitirPropuesta(String request) throws Exception {
		GenericServiceResponse result = getServicesImpl().lbaw_EmitirPropuesta(request);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	

	@POST
	@Path("/endososDetallesImpres")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response endososDetallesImpres(String request) throws Exception {
		GenericServiceResponse result = getServicesImpl().endososDetallesImpres(request);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	
	@POST
	@Path("/riesgosImpres")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	//@ApiOperation(value = "validaNumeroDocumentoXml", notes = "...", response = ValidaNumeroDocumentoResponse.class)
	public Response riesgosImpres(String request) throws Exception {
		GenericServiceResponse result = getServicesImpl().riesgosImpres(request);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	@POST
	@Path("/getProdUsuario")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getProdUsuario(String request) throws Exception {
		GenericServiceResponse result = getServicesImpl().getProdUsuario(request);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	@POST
	@Path("/imprimirPoliza")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response imprimirPoliza(String request) throws Exception {
		GenericServiceResponse result = getServicesImpl().imprimirPoliza(request);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	@POST
	@Path("/getBinaryFile")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getBinaryFile(String request) throws Exception {
		GenericServiceResponse result = getServicesImpl().getBinaryFile(request);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/verifReimpresion")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response verifReimpresion(String request) throws Exception {
		GenericServiceResponse result = getServicesImpl().verifReimpresion(request);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/getCertMercosur")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getCertMercosur(String request) throws Exception {
		GenericServiceResponse result = getServicesImpl().getCertMercosur(request);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	@POST
	@Path("/generarZIP")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response generarZIP(String request) throws Exception {
		GenericServiceResponse result = getServicesImpl().generarZIP(request);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/uploadFile")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response uploadFile(String request) throws Exception {
		GenericServiceResponse result = getServicesImpl().uploadFile(request);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
}