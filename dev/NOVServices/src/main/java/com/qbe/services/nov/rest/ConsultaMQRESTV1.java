package com.qbe.services.nov.rest;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qbe.services.exutils.ExceptionUtils;
import com.qbe.services.nov.ConsultaMQService;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.jaxb.mqmensaje.getorganizadores.GetOrganizadores;
import com.qbe.services.nov.jaxb.mqmensaje.getorganizadores.GetOrganizadoresResponse;
import com.qbe.services.nov.jaxb.mqmensaje.legajoempleadolba.LegajoEmpleadoLBA;
import com.qbe.services.nov.jaxb.mqmensaje.legajoempleadolba.LegajoEmpleadoLBAResponse;
import com.qbe.services.nov.jaxb.mqmensaje.listadosucursalesporbanco.ListadoSucursalesPorBanco;
import com.qbe.services.nov.jaxb.mqmensaje.listadosucursalesporbanco.ListadoSucursalesPorBancoResponse;
import com.qbe.services.nov.jaxb.mqmensaje.productoreshabilitadosparacotizar.ProductoresHabilitadosParaCotizar;
import com.qbe.services.nov.jaxb.mqmensaje.productoreshabilitadosparacotizar.ProductoresHabilitadosParaCotizarResponse;
import com.qbe.services.nov.jaxb.mqmensaje.productosporproductor.ProductosPorProductor;
import com.qbe.services.nov.jaxb.mqmensaje.productosporproductor.ProductosPorProductorResponse;
import com.qbe.services.nov.jaxb.mqmensaje.renovacionporusuario.RenovacionPorUsuario;
import com.qbe.services.nov.jaxb.mqmensaje.renovacionporusuario.RenovacionPorUsuarioResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ramiro
 * @author cesar
 *
 */
@Path("/v1/ConsultaMQAPI")
@Api(value="ConsultaMQ")
public class ConsultaMQRESTV1 extends ConsultaMQService {

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	
	public ConsultaMQRESTV1() {
	}
	
	@POST
	@Path("/productosPorProductor")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})  
	@ApiOperation(value = "Op migrada productos por productor", notes = "...", response = ProductosPorProductorResponse.class)
	public Response productosPorProductorJson(
			@QueryParam("usuarcod") String usuarcod) throws Exception {
		
		ProductosPorProductorResponse result = getServicesImpl().productosPorProductor(usuarcod);
        String json = NovRestServicesUtil.marshalAsJson(ProductosPorProductor.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(json).build();
	}

	@POST
	@Path("/productosPorProductor")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	@ApiOperation(value = "Op migrada productos por productor", notes = "...", response = ProductosPorProductorResponse.class)
	public Response productosPorProductorXml(
			@QueryParam("usuarcod") String usuarcod) throws Exception {
		
		ProductosPorProductorResponse result = getServicesImpl().productosPorProductor(usuarcod);
        String xml = NovRestServicesUtil.marshalAsXml(ProductosPorProductor.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}


	@POST
	@Path("/productoresHabilitadosParaCotizar")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response productoresHabilitadosParaCotizarJson(
			@DefaultValue("") @QueryParam("aplicarXSL") String aplicarXSL,
			@QueryParam("ramopcod") String ramopcod,
			@QueryParam("usuarcod") String usuarcod,
			@QueryParam("cliensecas") String cliensecas,
			@QueryParam("nivelclas") String nivelclas) throws Exception {
		
		ProductoresHabilitadosParaCotizarResponse result = getServicesImpl().productoresHabilitadosParaCotizar(aplicarXSL, ramopcod, usuarcod, cliensecas, nivelclas);
        String json = NovRestServicesUtil.marshalAsJson(ProductoresHabilitadosParaCotizar.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(json).build();
	}

	@POST
	@Path("/productoresHabilitadosParaCotizar")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response productoresHabilitadosParaCotizarXml(
			@DefaultValue("") @QueryParam("aplicarXSL") String aplicarXSL,
			@QueryParam("ramopcod") String ramopcod,
			@QueryParam("usuarcod") String usuarcod,
			@QueryParam("cliensecas") String cliensecas,
			@QueryParam("nivelclas") String nivelclas) throws Exception {
		
		ProductoresHabilitadosParaCotizarResponse result = getServicesImpl().productoresHabilitadosParaCotizar(aplicarXSL, ramopcod, usuarcod, cliensecas, nivelclas);
        String xml = NovRestServicesUtil.marshalAsXml(ProductoresHabilitadosParaCotizar.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}
	

	@POST
	@Path("/getOrganizadores")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response getOrganizadoresJson(
			@QueryParam("usuario") String usuario,
			@QueryParam("agenteclase") String agenteclase,
			@QueryParam("agentecodigo") String agentecodigo) throws Exception {
		
		GetOrganizadoresResponse result = getServicesImpl().getOrganizadores(usuario, agenteclase, agentecodigo);
        String json = NovRestServicesUtil.marshalAsJson(GetOrganizadores.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(json).build();
	}

	@POST
	@Path("/getOrganizadores")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response getOrganizadoresXml(
			@QueryParam("usuario") String usuario,
			@QueryParam("agenteclase") String agenteclase,
			@QueryParam("agentecodigo") String agentecodigo) throws Exception {
		
		GetOrganizadoresResponse result = getServicesImpl().getOrganizadores(usuario, agenteclase, agentecodigo);
        String xml = NovRestServicesUtil.marshalAsXml(GetOrganizadores.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}
	
	@POST
	@Path("/renovacionPorUsuario")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})  
	@ApiOperation(value = "1660_RenovacionxUsuario.xml")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response renovacionPorUsuarioJson(
			@QueryParam("usuarcod") String usuarcod) throws Exception {
		
		RenovacionPorUsuarioResponse result = getServicesImpl().renovacionPorUsuario(usuarcod);
        String json = NovRestServicesUtil.marshalAsJson(RenovacionPorUsuario.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(json).build();
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/renovacionPorUsuario")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response renovacionPorUsuarioXml(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().renovacionPorUsuarioXML(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/listadoSucursalesPorBanco")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})  
	@ApiOperation(value = "1018_ListadoSucursalesxBanco.xml")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response listadoSucursalesPorBancoJson(
			@DefaultValue("") @QueryParam("aplicarXSL") String aplicarXSL,
			@QueryParam("bancocod") String bancocod) throws Exception {
		
		ListadoSucursalesPorBancoResponse result = getServicesImpl().listadoSucursalesPorBanco(bancocod, aplicarXSL);
        String json = NovRestServicesUtil.marshalAsJson(ListadoSucursalesPorBanco.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(json).build();
	}

	@POST
	@Path("/listadoSucursalesPorBanco")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	@ApiOperation(value = "1018_ListadoSucursalesxBanco.xml")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response listadoSucursalesPorBancoXml(
			@DefaultValue("") @QueryParam("aplicarXSL") String aplicarXSL,
			@QueryParam("bancocod") String bancocod) throws Exception {
		
		ListadoSucursalesPorBancoResponse result = getServicesImpl().listadoSucursalesPorBanco(bancocod, aplicarXSL);
        String xml = NovRestServicesUtil.marshalAsXml(ListadoSucursalesPorBanco.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}
	
	@POST
	@Path("/legajoEmpleadoLBA")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})  
	@ApiOperation(value = "1027_LegajoempleadoLba.xml")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response legajoEmpleadoLBAJson(
			@QueryParam("bancocod") String bancocod,
			@QueryParam("sucurcod") String sucurcod,
			@QueryParam("legajnum") String legajnum ) throws Exception {
		
		LegajoEmpleadoLBAResponse result = getServicesImpl().legajoEmpleadoLBA(bancocod, sucurcod, legajnum);
        String json = NovRestServicesUtil.marshalAsJson(LegajoEmpleadoLBA.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(json).build();
	}

	@POST
	@Path("/legajoEmpleadoLBA")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	@ApiOperation(value = "1027_LegajoempleadoLba.xml")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response legajoEmpleadoLBAXml(
			@QueryParam("bancocod") String bancocod,
			@QueryParam("sucurcod") String sucurcod,
			@QueryParam("legajnum") String legajnum ) throws Exception {
		
		LegajoEmpleadoLBAResponse result = getServicesImpl().legajoEmpleadoLBA(bancocod, sucurcod, legajnum);
        String xml = NovRestServicesUtil.marshalAsXml(LegajoEmpleadoLBA.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}
	
	
	@GET
	@Path("/getCampanasHabilitadasProductor")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response getCampanasHabilitadasProductorXml(
			@QueryParam("usuarcod") String usuarcod,
			@QueryParam("ramopcod") String ramopcod,
			@QueryParam("agentcla") String agentcla,
			@QueryParam("agentcod") String agentcod) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().campanasHabilitadasProductor(usuarcod, ramopcod, agentcla, agentcod);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	/**
	 * @param usuarcod
	 * @param ramopcod
	 * @param polizann
	 * @param polizsec
	 * @param campacod
	 * @param cobrocod
	 * @return
	 * @throws Exception
	 */
	@GET
	//@Path("/getFormasDePagoxCampana" (?i)employees)
	@Path("/{getFormasDePagoxCampana : (?i)getFormasDePagoxCampana}")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getFormasDePagoxCampana(
			@QueryParam("usuarcod") String usuarcod,
			@QueryParam("ramopcod") String ramopcod, 
			@QueryParam("polizann") String polizann,
			@QueryParam("polizsec") String polizsec,
			@QueryParam("campacod") String campacod,
			@QueryParam("cobrocod") String cobrocod) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().formasDePagoxCampana(usuarcod, ramopcod, polizann, polizsec, campacod, cobrocod);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	//  curl -v --header "Content-Type: application/xml" -H "Accept: application/xml" -X GET  "http://10.1.10.30:8080/NOVServices/rest/v1/ConsultaMQAPI/getListadoVehiculosxProd?usuarcod=EX009001L&aumarcod=00009&auvehdes=AGIL&procann=2015&procmes=10&procdia=23&auprocod=03"

	
	@GET
	@Path("/getListadoVehiculosxProd")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response getListadoVehiculosxProd(
			@QueryParam("usuarcod") String usuarcod,
			@QueryParam("aumarcod") String aumarcod,  
			@QueryParam("auvehdes") String auvehdes, 
			@QueryParam("procann") String procann, 
			@QueryParam("procmes") String procmes, 
			@QueryParam("procdia") String procdia, 
			@QueryParam("auprocod") String auprocod) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().listadoVehiculosxProd(usuarcod, aumarcod, auvehdes, procann, procmes, procdia, auprocod);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@GET
	@Path("/getDispRastreo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response getDispRastreo(
			@QueryParam("usuario") String usuario, 
			@QueryParam("ramopcod") String ramopcod,   
			@QueryParam("modeautcod") String modeautcod, 
			@QueryParam("sumaseg") String sumaseg,  
			@QueryParam("provi") String provi, 
			@QueryParam("localidadcod") String localidadcod, 
			@QueryParam("opcion") String opcion, 
			@QueryParam("operacion") String operacion, 
			@QueryParam("prescod") String prescod, 
			@QueryParam("instala") String instala) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().getDispRastreo(usuario, ramopcod, modeautcod, sumaseg, provi, localidadcod, opcion, operacion, prescod, instala);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/getListadoCoberturas")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response getListadoCoberturas(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().getListadoCoberturas(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	@POST
	@Path("/msjGralCotizacion")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response msjGralCotizacion(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().msjGralCotizacion(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	@POST
	@Path("/calculoPremio")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response calculoPremio(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().calculoPremio(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	@POST
	@Path("/calculoPrimeraCuota")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response calculoPrimeraCuota(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().calculoPrimeraCuota(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	

	@POST
	@Path("/prorrateoPrimas")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response prorrateoPrimas(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().prorrateoPrimas(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	@POST
	@Path("/listadoObjetos")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response listadoObjetos(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().listadoObjetos(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	@POST
	@Path("/integracionGral")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response integracionGral(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().integracionGral(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	@POST
	@Path("/integracionHogarCartGral")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response integracionHogarCartGral(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().integracionHogarCartGral(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/integracionConsorcio")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response integracionConsorcio(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().integracionConsorcio(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/leyendasxProdActCobertura")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response leyendasxProdActCobertura(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().leyendasxProdActCobertura(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/montosRCxAscensor")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  

	public Response montosRCxAscensor(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().montosRCxAscensor(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/parametrosxProducto")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  

	public Response parametrosxProducto(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().parametrosxProducto(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	
	@POST
	@Path("/polizasColectivasxProductor")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response polizasColectivasxProductor(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().polizasColectivasxProductor(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/formasDePagoxPolizaColectiva")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response formasDePagoxPolizaColectiva(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().formasDePagoxPolizaColectiva(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	

	@POST
	@Path("/convenioProdOrganiz")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response convenioProdOrganiz(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().convenioProdOrganiz(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/periodoCotizar")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response periodoCotizar(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().periodoCotizar(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/estabilizacion")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response estabilizacion(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().estabilizacion(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	

	
	@POST
	@Path("/planPagoxProdPolMedCobro")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response planPagoxProdPolMedCobro(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().planPagoxProdPolMedCobro(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	
	@POST
	@Path("/polizasxCanalHabilOv")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response polizasxCanalHabilOv(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().polizasxCanalHabilOv(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/coberturasParaImpresion")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response coberturasParaImpresion(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().coberturasParaImpresion(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	

	@POST
	@Path("/integracionAutosMPL")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response integracionAutosMPL(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().integracionAutosMPL(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}

	
	@POST
	@Path("/integracionATM")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response integracionATM(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().integracionATM(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}

	

	@POST
	@Path("/cultivosxProducto")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response cultivosxProducto(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().cultivosxProducto(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	
	@POST
	@Path("/provinciasxCultivo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response provinciasxCultivo(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().provinciasxCultivo(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}

	
	
	@POST
	@Path("/partidosxProvinciasCultivo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response partidosxProvinciasCultivo(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().partidosxProvinciasCultivo(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}

	
	
	@POST
	@Path("/localidadesxPartidoProvinciasCultivo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response localidadesxPartidoProvinciasCultivo(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().localidadesxPartidoProvinciasCultivo(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/franquiciasGranizo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response franquiciasGranizo(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().franquiciasGranizo(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	
	@POST
	@Path("/planesGranizo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response planesGranizo(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().planesGranizo(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	
	@POST
	@Path("/cotizacionGRA")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response cotizacionGRA(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().cotizacionGRA(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	
	@POST
	@Path("/integracionGranizo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response integracionGranizo(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().integracionGranizo(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	
	@POST
	@Path("/marcaTipoVehiculo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response marcaTipoVehiculo(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().marcaTipoVehiculo(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/datosGralPoliza")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response datosGralPoliza(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().datosGralPoliza(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	
	@POST
	@Path("/detalleAvisoVencimiento")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response detalleAvisoVencimiento(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().detalleAvisoVencimiento(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	
	@POST
	@Path("/deudaAVencerTotales")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response deudaAVencerTotales(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().deudaAVencerTotales(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/cartasReclamosTotales")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response cartasReclamosTotales(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().cartasReclamosTotales(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	@POST
	@Path("/siniestrosTotales")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response siniestrosTotales(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().siniestrosTotales(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/pagosSiniestrosTot")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response pagosSiniestrosTot(String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().pagosSiniestrosTot(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	@POST
	@Path("/exigibleTotales")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response exigibleTotales(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().exigibleTotales(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/anulacionesTotales")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response anulacionesTotales(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().anulacionesTotales(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/cartera1603")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response cartera1603(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().cartera1603(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/legajoempleadoNew")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response legajoempleadoNew(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().legajoempleadoNew(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/suscripcionORyPR")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response suscripcionORyPR(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().suscripcionORyPR(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	
	@POST
	@Path("/archSus")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response archSus(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().archSus(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	
	@POST
	@Path("/avisoBajadaArchivo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response avisoBajadaArchivo(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().avisoBajadaArchivo(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	//29/8/17 ------------------------------------------------------------------->
	
	@POST
	@Path("/cotizacionUnificadoRenovacion")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response cotizacionUnificadoRenovacion(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().cotizacionUnificadoRenovacion(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/renovacionPolizaColectiva")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response renovacionPolizaColectiva(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().renovacionPolizaColectiva(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/objetosxCertificados")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response objetosxCertificados(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().objetosxCertificados(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/datosEdificios")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response datosEdificios(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().datosEdificios(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/renovacionRiesgos")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response renovacionRiesgos(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().renovacionRiesgos(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/detalleCoberxProd")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response detalleCoberxProd(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().detalleCoberxProd(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	//29/8/17 ------------------------------------------------------------------->
	
	
	
	//08/09/17 ------------------------------------------------------------------->
	@POST
	@Path("/parametrosPolizaColectiva")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response parametrosPolizaColectiva(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().parametrosPolizaColectiva(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/tipoUsoxProdPoliza")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response tipoUsoxProdPolizaColectiva(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().tipoUsoxProdPoliza(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/sumasPersonalDomestico")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response sumasPersonalDomestico(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().sumasPersonalDomestico(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/validarVehiculo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response validarVehiculo(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().validarVehiculo(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/validarPersona")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response validarPersona(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().validarPersona(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/apoderadoxClienteSec")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response apoderadoxClienteSec(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getApoderadoxClienteSec(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/integracionGralNew")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response integracionGralNew(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getIntegracionGralNew(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/integracionHOM1")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response integracionHOM1(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getIntegracionHOM1(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/calculoPrimeraCuotaHOM1")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response cotizacionHOM1(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getCotizacionHOM1(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/retencionesComisiones")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response retencionesComisiones(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getRetencionesComisiones(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/ePolizaAUS1")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response ePolizaAUS1(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getEPolizaAUS1(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/actividadesCanal")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response actividadesCanal(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getActividadesCanal(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/tpProducto")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response tpProducto(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getTpProducto(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/planCuotasVigencia")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getPlanCuotasVigencia(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getPlanCuotasVigencia(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/marcasProductorTomador")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getMarcasProductorTomador(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getMarcasProductorTomador(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/franquiciasCoberturas")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getFranquiciasCoberturas(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getFranquiciasCoberturas(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/accidentesPersonales")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getAccidentesPersonales(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getAccidentesPersonales(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
		
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/bajaEndoso")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response geBajaEndoso(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getBajaEndoso(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/npsAus1")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response npsAUS1(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getNpsAUS1(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/getRolesAdministrativos")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getRolesAdministrativos(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getRolesAdministrativos(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/getPolizasColectivasProductorMotos")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getPolizasColectivasProductorMotos(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getPolizasColectivasProductorMotos(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/getMarcasMotos")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getMarcasMotos(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getMarcasMotos(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/getModelosMotos")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getModelosMotos(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getModelosMotos(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	
	//08/09/17 ------------------------------------------------------------------->
	
	//	@HEAD
	@GET
	@Path("/version")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response getVersion() throws Exception {
		try {
			return Response
					.status(Response.Status.OK)
					.header("VERSION", "1")
					.build();
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(Response.Status.PRECONDITION_FAILED)
					.entity(e.getMessage()).build();
		}
	}	
}