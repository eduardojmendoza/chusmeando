package com.qbe.services.nov.jaxb.mqemision.putsolicaus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
                <COBERCOD>013</COBERCOD>
                <COBERORD>07</COBERORD>
                <CAPITASG>171000.00</CAPITASG>
                <CAPITIMP>1710.00</CAPITIMP>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Cobertura {
	
    @XmlElement(name="COBERCOD")	
	public String cobercod;
	
    @XmlElement(name="COBERORD")	
	public String coberord;
	
    @XmlElement(name="CAPITASG")	
	public String capitasg;
	
    @XmlElement(name="CAPITIMP")	
	public String capitimp;
	
    
}
