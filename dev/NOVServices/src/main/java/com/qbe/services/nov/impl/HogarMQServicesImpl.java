package com.qbe.services.nov.impl;

import java.util.logging.Logger;

import com.qbe.services.lbahogarmq.impl.lbaw_GetCotiz;
import com.qbe.services.lbahogarmq.impl.lbaw_GetPolCamp;
import com.qbe.services.lbahogarmq.impl.lbaw_GetPolCob;
import com.qbe.services.lbahogarmq.impl.lbaw_GetPolPlanes;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.ofVirtualLBA.impl.lbaw_GetTiposIVA;
import com.qbe.vbcompat.string.StringHolder;

/**
 * Implementación del servicio
 * 
 * @author ramiro
 *
 */
public class HogarMQServicesImpl {

	private static final String EMPTY_STRING = "";

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	public HogarMQServicesImpl() {
	}

	public GenericServiceResponse getPolCamp(String request) throws Exception {
		lbaw_GetPolCamp comp = new lbaw_GetPolCamp();
		StringHolder sh = new StringHolder();
		int resultCode = comp.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}


	public GenericServiceResponse getPolCob(String request) throws Exception {
		lbaw_GetPolCob comp = new lbaw_GetPolCob();
		StringHolder sh = new StringHolder();
		int resultCode = comp.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}


	public GenericServiceResponse getPolPlanes(String request) throws Exception {
		lbaw_GetPolPlanes comp = new lbaw_GetPolPlanes();
		StringHolder sh = new StringHolder();
		int resultCode = comp.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}


	public GenericServiceResponse getCotiz(String request) throws Exception {
		lbaw_GetCotiz comp = new lbaw_GetCotiz();
		StringHolder sh = new StringHolder();
		int resultCode = comp.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	
	
	
	
}
