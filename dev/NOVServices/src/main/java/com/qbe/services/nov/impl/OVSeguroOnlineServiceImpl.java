package com.qbe.services.nov.impl;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.segurosOnline.impl.NBWSA_MQGenericoAIS;
import com.qbe.services.segurosOnline.impl.NbwsA_Alta;
import com.qbe.vbcompat.string.StringHolder;

public class OVSeguroOnlineServiceImpl {

	protected Logger logger = Logger.getLogger(getClass().getName());

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse nbwsA_Alta(String request) throws Exception {
		NbwsA_Alta comp = new NbwsA_Alta();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = comp.IAction_Execute(request, sh, "");
		logger.log(Level.FINE,
				"Resultado de ejecución de nbwsA_Alta: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	public GenericServiceResponse SusyDesusepoliza(String request) throws Exception {
		NBWSA_MQGenericoAIS comp = new NBWSA_MQGenericoAIS();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = comp.IAction_Execute(request, sh, "");
		logger.log(Level.FINE, "Resultado de ejecución de 1185_SusyDesus_epoliza: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse productosClientes(String request) throws Exception {
		StringHolder sh = new StringHolder();
		NBWSA_MQGenericoAIS comp = new NBWSA_MQGenericoAIS();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = comp.IAction_Execute(request, sh, "");
		logger.log(Level.FINE, "Resultado de ejecución de 1184_ProductosClientes: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null) {
			throw new Exception("Respuesta en NULL del componente");
		}
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse listadoePolizas(String request) throws Exception {
		StringHolder sh = new StringHolder();
		NBWSA_MQGenericoAIS comp = new NBWSA_MQGenericoAIS();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = comp.IAction_Execute(request, sh, "");
		logger.log(Level.FINE, "Resultado de ejecución de 1187_ListadoePolizas: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null) {
			throw new Exception("Respuesta en NULL del componente");
		}
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	public GenericServiceResponse envioPoliza(String request) throws Exception {
		StringHolder sh = new StringHolder();
		NBWSA_MQGenericoAIS comp = new NBWSA_MQGenericoAIS();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = comp.IAction_Execute(request, sh, "");
		logger.log(Level.FINE,
				"Resultado de ejecución de 1188_EnvioPoliza: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null) {
			throw new Exception("Respuesta en NULL del componente");
		}
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	public GenericServiceResponse seguimientoCobranzas(String request) throws Exception {
		StringHolder sh = new StringHolder();
		NBWSA_MQGenericoAIS comp = new NBWSA_MQGenericoAIS();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = comp.IAction_Execute(request, sh, "");
		logger.log(Level.FINE,
				"Resultado de ejecución de 1430_SeguimientoCobranzas: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null) {
			throw new Exception("Respuesta en NULL del componente");
		}
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	public GenericServiceResponse seguimientoSiniestros(String request) throws Exception {
		StringHolder sh = new StringHolder();
		NBWSA_MQGenericoAIS comp = new NBWSA_MQGenericoAIS();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = comp.IAction_Execute(request, sh, "");
		logger.log(Level.FINE,
				"Resultado de ejecución de 1315_SeguimientoSiniestros: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null) {
			throw new Exception("Respuesta en NULL del componente");
		}
		return new GenericServiceResponse(resultCode, sh.getValue());
	}
}
