package com.qbe.services.nov.jaxb.mqmensaje.listadosucursalesporbanco;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * 
            <option value="0055">0055 - CORDOBA</option>    
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Option {
	
    @XmlAttribute(name="value")	
	public String value;
	
    @XmlValue
	public String texto;
    
}
