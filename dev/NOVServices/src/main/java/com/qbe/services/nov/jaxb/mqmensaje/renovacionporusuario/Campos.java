package com.qbe.services.nov.jaxb.mqmensaje.renovacionporusuario;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.jaxb.AnyXmlElement;

/**
 * 
 * 
 <CAMPOS>
            <CANTELEM>9</CANTELEM>
            <RESULTADOS>
                <RESULTADO>
                    <RAMOPCOD>AUS1</RAMOPCOD>
                    <RAMOPDES>AUTOMOTORES SCORING EN PESOS</RAMOPDES>
                    <RAMOPDAB>AUTOMOTORES</RAMOPDAB>
                    <MONENCOD>1</MONENCOD>
                </RESULTADO>
            </RESULTADOS>
        </CAMPOS>
 * @author ramiro
 *
 * 
 * 
 * 
 */
@XmlRootElement
//@XmlType(name = "NOVServiceResponse", propOrder = {
//	    "code", "response"
//	})
@XmlAccessorType(XmlAccessType.FIELD)
public class Campos {

    @XmlElement(name="CANTELEM")	
	public Integer cantElem;
	
	@XmlElementWrapper( name = "RESULTADOS", required = true)
    @XmlElement(name="RESULTADO")	
	public List<Resultado> resultados;
}
