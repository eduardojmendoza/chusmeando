package com.qbe.services.nov.rest;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qbe.services.nov.AisMqService;
import com.qbe.services.nov.impl.LBAEcouponsMQServicesImpl;
import com.qbe.services.nov.jaxb.mqecoupons.getcliensec.GetCliensec;
import com.qbe.services.nov.jaxb.mqecoupons.getcliensec.GetCliensecResponse;
import com.qbe.services.nov.jaxb.mqecoupons.getcupimp.GetCupImp;
import com.qbe.services.nov.jaxb.mqecoupons.getcupimp.GetCupImpResponse;
import com.qbe.services.nov.jaxb.mqecoupons.getpolaus.GetPolAus;
import com.qbe.services.nov.jaxb.mqecoupons.getpolaus.GetPolAusResponse;
import com.qbe.services.nov.jaxb.mqecoupons.getpolhom.GetPolHom;
import com.qbe.services.nov.jaxb.mqecoupons.getpolhom.GetPolHomResponse;
import com.qbe.services.nov.jaxb.mqecoupons.getunlockaus.GetUnlockAus;
import com.qbe.services.nov.jaxb.mqecoupons.getunlockaus.GetUnlockAusResponse;
import com.qbe.services.nov.jaxb.mqecoupons.getunlockhom.GetUnlockHom;
import com.qbe.services.nov.jaxb.mqecoupons.getunlockhom.GetUnlockHomResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author jose
 *
 */
@Path("/v1/LBAMQEcouponsAPI")
@Api(value="LBAEcoupons")
public class LBAEcouponsMQRESTV1 extends AisMqService {
 
	protected Logger logger = Logger.getLogger(getClass().getName());
	
	protected LBAEcouponsMQServicesImpl servicesImpl;
	
	public LBAEcouponsMQRESTV1() {
	}
	
	public LBAEcouponsMQServicesImpl getServicesImpl() {
		if (servicesImpl == null ) {
			this.servicesImpl = (LBAEcouponsMQServicesImpl)getContext().getBean("LBAEcouponsMQServices");
		}
		return servicesImpl;
	}
	
	
	@POST
	@Path("/getCliensec")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	@ApiOperation(value = "getCliensec", notes = "...", response = GetCliensecResponse.class)
	public Response getCliensec(String request) throws Exception {
		logger.log(Level.FINE, "Request de entrada: " + request);
		GetCliensecResponse result = getServicesImpl().getCliensec(request);
		String xml = NovRestServicesUtil.marshalAsXml(GetCliensec.class, result.response);
		Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}
	
	
	@POST
	@Path("/getPolAus")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	@ApiOperation(value = "getPolAus", notes = "...", response = GetPolAusResponse.class)
	public Response getPolAus(String request) throws Exception {
		
		GetPolAusResponse result = getServicesImpl().getPolAus(request);
		String xml = NovRestServicesUtil.marshalAsXml(GetPolAus.class, result.response);
		Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}
	
	
	@POST
	@Path("/getPolHom")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML}) 
	@ApiOperation(value = "getPolHom", notes = "...", response = GetPolHomResponse.class)
	public Response getPolHom(String request) throws Exception {
		
		GetPolHomResponse result = getServicesImpl().getPolHom(request);
		String xml = NovRestServicesUtil.marshalAsXml(GetPolHom.class, result.response);
		Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}
	
	
	@POST
	@Path("/getCupImp")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML}) 
	@ApiOperation(value = "getCupImp", notes = "...", response = GetCupImpResponse.class)
	public Response getCupImp(String request) throws Exception {
		
		GetCupImpResponse result = getServicesImpl().getCupImp(request);
		String xml = NovRestServicesUtil.marshalAsXml(GetCupImp.class, result.response);
		Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}
	
	
	//Servicio aun no solicitado
	@POST
	@Path("/getUnlockAus")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	@ApiOperation(value = "getUnlockAus", notes = "...", response = GetUnlockAusResponse.class)
	public Response getUnlockAus(String request) throws Exception {
		
		GetUnlockAusResponse result = getServicesImpl().getUnlockAus(request);
		String xml = NovRestServicesUtil.marshalAsXml(GetUnlockAus.class, result.response);
		Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}
	
	//Servicio aun no solicitado
	@POST
	@Path("/getUnlockHom")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML}) 
	@ApiOperation(value = "getUnlockHom", notes = "...", response = GetUnlockHomResponse.class)
	public Response getUnlockHom(String request) throws Exception {
		
		GetUnlockHomResponse result = getServicesImpl().getUnlockHom(request);
		String xml = NovRestServicesUtil.marshalAsXml(GetUnlockHom.class, result.response);
	    Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}
	
	
}
