package com.qbe.services.nov.jaxb.mqmensajegestion.actividadocupacion;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.qbe.services.nov.ResponseWithEstado;
import com.qbe.services.nov.ServiceResponse;


/**
 * Wrapper de ActividadOcupacion que incluye el resultCode
 * 
 *  
 * @author cesar
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ActividadOcupacionResponse implements ServiceResponse {
	
	@XmlElement( name = "resultCode", required = true)
	public int resultCode;
	
	@XmlElement( name = "response", required = true)
	public ActividadOcupacion response;
	
	public ActividadOcupacionResponse() {
	}

	@Override
	public ResponseWithEstado getResponseWithEstado() {
		return response;
	}

	@Override
	public int getResultCode() {
		return resultCode;
	}
}
