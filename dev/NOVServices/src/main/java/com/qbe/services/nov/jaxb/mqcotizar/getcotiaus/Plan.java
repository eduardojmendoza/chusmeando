package com.qbe.services.nov.jaxb.mqcotizar.getcotiaus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
            <PLAN>
                <PLANNCOD>01199</PLANNCOD>
                <PLANNDES><![CDATA[Responsabilidad Civil/FRANQUICIA NO APLICABLE]]></PLANNDES>
                <ORDEN>0008</ORDEN>
                <LUNPAR>N</LUNPAR>
                <GRANIZO>N</GRANIZO>
                <ROBOCON>N</ROBOCON>
                <ESRC>S</ESRC>
                <CON_HIJOS>
                    <PRIMA>1442,39</PRIMA>
                    <RECARGOS>0,00</RECARGOS>
                    <IVAIMPOR>302,90</IVAIMPOR>
                    <IVAIMPOA>0,00</IVAIMPOA>
                    <IVARETEN>0,00</IVARETEN>
                    <DEREMI>0,00</DEREMI>
                    <SELLADO>0,00</SELLADO>
                    <INGBRU>0,00</INGBRU>
                    <IMPUES>31,72</IMPUES>
                    <PRECIO>1777,01</PRECIO>
                </CON_HIJOS>
                <SIN_HIJOS>
                    <PRIMA>1311,32</PRIMA>
                    <RECARGOS>0,00</RECARGOS>
                    <IVAIMPOR>275,38</IVAIMPOR>
                    <IVAIMPOA>0,00</IVAIMPOA>
                    <IVARETEN>0,00</IVARETEN>
                    <DEREMI>0,00</DEREMI>
                    <SELLADO>0,00</SELLADO>
                    <INGBRU>0,00</INGBRU>
                    <IMPUES>28,85</IMPUES>
                    <PRECIO>1615,55</PRECIO>
                </SIN_HIJOS>
            </PLAN>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Plan {
	
    @XmlElement(name="PLANNCOD")	
	public String planncod;
	
    @XmlElement(name="PLANNDES")	
	public String planndes;
	
    @XmlElement(name="ORDEN")	
	public String orden;
	
    @XmlElement(name="LUNPAR")	
	public String lunpar;
	
    @XmlElement(name="GRANIZO")	
	public String granizo;

    @XmlElement(name="ROBOCON")	
	public String robocon;
    
    @XmlElement(name="CON_HIJOS")	
	public CostoPlan conHijos;
    
    @XmlElement(name="SIN_HIJOS")	
	public CostoPlan sinHijos;
    
    
}
