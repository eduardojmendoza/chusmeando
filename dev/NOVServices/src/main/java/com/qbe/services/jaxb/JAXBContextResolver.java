package com.qbe.services.jaxb;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;

import com.qbe.services.nov.NOVServiceResponse;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.api.json.JSONJAXBContext;

/**
 * Inteneto de usarla para marshalear NOVServiceResponse a JSON. No anduvo, ver comentarios en esa clase
 * @author ramiro
 *
 */
@Deprecated
@Provider
public class JAXBContextResolver implements ContextResolver<JAXBContext> {
 
     private JAXBContext context;
     private Class[] types = {NOVServiceResponse.class};
 
     public JAXBContextResolver() throws Exception {
    	 //Este no genera Arrays
         this.context = new JSONJAXBContext(JSONConfiguration
        		 .natural()
        		 .humanReadableFormatting(true)
        		 .rootUnwrapping(true)
//        		 .usePrefixesAtNaturalAttributes()  No lo pongo así no pone el @ para los atributos
        		 .build(), types); 

     }
 
     public JAXBContext getContext(Class<?> objectType) {
         for (Class type : types) {
             if (type == objectType) {
                 return context;
             }
         }
         return null;
     }
 }