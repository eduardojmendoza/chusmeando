package com.qbe.services.nov.jaxb.mqecoupons.getunlockaus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;

@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetUnlockAus implements ResponseWithEstado{
	@XmlElement( name = "Estado", required = true)
	public Estado estado;
	
	
	
	public GetUnlockAus(){
	}
	
	@Override
	public Estado getEstado() {
		return estado;
	}
}
