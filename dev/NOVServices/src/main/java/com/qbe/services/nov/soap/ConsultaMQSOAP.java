package com.qbe.services.nov.soap;

import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.qbe.services.nov.ConsultaMQService;
import com.qbe.services.nov.jaxb.mqmensaje.productosporproductor.ProductosPorProductorResponse;

@WebService(
		targetNamespace = "http://nov.services.qbe.com/", 
		portName = "ConsultaMQPort", 
		serviceName = "ConsultaMQ")
public class ConsultaMQSOAP extends ConsultaMQService {

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	public ConsultaMQSOAP() {
	}

	@WebMethod(operationName = "ProductosxProductor", action = "ProductosxProductor")
	public ProductosPorProductorResponse productosPorProductor(
			@WebParam(name = "Usuarcod") String usuarcod) throws Exception {

		return getServicesImpl().productosPorProductor(usuarcod);
	}
	
}
