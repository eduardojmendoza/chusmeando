package com.qbe.services.nov.impl;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.jaxb.mqemision.putsolicaus.PutSolicAUS;
import com.qbe.services.nov.jaxb.mqemision.putsolicaus.PutSolicAUSResponse;
import com.qbe.services.ovmq.impl.lbaw_OVEndososDetalles;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVValidaNumDoc;
import com.qbe.services.ovmqemision.impl.lbaw_GenerarZIP;
import com.qbe.services.ovmqemision.impl.lbaw_GetCertMercosur;
import com.qbe.services.ovmqemision.impl.lbaw_GetProdUsuario;
import com.qbe.services.ovmqemision.impl.lbaw_OVAisPutSolicAUS;
import com.qbe.services.ovmqemision.impl.lbaw_OVEmisPropuesta;
import com.qbe.services.ovmqemision.impl.lbaw_OVEndososImpres;
import com.qbe.services.ovmqemision.impl.lbaw_OVGetBinaryFile;
import com.qbe.services.ovmqemision.impl.lbaw_OVImprimirPoliza;
import com.qbe.services.ovmqemision.impl.lbaw_OVRiesgosImpres;
import com.qbe.services.ovmqemision.impl.lbaw_OVVerifImpresion;
import com.qbe.services.ovmqemision.impl.lbaw_UploadFile;
import com.qbe.vbcompat.string.StringHolder;

/**
 * Implementación del servicio
 * 
 * @author ramiro
 *
 */
public class OVMQEmisionServicesImpl {

	private static final String EMPTY_STRING = "";

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	public OVMQEmisionServicesImpl() {
	}

	public PutSolicAUSResponse putSolicAUS(
			String reqXML) throws Exception {
		
		lbaw_OVAisPutSolicAUS psa = new lbaw_OVAisPutSolicAUS();
		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = psa.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución : [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(PutSolicAUS.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        PutSolicAUS result = (PutSolicAUS) unmarshaller.unmarshal(new StringReader(sh.getValue()));
        
        //Retorno un Wrapper que incluye el resultCode
        PutSolicAUSResponse response = new PutSolicAUSResponse();
        response.resultCode = resultCode;
        response.response = result;
        
		return response;
		
	}
	
	
	public GenericServiceResponse lbaw_EmitirPropuesta(String request) throws Exception {
		lbaw_OVEmisPropuesta vnd = new lbaw_OVEmisPropuesta();
		StringHolder sh = new StringHolder();
		int resultCode = vnd.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse endososDetallesImpres(String request) throws Exception {
		lbaw_OVEndososImpres vnd = new lbaw_OVEndososImpres();
		StringHolder sh = new StringHolder();
		int resultCode = vnd.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	

	public GenericServiceResponse riesgosImpres(String request) throws Exception {
		lbaw_OVRiesgosImpres vnd = new lbaw_OVRiesgosImpres();
		StringHolder sh = new StringHolder();
		int resultCode = vnd.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	
	public GenericServiceResponse getProdUsuario(String request) throws Exception {
		lbaw_GetProdUsuario gpu = new lbaw_GetProdUsuario();
		StringHolder sh = new StringHolder();
		int resultCode = gpu.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse imprimirPoliza(String request) throws Exception {
		lbaw_OVImprimirPoliza ipo = new lbaw_OVImprimirPoliza();
		StringHolder sh = new StringHolder();
		logger.log(Level.INFO, "Ejecutando mensaje de impresion de polizas: "+request);
		int resultCode = ipo.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse getBinaryFile(String request) throws Exception {
		lbaw_OVGetBinaryFile gpu = new lbaw_OVGetBinaryFile();
		StringHolder sh = new StringHolder();
		int resultCode = gpu.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	
	
	public GenericServiceResponse verifReimpresion(String request) throws Exception {
		lbaw_OVVerifImpresion gpu = new lbaw_OVVerifImpresion();
		StringHolder sh = new StringHolder();
		int resultCode = gpu.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse getCertMercosur(String request) throws Exception {
		lbaw_GetCertMercosur gpu = new lbaw_GetCertMercosur();
		StringHolder sh = new StringHolder();
		int resultCode = gpu.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	public GenericServiceResponse generarZIP(String request) throws Exception {
		lbaw_GenerarZIP gzip = new lbaw_GenerarZIP();
		StringHolder sh = new StringHolder();
		int resultCode = gzip.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse uploadFile(String request) throws Exception {
		lbaw_UploadFile gUpFile = new lbaw_UploadFile();
		StringHolder sh = new StringHolder();
		int resultCode = gUpFile.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
}
