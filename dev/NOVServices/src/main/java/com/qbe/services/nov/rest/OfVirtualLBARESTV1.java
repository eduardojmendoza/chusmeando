package com.qbe.services.nov.rest;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qbe.services.exutils.ExceptionUtils;
import com.qbe.services.nov.AisMqService;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.impl.OfVirtualLBAServicesImpl;
import com.qbe.services.nov.jaxb.mqcotizar.getcotiaus.GetCotiAUS;
import com.qbe.services.nov.jaxb.mqcotizar.getcotiaus.GetCotiAUSRequest;
import com.qbe.services.nov.jaxb.mqcotizar.getcotiaus.GetCotiAUSResponse;
import com.qbe.services.nov.jaxb.mqcotizar.validanumerodocumento.ValidaNumeroDocumento;
import com.qbe.services.nov.jaxb.mqcotizar.validanumerodocumento.ValidaNumeroDocumentoResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ramiro
 *
 */
@Path("/v1/OfVirtualLBAAPI")
@Api(value="OfVirtualLBA")
public class OfVirtualLBARESTV1 extends AisMqService {

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	protected OfVirtualLBAServicesImpl servicesImpl;
	
	public OfVirtualLBARESTV1() {
	}
	
	public OfVirtualLBAServicesImpl getServicesImpl() {
		if (servicesImpl == null ) {
			this.servicesImpl = (OfVirtualLBAServicesImpl)getContext().getBean("OfVirtualLBAServices");
		}
		return servicesImpl;
	}
	
	@GET
	@Path("/getTiposIVA")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getTiposIVA()  {
		
		GenericServiceResponse result;
		try {
			result = getServicesImpl().getTiposIVA();
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * Está puesto acá para poder llamarlo para validar la impl, pero no va an este módulo sino en un LBAVIRTUAL
	 * @return
	 */
	@GET
	@Path("/getNroCot")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getNroCot()  {
		
		GenericServiceResponse result;
		try {
			result = getServicesImpl().getNroCot();
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
		
	
	
}