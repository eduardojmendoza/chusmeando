package com.qbe.services.nov.jaxb.mqmensajegestion.perfilusuario;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;
import com.qbe.services.nov.jaxb.mqmensaje.productosporproductor.Campos;
import com.qbe.services.nov.jaxb.mqmensaje.productosporproductor.Producto;

/**
*
* Respuesta recibida del MQMensaje
* 

Este es el resultado de aplicar el PerfilUsuario.xsl a la definción original

<Response>
	<Estado resultado="true" mensaje="" />
	<USER_NIVEL>A</USER_NIVEL>
	<USUARNOM>FAELLA</USUARNOM>
	<CLIENTIP>B</CLIENTIP>
	<BANCOCOD>4013</BANCOCOD>
	<BANCONOM>HSBC BANK ARGENTINA (PRENDAS)</BANCONOM>
	<GRUPOHSBC>N</GRUPOHSBC>
	<ACCESO>
		<MMENU>050000</MMENU>
		<MMENU>100000</MMENU>
[..........................................]
	</ACCESO>
	<EMISIONES>
		<EMISION>
			<RAMOPCOD>AUS1</RAMOPCOD>
		</EMISION>
		<EMISION>
			<RAMOPCOD>AUS1</RAMOPCOD>
		</EMISION>
	</EMISIONES>
	<MAILUSER>BRENDA.VELASCO@zurich.com.AR</MAILUSER>
	<PEOPLESOFTEC>63713249</PEOPLESOFTEC>
	<USUARNOMEC>Administrativo, ACUTAIN, RICARDO EUGENIO</USUARNOMEC>
	<MAILEC>JORGE.ARENA@LBA.COM.AR</MAILEC>
</Response>


* @author ramiro
*
*/
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfilUsuario implements ResponseWithEstado {
	@XmlElement( name = "Estado", required = true)
	public Estado estado;
	
	@XmlElement( name = "USER_NIVEL", required = true)
	private String userNivel;

	@XmlElement( name = "USUARNOM", required = true)
	private String usuarNom;
	
	@XmlElement( name = "CLIENTIP", required = true)
	private String clienTip;

	@XmlElement( name = "BANCOCOD", required = true)
	private String bancoCod;

	@XmlElement( name = "BANCONOM", required = true)
	private String bancoNom;

	@XmlElement( name = "GRUPOHSBC", required = true)
	private String grupoHsbc;

	@XmlElementWrapper( name = "ACCESO", required = true)
    @XmlElement(name="MMENU")	
	public List<String> acceso;
	
	@XmlElementWrapper( name = "EMISIONES", required = true)
    @XmlElement(name="EMISION")	
	public List<Emision> emisiones;
	
	@XmlElement( name = "MAILUSER", required = true)
	private String mailUser;

	@XmlElement( name = "PEOPLESOFTEC", required = true)
	private String peoplesoftTec;

	@XmlElement( name = "USUARNOMEC", required = true)
	private String usuarNomEC;

	@XmlElement( name = "MAILEC", required = true)
	private String mailEC;
	
	public PerfilUsuario() {
	}

	@Override
	public Estado getEstado() {
		return estado;
	}

}
