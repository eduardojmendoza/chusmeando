package com.qbe.services.nov.jaxb.mqecoupons.getpolhom;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.qbe.services.nov.ResponseWithEstado;
import com.qbe.services.nov.ServiceResponse;

/**
 * Wrapper que incluye el resultCode
 * 
 * 
 * 
 * 
 *  
 * @author jose
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class GetPolHomResponse implements ServiceResponse{
	
	@XmlElement( name = "resultCode", required = true)
	public int resultCode;
	
	@XmlElement( name = "response", required = true)
	public GetPolHom response;
	
	public GetPolHomResponse(){	
	}
	
	@Override
	public ResponseWithEstado getResponseWithEstado() {
		return response;
	}

	@Override
	public int getResultCode() {
		return resultCode;
	}
	
	public GetPolHom getResponse() {
		return response;
	}

}
