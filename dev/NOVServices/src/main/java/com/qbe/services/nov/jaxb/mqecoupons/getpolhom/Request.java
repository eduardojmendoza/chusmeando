package com.qbe.services.nov.jaxb.mqecoupons.getpolhom;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
       <REQUEST>
          <CLIENSEC>101976143</CLIENSEC>
          <TIPOSEG>HOM1</TIPOSEG>
	      <POLIZA>0000004773</POLIZA>
       </REQUEST>
 * 
 * @author jose
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Request {
	 
	@XmlElement(name="CLIENSEC")	
	public String cliensec;
	
    @XmlElement(name="TIPOSEG")	
	public String tiposeg;
	
    @XmlElement(name="POLIZA")	
	public String poliza;

}
