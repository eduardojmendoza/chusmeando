package com.qbe.services.nov.jaxb.mqecoupons.getcupimp;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;

/**
*
* Respuesta recibida del MQMensaje
* 
 <Response>
     <Estado resultado="true" mensaje=""/>
     <RECIBOS>
        <RECIBO>
           <POLIZA>AUS1-00-000001/0000-0001-856577</POLIZA>
           <DESDE>01/10/2016</DESDE>
           <HASTA>01/11/2016</HASTA>
           <CODIGO>901867616</CODIGO>
           <VENCIMIENTO>16/11/2016</VENCIMIENTO>
           <IMPORTE>2549.38</IMPORTE>
        </RECIBO>
        <RECIBO>
           <POLIZA>AUS1-00-000001/0000-0001-981402</POLIZA>
           <DESDE>01/11/2016</DESDE>
           <HASTA>01/12/2016</HASTA>
           <CODIGO>911387512</CODIGO>
           <VENCIMIENTO>16/12/2016</VENCIMIENTO>
           <IMPORTE>2485.44</IMPORTE>
        </RECIBO>
     </RECIBOS>
     <HAY_DATOS_POLIZA_RENOVACION>S</HAY_DATOS_POLIZA_RENOVACION>
</Response>
* 
* 
* @author jose
*
*/
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetCupImp implements ResponseWithEstado {
	@XmlElement( name = "Estado", required = true)
	public Estado estado;
	
	@XmlElementWrapper(name="RECIBOS")
    @XmlElement(name="RECIBO")	
	public List<Recibo> recibos;
	
	@XmlElement(name="HAY_DATOS_POLIZA_RENOVACION")
	public String hayDatosPolRenov;
	
	public GetCupImp(){ 	
    }

	@Override
	public Estado getEstado() {
		return estado;
	}
}
