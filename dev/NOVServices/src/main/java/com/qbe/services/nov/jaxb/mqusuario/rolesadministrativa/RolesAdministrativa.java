package com.qbe.services.nov.jaxb.mqusuario.rolesadministrativa;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;
import com.qbe.services.nov.jaxb.mqmensaje.productosporproductor.Campos;
import com.qbe.services.nov.jaxb.mqmensaje.productosporproductor.Producto;

/**
*
* Respuesta recibida del MQMensaje
* 

<Response>
    <Estado resultado="true" mensaje=""/>
    <REGS>
        <REG>
            <NOMBRE><![CDATA[ABELENDA GUSTAVO DANIEL]]></NOMBRE>
            <NIVEL>OR</NIVEL>
            <CLIENSEC>101878267</CLIENSEC>
        </REG>
        <REG>
            <NOMBRE><![CDATA[ABELENDA GUSTAVO DANIEL]]></NOMBRE>
            <NIVEL>PR</NIVEL>
            <CLIENSEC>101878267</CLIENSEC>
        </REG>
* @author ramiro
*
*/
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class RolesAdministrativa implements ResponseWithEstado {
	@XmlElement( name = "Estado", required = true)
	public Estado estado;
	
	@XmlElementWrapper( name = "REGS", required = true)
    @XmlElement(name="REG")	
	public List<Reg> regs;
	
	public RolesAdministrativa() {
	}

	@Override
	public Estado getEstado() {
		return estado;
	}

}
