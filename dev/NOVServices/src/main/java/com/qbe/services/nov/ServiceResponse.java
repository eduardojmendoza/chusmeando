package com.qbe.services.nov;

public interface ServiceResponse {

	public ResponseWithEstado getResponseWithEstado();
	
	public int getResultCode();
}