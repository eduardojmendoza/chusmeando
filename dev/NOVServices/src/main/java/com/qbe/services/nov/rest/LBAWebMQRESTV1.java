package com.qbe.services.nov.rest;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qbe.services.nov.AisMqService;
import com.qbe.services.nov.impl.LBAWebMQServicesImpl;
import com.qbe.services.nov.jaxb.lbawebmq.getsumaasegurada.GetSumaAsegurada;
import com.qbe.services.nov.jaxb.lbawebmq.getsumaasegurada.GetSumaAseguradaResponse;
import com.qbe.services.nov.jaxb.lbawebmq.getzona.GetZona;
import com.qbe.services.nov.jaxb.lbawebmq.getzona.GetZonaResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ramiro
 *
 */
@Path("/v1/LBAWebMQAPI")
@Api(value="LBAWebMQ")
public class LBAWebMQRESTV1 extends AisMqService {

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	protected LBAWebMQServicesImpl servicesImpl;
	
	public LBAWebMQRESTV1() {
	}
	
	public LBAWebMQServicesImpl getServicesImpl() {
		if (servicesImpl == null ) {
			this.servicesImpl = (LBAWebMQServicesImpl)getContext().getBean("LBAWebMQServices");
		}
		return servicesImpl;
	}
	

	@GET
	@Path("/getZona")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})  
	@ApiOperation(value = "getZonaJson", notes = "...", response = GetZonaResponse.class)
	public Response getZonaJson(
			@DefaultValue("") @QueryParam("ramopcod") String ramopcod,
			@DefaultValue("") @QueryParam("provicod") String provicod,
			@DefaultValue("") @QueryParam("cpacodpo") String cpacodpo) throws Exception {

		GetZonaResponse result = getServicesImpl().getZona(ramopcod, provicod, cpacodpo);
        String json = NovRestServicesUtil.marshalAsJson(GetZona.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(json).build();
	}

	@GET
	@Path("/getZona")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	@ApiOperation(value = "getZonaXml", notes = "...", response = GetZonaResponse.class)
	public Response getZonaXml(
			@DefaultValue("") @QueryParam("ramopcod") String ramopcod,
			@DefaultValue("") @QueryParam("provicod") String provicod,
			@DefaultValue("") @QueryParam("cpacodpo") String cpacodpo) throws Exception {
		
		GetZonaResponse result = getServicesImpl().getZona(ramopcod, provicod, cpacodpo);
        String xml = NovRestServicesUtil.marshalAsXml(GetZona.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}

	
	@GET
	@Path("/getSumaAsegurada")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})  
	@ApiOperation(value = "getSumaAseguradaJson", notes = "...", response = GetSumaAseguradaResponse.class)
	public Response getSumaAseguradaJson(
			@DefaultValue("") @QueryParam("aumodcod") String aumodcod,
			@DefaultValue("") @QueryParam("auprocod") String auprocod) throws Exception {

		GetSumaAseguradaResponse result = getServicesImpl().getSumaAsegurada(aumodcod, auprocod);
        String json = NovRestServicesUtil.marshalAsJson(GetSumaAsegurada.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(json).build();
	}

	@GET
	@Path("/getSumaAsegurada")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	@ApiOperation(value = "getSumaAseguradaXml", notes = "...", response = GetSumaAseguradaResponse.class)
	public Response getSumaAseguradaXml(
			@DefaultValue("") @QueryParam("aumodcod") String aumodcod,
			@DefaultValue("") @QueryParam("auprocod") String auprocod) throws Exception {
		
		GetSumaAseguradaResponse result = getServicesImpl().getSumaAsegurada(aumodcod, auprocod);
        String xml = NovRestServicesUtil.marshalAsXml(GetSumaAsegurada.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}

		
	
	
}