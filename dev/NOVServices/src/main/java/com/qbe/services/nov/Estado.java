package com.qbe.services.nov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Representa el estado de la ejecución de una operación.
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Estado {

    @XmlAttribute(name="mensaje")	
	public String mensaje;
	
    @XmlAttribute(name="resultado")	
	public Boolean resultado;
}
