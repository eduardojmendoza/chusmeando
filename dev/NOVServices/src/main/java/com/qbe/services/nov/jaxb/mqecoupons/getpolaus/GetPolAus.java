package com.qbe.services.nov.jaxb.mqecoupons.getpolaus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;
import com.qbe.services.nov.jaxb.mqemision.putsolicaus.Resultado;

/**
*
* Respuesta recibida del MQMensaje
* 
 <Response>
       <Estado resultado="true" mensaje=""/>
       <PARAMETROS>
         <CLIENSEC>000412335</CLIENSEC>
         <TIPOSEG>AUS1</TIPOSEG>
         <POLIZA>0001856577</POLIZA>
       </PARAMETROS>
       <POLIZANN>00</POLIZANN>
       <POLIZSEC>000001</POLIZSEC>
       <CERTIPOL>0000</CERTIPOL>
       <CERTIANN>0001</CERTIANN>
       <CERTISEC>856577</CERTISEC>
       <SUPLENUM>0000</SUPLENUM>
       <CCOBRO>0001</CCOBRO>
       <BIEN><![CDATA[PEUGEOT                       207]]></BIEN>
       <SUSCRIPTO>No</SUSCRIPTO>
       <POLIZA_RENOVACION>AUS1-00-000001/0000-0001-981402</POLIZA_RENOVACION>
       <MARCA_RENOVACION desc='Poliza Anterior'>2</MARCA_RENOVACION>
 </Response>
* 
* 
* @author jose
*
*/
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetPolAus implements ResponseWithEstado {
	@XmlElement( name = "Estado", required = true)
	public Estado estado;
	
	@XmlElement(name="PARAMETROS")	
	public Parametro parametros;
	
	@XmlElement(name="POLIZANN")	
	public String polizann;
	
	@XmlElement(name="POLIZSEC")	
	public String polizsec;
	
	@XmlElement(name="CERTIPOL")	
	public String certipol;
	
	@XmlElement(name="CERTIANN")	
	public String certiann;
	
	@XmlElement(name="CERTISEC")	
	public String certisec;
	
	@XmlElement(name="SUPLENUM")	
	public String suplenum;
	
	@XmlElement(name="CCOBRO")	
	public String ccobro;
	
	@XmlElement(name="BIEN")	
	public String bien;
	
	@XmlElement(name="SUSCRIPTO")	
	public String suscripto;
	
	@XmlElement(name="POLIZA_RENOVACION")	
	public String poliza_renovacion;
	
	@XmlElement(name="MARCA_RENOVACION")	
	public MarcaRenovacion marca_renovacion;
    
    public GetPolAus(){ 	
    }

	@Override
	public Estado getEstado() {
		return estado;
	}
}
