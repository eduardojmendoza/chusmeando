package com.qbe.services.nov.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.siniestros.impl.lbaw_siniMQGenerico;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.vbcompat.string.StringHolder;

/**
 * Implementación del servicio SiniMQGenericoServicesImpl 
 * 
 * @author ramiro
 * @author cesar
 *
 */
public class SiniMQGenericoServicesImpl {

	
	private static final String EMPTY_STRING = "";
	static String propsFilename = System.getProperty("cms.config.properties");
	static final String regexDefinicion = "<DEFINICION>+[_a-zA-Z0-9.]+</DEFINICION>";
	static final String KEYDEFINICIONXML = "siniestro.definicion.path";
	
	protected Logger logger = Logger.getLogger(getClass().getName());
		
	public SiniMQGenericoServicesImpl () {
	}
	
	private lbaw_siniMQGenerico getSiniMQGenMensaje() {
		lbaw_siniMQGenerico sinmqgen = new lbaw_siniMQGenerico();
		return sinmqgen;
	}
	
	public GenericServiceResponse getDatosClienteRiesgo(String usuarcod, String ramopcod, String polizann,String polizsec,String certipol,String certiann,String certisec,String suplenum,String fecstro) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		String reqXML = createGetDatosClienteRiesgoRequestString(usuarcod, ramopcod,polizann,polizsec,certipol,certiann,certisec,suplenum,fecstro);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = sinmqgen.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getDatosClienteRiesgo: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	private String createGetDatosClienteRiesgoRequestString(String usuarcod, String ramopcod,String polizann,String polizsec,String certipol,String certiann,String certisec,String suplenum,String fecstro) {
		return  "<Request>"
				+"            <DEFINICION>siniestroDenuncia_getDatosCLIENTE_RIESGO.xml</DEFINICION>"
				+"            <USUARCOD>"+usuarcod+"</USUARCOD>"
				+"            <RAMOPCOD>"+ramopcod+"</RAMOPCOD>"
				+"            <POLIZANN>"+polizann+"</POLIZANN>"
				+"            <POLIZSEC>"+polizsec+"</POLIZSEC>"
				+"            <CERTIPOL>"+certipol+"</CERTIPOL>"
				+"            <CERTIANN>"+certiann+"</CERTIANN>"
				+"            <CERTISEC>"+certisec+"</CERTISEC>"
				+"            <SUPLENUM>"+suplenum+"</SUPLENUM>"
				+"            <FECSTRO>"+fecstro+"</FECSTRO>"
				+"        </Request>";
	}
	
	
	public GenericServiceResponse getListadoClientes(String reqXML) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		//String reqXML = createGetListadoClientesRequestString(usuarcod,nivelclas,cliensecas,nivelcla1,cliensec1,nivelcla2,cliensec2,nivelcla3,cliensec3,ramo,poliza,certif,patente,apellido,documtip,documdat,fechasinie,tiposinie,swbusca,orden,columna,nroqry,retoma,retornarrequest);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = sinmqgen.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getDatosClienteRiesgo: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	private String createGetListadoClientesRequestString(String usuarcod,String nivelclas,String cliensecas,String nivelcla1,String cliensec1,String nivelcla2,String cliensec2,String nivelcla3,String cliensec3,String ramo,String poliza,String certif,String patente,String apellido,String documtip,String documdat,String fechasinie,String tiposinie,String swbusca,String orden,String columna,String nroqry,String retoma,String retornarrequest) {
		return  
				"<Request>"
				+"    <DEFINICION>siniestroDenuncia_getListadoClientes.xml</DEFINICION>"
				+"    <USUARCOD>"+usuarcod+"</USUARCOD>"
				+"    <NIVELCLAS>"+nivelclas+"</NIVELCLAS>"
				+"    <CLIENSECAS>"+cliensecas+"</CLIENSECAS>"
				+"    <NIVELCLA1>"+nivelcla1+"</NIVELCLA1>"
				+"    <CLIENSEC1>"+cliensec1+"</CLIENSEC1>"
				+"    <NIVELCLA2>"+nivelcla2+"</NIVELCLA2>"
				+"    <CLIENSEC2>"+cliensec2+"</CLIENSEC2>"
				+"    <NIVELCLA3>"+nivelcla3+"</NIVELCLA3>"
				+"    <CLIENSEC3>"+cliensec3+"</CLIENSEC3>"
				+"    <RAMO>"+ramo+"</RAMO>"
				+"    <POLIZA>"+poliza+"</POLIZA>"
				+"    <CERTIF>"+certif+"</CERTIF>"
				+"    <PATENTE>"+patente+"</PATENTE>"
				+"    <APELLIDO>"+apellido+"</APELLIDO>"
				+"    <DOCUMTIP>"+documtip+"</DOCUMTIP>"
				+"    <DOCUMDAT>"+documdat+"</DOCUMDAT>"
				+"    <FECHASINIE>"+fechasinie+"</FECHASINIE>"
				+"    <TIPOSINIE>"+tiposinie+"</TIPOSINIE>"
				+"    <SWBUSCA>"+swbusca+"</SWBUSCA>"
				+"    <ORDEN>"+orden+"</ORDEN>"
				+"    <COLUMNA>"+columna+"</COLUMNA>"
				+"    <NROQRY>"+nroqry+"</NROQRY>"
				+"    <RETOMA>"+retoma+"<RETOMA>"
				+"    <RETORNARREQUEST>"+retornarrequest+"<RETORNARREQUEST>"
				+"</Request>";	
				
	}
	
	public GenericServiceResponse putForm1AIS(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();		
		if ("PROD".equals(loadConfig(KEYDEFINICIONXML))){
			getDefXml(request);
		}
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de putForm1AIS: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	

	public GenericServiceResponse putForm2AIS(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		if ("PROD".equals(loadConfig(KEYDEFINICIONXML))){
			getDefXml(request);
		}
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);		
		logger.log(Level.FINE, "Resultado de ejecución de putForm2AIS: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	public GenericServiceResponse putForm3AIS(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();		
		if ("PROD".equals(loadConfig(KEYDEFINICIONXML))){
			getDefXml(request);
		}
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de putForm3AIS: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	public GenericServiceResponse putForm5AIS(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		if ("PROD".equals(loadConfig(KEYDEFINICIONXML))){
			getDefXml(request);
		}
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de putForm5AIS: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	
	public GenericServiceResponse getComboRamo(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getComboRamo: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	
	public GenericServiceResponse getComboDOCUMTIP(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getComboDOCUMTIP: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	
	
	public GenericServiceResponse getComboCOMPANIA(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getComboCOMPANIA: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	
	
	public GenericServiceResponse getComboJURISCOD(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getComboJURISCOD: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	
	public GenericServiceResponse getComboROBOSLUG(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getComboROBOSLUG: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	
	public GenericServiceResponse getComboAUUSOCOD(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getComboAUUSOCOD: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	
	
	public GenericServiceResponse getComboAUTIPCOD(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getComboAUTIPCOD: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	
	
	public GenericServiceResponse getComboCENSICOD(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getComboCENSICOD: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	
	
	

	public GenericServiceResponse getComboAUCOLCOD(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getComboAUCOLCOD: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	
	
	public GenericServiceResponse getComboAUMARCOD(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getComboAUMARCOD: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}
	
	
	public GenericServiceResponse getComboAUMODCOD(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getComboAUMODCOD: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		
	}

	public GenericServiceResponse putForm6AIS(String request) throws Exception{
			lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
			StringHolder sh = new StringHolder();
			if ("PROD".equals(loadConfig(KEYDEFINICIONXML))){
				request = getDefXml(request);
			}
			logger.log(Level.INFO, "Recibido request: " + request);
			int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
			logger.log(Level.FINE, "Resultado de ejecución de putForm6AIS: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
			if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
			return new GenericServiceResponse(resultCode,sh.getValue());
			
		}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse getProveedores(String request) throws Exception{
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getProveedores: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	/**
	 * Método que retorna el valor de la key que esta dentro del archivo de
	 * propiedades
	 * 
	 * @param key
	 * @return
	 * @throws IOException
	 */
	private static String loadConfig(String key) throws IOException {
		Properties fileProper = new Properties();
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(propsFilename);
		fileProper.load(in);
		in.close();
		return fileProper.getProperty(key);

	}
	
	
	/**
	 * @param request
	 * @throws IOException 
	 */
	private String getDefXml(String request) throws IOException {
		String defName = request.substring(request.indexOf("<DEFINICION>")+12, request.indexOf("</DEFINICION>"));
		return request.replaceAll(regexDefinicion,"<DEFINICION>PROD/"+ defName+"</DEFINICION>");
	}

	public GenericServiceResponse getDatosCliente(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getDatosCliente: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}


    public GenericServiceResponse getDesbloqCLIENTE(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getDesbloqCLIENTE: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
    }

    public GenericServiceResponse getListadoRiesgos(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getListadoRiesgos: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
    }

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse putForm7AIS(String request) throws Exception {
		lbaw_siniMQGenerico sinmqgen = getSiniMQGenMensaje();		
		StringHolder sh = new StringHolder();
		if ("PROD".equals(loadConfig(KEYDEFINICIONXML))){
			request = getDefXml(request);
		}
		logger.log(Level.INFO, "Recibido request: " + request);
		int resultCode = sinmqgen.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de putForm7AIS: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

}
