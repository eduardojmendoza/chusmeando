package com.qbe.services.nov.soap;

import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.qbe.services.nov.ConsultaMQGestionService;
import com.qbe.services.nov.jaxb.mqmensajegestion.perfilusuario.PerfilUsuarioResponse;

@WebService(
		targetNamespace = "http://nov.services.qbe.com/", 
		portName = "ConsultaMQGestionPort", 
		serviceName = "ConsultaMQGestion")
public class ConsultaMQGestionSOAP extends ConsultaMQGestionService {

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	public ConsultaMQGestionSOAP() {
	}

	@WebMethod(operationName = "PerfilUsuario_1000", action = "PerfilUsuario_1000")
	public PerfilUsuarioResponse perfilUsuario(
			@WebParam(name = "AplicarXSL") String aplicarXSL,
			@WebParam(name = "Usuario") String usuario) throws Exception {

		return getServicesImpl().perfilUsuario_1000(aplicarXSL, usuario);
	}
	
}
