package com.qbe.services.nov.jaxb.mqmensaje.renovacionporusuario;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;

/**
 *
 * Respuesta recibida del MQMensaje
 * 
  <Response>
	<Estado resultado='true' mensaje='' />
	<CAMPOS>
		<PRODUCTOS>
			<PRODUCTO>

 * @author ramiro
 *
 */
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class RenovacionPorUsuario implements ResponseWithEstado {
	@XmlElement( name = "Estado", required = true)
	public Estado estado;
	
	@XmlElement( name = "CAMPOS", required = true)
	private Campos campos;

	public RenovacionPorUsuario() {
	}

	@Override
	public Estado getEstado() {
		return estado;
	}
}
