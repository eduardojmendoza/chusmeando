package com.qbe.services.nov.jaxb.mqcotizar.getcotiaus;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;
import com.qbe.services.nov.jaxb.mqmensaje.productosporproductor.Campos;
import com.qbe.services.nov.jaxb.mqmensaje.productosporproductor.Producto;

/**
*
* Respuesta recibida del MQMensaje
* 
<Response>
        <Estado resultado="true" mensaje=""/>
        <COT_NRO>2147611</COT_NRO>
        <CAMPA_COD>1681</CAMPA_COD>
        <AGE_COD>3233</AGE_COD>
        <AGE_CLA>PR</AGE_CLA>
        <CAMPA_TEL/>
        <CAMPA_TEL_FORM/>
        <SUMASEG>00020100000</SUMASEG>
        <SUMALBA>00020100000</SUMALBA>
        <SILUNETA>S</SILUNETA>
        <TIENEHIJOS>S</TIENEHIJOS>
        <PLANES>
            <PLAN>
                <PLANNCOD>01199</PLANNCOD>
                <PLANNDES><![CDATA[Responsabilidad Civil/FRANQUICIA NO APLICABLE]]></PLANNDES>
                <ORDEN>0008</ORDEN>
                <LUNPAR>N</LUNPAR>
                <GRANIZO>N</GRANIZO>
                <ROBOCON>N</ROBOCON>
                <ESRC>S</ESRC>
                <CON_HIJOS>
                    <PRIMA>1442,39</PRIMA>
                    <RECARGOS>0,00</RECARGOS>
                    <IVAIMPOR>302,90</IVAIMPOR>
                    <IVAIMPOA>0,00</IVAIMPOA>
                    <IVARETEN>0,00</IVARETEN>
                    <DEREMI>0,00</DEREMI>
                    <SELLADO>0,00</SELLADO>
                    <INGBRU>0,00</INGBRU>
                    <IMPUES>31,72</IMPUES>
                    <PRECIO>1777,01</PRECIO>
                </CON_HIJOS>
                <SIN_HIJOS>
                    <PRIMA>1311,32</PRIMA>
                    <RECARGOS>0,00</RECARGOS>
                    <IVAIMPOR>275,38</IVAIMPOR>
                    <IVAIMPOA>0,00</IVAIMPOA>
                    <IVARETEN>0,00</IVARETEN>
                    <DEREMI>0,00</DEREMI>
                    <SELLADO>0,00</SELLADO>
                    <INGBRU>0,00</INGBRU>
                    <IMPUES>28,85</IMPUES>
                    <PRECIO>1615,55</PRECIO>
                </SIN_HIJOS>
            </PLAN>

* 
* 
* @author ramiro
*
*/
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetCotiAUS implements ResponseWithEstado {
	@XmlElement( name = "Estado", required = true)
	public Estado estado;
	
    @XmlElement(name="COT_NRO")	
	public String cotNro;

    @XmlElement(name="CAMPA_COD")	
	public String campaCod;

    @XmlElement(name="AGE_COD")	
	public String ageCod;

    @XmlElement(name="AGE_CLA")	
	public String ageCla;

    @XmlElement(name="CAMPA_TEL")	
	public String campaTel;

    @XmlElement(name="CAMPA_TEL_FORM")	
	public String campaTelForm;

    @XmlElement(name="SUMASEG")	
	public String sumaseg;

    @XmlElement(name="SUMALBA")	
	public String sumalba;

    @XmlElement(name="SILUNETA")	
	public String siluneta;

    @XmlElement(name="TIENEHIJOS")	
	public String tienehijos;

	@XmlElementWrapper( name = "PLANES", required = true)
    @XmlElement(name="PLAN")	
	public List<Plan> planes;
	
	public GetCotiAUS() {
	}

	@Override
	public Estado getEstado() {
		return estado;
	}

}
