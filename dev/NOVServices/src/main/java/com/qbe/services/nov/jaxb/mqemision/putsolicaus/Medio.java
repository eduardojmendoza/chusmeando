package com.qbe.services.nov.jaxb.mqemision.putsolicaus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
            <MEDIO>
                <MEDCOSEC>0</MEDCOSEC>
                <MEDCOCOD>EMA</MEDCOCOD>
                <MEDCODAT>MARIA.ARMATI@zurich.com.AR</MEDCODAT>
                <MEDCOSYS/>
            </MEDIO>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Medio {
	
    @XmlElement(name="MEDCOSEC")	
	public String medcosec;
	
    @XmlElement(name="MEDCOCOD")	
	public String medcocod;
	
    @XmlElement(name="MEDCODAT")	
	public String medcodat;
	
    @XmlElement(name="MEDCOSYS")	
	public String medcosys;
	
    
}
