package com.qbe.services.nov.jaxb.mqmensaje.renovacionporusuario;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**

                <RESULTADO>
                    <RAMOPCOD>AUS1</RAMOPCOD>
                    <RAMOPDES>AUTOMOTORES SCORING EN PESOS</RAMOPDES>
                    <RAMOPDAB>AUTOMOTORES</RAMOPDAB>
                    <MONENCOD>1</MONENCOD>
                </RESULTADO>
 * 
 * 
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Resultado {

    @XmlElement(name="RAMOPCOD")	
	public String ramopcod;
	
    @XmlElement(name="RAMOPDES")	
	public String ramopdes;
	
    @XmlElement(name="RAMOPDAB")	
	public String ramopdab;
	
    @XmlElement(name="MONENCOD")	
	public String monencod;
	
}
