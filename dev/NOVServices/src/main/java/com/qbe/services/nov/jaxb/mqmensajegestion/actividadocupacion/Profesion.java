package com.qbe.services.nov.jaxb.mqmensajegestion.actividadocupacion;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Profesion {
	@XmlElement(name="PROFECOD", required = true)	
	public String profecod;
	
	@XmlElement(name="PROFEDES", required = true)	
	public String profedes;
			
}
