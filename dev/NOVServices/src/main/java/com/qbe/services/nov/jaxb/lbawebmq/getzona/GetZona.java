package com.qbe.services.nov.jaxb.lbawebmq.getzona;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;

/**
*
* Respuesta recibida del MQMensaje
* 
<Response>
        <Estado resultado="true" mensaje=""/>

* 
* 
* @author ramiro
*
*/
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetZona implements ResponseWithEstado {
	@XmlElement( name = "Estado", required = true)
	public Estado estado;

	@XmlElement( name = "CODIZONA", required = true)
	public String codizona;

	@XmlElement( name = "ZONASDES", required = true)
	public String zonasdes;

	public GetZona() {
	}

	@Override
	public Estado getEstado() {
		return estado;
	}

}
