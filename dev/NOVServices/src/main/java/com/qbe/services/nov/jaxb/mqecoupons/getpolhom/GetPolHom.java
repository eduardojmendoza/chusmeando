package com.qbe.services.nov.jaxb.mqecoupons.getpolhom;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;

/**
*
* Respuesta recibida del MQMensaje
* 
 <Response>
        <Estado resultado="true" mensaje=""/>
        <REQUEST>
          <CLIENSEC>101976143</CLIENSEC>
          <TIPOSEG>HOM1</TIPOSEG>
	      <POLIZA>0000004773</POLIZA>
        </REQUEST>
        <POLIZANN>00</POLIZANN>
        <POLIZSEC>157838</POLIZSEC>
        <CERTIPOL>0000</CERTIPOL>
        <CERTIANN>0000</CERTIANN>
        <CERTISEC>004773</CERTISEC>
        <SUPLENUM>0002</SUPLENUM>
        <CCOBRO>0001</CCOBRO>
        <BIEN><![CDATA[CASA/PB/1ER.PISO                        ]]></BIEN>
        <DIRBIEN><![CDATA[LA RIOJA                           04514         ALBERDI                                                                          ]]></DIRBIEN>
        <SUSCRIPTO>No</SUSCRIPTO>
        <POLIZA_RENOVACION>HOM1-00-157838/0000-0000-005228</POLIZA_RENOVACION>
        <MARCA_RENOVACION desc="Poliza Anterior">2</MARCA_RENOVACION>
 </Response>
* 
* 
* @author jose
*
*/
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetPolHom implements ResponseWithEstado {
	@XmlElement( name = "Estado", required = true)
	public Estado estado;
	
	@XmlElement(name="REQUEST")	
	public Request request;
	
	@XmlElement(name="POLIZANN")	
	public String polizann;
	
	@XmlElement(name="POLIZSEC")	
	public String polizsec;
	
	@XmlElement(name="CERTIPOL")	
	public String certipol;
	
	@XmlElement(name="CERTIANN")	
	public String certiann;
	
	@XmlElement(name="CERTISEC")	
	public String certisec;
	
	@XmlElement(name="SUPLENUM")	
	public String suplenum;
	
	@XmlElement(name="CCOBRO")	
	public String ccobro;
	
	@XmlElement(name="BIEN")	
	public String bien;
	
	@XmlElement(name="DIRBIEN")	
	public String dirbien;
	
	@XmlElement(name="SUSCRIPTO")	
	public String suscripto;
	
	@XmlElement(name="POLIZA_RENOVACION")	
	public String poliza_renovacion;
	
	@XmlElement(name="MARCA_RENOVACION")	
	public MarcaRenovacion marca_renovacion;
	
    public GetPolHom(){  	
    }

	@Override
	public Estado getEstado() {
		return estado;
	}
}
