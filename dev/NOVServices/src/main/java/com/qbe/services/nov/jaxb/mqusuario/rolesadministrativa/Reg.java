package com.qbe.services.nov.jaxb.mqusuario.rolesadministrativa;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * 
        <REG>
            <NOMBRE><![CDATA[ABELENDA GUSTAVO DANIEL]]></NOMBRE>
            <NIVEL>PR</NIVEL>
            <CLIENSEC>101878267</CLIENSEC>
        </REG>
 * 
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Reg {
	
    @XmlElement(name="NOMBRE")	
	public String nombre;
	
    @XmlElement(name="NIVEL")	
	public String nivel;
	
    @XmlElement(name="CLIENSEC")	
	public String cliensec;
	
}
