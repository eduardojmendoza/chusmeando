package com.qbe.services.nov.impl;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.ovmq.impl.Lbaw_OVRetencionesComisiones;
import com.qbe.services.ovmq.impl.lbaw_OVAnulxPagoDetalles;
import com.qbe.services.ovmq.impl.lbaw_OVBrutoFacturar;
import com.qbe.services.ovmq.impl.lbaw_OVCartasDetRecibos;
import com.qbe.services.ovmq.impl.lbaw_OVCartasReclaDetalle;
import com.qbe.services.ovmq.impl.lbaw_OVCliensecxUsu;
import com.qbe.services.ovmq.impl.lbaw_OVClientesConsulta;
import com.qbe.services.ovmq.impl.lbaw_OVCoberturasDetalle;
import com.qbe.services.ovmq.impl.lbaw_OVCtaCteDetalle;
import com.qbe.services.ovmq.impl.lbaw_OVCtaCteaPC;
import com.qbe.services.ovmq.impl.lbaw_OVDatosGralCliente;
import com.qbe.services.ovmq.impl.lbaw_OVDetalleRecibo;
import com.qbe.services.ovmq.impl.lbaw_OVDeudaVceDetPoliza;
import com.qbe.services.ovmq.impl.lbaw_OVDeudaVceDetalles;
import com.qbe.services.ovmq.impl.lbaw_OVEndososComi;
import com.qbe.services.ovmq.impl.lbaw_OVEndososPrima;
import com.qbe.services.ovmq.impl.lbaw_OVExigibleDetPoliza;
import com.qbe.services.ovmq.impl.lbaw_OVExigibleDetalles;
import com.qbe.services.ovmq.impl.lbaw_OVExigibleaPC;
import com.qbe.services.ovmq.impl.lbaw_OVLOGetCantRecibos;
import com.qbe.services.ovmq.impl.lbaw_OVLOGetNroLiqui;
import com.qbe.services.ovmq.impl.lbaw_OVLOGetRecibos;
import com.qbe.services.ovmq.impl.lbaw_OVLOLiquixProd;
import com.qbe.services.ovmq.impl.lbaw_OVLOReimpLiqui;
import com.qbe.services.ovmq.impl.lbaw_OVOpeEmiDetalles;
import com.qbe.services.ovmq.impl.lbaw_OVOpePendDetalles;
import com.qbe.services.ovmq.impl.lbaw_OVSiniConsPagos;
import com.qbe.services.ovmq.impl.lbaw_OVSiniConsulta;
import com.qbe.services.ovmq.impl.lbaw_OVSiniDatosAdic;
import com.qbe.services.ovmq.impl.lbaw_OVSiniDetalle;
import com.qbe.services.ovmq.impl.lbaw_OVSiniGestiones;
import com.qbe.services.ovmq.impl.lbaw_OVSiniListadoDetalles;
import com.qbe.services.ovmq.impl.lbaw_OVSiniPagosDetalles;
import com.qbe.services.ovmq.impl.lbaw_OVSiniSubcober;
import com.qbe.services.ovmq.impl.lbaw_OVSitCobranzas;
import com.qbe.vbcompat.string.StringHolder;

/**
 * 
 * @author cbonilla
 *
 */
public class OVMQSoloServicesImpl {
	
	protected Logger logger = Logger.getLogger(getClass().getName());
	
	private static final String EMPTY_STRING = "";

	public GenericServiceResponse endososPrima(String request) throws Exception {	
			lbaw_OVEndososPrima ovep = new lbaw_OVEndososPrima();
			StringHolder sh = new StringHolder();
			logger.log(Level.FINE, "Recibido request: " + request);
			int resultCode = ovep.IAction_Execute(request, sh, EMPTY_STRING);
			logger.log(Level.FINE, "Resultado de ejecución de endososPrima: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
			if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
			return new GenericServiceResponse(resultCode,sh.getValue());
		}
	
	
	
	public GenericServiceResponse datosGralCliente(String request) throws Exception {	
		lbaw_OVDatosGralCliente ovdgd = new lbaw_OVDatosGralCliente();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovdgd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de datosGralCliente: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	

	public GenericServiceResponse sitCobranzas(String request) throws Exception {	
		lbaw_OVSitCobranzas ovdgd = new lbaw_OVSitCobranzas();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovdgd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVSitCobranzas: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse siniConsulta(String request) throws Exception {	
		lbaw_OVSiniConsulta ovsc = new lbaw_OVSiniConsulta();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsc.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVSiniConsulta: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	public GenericServiceResponse endososComi(String request) throws Exception {	
		lbaw_OVEndososComi ovsc = new lbaw_OVEndososComi();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsc.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de endososComi: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse coberturasDetalle(String request) throws Exception {	
		lbaw_OVCoberturasDetalle ovsc = new lbaw_OVCoberturasDetalle();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsc.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVCoberturasDetalle: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse clientesConsulta(String request) throws Exception {	
		lbaw_OVClientesConsulta ovsc = new lbaw_OVClientesConsulta();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsc.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVClientesConsulta: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	

	
	public GenericServiceResponse detalleRecibo(String request) throws Exception {	
		lbaw_OVDetalleRecibo ovsc = new lbaw_OVDetalleRecibo();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsc.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de DetalleRecibo: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	

	public GenericServiceResponse siniConsPagos(String request) throws Exception {	
		lbaw_OVSiniConsPagos ovsc = new lbaw_OVSiniConsPagos();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsc.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de siniConsPagos: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	

	public GenericServiceResponse opePendDetalles(String request) throws Exception {	
		lbaw_OVOpePendDetalles ovsc = new lbaw_OVOpePendDetalles();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsc.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de opePendDetalles: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse opeEmiDetalles(String request) throws Exception {	
		lbaw_OVOpeEmiDetalles ovsc = new lbaw_OVOpeEmiDetalles();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsc.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVOpeEmiDetalles: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	

	public GenericServiceResponse exigibleDetalles(String request) throws Exception {	
		lbaw_OVExigibleDetalles ovsc = new lbaw_OVExigibleDetalles();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsc.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_exigibleTotales: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	
	public GenericServiceResponse exigibleDetPoliza(String request) throws Exception {	
		lbaw_OVExigibleDetPoliza ovsc = new lbaw_OVExigibleDetPoliza();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsc.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_exigibleTotales: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	

	public GenericServiceResponse exigibleaPC(String request) throws Exception {	
		lbaw_OVExigibleaPC ovsc = new lbaw_OVExigibleaPC();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsc.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVExigibleaPC: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	
	public GenericServiceResponse deudaVceDetPoliza(String request) throws Exception {	
		lbaw_OVDeudaVceDetPoliza ovsc = new lbaw_OVDeudaVceDetPoliza();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsc.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVExigibleaPC: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	

	public GenericServiceResponse deudaVceDetalles(String request) throws Exception {	
		lbaw_OVDeudaVceDetalles ovsc = new lbaw_OVDeudaVceDetalles();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsc.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVDeudaVceDetalles: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	public GenericServiceResponse siniDetalle(String request) throws Exception {	
		lbaw_OVSiniDetalle ovsd = new lbaw_OVSiniDetalle();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVSiniDetalle: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	public GenericServiceResponse siniDatosAdic(String request) throws Exception {	
		lbaw_OVSiniDatosAdic ovsd = new lbaw_OVSiniDatosAdic();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVSiniDatosAdic: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse siniGestiones(String request) throws Exception {	
		lbaw_OVSiniGestiones ovsd = new lbaw_OVSiniGestiones();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVSiniGestiones: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse siniSubcober(String request) throws Exception {	
		lbaw_OVSiniSubcober ovsd = new lbaw_OVSiniSubcober();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVSiniSubcober: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	
	public GenericServiceResponse cartasReclaDetalle(String request) throws Exception {	
		lbaw_OVCartasReclaDetalle ovsd = new lbaw_OVCartasReclaDetalle();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVCartasReclaDetalle: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse cartasDetRecibos(String request) throws Exception {	
		lbaw_OVCartasDetRecibos ovsd = new lbaw_OVCartasDetRecibos();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVCartasDetRecibos: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	

	public GenericServiceResponse siniListadoDetalles(String request) throws Exception {	
		lbaw_OVSiniListadoDetalles ovsd = new lbaw_OVSiniListadoDetalles();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVSiniListadoDetalles: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse lOGetRecibos(String request) throws Exception {	
		lbaw_OVLOGetRecibos ovsd = new lbaw_OVLOGetRecibos();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVLOGetRecibos: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse lOGetCantRecibos(String request) throws Exception {	
		lbaw_OVLOGetCantRecibos ovsd = new lbaw_OVLOGetCantRecibos();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVLOGetCantRecibos: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse lOGetNroLiqui(String request) throws Exception {	
		lbaw_OVLOGetNroLiqui ovsd = new lbaw_OVLOGetNroLiqui();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVLOGetNroLiqui: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse lOLiquixProd(String request) throws Exception {	
		lbaw_OVLOLiquixProd ovsd = new lbaw_OVLOLiquixProd();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVlOLiquixProd: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse lOReimpLiqui(String request) throws Exception {	
		lbaw_OVLOReimpLiqui ovsd = new lbaw_OVLOReimpLiqui();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVLOReimpLiqui: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse siniPagosDetalles(String request) throws Exception {	
		lbaw_OVSiniPagosDetalles ovsd = new lbaw_OVSiniPagosDetalles();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVLOReimpLiqui: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse brutoFacturar(String request) throws Exception {	
		lbaw_OVBrutoFacturar ovsd = new lbaw_OVBrutoFacturar();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVBrutoFacturar: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse ctaCteDetalle(String request) throws Exception {	
		lbaw_OVCtaCteDetalle ovsd = new lbaw_OVCtaCteDetalle();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVCtaCteDetalle: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse ctaCteaPC(String request) throws Exception {	
		lbaw_OVCtaCteaPC ovsd = new lbaw_OVCtaCteaPC();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVCtaCteDetalle: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	

	public GenericServiceResponse anulxPagoDetalles(String request) throws Exception {	
		lbaw_OVAnulxPagoDetalles ovsd = new lbaw_OVAnulxPagoDetalles();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVAnulxPagoDetalles: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse cliensecxUsu(String request) throws Exception {	
		lbaw_OVCliensecxUsu ovsd = new lbaw_OVCliensecxUsu();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovsd.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de lbaw_OVAnulxPagoDetalles: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}



	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse getRetencionesComisiones(String request) throws Exception{
		Lbaw_OVRetencionesComisiones ovrc = new Lbaw_OVRetencionesComisiones();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = ovrc.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de Lbaw_OVRetencionesComisiones: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	
	
}
