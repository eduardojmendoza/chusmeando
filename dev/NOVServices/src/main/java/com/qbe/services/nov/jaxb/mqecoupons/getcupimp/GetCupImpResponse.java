package com.qbe.services.nov.jaxb.mqecoupons.getcupimp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.qbe.services.nov.ResponseWithEstado;
import com.qbe.services.nov.ServiceResponse;

/**
 * Wrapper que incluye el resultCode
 * 
 * 
 * 
 * 
 *  
 * @author jose
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class GetCupImpResponse implements ServiceResponse{

	@XmlElement( name = "resultCode", required = true)
	public int resultCode;
	
	@XmlElement( name = "response", required = true)
	public GetCupImp response;
	
	public GetCupImpResponse(){		
	}
	
	@Override
	public ResponseWithEstado getResponseWithEstado() {
		return response;
	}

	@Override
	public int getResultCode() {
		return resultCode;
	}
	
	 public GetCupImp getResponse() {
			return response;
		}
}
