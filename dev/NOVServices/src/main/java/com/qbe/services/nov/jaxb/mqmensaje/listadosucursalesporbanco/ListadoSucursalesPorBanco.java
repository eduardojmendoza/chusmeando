package com.qbe.services.nov.jaxb.mqmensaje.listadosucursalesporbanco;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;

/**
 * 
 * 
<Response>
        <Estado resultado="true" mensaje=""/>
        <options>
            <option value="0001">0001 - 25 DE MAYO</option>
            <option value="0013">0013 - 25 DE MAYO</option>
            <option value="0039">0039 - 25 DE MAYO</option>
            <option value="0042">0042 - CABALLITO</option>
            <option value="0047">0047 - NEUQUEN</option>
            <option value="0051">0051 - MENDOZA</option>
            <option value="0052">0052 - TUCUMAN</option>
            <option value="0053">0053 - CONCEPCION</option>
            <option value="0054">0054 - AVELLANEDA</option>
            <option value="0055">0055 - CORDOBA</option>    
 * 
 * @author ramiro
 *
 */
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class ListadoSucursalesPorBanco implements ResponseWithEstado {

	@XmlElement( name = "Estado", required = true)
	public Estado estado;
	
    @XmlElementWrapper(name="options")
	@XmlElement(name="option")	
	public List<Option> options;

	@Override
	public Estado getEstado() {
		return estado;
	}
}
