package com.qbe.services.nov.rest;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qbe.services.exutils.ExceptionUtils;
import com.qbe.services.nov.AisMqService;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.impl.OVSeguroOnlineServiceImpl;

import io.swagger.annotations.Api;

/**
 * 
 * @author Mauro Plaquin
 * 
 */
@Path("/v1/SeguroOnlineAPI")
@Api(value = "SeguroOnline")
public class OVSeguroOnlineRESTV1 extends AisMqService {

	protected Logger logger = Logger.getLogger(getClass().getName());

	public OVSeguroOnlineRESTV1() {
	}

	private OVSeguroOnlineServiceImpl getServicesImpl() {
		OVSeguroOnlineServiceImpl ovmq = new OVSeguroOnlineServiceImpl();
		return ovmq;
	}

	@GET
	@Path("/version")
	public Response getVersion() throws Exception {
		try {
			return Response.status(Response.Status.OK).header("VERSION", "1").build();
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(Response.Status.PRECONDITION_FAILED).entity(e.getMessage()).build();
		}
	}

	@POST
	@Path("/nbwsA_Alta")
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response nbwsA_Alta(String request) throws Exception {
		GenericServiceResponse result;
		try {
			result = getServicesImpl().nbwsA_Alta(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
			return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}

	@POST
	@Path("/SusyDesusepoliza")
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response SusyDesusepoliza(String request) throws Exception {
		GenericServiceResponse result;
		try {
			result = getServicesImpl().SusyDesusepoliza(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
			return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/productosClientes")
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response productosClientes(String request) throws Exception {
		GenericServiceResponse result;
		try {
			result = getServicesImpl().productosClientes(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
			return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/listadoePolizas")
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response listadoePolizas(String request) throws Exception {
		GenericServiceResponse result;
		try {
			result = getServicesImpl().listadoePolizas(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
			return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}

	
	
	//30/08/17 ----------------------------------------------------------->
	@POST
	@Path("/envioPoliza")
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response envioPoliza(String request) throws Exception {
		GenericServiceResponse result;
		try {
			result = getServicesImpl().envioPoliza(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
			return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/seguimientoCobranzas")
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response seguimientoCobranzas(String request) throws Exception {
		GenericServiceResponse result;
		try {
			result = getServicesImpl().seguimientoCobranzas(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
			return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/seguimientoSiniestros")
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response seguimientoSiniestros(String request) throws Exception {
		GenericServiceResponse result;
		try {
			result = getServicesImpl().seguimientoSiniestros(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
			return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	//30/08/17 ----------------------------------------------------------->
	
}