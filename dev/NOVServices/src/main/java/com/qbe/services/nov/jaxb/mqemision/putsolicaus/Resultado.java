package com.qbe.services.nov.jaxb.mqemision.putsolicaus;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

/**
 * 
        <RESULTADO>
            <SOLICITUDGRABADA>SI</SOLICITUDGRABADA>
            <SOLICITUDOK>SI</SOLICITUDOK>
            <MENSAJE>PROPUESTA EMITIDA</MENSAJE>
            <RAMOPCOD>AUS1</RAMOPCOD>
            <POLIZANN>00</POLIZANN>
            <POLIZSEC>000001</POLIZSEC>
            <CERTIPOL>9000</CERTIPOL>
            <CERTIANN>2017</CERTIANN>
            <CERTISEC>973574</CERTISEC>
            <SUPLENUM>0000</SUPLENUM>
            <TEXTOS>
                <TEXTO/>
            </TEXTOS>
        </RESULTADO>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Resultado {
	
    @XmlElement(name="SOLICITUDGRABADA")	
	public String solicitudgrabada;
	
    @XmlElement(name="SOLICITUDOK")	
	public String solicitudok;
	
    @XmlElement(name="MENSAJE")	
	public String mensaje;
	
    @XmlElement(name="RAMOPCOD")	
	public String ramopcod;
	
    @XmlElement(name="POLIZANN")	
	public String polizann;
	
    @XmlElement(name="POLIZSEC")	
	public String polizsec;
	
    @XmlElement(name="CERTIPOL")	
	public String certipol;
	
    @XmlElement(name="CERTIANN")	
	public String certiann;
	
    @XmlElement(name="CERTISEC")	
	public String certisec;
	
    @XmlElement(name="SUPLENUM")	
	public String suplenum;
	
    @XmlElementWrapper(name="TEXTOS")
    @XmlElement(name="TEXTO")	
	public List<Texto> textos;
    
}
