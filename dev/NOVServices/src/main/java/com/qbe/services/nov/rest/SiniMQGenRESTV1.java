package com.qbe.services.nov.rest;


import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qbe.services.exutils.ExceptionUtils;
import com.qbe.services.nov.AisMqService;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.impl.SiniMQGenericoServicesImpl;

import io.swagger.annotations.Api;


@Path("/v1/SiniMQGenAPI")
@Api(value="SiniMQGen")
public class SiniMQGenRESTV1 extends AisMqService {
	
	protected Logger logger = Logger.getLogger(getClass().getName());

	public SiniMQGenRESTV1() {
		
	}

	protected SiniMQGenericoServicesImpl servicesImpl;
	
	public SiniMQGenericoServicesImpl getServicesImpl() {
		if (servicesImpl == null ) {
			this.servicesImpl = (SiniMQGenericoServicesImpl)getContext().getBean("SiniMQGenServices");
		}
		return servicesImpl;
	}
	
	@POST
	@Path("/getDatosCliente")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})
	public Response getDatosCliente(String request)  {
		GenericServiceResponse result;
		try {
			result = getServicesImpl().getDatosCliente(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}



	@GET
	@Path("/getDatosClienteRiesgo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getDatosClienteRiesgo(
			@QueryParam("usuarcod") String usuarcod,
			@QueryParam("ramopcod") String ramopcod,
			@QueryParam("polizann") String polizann,
			@QueryParam("polizsec") String polizsec,
			@QueryParam("certipol") String certipol,
			@QueryParam("certiann") String certiann,
			@QueryParam("certisec") String certisec,
			@QueryParam("suplenum") String suplenum,
			@QueryParam("fecstro") String fecstro)  {
		GenericServiceResponse result;
		try {
			if(usuarcod == null) {
				usuarcod = "";
			}
			result = getServicesImpl().getDatosClienteRiesgo(usuarcod, ramopcod,polizann,polizsec,certipol,certiann,certisec,suplenum,fecstro);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/getListadoClientes")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getListadoClientes(String request)  {
		
		
		
		GenericServiceResponse result;
		try {
			
			result = getServicesImpl().getListadoClientes(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/putForm1AIS")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response putForm1AIS(
			String request)  {
		
		GenericServiceResponse result;
		try {
			
			result = getServicesImpl().putForm1AIS(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/putForm2AIS")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response putForm2AIS(
			String request)  {
		
		GenericServiceResponse result;
		try {
			
			result = getServicesImpl().putForm2AIS(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/putForm3AIS")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response putForm3AIS(
			String request)  {
		
		GenericServiceResponse result;
		try {
			
			result = getServicesImpl().putForm3AIS(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/putForm5AIS")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response putForm5AIS(
			String request)  {
		
		GenericServiceResponse result;
		try {
			
			result = getServicesImpl().putForm5AIS(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/putForm6AIS")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response putForm6AIS(
			String request)  {
		
		GenericServiceResponse result;
		try {
			
			result = getServicesImpl().putForm6AIS(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/getComboRamo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getComboRamo(String request)  {
		
		GenericServiceResponse result;
		try {
			
			result = getServicesImpl().getComboRamo(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/getComboDOCUMTIP")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getComboDOCUMTIP(String request)  {
		GenericServiceResponse result;
		try {			
			result = getServicesImpl().getComboDOCUMTIP(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	
	@POST
	@Path("/getComboCOMPANIA")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getComboCOMPANIA(String request)  {
		GenericServiceResponse result;
		try {			
			result = getServicesImpl().getComboCOMPANIA(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	
	@POST
	@Path("/getComboJURISCOD")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getComboJURISCOD(String request)  {
		GenericServiceResponse result;
		try {			
			result = getServicesImpl().getComboJURISCOD(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/getComboROBOSLUG")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getComboROBOSLUG(String request)  {
		GenericServiceResponse result;
		try {			
			result = getServicesImpl().getComboROBOSLUG(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/getComboAUUSOCOD")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML}) 
	public Response getComboAUUSOCOD(String request)  {
		GenericServiceResponse result;
		try {			
			result = getServicesImpl().getComboAUUSOCOD(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	
	@POST
	@Path("/getComboAUTIPCOD")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML}) 
	public Response getComboAUTIPCOD(String request)  {
		GenericServiceResponse result;
		try {			
			result = getServicesImpl().getComboAUTIPCOD(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	
	@POST
	@Path("/getComboCENSICOD")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML}) 
	public Response getComboCENSICOD(String request)  {
		GenericServiceResponse result;
		try {			
			result = getServicesImpl().getComboCENSICOD(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	

	@POST
	@Path("/getComboAUCOLCOD")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML}) 
	public Response getComboAUCOLCOD(String request)  {
		GenericServiceResponse result;
		try {			
			result = getServicesImpl().getComboAUCOLCOD(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	
	@POST
	@Path("/getComboAUMARCOD")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML}) 
	public Response getComboAUMARCOD(String request)  {
		GenericServiceResponse result;
		try {			
			result = getServicesImpl().getComboAUMARCOD(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	
	@POST
	@Path("/getComboAUMODCOD")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML}) 
	public Response getComboAUMODCOD(String request)  {
		GenericServiceResponse result;
		try {			
			result = getServicesImpl().getComboAUMODCOD(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 */
	@POST
	@Path("/getProveedores")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML}) 
	public Response getProveedores(String request)  {
		GenericServiceResponse result;
		try {			
			result = getServicesImpl().getProveedores(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	
	@GET
	@Path("/version")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response getVersion() throws Exception {
		try {
			return Response
					.status(Response.Status.OK)
					.header("VERSION", "1")
					.build();
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(Response.Status.PRECONDITION_FAILED)
					.entity(e.getMessage()).build();
		}
	}


	@POST
	@Path("/getListadoRiesgos")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})
	public Response getListadoRiesgos(String request)  {
		GenericServiceResponse result;
		try {
			result = getServicesImpl().getListadoRiesgos(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
			return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}

	@POST
	@Path("/getDesbloqCLIENTE")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})
	public Response getDesbloqCliente(String request)  {
		GenericServiceResponse result;
		try {
			result = getServicesImpl().getDesbloqCLIENTE(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
			return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	/**
	 * @param request
	 * @return
	 */
	@POST
	@Path("/putForm7AIS")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response putForm7AIS(
			String request)  {
		
		GenericServiceResponse result;
		try {
			
			result = getServicesImpl().putForm7AIS(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}


	
	
	
}
