package com.qbe.services.nov.rest;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qbe.services.nov.AisMqService;
import com.qbe.services.nov.impl.OVMQUsuarioServicesImpl;
import com.qbe.services.nov.jaxb.mqusuario.rolesadministrativa.RolesAdministrativa;
import com.qbe.services.nov.jaxb.mqusuario.rolesadministrativa.RolesAdministrativaResponse;

import io.swagger.annotations.Api;

/**
 * 
 * @author ramiro
 *
 */
@Path("/v1/OVMQUsuarioAPI")
@Api(value="OVMQUsuario")
public class OVMQUsuarioRESTV1 extends AisMqService {

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	protected OVMQUsuarioServicesImpl servicesImpl;
	
	public OVMQUsuarioRESTV1() {
	}
	
	public OVMQUsuarioServicesImpl getServicesImpl() {
		if (servicesImpl == null ) {
			this.servicesImpl = (OVMQUsuarioServicesImpl)getContext().getBean("OVMQUsuarioServices");
		}
		return servicesImpl;
	}
	
	@POST
	@Path("/rolesAdministrativa")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response rolesAdministrativaJson(
			@QueryParam("usuario") String usuario) throws Exception {
		
		RolesAdministrativaResponse result = getServicesImpl().rolesAdministrativa(usuario);
        String json = NovRestServicesUtil.marshalAsJson(RolesAdministrativa.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(json).build();
	}

	@POST
	@Path("/rolesAdministrativa")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response rolesAdministrativaXml(
			@QueryParam("usuario") String usuario) throws Exception {
		
		RolesAdministrativaResponse result = getServicesImpl().rolesAdministrativa(usuario);
        String xml = NovRestServicesUtil.marshalAsXml(RolesAdministrativa.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}

}