package com.qbe.services.nov.jaxb.mqemision.putsolicaus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
			<ASEGURADO>
				<DOCUMDAT>20800900</DOCUMDAT>
				<NOMBREAS>DIMARTINO,TATA</NOMBREAS>
			</ASEGURADO>        
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Asegurado {
	
    @XmlElement(name="DOCUMDAT")	
	public String documdat;
	
    @XmlElement(name="NOMBREAS")	
	public String nombreas;
	
   
}
