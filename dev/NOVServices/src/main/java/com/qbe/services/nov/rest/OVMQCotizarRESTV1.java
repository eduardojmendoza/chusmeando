package com.qbe.services.nov.rest;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qbe.services.nov.AisMqService;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.impl.OVMQCotizarServicesImpl;
import com.qbe.services.nov.jaxb.mqcotizar.getcotiaus.GetCotiAUS;
import com.qbe.services.nov.jaxb.mqcotizar.getcotiaus.GetCotiAUSRequest;
import com.qbe.services.nov.jaxb.mqcotizar.getcotiaus.GetCotiAUSResponse;
import com.qbe.services.nov.jaxb.mqcotizar.validanumerodocumento.ValidaNumeroDocumento;
import com.qbe.services.nov.jaxb.mqcotizar.validanumerodocumento.ValidaNumeroDocumentoResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author cesar
 * @author ramiro
 *
 */
@Path("/v1/OVMQCotizarAPI")
@Api(value="OVMQCotizar")
public class OVMQCotizarRESTV1 extends AisMqService {

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	protected OVMQCotizarServicesImpl servicesImpl;
	
	public OVMQCotizarRESTV1() {
	}
	
	public OVMQCotizarServicesImpl getServicesImpl() {
		if (servicesImpl == null ) {
			this.servicesImpl = (OVMQCotizarServicesImpl)getContext().getBean("OVMQCotizarServices");
		}
		return servicesImpl;
	}
	

	@POST
	@Path("/getCotiAUS")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	//@ApiOperation(value = "getCotiAUSXml", notes = "...", response = GetCotiAUSResponse.class)
	public Response getCotiAUSXml(String request) throws Exception {
		GenericServiceResponse result = getServicesImpl().getCotiAUS(request);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}

	@GET
	@Path("/validaNumeroDocumento")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})  
	@ApiOperation(value = "validaNumeroDocumentoJson", notes = "...", response = ValidaNumeroDocumentoResponse.class)
	public Response validaNumeroDocumentoJson(
			@DefaultValue("") @QueryParam("usuario") String usuario,
			@DefaultValue("") @QueryParam("tipo") String tipo,
			@DefaultValue("") @QueryParam("numero") String numero) throws Exception {

		ValidaNumeroDocumentoResponse result = getServicesImpl().validaNumeroDocumento(usuario, tipo, numero);
        String json = NovRestServicesUtil.marshalAsJson(ValidaNumeroDocumento.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(json).build();
	}

	@GET
	@Path("/validaNumeroDocumento")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	@ApiOperation(value = "validaNumeroDocumentoXml", notes = "...", response = ValidaNumeroDocumentoResponse.class)
	public Response validaNumeroDocumentoXml(@QueryParam("usuario") String usuario,
			@QueryParam("tipo") String tipo,
			@QueryParam("numero") String numero) throws Exception {
		
		ValidaNumeroDocumentoResponse result = getServicesImpl().validaNumeroDocumento(usuario, tipo, numero);
        String xml = NovRestServicesUtil.marshalAsXml(ValidaNumeroDocumento.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(xml).build();
	}

	
	@GET
	@Path("/getKilometraje")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	//@ApiOperation(value = "validaNumeroDocumentoXml", notes = "...", response = ValidaNumeroDocumentoResponse.class)
	public Response getKilometraje(
			@QueryParam("campacod") String campacod,
			@QueryParam("agentcod") String agentcod,
			@QueryParam("agentcla") String agentcla,
			@QueryParam("codizona") String codizona,
			@QueryParam("fecinici") String fecinici,
			@QueryParam("fecinvig") String fecinvig) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().getKilometraje(campacod, agentcod, agentcla, codizona, fecinici, fecinvig);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	@POST
	@Path("/getCentrosIns")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	//@ApiOperation(value = "validaNumeroDocumentoXml", notes = "...", response = ValidaNumeroDocumentoResponse.class)
	public Response getCentrosIns(String request) throws Exception {
		GenericServiceResponse result = getServicesImpl().getCentrosIns(request);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	@POST
	@Path("/tipoInspeccion")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	//@ApiOperation(value = "validaNumeroDocumentoXml", notes = "...", response = ValidaNumeroDocumentoResponse.class)
	public Response tipoInspeccion(String request) throws Exception {
		GenericServiceResponse result = getServicesImpl().tipoInspeccion(request);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	@POST
	@Path("/validaCuentas")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response validaCuentas(String request) throws Exception {
		GenericServiceResponse result = getServicesImpl().validaCuentas(request);
		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
}