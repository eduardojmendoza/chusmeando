package com.qbe.services.nov.jaxb.mqcotizar.getcotiaus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
		<HIJO>
			<NACIMHIJO>01011994</NACIMHIJO>
			<EDADHIJO>21</EDADHIJO>
			<SEXOHIJO>F</SEXOHIJO>
			<ESTADOHIJO>S</ESTADOHIJO>
			<INCLUIDO>1</INCLUIDO>
		</HIJO>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Hijo {
	
    @XmlElement(name="NACIMHIJO")	
	public String nacimhijo;
	
    @XmlElement(name="EDADHIJO")	
	public String edadhijo;
	
    @XmlElement(name="SEXOHIJO")	
	public String sexohijo;
	
    @XmlElement(name="ESTADOHIJO")	
	public String estadohijo;
	
    @XmlElement(name="INCLUIDO")	
	public String incluido;

    
}
