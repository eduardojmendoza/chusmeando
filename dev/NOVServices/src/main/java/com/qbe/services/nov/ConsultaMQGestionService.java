package com.qbe.services.nov;

import com.qbe.services.nov.impl.ConsultaMQGestionServicesImpl;

public abstract class ConsultaMQGestionService extends AisMqService {

	protected ConsultaMQGestionServicesImpl servicesImpl;

	public ConsultaMQGestionService() {
		super();
	}

	public ConsultaMQGestionServicesImpl getServicesImpl() {
		if (servicesImpl == null ) {
			this.servicesImpl = (ConsultaMQGestionServicesImpl)getContext().getBean("ConsultaMQGestionServices");
		}
		return servicesImpl;
	}

}