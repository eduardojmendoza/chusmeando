package com.qbe.services.nov.jaxb.mqmensaje.productosporproductor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**

				<AREA Nombre="RAMOPCOD" Visible="SI" TipoDato="TEXTO"/>
				<AREA Nombre="RAMOPDES" Visible="SI" TipoDato="TEXTO"/>
				<AREA Nombre="RAMOPDAB" Visible="SI" TipoDato="TEXTO"/>
				<AREA Nombre="MONENCOD" Visible="SI" TipoDato="TEXTO"/>
 * 
 * 
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Producto {

    @XmlElement(name="RAMOPCOD")	
	public String ramopcod;
	
    @XmlElement(name="RAMOPDES")	
	public String ramopdes;
	
    @XmlElement(name="RAMOPDAB")	
	public String ramopdab;
	
    @XmlElement(name="MONENCOD")	
	public String monencod;
	
}
