package com.qbe.services.nov.jaxb.mqmensaje.legajoempleadolba;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;

/**
 * 
 * 
<Response>
        <Estado resultado="true" mensaje=""/>
        <CAMPOS>
            <CLIENAP1><![CDATA[CHIESA]]></CLIENAP1>
            <CLIENAP2><![CDATA[]]></CLIENAP2>
            <CLIENNOM><![CDATA[SABRINA GABRIELA]]></CLIENNOM>
            <MSGERROR><![CDATA[]]></MSGERROR>
        </CAMPOS>
    </Response>    
 * 
 * @author ramiro
 *
 */
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class LegajoEmpleadoLBA implements ResponseWithEstado {

	@XmlElement( name = "Estado", required = true)
	public Estado estado;
	
	@XmlElement( name = "CAMPOS", required = true)
	private Campos campos;

	@Override
	public Estado getEstado() {
		return estado;
	}
}
