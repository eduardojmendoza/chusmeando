package com.qbe.services.nov.jaxb.mqmensaje.legajoempleadolba;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * 
 * 
        <CAMPOS>
            <CLIENAP1><![CDATA[CHIESA]]></CLIENAP1>
            <CLIENAP2><![CDATA[]]></CLIENAP2>
            <CLIENNOM><![CDATA[SABRINA GABRIELA]]></CLIENNOM>
            <MSGERROR><![CDATA[]]></MSGERROR>
        </CAMPOS> * 
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Campos {
	
    @XmlElement(name="CLIENAP1")	
	public String clienap1;
	
    @XmlElement(name="CLIENAP2")	
	public String clienap2;
	
    @XmlElement(name="CLIENNOM")	
	public String cliennom;
	
    @XmlElement(name="MSGERROR")	
	public String msgerror;
	

}
