package com.qbe.services.nov;

/**
 * 
 * Respuesta para los mensajes que incluye el código de error y el XML/HTML de la respuesta como String
 * 
 * @author ramiro
 *
 */
public class GenericServiceResponse {
	private int code;
	
	private String response;

	public GenericServiceResponse() {
	}

	public GenericServiceResponse(int code, String response) {
		this.response = response;
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int resultCode) {
		this.code = resultCode;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
