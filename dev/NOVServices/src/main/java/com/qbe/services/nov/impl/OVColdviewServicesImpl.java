package com.qbe.services.nov.impl;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.coldview.estructurado.ColdviewTester;
import com.qbe.services.coldview.estructurado.GetXMLPDF;
import com.qbe.services.coldview.estructurado.GetXMLPage;
import com.qbe.services.coldview.estructurado.SendPDFMail;
import com.qbe.services.coldview.estructurado.SendTXTMail;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.vbcompat.string.StringHolder;

public class OVColdviewServicesImpl {

	protected Logger logger = Logger.getLogger(getClass().getName());

	public GenericServiceResponse testConnect(String request) throws Exception {
		ColdviewTester comp = new ColdviewTester();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = comp.IAction_Execute(request, sh, "");
		logger.log(Level.FINE, "Resultado de ejecución de testConnect: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse getXMLPDF(String request) throws Exception {
		GetXMLPDF comp = new GetXMLPDF();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = comp.IAction_Execute(request, sh, "");
		logger.log(Level.FINE, "Resultado de ejecución de getXMLPDF: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}


	public GenericServiceResponse getXMLPage(String request) throws Exception {
		GetXMLPage comp = new GetXMLPage();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = comp.IAction_Execute(request, sh, "");
		logger.log(Level.FINE, "Resultado de ejecución de getXMLPage: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	

	public GenericServiceResponse sendTXTMail(String request) throws Exception {
		SendTXTMail comp = new SendTXTMail();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = comp.IAction_Execute(request, sh, "");
		logger.log(Level.FINE, "Resultado de ejecución de sendPDFMail: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		}


	public GenericServiceResponse sendPDFMail(String request) throws Exception {
		SendPDFMail comp = new SendPDFMail();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = comp.IAction_Execute(request, sh, "");
		logger.log(Level.FINE, "Resultado de ejecución de sendPDFMail: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
		}
}
