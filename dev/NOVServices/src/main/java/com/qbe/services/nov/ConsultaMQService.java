package com.qbe.services.nov;

import com.qbe.services.nov.impl.ConsultaMQServicesImpl;

public abstract class ConsultaMQService extends AisMqService {

	protected ConsultaMQServicesImpl servicesImpl;

	public ConsultaMQService() {
		super();
	}

	public ConsultaMQServicesImpl getServicesImpl() {
		if (servicesImpl == null ) {
			this.servicesImpl = (ConsultaMQServicesImpl)getContext().getBean("ConsultaMQServices");
		}
		return servicesImpl;
	}
	
}