package com.qbe.services.nov.impl;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.jaxb.mqcotizar.getcotiaus.GetCotiAUS;
import com.qbe.services.nov.jaxb.mqcotizar.getcotiaus.GetCotiAUSResponse;
import com.qbe.services.nov.jaxb.mqcotizar.validanumerodocumento.ValidaNumeroDocumento;
import com.qbe.services.nov.jaxb.mqcotizar.validanumerodocumento.ValidaNumeroDocumentoResponse;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVGetCotiAUS;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVValidaCuentas;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVValidaNumDoc;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVTipoInspeccion;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVGetCentrosIns;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVGetKilometraje;

/**
 * Implementación del servicio
 * 
 * @author ramiro
 *
 */
public class OVMQCotizarServicesImpl {

	private static final String EMPTY_STRING = "";

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	public OVMQCotizarServicesImpl() {
	}

	private lbaw_OVGetCotiAUS getlbaw_OVGetCotiAUS() {
		lbaw_OVGetCotiAUS ovgca = new lbaw_OVGetCotiAUS();
		return ovgca;
	}

	public GenericServiceResponse getCotiAUS(String reqXML) throws Exception {
		lbaw_OVGetCotiAUS ovgca = getlbaw_OVGetCotiAUS();
		StringHolder sh = new StringHolder();
		int resultCode = ovgca.IAction_Execute(reqXML, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	public ValidaNumeroDocumentoResponse validaNumeroDocumento(String usuario, String tipo, String numero) throws Exception {
		lbaw_OVValidaNumDoc vnd = new lbaw_OVValidaNumDoc();
		String reqXml = createValidaNumeroDocumentoRequestString(usuario, tipo, numero);
		StringHolder sh = new StringHolder();
		int resultCode = vnd.IAction_Execute(reqXml, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(ValidaNumeroDocumento.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        ValidaNumeroDocumento result = (ValidaNumeroDocumento) unmarshaller.unmarshal(new StringReader(sh.getValue()));
        
        //Retorno un Wrapper que incluye el resultCode
        ValidaNumeroDocumentoResponse response = new ValidaNumeroDocumentoResponse();
        response.resultCode = resultCode;
        response.response = result;
        
		return response;
	}

	
	private String createValidaNumeroDocumentoRequestString(String usuario, String tipo, String numero) {
		return "<Request>"
				+ "<USUARIO>" + usuario + "</USUARIO>"
				+ "<DOCUMTIP>" + tipo + "</DOCUMTIP>"
				+ "<DOCUMNRO>" + numero + "</DOCUMNRO>"
				+ "</Request>";
	}
			

	public GenericServiceResponse getKilometraje(String campacod,String agentcod,String agentcla,String codizona,String fecinici,String fecinvig) throws Exception {
		lbaw_OVGetKilometraje vnd = new lbaw_OVGetKilometraje();
		String reqXml = createGetKilometrajeRequestString(campacod, agentcod, agentcla, codizona, fecinici, fecinvig);
		StringHolder sh = new StringHolder();
		int resultCode = vnd.IAction_Execute(reqXml, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	
	private String createGetKilometrajeRequestString(String campacod,String agentcod,String agentcla,String codizona,String fecinici,String fecinvig) {
		return "    <Request>"
				+"        <CAMPACOD>"+campacod+"</CAMPACOD>"
				+"        <AGENTCOD>"+agentcod+"</AGENTCOD>"
				+"        <AGENTCLA>"+agentcla+"</AGENTCLA>"
				+"        <CODIZONA>"+codizona+"</CODIZONA>"
				+"        <FECINICI>"+fecinici+"</FECINICI>"
				+"        <FECINVIG>"+fecinvig+"</FECINVIG>"
				+"    </Request>";

	}
	
	public GenericServiceResponse tipoInspeccion(String request) throws Exception {
		lbaw_OVTipoInspeccion vnd = new lbaw_OVTipoInspeccion();
		StringHolder sh = new StringHolder();
		int resultCode = vnd.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}


	public GenericServiceResponse getCentrosIns(String request) throws Exception {
		lbaw_OVGetCentrosIns vnd = new lbaw_OVGetCentrosIns();
		StringHolder sh = new StringHolder();
		int resultCode = vnd.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse validaCuentas(String request) throws Exception {
		lbaw_OVValidaCuentas vnd = new lbaw_OVValidaCuentas();
		StringHolder sh = new StringHolder();
		int resultCode = vnd.IAction_Execute(request, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
}
