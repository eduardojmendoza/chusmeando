package com.qbe.services.nov.jaxb.mqcotizar.validanumerodocumento;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.nov.Estado;
import com.qbe.services.nov.ResponseWithEstado;

/**
*
* Respuesta recibida del MQMensaje
* 
<Response>
        <Estado resultado="true" mensaje=""/>

* 
* 
* @author ramiro
*
*/
@XmlRootElement( name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidaNumeroDocumento implements ResponseWithEstado {
	@XmlElement( name = "Estado", required = true)
	public Estado estado;

	public ValidaNumeroDocumento() {
	}

	@Override
	public Estado getEstado() {
		return estado;
	}

}
