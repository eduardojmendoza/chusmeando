package com.qbe.services.nov.jaxb.mqmensaje.productoreshabilitadosparacotizar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * 
        <option value="4164|PR|ASESORES SEGUROENLIN EA S.A." agentecodigo="4164">4164|PR|ASESORES SEGUROENLIN EA S.A.</option>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Option {
	
    @XmlAttribute(name="value")	
	public String value;
	
    @XmlAttribute(name="agentecodigo")	
	public String agentecodigo;

    @XmlValue
	public String texto;
    
}
