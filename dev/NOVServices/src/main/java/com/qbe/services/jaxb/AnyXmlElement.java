package com.qbe.services.jaxb;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


/**
 * <p>Java class for AnyXmlElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AnyXmlElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;any processContents='lax' maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Deprecated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnyXmlElement", propOrder = {
    "any"
})
public class AnyXmlElement {

	private static final String MENSAJE_ATTRIBUTE_NAME = "mensaje";

	private static final String RESULTADO_ATTRIBUTE_NAME = "resultado";

	private static final String ESTADO_TAG_NAME = "Estado";

	private static final String REQUEST_TAG_NAME = "Request";

	protected static Logger logger = Logger.getLogger(AnyXmlElement.class.getName());

    @XmlAnyElement(lax = true)
    @XmlMixed
    protected List<Object> any;

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Element }
     * {@link Object }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

	/**
	 * Formatea this como el String que esperan las VBObjectClass.
	 * Este método lo wrapea con <Request></Request> y retorna String de todo el doc
	 * 
	 * Ejemplo, si this es <def>1</def><usuarcod>23</usuarcod>, este método retorna
	 * <Request><def>1</def><usuarcod>23</usuarcod></Request>
	 * 
	 * Este método usa JAXB para hacer el marshal, por lo que respeta la estructura tal cual incluso CDATA Sections.
	 * Había una versión anterior que usaaba DOM y no respetaba el CDATA
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */

	public String formatAsRequestString() throws Exception {
		return wrapWithTagName(REQUEST_TAG_NAME);
	}

	//FIXME separar en dos métodos, uno marshal() publico y este.
	public String wrapWithTagName(String tagName) throws Exception {

		StringWriter writer = new StringWriter();
		try {
			JAXBContext context = JAXBContext.newInstance(AnyXmlElement.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FRAGMENT, true);
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			m.marshal(this, writer);
			String xml = writer.getBuffer().toString().trim();
			StringBuffer sb = new StringBuffer();
			sb.append("<"+tagName+">");
			sb.append(xml);
			sb.append("</"+tagName+">");
			return sb.toString();
		} catch (PropertyException e) {
			String msg = "Exception al hacer el marshal del nodo";
			logger.log(Level.SEVERE, msg, e);
			throw new Exception(msg, e);
		} catch (JAXBException e) {
			String msg = "Exception al hacer el marshal del nodo";
			logger.log(Level.SEVERE, msg, e);
			throw new Exception(msg, e);
		}
	}
	/**
	 * Crea un AnyXmlElement cuyos elementos son los children del root node del xmlString.
	 * 
	 * @param xmlString En formato <*>....</*>, por ejemplo <Response>, este método retorna un AnyXmlElement con los hijos del <*>
	 * @return
	 */
	public static AnyXmlElement newWithRootChildrenNodes(String xmlString) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			docFactory.setCoalescing(false);
			docFactory.setIgnoringComments(true);
			docFactory.setIgnoringElementContentWhitespace(true);
			docFactory.setValidating(true);
			
			Document doc = docFactory.newDocumentBuilder().parse(new InputSource(new StringReader(xmlString)));
			Element rootElement = doc.getDocumentElement();
			AnyXmlElement r = new AnyXmlElement();
			
			//Saco los whitespaces
			//Crazy method
			//http://stackoverflow.com/a/20089869
			XPathFactory xpathFactory = XPathFactory.newInstance();
			// XPath to find empty text nodes.
			XPathExpression xpathExp = xpathFactory.newXPath().compile("//text()[normalize-space(.) = '']");  
			NodeList emptyTextNodes = (NodeList) xpathExp.evaluate(doc, XPathConstants.NODESET);
			// Remove each empty text node from document.
			for (int i = 0; i < emptyTextNodes.getLength(); i++) {
			  Node emptyTextNode = emptyTextNodes.item(i);
			  emptyTextNode.getParentNode().removeChild(emptyTextNode);
			}
			
			//Método viejo, que encima no es recursivo. No debería haber ninguno text node
			NodeList nl = rootElement.getChildNodes();
			for (int i = 0; i < nl.getLength(); i++) {
				Node node = nl.item(i);
				if (( node.getNodeType() == Node.ELEMENT_NODE) || ( node.getNodeType() == Node.CDATA_SECTION_NODE)) {
					r.getAny().add(node);
				} else if ( node.getNodeType() == Node.TEXT_NODE)  {
					//FIXME si va para json usar un AnyXmlElement que tenga esta logica, sino dejar el que maneja TEXT_NODE
					String msg = "Porque no se puede mapear a JSON, NO estoy agregando en el AnyXML el node: " + node + " de tipo: " + node.getNodeType();
					logger.log(Level.WARNING,msg);
				} else {
					String msg = "NO estoy agregando en el AnyXML el node: " + node + " de tipo: " + node.getNodeType();
					logger.log(Level.WARNING,msg);
				}
			}
			
			return r;
		} catch (Exception e) {
			String msg = "Exception al deserializar el xmlString de respuesta del componente: [" + xmlString + "] msg: [" + e.getMessage() + "]";
			logger.log(Level.SEVERE,msg, e);
			return newForEstadoWithMessage(msg);
		}
	}

	/**
	 * Crea un AnyXmlElement con un único Element <Estado resultado="false" mensaje="$1"/>,
	 * con $1=msg
	 *  
	 * @param msg
	 * @return
	 */
	public static AnyXmlElement newForEstadoWithMessage(String msg) {
		DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = f.newDocumentBuilder();
			Document doc = builder.newDocument();
			Element estado = (Element)doc.createElement(ESTADO_TAG_NAME);
			estado.setAttribute(RESULTADO_ATTRIBUTE_NAME, "false");
			estado.setAttribute(MENSAJE_ATTRIBUTE_NAME, msg);
			doc.appendChild(estado);
			AnyXmlElement any = new AnyXmlElement();
			any.getAny().add(estado);
			return any;
		} catch ( ParserConfigurationException e) {
			//No puedo ni crear la respuesta...
			logger.log(Level.SEVERE, "No puedo generar el AnyXML para una respuesta de error con msg: " + msg);
			return new AnyXmlElement();
		}
	}
	
	/**
	 * Crea un AnyXmlElement con un único Element <Estado resultado="$valor" mensaje="$msg"/>,
	 * con $1=msg
	 *  
	 * @param msg
	 * @return
	 */
	public static AnyXmlElement newForEstadoWithMessage(String valor, String msg) {
		DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = f.newDocumentBuilder();
			Document doc = builder.newDocument();
			Element estado = (Element)doc.createElement(ESTADO_TAG_NAME);
			estado.setAttribute(RESULTADO_ATTRIBUTE_NAME, valor);
			estado.setAttribute(MENSAJE_ATTRIBUTE_NAME, msg);
			doc.appendChild(estado);
			AnyXmlElement any = new AnyXmlElement();
			any.getAny().add(estado);
			return any;
		} catch ( ParserConfigurationException e) {
			//No puedo ni crear la respuesta...
			logger.log(Level.SEVERE, "No puedo generar el AnyXML para una respuesta de error con msg: " + msg);
			return new AnyXmlElement();
		}
	}
	
	/**
	 * Crea un AnyXmlElement con un único Element <Estado resultado="$resultado" mensaje="$message">$text</Estado>,
	 * text dentro de un CDATA
	 *  
	 * @param resultado
	 * @param message
	 * @param text
	 * 
	 * @return
	 */
	public static AnyXmlElement newForEstado(String resultado, String message, String text) {
		DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = f.newDocumentBuilder();
			Document doc = builder.newDocument();
			Element estado = (Element)doc.createElement(ESTADO_TAG_NAME);
			estado.setAttribute(RESULTADO_ATTRIBUTE_NAME, "false");
			estado.setAttribute(MENSAJE_ATTRIBUTE_NAME, message);
			CDATASection cdata = doc.createCDATASection(text);
			estado.appendChild(cdata);
			doc.appendChild(estado);
			AnyXmlElement any = new AnyXmlElement();
			any.getAny().add(estado);
			return any;
		} catch ( ParserConfigurationException e) {
			//No puedo ni crear la respuesta...
			logger.log(Level.SEVERE, String.format("No puedo generar el AnyXML para una respuesta de error con mensaje: %s y texto %s ", message, text));
			return new AnyXmlElement();
		}
	}
	
}
