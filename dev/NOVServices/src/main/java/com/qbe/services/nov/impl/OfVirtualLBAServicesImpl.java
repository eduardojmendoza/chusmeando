package com.qbe.services.nov.impl;

import java.util.logging.Logger;

import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.ofVirtualLBA.impl.lbaw_GetTiposIVA;
import com.qbe.services.ofvirtuallba.impl.lbaw_GetNroCot;
import com.qbe.vbcompat.string.StringHolder;

/**
 * Implementación del servicio
 * 
 * @author ramiro
 *
 */
public class OfVirtualLBAServicesImpl {

	private static final String EMPTY_STRING = "";

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	public OfVirtualLBAServicesImpl() {
	}


	public GenericServiceResponse getTiposIVA() throws Exception {
		lbaw_GetTiposIVA gti = new lbaw_GetTiposIVA();
		String reqXml = createGetTiposIVARequestString();
		StringHolder sh = new StringHolder();
		int resultCode = gti.IAction_Execute(reqXml, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	
	private String createGetTiposIVARequestString() {
		return "<Request>"
				+"</Request>";

	}


	public GenericServiceResponse getNroCot() throws Exception {
		lbaw_GetNroCot gnc = new lbaw_GetNroCot();
		String reqXml = createGetNroCotRequestString();
		StringHolder sh = new StringHolder();
		int resultCode = gnc.IAction_Execute(reqXml, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	
	private String createGetNroCotRequestString() {
		return "<Request>"
				+"</Request>";

	}

	
	
	
	
}
