package com.qbe.services.nov.jaxb.mqcotizar.getcotiaus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
                <CON_HIJOS>
                    <PRIMA>1442,39</PRIMA>
                    <RECARGOS>0,00</RECARGOS>
                    <IVAIMPOR>302,90</IVAIMPOR>
                    <IVAIMPOA>0,00</IVAIMPOA>
                    <IVARETEN>0,00</IVARETEN>
                    <DEREMI>0,00</DEREMI>
                    <SELLADO>0,00</SELLADO>
                    <INGBRU>0,00</INGBRU>
                    <IMPUES>31,72</IMPUES>
                    <PRECIO>1777,01</PRECIO>
                </CON_HIJOS>
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CostoPlan {
	
    @XmlElement(name="PRIMA")	
	public String prima;
	
    @XmlElement(name="RECARGOS")	
	public String recargos;
	
    @XmlElement(name="IVAIMPOR")	
	public String ivaimpor;
	
    @XmlElement(name="IVAIMPOA")	
	public String ivaimpoa;
	
    @XmlElement(name="IVARETEN")	
	public String ivareten;

    @XmlElement(name="DEREMI")	
	public String deremi;
    
    @XmlElement(name="SELLADO")	
	public String sellado;
    
    @XmlElement(name="INGBRU")	
	public String ingbru;
    
    @XmlElement(name="IMPUES")	
	public String impues;
    
    @XmlElement(name="PRECIO")	
	public String precio;
        
}
