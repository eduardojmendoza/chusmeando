package com.qbe.services.nov.rest.swagger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.util.Json;
import io.swagger.util.Yaml;

public class Bootstrap extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        //@see https://github.com/swagger-api/swagger-core/issues/960
        //Puesto para que levante las annotations de JAXB cuando arma el swagger
        Json.mapper().registerModule(new JaxbAnnotationModule());
        Yaml.mapper().registerModule(new JaxbAnnotationModule());
        // OJO también hay que tener getters, no le da bola al @XmlAccessorType(XmlAccessType.FIELD)

		// @see http://stackoverflow.com/questions/27282681/how-to-make-swagger-recognize-xmltransient-e-g-not-ignore-it
        // @see http://stackoverflow.com/a/27360099
        BeanConfig beanConfig = new BeanConfig();
        try {
			if ( "dev".equals(CurrentProfile.getProfileName())) {
				beanConfig.setHost("localhost:7001");
			} else {
			    beanConfig.setHost("10.1.10.30:8080");        	
			}
		} catch (CurrentProfileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			beanConfig.setHost("localhost:7001");
		}
        beanConfig.setBasePath("/NOVServices/rest");
        beanConfig.setResourcePackage("com.qbe.services.nov.rest");
        beanConfig.setScan(true);
        beanConfig.setPrettyPrint(true);
    }
}