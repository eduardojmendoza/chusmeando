package com.qbe.services.nov.impl;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.qbe.services.nov.jaxb.lbawebmq.getsumaasegurada.GetSumaAsegurada;
import com.qbe.services.nov.jaxb.lbawebmq.getsumaasegurada.GetSumaAseguradaResponse;
import com.qbe.services.nov.jaxb.lbawebmq.getzona.GetZona;
import com.qbe.services.nov.jaxb.lbawebmq.getzona.GetZonaResponse;
import com.qbe.services.nov.jaxb.mqcotizar.getcotiaus.GetCotiAUS;
import com.qbe.services.nov.jaxb.mqcotizar.getcotiaus.GetCotiAUSResponse;
import com.qbe.services.nov.jaxb.mqcotizar.validanumerodocumento.ValidaNumeroDocumento;
import com.qbe.services.nov.jaxb.mqcotizar.validanumerodocumento.ValidaNumeroDocumentoResponse;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVGetCotiAUS;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVValidaNumDoc;
import com.qbe.services.webmq.impl.lbaw_GetSumAseg;
import com.qbe.services.webmq.impl.lbaw_GetZona;
import com.qbe.vbcompat.string.StringHolder;

/**
 * Implementación del servicio
 * 
 * @author ramiro
 *
 */
public class LBAWebMQServicesImpl {

	private static final String EMPTY_STRING = "";

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	public LBAWebMQServicesImpl() {
	}

	private lbaw_OVGetCotiAUS getlbaw_OVGetCotiAUS() {
		lbaw_OVGetCotiAUS ovgca = new lbaw_OVGetCotiAUS();
		return ovgca;
	}

	public GetCotiAUSResponse getCotiAUS(
			String reqXML) throws Exception {
		
		lbaw_OVGetCotiAUS ovgca = getlbaw_OVGetCotiAUS();
		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = ovgca.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución : [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(GetCotiAUS.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        GetCotiAUS result = (GetCotiAUS) unmarshaller.unmarshal(new StringReader(sh.getValue()));
        
        //Retorno un Wrapper que incluye el resultCode
        GetCotiAUSResponse response = new GetCotiAUSResponse();
        response.resultCode = resultCode;
        response.response = result;
        
		return response;
	}

	public ValidaNumeroDocumentoResponse validaNumeroDocumento(String usuario, String tipo, String numero) throws Exception {
		lbaw_OVValidaNumDoc vnd = new lbaw_OVValidaNumDoc();
		String reqXml = createValidaNumeroDocumentoRequestString(usuario, tipo, numero);
		StringHolder sh = new StringHolder();
		int resultCode = vnd.IAction_Execute(reqXml, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(ValidaNumeroDocumento.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        ValidaNumeroDocumento result = (ValidaNumeroDocumento) unmarshaller.unmarshal(new StringReader(sh.getValue()));
        
        //Retorno un Wrapper que incluye el resultCode
        ValidaNumeroDocumentoResponse response = new ValidaNumeroDocumentoResponse();
        response.resultCode = resultCode;
        response.response = result;
        
		return response;
	}

	
	private String createValidaNumeroDocumentoRequestString(String usuario, String tipo, String numero) {
		return "<Request>"
				+ "<USUARIO>" + usuario + "</USUARIO>"
				+ "<DOCUMTIP>" + tipo + "</DOCUMTIP>"
				+ "<DOCUMNRO>" + numero + "</DOCUMNRO>"
				+ "</Request>";
	}

	public GetZonaResponse getZona(String ramopcod, String provicod, String cpacodpo) throws Exception {
		lbaw_GetZona ac = new lbaw_GetZona();
		String reqXml = createGetZomaRequestString(ramopcod, provicod, cpacodpo);
		StringHolder sh = new StringHolder();
		int resultCode = ac.IAction_Execute(reqXml, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(GetZona.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        GetZona result = (GetZona) unmarshaller.unmarshal(new StringReader(sh.getValue()));
        
        //Retorno un Wrapper que incluye el resultCode
        GetZonaResponse response = new GetZonaResponse();
        response.resultCode = resultCode;
        response.response = result;
        
		return response;
	}
			

	private String createGetZomaRequestString(String ramopcod, String provicod, String cpacodpo) {
		return "<Request>"
				+ "<RAMOPCOD>" + ramopcod + "</RAMOPCOD>"
				+ "<PROVICOD>" + provicod + "</PROVICOD>"
				+ "<CPACODPO>" + cpacodpo + "</CPACODPO>"
				+ "</Request>";
	}


	public GetSumaAseguradaResponse getSumaAsegurada(String aumodcod, String auprocod) throws Exception {
		lbaw_GetSumAseg ac = new lbaw_GetSumAseg();
		String reqXml = createGetSumAsegRequestString(aumodcod, auprocod);
		StringHolder sh = new StringHolder();
		int resultCode = ac.IAction_Execute(reqXml, sh, EMPTY_STRING);
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(GetSumaAsegurada.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        GetSumaAsegurada result = (GetSumaAsegurada) unmarshaller.unmarshal(new StringReader(sh.getValue()));
        
        //Retorno un Wrapper que incluye el resultCode
        GetSumaAseguradaResponse response = new GetSumaAseguradaResponse();
        response.resultCode = resultCode;
        response.response = result;
        
		return response;
	}
			

	private String createGetSumAsegRequestString(String aumodcod, String auprocod) {
		return "<Request>"
				+ "<AUMODCOD>" + aumodcod + "</AUMODCOD>"
				+ "<AUPROCOD>" + auprocod + "</AUPROCOD>"
				+ "</Request>";
	}
	
	
}
