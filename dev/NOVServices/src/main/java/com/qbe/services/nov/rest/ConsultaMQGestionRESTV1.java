package com.qbe.services.nov.rest;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qbe.services.exutils.ExceptionUtils;
import com.qbe.services.nov.ConsultaMQGestionService;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.jaxb.mqmensajegestion.perfilusuario.PerfilUsuario;
import com.qbe.services.nov.jaxb.mqmensajegestion.perfilusuario.PerfilUsuarioResponse;

import io.swagger.annotations.Api;

/**
 * 
 * @author ramiro
 * @author cesar
 */
@Path("/v1/ConsultaMQGestionAPI")
@Api(value="ConsultaMQGestion")
public class ConsultaMQGestionRESTV1 extends ConsultaMQGestionService {

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	public ConsultaMQGestionRESTV1() {
	}
	
	@POST
	@Path("/perfilUsuario")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response perfilUsuarioJson(
			@DefaultValue("") @QueryParam("AplicarXSL") String aplicarXSL,
			@DefaultValue("") @QueryParam("Usuario") String usuario) throws Exception {
		
		PerfilUsuarioResponse result = getServicesImpl().perfilUsuario_1000(aplicarXSL, usuario);
        String json = NovRestServicesUtil.marshalAsJson(PerfilUsuario.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(json).build();
	}
	
	@POST
	@Path("/perfilUsuario")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response perfilUsuarioXml(
			@DefaultValue("") @QueryParam("AplicarXSL") String aplicarXSL,
			@DefaultValue("") @QueryParam("Usuario") String usuario) throws Exception {
		
		PerfilUsuarioResponse result = getServicesImpl().perfilUsuario_1000(aplicarXSL, usuario);
        String json = NovRestServicesUtil.marshalAsXml(PerfilUsuario.class, result.response);		
        Response.Status status = NovRestServicesUtil.getResponseStatus(result);
		return Response.status(status).entity(json).build();
	}
			
  	@GET
	@Path("/getActividadOcupacion")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response actividadOcupacionXml(
			@DefaultValue("") @QueryParam("buscar") String buscar) throws Exception {
		
  		GenericServiceResponse result = getServicesImpl().getActividadOcupacion(buscar);
  		Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
  	@POST
	@Path("/getHistorialPoliza")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getHistorialPoliza(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getHistorialPoliza(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}

  	  	
  	@POST
	@Path("/datoPolizaRiesgo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response datoPolizaRiesgo(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().datoPolizaRiesgo(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	
  	
  	@POST
	@Path("/domiciliodeCorrespondencia")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response domiciliodeCorrespondencia(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().domiciliodeCorrespondencia(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	
  	
  	@POST
	@Path("/coberturas")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response coberturas(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().coberturas(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	
  	@POST
	@Path("/mediosdePago")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response mediosdePago(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().mediosdePago(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	
  	
  	@POST
	@Path("/datosApoderadoAcreedor")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response datosApoderadoAcreedor(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().datosApoderadoAcreedor(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	
  	@POST
	@Path("/pendientesTotales")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response pendientesTotales(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().pendientesTotales(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	
  	@POST
	@Path("/opRet")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response opRet(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().opRet(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	
  	@POST
	@Path("/emitidasTotales")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response emitidasTotales(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().emitidasTotales(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	
  	@POST
	@Path("/aviVenPolTotales")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response aviVenPolTotales(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().aviVenPolTotales(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	
  	@POST
	@Path("/getDeudaCobradaCanalesDetalle")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getDeudaCobradaCanalesDetalle(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getDeudaCobradaCanalesDetalle(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	
  	@POST
	@Path("/getDeudaCobradaCanales")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getDeudaCobradaCanales(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getDeudaCobradaCanales(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	
  	
  	@POST
	@Path("/getDeudaCobradaTotales")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getDeudaCobradaTotales(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getDeudaCobradaTotales(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	
  	@POST
	@Path("/workflows")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response workflows(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().workflows(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	

  	@POST
	@Path("/getRecibosNotasDeCredito")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getRecibosNotasDeCredito(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getRecibosNotasDeCredito(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	
  	@POST
	@Path("/getCuponesPendientesDetalle")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getCuponesPendientesDetalle(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getCuponesPendientesDetalle(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
  	
	@POST
	@Path("/getCuponesPendientes")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getCuponesPendientes(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getCuponesPendientes(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
	
	@POST
	@Path("/productoresHabilitadosListOp")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response productoresHabilitadosListOp(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().productoresHabilitadosListOp(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
  	
	
	@POST
	@Path("/mediosdepagoxcia")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
//	@ApiOperation(value = "Devuelve xxxxx", response = String.class, produces = "......")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response mediosdepagoxcia( String request) throws Exception {
		
		GenericServiceResponse result = getServicesImpl().mediosdepagoxcia(request);		
        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
  		return Response.status(status).entity(result.getResponse()).build();
	}
	
	
	
//	@HEAD
	@GET
	@Path("/version")
//	@ApiResponses(value = {
//			@ApiResponse(code = 200, message = "Success", response = String.class),
//			@ApiResponse(code = 500, message = "Error", response = String.class) })
	public Response getVersion() throws Exception {
		try {
			return Response
					.status(Response.Status.OK)
					.header("VERSION", "1")
					.build();
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(Response.Status.PRECONDITION_FAILED)
					.entity(e.getMessage()).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/transArchivoInterfases")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response transfiereArchivoInterfases( String request) throws Exception {
		try{	
			GenericServiceResponse result = getServicesImpl().transfiereArchivoInterfases(request);		
	        Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/consultaDeLote")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response consultaDeLote( String request) throws Exception {
		
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().consultaDeLote(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/consultaOperacionesPorLote")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response consultaOperacionesPorLote( String request) throws Exception {
		
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().consultaOperacionesPorLote(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/nombreMailBox")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response nombreMailBox( String request) throws Exception {
		
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().nombreMailBox(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/archivoDeRetorno")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response archivoDeRetorno( String request) throws Exception {
		
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().archivoDeRetorno(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/consultadeSolicitudesRechazadas")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response consultadeSolicitudesRechazadas( String request) throws Exception {
		
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().consultadeSolicitudesRechazadas(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/estadoArchivo")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response estadoArchivo( String request) throws Exception {
		
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().estadoArchivo(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/altaEndoso")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response altaEndoso( String request) throws Exception {
		
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().altaEndoso(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/consultaSolicitudes")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response consultaSolicitudes( String request) throws Exception {
		
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().consultaSolicitudes(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/busquedaConstanciasPago")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response busquedaConstanciasPago( String request) throws Exception {
		
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().busquedaConstanciasPago(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/getPolizasEndosables")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getPolizasEndosables( String request) throws Exception {
		
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getPolizasEndosables(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/cuponesConstanciasPago")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response cuponesConstanciasPago( String request) throws Exception {
		
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().cuponesConstanciasPago(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
}