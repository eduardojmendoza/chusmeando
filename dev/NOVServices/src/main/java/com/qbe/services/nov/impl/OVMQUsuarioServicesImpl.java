package com.qbe.services.nov.impl;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.qbe.services.jaxb.AnyXmlElement;
import com.qbe.services.mqgeneric.impl.LBAW_MQMensajeGestion;
import com.qbe.services.nov.NOVServiceResponse;
import com.qbe.services.nov.jaxb.mqmensajegestion.perfilusuario.PerfilUsuario;
import com.qbe.services.nov.jaxb.mqmensajegestion.perfilusuario.PerfilUsuarioResponse;
import com.qbe.services.nov.jaxb.mqusuario.rolesadministrativa.RolesAdministrativa;
import com.qbe.services.nov.jaxb.mqusuario.rolesadministrativa.RolesAdministrativaResponse;
import com.qbe.services.ovmqusuario.impl.lbaw_OVRolesAdmin;
import com.qbe.vbcompat.string.StringHolder;

/**
 * Implementación del servicio
 * 
 * @author ramiro
 *
 */
public class OVMQUsuarioServicesImpl {

	private static final String EMPTY_STRING = "";

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	public OVMQUsuarioServicesImpl() {
	}

	private lbaw_OVRolesAdmin getlbaw_OVRolesAdminn() {
		lbaw_OVRolesAdmin ovra = new lbaw_OVRolesAdmin();
		return ovra;
	}

	public RolesAdministrativaResponse rolesAdministrativa(
			String usuario) throws Exception {
		
		lbaw_OVRolesAdmin ovra = getlbaw_OVRolesAdminn();
		
		StringHolder sh = new StringHolder();
		String reqXML = createRolesAdministrativaRequestString(usuario);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = ovra.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de rolesAdministrativa: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(RolesAdministrativa.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        RolesAdministrativa result = (RolesAdministrativa) unmarshaller.unmarshal(new StringReader(sh.getValue()));
        
        //Retorno un Wrapper que incluye el resultCode
        RolesAdministrativaResponse response = new RolesAdministrativaResponse();
        response.resultCode = resultCode;
        response.response = result;
        
		return response;
	}

	private String createRolesAdministrativaRequestString(String usuario) {
		return "<Request>"
				+ "<USUARIO>" + usuario + "</USUARIO>"
				+ "</Request>";
	}
			
	

	
}
