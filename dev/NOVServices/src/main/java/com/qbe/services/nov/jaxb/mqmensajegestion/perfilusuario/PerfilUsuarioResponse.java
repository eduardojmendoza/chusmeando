package com.qbe.services.nov.jaxb.mqmensajegestion.perfilusuario;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.qbe.services.nov.ResponseWithEstado;
import com.qbe.services.nov.ServiceResponse;


/**
 * Wrapper de PerfilUsuario que incluye el resultCode
 * 
 *  
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfilUsuarioResponse implements ServiceResponse {
	
	@XmlElement( name = "resultCode", required = true)
	public int resultCode;
	
	@XmlElement( name = "response", required = true)
	public PerfilUsuario response;
	
	public PerfilUsuarioResponse() {
	}

	@Override
	public ResponseWithEstado getResponseWithEstado() {
		return response;
	}

	@Override
	public int getResultCode() {
		return resultCode;
	}
}
