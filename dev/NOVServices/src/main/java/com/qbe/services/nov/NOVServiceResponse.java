package com.qbe.services.nov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.jaxb.AnyXmlElement;


/**
 * Usada para no tener que especificar clases JAXB específicas de cada operación
 * 
 * Deprecada porque al ser genérica no tiene suficiente información como para generar un JSON equivalente a un XML.
 * Por ejemplo no genera arrays consistentemente, si en el XML viene <campos><campo/><campos>, lo manda como {}, pero
 * si hay varios como en <campos><campo/><campo/><campos> puede generar un Array de acuerdo a ciertos parámetros del  JAXBContext
 * @author ramiro
 *
 */
@Deprecated
@XmlRootElement
@XmlType(name = "NOVServiceResponse", propOrder = {
	    "code", "response"
	})
@XmlAccessorType(XmlAccessType.FIELD)
public class NOVServiceResponse {
	@XmlElement( name = "code", required = true)
	private int code;
	
	@XmlElement( name = "Response", required = true)
	private AnyXmlElement response;

	public NOVServiceResponse() {
	}

	public NOVServiceResponse(int code, AnyXmlElement response) {
		this.response = response;
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int resultCode) {
		this.code = resultCode;
	}

	public AnyXmlElement getResponse() {
		return response;
	}

	public void setResponse(AnyXmlElement response) {
		this.response = response;
	}

}
