package com.qbe.services.nov.impl;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;

import com.qbe.services.mqgeneric.impl.LBAW_MQMensaje;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.jaxb.mqmensaje.getorganizadores.GetOrganizadores;
import com.qbe.services.nov.jaxb.mqmensaje.getorganizadores.GetOrganizadoresResponse;
import com.qbe.services.nov.jaxb.mqmensaje.legajoempleadolba.LegajoEmpleadoLBA;
import com.qbe.services.nov.jaxb.mqmensaje.legajoempleadolba.LegajoEmpleadoLBAResponse;
import com.qbe.services.nov.jaxb.mqmensaje.listadosucursalesporbanco.ListadoSucursalesPorBanco;
import com.qbe.services.nov.jaxb.mqmensaje.listadosucursalesporbanco.ListadoSucursalesPorBancoResponse;
import com.qbe.services.nov.jaxb.mqmensaje.productoreshabilitadosparacotizar.ProductoresHabilitadosParaCotizar;
import com.qbe.services.nov.jaxb.mqmensaje.productoreshabilitadosparacotizar.ProductoresHabilitadosParaCotizarResponse;
import com.qbe.services.nov.jaxb.mqmensaje.productosporproductor.ProductosPorProductor;
import com.qbe.services.nov.jaxb.mqmensaje.productosporproductor.ProductosPorProductorResponse;
import com.qbe.services.nov.jaxb.mqmensaje.renovacionporusuario.RenovacionPorUsuario;
import com.qbe.services.nov.jaxb.mqmensaje.renovacionporusuario.RenovacionPorUsuarioResponse;
import com.qbe.services.ovmq.impl.lbaw_OVExigibleTotales;
import com.qbe.vbcompat.string.StringHolder;

/**
 * Implementación del servicio
 * 
 * @author ramiro
 * @author cesar
 *
 */
public class ConsultaMQServicesImpl {

	private static final String EMPTY_STRING = "";

	protected Logger logger = Logger.getLogger(getClass().getName());

	public ConsultaMQServicesImpl() {
	}

	private LBAW_MQMensaje getMQMensaje() {
		LBAW_MQMensaje mqmg = new LBAW_MQMensaje();
		return mqmg;
	}

	public ProductosPorProductorResponse productosPorProductor(String usuarcod) throws Exception {

		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		String reqXML = createProductosPorProductorRequestString(usuarcod);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = mqmg.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de productosPorProductor: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		// SH tiene un XML como String
		// Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(ProductosPorProductor.class);

		Unmarshaller unmarshaller = jc.createUnmarshaller();
		ProductosPorProductor ppp = (ProductosPorProductor) unmarshaller.unmarshal(new StringReader(sh.getValue()));

		// Retorno un Wrapper que incluye el resultCode
		ProductosPorProductorResponse pppr = new ProductosPorProductorResponse();
		pppr.resultCode = resultCode;
		pppr.response = ppp;

		return pppr;
	}

	public ProductoresHabilitadosParaCotizarResponse productoresHabilitadosParaCotizar(String aplicarXSL,
			String ramopcod, String usuarcod, String cliensecas, String nivelclas) throws Exception {

		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		String reqXML = createProductoresHabilitadosParaCotizarRequestString(aplicarXSL, ramopcod, usuarcod, cliensecas,
				nivelclas);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = mqmg.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de productoresHabilitadosParaCotizar: [code: " + resultCode
				+ ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		// SH tiene un XML como String
		// Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(ProductoresHabilitadosParaCotizar.class);

		Unmarshaller unmarshaller = jc.createUnmarshaller();
		ProductoresHabilitadosParaCotizar phc = (ProductoresHabilitadosParaCotizar) unmarshaller
				.unmarshal(new StringReader(sh.getValue()));

		// Retorno un Wrapper que incluye el resultCode
		ProductoresHabilitadosParaCotizarResponse phcr = new ProductoresHabilitadosParaCotizarResponse();
		phcr.resultCode = resultCode;
		phcr.response = phc;

		return phcr;
	}

	private String createProductoresHabilitadosParaCotizarRequestString(String aplicarXSL, String ramopcod,
			String usuarcod, String cliensecas, String nivelclas) {
		StringBuffer sb = new StringBuffer();
		String base = "<Request>" + "<DEFINICION>ProductoresHabilitadosParaCotizar.xml</DEFINICION>" + "<AplicarXSL>"
				+ aplicarXSL + "</AplicarXSL>" + "<RAMOPCOD>" + ramopcod + "</RAMOPCOD>" + "<USUARCOD>" + usuarcod
				+ "</USUARCOD>";

		sb.append(base);

		if (!StringUtils.isEmpty(cliensecas)) {
			sb.append("<CLIENSECAS>");
			sb.append(cliensecas);
			sb.append("</CLIENSECAS>");
		}
		if (!StringUtils.isEmpty(nivelclas)) {
			sb.append("<NIVELCLAS>");
			sb.append(nivelclas);
			sb.append("</NIVELCLAS>");
		}
		sb.append("</Request>");
		return sb.toString();
	}

	private String createProductosPorProductorRequestString(String usuarcod) {
		return "<Request>" + "<DEFINICION>ProductosxProductor.xml</DEFINICION>" + "<USUARCOD>" + usuarcod
				+ "</USUARCOD>" + "</Request>";
	}

	public GetOrganizadoresResponse getOrganizadores(String usuario, String agenteclase, String agentecodigo)
			throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		String reqXML = createGetOrganizadoresRequestString(usuario, agenteclase, agentecodigo);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = mqmg.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de getOrganizadores: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		// SH tiene un XML como String
		// Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(GetOrganizadores.class);

		Unmarshaller unmarshaller = jc.createUnmarshaller();
		GetOrganizadores go = (GetOrganizadores) unmarshaller.unmarshal(new StringReader(sh.getValue()));

		// Retorno un Wrapper que incluye el resultCode
		GetOrganizadoresResponse gor = new GetOrganizadoresResponse();
		gor.resultCode = resultCode;
		gor.response = go;

		return gor;
	}

	private String createGetOrganizadoresRequestString(String usuario, String agenteclase, String agentecodigo) {
		return "<Request>" + "<DEFINICION>GetOrganizadores.xml</DEFINICION>" + "<USUARIO>" + usuario + "</USUARIO>"
				+ "<AGENTECLASE>" + agenteclase + "</AGENTECLASE>" + "<AGENTECODIGO>" + agentecodigo + "</AGENTECODIGO>"
				+ "</Request>";
	}

	public RenovacionPorUsuarioResponse renovacionPorUsuario(String usuarcod) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		String reqXML = createRenovacionPorUsuarioRequestString(usuarcod);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = mqmg.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de getOrganizadores: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		// SH tiene un XML como String
		// Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(RenovacionPorUsuario.class);

		Unmarshaller unmarshaller = jc.createUnmarshaller();
		RenovacionPorUsuario go = (RenovacionPorUsuario) unmarshaller.unmarshal(new StringReader(sh.getValue()));

		// Retorno un Wrapper que incluye el resultCode
		RenovacionPorUsuarioResponse gor = new RenovacionPorUsuarioResponse();
		gor.resultCode = resultCode;
		gor.response = go;

		return gor;
	}

	private String createRenovacionPorUsuarioRequestString(String usuarcod) {
		return "<Request>" + "<DEFINICION>1660_RenovacionxUsuario.xml</DEFINICION>" + "<USUARCOD>" + usuarcod
				+ "</USUARCOD>" + "</Request>";
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse renovacionPorUsuarioXML(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de RenovacionPorUsuario: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null) {
			throw new Exception("Respuesta en NULL del componente");
		}
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	public ListadoSucursalesPorBancoResponse listadoSucursalesPorBanco(String bancocod, String aplicarXSL)
			throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		String reqXML = createListadoSucursalesPorBancoRequestString(bancocod, aplicarXSL);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = mqmg.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de getOrganizadores: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		// SH tiene un XML como String
		// Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(ListadoSucursalesPorBanco.class);

		Unmarshaller unmarshaller = jc.createUnmarshaller();
		ListadoSucursalesPorBanco content = (ListadoSucursalesPorBanco) unmarshaller
				.unmarshal(new StringReader(sh.getValue()));

		// Retorno un Wrapper que incluye el resultCode
		ListadoSucursalesPorBancoResponse response = new ListadoSucursalesPorBancoResponse();
		response.resultCode = resultCode;
		response.response = content;

		return response;
	}

	private String createListadoSucursalesPorBancoRequestString(String bancocod, String aplicarXSL) {
		return "<Request>" + "<DEFINICION>1018_ListadoSucursalesxBanco.xml</DEFINICION>" + "<BANCOCOD>" + bancocod
				+ "</BANCOCOD>" + (StringUtils.isEmpty(aplicarXSL) ? "" : "<AplicarXSL>" + aplicarXSL + "</AplicarXSL>")
				+ "</Request>";
	}

	public LegajoEmpleadoLBAResponse legajoEmpleadoLBA(String bancocod, String sucurcod, String legajnum)
			throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		String reqXML = createLegajoEmpleadoLBARequestString(bancocod, sucurcod, legajnum);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = mqmg.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de getOrganizadores: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		// SH tiene un XML como String
		// Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(LegajoEmpleadoLBA.class);

		Unmarshaller unmarshaller = jc.createUnmarshaller();
		LegajoEmpleadoLBA content = (LegajoEmpleadoLBA) unmarshaller.unmarshal(new StringReader(sh.getValue()));

		// Retorno un Wrapper que incluye el resultCode
		LegajoEmpleadoLBAResponse response = new LegajoEmpleadoLBAResponse();
		response.resultCode = resultCode;
		response.response = content;

		return response;
	}

	private String createLegajoEmpleadoLBARequestString(String bancocod, String sucurcod, String legajnum) {
		return "<Request>" + "<DEFINICION>1027_LegajoempleadoLba.xml</DEFINICION>" + "<BANCOCOD>" + bancocod
				+ "</BANCOCOD>" + "<SUCURCOD>" + sucurcod + "</SUCURCOD>" + "<LEGAJNUM>" + legajnum + "</LEGAJNUM>"
				+ "</Request>";
	}

	public GenericServiceResponse campanasHabilitadasProductor(String usuarcod, String ramocod, String agentcla,
			String agentcod) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		String reqXML = createCampanasHabilitadasProductorRequestString(usuarcod, ramocod, agentcla, agentcod);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = mqmg.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de CampanasHabilitadasProductor: [code: " + resultCode
				+ ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		// SH tiene un XML como String
		// Transformo la respuesta en una JAXB
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	private String createCampanasHabilitadasProductorRequestString(String usuarcod, String ramocod, String agentcla,
			String agentcod) {
		return "<Request>" + "        <DEFINICION>CampanasHabilitadasProductor.xml</DEFINICION>" + "        <USUARCOD>"
				+ usuarcod + "</USUARCOD>" + "        <RAMOPCOD>" + ramocod + "</RAMOPCOD>" + "        <AGENTCLA>"
				+ agentcla + "</AGENTCLA>" + "        <AGENTCOD>" + agentcod + "</AGENTCOD>" + "    </Request>";
	}

	/**
	 * @param usuarcod
	 * @param ramopcod
	 * @param polizann
	 * @param polizsec
	 * @param campacod
	 * @param cobrocod
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse formasDePagoxCampana(String usuarcod, String ramopcod, String polizann,
			String polizsec, String campacod, String cobrocod) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		String reqXML = createFormasDePagoxCampanaRequestString(usuarcod, ramopcod, polizann, polizsec, campacod,
				cobrocod);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = mqmg.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de FormasDePagoxCampana: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	/**
	 * @param usuarcod
	 * @param ramopcod
	 * @param polizann
	 * @param polizsec
	 * @param campacod
	 * @param cobrocod
	 * @return
	 */
	private String createFormasDePagoxCampanaRequestString(String usuarcod, String ramopcod, String polizann,
			String polizsec, String campacod, String cobrocod) {
		StringBuilder formasDePagoXML = new StringBuilder();
		formasDePagoXML.append("<Request>");
		formasDePagoXML.append("        <DEFINICION>FormasDePagoxCampana.xml</DEFINICION>");
		formasDePagoXML.append("        <USUARCOD>" + usuarcod + "</USUARCOD>");
		formasDePagoXML.append("        <RAMOPCOD>" + ramopcod + "</RAMOPCOD>");
		formasDePagoXML.append("        <POLIZANN>" + polizann + "</POLIZANN>");
		formasDePagoXML.append("        <POLIZSEC>" + polizsec + "</POLIZSEC>");
		formasDePagoXML.append("        <CAMPACOD>" + campacod + "</CAMPACOD>");
		if (cobrocod != null && !"".equals(cobrocod)) {
			formasDePagoXML.append("        <COBROCOD>" + cobrocod + "</COBROCOD>");
		}
		formasDePagoXML.append("</Request>");
		return formasDePagoXML.toString();

	}

	public GenericServiceResponse listadoVehiculosxProd(String usuarcod, String aumarcod, String auvehdes,
			String procann, String procmes, String procdia, String auprocod) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		String reqXML = createListadoVehiculosxProdRequestString(usuarcod, aumarcod, auvehdes, procann, procmes,
				procdia, auprocod);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = mqmg.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 1215_ListadoVehiculosxProd: [code: " + resultCode
				+ ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	private String createListadoVehiculosxProdRequestString(String usuarcod, String aumarcod, String auvehdes,
			String procann, String procmes, String procdia, String auprocod) {
		return "<Request>" + "    <DEFINICION>1215_ListadoVehiculosxProd.xml</DEFINICION>" + "    <USUARCOD>" + usuarcod
				+ "</USUARCOD>" + "    <AUMARCOD>" + aumarcod + "</AUMARCOD>" + "    <AUVEHDES>" + auvehdes
				+ "</AUVEHDES>" + "    <PROCANN>" + procann + "</PROCANN>" + "    <PROCMES>" + procmes + "</PROCMES>"
				+ "    <PROCDIA>" + procdia + "</PROCDIA>" + "    <AUPROCOD>" + auprocod + "</AUPROCOD>" + "</Request>";

	}

	public GenericServiceResponse getDispRastreo(String usuario, String ramopcod, String modeautcod, String sumaseg,
			String provi, String localidadcod, String opcion, String operacion, String prescod, String instala)
			throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		String reqXML = create1510_GetDispRastreoRequestString(usuario, ramopcod, modeautcod, sumaseg, provi,
				localidadcod, opcion, operacion, prescod, instala);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = mqmg.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 1510_GetDispRastreo: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	private String create1510_GetDispRastreoRequestString(String usuario, String ramopcod, String modeautcod,
			String sumaseg, String provi, String localidadcod, String opcion, String operacion, String prescod,
			String instala) {
		return "<Request>" + "    <DEFINICION>1510_GetDispRastreo.xml</DEFINICION>" + "    <USUARIO>" + usuario
				+ "</USUARIO>" + "    <RAMOPCOD>" + ramopcod + "</RAMOPCOD>" + "    <MODEAUTCOD>" + modeautcod
				+ "</MODEAUTCOD>" + "    <SUMASEG>" + sumaseg + "</SUMASEG>" + "    <PROVI>" + provi + "</PROVI>"
				+ "    <LOCALIDADCOD>" + localidadcod + "</LOCALIDADCOD>" + "    <OPCION>" + opcion + "</OPCION>"
				+ "    <OPERACION>" + operacion + "</OPERACION>" + "    <PRESCOD>" + prescod + "</PRESCOD>"
				+ "    <INSTALA>" + instala + "</INSTALA>" + "</Request>";

	}

	public GenericServiceResponse getListadoCoberturas(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de ListadoCoberturas: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	public GenericServiceResponse msjGralCotizacion(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de msjGralCotizacion: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	public GenericServiceResponse calculoPremio(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de CalculoPremio: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	public GenericServiceResponse prorrateoPrimas(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de prorrateoPrimas: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	public GenericServiceResponse listadoObjetos(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de listadoObjetos: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	public GenericServiceResponse integracionGral(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de integracionGral: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	public GenericServiceResponse integracionHogarCartGral(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de integracionHogarCartGral: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	public GenericServiceResponse integracionConsorcio(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de integracionConsorcio: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	public GenericServiceResponse leyendasxProdActCobertura(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de leyendasxProdActCobertura: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	public GenericServiceResponse montosRCxAscensor(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de montosRCxAscensor: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	public GenericServiceResponse parametrosxProducto(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de parametrosxProducto: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());

	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse polizasColectivasxProductor(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de PolizasColectivasxProductor: [code: " + resultCode
				+ ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	public GenericServiceResponse formasDePagoxPolizaColectiva(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de formasDePagoxPolizaColectiva: [code: " + resultCode
				+ ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse convenioProdOrganiz(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de convenioProdOrganiz: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse periodoCotizar(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de periodoCotizar: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse estabilizacion(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de estabilizacion: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse planPagoxProdPolMedCobro(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de planPagoxProdPolMedCobro: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse polizasxCanalHabilOv(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de polizasxCanalHabilOv: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse coberturasParaImpresion(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de coberturasParaImpresion: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse integracionAutosMPL(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de integracionAutosMPL: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse integracionATM(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de integracionATM: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse cultivosxProducto(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de cultivosxProducto: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse provinciasxCultivo(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de provinciasxCultivo: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse partidosxProvinciasCultivo(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de partidosxProvinciasCultivo: [code: " + resultCode
				+ ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse localidadesxPartidoProvinciasCultivo(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de localidadesxPartidoProvinciasCultivo: [code: " + resultCode
				+ ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse franquiciasGranizo(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de franquiciasGranizo: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse planesGranizo(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de planesGranizo: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse cotizacionGRA(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de cotizacionGRA: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse integracionGranizo(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de integracionGranizo: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	public GenericServiceResponse marcaTipoVehiculo(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de marcaTipoVehiculo: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse datosGralPoliza(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de datosGralPoliza: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}


	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse detalleAvisoVencimiento(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de detalleAvisoVencimiento: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse deudaAVencerTotales(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de deudaAVencerTotales: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse cartasReclamosTotales(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de cartasReclamosTotales: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse siniestrosTotales(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 1309_SiniestrosTotales: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse pagosSiniestrosTot(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 1310_PagosSiniestrosTot: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse exigibleTotales(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 1400_exigibleTotales: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse anulacionesTotales(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 1409_anulacionesTotales: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse cartera1603(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de 1603_cartera: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse legajoempleadoNew(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de legajoempleadoNew: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse suscripcionORyPR(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de ORyPRParaSuscripcion: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse archSus(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de 1634_archSus: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse avisoBajadaArchivo(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 1632_AvisoBajadaArchivo: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse cotizacionUnificadoRenovacion(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 2480_CotizacionUnificadoRenovacion: [code: " + resultCode
				+ ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse renovacionPolizaColectiva(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 2400_RenovacionPolizaColectiva: [code: " + resultCode
				+ ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse objetosxCertificados(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 2440_ObjetosXCertificados: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse datosEdificios(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 2450_DatosEdificios: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse renovacionRiesgos(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 2460_RenovacionRiegos: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse detalleCoberxProd(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 1123_OVDetalleCoberXProd: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse tipoUsoxProdPoliza(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 1123_OVDetalleCoberXProd: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse parametrosPolizaColectiva(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de 1123_OVDetalleCoberXProd: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 */
	public GenericServiceResponse sumasPersonalDomestico(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de sumasPersonalDomestico: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse validarVehiculo(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de validarVehiculo: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse validarPersona(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de ejecución de validarPersona: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse getApoderadoxClienteSec(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getApoderadoxClienteSec: [code: " + resultCode + ", texto: "
				+ sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse calculoPrimeraCuota(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de calculo primera cuota: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception 
	 */
	public GenericServiceResponse getIntegracionGralNew(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getIntegracionGralNew: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception 
	 */
	public GenericServiceResponse getIntegracionHOM1(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getIntegracionHOM1: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse getCotizacionHOM1(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();

		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getCotizacionHOM1: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse getRetencionesComisiones(String request) throws Exception{
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getCotizacionHOM1: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 */
	public GenericServiceResponse getEPolizaAUS1(String request) throws Exception{
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getEpolizaAUS1: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse getActividadesCanal(String request) throws Exception{
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getActividadesCanal: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse getTpProducto(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getTpProducto: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse getPlanCuotasVigencia(String request) throws Exception{
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getPlanCuotasVigencia: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse getMarcasProductorTomador(String request)throws Exception{
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getMarcasProductorTomador: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse getFranquiciasCoberturas(String request) throws Exception{
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getFranquiciasCoberturas: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse getAccidentesPersonales(String request)throws Exception{
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getAccidentesPersonales: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse getBajaEndoso(String request)throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getBajaEndoso: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception 
	 */
	public GenericServiceResponse getNpsAUS1(String request) throws Exception {
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getNpsAUS1: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

	public GenericServiceResponse getRolesAdministrativos(String request) throws Exception{
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getRolesAdministrativos: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}
	
	public GenericServiceResponse getPolizasColectivasProductorMotos(String request) throws Exception{
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getPolizasColectivasProductorMotos: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}
	
	public GenericServiceResponse getMarcasMotos(String request) throws Exception{
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getMarcasMotos: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}
	
	public GenericServiceResponse getModelosMotos(String request) throws Exception{
		LBAW_MQMensaje mqmg = getMQMensaje();
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE,
				"Resultado de getModelosMotos: [code: " + resultCode + ", texto: " + sh.getValue() + "]");
		if (sh.getValue() == null)
			throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode, sh.getValue());
	}

}
