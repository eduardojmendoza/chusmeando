package com.qbe.services.nov.rest;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qbe.services.exutils.ExceptionUtils;
import com.qbe.services.nov.AisMqService;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.impl.OVColdviewServicesImpl;



import io.swagger.annotations.Api;

/**
 * 
 * @author ramiro
 * 
 */

@Path("/v1/OVColdview")
@Api(value="OVColdview")
public class OVColdviewRESTV1 extends AisMqService{

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	private OVColdviewServicesImpl getServicesImpl(){
		OVColdviewServicesImpl ovmq = new OVColdviewServicesImpl();
		return ovmq;
	}
	
	@POST
	@Path("/test")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response testConnect(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().testConnect(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		} 
	}
	
		
	@GET
	@Path("/version")
	public Response getVersion() throws Exception {
		try {
			return Response
					.status(Response.Status.OK)
					.header("VERSION", "1")
					.build();
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(Response.Status.PRECONDITION_FAILED)
					.entity(e.getMessage()).build();
		}
	}
	
	
	@POST
	@Path("/getXMLPDF")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getXMLPDF(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getXMLPDF(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		} 
	}
	
	@POST
	@Path("/sendTXTMail")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response sendTXTMail(String request) throws Exception {
	GenericServiceResponse result;
	try {	
	result = getServicesImpl().sendTXTMail(request);
	Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	 	return Response.status(status).entity(result.getResponse()).build();
	} catch (Exception e) {
	String stackTrace = ExceptionUtils.fullStackTrace(e);
	return Response.serverError().entity(stackTrace).build();
	} 
	}
	
	@POST
	@Path("/sendPDFMail")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response sendPDFMail(String request) throws Exception {
	GenericServiceResponse result;
	try {	
	result = getServicesImpl().sendPDFMail(request);
	Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	 	return Response.status(status).entity(result.getResponse()).build();
	} catch (Exception e) {
	String stackTrace = ExceptionUtils.fullStackTrace(e);
	return Response.serverError().entity(stackTrace).build();
	} 
	}
	
	@POST
	@Path("/getXMLPage")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getXMLPage(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getXMLPage(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		} 
	}


}
