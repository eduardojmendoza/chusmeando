package com.qbe.services.nov.rest;


import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qbe.services.exutils.ExceptionUtils;
import com.qbe.services.nov.AisMqService;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.impl.HogarMQServicesImpl;
import com.qbe.services.nov.impl.LBAWA_OVCotEndososServicesImpl;

import io.swagger.annotations.Api;



/**
 * 
 * @author cbonilla
 *
 */
@Path("/v1/CotEndososAPI")
@Api(value="CotEndosos")
public class LBAWA_OVCotEndososRESTV1 extends AisMqService {

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	protected LBAWA_OVCotEndososServicesImpl servicesImpl;
	
	public LBAWA_OVCotEndososRESTV1() {
	}
	
	public LBAWA_OVCotEndososServicesImpl getServicesImpl() {
		if (servicesImpl == null ) {
			this.servicesImpl = (LBAWA_OVCotEndososServicesImpl)getContext().getBean("CotEndososServices");
		}
		return servicesImpl;
	}

	
	@POST
	@Path("/cotizadorEndosos")
	@Produces({MediaType.APPLICATION_XML})
	@Consumes({MediaType.APPLICATION_XML})  
	public Response cotizadorEndosos(String request)  {
		
		GenericServiceResponse result;
		try {
			result = getServicesImpl().cotizadorEndosos(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/datosGeneralesPolizaHogar")
	@Produces({MediaType.APPLICATION_XML})	
	@Consumes({MediaType.APPLICATION_XML})  
	public Response datosGeneralesPolizaHogar(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().datosGeneralesPolizaHogar(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/precioUltimaFacturacionHogar")
	@Produces({MediaType.APPLICATION_XML})	
	@Consumes({MediaType.APPLICATION_XML})  
	public Response precioUltimaFacturacionHogar(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().precioUltimaFacturacionHogar(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	@POST
	@Path("/getDatosRiesgoHogar")
	@Produces({MediaType.APPLICATION_XML})	
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getDatosRiesgoHogar(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getDatosRiesgoHogar(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	@POST
	@Path("/getDatosRiesgoHogarManual")
	@Produces({MediaType.APPLICATION_XML})	
	@Consumes({MediaType.APPLICATION_XML})  
	public Response getDatosRiesgoHogarManual(String request) throws Exception {
		GenericServiceResponse result;
		try {		
			result = getServicesImpl().getDatosRiesgoHogarManual(request);
			Response.Status status = NovRestServicesUtil.mapResultCodeToStatus(result.getCode());
	  		return Response.status(status).entity(result.getResponse()).build();
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.fullStackTrace(e);
			return Response.serverError().entity(stackTrace).build();
		}
	}
	
	
	
}
