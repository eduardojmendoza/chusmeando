package com.qbe.services.nov.jaxb.mqecoupons.getcupimp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
*
* Respuesta recibida del MQMensaje
* 
 
        <RECIBO>
           <POLIZA>AUS1-00-000001/0000-0001-856577</POLIZA>
           <DESDE>01/10/2016</DESDE>
           <HASTA>01/11/2016</HASTA>
           <CODIGO>901867616</CODIGO>
           <VENCIMIENTO>16/11/2016</VENCIMIENTO>
           <IMPORTE>2549.38</IMPORTE>
        </RECIBO>
        <RECIBO>
           <POLIZA>AUS1-00-000001/0000-0001-981402</POLIZA>
           <DESDE>01/11/2016</DESDE>
           <HASTA>01/12/2016</HASTA>
           <CODIGO>911387512</CODIGO>
           <VENCIMIENTO>16/12/2016</VENCIMIENTO>
           <IMPORTE>2485.44</IMPORTE>
        </RECIBO>
    
* 
* @author jose
*
*/
@XmlAccessorType(XmlAccessType.FIELD)
public class Recibo {
	@XmlElement(name="POLIZA")
	public String poliza;
	
	@XmlElement(name="DESDE")
	public String desde;
	
	@XmlElement(name="HASTA")
	public String hasta;
	
	@XmlElement(name="CODIGO")
	public String codigo;
	
	@XmlElement(name="VENCIMIENTO")
	public String vencimiento;
	
	@XmlElement(name="IMPORTE")
	public String importe;
}
