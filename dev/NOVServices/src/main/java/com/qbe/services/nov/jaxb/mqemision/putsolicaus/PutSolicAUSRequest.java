package com.qbe.services.nov.jaxb.mqemision.putsolicaus;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
*
* Request
* 
    <Request>
        <POLVENCIANN>0000</POLVENCIANN>
        <POLVENCIMES>00</POLVENCIMES>
        <POLVENVIDIA>00</POLVENVIDIA>
        <RAMOPCOD>AUS1</RAMOPCOD>
        <POLIZANN>0</POLIZANN>
        <POLIZSEC>1</POLIZSEC>
        <CERTIPOL>9000</CERTIPOL>
        <CERTIANN>0</CERTIANN>
        <SUPLENUM>0</SUPLENUM>
        <FRANQCOD>99</FRANQCOD>
        <PQTDES>TERCEROS COMPLETOS (SIN DAOS PARCIALES)/FRANQUICIA NO APLICABLE</PQTDES>
        <PLANCOD>014</PLANCOD>
        <HIJOS1416>0</HIJOS1416>
        <HIJOS1729>0</HIJOS1729>
        <HIJOS1729ND>0</HIJOS1729ND>
        <ZONA>0001</ZONA>
        <CODPROV>1</CODPROV>
        <SUMALBA>171000.00</SUMALBA>
        <CLIENIVA>3</CLIENIVA>
        <SUMASEG>171000.00</SUMASEG>
        <CLUB_LBA>S</CLUB_LBA>
        <DESTRUCCION_80>N</DESTRUCCION_80>
        <CPAANO>2014</CPAANO>
        <CTAKMS>0</CTAKMS>
        <ESCERO>N</ESCERO>
        <TIENEPLAN>N</TIENEPLAN>
        <COND_ADIC>0</COND_ADIC>
        <ASEG_ADIC>N</ASEG_ADIC>
        <FH_NAC>19760112</FH_NAC>
        <SEXO>M</SEXO>
        <ESTCIV>S</ESTCIV>
        <SIFMVEHI_DES><![CDATA[C3 PICASSO 1.6I EXCLUSIVE MY WAY NACIONAL (2011-2015)]]></SIFMVEHI_DES>
        <PROFECOD>000000</PROFECOD>
        <SIACCESORIOS>N</SIACCESORIOS>
        <REFERIDO>0</REFERIDO>
        <MOTORNUM>GYUG679G68</MOTORNUM>
        <CHASINUM>Y56Y56Y56Y56</CHASINUM>
        <PATENNUM>MUS445</PATENNUM>
        <AUMARCOD>00010</AUMARCOD>
        <AUMODCOD>00034</AUMODCOD>
        <AUSUBCOD>00008</AUSUBCOD>
        <AUADICOD>03796</AUADICOD>
        <AUMODORI>N</AUMODORI>
        <AUUSOCOD>1</AUUSOCOD>
        <AUVTVCOD>N</AUVTVCOD>
        <AUVTVDIA>0</AUVTVDIA>
        <AUVTVMES>0</AUVTVMES>
        <AUVTVANN>0</AUVTVANN>
        <VEHCLRCOD>9</VEHCLRCOD>
        <AUKLMNUM>8000</AUKLMNUM>
        <FABRICAN>2012</FABRICAN>
        <FABRICMES>1</FABRICMES>
        <GUGARAGE>S</GUGARAGE>
        <GUDOMICI>S</GUDOMICI>
        <AUCATCOD>14</AUCATCOD>
        <AUTIPCOD>0005</AUTIPCOD>
        <AUCIASAN/>
        <AUANTANN>15</AUANTANN>
        <AUNUMSIN>0</AUNUMSIN>
        <AUNUMKMT>0</AUNUMKMT>
        <AUUSOGNC>N</AUUSOGNC>
        <USUARCOD>EX009005L</USUARCOD>
        <SITUCPOL>O</SITUCPOL>
        <CLIENSEC>47843</CLIENSEC>
        <NUMEDOCU>18069158</NUMEDOCU>
        <TIPODOCU>1</TIPODOCU>
        <DOMICSEC>1</DOMICSEC>
        <CUENTSEC>1</CUENTSEC>
        <COBROFOR>1</COBROFOR>
        <EDADACTU>39</EDADACTU>
        <COBERTURAS/>
        <ASEGURADOS/>
        <CAMP_CODIGO>1681</CAMP_CODIGO>
        <NRO_PROD>3233</NRO_PROD>
        <SUCURSAL_CODIGO>8888</SUCURSAL_CODIGO>
        <LEGAJO_VEND/>
        <EMPRESA_CODIGO>0</EMPRESA_CODIGO>
        <EMPL/>
        <LEGAJO_GTE/>
        <EMPL_NOMYAPE/>
        <CLIENTE_NOMYAPE>ARMATI MARIA ADRIANA</CLIENTE_NOMYAPE>
        <ACCESORIOS/>
        <ESTAINSP>0</ESTAINSP>
        <INSPECOD>0</INSPECOD>
        <INSPECTOR/>
        <INSPEADOM>S</INSPEADOM>
        <OBSERTXT/>
        <INSPEOBLEA/>
        <INSPEKMS>0</INSPEKMS>
        <CENTRCOD_INS/>
        <CENTRDES/>
        <RASTREO>0</RASTREO>
        <ALARMCOD/>
        <INSPEANN>0</INSPEANN>
        <INSPEMES>0</INSPEMES>
        <INSPEDIA>0</INSPEDIA>
        <NUMEDOCU_ACRE/>
        <TIPODOCU_ACRE>0</TIPODOCU_ACRE>
        <APELLIDO/>
        <NOMBRES/>
        <DOMICDOM/>
        <DOMICDNU/>
        <DOMICESC/>
        <DOMICPIS/>
        <DOMICPTA/>
        <DOMICPOB/>
        <DOMICCPO>0</DOMICCPO>
        <PROVICOD>0</PROVICOD>
        <PAISSCOD/>
        <BANCOCOD>9000</BANCOCOD>
        <COBROCOD>4</COBROCOD>
        <EFECTANN2>2015</EFECTANN2>
        <EFECTMES>11</EFECTMES>
        <EFECTDIA>09</EFECTDIA>
        <INSPEANN>0</INSPEANN>
        <INSPEMES>0</INSPEMES>
        <INSPEDIA>0</INSPEDIA>
        <ENTDOMSC>2</ENTDOMSC>
        <ANOTACIO><![CDATA[]]></ANOTACIO>
        <PRECIO_MENSUAL>1364.10</PRECIO_MENSUAL>
        <MODEAUTCOD>00010000340000803796N</MODEAUTCOD>
        <EFECTANN>2012</EFECTANN>
        <NACIMANN>1976</NACIMANN>
        <NACIMMES>01</NACIMMES>
        <NACIMDIA>12</NACIMDIA>
        <IVA>3</IVA>
        <IBB>0</IBB>
        <ESTADO>S</ESTADO>
        <KMSRNGCOD>8000</KMSRNGCOD>
        <SIGARAGE>S</SIGARAGE>
        <SINIESTROS>0</SINIESTROS>
        <GAS>N</GAS>
        <PROVI>1</PROVI>
        <LOCALIDADCOD>1005</LOCALIDADCOD>
        <CLUBLBA>S</CLUBLBA>
        <CLUBECO>S</CLUBECO>
        <GRANIZO>N</GRANIZO>
        <ROBOCONT>N</ROBOCONT>
        <DATOSPLAN>01499</DATOSPLAN>
        <CAMPACOD>1681</CAMPACOD>
        <CAMP_DESC><![CDATA[FULL COMISIÓN - SIN DESCUENTO (ACCIÓN 2015)]]></CAMP_DESC>
        <AGECLA>PR</AGECLA>
        <AGENTCLA>PR</AGENTCLA>
        <AGECOD>3233</AGECOD>
        <PORTAL>LBA_PRODUCTORES</PORTAL>
        <COBROTIP>AC</COBROTIP>
        <CUITNUME/>
        <RAZONSOC><![CDATA[]]></RAZONSOC>
        <CLIEIBTP>0</CLIEIBTP>
        <NROIIBB/>
        <LUNETA>N</LUNETA>
        <NROFACTU/>
        <DDJJ_STRING/>
        <DERIVACI/>
        <ALEATORIO/>
        <ESTAIDES/>
        <DISPODES/>
        <INSTALADP>N</INSTALADP>
        <POSEEDISP>N</POSEEDISP>
        <AUTIPGAMA>B</AUTIPGAMA>
        <SN_NBWS>S</SN_NBWS>
        <EMAIL_NBWS>RAMIROGM@GMAIL.COM</EMAIL_NBWS>
        <SN_EPOLIZA>N</SN_EPOLIZA>
        <EMAIL_EPOLIZA>MARIA.ARMATI@zurich.com.AR</EMAIL_EPOLIZA>
        <SN_PRESTAMAIL>N</SN_PRESTAMAIL>
        <PASSEPOL/>
        <MEDIOSCONTACTO>
            <MEDIO>
                <MEDCOSEC>0</MEDCOSEC>
                <MEDCOCOD>EMA</MEDCOCOD>
                <MEDCODAT>MARIA.ARMATI@zurich.com.AR</MEDCODAT>
                <MEDCOSYS/>
            </MEDIO>
            <MEDIO>
                <MEDCOSEC/>
                <MEDCOCOD/>
                <MEDCODAT/>
                <MEDCOSYS/>
            </MEDIO>
            <MEDIO>
                <MEDCOSEC/>
                <MEDCOCOD/>
                <MEDCODAT/>
                <MEDCOSYS/>
            </MEDIO>
            <MEDIO>
                <MEDCOSEC/>
                <MEDCOCOD/>
                <MEDCODAT/>
                <MEDCOSYS/>
            </MEDIO>
            <MEDIO>
                <MEDCOSEC/>
                <MEDCOCOD/>
                <MEDCODAT/>
                <MEDCOSYS/>
            </MEDIO>
            <MEDIO>
                <MEDCOSEC/>
                <MEDCOCOD/>
                <MEDCODAT/>
                <MEDCOSYS/>
            </MEDIO>
            <MEDIO>
                <MEDCOSEC/>
                <MEDCOCOD/>
                <MEDCODAT/>
                <MEDCOSYS/>
            </MEDIO>
            <MEDIO>
                <MEDCOSEC/>
                <MEDCOCOD/>
                <MEDCODAT/>
                <MEDCOSYS/>
            </MEDIO>
            <MEDIO>
                <MEDCOSEC/>
                <MEDCOCOD/>
                <MEDCODAT/>
                <MEDCOSYS/>
            </MEDIO>
            <MEDIO>
                <MEDCOSEC/>
                <MEDCOCOD/>
                <MEDCODAT/>
                <MEDCOSYS/>
            </MEDIO>
        </MEDIOSCONTACTO>
        <CERTISEC>1602</CERTISEC>
        <TIPOOPERACION>8</TIPOOPERACION>
        <OPCION>01</OPCION>
        <FUNCION>1</FUNCION>
        <GENERARLOG/>
        <CLICLIENAP1><![CDATA[ARMATI]]></CLICLIENAP1>
        <AISCLICLIENNOM><![CDATA[MARIA ADRIANA]]></AISCLICLIENNOM>
        <CLICLIENNOM><![CDATA[MARIA ADRIANA]]></CLICLIENNOM>
        <CLICLIENTIP>00</CLICLIENTIP>
        <UIFCUIT/>
        <CLIDOMICCAL>PAR</CLIDOMICCAL>
        <CLIDOMICDOM><![CDATA[FLORIDA]]></CLIDOMICDOM>
        <CLIDOMICDNU>229</CLIDOMICDNU>
        <CLIDOMICESC/>
        <CLIDOMICPIS/>
        <CLIDOMICPTA/>
        <CLIDOMICPOB><![CDATA[CAPITAL FEDERAL]]></CLIDOMICPOB>
        <CLIDOMICCPO>1005</CLIDOMICCPO>
        <CLIDOMPROVICOD>1</CLIDOMPROVICOD>
        <CLIDOMPAISSCOD>00</CLIDOMPAISSCOD>
        <CUENTDC/>
        <CUENNUME>5323600000017000</CUENNUME>
        <VENCIANN>2018</VENCIANN>
        <VENCIMES>1</VENCIMES>
        <CONDUCTORES>
            <CONDUCTOR>
                <CONDUSEC>1</CONDUSEC>
                <CONDUTDO>1</CONDUTDO>
                <CONDUDOC>18069158</CONDUDOC>
                <APELLIDOHIJO><![CDATA[ARMATI]]></APELLIDOHIJO>
                <NOMBREHIJO><![CDATA[MARIA ADRIANA]]></NOMBREHIJO>
                <SEXOHIJO>M</SEXOHIJO>
                <ESTADOHIJO>S</ESTADOHIJO>
                <NACIMHIJO>12/01/1976</NACIMHIJO>
                <EXCLUIDO>N</EXCLUIDO>
            </CONDUCTOR>
        </CONDUCTORES>
        <FESOLANN>2015</FESOLANN>
        <FESOLMES>11</FESOLMES>
        <FESOLDIA>9</FESOLDIA>
        <FEINGANN>2015</FEINGANN>
        <FEINGMES>11</FEINGMES>
        <FEINGDIA>9</FEINGDIA>
        <AGECLA>PR</AGECLA>
        <AGENTCLA>PR</AGENTCLA>
        <AGECOD>3233</AGECOD>
        <COBERTURAS>
            <COBERTURA>
                <COBERCOD>002</COBERCOD>
                <COBERORD>02</COBERORD>
                <CAPITASG>300000.00</CAPITASG>
                <CAPITIMP>300000.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>004</COBERCOD>
                <COBERORD>03</COBERORD>
                <CAPITASG>171000.00</CAPITASG>
                <CAPITIMP>1710.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>009</COBERCOD>
                <COBERORD>05</COBERORD>
                <CAPITASG>171000.00</CAPITASG>
                <CAPITIMP>1710.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>013</COBERCOD>
                <COBERORD>07</COBERORD>
                <CAPITASG>171000.00</CAPITASG>
                <CAPITIMP>1710.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>003</COBERCOD>
                <COBERORD>09</COBERORD>
                <CAPITASG>6.00</CAPITASG>
                <CAPITIMP>6.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>905</COBERCOD>
                <COBERORD>17</COBERORD>
                <CAPITASG>20000.00</CAPITASG>
                <CAPITIMP>20000.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>027</COBERCOD>
                <COBERORD>22</COBERORD>
                <CAPITASG>171000.00</CAPITASG>
                <CAPITIMP>1710.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>028</COBERCOD>
                <COBERORD>23</COBERORD>
                <CAPITASG>171000.00</CAPITASG>
                <CAPITIMP>1710.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>030</COBERCOD>
                <COBERORD>26</COBERORD>
                <CAPITASG>200000.00</CAPITASG>
                <CAPITIMP>200000.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>031</COBERCOD>
                <COBERORD>27</COBERORD>
                <CAPITASG>3800000.00</CAPITASG>
                <CAPITIMP>3800000.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>032</COBERCOD>
                <COBERORD>28</COBERORD>
                <CAPITASG>60000.00</CAPITASG>
                <CAPITIMP>60000.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>033</COBERCOD>
                <COBERORD>29</COBERORD>
                <CAPITASG>60000.00</CAPITASG>
                <CAPITIMP>60000.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>040</COBERCOD>
                <COBERORD>31</COBERORD>
                <CAPITASG>3000.00</CAPITASG>
                <CAPITIMP>3000.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>041</COBERCOD>
                <COBERORD>32</COBERORD>
                <CAPITASG>171000.00</CAPITASG>
                <CAPITIMP>1710.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>039</COBERCOD>
                <COBERORD>33</COBERORD>
                <CAPITASG>30000.00</CAPITASG>
                <CAPITIMP>30000.00</CAPITIMP>
            </COBERTURA>
            <COBERTURA>
                <COBERCOD>810</COBERCOD>
                <COBERORD>35</COBERORD>
                <CAPITASG>3000.00</CAPITASG>
                <CAPITIMP>3000.00</CAPITIMP>
            </COBERTURA>
        </COBERTURAS>
        <AUPAISCOD>00</AUPAISCOD>
        <AUPROVICOD>1</AUPROVICOD>
        <AUDOMCPACODPO>1005</AUDOMCPACODPO>
        <CENTRCOD>0010</CENTRCOD>
        <PLANPOLICOD>014</PLANPOLICOD>
        <CLIDOMCPACODPO>1005</CLIDOMCPACODPO>
        <CLIDOMICPRE>1165</CLIDOMICPRE>
        <CLIDOMICTLF>116567467467</CLIDOMICTLF>
        <SWACREPR>0</SWACREPR>
        <SUCURCOD>8888</SUCURCOD>
        <VIENEDMS>O</VIENEDMS>
        <PLANPAGOCOD>998</PLANPAGOCOD>
        <APOD>00</APOD>
        <SWAPODER>N</SWAPODER>
        <OCUPACODCLIE/>
        <OCUPACODAPOD/>
        <CLIENSECAPOD>0</CLIENSECAPOD>
        <DOMISECAPOD/>
    </Request>

* 
* @author ramiro
*
*/
@XmlRootElement( name = "Request")
@XmlAccessorType(XmlAccessType.FIELD)
public class PutSolicAUSRequest {
	
    @XmlElement(name="POLVENCIANN")
    public String polvenciann;


    @XmlElement(name="POLVENCIMES")
    public String polvencimes;


    @XmlElement(name="POLVENVIDIA")
    public String polvenvidia;


    @XmlElement(name="RAMOPCOD")
    public String ramopcod;


    @XmlElement(name="POLIZANN")
    public String polizann;


    @XmlElement(name="POLIZSEC")
    public String polizsec;


    @XmlElement(name="CERTIPOL")
    public String certipol;


    @XmlElement(name="CERTIANN")
    public String certiann;


    @XmlElement(name="SUPLENUM")
    public String suplenum;


    @XmlElement(name="FRANQCOD")
    public String franqcod;


    @XmlElement(name="PQTDES")
    public String pqtdes;


    @XmlElement(name="PLANCOD")
    public String plancod;


    @XmlElement(name="HIJOS1416")
    public String hijos1416;


    @XmlElement(name="HIJOS1729")
    public String hijos1729;


    @XmlElement(name="HIJOS1729ND")
    public String hijos1729nd;


    @XmlElement(name="ZONA")
    public String zona;


    @XmlElement(name="CODPROV")
    public String codprov;


    @XmlElement(name="SUMALBA")
    public String sumalba;


    @XmlElement(name="CLIENIVA")
    public String clieniva;


    @XmlElement(name="SUMASEG")
    public String sumaseg;


    @XmlElement(name="CLUB_LBA")
    public String club_lba;


    @XmlElement(name="DESTRUCCION_80")
    public String destruccion_80;


    @XmlElement(name="CPAANO")
    public String cpaano;


    @XmlElement(name="CTAKMS")
    public String ctakms;


    @XmlElement(name="ESCERO")
    public String escero;


    @XmlElement(name="TIENEPLAN")
    public String tieneplan;


    @XmlElement(name="COND_ADIC")
    public String cond_adic;


    @XmlElement(name="FH_NAC")
    public String fh_nac;


    @XmlElement(name="SEXO")
    public String sexo;


    @XmlElement(name="ESTCIV")
    public String estciv;


    @XmlElement(name="SIFMVEHI_DES")
    public String sifmvehi_des;


    @XmlElement(name="PROFECOD")
    public String profecod;


    @XmlElement(name="SIACCESORIOS")
    public String siaccesorios;


    @XmlElement(name="REFERIDO")
    public String referido;


    @XmlElement(name="MOTORNUM")
    public String motornum;


    @XmlElement(name="CHASINUM")
    public String chasinum;


    @XmlElement(name="PATENNUM")
    public String patennum;


    @XmlElement(name="AUMARCOD")
    public String aumarcod;


    @XmlElement(name="AUMODCOD")
    public String aumodcod;


    @XmlElement(name="AUSUBCOD")
    public String ausubcod;


    @XmlElement(name="AUADICOD")
    public String auadicod;


    @XmlElement(name="AUMODORI")
    public String aumodori;


    @XmlElement(name="AUUSOCOD")
    public String auusocod;


    @XmlElement(name="AUVTVCOD")
    public String auvtvcod;


    @XmlElement(name="AUVTVDIA")
    public String auvtvdia;


    @XmlElement(name="AUVTVMES")
    public String auvtvmes;


    @XmlElement(name="AUVTVANN")
    public String auvtvann;


    @XmlElement(name="VEHCLRCOD")
    public String vehclrcod;


    @XmlElement(name="AUKLMNUM")
    public String auklmnum;


    @XmlElement(name="FABRICAN")
    public String fabrican;


    @XmlElement(name="FABRICMES")
    public String fabricmes;


    @XmlElement(name="GUGARAGE")
    public String gugarage;


    @XmlElement(name="GUDOMICI")
    public String gudomici;


    @XmlElement(name="AUCATCOD")
    public String aucatcod;


    @XmlElement(name="AUTIPCOD")
    public String autipcod;


    @XmlElement(name="AUCIASAN")
    public String auciasan;


    @XmlElement(name="AUANTANN")
    public String auantann;


    @XmlElement(name="AUNUMSIN")
    public String aunumsin;


    @XmlElement(name="AUNUMKMT")
    public String aunumkmt;


    @XmlElement(name="AUUSOGNC")
    public String auusognc;


    @XmlElement(name="USUARCOD")
    public String usuarcod;


    @XmlElement(name="SITUCPOL")
    public String situcpol;


    @XmlElement(name="CLIENSEC")
    public String cliensec;


    @XmlElement(name="NUMEDOCU")
    public String numedocu;


    @XmlElement(name="TIPODOCU")
    public String tipodocu;


    @XmlElement(name="DOMICSEC")
    public String domicsec;


    @XmlElement(name="CUENTSEC")
    public String cuentsec;


    @XmlElement(name="COBROFOR")
    public String cobrofor;


    @XmlElement(name="EDADACTU")
    public String edadactu;


    @XmlElementWrapper( name = "ASEGURADOS", required = false)
    @XmlElement(name="ASEGURADO")
    public List<Asegurado> asegurado;


    @XmlElement(name="CAMP_CODIGO")
    public String camp_codigo;


    @XmlElement(name="NRO_PROD")
    public String nro_prod;


    @XmlElement(name="SUCURSAL_CODIGO")
    public String sucursal_codigo;


    @XmlElement(name="LEGAJO_VEND")
    public String legajo_vend;


    @XmlElement(name="EMPRESA_CODIGO")
    public String empresa_codigo;


    @XmlElement(name="EMPL")
    public String empl;


    @XmlElement(name="LEGAJO_GTE")
    public String legajo_gte;


    @XmlElement(name="EMPL_NOMYAPE")
    public String empl_nomyape;


    @XmlElement(name="CLIENTE_NOMYAPE")
    public String cliente_nomyape;


    @XmlElement(name="ACCESORIOS")
    public String accesorios;


    @XmlElement(name="ESTAINSP")
    public String estainsp;


    @XmlElement(name="INSPECOD")
    public String inspecod;


    @XmlElement(name="INSPECTOR")
    public String inspector;


    @XmlElement(name="INSPEADOM")
    public String inspeadom;


    @XmlElement(name="OBSERTXT")
    public String obsertxt;


    @XmlElement(name="INSPEOBLEA")
    public String inspeoblea;


    @XmlElement(name="INSPEKMS")
    public String inspekms;


    @XmlElement(name="CENTRCOD_INS")
    public String centrcod_ins;


    @XmlElement(name="CENTRDES")
    public String centrdes;


    @XmlElement(name="RASTREO")
    public String rastreo;


    @XmlElement(name="ALARMCOD")
    public String alarmcod;


    @XmlElement(name="NUMEDOCU_ACRE")
    public String numedocu_acre;


    @XmlElement(name="TIPODOCU_ACRE")
    public String tipodocu_acre;


    @XmlElement(name="APELLIDO")
    public String apellido;


    @XmlElement(name="NOMBRES")
    public String nombres;


    @XmlElement(name="DOMICDOM")
    public String domicdom;


    @XmlElement(name="DOMICDNU")
    public String domicdnu;


    @XmlElement(name="DOMICESC")
    public String domicesc;


    @XmlElement(name="DOMICPIS")
    public String domicpis;


    @XmlElement(name="DOMICPTA")
    public String domicpta;


    @XmlElement(name="DOMICPOB")
    public String domicpob;


    @XmlElement(name="DOMICCPO")
    public String domiccpo;


    @XmlElement(name="PROVICOD")
    public String provicod;


    @XmlElement(name="PAISSCOD")
    public String paisscod;


    @XmlElement(name="BANCOCOD")
    public String bancocod;


    @XmlElement(name="COBROCOD")
    public String cobrocod;


    @XmlElement(name="EFECTANN2")
    public String efectann2;


    @XmlElement(name="EFECTMES")
    public String efectmes;


    @XmlElement(name="EFECTDIA")
    public String efectdia;


    @XmlElement(name="INSPEANN")
    public String inspeann;


    @XmlElement(name="INSPEMES")
    public String inspemes;


    @XmlElement(name="INSPEDIA")
    public String inspedia;


    @XmlElement(name="ENTDOMSC")
    public String entdomsc;


    @XmlElement(name="ANOTACIO")
    public String anotacio;


    @XmlElement(name="PRECIO_MENSUAL")
    public String precio_mensual;


    @XmlElement(name="MODEAUTCOD")
    public String modeautcod;


    @XmlElement(name="EFECTANN")
    public String efectann;


    @XmlElement(name="NACIMANN")
    public String nacimann;


    @XmlElement(name="NACIMMES")
    public String nacimmes;


    @XmlElement(name="NACIMDIA")
    public String nacimdia;


    @XmlElement(name="IVA")
    public String iva;


    @XmlElement(name="IBB")
    public String ibb;


    @XmlElement(name="ESTADO")
    public String estado;


    @XmlElement(name="KMSRNGCOD")
    public String kmsrngcod;


    @XmlElement(name="SIGARAGE")
    public String sigarage;


    @XmlElement(name="SINIESTROS")
    public String siniestros;


    @XmlElement(name="GAS")
    public String gas;


    @XmlElement(name="PROVI")
    public String provi;


    @XmlElement(name="LOCALIDADCOD")
    public String localidadcod;


    @XmlElement(name="CLUBLBA")
    public String clublba;


    @XmlElement(name="CLUBECO")
    public String clubeco;


    @XmlElement(name="GRANIZO")
    public String granizo;


    @XmlElement(name="ROBOCONT")
    public String robocont;


    @XmlElement(name="DATOSPLAN")
    public String datosplan;


    @XmlElement(name="CAMPACOD")
    public String campacod;


    @XmlElement(name="CAMP_DESC")
    public String camp_desc;


    @XmlElement(name="PORTAL")
    public String portal;


    @XmlElement(name="COBROTIP")
    public String cobrotip;


    @XmlElement(name="CUITNUME")
    public String cuitnume;


    @XmlElement(name="RAZONSOC")
    public String razonsoc;


    @XmlElement(name="CLIEIBTP")
    public String clieibtp;


    @XmlElement(name="NROIIBB")
    public String nroiibb;


    @XmlElement(name="LUNETA")
    public String luneta;


    @XmlElement(name="NROFACTU")
    public String nrofactu;


    @XmlElement(name="DDJJ_STRING")
    public String ddjj_string;


    @XmlElement(name="DERIVACI")
    public String derivaci;


    @XmlElement(name="ALEATORIO")
    public String aleatorio;


    @XmlElement(name="ESTAIDES")
    public String estaides;


    @XmlElement(name="DISPODES")
    public String dispodes;


    @XmlElement(name="INSTALADP")
    public String instaladp;


    @XmlElement(name="POSEEDISP")
    public String poseedisp;


    @XmlElement(name="AUTIPGAMA")
    public String autipgama;


    @XmlElement(name="SN_NBWS")
    public String sn_nbws;


    @XmlElement(name="EMAIL_NBWS")
    public String email_nbws;


    @XmlElement(name="SN_EPOLIZA")
    public String sn_epoliza;


    @XmlElement(name="EMAIL_EPOLIZA")
    public String email_epoliza;


    @XmlElement(name="SN_PRESTAMAIL")
    public String sn_prestamail;


    @XmlElement(name="PASSEPOL")
    public String passepol;


    @XmlElementWrapper( name = "MEDIOSCONTACTO", required = false)
    @XmlElement(name="MEDIO")
    public List<Medio> medio;


    @XmlElement(name="CERTISEC")
    public String certisec;


    @XmlElement(name="TIPOOPERACION")
    public String tipooperacion;


    @XmlElement(name="OPCION")
    public String opcion;


    @XmlElement(name="FUNCION")
    public String funcion;


    @XmlElement(name="GENERARLOG")
    public String generarlog;


    @XmlElement(name="CLICLIENAP1")
    public String cliclienap1;


    @XmlElement(name="AISCLICLIENNOM")
    public String aisclicliennom;


    @XmlElement(name="CLICLIENNOM")
    public String clicliennom;


    @XmlElement(name="CLICLIENTIP")
    public String cliclientip;


    @XmlElement(name="UIFCUIT")
    public String uifcuit;


    @XmlElement(name="CLIDOMICCAL")
    public String clidomiccal;


    @XmlElement(name="CLIDOMICDOM")
    public String clidomicdom;


    @XmlElement(name="CLIDOMICDNU")
    public String clidomicdnu;


    @XmlElement(name="CLIDOMICESC")
    public String clidomicesc;


    @XmlElement(name="CLIDOMICPIS")
    public String clidomicpis;


    @XmlElement(name="CLIDOMICPTA")
    public String clidomicpta;


    @XmlElement(name="CLIDOMICPOB")
    public String clidomicpob;


    @XmlElement(name="CLIDOMICCPO")
    public String clidomiccpo;


    @XmlElement(name="CLIDOMPROVICOD")
    public String clidomprovicod;


    @XmlElement(name="CLIDOMPAISSCOD")
    public String clidompaisscod;


    @XmlElement(name="CUENTDC")
    public String cuentdc;


    @XmlElement(name="CUENNUME")
    public String cuennume;


    @XmlElement(name="VENCIANN")
    public String venciann;


    @XmlElement(name="VENCIMES")
    public String vencimes;


    @XmlElementWrapper( name = "CONDUCTORES", required = false)
    @XmlElement(name="CONDUCTOR")
    public List<Conductor> conductor;


    @XmlElement(name="FESOLANN")
    public String fesolann;


    @XmlElement(name="FESOLMES")
    public String fesolmes;


    @XmlElement(name="FESOLDIA")
    public String fesoldia;


    @XmlElement(name="FEINGANN")
    public String feingann;


    @XmlElement(name="FEINGMES")
    public String feingmes;


    @XmlElement(name="FEINGDIA")
    public String feingdia;


    @XmlElement(name="AGECLA")
    public String agecla;


    @XmlElement(name="AGENTCLA")
    public String agentcla;


    @XmlElement(name="AGECOD")
    public String agecod;


    @XmlElementWrapper( name = "COBERTURAS", required = false)
    @XmlElement(name="COBERTURA")
    public List<Cobertura> cobertura;


    @XmlElement(name="AUPAISCOD")
    public String aupaiscod;


    @XmlElement(name="AUPROVICOD")
    public String auprovicod;


    @XmlElement(name="AUDOMCPACODPO")
    public String audomcpacodpo;


    @XmlElement(name="CENTRCOD")
    public String centrcod;


    @XmlElement(name="PLANPOLICOD")
    public String planpolicod;


    @XmlElement(name="ASEG_ADIC")
    public String aseg_adic;


    @XmlElement(name="CLIDOMCPACODPO")
    public String clidomcpacodpo;


    @XmlElement(name="CLIDOMICPRE")
    public String clidomicpre;


    @XmlElement(name="CLIDOMICTLF")
    public String clidomictlf;


    @XmlElement(name="SWACREPR")
    public String swacrepr;


    @XmlElement(name="SUCURCOD")
    public String sucurcod;


    @XmlElement(name="VIENEDMS")
    public String vienedms;


    @XmlElement(name="PLANPAGOCOD")
    public String planpagocod;


    @XmlElement(name="APOD")
    public String apod;


    @XmlElement(name="SWAPODER")
    public String swapoder;


    @XmlElement(name="OCUPACODCLIE")
    public String ocupacodclie;


    @XmlElement(name="OCUPACODAPOD")
    public String ocupacodapod;


    @XmlElement(name="CLIENSECAPOD")
    public String cliensecapod;


    @XmlElement(name="DOMISECAPOD")
    public String domisecapod;


	
	public PutSolicAUSRequest() {
	}

}
