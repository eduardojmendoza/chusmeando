package com.qbe.services.nov.jaxb.mqcotizar.getcotiaus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
		<ACCESORIO>
			<CODIGOACC>5</CODIGOACC>
			<DESCRIPCIONACC><![CDATA[EQUIPO GNC: GAS S.A.]]></DESCRIPCIONACC>
			<PRECIOACC>8000</PRECIOACC>
		</ACCESORIO>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Accesorio {
	
    @XmlElement(name="CODIGOACC")	
	public String codigoacc;
	
    @XmlElement(name="DESCRIPCIONACC")	
	public String descripcionacc;
	
    @XmlElement(name="PRECIOACC")	
	public String precioacc;
	
}
