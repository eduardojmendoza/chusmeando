package com.qbe.services.nov.impl;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.qbe.services.ecoupons.impl.ECup_GetPolAUS;
import com.qbe.services.ecoupons.impl.Ecup_GetPolHOM;
import com.qbe.services.ecoupons.impl.ECup_GetUnlockAUS;
import com.qbe.services.ecoupons.impl.Ecup_GetCliensec;
import com.qbe.services.ecoupons.impl.Ecup_GetCupImp;
import com.qbe.services.ecoupons.impl.Ecup_GetUnlockHOM;
import com.qbe.services.nov.jaxb.mqecoupons.getcliensec.GetCliensec;
import com.qbe.services.nov.jaxb.mqecoupons.getcliensec.GetCliensecResponse;
import com.qbe.services.nov.jaxb.mqecoupons.getcupimp.GetCupImp;
import com.qbe.services.nov.jaxb.mqecoupons.getcupimp.GetCupImpResponse;
import com.qbe.services.nov.jaxb.mqecoupons.getpolaus.GetPolAus;
import com.qbe.services.nov.jaxb.mqecoupons.getpolaus.GetPolAusResponse;
import com.qbe.services.nov.jaxb.mqecoupons.getpolhom.GetPolHom;
import com.qbe.services.nov.jaxb.mqecoupons.getpolhom.GetPolHomResponse;
import com.qbe.services.nov.jaxb.mqecoupons.getunlockaus.GetUnlockAus;
import com.qbe.services.nov.jaxb.mqecoupons.getunlockaus.GetUnlockAusResponse;
import com.qbe.services.nov.jaxb.mqecoupons.getunlockhom.GetUnlockHom;
import com.qbe.services.nov.jaxb.mqecoupons.getunlockhom.GetUnlockHomResponse;
import com.qbe.vbcompat.string.StringHolder;

/**
 * Implementación del servicio
 * 
 * @author jose
 *
 */
public class LBAEcouponsMQServicesImpl {

	private static final String EMPTY_STRING = "";

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	public LBAEcouponsMQServicesImpl(){	
	}
	
    public GetCliensecResponse getCliensec(String reqXML) throws Exception {
		
		Ecup_GetCliensec gcs = new Ecup_GetCliensec();
		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = gcs.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución : [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(GetCliensec.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        GetCliensec result = (GetCliensec) unmarshaller.unmarshal(new StringReader(sh.getValue()));
        
        //Retorno un Wrapper que incluye el resultCode
        GetCliensecResponse response = new GetCliensecResponse();
        response.resultCode = resultCode;
        response.response = result;
        
		return response;
	}
	
	public GetPolAusResponse getPolAus(String reqXML) throws Exception {
		
		ECup_GetPolAUS gpa = new ECup_GetPolAUS();
		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = gpa.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución : [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(GetPolAus.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        GetPolAus result = (GetPolAus) unmarshaller.unmarshal(new StringReader(sh.getValue()));
        
        //Retorno un Wrapper que incluye el resultCode
        GetPolAusResponse response = new GetPolAusResponse();
        response.resultCode = resultCode;
        response.response = result;
        
		return response;
	}
	
	public GetPolHomResponse getPolHom(String reqXML) throws Exception {
		 
		Ecup_GetPolHOM gph = new Ecup_GetPolHOM();
		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = gph.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución : [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(GetPolHom.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        GetPolHom result = (GetPolHom) unmarshaller.unmarshal(new StringReader(sh.getValue()));
        
        //Retorno un Wrapper que incluye el resultCode
        GetPolHomResponse response = new GetPolHomResponse();
        response.resultCode = resultCode;
        response.response = result;
        
		return response;
	}
	
	
	public GetCupImpResponse getCupImp(String reqXML) throws Exception {
		 
		Ecup_GetCupImp gci = new Ecup_GetCupImp();
		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = gci.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución : [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(GetCupImp.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        GetCupImp result = (GetCupImp) unmarshaller.unmarshal(new StringReader(sh.getValue()));
        
        //Retorno un Wrapper que incluye el resultCode
        GetCupImpResponse response = new GetCupImpResponse();
        response.resultCode = resultCode;
        response.response = result;
        
		return response;
	}
	
	
	public GetUnlockAusResponse getUnlockAus(String reqXML) throws Exception {
		
		ECup_GetUnlockAUS gua = new ECup_GetUnlockAUS();
		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = gua.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución : [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(GetUnlockAus.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        GetUnlockAus result = (GetUnlockAus) unmarshaller.unmarshal(new StringReader(sh.getValue()));
        
        //Retorno un Wrapper que incluye el resultCode
        GetUnlockAusResponse response = new GetUnlockAusResponse();
        response.resultCode = resultCode;
        response.response = result;
        
		return response;
	}
	
    public GetUnlockHomResponse getUnlockHom(String reqXML) throws Exception {
		
    	Ecup_GetUnlockHOM guh = new Ecup_GetUnlockHOM();
		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = guh.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución : [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(GetUnlockHom.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        GetUnlockHom result = (GetUnlockHom) unmarshaller.unmarshal(new StringReader(sh.getValue()));
        
        //Retorno un Wrapper que incluye el resultCode
        GetUnlockHomResponse response = new GetUnlockHomResponse();
        response.resultCode = resultCode;
        response.response = result;
        
		return response;
	}
	
	
	
}
