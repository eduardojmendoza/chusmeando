package com.qbe.services.nov.impl;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.qbe.services.jaxb.AnyXmlElement;
import com.qbe.services.mqgeneric.impl.LBAW_MQMensaje;
import com.qbe.services.mqgeneric.impl.LBAW_MQMensajeGestion;
import com.qbe.services.nov.NOVServiceResponse;
import com.qbe.services.nov.GenericServiceResponse;
import com.qbe.services.nov.jaxb.mqmensajegestion.actividadocupacion.ActividadOcupacion;
import com.qbe.services.nov.jaxb.mqmensajegestion.actividadocupacion.ActividadOcupacionResponse;
import com.qbe.services.nov.jaxb.mqmensajegestion.perfilusuario.PerfilUsuario;
import com.qbe.services.nov.jaxb.mqmensajegestion.perfilusuario.PerfilUsuarioResponse;
import com.qbe.vbcompat.string.StringHolder;

/**
 * Implementación del servicio
 * 
 * @author ramiro
 * @author cesar
 */
public class ConsultaMQGestionServicesImpl {

	private static final String EMPTY_STRING = "";

	protected Logger logger = Logger.getLogger(getClass().getName());
	
	public ConsultaMQGestionServicesImpl() {
	}

	private LBAW_MQMensajeGestion getMQMensajeGestion() {
		LBAW_MQMensajeGestion mqmg = new LBAW_MQMensajeGestion();
		return mqmg;
	}

	public PerfilUsuarioResponse perfilUsuario_1000(
			String aplicarXSL,
			String usuario) throws Exception {
		
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();
		
		StringHolder sh = new StringHolder();
		String reqXML = createPerfilUsuario_1000RequestString(aplicarXSL, usuario);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = mqmg.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de PerfilUsuario_1000: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		JAXBContext jc = JAXBContext.newInstance(PerfilUsuario.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        PerfilUsuario pu = (PerfilUsuario) unmarshaller.unmarshal(new StringReader(sh.getValue()));
        
        //Retorno un Wrapper que incluye el resultCode
        PerfilUsuarioResponse pur = new PerfilUsuarioResponse();
        pur.resultCode = resultCode;
        pur.response = pu;
        
		return pur;
	}

	private String createPerfilUsuario_1000RequestString(String aplicarXSL, String usuario) {
		return "<Request>"
				+ "<DEFINICION>1000_PerfilUsuario.xml</DEFINICION>"
				+ "<AplicarXSL>" + aplicarXSL + "</AplicarXSL>"
				+ "<USUARIO>" + usuario + "</USUARIO>"
				+ "</Request>";
	}
			
	
	public GenericServiceResponse getActividadOcupacion(
			String buscar) throws Exception {
		
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();
		
		StringHolder sh = new StringHolder();
		String reqXML = getActividadOcupacionRequestString(buscar);
		logger.log(Level.FINE, "Recibido request: " + reqXML);
		int resultCode = mqmg.IAction_Execute(reqXML, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getActividadOcupacion: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	private String getActividadOcupacionRequestString(String buscar) {
		return "<Request>"
				+ "<DEFINICION>getActividadOcupacion.xml</DEFINICION>"
				+ "<BUSCAR>" + buscar + "</BUSCAR>"
				+ "</Request>";
	}
	
	
	
	public GenericServiceResponse getHistorialPoliza(String request) throws Exception {
		
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();
		
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getHistorialPoliza: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse datoPolizaRiesgo(String request) throws Exception {
			LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
			StringHolder sh = new StringHolder();
			logger.log(Level.FINE, "Recibido request: " + request);
			int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
			logger.log(Level.FINE, "Resultado de ejecución de datoPolizaRiesgo: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
			if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
			//SH tiene un XML como String
			//Transformo la respuesta en una JAXB
			return new GenericServiceResponse(resultCode,sh.getValue());
		}
	
	
	
	public GenericServiceResponse domiciliodeCorrespondencia(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de domiciliodeCorrespondencia: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse coberturas(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de coberturas: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse mediosdePago(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de mediosdePago: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse datosApoderadoAcreedor(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de datosApoderadoAcreedor: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		//SH tiene un XML como String
		//Transformo la respuesta en una JAXB
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse datosGeneralesPolizaHogar(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de datosGeneralesPolizaHogar: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse pendientesTotales(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de pendientesTotales: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse opRet(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de opRet: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	public GenericServiceResponse emitidasTotales(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de emitidasTotales: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	public GenericServiceResponse aviVenPolTotales(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de aviVenPolTotales: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	
	public GenericServiceResponse getDeudaCobradaCanalesDetalle(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de GetDeudaCobradaCanalesDetalle: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse getDeudaCobradaCanales(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getDeudaCobradaCanales: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	

	public GenericServiceResponse getDeudaCobradaTotales(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getDeudaCobradaTotales: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	public GenericServiceResponse workflows(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de workflows: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	public GenericServiceResponse getRecibosNotasDeCredito(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getRecibosNotasDeCredito: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	
	public GenericServiceResponse getCuponesPendientesDetalle(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getCuponesPendientesDetalle: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	public GenericServiceResponse getCuponesPendientes(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de getCuponesPendientes: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	public GenericServiceResponse productoresHabilitadosListOp(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de productoresHabilitadosListOp: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	public GenericServiceResponse mediosdepagoxcia(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de mediosdepagoxcia: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse transfiereArchivoInterfases(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de TransfiereArchivoInterfases: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse consultaDeLote(String request)  throws Exception{
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de consultadeLote: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception 
	 */
	public GenericServiceResponse consultaOperacionesPorLote(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de consultaOperacionesPorLote: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	public GenericServiceResponse nombreMailBox(String request) throws Exception{
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de nombreMailBox: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse archivoDeRetorno(String request) throws Exception{
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de archivoDeRetorno: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse consultadeSolicitudesRechazadas(String request) throws Exception{
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de consultadeSolicitudesRechazadas: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse estadoArchivo(String request) throws Exception{
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de estadoArchivo: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse altaEndoso(String request) throws Exception{
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de altaEndoso: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public GenericServiceResponse consultaSolicitudes(String request) throws Exception{
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de consultaSolicitudes: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception 
	 */
	public GenericServiceResponse busquedaConstanciasPago(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de busquedaConstanciasPago: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}

	public GenericServiceResponse getPolizasEndosables(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de busquedaConstanciasPago: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception 
	 */
	public GenericServiceResponse cuponesConstanciasPago(String request) throws Exception {
		LBAW_MQMensajeGestion mqmg = getMQMensajeGestion();			
		StringHolder sh = new StringHolder();
		logger.log(Level.FINE, "Recibido request: " + request);
		int resultCode = mqmg.IAction_Execute(request, sh, EMPTY_STRING);
		logger.log(Level.FINE, "Resultado de ejecución de cuponesConstanciasPago: [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
		if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
		return new GenericServiceResponse(resultCode,sh.getValue());
	}
	
}
