package com.qbe.services.nov.rest;

import java.io.StringReader;
import java.io.StringWriter;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.persistence.jaxb.MarshallerProperties;

import com.qbe.services.nov.ServiceResponse;

public class NovRestServicesUtil {

	public static String marshalAsXml(Class rootClass, Object root) throws JAXBException, PropertyException {
		JAXBContext jc = JAXBContext.newInstance(rootClass);
	    Marshaller marshaller = jc.createMarshaller();
	    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_XML);
	    StringWriter sw = new StringWriter();
	    marshaller.marshal(root, sw);
		return sw.toString();
	}

	public static String marshalAsJson(Class rootClass, Object root) throws JAXBException, PropertyException {
		return marshalAsJson(rootClass, root, false);
	}

	public static String marshalAsJson(Class rootClass, Object root, boolean includeRoot) throws JAXBException, PropertyException {
		JAXBContext jc = JAXBContext.newInstance(rootClass);
	    Marshaller marshaller = jc.createMarshaller();
	    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
	    marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, includeRoot);
	    marshaller.setProperty(MarshallerProperties.JSON_MARSHAL_EMPTY_COLLECTIONS, true);
	    marshaller.setProperty(MarshallerProperties.JSON_REDUCE_ANY_ARRAYS, false);
	    marshaller.setProperty(MarshallerProperties.JSON_WRAPPER_AS_ARRAY_NAME, true);
	    marshaller.setProperty(MarshallerProperties.JSON_VALUE_WRAPPER, "$");
	    //- See more at: http://www.eclipse.org/eclipselink/documentation/2.5/moxy/json003.htm#sthash.YpVudEtO.dpuf
	    	
	    StringWriter sw = new StringWriter();
	    marshaller.marshal(root, sw);
		return sw.toString();
	}

	/**
	 * TODO Si hay problemas ver la implementación en https://wiki.eclipse.org/EclipseLink/DesignDocs/405161
	 * 
	 * NOTA Con moxy 2.5.1 da error, por ejemplo en el test de GetCotiAUSRequest unmarshalea bien ACCESORIOS pero no levanta ningún HIJO
	 * 
	 * @param rootClass
	 * @param json
	 * @return
	 * @throws JAXBException
	 * @throws PropertyException
	 */
	public static <T> Object unmarshalFromJson(Class<T> rootClass, String json) throws JAXBException, PropertyException {
		return unmarshalFromJson(rootClass, json, false);
	}
	
	
	public static <T> Object unmarshalFromJson(Class<T> rootClass, String json, boolean includeRoot) throws JAXBException, PropertyException {
		JAXBContext jc = JAXBContext.newInstance(rootClass);
	    Unmarshaller unmarshaller  = jc.createUnmarshaller();
	    unmarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
	    unmarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, includeRoot);
	    unmarshaller.setProperty(MarshallerProperties.JSON_WRAPPER_AS_ARRAY_NAME, true);
	    unmarshaller.setProperty(MarshallerProperties.JSON_VALUE_WRAPPER, "$");
	    //- See more at: http://www.eclipse.org/eclipselink/documentation/2.5/moxy/json003.htm#sthash.YpVudEtO.dpuf
	    	
	    JAXBElement<T> jaxbWrapper = unmarshaller.unmarshal(new StreamSource(new StringReader(json)), rootClass );
		return jaxbWrapper.getValue();
	}
	
	public static Object unmarshalFromXml(Class rootClass, String xml) throws JAXBException, PropertyException {
		JAXBContext jc = JAXBContext.newInstance(rootClass);
	    Unmarshaller unmarshaller  = jc.createUnmarshaller();
	    unmarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_XML);
	    Object o = unmarshaller.unmarshal(new StringReader(xml));
		return o;
	}

	
	public static Response.Status getResponseStatus(ServiceResponse result) {
		Response.Status status = Response.Status.OK;
	    
		if ( !result.getResponseWithEstado().getEstado().resultado ) {
			status = Response.Status.INTERNAL_SERVER_ERROR;
		}

		switch (result.getResultCode()) {

		case 0: //OK, queda lo que esté
			break;
		case 1: //
			status = Response.Status.INTERNAL_SERVER_ERROR;
			break;
		case 3: //
			status = Response.Status.SERVICE_UNAVAILABLE;
			break;
		default:
			break;
		}
		
		return status;
	}
	
	public static Response.Status mapResultCodeToStatus(int resultCode) {
		Response.Status status = Response.Status.OK;

		switch (resultCode) {

		case 0: // OK
			break;
		case 1: //
			status = Response.Status.INTERNAL_SERVER_ERROR;
			break;
		case 3: //
			status = Response.Status.SERVICE_UNAVAILABLE;
			break;
		default:
			break;
		}
		return status;
	}
	
}
