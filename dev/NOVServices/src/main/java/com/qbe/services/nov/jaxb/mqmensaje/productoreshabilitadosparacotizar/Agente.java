package com.qbe.services.nov.jaxb.mqmensaje.productoreshabilitadosparacotizar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * 
 * 
<ROOT>
   <AGENTE AGENTCLA="PR" AGENTCOD="1670"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="6662"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="0188"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="2000"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="7082"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="8713"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="7916"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="7933"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="8078"/>
   <AGENTE AGENTCLA="PR" AGENTCOD="8165"/>
</ROOT>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Agente {
	
    @XmlAttribute(name="AGENTCLA")	
	public String agentcla;
	
    @XmlAttribute(name="AGENTCOD")	
	public String agentcod;

}
