package com.qbe.connector.mq.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.connector.mq.MQConnector;
import com.qbe.connector.mq.MQConnectorException;

/**
 * Implementa MQConnector a partir de archivos que definen la respuesta para un request específico.
 * Carga un map al inicializarse en el que tiene pares de request-response, y cada send responde 
 * con el response que corresponda o null si no tiene respuesta predefinida para ese request
 * 
 * @author ramiro
 *
 */
public class MQConnectorMock implements MQConnector {

	private static Logger logger = Logger.getLogger(MQConnectorMock.class.getName());
	
	/**
	 * Mapa de request->response
	 */
	private Map<String, String> messageMap;

	/**
	 * Parámetro no usado por el conector, guarda el valor que se le asigne
	 */
	private long receiveTimeout;

	/**
	 * Parámetro no usado por el conector, guarda el valor que se le asigne
	 */
	private String queueManagerName;

	/**
	 * Parámetro no usado por el conector, guarda el valor que se le asigne
	 */
	private String putQueueName;

	/**
	 * Parámetro no usado por el conector, guarda el valor que se le asigne
	 */
	private String getQueueName;
	
	public MQConnectorMock() {
		if ( messageMap == null) initializeMessageMap();
	}
	
	private void initializeMessageMap() {
		 messageMap = new HashMap<String, String>();
		 init();
	}

	/**
	 * Carga los mensajes desde archivos
	 * 
	 * FALTA: hacer que levante todos los que encuentre en un directorio sin tener que especificarle 1x1...
	 */
	public void init()  {
		try {
			String msg0004AreaIn = loadFile("MSG_0004/area_in.txt");
			String msg0004AreaOut = loadFile("MSG_0004/area_out.txt");
			this.saveAreaPair(msg0004AreaIn, msg0004AreaOut);
			
			String msg0033AreaIn = loadFile("MSG_0033/area_in.txt");
			String msg0033AreaOut = loadFile("MSG_0033/area_out.txt");
			this.saveAreaPair(msg0033AreaIn, msg0033AreaOut);
			
			String msg0037AreaIn = loadFile("MSG_0037/area_in.txt");
			String msg0037AreaOut = loadFile("MSG_0037/area_out.txt");
			this.saveAreaPair(msg0037AreaIn, msg0037AreaOut);
			
			String msg0047AreaIn = loadFile("MSG_0047/area_in.txt");
			String msg0047AreaOut = loadFile("MSG_0047/area_out.txt");
			this.saveAreaPair(msg0047AreaIn, msg0047AreaOut);
			
			String msg0059AreaIn = loadFile("MSG_0059/area_in.txt");
			String msg0059AreaOut = loadFile("MSG_0059/area_out.txt");
			this.saveAreaPair(msg0059AreaIn, msg0059AreaOut);
			
			String msg0059_1AreaIn = loadFile("MSG_0059_1/area_in.txt");
			String msg0059_1AreaOut = loadFile("MSG_0059_1/area_out.txt");
			this.saveAreaPair(msg0059_1AreaIn, msg0059_1AreaOut);
			
			String msg1000AreaIn = loadFile("MSG_1000/area_in.txt");
			String msg1000AreaOut = loadFile("MSG_1000/area_out.txt");
			this.saveAreaPair(msg1000AreaIn, msg1000AreaOut);

			String msg1651AreaIn = loadFile("MSG_1651/area_in.txt");
			String msg1651AreaOut = loadFile("MSG_1651/area_out.txt");
			this.saveAreaPair(msg1651AreaIn, msg1651AreaOut);

			String msg1650AreaIn = loadFile("MSG_1650/area_in.txt");
			String msg1650AreaOut = loadFile("MSG_1650/area_out.txt");
			this.saveAreaPair(msg1650AreaIn, msg1650AreaOut);

			String msg1554AreaIn = loadFile("MSG_1554/area_in.txt");
			String msg1554AreaOut = loadFile("MSG_1554/area_out.txt");
			this.saveAreaPair(msg1554AreaIn, msg1554AreaOut);

			String msg1659AreaIn = loadFile("MSG_1659/area_in.txt");
			String msg1659AreaOut = loadFile("MSG_1659/area_out.txt");
			this.saveAreaPair(msg1659AreaIn, msg1659AreaOut);
			
			String msg1660AreaIn = loadFile("MSG_1660/area_in.txt");
			String msg1660AreaOut = loadFile("MSG_1660/area_out.txt");
			this.saveAreaPair(msg1660AreaIn, msg1660AreaOut);

			String msg1018AreaIn = loadFile("MSG_1018/area_in.txt");
			String msg1018AreaOut = loadFile("MSG_1018/area_out.txt");
			this.saveAreaPair(msg1018AreaIn, msg1018AreaOut);

			String msg1027AreaIn = loadFile("MSG_1027/area_in.txt");
			String msg1027AreaOut = loadFile("MSG_1027/area_out.txt");
			this.saveAreaPair(msg1027AreaIn, msg1027AreaOut);


			String msg1102AreaIn = loadFile("MSG_1102/area_in.txt");
			String msg1102AreaOut = loadFile("MSG_1102/area_out.txt");
			this.saveAreaPair(msg1102AreaIn, msg1102AreaOut);
			
			String msg1103AreaIn = loadFile("MSG_1103/area_in.txt");
			String msg1103AreaOut = loadFile("MSG_1103/area_out.txt");
			this.saveAreaPair(msg1103AreaIn, msg1103AreaOut);
			
			String msg1104AreaIn = loadFile("MSG_1104/area_in.txt");
			String msg1104AreaOut = loadFile("MSG_1104/area_out.txt");
			this.saveAreaPair(msg1104AreaIn, msg1104AreaOut);
			
			String msg1107AreaIn = loadFile("MSG_1107/area_in.txt");
			String msg1107AreaOut = loadFile("MSG_1107/area_out.txt");
			this.saveAreaPair(msg1107AreaIn, msg1107AreaOut);
			
			String msg1109AreaIn = loadFile("MSG_1109/area_in.txt");
			String msg1109AreaOut = loadFile("MSG_1109/area_out.txt");
			this.saveAreaPair(msg1109AreaIn, msg1109AreaOut);
			
			String msg1112AreaIn = loadFile("MSG_1112/area_in.txt");
			String msg1112AreaOut = loadFile("MSG_1112/area_out.txt");
			this.saveAreaPair(msg1112AreaIn, msg1112AreaOut);
			
			String msg1116AreaIn = loadFile("MSG_1116/area_in.txt");
			String msg1116AreaOut = loadFile("MSG_1116/area_out.txt");
			this.saveAreaPair(msg1116AreaIn, msg1116AreaOut);
			
			String msg1117AreaIn = loadFile("MSG_1117/area_in.txt");
			String msg1117AreaOut = loadFile("MSG_1117/area_out.txt");
			this.saveAreaPair(msg1117AreaIn, msg1117AreaOut);
			
			String msg1118AreaIn = loadFile("MSG_1118/area_in.txt");
			String msg1118AreaOut = loadFile("MSG_1118/area_out.txt");
			this.saveAreaPair(msg1118AreaIn, msg1118AreaOut);
			
			String msg1119AreaIn = loadFile("MSG_1119/area_in.txt");
			String msg1119AreaOut = loadFile("MSG_1119/area_out.txt");
			this.saveAreaPair(msg1119AreaIn, msg1119AreaOut);
			
			String msg1120AreaIn = loadFile("MSG_1120/area_in.txt");
			String msg1120AreaOut = loadFile("MSG_1120/area_out.txt");
			this.saveAreaPair(msg1120AreaIn, msg1120AreaOut);
			
			String msg1213AreaIn = loadFile("MSG_1213/area_in.txt");
			String msg1213AreaOut = loadFile("MSG_1213/area_out.txt");
			this.saveAreaPair(msg1213AreaIn, msg1213AreaOut);
			
			String msg1410AreaIn = loadFile("MSG_1410/area_in.txt");
			String msg1410AreaOut = loadFile("MSG_1410/area_out.txt");
			this.saveAreaPair(msg1410AreaIn, msg1410AreaOut);
			
			String msg1425AreaIn = loadFile("MSG_1425/area_in.txt");
			String msg1425AreaOut = loadFile("MSG_1425/area_out.txt");
			this.saveAreaPair(msg1425AreaIn, msg1425AreaOut);
			
			String msg1426AreaIn = loadFile("MSG_1426/area_in.txt");
			String msg1426AreaOut = loadFile("MSG_1426/area_out.txt");
			this.saveAreaPair(msg1426AreaIn, msg1426AreaOut);
			
			String msg1540AreaIn = loadFile("MSG_1540/area_in.txt");
			String msg1540AreaOut = loadFile("MSG_1540/area_out.txt");
			this.saveAreaPair(msg1540AreaIn, msg1540AreaOut);
			
			String msg1427AreaIn = loadFile("MSG_1427/area_in.txt");
			String msg1427AreaOut = loadFile("MSG_1427/area_out.txt");
			this.saveAreaPair(msg1427AreaIn, msg1427AreaOut);
			
			String msg1428AreaIn = loadFile("MSG_1428/area_in.txt");
			String msg1428AreaOut = loadFile("MSG_1428/area_out.txt");
			this.saveAreaPair(msg1428AreaIn, msg1428AreaOut);
			
			String msg1430AreaIn = loadFile("MSG_1430/area_in.txt");
			String msg1430AreaOut = loadFile("MSG_1430/area_out.txt");
			this.saveAreaPair(msg1430AreaIn, msg1430AreaOut);
			
			String msg1603AreaIn = loadFile("MSG_1603/area_in.txt");
			String msg1603AreaOut = loadFile("MSG_1603/area_out.txt");
			this.saveAreaPair(msg1603AreaIn, msg1603AreaOut);
			
			String msg1605AreaIn = loadFile("MSG_1605/area_in.txt");
			String msg1605AreaOut = loadFile("MSG_1605/area_out.txt");
			this.saveAreaPair(msg1605AreaIn, msg1605AreaOut);
			
			String msg1606AreaIn = loadFile("MSG_1606/area_in.txt");
			String msg1606AreaOut = loadFile("MSG_1606/area_out.txt");
			this.saveAreaPair(msg1606AreaIn, msg1606AreaOut);
			
			String msg1607AreaIn = loadFile("MSG_1607/area_in.txt");
			String msg1607AreaOut = loadFile("MSG_1607/area_out.txt");
			this.saveAreaPair(msg1607AreaIn, msg1607AreaOut);
			
			String msg1510AreaIn = loadFile("MSG_1510/area_in.txt");
			String msg1510AreaOut = loadFile("MSG_1510/area_out.txt");
			this.saveAreaPair(msg1510AreaIn, msg1510AreaOut);
			
			String msg1535AreaIn = loadFile("MSG_1535/area_in.txt");
			String msg1535AreaOut = loadFile("MSG_1535/area_out.txt");
			this.saveAreaPair(msg1535AreaIn, msg1535AreaOut);
			
			String msg1522AreaIn = loadFile("MSG_1522/area_in.txt");
			String msg1522AreaOut = loadFile("MSG_1522/area_out.txt");
			this.saveAreaPair(msg1522AreaIn, msg1522AreaOut);
			
			String msg1525AreaIn = loadFile("MSG_1525/area_in.txt");
			String msg1525AreaOut = loadFile("MSG_1525/area_out.txt");
			this.saveAreaPair(msg1525AreaIn, msg1525AreaOut);
			
			String msg1526AreaIn = loadFile("MSG_1526/area_in.txt");
			String msg1526AreaOut = loadFile("MSG_1526/area_out.txt");
			this.saveAreaPair(msg1526AreaIn, msg1526AreaOut);
			
			String msg1542AreaIn = loadFile("MSG_1542/area_in.txt");
			String msg1542AreaOut = loadFile("MSG_1542/area_out.txt");
			this.saveAreaPair(msg1542AreaIn, msg1542AreaOut);
			
			String msg1553AreaIn = loadFile("MSG_1553/area_in.txt");
			String msg1553AreaOut = loadFile("MSG_1553/area_out.txt");
			this.saveAreaPair(msg1553AreaIn, msg1553AreaOut);
			
			String msg1653AreaIn = loadFile("MSG_1653/area_in.txt");
			String msg1653AreaOut = loadFile("MSG_1653/area_out.txt");
			this.saveAreaPair(msg1653AreaIn, msg1653AreaOut);
			
			String msg1215AreaIn = loadFile("MSG_1215/area_in.txt");
			String msg1215AreaOut = loadFile("MSG_1215/area_out.txt");
			this.saveAreaPair(msg1215AreaIn, msg1215AreaOut);
			
			String msg1101AreaIn = loadFile("MSG_1101/area_in.txt");
			String msg1101AreaOut = loadFile("MSG_1101/area_out.txt");
			this.saveAreaPair(msg1101AreaIn, msg1101AreaOut);
			
			String msg1114AreaIn = loadFile("MSG_1114/area_in.txt");
			String msg1114AreaOut = loadFile("MSG_1114/area_out.txt");
			this.saveAreaPair(msg1114AreaIn, msg1114AreaOut);
			
			String msg1123AreaIn = loadFile("MSG_1123/area_in.txt");
			String msg1123AreaOut = loadFile("MSG_1123/area_out.txt");
			this.saveAreaPair(msg1123AreaIn, msg1123AreaOut);
			
			String msg1123GenAreaIn = loadFile("MSG_1123Gen/area_in.txt");
			String msg1123GenAreaOut = loadFile("MSG_1123Gen/area_out.txt");
			this.saveAreaPair(msg1123GenAreaIn, msg1123GenAreaOut);
			
			String msg1202AreaIn = loadFile("MSG_1202/area_in.txt");
			String msg1202AreaOut = loadFile("MSG_1202/area_out.txt");
			this.saveAreaPair(msg1202AreaIn, msg1202AreaOut);
			
			String msg1202_2AreaIn = loadFile("MSG_1202_2/area_in.txt");
			String msg1202_2AreaOut = loadFile("MSG_1202_2/area_out.txt");
			this.saveAreaPair(msg1202_2AreaIn, msg1202_2AreaOut);

			String msg1901AreaIn = loadFile("MSG_1901/area_in.txt");
			String msg1901AreaOut = loadFile("MSG_1901/area_out.txt");
			this.saveAreaPair(msg1901AreaIn, msg1901AreaOut);

			String msg1701AreaIn = loadFile("MSG_1701/area_in.txt");
			String msg1701AreaOut = loadFile("MSG_1701/area_out.txt");
			this.saveAreaPair(msg1701AreaIn, msg1701AreaOut);
			
			String msg1543AreaIn = loadFile("MSG_1543/area_in.txt");
			String msg1543AreaOut = loadFile("MSG_1543/area_out.txt");
			this.saveAreaPair(msg1543AreaIn, msg1543AreaOut);
			
			String msg1549AreaIn = loadFile("MSG_1549/area_in.txt");
			String msg1549AreaOut = loadFile("MSG_1549/area_out.txt");
			this.saveAreaPair(msg1549AreaIn, msg1549AreaOut);
			
			String msg1552AreaIn = loadFile("MSG_1552/area_in.txt");
			String msg1552AreaOut = loadFile("MSG_1552/area_out.txt");
			this.saveAreaPair(msg1552AreaIn, msg1552AreaOut);
			
			String msg1581AreaIn = loadFile("MSG_1581/area_in.txt");
			String msg1581AreaOut = loadFile("MSG_1581/area_out.txt");
			this.saveAreaPair(msg1581AreaIn, msg1581AreaOut);
			
			String msg1582AreaIn = loadFile("MSG_1582/area_in.txt");
			String msg1582AreaOut = loadFile("MSG_1582/area_out.txt");
			this.saveAreaPair(msg1582AreaIn, msg1582AreaOut);
			
			String msg1583AreaIn = loadFile("MSG_1583/area_in.txt");
			String msg1583AreaOut = loadFile("MSG_1583/area_out.txt");
			this.saveAreaPair(msg1583AreaIn, msg1583AreaOut);
			
			String msg1184AreaIn = loadFile("MSG_1184/area_in.txt");
			String msg1184AreaOut = loadFile("MSG_1184/area_out.txt");
			this.saveAreaPair(msg1184AreaIn, msg1184AreaOut);
			
			String msg1185AreaIn = loadFile("MSG_1185/area_in.txt");
			String msg1185AreaOut = loadFile("MSG_1185/area_out.txt");
			this.saveAreaPair(msg1185AreaIn, msg1185AreaOut);
			
			//La 1187 tiene TR, así que cargo 2 mensajes. El 2do es inventado ( es el OUT del 1ro con OK en vez de TR ). Pero para probar sirve
			String msg1187AreaIn = loadFile("MSG_1187/area_in.txt");
			String msg1187AreaOut = loadFile("MSG_1187/area_out.txt");
			this.saveAreaPair(msg1187AreaIn, msg1187AreaOut);
			
			String msg1187_2AreaIn = loadFile("MSG_1187_2/area_in.txt");
			String msg1187_2AreaOut = loadFile("MSG_1187_2/area_out.txt");
			this.saveAreaPair(msg1187_2AreaIn, msg1187_2AreaOut);

			String msg1188AreaIn = loadFile("MSG_1188/area_in.txt");
			String msg1188AreaOut = loadFile("MSG_1188/area_out.txt");
			this.saveAreaPair(msg1188AreaIn, msg1188AreaOut);
			
			String msg1301AreaIn = loadFile("MSG_1301/area_in.txt");
			String msg1301AreaOut = loadFile("MSG_1301/area_out.txt");
			this.saveAreaPair(msg1301AreaIn, msg1301AreaOut);
			
			String msg1302AreaIn = loadFile("MSG_1302/area_in.txt");
			String msg1302AreaOut = loadFile("MSG_1302/area_out.txt");
			this.saveAreaPair(msg1302AreaIn, msg1302AreaOut);
			
			String msg1304AreaIn = loadFile("MSG_1304/area_in.txt");
			String msg1304AreaOut = loadFile("MSG_1304/area_out.txt");
			this.saveAreaPair(msg1304AreaIn, msg1304AreaOut);
			
			String msg1305AreaIn = loadFile("MSG_1305/area_in.txt");
			String msg1305AreaOut = loadFile("MSG_1305/area_out.txt");
			this.saveAreaPair(msg1305AreaIn, msg1305AreaOut);
			
			String msg1306AreaIn = loadFile("MSG_1306/area_in.txt");
			String msg1306AreaOut = loadFile("MSG_1306/area_out.txt");
			this.saveAreaPair(msg1306AreaIn, msg1306AreaOut);
			
			String msg1307AreaIn = loadFile("MSG_1307/area_in.txt");
			String msg1307AreaOut = loadFile("MSG_1307/area_out.txt");
			this.saveAreaPair(msg1307AreaIn, msg1307AreaOut);
			
			String msg1309AreaIn = loadFile("MSG_1309/area_in.txt");
			String msg1309AreaOut = loadFile("MSG_1309/area_out.txt");
			this.saveAreaPair(msg1309AreaIn, msg1309AreaOut);
			
			String msg1315AreaIn = loadFile("MSG_1315/area_in.txt");
			String msg1315AreaOut = loadFile("MSG_1315/area_out.txt");
			this.saveAreaPair(msg1315AreaIn, msg1315AreaOut);
			
			String msg1340AreaIn = loadFile("MSG_1340/area_in.txt");
			String msg1340AreaOut = loadFile("MSG_1340/area_out.txt");
			this.saveAreaPair(msg1340AreaIn, msg1340AreaOut);
			
			String msg1346AreaIn = loadFile("MSG_1346/area_in.txt");
			String msg1346AreaOut = loadFile("MSG_1346/area_out.txt");
			this.saveAreaPair(msg1346AreaIn, msg1346AreaOut);
			
			String msg1348AreaIn = loadFile("MSG_1348/area_in.txt");
			String msg1348AreaOut = loadFile("MSG_1348/area_out.txt");
			this.saveAreaPair(msg1348AreaIn, msg1348AreaOut);
			
			String msg1361AreaIn = loadFile("MSG_1361/area_in.txt");
			String msg1361AreaOut = loadFile("MSG_1361/area_out.txt");
			this.saveAreaPair(msg1361AreaIn, msg1361AreaOut);

			String msg1341AreaIn = loadFile("MSG_1341/area_in.txt");
			String msg1341AreaOut = loadFile("MSG_1341/area_out.txt");
			this.saveAreaPair(msg1341AreaIn, msg1341AreaOut);
			
			String msg1347AreaIn = loadFile("MSG_1347/area_in.txt");
			String msg1347AreaOut = loadFile("MSG_1347/area_out.txt");
			this.saveAreaPair(msg1347AreaIn, msg1347AreaOut);
			
			String msg1584AreaIn = loadFile("MSG_1584/area_in.txt");
			String msg1584AreaOut = loadFile("MSG_1584/area_out.txt");
			this.saveAreaPair(msg1584AreaIn, msg1584AreaOut);

			String msgTest1In = loadFile("MSG_TEST_1/area_in.txt");
			String msgTest1Out = loadFile("MSG_TEST_1/area_out.txt");
			this.saveAreaPair(msgTest1In, msgTest1Out);

			String msgTest2In = loadFile("MSG_TEST_2/area_in.txt");
			String msgTest2Out = loadFile("MSG_TEST_2/area_out.txt");
			this.saveAreaPair(msgTest2In, msgTest2Out);
			
			String msg1101UsuAreaIn = loadFile("MSG_1101Usu/area_in.txt");
			String msg1101UsuAreaOut = loadFile("MSG_1101Usu/area_out.txt");
			this.saveAreaPair(msg1101UsuAreaIn, msg1101UsuAreaOut);
			
			String msg1400AreaIn = loadFile("MSG_1400/area_in.txt");
			String msg1400AreaOut = loadFile("MSG_1400/area_out.txt");
			this.saveAreaPair(msg1400AreaIn, msg1400AreaOut);
			
			String msg1401AreaIn = loadFile("MSG_1401/area_in.txt");
			String msg1401AreaOut = loadFile("MSG_1401/area_out.txt");
			this.saveAreaPair(msg1401AreaIn, msg1401AreaOut);
			
			String msg1402AreaIn = loadFile("MSG_1402/area_in.txt");
			String msg1402AreaOut = loadFile("MSG_1402/area_out.txt");
			this.saveAreaPair(msg1402AreaIn, msg1402AreaOut);
			
			String msg1406AreaIn = loadFile("MSG_1406/area_in.txt");
			String msg1406AreaOut = loadFile("MSG_1406/area_out.txt");
			this.saveAreaPair(msg1406AreaIn, msg1406AreaOut);
			
			String msg1407AreaIn = loadFile("MSG_1407/area_in.txt");
			String msg1407AreaOut = loadFile("MSG_1407/area_out.txt");
			this.saveAreaPair(msg1407AreaIn, msg1407AreaOut);
			
			String msg1408AreaIn = loadFile("MSG_1408/area_in.txt");
			String msg1408AreaOut = loadFile("MSG_1408/area_out.txt");
			this.saveAreaPair(msg1408AreaIn, msg1408AreaOut);
			
			String msg1411AreaIn = loadFile("MSG_1411/area_in.txt");
			String msg1411AreaOut = loadFile("MSG_1411/area_out.txt");
			this.saveAreaPair(msg1411AreaIn, msg1411AreaOut);
			
			String msg1412AreaIn = loadFile("MSG_1412/area_in.txt");
			String msg1412AreaOut = loadFile("MSG_1412/area_out.txt");
			this.saveAreaPair(msg1412AreaIn, msg1412AreaOut);
			
			String msg1418AreaIn = loadFile("MSG_1418/area_in.txt");
			String msg1418AreaOut = loadFile("MSG_1418/area_out.txt");
			this.saveAreaPair(msg1418AreaIn, msg1418AreaOut);

			String msg1413AreaIn = loadFile("MSG_1413/area_in.txt");
			String msg1413AreaOut = loadFile("MSG_1413/area_out.txt");
			this.saveAreaPair(msg1413AreaIn, msg1413AreaOut);

			String msg1803AreaIn = loadFile("MSG_1803/area_in.txt");
			String msg1803AreaOut = loadFile("MSG_1803/area_out.txt");
			this.saveAreaPair(msg1803AreaIn, msg1803AreaOut);
			
			String msg1415AreaIn = loadFile("MSG_1415/area_in.txt");
			String msg1415AreaOut = loadFile("MSG_1415/area_out.txt");
			this.saveAreaPair(msg1415AreaIn, msg1415AreaOut);
			
			String msg1416AreaIn = loadFile("MSG_1416/area_in.txt");
			String msg1416AreaOut = loadFile("MSG_1416/area_out.txt");
			this.saveAreaPair(msg1416AreaIn, msg1416AreaOut);
			
			String msg1417AreaIn = loadFile("MSG_1417/area_in.txt");
			String msg1417AreaOut = loadFile("MSG_1417/area_out.txt");
			this.saveAreaPair(msg1417AreaIn, msg1417AreaOut);
			
			String msg1420AreaIn = loadFile("MSG_1420/area_in.txt");
			String msg1420AreaOut = loadFile("MSG_1420/area_out.txt");
			this.saveAreaPair(msg1420AreaIn, msg1420AreaOut);
			
			String msg1421AreaIn = loadFile("MSG_1421/area_in.txt");
			String msg1421AreaOut = loadFile("MSG_1421/area_out.txt");
			this.saveAreaPair(msg1421AreaIn, msg1421AreaOut);
			
			String msg1110AreaIn = loadFile("MSG_1110/area_in.txt");
			String msg1110AreaOut = loadFile("MSG_1110/area_out.txt");
			this.saveAreaPair(msg1110AreaIn, msg1110AreaOut);
			
			String msg1010AreaIn = loadFile("MSG_1010/area_in.txt");
			String msg1010AreaOut = loadFile("MSG_1010/area_out.txt");
			this.saveAreaPair(msg1010AreaIn, msg1010AreaOut);
			
			String msg1419AreaIn = loadFile("MSG_1419/area_in.txt");
			String msg1419AreaOut = loadFile("MSG_1419/area_out.txt");
			this.saveAreaPair(msg1419AreaIn, msg1419AreaOut);
			
			String msg1801AreaIn = loadFile("MSG_1801/area_in.txt");
			String msg1801AreaOut = loadFile("MSG_1801/area_out.txt");
			this.saveAreaPair(msg1801AreaIn, msg1801AreaOut);
			
			String msg1230AreaIn = loadFile("MSG_1230/area_in.txt");
			String msg1230AreaOut = loadFile("MSG_1230/area_out.txt");
			this.saveAreaPair(msg1230AreaIn, msg1230AreaOut);
			
			String msg1232AreaIn = loadFile("MSG_1232/area_in.txt");
			String msg1232AreaOut = loadFile("MSG_1232/area_out.txt");
			this.saveAreaPair(msg1232AreaIn, msg1232AreaOut);
			
			String msg1903AreaIn = loadFile("MSG_1903/area_in.txt");
			String msg1903AreaOut = loadFile("MSG_1903/area_out.txt");
			this.saveAreaPair(msg1903AreaIn, msg1903AreaOut);
			
			String msg2120AreaIn = loadFile("MSG_2120/area_in.txt");
			String msg2120AreaOut = loadFile("MSG_2120/area_out.txt");
			this.saveAreaPair(msg2120AreaIn, msg2120AreaOut);
			
			String msg2000AreaIn = loadFile("MSG_2000/area_in.txt");
			String msg2000AreaOut = loadFile("MSG_2000/area_out.txt");
			this.saveAreaPair(msg2000AreaIn, msg2000AreaOut);
			
			String msg2010AreaIn = loadFile("MSG_2010/area_in.txt");
			String msg2010AreaOut = loadFile("MSG_2010/area_out.txt");
			this.saveAreaPair(msg2010AreaIn, msg2010AreaOut);
			
			String msg2020AreaIn = loadFile("MSG_2020/area_in.txt");
			String msg2020AreaOut = loadFile("MSG_2020/area_out.txt");
			this.saveAreaPair(msg2020AreaIn, msg2020AreaOut);
			
			String msg1020AreaIn = loadFile("MSG_1020/area_in.txt");
			String msg1020AreaOut = loadFile("MSG_1020/area_out.txt");
			this.saveAreaPair(msg1020AreaIn, msg1020AreaOut);
			
			String msg0044AreaIn = loadFile("MSG_0044/area_in.txt");
			String msg0044AreaOut = loadFile("MSG_0044/area_out.txt");
			this.saveAreaPair(msg0044AreaIn, msg0044AreaOut);
			
			String msg2080AreaIn = loadFile("MSG_2080/area_in.txt");
			String msg2080AreaOut = loadFile("MSG_2080/area_out.txt");
			this.saveAreaPair(msg2080AreaIn, msg2080AreaOut);
			
			String msg2060AreaIn = loadFile("MSG_2060/area_in.txt");
			String msg2060AreaOut = loadFile("MSG_2060/area_out.txt");
			this.saveAreaPair(msg2060AreaIn, msg2060AreaOut);
			
			String msg2040AreaIn = loadFile("MSG_2040/area_in.txt");
			String msg2040AreaOut = loadFile("MSG_2040/area_out.txt");
			this.saveAreaPair(msg2040AreaIn, msg2040AreaOut);
			
			String msg2070AreaIn = loadFile("MSG_2070/area_in.txt");
			String msg2070AreaOut = loadFile("MSG_2070/area_out.txt");
			this.saveAreaPair(msg2070AreaIn, msg2070AreaOut);
			
			String msg2030AreaIn = loadFile("MSG_2030/area_in.txt");
			String msg2030AreaOut = loadFile("MSG_2030/area_out.txt");
			this.saveAreaPair(msg2030AreaIn, msg2030AreaOut);
			/*
			String msg1804AreaIn = loadFile("MSG_1804/area_in.txt");
			String msg1804AreaOut = loadFile("MSG_1804/area_out.txt");
			this.saveAreaPair(msg1804AreaIn, msg1804AreaOut);
			Se comenta esta carga de archivos ya que no existe en resources MSG_1804*/
			
			// NOVServices i900
			
			String msg1101_i900_AreaIn = loadFile("MSG_1101_i900/area_in.txt");
			String msg1101_i900_AreaOut = loadFile("MSG_1101_i900/area_out.txt");
			this.saveAreaPair(msg1101_i900_AreaIn, msg1101_i900_AreaOut);
			
			// ecoupons
//			0001
		/*	String msg0001_AreaIn = loadFile("MSG_0001/area_in.txt");
			String msg0001_AreaOut = loadFile("MSG_0001/area_out.txt");
			this.saveAreaPair(msg0001_AreaIn, msg0001_AreaOut); */
			
//			0002
		/*	String msg0002_AreaIn = loadFile("MSG_0002/area_in.txt");
			String msg0002_AreaOut = loadFile("MSG_0002/area_out.txt");
			this.saveAreaPair(msg0002_AreaIn, msg0002_AreaOut); */
			
//			0003
		/*	String msg0003_AreaIn = loadFile("MSG_0003/area_in.txt");
			String msg0003_AreaOut = loadFile("MSG_0003/area_out.txt");
			this.saveAreaPair(msg0003_AreaIn, msg0003_AreaOut); */
			
			
//			loadAreasFromPropertiesFile("1010");
			loadAreasFromPropertiesFile("1308");
//			loadAreasFromPropertiesFile("1110");
			loadAreasFromPropertiesFile("1106");
			loadAreasFromPropertiesFile("1108");
			loadAreasFromPropertiesFile("1113");
			//loadAreasFromPropertiesFile("1400");
			loadAreasFromPropertiesFile("1403");
			loadAreasFromPropertiesFile("1405");
			loadAreasFromPropertiesFile("1409");
			loadAreasFromPropertiesFile("1419_a1");
			loadAreasFromPropertiesFile("1419_a2");
			loadAreasFromPropertiesFile("1419_a3");
			loadAreasFromPropertiesFile("1419_a4");
			loadAreasFromPropertiesFile("1419_a5");
			loadAreasFromPropertiesFile("0004_1");
			loadAreasFromPropertiesFile("0047_1");
			loadAreasFromPropertiesFile("0047_2");
			loadAreasFromPropertiesFile("0047_3");
			loadAreasFromPropertiesFile("1540");
			loadAreasFromPropertiesFile("0051");
			loadAreasFromPropertiesFile("1008");
			loadAreasFromPropertiesFile("GetDispRastreo");

		} catch (IOException e) {
			logger.log(Level.SEVERE, "No pude cargar los archivos del AISMockConnector", e);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "No pude cargar los archivos del AISMockConnector", e);
		}
	}

	/**
	 * Carga un par request/response de un archivo properties
	 * 
	 * @param message
	 * @throws IOException
	 */
	private void loadAreasFromPropertiesFile(String message)
			throws IOException {
		Properties props = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("areas_"+ message+".properties");
		if ( is == null ) {
			logger.log(Level.WARNING, "No pude cargar el archivo properties para el mensaje: " + message);
		}
		props.load(is);
		this.saveAreaPair(props.getProperty("areaIn"), props.getProperty("areaOut"));
	}
	
	/**
	 * Graba el par en el mapa de requests
	 * 
	 * @param key
	 * @param value
	 */
	private void saveAreaPair(String key, String value) {
		if ( key != null && value != null) {
			this.messageMap.put(key,value);
		}
		
	}

	/**
	 * UTIL que carga un archivo en un string
	 * FALTA reemplazarlo por un FileUtils de apache commons
	 * 
	 * @return
	 */
	protected static String loadFile(String filename) {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
		if ( is == null) {
			logger.log(Level.WARNING, "No pude cargar el archivo: " + filename);
			return null;
		}
		return new Scanner(is).useDelimiter("\\Z").next();
	}


	/**
	 * Simula enviar el mensaje a AIS.
	 * Devuelve la respuesta que tenga configurada para el request
	 */
	@Override
	public String sendMessage(String request, long timeout) throws MQConnectorException {
		return this.sendMessage(request);
	}
		
	/**
	 * Simula enviar el mensaje a AIS.
	 * Devuelve la respuesta que tenga configurada para el request
	 */
	@Override
	public String sendMessage(String request) throws MQConnectorException {
		String response = messageMap.get(request);
		if ( response == null) {
			throw new MQConnectorException("AISConnectorMock: Sin respuesta cargada para el mensaje");
		}
		logger.log(Level.FINE, "REQ/RESP OK: [[" + request + "],[" + response + "]]");
		
		return response;
	}

	@Override
	public void setQueueManagerName(String queueManagerName) {
		this.queueManagerName = queueManagerName;

	}

	@Override
	public String getQueueManagerName() {
		return queueManagerName;
	}

	@Override
	public void setPutQueueName(String putQueueName) {
		this.putQueueName = putQueueName;
	}

	@Override
	public String getPutQueueName() {
		return putQueueName;
	}

	@Override
	public void setGetQueueName(String getQueueName) {
		this.getQueueName = getQueueName;
	}

	@Override
	public String getGetQueueName() {
		return getQueueName;
	}

	/**
	 * NOOP en el caso del mock
	 */
	@Override
	public void setMatchCriteria(int criteria) {
	}

	@Override
	public int getMatchCriteria() {
		return 1;
	}

	public long getReceiveTimeout() {
		return receiveTimeout;
	}

	public void setReceiveTimeout(long receiveTimeout) {
		this.receiveTimeout = receiveTimeout;
	}

}
