package com.qbe.connector.mq.impl;

import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;

import com.ibm.mq.jms.MQDestination;
import com.ibm.msg.client.wmq.WMQConstants;

import org.apache.commons.lang.StringUtils;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;

import com.qbe.connector.mq.MQConnector;
import com.qbe.connector.mq.MQConnectorException;
import com.qbe.connector.mq.MQConnectorTimeoutException;


/**
 * Implementación de MQConnector usando Spring JmsTemplate para la comunicación con MQ
 * 
 * @author ramiro
 *
 */
public class SpringMQConnector implements MQConnector {

	/**
	 * Pausa, en milisegundos, entre read()s de JSMS
	 */
	private static final int READ_PAUSE = 1 * 1000;

	protected static Logger logger = Logger.getLogger(SpringMQConnector.class.getName());

	/**
	 * Character set id a utilizar 
	 */
	private String ccsid;
	
	/**
	 * Encoding a utilizar
	 */
	private String encoding;

	/**
	 * Para enviar mensajes
	 */
	private JmsTemplate senderJMSTemplate;

	/**
	 * Para recibir mensajes
	 */
	private JmsTemplate receiverJMSTemplate;

	/**
	 * Tiempo que espera a recibir una respuesta.
	 * Por default, no esperamos nada.
	 */
	private long receiveTimeout = JmsTemplate.RECEIVE_TIMEOUT_NO_WAIT;
	
	/**
	 * ExecutorService usado para los reads con timeout
	 */
	private ExecutorService executor = Executors.newCachedThreadPool();

	/**
	 * Criterio por default
	 */
	private int matchCriteria = MSG_ID_2_MSG_ID;
			
	public SpringMQConnector() {
	}

	/**
	 * buckle your seatbelt Dorothy, because Kansas is going bye-bye
	 */
	public void destroy() {
		if ( this.executor != null ) {
			List<Runnable> rs = executor.shutdownNow();
			for (Runnable runnable : rs) {
				logger.log(Level.WARNING, "en el destroy de un SpringMQConnector, quedó colgado un: " + runnable);
			}
		}
	}
	
	/** 
	 * Envía un mensaje y busca la respuesta relacionada.
	 * Espera hasta receiveTimeout para devolver la respuesta
	 * 
	 * @see com.qbe.connector.mq.SpringMQConnector#sendMessage(java.lang.String, long)
	 */
	@Override
	public String sendMessage(final String value) throws MQConnectorException{
		return this.sendMessage(value, receiveTimeout);
	}
	
	
	/** 
	 * Envía un mensaje y busca la respuesta relacionada.
	 * Espera hasta el timeout recibido como parámetro
	 * No usa el receiveSelectedAndConvert() para manejar el timeout porque este bloquea el Thread mientras espera la respuesta.
	 * En cambio lee cada READ_PAUSE milisegundos hasta que encuentra el mensaje correlacionado o excede el timeout.
	 * 
	 * @see com.qbe.connector.mq.MQConnector#sendMessage(java.lang.String, long)
	 */
	@Override
	public String sendMessage(final String value, final long timeout) throws MQConnectorException{
		try {
			logger.log(Level.FINEST, String.format("Envío de mensaje usando ConnectionFactory: %s", this.getSenderJMSTemplate().getConnectionFactory()));
			logger.log(Level.FINE, String.format("PUT destination: %s \nGET destination: %s ", 
					this.getSenderJMSTemplate().getDefaultDestination(), this.getReceiverJMSTemplate().getDefaultDestination()));
			logger.log(Level.FINE,"Enviamos: {0} con timeout {1}",new Object[] { value, new Long(timeout) });

//			System.out.println(MessageFormat.format("Enviamos: {0} con timeout {1}",new Object[] { value, new Long(timeout) }));
			
			MQTextMessageCreator mc = new MQTextMessageCreator();
			mc.value = value;
			mc.ccsid = Integer.parseInt(ccsid);
			mc.encoding = Integer.parseInt(encoding);
			
			getSenderJMSTemplate().send(mc);
			logger.log(Level.FINEST,"Enviamos mensaje: {0}",mc.tmessage);
			final String matchSelector = getMatchSelector(mc.tmessage);
			logger.log(Level.FINEST,"Buscando respuesta con selector: {0}",matchSelector);
			
			String response = null;
			if ( timeout == JmsTemplate.RECEIVE_TIMEOUT_NO_WAIT ) {
				//Este no necesita timeout, vamos derecho
				response = (String)getReceiverJMSTemplate().receiveSelectedAndConvert(matchSelector);
			} else {
				//Recuperamos con un timeout no bloqueante
				Future<String> future = executor.submit(new Callable<String>() {
				    public String call() throws Exception {
						String response = null;
						int counter = 1;
						do {
							response = (String)getReceiverJMSTemplate().receiveSelectedAndConvert(matchSelector);
							if ( response == null ) {
								Thread.sleep(READ_PAUSE);
							}
							counter++;
						} while (response == null);
						return response;
				    }
				});
				response = future.get(timeout, TimeUnit.MILLISECONDS); //timeout is in seconds
			}
			if ( response == null ) {
				throw new MQConnectorTimeoutException("TIMEOUT " + timeout + " excedido al comunicarse via MQ");
			}
			logger.log(Level.FINEST, "REQ/RESP OK: [[" + value.trim() + "],[" + response.trim() + "]]");
			logger.log(Level.FINE, "REQ/RESP OK: [[" + StringUtils.abbreviate(value, 1024) + "],[" + StringUtils.abbreviate(response, 1024) + "]]");
//			System.out.println("REQ/RESP OK: [[" + StringUtils.abbreviate(value, 1024) + "],[" + response + "]]");
			return response;
		} catch (TimeoutException e) {
			throw new MQConnectorTimeoutException("TIMEOUT " + timeout + " excedido al comunicarse via MQ");
		} catch (InterruptedException e) {
			logger.log(Level.SEVERE, "InterruptedException al buscar un mensaje en MQ", e); 
			throw new MQConnectorException("InterruptedException al buscar un mensaje en MQ", e);
		} catch (ExecutionException e) {
			logger.log(Level.SEVERE, "ExecutionException al buscar un mensaje en MQ", e); 
			throw new MQConnectorException("ExecutionException al buscar un mensaje en MQ", e);
		} catch (JmsException e) {
			logger.log(Level.SEVERE, "JmsException en sendMessage", e); 
			throw new MQConnectorException(e.getErrorCode(), e );
		} catch (JMSException e) {
			logger.log(Level.SEVERE, "JMSException en sendMessage", e); 
			throw new MQConnectorException(e.getErrorCode(), e);
		}
	}



	/**
	 * Retorna el selector a usar en JMSTemplate.receiveSelectedAndConvert
	 * 
	 * http://docs.oracle.com/cd/E13171_01/alsb/docs25/interopjms/MsgIDPatternforJMS.html
	 * http://publib.boulder.ibm.com/infocenter/wmqv7/v7r0/index.jsp?topic=%2Fcom.ibm.mq.csqzaw.doc%2Fjm25470_.htm
	 * 
	 * @param mc
	 * @return
	 * @throws JMSException
	 * @throws MQConnectorException
	 */
	private String getMatchSelector(Message msg) throws JMSException, MQConnectorException {
		switch (this.matchCriteria) {
			case MSG_ID_2_MSG_ID:
				return "JMSMessageID = '" + msg.getJMSMessageID() + "'";
			case MSG_ID_2_CORRELATION_ID:
				return "JMSCorrelationID = '" + msg.getJMSMessageID() + "'";
			case CORRELATION_ID_2_CORRELATION_ID:
				return "JMSCorrelationID = '" + msg.getJMSCorrelationID() + "'"; 
			case CORRELATION_ID_2_MSG_ID:
				return "JMSMessageID = '" + msg.getJMSCorrelationID() + "'"; 
			default:
				throw new MQConnectorException("Sin criterio de match especificado");
		}
	}


	@Override
	public String getGetQueueName() {
		return null;
	}

	@Override
	public void setGetQueueName(String getQueueName) {
	}

	@Override
	public String getPutQueueName() {
		return null;
	}

	@Override
	public void setPutQueueName(String putQueueName) {
	}

	@Override
	public String getQueueManagerName() {
		return null;
	}

	@Override
	public void setQueueManagerName(String queueManagerName) {
	}

	@Override
	public void setMatchCriteria(int criteria) {
		this.matchCriteria = criteria;
	}

	@Override
	public int getMatchCriteria() {
		return this.matchCriteria;
	}

	public JmsTemplate getSenderJMSTemplate() {
		return senderJMSTemplate;
	}

	public void setSenderJMSTemplate(JmsTemplate senderJMSTemplate) {
		this.senderJMSTemplate = senderJMSTemplate;
	}

	public JmsTemplate getReceiverJMSTemplate() {
		return receiverJMSTemplate;
	}

	public void setReceiverJMSTemplate(JmsTemplate receiverJMSTemplate) {
		this.receiverJMSTemplate = receiverJMSTemplate;
	}

	public long getReceiveTimeout() {
		return receiveTimeout;
	}

	public void setReceiveTimeout(long receiveTimeout) {
		this.receiveTimeout = receiveTimeout;
	}
	
	public String getCcsid() {
		return ccsid;
	}

	public void setCcsid(String ccsid) {
		this.ccsid = ccsid;
	}
	
	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	
}


