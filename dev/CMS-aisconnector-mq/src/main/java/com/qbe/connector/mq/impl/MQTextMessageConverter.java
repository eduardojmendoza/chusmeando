package com.qbe.connector.mq.impl;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.commons.lang.StringUtils;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

/**
 * Convierte mensajes de texto, ya sea TextMessages o ByteMessages que tengan definido un JMS_IBM_Character_Set
 * 
 * @author ramiro
 *
 */
public class MQTextMessageConverter implements MessageConverter {

	protected static Logger logger = Logger.getLogger(MQTextMessageConverter.class.getName());

	@Override
	public Object fromMessage(Message message) throws JMSException,
			MessageConversionException {
		// Espero un TextMessage
		if ((message instanceof TextMessage)) {
			TextMessage tm = (TextMessage)message;
			// AIS devuelve el buffer completo, y este incluye basura más allá del mensaje.
			// Tomo como delimitador un 0, como un Null terminated String
			// No logueo basura, pero devuelvo el completo por si acaso hay código migrado que parsea más allá del 0
			String buffer = tm.getText();
			String cleanMessage = StringUtils.substringBefore(buffer, Character.toString((char)0));
			logger.log(Level.FINEST, "Convirtiendo TextMessage [{0}]", cleanMessage);
			return buffer;
		} else if ((message instanceof BytesMessage)) {
			BytesMessage bm = (BytesMessage)message;
			logger.log(Level.FINEST, "Convirtiendo BytesMessage [{0}]", bm);
			String ibmCharacterSet = bm.getStringProperty("JMS_IBM_Character_Set");
			if ( ! ( "ISO-8859-1".equals(ibmCharacterSet) || "UTF-8".equals(ibmCharacterSet))) {
				throw new MessageConversionException("Recibí un BytesMessage con un CharacterSet que no puedo decodificar: " + ibmCharacterSet);
			}
			CharsetDecoder cd = Charset.forName(ibmCharacterSet).newDecoder();
			long bll = bm.getBodyLength();
			if ( bll > Integer.MAX_VALUE) {
				throw new MessageConversionException("Recibí un BytesMessage con length (long)" + bll + ". No lo puedo meter en un byte[(int)]");
			}
			int bli = (int)bm.getBodyLength();
			byte[] stringBytes = new byte[bli];
			bm.readBytes(stringBytes);
			ByteBuffer bbuffer = ByteBuffer.wrap(stringBytes);
			CharBuffer cbuffer;
			try {
				cbuffer = cd.decode(bbuffer);
				String data = cbuffer.toString();
				return data;
			} catch (CharacterCodingException e) {
				throw new MessageConversionException("Exception al decodificar un BytesMessage con Character Set " + ibmCharacterSet, e);
			}
		}
		throw new MessageConversionException("Esperaba un TextMessage o un BytesMessage, pero vino un " + message.getClass().getName() + " toString() == " + message);
	}

	@Override
	public Message toMessage(Object obj, Session session) throws JMSException,
			MessageConversionException {
		
		logger.log(Level.FINEST, "Creando TextMessage a partir de [{0}]", obj);

		if ( !(obj instanceof String)) {
			throw new MessageConversionException("El objeto no es un String");
		}
		String msg = (String)obj;
		TextMessage tm = session.createTextMessage(msg);
		return tm;
	}

}
