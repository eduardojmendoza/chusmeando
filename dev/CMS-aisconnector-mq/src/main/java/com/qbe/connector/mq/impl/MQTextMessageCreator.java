package com.qbe.connector.mq.impl;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.jms.core.MessageCreator;

import com.ibm.msg.client.wmq.WMQConstants;

/**
 * Crea un Message usando un MQTextMessageConverter
 * 
 * @author ramiro
 *
 */
public class MQTextMessageCreator implements MessageCreator {

	public TextMessage tmessage;
	public String value;
	public int ccsid;
	public int encoding;

	@Override
	public Message createMessage(Session session) throws JMSException {
		MQTextMessageConverter conv = new MQTextMessageConverter();
		tmessage = (TextMessage) conv.toMessage(value, session);
		// Agrego encoding y charset http://www-01.ibm.com/support/knowledgecenter/SSFKSJ_7.5.0/com.ibm.mq.dev.doc/q032120_.htm	
		tmessage.setIntProperty(WMQConstants.JMS_IBM_CHARACTER_SET, ccsid); 
		tmessage.setIntProperty(WMQConstants.WMQ_ENCODING, encoding);
		return tmessage;
	}
	
}