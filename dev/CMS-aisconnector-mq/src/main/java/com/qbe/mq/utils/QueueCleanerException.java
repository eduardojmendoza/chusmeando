package com.qbe.mq.utils;

public class QueueCleanerException extends Exception {

	public QueueCleanerException() {
	}

	public QueueCleanerException(String message) {
		super(message);
	}

	public QueueCleanerException(Throwable cause) {
		super(cause);
	}

	public QueueCleanerException(String message, Throwable cause) {
		super(message, cause);
	}

}
