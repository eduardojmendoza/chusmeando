package com.qbe.mq.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.commons.io.FileUtils;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.BrowserCallback;
import org.springframework.jms.core.JmsTemplate;

import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.msg.client.wmq.common.CommonConstants;

/**
 * Limpia mensajes de una Queue MQ, y los graba en un directorio para un análisis forense
 * 
 * @author ramiro
 *
 */
public class QueueCleaner {

	/**
	 * Tiempo máximo para dejar un mensaje en una MQ, en milisegundos
	 */
	private static final int MESSAGE_AGE = 1 * 24 * 3600 * 1000;

	private static final int RECEIVE_TIMEOUT = 10000;

	private static final String MSG_ROOT_DIR = "/tmp/queuecleaner/";
	
	protected static Logger logger = Logger.getLogger(QueueCleaner.class.getName());
	

	public static void cleanOldMessages(String hostname, int port, String channel, String queueManager, String queueName) throws QueueCleanerException {

		try {
			
			final long instanceId = System.currentTimeMillis();
			final long now = System.currentTimeMillis();
	    	 logger.log(Level.FINE, "Starting instance: " + instanceId);
	    	 logger.log(Level.FINE, String.format("connecting to %s %d %s %s %s: ", hostname, port, channel, queueManager, queueName));
			
			MQQueueConnectionFactory connectionFactory = new MQQueueConnectionFactory();
			connectionFactory.setHostName(hostname);
			connectionFactory.setPort(port);
			connectionFactory.setChannel(channel);
			connectionFactory.setQueueManager(queueManager);
			connectionFactory.setTransportType(1);

			int client = CommonConstants.WMQ_CLIENT_NONJMS_MQ; //1

			
			final MQQueue getQueue = new MQQueue();
			getQueue.setBaseQueueManagerName(queueManager);

			getQueue.setBaseQueueName(queueName);
			getQueue.setTargetClient(client);
			
			int waitInterval = 1500;

			JmsTemplate getTemplate = new JmsTemplate();
			getTemplate.setConnectionFactory(connectionFactory);
			getTemplate.setDefaultDestination(getQueue);
			getTemplate.setReceiveTimeout(waitInterval);
			
/*
JMSMessage class: jms_text
  JMSType:          null
  JMSDeliveryMode:  2
  JMSExpiration:    0
  JMSPriority:      4
  JMSMessageID:     ID:414d512044514d4c4241465741523031b6d9f3522f048620
  JMSTimestamp:     1393723607490
  JMSCorrelationID: null
  JMSDestination:   null
  JMSReplyTo:       null
  JMSRedelivered:   false
    JMSXAppID: 351800/ANBEMRGCY/MSGMQOVLBA 
    JMSXDeliveryCount: 1
    JMSXUserID: anbemrgcy   
    JMS_IBM_Character_Set: ISO-8859-1
    JMS_IBM_Encoding: 546
    JMS_IBM_Format: MQSTR   
    JMS_IBM_MsgType: 8
    JMS_IBM_PutApplType: 8
    JMS_IBM_PutDate: 20140302
    JMS_IBM_PutTime: 01264749 
 */
		getTemplate.browse(getQueue, new BrowserCallback<Object>() {

			@Override
			public Object doInJms(Session session, QueueBrowser browser) throws JMSException {
				@SuppressWarnings("unchecked")
				Enumeration<Object> enu =  browser.getEnumeration();
				int cant = 0;
				while ( enu.hasMoreElements()) {
					Object msg = enu.nextElement();
					if ( msg instanceof TextMessage) {
						TextMessage textMsg = (TextMessage) msg;
						long timestamp = textMsg.getJMSTimestamp();
						String response = textMsg.toString();
						if ( now - timestamp > MESSAGE_AGE ) {
					    	 logger.log(Level.FINEST, "About to clean one message");

							 MessageConsumer consumer = session.createConsumer(getQueue, "JMSMessageID='" + textMsg.getJMSMessageID() + "'");
						     Message message = consumer.receive(RECEIVE_TIMEOUT);
						     if ( message == null) {
						    	 logger.log(Level.WARNING, "Timeout: No pude recuperar un mensaje para limpiarlo: " + response);
						     } else {
									try {
										FileUtils.writeStringToFile(new File(MSG_ROOT_DIR + instanceId + "-" + cant + ".mqmsg"), response, Charset.forName("UTF-8"));
									} catch (IOException e) {
										logger.log(Level.WARNING, "No pude grabar el archivo para el mensaje limpiado: " + response);
									}
									if (response != null )  {
										cant++;
									}

						     }
						} else {
					    	 logger.log(Level.FINEST, "Skipping one message, too young");
						}
					}
				}
				logger.log(Level.FINE, "Finished. Cleaned " + cant + " messages");
				return null;
			}
		});

		} catch (JmsException e) {
			throw new QueueCleanerException(e);
		} catch (JMSException e) {
			throw new QueueCleanerException(e);
		}

	}

}
