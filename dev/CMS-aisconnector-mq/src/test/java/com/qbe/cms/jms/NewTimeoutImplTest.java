package com.qbe.cms.jms;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Test;

import com.qbe.connector.mq.MQConnectorException;
import com.qbe.connector.mq.MQConnectorTimeoutException;

public class NewTimeoutImplTest {

	public NewTimeoutImplTest() {

	}

	@Test
	public void testExecutorWhenTimeout() throws MQConnectorException {
		long userTimeout = 10 * 1000;
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<String> future = executor.submit(new Callable<String>() {
		    public String call() throws Exception {
				String response = null;
				int counter = 1;
				do {
					//Simulo que voy a buscar un mensaje y no lo encuentra
					response = counter == 5 ? "text_message" : null;
					if ( response == null ) {
						System.out.println("Response es null. counter: " + counter + ". Espero un rato y pruebo de nuevo");
						Thread.sleep(1 * 1000);
					}
					counter++;
				} while (response == null);
				return response;
		    }
		});
		try {
			String response = future.get(userTimeout, TimeUnit.MILLISECONDS); //timeout is in seconds
			if ( response == null ) {
				throw new MQConnectorTimeoutException("TIMEOUT " + userTimeout + " excedido al comunicarse via MQ");
			}
			executor.shutdownNow();
			System.out.println("Retornando " + response);
		} catch (TimeoutException e) {
			throw new MQConnectorTimeoutException("TIMEOUT " + userTimeout + " excedido al comunicarse via MQ");
		} catch (InterruptedException e) {
			throw new MQConnectorException("InterruptedException al buscar un mensaje en MQ", e);
		} catch (ExecutionException e) {
			throw new MQConnectorException("ExecutionException al buscar un mensaje en MQ", e);
		}
	}
}
