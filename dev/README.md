# INSTALACION

Hola! Para compilar este proyecto usando JDK6 NO HAY QUE HACER NADA ESPECIAL

Pero! No usar new features de Java 7 o superior, o clases de rt.jar de java 7 o superior.

Esto porque rompe SegurosMobileServices que corre en un jdk 6 :/

El plugin de maven compiler genera 1.6

Para estar seguros, correr esto después de un mvn clean install, que lista los .class que NO sean versión 50.0 ( Java 1.6 ):

```
find . -name "*.class" | grep -v prjorig | grep -v vbc-source | grep -v outputVBC | xargs file | grep -v 50.0
./CMS-Siniestros/src/main/java/com/qbe/services/siniestros/impl/lbawA_SiniDenuncia.class:                                                                                              data
./CMS-Siniestros/src/main/java/com/qbe/services/siniestros/impl/lbawA_SiniDenuncia$1.class:                                                                                            data
```
Esos dos archivos no sé que hacen ahí...


Para SegurosMobileServices:

```
mvn clean install
cd CMS-SegurosMobile
mvn clean install
cd ..
cd CMS-SegurosMobileServices
mvn clean install
```




Listado de JAR a instalar antes de importar las dependencias de MAVEN
( los archivos deben de estar dentro de la carpeta `lib` )


```
mvn install:install-file -Dfile=lib/vbc.jar -DgroupId=com.diamondedge -DartifactId=vbc -Dversion=4 -Dpackaging=jar

mvn install:install-file -Dfile=lib/com.ibm.mq-7.5.jar -DgroupId=com.ibm.mq -DartifactId=mq -Dversion=7.5 -Dpackaging=jar

mvn install:install-file -Dfile=lib/com.ibm.mqjms-7.5.jar -DgroupId=com.ibm.mq -DartifactId=mqjms -Dversion=7.5 -Dpackaging=jar

mvn install:install-file -Dfile=lib/com.ibm.mq.jmqi-7.5.jar -DgroupId=com.ibm.mq  -DartifactId=jmqi -Dversion=7.5 -Dpackaging=jar

mvn install:install-file -Dfile=lib/dhbcore.jar -DgroupId=com.ibm -DartifactId=dhbcore -Dversion=7.5 -Dpackaging=jar

mvn install:install-file -Dfile=lib/com.ibm.mq.headers-7.5.jar -DgroupId=com.ibm.mq  -DartifactId=headers -Dversion=7.5 -Dpackaging=jar

mvn install:install-file -Dfile=lib/jcoldapi-2.27.jar -DgroupId=com.amco -DartifactId=jcoldapi -Dversion=2.27 -Dpackaging=jar

mvn install:install-file -Dfile=lib/sqljdbc4.jar -DgroupId=com.microsoft -DartifactId=sqljdbc4 -Dversion=3.0 -Dpackaging=jar

mvn install:install-file -Dfile=lib/cobertura-maven-plugin-2.5.2.jar -DgroupId=org.codehaus.mojo -DartifactId=cobertura-maven-plugin -Dversion=2.5.2 -Dpackaging=jar
```

Instalar las dependencias de MAVEN
```
mvn clean install -DskipTests
```
