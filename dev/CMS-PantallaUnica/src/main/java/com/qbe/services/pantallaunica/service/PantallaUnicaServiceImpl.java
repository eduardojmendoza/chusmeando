package com.qbe.services.pantallaunica.service;

import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebParam;

import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;

import com.qbe.services.pantallaunica.model.DataInicio;
import com.qbe.services.pantallaunica.model.Documento;
import com.qbe.services.pantallaunica.model.Producto;

/**
 * Implementación de PantallaUnicaService, que implementa dos tipos de operaciones:
 *  
 * FIXME: corregir esto
 * En algunas operaciones se comporta como un wrapper de SegurosOnline con alguna lógica de conversión. Estas operaciones 
 * están implementadas en OSB, y en esta clase se definen como dummies para facilitar la generación del WSDL via JAXB y
 * y poder hacer algunas pruebas sin involucrar al OSB.
 * 
 * Otras operaciones incluyen lógica que resultó más cómodo y simple implementar acá; en este caso las operaciones en el
 * OSB son un pasamanos.
 * 
 * @author ramiro
 *
 */
public class PantallaUnicaServiceImpl implements PantallaUnicaService {

	private static Logger logger = Logger.getLogger(PantallaUnicaServiceImpl.class.getName());

	protected static ApplicationContext context; 

	protected synchronized static ApplicationContext getContext() {

		if ( context == null) {
			context = new ClassPathXmlApplicationContext("classpath*:spring-beans-seguros-mobile.xml");
		}
		return context;
	}


	/**
	 * Retorna datos del cliente y sus productos.
	 * 
	 * Implementado en OSB, esta es una implementación dummy.
	 * 
	 */
	@Override
	@WebMethod(operationName = "productosCliente", action = "productosCliente")
	public DataInicio productosCliente(
			@WebParam(name = "documento") Documento documento) {

		DataInicio di = new DataInicio();
//		di.getProducto().add(getDummyProduct());
//		UsuarioInicio dui = new UsuarioInicio();
//		dui.setApellido("Federer");
//		dui.setNombre("Roger");
//		dui.setCliensec(12345678);
//		dui.setDocumento(documento);
//		di.setUsuarioInicio(dui);
		return di;
	}

	private Producto getDummyProduct() {
		Producto p = new Producto();
//		p.ciaascod = "0001";
//		p.ramopcod = "AUS1";
//		p.polizann = 0;
//		p.polizsec = 1;
//		p.certipol = 0;
//		p.certiann = 1;
//		p.certisec = 411665;
//		p.suplenum = 0;
//		p.ramopdes = "AUTOSCORING";
//		p.cliensecv = 101555163;
//		p.clienap1v = "QBE SEGUROS LA BUENO";
//		p.clienap2v = "S AIRES";
//		p.cliennomv = null;
//		p.agentcod = 9243;
//		p.agentcla = "PR";
//		p.tomaries = "CITROEN C3 10DBS40005787";
//		p.situcpol = "PROX.RENOV";
//		p.emisiann = 0;
//		p.emisimes = 0;
//		p.emisidia = 0;
//		p.tipoprod = "M";
//		p.patente = "JCB046";
//		p.dato1 = "00010";
//		p.dato2 = "10DBS40005787";
//		p.dato3 = "JCB046";
//		p.dato4 = "2010";
//		p.cobrodes = "MC";
//		p.swsuscri = null;
//		p.mail = "ramiro@snoopconsulting.com";
//		p.clave = null;
//		p.swclave = null;
//		p.mensasin = null;
//		p.mensasus = null;
//		p.swimprim = "S";
//		p.cobranza = "S";
//		p.siniestro = "S";
//		p.constancia = "S";
//		p.habilitadoNBWS = "S";
//		p.positiveid = "A";
//		p.polizaExcluida = "N";
//		p.habilitadoNavegacion = "S";
//		p.masDeCincoCert = "N";
//		p.habilitadoEPoliza = "S";
		return p;
	}


	
}
