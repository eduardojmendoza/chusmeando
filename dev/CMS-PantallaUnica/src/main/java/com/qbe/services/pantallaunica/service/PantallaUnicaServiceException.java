package com.qbe.services.pantallaunica.service;

public class PantallaUnicaServiceException extends Exception {

	public PantallaUnicaServiceException() {
	}

	public PantallaUnicaServiceException(String message) {
		super(message);
	}

	public PantallaUnicaServiceException(Throwable cause) {
		super(cause);
	}

	public PantallaUnicaServiceException(String message, Throwable cause) {
		super(message, cause);
	}

}
