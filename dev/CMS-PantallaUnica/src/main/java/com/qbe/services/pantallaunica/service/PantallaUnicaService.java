package com.qbe.services.pantallaunica.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.qbe.services.pantallaunica.model.DataInicio;
import com.qbe.services.pantallaunica.model.Documento;

/**
 * Este servicio exporta operaciones que son usadas por la aplicación "PantallaUnica".
 * 
 * Está pensado para ser a su vez wrappeado desde OSB, usando el mismo WSDL que se genera de esta interface via JAXB.
 * 
 * @author ramiro
 *
 */
@WebService(
		name = "PantallaUnicaService" 
		,targetNamespace = "http://cms.services.qbe.com/pantallaunica"
)
public interface PantallaUnicaService {
	
	@WebMethod(operationName = "productosCliente", action = "productosCliente")
	public DataInicio productosCliente ( 
			@WebParam(name = "documento" ) Documento documento);

	
}
