package com.qbe.services.siniestros.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.siniestros.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_siniPutLogAud implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniPutLogAud";
  static final String mcteStoreProc = "P_SINI_INSERT_LOGAUD";
  /**
   * 
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_SINIEANNAIS = "//SINIEANNAIS";
  static final String mcteParam_SINIENUMAIS = "//SINIENUMAIS";
  static final String mcteParam_SINIENUMSQL = "//SINIENUMSQL";
  static final String mcteParam_NROFORM = "//NROFORM";
  static final String mcteParam_GRABAUSU = "//GRABAUSU";
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_GRABASVIA = "//GRABASVIA";
  static final String mcteParam_ESTADODAT = "//ESTADODAT";
  static final String mcteParam_RECARGA = "//RECARGA";
  static final String mcteParam_ESTADOAIS = "//ESTADOAIS";
  static final String mcteParam_ESTADOMID = "//ESTADOMID";
  static final String mcteParam_SINFECHA = "//SINFECHA";
  static final String mcteParam_DENFECHA = "//DENFECHA";
  static final String mcteParam_GRABASFECHA = "//GRABASFECHA";
  static final String mcteParam_GRABAAFECHA = "//GRABAAFECHA";
  static final String mcteParam_NROSAC = "//NROSAC";
  static final String mcteParam_ORDNRO1 = "//ORDNRO1";
  static final String mcteParam_ORDNRO2 = "//ORDNRO2";
  static final String mcteParam_ORDNRO3 = "//ORDNRO3";
  static final String mcteParam_XMLErrores = "//XMLErrores";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String mvarRAMOPCOD = "";
    String mvarSINIEANNAIS = "";
    String mvarSINIENUMAIS = "";
    String mvarSINIENUMSQL = "";
    String mvarNROFORM = "";
    String mvarGRABAUSU = "";
    String mvarCLIENSEC = "";
    String mvarGRABASVIA = "";
    String mvarESTADODAT = "";
    String mvarRECARGA = "";
    String mvarESTADOAIS = "";
    String mvarESTADOMID = "";
    String mvarSINFECHA = "";
    String mvarDENFECHA = "";
    String mvarGRABASFECHA = "";
    String mvarGRABAAFECHA = "";
    String mvarNROSAC = "";
    String mvarORDNRO1 = "";
    String mvarORDNRO2 = "";
    String mvarORDNRO3 = "";
    String mvarXMLErrores = "";
    //
    //
    //Par�metros de entrada
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      mvarSINIEANNAIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINIEANNAIS )  );

      //Levanto los datos del XML de entrada.
      wvarStep = 20;

      wvarStep = 21;
      mvarRAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  );

      wvarStep = 22;
      mvarSINIEANNAIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINIEANNAIS )  );

      wvarStep = 23;
      mvarSINIENUMAIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINIENUMAIS )  );

      wvarStep = 24;
      mvarSINIENUMSQL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINIENUMSQL )  );

      wvarStep = 25;
      mvarNROFORM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NROFORM )  );

      wvarStep = 26;
      mvarGRABAUSU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_GRABAUSU )  );

      wvarStep = 27;
      mvarCLIENSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC )  );

      wvarStep = 28;
      mvarGRABASVIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_GRABASVIA )  );

      wvarStep = 29;
      mvarESTADODAT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ESTADODAT )  );

      wvarStep = 30;
      mvarRECARGA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RECARGA )  );

      wvarStep = 31;
      mvarESTADOAIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ESTADOAIS )  );

      wvarStep = 32;
      mvarESTADOMID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ESTADOMID )  );

      wvarStep = 33;
      //Dato obligatorio de entrada, no puede venir vac�o.
      mvarSINFECHA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINFECHA )  );

      wvarStep = 34;
      //Dato obligatorio de entrada, no puede venir vac�o.
      mvarDENFECHA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DENFECHA )  );

      wvarStep = 35;
      mvarGRABASFECHA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_GRABASFECHA )  );

      wvarStep = 36;
      mvarGRABAAFECHA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_GRABAAFECHA )  );

      wvarStep = 37;
      mvarNROSAC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NROSAC )  );

      wvarStep = 38;
      mvarORDNRO1 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ORDNRO1 )  );

      wvarStep = 39;
      mvarORDNRO2 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ORDNRO2 )  );

      wvarStep = 40;
      mvarORDNRO3 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ORDNRO3 )  );

      wvarStep = 41;
      mvarXMLErrores = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_XMLErrores )  );


      //Si hay datos num�ricos en blanco mando como default el cero (solo para los campos que no permiten NULL)
      if( Strings.trim( mvarSINIEANNAIS ).equals( "" ) )
      {
        mvarSINIEANNAIS = "0";
      }
      if( Strings.trim( mvarSINIENUMAIS ).equals( "" ) )
      {
        mvarSINIENUMAIS = "0";
      }
      if( Strings.trim( mvarSINIENUMSQL ).equals( "" ) )
      {
        mvarSINIENUMSQL = "0";
      }

      wvarStep = 42;
      //TODO: OJOOOOOOOOOOOO En mi PC no funciona con Trx. Para debuggear cambiar. Luego volver a su forma con Trx
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
      wobjDBCmd = new Command();

      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 60;
      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( "0" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 70;
      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarRAMOPCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 80;
      wobjDBParm = new Parameter( "@SINIEANNAIS", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarSINIEANNAIS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 90;
      wobjDBParm = new Parameter( "@SINIENUMAIS", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarSINIENUMAIS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 100;
      wobjDBParm = new Parameter( "@SINIENUMSQL", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarSINIENUMSQL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 110;
      wobjDBParm = new Parameter( "@NROFORM", AdoConst.adSmallInt, AdoConst.adParamInput, 0, new Variant( mvarNROFORM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 120;
      wobjDBParm = new Parameter( "@GRABAUSU", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarGRABAUSU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 130;
      wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adInteger, AdoConst.adParamInput, 0, null );
      wobjDBCmd.getParameters().append( wobjDBParm );
      if( mvarCLIENSEC.equals( "" ) )
      {
        wobjDBParm.getValue().setNull();
      }
      else
      {
        wobjDBParm.setValue( new Variant( mvarCLIENSEC ) );
      }
      wobjDBParm = (Parameter) null;

      wvarStep = 140;
      wobjDBParm = new Parameter( "@GRABASVIA", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarGRABASVIA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 150;
      wobjDBParm = new Parameter( "@ESTADODAT", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarESTADODAT ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 160;
      wobjDBParm = new Parameter( "@RECARGA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarRECARGA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 170;
      wobjDBParm = new Parameter( "@ESTADOAIS", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarESTADOAIS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 180;
      wobjDBParm = new Parameter( "@ESTADOMID", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( mvarESTADOMID ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 190;
      wobjDBParm = new Parameter( "@SINFECHA", AdoConst.adChar, AdoConst.adParamInput, 17, new Variant( mvarSINFECHA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 200;
      wobjDBParm = new Parameter( "@DENFECHA", AdoConst.adChar, AdoConst.adParamInput, 17, new Variant( mvarDENFECHA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 210;
      wobjDBParm = new Parameter( "@GRABASFECHA", AdoConst.adChar, AdoConst.adParamInput, 17, null );
      //Si la fecha vino vac�a, env�o NULL
      if( mvarGRABASFECHA.equals( "" ) )
      {
        wobjDBParm.getValue().setNull();
      }
      else
      {
        wobjDBParm.setValue( new Variant( mvarGRABASFECHA ) );
      }
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 220;
      wobjDBParm = new Parameter( "@GRABAAFECHA", AdoConst.adChar, AdoConst.adParamInput, 17, null );
      //Si la fecha vino vac�a, env�o NULL
      if( mvarGRABAAFECHA.equals( "" ) )
      {
        wobjDBParm.getValue().setNull();
      }
      else
      {
        wobjDBParm.setValue( new Variant( mvarGRABAAFECHA ) );
      }
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 230;
      wobjDBParm = new Parameter( "@NROSAC", AdoConst.adInteger, AdoConst.adParamInput, 0, null );
      //Si el nro de SAC vino vac�o, env�o NULL
      if( mvarNROSAC.equals( "" ) )
      {
        wobjDBParm.getValue().setNull();
      }
      else
      {
        wobjDBParm.setValue( new Variant( mvarNROSAC ) );
      }
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 240;
      wobjDBParm = new Parameter( "@ORDNRO1", AdoConst.adInteger, AdoConst.adParamInput, 0, null );
      if( mvarORDNRO1.equals( "" ) )
      {
        wobjDBParm.getValue().setNull();
      }
      else
      {
        wobjDBParm.setValue( new Variant( mvarORDNRO1 ) );
      }
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 250;
      wobjDBParm = new Parameter( "@ORDNRO2", AdoConst.adInteger, AdoConst.adParamInput, 0, null );
      if( mvarORDNRO2.equals( "" ) )
      {
        wobjDBParm.getValue().setNull();
      }
      else
      {
        wobjDBParm.setValue( new Variant( mvarORDNRO2 ) );
      }
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 260;
      wobjDBParm = new Parameter( "@ORDNRO3", AdoConst.adInteger, AdoConst.adParamInput, 0, null );
      if( mvarORDNRO3.equals( "" ) )
      {
        wobjDBParm.getValue().setNull();
      }
      else
      {
        wobjDBParm.setValue( new Variant( mvarORDNRO3 ) );
      }
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 270;
      wobjDBParm = new Parameter( "@XMLErrores", AdoConst.adVarChar, AdoConst.adParamInput, 4000, new Variant( mvarXMLErrores ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 280;
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );

      wvarStep = 290;
      //Controlamos la respuesta del SQL
      if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      else
      {
        wvarMensaje = "Error al insertar en la tabla SQL SINI_LOGAUD (log de auditor�a)";
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='" + wvarMensaje + "' /></Response>" );
      }

      wvarStep = 300;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      wvarStep = 310;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = null;
        wobjXMLRequest = null;

        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    finally {
		try {
			if (wobjDBCnn != null)
				wobjDBCnn.close();
		} catch (Exception e) {
		}
	}
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
