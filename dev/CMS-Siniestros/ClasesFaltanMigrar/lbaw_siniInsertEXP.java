package com.qbe.services.siniestros.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.siniestros.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_siniInsertEXP implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniInsertEXP";
  static final String mcteStoreProc = "P_SINI_INSERT_EXPORTAR";
  /**
   * 
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_SINIEANNAIS = "//SINIEANNAIS";
  static final String mcteParam_SINIENUMAIS = "//SINIENUMAIS";
  static final String mcteParam_SINIENUMSQL = "//SINIENUMSQL";
  static final String mcteParam_GRABFECHA = "//GRABFECHA";
  static final String mcteParam_USUARCOD = "//USUARCOD";
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_XMLDATOS = "//XMLDATOS";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String mvarRAMOPCOD = "";
    String mvarSINIEANNAIS = "";
    String mvarSINIENUMAIS = "";
    String mvarSINIENUMSQL = "";
    String mvarGRABFECHA = "";
    String mvarUSUARCOD = "";
    String mvarCLIENSEC = "";
    String mvarXMLDATOS = "";
    //
    //
    //Par�metros de entrada
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 20;
      wvarStep = 21;
      mvarRAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  );
      wvarStep = 22;
      mvarSINIEANNAIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINIEANNAIS )  );
      wvarStep = 23;
      mvarSINIENUMAIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINIENUMAIS )  );
      wvarStep = 24;
      mvarSINIENUMSQL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINIENUMSQL )  );
      wvarStep = 25;
      mvarGRABFECHA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_GRABFECHA )  );
      wvarStep = 26;
      if( Strings.len( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC )  ) ) ) > 0 )
      {
        mvarCLIENSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC )  );
      }
      else
      {
        mvarCLIENSEC = "0";
      }
      wvarStep = 27;
      mvarXMLDATOS = wobjXMLRequest.selectSingleNode( mcteParam_XMLDATOS ) .toString();
      wvarStep = 28;
      mvarUSUARCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD )  );

      wvarStep = 40;
      //TODO: OJOOOOOOOOOOOO En mi PC no funciona con Trx. Para debuggear cambiar. Luego volver a su forma con Trx
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
      wobjDBCmd = new Command();

      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 60;
      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( "0" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 61;
      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarRAMOPCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 62;
      wobjDBParm = new Parameter( "@SINIEANNAIS", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarSINIEANNAIS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 63;
      wobjDBParm = new Parameter( "@SINIENUMAIS", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarSINIENUMAIS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 64;
      wobjDBParm = new Parameter( "@SINIENUMSQL", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarSINIENUMSQL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 64;
      wobjDBParm = new Parameter( "@GRABFECHA", AdoConst.adVarChar, AdoConst.adParamInput, 8, new Variant( Strings.left( mvarGRABFECHA, 8 ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 65;
      wobjDBParm = new Parameter( "@USUARCOD", AdoConst.adVarChar, AdoConst.adParamInput, 10, new Variant( Strings.left( mvarUSUARCOD, 10 ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 66;
      wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarCLIENSEC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 70;
      wobjDBParm = new Parameter( "@XMLDATOS", AdoConst.adLongVarChar, AdoConst.adParamInput, Strings.len( mvarXMLDATOS ), new Variant( mvarXMLDATOS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 80;
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );

      wvarStep = 250;
      //Controlamos la respuesta del SQL
      if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "Se ha registrado exitosamente la informaci�n del siniestro para su posterior exportacion." + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      else
      {
        wvarMensaje = "Error al insertar en la tabla SQL SINI_EXPORTAR";
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='" + wvarMensaje + "' /></Response>" );
      }

      wvarStep = 260;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      wvarStep = 270;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = null;
        wobjXMLRequest = null;

        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    finally {
		try {
			if (wobjDBCnn != null)
				wobjDBCnn.close();
		} catch (Exception e) {
		}
	}
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
