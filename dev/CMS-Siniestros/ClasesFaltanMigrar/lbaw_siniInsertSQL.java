package com.qbe.services.siniestros.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.siniestros.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_siniInsertSQL implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniInsertSQL";
  static final String mcteStoreProc = "P_SINI_INSERT_SINIESTRO";
  /**
   * 
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_SINIEANNAIS = "//SINIEANNAIS";
  static final String mcteParam_SINIENUMAIS = "//SINIENUMAIS";
  static final String mcteParam_GRABSVIA = "//GRABSVIA";
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_GRABAUSU = "//GRABAUSU";
  static final String mcteParam_NROFORM = "//NROFORM";
  static final String mcteParam_ESTADODAT = "//ESTADODAT";
  static final String mcteParam_MSINIENUM = "//MSINIENUM";
  static final String mcteParam_RECARGA = "//RECARGA";
  static final String mcteParam_MARENVAIS = "//MARENVAIS";
  static final String mcteParam_ESTADOAIS = "//ESTADOAIS";
  static final String mcteParam_MARENVMID = "//MARENVMID";
  static final String mcteParam_ESTADOMID = "//ESTADOMID";
  static final String mcteParam_XMLDatosAIS = "//XMLDatosAIS";
  static final String mcteParam_XMLDatosMID = "//XMLDatosMID";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String mvarRAMOPCOD = "";
    String mvarSINIEANNAIS = "";
    String mvarSINIENUMAIS = "";
    String mvarGRABSVIA = "";
    String mvarCLIENSEC = "";
    String mvarGRABAUSU = "";
    String mvarNROFORM = "";
    String mvarESTADODAT = "";
    String mvarMSINIENUM = "";
    String mvarRECARGA = "";
    String mvarMARENVAIS = "";
    String mvarESTADOAIS = "";
    String mvarMARENVMID = "";
    String mvarESTADOMID = "";
    String mvarXMLDatosAIS = "";
    String mvarXMLDatosMID = "";
    int mvarID_SINIESTRO = 0;
    java.util.Date mvarFECHA_ALTA = DateTime.EmptyDate;
    //
    //
    //Par�metros de entrada
    //Par�metros de salida
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 20;
      wvarStep = 21;
      mvarRAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  );
      wvarStep = 22;
      mvarNROFORM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NROFORM )  );
      wvarStep = 23;
      mvarSINIEANNAIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINIEANNAIS )  );
      wvarStep = 24;
      mvarSINIENUMAIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINIENUMAIS )  );
      wvarStep = 25;
      mvarGRABSVIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_GRABSVIA )  );
      wvarStep = 26;
      mvarCLIENSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC )  );
      wvarStep = 27;
      mvarGRABAUSU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_GRABAUSU )  );
      wvarStep = 28;
      mvarESTADODAT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ESTADODAT )  );
      wvarStep = 29;
      mvarMSINIENUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MSINIENUM )  );
      wvarStep = 30;
      mvarRECARGA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RECARGA )  );
      wvarStep = 31;
      mvarMARENVAIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MARENVAIS )  );
      wvarStep = 32;
      mvarESTADOAIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ESTADOAIS )  );
      wvarStep = 33;
      mvarMARENVMID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MARENVMID )  );
      wvarStep = 34;
      mvarESTADOMID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ESTADOMID )  );
      wvarStep = 35;
      mvarXMLDatosAIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_XMLDatosAIS )  );
      wvarStep = 36;
      mvarXMLDatosMID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_XMLDatosMID )  );

      wvarStep = 40;
      //TODO: OJOOOOOOOOOOOO En mi PC no funciona con Trx. Para debuggear cambiar. Luego volver a su forma con Trx
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
      wobjDBCmd = new Command();

      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 60;
      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( "0" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 65;
      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarRAMOPCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 70;
      wobjDBParm = new Parameter( "@NROFORM", AdoConst.adSmallInt, AdoConst.adParamInput, 0, new Variant( mvarNROFORM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 80;
      wobjDBParm = new Parameter( "@SINIEANNAIS", AdoConst.adInteger, AdoConst.adParamInput, 0, null );
      //Si mvarSINIEANNAIS vino vac��, env�o NULL
      if( mvarSINIEANNAIS.equals( "" ) )
      {
        wobjDBParm.getValue().setNull();
      }
      else
      {
        wobjDBParm.setValue( new Variant( mvarSINIEANNAIS ) );
      }
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 90;
      wobjDBParm = new Parameter( "@SINIENUMAIS", AdoConst.adInteger, AdoConst.adParamInput, 0, null );
      //Si mvarSINIENUMAIS vino vac��, env�o NULL
      if( mvarSINIENUMAIS.equals( "" ) )
      {
        wobjDBParm.getValue().setNull();
      }
      else
      {
        wobjDBParm.setValue( new Variant( mvarSINIENUMAIS ) );
      }
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 100;
      wobjDBParm = new Parameter( "@GRABSVIA", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarGRABSVIA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 110;
      wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adInteger, AdoConst.adParamInput, 0, null );
      //Si mvarCLIENSEC vino vac��, env�o NULL
      if( mvarCLIENSEC.equals( "" ) )
      {
        wobjDBParm.getValue().setNull();
      }
      else
      {
        wobjDBParm.setValue( new Variant( mvarCLIENSEC ) );
      }
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 120;
      wobjDBParm = new Parameter( "@GRABAUSU", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarGRABAUSU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 130;
      wobjDBParm = new Parameter( "@ESTADODAT", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarESTADODAT ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 140;
      wobjDBParm = new Parameter( "@MSINIENUM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarMSINIENUM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 150;
      wobjDBParm = new Parameter( "@RECARGA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarRECARGA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 160;
      wobjDBParm = new Parameter( "@MARENVAIS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarMARENVAIS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 170;
      wobjDBParm = new Parameter( "@ESTADOAIS", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarESTADOAIS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 180;
      wobjDBParm = new Parameter( "@MARENVMID", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarMARENVMID ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 190;
      wobjDBParm = new Parameter( "@ESTADOMID", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( mvarESTADOMID ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 200;
      wobjDBParm = new Parameter( "@XMLDatosAIS", AdoConst.adLongVarChar, AdoConst.adParamInput, Strings.len( wobjXMLRequest.getDocument().getDocumentElement().toString() ), new Variant( /*unsup wobjXMLRequest.selectSingleNode( "//XMLDatosAIS" ) */.toString()).setNull() );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 210;
      wobjDBParm = new Parameter( "@XMLDatosMID", AdoConst.adLongVarChar, AdoConst.adParamInput, Strings.len( wobjXMLRequest.getDocument().getDocumentElement().toString() ), new Variant( /*unsup wobjXMLRequest.selectSingleNode( "//XMLDatosMID" ) */.toString()).setNull() );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 220;
      wobjDBParm = new Parameter( "@ID_SINIESTRO", AdoConst.adInteger, AdoConst.adParamOutput, 0, new Variant( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 230;
      wobjDBParm = new Parameter( "@FECHA_ALTA", AdoConst.adChar, AdoConst.adParamOutput, 20, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 240;
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );

      wvarStep = 250;
      //Controlamos la respuesta del SQL
      if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><ID_SINIESTRO>" + Strings.right( ("0000000000" + wobjDBCmd.getParameters().getParameter("@ID_SINIESTRO").getValue()), 10 ) + "</ID_SINIESTRO><FECHA_ALTA>" + wobjDBCmd.getParameters().getParameter("@FECHA_ALTA").getValue() + "</FECHA_ALTA></Response>" );
      }
      else
      {
        wvarMensaje = "Error al insertar en la tabla SQL SINI_SINIESTROS";
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='" + wvarMensaje + "' /></Response>" );
      }

      wvarStep = 260;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      wvarStep = 270;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = null;
        wobjXMLRequest = null;

        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    finally {
		try {
			if (wobjDBCnn != null)
				wobjDBCnn.close();
		} catch (Exception e) {
		}
	}
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
