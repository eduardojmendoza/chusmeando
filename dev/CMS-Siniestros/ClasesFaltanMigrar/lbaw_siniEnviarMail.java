package com.qbe.services.siniestros.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.siniestros.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : lbaw_siniEnviarMail
 *  File Name : lbaw_siniEnviarMail.cls
 *  Creation Date: 07/12/2005
 *  Programmer : Fernando Osores / Rodrigo Goncalves.
 *  Abstract :    Envia un mail en funci�n de los parametros de entrada y de un archivo
 *        XML de configuraci�n. Este debe estar dentro del directorio de ejecucion en
 *        /TemplateMail.
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class lbaw_siniEnviarMail implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniEnviarMail";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_PARAMETROS = "//PARAMETROS/PARAMETRO";
  static final String mcteParam_PARAM_NOMBRE = "PARAM_NOMBRE";
  static final String mcteParam_PARAM_VALOR = "PARAM_VALOR";
  static final String mcteParam_FORMAT_FILE = "//FORMAT_FILE";
  static final String mcteParam_FROM = "//FROM";
  static final String mcteParam_SUBJECT = "//SUBJECT";
  static final String mcteParam_TO = "//TO";
  static final String mcteParam_REPLYTO = "//REPLYTO";
  static final String mcteParam_IMPORTANCE = "//IMPORTANCE";
  static final String mcteParam_BODYFORMAT = "//BODYFORMAT";
  static final String mcteParam_MAILFORMAT = "//MAILFORMAT";
  static final String mcteParam_BODY = "//BODY";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Envia un mail on DEMAND.
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLFormat = null;
    org.w3c.dom.Node wobjElement = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarFormatFile = "";
    String wvarParamNombre = "";
    String wvarParamValor = "";
    String wvarFrom = "";
    String wvarTo = "";
    String wvarSubject = "";
    String wvarReplyTo = "";
    String wvarImportance = "";
    String wvarBodyFormat = "";
    String wvarMailFormat = "";
    String wvarBody = "";
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      // CARGO EL XML DE ENTRADA
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      wvarFormatFile = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FORMAT_FILE )  );
      //
      // CARGO EL XML CON EL FORMATO DEL MAIL
      //
      wvarStep = 30;
      wobjXMLFormat = new XmlDomExtended();
      System.out.println( wobjXMLFormat.loadXML( Request ) );

      System.out.println( wobjXMLFormat.load( System.getProperty("user.dir") + "\\TemplateMail\\" + wvarFormatFile + ".xml" ) );
      //
      // CARGO EL RESTO DE LOS PARAMETROS
      //
      wvarFrom = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_FROM )  );
      wvarTo = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TO )  );

      // SI MANDA UN NODO SUBJECT USO ESE, SINO USO EL DEFAULT
      if( wobjXMLRequest.selectNodes( mcteParam_SUBJECT ) .getLength() == 0 )
      {
        wvarSubject = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_SUBJECT )  );
      }
      else
      {
        if( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SUBJECT )  ).equals( "" ) )
        {
          wvarSubject = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_SUBJECT )  );
        }
        else
        {
          wvarSubject = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SUBJECT )  );
        }
      }
      //
      wvarReplyTo = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_REPLYTO )  );
      wvarImportance = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_IMPORTANCE )  );
      wvarBodyFormat = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_BODYFORMAT )  );
      wvarMailFormat = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_MAILFORMAT )  );
      wvarBody = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_BODY )  );
      //
      wvarStep = 40;
      //
      // RECORRO LA LISTA DE PARAMETROS Y VALORES
      for( int nwobjElement = 0; nwobjElement < wobjXMLRequest.selectNodes( mcteParam_PARAMETROS ) .getLength(); nwobjElement++ )
      {
        wobjElement = wobjXMLRequest.selectNodes( mcteParam_PARAMETROS ) .item( nwobjElement );
        wvarParamNombre = XmlDomExtended.getText( wobjElement.selectSingleNode( mcteParam_PARAM_NOMBRE )  );
        wvarParamValor = XmlDomExtended.getText( wobjElement.selectSingleNode( mcteParam_PARAM_VALOR )  );
        //
        wvarBody = Strings.replace( wvarBody, wvarParamNombre, wvarParamValor );
      }
      //
      wobjXMLRequest = null;
      wobjXMLFormat = null;
      wobjElement = (org.w3c.dom.Node) null;
      //
      // EJECUTO EL ENVIO DE MAILS
      wvarStep = 50;
      if( invoke( "p_Send_EMail", new Variant[] { new Variant(wvarFrom), new Variant(wvarTo), new Variant(wvarSubject), new Variant(wvarImportance), new Variant(wvarBodyFormat), new Variant(wvarMailFormat), new Variant(wvarBody), new Variant(wvarReplyTo) } ) )
      {
        wvarStep = 60;
        Response.set( "<Response><Estado resultado=\"true\" mensaje=\"EMAIL ENVIADO\" /></Response>" );
      }
      else
      {
        wvarStep = 70;
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO ENVIAR EL EMAIL.\" /></Response>" );
      }
      //
      wvarStep = 80;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = null;
        wobjXMLFormat = null;
        wobjElement = (org.w3c.dom.Node) null;
        //
        //    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
        // '                     mcteClassName, _
        //                     wcteFnName, _
        //  '                    wvarStep, _
        //                   "Error= [" & Err.Number & "] - " & Err.Description, _
        //    '                  vbLogEventTypeError
        /*unsup Err.getError().Number( new Variant(), new Variant() ) */;
        IAction_Execute = 1;
        //    mobjCOM_Context.SetAbort
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  /**
   *  *****************************************************************
   *  Function : p_Send_EMail
   *  Abstract : Realiza el envio de mails utilizando CDONTS
   *  Synopsis : Private Function p_Send_EMail(ByVal pFrom As String, ByVal pTo As String,
   *                             ByVal pSubject As String, ByVal pImportance As String,
   *                             ByVal pBodyFormat As String, ByVal pMailFormat As String,
   *                             ByVal pBody As String) As Boolean
   *  *****************************************************************
   */
  private boolean p_Send_EMail( String pvarFrom, String pvarTo, String pvarSubject, String pvarImportance, String pvarBodyFormat, String pvarMailFormat, String pvarBody, String pvarReplyTo )
  {
    boolean p_Send_EMail = false;
    Variant CdoMailFormatMime = new Variant();
    Variant CdoBodyFormatHTML = new Variant();
    CDONTS.NewMail wobjMail = new CDONTS.NewMail();
    //
    wobjMail = new CDONTS.NewMail();
    //
    try 
    {
      //
      wobjMail.From.set( pvarFrom );
      wobjMail.To.set( pvarTo );
      wobjMail.Value( "Reply-To" ) = pvarReplyTo;
      wobjMail.Subject.set( pvarSubject );
      wobjMail.Importance.set( pvarImportance );
      //pvarBodyFormat
      wobjMail.BodyFormat.set( CdoBodyFormatHTML );
      //wobjMail.MailFormat = pvarMailFormat
      wobjMail.MailFormat.set( CdoMailFormatMime );
      //wobjMail.ContentBase = "inline"
      //wobjMail.AttachURL App.Path + "\cam_OficinaVirtual\WAVES.GIF", "pirulo.gif"
      //wobjMail.AttachFile App.Path + "\cam_OficinaVirtual\WAVES.GIF", "prueba.gif"
      wobjMail.Body.set( pvarBody );
      wobjMail.send();
      //
      wobjMail = (CDONTS.NewMail) null;
      //
      p_Send_EMail = true;
      return p_Send_EMail;
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        p_Send_EMail = false;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return p_Send_EMail;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
