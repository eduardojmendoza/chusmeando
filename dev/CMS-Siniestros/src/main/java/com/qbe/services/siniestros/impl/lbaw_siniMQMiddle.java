package com.qbe.services.siniestros.impl;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.base64.Base64Encoding;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.framework.jaxb.Estado;
import com.qbe.vbcompat.framework.jaxb.Response;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.MQMWNamespaceResolver;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
/**
 * Envía un mensaje al servicio 4 ( coldview ) de MW via MQ, y devuelve un PDF
 */

public class lbaw_siniMQMiddle implements VBObjectClass
{
	
	/**
	 * Usuario, antes definido en el archivo de configuración de MQ
	 */
  private static final String USERID = "compusrdes";
  
	  protected static Logger logger = Logger.getLogger(lbaw_siniMQMiddle.class.getName());

  /**
   * Implementacion de los objetos
   */
  static final String gcteQueueManager = "//QUEUEMANAGER";
  static final String gctePutQueue = "//PUTQUEUE";
  static final String gcteGetQueue = "//GETQUEUE";
  static final String gcteGMOWaitInterval = "//GMO_WAITINTERVAL";
  static final String gcteClassMQConnection = "WD.Frame2MQ";
  public static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniMQMiddle";
  static final String mcteSubDirName = "DefinicionesXML/Denuncia";
  
private EventLog mobjEventLog = new EventLog();
  private String mvarConsultaRealizada = "";
  private boolean mvarCancelacionManual = false;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   */
  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarGMOWaitInterval = 0;
    int wvarStep = 0;
    int wvarx = 0;
    int wvarNroArch = 0;
    byte[] warrayDatos = null;
    String wvarDatosPDF = "";
    String wvarRaiz = "";
    String wvarInputMQ = "";
    String wvarError = "";
    String wvarTextoError = "";
    String wvarUsuario = "";
    String wvarDefinitionFile = "";
    String wvarConfFileName = "";
    String wvarTimeStamp = "";
    String wvarCDATA = "";
    String wvarLoguear = "";
    String wvarNomArchivo = "";
    String wvarTextoALoguear = "";
    String wvarExisteArchivo = "";
    String wvarHoraInicio = "";
    String wvarRespuestaFiltrada = "";
    String wvarPasada1 = "";
    String wvarPasada2 = "";
    float wvarTiempo = 0;
    XmlDomExtended wobjXMLDefinition = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLInputMQ = null;
    XmlDomExtended wobjXMLOutputMQ = null;
    XmlDomExtended wvarXMLPDF = null;
    org.w3c.dom.Element wobjNodoBase64 = null;
    org.w3c.dom.Element wobjNodo = null;
    org.w3c.dom.CDATASection wobjCdataNodo = null;
    org.w3c.dom.NodeList wobjNodosEntrada = null;
    org.w3c.dom.Element wobjXMLSobre = null;
    org.w3c.dom.Node wobjNodoEntrada = null;
    String wvarFaultCode = "";
    String wvarNodoPDF = "";
    String wvarNodoRaiz = "";
    String RespuestaMQ = "";
    int wvarMQError = 0;
    MQProxy wobjFrame2MQ = null;

    try 
    {
      wvarHoraInicio = DateTime.format( DateTime.now() );
      wvarTiempo = DateTime.timer();

      wvarStep = 10;

      // Crea Objetos XMLDom -----------------------------
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLDefinition = new XmlDomExtended();
      wobjXMLInputMQ = new XmlDomExtended();
      //-----------------------------------------------------
      wvarStep = 20;
      // Carga XML del Request ASP---------------------------
      wobjXMLRequest.loadXML( pvarRequest );
      //-----------------------------------------------------
      // Carga Nombre de Archivo de Definición------------
      if( ! (wobjXMLRequest.selectSingleNode( "//DEFINICION" )  == (org.w3c.dom.Node) null) )
      {
        wvarDefinitionFile = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DEFINICION" )  );
      }
      else
      {
        wvarTextoError = " No se encontró nodo DEFINICION en Request o XML mal formado";
        //FIXEDunsup GoTo ManejoError
        throw new Exception(wvarTextoError);
      }
      //----------------------------------------------------
      // Carga Archivo de Definición----------------------------
      wobjXMLDefinition.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteSubDirName + File.separator + wvarDefinitionFile ));
      wvarStep = 30;

      // Carga Nombre de Archivo de Definición MQ --------------------
      wvarConfFileName = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/MQCONFIGFILE" )  );
      wvarStep = 40;

      // Carga Tiempo de time-out MQ------------------------------
      wvarGMOWaitInterval = Obj.toInt( XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/TIMEOUT" )  ) );
      wvarStep = 50;

      // Carga nombre Archivo de Logueo
      wvarLoguear = "NO";

      if( ! (wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" )  == (org.w3c.dom.Node) null) )
      {
        wvarLoguear = "SI";
        if( ! (wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
        {
          wvarNomArchivo = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) );
        }
      }

      // Carga Nodo Sobre de la Definición--------------------------
      wvarStep = 60;
      wobjXMLSobre = (org.w3c.dom.Element) wobjXMLDefinition.selectSingleNode( "//XMLDEFINICION" ) .getFirstChild();

      wvarStep = 70;
      //Acá cambié la forma en que generaba el XML. Si explota ver el VB
      // Carga el Sobre en la variable de InputResponse a MQ---------------------
//    Uso este método prettyPrintComplete porque genera los namespaces en los nodos, en este caso en el share:sendPdfReport
      wvarInputMQ = XmlDomExtended.prettyPrintComplete(wobjXMLSobre);

      //De acá en adelante uso:
      //wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz, new MQMWNamespaceResolver() )
      // para que pueda resolver el caso de las expresiones xpath con forma share:sendPdfReport.
      // el MQMWNamespaceResolver tiene cableados los nombres de los NSs.
      // Para no cablearlo tanto podríamos usar un NamaespaceResolver que tomara la definición del doc, pero en este caso está fijo por config,
      // así que no es necesario.
      // Las alternativas están descriptas en http://www.ibm.com/developerworks/library/x-nmspccontext/
      
      wvarStep = 80;
      // carga Envelope de xml definicion
      wobjXMLInputMQ.loadXML( wvarInputMQ );
      //DONEunsup wobjXMLInputMQ.resolveExternals = true;

      // Carga los Parametros de Entrada desde la Definicion-------------
      wobjNodosEntrada = wobjXMLDefinition.selectNodes( "//ENTRADA/PARAMETRO" ) ;

      wvarStep = 90;

      // Recorre los Parametros----------------------------------------
      wvarCDATA = "NO";

      for( int nwobjNodoEntrada = 0; nwobjNodoEntrada < wobjNodosEntrada.getLength(); nwobjNodoEntrada++ )
      {
        wobjNodoEntrada = wobjNodosEntrada.item( nwobjNodoEntrada );

        //Que existan todos los nodos definidos, en el Request
        if( (wobjXMLRequest.selectSingleNode( ("//" + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) )) )  == (org.w3c.dom.Node) null) && (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == (org.w3c.dom.Node) null) )
        {

          wvarTextoError = " No se encontró nodo " + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) + " en Request";
          //FIXEDunsup GoTo ManejoError

          throw new Exception(wvarTextoError);
        }
        else
        {

          //Que Exista Nodo Raiz
          if( Strings.toUpperCase( XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "RAIZ" ) )
          {

            //MC - Agregado ----------------------------------------------------------
            if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == (org.w3c.dom.Node) null) )
            {

              wvarNodoRaiz = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) );

            }
            else
            {

              wvarNodoRaiz = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) )  );

            }
            //MC - Fin Agregado ------------------------------------------------------
            if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "CDATA" ) == (org.w3c.dom.Node) null) )
            {

              wvarCDATA = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "CDATA" ) );

            }

          }

        }

      }

      if( !wvarNodoRaiz.equals( "" ) )
      {

        for( int nwobjNodoEntrada = 0; nwobjNodoEntrada < wobjNodosEntrada.getLength(); nwobjNodoEntrada++ )
        {
          wobjNodoEntrada = wobjNodosEntrada.item( nwobjNodoEntrada );

          
          if( Strings.toUpperCase( XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "BASE64" ) )
          {

            wvarNodoPDF = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) );

            wvarDatosPDF = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" +
            XmlDomExtended.marshal(wobjXMLRequest.selectSingleNode( "//" + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) ) .getFirstChild());

            //FIXME Esto de convertir bytes es muy probable que NO ande. revisar
            // Redimensiona Array de Bytes para alojar datos a convertir a BASE64
            warrayDatos = new byte[Strings.len( wvarDatosPDF )+1];

            for( wvarx = 1; wvarx <= Strings.len( wvarDatosPDF ); wvarx++ )
            {

              warrayDatos[wvarx - 1] = (byte) Strings.asc( Strings.mid( wvarDatosPDF, wvarx, 1 ) );

            }

            wobjNodoBase64 = wobjXMLInputMQ.getDocument().createElement( wvarNodoPDF );

            wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz, new MQMWNamespaceResolver() ).appendChild( wobjNodoBase64 );

            wobjNodoBase64.setAttribute( "xmlns:dt", "urn:schemas-microsoft-com:datatypes" );

            //FIXEDunsup wobjNodoBase64.dataType = "bin.base64";

            //FIXEDunsup wobjNodoBase64.nodeTypedValue = warrayDatos;
            
            wobjNodoBase64.setTextContent(Base64Encoding.encode(warrayDatos));

            wobjNodoBase64 = (org.w3c.dom.Element) null;

          }
          else if( Strings.toUpperCase( XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "TEXTO" ) )
          {

            wvarNodoPDF = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) );

            if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == (org.w3c.dom.Node) null) )
            {
              wvarDatosPDF = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) );
            }
            else
            {
              wvarDatosPDF = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) )  );
            }

            wobjNodo = wobjXMLInputMQ.getDocument().createElement( wvarNodoPDF );
            wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz, new MQMWNamespaceResolver() ).appendChild( wobjNodo );
            //FIXEDunsup wobjNodo.nodeTypedValue = wvarDatosPDF;
            wobjNodo.setTextContent(wvarDatosPDF);

          }
          else if( Strings.toUpperCase( XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "XML" ) )
          {

            wvarNodoPDF = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) );

            if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == (org.w3c.dom.Node) null) )
            {
              wvarDatosPDF = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) );
            }
            else
            {
                wvarDatosPDF = XmlDomExtended.marshal(wobjXMLRequest.selectSingleNode( "//" + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) ) .getFirstChild());
            }
            //El MQMW tiene un fix acá. Si explota, revisarlo
            wobjNodo = wobjXMLInputMQ.getDocument().createElement( wvarNodoPDF );
            wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz, new MQMWNamespaceResolver() ).appendChild( wobjNodo );
            wobjNodo.setTextContent(wvarDatosPDF);

          }
          else
          {

          }
        }
      }

      wvarStep = 100;

      // Reemplaza nodo Raiz por un CDATA
      if( Strings.toUpperCase( wvarCDATA ).equals( "SI" ) )
      {
          wvarRaiz = XmlDomExtended.marshal(wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz, new MQMWNamespaceResolver() ));
        wobjCdataNodo = wobjXMLInputMQ.getDocument().createCDATASection( wvarRaiz );
        wobjNodo = (org.w3c.dom.Element) wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz, new MQMWNamespaceResolver() ) ;
        wobjXMLInputMQ.selectSingleNode( "//" + wvarNodoRaiz, new MQMWNamespaceResolver() ).getParentNode().replaceChild( wobjCdataNodo, wobjNodo );
      }

      wvarStep = 110;
      //---------------------------------------
      //Carga usuario ComPlus
      XmlDomExtended.setText( wobjXMLInputMQ.selectSingleNode( "//UserId" ) , USERID );
      //---------------------------------------
      wvarStep = 120;
      wvarTimeStamp = DateTime.year( DateTime.now() ) + "-" + Strings.right( ("00" + DateTime.month( DateTime.now() )), 2 ) + "-" + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) + "T" + Strings.right( ("00" + DateTime.hour( DateTime.now() )), 2 ) + ":" + Strings.right( ("00" + DateTime.minute( DateTime.now() )), 2 ) + ":" + Strings.right( ("00" + DateTime.second( DateTime.now() )), 2 ) + ".000000-03:00";
      XmlDomExtended.setText( wobjXMLInputMQ.selectSingleNode( "//MsgCreatTmsp" ) , wvarTimeStamp );

      wvarStep = 130;
      //Instancia y llama a MQ ---------------------------------
      wobjFrame2MQ = MQProxy.getInstance(wvarConfFileName);
      StringHolder shRespuestaMQ = new StringHolder(RespuestaMQ);
      String request = wobjXMLInputMQ.marshal();
      wvarMQError = wobjFrame2MQ.execute(request, shRespuestaMQ);
      RespuestaMQ = shRespuestaMQ.getValue();
      
      if( wvarMQError != 0 )
      {
        wvarStep = 131;

        pvarResponse.set(Response.marshaled(Estado.FALSE, "El servicio de consulta no se encuentra disponible", "Codigo Error:" + wvarMQError));

        //DA este código es para que aparezca un log en el APP en caso de haber problemas al conectar con MQ de Middleware
        mobjEventLog.Log("EventLog_Category.evtLog_Category_Logical", mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + RespuestaMQ + " Area:" + " Hora:" + DateTime.now(), vbLogEventTypeError );

        //DA - 26/09/2007: se quita este código ya que es un error controlado.
        //mobjCOM_Context.SetAbort
        //IAction_Execute = 1
      }
      else
      {
        wvarStep = 132;
        if( Strings.asc( Strings.right( RespuestaMQ, 1 ) ) == 0 )
        {
          wvarRespuestaFiltrada = Strings.left( RespuestaMQ, Strings.len( RespuestaMQ ) - 1 );
        }
        else
        {
          wvarRespuestaFiltrada = RespuestaMQ;
        }

        wvarStep = 133;
        wvarXMLPDF = new XmlDomExtended();
        wvarXMLPDF.loadXML( wvarRespuestaFiltrada );

        wvarStep = 134;
        if( ! (wvarXMLPDF.selectSingleNode( ("//" + Strings.replace( wvarNodoRaiz, "share:", "" ) + "Return") )  == (org.w3c.dom.Node) null) )
        {
          wvarPasada1 = "<PDF>" + XmlDomExtended.marshal( wvarXMLPDF.selectSingleNode( "//" + Strings.replace( wvarNodoRaiz, "share:", "" ) + "Return" )  ) + "</PDF>";
        }
        else
        {
          wvarPasada1 = "";
        }

        wvarStep = 135;
        if( ! (wvarXMLPDF.selectSingleNode( "//faultstring" )  == (org.w3c.dom.Node) null) )
        {
          wvarFaultCode = "<faultstring>" + XmlDomExtended.getText( wvarXMLPDF.selectSingleNode( "//faultstring" )  ) + "</faultstring>";
        }
        else
        {
          wvarFaultCode = "";
        }

        wvarStep = 136;
        pvarResponse.set("<Response><Estado resultado='true' />" + wvarPasada1 + wvarFaultCode + "</Response>");

        wvarStep = 137;
      }

      wvarStep = 140;

      if( wvarLoguear.equals( "SI" ) )
      {
    	  //FIXME Loguear con logger
//        wvarNroArch = FileSystem.getFreeFile();
//        wvarExisteArchivo = "";
//        wvarExisteArchivo = "" /*unsup this.Dir( System.getProperty("user.dir") + "\\LOG\\" + wvarNomArchivo, 0 ) */;
//
//        if( !wvarExisteArchivo.equals( "" ) )
//        {
//          FileSystem.kill( System.getProperty("user.dir") + "\\LOG\\" + wvarNomArchivo );
//        }

        //error: Opening files for 'Binary' access is not supported.
        //unsup: Open App.Path & "\LOG\" & wvarNomArchivo For Binary As wvarNroArch
        wvarTextoALoguear = "<LOGs><LOG InicioConsulta=\"" + wvarHoraInicio + "\" FinConsulta=\"" + DateTime.now() + "\" TiempoIncurrido=\"" + (DateTime.timer() - wvarTiempo) + " seg\"" + ">" + "<AreaIn>" + wobjXMLInputMQ.getDocument().getDocumentElement().toString() + "</AreaIn>" + "<AreaOut>" + RespuestaMQ + "</AreaOut>" + "</LOG></LOGs>";
        logger.log(Level.FINE, wvarTextoALoguear);
        /*unsup this.Put( wvarNroArch, 0, new Variant( wvarTextoALoguear ) ) */;
//        FileSystem.close();
      }

      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
    }
    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
        pvarResponse.set(Response.marshaled(Estado.FALSE, "El servicio de consulta no se encuentra disponible", "Codigo Error:3"));
		return 3;
	}
    catch( Exception _e_ )
    {
      Err.set( _e_ );
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/

        //FIXEDunsup Resume ClearObjects. No se necesita, el ClearObjects solamente nilea variables :S
        ManejoError: 

        wvarError = Err.getError().getDescription();

        String faultstring = "<faultstring>" + "Linea: " + wvarStep + "\nError: " + wvarError + "\nTextError: " + wvarTextoError + "</faultstring>";
        pvarResponse.set(Response.marshaledResultadoFalse(String.format("Exception en %s",this.getClass().getName()), faultstring));

        //FIXEDunsup GoTo ClearObjects. No se necesita, el ClearObjects solamente nilea variables :s
        Err.clear();
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   * http://msdn.microsoft.com/en-us/library/windows/desktop/ms678077(v=vs.85).aspx
   * 
   * Constant	Value	Description	
   * adTypeBinary	1	Indicates binary data.
   * adTypeText		2	Default. Indicates text data, which is in the character set specified by Charset.
   * 
   * Mode es permisos, no es relevante
   * 
   * Toma el pDatos que viene como parámetro.
   * Lo mete en un XML dentro del tag PDF ( es el string base64 ), con tipo bin.base64
   * Lo mete en el Stream, como binario
   * Resetea el Stream
   * Lo pasa a tipo "texto"
   * Retorna lo que metió, como texto
   * 
   * Creo que se podría reemplezar por un Base64Encoding.decode
   * 
   * No tiene uso actualmente
   * 
   */
  public String DecodificaBase64( String pDatos ) throws Exception
  {
    String DecodificaBase64 = "";
	  /*
    XmlDomExtended wobjXMLPdf = null;
    org.w3c.dom.Node wobjoNode = null;
    ADODB.Stream wobjStream = new ADODB.Stream();
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    wobjXMLPdf = new XmlDomExtended();
    wobjXMLPdf.loadXML( "<PDF xmlns:dt=\"urn:schemas-microsoft-com:datatypes\" dt:dt=\"bin.base64\">" + pDatos + "</PDF>" );

    wobjoNode = XmlDomExtended.Node_selectSingleNode(wobjXMLPdf.getDocument().getDocumentElement(), "//PDF" );

    wobjStream = new ADODB.Stream();
    wobjStream.Charset.set( "Windows-1252" );
    wobjStream.Mode.set( 0 );
    wobjStream.Type.set( 1 );
    wobjStream.Open();
    wobjStream.Write( wobjoNode.getNodeValue());
    wobjStream.Position.set( 0 );
    wobjStream.Type.set( 2 );
    DecodificaBase64 = wobjStream.ReadText.toString();
    wobjStream.Close();

    wobjXMLPdf = null;
    wobjoNode = (org.w3c.dom.Node) null;
    wobjStream = (ADODB.Stream) null;
	*/

    return DecodificaBase64;
  }

  public void Activate() throws Exception
  {

    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/


  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
