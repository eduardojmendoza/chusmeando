package com.qbe.services.siniestros.impl;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.db.AdoUtils;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.ado.AdoConst;
import diamondedge.ado.AdoError;
import diamondedge.ado.Command;
import diamondedge.ado.Connection;
import diamondedge.ado.Parameter;
import diamondedge.ado.Recordset;
import diamondedge.util.Err;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_siniGetLista implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  public static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniGetLista";
  
  protected static Logger logger = Logger.getLogger(lbaw_siniGetLista.class.getName());

  static final String mcteStoreProc = "P_SINI_SELECT_SINIESTRO_LISTA";

  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    String wvarResult = "";
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String mvarSINIEANNSQL = "";
    String mvarSINIENUMSQL = "";
    
    java.sql.Connection jdbcConn = null;
    java.sql.ResultSet cursor = null;
    //
    //Parámetros de entrada
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      wvarStep = 10;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();

      jdbcConn = connFactory.getJDBCConnection(ModGeneral.gcteDB);
      String sp = "{call " + mcteStoreProc + "()}";
      CallableStatement ps = jdbcConn.prepareCall(sp);
      
      boolean result = ps.execute();

      if (result) {
    	  //El primero es un ResultSet
      } else {
    	  //Salteo el "updateCount"
    	  int uc = ps.getUpdateCount();
    	  if (uc  == -1) {
  			throw new Exception("No puedo recuperar el ResultSet");
    	  }
    	  result = ps.getMoreResults();
      }

      cursor = ps.getResultSet();

      if( cursor.next())
      {
        //
        wvarStep = 140;
        wobjXMLResponse = new XmlDomExtended();
        wobjXSLResponse = new XmlDomExtended();
        //
        wvarStep = 150;
        wobjXSLResponse.loadXML( p_GetXSL());

        wvarStep = 150;
        wobjXMLResponse.load(AdoUtils.saveResultSet(cursor));

        //
        wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );

        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><LISTADO>" + wvarResult + "</LISTADO></Response>" );

        wvarStep = 180;
      }
      else
      {
        wvarMensaje = "No hay denuncias pendientes de ingresar al AIS / MIDDLEWARE";
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='" + wvarMensaje + "' /></Response>" );
      }

      wvarStep = 140;
      IAction_Execute = 0;

      wvarStep = 150;
      return IAction_Execute;
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
  		logger.log(Level.SEVERE, "en lbaw_siniGetLista", _e_);
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        throw new ComponentExecutionException(_e_);
    }
    finally {
    	try {
			if ( cursor != null) {
				cursor.close();
			}
		} catch (SQLException e) {
	  		logger.log(Level.WARNING, "al querer cerrar el cursor:", e);
		}
    	try {
			if ( jdbcConn != null) {
				jdbcConn.close();
			}
		} catch (SQLException e) {
	  		logger.log(Level.WARNING, "al querer cerrar el jdbcConn:", e);
		}
	}
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";

    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='DENUNCIA'>";

    wvarStrXSL = wvarStrXSL + "      <xsl:element name='RAMOPCOD'><xsl:value-of select='@RAMOPCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SINIENUMSQL'><xsl:value-of select='@SINIENUMSQL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SINIEANNAIS'><xsl:value-of select='@SINIEANNAIS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SINIENUMAIS'><xsl:value-of select='@SINIENUMAIS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NROFORM'><xsl:value-of select='@NROFORM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='GRABSFH'><xsl:value-of select='@GRABSFH' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ESTADODAT'><xsl:value-of select='@ESTADODAT' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='RECARGA'><xsl:value-of select='@RECARGA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='MARENVAIS'><xsl:value-of select='@MARENVAIS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ESTADOAIS'><xsl:value-of select='@ESTADOAIS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='MARENVMID'><xsl:value-of select='@MARENVMID' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ESTADOMID'><xsl:value-of select='@ESTADOMID' /></xsl:element>";

    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    //wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='XMLDatosAIS'/>"
    //wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='XMLDatosMID'/>"
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";

    p_GetXSL = wvarStrXSL;

    return p_GetXSL;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
