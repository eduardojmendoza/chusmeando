package com.qbe.services.siniestros.impl;
import java.sql.CallableStatement;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_siniUpdMenvMID implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  public static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniUpdMenvMID";

  protected static Logger logger = Logger.getLogger(lbaw_siniUpdMenvMID.class.getName());

  static final String mcteStoreProc = "P_SINI_UPD_SINIESTRO_MARENVMID";
  
  static final String mcteParam_SINIENUMSQL = "//SINIENUMSQL";
  
  static final String mcteParam_MARENVMID = "//MARENVMID";
  
  static final String mcteParam_ESTADOMID = "//ESTADOMID";
  
  private EventLog mobjEventLog = new EventLog();

  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    int wvarStep = 0;
    String wvarMensaje = "";
    String mvarSINIENUMSQL = "";
    String mvarMARENVMID = "";
    String mvarESTADOMID = "";
    java.sql.Connection jdbcConn = null;
    java.sql.ResultSet cursor = null;
    //
    //
    //Parámetros de entrada
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 20;
      mvarSINIENUMSQL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINIENUMSQL )  );
      mvarMARENVMID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MARENVMID )  );
      mvarESTADOMID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ESTADOMID )  );

      wvarStep = 30;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      jdbcConn = connFactory.getJDBCConnection(ModGeneral.gcteDB);
      
      String sp = "{? = call " + mcteStoreProc + "(?,?,?)}";
      CallableStatement ps = jdbcConn.prepareCall(sp);
      int posiSP = 1;
      ps.registerOutParameter(posiSP++, Types.INTEGER);

//      wobjDBParm = new Parameter( "@SINIENUMSQL", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarSINIENUMSQL ) );
      ps.setInt(posiSP++, Integer.parseInt(mvarSINIENUMSQL));

//      wobjDBParm = new Parameter( "@MARENVMID", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarMARENVMID ) );
      ps.setString(posiSP++, mvarMARENVMID);

//      wobjDBParm = new Parameter( "@ESTADOMID", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( mvarESTADOMID ) );
      ps.setString(posiSP++, mvarESTADOMID);

      logger.log(Level.FINE, String.format("Por ejecutar: {? = call " + mcteStoreProc + "(%s,%s,%s)}",mvarSINIENUMSQL, mvarMARENVMID, mvarESTADOMID));
      boolean result = ps.execute();
      
      if (result) {
    	  //El primero es un ResultSet
      } else {
    	  //Salteo el "updateCount"
    	  int uc = ps.getUpdateCount();
    	  if (uc  == -1) {
    		  //System.out.println("No hay resultados");
    	  }
    	  result = ps.getMoreResults();
      }
      cursor = ps.getResultSet();

      //Controlamos la respuesta del SQL
      if( cursor != null && cursor.next())
      {
      	int rtado = cursor.getInt(1);
        if( rtado >= 0 )
        {
          pvarResponse.set( "<Response><Estado resultado=\"true\"  mensaje=\"\" /></Response>" );
        }
        else
        {
          wvarMensaje = "Error al actualizar la MARENVMID en la tabla SQL SINI_SINIESTROS";
          pvarResponse.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        }

      } else {
          wvarMensaje = "Error al actualizar la MARENVMID en la tabla SQL SINI_SINIESTROS: Sin respuesta";
          if ( cursor == null ) {
        	  wvarMensaje = wvarMensaje + ". cursor es NULL";
          }
          pvarResponse.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
      }
      

      wvarStep = 100;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      wvarStep = 110;
      if ( cursor != null ) cursor.close();
      if ( jdbcConn != null ) jdbcConn.close();
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        throw new ComponentExecutionException(_e_);
    }
    finally {
		try {
			if (jdbcConn != null)
				jdbcConn.close();
		} catch (Exception e) {
			logger.log(Level.WARNING, "Exception al hacer un close", e);
		}
	}
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
