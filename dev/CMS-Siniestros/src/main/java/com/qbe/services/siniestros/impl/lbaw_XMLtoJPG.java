package com.qbe.services.siniestros.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.qbe.vbcompat.base64.Base64Encoding;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

public class lbaw_XMLtoJPG implements VBObjectClass {
	public static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_XMLtoJPG";
	private static final int TOLERANCIA = 15;

	protected static Logger logger = Logger.getLogger(lbaw_XMLtoJPG.class.getName());

	public int IAction_Execute(String pvarRequest, StringHolder pvarResponse, String pvarContextInfo) {
		XmlDomExtended xmlRequest = new XmlDomExtended();
		XmlDomExtended xmlResponse = new XmlDomExtended();
		int IAction_Execute = 0;
		try {
			xmlRequest.loadXML(pvarRequest);
			Color transparente = getColorTransparente(xmlRequest);

			String plantilla = XmlDomExtended.getText(xmlRequest.selectSingleNode("//croquis/@mapa"));
			NodeList imagenNodos = xmlRequest.selectNodes("//objetos/imagen");

			BufferedImage imagenPlantilla = getImagenPlantilla(plantilla);
			List<Imagen> imagenes = obtenerImagenesConAlpha(imagenNodos, transparente);

			Graphics2D graphicsPlantila = (Graphics2D) imagenPlantilla.getGraphics();
			for (Imagen imagen : imagenes) {
				graphicsPlantila.drawImage(imagen.getImage(), imagen.getX(), imagen.getY(), null);
			}

			xmlResponse.loadXML("<Response><Estado resultado='true' mensaje='' /></Response>");

			String base64Image = getBase64Image(imagenPlantilla);

			Node binDataNode = xmlResponse.getDocument().createElement("BinData");
			xmlResponse.selectSingleNode("//Response").appendChild(binDataNode);
			XmlDomExtended.setText(binDataNode, base64Image);

			pvarResponse.set(xmlResponse.marshal());
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			xmlResponse.loadXML("<Response><Estado resultado='false' mensaje='" + e.getMessage() + "' /></Response>");
			try {
				pvarResponse.set(xmlResponse.marshal());
			} catch (XmlDomExtendedException e1) {
				logger.log(Level.SEVERE, e1.getMessage(), e1);
			}
			IAction_Execute = 1;
		}

		return IAction_Execute;
	}

	private BufferedImage getImagenPlantilla(String nombre) throws IOException {
		BufferedImage bi = ImageIO
				.read(this.getClass().getResourceAsStream("/Images/" + nombre.toLowerCase() + ".bmp"));
		BufferedImage biAlpha = new BufferedImage(bi.getWidth(), bi.getHeight(), BufferedImage.TYPE_INT_ARGB);
		biAlpha.getGraphics().drawImage(bi, 0, 0, null);
		return biAlpha;
	}

	private Color getColorTransparente(XmlDomExtended wobjXMLRequest) throws Exception {

		NamedNodeMap transparenciaAtt = wobjXMLRequest.selectSingleNode("//transparencia").getAttributes();
		int lngRed = Integer.parseInt(transparenciaAtt.getNamedItem("rojo").getNodeValue());
		int lngGreen = Integer.parseInt(transparenciaAtt.getNamedItem("verde").getNodeValue());
		int lngBlue = Integer.parseInt(transparenciaAtt.getNamedItem("azul").getNodeValue());

		return new Color(lngRed, lngGreen, lngBlue, 0);
	}

	private List<Imagen> obtenerImagenesConAlpha(NodeList imagenNodes, Color alphaColor) {
		List<Imagen> imagenes = new ArrayList<Imagen>();
		for (int i = 0; i < imagenNodes.getLength(); i++) {
			Node imagenNode = imagenNodes.item(i);
			NamedNodeMap attibutes = imagenNode.getAttributes();
			Imagen imagen = new Imagen(Integer.parseInt(attibutes.getNamedItem("x").getNodeValue()),
					Integer.parseInt(attibutes.getNamedItem("y").getNodeValue()), attibutes.getNamedItem("archivo")
							.getNodeValue().toLowerCase(), alphaColor);
			cargarImagenConAlpha(imagen);
			imagenes.add(imagen);
		}
		return imagenes;
	}

	private Imagen cargarImagenConAlpha(Imagen imagen) {
		try {
			BufferedImage bi = ImageIO.read(this.getClass().getResourceAsStream(
					"/Images/" + imagen.getNombre() + ".bmp"));
			BufferedImage biAlpha = convert(bi, BufferedImage.TYPE_INT_ARGB);
			setAlpha(biAlpha, imagen.getAlpha(), TOLERANCIA);
			imagen.setImage(biAlpha);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return imagen;
	}

	private String getBase64Image(BufferedImage image) throws IOException {

		BufferedImage resultImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
		resultImage.getGraphics().drawImage(image, 0, 0, null);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ImageIO.write(resultImage, "jpg", outputStream);
		outputStream.flush();
		return Base64Encoding.encode(outputStream.toByteArray());
	}

	private BufferedImage convert(BufferedImage src, int bufImgType) {
		BufferedImage img = new BufferedImage(src.getWidth(), src.getHeight(), bufImgType);
		Graphics2D g2d = img.createGraphics();
		g2d.drawImage(src, 0, 0, null);
		g2d.dispose();
		return img;
	}

	private void setAlpha(BufferedImage imagen, Color colorAlpha, int tolerancia) {
		for (int cx = 0; cx < imagen.getWidth(); cx++) {
			int redAlpha = colorAlpha.getRed();
			int greenAlpha = colorAlpha.getGreen() << 8;
			int blueAlpha = colorAlpha.getBlue() << 16;
			for (int cy = 0; cy < imagen.getHeight(); cy++) {
				int color = imagen.getRGB(cx, cy);
				int diference = Math.abs(0x000000ff - (color & redAlpha));
				diference += Math.abs(0x000000ff - ((color & greenAlpha) >> 8));
				diference += Math.abs(0x000000ff - ((color & blueAlpha) >> 16));
				if (diference <= tolerancia) {
					int newcolor = color & 0x00ffffff;
					imagen.setRGB(cx, cy, newcolor);
				}
			}

		}
	}

	private class Imagen {
		private int x;
		private int y;
		private String nombre;
		private Color alpha;
		private BufferedImage image;

		public Imagen() {
		}

		public Imagen(int x, int y, String nombre, Color alpha) {
			super();
			this.x = x;
			this.y = y;
			this.nombre = nombre;
			this.alpha = alpha;
		}

		public void setImage(BufferedImage image) {
			this.image = image;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

		public String getNombre() {
			return nombre;
		}

		public Color getAlpha() {
			return alpha;
		}

		public BufferedImage getImage() {
			return image;
		}
	}
}
