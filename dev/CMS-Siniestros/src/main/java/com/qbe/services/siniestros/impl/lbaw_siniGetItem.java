package com.qbe.services.siniestros.impl;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_siniGetItem implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  public static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniGetItem";
  
  protected static Logger logger = Logger.getLogger(lbaw_siniGetItem.class.getName());

  static final String mcteStoreProc = "P_SINI_SELECT_SINIESTRO_ITEM";
  static final String mcteParam_SINIENUMSQL = "//SINIENUMSQL";

  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    String wvarResXMLDatosAIS = "";
    String wvarResXMLDatosMID = "";
    String wvarResult = "";
    int wvarStep = 0;
    String wvarMensaje = "";
    String mvarSINIENUMSQL = "";
    java.sql.Connection jdbcConn = null;
    java.sql.ResultSet cursor = null;
    
    //
    //
    //Parámetros de entrada
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 20;
      mvarSINIENUMSQL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINIENUMSQL )  );

      wvarStep = 30;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      jdbcConn = connFactory.getJDBCConnection(ModGeneral.gcteDB);
      
      String sp = "{call " + mcteStoreProc + "(?)}";
      CallableStatement ps = jdbcConn.prepareCall(sp);
      int posiSP = 1;

//      wobjDBParm = new Parameter( "@SINIENUMSQL", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarSINIENUMSQL ) );
      ps.setInt(posiSP++, Integer.parseInt(mvarSINIENUMSQL));

      boolean result = ps.execute();
      
      if (result) {
    	  //El primero es un ResultSet
      } else {
    	  //Salteo el "updateCount"
    	  int uc = ps.getUpdateCount();
    	  if (uc  == -1) {
    		  //System.out.println("No hay resultados");
    	  }
    	  result = ps.getMoreResults();
      }
      cursor = ps.getResultSet();


      if( cursor.next())
      {
        //
        wvarStep = 150;
        
//      wvarResXMLDatosAIS = wrstDBResult.getField("XMLDatosAIS").getValue().toString();
//      wvarResXMLDatosMID = wrstDBResult.getField("XMLDatosMID").getValue().toString();
        wvarResXMLDatosAIS = cursor.getString("XMLDatosAIS");
        wvarResXMLDatosMID = cursor.getString("XMLDatosMID");
        
        //
        wvarResult = wvarResXMLDatosAIS + wvarResXMLDatosMID;
        //
        pvarResponse.set( "<Response><Estado resultado=\"true\" mensaje=\"\"  />" + wvarResult + "</Response>" );
        //
        wvarStep = 180;

      }
      else
      {
        wvarMensaje = "No se encontró el item solicitado en la base de SQL";
        pvarResponse.set( "<Response><Estado resultado=\"false\" mensaje=\""+ wvarMensaje + " \" /></Response>" );
      }


      IAction_Execute = 0;

      wvarStep = 110;
      cursor.close();
      jdbcConn.close();
      return IAction_Execute;
    }
    catch( Exception _e_ )
    {
        Err.set( _e_ );
          mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
          throw new ComponentExecutionException(_e_);
    }
	  finally {
		try {
			if (jdbcConn != null)
				jdbcConn.close();
		} catch (Exception e) {
			logger.log(Level.WARNING, "Exception al hacer un close", e);
		}
  	}
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjEventLog = null;
    //
  }
}
