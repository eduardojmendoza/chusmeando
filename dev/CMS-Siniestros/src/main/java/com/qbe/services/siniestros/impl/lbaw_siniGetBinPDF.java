package com.qbe.services.siniestros.impl;
import com.qbe.services.ovmqemision.impl.lbaw_OVGetBinaryFile;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;

public class lbaw_siniGetBinPDF implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniGetBinPDF";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {

	  lbaw_OVGetBinaryFile gbf = new lbaw_OVGetBinaryFile();
	  return gbf.IAction_Execute(Request,Response,ContextInfo);
  }

  public void Activate() throws Exception
  {
  }

  public boolean CanBePooled() throws Exception
  {
	  return true;
  }

  public void Deactivate() throws Exception
  {
  }
}
