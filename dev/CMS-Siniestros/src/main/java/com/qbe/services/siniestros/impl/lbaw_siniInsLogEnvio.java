package com.qbe.services.siniestros.impl;
import java.sql.Types;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.ado.AdoConst;
import diamondedge.ado.Command;
import diamondedge.ado.Connection;
import diamondedge.ado.Parameter;
import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_siniInsLogEnvio implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  public static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniInsLogEnvio";

  protected static Logger logger = Logger.getLogger(lbaw_siniInsLogEnvio.class.getName());

  static final String mcteStoreProc = "P_SINI_INSERT_LOGENVIO";
  /**
   * 
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_SINIEANNAIS = "//SINIEANNAIS";
  static final String mcteParam_SINIENUMAIS = "//SINIENUMAIS";
  static final String mcteParam_SINIENUMSQL = "//SINIENUMSQL";
  static final String mcteParam_ENVIADOAIS = "//ENVIADOAIS";
  static final String mcteParam_ENVIADOMID = "//ENVIADOMID";
  static final String mcteParam_ESTADOS = "//ESTADOS";

  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    int wvarStep = 0;
    String wvarMensaje = "";
    String mvarRAMOPCOD = "";
    String mvarSINIEANNAIS = "";
    String mvarSINIENUMAIS = "";
    String mvarSINIENUMSQL = "";
    String mvarENVIADOAIS = "";
    String mvarENVIADOMID = "";
    String mvarXMLErrores = "";
    //
    //
    //Parámetros de entrada
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarRAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  );
      wvarStep = 21;
      mvarSINIEANNAIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINIEANNAIS )  );
      if( Strings.trim( mvarSINIEANNAIS ).equals( "" ) )
      {
        mvarSINIEANNAIS = "0";
      }
      wvarStep = 22;
      mvarSINIENUMAIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINIENUMAIS )  );
      if( Strings.trim( mvarSINIENUMAIS ).equals( "" ) )
      {
        mvarSINIENUMAIS = "0";
      }
      wvarStep = 23;
      mvarSINIENUMSQL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SINIENUMSQL )  );
      wvarStep = 24;
      mvarENVIADOAIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ENVIADOAIS )  );
      wvarStep = 25;
      mvarENVIADOMID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ENVIADOMID )  );
      wvarStep = 26;
      mvarXMLErrores = wobjXMLRequest.selectSingleNode( mcteParam_ESTADOS ) .toString();
      //
      wvarStep = 30;
      
      final MapSqlParameterSource params = new MapSqlParameterSource();
	     
//      wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
//      wobjDBCmd.setActiveConnection( wobjDBCnn );
//      wobjDBCmd.setCommandText( mcteStoreProc );
//      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      
	  SimpleJdbcCall call = new SimpleJdbcCall(JDBCConnectionFactory.getDataSourceForUDLName(ModGeneral.gcteDB))
	    .withProcedureName(mcteStoreProc)

//      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( "0" ) );
//      wobjDBCmd.getParameters().append( wobjDBParm );
//      wobjDBParm = (Parameter) null;
	    .withReturnValue()
	    .declareParameters(new SqlOutParameter("RETURN_VALUE", Types.INTEGER));
	  

//	  wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarRAMOPCOD ) );
//      wobjDBCmd.getParameters().append( wobjDBParm );
//      wobjDBParm = (Parameter) null;
	    call.declareParameters(new SqlParameter("RAMOPCOD", Types.CHAR, 6));
	    params.addValue("RAMOPCOD", mvarRAMOPCOD);
	    
//      wobjDBParm = new Parameter( "@SINIEANNAIS", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarSINIEANNAIS ) );
//      wobjDBCmd.getParameters().append( wobjDBParm );
//      wobjDBParm = (Parameter) null;
	    call.declareParameters(new SqlParameter("SINIEANNAIS", Types.INTEGER));
	    params.addValue("SINIEANNAIS", mvarSINIEANNAIS);

//      wobjDBParm = new Parameter( "@SINIENUMAIS", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarSINIENUMAIS ) );
//      wobjDBCmd.getParameters().append( wobjDBParm );
//      wobjDBParm = (Parameter) null;
	    call.declareParameters(new SqlParameter("SINIENUMAIS", Types.INTEGER));
	    params.addValue("SINIENUMAIS", mvarSINIENUMAIS);

//      wobjDBParm = new Parameter( "@SINIENUMSQL", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarSINIENUMSQL ) );
//      wobjDBCmd.getParameters().append( wobjDBParm );
//      wobjDBParm = (Parameter) null;
	    call.declareParameters(new SqlParameter("SINIENUMSQL", Types.INTEGER));
	    params.addValue("SINIENUMSQL", mvarSINIENUMSQL);

//      wobjDBParm = new Parameter( "@ENVIADOAIS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarENVIADOAIS ) );
//      wobjDBCmd.getParameters().append( wobjDBParm );
//      wobjDBParm = (Parameter) null;
	    call.declareParameters(new SqlParameter("ENVIADOAIS", Types.CHAR, 1));
	    params.addValue("ENVIADOAIS", mvarENVIADOAIS);

//      wobjDBParm = new Parameter( "@ENVIADOMID", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarENVIADOMID ) );
//      wobjDBCmd.getParameters().append( wobjDBParm );
//      wobjDBParm = (Parameter) null;
	    call.declareParameters(new SqlParameter("ENVIADOMID", Types.CHAR, 1));
	    params.addValue("ENVIADOMID", mvarENVIADOMID);

//      wobjDBParm = new Parameter( "@XMLErrores", AdoConst.adChar, AdoConst.adParamInput, 4000, new Variant( mvarXMLErrores ) );
//      wobjDBCmd.getParameters().append( wobjDBParm );
//      wobjDBParm = (Parameter) null;
	    call.declareParameters(new SqlParameter("XMLErrores", Types.CHAR, 4000));
	    params.addValue("XMLErrores", mvarXMLErrores);

//      //
//      wvarStep = 120;
//      wobjDBCmd.execute();
	    Map<String, Object> out = call.execute(params);

	    Object returnValueParam = out.get("RETURN_VALUE");
	    boolean returnValue = Integer.parseInt(returnValueParam.toString()) >= 0;
      //
      wvarStep = 130;
      //Controlamos la respuesta del SQL
      if ( returnValue )
      {
        pvarResponse.set( "<Response><Estado resultado=\"true\" mensaje=\"\" /></Response>" );
      }
      else
      {
        wvarMensaje = "Error al insertar en la tabla SQL SINI_LOGENVIO";
        pvarResponse.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
      }
      //
      wvarStep = 140;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "mvarXMLErrores: " + mvarXMLErrores + "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        throw new ComponentExecutionException(_e_);
    }
    finally {

	}
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
