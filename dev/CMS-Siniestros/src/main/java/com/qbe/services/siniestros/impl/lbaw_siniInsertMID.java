package com.qbe.services.siniestros.impl;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.NodeList;

import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.base64.Base64Encoding;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_siniInsertMID implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  public static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniInsertMID";
  
  protected static Logger logger = Logger.getLogger(lbaw_siniInsertMID.class.getName());

  /**
   * 
   * Constantes de párametros de Entrada
   */
  static final String mcteParam_XMLDatosMID = "//xmlDataSource";
  static final String mcteParam_Metodo = "//Metodo";
  static final String mcteParam_ColdviewMetadata = "//ColdviewMetadata";
  static final String mcteParam_EmailTo = "//EmailTo";
  /**
   * DA - 27/10/2008
   */
  static final String mcteParam_recipientsCC = "//recipientsCC";
  static final String mcteParam_PDFPWD = "//PDFPWD";
  static final String mcteParam_DevuelvePDF = "//DevuelvePDF";
  static final String mcteParam_ReportId = "//ReportId";
  /**
   * 
   * 26/05/2008 - DA
   */
  static final String mcteTempFilesServer = "LBAVirtual_TempFilesServer.xml";
  /**
   * Carpeta donde se encuentran los Templates de XML
   */
  static final String mcteSubDirName = "";

  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    String wvarRequest = "";
    String wvarResponse = "";
    XmlDomExtended wobjXMLResponseMID = null;
    XmlDomExtended wobjXMLRequest = null;
    VBObjectClass wobjClass = null;
    int wvarStep = 0;
    String wvarEstadoMID = "";
    String wvarDefinirDefaultONo = "";
    XmlDomExtended wvarXMLDatosMID = null;
    String wvarMetodo = "";
    String wvarColdviewMetadata = "";
    String wvarEmailTo = "";
    String wvarRecipientsCC = "";
    String wvarPDFPWD = "";
    String wvarDevuelvePDF = "";
    String wvarMIDFaultCode = "";
    String wvarReportId = "";
    String mvarRutaDestino = "";
    XmlDomExtended wobjXMLTempFilesServer = null;
    String wvarRutaPDF = "";
    //
    //
    //Parámetros de entrada
    //DA - 24/10/2008
    //26/05/2008 - DA: se toma desde el archivo LBAVirtual_TempFilesServer.xml
    //Parámetros de salida
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      //Obtiene los valores de los parámetros
      wvarStep = 20;
      wvarStep = 21;
      wvarXMLDatosMID = new XmlDomExtended();
      wvarXMLDatosMID.loadXML( XmlDomExtended.marshal(wobjXMLRequest.selectSingleNode( mcteParam_XMLDatosMID )));
      wvarStep = 22;
      wvarMetodo = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Metodo )  );
      wvarStep = 23;
      wvarColdviewMetadata = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ColdviewMetadata )  );
      wvarStep = 24;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_EmailTo )  == (org.w3c.dom.Node) null) )
      {
        wvarEmailTo = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EmailTo )  );
      }
      else
      {
        wvarEmailTo = "";
      }
      //DA - 27/10/2008
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_recipientsCC )  == (org.w3c.dom.Node) null) )
      {
        wvarRecipientsCC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_recipientsCC )  );
      }
      else
      {
        wvarRecipientsCC = "";
      }


      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_PDFPWD )  == (org.w3c.dom.Node) null) )
      {
        wvarPDFPWD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PDFPWD )  );
      }
      else
      {
        wvarPDFPWD = "";
      }

      wvarStep = 23;
      wvarDevuelvePDF = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DevuelvePDF )  );
      wvarReportId = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ReportId )  );

      //Si XMLDatosMID tiene CROQUIS convertirlos a BASE64 e insertarlos en el XMLIN
      wvarStep = 70;
      ProcesarCroquis(wvarXMLDatosMID);

      //Enviar el xml generado a MIDDLEWARE
      wvarStep = 110;
      wvarRequest = "<Request>";
      wvarRequest = wvarRequest + "<DEFINICION>" + wvarMetodo + "</DEFINICION>";
      //XmlDataSource
      wvarRequest = wvarRequest + XmlDomExtended.marshal(wvarXMLDatosMID.getDocument().getDocumentElement());
      wvarRequest = wvarRequest + "<coldViewMetadata>" + wvarColdviewMetadata + "</coldViewMetadata>";
      wvarRequest = wvarRequest + "<reportId>" + wvarReportId + "</reportId>";
      wvarRequest = wvarRequest + "<recipientTO>" + wvarEmailTo + "</recipientTO>";
      //DA - 27/10/2008
      wvarRequest = wvarRequest + "<recipientsCC>" + wvarRecipientsCC + "</recipientsCC>";
      wvarRequest = wvarRequest + "<attachPassword>" + wvarPDFPWD + "</attachPassword>";
      wvarRequest = wvarRequest + "</Request>";

      wobjClass = new lbaw_siniMQMiddle();
      StringHolder shMid = new StringHolder();
      wobjClass.IAction_Execute(wvarRequest, shMid, "" );
      wvarResponse = shMid.getValue();

      //Analizar devolucion de MIDDLEWARE
      wvarStep = 120;
      wobjXMLResponseMID = new XmlDomExtended();
      wobjXMLResponseMID.loadXML( wvarResponse );

      if( ! (wobjXMLResponseMID.selectSingleNode( "//Response" )  == (org.w3c.dom.Node) null) )
      {
        if( Strings.toUpperCase( XmlDomExtended.getText( wobjXMLResponseMID.selectSingleNode( "//Response/Estado/@resultado" )  ) ).equals( "TRUE" ) )
        {
          //Analisis de posibles errores de MIDDLEWARE
          if( ! (wobjXMLResponseMID.selectSingleNode( "//faultstring" )  == (org.w3c.dom.Node) null) ) //FIXME OJO asegurarse que vuelva el faultstring
          {
            wvarMIDFaultCode = XmlDomExtended.getText( wobjXMLResponseMID.selectSingleNode( "//faultstring" )  );
            if( Strings.find( 1, wvarMIDFaultCode, "E1", true ) > 0 )
            {
              //Fallo EMAIL
              wvarEstadoMID = "E";
            }
            else if( Strings.find( 1, wvarMIDFaultCode, "E2", true ) > 0 )
            {
              //Fallo COLDVIEW
              wvarEstadoMID = "C";
            }
            else if( Strings.find( 1, wvarMIDFaultCode, "E3", true ) > 0 )
            {
              //Fallo COLDVIEW y EMAIL
              wvarEstadoMID = "CE";
            }
            else if( Strings.find( 1, wvarMIDFaultCode, "E4", true ) > 0 )
            {
              //Fallo PDF
              wvarEstadoMID = "P";
            }
            else if( Strings.find( 1, wvarMIDFaultCode, "E5", true ) > 0 )
            {
              //Fallo PDF y EMAIL
              wvarEstadoMID = "PE";
            }
            else if( Strings.find( 1, wvarMIDFaultCode, "E6", true ) > 0 )
            {
              //Fallo PDF y COLDVIEW
              wvarEstadoMID = "PC";
            }
            else if( Strings.find( 1, wvarMIDFaultCode, "E7", true ) > 0 )
            {
              //Fallo PDF y COLDVIEW y EMAIL
              wvarEstadoMID = "PCE";
            }
            else
            {
              //Fallo no registrado, se asume fallo de middleware
              wvarEstadoMID = "PCE";
            }
          }
          else
          {
            //GENERADO OK, no se retorna valor
            wvarEstadoMID = "";
          }
        }
        else
        {
          //Errores en la ejecucion del componente de middleware, se asume falla de middleware
          wvarEstadoMID = "PCE";
        }
      }
      else
      {
        //Errores en la ejecucion del componente de middleware, se asume falla de middleware
        wvarEstadoMID = "PCE";
      }
      //
      //26/05/2008 - DA: busco la ruta de destino del PDF (para grabación temporal)
      wvarStep = 130;
      wobjXMLTempFilesServer = new XmlDomExtended();
      wobjXMLTempFilesServer.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteTempFilesServer));

      mvarRutaDestino = XmlDomExtended.getText( wobjXMLTempFilesServer.selectSingleNode( "//PATH" )  );

      //Si se pidió la devolucion del PDF generado, persistir el PDF en un archivo
      wvarStep = 140;
      if( (wvarDevuelvePDF.equals( "S" )) && (wvarEstadoMID.equals( "" )) )
      {
        if( ! (wobjXMLResponseMID.selectSingleNode( "//PDF" )  == (org.w3c.dom.Node) null) )
        {
          //26/05/2008 - DA: se agrega el grabado en un fileserver
          //wvarRutaPDF = obtenerNombreRandom(App.Path & "\PDF\", "PDF")
          wvarRutaPDF = obtenerNombreRandom(mvarRutaDestino + "/pdf/","PDF");
          SaveBinaryData(wvarRutaPDF,wobjXMLResponseMID.selectSingleNode( "//PDF").getTextContent());
        }
        else
        {
          wvarRutaPDF = "";
        }
      }
      else
      {
        wvarRutaPDF = "";
      }

      //Setear el valor de los parametros a devolver (ruta del PDF)
      wvarStep = 150;
      
      String urlRutaPDF = wvarRutaPDF.length() > 0 ? com.qbe.services.ovmqemision.Constants.CMS_PREFIX + wvarRutaPDF : wvarRutaPDF;
      pvarResponse.set( "<Response><Estado resultado=\"true\"  mensaje=\"\" /><RUTAPDF>" + urlRutaPDF + "</RUTAPDF><ESTADOMID>" + wvarEstadoMID + "</ESTADOMID></Response>" );

      wvarStep = 170;
      wobjXMLRequest = null;
      wobjXMLResponseMID = null;
      IAction_Execute = 0;

      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      logger.log(Level.SEVERE,"Exception:",_e_);
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        throw new ComponentExecutionException(_e_);
      }
  }

  private void ProcesarCroquis( XmlDomExtended pvarXMLDatosMID ) throws Exception
  {
    org.w3c.dom.Element wobjNodo64 = null;
    org.w3c.dom.Node wobjNodoCroquis = null;
    org.w3c.dom.Element wobjAUX = null;
    //
    wobjAUX = pvarXMLDatosMID.getDocument().createElement( "IMAGES" );
    XmlDomExtended.nodeImportAsChildNode(pvarXMLDatosMID.selectSingleNode( "//XMLIN" ),wobjAUX );
    //
    //Croquis y bollos del vehiculo asegurado
    NodeList croquis = pvarXMLDatosMID.selectNodes( "//XMLIN/croquis" );
    for( int nwobjNodoCroquis = 0; nwobjNodoCroquis < croquis.getLength(); nwobjNodoCroquis++ )
    {
      wobjNodoCroquis = croquis.item(nwobjNodoCroquis);
      //
      //Armo el Base 64 de cada croquis
      wobjNodo64 = fncXMLtoJPG(XmlDomExtended.marshal(wobjNodoCroquis), XmlDomExtended.getText( wobjNodoCroquis.getAttributes().getNamedItem( "tipo") ) );
      //
      if( ! (wobjNodo64 == (org.w3c.dom.Element) null) )
      {
        //Lo apendeo al XML que viajará al Middleware
    	XmlDomExtended.nodeImportAsChildNode(pvarXMLDatosMID.selectSingleNode( "//XMLIN/IMAGES" ),wobjNodo64 );
      }
      pvarXMLDatosMID.selectSingleNode( "//XMLIN" ).removeChild( pvarXMLDatosMID.selectSingleNode( "//XMLIN/croquis[@tipo='" + XmlDomExtended.getText( wobjNodoCroquis.getAttributes().getNamedItem( "tipo" ) ) + "']" )  );
    }

    //Bollos de OTROS vehiculos involucrados
    croquis = pvarXMLDatosMID.selectNodes( "//VEHICULO/croquis" );
    for( int nwobjNodoCroquis = 0; nwobjNodoCroquis < croquis.getLength(); nwobjNodoCroquis++ )
    {
      wobjNodoCroquis = croquis.item(nwobjNodoCroquis);
      //
      //Armo el Base 64 de cada croquis
      wobjNodo64 = fncXMLtoJPG(XmlDomExtended.marshal(wobjNodoCroquis), XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjNodoCroquis.getParentNode(), "ID") ));
      //
      if( ! (wobjNodo64 == (org.w3c.dom.Element) null) )
      {
        //Lo apendeo al XML que viajará al Middleware
    	XmlDomExtended.nodeImportAsChildNode(pvarXMLDatosMID.selectSingleNode( "//XMLIN/IMAGES" ), wobjNodo64);
      }
      pvarXMLDatosMID.selectSingleNode( "//VEHICULO[ID='" + 
    		  XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjNodoCroquis.getParentNode(), "ID" ) ) + "']" ).
    		  	removeChild( pvarXMLDatosMID.selectSingleNode( "//VEHICULO[ID='" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjNodoCroquis.getParentNode(), "ID" ) ) + "']/croquis" )  );
    }
    //
  }

  private org.w3c.dom.Element fncXMLtoJPG( String pvarRequestXML, String pvarTipoCroquis ) throws Exception
  {
    org.w3c.dom.Element fncXMLtoJPG = null;
    XmlDomExtended wobjLocResponse = null;
    XmlDomExtended wobjResponse = null;
    org.w3c.dom.Element wobjLocNodo = null;
    VBObjectClass wobjClass = null;
    String wvarResponse = "";
    //
    //
    wobjClass = new lbaw_XMLtoJPG();
    StringHolder shLog = new StringHolder();
    wobjClass.IAction_Execute(pvarRequestXML, shLog, "" );
    wvarResponse = shLog.getValue();
    //
    wobjLocResponse = new XmlDomExtended();
    //
    wobjLocResponse.loadXML( wvarResponse );
    //
    //Verifico la respuesta del COM+
    if( ! (wobjLocResponse.selectSingleNode( "//Response" )  == (org.w3c.dom.Node) null) )
    {
      if( Strings.toUpperCase( XmlDomExtended.getText( wobjLocResponse.selectSingleNode( "//Response/Estado/@resultado" )  ) ).equals( "TRUE" ) )
      {
        //
        wobjResponse = new XmlDomExtended();
        wobjResponse.loadXML( "<IMAGE></IMAGE>" );

        wobjLocNodo = wobjResponse.getDocument().createElement( "NAME" );
        XmlDomExtended.setText( wobjLocNodo, pvarTipoCroquis );
        XmlDomExtended.nodeImportAsChildNode(wobjResponse.selectSingleNode( "//IMAGE" ),wobjLocNodo );

        wobjLocNodo = wobjResponse.getDocument().createElement( "CONTENT" );
        XmlDomExtended.setText( wobjLocNodo, XmlDomExtended.getText( wobjLocResponse.selectSingleNode( "//BinData" )  ) );
        wobjLocNodo.setAttribute( "DataType", "bin.Base64" );
        XmlDomExtended.nodeImportAsChildNode(wobjResponse.selectSingleNode( "//IMAGE" ),wobjLocNodo );
        //
        fncXMLtoJPG = wobjResponse.getDocument().getDocumentElement();
      }
      else
      {
        fncXMLtoJPG = (org.w3c.dom.Element) null;
      }
    }
    else
    {
      fncXMLtoJPG = (org.w3c.dom.Element) null;
    }
    //
    wobjLocResponse = null;
    wobjResponse = null;
    wobjLocNodo = (org.w3c.dom.Element) null;

    return fncXMLtoJPG;
  }

  public String SaveBinaryData( String FileName, String ByteArray ) throws Exception
  {
	  
	byte[] bytes = Base64Encoding.decode(ByteArray);
	FileUtils.writeByteArrayToFile(new File(FileName), bytes);
	return FileName;
	
	/*
    String SaveBinaryData = "";
    XmlDomExtended wobjXMLPdf = null;
    org.w3c.dom.Node wobjoNode = null;
    ADODB.Stream BinaryStream = new ADODB.Stream();


    //unsup On Error GoTo 0 not supported in this context
    wobjXMLPdf = new XmlDomExtended();
    wobjXMLPdf.loadXML( "<PDF xmlns:dt=\"urn:schemas-microsoft-com:datatypes\" dt:dt=\"bin.base64\">" + ByteArray + "</PDF>" );

    wobjoNode = wobjXMLPdf.getDocument().getDocumentElement().selectSingleNode( "//PDF" );

    //Create Stream object
    BinaryStream = new ADODB.Stream();

    //Specify stream type - we want To save binary data.
    BinaryStream.Charset.set( "Windows-1252" );
    BinaryStream.Type.set( adTypeBinary );
    BinaryStream.Mode.set( 0 );

    //Open the stream And write binary data To the object
    BinaryStream.Open();
    BinaryStream.Write( wobjoNode.nodeTypedValue);

    //Save binary data To disk
    BinaryStream.Position.set( 0 );
    BinaryStream.SaveToFile( new Variant( FileName ), adSaveCreateOverWrite );

    BinaryStream.Close();
    BinaryStream = (ADODB.Stream) null;

    SaveBinaryData = FileName;

    return SaveBinaryData;
    */
  }

  private String obtenerNombreRandom( String Ruta, String Extension ) throws Exception
  {
    String obtenerNombreRandom = "";
    int wvarRandom = 0;
    boolean wvarGeneradoOK = false;
    String wvarNombreArchivo = "";

    //
    //Genera numero random entre 0 y 9999 como nombre de archivo
    //y verifica que no exista un archivo con ese nombre
    wvarGeneradoOK = false;
    while( ! (wvarGeneradoOK) )
    {
      //
      wvarRandom = (int)Math.rint( VB.rnd() * 10000 );
      wvarNombreArchivo = Ruta + "Denuncia" + Strings.right( ("0000" + String.valueOf( wvarRandom )), 4 ) + "." + Extension;
      //
      File f = new File(wvarNombreArchivo);
      if( !f.exists())
      {
        wvarGeneradoOK = true;
      }
      else
      {
        wvarGeneradoOK = false;
      }
    }

    obtenerNombreRandom = wvarNombreArchivo;

    return obtenerNombreRandom;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
