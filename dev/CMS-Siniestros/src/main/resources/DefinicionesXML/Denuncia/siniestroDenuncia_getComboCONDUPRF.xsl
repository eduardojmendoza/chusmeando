<?xml version="1.0" encoding="UTF-8"?>
<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2007. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: siniestroDenuncia_getComboCONDUPRF.xsl

Fecha de Creación: 27/06/2007

Desarrollador: Daniel Armentano

Descripción: Este archivo se utiliza para dar formato al XML resultante de la llamada
	al AIS desde el archivo getActividadOcupacion.xml.

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<options>
			<xsl:for-each select="//PROFESION[PROFECOD != '']">
				<xsl:sort select="./PROFEDES"/>
				<option>
					<xsl:attribute name="value"><xsl:value-of select="PROFECOD"/></xsl:attribute>
					<xsl:value-of select="PROFEDES"/>
				</option>
			</xsl:for-each>
		</options>
	</xsl:template>
</xsl:stylesheet>
