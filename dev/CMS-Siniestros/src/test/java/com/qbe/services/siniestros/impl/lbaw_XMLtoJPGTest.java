package com.qbe.services.siniestros.impl;

import java.util.logging.Logger;

import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Node;

import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

public class lbaw_XMLtoJPGTest {

	protected static Logger logger = Logger.getLogger(lbaw_XMLtoJPG.class.getName());

	@Test
	public void test() throws Exception {

		XmlDomExtended log = new XmlDomExtended();
		log.load(this.getClass().getResourceAsStream("/log_lbaw_siniInsertMID-bug798.xml"));
		Node request = log.selectSingleNode("//croquis");
		lbaw_XMLtoJPG XMLtoJPGnew = new lbaw_XMLtoJPG();

		StringHolder pvarResponse = new StringHolder();
		int result = XMLtoJPGnew.IAction_Execute(XmlDomExtended.marshal(request), pvarResponse, null);
		Assert.assertTrue("Error al generar la imagen", result == 0);
		
		XmlDomExtended response = new XmlDomExtended();
		response.loadXML(pvarResponse.getValue());
		String imageBase64 = XmlDomExtended.getText(response.selectSingleNode("//BinData"));
		
		Assert.assertTrue("La imagen es vacia", imageBase64.length() > 0);
		
		//FileUtils.writeByteArrayToFile(new File("/tmp/output.jpg"), Base64Encoding.decode(imageBase64));
	}
}
