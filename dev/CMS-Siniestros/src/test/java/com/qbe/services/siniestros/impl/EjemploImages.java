package com.qbe.services.siniestros.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.LookupOp;
import java.awt.image.RescaleOp;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import javax.imageio.ImageIO;

import org.springframework.util.StreamUtils;

public class EjemploImages {

	public static void main(String[] args) {
		new EjemploImages().prueba1();
	}

	public void prueba1() {
		try {
			BufferedImage fondo = ImageIO.read(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("Images/auto_bollos.bmp"));
			BufferedImage arbol = ImageIO.read(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("Images/arbol.bmp"));
			BufferedImage bollo = ImageIO.read(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("Images/bollo.bmp"));

			BufferedImage stageImage = new BufferedImage(fondo.getWidth(), fondo.getHeight(),
					BufferedImage.TYPE_INT_ARGB);

			BufferedImage bolloAlpha = convert(bollo, BufferedImage.TYPE_INT_ARGB);
			setAlpha(bolloAlpha, new Color(0x00ffffff),50);
			stageImage.getGraphics().drawImage(fondo, 0, 0, Color.WHITE, null);
			stageImage.getGraphics().drawImage(arbol, 20, 20, null);
			stageImage.getGraphics().drawImage(bolloAlpha, 40, 20, null);
			stageImage.getGraphics().drawImage(bolloAlpha, 60, 30, null);
			stageImage.getGraphics().drawImage(bolloAlpha, 80, 60, null);

			float[] scales = { 1f, 1f, 1f, 0.5f };
			float[] offsets = new float[4];
			RescaleOp rop = new RescaleOp(scales, offsets, null);

			((Graphics2D) stageImage.getGraphics()).drawImage(stageImage, rop, 0, 0);

			BufferedImage resultImage = new BufferedImage(stageImage.getWidth(), stageImage.getHeight(),
		            BufferedImage.TYPE_INT_RGB); 
			resultImage.getGraphics().drawImage(stageImage, 0,0,null);
			
			ImageIO.write(resultImage, "jpg", new File("/tmp/output.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void prueba2() {
		try {
			BufferedImage resultImage = new BufferedImage(50, 50, BufferedImage.TYPE_INT_ARGB);

			Graphics2D g2d = (Graphics2D) resultImage.getGraphics();
			// g2d.setColor(Color.)
			// g2d.drawRect(0, 0, 50, 50);

			ImageIO.write(resultImage, "png", new File("/tmp/output.png"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public BufferedImage convert(BufferedImage src, int bufImgType) {
		BufferedImage img = new BufferedImage(src.getWidth(), src.getHeight(), bufImgType);
		Graphics2D g2d = img.createGraphics();
		g2d.drawImage(src, 0, 0, null);
		g2d.dispose();
		return img;
	}

	public void setAlpha(BufferedImage imagen, Color colorAlpha, int tolerancia) {
		for (int cx = 0; cx < imagen.getWidth(); cx++) {
			int redAlpha = colorAlpha.getRed();
			int greenAlpha = colorAlpha.getGreen() << 8;
			int blueAlpha = colorAlpha.getBlue() << 16;
			for (int cy = 0; cy < imagen.getHeight(); cy++) {
				int color = imagen.getRGB(cx, cy);
				int diference = Math.abs(0x000000ff - (color & redAlpha));
				diference += Math.abs(0x000000ff - ((color & greenAlpha) >> 8));
				diference += Math.abs(0x000000ff - ((color & blueAlpha) >> 16));
				if (diference < tolerancia) {
					int newcolor = color & 0x00ffffff;
					imagen.setRGB(cx, cy, newcolor);
				}
			}

		}
	}
}