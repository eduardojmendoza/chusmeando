package com.qbe.services.siniestros.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.siniestros.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class BITMAPINFO implements com.qbe.services.Cloneable
{
  public com.qbe.services.BITMAPINFOHEADER bmiHeader = new com.qbe.services.BITMAPINFOHEADER();
  public com.qbe.services.RGBQUAD bmiColors = new com.qbe.services.RGBQUAD();

  public Object clone()
  {
    try 
    {
      BITMAPINFO clone = (BITMAPINFO) super.clone();
      //todo: make clones of any members that are objects ie. clone.obj = obj.clone();
      clone.bmiHeader = bmiHeader == null ? null : (com.qbe.services.BITMAPINFOHEADER) bmiHeader.clone();
      clone.bmiColors = bmiColors == null ? null : (com.qbe.services.RGBQUAD) bmiColors.clone();
      return clone;
    } 
    catch( CloneNotSupportedException e ) {}
    return null;
  }
}
