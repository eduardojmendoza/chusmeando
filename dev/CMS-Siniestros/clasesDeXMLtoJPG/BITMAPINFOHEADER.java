package com.qbe.services.siniestros.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.siniestros.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Tama�o de este encabezado
 */

public class BITMAPINFOHEADER implements com.qbe.services.Cloneable
{
  public int biSize = 0;
  /**
   * Pixels de ancho imagen
   */
  public int biWidth = 0;
  /**
   * Pixels de alto imagen
   */
  public int biHeight = 0;
  /**
   * Siempre 1
   */
  public int biPlanes = 0;
  /**
   * Numero de bits de color por pixel: 1, 4, 8, or 24
   */
  public int byBitCount = 0;
  /**
   * 0  sin comprimir
   */
  public int biCompression = 0;
  /**
   * Tama�o del bitmap en bytes,  0  sin comprimir
   */
  public int biSizeImage = 0;
  /**
   * Resolucion horizontal en pixels por metro
   */
  public int biXPelsPerMeter = 0;
  /**
   * Resolucion vertical en pixels por metro
   */
  public int biYPelsPerMeter = 0;
  /**
   * number de colores actualmente usados (puede ser 0)
   */
  public int biClrUsed = 0;
  /**
   * que color es el mas importante (0 todos)
   */
  public int biClrImportant = 0;

  public Object clone()
  {
    try 
    {
      BITMAPINFOHEADER clone = (BITMAPINFOHEADER) super.clone();
      //todo: make clones of any members that are objects ie. clone.obj = obj.clone();
      return clone;
    } 
    catch( CloneNotSupportedException e ) {}
    return null;
  }
}
