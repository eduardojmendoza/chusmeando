package com.qbe.services.siniestros.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.siniestros.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * ID del componente del bloque          [0-255]
 */

public class BLOQUE implements com.qbe.services.Cloneable
{
  public int mvarID = 0;
  /**
   * Factor de sampleo Horizontal       [1-4]
   */
  public int mvarSampleoHor = 0;
  /**
   * Factor de sampleo vertical         [1-4]
   */
  public int mvarSampleoVer = 0;
  /**
   * tabla de cuantizacion         [0-3]
   */
  public int mvarTablaCuantizacion = 0;
  /**
   * coeficientes DCT
   */
  public int[] mvarCoefDCT = null;

  public Object clone()
  {
    try 
    {
      BLOQUE clone = (BLOQUE) super.clone();
      //todo: make clones of any members that are objects ie. clone.obj = obj.clone();
      clone.mvarCoefDCT = mvarCoefDCT == null ? null : (int[]) mvarCoefDCT.clone();
      return clone;
    } 
    catch( CloneNotSupportedException e ) {}
    return null;
  }
}
