VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_siniInsertAIS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context As ObjectContext
Private mobjEventLog    As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction


'Datos de la accion
Const mcteClassName             As String = "lbawA_SiniDenuncia.lbaw_siniInsertAIS"
'
'Constantes de p�rametros de Entrada
Const mcteParam_SINIENUMSQL                As String = "//SINIENUMSQL"

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wobjXMLResponseAIS  As MSXML2.DOMDocument
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    
    'Par�metros de entrada
    Dim mvarXMLDatosAIS     As String
    Dim wvarDEFINICION      As String
    
    'Par�metros de salida
    Dim wvarSINIEANNAIS     As String
    Dim wvarSINIENUMAIS     As String
    Dim wvarFECHA_GRABACION As String
    Dim wvarMODO            As String
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    wvarRequest = Replace(pvarRequest, "<CAMPOS>", "")
    wvarRequest = Replace(wvarRequest, "</CAMPOS>", "")
    
    wvarStep = 20
    'Llamo al MQ Gen�rico para guardar en AIS
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_SiniDenuncia.lbaw_siniMQGenerico")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing

    wvarStep = 30
    Set wobjXMLResponseAIS = CreateObject("MSXML2.DOMDocument")
        wobjXMLResponseAIS.async = False
        wobjXMLResponseAIS.loadXML (wvarResponse)
        
    wvarStep = 40
    If Not wobjXMLResponseAIS.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If (wobjXMLResponseAIS.selectSingleNode("//Response/Estado/@resultado").Text) = "true" Then
            wvarStep = 50
            'Grab�
            wvarFECHA_GRABACION = wobjXMLResponseAIS.selectSingleNode("//GRABAANN").Text & Right("00" & wobjXMLResponseAIS.selectSingleNode("//GRABAMES").Text, 2) & Right("00" & wobjXMLResponseAIS.selectSingleNode("//GRABADIA").Text, 2) & " " & Right("00" & wobjXMLResponseAIS.selectSingleNode("//GRABAHOR").Text, 2) & ":" & Right("00" & wobjXMLResponseAIS.selectSingleNode("//GRABAMIN").Text, 2)
            wvarSINIEANNAIS = wobjXMLResponseAIS.selectSingleNode("//SINIEANN").Text
            wvarSINIENUMAIS = wobjXMLResponseAIS.selectSingleNode("//SINIENUM").Text
            wvarMODO = "O"
        Else
            wvarStep = 60
            'No grab�
            If wobjXMLResponseAIS.selectSingleNode("//Response/Estado/@mensaje").Text = "CO" Then
                wvarMODO = "C"
            Else
                wvarMODO = "NG"
            End If
            wvarFECHA_GRABACION = ""
            wvarSINIEANNAIS = ""
            wvarSINIENUMAIS = ""
        End If
    Else
        wvarStep = 70
        'Problemas al ejecutar el COM+ de grabaci�n en AIS. Asumimos un NONSys detectado al guardar.
        wvarFECHA_GRABACION = ""
        wvarSINIEANNAIS = ""
        wvarSINIENUMAIS = ""
        wvarMODO = "NG"
    End If
    
    wvarStep = 80
    pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><SINIEANNAIS>" & wvarSINIEANNAIS & "</SINIEANNAIS><SINIENUMAIS>" & wvarSINIENUMAIS & "</SINIENUMAIS><FECHA_GRABACION>" & wvarFECHA_GRABACION & "</FECHA_GRABACION><MODO>" & wvarMODO & "</MODO></Response>"

    wvarStep = 90
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 100
    Set wobjXMLResponseAIS = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjXMLResponseAIS = Nothing
    '
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    '
End Function

Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub










