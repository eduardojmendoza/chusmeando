VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_siniGetLista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbawA_SiniDenuncia.lbaw_siniGetLista"
Const mcteStoreProc                 As String = "P_SINI_SELECT_SINIESTRO_LISTA"

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    
    Dim wvarResult          As String
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    
    'Parámetros de entrada
    Dim mvarSINIEANNSQL       As String
    Dim mvarSINIENUMSQL       As String

    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    
    wvarStep = 10
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    
    wvarStep = 40
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc
    
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    
    If Not wrstDBResult.EOF Then
        '
        wvarStep = 140
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 150
        wobjXSLResponse.async = False
        Call wobjXSLResponse.loadXML(p_GetXSL())
        
        wvarStep = 150
        wrstDBResult.Save wobjXMLResponse, adPersistXML
        '
        wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
        
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><LISTADO>" & wvarResult & "</LISTADO></Response>"
        
        wvarStep = 180
        Set wobjXMLResponse = Nothing
        Set wobjXSLResponse = Nothing
    Else
        wvarMensaje = "No hay denuncias pendientes de ingresar al AIS / MIDDLEWARE"
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje='" & wvarMensaje & "' /></Response>"
    End If
    

    wvarStep = 140
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 150
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    
    Set wobjHSBC_DBCnn = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='DENUNCIA'>"
        
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='RAMOPCOD'><xsl:value-of select='@RAMOPCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='SINIENUMSQL'><xsl:value-of select='@SINIENUMSQL' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='SINIEANNAIS'><xsl:value-of select='@SINIEANNAIS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='SINIENUMAIS'><xsl:value-of select='@SINIENUMAIS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NROFORM'><xsl:value-of select='@NROFORM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='GRABSFH'><xsl:value-of select='@GRABSFH' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESTADODAT'><xsl:value-of select='@ESTADODAT' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='RECARGA'><xsl:value-of select='@RECARGA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='MARENVAIS'><xsl:value-of select='@MARENVAIS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESTADOAIS'><xsl:value-of select='@ESTADOAIS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='MARENVMID'><xsl:value-of select='@MARENVMID' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESTADOMID'><xsl:value-of select='@ESTADOMID' /></xsl:element>"

    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    'wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='XMLDatosAIS'/>"
    'wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='XMLDatosMID'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    
    p_GetXSL = wvarStrXSL
    
End Function

Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub




