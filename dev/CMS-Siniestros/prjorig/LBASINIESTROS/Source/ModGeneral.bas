Attribute VB_Name = "ModGeneral"
Option Explicit

Public Const gcteDB As String = "lbawA_OfVirtualLBA.udl"

' Parametros XML de Configuracion
Public Const gcteQueueManager       As String = "//QUEUEMANAGER"
Public Const gctePutQueue           As String = "//PUTQUEUE"
Public Const gcteGetQueue           As String = "//GETQUEUE"
Public Const gcteGMOWaitInterval    As String = "//GMO_WAITINTERVAL"

Public Const gcteClassMQConnection As String = "WD.Frame2MQ"

Public Function MidAsString(pvarStringCompleto As String, ByRef pvarActualCounter As Long, pvarLongitud As Long) As String
    MidAsString = Mid(pvarStringCompleto, pvarActualCounter, pvarLongitud)
    pvarActualCounter = pvarActualCounter + pvarLongitud
End Function

Public Function CompleteZero(pvarString As String, pvarLongitud As Long) As String
    Dim wvarCounter As Long
    Dim wvarstrTemp As String
    For wvarCounter = 1 To pvarLongitud
        wvarstrTemp = wvarstrTemp & "0"
    Next
    CompleteZero = Right(wvarstrTemp & pvarString, pvarLongitud)
End Function

Public Function GetErrorInformacionDato(pobjXMLContenedor As IXMLDOMNode, Optional pvarPathDato As String, Optional pvarTipoDato As String, Optional pobjLongitud As IXMLDOMNode, Optional pobjDecimales As IXMLDOMNode, Optional pobjDefault As IXMLDOMNode, Optional pobjObligatorio As IXMLDOMNode) As String
    Dim wobjNodoValor As IXMLDOMNode
    Dim wvarDatoValue As String
    Dim wvarCounter As Long
    Dim wvarCampoNumerico As Double
    Dim wvarIsDatoObligatorio As Boolean
    
    If Not pobjObligatorio Is Nothing Then
        If pobjObligatorio.Text = "SI" Then
            'Es un Dato Obligatorio
            wvarIsDatoObligatorio = True
            Set wobjNodoValor = pobjXMLContenedor.selectSingleNode("./" & Mid(pvarPathDato, 3))
            If wobjNodoValor Is Nothing Then
                GetErrorInformacionDato = "Nodo Obligatorio " & Mid(pvarPathDato, 3) & " NO INFORMADO"
                GoTo ClearObjects:
            End If
        End If
    End If
    
    Set wobjNodoValor = pobjXMLContenedor.selectSingleNode("./" & Mid(pvarPathDato, 3))
    If wobjNodoValor Is Nothing Then
        If Not pobjDefault Is Nothing Then wvarDatoValue = pobjDefault.Text
    Else
        wvarDatoValue = wobjNodoValor.Text
    End If
    '
    Select Case pvarTipoDato
        Case "TEXTO", "TEXTOIZQUIERDA": 'Dato del Tipo String
            If wvarIsDatoObligatorio And Trim(wvarDatoValue) = "" Then
                GetErrorInformacionDato = "Campo Obligatorio " & Mid(pvarPathDato, 3) & " SIN VALOR INGRESADO"
                GoTo ClearObjects
            End If
        Case "ENTERO", "DECIMAL": 'Dato del Tipo Numerico
            If wvarDatoValue = "" Then wvarDatoValue = "0"
            If Not IsNumeric(wvarDatoValue) Then
                GetErrorInformacionDato = "Campo " & Mid(pvarPathDato, 3) & " CON FORMATO INVALIDO (Valor Informado: " & wvarDatoValue & ". Requerido: Numerico)"
                GoTo ClearObjects
            Else
                If wvarIsDatoObligatorio And CDbl(wvarDatoValue) = 0 Then
                    GetErrorInformacionDato = "Campo Obligatorio " & Mid(pvarPathDato, 3) & " SIN VALOR INGRESADO"
                    GoTo ClearObjects
                End If
            End If
        Case "FECHA": 'Dato del Tipo "Fecha" Debe venir del tipo dd/mm/yyyy devuelve YYYYMMDD
            If wvarIsDatoObligatorio And Not (wvarDatoValue Like "*/*/*") Then
                GetErrorInformacionDato = "Campo Obligatorio " & Mid(pvarPathDato, 3) & " VALOR INGRESADO EN FORMATO INVALIDO (Valor Informado: " & wvarDatoValue & ". Requerido: dd/mm/yyyy)"
            End If
    End Select
    '
ClearObjects:
    Set wobjNodoValor = Nothing
End Function

Public Function GetRequestRetornado(pobjXMLRequest As DOMDocument, pobjXMLRequestDef As IXMLDOMNode, pvarStrRetorno) As IXMLDOMNode
Dim wobjNodoRequestDef  As IXMLDOMNode
Dim wobjNodoVectorDef As IXMLDOMNode
Dim wobjNewNodo         As IXMLDOMNode
Dim pvarStartCount      As Long
Dim wvarLastValue       As String
Dim wvarCount           As Long

    pvarStartCount = 5 'Desestimo el Numero de mensaje
    '
    For Each wobjNodoRequestDef In pobjXMLRequestDef.childNodes
        '
        If wobjNodoRequestDef.Attributes.getNamedItem("Cantidad") Is Nothing Then
            'No proceso los vectores del Request
            If pobjXMLRequest.selectSingleNode("//" & wobjNodoRequestDef.Attributes.getNamedItem("Nombre").Text) Is Nothing Then
                Set wobjNewNodo = pobjXMLRequest.createElement(wobjNodoRequestDef.Attributes.getNamedItem("Nombre").Text)
                pobjXMLRequest.childNodes(0).appendChild wobjNewNodo
            Else
                Set wobjNewNodo = pobjXMLRequest.selectSingleNode("//" & wobjNodoRequestDef.Attributes.getNamedItem("Nombre").Text)
            End If
            '
            If wobjNodoRequestDef.Attributes.getNamedItem("Decimales") Is Nothing Then
                wvarLastValue = Mid(pvarStrRetorno, pvarStartCount, wobjNodoRequestDef.Attributes.getNamedItem("Enteros").Text)
            Else
                wvarLastValue = Mid(pvarStrRetorno, pvarStartCount, wobjNodoRequestDef.Attributes.getNamedItem("Enteros").Text + wobjNodoRequestDef.Attributes.getNamedItem("Decimales").Text)
            End If
            '
            Select Case wobjNodoRequestDef.Attributes.getNamedItem("TipoDato").Text
                Case "ENTERO"
                    wobjNewNodo.Text = Val(wvarLastValue)
                Case "DECIMAL"
                    wobjNewNodo.Text = Val(wvarLastValue) / (10 ^ Val(wobjNodoRequestDef.Attributes.getNamedItem("Decimales").Text))
                Case "FECHA"
                    wobjNewNodo.Text = Right(wvarLastValue, 2) & "/" & Mid(wvarLastValue, 5, 2) & "/" & Left(wvarLastValue, 4)
                Case Else
                    wobjNewNodo.Text = Trim(wvarLastValue)
            End Select
            If wobjNodoRequestDef.Attributes.getNamedItem("Decimales") Is Nothing Then
                pvarStartCount = pvarStartCount + wobjNodoRequestDef.Attributes.getNamedItem("Enteros").Text
            Else
                pvarStartCount = pvarStartCount + wobjNodoRequestDef.Attributes.getNamedItem("Enteros").Text + wobjNodoRequestDef.Attributes.getNamedItem("Decimales").Text
            End If
        Else
            'Salteo todos los registros del vector
            For wvarCount = 1 To wobjNodoRequestDef.Attributes.getNamedItem("Cantidad").Text
                For Each wobjNodoVectorDef In wobjNodoRequestDef.childNodes
                    If wobjNodoVectorDef.Attributes.getNamedItem("Decimales") Is Nothing Then
                        pvarStartCount = pvarStartCount + wobjNodoVectorDef.Attributes.getNamedItem("Enteros").Text
                    Else
                        pvarStartCount = pvarStartCount + wobjNodoVectorDef.Attributes.getNamedItem("Enteros").Text + wobjNodoVectorDef.Attributes.getNamedItem("Decimales").Text
                    End If
                Next wobjNodoVectorDef
            Next wvarCount
        End If
        '
    Next wobjNodoRequestDef
    '
    Set GetRequestRetornado = pobjXMLRequest.childNodes(0)
    Set wobjNewNodo = Nothing
    Set wobjNodoRequestDef = Nothing
End Function

