VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_siniInsertMID"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context As ObjectContext
Private mobjEventLog    As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction


'Datos de la accion
Const mcteClassName             As String = "lbawA_SiniDenuncia.lbaw_siniInsertMID"
'
'Constantes de párametros de Entrada
Const mcteParam_XMLDatosMID             As String = "//xmlDataSource"
Const mcteParam_Metodo                  As String = "//Metodo"
Const mcteParam_ColdviewMetadata        As String = "//ColdviewMetadata"
Const mcteParam_EmailTo                 As String = "//EmailTo"
Const mcteParam_recipientsCC            As String = "//recipientsCC" 'DA - 27/10/2008
Const mcteParam_PDFPWD                  As String = "//PDFPWD"
Const mcteParam_DevuelvePDF             As String = "//DevuelvePDF"
Const mcteParam_ReportId                As String = "//ReportId"
'
'26/05/2008 - DA
Const mcteTempFilesServer = "LBAVirtual_TempFilesServer.xml"

'Carpeta donde se encuentran los Templates de XML
Const mcteSubDirName                    As String = ""


Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName         As String = "IAction_Execute"
    '
    Dim wvarRequest          As String
    Dim wvarResponse         As String
    Dim wobjXMLResponseMID   As MSXML2.DOMDocument
    Dim wobjXMLRequest       As MSXML2.DOMDocument
    Dim wobjClass            As HSBCInterfaces.IAction
    Dim wvarStep             As Long
    Dim wvarEstadoMID        As String
    Dim wvarDefinirDefaultONo As String
   
    'Parámetros de entrada
    Dim wvarXMLDatosMID      As MSXML2.DOMDocument
    Dim wvarMetodo           As String
    Dim wvarColdviewMetadata As String
    Dim wvarEmailTo          As String
    Dim wvarRecipientsCC     As String 'DA - 24/10/2008
    Dim wvarPDFPWD           As String
    Dim wvarDevuelvePDF      As String
    Dim wvarMIDFaultCode     As String
    Dim wvarReportId         As String
    '26/05/2008 - DA: se toma desde el archivo LBAVirtual_TempFilesServer.xml
    Dim mvarRutaDestino         As String
    Dim wobjXMLTempFilesServer  As MSXML2.DOMDocument
    
    'Parámetros de salida
    Dim wvarRutaPDF          As String

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    
    'Obtiene los valores de los parámetros
    wvarStep = 20
    With wobjXMLRequest
        wvarStep = 21
        Set wvarXMLDatosMID = CreateObject("MSXML2.DOMDocument")
            wvarXMLDatosMID.async = False
        wvarXMLDatosMID.loadXML (.selectSingleNode(mcteParam_XMLDatosMID).xml)
        wvarStep = 22
        wvarMetodo = .selectSingleNode(mcteParam_Metodo).Text
        wvarStep = 23
        wvarColdviewMetadata = .selectSingleNode(mcteParam_ColdviewMetadata).Text
        wvarStep = 24
        If Not .selectSingleNode(mcteParam_EmailTo) Is Nothing Then
            wvarEmailTo = .selectSingleNode(mcteParam_EmailTo).Text
        Else
            wvarEmailTo = ""
        End If
        'DA - 27/10/2008
        If Not .selectSingleNode(mcteParam_recipientsCC) Is Nothing Then
            wvarRecipientsCC = .selectSingleNode(mcteParam_recipientsCC).Text
        Else
            wvarRecipientsCC = ""
        End If
        

        If Not .selectSingleNode(mcteParam_PDFPWD) Is Nothing Then
            wvarPDFPWD = .selectSingleNode(mcteParam_PDFPWD).Text
        Else
            wvarPDFPWD = ""
        End If
        
        wvarStep = 23
        wvarDevuelvePDF = .selectSingleNode(mcteParam_DevuelvePDF).Text
        wvarReportId = .selectSingleNode(mcteParam_ReportId).Text
    End With
    
    'Si XMLDatosMID tiene CROQUIS convertirlos a BASE64 e insertarlos en el XMLIN
    wvarStep = 70
    ProcesarCroquis wvarXMLDatosMID
        
    'Enviar el xml generado a MIDDLEWARE
    wvarStep = 110
    wvarRequest = "<Request>"
    wvarRequest = wvarRequest & "<DEFINICION>" & wvarMetodo & "</DEFINICION>"
    wvarRequest = wvarRequest & wvarXMLDatosMID.xml 'XmlDataSource
    wvarRequest = wvarRequest & "<coldViewMetadata>" & wvarColdviewMetadata & "</coldViewMetadata>"
    wvarRequest = wvarRequest & "<reportId>" & wvarReportId & "</reportId>"
    wvarRequest = wvarRequest & "<recipientTO>" & wvarEmailTo & "</recipientTO>"
    wvarRequest = wvarRequest & "<recipientsCC>" & wvarRecipientsCC & "</recipientsCC>" 'DA - 27/10/2008
    wvarRequest = wvarRequest & "<attachPassword>" & wvarPDFPWD & "</attachPassword>"
    wvarRequest = wvarRequest & "</Request>"
                        
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_SiniDenuncia.lbaw_siniMQMiddle")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
       
    'Analizar devolucion de MIDDLEWARE
    wvarStep = 120
    Set wobjXMLResponseMID = CreateObject("MSXML2.DOMDocument")
    With wobjXMLResponseMID
        .async = False
        Call .loadXML(wvarResponse)
    End With

    If Not wobjXMLResponseMID.selectSingleNode("//Response") Is Nothing Then
        If (UCase(wobjXMLResponseMID.selectSingleNode("//Response/Estado/@resultado").Text) = "TRUE") Then
            'Analisis de posibles errores de MIDDLEWARE
            If Not wobjXMLResponseMID.selectSingleNode("//faultstring") Is Nothing Then
                wvarMIDFaultCode = wobjXMLResponseMID.selectSingleNode("//faultstring").Text
                If InStr(1, wvarMIDFaultCode, "E1", vbTextCompare) > 0 Then
                    'Fallo EMAIL
                    wvarEstadoMID = "E"
                ElseIf InStr(1, wvarMIDFaultCode, "E2", vbTextCompare) > 0 Then
                    'Fallo COLDVIEW
                    wvarEstadoMID = "C"
                ElseIf InStr(1, wvarMIDFaultCode, "E3", vbTextCompare) > 0 Then
                    'Fallo COLDVIEW y EMAIL
                    wvarEstadoMID = "CE"
                ElseIf InStr(1, wvarMIDFaultCode, "E4", vbTextCompare) > 0 Then
                    'Fallo PDF
                    wvarEstadoMID = "P"
                ElseIf InStr(1, wvarMIDFaultCode, "E5", vbTextCompare) > 0 Then
                    'Fallo PDF y EMAIL
                    wvarEstadoMID = "PE"
                ElseIf InStr(1, wvarMIDFaultCode, "E6", vbTextCompare) > 0 Then
                    'Fallo PDF y COLDVIEW
                    wvarEstadoMID = "PC"
                ElseIf InStr(1, wvarMIDFaultCode, "E7", vbTextCompare) > 0 Then
                    'Fallo PDF y COLDVIEW y EMAIL
                    wvarEstadoMID = "PCE"
                Else
                    'Fallo no registrado, se asume falló de middleware
                    wvarEstadoMID = "PCE"
                End If
            Else
                'TODO GENERADO OK, no se retorna valor
                wvarEstadoMID = ""
            End If
        Else
            'Errores en la ejecucion del componente de middleware, se asume falla de middleware
            wvarEstadoMID = "PCE"
        End If
    Else
        'Errores en la ejecucion del componente de middleware, se asume falla de middleware
        wvarEstadoMID = "PCE"
    End If
    '
    '26/05/2008 - DA: busco la ruta de destino del PDF (para grabación temporal)
    wvarStep = 130
    Set wobjXMLTempFilesServer = CreateObject("MSXML2.DOMDocument")
        wobjXMLTempFilesServer.async = False
    Call wobjXMLTempFilesServer.Load(App.Path & "\" & mcteTempFilesServer)
    
    mvarRutaDestino = wobjXMLTempFilesServer.selectSingleNode("//PATH").Text
   
    'Si se pidió la devolucion del PDF generado, persistir el PDF en un archivo
    wvarStep = 140
    If wvarDevuelvePDF = "S" And wvarEstadoMID = "" Then
        If Not wobjXMLResponseMID.selectSingleNode("//PDF") Is Nothing Then
            '26/05/2008 - DA: se agrega el grabado en un fileserver
            'wvarRutaPDF = obtenerNombreRandom(App.Path & "\PDF\", "PDF")
            wvarRutaPDF = obtenerNombreRandom(mvarRutaDestino & "\PDF\", "PDF")
            SaveBinaryData wvarRutaPDF, wobjXMLResponseMID.selectSingleNode("//PDF").Text
        Else
            wvarRutaPDF = ""
        End If
    Else
        wvarRutaPDF = ""
    End If
       
    'Setear el valor de los parametros a devolver (ruta del PDF)
    wvarStep = 150
    pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><RUTAPDF>" & wvarRutaPDF & "</RUTAPDF><ESTADOMID>" & wvarEstadoMID & "</ESTADOMID></Response>"
   
    wvarStep = 170
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponseMID = Nothing
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    
    '
End Function

Private Sub ProcesarCroquis(ByRef pvarXMLDatosMID As MSXML2.DOMDocument)
    '
    Dim wobjNodo64          As MSXML2.IXMLDOMElement
    Dim wobjNodoCroquis     As MSXML2.IXMLDOMElement
    Dim wobjAUX             As MSXML2.IXMLDOMElement
    
    Set wobjAUX = pvarXMLDatosMID.createElement("IMAGES")
        pvarXMLDatosMID.selectSingleNode("//XMLIN").appendChild wobjAUX
    '
    'Croquis y bollos del vehiculo asegurado
    For Each wobjNodoCroquis In pvarXMLDatosMID.selectNodes("//XMLIN/croquis")
        '
        'Armo el Base 64 de cada croquis
        Set wobjNodo64 = fncXMLtoJPG(wobjNodoCroquis.xml, wobjNodoCroquis.Attributes.getNamedItem("tipo").Text)
        '
        If Not wobjNodo64 Is Nothing Then
            'Lo apendeo al XML que viajará al Middleware
            pvarXMLDatosMID.selectSingleNode("//XMLIN/IMAGES").appendChild wobjNodo64
            pvarXMLDatosMID.selectSingleNode("//XMLIN").removeChild pvarXMLDatosMID.selectSingleNode("//XMLIN/croquis[@tipo='" & wobjNodoCroquis.Attributes.getNamedItem("tipo").Text & "']")
        Else
            'Si fallo la generacion del nodo en base 64 remueve la imagen para no
            'enviarla a MIDDLEWARE
            pvarXMLDatosMID.selectSingleNode("//XMLIN").removeChild pvarXMLDatosMID.selectSingleNode("//XMLIN/croquis[@tipo='" & wobjNodoCroquis.Attributes.getNamedItem("tipo").Text & "']")
        End If
    Next
    
    'Bollos de OTROS vehiculos involucrados
    For Each wobjNodoCroquis In pvarXMLDatosMID.selectNodes("//VEHICULO/croquis")
        '
        'Armo el Base 64 de cada croquis
        Set wobjNodo64 = fncXMLtoJPG(wobjNodoCroquis.xml, wobjNodoCroquis.parentNode.selectSingleNode("ID").Text)
        '
        If Not wobjNodo64 Is Nothing Then
            'Lo apendeo al XML que viajará al Middleware
            pvarXMLDatosMID.selectSingleNode("//XMLIN/IMAGES").appendChild wobjNodo64
            pvarXMLDatosMID.selectSingleNode("//VEHICULO[ID='" & wobjNodoCroquis.parentNode.selectSingleNode("ID").Text & "']").removeChild pvarXMLDatosMID.selectSingleNode("//VEHICULO[ID='" & wobjNodoCroquis.parentNode.selectSingleNode("ID").Text & "']/croquis")
        Else
            'Si fallo la generacion del nodo en base 64 remueve la imagen para no
            'enviarla a MIDDLEWARE
            pvarXMLDatosMID.selectSingleNode("//VEHICULO[ID='" & wobjNodoCroquis.parentNode.selectSingleNode("ID").Text & "']").removeChild pvarXMLDatosMID.selectSingleNode("//VEHICULO[ID='" & wobjNodoCroquis.parentNode.selectSingleNode("ID").Text & "']/croquis")
        End If
    Next
    '
End Sub

Private Function fncXMLtoJPG(ByVal pvarRequestXML As String, ByVal pvarTipoCroquis As String) As MSXML2.IXMLDOMElement
    '
    Dim wobjLocResponse     As MSXML2.DOMDocument
    Dim wobjResponse        As MSXML2.DOMDocument
    Dim wobjLocNodo         As MSXML2.IXMLDOMElement
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarResponse        As String
    '
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_SiniDenuncia.lbaw_XMLtoJPG")
    Call wobjClass.Execute(pvarRequestXML, wvarResponse, "")
    Set wobjClass = Nothing
    '
    Set wobjLocResponse = CreateObject("MSXML2.DOMDocument")
        wobjLocResponse.async = False
    '
    Call wobjLocResponse.loadXML(wvarResponse)
    '
    'Verifico la respuesta del COM+
    If Not wobjLocResponse.selectSingleNode("//Response") Is Nothing Then
        If (UCase(wobjLocResponse.selectSingleNode("//Response/Estado/@resultado").Text) = "TRUE") Then
            '
            Set wobjResponse = CreateObject("MSXML2.DOMDocument")
                wobjResponse.async = False
                wobjResponse.loadXML ("<IMAGE></IMAGE>")
                
            Set wobjLocNodo = wobjResponse.createElement("NAME")
            wobjLocNodo.Text = pvarTipoCroquis
            wobjResponse.selectSingleNode("//IMAGE").appendChild wobjLocNodo
                
            Set wobjLocNodo = wobjResponse.createElement("CONTENT")
            wobjLocNodo.Text = wobjLocResponse.selectSingleNode("//BinData").Text
            wobjLocNodo.setAttribute "DataType", "bin.Base64"
            wobjResponse.selectSingleNode("//IMAGE").appendChild wobjLocNodo
            '
            Set fncXMLtoJPG = wobjResponse.documentElement
        Else
            Set fncXMLtoJPG = Nothing
        End If
    Else
        Set fncXMLtoJPG = Nothing
    End If
    '
    Set wobjLocResponse = Nothing
    Set wobjResponse = Nothing
    Set wobjLocNodo = Nothing
    
End Function

Function SaveBinaryData(FileName, ByteArray)
    
    Const adTypeBinary = 1
    Const adSaveCreateOverWrite = 2
    
    On Error GoTo 0

    Dim wobjXMLPdf As MSXML2.DOMDocument
    Set wobjXMLPdf = CreateObject("MSXML2.DOMDocument")
    wobjXMLPdf.async = False
    wobjXMLPdf.loadXML "<PDF xmlns:dt=""urn:schemas-microsoft-com:datatypes"" dt:dt=""bin.base64"">" & ByteArray & "</PDF>"

    Dim wobjoNode As MSXML2.IXMLDOMNode
    Set wobjoNode = wobjXMLPdf.documentElement.selectSingleNode("//PDF")

    'Create Stream object
    Dim BinaryStream
    Set BinaryStream = CreateObject("ADODB.Stream")

    'Specify stream type - we want To save binary data.
    BinaryStream.Charset = "Windows-1252"
    BinaryStream.Type = adTypeBinary
    BinaryStream.Mode = 0

    'Open the stream And write binary data To the object
    BinaryStream.Open
    BinaryStream.Write wobjoNode.nodeTypedValue

    'Save binary data To disk
    BinaryStream.Position = 0
    BinaryStream.SaveToFile FileName, adSaveCreateOverWrite

    BinaryStream.Close
    Set BinaryStream = Nothing
    
    SaveBinaryData = FileName
    
End Function

Private Function obtenerNombreRandom(ByVal Ruta As String, ByVal Extension As String) As String
    
    Dim wvarRandom          As Integer
    Dim wvarGeneradoOK      As Boolean
    Dim wvarNombreArchivo   As String
    '
    'Genera numero random entre 0 y 9999 como nombre de archivo
    'y verifica que no exista un archivo con ese nombre
    wvarGeneradoOK = False
    While Not wvarGeneradoOK
        '
        wvarRandom = Rnd * 10000
        wvarNombreArchivo = Ruta & "Denuncia" & Right("0000" & CStr(wvarRandom), 4) & "." & Extension
        '
        If Dir(wvarNombreArchivo) = "" Then
            wvarGeneradoOK = True
        Else
            wvarGeneradoOK = False
        End If
    Wend
    
    obtenerNombreRandom = wvarNombreArchivo
    
End Function

Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub
