VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_siniAppEnvio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbawA_SiniDenuncia.lbaw_siniAppEnvio"

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLDetalleDenuncia As MSXML2.DOMDocument
    Dim wobjXMLDenuncias    As MSXML2.DOMDocument
    Dim wobjXMLDenuncia     As MSXML2.IXMLDOMNode
    Dim wobjXMLDatosAIS     As MSXML2.DOMDocument
    Dim wobjXMLDatosMID     As MSXML2.DOMDocument
    Dim wobjClass           As HSBCInterfaces.IAction

    Dim wvarStep            As Long
    
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    
    Dim wvarRAMOPCOD        As String
    Dim wvarSINIEANNAIS     As String
    Dim wvarSINIENUMAIS     As String
    Dim wvarSINIENUMSQL     As String
    Dim wvarMARENVAIS       As String
    Dim wvarENVIADOAIS      As String
    Dim wvarMARENVMID       As String
    Dim wvarENVIADOMID      As String
    Dim wvarGRABSFH         As String
    Dim wvarXMLErrores      As String
    Dim wvarResultado       As Boolean
    Dim wvarEstados         As String
    Dim wvarFormulario      As Integer
    Dim wvarReportId        As String
    Dim wvarPDFPWD          As String
    Dim wvarElementAux      As MSXML2.IXMLDOMElement
    
    Dim wvarFechaGrabacion  As String
    Dim wvarSinieAnn        As String
    Dim wvarSinieNum        As String
    Dim wvarMODO            As String
    Dim wvarMetodo          As String
    
    Dim wvarXmlResponse     As String
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarXmlResponse = ""
    '
    wvarStep = 10
    '
    'Obtener listado de SQL con registros con marcas MARENVAIS = P y MARENVMID = P
    wvarRequest = "<Request></Request>"
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_SiniDenuncia.lbaw_siniGetLista")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    '
    wvarStep = 20
    '
    Set wobjXMLDenuncias = CreateObject("MSXML2.DOMDocument")
    With wobjXMLDenuncias
        .async = False
        Call .loadXML(wvarResponse)
    End With
    '
    wvarStep = 30
    '
    '-------------------------------------------------------------------------------
    'SI Hay denuncia?
    '-------------------------------------------------------------------------------
    If wobjXMLDenuncias.selectNodes("//DENUNCIA").length > 0 Then
        '
        wvarStep = 40
        '
        'Para cada denuncia de la lista...
        For Each wobjXMLDenuncia In wobjXMLDenuncias.selectNodes("//DENUNCIA")
            '
            wvarRAMOPCOD = wobjXMLDenuncia.selectSingleNode("RAMOPCOD").Text
            wvarSINIEANNAIS = wobjXMLDenuncia.selectSingleNode("SINIEANNAIS").Text
            wvarSINIENUMAIS = wobjXMLDenuncia.selectSingleNode("SINIENUMAIS").Text
            wvarSINIENUMSQL = wobjXMLDenuncia.selectSingleNode("SINIENUMSQL").Text
            wvarMARENVAIS = wobjXMLDenuncia.selectSingleNode("MARENVAIS").Text
            wvarENVIADOAIS = ""
            wvarMARENVMID = wobjXMLDenuncia.selectSingleNode("MARENVMID").Text
            wvarENVIADOMID = ""
            wvarGRABSFH = wobjXMLDenuncia.selectSingleNode("GRABSFH").Text
            wvarXMLErrores = ""
            wvarFormulario = CInt(wobjXMLDenuncia.selectSingleNode("NROFORM").Text)
            '
            wvarStep = 50
            '
            'Recuperar denuncia de SQL
            wvarRequest = "<Request><SINIENUMSQL>" & wvarSINIENUMSQL & "</SINIENUMSQL></Request>"
            Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_SiniDenuncia.lbaw_siniGetItem")
            Call wobjClass.Execute(wvarRequest, wvarResponse, "")
            Set wobjClass = Nothing
            '
            wvarStep = 60
            '
            Set wobjXMLDetalleDenuncia = CreateObject("MSXML2.DOMDocument")
            With wobjXMLDetalleDenuncia
                .async = False
                Call .loadXML(wvarResponse)
            End With
            '
            wvarStep = 70
            '
            '-------------------------------------------------------------------------------
            'SI MARENVAIS = "P" envia la denuncia al AIS
            '-------------------------------------------------------------------------------
            If wvarMARENVAIS = "P" Then
                '
                'DA - 04/11/2008: controla que existan los datos a enviar al AIS y no pinche la app de env�o.
                If Not wobjXMLDetalleDenuncia.selectSingleNode("//XMLDatosAIS/XMLIN/CAMPOS") Is Nothing Then
                    '
                    Set wobjXMLDatosAIS = CreateObject("MSXML2.DOMDocument")
                    With wobjXMLDatosAIS
                        .async = False
                        'DA - 04/11/2008: antes buscaba //CAMPOS pero correponde que busque //XMLDatosAIS
                        'Call .loadXML(wobjXMLDetalleDenuncia.selectSingleNode("//CAMPOS").xml)
                        Call .loadXML(wobjXMLDetalleDenuncia.selectSingleNode("//XMLDatosAIS/XMLIN/CAMPOS").xml)
                    End With
                    '
                    wvarStep = 80
                    '
                    'Inserta en el XML que va al AIS el SINIENUMSQL
                    wobjXMLDatosAIS.selectSingleNode("//SINIESQL").Text = wvarSINIENUMSQL
                    '
                    wvarStep = 90
                    '
                    'Si esta faltando la fecha de denuncia, la inserta en el xml desde el SQL
                    If Trim(wobjXMLDatosAIS.selectSingleNode("//DENUCDIA").Text) = "" Then
                        wobjXMLDatosAIS.selectSingleNode("//DENUCDIA").Text = Mid(wvarGRABSFH, 9, 2)
                        wobjXMLDatosAIS.selectSingleNode("//DENUCMES").Text = Mid(wvarGRABSFH, 6, 2)
                        wobjXMLDatosAIS.selectSingleNode("//DENUCANN").Text = Mid(wvarGRABSFH, 1, 4)
                    End If
                    '
                    wvarStep = 100
                    '
                    'Guardo en AIS
                    
                    '     Dim wvarFileGet
                    '         wvarFileGet = FreeFile()
                    '     Open App.Path & "\XMLDatosAIS.xml" For Binary Access Write As wvarFileGet
                    '     Put wvarFileGet, , wobjXMLDatosAIS.xml
                    '     Close wvarFileGet
        
                    wvarResultado = fncGuardarAIS(wobjXMLDatosAIS.xml, wvarFormulario, wvarFechaGrabacion, wvarSinieAnn, wvarSinieNum, wvarMODO)
                    '
                    wvarStep = 110
                    '
                    'Envi� con exito? (guard�?)
                    If wvarResultado Then
                        If (wvarSinieNum <> "0") And (wvarSinieNum <> "") Then
                            'Update SQL con MARENVAIS = E
                            ActualizarMarenvAIS wvarSINIENUMSQL
                            wvarENVIADOAIS = "S"
                            wvarMARENVAIS = "E"
                            wvarXMLErrores = wvarXMLErrores & "<ENVIOAIS>Enviado a AIS con �xito</ENVIOAIS>"
                        Else
                            wvarENVIADOAIS = "N"
                            wvarMARENVAIS = "P"
                            wvarXMLErrores = wvarXMLErrores & "<ENVIOAIS>Fall� el envio al AIS</ENVIOAIS>"
                        End If
                    Else
                        wvarENVIADOAIS = "N"
                        wvarMARENVAIS = "P"
                        wvarXMLErrores = wvarXMLErrores & "<ENVIOAIS>Fall� el envio al AIS</ENVIOAIS>"
                    End If
                    '
                    wvarStep = 120
                    '
                    wvarXMLErrores = wvarXMLErrores & "<SINIEANN>" & wvarSinieAnn & "</SINIEANN>"
                    wvarXMLErrores = wvarXMLErrores & "<SINIENUM>" & wvarSinieNum & "</SINIENUM>"
                    wvarXMLErrores = wvarXMLErrores & "<MODO>" & wvarMODO & "</MODO>"
                    '
                    'DA - 24/08/2007: si grab� en AIS, tiene que mandar al impreso el n�mero generado por el AIS
                    wvarSINIEANNAIS = wvarSinieAnn
                    wvarSINIENUMAIS = wvarSinieNum
                    '
                    Set wobjXMLDatosAIS = Nothing
                '
                Else
                    'DOING
                    'DA - 04/11/2008: no se encontr� el dato en SQL. Esto evita que pinche si no se encuentra el dato en SQL
                    wvarENVIADOAIS = "N"
                    wvarMARENVAIS = "X"
                    wvarXMLErrores = wvarXMLErrores & "<ENVIOAIS>No se encontr� el dato en SQL</ENVIOAIS>"
                    'Update SQL con MARENVAIS = X
                    ActualizarMarenvAIS_X wvarSINIENUMSQL
                End If
            Else
                'Se limpia el contenido de la variable para que la p�gina no muestre el dato
                wvarMARENVAIS = ""
            End If
            '-------------------------------------------------------------------------------
            
            '-------------------------------------------------------------------------------
            'SI MARENVMID = "P" envia la denuncia a middleware
            '-------------------------------------------------------------------------------
            If wvarMARENVMID = "P" Then
                '
                'DA - 04/11/2008: controla que existan los datos a enviar a MDW y no pinche la app de env�o.
                If Not wobjXMLDetalleDenuncia.selectSingleNode("//XMLDatosMID/XMLIN/CAMPOS") Is Nothing Then
                    '
                    Set wobjXMLDatosMID = CreateObject("MSXML2.DOMDocument")
                    With wobjXMLDatosMID
                        .async = False
                        Call .loadXML(wobjXMLDetalleDenuncia.selectSingleNode("//XMLDatosMID").xml)
                    End With
                    '
                    wvarStep = 125
                    '
                    'MHC:
                    'Quitar estas lineas cuando se normalice la situaci�n de las denuncias pendientes
                    'en UAT
                    If wobjXMLDatosMID.selectSingleNode("//NUMEROSINIESTROCOMPLETO") Is Nothing Then
                        Set wvarElementAux = wobjXMLDatosMID.createElement("NUMEROSINIESTROCOMPLETO")
                        wobjXMLDatosMID.selectSingleNode("//CAMPOS").appendChild wvarElementAux
                    End If
                    '
                    wvarStep = 130
                    '
                    'Inserta en el XML que va al MID el SINIENUMSQL
                    If wvarSINIENUMAIS <> "0" Then
                        wobjXMLDatosMID.selectSingleNode("//SINIEANN").Text = wvarSINIEANNAIS
                        wobjXMLDatosMID.selectSingleNode("//SINIENUM").Text = wvarSINIENUMAIS
                        '
                        'DA - 24/08/2007: buscar el RAMOPCOD en el XMLDatosMID recuperado del SQL
                        wobjXMLDatosMID.selectSingleNode("//NUMEROSINIESTROCOMPLETO").Text = wobjXMLDatosMID.selectSingleNode("//RAMOPCOD").Text & "-" & wvarSINIEANNAIS & "-" & wvarSINIENUMAIS
                    Else
                        wobjXMLDatosMID.selectSingleNode("//SINIESQL").Text = wvarSINIENUMSQL
                        'DA - 24/08/2007
                        wobjXMLDatosMID.selectSingleNode("//NUMEROSINIESTROCOMPLETO").Text = wvarSINIENUMSQL
                    End If
                    '
                    wvarStep = 140
                    '
                    'Si esta faltando la fecha de denuncia, la inserta en el xml desde el SQL
                    If Trim(wobjXMLDatosMID.selectSingleNode("//DENUCDIA").Text) = "" Then
                        wobjXMLDatosMID.selectSingleNode("//DENUCDIA").Text = Mid(wvarGRABSFH, 9, 2)
                        wobjXMLDatosMID.selectSingleNode("//DENUCMES").Text = Mid(wvarGRABSFH, 6, 2)
                        wobjXMLDatosMID.selectSingleNode("//DENUCANN").Text = Mid(wvarGRABSFH, 1, 4)
                    End If
                    '
                    wvarStep = 150
                    '
                    'Envio a MID
                    If wobjXMLDatosMID.selectSingleNode("//DENUNMAIL").Text = "" Then
                        wvarMetodo = "ismGenerateAndPublishPdfReport.xml"
                    Else
                        wvarMetodo = "ismGeneratePublishAndSendPdfReport.xml"
                    End If
                    '
                    wvarStep = 160
                    '
                    'Seleccion de ReportId
                    Select Case wvarFormulario
                        Case 1: wvarReportId = "DenunciaAseguradoAutosF1_1.0"
                        Case 2: wvarReportId = "DenunciaAseguradoAutosF2_1.0"
                        Case 3: wvarReportId = "DenunciaAseguradoAutosF3_1.0"
                        Case 4: wvarReportId = "DenunciaDeTerceros_1.0"
                        Case 5: wvarReportId = "RiesgosVarios_1.0"
                    End Select
                    '
                    wvarStep = 170
                    '
                    If Not wobjXMLDatosMID.selectSingleNode("//PDFPWD") Is Nothing Then
                        wvarPDFPWD = wobjXMLDatosMID.selectSingleNode("//PDFPWD").Text
                    Else
                        wvarPDFPWD = ""
                    End If
                    '
                    wvarEstados = fncEnviaMiddleware(wobjXMLDatosMID.selectSingleNode("//XMLIN").xml, wvarMetodo, wvarReportId, wvarPDFPWD)
                    '
                    wvarStep = 180
                    '
                    'Si "P"DF est� ok y "E"mail est� ok y "C"oldview est� ok...
                    If InStr(1, wvarEstados, "P") = 0 And InStr(1, wvarEstados, "C") = 0 And InStr(1, wvarEstados, "E") = 0 Then
                        'Actualiza el SQL con el estado "E"nviado
                        wvarMARENVMID = "E"
                        wvarENVIADOMID = "S"
                        wvarXMLErrores = wvarXMLErrores & "<ENVIOMID>Enviado a Middleware con �xito</ENVIOMID>"
                    Else
                        'Actualiza el SQL con el estado "P"endiente, y actualiza el wvarEstados
                        wvarMARENVMID = "P"
                        wvarENVIADOMID = "N"
                        wvarXMLErrores = wvarXMLErrores & "<ENVIOMID>Hubo un error en Middleware (" & wvarEstados & ")</ENVIOMID>"
                    End If
                    '
                    'DA - 04/11/2008: se quita de ac� la invocaci�n al UPD de la MARENVMID. Se pone en step 190 (ver mas abajo)
                    Set wobjXMLDatosMID = Nothing
                Else
                    wvarStep = 185
                    'DA - 04/11/2008: no se encontr� el dato en SQL. Esto evita que pinche si no se encuentra el dato en SQL
                    wvarMARENVMID = "X"
                    wvarENVIADOMID = "N"
                    wvarXMLErrores = wvarXMLErrores & "<ENVIOMID>No se encontr� el dato en SQL</ENVIOMID>"
                End If
                '
                'DA - 04/11/2008: se pone ac� la invocaci�n al UPD de la MARENVMID.
                wvarStep = 190
                wvarRequest = "<Request>"
                wvarRequest = wvarRequest & "<SINIENUMSQL>" & wvarSINIENUMSQL & "</SINIENUMSQL>"
                wvarRequest = wvarRequest & "<MARENVMID>" & wvarMARENVMID & "</MARENVMID>"
                wvarRequest = wvarRequest & "<ESTADOMID>" & wvarEstados & "</ESTADOMID>"
                wvarRequest = wvarRequest & "</Request>"
                '
                Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_SiniDenuncia.lbaw_siniUpdMenvMID")
                Call wobjClass.Execute(wvarRequest, wvarResponse, "")
                Set wobjClass = Nothing
                '
            Else
                'Se limpia el contenido de la variable para que la p�gina no muestre el dato
                wvarMARENVMID = ""
            End If
            '-------------------------------------------------------------------------------
            '
            wvarStep = 200
            '
            '-------------------------------------------------------------------------------
            'INSERT log de Envio
            '-------------------------------------------------------------------------------
            wvarRequest = "<Request>"
            wvarRequest = wvarRequest & "<RAMOPCOD>" & wvarRAMOPCOD & "</RAMOPCOD>"
            wvarRequest = wvarRequest & "<SINIEANNAIS>" & wvarSINIEANNAIS & "</SINIEANNAIS>"
            wvarRequest = wvarRequest & "<SINIENUMAIS>" & wvarSINIENUMAIS & "</SINIENUMAIS>"
            wvarRequest = wvarRequest & "<SINIENUMSQL>" & wvarSINIENUMSQL & "</SINIENUMSQL>"
            wvarRequest = wvarRequest & "<ENVIADOAIS>" & wvarENVIADOAIS & "</ENVIADOAIS>"
            wvarRequest = wvarRequest & "<ENVIADOMID>" & wvarENVIADOMID & "</ENVIADOMID>"
            wvarRequest = wvarRequest & "<ESTADOS>" & wvarXMLErrores & "</ESTADOS>"
            wvarRequest = wvarRequest & "</Request>"
            '
            Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_SiniDenuncia.lbaw_siniInsLogEnvio")
            Call wobjClass.Execute(wvarRequest, wvarResponse, "")
            Set wobjClass = Nothing
            '
            wvarStep = 210
            '
            Set wobjXMLDetalleDenuncia = Nothing
            '
            '-------------------------------------------------------------------------------
            'Inserta en Response
            '-------------------------------------------------------------------------------
            wvarXmlResponse = wvarXmlResponse + "<DENUNCIA>" & _
                                                    "<NROFORM>" & CStr(wvarFormulario) & _
                                                    "</NROFORM><RAMOPCOD>" & wvarRAMOPCOD & _
                                                    "</RAMOPCOD><SINIEANNAIS>" & wvarSINIEANNAIS & _
                                                    "</SINIEANNAIS><SINIENUMAIS>" & wvarSINIENUMAIS & _
                                                    "</SINIENUMAIS><SINIENUMSQL>" & wvarSINIENUMSQL & _
                                                    "</SINIENUMSQL><MARENVAIS>" & wvarMARENVAIS & _
                                                    "</MARENVAIS><MARENVMID>" & wvarMARENVMID & _
                                                    "</MARENVMID><ESTADOS>" & wvarXMLErrores & _
                                                    "</ESTADOS>" & _
                                                "</DENUNCIA>"
            '
            wvarStep = 220
            '
            Set wobjXMLDetalleDenuncia = Nothing
            '
        Next wobjXMLDenuncia
        '
        Set wobjXMLDenuncias = Nothing
        '
    End If
    '-------------------------------------------------------------------------------

    wvarStep = 300
    pvarResponse = "<Response><DENUNCIAS>" & wvarXmlResponse & "</DENUNCIAS></Response>"
    Exit Function
    
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
       
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function fncGuardarAIS(ByVal pobjXMLDatosAIS As String, ByVal pFormulario As Integer, ByRef pFechaGrabacion, ByRef pSinieAnn, ByRef pSinieNum, ByRef pModo) As Boolean
    'En esta funcion se envia la denuncia al AIS
    'El retorno de la misma debe ser TRUE o FALSE dependiendo del exito del envio de la
    'denuncia.
    '
    Dim wvarRequest                     As String
    Dim wvarResponse                    As String
    Dim wobjResponse                    As MSXML2.DOMDocument
    Dim wvarXML_Definicion_Insert_AIS   As String
    Dim wobjClass                       As HSBCInterfaces.IAction
    
    'Determino cu�l es el XML de definici�n para grabar en AIS.
    Select Case pFormulario
        Case 1
            wvarXML_Definicion_Insert_AIS = "siniestroDenuncia_putForm1AIS.xml"
        Case 2
            wvarXML_Definicion_Insert_AIS = "siniestroDenuncia_putForm2AIS.xml"
        Case 3
            wvarXML_Definicion_Insert_AIS = "siniestroDenuncia_putForm3AIS.xml"
        Case 5
            wvarXML_Definicion_Insert_AIS = "siniestroDenuncia_putForm5AIS.xml"
    End Select
    
    wvarRequest = "<Request>" & pobjXMLDatosAIS & "<DEFINICION>" & wvarXML_Definicion_Insert_AIS & "</DEFINICION></Request>"
    'Llamo al COM+ que guarda en AIS

    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_SiniDenuncia.lbaw_siniInsertAIS")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    
    '
    Set wobjResponse = CreateObject("MSXML2.DOMDocument")
        wobjResponse.async = False
    Call wobjResponse.loadXML(wvarResponse)
    '
    If Not wobjResponse.selectSingleNode("//MODO") Is Nothing Then
        pFechaGrabacion = wobjResponse.selectSingleNode("//FECHA_GRABACION").Text
        pSinieAnn = wobjResponse.selectSingleNode("//SINIEANNAIS").Text
        pSinieNum = wobjResponse.selectSingleNode("//SINIENUMAIS").Text
        pModo = wobjResponse.selectSingleNode("//MODO").Text
        fncGuardarAIS = True
    Else
        'Fall� el COM+. Asumimos estado NONSys detectado en la p�gina.
        pFechaGrabacion = ""
        pSinieAnn = "0"
        pSinieNum = "0"
        pModo = "NG"
        fncGuardarAIS = False
    End If
    '
    Set wobjResponse = Nothing

End Function

Private Function fncEnviaMiddleware(ByVal pXMLDatosMID As String, ByVal pMetodo As String, ByVal pReportId As String, ByVal pPDFPWD As String) As String
    'Funcion que va a enviar la denuncia a middleware
    Dim wvarDENUNMAIL        As String
    'DA - 24/10/2008
    Dim wvarDENUNMAILCC      As String
    Dim wobjXMLMID           As MSXML2.DOMDocument
    Dim wobjXMLResponse      As MSXML2.DOMDocument
    Dim wobjClass            As HSBCInterfaces.IAction
    
    Dim wvarRequest          As String
    Dim wvarResponse         As String
    
    Dim wvarColdviewMetadata As String
    Dim wvarNroSiniestroDef  As String
    Dim wvarNroSiniestroProv As String
    Dim wvarFecha            As String
    Dim wvarDocumento        As String
    Dim wvarPoliza           As String
    Dim wvarRECARGA          As String
    Dim wvarEstadoDatos      As String
    Dim wvarCanal            As String
    Dim wvarTipo             As String
    Dim wvarAutor            As String
    
    Set wobjXMLMID = CreateObject("MSXML2.DOMDocument")
    With wobjXMLMID
        .async = False
        Call .loadXML(pXMLDatosMID)
    End With
    
    With wobjXMLMID
        'Si tiene SINIENUM pasa a MID el numero de siniestro del AIS, sino pasa el de SQL
        'SOLO UNO DE LOS DOS TIENE QUE PASAR A MIDDLEWARE
        If .selectSingleNode("//SINIENUM").Text <> "" Then
            wvarNroSiniestroDef = .selectSingleNode("//RAMOPCOD").Text & "-" & .selectSingleNode("//SINIEANN").Text & "-" & .selectSingleNode("//SINIENUM").Text
            wvarNroSiniestroProv = ""
        Else
            wvarNroSiniestroDef = ""
            wvarNroSiniestroProv = Right("0000000000" & .selectSingleNode("//SINIESQL").Text, 10)
        End If
        wvarFecha = .selectSingleNode("//DENUCANN").Text & .selectSingleNode("//DENUCMES").Text & .selectSingleNode("//DENUCDIA").Text
        wvarDocumento = .selectSingleNode("//DOCUMDAT").Text
        wvarPoliza = .selectSingleNode("//RAMOPCOD").Text & .selectSingleNode("//POLIZANN").Text & .selectSingleNode("//POLIZSEC").Text & .selectSingleNode("//CERTIPOL").Text & .selectSingleNode("//CERTIANN").Text & .selectSingleNode("//CERTISEC").Text
        wvarRECARGA = .selectSingleNode("//RECARGA").Text
        wvarEstadoDatos = .selectSingleNode("//ESTADODAT").Text
        wvarCanal = .selectSingleNode("//GRABSVIA").Text
        wvarAutor = "LBA_VO_FrontEnd"
        wvarTipo = "LBASiniestros"
        wvarDENUNMAIL = .selectSingleNode("//DENUNMAIL").Text
        
        'DA - 24/10/2008: se agrega a pedido de CS
        If Not .selectSingleNode("//DENUNMAILCC") Is Nothing Then
            wvarDENUNMAILCC = .selectSingleNode("//DENUNMAILCC").Text
        Else
            wvarDENUNMAILCC = ""
        End If
        
    End With
    
    wvarColdviewMetadata = "NroSiniestroDef=" & wvarNroSiniestroDef & _
                           ";NroSiniestroProv=" & wvarNroSiniestroProv & _
                           ";Fecha=" & wvarFecha & _
                           ";documento=" & wvarDocumento & _
                           ";poliza=" & wvarPoliza & _
                           ";recarga=" & wvarRECARGA & _
                           ";estadodatos=" & wvarEstadoDatos & _
                           ";canal=" & wvarCanal & _
                           ";tipo=" & wvarTipo & _
                           ";autor=" & wvarAutor & _
                           ";Category=Ordinario"
    
    wvarRequest = "<Request>"
    wvarRequest = wvarRequest & "<xmlDataSource>" & pXMLDatosMID & "</xmlDataSource>"
    wvarRequest = wvarRequest & "<Metodo>" & pMetodo & "</Metodo>"
    wvarRequest = wvarRequest & "<ColdviewMetadata>" & wvarColdviewMetadata & "</ColdviewMetadata>"
    wvarRequest = wvarRequest & "<EmailTo>" & wvarDENUNMAIL & "</EmailTo>"
    wvarRequest = wvarRequest & "<recipientsCC>" & wvarDENUNMAILCC & "</recipientsCC>"
    wvarRequest = wvarRequest & "<ReportId>" & pReportId & "</ReportId>"
    wvarRequest = wvarRequest & "<DevuelvePDF>N</DevuelvePDF>"
    wvarRequest = wvarRequest & "<attachPassword>" & pPDFPWD & "</attachPassword>"
    wvarRequest = wvarRequest & "</Request>"

    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_SiniDenuncia.lbaw_siniInsertMID")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    With wobjXMLResponse
        .async = False
        Call .loadXML(wvarResponse)
    End With
    
    If Not wobjXMLResponse.selectSingleNode("//ESTADOMID") Is Nothing Then
        If Trim(wobjXMLResponse.selectSingleNode("//ESTADOMID").Text) = "E" Then
            fncEnviaMiddleware = ""
        Else
            fncEnviaMiddleware = wobjXMLResponse.selectSingleNode("//ESTADOMID").Text
        End If
    Else
        'Si fallo el envio a MID, devuelve condici�n de error
        fncEnviaMiddleware = "PCE"
    End If
    
End Function

Private Sub ActualizarMarenvAIS(ByVal pvarSINIENUMSQL As String)
    
    Dim wvarRequest          As String
    Dim wvarResponse         As String
    Dim wobjClass            As HSBCInterfaces.IAction
    '
    wvarRequest = "<Request>"
    wvarRequest = wvarRequest & "<SINIENUMSQL>" & pvarSINIENUMSQL & "</SINIENUMSQL>"
    wvarRequest = wvarRequest & "</Request>"
    '
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_SiniDenuncia.lbaw_siniUpdMenvAIS")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    
End Sub
'DA - 04/11/2008: nueva funci�n
Private Sub ActualizarMarenvAIS_X(ByVal pvarSINIENUMSQL As String)
    
    Dim wvarRequest          As String
    Dim wvarResponse         As String
    Dim wobjClass            As HSBCInterfaces.IAction
    '
    wvarRequest = "<Request>"
    wvarRequest = wvarRequest & "<SINIENUMSQL>" & pvarSINIENUMSQL & "</SINIENUMSQL>"
    wvarRequest = wvarRequest & "</Request>"
    '
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_SiniDenuncia.lbaw_siniUpdMenvAISX")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    
End Sub

Private Sub ObjectControl_Activate()

    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter

    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub






