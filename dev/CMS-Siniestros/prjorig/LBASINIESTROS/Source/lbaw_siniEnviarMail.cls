VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "lbaw_siniEnviarMail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-----------------------------------------------------------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
'           CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
'
'THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
'IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
'DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
'TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
'FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
'OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
'INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
'OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
'SUBSTANTIAL DAMAGES.
'-------------------------------------------------------------------------------------------------------------------------------------
' Module Name : lbaw_siniEnviarMail
' File Name : lbaw_siniEnviarMail.cls
' Creation Date: 07/12/2005
' Programmer : Fernando Osores / Rodrigo Goncalves.
' Abstract :    Envia un mail en función de los parametros de entrada y de un archivo
'       XML de configuración. Este debe estar dentro del directorio de ejecucion en
'       /TemplateMail.
' *****************************************************************
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_SiniDenuncia.lbaw_siniEnviarMail"

'Parametros XML de Entrada
Const mcteParam_PARAMETROS      As String = "//PARAMETROS/PARAMETRO"
Const mcteParam_PARAM_NOMBRE    As String = "PARAM_NOMBRE"
Const mcteParam_PARAM_VALOR     As String = "PARAM_VALOR"
Const mcteParam_FORMAT_FILE     As String = "//FORMAT_FILE"
Const mcteParam_FROM            As String = "//FROM"
Const mcteParam_SUBJECT         As String = "//SUBJECT"
Const mcteParam_TO              As String = "//TO"
Const mcteParam_REPLYTO         As String = "//REPLYTO"
Const mcteParam_IMPORTANCE      As String = "//IMPORTANCE"
Const mcteParam_BODYFORMAT      As String = "//BODYFORMAT"
Const mcteParam_MAILFORMAT      As String = "//MAILFORMAT"
Const mcteParam_BODY            As String = "//BODY"

' *****************************************************************
' Function : IAction_Execute
' Abstract : Envia un mail on DEMAND.
' Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLFormat       As MSXML2.DOMDocument
    Dim wobjElement         As MSXML2.IXMLDOMElement
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    '
    Dim wvarFormatFile      As String
    Dim wvarParamNombre     As String
    Dim wvarParamValor      As String
    Dim wvarFrom            As String
    Dim wvarTo              As String
    Dim wvarSubject         As String
    Dim wvarReplyTo         As String
    Dim wvarImportance      As String
    Dim wvarBodyFormat      As String
    Dim wvarMailFormat      As String
    Dim wvarBody            As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    ' CARGO EL XML DE ENTRADA
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        '
        wvarStep = 20
        wvarFormatFile = .selectSingleNode(mcteParam_FORMAT_FILE).Text
        '
        ' CARGO EL XML CON EL FORMATO DEL MAIL
        '
        wvarStep = 30
        Set wobjXMLFormat = CreateObject("MSXML2.DOMDocument")
        wobjXMLFormat.async = False
        Debug.Print wobjXMLFormat.loadXML(Request)
        
        Debug.Print wobjXMLFormat.Load(App.Path & "\TemplateMail\" & wvarFormatFile & ".xml")
        '
        ' CARGO EL RESTO DE LOS PARAMETROS
        '
        
        wvarFrom = wobjXMLFormat.selectSingleNode(mcteParam_FROM).Text
        wvarTo = .selectSingleNode(mcteParam_TO).Text
        
        ' SI MANDA UN NODO SUBJECT USO ESE, SINO USO EL DEFAULT
        If .selectNodes(mcteParam_SUBJECT).length = 0 Then
            wvarSubject = wobjXMLFormat.selectSingleNode(mcteParam_SUBJECT).Text
        Else
            If .selectSingleNode(mcteParam_SUBJECT).Text = "" Then
                wvarSubject = wobjXMLFormat.selectSingleNode(mcteParam_SUBJECT).Text
            Else
                wvarSubject = .selectSingleNode(mcteParam_SUBJECT).Text
            End If
        End If
        '
        wvarReplyTo = wobjXMLFormat.selectSingleNode(mcteParam_REPLYTO).Text
        wvarImportance = wobjXMLFormat.selectSingleNode(mcteParam_IMPORTANCE).Text
        wvarBodyFormat = wobjXMLFormat.selectSingleNode(mcteParam_BODYFORMAT).Text
        wvarMailFormat = wobjXMLFormat.selectSingleNode(mcteParam_MAILFORMAT).Text
        wvarBody = wobjXMLFormat.selectSingleNode(mcteParam_BODY).Text
        '
        wvarStep = 40
        '
        ' RECORRO LA LISTA DE PARAMETROS Y VALORES
        For Each wobjElement In .selectNodes(mcteParam_PARAMETROS)
            wvarParamNombre = wobjElement.selectSingleNode(mcteParam_PARAM_NOMBRE).Text
            wvarParamValor = wobjElement.selectSingleNode(mcteParam_PARAM_VALOR).Text
            '
            wvarBody = Replace(wvarBody, wvarParamNombre, wvarParamValor)
        Next
    End With
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLFormat = Nothing
    Set wobjElement = Nothing
    '
    ' EJECUTO EL ENVIO DE MAILS
    wvarStep = 50
    If p_Send_EMail(wvarFrom, wvarTo, wvarSubject, wvarImportance, wvarBodyFormat, wvarMailFormat, wvarBody, wvarReplyTo) Then
        wvarStep = 60
        Response = "<Response><Estado resultado=""true"" mensaje=""EMAIL ENVIADO"" /></Response>"
    Else
        wvarStep = 70
        Response = "<Response><Estado resultado=""false"" mensaje=""NO SE PUDO ENVIAR EL EMAIL."" /></Response>"
    End If
    '
    wvarStep = 80
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLRequest = Nothing
    Set wobjXMLFormat = Nothing
    Set wobjElement = Nothing
    '
'    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
'                     mcteClassName, _
'                     wcteFnName, _
 '                    wvarStep, _
                     Err.Number, _
  '                   "Error= [" & Err.Number & "] - " & Err.Description, _
   '                  vbLogEventTypeError
    IAction_Execute = 1
'    mobjCOM_Context.SetAbort
End Function

' *****************************************************************
' Function : p_Send_EMail
' Abstract : Realiza el envio de mails utilizando CDONTS
' Synopsis : Private Function p_Send_EMail(ByVal pFrom As String, ByVal pTo As String,
'                            ByVal pSubject As String, ByVal pImportance As String,
'                            ByVal pBodyFormat As String, ByVal pMailFormat As String,
'                            ByVal pBody As String) As Boolean
' *****************************************************************
Private Function p_Send_EMail(ByVal pvarFrom As String, ByVal pvarTo As String, _
                            ByVal pvarSubject As String, ByVal pvarImportance As String, _
                            ByVal pvarBodyFormat As String, ByVal pvarMailFormat As String, _
                            ByVal pvarBody As String, ByVal pvarReplyTo As String) As Boolean
    Dim wobjMail As CDONTS.NewMail
    '
    Set wobjMail = CreateObject("CDONTS.NewMail")
    '
    On Error GoTo ErrorCreateMail
    '
    wobjMail.From = pvarFrom
    wobjMail.To = pvarTo
    wobjMail.Value("Reply-To") = pvarReplyTo
    wobjMail.Subject = pvarSubject
    wobjMail.Importance = pvarImportance
    wobjMail.BodyFormat = CdoBodyFormatHTML 'pvarBodyFormat
    'wobjMail.MailFormat = pvarMailFormat
    wobjMail.MailFormat = CdoMailFormatMime
    'wobjMail.ContentBase = "inline"
    'wobjMail.AttachURL App.Path + "\cam_OficinaVirtual\WAVES.GIF", "pirulo.gif"
    'wobjMail.AttachFile App.Path + "\cam_OficinaVirtual\WAVES.GIF", "prueba.gif"
    wobjMail.Body = pvarBody
    wobjMail.send
    '
    Set wobjMail = Nothing
    '
    p_Send_EMail = True
    Exit Function
ErrorCreateMail:
    p_Send_EMail = False
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub




