package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class BITMAPPalette implements com.qbe.services.Cloneable
{
  public byte lngBlue = 0;
  public byte lngGreen = 0;
  public byte lngRed = 0;
  public byte lngReserved = 0;

  public Object clone()
  {
    try 
    {
      BITMAPPalette clone = (BITMAPPalette) super.clone();
      //todo: make clones of any members that are objects ie. clone.obj = obj.clone();
      return clone;
    } 
    catch( CloneNotSupportedException e ) {}
    return null;
  }
}
