package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class TABLACUANTIZACION implements com.qbe.services.Cloneable
{
  public int[] mvarCuantizacion = new int[65];
  public double[] mvarEscala = new double[65];

  public Object clone()
  {
    try 
    {
      TABLACUANTIZACION clone = (TABLACUANTIZACION) super.clone();
      //todo: make clones of any members that are objects ie. clone.obj = obj.clone();
      clone.mvarCuantizacion = mvarCuantizacion == null ? null : (int[]) mvarCuantizacion.clone();
      clone.mvarEscala = mvarEscala == null ? null : (double[]) mvarEscala.clone();
      return clone;
    } 
    catch( CloneNotSupportedException e ) {}
    return null;
  }
}
