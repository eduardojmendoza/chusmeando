package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_siniGetLista implements Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniGetLista";
  static final String mcteStoreProc = "P_SINI_SELECT_SINIESTRO_LISTA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    Object wobjHSBC_DBCnn = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
    String wvarResult = "";
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String mvarSINIEANNSQL = "";
    String mvarSINIENUMSQL = "";
    //
    //
    //Parámetros de entrada
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      wobjDBCmd = new Command();


      wvarStep = 40;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );

      if( ! (wrstDBResult.isEOF()) )
      {
        //
        wvarStep = 140;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 150;
        //unsup wobjXSLResponse.async = false;
        wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );

        wvarStep = 150;
        /*unsup wrstDBResult.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
        //
        wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );

        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><LISTADO>" + wvarResult + "</LISTADO></Response>" );

        wvarStep = 180;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
      }
      else
      {
        wvarMensaje = "No hay denuncias pendientes de ingresar al AIS / MIDDLEWARE";
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='" + wvarMensaje + "' /></Response>" );
      }


      wvarStep = 140;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      wvarStep = 150;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;

        if( ! (wrstDBResult == (Recordset) null) )
        {
          if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;

        wobjHSBC_DBCnn = null;

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";

    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='DENUNCIA'>";

    wvarStrXSL = wvarStrXSL + "      <xsl:element name='RAMOPCOD'><xsl:value-of select='@RAMOPCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SINIENUMSQL'><xsl:value-of select='@SINIENUMSQL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SINIEANNAIS'><xsl:value-of select='@SINIEANNAIS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SINIENUMAIS'><xsl:value-of select='@SINIENUMAIS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NROFORM'><xsl:value-of select='@NROFORM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='GRABSFH'><xsl:value-of select='@GRABSFH' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ESTADODAT'><xsl:value-of select='@ESTADODAT' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='RECARGA'><xsl:value-of select='@RECARGA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='MARENVAIS'><xsl:value-of select='@MARENVAIS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ESTADOAIS'><xsl:value-of select='@ESTADOAIS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='MARENVMID'><xsl:value-of select='@MARENVMID' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ESTADOMID'><xsl:value-of select='@ESTADOMID' /></xsl:element>";

    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    //wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='XMLDatosAIS'/>"
    //wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='XMLDatosMID'/>"
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";

    p_GetXSL = wvarStrXSL;

    return p_GetXSL;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
