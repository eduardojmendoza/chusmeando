package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_siniGeneraORep implements Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniGeneraORep";
  static final String mcteStoreProc = "P_SINI_INSERT_ORDENREP";
  /**
   * 
   */
  static final String mcteParam_TALLECOD = "//TALLECOD";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String mvarTALLECOD = "";
    int mvarID_ORDEN = 0;
    //
    //
    //Parámetros de entrada
    //Parámetros de salida
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 20;
      mvarTALLECOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TALLECOD ) */ );

      wvarStep = 30;
      //TODO: OJOOOOOOOOOOOO Debe ser un DBConnectionTrx (con Trx)
      wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      wobjDBCmd = new Command();

      wvarStep = 40;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 50;
      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( "0" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 60;
      wobjDBParm = new Parameter( "@TALLECOD", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( mvarTALLECOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 70;
      wobjDBParm = new Parameter( "@ID_ORDEN", AdoConst.adInteger, AdoConst.adParamOutput, 0, new Variant( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 80;
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );

      wvarStep = 90;
      //Controlamos la respuesta del SQL
      if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><ID_ORDEN>" + Strings.right( ("0000000000" + wobjDBCmd.getParameters().getParameter("@ID_ORDEN").getValue()), 10 ) + "</ID_ORDEN></Response>" );
      }
      else
      {
        wvarMensaje = "Error al insertar en la tabla SQL SINI_SINIESTROS";
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='" + wvarMensaje + "' /></Response>" );
      }

      wvarStep = 100;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      wvarStep = 110;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
