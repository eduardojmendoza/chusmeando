package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class SAFEARRAYBOUND implements com.qbe.services.Cloneable
{
  public int cElements = 0;
  public int lLbound = 0;

  public Object clone()
  {
    try 
    {
      SAFEARRAYBOUND clone = (SAFEARRAYBOUND) super.clone();
      //todo: make clones of any members that are objects ie. clone.obj = obj.clone();
      return clone;
    } 
    catch( CloneNotSupportedException e ) {}
    return null;
  }
}
