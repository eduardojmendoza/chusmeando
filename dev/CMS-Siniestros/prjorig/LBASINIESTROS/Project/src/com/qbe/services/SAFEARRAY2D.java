package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class SAFEARRAY2D implements com.qbe.services.Cloneable
{
  public int cDims = 0;
  public int fFeatures = 0;
  public int cbElements = 0;
  public int cLocks = 0;
  public int pvData = 0;
  public com.qbe.services.SAFEARRAYBOUND[] Bounds = (com.qbe.services.SAFEARRAYBOUND[]) VB.initArray( new com.qbe.services.SAFEARRAYBOUND[2], com.qbe.services.SAFEARRAYBOUND.class );

  public Object clone()
  {
    try 
    {
      SAFEARRAY2D clone = (SAFEARRAY2D) super.clone();
      //todo: make clones of any members that are objects ie. clone.obj = obj.clone();
      //todo: clone objects in array
      clone.Bounds = Bounds == null ? null : (com.qbe.services.SAFEARRAYBOUND[]) Bounds.clone();
      return clone;
    } 
    catch( CloneNotSupportedException e ) {}
    return null;
  }
}
