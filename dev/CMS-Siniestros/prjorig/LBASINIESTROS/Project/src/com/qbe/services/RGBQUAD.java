package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class RGBQUAD implements com.qbe.services.Cloneable
{
  public byte rgbBlue = 0;
  public byte rgbGreen = 0;
  public byte rgbRed = 0;
  public byte rgbReserved = 0;

  public Object clone()
  {
    try 
    {
      RGBQUAD clone = (RGBQUAD) super.clone();
      //todo: make clones of any members that are objects ie. clone.obj = obj.clone();
      return clone;
    } 
    catch( CloneNotSupportedException e ) {}
    return null;
  }
}
