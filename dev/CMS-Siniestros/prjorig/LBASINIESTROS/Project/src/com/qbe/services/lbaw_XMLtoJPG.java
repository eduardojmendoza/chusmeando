package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * ******************************************************************************
 * COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
 * LIMITED 2007. ALL RIGHTS RESERVED
 * This software is only to be used for the purpose for which it has been provided.
 * No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
 * system or translated in any human or computer language in any way or for any other
 * purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
 * Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
 * offence, which can result in heavy fines and payment of substantial damages
 * ******************************************************************************
 * Nombre del Modulo: GeneraImagen - BMP
 * Fecha de Creación: 25/04/2007
 * PPcR: xxxxxxx -x
 * Desarrollador: Gabriel E. D'Agnone
 * Descripción: Superponer Imagenes BMP, generando como salida imagen con compresión Jpeg en formato BASE64
 * 
 * 
 * 
 * Objetos del FrameWork
 * Encabezado imagen de Windows, total 40 bytes
 * Paleta de colores en Windows, 4 bytes * 256 = 1024
 */

public class lbaw_XMLtoJPG implements Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_XMLtoJPG";
  /**
   *  Tabla para reordenar en Zig-Zag los pixels del
   *  bloque 8x8 pixels
   * Constantes
   *  Marca DCT Base
   */
  private static final int mcteSOF0 = 0xc0;
  private static final int mcteDHT = 0xc4;
  /**
   *  Marca Comienzo Imagen
   */
  private static final int mcteSOI = 0xd8;
  /**
   *  Marca Fin imagen
   */
  private static final int mcteEOI = 0xd9;
  /**
   * 
   */
  private static final int mcteSOS = 0xda;
  private static final int mcteDQT = 0xdb;
  private static final int mcteCOM = 0xfe;
  private static final int BI_RGB = 0;
  private static final int DIB_RGB_COLORS = 0;
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private int mvarPrecisionSampleo = 0;
  private int mvarNroLineas = 0;
  private int mvarNroColumnas = 0;
  private int mvarNroBloques = 0;
  private int mvarColumnasMax = 0;
  private int mvarFilasMax = 0;
  private byte[] mvarDataJpeg = null;
  private int mvarChrJpeg = 0;
  private int mvarPtrJpeg = 0;
  private int mvarPtrBit = 0;
  private float[][] mvarBufferBlock = new float[8][8];
  private com.qbe.services.TABLACUANTIZACION[] mvarTablasCuantizacion = (com.qbe.services.TABLACUANTIZACION[]) VB.initArray( new com.qbe.services.TABLACUANTIZACION[4], com.qbe.services.TABLACUANTIZACION.class );
  private com.qbe.services.TABLAHUFFMAN[] mvarTablasDC = (com.qbe.services.TABLAHUFFMAN[]) VB.initArray( new com.qbe.services.TABLAHUFFMAN[4], com.qbe.services.TABLAHUFFMAN.class );
  private com.qbe.services.TABLAHUFFMAN[] mvarTablasAC = (com.qbe.services.TABLAHUFFMAN[]) VB.initArray( new com.qbe.services.TABLAHUFFMAN[4], com.qbe.services.TABLAHUFFMAN.class );
  private com.qbe.services.BLOQUE[] mvarBloqueActual = null;
  private int mvarCalidad = 0;
  private String mvarComentario = "";
  /**
   * ******************************************************************************
   *  VARIABLES DE MODULO
   * ******************************************************************************
   */
  private String mvarErrorDescri = "";
  /**
   * 
   */
  private com.qbe.services.WINBMPFILEHEADER mvarEncabezadoImagen = new com.qbe.services.WINBMPFILEHEADER();
  /**
   * 
   */
  private com.qbe.services.BITMAPINFOHEADER mvarInfoImagen = new com.qbe.services.BITMAPINFOHEADER();
  /**
   *  Array con la paleta de colores utilizada
   */
  private com.qbe.services.BITMAPPalette[] mvarPaleta = null;
  /**
   *  Color RGB utilizado para dar transparencia
   */
  private com.qbe.services.BITMAPPalette mvarPaletaTrans = new com.qbe.services.BITMAPPalette();
  private byte[][] mvarImagen = null;
  /**
   *  Array dos dimensiones (x,y) con la imagen
   *    completa en pixels
   */
  private byte[][] mvarColorR = null;
  /**
   *  Array dos dimensiones (x,y) con la componente
   *    de color rojo
   */
  private byte[][] mvarColorG = null;
  /**
   *  idem para color verde
   */
  private byte[][] mvarColorB = null;
  /**
   *  idem color azul
   */
  private byte[] mvarImagenJPG = null;
  /**
   *  Array 1 dimension con los bytes de la imagen
   *    comprimida en formato JPG
   *  Dimension x de la imagen BMP multiplo de 4
   */
  private int mvarPitch = 0;
  /**
   *  Puntero a la paleta mvarPaleta() con el color
   */
  private int mvarTransparencia = 0;
  /**
   * Variables de Modulo
   */
  private int[] mvarCuantLuminancia = new int[65];
  /**
   *  Cantidad de luminancia del bloque 8x8
   */
  private int[] mvarCuantCrominancia = new int[65];
  /**
   *  Cantidad de crominancia del bloque 8x8
   */
  private double[] mvarEscalaDCT = new double[8];
  /**
   *  Escala Transformada Discreta del Coseno (DCT)
   */
  private int[][] mvarZigZag = new int[8][8];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";
  /**
   * static variable for method: TransformadaDiscretaCoseno
   */
  private float t0 = 0;
  /**
   * static variable for method: TransformadaDiscretaCoseno
   */
  private float t1 = 0;
  /**
   * static variable for method: TransformadaDiscretaCoseno
   */
  private float t2 = 0;
  /**
   * static variable for method: TransformadaDiscretaCoseno
   */
  private float t3 = 0;
  /**
   * static variable for method: TransformadaDiscretaCoseno
   */
  private float t4 = 0;
  /**
   * static variable for method: TransformadaDiscretaCoseno
   */
  private float t5 = 0;
  /**
   * static variable for method: TransformadaDiscretaCoseno
   */
  private float t6 = 0;
  /**
   * static variable for method: TransformadaDiscretaCoseno
   */
  private float t7 = 0;
  /**
   * static variable for method: TransformadaDiscretaCoseno
   */
  private float t8 = 0;
  /**
   * static variable for method: TransformadaDiscretaCoseno
   */
  private int wvari = 0;
  /**
   * static variable for method: OptimizaTablasHuffman
   * static variable for method: InsertSequentialScans
   */
  private final int MaxNb = 10;

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    org.w3c.dom.NodeList wobjImagenes = null;
    org.w3c.dom.Node wobjNodoImagen = null;
    org.w3c.dom.Element wobjEle = null;
    String wvarPlantilla = "";
    String wvarImagen = "";
    String wvarSalida = "";
    String wvarResponseXML = "";
    String wvarExisteArchivo = "";
    int wvarPosX = 0;
    int wvarPosY = 0;
    int wvarStep = 0;
    int wvarFileGet = 0;
    Variant wvarx = new Variant();
    int wvarY = 0;
    byte[] wvarArrBytes = null;

    //
    // busca nombre archivo para ver si existe
    wvarStep = 10;

    try 
    {

      wvarExisteArchivo = "";

      // ****** Lee XML de Request ************************************
      wobjXMLRequest = new diamondedge.util.XmlDom();

      //unsup wobjXMLRequest.async = false;

      wobjXMLRequest.loadXML( pvarRequest );

      mvarPaletaTrans.lngRed = Byte.valueOf( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//transparencia" ) */.getAttributes().getNamedItem( "rojo" ) ) ).byteValue();
      mvarPaletaTrans.lngGreen = Byte.valueOf( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//transparencia" ) */.getAttributes().getNamedItem( "verde" ) ) ).byteValue();
      mvarPaletaTrans.lngBlue = Byte.valueOf( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//transparencia" ) */.getAttributes().getNamedItem( "azul" ) ) ).byteValue();

      wvarStep = 20;
      wvarPlantilla = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//croquis" ) */.getAttributes().getNamedItem( "mapa" ) );

      wvarStep = 30;
      wobjImagenes = null /*unsup wobjXMLRequest.selectNodes( "//objetos/imagen" ) */;

      wvarStep = 40;

      // ***** Carga archivo con mapa ***********************************************************
      wvarExisteArchivo = "";

      wvarExisteArchivo = "" /*unsup this.Dir( System.getProperty("user.dir") + "\\Images\\" + wvarPlantilla + ".bmp", 0 ) */;

      mvarErrorDescri = "";

      if( !wvarExisteArchivo.equals( "" ) )
      {

        invoke( "CargarPlantilla", new Variant[] { new Variant(wvarPlantilla) } );
      }
      else
      {

        mvarErrorDescri = "El archivo croquis: " + wvarPlantilla + " ,no existe o no es .bmp";
        //unsup GoTo ErrorHandler
      }

      wvarStep = 50;

      // ******** Carga objeto imagenes y las superpone a mapa************************************
      for( int nwobjNodoImagen = 0; nwobjNodoImagen < wobjImagenes.getLength(); nwobjNodoImagen++ )
      {
        wobjNodoImagen = wobjImagenes.item( nwobjNodoImagen );

        wvarImagen = diamondedge.util.XmlDom.getText( wobjNodoImagen.getAttributes().getNamedItem( "archivo" ) );

        wvarPosX = Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodoImagen.getAttributes().getNamedItem( "x" ) ) );
        wvarPosY = Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodoImagen.getAttributes().getNamedItem( "y" ) ) );

        //wvarExisteArchivo = Dir(Server.MapPath(wvarImagen))
        wvarExisteArchivo = "";
        wvarExisteArchivo = "" /*unsup this.Dir( System.getProperty("user.dir") + "\\Images\\" + wvarImagen + ".bmp", 0 ) */;
        if( !wvarExisteArchivo.equals( "" ) )
        {

          invoke( "AgregarImagen", new Variant[] { new Variant(wvarImagen), new Variant(wvarPosX), new Variant(wvarPosY) } );

        }
        else
        {
          mvarErrorDescri = "El archivo a superponer: " + wvarPlantilla + " ,no existe o no es .bmp";
          //unsup GoTo ErrorHandler
        }
        if( !mvarErrorDescri.equals( "" ) )
        {
          //unsup GoTo ErrorHandler
        }

      }


      // *********** Comprime la imagen resultado a Jpeg**********************************
      invoke( "GeneraJpeg", new Variant[] {} );

      // **********************************************************************************
      wvarResponseXML = "<Response><Estado resultado='true' mensaje='' /></Response>";

      // ************ Convierte Imagen Jpeg en Base64 **************************************
      wvarStep = 60;
      wobjXMLResponse = new diamondedge.util.XmlDom();

      //unsup wobjXMLResponse.async = false;

      wobjXMLResponse.loadXML( wvarResponseXML );

      //unsup wobjXMLResponse.resolveExternals = true;

      wobjEle = wobjXMLResponse.getDocument().createElement( "BinData" );

      /*unsup wobjXMLResponse.selectSingleNode( "//Response" ) */.appendChild( wobjEle );

      //Convierte a BASE64
      wobjEle.setAttribute( "xmlns:dt", "urn:schemas-microsoft-com:datatypes" );

      //unsup wobjEle.dataType = "bin.base64";

      //unsup wobjEle.nodeTypedValue = mvarImagenJPG;

      wvarStep = 100;

      pvarResponse.set( wobjXMLResponse.getDocument().getDocumentElement().toString() );

      wobjEle = (org.w3c.dom.Element) null;
      wobjXMLResponse = (diamondedge.util.XmlDom) null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjImagenes = (org.w3c.dom.NodeList) null;
      wobjNodoImagen = (org.w3c.dom.Node) null;

      //Libera la memoria utilizada en matrices globales
      /*unsup this.Erase( new Variant( mvarImagenJPG ), null, null, null, null, null, null ) */;
      /*unsup this.Erase( new Variant( mvarPaleta ), null, null, null, null, null, null ) */;
      /*unsup this.Erase( new Variant( mvarImagen ), null, null, null, null, null, null ) */;
      /*unsup this.Erase( new Variant( mvarColorR ), null, null, null, null, null, null ) */;
      /*unsup this.Erase( new Variant( mvarColorG ), null, null, null, null, null, null ) */;
      /*unsup this.Erase( new Variant( mvarColorB ), null, null, null, null, null, null ) */;

      wvarStep = 110;

      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      return IAction_Execute;

      // *************** Manejo de errores **********************************************************
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " --- " + mvarErrorDescri + " " + System.getProperty("user.dir") + "\\Images\\" + wvarImagen + ".bmp", vbLogEventTypeError );

        wvarResponseXML = "<Response><Estado resultado='false' mensaje='Error:" + mvarErrorDescri + ", wvarStep:" + wvarStep + " /></Response>";
        pvarResponse.set( wvarResponseXML );

        wobjEle = (org.w3c.dom.Element) null;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;
        wobjImagenes = (org.w3c.dom.NodeList) null;
        wobjNodoImagen = (org.w3c.dom.Node) null;

        if( !mvarErrorDescri.equals( "" ) )
        {
          /*unsup mobjCOM_Context.SetComplete() */;
          IAction_Execute = 0;
        }
        else
        {
          /*unsup mobjCOM_Context.SetAbort() */;
          IAction_Execute = 1;
        }

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  /**
   * DESDE ACA CLASE BMP
   * ******************************************************************************
   * ******************************************************************************
   *  Proposito:   Carga la imagen utilizada como plantilla (mapa) en el array de imagen
   *              mVarImagen, mvarPaleta y los encabezados de archivo
   *  Entrada:    pvarNombreImagen: Nombre del archivo BMP
   * 
   * *******************************************************************************
   */
  private void CargarPlantilla( String pvarNombreImagen ) throws Exception
  {
    int wvarx = 0;
    int wvarY = 0;
    int wvarFileGet = 0;
    byte wvarDummy = 0;
    int cont = 0;

    // On Error Resume Next (optionally ignored)
    //Control de bucles
    //Nro de archivo libre
    mvarErrorDescri = "";

    // Abre archivo plantilla
    wvarFileGet = FileSystem.getFreeFile();

    //error: Opening files for 'Binary' access is not supported.
    //unsup: Open App.Path & "\Images\" & pvarNombreImagen & ".bmp" For Binary Access Read As wvarFileGet
    // Carga encabezado
    //error: syntax error: near "Get":
    //unsup: Get wvarFileGet, , mvarEncabezadoImagen
    // Carga info archivo
    //error: syntax error: near "Get":
    //unsup: Get wvarFileGet, , mvarInfoImagen
    // Carga paleta de colores
    //Verifica que la paleta sea de 8 bits
    // ******************************************************************************
    // Propósito:  Superponer a la plantilla cada una de las imagenes recibidas en el XML
    //
    // Entrada:    pvarNombreImagen: Nombre del archivo BMP
    //             pvarCoordX: coordenada x
    //             pvarCoordY: coordenada y
    // *******************************************************************************
    //
  }

  private void AgregarImagen( String pvarNombreImagen, int pvarCoordX, int pvarCoordY ) throws Exception
  {
    com.qbe.services.WINBMPFILEHEADER wvarEncabezado = new com.qbe.services.WINBMPFILEHEADER();
    com.qbe.services.BITMAPINFOHEADER wvarInfoAnexar = new com.qbe.services.BITMAPINFOHEADER();
    com.qbe.services.BITMAPPalette[] wvarPaleta = null;
    com.qbe.services.BITMAPPalette wvarPaletaTrans = new com.qbe.services.BITMAPPalette();
    byte wvarColorByte = 0;
    int wvarx = 0;
    int wvarY = 0;
    byte wvarDummy = 0;
    byte[][] wvarPixelsAnexar = null;
    int wvarPitchAnexar = 0;
    int wvarFileGet = 0;
    byte[][] wvarColorR = null;
    byte[][] wvarColorG = null;
    byte[][] wvarColorB = null;
    // On Error Resume Next (optionally ignored)
    // 14 bytes
    // 40 bytes
    // Array con la paleta de colores utilizada
    // almacena temporalmente  el byte de color
    // Control bucles
    // imagen a anexar
    // nro archivo libre
    mvarErrorDescri = "";

    wvarFileGet = FileSystem.getFreeFile();

    //error: Opening files for 'Binary' access is not supported.
    //unsup: Open App.Path & "\Images\" & pvarNombreImagen & ".bmp" For Binary Access Read As wvarFileGet
    // Carga encabezado
    //error: syntax error: near "Get":
    //unsup: Get wvarFileGet, , wvarEncabezado
    // Carga info imagen
    //error: syntax error: near "Get":
    //unsup: Get wvarFileGet, , wvarInfoAnexar
    //
  }

  private void GeneraJpeg() throws Exception
  {
    int wvarAncho = 0;
    int wvarAlto = 0;

    //Dim wobjJpeg As JPEG
    //Set wobjJpeg = New JPEG
    // Se debe convertir la imagen de 8 bits a 24 bits que necesita la clase JPEG para comprimir
    //ConvierteRGB
    // Ajusta el ancho para que sea multiplo de 8 pixels
    wvarAncho = (int)Math.rint( Math.floor( (mvarInfoImagen.biWidth / 8) ) * 8 );

    // Ajusta el alto para que sea multiplo de 8 pixels
    wvarAlto = (int)Math.rint( Math.floor( (mvarInfoImagen.biHeight / 8) ) * 8 );

    mvarImagenJPG = invoke( "BMPtoJPG", new Variant[] { new Variant(wvarAncho), new Variant(wvarAlto), new Variant(mvarColorR), new Variant(mvarColorG), new Variant(mvarColorB) } );

    //Set wobjJpeg = Nothing
  }

  private void ConvierteRGB() throws Exception
  {
    int wvarx = 0;
    int wvarY = 0;
    int linea = 0;

    // On Error Resume Next (optionally ignored)
    mvarColorR = new byte[mvarPitch+1][mvarInfoImagen.biHeight+1];

    mvarColorB = new byte[mvarPitch+1][mvarInfoImagen.biHeight+1];

    mvarColorG = new byte[mvarPitch+1][mvarInfoImagen.biHeight+1];

    for( wvarY = 1; wvarY <= mvarInfoImagen.biHeight; wvarY++ )
    {

      for( wvarx = 1; wvarx <= mvarPitch; wvarx++ )
      {

        mvarColorR[wvarx - 1][wvarY - 1] = mvarPaleta[mvarImagen[wvarx][wvarY]].lngRed;
        mvarColorG[wvarx - 1][wvarY - 1] = mvarPaleta[mvarImagen[wvarx][wvarY]].lngGreen;
        mvarColorB[wvarx - 1][wvarY - 1] = mvarPaleta[mvarImagen[wvarx][wvarY]].lngBlue;

      }

    }


  }

  private byte[] BMPtoJPG( int pvarAncho, int pvarAlto, byte[] pvarImgR, byte[] pvarImgG, byte[] pvarImgB ) throws Exception
  {
    byte[] BMPtoJPG = null;
    int[] wvarIndiceBloque = null;
    int[] wvarTablaDC = null;
    int[] wvarTablaAC = null;
    int wvari = 0;



    // On Error Resume Next (optionally ignored)
    invoke( "Inicializar", new Variant[] {} );

    invoke( "SampleoImagen", new Variant[] { new Variant(pvarAncho), new Variant(pvarAlto), new Variant(pvarImgR), new Variant(pvarImgG), new Variant(pvarImgB), new Variant(0), new Variant(0) } );

    wvarIndiceBloque = new int[mvarNroBloques - 1+1];
    wvarTablaDC = new int[mvarNroBloques - 1+1];
    wvarTablaAC = new int[mvarNroBloques - 1+1];

    for( wvari = 0; wvari <= (mvarNroBloques - 1); wvari++ )
    {
      wvarIndiceBloque[wvari] = wvari;
      wvarTablaDC[wvari] = (wvari == 0 ? 0 : 1);
      wvarTablaAC[wvari] = (wvari == 0 ? 0 : 1);
    }

    wvari = invoke( "OptimizaTablasHuffman", new Variant[] { new Variant(wvarIndiceBloque), new Variant(wvarTablaDC), new Variant(wvarTablaAC), new Variant(0), new Variant(mvarNroBloques - 1) } );

    wvari = (int)Math.rint( (1.3 * wvari) + 1000 + Strings.len( mvarComentario ) );

    mvarDataJpeg = new byte[wvari+1];
    mvarPtrJpeg = 0;

    invoke( "InsertMarker", new Variant[] { new Variant(mcteSOI) } );

    invoke( "InsertJFIF", new Variant[] {} );

    if( Strings.len( mvarComentario ) > 0 )
    {
      invoke( "InsertCOM", new Variant[] { new Variant(mvarComentario) } );
    }
    invoke( "InsertCOM", new Variant[] { new Variant("JPEG Encoder Class" + System.getProperty("line.separator") + "COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2007" + System.getProperty("line.separator") + "ALL RIGHTS RESERVED") } );

    invoke( "InsertDQT", new Variant[] { new Variant(mvarPtrJpeg), new Variant(0) } );
    if( mvarNroBloques > 1 )
    {
      invoke( "InsertDQT", new Variant[] { new Variant(mvarPtrJpeg), new Variant(1) } );
    }

    invoke( "InsertSOF", new Variant[] { new Variant(mcteSOF0) } );

    invoke( "InsertDHT", new Variant[] { new Variant(mvarPtrJpeg), new Variant(0), new Variant(false) } );
    invoke( "InsertDHT", new Variant[] { new Variant(mvarPtrJpeg), new Variant(0), new Variant(true) } );
    if( mvarNroBloques > 1 )
    {
      invoke( "InsertDHT", new Variant[] { new Variant(mvarPtrJpeg), new Variant(1), new Variant(false) } );
      invoke( "InsertDHT", new Variant[] { new Variant(mvarPtrJpeg), new Variant(1), new Variant(true) } );
    }

    invoke( "InsertSequentialScans", new Variant[] { new Variant(wvarIndiceBloque), new Variant(wvarTablaDC), new Variant(wvarTablaAC), new Variant(0), new Variant(mvarNroBloques - 1) } );

    invoke( "InsertMarker", new Variant[] { new Variant(mcteEOI) } );

    mvarDataJpeg = (byte[]) VB.copyArray( mvarDataJpeg, new byte[mvarPtrJpeg - 1+1] );

    BMPtoJPG = mvarDataJpeg;
    /*unsup this.Erase( new Variant( mvarDataJpeg ), null, null, null, null, null, null ) */;

    return BMPtoJPG;
  }

  private void Inicializar() throws Exception
  {
    int wvari = 0;
    int wvarJ = 0;
    int wvarDX = 0;
    int wvarZZ = 0;

    wvari = 0;
    wvarJ = 0;
    wvarDX = 1;
    for( wvarZZ = 0; wvarZZ <= 63; wvarZZ++ )
    {
      mvarZigZag[wvari][wvarJ] = wvarZZ;
      wvari = wvari + wvarDX;
      // Matriz de Zig-Zag
      wvarJ = wvarJ - wvarDX;
      //  0   1   5   6  14  15  27  28
      if( wvari > 7 )
      {
        //  2   4   7  13  16  26  29  42
        wvari = 7;
        //  3   8  12  17  25  30  41  43
        wvarJ = wvarJ + 2;
        //  9  11  18  24  31  40  44  53
        wvarDX = -1;
        // 10  19  23  32  39  45  52  54
      }
      else if( wvarJ > 7 )
      {
        // 20  22  33  38  46  51  55  60
        wvarJ = 7;
        // 21  34  37  47  50  56  59  61
        wvari = wvari + 2;
        // 35  36  48  49  57  58  62  63
        wvarDX = 1;
      }
      else if( wvari < 0 )
      {
        //check (j>7) first
        wvari = 0;
        wvarDX = 1;
      }
      else if( wvarJ < 0 )
      {
        wvarJ = 0;
        wvarDX = -1;
      }
    }


    mvarCuantLuminancia[0] = 16;
    mvarCuantLuminancia[1] = 11;
    mvarCuantLuminancia[2] = 12;
    mvarCuantLuminancia[3] = 14;
    mvarCuantLuminancia[4] = 12;
    mvarCuantLuminancia[5] = 10;
    mvarCuantLuminancia[6] = 16;
    mvarCuantLuminancia[7] = 14;
    mvarCuantLuminancia[8] = 13;
    mvarCuantLuminancia[9] = 14;
    mvarCuantLuminancia[10] = 18;
    mvarCuantLuminancia[11] = 17;
    mvarCuantLuminancia[12] = 16;
    mvarCuantLuminancia[13] = 19;
    mvarCuantLuminancia[14] = 24;
    mvarCuantLuminancia[15] = 40;
    mvarCuantLuminancia[16] = 26;
    mvarCuantLuminancia[17] = 24;
    mvarCuantLuminancia[18] = 22;
    mvarCuantLuminancia[19] = 22;
    mvarCuantLuminancia[20] = 24;
    mvarCuantLuminancia[21] = 49;
    mvarCuantLuminancia[22] = 35;
    mvarCuantLuminancia[23] = 37;
    mvarCuantLuminancia[24] = 29;
    mvarCuantLuminancia[25] = 40;
    mvarCuantLuminancia[26] = 58;
    mvarCuantLuminancia[27] = 51;
    mvarCuantLuminancia[28] = 61;
    mvarCuantLuminancia[29] = 60;
    mvarCuantLuminancia[30] = 57;
    mvarCuantLuminancia[31] = 51;
    mvarCuantLuminancia[32] = 56;
    mvarCuantLuminancia[33] = 55;
    mvarCuantLuminancia[34] = 64;
    mvarCuantLuminancia[35] = 72;
    mvarCuantLuminancia[36] = 92;
    mvarCuantLuminancia[37] = 78;
    mvarCuantLuminancia[38] = 64;
    mvarCuantLuminancia[39] = 68;
    mvarCuantLuminancia[40] = 87;
    mvarCuantLuminancia[41] = 69;
    mvarCuantLuminancia[42] = 55;
    mvarCuantLuminancia[43] = 56;
    mvarCuantLuminancia[44] = 80;
    mvarCuantLuminancia[45] = 109;
    mvarCuantLuminancia[46] = 81;
    mvarCuantLuminancia[47] = 87;
    mvarCuantLuminancia[48] = 95;
    mvarCuantLuminancia[49] = 98;
    mvarCuantLuminancia[50] = 103;
    mvarCuantLuminancia[51] = 104;
    mvarCuantLuminancia[52] = 103;
    mvarCuantLuminancia[53] = 62;
    mvarCuantLuminancia[54] = 77;
    mvarCuantLuminancia[55] = 113;
    mvarCuantLuminancia[56] = 121;
    mvarCuantLuminancia[57] = 112;
    mvarCuantLuminancia[58] = 100;
    mvarCuantLuminancia[59] = 120;
    mvarCuantLuminancia[60] = 92;
    mvarCuantLuminancia[61] = 101;
    mvarCuantLuminancia[62] = 103;
    mvarCuantLuminancia[63] = 99;


    mvarCuantCrominancia[0] = 17;
    mvarCuantCrominancia[1] = 18;
    mvarCuantCrominancia[2] = 18;
    mvarCuantCrominancia[3] = 24;
    mvarCuantCrominancia[4] = 21;
    mvarCuantCrominancia[5] = 24;
    mvarCuantCrominancia[6] = 47;
    mvarCuantCrominancia[7] = 26;
    mvarCuantCrominancia[8] = 26;
    mvarCuantCrominancia[9] = 47;
    mvarCuantCrominancia[10] = 99;
    mvarCuantCrominancia[11] = 66;
    mvarCuantCrominancia[12] = 56;
    mvarCuantCrominancia[13] = 66;
    mvarCuantCrominancia[14] = 99;
    mvarCuantCrominancia[15] = 99;
    mvarCuantCrominancia[16] = 99;
    mvarCuantCrominancia[17] = 99;
    mvarCuantCrominancia[18] = 99;
    mvarCuantCrominancia[19] = 99;
    mvarCuantCrominancia[20] = 99;
    mvarCuantCrominancia[21] = 99;
    mvarCuantCrominancia[22] = 99;
    mvarCuantCrominancia[23] = 99;
    mvarCuantCrominancia[24] = 99;
    mvarCuantCrominancia[25] = 99;
    mvarCuantCrominancia[26] = 99;
    mvarCuantCrominancia[27] = 99;
    mvarCuantCrominancia[28] = 99;
    mvarCuantCrominancia[29] = 99;
    mvarCuantCrominancia[30] = 99;
    mvarCuantCrominancia[31] = 99;
    mvarCuantCrominancia[32] = 99;
    mvarCuantCrominancia[33] = 99;
    mvarCuantCrominancia[34] = 99;
    mvarCuantCrominancia[35] = 99;
    mvarCuantCrominancia[36] = 99;
    mvarCuantCrominancia[37] = 99;
    mvarCuantCrominancia[38] = 99;
    mvarCuantCrominancia[39] = 99;
    mvarCuantCrominancia[40] = 99;
    mvarCuantCrominancia[41] = 99;
    mvarCuantCrominancia[42] = 99;
    mvarCuantCrominancia[43] = 99;
    mvarCuantCrominancia[44] = 99;
    mvarCuantCrominancia[45] = 99;
    mvarCuantCrominancia[46] = 99;
    mvarCuantCrominancia[47] = 99;
    mvarCuantCrominancia[48] = 99;
    mvarCuantCrominancia[49] = 99;
    mvarCuantCrominancia[50] = 99;
    mvarCuantCrominancia[51] = 99;
    mvarCuantCrominancia[52] = 99;
    mvarCuantCrominancia[53] = 99;
    mvarCuantCrominancia[54] = 99;
    mvarCuantCrominancia[55] = 99;
    mvarCuantCrominancia[56] = 99;
    mvarCuantCrominancia[57] = 99;
    mvarCuantCrominancia[58] = 99;
    mvarCuantCrominancia[59] = 99;
    mvarCuantCrominancia[60] = 99;
    mvarCuantCrominancia[61] = 99;
    mvarCuantCrominancia[62] = 99;
    mvarCuantCrominancia[63] = 99;


    //0.25 / Cos(4 / 16 * PI)
    mvarEscalaDCT[0] = 0.353553390593273;
    //0.25 / Cos(1 / 16 * PI)
    mvarEscalaDCT[1] = 0.25489778955208;
    //0.25 / Cos(2 / 16 * PI)
    mvarEscalaDCT[2] = 0.270598050073098;
    //0.25 / Cos(3 / 16 * PI)
    mvarEscalaDCT[3] = 0.300672443467523;
    //0.25 / Cos(4 / 16 * PI)
    mvarEscalaDCT[4] = 0.353553390593273;
    //0.25 / Cos(5 / 16 * PI)
    mvarEscalaDCT[5] = 0.449988111568207;
    //0.25 / Cos(6 / 16 * PI)
    mvarEscalaDCT[6] = 0.653281482438186;
    //0.25 / Cos(7 / 16 * PI)
    mvarEscalaDCT[7] = 1.28145772387074;

    //Setea espacio de color 4:4:4
    invoke( "SeteaFrecuenciasSampleo", new Variant[] { new Variant(1), new Variant(1), new Variant(1), new Variant(1), new Variant(1), new Variant(1) } );
    // Setea el nivel de calidad en 50%
    setCalidadImagen( 75 );

  }

  private void SeteaFrecuenciasSampleo( int pvarHorY, int pvarVerY, int pvarHorCr, int pvarVerCr, int pvarHorCb, int pvarVerCb ) throws Exception
  {
    int wvari = 0;


    if( (pvarHorY < 1) || (pvarHorY > 4) )
    {
      Err.raise( 1, "", "Valor Sampleo invalido" );
    }
    if( (pvarVerY < 1) || (pvarVerY > 4) )
    {
      Err.raise( 1, "", "Valor Sampleo invalido" );
    }

    if( (pvarHorCr | pvarHorCb | pvarVerCr | pvarVerCb) == 0 )
    {
      mvarNroBloques = 1;
      mvarBloqueActual = (com.qbe.services.BLOQUE[]) VB.initArray( new com.qbe.services.BLOQUE[1], com.qbe.services.BLOQUE.class );
      mvarBloqueActual[0].mvarSampleoHor = 1;
      mvarBloqueActual[0].mvarSampleoVer = 1;
    }
    else
    {
      if( (pvarHorCr < 1) || (pvarHorCr > 4) )
      {
        Err.raise( 1, "", "Valor Sampleo invalido" );
      }
      if( (pvarHorCb < 1) || (pvarHorCb > 4) )
      {
        Err.raise( 1, "", "Valor Sampleo invalido" );
      }
      if( (pvarVerCr < 1) || (pvarVerCr > 4) )
      {
        Err.raise( 1, "", "Valor Sampleo invalido" );
      }
      if( (pvarVerCb < 1) || (pvarVerCb > 4) )
      {
        Err.raise( 1, "", "Valor Sampleo invalido" );
      }
      mvarNroBloques = 3;
      mvarBloqueActual = (com.qbe.services.BLOQUE[]) VB.initArray( new com.qbe.services.BLOQUE[3], com.qbe.services.BLOQUE.class );
      mvarBloqueActual[0].mvarSampleoHor = pvarHorY;
      mvarBloqueActual[0].mvarSampleoVer = pvarVerY;
      mvarBloqueActual[0].mvarTablaCuantizacion = 0;
      mvarBloqueActual[1].mvarSampleoHor = pvarHorCr;
      mvarBloqueActual[1].mvarSampleoVer = pvarVerCr;
      mvarBloqueActual[1].mvarTablaCuantizacion = 1;
      mvarBloqueActual[2].mvarSampleoHor = pvarHorCb;
      mvarBloqueActual[2].mvarSampleoVer = pvarVerCb;
      mvarBloqueActual[2].mvarTablaCuantizacion = 1;
    }

    mvarColumnasMax = -1;
    mvarFilasMax = -1;
    for( wvari = 0; wvari <= (mvarNroBloques - 1); wvari++ )
    {
      if( mvarColumnasMax < mvarBloqueActual[wvari].mvarSampleoHor )
      {
        mvarColumnasMax = mvarBloqueActual[wvari].mvarSampleoHor;
      }
      if( mvarFilasMax < mvarBloqueActual[wvari].mvarSampleoVer )
      {
        mvarFilasMax = mvarBloqueActual[wvari].mvarSampleoVer;
      }
    }

  }

  private void ExpandDQT( int Tqi ) throws Exception
  {
    int wvari = 0;
    int j = 0;
    byte k = 0;
    int maxvalue = 0;

    if( mvarPrecisionSampleo == 12 )
    {
      maxvalue = 65535;
    }
    else
    {
      maxvalue = 255;
    }

    for( wvari = 0; wvari <= 7; wvari++ )
    {
      for( j = 0; j <= 7; j++ )
      {
        k = (byte)( mvarZigZag[wvari][j] );
        if( (mvarTablasCuantizacion[Tqi].mvarCuantizacion[k] < 1) || (mvarTablasCuantizacion[Tqi].mvarCuantizacion[k] > maxvalue) )
        {
          Err.raise( 1, "", "Tabla de cuantizacion no valida" );
        }
        mvarTablasCuantizacion[Tqi].mvarEscala[k] = (mvarEscalaDCT[wvari] * mvarEscalaDCT[j]) / ((double)mvarTablasCuantizacion[Tqi].mvarCuantizacion[k]);
      }
    }

  }

  private void setCalidadImagen( int pvarValor ) throws Exception
  {
    int wvari = 0;
    int wvarQvalor = 0;
    int wvarMaxValor = 0;
    int wvarEscala = 0;
    //public
    wvarMaxValor = 255;

    if( (pvarValor > 0) && (pvarValor <= 100) )
    {
      mvarCalidad = pvarValor;

      if( mvarCalidad < 50 )
      {
        if( mvarCalidad <= 0 )
        {
          wvarEscala = 5000;
        }
        else
        {
          wvarEscala = (int)Math.rint( 5000 / mvarCalidad );
        }
      }
      else
      {
        if( mvarCalidad > 100 )
        {
          wvarEscala = 0;
        }
        else
        {
          wvarEscala = 200 - (mvarCalidad * 2);
        }
      }

      for( wvari = 0; wvari <= 63; wvari++ )
      {
        wvarQvalor = (int)Math.rint( ((mvarCuantLuminancia[wvari] * wvarEscala) + 50) / 100 );
        if( wvarQvalor <= 0 )
        {
          wvarQvalor = 1;
        }
        else if( wvarQvalor > wvarMaxValor )
        {
          wvarQvalor = wvarMaxValor;
        }
        mvarTablasCuantizacion[0].mvarCuantizacion[wvari] = wvarQvalor;
      }
      for( wvari = 0; wvari <= 63; wvari++ )
      {
        wvarQvalor = (int)Math.rint( ((mvarCuantCrominancia[wvari] * wvarEscala) + 50) / 100 );
        if( wvarQvalor <= 0 )
        {
          wvarQvalor = 1;
        }
        else if( wvarQvalor > wvarMaxValor )
        {
          wvarQvalor = wvarMaxValor;
        }
        mvarTablasCuantizacion[1].mvarCuantizacion[wvari] = wvarQvalor;
      }

      invoke( "ExpandDQT", new Variant[] { new Variant(0) } );
      invoke( "ExpandDQT", new Variant[] { new Variant(1) } );
    }

  }

  private int getCalidadImagen() throws Exception
  {
    int CalidadImagen = 0;
    //public
    CalidadImagen = mvarCalidad;
    return CalidadImagen;
  }

  private int SampleoImagen( int pvarAncho, int pvarAlto, byte[] pvarImagenR, byte[] pvarImagenG, byte[] pvarImagenB, int lSrcLeft, int lSrcTop ) throws Exception
  {
    int SampleoImagen = 0;
    int wvarPuntero = 0;
    com.qbe.services.BITMAPINFO wvarInfoBitmap = new com.qbe.services.BITMAPINFO();
    com.qbe.services.SAFEARRAY2D wvarSafeArray = new com.qbe.services.SAFEARRAY2D();
    int wvarF = 0;
    Variant wvarIndiceDCT = new Variant();
    float wvarEscalaRojo = 0;
    float wvarEscalaVerde = 0;
    float wvarEscalaAzul = 0;
    float wvarEscalaNivel = 0;
    int wvari = 0;
    int wvarJ = 0;
    int wvarP = 0;
    int wvarQ = 0;

    //Dim hDIb       As Long
    //Dim hBmpOld    As Long
    //Dim hDC        As Long
    //Dim Pixel()    As Byte
    //Dim xi         As Long
    //Dim yi         As Long
    //Dim xi2        As Long
    //Dim yi2        As Long
    //Dim xi8        As Long
    //Dim yi8        As Long
    //Dim i0         As Long
    //Dim j0         As Long
    // Variables de control de bucles
    // On Error Resume Next (optionally ignored)
    mvarPrecisionSampleo = 8;
    mvarNroLineas = pvarAlto;
    mvarNroColumnas = pvarAncho;


    wvarInfoBitmap.bmiHeader.biSize = Strings.len( wvarInfoBitmap.bmiHeader.toString() );
    wvarInfoBitmap.bmiHeader.biWidth = (pvarAncho + 7) & 0xfffffff8;
    wvarInfoBitmap.bmiHeader.biHeight = (pvarAlto + 7) & 0xfffffff8;
    wvarInfoBitmap.bmiHeader.biPlanes = 1;
    wvarInfoBitmap.bmiHeader.byBitCount = 24;
    wvarInfoBitmap.bmiHeader.biCompression = BI_RGB;
    wvarInfoBitmap.bmiHeader.biSizeImage = (((wvarInfoBitmap.bmiHeader.biWidth * 3) + 3) & 0xfffffffc) * wvarInfoBitmap.bmiHeader.biHeight;


    wvarSafeArray.cbElements = 1;
    wvarSafeArray.cDims = 2;
    wvarSafeArray.Bounds[0].lLbound = 0;
    wvarSafeArray.Bounds[0].cElements = wvarInfoBitmap.bmiHeader.biHeight;
    wvarSafeArray.Bounds[1].lLbound = 0;
    wvarSafeArray.Bounds[1].cElements = ((wvarInfoBitmap.bmiHeader.biWidth * 3) + 3) & 0xfffffffc;
    wvarSafeArray.pvData = wvarPuntero;


    for( wvarF = 0; wvarF <= (mvarNroBloques - 1); wvarF++ )
    {
      
      if( wvarF == 0 )
      {
        wvarEscalaRojo = (float)(0.299);
        wvarEscalaVerde = (float)(0.587);
        wvarEscalaAzul = (float)(0.114);
        wvarEscalaNivel = -128;
      }
      else if( wvarF == 1 )
      {
        wvarEscalaRojo = (float)(-0.16874);
        wvarEscalaVerde = (float)(-0.33126);
        wvarEscalaAzul = (float)(0.5);
        wvarEscalaNivel = 0;
      }
      else if( wvarF == 2 )
      {
        wvarEscalaRojo = (float)(0.5);
        wvarEscalaVerde = (float)(-0.41869);
        wvarEscalaAzul = (float)(-0.08131);
        wvarEscalaNivel = 0;
      }

      mvarBloqueActual[wvarF].mvarID = wvarF + 1;

      //xi = -Int(-mvarNroColumnas * .mvarSampleoHor / mvarColumnasMax)
      //yi = -Int(-mvarNroLineas * .mvarSampleoVer / mvarFilasMax)
      //xi8 = ((xi + 7) And &HFFFFFFF8)
      //yi8 = ((yi + 7) And &HFFFFFFF8)
      //error: syntax error: near " .":
      //unsup: ReDim .mvarCoefDCT(pvarAlto * pvarAncho - 1) '-1
      //-1
      //xi2 = xi8
      //yi2 = yi8
      wvarIndiceDCT.set( 0 );


      for( wvarJ = 0; wvarJ <= (int)Math.rint( ((pvarAlto / 8) - 1) ); wvarJ++ )
      {
        for( wvari = 0; wvari <= (int)Math.rint( ((pvarAncho / 8) - 1) ); wvari++ )
        {
          for( wvarP = 0; wvarP <= 7; wvarP++ )
          {
            for( wvarQ = 0; wvarQ <= 7; wvarQ++ )
            {

              mvarBufferBlock[wvarQ][wvarP] = (wvarEscalaRojo * pvarImagenR[((8 * wvari) + wvarQ)][((8 * wvarJ) + wvarP)]) + (wvarEscalaVerde * pvarImagenG[((8 * wvari) + wvarQ)][((8 * wvarJ) + wvarP)]) + (wvarEscalaAzul * pvarImagenB[((8 * wvari) + wvarQ)][((8 * wvarJ) + wvarP)]) + wvarEscalaNivel;

            }
          }

          invoke( "TransformadaDiscretaCoseno", new Variant[] {} );

          invoke( "Cauntifica", new Variant[] { new Variant(mvarBloqueActual[wvarF].mvarCoefDCT), new Variant(wvarIndiceDCT), new Variant(mvarTablasCuantizacion[mvarBloqueActual[wvarF].mvarTablaCuantizacion].mvarEscala) } );

        }

      }


    }

    return SampleoImagen;
  }

  private void TransformadaDiscretaCoseno() throws Exception
  {




    //
    for( wvari = 0; wvari <= 7; wvari++ )
    {
      t0 = mvarBufferBlock[wvari][0] + mvarBufferBlock[wvari][7];
      t1 = mvarBufferBlock[wvari][0] - mvarBufferBlock[wvari][7];
      t2 = mvarBufferBlock[wvari][1] + mvarBufferBlock[wvari][6];
      t3 = mvarBufferBlock[wvari][1] - mvarBufferBlock[wvari][6];
      t4 = mvarBufferBlock[wvari][2] + mvarBufferBlock[wvari][5];
      t5 = mvarBufferBlock[wvari][2] - mvarBufferBlock[wvari][5];
      t6 = mvarBufferBlock[wvari][3] + mvarBufferBlock[wvari][4];
      t7 = mvarBufferBlock[wvari][3] - mvarBufferBlock[wvari][4];

      t7 = t7 + t5;
      t8 = t0 - t6;
      t6 = t6 + t0;
      t0 = t2 + t4;
      //Cos(2 * PI/8)
      t2 = (float)(((t2 - t4) + t8) * 0.707106781186548);
      //
      t4 = t1 + t3;
      //Cos(2 * PI/8)
      t3 = (float)((t3 + t5) * 0.707106781186548);
      //Cos(3 * PI/8)
      t5 = (float)((t4 - t7) * 0.382683432365091);
      //Cos(PI/8) - Cos(3 * PI/8)
      t7 = (float)((t7 * 0.541196100146196) - t5);
      //Cos(PI/8) + Cos(3 * PI/8)
      t4 = (float)((t4 * 1.30656296487638) - t5);
      t5 = t1 + t3;
      t1 = t1 - t3;

      mvarBufferBlock[wvari][0] = t6 + t0;
      mvarBufferBlock[wvari][4] = t6 - t0;
      mvarBufferBlock[wvari][1] = t5 + t4;
      mvarBufferBlock[wvari][7] = t5 - t4;
      mvarBufferBlock[wvari][2] = t8 + t2;
      mvarBufferBlock[wvari][6] = t8 - t2;
      mvarBufferBlock[wvari][5] = t1 + t7;
      mvarBufferBlock[wvari][3] = t1 - t7;
    }

    for( wvari = 0; wvari <= 7; wvari++ )
    {
      t0 = mvarBufferBlock[0][wvari] + mvarBufferBlock[7][wvari];
      t1 = mvarBufferBlock[0][wvari] - mvarBufferBlock[7][wvari];
      t2 = mvarBufferBlock[1][wvari] + mvarBufferBlock[6][wvari];
      t3 = mvarBufferBlock[1][wvari] - mvarBufferBlock[6][wvari];
      t4 = mvarBufferBlock[2][wvari] + mvarBufferBlock[5][wvari];
      t5 = mvarBufferBlock[2][wvari] - mvarBufferBlock[5][wvari];
      t6 = mvarBufferBlock[3][wvari] + mvarBufferBlock[4][wvari];
      t7 = mvarBufferBlock[3][wvari] - mvarBufferBlock[4][wvari];

      t7 = t7 + t5;
      t8 = t0 - t6;
      t6 = t6 + t0;
      t0 = t2 + t4;
      //Cos(2 * PI/8)
      t2 = (float)(((t2 - t4) + t8) * 0.707106781186548);
      t4 = t1 + t3;
      //Cos(2 * PI/8)
      t3 = (float)((t3 + t5) * 0.707106781186548);
      //Cos(3 * PI/8)
      t5 = (float)((t4 - t7) * 0.382683432365091);
      //Cos(PI/8) - Cos(3 * PI/8)
      t7 = (float)((t7 * 0.541196100146196) - t5);
      //Cos(PI/8) + Cos(3 * PI/8)
      t4 = (float)((t4 * 1.30656296487638) - t5);
      t5 = t1 + t3;
      t1 = t1 - t3;

      mvarBufferBlock[0][wvari] = t6 + t0;
      mvarBufferBlock[4][wvari] = t6 - t0;
      mvarBufferBlock[1][wvari] = t5 + t4;
      mvarBufferBlock[7][wvari] = t5 - t4;
      mvarBufferBlock[2][wvari] = t8 + t2;
      mvarBufferBlock[6][wvari] = t8 - t2;
      mvarBufferBlock[5][wvari] = t1 + t7;
      mvarBufferBlock[3][wvari] = t1 - t7;
    }
  }

  private void Cauntifica( int[] pvarBloque, Variant pvarIndice, double[] pvarEscala ) throws Exception
  {
    int wvari = 0;
    int wvarJ = 0;
    int wvarZigZag = 0;

    // On Error Resume Next (optionally ignored)
    for( wvarJ = 0; wvarJ <= 7; wvarJ++ )
    {

      for( wvari = 0; wvari <= 7; wvari++ )
      {

        wvarZigZag = mvarZigZag[wvari][wvarJ];

        pvarBloque[pvarIndice.add( new Variant( wvarZigZag ) ).toInt()] = (int)Math.rint( mvarBufferBlock[wvari][wvarJ] * pvarEscala[wvarZigZag] );

      }

    }

    pvarIndice.set( pvarIndice.add( new Variant( 64 ) ) );

  }

  private int OptimizaTablasHuffman( int[] pvarIndiceBloque, int[] pvarTd, int[] pvarTa, int pvarFirstIndex, int pvarSecondIndex ) throws Exception
  {
    int OptimizaTablasHuffman = 0;
    int wvarF = 0;
    int wvarG = 0;
    int wvari = 0;
    int wvarJ = 0;
    int wvarK = 0;
    int wvarK1 = 0;
    int wvarK2 = 0;
    int wvarNb = 0;
    int[] wvarFreq = new int[257];
    int[] wvarFreq2 = null;
    boolean[] wvarIsInter = null;
    boolean[] wvarTdUsed = null;
    boolean[] wvarTaUsed = null;
    boolean wvarFlag = false;




    wvarIsInter = new boolean[pvarSecondIndex+1];
    wvarTaUsed = new boolean[4];
    wvarTdUsed = new boolean[4];



    wvarF = pvarFirstIndex;
    wvarG = pvarFirstIndex;
    wvarNb = 0;
    wvarFlag = false;
    while( wvarF <= pvarSecondIndex )
    {

      wvarNb = wvarNb + (mvarBloqueActual[pvarIndiceBloque[wvarG]].mvarSampleoHor * mvarBloqueActual[pvarIndiceBloque[wvarG]].mvarSampleoVer);
      wvarG = wvarG + 1;

      if( wvarNb > MaxNb )
      {
        wvarFlag = true;
        if( wvarF != (wvarG - 1) )
        {
          wvarG = wvarG - 1;
        }
      }
      else
      {
        if( ((wvarG - wvarF) == 3) || (wvarG > pvarSecondIndex) )
        {
          wvarFlag = true;
        }
      }

      if( wvarFlag )
      {
        if( wvarF == (wvarG - 1) )
        {
          wvarTdUsed[pvarTd[wvarF]] = true;
          wvarTaUsed[pvarTa[wvarF]] = true;
          wvarIsInter[wvarF] = false;
        }
        else
        {
          for( wvari = wvarF; wvari <= (wvarG - 1); wvari++ )
          {
            wvarTdUsed[pvarTd[wvari]] = true;
            wvarTaUsed[pvarTa[wvari]] = true;
            wvarIsInter[wvari] = true;
          }
        }
        wvarNb = 0;
        wvarF = wvarG;
        wvarFlag = false;
      }
    }



    for( wvari = 0; wvari <= 3; wvari++ )
    {
      if( wvarTdUsed[wvari] )
      {
        for( wvarF = pvarFirstIndex; wvarF <= pvarSecondIndex; wvarF++ )
        {
          if( pvarTd[wvarF] == wvari )
          {
            if( wvarIsInter[wvarF] )
            {
              invoke( "CollectStatisticsDCInterleaved", new Variant[] { new Variant(mvarBloqueActual[pvarIndiceBloque[wvarF]].mvarCoefDCT), new Variant(wvarFreq), new Variant(mvarBloqueActual[pvarIndiceBloque[wvarF]].mvarSampleoHor), new Variant(mvarBloqueActual[pvarIndiceBloque[wvarF]].mvarSampleoVer) } );
            }
            else
            {
              invoke( "CollectStatisticsDCNonInterleaved", new Variant[] { new Variant(mvarBloqueActual[pvarIndiceBloque[wvarF]].mvarCoefDCT), new Variant(wvarFreq) } );
            }
          }
        }


        wvarFreq2 = wvarFreq;
        invoke( "OptimizeHuffman", new Variant[] { new Variant(mvarTablasDC[wvari]), new Variant(wvarFreq) } );
        invoke( "ExpandHuffman", new Variant[] { new Variant(mvarTablasDC[wvari]), new Variant((new Variant(mvarPrecisionSampleo == 12) ? new Variant(15) : new Variant(11))) } );


        for( wvarJ = 0; wvarJ <= 15; wvarJ++ )
        {
          if( wvarFreq2[wvarJ] != 0 )
          {
            wvarK1 = (int)Math.rint( wvarJ + Math.floor( (Math.log( mvarTablasDC[wvari].mvarEHUFSI[wvarJ] ) * 1.442695040889) ) + 1 );
            wvarK2 = wvarK2 + (wvarFreq2[wvarJ] * wvarK1);
            wvarK = (int)Math.rint( wvarK + Math.floor( (wvarK2 / 8) ) );
            wvarK2 = wvarK2 % 8;
          }
        }

      }
      if( wvarTaUsed[wvari] )
      {
        for( wvarF = pvarFirstIndex; wvarF <= pvarSecondIndex; wvarF++ )
        {
          if( pvarTd[wvarF] == wvari )
          {
            invoke( "CollectStatisticsAC", new Variant[] { new Variant(mvarBloqueActual[pvarIndiceBloque[wvarF]].mvarCoefDCT), new Variant(wvarFreq) } );
          }
        }


        wvarFreq2 = wvarFreq;
        invoke( "OptimizeHuffman", new Variant[] { new Variant(mvarTablasAC[wvari]), new Variant(wvarFreq) } );
        invoke( "ExpandHuffman", new Variant[] { new Variant(mvarTablasAC[wvari]), new Variant(255) } );


        for( wvarJ = 0; wvarJ <= 255; wvarJ++ )
        {
          if( wvarFreq2[wvarJ] != 0 )
          {
            wvarK1 = (int)Math.rint( (wvarJ & 15) + Math.floor( (Math.log( mvarTablasAC[wvari].mvarEHUFSI[wvarJ] ) * 1.442695040889) ) + 1 );
            wvarK2 = wvarK2 + (wvarFreq2[wvarJ] * wvarK1);
            wvarK = (int)Math.rint( wvarK + Math.floor( (wvarK2 / 8) ) );
            wvarK2 = wvarK2 % 8;
          }
        }

      }
    }

    if( (wvarK2 % 8) != 0 )
    {
      wvarK = wvarK + 1;
    }
    OptimizaTablasHuffman = wvarK;

    return OptimizaTablasHuffman;
  }

  private void CollectStatisticsDCNonInterleaved( int[] data, int[] freqdc ) throws Exception
  {
    int Diff = 0;
    int Pred = 0;
    int n = 0;
    int p = 0;
    int s = 0;
    //DC Diferencia
    //DC Predictor
    //Numero de coeficientes en data()
    //Indice para el coeficiente en data()
    //
    n = (data.length-1) + 1;
    p = 0;
    Pred = 0;
    while( p != n )
    {
      Diff = data[p] - Pred;
      Pred = data[p];

      if( Diff < 0 )
      {
        s = (int)Math.rint( Math.floor( (Math.log( -Diff ) * 1.442695040889) ) + 1 );
      }
      else if( Diff > 0 )
      {
        s = (int)Math.rint( Math.floor( (Math.log( Diff ) * 1.442695040889) ) + 1 );
      }
      else
      {
        s = 0;
      }

      freqdc[s] = freqdc[s] + 1;
      p = p + 64;
    }

  }

  private void CollectStatisticsDCInterleaved( int[] data, int[] freqdc, int Hi, int Vi ) throws Exception
  {
    int[] p = null;
    int f = 0;
    int g = 0;
    int h = 0;
    int wvari = 0;
    int j = 0;
    int n = 0;
    int s = 0;
    int Diff = 0;
    int Pred = 0;
    int pLF = 0;
    int MCUr = 0;
    int MCUx = 0;
    int MCUy = 0;


    n = (data.length-1) + 1;
    p = new int[Vi - 1+1];


    MCUx = (int)Math.rint( Math.floor( ((mvarNroColumnas + (8 * mvarColumnasMax)) - 1) / (8 * mvarColumnasMax) ) );
    MCUy = (int)Math.rint( Math.floor( ((mvarNroLineas + (8 * mvarFilasMax)) - 1) / (8 * mvarFilasMax) ) );

    h = (int)Math.rint( Math.floor( (-Math.floor( ((-mvarNroColumnas * Hi) / mvarColumnasMax) ) + 7) / 8 ) );

    for( g = 0; g <= (Vi - 1); g++ )
    {
      p[g] = 64 * h * g;
    }
    pLF = 64 * h * (Vi - 1);

    MCUr = h % Hi;
    if( MCUr == 0 )
    {
      MCUr = Hi;
    }

    for( j = 1; j <= (MCUy - 1); j++ )
    {


      for( wvari = 1; wvari <= (MCUx - 1); wvari++ )
      {
        for( g = 1; g <= Vi; g++ )
        {
          for( h = 1; h <= Hi; h++ )
          {

            Diff = data[p[(g - 1)]] - Pred;
            Pred = data[p[g - 1]];
            p[g - 1] = p[(g - 1)] + 64;
            if( Diff < 0 )
            {
              s = (int)Math.rint( Math.floor( (Math.log( -Diff ) * 1.442695040889) ) + 1 );
            }
            else if( Diff > 0 )
            {
              s = (int)Math.rint( Math.floor( (Math.log( Diff ) * 1.442695040889) ) + 1 );
            }
            else
            {
              s = 0;
            }
            freqdc[s] = freqdc[s] + 1;

          }
        }
      }


      for( g = 1; g <= Vi; g++ )
      {
        for( h = 1; h <= Hi; h++ )
        {
          if( h > MCUr )
          {
            s = 0;
          }
          else
          {
            Diff = data[p[(g - 1)]] - Pred;
            Pred = data[p[g - 1]];
            p[g - 1] = p[(g - 1)] + 64;

            if( Diff < 0 )
            {
              s = (int)Math.rint( Math.floor( (Math.log( -Diff ) * 1.442695040889) ) + 1 );
            }
            else if( Diff > 0 )
            {
              s = (int)Math.rint( Math.floor( (Math.log( Diff ) * 1.442695040889) ) + 1 );
            }
            else
            {
              s = 0;
            }
          }
          freqdc[s] = freqdc[s] + 1;
        }
      }


      for( g = 0; g <= (Vi - 1); g++ )
      {
        p[g] = p[g] + pLF;
      }
    }



    for( wvari = 1; wvari <= MCUx; wvari++ )
    {
      for( g = 1; g <= Vi; g++ )
      {
        for( h = 1; h <= Hi; h++ )
        {
          if( (p[(g - 1)] >= n) || ((wvari == MCUx) && (h > MCUr)) )
          {
            s = 0;
          }
          else
          {
            Diff = data[p[(g - 1)]] - Pred;
            Pred = data[p[g - 1]];
            p[g - 1] = p[(g - 1)] + 64;

            if( Diff < 0 )
            {
              s = (int)Math.rint( Math.floor( (Math.log( -Diff ) * 1.442695040889) ) + 1 );
            }
            else if( Diff > 0 )
            {
              s = (int)Math.rint( Math.floor( (Math.log( Diff ) * 1.442695040889) ) + 1 );
            }
            else
            {
              s = 0;
            }
          }
          freqdc[s] = freqdc[s] + 1;
        }
      }
    }

  }

  private void CollectStatisticsAC( int[] data, int[] freqac ) throws Exception
  {
    int code = 0;
    int n = 0;
    int p = 0;
    int p2 = 0;
    int r = 0;
    int rs = 0;


    n = (data.length-1) + 1;
    p = 0;
    while( p != n )
    {
      p = p + 1;
      p2 = p + 63;

      r = 0;
      while( p != p2 )
      {
        if( data[p] == 0 )
        {
          r = r + 1;
        }
        else
        {
          while( r > 15 )
          {
            freqac[240] = freqac[240] + 1;
            r = r - 16;
          }
          code = data[p];
          if( code < 0 )
          {
            rs = (int)Math.rint( Math.floor( (Math.log( -code ) * 1.442695040889) ) + 1 );
          }
          else if( code > 0 )
          {
            rs = (int)Math.rint( Math.floor( (Math.log( code ) * 1.442695040889) ) + 1 );
          }
          else
          {
            rs = 0;
          }

          rs = (r * 16) | rs;
          freqac[rs] = freqac[rs] + 1;
          r = 0;
        }
        p = p + 1;
      }
      if( r != 0 )
      {
        freqac[0] = freqac[0] + 1;
      }
    }

  }

  private void OptimizeHuffman( com.qbe.services.TABLAHUFFMAN TheHuff, int[] freq ) throws Exception
  {
    int wvari = 0;
    int j = 0;
    int k = 0;
    int n = 0;
    int V1 = 0;
    int V2 = 0;
    int[] others = new int[257];
    int[] codesize = new int[257];
    int[] mvarBITS = new int[257];
    int swp = 0;
    int swp2 = 0;



    for( wvari = 0; wvari <= 256; wvari++ )
    {
      others[wvari] = -1;
    }
    freq[256] = 1;


    do
    {
      V1 = -1;
      V2 = -1;
      swp = 2147483647;
      swp2 = 2147483647;
      for( wvari = 0; wvari <= 256; wvari++ )
      {
        if( freq[wvari] != 0 )
        {
          if( freq[wvari] <= swp2 )
          {
            if( freq[wvari] <= swp )
            {
              swp2 = swp;
              V2 = V1;
              swp = freq[wvari];
              V1 = wvari;
            }
            else
            {
              swp2 = freq[wvari];
              V2 = wvari;
            }
          }
        }
      }
      if( V2 == -1 )
      {
        freq[V1] = 0;
        break;
      }
      freq[V1] = freq[V1] + freq[V2];
      freq[V2] = 0;
      codesize[V1] = codesize[V1] + 1;
      while( others[V1] >= 0 )
      {
        V1 = others[V1];
        codesize[V1] = codesize[V1] + 1;
      }
      others[V1] = V2;
      codesize[V2] = codesize[V2] + 1;
      while( others[V2] >= 0 )
      {
        V2 = others[V2];
        codesize[V2] = codesize[V2] + 1;
      }
    }
    while( true );


    n = 0;
    for( wvari = 0; wvari <= 256; wvari++ )
    {
      if( codesize[wvari] != 0 )
      {
        mvarBITS[codesize[wvari]] = mvarBITS[codesize[wvari]] + 1;
        if( n < codesize[wvari] )
        {
          n = codesize[wvari];
        }
      }
    }


    wvari = n;
    while( wvari > 16 )
    {
      while( mvarBITS[wvari] > 0 )
      {
        for( j = wvari - 2; j >= 1; j-- )
        {
          if( mvarBITS[j] > 0 )
          {
            break;
          }
        }
        mvarBITS[wvari] = mvarBITS[wvari] - 2;
        mvarBITS[wvari - 1] = mvarBITS[(wvari - 1)] + 1;
        mvarBITS[j + 1] = mvarBITS[j + 1] + 2;
        mvarBITS[j] = mvarBITS[j] - 1;
      }
      wvari = wvari - 1;
    }
    mvarBITS[wvari] = mvarBITS[wvari] - 1;


    for( wvari = 1; wvari <= 16; wvari++ )
    {
      TheHuff.mvarBITS[wvari - 1] = (byte)( mvarBITS[wvari] );
    }
    k = 0;
    for( wvari = 1; wvari <= n; wvari++ )
    {
      for( j = 0; j <= 255; j++ )
      {
        if( codesize[j] == wvari )
        {
          TheHuff.mvarHUFFVAL[k] = (byte)( j );
          k = k + 1;
        }
      }
    }

  }

  private void ExpandHuffman( com.qbe.services.TABLAHUFFMAN TheHuff, int MaxSymbol ) throws Exception
  {
    int wvari = 0;
    int j = 0;
    int k = 0;
    int si = 0;
    int code = 0;
    int symbol = 0;




    for( wvari = 0; wvari <= 255; wvari++ )
    {
      TheHuff.mvarEHUFSI[wvari] = 0;
      TheHuff.mvarEHUFCO[wvari] = -1;
    }

    j = 0;
    si = 1;
    code = 0;
    for( wvari = 0; wvari <= 15; wvari++ )
    {
      k = j + TheHuff.mvarBITS[wvari];
      if( k > 256 )
      {
        Err.raise( 1, "", "Tabla Huffman Invalida" );
      }
      if( j == k )
      {
        TheHuff.mvarMINCODE[wvari] = j - code;
        TheHuff.mvarMAXCODE[wvari] = -1;
      }
      else
      {
        TheHuff.mvarMINCODE[wvari] = j - code;
        while( j < k )
        {
          symbol = TheHuff.mvarHUFFVAL[j];
          if( symbol > MaxSymbol )
          {
            Err.raise( 1, "", "Tabla Huffman Invalida" );
          }
          if( TheHuff.mvarEHUFCO[symbol] >= 0 )
          {
            Err.raise( 1, "", "Tabla Huffman Invalida" );
          }
          TheHuff.mvarEHUFSI[symbol] = si;
          TheHuff.mvarEHUFCO[symbol] = code;
          code = code + 1;
          j = j + 1;
        }
        TheHuff.mvarMAXCODE[wvari] = code - 1;
      }
      si = si * 2;
      if( code >= si )
      {
        Err.raise( 1, "", "Tabla Huffman Invalida" );
      }
      code = code * 2;
    }
    if( j == 0 )
    {
      Err.raise( 1, "", "Tabla Huffman Invalida" );
    }

  }

  private void setComment( String Value ) throws Exception
  {
    //public
    if( Strings.len( Value ) > 65535 )
    {
      Err.raise( 1, "", "Longitud Comentario invalida" );
    }
    mvarComentario = Value;
  }

  private String getComment() throws Exception
  {
    String Comment = "";
    //public
    Comment = mvarComentario;
    return Comment;
  }

  private void InsertMarker( int TheMarker ) throws Exception
  {
    mvarDataJpeg[mvarPtrJpeg] = (byte)( 255 );
    mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( TheMarker );
    mvarPtrJpeg = mvarPtrJpeg + 2;
  }

  private void InsertJFIF() throws Exception
  {
    if( (mvarPtrJpeg + 17) > (mvarDataJpeg.length-1) )
    {
      Err.raise( 9, "", "" );
    }


    mvarDataJpeg[mvarPtrJpeg] = (byte)( 0xff );
    mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( 0xe0 );
    mvarDataJpeg[mvarPtrJpeg + 2] = (byte)( 0x0 );
    mvarDataJpeg[mvarPtrJpeg + 3] = (byte)( 0x10 );

    mvarPtrJpeg = mvarPtrJpeg + 4;


    mvarDataJpeg[mvarPtrJpeg] = (byte)( 0x4a );
    mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( 0x46 );
    mvarDataJpeg[mvarPtrJpeg + 2] = (byte)( 0x49 );
    mvarDataJpeg[mvarPtrJpeg + 3] = (byte)( 0x46 );

    mvarPtrJpeg = mvarPtrJpeg + 4;



    mvarDataJpeg[mvarPtrJpeg] = (byte)( 0x0 );
    mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( 0x1 );
    mvarDataJpeg[mvarPtrJpeg + 2] = (byte)( 0x1 );
    mvarDataJpeg[mvarPtrJpeg + 3] = (byte)( 0x1 );

    mvarPtrJpeg = mvarPtrJpeg + 4;



    mvarDataJpeg[mvarPtrJpeg] = (byte)( 0x0 );
    mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( 0x1 );
    mvarDataJpeg[mvarPtrJpeg + 2] = (byte)( 0x0 );
    mvarDataJpeg[mvarPtrJpeg + 3] = (byte)( 0x1 );

    mvarPtrJpeg = mvarPtrJpeg + 4;

    mvarDataJpeg[mvarPtrJpeg] = (byte)( 0x0 );
    mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( 0x0 );

    mvarPtrJpeg = mvarPtrJpeg + 2;


  }

  private void InsertCOM( String TheComment ) throws Exception
  {
    int wvari = 0;
    int Lx = 0;

    Lx = Strings.len( TheComment ) + 2;
    if( Lx > 2 )
    {
      mvarDataJpeg[mvarPtrJpeg] = (byte)( 255 );
      mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( mcteCOM );
      mvarDataJpeg[mvarPtrJpeg + 2] = (byte)Math.rint( Math.floor( Lx / 256 ) );
      mvarDataJpeg[mvarPtrJpeg + 3] = (byte)( Lx & 255 );
      mvarPtrJpeg = mvarPtrJpeg + 4;
      for( wvari = 1; wvari <= Strings.len( TheComment ); wvari++ )
      {
        mvarDataJpeg[mvarPtrJpeg] = (byte)( Strings.asc( Strings.mid( TheComment, wvari, 1 ) ) );
        mvarPtrJpeg = mvarPtrJpeg + 1;
      }
    }
  }

  private void InsertDQT( int MarkerPos, int Tqi ) throws Exception
  {
    int wvari = 0;


    if( mvarPtrJpeg < (MarkerPos + 4) )
    {
      mvarPtrJpeg = MarkerPos + 4;
      mvarDataJpeg[mvarPtrJpeg - 4] = (byte)( 255 );
      mvarDataJpeg[mvarPtrJpeg - 3] = (byte)( mcteDQT );
    }
    for( wvari = 0; wvari <= 63; wvari++ )
    {
      if( mvarTablasCuantizacion[Tqi].mvarCuantizacion[wvari] > 255 )
      {
        break;
      }
    }
    if( wvari == 64 )
    {
      mvarDataJpeg[mvarPtrJpeg] = (byte)( Tqi );
      mvarPtrJpeg = mvarPtrJpeg + 1;
      for( wvari = 0; wvari <= 63; wvari++ )
      {
        mvarDataJpeg[mvarPtrJpeg] = (byte)( mvarTablasCuantizacion[Tqi].mvarCuantizacion[wvari] );
        mvarPtrJpeg = mvarPtrJpeg + 1;
      }
    }
    else
    {
      if( mvarPrecisionSampleo != 12 )
      {
        Err.raise( 1, "", "Precisión ilegal en tabla de cuantización" );
      }
      mvarDataJpeg[mvarPtrJpeg] = (byte)( Tqi | 16 );
      mvarPtrJpeg = mvarPtrJpeg + 1;
      for( wvari = 0; wvari <= 63; wvari++ )
      {
        mvarDataJpeg[mvarPtrJpeg] = (byte)Math.rint( Math.floor( mvarTablasCuantizacion[Tqi].mvarCuantizacion[wvari] / 256 ) );
        mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( mvarTablasCuantizacion[Tqi].mvarCuantizacion[wvari] & 255 );
        mvarPtrJpeg = mvarPtrJpeg + 2;
      }
    }

    mvarDataJpeg[MarkerPos + 2] = (byte)Math.rint( Math.floor( ((mvarPtrJpeg - MarkerPos) - 2) / 256 ) );
    mvarDataJpeg[MarkerPos + 3] = (byte)( ((mvarPtrJpeg - MarkerPos) - 2) & 255 );
  }

  private void InsertSOF( int SOFMarker ) throws Exception
  {
    int wvari = 0;
    int Lx = 0;

    Lx = 8 + (3 * mvarNroBloques);
    mvarDataJpeg[mvarPtrJpeg] = (byte)( 255 );
    mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( SOFMarker & 255 );
    mvarDataJpeg[mvarPtrJpeg + 2] = (byte)Math.rint( Math.floor( Lx / 256 ) );
    mvarDataJpeg[mvarPtrJpeg + 3] = (byte)( Lx & 255 );
    mvarDataJpeg[mvarPtrJpeg + 4] = (byte)( mvarPrecisionSampleo );
    mvarDataJpeg[mvarPtrJpeg + 5] = (byte)Math.rint( Math.floor( mvarNroLineas / 256 ) );
    mvarDataJpeg[mvarPtrJpeg + 6] = (byte)( mvarNroLineas & 255 );
    mvarDataJpeg[mvarPtrJpeg + 7] = (byte)Math.rint( Math.floor( mvarNroColumnas / 256 ) );
    mvarDataJpeg[mvarPtrJpeg + 8] = (byte)( mvarNroColumnas & 255 );
    mvarDataJpeg[mvarPtrJpeg + 9] = (byte)( mvarNroBloques );
    mvarPtrJpeg = mvarPtrJpeg + 10;
    for( wvari = 0; wvari <= (mvarNroBloques - 1); wvari++ )
    {
      mvarDataJpeg[mvarPtrJpeg] = (byte)( mvarBloqueActual[wvari].mvarID );
      mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( (mvarBloqueActual[wvari].mvarSampleoHor * 16) | mvarBloqueActual[wvari].mvarSampleoVer );
      mvarDataJpeg[mvarPtrJpeg + 2] = (byte)( mvarBloqueActual[wvari].mvarTablaCuantizacion );
      mvarPtrJpeg = mvarPtrJpeg + 3;
    }
  }

  private void InsertDHT( int MarkerPos, int HIndex, boolean IsAC ) throws Exception
  {
    int wvari = 0;
    int j = 0;

    if( mvarPtrJpeg < (MarkerPos + 4) )
    {
      mvarPtrJpeg = MarkerPos + 4;
      mvarDataJpeg[mvarPtrJpeg - 4] = (byte)( 255 );
      mvarDataJpeg[mvarPtrJpeg - 3] = (byte)( mcteDHT );
    }
    if( IsAC )
    {
      mvarDataJpeg[mvarPtrJpeg] = (byte)( HIndex | 16 );
      mvarPtrJpeg = mvarPtrJpeg + 1;
      j = 0;
      for( wvari = 0; wvari <= 15; wvari++ )
      {
        mvarDataJpeg[mvarPtrJpeg] = mvarTablasAC[HIndex].mvarBITS[wvari];
        mvarPtrJpeg = mvarPtrJpeg + 1;
        j = j + mvarTablasAC[HIndex].mvarBITS[wvari];
      }
      for( wvari = 0; wvari <= (j - 1); wvari++ )
      {
        mvarDataJpeg[mvarPtrJpeg] = mvarTablasAC[HIndex].mvarHUFFVAL[wvari];
        mvarPtrJpeg = mvarPtrJpeg + 1;
      }
    }
    else
    {
      mvarDataJpeg[mvarPtrJpeg] = (byte)( HIndex );
      mvarPtrJpeg = mvarPtrJpeg + 1;
      j = 0;
      for( wvari = 0; wvari <= 15; wvari++ )
      {
        mvarDataJpeg[mvarPtrJpeg] = mvarTablasDC[HIndex].mvarBITS[wvari];
        mvarPtrJpeg = mvarPtrJpeg + 1;
        j = j + mvarTablasDC[HIndex].mvarBITS[wvari];
      }
      for( wvari = 0; wvari <= (j - 1); wvari++ )
      {
        mvarDataJpeg[mvarPtrJpeg] = mvarTablasDC[HIndex].mvarHUFFVAL[wvari];
        mvarPtrJpeg = mvarPtrJpeg + 1;
      }
    }

    mvarDataJpeg[MarkerPos + 2] = (byte)Math.rint( Math.floor( ((mvarPtrJpeg - MarkerPos) - 2) / 256 ) );
    mvarDataJpeg[MarkerPos + 3] = (byte)( ((mvarPtrJpeg - MarkerPos) - 2) & 255 );
  }

  private void InsertSequentialScans( int[] CompIndex, int[] Td, int[] Ta, int FirstIndex, int SecondIndex ) throws Exception
  {
    int f = 0;
    int g = 0;
    int Nb = 0;
    boolean flag = false;


    f = FirstIndex;
    g = FirstIndex;
    Nb = 0;
    flag = false;
    while( f <= SecondIndex )
    {

      Nb = Nb + (mvarBloqueActual[CompIndex[g]].mvarSampleoHor * mvarBloqueActual[CompIndex[g]].mvarSampleoVer);
      g = g + 1;

      if( Nb > MaxNb )
      {
        flag = true;
        if( f != (g - 1) )
        {
          g = g - 1;
        }
      }
      else
      {
        if( ((g - f) == 3) || (g > SecondIndex) )
        {
          flag = true;
        }
      }

      if( flag )
      {
        if( f == (g - 1) )
        {
          invoke( "InsertSOSNonInterleaved", new Variant[] { new Variant(CompIndex[f]), new Variant(Td[f]), new Variant(Ta[f]) } );
        }
        else
        {
          invoke( "InsertSOSInterleaved", new Variant[] { new Variant(CompIndex), new Variant(Td), new Variant(Ta), new Variant(f), new Variant(g - 1) } );
        }
        Nb = 0;
        f = g;
        flag = false;
      }
    }

  }

  private void InsertSOSNonInterleaved( int CompIndex, int Td, int Ta ) throws Exception
  {
    Variant p = new Variant();
    int n = 0;
    Variant Pred = new Variant();


    mvarDataJpeg[mvarPtrJpeg] = (byte)( 255 );
    mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( mcteSOS );
    mvarDataJpeg[mvarPtrJpeg + 2] = (byte)Math.rint( Math.floor( 8 / 256 ) );
    mvarDataJpeg[mvarPtrJpeg + 3] = (byte)( 8 & 255 );
    mvarDataJpeg[mvarPtrJpeg + 4] = (byte)( 1 );
    mvarPtrJpeg = mvarPtrJpeg + 5;
    mvarDataJpeg[mvarPtrJpeg] = (byte)( mvarBloqueActual[CompIndex].mvarID );
    mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( (Td * 16) | Ta );
    mvarPtrJpeg = mvarPtrJpeg + 2;
    mvarDataJpeg[mvarPtrJpeg] = (byte)( 0 );
    mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( 63 );
    mvarDataJpeg[mvarPtrJpeg + 2] = (byte)( 0 );
    mvarPtrJpeg = mvarPtrJpeg + 3;


    p.set( 0 );
    n = (mvarBloqueActual[CompIndex].mvarCoefDCT.length-1) + 1;
    Pred.set( 0 );

    invoke( "WriteBitsBegin", new Variant[] {} );
    while( p.toInt() != n )
    {
      invoke( "EncodeCoefficients", new Variant[] { new Variant(mvarBloqueActual[CompIndex].mvarCoefDCT), new Variant(p), new Variant(Pred), new Variant(Td), new Variant(Ta) } );
    }
    invoke( "WriteBitsEnd", new Variant[] {} );


  }

  private void EncodeCoefficients( int[] data, Variant p, Variant Pred, int Td, int Ta ) throws Exception
  {
    int r = 0;
    int rs = 0;
    int si = 0;
    int code = 0;
    int p2 = 0;


    p2 = p.add( new Variant( 64 ) ).toInt();

    code = data[p.toInt()] - Pred.toInt();
    Pred.set( data[p.toInt()] );
    p.set( p.add( new Variant( 1 ) ) );

    si = 1;
    rs = 0;
    if( code < 0 )
    {
      while( si <= -code )
      {
        si = si * 2;
        rs = rs + 1;
      }
      code = code - 1;
    }
    else
    {
      while( si <= code )
      {
        si = si * 2;
        rs = rs + 1;
      }
    }
    si = (int)Math.rint( Math.floor( si / 2 ) );
    invoke( "WriteBits", new Variant[] { new Variant(mvarTablasDC[Td].mvarEHUFSI[rs]), new Variant(mvarTablasDC[Td].mvarEHUFCO[rs]) } );
    invoke( "WriteBits", new Variant[] { new Variant(si), new Variant(code) } );

    r = 0;
    do
    {
      if( data[p.toInt()] == 0 )
      {
        r = r + 1;
      }
      else
      {
        while( r > 15 )
        {
          invoke( "WriteBits", new Variant[] { new Variant(mvarTablasAC[Ta].mvarEHUFSI[240]), new Variant(mvarTablasAC[Ta].mvarEHUFCO[240]) } );
          r = r - 16;
        }
        code = data[p.toInt()];
        rs = r * 16;
        si = 1;
        if( code < 0 )
        {
          while( si <= -code )
          {
            si = si * 2;
            rs = rs + 1;
          }
          code = code - 1;
        }
        else
        {
          while( si <= code )
          {
            si = si * 2;
            rs = rs + 1;
          }
        }
        si = (int)Math.rint( Math.floor( si / 2 ) );
        invoke( "WriteBits", new Variant[] { new Variant(mvarTablasAC[Ta].mvarEHUFSI[rs]), new Variant(mvarTablasAC[Ta].mvarEHUFCO[rs]) } );
        invoke( "WriteBits", new Variant[] { new Variant(si), new Variant(code) } );
        r = 0;
      }
      p.set( p.add( new Variant( 1 ) ) );
    }
    while( p.toInt() < p2 );
    if( r != 0 )
    {
      invoke( "WriteBits", new Variant[] { new Variant(mvarTablasAC[Ta].mvarEHUFSI[0]), new Variant(mvarTablasAC[Ta].mvarEHUFCO[0]) } );
    }

  }

  private void WriteBitsBegin() throws Exception
  {
    mvarChrJpeg = 0;
    mvarPtrBit = 128;
  }

  private void WriteBitsEnd() throws Exception
  {
    if( mvarPtrBit != 128 )
    {
      invoke( "WriteBits", new Variant[] { new Variant(mvarPtrBit), new Variant(-1) } );
    }
  }

  private void WriteBits( int si, int code ) throws Exception
  {
    while( si > 0 )
    {
      if( (code & si) != 0 )
      {
        mvarChrJpeg = mvarChrJpeg | mvarPtrBit;
      }
      if( mvarPtrBit == 1 )
      {
        mvarDataJpeg[mvarPtrJpeg] = (byte)( mvarChrJpeg );
        if( mvarChrJpeg == 255 )
        {
          mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( 0 );
          mvarPtrJpeg = mvarPtrJpeg + 2;
        }
        else
        {
          mvarPtrJpeg = mvarPtrJpeg + 1;
        }
        mvarChrJpeg = 0;
        mvarPtrBit = 128;
      }
      else
      {
        mvarPtrBit = (int)Math.rint( Math.floor( mvarPtrBit / 2 ) );
      }
      si = (int)Math.rint( Math.floor( si / 2 ) );
    }
  }

  private void InsertSOSInterleaved( int[] CompIndex, int[] Td, int[] Ta, int FirstIndex, int SecondIndex ) throws Exception
  {
    int f = 0;
    int g = 0;
    int h = 0;
    int wvari = 0;
    int j = 0;
    int Lx = 0;
    int Ns = 0;
    int MCUx = 0;
    int MCUy = 0;
    int[][] p = null;
    int[] pLF = null;
    int[] Pred = null;
    int[] MCUr = null;
    int[] Pad64 = new int[64];





    Ns = (SecondIndex - FirstIndex) + 1;
    Lx = 6 + (2 * Ns);


    mvarDataJpeg[mvarPtrJpeg] = (byte)( 255 );
    mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( mcteSOS );
    mvarDataJpeg[mvarPtrJpeg + 2] = (byte)Math.rint( Math.floor( Lx / 256 ) );
    mvarDataJpeg[mvarPtrJpeg + 3] = (byte)( Lx & 255 );
    mvarDataJpeg[mvarPtrJpeg + 4] = (byte)( Ns );
    mvarPtrJpeg = mvarPtrJpeg + 5;
    for( wvari = FirstIndex; wvari <= SecondIndex; wvari++ )
    {
      mvarDataJpeg[mvarPtrJpeg] = (byte)( mvarBloqueActual[CompIndex[wvari]].mvarID );
      mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( (Td[wvari] * 16) | Ta[wvari] );
      mvarPtrJpeg = mvarPtrJpeg + 2;
    }
    mvarDataJpeg[mvarPtrJpeg] = (byte)( 0 );
    mvarDataJpeg[mvarPtrJpeg + 1] = (byte)( 63 );
    mvarDataJpeg[mvarPtrJpeg + 2] = (byte)( 0 );
    mvarPtrJpeg = mvarPtrJpeg + 3;

    p = new int[SecondIndex+1][mvarFilasMax - 1+1];
    Pred = new int[SecondIndex+1];
    pLF = new int[SecondIndex+1];
    MCUr = new int[SecondIndex+1];

    MCUx = (int)Math.rint( Math.floor( ((mvarNroColumnas + (8 * mvarColumnasMax)) - 1) / (8 * mvarColumnasMax) ) );
    MCUy = (int)Math.rint( Math.floor( ((mvarNroLineas + (8 * mvarFilasMax)) - 1) / (8 * mvarFilasMax) ) );

    for( f = FirstIndex; f <= SecondIndex; f++ )
    {
      h = (int)Math.rint( Math.floor( (-Math.floor( ((-mvarNroColumnas * mvarBloqueActual[CompIndex[f]].mvarSampleoHor) / mvarColumnasMax) ) + 7) / 8 ) );

      for( g = 0; g <= (mvarBloqueActual[CompIndex[f]].mvarSampleoVer - 1); g++ )
      {
        p[f][g] = 64 * h * g;
      }
      pLF[f] = 64 * h * (mvarBloqueActual[CompIndex[f]].mvarSampleoVer - 1);

      MCUr[f] = h % mvarBloqueActual[CompIndex[f]].mvarSampleoHor;
      if( MCUr[f] == 0 )
      {
        MCUr[f] = mvarBloqueActual[CompIndex[f]].mvarSampleoHor;
      }
    }

    invoke( "WriteBitsBegin", new Variant[] {} );
    for( j = 1; j <= (MCUy - 1); j++ )
    {


      for( wvari = 1; wvari <= (MCUx - 1); wvari++ )
      {
        for( f = FirstIndex; f <= SecondIndex; f++ )
        {
          for( g = 1; g <= mvarBloqueActual[CompIndex[f]].mvarSampleoVer; g++ )
          {
            for( h = 1; h <= mvarBloqueActual[CompIndex[f]].mvarSampleoHor; h++ )
            {
              invoke( "EncodeCoefficients", new Variant[] { new Variant(mvarBloqueActual[CompIndex[f]].mvarCoefDCT), new Variant(new Variant( p[f][g - 1] ))/*warning: ByRef value change will be lost.*/, new Variant(new Variant( Pred[f] ))/*warning: ByRef value change will be lost.*/, new Variant(Td[f]), new Variant(Ta[f]) } );
            }
          }
        }
      }


      for( f = FirstIndex; f <= SecondIndex; f++ )
      {
        for( g = 1; g <= mvarBloqueActual[CompIndex[f]].mvarSampleoVer; g++ )
        {
          for( h = 1; h <= mvarBloqueActual[CompIndex[f]].mvarSampleoHor; h++ )
          {
            if( h > MCUr[f] )
            {
              Pad64[0] = Pred[f];
              invoke( "EncodeCoefficients", new Variant[] { new Variant(Pad64), new Variant(new Variant( 0 ))/*warning: ByRef value change will be lost.*/, new Variant(new Variant( Pred[f] ))/*warning: ByRef value change will be lost.*/, new Variant(Td[f]), new Variant(Ta[f]) } );
            }
            else
            {
              invoke( "EncodeCoefficients", new Variant[] { new Variant(mvarBloqueActual[CompIndex[f]].mvarCoefDCT), new Variant(new Variant( p[f][g - 1] ))/*warning: ByRef value change will be lost.*/, new Variant(new Variant( Pred[f] ))/*warning: ByRef value change will be lost.*/, new Variant(Td[f]), new Variant(Ta[f]) } );
            }
          }
        }
      }


      for( f = FirstIndex; f <= SecondIndex; f++ )
      {
        for( g = 0; g <= (mvarBloqueActual[CompIndex[f]].mvarSampleoVer - 1); g++ )
        {
          p[f][g] = p[f][g] + pLF[f];
        }
      }
    }


    for( wvari = 1; wvari <= MCUx; wvari++ )
    {
      for( f = FirstIndex; f <= SecondIndex; f++ )
      {
        for( g = 1; g <= mvarBloqueActual[CompIndex[f]].mvarSampleoVer; g++ )
        {
          for( h = 1; h <= mvarBloqueActual[CompIndex[f]].mvarSampleoHor; h++ )
          {
            if( (p[f][(g - 1)] > (mvarBloqueActual[CompIndex[f]].mvarCoefDCT.length-1)) || ((wvari == MCUx) && (h > MCUr[f])) )
            {
              Pad64[0] = Pred[f];
              invoke( "EncodeCoefficients", new Variant[] { new Variant(Pad64), new Variant(new Variant( 0 ))/*warning: ByRef value change will be lost.*/, new Variant(new Variant( Pred[f] ))/*warning: ByRef value change will be lost.*/, new Variant(Td[f]), new Variant(Ta[f]) } );
            }
            else
            {
              invoke( "EncodeCoefficients", new Variant[] { new Variant(mvarBloqueActual[CompIndex[f]].mvarCoefDCT), new Variant(new Variant( p[f][g - 1] ))/*warning: ByRef value change will be lost.*/, new Variant(new Variant( Pred[f] ))/*warning: ByRef value change will be lost.*/, new Variant(Td[f]), new Variant(Ta[f]) } );
            }
          }
        }
      }
    }

    invoke( "WriteBitsEnd", new Variant[] {} );

  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
