package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_siniAppEnvio implements Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniAppEnvio";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLDetalleDenuncia = null;
    diamondedge.util.XmlDom wobjXMLDenuncias = null;
    org.w3c.dom.Node wobjXMLDenuncia = null;
    diamondedge.util.XmlDom wobjXMLDatosAIS = null;
    diamondedge.util.XmlDom wobjXMLDatosMID = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarRAMOPCOD = "";
    String wvarSINIEANNAIS = "";
    String wvarSINIENUMAIS = "";
    String wvarSINIENUMSQL = "";
    String wvarMARENVAIS = "";
    String wvarENVIADOAIS = "";
    String wvarMARENVMID = "";
    String wvarENVIADOMID = "";
    String wvarGRABSFH = "";
    String wvarXMLErrores = "";
    boolean wvarResultado = false;
    String wvarEstados = "";
    int wvarFormulario = 0;
    String wvarReportId = "";
    String wvarPDFPWD = "";
    org.w3c.dom.Element wvarElementAux = null;
    Variant wvarFechaGrabacion = new Variant();
    Variant wvarSinieAnn = new Variant();
    Variant wvarSinieNum = new Variant();
    Variant wvarMODO = new Variant();
    String wvarMetodo = "";
    String wvarXmlResponse = "";
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarXmlResponse = "";
      //
      wvarStep = 10;
      //
      //Obtener listado de SQL con registros con marcas MARENVAIS = P y MARENVMID = P
      wvarRequest = "<Request></Request>";
      wobjClass = new Variant( new com.qbe.services.lbaw_siniGetLista() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.lbaw_siniGetLista().toObject());
      wobjClass.Execute( wvarRequest, wvarResponse, "" );
      wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
      //
      wvarStep = 20;
      //
      wobjXMLDenuncias = new diamondedge.util.XmlDom();
      //unsup wobjXMLDenuncias.async = false;
      wobjXMLDenuncias.loadXML( wvarResponse );
      //
      wvarStep = 30;
      //
      //-------------------------------------------------------------------------------
      //SI Hay denuncia?
      //-------------------------------------------------------------------------------
      if( null /*unsup wobjXMLDenuncias.selectNodes( "//DENUNCIA" ) */.getLength() > 0 )
      {
        //
        wvarStep = 40;
        //
        //Para cada denuncia de la lista...
        for( int nwobjXMLDenuncia = 0; nwobjXMLDenuncia < null /*unsup wobjXMLDenuncias.selectNodes( "//DENUNCIA" ) */.getLength(); nwobjXMLDenuncia++ )
        {
          wobjXMLDenuncia = null /*unsup wobjXMLDenuncias.selectNodes( "//DENUNCIA" ) */.item( nwobjXMLDenuncia );
          //
          wvarRAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDenuncia.selectSingleNode( "RAMOPCOD" ) */ );
          wvarSINIEANNAIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDenuncia.selectSingleNode( "SINIEANNAIS" ) */ );
          wvarSINIENUMAIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDenuncia.selectSingleNode( "SINIENUMAIS" ) */ );
          wvarSINIENUMSQL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDenuncia.selectSingleNode( "SINIENUMSQL" ) */ );
          wvarMARENVAIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDenuncia.selectSingleNode( "MARENVAIS" ) */ );
          wvarENVIADOAIS = "";
          wvarMARENVMID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDenuncia.selectSingleNode( "MARENVMID" ) */ );
          wvarENVIADOMID = "";
          wvarGRABSFH = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDenuncia.selectSingleNode( "GRABSFH" ) */ );
          wvarXMLErrores = "";
          wvarFormulario = Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDenuncia.selectSingleNode( "NROFORM" ) */ ) );
          //
          wvarStep = 50;
          //
          //Recuperar denuncia de SQL
          wvarRequest = "<Request><SINIENUMSQL>" + wvarSINIENUMSQL + "</SINIENUMSQL></Request>";
          wobjClass = new Variant( new com.qbe.services.lbaw_siniGetItem() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.lbaw_siniGetItem().toObject());
          wobjClass.Execute( wvarRequest, wvarResponse, "" );
          wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
          //
          wvarStep = 60;
          //
          wobjXMLDetalleDenuncia = new diamondedge.util.XmlDom();
          //unsup wobjXMLDetalleDenuncia.async = false;
          wobjXMLDetalleDenuncia.loadXML( wvarResponse );
          //
          wvarStep = 70;
          //
          //-------------------------------------------------------------------------------
          //SI MARENVAIS = "P" envia la denuncia al AIS
          //-------------------------------------------------------------------------------
          if( wvarMARENVAIS.equals( "P" ) )
          {
            //
            //DA - 04/11/2008: controla que existan los datos a enviar al AIS y no pinche la app de env�o.
            if( ! (null /*unsup wobjXMLDetalleDenuncia.selectSingleNode( "//XMLDatosAIS/XMLIN/CAMPOS" ) */ == (org.w3c.dom.Node) null) )
            {
              //
              wobjXMLDatosAIS = new diamondedge.util.XmlDom();
              //unsup wobjXMLDatosAIS.async = false;
              //DA - 04/11/2008: antes buscaba //CAMPOS pero correponde que busque //XMLDatosAIS
              //Call .loadXML(wobjXMLDetalleDenuncia.selectSingleNode("//CAMPOS").xml)
              wobjXMLDatosAIS.loadXML( /*unsup wobjXMLDetalleDenuncia.selectSingleNode( "//XMLDatosAIS/XMLIN/CAMPOS" ) */.toString() );
              //
              wvarStep = 80;
              //
              //Inserta en el XML que va al AIS el SINIENUMSQL
              diamondedge.util.XmlDom.setText( null /*unsup wobjXMLDatosAIS.selectSingleNode( "//SINIESQL" ) */, wvarSINIENUMSQL );
              //
              wvarStep = 90;
              //
              //Si esta faltando la fecha de denuncia, la inserta en el xml desde el SQL
              if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDatosAIS.selectSingleNode( "//DENUCDIA" ) */ ) ).equals( "" ) )
              {
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLDatosAIS.selectSingleNode( "//DENUCDIA" ) */, Strings.mid( wvarGRABSFH, 9, 2 ) );
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLDatosAIS.selectSingleNode( "//DENUCMES" ) */, Strings.mid( wvarGRABSFH, 6, 2 ) );
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLDatosAIS.selectSingleNode( "//DENUCANN" ) */, Strings.mid( wvarGRABSFH, 1, 4 ) );
              }
              //
              wvarStep = 100;
              //
              //Guardo en AIS
              //     Dim wvarFileGet
              //         wvarFileGet = FreeFile()
              //     Open App.Path & "\XMLDatosAIS.xml" For Binary Access Write As wvarFileGet
              //     Put wvarFileGet, , wobjXMLDatosAIS.xml
              //     Close wvarFileGet
              wvarResultado = invoke( "fncGuardarAIS", new Variant[] { new Variant(wobjXMLDatosAIS.getDocument().getDocumentElement().toString()), new Variant(wvarFormulario), new Variant(wvarFechaGrabacion), new Variant(wvarSinieAnn), new Variant(wvarSinieNum), new Variant(wvarMODO) } );
              //
              wvarStep = 110;
              //
              //Envi� con exito? (guard�?)
              if( wvarResultado )
              {
                if( (!wvarSinieNum.toString().equals( "0" )) && (!wvarSinieNum.toString().equals( "" )) )
                {
                  //Update SQL con MARENVAIS = E
                  invoke( "ActualizarMarenvAIS", new Variant[] { new Variant(wvarSINIENUMSQL) } );
                  wvarENVIADOAIS = "S";
                  wvarMARENVAIS = "E";
                  wvarXMLErrores = wvarXMLErrores + "<ENVIOAIS>Enviado a AIS con �xito</ENVIOAIS>";
                }
                else
                {
                  wvarENVIADOAIS = "N";
                  wvarMARENVAIS = "P";
                  wvarXMLErrores = wvarXMLErrores + "<ENVIOAIS>Fall� el envio al AIS</ENVIOAIS>";
                }
              }
              else
              {
                wvarENVIADOAIS = "N";
                wvarMARENVAIS = "P";
                wvarXMLErrores = wvarXMLErrores + "<ENVIOAIS>Fall� el envio al AIS</ENVIOAIS>";
              }
              //
              wvarStep = 120;
              //
              wvarXMLErrores = wvarXMLErrores + "<SINIEANN>" + wvarSinieAnn + "</SINIEANN>";
              wvarXMLErrores = wvarXMLErrores + "<SINIENUM>" + wvarSinieNum + "</SINIENUM>";
              wvarXMLErrores = wvarXMLErrores + "<MODO>" + wvarMODO + "</MODO>";
              //
              //DA - 24/08/2007: si grab� en AIS, tiene que mandar al impreso el n�mero generado por el AIS
              wvarSINIEANNAIS = wvarSinieAnn.toString();
              wvarSINIENUMAIS = wvarSinieNum.toString();
              //
              wobjXMLDatosAIS = (diamondedge.util.XmlDom) null;
              //
            }
            else
            {
              //DOING
              //DA - 04/11/2008: no se encontr� el dato en SQL. Esto evita que pinche si no se encuentra el dato en SQL
              wvarENVIADOAIS = "N";
              wvarMARENVAIS = "X";
              wvarXMLErrores = wvarXMLErrores + "<ENVIOAIS>No se encontr� el dato en SQL</ENVIOAIS>";
              //Update SQL con MARENVAIS = X
              invoke( "ActualizarMarenvAIS_X", new Variant[] { new Variant(wvarSINIENUMSQL) } );
            }
          }
          else
          {
            //Se limpia el contenido de la variable para que la p�gina no muestre el dato
            wvarMARENVAIS = "";
          }
          //-------------------------------------------------------------------------------
          //-------------------------------------------------------------------------------
          //SI MARENVMID = "P" envia la denuncia a middleware
          //-------------------------------------------------------------------------------
          if( wvarMARENVMID.equals( "P" ) )
          {
            //
            //DA - 04/11/2008: controla que existan los datos a enviar a MDW y no pinche la app de env�o.
            if( ! (null /*unsup wobjXMLDetalleDenuncia.selectSingleNode( "//XMLDatosMID/XMLIN/CAMPOS" ) */ == (org.w3c.dom.Node) null) )
            {
              //
              wobjXMLDatosMID = new diamondedge.util.XmlDom();
              //unsup wobjXMLDatosMID.async = false;
              wobjXMLDatosMID.loadXML( /*unsup wobjXMLDetalleDenuncia.selectSingleNode( "//XMLDatosMID" ) */.toString() );
              //
              wvarStep = 125;
              //
              //MHC:
              //Quitar estas lineas cuando se normalice la situaci�n de las denuncias pendientes
              //en UAT
              if( null /*unsup wobjXMLDatosMID.selectSingleNode( "//NUMEROSINIESTROCOMPLETO" ) */ == (org.w3c.dom.Node) null )
              {
                wvarElementAux = wobjXMLDatosMID.getDocument().createElement( "NUMEROSINIESTROCOMPLETO" );
                /*unsup wobjXMLDatosMID.selectSingleNode( "//CAMPOS" ) */.appendChild( wvarElementAux );
              }
              //
              wvarStep = 130;
              //
              //Inserta en el XML que va al MID el SINIENUMSQL
              if( !wvarSINIENUMAIS.equals( "0" ) )
              {
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLDatosMID.selectSingleNode( "//SINIEANN" ) */, wvarSINIEANNAIS );
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLDatosMID.selectSingleNode( "//SINIENUM" ) */, wvarSINIENUMAIS );
                //
                //DA - 24/08/2007: buscar el RAMOPCOD en el XMLDatosMID recuperado del SQL
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLDatosMID.selectSingleNode( "//NUMEROSINIESTROCOMPLETO" ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDatosMID.selectSingleNode( "//RAMOPCOD" ) */ ) + "-" + wvarSINIEANNAIS + "-" + wvarSINIENUMAIS );
              }
              else
              {
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLDatosMID.selectSingleNode( "//SINIESQL" ) */, wvarSINIENUMSQL );
                //DA - 24/08/2007
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLDatosMID.selectSingleNode( "//NUMEROSINIESTROCOMPLETO" ) */, wvarSINIENUMSQL );
              }
              //
              wvarStep = 140;
              //
              //Si esta faltando la fecha de denuncia, la inserta en el xml desde el SQL
              if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDatosMID.selectSingleNode( "//DENUCDIA" ) */ ) ).equals( "" ) )
              {
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLDatosMID.selectSingleNode( "//DENUCDIA" ) */, Strings.mid( wvarGRABSFH, 9, 2 ) );
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLDatosMID.selectSingleNode( "//DENUCMES" ) */, Strings.mid( wvarGRABSFH, 6, 2 ) );
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLDatosMID.selectSingleNode( "//DENUCANN" ) */, Strings.mid( wvarGRABSFH, 1, 4 ) );
              }
              //
              wvarStep = 150;
              //
              //Envio a MID
              if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDatosMID.selectSingleNode( "//DENUNMAIL" ) */ ).equals( "" ) )
              {
                wvarMetodo = "ismGenerateAndPublishPdfReport.xml";
              }
              else
              {
                wvarMetodo = "ismGeneratePublishAndSendPdfReport.xml";
              }
              //
              wvarStep = 160;
              //
              //Seleccion de ReportId
              
              if( wvarFormulario == 1 )
              {
                wvarReportId = "DenunciaAseguradoAutosF1_1.0";
              }
              else if( wvarFormulario == 2 )
              {
                wvarReportId = "DenunciaAseguradoAutosF2_1.0";
              }
              else if( wvarFormulario == 3 )
              {
                wvarReportId = "DenunciaAseguradoAutosF3_1.0";
              }
              else if( wvarFormulario == 4 )
              {
                wvarReportId = "DenunciaDeTerceros_1.0";
              }
              else if( wvarFormulario == 5 )
              {
                wvarReportId = "RiesgosVarios_1.0";
              }
              //
              wvarStep = 170;
              //
              if( ! (null /*unsup wobjXMLDatosMID.selectSingleNode( "//PDFPWD" ) */ == (org.w3c.dom.Node) null) )
              {
                wvarPDFPWD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDatosMID.selectSingleNode( "//PDFPWD" ) */ );
              }
              else
              {
                wvarPDFPWD = "";
              }
              //
              wvarEstados = invoke( "fncEnviaMiddleware", new Variant[] { new Variant(null /*unsup wobjXMLDatosMID.selectSingleNode( new Variant("//XMLIN") ) */.toString()), new Variant(wvarMetodo), new Variant(wvarReportId), new Variant(wvarPDFPWD) } );
              //
              wvarStep = 180;
              //
              //Si "P"DF est� ok y "E"mail est� ok y "C"oldview est� ok...
              if( (Strings.find( 1, wvarEstados, "P" ) == 0) && (Strings.find( 1, wvarEstados, "C" ) == 0) && (Strings.find( 1, wvarEstados, "E" ) == 0) )
              {
                //Actualiza el SQL con el estado "E"nviado
                wvarMARENVMID = "E";
                wvarENVIADOMID = "S";
                wvarXMLErrores = wvarXMLErrores + "<ENVIOMID>Enviado a Middleware con �xito</ENVIOMID>";
              }
              else
              {
                //Actualiza el SQL con el estado "P"endiente, y actualiza el wvarEstados
                wvarMARENVMID = "P";
                wvarENVIADOMID = "N";
                wvarXMLErrores = wvarXMLErrores + "<ENVIOMID>Hubo un error en Middleware (" + wvarEstados + ")</ENVIOMID>";
              }
              //
              //DA - 04/11/2008: se quita de ac� la invocaci�n al UPD de la MARENVMID. Se pone en step 190 (ver mas abajo)
              wobjXMLDatosMID = (diamondedge.util.XmlDom) null;
            }
            else
            {
              wvarStep = 185;
              //DA - 04/11/2008: no se encontr� el dato en SQL. Esto evita que pinche si no se encuentra el dato en SQL
              wvarMARENVMID = "X";
              wvarENVIADOMID = "N";
              wvarXMLErrores = wvarXMLErrores + "<ENVIOMID>No se encontr� el dato en SQL</ENVIOMID>";
            }
            //
            //DA - 04/11/2008: se pone ac� la invocaci�n al UPD de la MARENVMID.
            wvarStep = 190;
            wvarRequest = "<Request>";
            wvarRequest = wvarRequest + "<SINIENUMSQL>" + wvarSINIENUMSQL + "</SINIENUMSQL>";
            wvarRequest = wvarRequest + "<MARENVMID>" + wvarMARENVMID + "</MARENVMID>";
            wvarRequest = wvarRequest + "<ESTADOMID>" + wvarEstados + "</ESTADOMID>";
            wvarRequest = wvarRequest + "</Request>";
            //
            wobjClass = new Variant( new com.qbe.services.lbaw_siniUpdMenvMID() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.lbaw_siniUpdMenvMID().toObject());
            wobjClass.Execute( wvarRequest, wvarResponse, "" );
            wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
            //
          }
          else
          {
            //Se limpia el contenido de la variable para que la p�gina no muestre el dato
            wvarMARENVMID = "";
          }
          //-------------------------------------------------------------------------------
          //
          wvarStep = 200;
          //
          //-------------------------------------------------------------------------------
          //INSERT log de Envio
          //-------------------------------------------------------------------------------
          wvarRequest = "<Request>";
          wvarRequest = wvarRequest + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>";
          wvarRequest = wvarRequest + "<SINIEANNAIS>" + wvarSINIEANNAIS + "</SINIEANNAIS>";
          wvarRequest = wvarRequest + "<SINIENUMAIS>" + wvarSINIENUMAIS + "</SINIENUMAIS>";
          wvarRequest = wvarRequest + "<SINIENUMSQL>" + wvarSINIENUMSQL + "</SINIENUMSQL>";
          wvarRequest = wvarRequest + "<ENVIADOAIS>" + wvarENVIADOAIS + "</ENVIADOAIS>";
          wvarRequest = wvarRequest + "<ENVIADOMID>" + wvarENVIADOMID + "</ENVIADOMID>";
          wvarRequest = wvarRequest + "<ESTADOS>" + wvarXMLErrores + "</ESTADOS>";
          wvarRequest = wvarRequest + "</Request>";
          //
          wobjClass = new Variant( new com.qbe.services.lbaw_siniInsLogEnvio() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.lbaw_siniInsLogEnvio().toObject());
          wobjClass.Execute( wvarRequest, wvarResponse, "" );
          wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
          //
          wvarStep = 210;
          //
          wobjXMLDetalleDenuncia = (diamondedge.util.XmlDom) null;
          //
          //-------------------------------------------------------------------------------
          //Inserta en Response
          //-------------------------------------------------------------------------------
          wvarXmlResponse = wvarXmlResponse + "<DENUNCIA>" + "<NROFORM>" + String.valueOf( wvarFormulario ) + "</NROFORM><RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD><SINIEANNAIS>" + wvarSINIEANNAIS + "</SINIEANNAIS><SINIENUMAIS>" + wvarSINIENUMAIS + "</SINIENUMAIS><SINIENUMSQL>" + wvarSINIENUMSQL + "</SINIENUMSQL><MARENVAIS>" + wvarMARENVAIS + "</MARENVAIS><MARENVMID>" + wvarMARENVMID + "</MARENVMID><ESTADOS>" + wvarXMLErrores + "</ESTADOS>" + "</DENUNCIA>";
          //
          wvarStep = 220;
          //
          wobjXMLDetalleDenuncia = (diamondedge.util.XmlDom) null;
          //
        }
        //
        wobjXMLDenuncias = (diamondedge.util.XmlDom) null;
        //
      }
      //-------------------------------------------------------------------------------
      wvarStep = 300;
      pvarResponse.set( "<Response><DENUNCIAS>" + wvarXmlResponse + "</DENUNCIAS></Response>" );
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGuardarAIS( String pobjXMLDatosAIS, int pFormulario, Variant pFechaGrabacion, Variant pSinieAnn, Variant pSinieNum, Variant pModo ) throws Exception
  {
    boolean fncGuardarAIS = false;
    String wvarRequest = "";
    String wvarResponse = "";
    diamondedge.util.XmlDom wobjResponse = null;
    String wvarXML_Definicion_Insert_AIS = "";
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    //En esta funcion se envia la denuncia al AIS
    //El retorno de la misma debe ser TRUE o FALSE dependiendo del exito del envio de la
    //denuncia.
    //
    //Determino cu�l es el XML de definici�n para grabar en AIS.
    
    if( pFormulario == 1 )
    {
      wvarXML_Definicion_Insert_AIS = "siniestroDenuncia_putForm1AIS.xml";
    }
    else if( pFormulario == 2 )
    {
      wvarXML_Definicion_Insert_AIS = "siniestroDenuncia_putForm2AIS.xml";
    }
    else if( pFormulario == 3 )
    {
      wvarXML_Definicion_Insert_AIS = "siniestroDenuncia_putForm3AIS.xml";
    }
    else if( pFormulario == 5 )
    {
      wvarXML_Definicion_Insert_AIS = "siniestroDenuncia_putForm5AIS.xml";
    }

    wvarRequest = "<Request>" + pobjXMLDatosAIS + "<DEFINICION>" + wvarXML_Definicion_Insert_AIS + "</DEFINICION></Request>";
    //Llamo al COM+ que guarda en AIS
    wobjClass = new Variant( new com.qbe.services.lbaw_siniInsertAIS() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.lbaw_siniInsertAIS().toObject());
    wobjClass.Execute( wvarRequest, wvarResponse, "" );
    wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;

    //
    wobjResponse = new diamondedge.util.XmlDom();
    //unsup wobjResponse.async = false;
    wobjResponse.loadXML( wvarResponse );
    //
    if( ! (null /*unsup wobjResponse.selectSingleNode( "//MODO" ) */ == (org.w3c.dom.Node) null) )
    {
      pFechaGrabacion.set( diamondedge.util.XmlDom.getText( null /*unsup wobjResponse.selectSingleNode( "//FECHA_GRABACION" ) */ ) );
      pSinieAnn.set( diamondedge.util.XmlDom.getText( null /*unsup wobjResponse.selectSingleNode( "//SINIEANNAIS" ) */ ) );
      pSinieNum.set( diamondedge.util.XmlDom.getText( null /*unsup wobjResponse.selectSingleNode( "//SINIENUMAIS" ) */ ) );
      pModo.set( diamondedge.util.XmlDom.getText( null /*unsup wobjResponse.selectSingleNode( "//MODO" ) */ ) );
      fncGuardarAIS = true;
    }
    else
    {
      //Fall� el COM+. Asumimos estado NONSys detectado en la p�gina.
      pFechaGrabacion.set( "" );
      pSinieAnn.set( "0" );
      pSinieNum.set( "0" );
      pModo.set( "NG" );
      fncGuardarAIS = false;
    }
    //
    wobjResponse = (diamondedge.util.XmlDom) null;

    return fncGuardarAIS;
  }

  private String fncEnviaMiddleware( String pXMLDatosMID, String pMetodo, String pReportId, String pPDFPWD ) throws Exception
  {
    String fncEnviaMiddleware = "";
    String wvarDENUNMAIL = "";
    String wvarDENUNMAILCC = "";
    diamondedge.util.XmlDom wobjXMLMID = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarColdviewMetadata = "";
    String wvarNroSiniestroDef = "";
    String wvarNroSiniestroProv = "";
    String wvarFecha = "";
    String wvarDocumento = "";
    String wvarPoliza = "";
    String wvarRECARGA = "";
    String wvarEstadoDatos = "";
    String wvarCanal = "";
    String wvarTipo = "";
    String wvarAutor = "";
    //Funcion que va a enviar la denuncia a middleware
    //DA - 24/10/2008
    wobjXMLMID = new diamondedge.util.XmlDom();
    //unsup wobjXMLMID.async = false;
    wobjXMLMID.loadXML( pXMLDatosMID );

    //Si tiene SINIENUM pasa a MID el numero de siniestro del AIS, sino pasa el de SQL
    //SOLO UNO DE LOS DOS TIENE QUE PASAR A MIDDLEWARE
    if( !diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//SINIENUM" ) */ ).equals( "" ) )
    {
      wvarNroSiniestroDef = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//RAMOPCOD" ) */ ) + "-" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//SINIEANN" ) */ ) + "-" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//SINIENUM" ) */ );
      wvarNroSiniestroProv = "";
    }
    else
    {
      wvarNroSiniestroDef = "";
      wvarNroSiniestroProv = Strings.right( "0000000000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//SINIESQL" ) */ ), 10 );
    }
    wvarFecha = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//DENUCANN" ) */ ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//DENUCMES" ) */ ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//DENUCDIA" ) */ );
    wvarDocumento = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//DOCUMDAT" ) */ );
    wvarPoliza = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//RAMOPCOD" ) */ ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//POLIZANN" ) */ ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//POLIZSEC" ) */ ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//CERTIPOL" ) */ ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//CERTIANN" ) */ ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//CERTISEC" ) */ );
    wvarRECARGA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//RECARGA" ) */ );
    wvarEstadoDatos = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//ESTADODAT" ) */ );
    wvarCanal = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//GRABSVIA" ) */ );
    wvarAutor = "LBA_VO_FrontEnd";
    wvarTipo = "LBASiniestros";
    wvarDENUNMAIL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//DENUNMAIL" ) */ );

    //DA - 24/10/2008: se agrega a pedido de CS
    if( ! (null /*unsup wobjXMLMID.selectSingleNode( "//DENUNMAILCC" ) */ == (org.w3c.dom.Node) null) )
    {
      wvarDENUNMAILCC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLMID.selectSingleNode( "//DENUNMAILCC" ) */ );
    }
    else
    {
      wvarDENUNMAILCC = "";
    }


    wvarColdviewMetadata = "NroSiniestroDef=" + wvarNroSiniestroDef + ";NroSiniestroProv=" + wvarNroSiniestroProv + ";Fecha=" + wvarFecha + ";documento=" + wvarDocumento + ";poliza=" + wvarPoliza + ";recarga=" + wvarRECARGA + ";estadodatos=" + wvarEstadoDatos + ";canal=" + wvarCanal + ";tipo=" + wvarTipo + ";autor=" + wvarAutor + ";Category=Ordinario";

    wvarRequest = "<Request>";
    wvarRequest = wvarRequest + "<xmlDataSource>" + pXMLDatosMID + "</xmlDataSource>";
    wvarRequest = wvarRequest + "<Metodo>" + pMetodo + "</Metodo>";
    wvarRequest = wvarRequest + "<ColdviewMetadata>" + wvarColdviewMetadata + "</ColdviewMetadata>";
    wvarRequest = wvarRequest + "<EmailTo>" + wvarDENUNMAIL + "</EmailTo>";
    wvarRequest = wvarRequest + "<recipientsCC>" + wvarDENUNMAILCC + "</recipientsCC>";
    wvarRequest = wvarRequest + "<ReportId>" + pReportId + "</ReportId>";
    wvarRequest = wvarRequest + "<DevuelvePDF>N</DevuelvePDF>";
    wvarRequest = wvarRequest + "<attachPassword>" + pPDFPWD + "</attachPassword>";
    wvarRequest = wvarRequest + "</Request>";

    wobjClass = new Variant( new com.qbe.services.lbaw_siniInsertMID() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.lbaw_siniInsertMID().toObject());
    wobjClass.Execute( wvarRequest, wvarResponse, "" );
    wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;

    wobjXMLResponse = new diamondedge.util.XmlDom();
    //unsup wobjXMLResponse.async = false;
    wobjXMLResponse.loadXML( wvarResponse );

    if( ! (null /*unsup wobjXMLResponse.selectSingleNode( "//ESTADOMID" ) */ == (org.w3c.dom.Node) null) )
    {
      if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//ESTADOMID" ) */ ) ).equals( "E" ) )
      {
        fncEnviaMiddleware = "";
      }
      else
      {
        fncEnviaMiddleware = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//ESTADOMID" ) */ );
      }
    }
    else
    {
      //Si fallo el envio a MID, devuelve condici�n de error
      fncEnviaMiddleware = "PCE";
    }

    return fncEnviaMiddleware;
  }

  private void ActualizarMarenvAIS( String pvarSINIENUMSQL ) throws Exception
  {
    String wvarRequest = "";
    String wvarResponse = "";
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;

    //
    wvarRequest = "<Request>";
    wvarRequest = wvarRequest + "<SINIENUMSQL>" + pvarSINIENUMSQL + "</SINIENUMSQL>";
    wvarRequest = wvarRequest + "</Request>";
    //
    wobjClass = new Variant( new com.qbe.services.lbaw_siniUpdMenvAIS() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.lbaw_siniUpdMenvAIS().toObject());
    wobjClass.Execute( wvarRequest, wvarResponse, "" );
    wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;

  }

  /**
   * DA - 04/11/2008: nueva funci�n
   */
  private void ActualizarMarenvAIS_X( String pvarSINIENUMSQL ) throws Exception
  {
    String wvarRequest = "";
    String wvarResponse = "";
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;

    //
    wvarRequest = "<Request>";
    wvarRequest = wvarRequest + "<SINIENUMSQL>" + pvarSINIENUMSQL + "</SINIENUMSQL>";
    wvarRequest = wvarRequest + "</Request>";
    //
    wobjClass = new Variant( new com.qbe.services.lbaw_siniUpdMenvAISX() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.lbaw_siniUpdMenvAISX().toObject());
    wobjClass.Execute( wvarRequest, wvarResponse, "" );
    wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;

  }

  private void ObjectControl_Activate() throws Exception
  {
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;


    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
