package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Tipo de archivo siempre 4D42h o "BM"
 */

public class WINBMPFILEHEADER implements com.qbe.services.Cloneable
{
  /**
   * Encabezado de archivo .BMP para Sistema Operativo Windows, total 14 bytes
   */
  public int strFileType = 0;
  /**
   * Tama�o de archivo en bytes
   */
  public int lngFileSize = 0;
  /**
   * Siempre 0
   */
  public int bytReserved1 = 0;
  /**
   * Siempre 0
   */
  public int bytReserved2 = 0;
  /**
   * Posicion de comienzo datos de imagen en bytes
   */
  public int lngBitmapOffset = 0;

  public Object clone()
  {
    try 
    {
      WINBMPFILEHEADER clone = (WINBMPFILEHEADER) super.clone();
      //todo: make clones of any members that are objects ie. clone.obj = obj.clone();
      return clone;
    } 
    catch( CloneNotSupportedException e ) {}
    return null;
  }
}
