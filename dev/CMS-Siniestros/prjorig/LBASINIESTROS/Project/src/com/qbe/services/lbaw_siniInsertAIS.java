package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_siniInsertAIS implements Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniInsertAIS";
  /**
   * 
   * Constantes de p�rametros de Entrada
   */
  static final String mcteParam_SINIENUMSQL = "//SINIENUMSQL";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    String wvarRequest = "";
    String wvarResponse = "";
    diamondedge.util.XmlDom wobjXMLResponseAIS = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String mvarXMLDatosAIS = "";
    String wvarDEFINICION = "";
    String wvarSINIEANNAIS = "";
    String wvarSINIENUMAIS = "";
    String wvarFECHA_GRABACION = "";
    String wvarMODO = "";
    //
    //
    //Par�metros de entrada
    //Par�metros de salida
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wvarRequest = Strings.replace( pvarRequest, "<CAMPOS>", "" );
      wvarRequest = Strings.replace( wvarRequest, "</CAMPOS>", "" );

      wvarStep = 20;
      //Llamo al MQ Gen�rico para guardar en AIS
      wobjClass = new Variant( new com.qbe.services.lbaw_siniMQGenerico() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.lbaw_siniMQGenerico().toObject());
      wobjClass.Execute( wvarRequest, wvarResponse, "" );
      wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;

      wvarStep = 30;
      wobjXMLResponseAIS = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponseAIS.async = false;
      wobjXMLResponseAIS.loadXML( wvarResponse );

      wvarStep = 40;
      if( ! (null /*unsup wobjXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {
          wvarStep = 50;
          //Grab�
          wvarFECHA_GRABACION = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponseAIS.selectSingleNode( "//GRABAANN" ) */ ) + Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponseAIS.selectSingleNode( "//GRABAMES" ) */ ), 2 ) + Strings.right( ("00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponseAIS.selectSingleNode( "//GRABADIA" ) */ )), 2 ) + " " + Strings.right( ("00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponseAIS.selectSingleNode( "//GRABAHOR" ) */ )), 2 ) + ":" + Strings.right( ("00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponseAIS.selectSingleNode( "//GRABAMIN" ) */ )), 2 );
          wvarSINIEANNAIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponseAIS.selectSingleNode( "//SINIEANN" ) */ );
          wvarSINIENUMAIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponseAIS.selectSingleNode( "//SINIENUM" ) */ );
          wvarMODO = "O";
        }
        else
        {
          wvarStep = 60;
          //No grab�
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponseAIS.selectSingleNode( "//Response/Estado/@mensaje" ) */ ).equals( "CO" ) )
          {
            wvarMODO = "C";
          }
          else
          {
            wvarMODO = "NG";
          }
          wvarFECHA_GRABACION = "";
          wvarSINIEANNAIS = "";
          wvarSINIENUMAIS = "";
        }
      }
      else
      {
        wvarStep = 70;
        //Problemas al ejecutar el COM+ de grabaci�n en AIS. Asumimos un NONSys detectado al guardar.
        wvarFECHA_GRABACION = "";
        wvarSINIEANNAIS = "";
        wvarSINIENUMAIS = "";
        wvarMODO = "NG";
      }

      wvarStep = 80;
      pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><SINIEANNAIS>" + wvarSINIEANNAIS + "</SINIEANNAIS><SINIENUMAIS>" + wvarSINIENUMAIS + "</SINIENUMAIS><FECHA_GRABACION>" + wvarFECHA_GRABACION + "</FECHA_GRABACION><MODO>" + wvarMODO + "</MODO></Response>" );

      wvarStep = 90;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      wvarStep = 100;
      wobjXMLResponseAIS = (diamondedge.util.XmlDom) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjXMLResponseAIS = (diamondedge.util.XmlDom) null;
        //
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
