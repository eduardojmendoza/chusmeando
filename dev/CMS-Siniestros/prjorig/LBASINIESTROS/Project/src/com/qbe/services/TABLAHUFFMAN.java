package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class TABLAHUFFMAN implements com.qbe.services.Cloneable
{
  /**
   * IScale(64)                         As Single
   */
  public byte[] mvarBITS = new byte[16];
  public byte[] mvarHUFFVAL = new byte[256];
  public int[] mvarEHUFSI = new int[256];
  public int[] mvarEHUFCO = new int[256];
  public int[] mvarMINCODE = new int[16];
  public int[] mvarMAXCODE = new int[16];

  public Object clone()
  {
    try 
    {
      TABLAHUFFMAN clone = (TABLAHUFFMAN) super.clone();
      //todo: make clones of any members that are objects ie. clone.obj = obj.clone();
      clone.mvarBITS = mvarBITS == null ? null : (byte[]) mvarBITS.clone();
      clone.mvarHUFFVAL = mvarHUFFVAL == null ? null : (byte[]) mvarHUFFVAL.clone();
      clone.mvarEHUFSI = mvarEHUFSI == null ? null : (int[]) mvarEHUFSI.clone();
      clone.mvarEHUFCO = mvarEHUFCO == null ? null : (int[]) mvarEHUFCO.clone();
      clone.mvarMINCODE = mvarMINCODE == null ? null : (int[]) mvarMINCODE.clone();
      clone.mvarMAXCODE = mvarMAXCODE == null ? null : (int[]) mvarMAXCODE.clone();
      return clone;
    } 
    catch( CloneNotSupportedException e ) {}
    return null;
  }
}
