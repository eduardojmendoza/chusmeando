package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class BITMAPINFO implements com.qbe.services.Cloneable
{
  public com.qbe.services.BITMAPINFOHEADER bmiHeader = new com.qbe.services.BITMAPINFOHEADER();
  public com.qbe.services.RGBQUAD bmiColors = new com.qbe.services.RGBQUAD();

  public Object clone()
  {
    try 
    {
      BITMAPINFO clone = (BITMAPINFO) super.clone();
      //todo: make clones of any members that are objects ie. clone.obj = obj.clone();
      clone.bmiHeader = bmiHeader == null ? null : (com.qbe.services.BITMAPINFOHEADER) bmiHeader.clone();
      clone.bmiColors = bmiColors == null ? null : (com.qbe.services.RGBQUAD) bmiColors.clone();
      return clone;
    } 
    catch( CloneNotSupportedException e ) {}
    return null;
  }
}
