package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_siniInsertMID implements Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniInsertMID";
  /**
   * 
   * Constantes de párametros de Entrada
   */
  static final String mcteParam_XMLDatosMID = "//xmlDataSource";
  static final String mcteParam_Metodo = "//Metodo";
  static final String mcteParam_ColdviewMetadata = "//ColdviewMetadata";
  static final String mcteParam_EmailTo = "//EmailTo";
  /**
   * DA - 27/10/2008
   */
  static final String mcteParam_recipientsCC = "//recipientsCC";
  static final String mcteParam_PDFPWD = "//PDFPWD";
  static final String mcteParam_DevuelvePDF = "//DevuelvePDF";
  static final String mcteParam_ReportId = "//ReportId";
  /**
   * 
   * 26/05/2008 - DA
   */
  static final String mcteTempFilesServer = "LBAVirtual_TempFilesServer.xml";
  /**
   * Carpeta donde se encuentran los Templates de XML
   */
  static final String mcteSubDirName = "";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";
  /**
   * static variable for method: SaveBinaryData
   */
  private final int adTypeBinary = 1;
  /**
   * static variable for method: SaveBinaryData
   */
  private final int adSaveCreateOverWrite = 2;

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    String wvarRequest = "";
    String wvarResponse = "";
    diamondedge.util.XmlDom wobjXMLResponseMID = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    int wvarStep = 0;
    String wvarEstadoMID = "";
    String wvarDefinirDefaultONo = "";
    diamondedge.util.XmlDom wvarXMLDatosMID = null;
    String wvarMetodo = "";
    String wvarColdviewMetadata = "";
    String wvarEmailTo = "";
    String wvarRecipientsCC = "";
    String wvarPDFPWD = "";
    String wvarDevuelvePDF = "";
    String wvarMIDFaultCode = "";
    String wvarReportId = "";
    String mvarRutaDestino = "";
    diamondedge.util.XmlDom wobjXMLTempFilesServer = null;
    String wvarRutaPDF = "";
    //
    //
    //Parámetros de entrada
    //DA - 24/10/2008
    //26/05/2008 - DA: se toma desde el archivo LBAVirtual_TempFilesServer.xml
    //Parámetros de salida
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );

      //Obtiene los valores de los parámetros
      wvarStep = 20;
      wvarStep = 21;
      wvarXMLDatosMID = new diamondedge.util.XmlDom();
      //unsup wvarXMLDatosMID.async = false;
      wvarXMLDatosMID.loadXML( /*unsup wobjXMLRequest.selectSingleNode( mcteParam_XMLDatosMID ) */.toString() );
      wvarStep = 22;
      wvarMetodo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Metodo ) */ );
      wvarStep = 23;
      wvarColdviewMetadata = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ColdviewMetadata ) */ );
      wvarStep = 24;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EmailTo ) */ == (org.w3c.dom.Node) null) )
      {
        wvarEmailTo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EmailTo ) */ );
      }
      else
      {
        wvarEmailTo = "";
      }
      //DA - 27/10/2008
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_recipientsCC ) */ == (org.w3c.dom.Node) null) )
      {
        wvarRecipientsCC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_recipientsCC ) */ );
      }
      else
      {
        wvarRecipientsCC = "";
      }


      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PDFPWD ) */ == (org.w3c.dom.Node) null) )
      {
        wvarPDFPWD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PDFPWD ) */ );
      }
      else
      {
        wvarPDFPWD = "";
      }

      wvarStep = 23;
      wvarDevuelvePDF = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DevuelvePDF ) */ );
      wvarReportId = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ReportId ) */ );

      //Si XMLDatosMID tiene CROQUIS convertirlos a BASE64 e insertarlos en el XMLIN
      wvarStep = 70;
      invoke( "ProcesarCroquis", new Variant[] { new Variant(wvarXMLDatosMID) } );

      //Enviar el xml generado a MIDDLEWARE
      wvarStep = 110;
      wvarRequest = "<Request>";
      wvarRequest = wvarRequest + "<DEFINICION>" + wvarMetodo + "</DEFINICION>";
      //XmlDataSource
      wvarRequest = wvarRequest + wvarXMLDatosMID.getDocument().getDocumentElement().toString();
      wvarRequest = wvarRequest + "<coldViewMetadata>" + wvarColdviewMetadata + "</coldViewMetadata>";
      wvarRequest = wvarRequest + "<reportId>" + wvarReportId + "</reportId>";
      wvarRequest = wvarRequest + "<recipientTO>" + wvarEmailTo + "</recipientTO>";
      //DA - 27/10/2008
      wvarRequest = wvarRequest + "<recipientsCC>" + wvarRecipientsCC + "</recipientsCC>";
      wvarRequest = wvarRequest + "<attachPassword>" + wvarPDFPWD + "</attachPassword>";
      wvarRequest = wvarRequest + "</Request>";

      wobjClass = new Variant( new com.qbe.services.lbaw_siniMQMiddle() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.lbaw_siniMQMiddle().toObject());
      wobjClass.Execute( wvarRequest, wvarResponse, "" );
      wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;

      //Analizar devolucion de MIDDLEWARE
      wvarStep = 120;
      wobjXMLResponseMID = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponseMID.async = false;
      wobjXMLResponseMID.loadXML( wvarResponse );

      if( ! (null /*unsup wobjXMLResponseMID.selectSingleNode( "//Response" ) */ == (org.w3c.dom.Node) null) )
      {
        if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponseMID.selectSingleNode( "//Response/Estado/@resultado" ) */ ) ).equals( "TRUE" ) )
        {
          //Analisis de posibles errores de MIDDLEWARE
          if( ! (null /*unsup wobjXMLResponseMID.selectSingleNode( "//faultstring" ) */ == (org.w3c.dom.Node) null) )
          {
            wvarMIDFaultCode = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponseMID.selectSingleNode( "//faultstring" ) */ );
            if( Strings.find( 1, wvarMIDFaultCode, "E1", true ) > 0 )
            {
              //Fallo EMAIL
              wvarEstadoMID = "E";
            }
            else if( Strings.find( 1, wvarMIDFaultCode, "E2", true ) > 0 )
            {
              //Fallo COLDVIEW
              wvarEstadoMID = "C";
            }
            else if( Strings.find( 1, wvarMIDFaultCode, "E3", true ) > 0 )
            {
              //Fallo COLDVIEW y EMAIL
              wvarEstadoMID = "CE";
            }
            else if( Strings.find( 1, wvarMIDFaultCode, "E4", true ) > 0 )
            {
              //Fallo PDF
              wvarEstadoMID = "P";
            }
            else if( Strings.find( 1, wvarMIDFaultCode, "E5", true ) > 0 )
            {
              //Fallo PDF y EMAIL
              wvarEstadoMID = "PE";
            }
            else if( Strings.find( 1, wvarMIDFaultCode, "E6", true ) > 0 )
            {
              //Fallo PDF y COLDVIEW
              wvarEstadoMID = "PC";
            }
            else if( Strings.find( 1, wvarMIDFaultCode, "E7", true ) > 0 )
            {
              //Fallo PDF y COLDVIEW y EMAIL
              wvarEstadoMID = "PCE";
            }
            else
            {
              //Fallo no registrado, se asume falló de middleware
              wvarEstadoMID = "PCE";
            }
          }
          else
          {
            //TODO GENERADO OK, no se retorna valor
            wvarEstadoMID = "";
          }
        }
        else
        {
          //Errores en la ejecucion del componente de middleware, se asume falla de middleware
          wvarEstadoMID = "PCE";
        }
      }
      else
      {
        //Errores en la ejecucion del componente de middleware, se asume falla de middleware
        wvarEstadoMID = "PCE";
      }
      //
      //26/05/2008 - DA: busco la ruta de destino del PDF (para grabación temporal)
      wvarStep = 130;
      wobjXMLTempFilesServer = new diamondedge.util.XmlDom();
      //unsup wobjXMLTempFilesServer.async = false;
      wobjXMLTempFilesServer.load( System.getProperty("user.dir") + "\\" + mcteTempFilesServer );

      mvarRutaDestino = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLTempFilesServer.selectSingleNode( "//PATH" ) */ );

      //Si se pidió la devolucion del PDF generado, persistir el PDF en un archivo
      wvarStep = 140;
      if( (wvarDevuelvePDF.equals( "S" )) && (wvarEstadoMID.equals( "" )) )
      {
        if( ! (null /*unsup wobjXMLResponseMID.selectSingleNode( "//PDF" ) */ == (org.w3c.dom.Node) null) )
        {
          //26/05/2008 - DA: se agrega el grabado en un fileserver
          //wvarRutaPDF = obtenerNombreRandom(App.Path & "\PDF\", "PDF")
          wvarRutaPDF = invoke( "obtenerNombreRandom", new Variant[] { new Variant(mvarRutaDestino + "\\PDF\\"), new Variant("PDF") } );
          invoke( "SaveBinaryData", new Variant[] { new Variant(wvarRutaPDF), new Variant(diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponseMID.selectSingleNode( new Variant("//PDF") ) */ )) } );
        }
        else
        {
          wvarRutaPDF = "";
        }
      }
      else
      {
        wvarRutaPDF = "";
      }

      //Setear el valor de los parametros a devolver (ruta del PDF)
      wvarStep = 150;
      pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><RUTAPDF>" + wvarRutaPDF + "</RUTAPDF><ESTADOMID>" + wvarEstadoMID + "</ESTADOMID></Response>" );

      wvarStep = 170;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLResponseMID = (diamondedge.util.XmlDom) null;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;

        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ProcesarCroquis( diamondedge.util.XmlDom pvarXMLDatosMID ) throws Exception
  {
    org.w3c.dom.Element wobjNodo64 = null;
    org.w3c.dom.Node wobjNodoCroquis = null;
    org.w3c.dom.Element wobjAUX = null;
    //
    wobjAUX = pvarXMLDatosMID.getDocument().createElement( "IMAGES" );
    /*unsup pvarXMLDatosMID.selectSingleNode( "//XMLIN" ) */.appendChild( wobjAUX );
    //
    //Croquis y bollos del vehiculo asegurado
    for( int nwobjNodoCroquis = 0; nwobjNodoCroquis < null /*unsup pvarXMLDatosMID.selectNodes( "//XMLIN/croquis" ) */.getLength(); nwobjNodoCroquis++ )
    {
      wobjNodoCroquis = null /*unsup pvarXMLDatosMID.selectNodes( "//XMLIN/croquis" ) */.item( nwobjNodoCroquis );
      //
      //Armo el Base 64 de cada croquis
      wobjNodo64 = invoke( "fncXMLtoJPG", new Variant[] { new Variant(wobjNodoCroquis.toString()), new Variant(diamondedge.util.XmlDom.getText( wobjNodoCroquis.getAttributes().getNamedItem( new Variant("tipo") ) )) } );
      //
      if( ! (wobjNodo64 == (org.w3c.dom.Element) null) )
      {
        //Lo apendeo al XML que viajará al Middleware
        /*unsup pvarXMLDatosMID.selectSingleNode( "//XMLIN/IMAGES" ) */.appendChild( wobjNodo64 );
        /*unsup pvarXMLDatosMID.selectSingleNode( "//XMLIN" ) */.removeChild( null /*unsup pvarXMLDatosMID.selectSingleNode( "//XMLIN/croquis[@tipo='" + diamondedge.util.XmlDom.getText( wobjNodoCroquis.getAttributes().getNamedItem( "tipo" ) ) + "']" ) */ );
      }
      else
      {
        //Si fallo la generacion del nodo en base 64 remueve la imagen para no
        //enviarla a MIDDLEWARE
        /*unsup pvarXMLDatosMID.selectSingleNode( "//XMLIN" ) */.removeChild( null /*unsup pvarXMLDatosMID.selectSingleNode( "//XMLIN/croquis[@tipo='" + diamondedge.util.XmlDom.getText( wobjNodoCroquis.getAttributes().getNamedItem( "tipo" ) ) + "']" ) */ );
      }
    }

    //Bollos de OTROS vehiculos involucrados
    for( int nwobjNodoCroquis = 0; nwobjNodoCroquis < null /*unsup pvarXMLDatosMID.selectNodes( "//VEHICULO/croquis" ) */.getLength(); nwobjNodoCroquis++ )
    {
      wobjNodoCroquis = null /*unsup pvarXMLDatosMID.selectNodes( "//VEHICULO/croquis" ) */.item( nwobjNodoCroquis );
      //
      //Armo el Base 64 de cada croquis
      wobjNodo64 = invoke( "fncXMLtoJPG", new Variant[] { new Variant(wobjNodoCroquis.toString()), new Variant(diamondedge.util.XmlDom.getText( null /*unsup wobjNodoCroquis.getParentNode().selectSingleNode( new Variant("ID") ) */ )) } );
      //
      if( ! (wobjNodo64 == (org.w3c.dom.Element) null) )
      {
        //Lo apendeo al XML que viajará al Middleware
        /*unsup pvarXMLDatosMID.selectSingleNode( "//XMLIN/IMAGES" ) */.appendChild( wobjNodo64 );
        /*unsup pvarXMLDatosMID.selectSingleNode( "//VEHICULO[ID='" + diamondedge.util.XmlDom.getText( null (*unsup wobjNodoCroquis.getParentNode().selectSingleNode( "ID" ) *) ) + "']" ) */.removeChild( null /*unsup pvarXMLDatosMID.selectSingleNode( "//VEHICULO[ID='" + diamondedge.util.XmlDom.getText( null (*unsup wobjNodoCroquis.getParentNode().selectSingleNode( "ID" ) *) ) + "']/croquis" ) */ );
      }
      else
      {
        //Si fallo la generacion del nodo en base 64 remueve la imagen para no
        //enviarla a MIDDLEWARE
        /*unsup pvarXMLDatosMID.selectSingleNode( "//VEHICULO[ID='" + diamondedge.util.XmlDom.getText( null (*unsup wobjNodoCroquis.getParentNode().selectSingleNode( "ID" ) *) ) + "']" ) */.removeChild( null /*unsup pvarXMLDatosMID.selectSingleNode( "//VEHICULO[ID='" + diamondedge.util.XmlDom.getText( null (*unsup wobjNodoCroquis.getParentNode().selectSingleNode( "ID" ) *) ) + "']/croquis" ) */ );
      }
    }
    //
  }

  private org.w3c.dom.Element fncXMLtoJPG( String pvarRequestXML, String pvarTipoCroquis ) throws Exception
  {
    org.w3c.dom.Element fncXMLtoJPG = null;
    diamondedge.util.XmlDom wobjLocResponse = null;
    diamondedge.util.XmlDom wobjResponse = null;
    org.w3c.dom.Element wobjLocNodo = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    String wvarResponse = "";
    //
    //
    wobjClass = new Variant( new com.qbe.services.lbaw_XMLtoJPG() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.lbaw_XMLtoJPG().toObject());
    wobjClass.Execute( pvarRequestXML, wvarResponse, "" );
    wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
    //
    wobjLocResponse = new diamondedge.util.XmlDom();
    //unsup wobjLocResponse.async = false;
    //
    wobjLocResponse.loadXML( wvarResponse );
    //
    //Verifico la respuesta del COM+
    if( ! (null /*unsup wobjLocResponse.selectSingleNode( "//Response" ) */ == (org.w3c.dom.Node) null) )
    {
      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjLocResponse.selectSingleNode( "//Response/Estado/@resultado" ) */ ) ).equals( "TRUE" ) )
      {
        //
        wobjResponse = new diamondedge.util.XmlDom();
        //unsup wobjResponse.async = false;
        wobjResponse.loadXML( "<IMAGE></IMAGE>" );

        wobjLocNodo = wobjResponse.getDocument().createElement( "NAME" );
        diamondedge.util.XmlDom.setText( wobjLocNodo, pvarTipoCroquis );
        /*unsup wobjResponse.selectSingleNode( "//IMAGE" ) */.appendChild( wobjLocNodo );

        wobjLocNodo = wobjResponse.getDocument().createElement( "CONTENT" );
        diamondedge.util.XmlDom.setText( wobjLocNodo, diamondedge.util.XmlDom.getText( null /*unsup wobjLocResponse.selectSingleNode( "//BinData" ) */ ) );
        wobjLocNodo.setAttribute( "DataType", "bin.Base64" );
        /*unsup wobjResponse.selectSingleNode( "//IMAGE" ) */.appendChild( wobjLocNodo );
        //
        fncXMLtoJPG = wobjResponse.getDocument().getDocumentElement();
      }
      else
      {
        fncXMLtoJPG = (org.w3c.dom.Element) null;
      }
    }
    else
    {
      fncXMLtoJPG = (org.w3c.dom.Element) null;
    }
    //
    wobjLocResponse = (diamondedge.util.XmlDom) null;
    wobjResponse = (diamondedge.util.XmlDom) null;
    wobjLocNodo = (org.w3c.dom.Element) null;

    return fncXMLtoJPG;
  }

  public String SaveBinaryData( String FileName, String ByteArray ) throws Exception
  {
    String SaveBinaryData = "";
    diamondedge.util.XmlDom wobjXMLPdf = null;
    org.w3c.dom.Node wobjoNode = null;
    ADODB.Stream BinaryStream = new ADODB.Stream();


    //unsup On Error GoTo 0 not supported in this context
    wobjXMLPdf = new diamondedge.util.XmlDom();
    //unsup wobjXMLPdf.async = false;
    wobjXMLPdf.loadXML( "<PDF xmlns:dt=\"urn:schemas-microsoft-com:datatypes\" dt:dt=\"bin.base64\">" + ByteArray + "</PDF>" );

    wobjoNode = null /*unsup wobjXMLPdf.getDocument().getDocumentElement().selectSingleNode( "//PDF" ) */;

    //Create Stream object
    BinaryStream = new ADODB.Stream();

    //Specify stream type - we want To save binary data.
    BinaryStream.Charset.set( "Windows-1252" );
    BinaryStream.Type.set( adTypeBinary );
    BinaryStream.Mode.set( 0 );

    //Open the stream And write binary data To the object
    BinaryStream.Open();
    BinaryStream.Write( new Variant() /*unsup wobjoNode.nodeTypedValue */ );

    //Save binary data To disk
    BinaryStream.Position.set( 0 );
    BinaryStream.SaveToFile( new Variant( FileName ), adSaveCreateOverWrite );

    BinaryStream.Close();
    BinaryStream = (ADODB.Stream) null;

    SaveBinaryData = FileName;

    return SaveBinaryData;
  }

  private String obtenerNombreRandom( String Ruta, String Extension ) throws Exception
  {
    String obtenerNombreRandom = "";
    int wvarRandom = 0;
    boolean wvarGeneradoOK = false;
    String wvarNombreArchivo = "";

    //
    //Genera numero random entre 0 y 9999 como nombre de archivo
    //y verifica que no exista un archivo con ese nombre
    wvarGeneradoOK = false;
    while( ! (wvarGeneradoOK) )
    {
      //
      wvarRandom = (int)Math.rint( VB.rnd() * 10000 );
      wvarNombreArchivo = Ruta + "Denuncia" + Strings.right( ("0000" + String.valueOf( wvarRandom )), 4 ) + "." + Extension;
      //
      if( "" /*unsup this.Dir( wvarNombreArchivo, 0 ) */.equals( "" ) )
      {
        wvarGeneradoOK = true;
      }
      else
      {
        wvarGeneradoOK = false;
      }
    }

    obtenerNombreRandom = wvarNombreArchivo;

    return obtenerNombreRandom;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
