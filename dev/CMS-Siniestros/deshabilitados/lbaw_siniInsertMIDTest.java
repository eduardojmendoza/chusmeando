package com.qbe.services.siniestros.impl;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Node;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

public class lbaw_siniInsertMIDTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIAction_Execute() throws IOException, XmlDomExtendedException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		lbaw_siniInsertMID comp = new lbaw_siniInsertMID();
		String log = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("log_lbaw_siniInsertMID.xml"));
		XmlDomExtended logXml = new XmlDomExtended();
		logXml.loadXML(log);
		Node requestNode = logXml.selectSingleNode("//LOG/Request");

		String pvarRequest = XmlDomExtended.marshal(requestNode); 
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
		assertTrue("No encontró resultado esperado. Vino: " + sh.getValue(),sh.getValue().contains("resultado=\"true\"") );
	}
	
	@Test
	public void testIAction_ExecuteBug1706() throws IOException, XmlDomExtendedException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("prod"));
		
		lbaw_siniInsertMID comp = new lbaw_siniInsertMID();
		String log = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("log_lbaw_siniInsertMID.xml"));
		XmlDomExtended logXml = new XmlDomExtended();
		logXml.loadXML(log);
		Node requestNode = logXml.selectSingleNode("//LOG/Request");

		String pvarRequest = XmlDomExtended.marshal(requestNode); 
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
		
		XmlDomExtended sendXml = new XmlDomExtended();
		sendXml.loadXML(sh.getValue());
		Node rutaPDFNode = sendXml.selectSingleNode("//Response/RUTAPDF");
		
		assertNotNull("La ruta a probar es:"+ rutaPDFNode.getTextContent().substring(rutaPDFNode.getTextContent().indexOf("/")), rutaPDFNode);
		assertTrue("Tamaño del archivo es vacio. Vino: " + sh.getValue(),FileUtils.sizeOf(new  File(rutaPDFNode.getTextContent().substring(rutaPDFNode.getTextContent().indexOf("/")))) > 0 );

	//	assertTrue("Tamaño del archivo es vacio." + FileUtils.sizeOf(new  File("cms:/tmp/NBWS/pdf/Denuncia9449.PDF".substring(4))) , FileUtils.sizeOf(new  File("/tmp/NBWS/pdf/Denuncia9449.PDF")) > 0 );
	}

	@Test
	public void testIAction_Execute_bug625() throws IOException, XmlDomExtendedException, CurrentProfileException {
//		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		lbaw_siniInsertMID comp = new lbaw_siniInsertMID();
		String log = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("log_lbaw_siniInsertMID-bug625.xml"));
		XmlDomExtended logXml = new XmlDomExtended();
		logXml.loadXML(log);
		Node requestNode = logXml.selectSingleNode("//LOG/Request");

		String pvarRequest = XmlDomExtended.marshal(requestNode); 
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
		assertTrue("No encontró resultado esperado. Vino: " + sh.getValue(),sh.getValue().contains("resultado=\"true\"") );
	}

	@Test
	public void testIAction_Execute_bug798() throws IOException, XmlDomExtendedException, CurrentProfileException {
//		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		lbaw_siniInsertMID comp = new lbaw_siniInsertMID();
		String log = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("log_lbaw_siniInsertMID-bug798.xml"));
		XmlDomExtended logXml = new XmlDomExtended();
		logXml.loadXML(log);
		Node requestNode = logXml.selectSingleNode("//LOG/Request");

		String pvarRequest = XmlDomExtended.marshal(requestNode); 
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
//		System.out.println(sh.getValue());
		assertTrue("No encontró resultado esperado. Vino: " + sh.getValue(),sh.getValue().contains("resultado=\"true\"") );
	}


	@Test
	public void test_ismGeneratePublishAndSendPdfReport() throws IOException, XmlDomExtendedException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		lbaw_siniInsertMID comp = new lbaw_siniInsertMID();
		String log = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("mqmw_ismGeneratePublishAndSendPdfReport_1.xml"));
		XmlDomExtended logXml = new XmlDomExtended();
		logXml.loadXML(log);
		Node requestNode = logXml.selectSingleNode("//Request");

		String pvarRequest = XmlDomExtended.marshal(requestNode); 
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
		
		assertNotNull(sh.getValue());
		System.out.println(sh.getValue());
		
		XmlDomExtended sendXml = new XmlDomExtended();
		sendXml.loadXML(sh.getValue());
		Node resultNode = sendXml.selectSingleNode("//Response/Estado/@resultado");
		assertNotNull("resultNode es null!", resultNode);
		assertEquals("Vino resultado non true", "true", XmlDomExtended.getText(resultNode));
		Node messageNode = sendXml.selectSingleNode("//Response/Estado/@mensaje");
		String message = XmlDomExtended.getText(messageNode);
		assertTrue("Vino un mensaje inesperado como respuesta: " + message, message.length() == 0 );

		Node rutaPDFNode = sendXml.selectSingleNode("//Response/RUTAPDF");
		String rutaPDF = XmlDomExtended.getText(rutaPDFNode);
		assertTrue("Vino una rutaPDF cuando no debería haber venido nada: " + rutaPDF, rutaPDF.length() == 0 );
	}

}
