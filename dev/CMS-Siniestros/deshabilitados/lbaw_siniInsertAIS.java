package com.qbe.services.siniestros.impl;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.Variant;


public class lbaw_siniInsertAIS extends BaseOSBClient implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniInsertAIS";
  /**
   * 
   * Constantes de párametros de Entrada
   */
  static final String mcteParam_SINIENUMSQL = "//SINIENUMSQL";

  private EventLog mobjEventLog = new EventLog();
  
  protected static Logger logger = Logger.getLogger(lbaw_siniInsertAIS.class.getName());


  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    String wvarRequest = "";
    String wvarResponse = "";
    XmlDomExtended wobjXMLResponseAIS = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String mvarXMLDatosAIS = "";
    String wvarDEFINICION = "";
    String wvarSINIEANNAIS = "";
    String wvarSINIENUMAIS = "";
    String wvarFECHA_GRABACION = "";
    String wvarMODO = "";
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wvarRequest = Strings.replace( pvarRequest, "<CAMPOS>", "" );
      wvarRequest = Strings.replace( wvarRequest, "</CAMPOS>", "" );

      wvarStep = 20;
      //Llamo al MQ Genérico para guardar en AIS/INSIS
      wvarResponse = getOsbConnector().executeRequest(lbaw_siniMQGenerico.ACTION_CODE, wvarRequest);

      wvarStep = 30;
      wobjXMLResponseAIS = new XmlDomExtended();
      wobjXMLResponseAIS.loadXML( wvarResponse );

      wvarStep = 40;
      if( ! (wobjXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended.getText( wobjXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          wvarStep = 50;
          //Grabó
          wvarFECHA_GRABACION = XmlDomExtended.getText( wobjXMLResponseAIS.selectSingleNode( "//GRABAANN" )  ) + Strings.right( "00" + XmlDomExtended.getText( wobjXMLResponseAIS.selectSingleNode( "//GRABAMES" )  ), 2 ) + Strings.right( ("00" + XmlDomExtended.getText( wobjXMLResponseAIS.selectSingleNode( "//GRABADIA" )  )), 2 ) + " " + Strings.right( ("00" + XmlDomExtended.getText( wobjXMLResponseAIS.selectSingleNode( "//GRABAHOR" )  )), 2 ) + ":" + Strings.right( ("00" + XmlDomExtended.getText( wobjXMLResponseAIS.selectSingleNode( "//GRABAMIN" )  )), 2 );
          wvarSINIEANNAIS = XmlDomExtended.getText( wobjXMLResponseAIS.selectSingleNode( "//SINIEANN" )  );
          wvarSINIENUMAIS = XmlDomExtended.getText( wobjXMLResponseAIS.selectSingleNode( "//SINIENUM" )  );
          wvarMODO = "O";
        }
        else
        {
          wvarStep = 60;
          //Logueo la respuesta para que se pueda al menos analizar posteriormente
          logger.log(Level.WARNING, String.format("WARNING: lbaw_siniInsertAIS - Respuesta de OSB en false al enviar una denuncia de siniestro a OSB. Response = [%s]", wvarResponse));
          //No grabó
          if( XmlDomExtended.getText( wobjXMLResponseAIS.selectSingleNode( "//Response/Estado/@mensaje" )  ).equals( "CO" ) )
          {
            wvarMODO = "C";
          }
          else
          {
            wvarMODO = "NG";
          }
          wvarFECHA_GRABACION = "";
          wvarSINIEANNAIS = "";
          wvarSINIENUMAIS = "";
        }
      }
      else
      {
        wvarStep = 70;
        //Problemas al ejecutar el COM+ de grabación en AIS. Asumimos un NONSys detectado al guardar.
        wvarFECHA_GRABACION = "";
        wvarSINIEANNAIS = "";
        wvarSINIENUMAIS = "";
        wvarMODO = "NG";
      }

      wvarStep = 80;
      pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><SINIEANNAIS>" + wvarSINIEANNAIS + "</SINIEANNAIS><SINIENUMAIS>" + wvarSINIENUMAIS + "</SINIENUMAIS><FECHA_GRABACION>" + wvarFECHA_GRABACION + "</FECHA_GRABACION><MODO>" + wvarMODO + "</MODO></Response>" );

      wvarStep = 90;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      wvarStep = 100;
      wobjXMLResponseAIS = null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        throw new ComponentExecutionException(_e_);
    }
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
