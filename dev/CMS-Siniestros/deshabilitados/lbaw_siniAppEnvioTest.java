package com.qbe.services.siniestros.impl;

import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Node;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

public class lbaw_siniAppEnvioTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIAction_Execute() throws IOException, XmlDomExtendedException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		lbaw_siniAppEnvio comp = new lbaw_siniAppEnvio();
		OSBConnector conn = new OSBConnector();
		//Cableado al de test porque en este ambiente tenemos datos
		String OV_SERVICE = "http://10.1.10.98:8011/OV/Proxy/OVSvc?wsdl";
		conn.setServiceURL(OV_SERVICE);
		comp.setOsbConnector(conn);

		String pvarRequest = "<Request/>"; 
		StringHolder sh = new StringHolder();
		System.out.println(comp.IAction_Execute(pvarRequest, sh, ""));
//		System.out.println(sh.getValue());
//		assertTrue("No encontró resultado esperado. Vino " + sh.getValue(),( sh.getValue().contains("<MODO>O</MODO>") || sh.getValue().contains("<MODO>NG</MODO>")));
		//FIXME buscar la respuesta completa, algo más que el "true"
	}

}
