package com.qbe.services.siniestros.impl;
import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.ado.Command;
import diamondedge.ado.Connection;
import diamondedge.ado.Parameter;
import diamondedge.ado.Recordset;
import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_siniAppEnvio extends BaseOSBClient implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_SiniDenuncia.lbaw_siniAppEnvio";
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLDetalleDenuncia = null;
    XmlDomExtended wobjXMLDenuncias = null;
    org.w3c.dom.Node wobjXMLDenuncia = null;
    XmlDomExtended wobjXMLDatosAIS = null;
    XmlDomExtended wobjXMLDatosMID = null;
    VBObjectClass wobjClass = null;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarRAMOPCOD = "";
    String wvarSINIEANNAIS = "";
    String wvarSINIENUMAIS = "";
    String wvarSINIENUMSQL = "";
    String wvarMARENVAIS = "";
    String wvarENVIADOAIS = "";
    String wvarMARENVMID = "";
    String wvarENVIADOMID = "";
    String wvarGRABSFH = "";
    String wvarXMLErrores = "";
    boolean wvarResultado = false;
    String wvarEstados = "";
    int wvarFormulario = 0;
    String wvarReportId = "";
    String wvarPDFPWD = "";
    org.w3c.dom.Element wvarElementAux = null;
    Variant wvarFechaGrabacion = new Variant();
    Variant wvarSinieAnn = new Variant();
    Variant wvarSinieNum = new Variant();
    Variant wvarMODO = new Variant();
    String wvarMetodo = "";
    String wvarXmlResponse = "";
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarXmlResponse = "";
      //
      wvarStep = 10;
      //
      //Obtener listado de SQL con registros con marcas MARENVAIS = P y MARENVMID = P
      wvarRequest = "<Request></Request>";
      wobjClass = new lbaw_siniGetLista();
      StringHolder shGetLista = new StringHolder();
      wobjClass.IAction_Execute(wvarRequest, shGetLista, "" );
      wvarResponse = shGetLista.getValue();
      //
      wvarStep = 20;
      //
      wobjXMLDenuncias = new XmlDomExtended();
      wobjXMLDenuncias.loadXML( wvarResponse );
      //
      wvarStep = 30;
      //
      //-------------------------------------------------------------------------------
      //SI Hay denuncia?
      //-------------------------------------------------------------------------------
      if( wobjXMLDenuncias.selectNodes( "//DENUNCIA" ) .getLength() > 0 )
      {
        //
        wvarStep = 40;
        //
        //Para cada denuncia de la lista...
        for( int nwobjXMLDenuncia = 0; nwobjXMLDenuncia < wobjXMLDenuncias.selectNodes( "//DENUNCIA" ) .getLength(); nwobjXMLDenuncia++ )
        {
          wobjXMLDenuncia = wobjXMLDenuncias.selectNodes( "//DENUNCIA" ) .item( nwobjXMLDenuncia );
          //
          wvarRAMOPCOD = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLDenuncia,"RAMOPCOD" )  );
          wvarSINIEANNAIS = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLDenuncia,"SINIEANNAIS" )  );
          wvarSINIENUMAIS = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLDenuncia,"SINIENUMAIS" )  );
          wvarSINIENUMSQL = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLDenuncia,"SINIENUMSQL" )  );
          wvarMARENVAIS = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLDenuncia,"MARENVAIS" )  );
          wvarENVIADOAIS = "";
          wvarMARENVMID = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLDenuncia,"MARENVMID" )  );
          wvarENVIADOMID = "";
          wvarGRABSFH = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLDenuncia,"GRABSFH" )  );
          wvarXMLErrores = "";
          wvarFormulario = Obj.toInt( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLDenuncia,"NROFORM" )  ) );
          //
          wvarStep = 50;
          //
          //Recuperar denuncia de SQL
          wvarRequest = "<Request><SINIENUMSQL>" + wvarSINIENUMSQL + "</SINIENUMSQL></Request>";
          wobjClass = new lbaw_siniGetItem();
          StringHolder shGetItem = new StringHolder();
          wobjClass.IAction_Execute(wvarRequest, shGetItem, "" );
          wvarResponse = shGetItem.getValue();
          //
          wvarStep = 60;
          //
          wobjXMLDetalleDenuncia = new XmlDomExtended();
          wobjXMLDetalleDenuncia.loadXML( wvarResponse );
          //
          wvarStep = 70;
          //
          //-------------------------------------------------------------------------------
          //SI MARENVAIS = "P" envia la denuncia al AIS ( en realidad, a OSB para que rutee a AIS o INSIS )
          //-------------------------------------------------------------------------------
          if( wvarMARENVAIS.equals( "P" ) )
          {
            //
            //DA - 04/11/2008: controla que existan los datos a enviar al AIS y no pinche la app de envío.
            if( ! (wobjXMLDetalleDenuncia.selectSingleNode( "//XMLDatosAIS/XMLIN/CAMPOS" )  == (org.w3c.dom.Node) null) )
            {
              //
              wobjXMLDatosAIS = new XmlDomExtended();
              //DA - 04/11/2008: antes buscaba //CAMPOS pero correponde que busque //XMLDatosAIS
              //Call .loadXML(wobjXMLDetalleDenuncia.selectSingleNode("//CAMPOS").xml)
              wobjXMLDatosAIS.loadXML( XmlDomExtended.marshal(wobjXMLDetalleDenuncia.selectSingleNode( "//XMLDatosAIS/XMLIN/CAMPOS" )) );
              //
              wvarStep = 80;
              //
              //Inserta en el XML que va al AIS el SINIENUMSQL
              XmlDomExtended.setText( wobjXMLDatosAIS.selectSingleNode( "//SINIESQL" ) , wvarSINIENUMSQL );
              //
              wvarStep = 90;
              //
              //Si esta faltando la fecha de denuncia, la inserta en el xml desde el SQL
              if( Strings.trim( XmlDomExtended.getText( wobjXMLDatosAIS.selectSingleNode( "//DENUCDIA" )  ) ).equals( "" ) )
              {
                XmlDomExtended.setText( wobjXMLDatosAIS.selectSingleNode( "//DENUCDIA" ) , Strings.mid( wvarGRABSFH, 9, 2 ) );
                XmlDomExtended.setText( wobjXMLDatosAIS.selectSingleNode( "//DENUCMES" ) , Strings.mid( wvarGRABSFH, 6, 2 ) );
                XmlDomExtended.setText( wobjXMLDatosAIS.selectSingleNode( "//DENUCANN" ) , Strings.mid( wvarGRABSFH, 1, 4 ) );
              }
              //
              wvarStep = 100;
              //
              //Guardo en AIS
              //     Dim wvarFileGet
              //         wvarFileGet = FreeFile()
              //     Open App.Path & "\XMLDatosAIS.xml" For Binary Access Write As wvarFileGet
              //     Put wvarFileGet, , wobjXMLDatosAIS.xml
              //     Close wvarFileGet
              wvarResultado = fncGuardarAIS(XmlDomExtended.marshal(wobjXMLDatosAIS.getDocument().getDocumentElement()), wvarFormulario,wvarFechaGrabacion,wvarSinieAnn,wvarSinieNum,wvarMODO);
              //
              wvarStep = 110;
              //
              //Envió con éxito? (no explotó algo en el camino?)
              if( wvarResultado )
              {
                if( (!wvarSinieNum.toString().equals( "0" )) && (!wvarSinieNum.toString().equals( "" )) )
                {
                  //Update SQL con MARENVAIS = E
                  ActualizarMarenvAIS(wvarSINIENUMSQL);
                  wvarENVIADOAIS = "S";
                  wvarMARENVAIS = "E";
                  wvarXMLErrores = wvarXMLErrores + "<ENVIOAIS>Enviado a AIS con éxito</ENVIOAIS>";
                }
                else
                {
                  wvarENVIADOAIS = "N";
                  wvarMARENVAIS = "P";
                  wvarXMLErrores = wvarXMLErrores + "<ENVIOAIS>Falló el envío al AIS</ENVIOAIS>";
                }
              }
              else
              {
                wvarENVIADOAIS = "N";
                wvarMARENVAIS = "P";
                wvarXMLErrores = wvarXMLErrores + "<ENVIOAIS>Falló el envio al AIS</ENVIOAIS>";
              }
              //
              wvarStep = 120;
              //
              wvarXMLErrores = wvarXMLErrores + "<SINIEANN>" + wvarSinieAnn + "</SINIEANN>";
              wvarXMLErrores = wvarXMLErrores + "<SINIENUM>" + wvarSinieNum + "</SINIENUM>";
              wvarXMLErrores = wvarXMLErrores + "<MODO>" + wvarMODO + "</MODO>";
              //
              //DA - 24/08/2007: si grabó en AIS, tiene que mandar al impreso el número generado por el AIS
              wvarSINIEANNAIS = wvarSinieAnn.toString();
              wvarSINIENUMAIS = wvarSinieNum.toString();
              //
              wobjXMLDatosAIS = null;
              //
            }
            else
            {
              //DOING
              //DA - 04/11/2008: no se encontró el dato en SQL. Esto evita que pinche si no se encuentra el dato en SQL
              wvarENVIADOAIS = "N";
              wvarMARENVAIS = "X";
              wvarXMLErrores = wvarXMLErrores + "<ENVIOAIS>No se encontró el dato en SQL</ENVIOAIS>";
              //Update SQL con MARENVAIS = X
              ActualizarMarenvAIS_X(wvarSINIENUMSQL);
            }
          }
          else
          {
            //Se limpia el contenido de la variable para que la página no muestre el dato
            wvarMARENVAIS = "";
          }
          //-------------------------------------------------------------------------------
          //-------------------------------------------------------------------------------
          //SI MARENVMID = "P" envia la denuncia a middleware
          //-------------------------------------------------------------------------------
          if( wvarMARENVMID.equals( "P" ) )
          {
            //
            //DA - 04/11/2008: controla que existan los datos a enviar a MDW y no pinche la app de envío.
            if( ! (wobjXMLDetalleDenuncia.selectSingleNode( "//XMLDatosMID/XMLIN/CAMPOS" )  == (org.w3c.dom.Node) null) )
            {
              //
              wobjXMLDatosMID = new XmlDomExtended();
              wobjXMLDatosMID.loadXML( XmlDomExtended.marshal(wobjXMLDetalleDenuncia.selectSingleNode( "//XMLDatosMID" )));
              //
              wvarStep = 125;
              //
              //MHC:
              //Quitar estas lineas cuando se normalice la situación de las denuncias pendientes
              //en UAT
              if( wobjXMLDatosMID.selectSingleNode( "//NUMEROSINIESTROCOMPLETO" )  == (org.w3c.dom.Node) null )
              {
                wvarElementAux = wobjXMLDatosMID.getDocument().createElement( "NUMEROSINIESTROCOMPLETO" );
                wobjXMLDatosMID.selectSingleNode( "//CAMPOS" ).appendChild( wvarElementAux );
              }
              //
              wvarStep = 130;
              //
              //Inserta en el XML que va al MID el SINIENUMSQL
              if( !wvarSINIENUMAIS.equals( "0" ) )
              {
                XmlDomExtended.setText( wobjXMLDatosMID.selectSingleNode( "//SINIEANN" ) , wvarSINIEANNAIS );
                XmlDomExtended.setText( wobjXMLDatosMID.selectSingleNode( "//SINIENUM" ) , wvarSINIENUMAIS );
                //
                //DA - 24/08/2007: buscar el RAMOPCOD en el XMLDatosMID recuperado del SQL
                XmlDomExtended.setText( wobjXMLDatosMID.selectSingleNode( "//NUMEROSINIESTROCOMPLETO" ) , XmlDomExtended.getText( wobjXMLDatosMID.selectSingleNode( "//RAMOPCOD" )  ) + "-" + wvarSINIEANNAIS + "-" + wvarSINIENUMAIS );
              }
              else
              {
                XmlDomExtended.setText( wobjXMLDatosMID.selectSingleNode( "//SINIESQL" ) , wvarSINIENUMSQL );
                //DA - 24/08/2007
                XmlDomExtended.setText( wobjXMLDatosMID.selectSingleNode( "//NUMEROSINIESTROCOMPLETO" ) , wvarSINIENUMSQL );
              }
              //
              wvarStep = 140;
              //
              //Si esta faltando la fecha de denuncia, la inserta en el xml desde el SQL
              if( Strings.trim( XmlDomExtended.getText( wobjXMLDatosMID.selectSingleNode( "//DENUCDIA" )  ) ).equals( "" ) )
              {
                XmlDomExtended.setText( wobjXMLDatosMID.selectSingleNode( "//DENUCDIA" ) , Strings.mid( wvarGRABSFH, 9, 2 ) );
                XmlDomExtended.setText( wobjXMLDatosMID.selectSingleNode( "//DENUCMES" ) , Strings.mid( wvarGRABSFH, 6, 2 ) );
                XmlDomExtended.setText( wobjXMLDatosMID.selectSingleNode( "//DENUCANN" ) , Strings.mid( wvarGRABSFH, 1, 4 ) );
              }
              //
              wvarStep = 150;
              //
              //Envio a MID
              if( XmlDomExtended.getText( wobjXMLDatosMID.selectSingleNode( "//DENUNMAIL" )  ).equals( "" ) )
              {
                wvarMetodo = "ismGenerateAndPublishPdfReport.xml";
              }
              else
              {
                wvarMetodo = "ismGeneratePublishAndSendPdfReport.xml";
              }
              //
              wvarStep = 160;
              //
              //Seleccion de ReportId
              
              if( wvarFormulario == 1 )
              {
                wvarReportId = "DenunciaAseguradoAutosF1_1.0";
              }
              else if( wvarFormulario == 2 )
              {
                wvarReportId = "DenunciaAseguradoAutosF2_1.0";
              }
              else if( wvarFormulario == 3 )
              {
                wvarReportId = "DenunciaAseguradoAutosF3_1.0";
              }
              else if( wvarFormulario == 4 )
              {
                wvarReportId = "DenunciaDeTerceros_1.0";
              }
              else if( wvarFormulario == 5 )
              {
                wvarReportId = "RiesgosVarios_1.0";
              }
              //
              wvarStep = 170;
              //
              if( ! (wobjXMLDatosMID.selectSingleNode( "//PDFPWD" )  == (org.w3c.dom.Node) null) )
              {
                wvarPDFPWD = XmlDomExtended.getText( wobjXMLDatosMID.selectSingleNode( "//PDFPWD" )  );
              }
              else
              {
                wvarPDFPWD = "";
              }
              //
              wvarEstados = fncEnviaMiddleware(XmlDomExtended.marshal(wobjXMLDatosMID.selectSingleNode("//XMLIN")),
            		  wvarMetodo,wvarReportId,wvarPDFPWD);
              //
              wvarStep = 180;
              //
              //Si "P"DF está ok y "E"mail está ok y "C"oldview está ok...
              if( (Strings.find( 1, wvarEstados, "P" ) == 0) && (Strings.find( 1, wvarEstados, "C" ) == 0) && (Strings.find( 1, wvarEstados, "E" ) == 0) )
              {
                //Actualiza el SQL con el estado "E"nviado
                wvarMARENVMID = "E";
                wvarENVIADOMID = "S";
                wvarXMLErrores = wvarXMLErrores + "<ENVIOMID>Enviado a Middleware con éxito</ENVIOMID>";
              }
              else
              {
                //Actualiza el SQL con el estado "P"endiente, y actualiza el wvarEstados
                wvarMARENVMID = "P";
                wvarENVIADOMID = "N";
                wvarXMLErrores = wvarXMLErrores + "<ENVIOMID>Hubo un error en Middleware (" + wvarEstados + ")</ENVIOMID>";
              }
              //
              //DA - 04/11/2008: se quita de acá la invocación al UPD de la MARENVMID. Se pone en step 190 (ver mas abajo)
              wobjXMLDatosMID = null;
            }
            else
            {
              wvarStep = 185;
              //DA - 04/11/2008: no se encontró el dato en SQL. Esto evita que pinche si no se encuentra el dato en SQL
              wvarMARENVMID = "X";
              wvarENVIADOMID = "N";
              wvarXMLErrores = wvarXMLErrores + "<ENVIOMID>No se encontró el dato en SQL</ENVIOMID>";
            }
            //
            //DA - 04/11/2008: se pone acá la invocación al UPD de la MARENVMID.
            wvarStep = 190;
            wvarRequest = "<Request>";
            wvarRequest = wvarRequest + "<SINIENUMSQL>" + wvarSINIENUMSQL + "</SINIENUMSQL>";
            wvarRequest = wvarRequest + "<MARENVMID>" + wvarMARENVMID + "</MARENVMID>";
            wvarRequest = wvarRequest + "<ESTADOMID>" + wvarEstados + "</ESTADOMID>";
            wvarRequest = wvarRequest + "</Request>";
            //
            wobjClass = new lbaw_siniUpdMenvMID();
            StringHolder shUpd = new StringHolder();
            wobjClass.IAction_Execute(wvarRequest, shUpd, "" );
            wvarResponse = shUpd.getValue();
            //
          }
          else
          {
            //Se limpia el contenido de la variable para que la página no muestre el dato
            wvarMARENVMID = "";
          }
          //-------------------------------------------------------------------------------
          //
          wvarStep = 200;
          //
          //-------------------------------------------------------------------------------
          //INSERT log de Envio
          //-------------------------------------------------------------------------------
          wvarRequest = "<Request>";
          wvarRequest = wvarRequest + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>";
          wvarRequest = wvarRequest + "<SINIEANNAIS>" + wvarSINIEANNAIS + "</SINIEANNAIS>";
          wvarRequest = wvarRequest + "<SINIENUMAIS>" + wvarSINIENUMAIS + "</SINIENUMAIS>";
          wvarRequest = wvarRequest + "<SINIENUMSQL>" + wvarSINIENUMSQL + "</SINIENUMSQL>";
          wvarRequest = wvarRequest + "<ENVIADOAIS>" + wvarENVIADOAIS + "</ENVIADOAIS>";
          wvarRequest = wvarRequest + "<ENVIADOMID>" + wvarENVIADOMID + "</ENVIADOMID>";
          wvarRequest = wvarRequest + "<ESTADOS>" + wvarXMLErrores + "</ESTADOS>";
          wvarRequest = wvarRequest + "</Request>";
          //
          wobjClass =new lbaw_siniInsLogEnvio();
          StringHolder shLog = new StringHolder();
          wobjClass.IAction_Execute(wvarRequest, shLog, "" );
          wvarResponse = shLog.getValue();
          //
          wvarStep = 210;
          //
          wobjXMLDetalleDenuncia = null;
          //
          //-------------------------------------------------------------------------------
          //Inserta en Response
          //-------------------------------------------------------------------------------
          wvarXmlResponse = wvarXmlResponse + "<DENUNCIA>" + "<NROFORM>" + String.valueOf( wvarFormulario ) + "</NROFORM><RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD><SINIEANNAIS>" + wvarSINIEANNAIS + "</SINIEANNAIS><SINIENUMAIS>" + wvarSINIENUMAIS + "</SINIENUMAIS><SINIENUMSQL>" + wvarSINIENUMSQL + "</SINIENUMSQL><MARENVAIS>" + wvarMARENVAIS + "</MARENVAIS><MARENVMID>" + wvarMARENVMID + "</MARENVMID><ESTADOS>" + wvarXMLErrores + "</ESTADOS>" + "</DENUNCIA>";
          //
          wvarStep = 220;
          //
          wobjXMLDetalleDenuncia = null;
          //
        }
        //
        wobjXMLDenuncias = null;
        //
      }
      //-------------------------------------------------------------------------------
      wvarStep = 300;
      pvarResponse.set( "<Response><DENUNCIAS>" + wvarXmlResponse + "</DENUNCIAS></Response>" );
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        throw new ComponentExecutionException(_e_);
    }
  }

  private boolean fncGuardarAIS( String pobjXMLDatosAIS, int pFormulario, Variant pFechaGrabacion, Variant pSinieAnn, Variant pSinieNum, Variant pModo ) throws Exception
  {
    boolean fncGuardarAIS = false;
    String wvarRequest = "";
    String wvarResponse = "";
    XmlDomExtended wobjResponse = null;
    String wvarXML_Definicion_Insert_AIS = "";
    lbaw_siniInsertAIS wobjClass = null;
    //En esta funcion se envia la denuncia al AIS
    //El retorno de la misma debe ser TRUE o FALSE dependiendo del exito del envio de la
    //denuncia.
    //
    //Determino cuál es el XML de definición para grabar en AIS.
    
    if( pFormulario == 1 )
    {
      wvarXML_Definicion_Insert_AIS = "siniestroDenuncia_putForm1AIS.xml";
    }
    else if( pFormulario == 2 )
    {
      wvarXML_Definicion_Insert_AIS = "siniestroDenuncia_putForm2AIS.xml";
    }
    else if( pFormulario == 3 )
    {
      wvarXML_Definicion_Insert_AIS = "siniestroDenuncia_putForm3AIS.xml";
    }
    else if( pFormulario == 5 )
    {
      wvarXML_Definicion_Insert_AIS = "siniestroDenuncia_putForm5AIS.xml";
    }

    wvarRequest = "<Request>" + pobjXMLDatosAIS + "<DEFINICION>" + wvarXML_Definicion_Insert_AIS + "</DEFINICION></Request>";
    // Llamo al COM+ que guarda en AIS/INSIS
    wobjClass = new lbaw_siniInsertAIS();
    wobjClass.setOsbConnector(getOsbConnector());
    StringHolder shInsert = new StringHolder();
    wobjClass.IAction_Execute(wvarRequest, shInsert, "" );
    wvarResponse = shInsert.getValue();

    //
    wobjResponse = new XmlDomExtended();
    wobjResponse.loadXML( wvarResponse );
    //
    if( ! (wobjResponse.selectSingleNode( "//MODO" )  == (org.w3c.dom.Node) null) )
    {
      pFechaGrabacion.set( XmlDomExtended.getText( wobjResponse.selectSingleNode( "//FECHA_GRABACION" )  ) );
      pSinieAnn.set( XmlDomExtended.getText( wobjResponse.selectSingleNode( "//SINIEANNAIS" )  ) );
      pSinieNum.set( XmlDomExtended.getText( wobjResponse.selectSingleNode( "//SINIENUMAIS" )  ) );
      pModo.set( XmlDomExtended.getText( wobjResponse.selectSingleNode( "//MODO" )  ) );
      fncGuardarAIS = true;
    }
    else
    {
      //Falló el COM+. Asumimos estado NONSys detectado en la página.
      pFechaGrabacion.set( "" );
      pSinieAnn.set( "0" );
      pSinieNum.set( "0" );
      pModo.set( "NG" );
      fncGuardarAIS = false;
    }
    //
    wobjResponse = null;

    return fncGuardarAIS;
  }

  private String fncEnviaMiddleware( String pXMLDatosMID, String pMetodo, String pReportId, String pPDFPWD ) throws Exception
  {
    String fncEnviaMiddleware = "";
    String wvarDENUNMAIL = "";
    String wvarDENUNMAILCC = "";
    XmlDomExtended wobjXMLMID = null;
    XmlDomExtended wobjXMLResponse = null;
    lbaw_siniInsertMID wobjClass = null;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarColdviewMetadata = "";
    String wvarNroSiniestroDef = "";
    String wvarNroSiniestroProv = "";
    String wvarFecha = "";
    String wvarDocumento = "";
    String wvarPoliza = "";
    String wvarRECARGA = "";
    String wvarEstadoDatos = "";
    String wvarCanal = "";
    String wvarTipo = "";
    String wvarAutor = "";
    //Funcion que va a enviar la denuncia a middleware
    //DA - 24/10/2008
    wobjXMLMID = new XmlDomExtended();
    wobjXMLMID.loadXML( pXMLDatosMID );

    //Si tiene SINIENUM pasa a MID el numero de siniestro del AIS, sino pasa el de SQL
    //SOLO UNO DE LOS DOS TIENE QUE PASAR A MIDDLEWARE
    if( !XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//SINIENUM" )  ).equals( "" ) )
    {
      wvarNroSiniestroDef = XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//RAMOPCOD" )  ) + "-" + XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//SINIEANN" )  ) + "-" + XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//SINIENUM" )  );
      wvarNroSiniestroProv = "";
    }
    else
    {
      wvarNroSiniestroDef = "";
      wvarNroSiniestroProv = Strings.right( "0000000000" + XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//SINIESQL" )  ), 10 );
    }
    wvarFecha = XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//DENUCANN" )  ) + XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//DENUCMES" )  ) + XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//DENUCDIA" )  );
    wvarDocumento = XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//DOCUMDAT" )  );
    wvarPoliza = XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//RAMOPCOD" )  ) + XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//POLIZANN" )  ) + XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//POLIZSEC" )  ) + XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//CERTIPOL" )  ) + XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//CERTIANN" )  ) + XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//CERTISEC" )  );
    wvarRECARGA = XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//RECARGA" )  );
    wvarEstadoDatos = XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//ESTADODAT" )  );
    wvarCanal = XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//GRABSVIA" )  );
    wvarAutor = "LBA_VO_FrontEnd";
    wvarTipo = "LBASiniestros";
    wvarDENUNMAIL = XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//DENUNMAIL" )  );

    //DA - 24/10/2008: se agrega a pedido de CS
    if( ! (wobjXMLMID.selectSingleNode( "//DENUNMAILCC" )  == (org.w3c.dom.Node) null) )
    {
      wvarDENUNMAILCC = XmlDomExtended.getText( wobjXMLMID.selectSingleNode( "//DENUNMAILCC" )  );
    }
    else
    {
      wvarDENUNMAILCC = "";
    }


    wvarColdviewMetadata = "NroSiniestroDef=" + wvarNroSiniestroDef + ";NroSiniestroProv=" + wvarNroSiniestroProv + ";Fecha=" + wvarFecha + ";documento=" + wvarDocumento + ";poliza=" + wvarPoliza + ";recarga=" + wvarRECARGA + ";estadodatos=" + wvarEstadoDatos + ";canal=" + wvarCanal + ";tipo=" + wvarTipo + ";autor=" + wvarAutor + ";Category=Ordinario";

    wvarRequest = "<Request>";
    wvarRequest = wvarRequest + "<xmlDataSource>" + pXMLDatosMID + "</xmlDataSource>";
    wvarRequest = wvarRequest + "<Metodo>" + pMetodo + "</Metodo>";
    wvarRequest = wvarRequest + "<ColdviewMetadata>" + wvarColdviewMetadata + "</ColdviewMetadata>";
    wvarRequest = wvarRequest + "<EmailTo>" + wvarDENUNMAIL + "</EmailTo>";
    wvarRequest = wvarRequest + "<recipientsCC>" + wvarDENUNMAILCC + "</recipientsCC>";
    wvarRequest = wvarRequest + "<ReportId>" + pReportId + "</ReportId>";
    wvarRequest = wvarRequest + "<DevuelvePDF>N</DevuelvePDF>";
    wvarRequest = wvarRequest + "<attachPassword>" + pPDFPWD + "</attachPassword>";
    wvarRequest = wvarRequest + "</Request>";

    wobjClass = new lbaw_siniInsertMID();
    StringHolder shInsert = new StringHolder();
    wobjClass.IAction_Execute(wvarRequest, shInsert, "" );
    wvarResponse = shInsert.getValue();

    wobjXMLResponse = new XmlDomExtended();
    wobjXMLResponse.loadXML( wvarResponse );

    if( ! (wobjXMLResponse.selectSingleNode( "//ESTADOMID" )  == (org.w3c.dom.Node) null) )
    {
      if( Strings.trim( XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//ESTADOMID" )  ) ).equals( "E" ) )
      {
        fncEnviaMiddleware = "";
      }
      else
      {
        fncEnviaMiddleware = XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//ESTADOMID" )  );
      }
    }
    else
    {
      //Si fallo el envio a MID, devuelve condición de error
      fncEnviaMiddleware = "PCE";
    }

    return fncEnviaMiddleware;
  }

  private void ActualizarMarenvAIS( String pvarSINIENUMSQL ) throws Exception
  {
    String wvarRequest = "";
    VBObjectClass wobjClass = null;

    //
    wvarRequest = "<Request>";
    wvarRequest = wvarRequest + "<SINIENUMSQL>" + pvarSINIENUMSQL + "</SINIENUMSQL>";
    wvarRequest = wvarRequest + "</Request>";
    //
    wobjClass = new lbaw_siniUpdMenvAIS();
    StringHolder shInsert = new StringHolder();
    wobjClass.IAction_Execute(wvarRequest, shInsert, "" );
    //No procesa la respuesta
  }

  /**
   * DA - 04/11/2008: nueva función
   */
  private void ActualizarMarenvAIS_X( String pvarSINIENUMSQL ) throws Exception
  {
    String wvarRequest = "";
    VBObjectClass wobjClass = null;

    //
    wvarRequest = "<Request>";
    wvarRequest = wvarRequest + "<SINIENUMSQL>" + pvarSINIENUMSQL + "</SINIENUMSQL>";
    wvarRequest = wvarRequest + "</Request>";
    //
    wobjClass = new lbaw_siniUpdMenvAISX();
    StringHolder shInsert = new StringHolder();
    wobjClass.IAction_Execute(wvarRequest, shInsert, "" );
    //No procesa la respuesta

  }

  private void ObjectControl_Activate() throws Exception
  {
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;


    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjEventLog = null;
    //
  }
}
