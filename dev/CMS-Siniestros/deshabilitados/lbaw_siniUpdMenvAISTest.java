package com.qbe.services.siniestros.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.vbcompat.string.StringHolder;

public class lbaw_siniUpdMenvAISTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIAction_Execute() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		lbaw_siniUpdMenvAIS upd = new lbaw_siniUpdMenvAIS();
		String request = "<Request><SINIENUMSQL>75951</SINIENUMSQL></Request>";
		StringHolder sh = new StringHolder();
		int result = upd.IAction_Execute(request, sh, null);
		assertTrue("No encontró resultado esperado. Vino " + sh.getValue(),sh.getValue().contains("resultado=\"true\""));

	}

}
