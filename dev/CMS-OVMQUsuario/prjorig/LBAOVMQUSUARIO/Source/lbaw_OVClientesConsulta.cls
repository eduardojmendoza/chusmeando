VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVClientesCons"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQUsuario.lbaw_OVClientesCons"
Const mcteOpID              As String = "1010"

'Parametros XML de Entrada
Const mcteParam_Usuario      As String = "//USUARIO"
Const mcteParam_NivelAs      As String = "//NIVELAS"
Const mcteParam_ClienSecAs   As String = "//CLIENSECAS"
Const mcteParam_DocumTip     As String = "//DOCUMTIP"
Const mcteParam_DocumNro     As String = "//DOCUMNRO"
Const mcteParam_Cliendes     As String = "//CLIENDES"
Const mcteParam_Producto     As String = "//PRODUCTO"
Const mcteParam_Poliza       As String = "//POLIZA"
Const mcteParam_Certi        As String = "//CERTI"
Const mcteParam_Patente      As String = "//PATENTE"
Const mcteParam_Motor        As String = "//MOTOR"
Const mcteParam_EstPol       As String = "//ESTPOL"
'Const mcteParam_Continuar    As String = "//CONTINUAR"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    'Dim wvarResultTR        As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarNivelAs         As String
    Dim wvarClienSecAs      As String
    Dim wvarProducto        As String
    Dim wvarPoliza          As String
    Dim wvarCerti           As String
    Dim wvarDocumTip        As String
    Dim wvarDocumNro        As String
    Dim wvarCliendes        As String
    Dim wvarPatente         As String
    Dim wvarMotor           As String
    Dim wvarEstPol          As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    Dim wvarMensaje         As String
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los par�metros que llegan desde la p�gina
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarNivelAs = Left(.selectSingleNode(mcteParam_NivelAs).Text & Space(2), 2)
        wvarClienSecAs = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSecAs).Text, 9)
        wvarProducto = Left(.selectSingleNode(mcteParam_Producto).Text & Space(4), 4)
        wvarPoliza = Right(String(8, "0") & .selectSingleNode(mcteParam_Poliza).Text, 8)
        wvarCerti = Right(String(14, "0") & .selectSingleNode(mcteParam_Certi).Text, 14)
        wvarDocumTip = Right("00" & .selectSingleNode(mcteParam_DocumTip).Text, 2)
        wvarDocumNro = Left(.selectSingleNode(mcteParam_DocumNro).Text & Space(11), 11)
        wvarCliendes = Left(.selectSingleNode(mcteParam_Cliendes).Text & Space(20), 20)
        wvarPatente = Left(.selectSingleNode(mcteParam_Patente).Text & Space(10), 10)
        wvarMotor = Left(.selectSingleNode(mcteParam_Motor).Text & Space(20), 20)
        wvarEstPol = Left(.selectSingleNode(mcteParam_EstPol).Text & Space(30), 30)
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los Parametros de la cola de MQ del archivo de configuraci�n
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.Load (App.Path & "\" & gcteParamFileName)
    wvarCiaAsCod = wobjXMLParametros.selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & Space(4) & "000000000  " & wvarNivelAs & wvarClienSecAs & "  000000000  000000000" & wvarDocumTip & wvarDocumNro & wvarCliendes & wvarProducto & wvarPoliza & wvarCerti & wvarPatente & wvarMotor & wvarEstPol
    wvarStep = 190
    wvarResult = ""
    wvarPos = 186 'cantidad de caracteres ocupados por par�metros de entrada
        
    wvarStep = 100
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 225
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
        
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 200
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 210
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 240
        wvarResult = ""
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        '
        wvarStep = 250
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 260
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML (wvarResult)
        '
        If wobjXMLResponse.selectNodes("//R").length <> 0 Then
            wvarStep = 270
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSL())
            '
            wvarStep = 280
            wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
            '
            wvarStep = 290
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
            '
            wvarStep = 300
        
            If wvarEstado = "TR" And Mid(strParseString, 21, 2) = "52" Then
                wvarMensaje = "<MSGEST><![CDATA[La b�squeda realizada puede estar incompleta. Si usted no encontr� el cliente deseado, ingrese un dato mas espec�fico y realice la b�squeda nuevamente.]]></MSGEST>"
                Response = "<Response><Estado resultado='true' mensaje='' />" & wvarMensaje & wvarResult & "</Response>"
            Else
                wvarMensaje = "<MSGEST></MSGEST>"
                Response = "<Response><Estado resultado='true' mensaje='' />" & wvarMensaje & wvarResult & "</Response>"
            End If
            '
        Else
            '
            If wvarEstado = "TR" And Mid(strParseString, 21, 2) = "52" Then
                Response = "<Response><Estado resultado='false' mensaje='La b�squeda realizada es muy extensa, por favor ingrese un dato mas espec�fico y real�cela nuevamente.' /></Response>"
            Else
                wvarStep = 310
                Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>"
            End If
            '
            Set wobjXMLResponse = Nothing
        End If
    End If
    '
    wvarStep = 320
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS MQ
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarDocumTip & wvarDocumNro & wvarCliendes & wvarProducto & wvarPoliza & wvarCerti & wvarPatente & wvarMotor & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarstrLen As Long) As String
Dim wvarResult As String
    '
    wvarResult = wvarResult & "<RS>"
    '
    While wvarPos < wvarstrLen And Trim(Mid(strParseString, wvarPos, 4)) <> ""
        wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 139) & "]]></R>"
        'wvarPos = wvarPos + 138
        wvarPos = wvarPos + 139
    Wend
    '
    wvarResult = wvarResult & "</RS>"
    '
    ParseoMensaje = wvarResult
    '
End Function
Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/'> "
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REGS'>"
    wvarStrXSL = wvarStrXSL & "              <xsl:apply-templates select='/RS/R'/>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RS/R'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REG'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='PROD'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,1,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='POL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,5,8)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERPOL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,13,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERANN'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,17,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERSEC'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,21,6)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CLIDES'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,40,30))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='TOMA'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,70,30))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='EST'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,100,30))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='SINI'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,130,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='ALERTEXI'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,131,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='AGE'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,132,2)' />-<xsl:value-of select='substring(.,134,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='RAMO'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,138,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='MARCAREIMPRESION'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,139,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIDES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='TOMA'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='EST'/>"
    '
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
    '
End Function


















