package com.qbe.services.ovmqusuario.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import com.qbe.vbcompat.format.VBFixesUtil;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVAnulxPagoDet implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQUsuario.lbaw_OVAnulxPagoDet";
  static final String mcteOpID = "1410";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_NivelAs = "//NIVELAS";
  static final String mcteParam_ClienSecAs = "//CLIENSECAS";
  static final String mcteParam_Nivel1 = "//NIVEL1";
  static final String mcteParam_ClienSec1 = "//CLIENSEC1";
  static final String mcteParam_Nivel2 = "//NIVEL2";
  static final String mcteParam_ClienSec2 = "//CLIENSEC2";
  static final String mcteParam_Nivel3 = "//NIVEL3";
  static final String mcteParam_ClienSec3 = "//CLIENSEC3";
  static final String mcteParam_Continuar = "//CONTINUAR";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarResultTR = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarNivelAs = "";
    String wvarClienSecAs = "";
    String wvarNivel1 = "";
    String wvarClienSec1 = "";
    String wvarNivel2 = "";
    String wvarClienSec2 = "";
    String wvarNivel3 = "";
    String wvarClienSec3 = "";
    String wvarContinuar = "";
    Variant wvarPos = new Variant();
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //Deber� venir desde la p�gina
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarNivelAs = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NivelAs )  ) + Strings.space( 2 ), 2 );
      wvarClienSecAs = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSecAs )  ), 9 );
      wvarNivel1 = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Nivel1 )  ) + Strings.space( 2 ), 2 );
      wvarClienSec1 = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec1 )  ), 9 );
      wvarNivel2 = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Nivel2 )  ) + Strings.space( 2 ), 2 );
      wvarClienSec2 = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec2 )  ), 9 );
      wvarNivel3 = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Nivel3 )  ) + Strings.space( 2 ), 2 );
      wvarClienSec3 = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec3 )  ), 9 );
      wvarContinuar = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Continuar )  );
      //
      //
      wvarStep = 15;
      if( Strings.trim( wvarContinuar ).equals( "" ) )
      {
        wvarContinuar = "00000000000000    0000000000000000000000";
      }
      //
      wvarStep = 20;
      wobjXMLRequest = null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los Parametros de la cola de MQ del archivo de configuraci�n
      wvarStep = 60;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      wobjXMLParametros = null;
      //
      wvarStep = 80;
      //
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + wvarNivel1 + wvarClienSec1 + wvarNivel2 + wvarClienSec2 + wvarNivel3 + wvarClienSec3 + wvarContinuar;
      wvarResult = "";
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos.set( 107 );
      //
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 36 ) );
      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();

      //
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 210;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 220;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        if( wvarEstado.equals( "TR" ) )
        {
          //
          wvarStep = 230;
          //Cargo las variables para llamar a un segundo mensaje de ser necesario
          wvarResultTR = wvarResultTR + "<CONTINUAR>" + Strings.mid( strParseString, 67, 40 ) + "</CONTINUAR>";
          //
        }
        //
        wvarStep = 240;
        wvarResult = "";
        wvarResultTR = wvarResultTR + "<MSGEST>" + wvarEstado + "</MSGEST>";
        wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        //
        wvarStep = 250;
        wobjXMLResponse = new XmlDomExtended();
        wobjXSLResponse = new XmlDomExtended();
        //
        wvarStep = 260;
        wobjXMLResponse.loadXML( wvarResult );
        //
        wvarStep = 270;
        if( wobjXMLResponse.selectNodes( "//R" ) .getLength() != 0 )
        {
          wobjXSLResponse.loadXML( p_GetXSL());
          //
          wvarStep = 280;
          wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
          //
          wvarStep = 290;
          wobjXMLResponse = null;
          wobjXSLResponse = null;
          //
          wvarStep = 300;
          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResultTR + wvarResult + "</Response>" );
          //
        }
        else
        {
          //
          wvarStep = 310;
          wobjXMLResponse = null;
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          //
        }
      }
      //
      wvarStep = 320;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());
      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
      try 
       {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarNivelAs + wvarClienSecAs + wvarNivel1 + wvarClienSec1 + wvarNivel2 + wvarClienSec2 + wvarNivel3 + wvarClienSec3 + wvarContinuar + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    //
    wvarResult = wvarResult + "<RS>";
    //
    while( (wvarPos.toInt() < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 1 ) ).toInt(), 4 ) ).equals( "" )) )
    {
      wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 92 ) + "]]></R>";
      wvarPos.set( wvarPos.add( new Variant( 92 ) ) );
    }
    //
    wvarResult = wvarResult + "</RS>";
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REGS'>";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REG'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RAMO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,1,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='PROD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,2,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,6,8)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERPOL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,14,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERANN'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,18,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERSEC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,22,6)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='FECANU'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,28,10)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='MON'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,38,3))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SIG'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,41,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='normalize-space(substring(.,41,1))' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMPANU'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,42,14)) div 100), \"###.###.##0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CLIDES'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,56,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,86,2)' />-<xsl:value-of select='substring(.,88,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SINI'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,92,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='FECENV'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='MON'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='SIG'/>";

    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    //
    return p_GetXSL;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
