
package com.qbe.segurosmobile;

import javax.jws.WebService;

@WebService(endpointInterface = "org.apache.cxf.HelloWorld")
public class HelloWorldImpl implements HelloWorld {

    public String sayHi(String text) {
        return "Hello " + text;
    }
}

