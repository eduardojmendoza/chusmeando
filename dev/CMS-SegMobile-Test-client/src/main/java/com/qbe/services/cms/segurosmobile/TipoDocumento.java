
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TipoDocumento.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TipoDocumento">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DNI"/>
 *     &lt;enumeration value="LE"/>
 *     &lt;enumeration value="LC"/>
 *     &lt;enumeration value="CUIT"/>
 *     &lt;enumeration value="CUIL"/>
 *     &lt;enumeration value="CI"/>
 *     &lt;enumeration value="PASAPORTE"/>
 *     &lt;enumeration value="OTRO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TipoDocumento")
@XmlEnum
public enum TipoDocumento {

    DNI,
    LE,
    LC,
    CUIT,
    CUIL,
    CI,
    PASAPORTE,
    OTRO;

    public String value() {
        return name();
    }

    public static TipoDocumento fromValue(String v) {
        return valueOf(v);
    }

}
