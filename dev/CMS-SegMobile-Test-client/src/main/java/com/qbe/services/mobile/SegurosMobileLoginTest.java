
package com.qbe.services.mobile;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;

import com.qbe.services.cms.segurosmobile.DataIngreso;
import com.qbe.services.cms.segurosmobile.DataIngresoPaso1;
import com.qbe.services.cms.segurosmobile.DataIngresoPaso2;
import com.qbe.services.cms.segurosmobile.DataInicio;
import com.qbe.services.cms.segurosmobile.DetallePoliza;
import com.qbe.services.cms.segurosmobile.Digito;
import com.qbe.services.cms.segurosmobile.DigitoFaltante;
import com.qbe.services.cms.segurosmobile.Digitos;
import com.qbe.services.cms.segurosmobile.Documento;
import com.qbe.services.cms.segurosmobile.EnviarCertificadosPorMailLog;
import com.qbe.services.cms.segurosmobile.IdPoliza;
import com.qbe.services.cms.segurosmobile.PreguntasSeguridad;
import com.qbe.services.cms.segurosmobile.RespuestaBasica;
import com.qbe.services.cms.segurosmobile.SegurosMobileService;
import com.qbe.services.cms.segurosmobile.SegurosMobileServiceException_Exception;
import com.qbe.services.cms.segurosmobile.SucursalesType;
import com.qbe.services.cms.segurosmobile.TipoCertificado;
import com.qbe.services.cms.segurosmobile.TipoDocumento;
import com.qbe.services.cms.segurosmobile.TiposDocumentoType;
import com.qbe.services.cms.segurosmobile.Vencimiento;
import com.qbe.services.cms.segurosmobile.VencimientoRegistroReminder;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * 
 * 
 */
public final class SegurosMobileLoginTest {

    private static final QName SERVICE_NAME = new QName("http://mobile.services.qbe.com/", "SegurosMobileServiceImplService");

    private SegurosMobileLoginTest() {
    }

    public static void main(String args[]) throws java.lang.Exception {
        URL wsdlURL = SegurosMobileServiceImplService.WSDL_LOCATION;
        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
            File wsdlFile = new File(args[0]);
            try {
                if (wsdlFile.exists()) {
                    wsdlURL = wsdlFile.toURI().toURL();
                } else {
                    wsdlURL = new URL(args[0]);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
      
        SegurosMobileServiceImplService ss = new SegurosMobileServiceImplService(wsdlURL, SERVICE_NAME);
        SegurosMobileService port = ss.getSegurosMobileServiceImplPort();  
        
        System.out.println("Invoking ingresarPaso1...");
        String usuario = "roberto_ferraris@hotmail.com";
        String password = "FERRARIS1955";
        String ipOrigen = "8.8.8.8";
        com.qbe.services.cms.segurosmobile.DataIngresoPaso1 paso1Response = port.ingresarPaso1(usuario, password, ipOrigen);
        System.out.println("ingresarPaso1.result=" + paso1Response);

        if ( paso1Response == null || ( !paso1Response.getCodError().equals("0")) || ( !paso1Response.getCodResultado().equals("OK"))) {
        	throw new RuntimeException("Respuesta paso 1 inválida");
        }
        int[] dni = {1, 1, 7, 8, 5, 3, 4, 5};
        
        com.qbe.services.cms.segurosmobile.DocumentoIncompleto.Digitos digitosFaltantes = paso1Response.getDocumentoIncompleto().getDigitos();
        Digitos digitosCompletos = new Digitos();
         
        DigitoFaltante df1 = digitosFaltantes.getDigito().get(0);
        Digito d1 = new Digito();
        d1.setId(df1.getId());
        d1.setValor(dni[df1.getPos() - 1]);
        digitosCompletos.getDigito().add(d1);
        
        DigitoFaltante df2 = digitosFaltantes.getDigito().get(1);
        Digito d2 = new Digito();
        d2.setId(df2.getId());
        d2.setValor(dni[df2.getPos() - 1]);
        digitosCompletos.getDigito().add(d2);

        DigitoFaltante df3 = digitosFaltantes.getDigito().get(2);
        Digito d3 = new Digito();
        d3.setId(df3.getId());
        d3.setValor(dni[df3.getPos() - 1]);
        digitosCompletos.getDigito().add(d3);

        System.out.println("Invoking ingresarPaso2...");

        com.qbe.services.cms.segurosmobile.TipoDocumento _ingresarPaso2_tipoDocumento = TipoDocumento.DNI;
        com.qbe.services.cms.segurosmobile.DataIngresoPaso2 _ingresarPaso2__return = port.ingresarPaso2(usuario, password, ipOrigen, _ingresarPaso2_tipoDocumento, digitosCompletos);
        System.out.println("ingresarPaso2.result=" + _ingresarPaso2__return);
        
        

    }

}
