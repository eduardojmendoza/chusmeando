
package com.qbe.services.cms.segurosmobile;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DataInicio complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DataInicio">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="productos" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="producto" type="{http://cms.services.qbe.com/segurosmobile}Producto" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="usuarioInicio" type="{http://cms.services.qbe.com/segurosmobile}UsuarioInicio"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataInicio", propOrder = {
    "productos",
    "usuarioInicio"
})
public class DataInicio {

    protected DataInicio.Productos productos;
    @XmlElement(required = true, nillable = true)
    protected UsuarioInicio usuarioInicio;

    /**
     * Gets the value of the productos property.
     * 
     * @return
     *     possible object is
     *     {@link DataInicio.Productos }
     *     
     */
    public DataInicio.Productos getProductos() {
        return productos;
    }

    /**
     * Sets the value of the productos property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataInicio.Productos }
     *     
     */
    public void setProductos(DataInicio.Productos value) {
        this.productos = value;
    }

    /**
     * Gets the value of the usuarioInicio property.
     * 
     * @return
     *     possible object is
     *     {@link UsuarioInicio }
     *     
     */
    public UsuarioInicio getUsuarioInicio() {
        return usuarioInicio;
    }

    /**
     * Sets the value of the usuarioInicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link UsuarioInicio }
     *     
     */
    public void setUsuarioInicio(UsuarioInicio value) {
        this.usuarioInicio = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="producto" type="{http://cms.services.qbe.com/segurosmobile}Producto" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "producto"
    })
    public static class Productos {

        @XmlElement(required = true, nillable = true)
        protected List<Producto> producto;

        /**
         * Gets the value of the producto property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the producto property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProducto().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Producto }
         * 
         * 
         */
        public List<Producto> getProducto() {
            if (producto == null) {
                producto = new ArrayList<Producto>();
            }
            return this.producto;
        }

    }

}
