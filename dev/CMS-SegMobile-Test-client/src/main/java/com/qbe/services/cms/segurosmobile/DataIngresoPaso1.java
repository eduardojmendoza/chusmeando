
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DataIngresoPaso1 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DataIngresoPaso1">
 *   &lt;complexContent>
 *     &lt;extension base="{http://cms.services.qbe.com/segurosmobile}RespuestaBasica">
 *       &lt;sequence>
 *         &lt;element name="documentoIncompleto" type="{http://cms.services.qbe.com/segurosmobile}DocumentoIncompleto"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataIngresoPaso1", propOrder = {
    "documentoIncompleto"
})
public class DataIngresoPaso1
    extends RespuestaBasica
{

    @XmlElement(required = true)
    protected DocumentoIncompleto documentoIncompleto;

    /**
     * Gets the value of the documentoIncompleto property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoIncompleto }
     *     
     */
    public DocumentoIncompleto getDocumentoIncompleto() {
        return documentoIncompleto;
    }

    /**
     * Sets the value of the documentoIncompleto property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoIncompleto }
     *     
     */
    public void setDocumentoIncompleto(DocumentoIncompleto value) {
        this.documentoIncompleto = value;
    }

}
