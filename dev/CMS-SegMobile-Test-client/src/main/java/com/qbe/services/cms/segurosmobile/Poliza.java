
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Poliza complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Poliza">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cobrodes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numeroMotor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="patente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoPoliza" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="textoPoliza" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Poliza", propOrder = {
    "cobrodes",
    "numeroMotor",
    "patente",
    "pid",
    "codigoPoliza",
    "textoPoliza"
})
public class Poliza {

    @XmlElement(required = true, nillable = true)
    protected String cobrodes;
    @XmlElement(required = true, nillable = true)
    protected String numeroMotor;
    @XmlElement(required = true, nillable = true)
    protected String patente;
    @XmlElement(required = true, nillable = true)
    protected String pid;
    @XmlElement(required = true, nillable = true)
    protected String codigoPoliza;
    @XmlElement(required = true, nillable = true)
    protected String textoPoliza;

    /**
     * Gets the value of the cobrodes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCobrodes() {
        return cobrodes;
    }

    /**
     * Sets the value of the cobrodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCobrodes(String value) {
        this.cobrodes = value;
    }

    /**
     * Gets the value of the numeroMotor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroMotor() {
        return numeroMotor;
    }

    /**
     * Sets the value of the numeroMotor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroMotor(String value) {
        this.numeroMotor = value;
    }

    /**
     * Gets the value of the patente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatente() {
        return patente;
    }

    /**
     * Sets the value of the patente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatente(String value) {
        this.patente = value;
    }

    /**
     * Gets the value of the pid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPid() {
        return pid;
    }

    /**
     * Sets the value of the pid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPid(String value) {
        this.pid = value;
    }

    /**
     * Gets the value of the codigoPoliza property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPoliza() {
        return codigoPoliza;
    }

    /**
     * Sets the value of the codigoPoliza property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPoliza(String value) {
        this.codigoPoliza = value;
    }

    /**
     * Gets the value of the textoPoliza property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextoPoliza() {
        return textoPoliza;
    }

    /**
     * Sets the value of the textoPoliza property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextoPoliza(String value) {
        this.textoPoliza = value;
    }

}
