
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RespuestaBasica complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RespuestaBasica">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codResultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codError" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="msgResultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RespuestaBasica", propOrder = {
    "codResultado",
    "codError",
    "msgResultado"
})
@XmlSeeAlso({
    DataIngresoPaso1 .class
})
public class RespuestaBasica {

    @XmlElement(required = true)
    protected String codResultado;
    @XmlElement(required = true, nillable = true)
    protected String codError;
    @XmlElement(required = true, nillable = true)
    protected String msgResultado;

    /**
     * Gets the value of the codResultado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodResultado() {
        return codResultado;
    }

    /**
     * Sets the value of the codResultado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodResultado(String value) {
        this.codResultado = value;
    }

    /**
     * Gets the value of the codError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodError() {
        return codError;
    }

    /**
     * Sets the value of the codError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodError(String value) {
        this.codError = value;
    }

    /**
     * Gets the value of the msgResultado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgResultado() {
        return msgResultado;
    }

    /**
     * Sets the value of the msgResultado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgResultado(String value) {
        this.msgResultado = value;
    }

}
