
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.qbe.services.cms.segurosmobile package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SetVencimientoRegistroReminderResponse_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "setVencimientoRegistroReminderResponse");
    private final static QName _EnviarCertificadosPorMail_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "enviarCertificadosPorMail");
    private final static QName _IngresoErroneo_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "IngresoErroneo");
    private final static QName _Registrar_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "registrar");
    private final static QName _GetSucursalesResponse_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "getSucursalesResponse");
    private final static QName _DataIngresoPaso1_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "DataIngresoPaso1");
    private final static QName _IngresoActivo_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "IngresoActivo");
    private final static QName _GetSucursales_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "getSucursales");
    private final static QName _SetVencimientoRegistroReminder_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "setVencimientoRegistroReminder");
    private final static QName _GetTiposDocumentoResponse_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "getTiposDocumentoResponse");
    private final static QName _IngresarPaso1Response_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "ingresarPaso1Response");
    private final static QName _GetVencimientoRegistroReminder_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "getVencimientoRegistroReminder");
    private final static QName _ProductosClienteResponse_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "productosClienteResponse");
    private final static QName _SendVencimientoRegistroMails_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "sendVencimientoRegistroMails");
    private final static QName _PrimerIngresoResponse_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "primerIngresoResponse");
    private final static QName _EnviarCertificadosPorMailResponse_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "enviarCertificadosPorMailResponse");
    private final static QName _DataIngresoPaso2_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "dataIngresoPaso2");
    private final static QName _EnviarCertificadosPorMailLog_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "EnviarCertificadosPorMailLog");
    private final static QName _DataInicio_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "DataInicio");
    private final static QName _SendVencimientoRegistroMailsResponse_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "sendVencimientoRegistroMailsResponse");
    private final static QName _GetTiposDocumento_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "getTiposDocumento");
    private final static QName _IngresarPaso1_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "ingresarPaso1");
    private final static QName _IngresarPaso2_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "ingresarPaso2");
    private final static QName _SegurosMobileServiceException_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "SegurosMobileServiceException");
    private final static QName _IngresarPaso2Response_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "ingresarPaso2Response");
    private final static QName _UsuarioInicio_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "UsuarioInicio");
    private final static QName _GetVencimientoRegistroReminderResponse_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "getVencimientoRegistroReminderResponse");
    private final static QName _GetDetallesPoliza_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "getDetallesPoliza");
    private final static QName _GetPreguntasResponse_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "getPreguntasResponse");
    private final static QName _GetDetallesPolizaResponse_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "getDetallesPolizaResponse");
    private final static QName _GetPreguntas_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "getPreguntas");
    private final static QName _PrimerIngreso_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "primerIngreso");
    private final static QName _RegistrarResponse_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "registrarResponse");
    private final static QName _ProductosCliente_QNAME = new QName("http://cms.services.qbe.com/segurosmobile", "productosCliente");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.qbe.services.cms.segurosmobile
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TiposDocumentoType }
     * 
     */
    public TiposDocumentoType createTiposDocumentoType() {
        return new TiposDocumentoType();
    }

    /**
     * Create an instance of {@link PreguntasSeguridad }
     * 
     */
    public PreguntasSeguridad createPreguntasSeguridad() {
        return new PreguntasSeguridad();
    }

    /**
     * Create an instance of {@link DataIngresoPaso2PrimerIngreso }
     * 
     */
    public DataIngresoPaso2PrimerIngreso createDataIngresoPaso2PrimerIngreso() {
        return new DataIngresoPaso2PrimerIngreso();
    }

    /**
     * Create an instance of {@link DocumentoIncompleto }
     * 
     */
    public DocumentoIncompleto createDocumentoIncompleto() {
        return new DocumentoIncompleto();
    }

    /**
     * Create an instance of {@link SucursalesType }
     * 
     */
    public SucursalesType createSucursalesType() {
        return new SucursalesType();
    }

    /**
     * Create an instance of {@link DataInicio }
     * 
     */
    public DataInicio createDataInicio() {
        return new DataInicio();
    }

    /**
     * Create an instance of {@link EnviarCertificadosPorMailResponse }
     * 
     */
    public EnviarCertificadosPorMailResponse createEnviarCertificadosPorMailResponse() {
        return new EnviarCertificadosPorMailResponse();
    }

    /**
     * Create an instance of {@link EnviarCertificadosPorMailLog }
     * 
     */
    public EnviarCertificadosPorMailLog createEnviarCertificadosPorMailLog() {
        return new EnviarCertificadosPorMailLog();
    }

    /**
     * Create an instance of {@link GetTiposDocumento }
     * 
     */
    public GetTiposDocumento createGetTiposDocumento() {
        return new GetTiposDocumento();
    }

    /**
     * Create an instance of {@link SendVencimientoRegistroMailsResponse }
     * 
     */
    public SendVencimientoRegistroMailsResponse createSendVencimientoRegistroMailsResponse() {
        return new SendVencimientoRegistroMailsResponse();
    }

    /**
     * Create an instance of {@link SegurosMobileServiceException }
     * 
     */
    public SegurosMobileServiceException createSegurosMobileServiceException() {
        return new SegurosMobileServiceException();
    }

    /**
     * Create an instance of {@link IngresarPaso2 }
     * 
     */
    public IngresarPaso2 createIngresarPaso2() {
        return new IngresarPaso2();
    }

    /**
     * Create an instance of {@link IngresarPaso1 }
     * 
     */
    public IngresarPaso1 createIngresarPaso1() {
        return new IngresarPaso1();
    }

    /**
     * Create an instance of {@link IngresarPaso2Response }
     * 
     */
    public IngresarPaso2Response createIngresarPaso2Response() {
        return new IngresarPaso2Response();
    }

    /**
     * Create an instance of {@link UsuarioInicio }
     * 
     */
    public UsuarioInicio createUsuarioInicio() {
        return new UsuarioInicio();
    }

    /**
     * Create an instance of {@link GetDetallesPoliza }
     * 
     */
    public GetDetallesPoliza createGetDetallesPoliza() {
        return new GetDetallesPoliza();
    }

    /**
     * Create an instance of {@link GetVencimientoRegistroReminderResponse }
     * 
     */
    public GetVencimientoRegistroReminderResponse createGetVencimientoRegistroReminderResponse() {
        return new GetVencimientoRegistroReminderResponse();
    }

    /**
     * Create an instance of {@link PrimerIngreso }
     * 
     */
    public PrimerIngreso createPrimerIngreso() {
        return new PrimerIngreso();
    }

    /**
     * Create an instance of {@link GetPreguntasResponse }
     * 
     */
    public GetPreguntasResponse createGetPreguntasResponse() {
        return new GetPreguntasResponse();
    }

    /**
     * Create an instance of {@link GetDetallesPolizaResponse }
     * 
     */
    public GetDetallesPolizaResponse createGetDetallesPolizaResponse() {
        return new GetDetallesPolizaResponse();
    }

    /**
     * Create an instance of {@link GetPreguntas }
     * 
     */
    public GetPreguntas createGetPreguntas() {
        return new GetPreguntas();
    }

    /**
     * Create an instance of {@link RegistrarResponse }
     * 
     */
    public RegistrarResponse createRegistrarResponse() {
        return new RegistrarResponse();
    }

    /**
     * Create an instance of {@link ProductosCliente }
     * 
     */
    public ProductosCliente createProductosCliente() {
        return new ProductosCliente();
    }

    /**
     * Create an instance of {@link SetVencimientoRegistroReminderResponse }
     * 
     */
    public SetVencimientoRegistroReminderResponse createSetVencimientoRegistroReminderResponse() {
        return new SetVencimientoRegistroReminderResponse();
    }

    /**
     * Create an instance of {@link EnviarCertificadosPorMail }
     * 
     */
    public EnviarCertificadosPorMail createEnviarCertificadosPorMail() {
        return new EnviarCertificadosPorMail();
    }

    /**
     * Create an instance of {@link DataIngresoPaso2IngresoErroneo }
     * 
     */
    public DataIngresoPaso2IngresoErroneo createDataIngresoPaso2IngresoErroneo() {
        return new DataIngresoPaso2IngresoErroneo();
    }

    /**
     * Create an instance of {@link Registrar }
     * 
     */
    public Registrar createRegistrar() {
        return new Registrar();
    }

    /**
     * Create an instance of {@link GetSucursalesResponse }
     * 
     */
    public GetSucursalesResponse createGetSucursalesResponse() {
        return new GetSucursalesResponse();
    }

    /**
     * Create an instance of {@link DataIngresoPaso2IngresoActivo }
     * 
     */
    public DataIngresoPaso2IngresoActivo createDataIngresoPaso2IngresoActivo() {
        return new DataIngresoPaso2IngresoActivo();
    }

    /**
     * Create an instance of {@link DataIngresoPaso1 }
     * 
     */
    public DataIngresoPaso1 createDataIngresoPaso1() {
        return new DataIngresoPaso1();
    }

    /**
     * Create an instance of {@link GetTiposDocumentoResponse }
     * 
     */
    public GetTiposDocumentoResponse createGetTiposDocumentoResponse() {
        return new GetTiposDocumentoResponse();
    }

    /**
     * Create an instance of {@link IngresarPaso1Response }
     * 
     */
    public IngresarPaso1Response createIngresarPaso1Response() {
        return new IngresarPaso1Response();
    }

    /**
     * Create an instance of {@link GetSucursales }
     * 
     */
    public GetSucursales createGetSucursales() {
        return new GetSucursales();
    }

    /**
     * Create an instance of {@link SetVencimientoRegistroReminder }
     * 
     */
    public SetVencimientoRegistroReminder createSetVencimientoRegistroReminder() {
        return new SetVencimientoRegistroReminder();
    }

    /**
     * Create an instance of {@link GetVencimientoRegistroReminder }
     * 
     */
    public GetVencimientoRegistroReminder createGetVencimientoRegistroReminder() {
        return new GetVencimientoRegistroReminder();
    }

    /**
     * Create an instance of {@link ProductosClienteResponse }
     * 
     */
    public ProductosClienteResponse createProductosClienteResponse() {
        return new ProductosClienteResponse();
    }

    /**
     * Create an instance of {@link PrimerIngresoResponse }
     * 
     */
    public PrimerIngresoResponse createPrimerIngresoResponse() {
        return new PrimerIngresoResponse();
    }

    /**
     * Create an instance of {@link SendVencimientoRegistroMails }
     * 
     */
    public SendVencimientoRegistroMails createSendVencimientoRegistroMails() {
        return new SendVencimientoRegistroMails();
    }

    /**
     * Create an instance of {@link DigitoFaltante }
     * 
     */
    public DigitoFaltante createDigitoFaltante() {
        return new DigitoFaltante();
    }

    /**
     * Create an instance of {@link Producto }
     * 
     */
    public Producto createProducto() {
        return new Producto();
    }

    /**
     * Create an instance of {@link Vencimiento }
     * 
     */
    public Vencimiento createVencimiento() {
        return new Vencimiento();
    }

    /**
     * Create an instance of {@link com.qbe.services.cms.segurosmobile.Digitos }
     * 
     */
    public com.qbe.services.cms.segurosmobile.Digitos createDigitos() {
        return new com.qbe.services.cms.segurosmobile.Digitos();
    }

    /**
     * Create an instance of {@link DataIngreso }
     * 
     */
    public DataIngreso createDataIngreso() {
        return new DataIngreso();
    }

    /**
     * Create an instance of {@link RespuestaBasica }
     * 
     */
    public RespuestaBasica createRespuestaBasica() {
        return new RespuestaBasica();
    }

    /**
     * Create an instance of {@link Digito }
     * 
     */
    public Digito createDigito() {
        return new Digito();
    }

    /**
     * Create an instance of {@link Pregunta }
     * 
     */
    public Pregunta createPregunta() {
        return new Pregunta();
    }

    /**
     * Create an instance of {@link Coberturas }
     * 
     */
    public Coberturas createCoberturas() {
        return new Coberturas();
    }

    /**
     * Create an instance of {@link VencimientoRegistroReminder }
     * 
     */
    public VencimientoRegistroReminder createVencimientoRegistroReminder() {
        return new VencimientoRegistroReminder();
    }

    /**
     * Create an instance of {@link Documento }
     * 
     */
    public Documento createDocumento() {
        return new Documento();
    }

    /**
     * Create an instance of {@link Poliza }
     * 
     */
    public Poliza createPoliza() {
        return new Poliza();
    }

    /**
     * Create an instance of {@link Cliente }
     * 
     */
    public Cliente createCliente() {
        return new Cliente();
    }

    /**
     * Create an instance of {@link Sucursal }
     * 
     */
    public Sucursal createSucursal() {
        return new Sucursal();
    }

    /**
     * Create an instance of {@link DetallePoliza }
     * 
     */
    public DetallePoliza createDetallePoliza() {
        return new DetallePoliza();
    }

    /**
     * Create an instance of {@link Riesgo }
     * 
     */
    public Riesgo createRiesgo() {
        return new Riesgo();
    }

    /**
     * Create an instance of {@link IdPoliza }
     * 
     */
    public IdPoliza createIdPoliza() {
        return new IdPoliza();
    }

    /**
     * Create an instance of {@link TiposDocumentoType.TiposDocumento }
     * 
     */
    public TiposDocumentoType.TiposDocumento createTiposDocumentoTypeTiposDocumento() {
        return new TiposDocumentoType.TiposDocumento();
    }

    /**
     * Create an instance of {@link PreguntasSeguridad.Preguntas }
     * 
     */
    public PreguntasSeguridad.Preguntas createPreguntasSeguridadPreguntas() {
        return new PreguntasSeguridad.Preguntas();
    }

    /**
     * Create an instance of {@link DataIngresoPaso2PrimerIngreso.Polizas }
     * 
     */
    public DataIngresoPaso2PrimerIngreso.Polizas createDataIngresoPaso2PrimerIngresoPolizas() {
        return new DataIngresoPaso2PrimerIngreso.Polizas();
    }

    /**
     * Create an instance of {@link DocumentoIncompleto.Digitos }
     * 
     */
    public DocumentoIncompleto.Digitos createDocumentoIncompletoDigitos() {
        return new DocumentoIncompleto.Digitos();
    }

    /**
     * Create an instance of {@link SucursalesType.Sucursales }
     * 
     */
    public SucursalesType.Sucursales createSucursalesTypeSucursales() {
        return new SucursalesType.Sucursales();
    }

    /**
     * Create an instance of {@link DataInicio.Productos }
     * 
     */
    public DataInicio.Productos createDataInicioProductos() {
        return new DataInicio.Productos();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetVencimientoRegistroReminderResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "setVencimientoRegistroReminderResponse")
    public JAXBElement<SetVencimientoRegistroReminderResponse> createSetVencimientoRegistroReminderResponse(SetVencimientoRegistroReminderResponse value) {
        return new JAXBElement<SetVencimientoRegistroReminderResponse>(_SetVencimientoRegistroReminderResponse_QNAME, SetVencimientoRegistroReminderResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarCertificadosPorMail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "enviarCertificadosPorMail")
    public JAXBElement<EnviarCertificadosPorMail> createEnviarCertificadosPorMail(EnviarCertificadosPorMail value) {
        return new JAXBElement<EnviarCertificadosPorMail>(_EnviarCertificadosPorMail_QNAME, EnviarCertificadosPorMail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataIngresoPaso2IngresoErroneo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "IngresoErroneo")
    public JAXBElement<DataIngresoPaso2IngresoErroneo> createIngresoErroneo(DataIngresoPaso2IngresoErroneo value) {
        return new JAXBElement<DataIngresoPaso2IngresoErroneo>(_IngresoErroneo_QNAME, DataIngresoPaso2IngresoErroneo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Registrar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "registrar")
    public JAXBElement<Registrar> createRegistrar(Registrar value) {
        return new JAXBElement<Registrar>(_Registrar_QNAME, Registrar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSucursalesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "getSucursalesResponse")
    public JAXBElement<GetSucursalesResponse> createGetSucursalesResponse(GetSucursalesResponse value) {
        return new JAXBElement<GetSucursalesResponse>(_GetSucursalesResponse_QNAME, GetSucursalesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataIngresoPaso1 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "DataIngresoPaso1")
    public JAXBElement<DataIngresoPaso1> createDataIngresoPaso1(DataIngresoPaso1 value) {
        return new JAXBElement<DataIngresoPaso1>(_DataIngresoPaso1_QNAME, DataIngresoPaso1 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataIngresoPaso2IngresoActivo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "IngresoActivo")
    public JAXBElement<DataIngresoPaso2IngresoActivo> createIngresoActivo(DataIngresoPaso2IngresoActivo value) {
        return new JAXBElement<DataIngresoPaso2IngresoActivo>(_IngresoActivo_QNAME, DataIngresoPaso2IngresoActivo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSucursales }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "getSucursales")
    public JAXBElement<GetSucursales> createGetSucursales(GetSucursales value) {
        return new JAXBElement<GetSucursales>(_GetSucursales_QNAME, GetSucursales.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetVencimientoRegistroReminder }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "setVencimientoRegistroReminder")
    public JAXBElement<SetVencimientoRegistroReminder> createSetVencimientoRegistroReminder(SetVencimientoRegistroReminder value) {
        return new JAXBElement<SetVencimientoRegistroReminder>(_SetVencimientoRegistroReminder_QNAME, SetVencimientoRegistroReminder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTiposDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "getTiposDocumentoResponse")
    public JAXBElement<GetTiposDocumentoResponse> createGetTiposDocumentoResponse(GetTiposDocumentoResponse value) {
        return new JAXBElement<GetTiposDocumentoResponse>(_GetTiposDocumentoResponse_QNAME, GetTiposDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarPaso1Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "ingresarPaso1Response")
    public JAXBElement<IngresarPaso1Response> createIngresarPaso1Response(IngresarPaso1Response value) {
        return new JAXBElement<IngresarPaso1Response>(_IngresarPaso1Response_QNAME, IngresarPaso1Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVencimientoRegistroReminder }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "getVencimientoRegistroReminder")
    public JAXBElement<GetVencimientoRegistroReminder> createGetVencimientoRegistroReminder(GetVencimientoRegistroReminder value) {
        return new JAXBElement<GetVencimientoRegistroReminder>(_GetVencimientoRegistroReminder_QNAME, GetVencimientoRegistroReminder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductosClienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "productosClienteResponse")
    public JAXBElement<ProductosClienteResponse> createProductosClienteResponse(ProductosClienteResponse value) {
        return new JAXBElement<ProductosClienteResponse>(_ProductosClienteResponse_QNAME, ProductosClienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendVencimientoRegistroMails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "sendVencimientoRegistroMails")
    public JAXBElement<SendVencimientoRegistroMails> createSendVencimientoRegistroMails(SendVencimientoRegistroMails value) {
        return new JAXBElement<SendVencimientoRegistroMails>(_SendVencimientoRegistroMails_QNAME, SendVencimientoRegistroMails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrimerIngresoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "primerIngresoResponse")
    public JAXBElement<PrimerIngresoResponse> createPrimerIngresoResponse(PrimerIngresoResponse value) {
        return new JAXBElement<PrimerIngresoResponse>(_PrimerIngresoResponse_QNAME, PrimerIngresoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarCertificadosPorMailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "enviarCertificadosPorMailResponse")
    public JAXBElement<EnviarCertificadosPorMailResponse> createEnviarCertificadosPorMailResponse(EnviarCertificadosPorMailResponse value) {
        return new JAXBElement<EnviarCertificadosPorMailResponse>(_EnviarCertificadosPorMailResponse_QNAME, EnviarCertificadosPorMailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataIngresoPaso2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "dataIngresoPaso2")
    public JAXBElement<DataIngresoPaso2> createDataIngresoPaso2(DataIngresoPaso2 value) {
        return new JAXBElement<DataIngresoPaso2>(_DataIngresoPaso2_QNAME, DataIngresoPaso2 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarCertificadosPorMailLog }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "EnviarCertificadosPorMailLog")
    public JAXBElement<EnviarCertificadosPorMailLog> createEnviarCertificadosPorMailLog(EnviarCertificadosPorMailLog value) {
        return new JAXBElement<EnviarCertificadosPorMailLog>(_EnviarCertificadosPorMailLog_QNAME, EnviarCertificadosPorMailLog.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataInicio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "DataInicio")
    public JAXBElement<DataInicio> createDataInicio(DataInicio value) {
        return new JAXBElement<DataInicio>(_DataInicio_QNAME, DataInicio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendVencimientoRegistroMailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "sendVencimientoRegistroMailsResponse")
    public JAXBElement<SendVencimientoRegistroMailsResponse> createSendVencimientoRegistroMailsResponse(SendVencimientoRegistroMailsResponse value) {
        return new JAXBElement<SendVencimientoRegistroMailsResponse>(_SendVencimientoRegistroMailsResponse_QNAME, SendVencimientoRegistroMailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTiposDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "getTiposDocumento")
    public JAXBElement<GetTiposDocumento> createGetTiposDocumento(GetTiposDocumento value) {
        return new JAXBElement<GetTiposDocumento>(_GetTiposDocumento_QNAME, GetTiposDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarPaso1 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "ingresarPaso1")
    public JAXBElement<IngresarPaso1> createIngresarPaso1(IngresarPaso1 value) {
        return new JAXBElement<IngresarPaso1>(_IngresarPaso1_QNAME, IngresarPaso1 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarPaso2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "ingresarPaso2")
    public JAXBElement<IngresarPaso2> createIngresarPaso2(IngresarPaso2 value) {
        return new JAXBElement<IngresarPaso2>(_IngresarPaso2_QNAME, IngresarPaso2 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SegurosMobileServiceException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "SegurosMobileServiceException")
    public JAXBElement<SegurosMobileServiceException> createSegurosMobileServiceException(SegurosMobileServiceException value) {
        return new JAXBElement<SegurosMobileServiceException>(_SegurosMobileServiceException_QNAME, SegurosMobileServiceException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarPaso2Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "ingresarPaso2Response")
    public JAXBElement<IngresarPaso2Response> createIngresarPaso2Response(IngresarPaso2Response value) {
        return new JAXBElement<IngresarPaso2Response>(_IngresarPaso2Response_QNAME, IngresarPaso2Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UsuarioInicio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "UsuarioInicio")
    public JAXBElement<UsuarioInicio> createUsuarioInicio(UsuarioInicio value) {
        return new JAXBElement<UsuarioInicio>(_UsuarioInicio_QNAME, UsuarioInicio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVencimientoRegistroReminderResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "getVencimientoRegistroReminderResponse")
    public JAXBElement<GetVencimientoRegistroReminderResponse> createGetVencimientoRegistroReminderResponse(GetVencimientoRegistroReminderResponse value) {
        return new JAXBElement<GetVencimientoRegistroReminderResponse>(_GetVencimientoRegistroReminderResponse_QNAME, GetVencimientoRegistroReminderResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDetallesPoliza }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "getDetallesPoliza")
    public JAXBElement<GetDetallesPoliza> createGetDetallesPoliza(GetDetallesPoliza value) {
        return new JAXBElement<GetDetallesPoliza>(_GetDetallesPoliza_QNAME, GetDetallesPoliza.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPreguntasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "getPreguntasResponse")
    public JAXBElement<GetPreguntasResponse> createGetPreguntasResponse(GetPreguntasResponse value) {
        return new JAXBElement<GetPreguntasResponse>(_GetPreguntasResponse_QNAME, GetPreguntasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDetallesPolizaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "getDetallesPolizaResponse")
    public JAXBElement<GetDetallesPolizaResponse> createGetDetallesPolizaResponse(GetDetallesPolizaResponse value) {
        return new JAXBElement<GetDetallesPolizaResponse>(_GetDetallesPolizaResponse_QNAME, GetDetallesPolizaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPreguntas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "getPreguntas")
    public JAXBElement<GetPreguntas> createGetPreguntas(GetPreguntas value) {
        return new JAXBElement<GetPreguntas>(_GetPreguntas_QNAME, GetPreguntas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrimerIngreso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "primerIngreso")
    public JAXBElement<PrimerIngreso> createPrimerIngreso(PrimerIngreso value) {
        return new JAXBElement<PrimerIngreso>(_PrimerIngreso_QNAME, PrimerIngreso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "registrarResponse")
    public JAXBElement<RegistrarResponse> createRegistrarResponse(RegistrarResponse value) {
        return new JAXBElement<RegistrarResponse>(_RegistrarResponse_QNAME, RegistrarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductosCliente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/segurosmobile", name = "productosCliente")
    public JAXBElement<ProductosCliente> createProductosCliente(ProductosCliente value) {
        return new JAXBElement<ProductosCliente>(_ProductosCliente_QNAME, ProductosCliente.class, null, value);
    }

}
