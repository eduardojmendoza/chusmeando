
package com.qbe.services.cms.segurosmobile;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PreguntasSeguridad complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PreguntasSeguridad">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="preguntas" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="pregunta" type="{http://cms.services.qbe.com/segurosmobile}Pregunta" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PreguntasSeguridad", propOrder = {
    "preguntas"
})
public class PreguntasSeguridad {

    protected PreguntasSeguridad.Preguntas preguntas;

    /**
     * Gets the value of the preguntas property.
     * 
     * @return
     *     possible object is
     *     {@link PreguntasSeguridad.Preguntas }
     *     
     */
    public PreguntasSeguridad.Preguntas getPreguntas() {
        return preguntas;
    }

    /**
     * Sets the value of the preguntas property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreguntasSeguridad.Preguntas }
     *     
     */
    public void setPreguntas(PreguntasSeguridad.Preguntas value) {
        this.preguntas = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="pregunta" type="{http://cms.services.qbe.com/segurosmobile}Pregunta" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "pregunta"
    })
    public static class Preguntas {

        @XmlElement(required = true, nillable = true)
        protected List<Pregunta> pregunta;

        /**
         * Gets the value of the pregunta property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the pregunta property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPregunta().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Pregunta }
         * 
         * 
         */
        public List<Pregunta> getPregunta() {
            if (pregunta == null) {
                pregunta = new ArrayList<Pregunta>();
            }
            return this.pregunta;
        }

    }

}
