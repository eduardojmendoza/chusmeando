
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Coberturas complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Coberturas">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoPlan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descripcionPlan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sumaLBA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sumaAsegurada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Coberturas", propOrder = {
    "codigoPlan",
    "descripcionPlan",
    "sumaLBA",
    "sumaAsegurada"
})
public class Coberturas {

    protected String codigoPlan;
    protected String descripcionPlan;
    protected String sumaLBA;
    protected String sumaAsegurada;

    /**
     * Gets the value of the codigoPlan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPlan() {
        return codigoPlan;
    }

    /**
     * Sets the value of the codigoPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPlan(String value) {
        this.codigoPlan = value;
    }

    /**
     * Gets the value of the descripcionPlan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionPlan() {
        return descripcionPlan;
    }

    /**
     * Sets the value of the descripcionPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionPlan(String value) {
        this.descripcionPlan = value;
    }

    /**
     * Gets the value of the sumaLBA property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSumaLBA() {
        return sumaLBA;
    }

    /**
     * Sets the value of the sumaLBA property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSumaLBA(String value) {
        this.sumaLBA = value;
    }

    /**
     * Gets the value of the sumaAsegurada property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSumaAsegurada() {
        return sumaAsegurada;
    }

    /**
     * Sets the value of the sumaAsegurada property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSumaAsegurada(String value) {
        this.sumaAsegurada = value;
    }

}
