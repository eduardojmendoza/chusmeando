
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DetallePoliza complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DetallePoliza">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operpte" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cliensec" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cliendes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="domicsec" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cuentsec" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="situcpol" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="acreedor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fecini" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fecultre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vigenciaDesde" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vigenciaHasta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cobrocod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cobrodab" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cobrotip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cobrodes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cuentnum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="campacod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="campades" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="grupoase" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clubeco" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tarjecod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="optp6511" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="riesgo" type="{http://cms.services.qbe.com/segurosmobile}Riesgo" minOccurs="0"/>
 *         &lt;element name="coberturas" type="{http://cms.services.qbe.com/segurosmobile}Coberturas" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DetallePoliza", propOrder = {
    "operpte",
    "cliensec",
    "cliendes",
    "domicsec",
    "cuentsec",
    "situcpol",
    "acreedor",
    "fecini",
    "fecultre",
    "vigenciaDesde",
    "vigenciaHasta",
    "cobrocod",
    "cobrodab",
    "cobrotip",
    "cobrodes",
    "cuentnum",
    "campacod",
    "campades",
    "grupoase",
    "clubeco",
    "tarjecod",
    "optp6511",
    "riesgo",
    "coberturas"
})
public class DetallePoliza {

    @XmlElement(required = true, nillable = true)
    protected String operpte;
    @XmlElement(required = true, nillable = true)
    protected String cliensec;
    @XmlElement(required = true, nillable = true)
    protected String cliendes;
    @XmlElement(required = true, nillable = true)
    protected String domicsec;
    @XmlElement(required = true, nillable = true)
    protected String cuentsec;
    @XmlElement(required = true, nillable = true)
    protected String situcpol;
    @XmlElement(required = true, nillable = true)
    protected String acreedor;
    @XmlElement(required = true, nillable = true)
    protected String fecini;
    @XmlElement(required = true, nillable = true)
    protected String fecultre;
    @XmlElement(required = true, nillable = true)
    protected String vigenciaDesde;
    @XmlElement(required = true, nillable = true)
    protected String vigenciaHasta;
    @XmlElement(required = true, nillable = true)
    protected String cobrocod;
    @XmlElement(required = true, nillable = true)
    protected String cobrodab;
    @XmlElement(required = true, nillable = true)
    protected String cobrotip;
    @XmlElement(required = true, nillable = true)
    protected String cobrodes;
    @XmlElement(required = true, nillable = true)
    protected String cuentnum;
    @XmlElement(required = true, nillable = true)
    protected String campacod;
    @XmlElement(required = true, nillable = true)
    protected String campades;
    @XmlElement(required = true, nillable = true)
    protected String grupoase;
    @XmlElement(required = true, nillable = true)
    protected String clubeco;
    @XmlElement(required = true, nillable = true)
    protected String tarjecod;
    @XmlElement(required = true, nillable = true)
    protected String optp6511;
    protected Riesgo riesgo;
    protected Coberturas coberturas;

    /**
     * Gets the value of the operpte property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperpte() {
        return operpte;
    }

    /**
     * Sets the value of the operpte property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperpte(String value) {
        this.operpte = value;
    }

    /**
     * Gets the value of the cliensec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCliensec() {
        return cliensec;
    }

    /**
     * Sets the value of the cliensec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCliensec(String value) {
        this.cliensec = value;
    }

    /**
     * Gets the value of the cliendes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCliendes() {
        return cliendes;
    }

    /**
     * Sets the value of the cliendes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCliendes(String value) {
        this.cliendes = value;
    }

    /**
     * Gets the value of the domicsec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomicsec() {
        return domicsec;
    }

    /**
     * Sets the value of the domicsec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomicsec(String value) {
        this.domicsec = value;
    }

    /**
     * Gets the value of the cuentsec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentsec() {
        return cuentsec;
    }

    /**
     * Sets the value of the cuentsec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentsec(String value) {
        this.cuentsec = value;
    }

    /**
     * Gets the value of the situcpol property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSitucpol() {
        return situcpol;
    }

    /**
     * Sets the value of the situcpol property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSitucpol(String value) {
        this.situcpol = value;
    }

    /**
     * Gets the value of the acreedor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcreedor() {
        return acreedor;
    }

    /**
     * Sets the value of the acreedor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcreedor(String value) {
        this.acreedor = value;
    }

    /**
     * Gets the value of the fecini property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecini() {
        return fecini;
    }

    /**
     * Sets the value of the fecini property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecini(String value) {
        this.fecini = value;
    }

    /**
     * Gets the value of the fecultre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecultre() {
        return fecultre;
    }

    /**
     * Sets the value of the fecultre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecultre(String value) {
        this.fecultre = value;
    }

    /**
     * Gets the value of the vigenciaDesde property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVigenciaDesde() {
        return vigenciaDesde;
    }

    /**
     * Sets the value of the vigenciaDesde property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVigenciaDesde(String value) {
        this.vigenciaDesde = value;
    }

    /**
     * Gets the value of the vigenciaHasta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVigenciaHasta() {
        return vigenciaHasta;
    }

    /**
     * Sets the value of the vigenciaHasta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVigenciaHasta(String value) {
        this.vigenciaHasta = value;
    }

    /**
     * Gets the value of the cobrocod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCobrocod() {
        return cobrocod;
    }

    /**
     * Sets the value of the cobrocod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCobrocod(String value) {
        this.cobrocod = value;
    }

    /**
     * Gets the value of the cobrodab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCobrodab() {
        return cobrodab;
    }

    /**
     * Sets the value of the cobrodab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCobrodab(String value) {
        this.cobrodab = value;
    }

    /**
     * Gets the value of the cobrotip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCobrotip() {
        return cobrotip;
    }

    /**
     * Sets the value of the cobrotip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCobrotip(String value) {
        this.cobrotip = value;
    }

    /**
     * Gets the value of the cobrodes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCobrodes() {
        return cobrodes;
    }

    /**
     * Sets the value of the cobrodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCobrodes(String value) {
        this.cobrodes = value;
    }

    /**
     * Gets the value of the cuentnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentnum() {
        return cuentnum;
    }

    /**
     * Sets the value of the cuentnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentnum(String value) {
        this.cuentnum = value;
    }

    /**
     * Gets the value of the campacod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampacod() {
        return campacod;
    }

    /**
     * Sets the value of the campacod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampacod(String value) {
        this.campacod = value;
    }

    /**
     * Gets the value of the campades property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampades() {
        return campades;
    }

    /**
     * Sets the value of the campades property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampades(String value) {
        this.campades = value;
    }

    /**
     * Gets the value of the grupoase property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrupoase() {
        return grupoase;
    }

    /**
     * Sets the value of the grupoase property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrupoase(String value) {
        this.grupoase = value;
    }

    /**
     * Gets the value of the clubeco property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClubeco() {
        return clubeco;
    }

    /**
     * Sets the value of the clubeco property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClubeco(String value) {
        this.clubeco = value;
    }

    /**
     * Gets the value of the tarjecod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarjecod() {
        return tarjecod;
    }

    /**
     * Sets the value of the tarjecod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarjecod(String value) {
        this.tarjecod = value;
    }

    /**
     * Gets the value of the optp6511 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOptp6511() {
        return optp6511;
    }

    /**
     * Sets the value of the optp6511 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOptp6511(String value) {
        this.optp6511 = value;
    }

    /**
     * Gets the value of the riesgo property.
     * 
     * @return
     *     possible object is
     *     {@link Riesgo }
     *     
     */
    public Riesgo getRiesgo() {
        return riesgo;
    }

    /**
     * Sets the value of the riesgo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Riesgo }
     *     
     */
    public void setRiesgo(Riesgo value) {
        this.riesgo = value;
    }

    /**
     * Gets the value of the coberturas property.
     * 
     * @return
     *     possible object is
     *     {@link Coberturas }
     *     
     */
    public Coberturas getCoberturas() {
        return coberturas;
    }

    /**
     * Sets the value of the coberturas property.
     * 
     * @param value
     *     allowed object is
     *     {@link Coberturas }
     *     
     */
    public void setCoberturas(Coberturas value) {
        this.coberturas = value;
    }

}
