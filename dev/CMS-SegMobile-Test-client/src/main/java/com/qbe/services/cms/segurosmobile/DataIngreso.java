
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DataIngreso complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DataIngreso">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultadoIngreso" type="{http://cms.services.qbe.com/segurosmobile}RespuestaBasica"/>
 *         &lt;element name="dataInicio" type="{http://cms.services.qbe.com/segurosmobile}DataInicio"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataIngreso", propOrder = {
    "resultadoIngreso",
    "dataInicio"
})
public class DataIngreso {

    @XmlElement(required = true, nillable = true)
    protected RespuestaBasica resultadoIngreso;
    @XmlElement(required = true, nillable = true)
    protected DataInicio dataInicio;

    /**
     * Gets the value of the resultadoIngreso property.
     * 
     * @return
     *     possible object is
     *     {@link RespuestaBasica }
     *     
     */
    public RespuestaBasica getResultadoIngreso() {
        return resultadoIngreso;
    }

    /**
     * Sets the value of the resultadoIngreso property.
     * 
     * @param value
     *     allowed object is
     *     {@link RespuestaBasica }
     *     
     */
    public void setResultadoIngreso(RespuestaBasica value) {
        this.resultadoIngreso = value;
    }

    /**
     * Gets the value of the dataInicio property.
     * 
     * @return
     *     possible object is
     *     {@link DataInicio }
     *     
     */
    public DataInicio getDataInicio() {
        return dataInicio;
    }

    /**
     * Sets the value of the dataInicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataInicio }
     *     
     */
    public void setDataInicio(DataInicio value) {
        this.dataInicio = value;
    }

}
