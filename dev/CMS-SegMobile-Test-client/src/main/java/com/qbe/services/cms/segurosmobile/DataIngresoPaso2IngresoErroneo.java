
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DataIngresoPaso2IngresoErroneo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DataIngresoPaso2IngresoErroneo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://cms.services.qbe.com/segurosmobile}dataIngresoPaso2">
 *       &lt;sequence>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codError" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codResultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="msgResultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataIngresoPaso2IngresoErroneo", propOrder = {
    "type",
    "codError",
    "codResultado",
    "msgResultado"
})
public class DataIngresoPaso2IngresoErroneo
    extends DataIngresoPaso2
{

    @XmlElement(required = true, defaultValue = "IngresoErroneo", nillable = true)
    protected String type;
    @XmlElement(required = true, nillable = true)
    protected String codError;
    @XmlElement(required = true, nillable = true)
    protected String codResultado;
    @XmlElement(required = true, nillable = true)
    protected String msgResultado;

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the codError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodError() {
        return codError;
    }

    /**
     * Sets the value of the codError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodError(String value) {
        this.codError = value;
    }

    /**
     * Gets the value of the codResultado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodResultado() {
        return codResultado;
    }

    /**
     * Sets the value of the codResultado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodResultado(String value) {
        this.codResultado = value;
    }

    /**
     * Gets the value of the msgResultado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgResultado() {
        return msgResultado;
    }

    /**
     * Sets the value of the msgResultado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgResultado(String value) {
        this.msgResultado = value;
    }

}
