
package com.qbe.services.cms.segurosmobile;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DocumentoIncompleto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DocumentoIncompleto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="longitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="digitos" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="digito" type="{http://cms.services.qbe.com/segurosmobile}DigitoFaltante" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentoIncompleto", propOrder = {
    "numero",
    "longitud",
    "digitos"
})
public class DocumentoIncompleto {

    @XmlElement(required = true)
    protected String numero;
    protected int longitud;
    protected DocumentoIncompleto.Digitos digitos;

    /**
     * Gets the value of the numero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Sets the value of the numero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Gets the value of the longitud property.
     * 
     */
    public int getLongitud() {
        return longitud;
    }

    /**
     * Sets the value of the longitud property.
     * 
     */
    public void setLongitud(int value) {
        this.longitud = value;
    }

    /**
     * Gets the value of the digitos property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoIncompleto.Digitos }
     *     
     */
    public DocumentoIncompleto.Digitos getDigitos() {
        return digitos;
    }

    /**
     * Sets the value of the digitos property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoIncompleto.Digitos }
     *     
     */
    public void setDigitos(DocumentoIncompleto.Digitos value) {
        this.digitos = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="digito" type="{http://cms.services.qbe.com/segurosmobile}DigitoFaltante" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "digito"
    })
    public static class Digitos {

        @XmlElement(required = true, nillable = true)
        protected List<DigitoFaltante> digito;

        /**
         * Gets the value of the digito property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the digito property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDigito().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DigitoFaltante }
         * 
         * 
         */
        public List<DigitoFaltante> getDigito() {
            if (digito == null) {
                digito = new ArrayList<DigitoFaltante>();
            }
            return this.digito;
        }

    }

}
