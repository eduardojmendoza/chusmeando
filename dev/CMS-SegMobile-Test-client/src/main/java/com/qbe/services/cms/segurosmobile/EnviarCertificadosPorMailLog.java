
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EnviarCertificadosPorMailLog complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnviarCertificadosPorMailLog">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codResultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="imprimirPolizaRequest" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="imprimirPolizaResponse" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="filespec" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnviarCertificadosPorMailLog", propOrder = {
    "codResultado",
    "imprimirPolizaRequest",
    "imprimirPolizaResponse",
    "filespec"
})
public class EnviarCertificadosPorMailLog {

    @XmlElement(required = true)
    protected String codResultado;
    @XmlElement(required = true)
    protected String imprimirPolizaRequest;
    @XmlElement(required = true)
    protected String imprimirPolizaResponse;
    @XmlElement(required = true)
    protected String filespec;

    /**
     * Gets the value of the codResultado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodResultado() {
        return codResultado;
    }

    /**
     * Sets the value of the codResultado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodResultado(String value) {
        this.codResultado = value;
    }

    /**
     * Gets the value of the imprimirPolizaRequest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImprimirPolizaRequest() {
        return imprimirPolizaRequest;
    }

    /**
     * Sets the value of the imprimirPolizaRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImprimirPolizaRequest(String value) {
        this.imprimirPolizaRequest = value;
    }

    /**
     * Gets the value of the imprimirPolizaResponse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImprimirPolizaResponse() {
        return imprimirPolizaResponse;
    }

    /**
     * Sets the value of the imprimirPolizaResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImprimirPolizaResponse(String value) {
        this.imprimirPolizaResponse = value;
    }

    /**
     * Gets the value of the filespec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilespec() {
        return filespec;
    }

    /**
     * Sets the value of the filespec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilespec(String value) {
        this.filespec = value;
    }

}
