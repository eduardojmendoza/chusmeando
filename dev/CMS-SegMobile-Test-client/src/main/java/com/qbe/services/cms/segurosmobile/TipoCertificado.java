
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoCertificado.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoCertificado">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PolizaCompleta"/>
 *     &lt;enumeration value="CertificadoMercosur"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoCertificado")
@XmlEnum
public enum TipoCertificado {

    @XmlEnumValue("PolizaCompleta")
    POLIZA_COMPLETA("PolizaCompleta"),
    @XmlEnumValue("CertificadoMercosur")
    CERTIFICADO_MERCOSUR("CertificadoMercosur");
    private final String value;

    TipoCertificado(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TipoCertificado fromValue(String v) {
        for (TipoCertificado c: TipoCertificado.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
