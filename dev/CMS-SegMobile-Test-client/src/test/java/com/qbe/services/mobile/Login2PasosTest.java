
package com.qbe.services.mobile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URL;

import javax.xml.namespace.QName;

import org.junit.Test;

import com.qbe.services.cms.segurosmobile.DataIngresoPaso2IngresoActivo;
import com.qbe.services.cms.segurosmobile.Digito;
import com.qbe.services.cms.segurosmobile.DigitoFaltante;
import com.qbe.services.cms.segurosmobile.Digitos;
import com.qbe.services.cms.segurosmobile.SegurosMobileService;
import com.qbe.services.cms.segurosmobile.TipoDocumento;

/**
 * 
 * 
 */
public final class Login2PasosTest {

    private static final QName SERVICE_NAME = new QName("http://mobile.services.qbe.com/", "SegurosMobileServiceImplService");

    public Login2PasosTest() {
    }

    @Test
    public void testLogin() throws java.lang.Exception {
        URL wsdlURL = SegurosMobileServiceImplService.WSDL_LOCATION;
      
        SegurosMobileServiceImplService ss = new SegurosMobileServiceImplService(wsdlURL, SERVICE_NAME);
        SegurosMobileService port = ss.getSegurosMobileServiceImplPort();  
        
        System.out.println("Invoking ingresarPaso1...");
        String usuario = "roberto_ferraris@hotmail.com";
        String password = "FERRARIS1955";
        String ipOrigen = "8.8.8.8";
        com.qbe.services.cms.segurosmobile.DataIngresoPaso1 paso1Response = port.ingresarPaso1(usuario, password, ipOrigen);
        System.out.println("ingresarPaso1.result=" + paso1Response);

        if ( paso1Response == null || ( !paso1Response.getCodError().equals("0")) || ( !paso1Response.getCodResultado().equals("OK"))) {
        	throw new RuntimeException("Respuesta paso 1 inválida");
        }
        int[] dni = {1, 1, 7, 8, 5, 3, 4, 5};
        
        com.qbe.services.cms.segurosmobile.DocumentoIncompleto.Digitos digitosFaltantes = paso1Response.getDocumentoIncompleto().getDigitos();
        Digitos digitosCompletos = new Digitos();
         
        DigitoFaltante df1 = digitosFaltantes.getDigito().get(0);
        Digito d1 = new Digito();
        d1.setId(df1.getId());
        d1.setValor(dni[df1.getPos() - 1]);
        digitosCompletos.getDigito().add(d1);
        
        DigitoFaltante df2 = digitosFaltantes.getDigito().get(1);
        Digito d2 = new Digito();
        d2.setId(df2.getId());
        d2.setValor(dni[df2.getPos() - 1]);
        digitosCompletos.getDigito().add(d2);

        DigitoFaltante df3 = digitosFaltantes.getDigito().get(2);
        Digito d3 = new Digito();
        d3.setId(df3.getId());
        d3.setValor(dni[df3.getPos() - 1]);
        digitosCompletos.getDigito().add(d3);

        System.out.println("Invoking ingresarPaso2...");

        com.qbe.services.cms.segurosmobile.TipoDocumento _ingresarPaso2_tipoDocumento = TipoDocumento.DNI;
        com.qbe.services.cms.segurosmobile.DataIngresoPaso2 paso2Response = port.ingresarPaso2(usuario, password, ipOrigen, _ingresarPaso2_tipoDocumento, digitosCompletos);
        System.out.println("ingresarPaso2.result=" + paso2Response);
        
        assertTrue(paso2Response instanceof DataIngresoPaso2IngresoActivo);
        DataIngresoPaso2IngresoActivo ia = (DataIngresoPaso2IngresoActivo)paso2Response;
        assertEquals("OK",ia.getResultado());
        assertEquals("FERRARIS",ia.getCliente().getApellido());
        assertEquals("11785345",ia.getDocumento().getNumero());

    }

}
