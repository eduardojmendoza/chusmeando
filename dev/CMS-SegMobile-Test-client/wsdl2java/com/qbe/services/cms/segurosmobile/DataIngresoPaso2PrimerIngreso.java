
package com.qbe.services.cms.segurosmobile;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DataIngresoPaso2PrimerIngreso complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DataIngresoPaso2PrimerIngreso">
 *   &lt;complexContent>
 *     &lt;extension base="{http://cms.services.qbe.com/segurosmobile}dataIngresoPaso2">
 *       &lt;sequence>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="medioIngreso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="accion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="viaInscripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="polizas" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="poliza" type="{http://cms.services.qbe.com/segurosmobile}Poliza" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataIngresoPaso2PrimerIngreso", propOrder = {
    "type",
    "medioIngreso",
    "accion",
    "viaInscripcion",
    "polizas"
})
public class DataIngresoPaso2PrimerIngreso
    extends DataIngresoPaso2
{

    @XmlElement(required = true, defaultValue = "PrimerIngreso", nillable = true)
    protected String type;
    @XmlElement(required = true, nillable = true)
    protected String medioIngreso;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer accion;
    @XmlElement(required = true, nillable = true)
    protected String viaInscripcion;
    protected DataIngresoPaso2PrimerIngreso.Polizas polizas;

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the medioIngreso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedioIngreso() {
        return medioIngreso;
    }

    /**
     * Sets the value of the medioIngreso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedioIngreso(String value) {
        this.medioIngreso = value;
    }

    /**
     * Gets the value of the accion property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAccion() {
        return accion;
    }

    /**
     * Sets the value of the accion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAccion(Integer value) {
        this.accion = value;
    }

    /**
     * Gets the value of the viaInscripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViaInscripcion() {
        return viaInscripcion;
    }

    /**
     * Sets the value of the viaInscripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViaInscripcion(String value) {
        this.viaInscripcion = value;
    }

    /**
     * Gets the value of the polizas property.
     * 
     * @return
     *     possible object is
     *     {@link DataIngresoPaso2PrimerIngreso.Polizas }
     *     
     */
    public DataIngresoPaso2PrimerIngreso.Polizas getPolizas() {
        return polizas;
    }

    /**
     * Sets the value of the polizas property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataIngresoPaso2PrimerIngreso.Polizas }
     *     
     */
    public void setPolizas(DataIngresoPaso2PrimerIngreso.Polizas value) {
        this.polizas = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="poliza" type="{http://cms.services.qbe.com/segurosmobile}Poliza" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "poliza"
    })
    public static class Polizas {

        @XmlElement(required = true, nillable = true)
        protected List<Poliza> poliza;

        /**
         * Gets the value of the poliza property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the poliza property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPoliza().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Poliza }
         * 
         * 
         */
        public List<Poliza> getPoliza() {
            if (poliza == null) {
                poliza = new ArrayList<Poliza>();
            }
            return this.poliza;
        }

    }

}
