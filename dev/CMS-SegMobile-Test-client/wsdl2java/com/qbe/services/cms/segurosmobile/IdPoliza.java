
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IdPoliza complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IdPoliza">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ramopcod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="polizann" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="polizsec" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="certipol" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="certiann" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="certisec" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="suplenum" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdPoliza", propOrder = {
    "ramopcod",
    "polizann",
    "polizsec",
    "certipol",
    "certiann",
    "certisec",
    "suplenum"
})
public class IdPoliza {

    @XmlElement(required = true, nillable = true)
    protected String ramopcod;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer polizann;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer polizsec;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer certipol;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer certiann;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer certisec;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer suplenum;

    /**
     * Gets the value of the ramopcod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRamopcod() {
        return ramopcod;
    }

    /**
     * Sets the value of the ramopcod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRamopcod(String value) {
        this.ramopcod = value;
    }

    /**
     * Gets the value of the polizann property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolizann() {
        return polizann;
    }

    /**
     * Sets the value of the polizann property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolizann(Integer value) {
        this.polizann = value;
    }

    /**
     * Gets the value of the polizsec property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolizsec() {
        return polizsec;
    }

    /**
     * Sets the value of the polizsec property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolizsec(Integer value) {
        this.polizsec = value;
    }

    /**
     * Gets the value of the certipol property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCertipol() {
        return certipol;
    }

    /**
     * Sets the value of the certipol property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCertipol(Integer value) {
        this.certipol = value;
    }

    /**
     * Gets the value of the certiann property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCertiann() {
        return certiann;
    }

    /**
     * Sets the value of the certiann property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCertiann(Integer value) {
        this.certiann = value;
    }

    /**
     * Gets the value of the certisec property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCertisec() {
        return certisec;
    }

    /**
     * Sets the value of the certisec property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCertisec(Integer value) {
        this.certisec = value;
    }

    /**
     * Gets the value of the suplenum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSuplenum() {
        return suplenum;
    }

    /**
     * Sets the value of the suplenum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSuplenum(Integer value) {
        this.suplenum = value;
    }

}
