
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for VencimientoRegistroReminder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VencimientoRegistroReminder">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="mail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="avisoDosSemanasEnviado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="avisoUnMesEnviado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VencimientoRegistroReminder", propOrder = {
    "id",
    "mail",
    "vencimiento",
    "avisoDosSemanasEnviado",
    "avisoUnMesEnviado"
})
public class VencimientoRegistroReminder {

    protected int id;
    @XmlElement(required = true)
    protected String mail;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar vencimiento;
    protected boolean avisoDosSemanasEnviado;
    protected boolean avisoUnMesEnviado;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the mail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMail() {
        return mail;
    }

    /**
     * Sets the value of the mail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMail(String value) {
        this.mail = value;
    }

    /**
     * Gets the value of the vencimiento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVencimiento() {
        return vencimiento;
    }

    /**
     * Sets the value of the vencimiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVencimiento(XMLGregorianCalendar value) {
        this.vencimiento = value;
    }

    /**
     * Gets the value of the avisoDosSemanasEnviado property.
     * 
     */
    public boolean isAvisoDosSemanasEnviado() {
        return avisoDosSemanasEnviado;
    }

    /**
     * Sets the value of the avisoDosSemanasEnviado property.
     * 
     */
    public void setAvisoDosSemanasEnviado(boolean value) {
        this.avisoDosSemanasEnviado = value;
    }

    /**
     * Gets the value of the avisoUnMesEnviado property.
     * 
     */
    public boolean isAvisoUnMesEnviado() {
        return avisoUnMesEnviado;
    }

    /**
     * Sets the value of the avisoUnMesEnviado property.
     * 
     */
    public void setAvisoUnMesEnviado(boolean value) {
        this.avisoUnMesEnviado = value;
    }

}
