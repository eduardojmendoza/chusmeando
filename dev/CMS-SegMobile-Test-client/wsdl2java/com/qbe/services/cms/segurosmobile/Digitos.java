
package com.qbe.services.cms.segurosmobile;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Digitos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Digitos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="digito" type="{http://cms.services.qbe.com/segurosmobile}Digito" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Digitos", propOrder = {
    "digito"
})
public class Digitos {

    @XmlElement(nillable = true)
    protected List<Digito> digito;

    /**
     * Gets the value of the digito property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the digito property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDigito().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Digito }
     * 
     * 
     */
    public List<Digito> getDigito() {
        if (digito == null) {
            digito = new ArrayList<Digito>();
        }
        return this.digito;
    }

}
