
package com.qbe.services.cms.segurosmobile;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TiposDocumento_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TiposDocumento_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tiposDocumento" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="tipoDocumento" type="{http://cms.services.qbe.com/segurosmobile}TipoDocumento" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TiposDocumento_type", propOrder = {
    "tiposDocumento"
})
public class TiposDocumentoType {

    protected TiposDocumentoType.TiposDocumento tiposDocumento;

    /**
     * Gets the value of the tiposDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link TiposDocumentoType.TiposDocumento }
     *     
     */
    public TiposDocumentoType.TiposDocumento getTiposDocumento() {
        return tiposDocumento;
    }

    /**
     * Sets the value of the tiposDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TiposDocumentoType.TiposDocumento }
     *     
     */
    public void setTiposDocumento(TiposDocumentoType.TiposDocumento value) {
        this.tiposDocumento = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="tipoDocumento" type="{http://cms.services.qbe.com/segurosmobile}TipoDocumento" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tipoDocumento"
    })
    public static class TiposDocumento {

        @XmlElement(required = true, nillable = true)
        @XmlSchemaType(name = "string")
        protected List<TipoDocumento> tipoDocumento;

        /**
         * Gets the value of the tipoDocumento property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tipoDocumento property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTipoDocumento().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TipoDocumento }
         * 
         * 
         */
        public List<TipoDocumento> getTipoDocumento() {
            if (tipoDocumento == null) {
                tipoDocumento = new ArrayList<TipoDocumento>();
            }
            return this.tipoDocumento;
        }

    }

}
