
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enviarCertificadosPorMail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enviarCertificadosPorMail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="poliza" type="{http://cms.services.qbe.com/segurosmobile}IdPoliza" minOccurs="0"/>
 *         &lt;element name="certificado" type="{http://cms.services.qbe.com/segurosmobile}tipoCertificado" minOccurs="0"/>
 *         &lt;element name="mail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enviarCertificadosPorMail", propOrder = {
    "poliza",
    "certificado",
    "mail"
})
public class EnviarCertificadosPorMail {

    protected IdPoliza poliza;
    @XmlSchemaType(name = "string")
    protected TipoCertificado certificado;
    protected String mail;

    /**
     * Gets the value of the poliza property.
     * 
     * @return
     *     possible object is
     *     {@link IdPoliza }
     *     
     */
    public IdPoliza getPoliza() {
        return poliza;
    }

    /**
     * Sets the value of the poliza property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdPoliza }
     *     
     */
    public void setPoliza(IdPoliza value) {
        this.poliza = value;
    }

    /**
     * Gets the value of the certificado property.
     * 
     * @return
     *     possible object is
     *     {@link TipoCertificado }
     *     
     */
    public TipoCertificado getCertificado() {
        return certificado;
    }

    /**
     * Sets the value of the certificado property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCertificado }
     *     
     */
    public void setCertificado(TipoCertificado value) {
        this.certificado = value;
    }

    /**
     * Gets the value of the mail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMail() {
        return mail;
    }

    /**
     * Sets the value of the mail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMail(String value) {
        this.mail = value;
    }

}
