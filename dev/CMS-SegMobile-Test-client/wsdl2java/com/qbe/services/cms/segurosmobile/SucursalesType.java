
package com.qbe.services.cms.segurosmobile;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Sucursales_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Sucursales_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sucursales" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="sucursal" type="{http://cms.services.qbe.com/segurosmobile}Sucursal" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Sucursales_type", propOrder = {
    "sucursales"
})
public class SucursalesType {

    protected SucursalesType.Sucursales sucursales;

    /**
     * Gets the value of the sucursales property.
     * 
     * @return
     *     possible object is
     *     {@link SucursalesType.Sucursales }
     *     
     */
    public SucursalesType.Sucursales getSucursales() {
        return sucursales;
    }

    /**
     * Sets the value of the sucursales property.
     * 
     * @param value
     *     allowed object is
     *     {@link SucursalesType.Sucursales }
     *     
     */
    public void setSucursales(SucursalesType.Sucursales value) {
        this.sucursales = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="sucursal" type="{http://cms.services.qbe.com/segurosmobile}Sucursal" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sucursal"
    })
    public static class Sucursales {

        @XmlElement(required = true, nillable = true)
        protected List<Sucursal> sucursal;

        /**
         * Gets the value of the sucursal property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the sucursal property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSucursal().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Sucursal }
         * 
         * 
         */
        public List<Sucursal> getSucursal() {
            if (sucursal == null) {
                sucursal = new ArrayList<Sucursal>();
            }
            return this.sucursal;
        }

    }

}
