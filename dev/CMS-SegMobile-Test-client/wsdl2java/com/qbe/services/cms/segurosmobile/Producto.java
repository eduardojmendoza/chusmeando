
package com.qbe.services.cms.segurosmobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Producto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Producto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ciaascod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ramopcod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="polizann" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="polizsec" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="certipol" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="certiann" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="certisec" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="suplenum" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ramopdes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cliensecv" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="clienap1v" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clienap2v" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cliennomv" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="agentcod" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="agentcla" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tomaries" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="situcpol" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="emisiann" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="emisimes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="emisidia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tipoprod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="patente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dato1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dato2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dato3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dato4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cobrodes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="swsuscri" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clave" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="swclave" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mensasin" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mensasus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="swimprim" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cobranza" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="siniestro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="constancia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="habilitadoNBWS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="positiveid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="polizaExcluida" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="habilitadoNavegacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="masDeCincoCert" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="habilitadoEPoliza" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vigenciaDesde" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vigenciaHasta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="detallePoliza" type="{http://cms.services.qbe.com/segurosmobile}DetallePoliza" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Producto", propOrder = {
    "ciaascod",
    "ramopcod",
    "polizann",
    "polizsec",
    "certipol",
    "certiann",
    "certisec",
    "suplenum",
    "ramopdes",
    "cliensecv",
    "clienap1V",
    "clienap2V",
    "cliennomv",
    "agentcod",
    "agentcla",
    "tomaries",
    "situcpol",
    "emisiann",
    "emisimes",
    "emisidia",
    "tipoprod",
    "patente",
    "dato1",
    "dato2",
    "dato3",
    "dato4",
    "cobrodes",
    "swsuscri",
    "mail",
    "clave",
    "swclave",
    "mensasin",
    "mensasus",
    "swimprim",
    "cobranza",
    "siniestro",
    "constancia",
    "habilitadoNBWS",
    "positiveid",
    "polizaExcluida",
    "habilitadoNavegacion",
    "masDeCincoCert",
    "habilitadoEPoliza",
    "vigenciaDesde",
    "vigenciaHasta",
    "detallePoliza"
})
public class Producto {

    @XmlElement(required = true, nillable = true)
    protected String ciaascod;
    @XmlElement(required = true, nillable = true)
    protected String ramopcod;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer polizann;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer polizsec;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer certipol;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer certiann;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer certisec;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer suplenum;
    @XmlElement(required = true, nillable = true)
    protected String ramopdes;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer cliensecv;
    @XmlElement(name = "clienap1v", required = true, nillable = true)
    protected String clienap1V;
    @XmlElement(name = "clienap2v", required = true, nillable = true)
    protected String clienap2V;
    @XmlElement(required = true, nillable = true)
    protected String cliennomv;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer agentcod;
    @XmlElement(required = true, nillable = true)
    protected String agentcla;
    @XmlElement(required = true, nillable = true)
    protected String tomaries;
    @XmlElement(required = true, nillable = true)
    protected String situcpol;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer emisiann;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer emisimes;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer emisidia;
    @XmlElement(required = true, nillable = true)
    protected String tipoprod;
    @XmlElement(required = true, nillable = true)
    protected String patente;
    @XmlElement(required = true, nillable = true)
    protected String dato1;
    @XmlElement(required = true, nillable = true)
    protected String dato2;
    @XmlElement(required = true, nillable = true)
    protected String dato3;
    @XmlElement(required = true, nillable = true)
    protected String dato4;
    @XmlElement(required = true, nillable = true)
    protected String cobrodes;
    @XmlElement(required = true, nillable = true)
    protected String swsuscri;
    @XmlElement(required = true, nillable = true)
    protected String mail;
    @XmlElement(required = true, nillable = true)
    protected String clave;
    @XmlElement(required = true, nillable = true)
    protected String swclave;
    @XmlElement(required = true, nillable = true)
    protected String mensasin;
    @XmlElement(required = true, nillable = true)
    protected String mensasus;
    @XmlElement(required = true, nillable = true)
    protected String swimprim;
    @XmlElement(required = true, nillable = true)
    protected String cobranza;
    @XmlElement(required = true, nillable = true)
    protected String siniestro;
    @XmlElement(required = true, nillable = true)
    protected String constancia;
    @XmlElement(required = true, nillable = true)
    protected String habilitadoNBWS;
    @XmlElement(required = true, nillable = true)
    protected String positiveid;
    @XmlElement(required = true, nillable = true)
    protected String polizaExcluida;
    @XmlElement(required = true, nillable = true)
    protected String habilitadoNavegacion;
    @XmlElement(required = true, nillable = true)
    protected String masDeCincoCert;
    @XmlElement(required = true, nillable = true)
    protected String habilitadoEPoliza;
    @XmlElement(required = true, nillable = true)
    protected String vigenciaDesde;
    @XmlElement(required = true, nillable = true)
    protected String vigenciaHasta;
    protected DetallePoliza detallePoliza;

    /**
     * Gets the value of the ciaascod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiaascod() {
        return ciaascod;
    }

    /**
     * Sets the value of the ciaascod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiaascod(String value) {
        this.ciaascod = value;
    }

    /**
     * Gets the value of the ramopcod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRamopcod() {
        return ramopcod;
    }

    /**
     * Sets the value of the ramopcod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRamopcod(String value) {
        this.ramopcod = value;
    }

    /**
     * Gets the value of the polizann property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolizann() {
        return polizann;
    }

    /**
     * Sets the value of the polizann property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolizann(Integer value) {
        this.polizann = value;
    }

    /**
     * Gets the value of the polizsec property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolizsec() {
        return polizsec;
    }

    /**
     * Sets the value of the polizsec property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolizsec(Integer value) {
        this.polizsec = value;
    }

    /**
     * Gets the value of the certipol property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCertipol() {
        return certipol;
    }

    /**
     * Sets the value of the certipol property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCertipol(Integer value) {
        this.certipol = value;
    }

    /**
     * Gets the value of the certiann property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCertiann() {
        return certiann;
    }

    /**
     * Sets the value of the certiann property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCertiann(Integer value) {
        this.certiann = value;
    }

    /**
     * Gets the value of the certisec property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCertisec() {
        return certisec;
    }

    /**
     * Sets the value of the certisec property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCertisec(Integer value) {
        this.certisec = value;
    }

    /**
     * Gets the value of the suplenum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSuplenum() {
        return suplenum;
    }

    /**
     * Sets the value of the suplenum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSuplenum(Integer value) {
        this.suplenum = value;
    }

    /**
     * Gets the value of the ramopdes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRamopdes() {
        return ramopdes;
    }

    /**
     * Sets the value of the ramopdes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRamopdes(String value) {
        this.ramopdes = value;
    }

    /**
     * Gets the value of the cliensecv property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCliensecv() {
        return cliensecv;
    }

    /**
     * Sets the value of the cliensecv property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCliensecv(Integer value) {
        this.cliensecv = value;
    }

    /**
     * Gets the value of the clienap1V property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienap1V() {
        return clienap1V;
    }

    /**
     * Sets the value of the clienap1V property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienap1V(String value) {
        this.clienap1V = value;
    }

    /**
     * Gets the value of the clienap2V property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienap2V() {
        return clienap2V;
    }

    /**
     * Sets the value of the clienap2V property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienap2V(String value) {
        this.clienap2V = value;
    }

    /**
     * Gets the value of the cliennomv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCliennomv() {
        return cliennomv;
    }

    /**
     * Sets the value of the cliennomv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCliennomv(String value) {
        this.cliennomv = value;
    }

    /**
     * Gets the value of the agentcod property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAgentcod() {
        return agentcod;
    }

    /**
     * Sets the value of the agentcod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAgentcod(Integer value) {
        this.agentcod = value;
    }

    /**
     * Gets the value of the agentcla property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentcla() {
        return agentcla;
    }

    /**
     * Sets the value of the agentcla property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentcla(String value) {
        this.agentcla = value;
    }

    /**
     * Gets the value of the tomaries property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTomaries() {
        return tomaries;
    }

    /**
     * Sets the value of the tomaries property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTomaries(String value) {
        this.tomaries = value;
    }

    /**
     * Gets the value of the situcpol property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSitucpol() {
        return situcpol;
    }

    /**
     * Sets the value of the situcpol property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSitucpol(String value) {
        this.situcpol = value;
    }

    /**
     * Gets the value of the emisiann property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEmisiann() {
        return emisiann;
    }

    /**
     * Sets the value of the emisiann property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEmisiann(Integer value) {
        this.emisiann = value;
    }

    /**
     * Gets the value of the emisimes property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEmisimes() {
        return emisimes;
    }

    /**
     * Sets the value of the emisimes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEmisimes(Integer value) {
        this.emisimes = value;
    }

    /**
     * Gets the value of the emisidia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEmisidia() {
        return emisidia;
    }

    /**
     * Sets the value of the emisidia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEmisidia(Integer value) {
        this.emisidia = value;
    }

    /**
     * Gets the value of the tipoprod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoprod() {
        return tipoprod;
    }

    /**
     * Sets the value of the tipoprod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoprod(String value) {
        this.tipoprod = value;
    }

    /**
     * Gets the value of the patente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatente() {
        return patente;
    }

    /**
     * Sets the value of the patente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatente(String value) {
        this.patente = value;
    }

    /**
     * Gets the value of the dato1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDato1() {
        return dato1;
    }

    /**
     * Sets the value of the dato1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDato1(String value) {
        this.dato1 = value;
    }

    /**
     * Gets the value of the dato2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDato2() {
        return dato2;
    }

    /**
     * Sets the value of the dato2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDato2(String value) {
        this.dato2 = value;
    }

    /**
     * Gets the value of the dato3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDato3() {
        return dato3;
    }

    /**
     * Sets the value of the dato3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDato3(String value) {
        this.dato3 = value;
    }

    /**
     * Gets the value of the dato4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDato4() {
        return dato4;
    }

    /**
     * Sets the value of the dato4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDato4(String value) {
        this.dato4 = value;
    }

    /**
     * Gets the value of the cobrodes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCobrodes() {
        return cobrodes;
    }

    /**
     * Sets the value of the cobrodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCobrodes(String value) {
        this.cobrodes = value;
    }

    /**
     * Gets the value of the swsuscri property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSwsuscri() {
        return swsuscri;
    }

    /**
     * Sets the value of the swsuscri property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSwsuscri(String value) {
        this.swsuscri = value;
    }

    /**
     * Gets the value of the mail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMail() {
        return mail;
    }

    /**
     * Sets the value of the mail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMail(String value) {
        this.mail = value;
    }

    /**
     * Gets the value of the clave property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClave() {
        return clave;
    }

    /**
     * Sets the value of the clave property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClave(String value) {
        this.clave = value;
    }

    /**
     * Gets the value of the swclave property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSwclave() {
        return swclave;
    }

    /**
     * Sets the value of the swclave property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSwclave(String value) {
        this.swclave = value;
    }

    /**
     * Gets the value of the mensasin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensasin() {
        return mensasin;
    }

    /**
     * Sets the value of the mensasin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensasin(String value) {
        this.mensasin = value;
    }

    /**
     * Gets the value of the mensasus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensasus() {
        return mensasus;
    }

    /**
     * Sets the value of the mensasus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensasus(String value) {
        this.mensasus = value;
    }

    /**
     * Gets the value of the swimprim property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSwimprim() {
        return swimprim;
    }

    /**
     * Sets the value of the swimprim property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSwimprim(String value) {
        this.swimprim = value;
    }

    /**
     * Gets the value of the cobranza property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCobranza() {
        return cobranza;
    }

    /**
     * Sets the value of the cobranza property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCobranza(String value) {
        this.cobranza = value;
    }

    /**
     * Gets the value of the siniestro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiniestro() {
        return siniestro;
    }

    /**
     * Sets the value of the siniestro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiniestro(String value) {
        this.siniestro = value;
    }

    /**
     * Gets the value of the constancia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConstancia() {
        return constancia;
    }

    /**
     * Sets the value of the constancia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConstancia(String value) {
        this.constancia = value;
    }

    /**
     * Gets the value of the habilitadoNBWS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHabilitadoNBWS() {
        return habilitadoNBWS;
    }

    /**
     * Sets the value of the habilitadoNBWS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHabilitadoNBWS(String value) {
        this.habilitadoNBWS = value;
    }

    /**
     * Gets the value of the positiveid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPositiveid() {
        return positiveid;
    }

    /**
     * Sets the value of the positiveid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPositiveid(String value) {
        this.positiveid = value;
    }

    /**
     * Gets the value of the polizaExcluida property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizaExcluida() {
        return polizaExcluida;
    }

    /**
     * Sets the value of the polizaExcluida property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizaExcluida(String value) {
        this.polizaExcluida = value;
    }

    /**
     * Gets the value of the habilitadoNavegacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHabilitadoNavegacion() {
        return habilitadoNavegacion;
    }

    /**
     * Sets the value of the habilitadoNavegacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHabilitadoNavegacion(String value) {
        this.habilitadoNavegacion = value;
    }

    /**
     * Gets the value of the masDeCincoCert property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasDeCincoCert() {
        return masDeCincoCert;
    }

    /**
     * Sets the value of the masDeCincoCert property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasDeCincoCert(String value) {
        this.masDeCincoCert = value;
    }

    /**
     * Gets the value of the habilitadoEPoliza property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHabilitadoEPoliza() {
        return habilitadoEPoliza;
    }

    /**
     * Sets the value of the habilitadoEPoliza property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHabilitadoEPoliza(String value) {
        this.habilitadoEPoliza = value;
    }

    /**
     * Gets the value of the vigenciaDesde property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVigenciaDesde() {
        return vigenciaDesde;
    }

    /**
     * Sets the value of the vigenciaDesde property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVigenciaDesde(String value) {
        this.vigenciaDesde = value;
    }

    /**
     * Gets the value of the vigenciaHasta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVigenciaHasta() {
        return vigenciaHasta;
    }

    /**
     * Sets the value of the vigenciaHasta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVigenciaHasta(String value) {
        this.vigenciaHasta = value;
    }

    /**
     * Gets the value of the detallePoliza property.
     * 
     * @return
     *     possible object is
     *     {@link DetallePoliza }
     *     
     */
    public DetallePoliza getDetallePoliza() {
        return detallePoliza;
    }

    /**
     * Sets the value of the detallePoliza property.
     * 
     * @param value
     *     allowed object is
     *     {@link DetallePoliza }
     *     
     */
    public void setDetallePoliza(DetallePoliza value) {
        this.detallePoliza = value;
    }

}
