#Seguros Mobile 


Borré el directorio /integration ( código del OSB ) y otras cosas de Insis en el commit:
f5a9b118eb65d618bea3b04728794998f6ec3e23
f5a9b118e... Saco archivos Insis y otros viejos

@ramirogm
ramirogm committed on Oct 29, 2015


el padre es https://github.com/snoopconsulting/migracioncomplus/commit/4f60ec0fb5c640d8b78c470ea23a7548ebe7a892
4f60ec0fb5c640d8b78c470ea23a7548ebe7a892

Entonces puedo crear un branch a partir de este y ahí tengo lo del osb? O checkouteo y copio lo que necesite al master ( pierdo la historia...)
O si le hago revert, vuelven los directorios y archivos que borré...

Hago revert. después "vuelvo a borrar":
    notas_ignacio, notas_leandro, notas_martin, SOAP_UI_Tests, Pruebas, Insis, BKPPlanillaMensajes




---------------

Hago build sin instalar entorno...

https://hub.docker.com/_/maven/

 ~/dev/migracioncomplus/dev   master±  docker run -it --rm --name my-maven-project -v "$PWD":/usr/src/mymaven -w /usr/src/mymaven maven:3.2-jdk-7 mvn clean install


[ERROR] Failed to execute goal on project CMS-aisconnector-mq: Could not resolve dependencies for project com.qbe.integration:CMS-aisconnector-mq:jar:3.0.68-SNAPSHOT: The following artifacts could not be resolved: com.ibm.mq:mq:jar:7.5, com.ibm.mq:mqjms:jar:7.5, com.ibm.mq:jmqi:jar:7.5, com.ibm:dhbcore:jar:7.5, com.ibm.mq:headers:jar:7.5: Could not find artifact com.ibm.mq:mq:jar:7.5 in central (https://repo.maven.apache.org/maven2) -> [Help 1]
[ERROR]




alias mvn="docker run -it --rm --name my-maven-project -v "$PWD":/usr/src/mymaven -w /usr/src/mymaven maven:3.2-jdk-7 mvn"



apache-maven-3.5.2/bin/mvn install:install-file -Dfile=lib/vbc.jar -DgroupId=com.diamondedge -DartifactId=vbc -Dversion=4 -Dpackaging=jar

apache-maven-3.5.2/bin/mvn install:install-file -Dfile=lib/com.ibm.mq-7.5.jar -DgroupId=com.ibm.mq -DartifactId=mq -Dversion=7.5 -Dpackaging=jar

apache-maven-3.5.2/bin/mvn install:install-file -Dfile=lib/com.ibm.mqjms-7.5.jar -DgroupId=com.ibm.mq -DartifactId=mqjms -Dversion=7.5 -Dpackaging=jar

apache-maven-3.5.2/bin/mvn install:install-file -Dfile=lib/com.ibm.mq.jmqi-7.5.jar -DgroupId=com.ibm.mq  -DartifactId=jmqi -Dversion=7.5 -Dpackaging=jar

apache-maven-3.5.2/bin/mvn install:install-file -Dfile=lib/dhbcore.jar -DgroupId=com.ibm -DartifactId=dhbcore -Dversion=7.5 -Dpackaging=jar

apache-maven-3.5.2/bin/mvn install:install-file -Dfile=lib/com.ibm.mq.headers-7.5.jar -DgroupId=com.ibm.mq  -DartifactId=headers -Dversion=7.5 -Dpackaging=jar

apache-maven-3.5.2/bin/mvn install:install-file -Dfile=lib/jcoldapi-2.27.jar -DgroupId=com.amco -DartifactId=jcoldapi -Dversion=2.27 -Dpackaging=jar

apache-maven-3.5.2/bin/mvn install:install-file -Dfile=lib/sqljdbc4.jar -DgroupId=com.microsoft -DartifactId=sqljdbc4 -Dversion=3.0 -Dpackaging=jar

apache-maven-3.5.2/bin/mvn install:install-file -Dfile=lib/cobertura-maven-plugin-2.5.2.jar -DgroupId=org.codehaus.mojo -DartifactId=cobertura-maven-plugin -Dversion=2.5.2 -Dpackaging=jar

apache-maven-3.5.2/bin/mvn clean install -DskipTests

cd el cmsegurosmobileservices
mvn install

están mal las versiones, el root pom tiene 3.0.68 y los hijos 3.0.1
pongo todo a 3.1

mvn --batch-mode release:update-versions -DautoVersionSubmodules=true -DdevelopmentVersion=3.1.0-SNAPSHOT

Hice:
 ~/dev/migracioncomplus/dev   master±  apache-maven-3.5.2/bin/mvn --batch-mode release:update-versions -DautoVersionSubmodules=true -DdevelopmentVersion=3.1.0-SNAPSHOT

  ~/dev/migracioncomplus/dev   master±  apache-maven-3.5.2/bin/mvn install -Dmaven.test.skip=true


Con esto resolví los builds de los CMS-common, pero hay 3 que están fuera de este esquema ( o sea no comparten el parent ) así que los tuve que actualizar a mano.
CMS-SegurosOnline
CMS-SegurosMobile
CMS-SegurosMobileServices

~/dev/migracioncomplus/dev/CMS-SegurosOnline   master±  ../apache-maven-3.5.2/bin/mvn install -Dmaven.test.skip=true
~/dev/migracioncomplus/dev/CMS-SegurosMobile   master±  ../apache-maven-3.5.2/bin/mvn install -Dmaven.test.skip=true
 ~/dev/migracioncomplus/dev/CMS-SegurosMobileServices   master±  ../apache-maven-3.5.2/bin/mvn install -Dmaven.test.skip=true

 E hizo el build nomás


 --------


 Cambios en el código.
 Defino en default.properties
 segurosmobile.smtpserver=ARP201VWNCAP.QBE-AR.localdomain
o sea que este nombre lo tienen que resolver en /etc/hosts
Y si lo quieren modificar, lo tienen que poner en el archivo de properties del entorno

Tiro de nuevo build. Incluyendo cms-commons

apache-maven-3.5.2/bin/mvn --batch-mode release:update-versions -DautoVersionSubmodules=true -DdevelopmentVersion=3.1.1-SNAPSHOT

CMS-SegurosOnline ; ../apache-maven-3.5.2/bin/mvn --batch-mode release:update-versions -DautoVersionSubmodules=true -DdevelopmentVersion=3.1.1-SNAPSHOT ; cd ..
CMS-SegurosMobile ; ../apache-maven-3.5.2/bin/mvn --batch-mode release:update-versions -DautoVersionSubmodules=true -DdevelopmentVersion=3.1.1-SNAPSHOT ; cd ..
CMS-SegurosMobileServices ; ../apache-maven-3.5.2/bin/mvn --batch-mode release:update-versions -DautoVersionSubmodules=true -DdevelopmentVersion=3.1.1-SNAPSHOT ; cd ..

ahora el build

apache-maven-3.5.2/bin/mvn install -Dmaven.test.skip=true

cd CMS-SegurosOnline ; ../apache-maven-3.5.2/bin/mvn install -Dmaven.test.skip=true; cd ..
cd CMS-SegurosMobile ; ../apache-maven-3.5.2/bin/mvn install -Dmaven.test.skip=true; cd ..
cd CMS-SegurosMobileServices ; ../apache-maven-3.5.2/bin/mvn install -Dmaven.test.skip=true; cd ..

listo.

lo trato de levantar...

Instalo un weblogic en bin
Le pongo weblogic/snoop123

bin/fmw_12/wls12213/user_projects/domains/base_domain
http://localhost:7001/console

Lo levanto con:
 ~/bin/fmw_12/wls12213/user_projects/domains/base_domain  ./startWebLogic.sh


levanta... le deployo el war


INFO: Refreshing Root WebApplicationContext: startup date [Wed Feb 07 13:19:46 ART 2018]; root of context hierarchy
Feb 07, 2018 1:19:46 PM org.springframework.beans.factory.xml.XmlBeanDefinitionReader loadBeanDefinitions
INFO: Loading XML bean definitions from class path resource [META-INF/cxf/cxf.xml]
Feb 07, 2018 1:19:46 PM org.springframework.beans.factory.xml.XmlBeanDefinitionReader loadBeanDefinitions
INFO: Loading XML bean definitions from URL [file:/Users/ramiro/bin/fmw_12/wls12213/user_projects/domains/base_domain/servers/AdminServer/tmp/_WL_user/CMS-SegurosMobileServices/dz1y2v/war/WEB-INF/cxf-servlet.xml]
Feb 07, 2018 1:19:46 PM org.springframework.beans.factory.xml.XmlBeanDefinitionReader loadBeanDefinitions
INFO: Loading XML bean definitions from class path resource [META-INF/cxf/cxf.xml]
Feb 07, 2018 1:19:46 PM org.springframework.beans.factory.support.DefaultListableBeanFactory registerBeanDefinition
INFO: Overriding bean definition for bean 'cxf': replacing [Generic bean: class [org.apache.cxf.bus.spring.SpringBus]; scope=; abstract=false; lazyInit=false; autowireMode=0; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=shutdown; defined in class path resource [META-INF/cxf/cxf.xml]] with [Generic bean: class [org.apache.cxf.bus.spring.SpringBus]; scope=; abstract=false; lazyInit=false; autowireMode=0; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=shutdown; defined in class path resource [META-INF/cxf/cxf.xml]]
Feb 07, 2018 1:19:46 PM org.springframework.beans.factory.support.DefaultListableBeanFactory registerBeanDefinition
INFO: Overriding bean definition for bean 'org.apache.cxf.bus.spring.BusWiringBeanFactoryPostProcessor': replacing [Generic bean: class [org.apache.cxf.bus.spring.BusWiringBeanFactoryPostProcessor]; scope=; abstract=false; lazyInit=false; autowireMode=0; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null; defined in class path resource [META-INF/cxf/cxf.xml]] with [Generic bean: class [org.apache.cxf.bus.spring.BusWiringBeanFactoryPostProcessor]; scope=; abstract=false; lazyInit=false; autowireMode=0; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null; defined in class path resource [META-INF/cxf/cxf.xml]]
Feb 07, 2018 1:19:46 PM org.springframework.beans.factory.support.DefaultListableBeanFactory registerBeanDefinition
INFO: Overriding bean definition for bean 'org.apache.cxf.bus.spring.Jsr250BeanPostProcessor': replacing [Generic bean: class [org.apache.cxf.bus.spring.Jsr250BeanPostProcessor]; scope=; abstract=false; lazyInit=false; autowireMode=0; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null; defined in class path resource [META-INF/cxf/cxf.xml]] with [Generic bean: class [org.apache.cxf.bus.spring.Jsr250BeanPostProcessor]; scope=; abstract=false; lazyInit=false; autowireMode=0; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null; defined in class path resource [META-INF/cxf/cxf.xml]]
Feb 07, 2018 1:19:46 PM org.springframework.beans.factory.support.DefaultListableBeanFactory registerBeanDefinition
INFO: Overriding bean definition for bean 'org.apache.cxf.bus.spring.BusExtensionPostProcessor': replacing [Generic bean: class [org.apache.cxf.bus.spring.BusExtensionPostProcessor]; scope=; abstract=false; lazyInit=false; autowireMode=0; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null; defined in class path resource [META-INF/cxf/cxf.xml]] with [Generic bean: class [org.apache.cxf.bus.spring.BusExtensionPostProcessor]; scope=; abstract=false; lazyInit=false; autowireMode=0; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null; defined in class path resource [META-INF/cxf/cxf.xml]]
Feb 07, 2018 1:19:46 PM org.springframework.beans.factory.xml.XmlBeanDefinitionReader loadBeanDefinitions
INFO: Loading XML bean definitions from class path resource [META-INF/cxf/cxf-servlet.xml]
Feb 07, 2018 1:19:46 PM org.springframework.beans.factory.support.DefaultListableBeanFactory preInstantiateSingletons
INFO: Pre-instantiating singletons in org.springframework.beans.factory.support.DefaultListableBeanFactory@5e5cab27: defining beans [cxf,org.apache.cxf.bus.spring.BusWiringBeanFactoryPostProcessor,org.apache.cxf.bus.spring.Jsr250BeanPostProcessor,org.apache.cxf.bus.spring.BusExtensionPostProcessor,*.*,cms-segurosMobile]; root of factory hierarchy
Feb 07, 2018 1:19:46 PM org.apache.cxf.service.factory.ReflectionServiceFactoryBean buildServiceFromClass
INFO: Creating Service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService from class com.qbe.services.mobile.SegurosMobileService
Feb 07, 2018 1:19:47 PM org.apache.cxf.service.factory.ReflectionServiceFactoryBean fillInSchemaCrossreferences
SEVERE: Schema element {http://cms.services.qbe.com/segurosmobile}UsuarioInicio references undefined type UsuarioInicio for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.
<Feb 7, 2018 1:19:47,281 PM ART> <Error> <org.apache.cxf.service.factory.ReflectionServiceFactoryBean> <BEA-000000> <Schema element {http://cms.services.qbe.com/segurosmobile}UsuarioInicio references undefined type UsuarioInicio for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.>
Feb 07, 2018 1:19:47 PM org.apache.cxf.service.factory.ReflectionServiceFactoryBean fillInSchemaCrossreferences
SEVERE: Schema element {http://cms.services.qbe.com/segurosmobile}DataIngresoPaso1 references undefined type DataIngresoPaso1 for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.
<Feb 7, 2018 1:19:47,282 PM ART> <Error> <org.apache.cxf.service.factory.ReflectionServiceFactoryBean> <BEA-000000> <Schema element {http://cms.services.qbe.com/segurosmobile}DataIngresoPaso1 references undefined type DataIngresoPaso1 for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.>
Feb 07, 2018 1:19:47 PM org.apache.cxf.service.factory.ReflectionServiceFactoryBean fillInSchemaCrossreferences
SEVERE: Schema element {http://cms.services.qbe.com/segurosmobile}dataIngresoPaso2 references undefined type dataIngresoPaso2 for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.
<Feb 7, 2018 1:19:47,282 PM ART> <Error> <org.apache.cxf.service.factory.ReflectionServiceFactoryBean> <BEA-000000> <Schema element {http://cms.services.qbe.com/segurosmobile}dataIngresoPaso2 references undefined type dataIngresoPaso2 for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.>
Feb 07, 2018 1:19:47 PM org.apache.cxf.service.factory.ReflectionServiceFactoryBean fillInSchemaCrossreferences
SEVERE: Schema element {http://cms.services.qbe.com/segurosmobile}IngresoErroneo references undefined type DataIngresoPaso2IngresoErroneo for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.
<Feb 7, 2018 1:19:47,283 PM ART> <Error> <org.apache.cxf.service.factory.ReflectionServiceFactoryBean> <BEA-000000> <Schema element {http://cms.services.qbe.com/segurosmobile}IngresoErroneo references undefined type DataIngresoPaso2IngresoErroneo for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.>
Feb 07, 2018 1:19:47 PM org.apache.cxf.service.factory.ReflectionServiceFactoryBean fillInSchemaCrossreferences
SEVERE: Schema element {http://cms.services.qbe.com/segurosmobile}DataInicio references undefined type DataInicio for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.
<Feb 7, 2018 1:19:47,283 PM ART> <Error> <org.apache.cxf.service.factory.ReflectionServiceFactoryBean> <BEA-000000> <Schema element {http://cms.services.qbe.com/segurosmobile}DataInicio references undefined type DataInicio for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.>
Feb 07, 2018 1:19:47 PM org.apache.cxf.service.factory.ReflectionServiceFactoryBean fillInSchemaCrossreferences
SEVERE: Schema element {http://cms.services.qbe.com/segurosmobile}IngresoActivo references undefined type DataIngresoPaso2IngresoActivo for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.
<Feb 7, 2018 1:19:47,283 PM ART> <Error> <org.apache.cxf.service.factory.ReflectionServiceFactoryBean> <BEA-000000> <Schema element {http://cms.services.qbe.com/segurosmobile}IngresoActivo references undefined type DataIngresoPaso2IngresoActivo for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.>
Feb 07, 2018 1:19:47 PM org.apache.cxf.service.factory.ReflectionServiceFactoryBean fillInSchemaCrossreferences
SEVERE: Schema element {http://cms.services.qbe.com/segurosmobile}EnviarCertificadosPorMailLog references undefined type EnviarCertificadosPorMailLog for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.
<Feb 7, 2018 1:19:47,284 PM ART> <Error> <org.apache.cxf.service.factory.ReflectionServiceFactoryBean> <BEA-000000> <Schema element {http://cms.services.qbe.com/segurosmobile}EnviarCertificadosPorMailLog references undefined type EnviarCertificadosPorMailLog for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.>
Feb 07, 2018 1:19:47 PM org.apache.cxf.service.factory.ReflectionServiceFactoryBean fillInSchemaCrossreferences
SEVERE: Schema element {http://cms.services.qbe.com/segurosmobile}PrimerIngreso references undefined type DataIngresoPaso2PrimerIngreso for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.
<Feb 7, 2018 1:19:47,284 PM ART> <Error> <org.apache.cxf.service.factory.ReflectionServiceFactoryBean> <BEA-000000> <Schema element {http://cms.services.qbe.com/segurosmobile}PrimerIngreso references undefined type DataIngresoPaso2PrimerIngreso for service {http://mobile.services.qbe.com/}SegurosMobileServiceImplService.>
Feb 07, 2018 1:19:47 PM org.apache.cxf.endpoint.ServerImpl initDestination
INFO: Setting the server's publish address to be /SegurosMobile

voy a la consola y le doy start servicing all requests

 ~/dev/migracioncomplus/dev   master±  curl http://localhost:7001/CMS-SegurosMobileServices/SegurosMobile
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><soap:Fault><faultcode>soap:Server</faultcode><faultstring>No such operation:  (HTTP GET PATH_INFO: /CMS-SegurosMobileServices/SegurosMobile)</faultstring></soap:Fault></soap:Body></soap:Envelope>%

bueno parece q algo anda

curl http://localhost:7001/CMS-SegurosMobileServices/SegurosMobile\?wsdl

devuelve ok

pruebo. Tomo los casos de test_cases_segmobile_produccion.txt

curl --data @getSucursales.xml -v -X POST http://localhost:7001/CMS-SegurosMobileServices/SegurosMobile
anduvo

bueno, para seguir probando necesito el weblogic de ellos y datos posta, porque tengo que recueprar una póliza específica

Ah conseguí un ejemplo de la doc.

lo grabo como enviarCertificadosPorMail.xml

curl --data @enviarCertificadosPorMail.xml -v -X POST http://localhost:7001/CMS-SegurosMobileServices/SegurosMobile

Me faltó definir el cms.config.properties en el entorno

Agrego en el script  ~/bin/fmw_12/wls12213/user_projects/domains/base_domain  vi bin/startWebLogic.sh

JAVA_OPTIONS="${SAVE_JAVA_OPTIONS} -Dcms.config.properties=/Users/ramiro/dev/migracioncomplus/dev/cms.config.properties"

y:
~/dev/migracioncomplus/dev/CMS-SegurosMobileServices   master±  touch /Users/ramiro/dev/migracioncomplus/dev/cms.config.properties

a ver si anda

Parece que le falta definir un valor para osbSvcURL...
veo si tengo archivos de profile de esto

~/dev/migracioncomplus/dev   master±  ln -s ../CMS-properties/dev.properties cms.config.properties
arg el valor es un url  file://
edito el script

pruebo...
a ojo, da error al ejecutar:
SEVERE: Al ejecutar lbaw_OVImprimirPoliza
com.qbe.services.cms.osbconnector.OSBConnectorException: javax.xml.ws.WebServiceException: java.io.FileNotFoundException: Response: '404: Not Found' for url: 'http://localhost:7001/CMS-fewebservices/MigratedComponentService?wsdl'

o sea que ese servicio tiene que estar disponible, no está embebido, y es en particular el valor de osbSvcURL


--------------

16/2/2018

Viendo las diferencias entre el war actual y el nuevo.
Cambios que hice:
- toqué el CMS-SegurosMobile/spring-beans-seguros-mobile.xml
- toqué el default properties en cms-common

Comparo viendo
 ~/dev/migracioncomplus/dev/CMS-SegurosMobileServices/comparetaVersiones   master±  diff -r CMS-SegurosMobileServices-2.3.66 CMS-SegurosMobileServices-3.1.1
 Las diferencias son:
 - build con un jdk distinto ( 8 en mi máquina, 6 en el original )
 - trajo versiones nuevas de algunas deps


veo de hacer el build con un Build-Jdk: 1.6.0_32
hm no hay jdk6 para mac ya...
docker? docker run -it --rm dockerfile/java:oracle-java8 java -version

bueno veo las diffs

b/dev/CMS-common/src/main/resources/default.properties

+# Seguros Mobile
+segurosmobile.smtpserver=ARP201VWNCAP.QBE-AR.localdomain

diff --git a/dev/cms.config.properties b/dev/cms.config.properties
new file mode 120000
index 000000000..3f7cf8397
--- /dev/null
+++ b/dev/cms.config.properties
@@ -0,0 +1 @@
+../CMS-properties/dev.properties
\ No newline at end of file

viendo historia ( lo que está ahora es 2.3.66 )


commit 436c8941ddfa9d381d18c47bcd2453efff82d4d0
Author: Ramiro González Maciel <ramiro@snoopconsulting.com>
Date:   Fri Oct 30 15:51:52 2015 -0300

    primera versión 3.0.1

diff --git a/dev/CMS-SegurosMobileServices/pom.xml b/dev/CMS-SegurosMobileServices/pom.xml
index 99c1d69df..475ff54ac 100644
--- a/dev/CMS-SegurosMobileServices/pom.xml
+++ b/dev/CMS-SegurosMobileServices/pom.xml
@@ -5,7 +5,7 @@
    <parent>
       <groupId>com.qbe.integration</groupId>
       <artifactId>cms</artifactId>
-      <version>2.3.66-SNAPSHOT</version>
+      <version>3.0.1-SNAPSHOT</version>

    </parent>

commit b92bc978a269615f22a808303944057bcb34f600
Author: gtsnoop <gtsnoop@github.com>
Date:   Wed Feb 18 17:17:26 2015 +0000

    [maven-release-plugin] prepare for next development iteration

diff --git a/dev/CMS-SegurosMobileServices/pom.xml b/dev/CMS-SegurosMobileServices/pom.xml
index f1ce9db76..99c1d69df 100644
--- a/dev/CMS-SegurosMobileServices/pom.xml
+++ b/dev/CMS-SegurosMobileServices/pom.xml
@@ -5,7 +5,7 @@
    <parent>
       <groupId>com.qbe.integration</groupId>
       <artifactId>cms</artifactId>
-      <version>2.3.65</version>
+      <version>2.3.66-SNAPSHOT</version>

    </parent>

Lo que pasa es que es un engendro.
El último build lo hice yo, y después hay commits de gustavo ávila que tocó un CSV de sucursales
https://github.com/snoopconsulting/migracioncomplus/commit/e3188e76b69349d32fa315cde3ebb47ad2f82cf1

pero no sé como armó el war... no tocó el pom

Bueno pruebo con el nuevo.

Pido entonces que desplieguen el war a ver q hace

repaso plan:

Tareas:
1) Cambiar en el código el IP que está cableado para que lo tome de una property. Probrlo localmente. 4hs
DONE
2) Armar un nuevo release con esta versión. Solamente cambia la parte java. Validar anteriormente que estemos modificando la última versión. 4hs entre idas y vueltas y conseguir los datos para tener el detalle de qué está instalado en cada server
DONE
3) Desplegarlo en DESA y UAT y probar que funciona bien y no rompe nada.Es desplegar un war, lo hace Jock estimo, simple.
Pedido a JOCK
Ya lo desplegó
DONE
http://10.152.85.[29,30,31]:8000/CMS-SegurosMobileServices

4) Instalar en DESA y UAT la parte de OSB de SegurosMobile que Jorge Machado dice que no está instalada. Mejor dicho, asegurarse de que esté, si es necesario instalarla. Puede complicarse, tenemos los "deployment plans" y todo pero no sé si siguen siendo válidos después de 3 años
Dice JOCK que ya está andando

5) Hacer las pruebas de la nueva funcionalidad completa probando desde OSB. Hay scripts para probar esto que usábamos en las WO viejas. 1 día entre idas y vueltas
Viendo

6) Coordinar la puesta en prod. Entiendo que habría que avisarles a los de Quares que son los que hicieron la app mobile para que validen/prueben

7) Redesplegar el war y validar que todo funciona ok.





