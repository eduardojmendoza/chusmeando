package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class GetNroCotATM implements Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetNroCotATM";
  static final String mcteStoreProc = "SPSNCV_BRO_COTIZACION_ATM";
  /**
   * Archivo de Errores
   */
  static final String mcteArchivoATM_XML = "LBA_VALIDACION_COT_ATM.XML";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  static final String mcteParam_COBROCOD = "FPAGO";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_Provi = "PROVCOD";
  static final String mcteParam_LocalidadCod = "LOCALIDAD";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetNroCot
   * static variable for method: fncValidaMQ
   * static variable for method: fncGetInstaCodMQ
   * static variable for method: fncValidaPolizasMQ
   */
  private final String wcteFnName = "fncValidaPolizasMQ";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant pvarRes = new Variant();
    Variant wvarCodErr = new Variant();
    //
    //
    //Declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      pvarRes.set( "" );
      wvarCodErr.set( "" );
      wvarMensaje.set( "" );
      if( ! (invoke( "fncGetNroCot", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*unsup mobjCOM_Context.SetAbort() */;
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*unsup mobjCOM_Context.SetComplete() */;
        }
        return IAction_Execute;
      }
      //
      pvarResponse.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + pvarRes + "</LBA_WS>" );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetNroCot( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncGetNroCot = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXML = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarContChild = 0;
    int wvarContNode = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_PROVI = "";
    String mvarWDB_LOCALIDADCOD = "";
    String mvarWDB_COBROCOD = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_USUARCOD = "";
    Variant varPlanDefec = new Variant();

    //
    //
    //
    //
    //
    try 
    {
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_REQUESTID = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID ) */ );
      }
      //
      wvarStep = 30;
      mvarWDB_CODINST = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }
      //
      wvarStep = 40;
      mvarWDB_POLIZANN = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN ) */ );
      }
      //
      wvarStep = 50;
      mvarWDB_POLIZSEC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC ) */ );
      }
      //
      wvarStep = 70;
      mvarWDB_PROVI = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi ) */ );
      }
      //
      wvarStep = 80;
      mvarWDB_LOCALIDADCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_LOCALIDADCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCod ) */ );
      }
      //
      wvarStep = 90;
      mvarWDB_COBROCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD ) */ );
      }
      //
      wvarStep = 100;
      mvarWDB_COBROTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP ) */ );
      }
      //
      wobjXMLList = (org.w3c.dom.NodeList) null;
      //
      wvarStep = 130;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 140;
      wobjDBParm = new Parameter( "@WDB_CODINST", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      //wobjDBParm.NumericScale = 0
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 150;
      wobjDBParm = new Parameter( "@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      //wobjDBParm.NumericScale = 0
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 160;
      wobjDBParm = new Parameter( "@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      //wobjDBParm.NumericScale = 0
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 180;
      wobjDBParm = new Parameter( "@WDB_PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVI.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVI)) );
      //wobjDBParm.NumericScale = 0
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 190;
      wobjDBParm = new Parameter( "@WDB_CODPOST", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_LOCALIDADCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_LOCALIDADCOD)) );
      //wobjDBParm.NumericScale = 0
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_COBROCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_COBROCOD)) );
      //wobjDBParm.NumericScale = 0
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_COBROTIP.equals( "" ) ? "" : mvarWDB_COBROTIP) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      //wobjDBParm.NumericScale = 0
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 230;
      wobjDBParm = new Parameter( "@WDB_NROCOT", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      //wobjDBParm.NumericScale = 0
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 240;
      wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      //wobjDBParm.NumericScale = 0
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 245;
      wobjDBParm = new Parameter( "@WDB_USUARCOD", AdoConst.adChar, AdoConst.adParamInputOutput, 10, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 250;
      wrstRes = new Recordset();
      wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
      wrstRes.setActiveConnection( (Connection) null );

      wobjXML = new diamondedge.util.XmlDom();
      //unsup wobjXML.async = false;
      wobjXML.load( System.getProperty("user.dir") + "\\" + mcteArchivoATM_XML );

      wvarStep = 260;
      //SI NO HUBO ERROR AGREGO LOS DATOS AL XML DE ENTRADA PARA LA RESPUESTA
      if( ! (wrstRes.isEOF()) )
      {
        if( Strings.toUpperCase( Strings.trim( wrstRes.getFields().getField(0).getValue().toString() ) ).equals( "OK" ) )
        {
          if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request/TIPOOPERAC" ) */ ) ).equals( "C" ) )
          {
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "COT_NRO", "" ) */ );
          }
          wvarStep = 270;
          diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//COT_NRO" ) */, wobjDBCmd.getParameters().getParameter("@WDB_NROCOT").getValue().toString() );

          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CERTISEC", "" ) */ );
          diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//CERTISEC" ) */, wobjDBCmd.getParameters().getParameter("@WDB_CERTISEC").getValue().toString() );

          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "USUARCOD", "" ) */ );
          mvarWDB_USUARCOD = wobjDBCmd.getParameters().getParameter("@WDB_USUARCOD").getValue().toString();
          diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//USUARCOD" ) */, mvarWDB_USUARCOD );

          //pvarRes = wobjXMLRequest.xml
          wvarCodErr.set( 0 );
          wvarMensaje.set( "" );
        }
        else
        {
          //error
          wvarStep = 280;
          if( null /*unsup wobjXML.selectNodes( ("//ERRORES/ATM/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']") ) */.getLength() > 0 )
          {
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXML.selectSingleNode( "//ERRORES/ATM/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/TEXTOERROR" ) */ ) );
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXML.selectSingleNode( "//ERRORES/ATM/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/VALORRESPUESTA" ) */ ) );
          }
          else
          {
            wvarCodErr.set( General.mcteErrorInesperadoCod );
            wvarMensaje.set( General.mcteErrorInesperadoDescr );
          }
          //
          fncGetNroCot = false;
          return fncGetNroCot;
        }
      }
      else
      {
        //error
        wvarCodErr.set( -100 );
        wvarMensaje.set( "No se retornaron datos de la base" );
      }
      //
      wvarStep = 290;
      wobjXML = (diamondedge.util.XmlDom) null;

      if( invoke( "fncValidaMQ", new Variant[] { new Variant(mvarWDB_POLIZANN), new Variant(mvarWDB_POLIZSEC), new Variant(mvarWDB_COBROCOD), new Variant(mvarWDB_COBROTIP), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } ) )
      {
        if( invoke( "fncValidaPolizasMQ", new Variant[] { new Variant(mvarWDB_USUARCOD), new Variant(mvarWDB_POLIZSEC), new Variant(mvarWDB_POLIZANN), new Variant(varPlanDefec), new Variant(wvarCodErr), new Variant(wvarMensaje), new Variant(pvarRes) } ) )
        {
          //agrego el PlanDefec
          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "TPLANATM", "" ) */ );
          diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//TPLANATM" ) */, varPlanDefec.toString() );
          pvarRes.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
          fncGetNroCot = true;
        }
        else
        {
          fncGetNroCot = false;
        }
      }
      else
      {
        fncGetNroCot = false;
      }
      //
      wrstRes.close();
      wobjDBCnn.close();
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return fncGetNroCot;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wvarCodErr.set( General.mcteErrorInesperadoCod );
        wvarMensaje.set( General.mcteErrorInesperadoDescr );
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        fncGetNroCot = false;
        /*unsup mobjCOM_Context.SetComplete() */;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetNroCot;
  }

  private boolean fncValidaMQ( String varPolizann, String varPolizsec, String varCobrocod, String varCobrotip, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncValidaMQ = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String mvarRequest = "";
    String mvarResponse = "";
    Variant oProd = new Variant();
    diamondedge.util.XmlDom mobjXMLDoc = null;
    Object wobjClass = null;
    String mvarProdok = "";
    org.w3c.dom.NodeList wobjXMLList = null;
    int wvarContNode = 0;


    //f ch.r
    wvarStep = 0;
    wvarCodErr.set( 0 );
    try 
    {
      // valida cobro
      wvarStep = 2;

      mvarRequest = "<Request><DEFINICION>FormasDePagoxPolizaColectiva.xml</DEFINICION>";
      mvarRequest = mvarRequest + "<USUARCOD></USUARCOD>";
      mvarRequest = mvarRequest + "<RAMOPCOD>ATD1</RAMOPCOD>";
      mvarRequest = mvarRequest + "<POLIZANN>" + varPolizann + "</POLIZANN>";
      mvarRequest = mvarRequest + "<POLIZSEC>" + varPolizsec + "</POLIZSEC></Request>";

      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( new Variant( mvarRequest ), new Variant( mvarResponse ), "" );
      wobjClass = null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );

      mvarProdok = "ER";

      if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {

          wobjXMLList = null /*unsup mobjXMLDoc.selectNodes( "//CAMPOS/T-PAGOS-SAL/FORMADEPAGO" ) */;
          for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
          {
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( "COBROCOD" ) */ ).equals( varCobrocod ) )
            {
              if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( "COBROTIP" ) */ ).equals( varCobrotip ) )
              {
                mvarProdok = "OK";
              }
            }
          }
        }
      }

      if( mvarProdok.equals( "ER" ) )
      {
        wvarCodErr.set( -53 );
        wvarMensaje.set( "Poliza colectiva no habilitada / Forma de Pago invalida" );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        fncValidaMQ = false;
        return fncValidaMQ;
      }


      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      wobjXMLList = (org.w3c.dom.NodeList) null;

      wvarStep = 240;
      if( wvarCodErr.toInt() == 0 )
      {
        fncValidaMQ = true;
      }
      else
      {
        fncValidaMQ = false;
      }

      fin: 
      return fncValidaMQ;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );
        wvarMensaje.set( General.mcteErrorInesperadoDescr );
        wvarCodErr.set( -1000 );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        fncValidaMQ = false;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncValidaMQ;
  }

  /**
   *  Devuelve el BANCOCOD o INSTACOD a partir del USUARCOD
   */
  private String fncGetInstaCodMQ( String varUsuarcod, Variant wvarCodErr, Variant wvarMensaje, Variant pvarRes )
  {
    String fncGetInstaCodMQ = "";
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String mvarRequest = "";
    String mvarResponse = "";
    Variant oProd = new Variant();
    diamondedge.util.XmlDom mobjXMLDoc = null;
    Object wobjClass = null;
    String mvarProdok = "";
    org.w3c.dom.NodeList wobjXMLList = null;
    int wvarContNode = 0;



    //f ch.r
    wvarStep = 0;
    wvarCodErr.set( 0 );
    try 
    {
      // valida cobro
      wvarStep = 2;


      mvarRequest = "<Request><DEFINICION>1000_PerfilUsuario.xml</DEFINICION>";
      mvarRequest = mvarRequest + "<AplicarXSL>1000_PerfilUsuario.xsl</AplicarXSL>";
      mvarRequest = mvarRequest + "<USUARIO>" + varUsuarcod + "</USUARIO></Request>";


      wobjClass = new lbawA_OVMQGen.lbaw_MQMensajeGestion();
      wobjClass.Execute( new Variant( mvarRequest ), new Variant( mvarResponse ), "" );
      wobjClass = null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );

      mvarProdok = "ER";
      if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {
          fncGetInstaCodMQ = diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/BANCOCOD" ) */ );
          mvarProdok = "OK";
        }
        else
        {
          mvarProdok = "ER";
        }
      }

      if( mvarProdok.equals( "ER" ) )
      {
        wvarCodErr.set( -53 );
        wvarMensaje.set( "Usuario Incorrecto / No posee banco asignado" );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetInstaCodMQ;
      }


      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      wobjXMLList = (org.w3c.dom.NodeList) null;

      wvarStep = 240;

      fin: 
      return fncGetInstaCodMQ;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );
        wvarCodErr.set( -1000 );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        //fncValidaMQ = False
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetInstaCodMQ;
  }

  private boolean fncValidaPolizasMQ( String varUsuarcod, String varPolizsec, String varPolizann, Variant varPlanDefec, Variant wvarCodErr, Variant wvarMensaje, Variant pvarRes )
  {
    boolean fncValidaPolizasMQ = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String mvarRequest = "";
    String mvarResponse = "";
    String varInstaCod = "";
    Variant oProd = new Variant();
    diamondedge.util.XmlDom mobjXMLDoc = null;
    Object wobjClass = null;
    String mvarProdok = "";
    org.w3c.dom.NodeList wobjXMLList = null;
    int wvarContNode = 0;



    //f ch.r
    wvarStep = 0;
    wvarCodErr.set( 0 );
    try 
    {
      // valida cobro
      wvarStep = 2;

      // Obtengo el varInstaCod
      varInstaCod = invoke( "fncGetInstaCodMQ", new Variant[] { new Variant(varUsuarcod), new Variant(wvarCodErr), new Variant(wvarMensaje), new Variant(pvarRes) } );

      mvarRequest = "<Request><DEFINICION>2130_PolizasxCanalHabilOv.xml</DEFINICION>";
      mvarRequest = mvarRequest + "<USUARCOD>" + varUsuarcod + "</USUARCOD>";
      mvarRequest = mvarRequest + "<INSTACOD>" + varInstaCod + "</INSTACOD>";
      mvarRequest = mvarRequest + "<RAMOPCOD>ATD1</RAMOPCOD></Request>";

      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( new Variant( mvarRequest ), new Variant( mvarResponse ), "" );
      wobjClass = null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );

      mvarProdok = "ER";
      if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {
          if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//CANTIDAD" ) */ ) ) > 0 )
          {
            wobjXMLList = null /*unsup mobjXMLDoc.selectNodes( "//CAMPOS/COBERTURAS/COBERTURA" ) */;

            for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
            {
              if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( "POLIZANN" ) */ ).equals( varPolizann ) )
              {
                if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( "POLIZSEC" ) */ ).equals( varPolizsec ) )
                {
                  mvarProdok = "OK";
                  // obtengo el plan por defecto
                  varPlanDefec.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( "PLANDEFEC" ) */ ) );
                }
              }
            }
          }
        }
      }

      if( mvarProdok.equals( "ER" ) )
      {
        wvarCodErr.set( -53 );
        wvarMensaje.set( "Poliza  no habilitada para el usuario" );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        fncValidaPolizasMQ = false;
        return fncValidaPolizasMQ;
      }


      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      wobjXMLList = (org.w3c.dom.NodeList) null;

      wvarStep = 240;
      if( wvarCodErr.toInt() == 0 )
      {
        fncValidaPolizasMQ = true;
      }
      else
      {
        fncValidaPolizasMQ = false;
      }

      fin: 
      return fncValidaPolizasMQ;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );
        wvarCodErr.set( -1000 );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        fncValidaPolizasMQ = false;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncValidaPolizasMQ;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
