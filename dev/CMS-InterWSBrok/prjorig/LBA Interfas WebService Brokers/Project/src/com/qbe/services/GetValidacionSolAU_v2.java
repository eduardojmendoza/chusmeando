package com.qbe.services.interWSBrok.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class GetValidacionSolAU_v2 implements VBObjectClass
{
  static final String mcteClassName = "LBA_InterWSBrok.GetValidacionSolAU";
  static final String mcteStoreProc = "SPSNCV_BRO_VALIDA_SOL_SCO";
  static final String mcteStoreProcSelect = "SPSNCV_BRO_SELECT_OPER_ESP";
  static final String mcteArchivoAUSSOL_XML = "LBA_VALIDACION_SOL_AU.XML";
  /**
   *  Parametros XML de Entrada
   */
  static final String mcteParam_Cliensec = "CERTISEC";
  static final String mcteParam_NacimAnn = "NACIMANN";
  static final String mcteParam_NacimMes = "NACIMMES";
  static final String mcteParam_NacimDia = "NACIMDIA";
  static final String mcteParam_Sexo = "SEXO";
  static final String mcteParam_Estado = "ESTADO";
  /**
   * jc 09/2010 en la cotizaci�n tiene otro nombre el tag
   * Const mcteParam_CLIENIVA       As String = "CLIENIVA"
   */
  static final String mcteParam_CLIENIVA = "IVA";
  static final String mcteParam_ModeAutCod = "MODEAUTCOD";
  static final String mcteParam_KMsrngCod = "KMSRNGCOD";
  static final String mcteParam_EfectAnn = "EFECTANN";
  static final String mcteParam_SiGarage = "SIGARAGE";
  static final String mcteParam_SumAseg = "SUMAASEG";
  static final String mcteParam_Siniestros = "SINIESTROS";
  static final String mcteParam_Gas = "GAS";
  static final String mcteParam_Provi = "PROVI";
  static final String mcteParam_LocalidadCod = "LOCALIDADCOD";
  /**
   * Revisar
   */
  static final String mcteParam_CampaCod = "CAMPACOD";
  /**
   * Revisar
   */
  static final String mcteParam_DatosPlan = "DATOSPLAN";
  static final String mcteParam_ClubLBA = "CLUBLBA";
  /**
   * Revisar
   */
  static final String mcteParam_Portal = "PORTAL";
  /**
   * jc 09/2010 en la cotizaci�n tiene otro nombre el tag
   * Const mcteParam_PRODUCTOR       As String = "PRODUCTOR"
   */
  static final String mcteParam_PRODUCTOR = "AGECOD";
  static final String mcteParam_Permite_GNC = "PERMITE_GNC";
  static final String mcteParam_VEHDES = "VEHDES";
  static final String mcteParam_CODZONA = "CODZONA";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_NRO_OPERACION_BROKER = "REQUESTID";
  static final String mcteParam_CODPROVCO = "PROVI";
  static final String mcteParam_CODPOSTCO = "LOCALIDADCOD";
  static final String mcteParam_COBROCOD = "COBROCOD";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_CUENNUME = "CUENNUME";
  static final String mcteParam_SUMAASEG = "SUMAASEG";
  static final String mcteParam_COLOR = "VEHCLRCOD";
  static final String mcteParam_DOCUMTIP_TIT = "TIPODOCU";
  static final String mcteParam_PLANNCOD = "PLANNCOD";
  static final String mcteParam_FRANQCOD = "FRANQCOD";
  static final String mcteParam_INSPECCION = "INSPECOD";
  static final String mcteParam_RASTREO = "RASTREO";
  static final String mcteParam_CODIGO_ALARMA = "ALARMCOD";
  static final String mcteParam_CATEGO = "AUCATCOD";
  /**
   * Accesorios
   */
  static final String mcteNodos_Accesorios = "//Request/ACCESORIOS/ACCESORIO";
  static final String mcteParam_PrecioAcc = "PRECIOACC";
  static final String mcteParam_DescripcionAcc = "DESCRIPCIONACC";
  static final String mcteParam_CodigoAcc = "CODIGOACC";
  /**
   * Hijos
   */
  static final String mcteNodos_Hijos = "//Request/HIJOS/HIJO";
  static final String mcteParam_SexoHijo = "SEXOHIJO";
  static final String mcteParam_EstadoHijo = "ESTADOHIJO";
  static final String mcteParam_NacimHijo = "NACIMHIJO";
  static final String mcteParam_NombreHijo = "NOMBREHIJO";
  static final String mcteParam_ApellidoHijo = "APELLIDOHIJO";
  static final String mcteParam_EdadHijo = "EDADHIJO";
  static final String mcteParam_TipoDocuHijo = "TIPODOCUHIJO";
  static final String mcteParam_NroDocuHijo = "NRODOCUHIJO";
  /**
   * jc 8/2010 agregado xml
   */
  static final String mcteParam_DESTRUCCION_80 = "DESTRUCCION_80";
  static final String mcteParam_Luneta = "LUNETA";
  static final String mcteParam_ClubEco = "CLUBECO";
  static final String mcteParam_Robocont = "ROBOCONT";
  static final String mcteParam_Granizo = "GRANIZO";
  static final String mcteParam_INSTALADP = "INSTALADP";
  static final String mcteParam_POSEEDISP = "POSEEDISP";
  /**
   * JC 2010-11 ch.r.
   */
  static final String mcteParam_CLIENTIP = "CLIENTIP";
  /**
   * JC 2010-11 ch.r.
   */
  static final String mcteParam_IBB = "IBB";
  /**
   * JC 2010-11 ch.r.
   */
  static final String mcteParam_CUITNUME = "CUITNUME";
  /**
   * JC 2010-11 ch.r.
   */
  static final String mcteParam_NROIBB = "NROIBB";
  /**
   * JC 2010-11 ch.r.
   */
  static final String mcteParam_RAZONSOC = "RAZONSOC";
  /**
   * AM
   */
  static final String mcteParam_ACREPREN = "//ACRE-PREN";
  static final String mcteParam_NUMEDOCUACRE = "//NUMEDOCU-ACRE";
  static final String mcteParam_TIPODOCUACRE = "//TIPODOCU-ACRE";
  static final String mcteParam_APELLIDOACRE = "//APELLIDO-ACRE";
  static final String mcteParam_NOMBRESACRE = "//NOMBRES-ACRE";
  static final String mcteParam_DOMICDOMACRE = "//DOMICDOM-ACRE";
  static final String mcteParam_DOMICDNUACRE = "//DOMICDNU-ACRE";
  static final String mcteParam_DOMICPISACRE = "//DOMICPIS-ACRE";
  static final String mcteParam_DOMICPTAACRE = "//DOMICPTA-ACRE";
  static final String mcteParam_DOMICPOBACRE = "//DOMICPOB-ACRE";
  static final String mcteParam_DOMICCPOACRE = "//DOMICCPO-ACRE";
  static final String mcteParam_PROVICODACRE = "//PROVICOD-ACRE";
  static final String mcteParam_PAISSCODACRE = "//PAISSCOD-ACRE";
  static final String mcteParam_EmpresaCodigo = "//CANAL";
  static final String mcteParam_SucursalCodigo = "//CANAL_SUCURSAL";
  static final String mcteParam_LegajoVend = "//LEGAJO_VEND";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncIntegracionAIS
   * static variable for method: fncValidaABase
   * static variable for method: fncComparaConCotizacion
   */
  private final String wcteFnName = "fncComparaConCotizacion";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    Variant mcteErrorInesperadoDescr = new Variant();
    int wvarStep = 0;
    Variant wvarCodErr = new Variant();
    Variant wvarMensaje = new Variant();
    Variant pvarRes = new Variant();
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      pvarRes.set( "" );
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        wvarStep = 20;
        IAction_Execute = 0;
        /*TBD mobjCOM_Context.SetComplete() ;*/
        return IAction_Execute;
      }
      wvarStep = 30;
      pvarResponse.set( pvarRes );
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<Response><Estado resultado=\"false\" mensaje=\"" + mcteErrorInesperadoDescr + "\" /></Response>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetComplete() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    Variant mcteErrorInesperadoDescr = new Variant();
    Object wobjClass = null;
    XmlDomExtended wobjXMLAux = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLRes = null;
    XmlDomExtended wobjXMLCheck = null;
    XmlDomExtended wobjXMLStatusTarjeta = null;
    XmlDomExtended wobjXMLStatusCanal = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.NodeList wobjXMLNodeList = null;
    org.w3c.dom.Node wobjXMLNodeError = null;
    org.w3c.dom.Node wobjXMLNodeSucu = null;
    XmlDomExtended wobjXMLNodeCober = null;
    com.qbe.services.HSBCInterfaces.IDBConnection wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarContChild = 0;
    int wvarContNode = 0;
    int wvarcounter = 0;
    String wvarErrorCodigo = "";
    int wvarStep = 0;
    String wvarTarjeta = "";
    String wvarStatusTarjeta = "";
    String wvarBancoCod = "";
    String wvarExisteSucu = "";
    String mvarRequest = "";
    String wvarResponse = "";
    String mvarWDB_USUARCOD = "";
    String mvarCUENNUME = "";
    java.util.Date wvarFechaActual = DateTime.EmptyDate;
    java.util.Date wvarFechaVigencia = DateTime.EmptyDate;





    wvarStep = 0;
    wvarCodErr.set( 0 );

    try 
    {
      //Chequeo que los datos esten OK
      //Chequeo que no venga nada en blanco
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      wobjXMLCheck = new XmlDomExtended();
      wobjXMLCheck.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteArchivoAUSSOL_XML));

      //Chequeo nodo padre
      wobjXMLNodeError = wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/VACIOS/CAMPOS" ) ;
      //Tomo codigo de error de Vacios
      wvarErrorCodigo = XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/VACIOS/N_ERROR" )  );

      wvarStep = 20;
      for( wvarContChild = 0; wvarContChild <= (wobjXMLNodeError.getChildNodes().getLength() - 1); wvarContChild++ )
      {
        if( ! (wobjXMLRequest.selectSingleNode( ("//" + wobjXMLNodeError.getChildNodes().item( wvarContChild ).getNodeName()) )  == (org.w3c.dom.Node) null) )
        {
          if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + wobjXMLNodeError.getChildNodes().item( wvarContChild ).getNodeName()) )  ) ).equals( "" ) )
          {
            //error
            wvarCodErr.set( wvarErrorCodigo );
            wvarMensaje.set( XmlDomExtended.getText( wobjXMLNodeError.getChildNodes().item( wvarContChild ) ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
        else
        {
          //error
          wvarCodErr.set( -1 );
          wvarMensaje.set( XmlDomExtended.getText( wobjXMLNodeError.getChildNodes().item( wvarContChild ) ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }


      //Chequeo Tipo y Longitud de Datos para el XML Padre
      wvarStep = 30;
      for( wvarContChild = 0; wvarContChild <= (wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().getLength() - 1); wvarContChild++ )
      {
        if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
        {
          //Si el Codigo de Cobro no es tarjeta, no chequea el Largo de Tarjeta y su Vencimiento
          if( ! ((VB.val( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) )  ) ) == 4)) && ((null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "CUENNUME" )) || (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "VENCIANN" )) || (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "VENCIMES" )) || (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "VENCIDIA" ))) )
          {
            //unsup GoTo Siguiente
          }
          //Chequeo la longitud y si tiene Largo Fijo
          if( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
          {
            if( ! (Strings.len( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ) ) ) == Obj.toInt( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") ) */ ) )) )
            {
              wvarCodErr.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
          else
          {
            if( Strings.len( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ) ) ) > Obj.toInt( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") ) */ ) ) )
            {
              wvarCodErr.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }

          //Chequeo el Tipo
          if( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
          {
            if( ! (new Variant( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ) ) ).isNumeric()) )
            {
              wvarCodErr.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            else
            {
              //Chequeo que el dato numerico no sea 0
              //Excluye los nodos COT_NRO, SINIESTROS, RASTREO, INSPECOD Y ALARMCOD
              if( (Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ) ) ) == 0) && ! (((wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ).getNodeName().equals( "COT_NRO" )) || (wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ).getNodeName().equals( "ALARMCOD" )) || (wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ).getNodeName().equals( "RASTREO" )) || (wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ).getNodeName().equals( "INSPECOD" )) || (wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ).getNodeName().equals( "SINIESTROS" )))) )
              {
                wvarCodErr.set( -199 );
                wvarMensaje.set( "El valor del dato numerico no puede estar en 0" );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
          }
        }
        Siguiente: 
          ;
      }

      //Chequeo Tipo y Longitud de Datos para Accesorios
      for( wvarContChild = 0; wvarContChild <= (wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) .getLength() - 1); wvarContChild++ )
      {
        wvarStep = 40;
        for( wvarContNode = 0; wvarContNode <= (wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) .item( wvarContChild ).getChildNodes().getLength() - 1); wvarContNode++ )
        {
          if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
          {
            //Chequeo la longitud y si tiene Largo Fijo
            if( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
            {
              if( ! (Strings.len( XmlDomExtended.getText( wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == Obj.toInt( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") )  ) )) )
              {
                wvarCodErr.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
            else
            {
              if( Strings.len( XmlDomExtended.getText( wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) > Obj.toInt( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") )  ) ) )
              {
                wvarCodErr.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }

            //Chequeo el Tipo
            if( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
            {
              if( ! (new Variant( XmlDomExtended.getText( wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) .item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ).isNumeric()) )
              {
                wvarCodErr.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
          }
        }
      }

      //Chequeo Tipo y Longitud de Datos de Hijos
      wvarStep = 50;
      for( wvarContChild = 0; wvarContChild <= (wobjXMLRequest.selectNodes( mcteNodos_Hijos ) .getLength() - 1); wvarContChild++ )
      {
        for( wvarContNode = 0; wvarContNode <= (wobjXMLRequest.selectNodes( mcteNodos_Hijos ) .item( wvarContChild ).getChildNodes().getLength() - 1); wvarContNode++ )
        {
          if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
          {
            //Chequeo la longitud y si tiene Largo Fijo
            if( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
            {
              if( ! (Strings.len( XmlDomExtended.getText( wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == Obj.toInt( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") )  ) )) )
              {
                wvarCodErr.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
            else
            {
              if( Strings.len( XmlDomExtended.getText( wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) > Obj.toInt( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") )  ) ) )
              {
                wvarCodErr.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }

            //Chequeo el Tipo
            if( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
            {
              if( ! (new Variant( XmlDomExtended.getText( wobjXMLRequest.selectNodes( mcteNodos_Hijos ) .item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ).isNumeric()) )
              {
                wvarCodErr.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( XmlDomExtended.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
          }
        }
      }

      //valido los datos
      wvarStep = 60;
      wobjXMLNodeError = wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS" ) ;
      for( wvarContChild = 0; wvarContChild <= (wobjXMLNodeError.getChildNodes().getLength() - 1); wvarContChild++ )
      {
        if( ! (wobjXMLRequest.selectSingleNode( ("//" + wobjXMLNodeError.getChildNodes().item( wvarContChild ).getNodeName()) )  == (org.w3c.dom.Node) null) )
        {
          if( null /*unsup wobjXMLNodeError.getChildNodes().item( wvarContChild ).selectNodes( ("VALOR[.='" + XmlDomExtended.getText( null (*unsup wobjXMLRequest.selectSingleNode( "//" + wobjXMLNodeError.getChildNodes().item( wvarContChild ).getNodeName() ) *) ) + "']") ) */.getLength() == 0 )
          {
            //error
            wvarCodErr.set( XmlDomExtended.getText( null /*unsup wobjXMLNodeError.getChildNodes().item( wvarContChild ).selectSingleNode( "N_ERROR" ) */ ) );
            wvarMensaje.set( XmlDomExtended.getText( null /*unsup wobjXMLNodeError.getChildNodes().item( wvarContChild ).selectSingleNode( "TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }

      //Chequeo EDAD
      wvarStep = 70;
      if( ! (invoke( "ValidarFechayRango", new Variant[] { new Variant(XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( new Variant("//" + mcteParam_NacimDia) )  ) + "/" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( new Variant(("//" + mcteParam_NacimMes)) )  ) + "/" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( new Variant(("//" + mcteParam_NacimAnn)) )  )), new Variant(17), new Variant(86), new Variant(wvarMensaje) } ).toBoolean()) )
      {
        //error
        wvarCodErr.set( -9 );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }

      //Valida la fecha de la Tarjeta (si Viene)
      wvarStep = 80;
      if( Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) )  ) ) == 4 )
      {
        if( ! ((wobjXMLRequest.selectSingleNode( "//VENCIDIA" )  == (org.w3c.dom.Node) null) && (wobjXMLRequest.selectSingleNode( "//VENCIMES" )  == (org.w3c.dom.Node) null) && (wobjXMLRequest.selectSingleNode( "//VENCIANN" )  == (org.w3c.dom.Node) null)) )
        {
          if( ! (invoke( "validarFecha", new Variant[] { new Variant(XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( new Variant("//VENCIDIA") )  ) + "/" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( new Variant("//VENCIMES") )  ) + "/" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( new Variant("//VENCIANN") )  )), new Variant(wvarMensaje) } ).toBoolean()) )
          {
            //error
            wvarCodErr.set( -13 );
            wvarMensaje.set( "Fecha de Tarjeta Invalida" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }


        //Valida el nro. de Tarjeta
        wvarStep = 92;

        mvarCUENNUME = "0";
        if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) )  == (org.w3c.dom.Node) null) )
        {
          mvarCUENNUME = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CUENNUME )  );
        }

        wvarTarjeta = "<Request>" + "<USUARIO/>" + "<COBROTIP>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) )  ) + "</COBROTIP>" + "<CTANUM>" + mvarCUENNUME + "</CTANUM>" + "<VENANN>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VENCIANN" )  ) + "</VENANN>" + "<VENMES>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VENCIMES" )  ) + "</VENMES>" + "</Request>";

        wvarStep = 94;
        //jc 09/2010 se reemplaza componente
        wobjClass = new lbawA_OVMQCotizar.lbaw_OVValidaCuentas();
        //error: function 'Execute' was not found.
        //unsup: Call wobjClass.Execute(wvarTarjeta, wvarStatusTarjeta, "")
        wobjClass = null;

        wvarStep = 96;
        wobjXMLStatusTarjeta = new XmlDomExtended();
        wobjXMLStatusTarjeta.loadXML( wvarStatusTarjeta );
        wobjXMLStatusTarjeta.loadXML( "<Response><Estado resultado='true' mensaje=''/></Response>" );

        wvarStep = 98;
        //jc 09/2010 se reemplaza componente
        //If Not UCase(wobjXMLStatusTarjeta.selectSingleNode("//VERIFICACION").Text) = "TRUE" Then
        if( XmlDomExtended.getText( wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
        {
          wvarCodErr.set( -198 );
          //jc 09/2010
          //wvarMensaje = "Numero de Tarjeta de Cr�dito invalido"
          wvarMensaje.set( XmlDomExtended.getText( wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@mensaje" )  ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      //jc 10/2010 se agrega CBU
      //El nro. de CBU(22) se conforma asi en cuennume(0,16) en  VENCIMES(16,2) y VENCIANN(18, 4)
      wvarStep = 800;
      if( Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) )  ) ) == 5 )
      {
        if( !Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) )  ) ).equals( "DB" ) )
        {
          wvarCodErr.set( -13 );
          wvarMensaje.set( "Para CBU campo COBROTIP debe valer DB" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        if( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -13 );
          wvarMensaje.set( "Nro. CBU incompleto (cuennume)" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        if( wobjXMLRequest.selectSingleNode( "//VENCIMES" )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -13 );
          wvarMensaje.set( "Nro. CBU incompleto (vencimes)" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        if( wobjXMLRequest.selectSingleNode( "//VENCIANN" )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -13 );
          wvarMensaje.set( "Nro. CBU incompleto (venciann)" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        //Valida el nro. de CBU
        wvarStep = 920;
        wvarTarjeta = "<Request>" + "<USUARIO/>" + "<COBROTIP>" + Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) )  ) ) + "</COBROTIP>" + "<CTANUM>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) )  ) + "</CTANUM>" + "<VENANN>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VENCIANN" )  ) + "</VENANN>" + "<VENMES>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VENCIMES" )  ) + "</VENMES>" + "</Request>";

        wvarStep = 940;
        wobjClass = new lbawA_OVMQCotizar.lbaw_OVValidaCuentas();
        wobjClass.Execute( wvarTarjeta, wvarStatusTarjeta, "" );
        wobjClass = (Object) null;

        wvarStep = 960;
        wobjXMLStatusTarjeta = new XmlDomExtended();
        wobjXMLStatusTarjeta.loadXML( wvarStatusTarjeta );

        wvarStep = 980;
        if( XmlDomExtended.getText( wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
        {
          wvarCodErr.set( -198 );
          wvarMensaje.set( XmlDomExtended.getText( wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@mensaje" )  ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      //JC FIN CBU
      //Chequeo el Nro. de Patente
      wvarStep = 100;
      if( Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//ESCERO" )  ) ).equals( "S" ) )
      {
        if( ! (Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PATENNUM" )  ) ).equals( "A/D" )) )
        {
          wvarCodErr.set( -14 );
          wvarMensaje.set( "Nro. de Patente Invalida" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        }
      }
      if( (Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//ESCERO" )  ) ).equals( "N" )) || ((Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//ESCERO" )  ) ).equals( "S" )) && ! ((Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PATENNUM" )  ) ).equals( "A/D" )))) )
      {
        wvarCodErr.set( "0" );
        wvarMensaje.set( "" );
        pvarRes.set( "" );
        for( wvarcounter = 1; wvarcounter <= 3; wvarcounter++ )
        {
          if( new Variant( Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PATENNUM" )  ), wvarcounter, 1 ) ).isNumeric() )
          {
            wvarCodErr.set( -14 );
            wvarMensaje.set( "Nro. de Patente Invalida" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          }
        }

        wvarStep = 105;
        for( wvarcounter = 4; wvarcounter <= 6; wvarcounter++ )
        {
          if( ! (new Variant( Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PATENNUM" )  ), wvarcounter, 1 ) ).isNumeric()) )
          {
            wvarCodErr.set( -14 );
            wvarMensaje.set( "Nro. de Patente Invalida" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          }
        }
      }
      if( wvarCodErr.toInt() != 0 )
      {
        return fncGetAll;
      }
      //jc 09/2010 valida dispositivo de rastreo
      // 12-11-2012 Si el plan es RC no requeire dispositivo
      if( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PLANNCOD" )  ).equals( "011" ) )
      {

        if( Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//INSTALADP" )  ) ).equals( "S" ) )
        {
          wvarCodErr.set( -298 );
          wvarMensaje.set( "No requiere instalar dispositivo de rastreo" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      else
      {

        wvarStep = 107;
        wvarTarjeta = "<Request>" + "<USUARIO/>" + "<MODEAUTCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ModeAutCod) )  ) + "</MODEAUTCOD>" + "<SUMASEG>" + String.valueOf( (Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SumAseg) )  ) ) * 100) ) + "</SUMASEG>" + "<PROVI>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) )  ) + "</PROVI>" + "<LOCALIDADCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) )  ) + "</LOCALIDADCOD>" + "<OPCION/><OPERACION/><PRESCOD/><INSTALA/>" + "</Request>";
        wobjClass = new lbawA_OVMQCotizar.lbaw_OVGetDispRastreo();
        wobjClass.Execute( wvarTarjeta, wvarStatusTarjeta, "" );
        wobjClass = (Object) null;

        wobjXMLStatusTarjeta = new XmlDomExtended();
        wobjXMLStatusTarjeta.loadXML( wvarStatusTarjeta );
        wobjXMLStatusTarjeta.loadXML( "<Response><Estado resultado='true' mensaje=''/><REQ>N</REQ><GAMA>B</GAMA><DISPGEN>0040</DISPGEN><DISPGENDESC>DISPOSITIVO DE RASTREO PARA LB</DISPGENDESC></Response>" );

        if( XmlDomExtended.getText( wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
        {
          wvarCodErr.set( -298 );
          wvarMensaje.set( "No se puede determinar si requiere dispositivo de rastreo" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( XmlDomExtended.getText( wobjXMLStatusTarjeta.selectSingleNode( "//REQ" )  ).equals( "S" ) )
          {
            if( Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//INSTALADP" )  ) ).equals( "N" ) )
            {
              wvarCodErr.set( -298 );
              wvarMensaje.set( "Se requiere instalar dispositivo de rastreo" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            else
            {
              if( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//RASTREO" )  ).equals( "0" ) )
              {
                wvarCodErr.set( -298 );
                wvarMensaje.set( "Falta informar dispositivo de rastreo" );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
          }
          else
          {
            if( Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//INSTALADP" )  ) ).equals( "S" ) )
            {
              wvarCodErr.set( -298 );
              wvarMensaje.set( "NO requiere instalar dispositivo de rastreo" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
        }
      }

      //Valida la fecha de Vigencia
      wvarStep = 110;
      if( ! (invoke( "validarFecha", new Variant[] { new Variant(XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( new Variant("//VIGENDIA") )  ) + "/" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( new Variant("//VIGENMES") )  ) + "/" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( new Variant("//VIGENANN") )  )), new Variant(wvarMensaje) } ).toBoolean()) )
      {
        //error
        wvarCodErr.set( -15 );
        wvarMensaje.set( "Fecha de Vigencia Invalida" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;

      }

      //Chequeo que la fecha de Vigencia sea mayor a hoy
      wvarStep = 112;
      wvarFechaActual = DateTime.dateSerial( DateTime.year( DateTime.now() ), DateTime.month( DateTime.now() ), DateTime.day( DateTime.now() ) );
      wvarFechaVigencia = DateTime.dateSerial( Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VIGENANN" )  ) ), Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VIGENMES" )  ) ), Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VIGENDIA" )  ) ) );

      if( wvarFechaVigencia.compareTo( wvarFechaActual ) < 0 )
      {
        wvarCodErr.set( -197 );
        wvarMensaje.set( "Fecha de Vigencia anterior a la fecha actual" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      //Chequeo que la fecha no sea mayor a 30 d�as a partir de hoy
      wvarStep = 114;
      if( wvarFechaVigencia.compareTo( DateTime.add( wvarFechaActual, 30 ) ) > 0 )
      {
        wvarCodErr.set( -196 );
        wvarMensaje.set( "Fecha de Vigencia posterior a 30 dias de la fecha actual" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;

      }
      //Chequeo los hijos
      wvarStep = 120;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Hijos ) ;
      for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
      {
        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_SexoHijo )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Sexo de Hijo en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo + "/VALOR[.='" + XmlDomExtended.getText( null (*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_SexoHijo ) *) ) + "']") ) .getLength() == 0 )
          {
            //error
            wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo + "/N_ERROR" )  ) );
            wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo + "/TEXTOERROR" )  ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
        wvarStep = 130;
        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_NacimHijo )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Fecha de Nacimiento de Hijo en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( ! (invoke( "ValidarFechayRango", new Variant[] { new Variant(Strings.left( new Variant(XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), new Variant(mcteParam_NacimHijo) )  )), new Variant(2) ) + "/" + Strings.mid( new Variant(XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), new Variant(mcteParam_NacimHijo) )  )), new Variant(3), new Variant(2) ) + "/" + Strings.right( new Variant(XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), new Variant(mcteParam_NacimHijo) )  )), new Variant(4) )), new Variant(17), new Variant(29), new Variant(wvarMensaje) } ).toBoolean()) )
          {
            //error
            wvarCodErr.set( -10 );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
        wvarStep = 150;
        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_EstadoHijo )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Estado Civil del Hijo en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo + "/VALOR[.='" + XmlDomExtended.getText( null (*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_EstadoHijo ) *) ) + "']") ) .getLength() == 0 )
          {
            //error
            wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo + "/N_ERROR" )  ) );
            wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo + "/TEXTOERROR" )  ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
        wvarStep = 160;

        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_NombreHijo )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Nombre del Hijo en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_NombreHijo )  ).equals( "" ) )
          {
            //error
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Nombre del Hijo en blanco" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }

        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_ApellidoHijo )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Apellido del Hijo en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_ApellidoHijo )  ).equals( "" ) )
          {
            //error
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Apellido del Hijo en blanco" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }

        wvarStep = 170;
        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_EdadHijo )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Edad del Hijo en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( VB.val( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_EdadHijo )  ) ) == 0 )
          {
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Edad del Hijo con Valor 0" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;

      //Chequeo los Accesorios
      wvarStep = 180;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) ;
      for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
      {
        wvarStep = 190;
        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_PrecioAcc )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Precio de Accesorio en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
          //Else
          //   If Val(wobjXMLList(wvarContNode).selectSingleNode(mcteParam_PrecioAcc).Text) = 0 Then
          //     wvarCodErr = -1
          //     wvarMensaje = "Campo de Precio de Accesorio en cero"
          //     pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
          //     Exit Function
          //   End If
        }

        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_CodigoAcc )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Codigo de Accesorio en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( VB.val( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_CodigoAcc )  ) ) == 0 )
          {
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Codigo de Accesorio en blanco" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }

        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_DescripcionAcc )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Descripcion de Accesorio en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_DescripcionAcc )  ).equals( "" ) )
          {
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Descripcion de Accesorio en blanco" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;

      //11/2010 se permiten distintos tipos de iva
      wvarStep = 195;
      // distino consumidor final valida cuit
      if( !XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) )  ).equals( "3" ) )
      {
        if( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUITNUME) )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -398 );
          wvarMensaje.set( "Debe informar Nro. de CUIT" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          wvarStep = 197;
          wvarTarjeta = "<Request>" + "<USUARIO/> " + "<DOCUMTIP>4</DOCUMTIP> " + "<DOCUMNRO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUITNUME) )  ) + "</DOCUMNRO> " + "</Request>";
          wvarStep = 198;
          wobjClass = new lbawA_OVMQCotizar.lbaw_OVValidaNumDoc();
          wobjClass.Execute( wvarTarjeta, wvarStatusTarjeta, "" );
          wobjClass = (Object) null;
          wvarStep = 199;
          wobjXMLStatusTarjeta = new XmlDomExtended();
          wobjXMLStatusTarjeta.loadXML( wvarStatusTarjeta );
          wvarStep = 200;
          if( XmlDomExtended.getText( wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
          {
            wvarCodErr.set( -398 );
            wvarMensaje.set( "Nro. de CUIT incorrecto o esta mal el formato" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }
      else
      {
        wvarStep = 201;

        if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUITNUME) )  == (org.w3c.dom.Node) null) )
        {
          if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUITNUME) )  ).equals( "0" )) )
          {
            wvarCodErr.set( -398 );
            wvarMensaje.set( "NO Debe informar Nro. de CUIT" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }

      //11/2010 se permiten distintos tipos de iva implica tambien ibb
      wvarStep = 202;
      // distino consumidor final valida nro.ibb
      if( !XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) )  ).equals( "3" ) )
      {
        if( (wobjXMLRequest.selectNodes( ("//" + mcteParam_NROIBB) ) */ == (org.w3c.dom.NodeList) null) || (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_IBB) )  == (org.w3c.dom.Node) null) )
        {
          wvarCodErr.set( -398 );
          wvarMensaje.set( "Debe informar Nro. de Ingresos Brutos" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          wvarStep = 203;
          wvarTarjeta = "<Request>" + "<USUARIO/> " + "<IIBBTIP>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_IBB) )  ) + "</IIBBTIP> " + "<IIBBNUM>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROIBB) )  ) + "</IIBBNUM> " + "</Request>";
          wvarStep = 204;
          wobjClass = new lbawA_OVMQCotizar.lbaw_OVValidaIIBB();
          wobjClass.Execute( wvarTarjeta, wvarStatusTarjeta, "" );
          wobjClass = (Object) null;
          wvarStep = 205;
          wobjXMLStatusTarjeta = new XmlDomExtended();
          wobjXMLStatusTarjeta.loadXML( wvarStatusTarjeta );
          wvarStep = 206;
          if( XmlDomExtended.getText( wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
          {
            wvarCodErr.set( -398 );
            wvarMensaje.set( "Nro. de Ingresos Brutos incorrecto o no corresponde a Tipo de Ingresos Brutos" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }
      else
      {
        wvarStep = 207;
        if( wobjXMLRequest.selectNodes( ("//" + mcteParam_NROIBB) ) .getLength() > 0 )
        {
          if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROIBB) )  == (org.w3c.dom.Node) null) )
          {
            if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROIBB) )  ).equals( "0" )) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "NO Debe informar Nro. de Ingresos Brutos" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
        }
      }
      // se valida razon social si corresponde x iva
      wvarStep = 208;
      // distino consumidor final valida cuit
      if( !XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) )  ).equals( "3" ) )
      {
        if( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAZONSOC) )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -398 );
          wvarMensaje.set( "Debe informar Razon Social" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      else
      {
        if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAZONSOC) )  == (org.w3c.dom.Node) null) )
        {
          if( !Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAZONSOC) )  ) ).equals( "" ) )
          {
            wvarCodErr.set( -398 );
            wvarMensaje.set( "No debe informar Razon Social" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }

      //fin 11/2010
      // Valido campos del Acreedor Prendario
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_ACREPREN )  == (org.w3c.dom.Node) null) )
      {
        if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ACREPREN )  ) ).equals( "S" ) )
        {

          if( ! ((wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCUACRE )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCUACRE )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLRequest.selectSingleNode( mcteParam_APELLIDOACRE )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLRequest.selectSingleNode( mcteParam_NOMBRESACRE )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOMACRE )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLRequest.selectSingleNode( mcteParam_DOMICDNUACRE )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLRequest.selectSingleNode( mcteParam_DOMICPISACRE )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLRequest.selectSingleNode( mcteParam_DOMICPTAACRE )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOBACRE )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLRequest.selectSingleNode( mcteParam_DOMICCPOACRE )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLRequest.selectSingleNode( mcteParam_PROVICODACRE )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLRequest.selectSingleNode( mcteParam_PAISSCODACRE )  == (org.w3c.dom.Node) null)) )
          {
            if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCUACRE )  ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Numero de Documento del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCUACRE )  ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Tipo de Documento del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_APELLIDOACRE )  ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Apellido del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NOMBRESACRE )  ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Nombre del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOMACRE )  ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Domicilio del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICDNUACRE )  ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Domicilio Numero del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICPISACRE )  ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Domicilio Piso del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICPTAACRE )  ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Domicilio Puerta del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOBACRE )  ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Domicilio POB del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICCPOACRE )  ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Numero de Documento del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PROVICODACRE )  ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Provincia del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PAISSCODACRE )  ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Pais del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
          else
          {
            wvarCodErr.set( -399 );
            wvarMensaje.set( "Debe informar todos los datos del Acreedor Prendario" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }
      wvarStep = 200;
      if( wvarCodErr.toInt() == 0 )
      {
        if( invoke( "fncValidaABase", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } ) )
        {

//TODO: Inicio Cambio
          wobjXMLRes = new XmlDomExtended();
          wobjXMLRes.loadXML( pvarRes.toString() );

          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "RAMOPCOD", "" ) */ );
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//RAMOPCOD" ) , XmlDomExtended.getText( wobjXMLRes.selectSingleNode( "//RAMOPCOD" )  ) );

          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "POLIZANN", "" ) */ );
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//POLIZANN" ) , XmlDomExtended.getText( wobjXMLRes.selectSingleNode( "//POLIZANN" )  ) );

          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "POLIZSEC", "" ) */ );
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//POLIZSEC" ) , XmlDomExtended.getText( wobjXMLRes.selectSingleNode( "//POLIZSEC" )  ) );

          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CERTIPOL", "" ) */ );
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//CERTIPOL" ) , XmlDomExtended.getText( wobjXMLRes.selectSingleNode( "//CERTIPOL" )  ) );

          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CERTIANN", "" ) */ );
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//CERTIANN" ) , XmlDomExtended.getText( wobjXMLRes.selectSingleNode( "//CERTIANN" )  ) );

          if( wobjXMLRequest.selectSingleNode( "//CERTISEC" )  == (org.w3c.dom.Node) null )
          {
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CERTISEC", "" ) */ );
          }
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//CERTISEC" ) , XmlDomExtended.getText( wobjXMLRes.selectSingleNode( "//CERTISEC" )  ) );

          //Se agregan las Coberturas y descripci�n del Plan para el AIS...
          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "PQTDES", "" ) */ );
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//PQTDES" ) , XmlDomExtended.getText( wobjXMLRes.selectSingleNode( "//PLANDES" )  ) );

          //Se agrega el AGECLA...
          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "AGECLA", "" ) */ );
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//AGECLA" ) , XmlDomExtended.getText( wobjXMLRes.selectSingleNode( "//AGECLA" )  ) );

          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "COBERTURAS", "" ) */ );


          for( wvarcounter = 1; wvarcounter <= 30; wvarcounter++ )
          {
            wvarStep = 210;

            if( ! (Obj.toInt( XmlDomExtended.getText( wobjXMLRes.selectSingleNode( ("//COBERCOD" + wvarcounter) )  ) ) == 0) )
            {

              /*unsup wobjXMLRequest.selectSingleNode( "//Request/COBERTURAS" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "COBERTURA", "" ) */ );

              /*unsup wobjXMLRequest.selectNodes( "//Request/COBERTURAS/COBERTURA" ) */.item( wvarcounter - 1 ).appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "COBERCOD", "" ) */ );
              XmlDomExtended.setText( wobjXMLRequest.selectNodes( "//Request/COBERTURAS/COBERTURA/COBERCOD" ) */.item( wvarcounter - 1 ), XmlDomExtended.getText( null /*unsup wobjXMLRes.selectSingleNode( "//COBERCOD" + wvarcounter )  ) );

              /*unsup wobjXMLRequest.selectNodes( "//Request/COBERTURAS/COBERTURA" ) */.item( wvarcounter - 1 ).appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "COBERORD", "" ) */ );
              XmlDomExtended.setText( wobjXMLRequest.selectNodes( "//Request/COBERTURAS/COBERTURA/COBERORD" ) */.item( wvarcounter - 1 ), XmlDomExtended.getText( null /*unsup wobjXMLRes.selectSingleNode( "//COBERORD" + wvarcounter )  ) );

              /*unsup wobjXMLRequest.selectNodes( "//Request/COBERTURAS/COBERTURA" ) */.item( wvarcounter - 1 ).appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CAPITASG", "" ) */ );
              XmlDomExtended.setText( wobjXMLRequest.selectNodes( "//Request/COBERTURAS/COBERTURA/CAPITASG" ) */.item( wvarcounter - 1 ), XmlDomExtended.getText( null /*unsup wobjXMLRes.selectSingleNode( "//CAPITASG" + wvarcounter )  ) );

              /*unsup wobjXMLRequest.selectNodes( "//Request/COBERTURAS/COBERTURA" ) */.item( wvarcounter - 1 ).appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CAPITIMP", "" ) */ );
              XmlDomExtended.setText( wobjXMLRequest.selectNodes( "//Request/COBERTURAS/COBERTURA/CAPITIMP" ) */.item( wvarcounter - 1 ), XmlDomExtended.getText( null /*unsup wobjXMLRes.selectSingleNode( "//CAPITIMP" + wvarcounter )  ) );

            }
          }
//TODO: Fin Cambio
          //AM Valido CANAL en caso de que se informe
          if( ! (wobjXMLRequest.selectSingleNode( "//CANAL" )  == (org.w3c.dom.Node) null) )
          {
            // Obtengo el USUARCOD de la respuesta de la BD
            wvarStep = 111;
//TODO: Inicio Cambio
            //Set wobjXMLRes = CreateObject("MSXML2.DOMDocument")
            //wobjXMLRes.async = False
            //Call wobjXMLRes.loadXML(pvarRes)
//TODO: Fin Cambio
             */
            //
            wvarStep = 112;
            mvarWDB_USUARCOD = "";
            if( ! (wobjXMLRes.selectSingleNode( "//USUARCOD" )  == (org.w3c.dom.Node) null) )
            {
              mvarWDB_USUARCOD = XmlDomExtended.getText( wobjXMLRes.selectSingleNode( "//USUARCOD" )  );
            }
            //
            // Si se informa el CANAL valido los otros dos campos
            if( wobjXMLRequest.selectSingleNode( "//CANAL_SUCURSAL" )  == (org.w3c.dom.Node) null )
            {
              wvarCodErr.set( -14 );
              wvarMensaje.set( "Debe informar Canal Sucursal" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( wobjXMLRequest.selectSingleNode( "//LEGAJO_VEND" )  == (org.w3c.dom.Node) null )
            {
              wvarCodErr.set( -14 );
              wvarMensaje.set( "Debe informar Legajo Vendedor" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }

            //AM Valido el CANAL Msg1000
            wvarStep = 921;
            mvarRequest = "<Request><DEFINICION>1000_PerfilUsuario.xml</DEFINICION>";
            mvarRequest = mvarRequest + "<AplicarXSL>1000_PerfilUsuario.xsl</AplicarXSL>";
            mvarRequest = mvarRequest + "<USUARIO>" + Strings.toUpperCase( mvarWDB_USUARCOD ) + "</USUARIO></Request>";


            wvarStep = 922;
            wobjClass = new lbawA_OVMQGen.lbaw_MQMensajeGestion();
            wobjClass.Execute( mvarRequest, wvarResponse, "" );
            wobjClass = (Object) null;

            wvarStep = 923;
            wobjXMLStatusCanal = new XmlDomExtended();

            wvarResponse = "<Response><Estado resultado='true' mensaje=''/><USER_NIVEL>D</USER_NIVEL><USUARNOM>BISVAL, JUAN CARLOS</USUARNOM><CLIENTIP>P</CLIENTIP><BANCOCOD>0150</BANCOCOD><BANCONOM>CANAL PRODUCTORES</BANCONOM><GRUPOHSBC>N</GRUPOHSBC><ACCESO><MMENU>050000</MMENU><MMENU>100000</MMENU><MMENU>200000</MMENU><MMENU>300000</MMENU><MMENU>350000</MMENU><MMENU>450000</MMENU><MMENU>490000</MMENU><MMENU>500000</MMENU><MMENU>550000</MMENU><MMENU>600000</MMENU><MMENU>051000</MMENU><MMENU>052000</MMENU><MMENU>053000</MMENU><MMENU>054000</MMENU><MMENU>101000</MMENU><MMENU>102000</MMENU><MMENU>103000</MMENU><MMENU>104000</MMENU><MMENU>105000</MMENU><MMENU>106000</MMENU><MMENU>108000</MMENU><MMENU>201000</MMENU><MMENU>202000</MMENU><MMENU>203000</MMENU><MMENU>204000</MMENU><MMENU>301000</MMENU><MMENU>302000</MMENU><MMENU>302500</MMENU><MMENU>303000</MMENU><MMENU>304000</MMENU><MMENU>305000</MMENU><MMENU>306000</MMENU><MMENU>307000</MMENU>";
            wvarResponse = wvarResponse + "<MMENU>351000</MMENU><MMENU>352000</MMENU><MMENU>451000</MMENU><MMENU>452000</MMENU><MMENU>453000</MMENU><MMENU>454000</MMENU><MMENU>455000</MMENU><MMENU>456000</MMENU><MMENU>501000</MMENU><MMENU>502000</MMENU><MMENU>551000</MMENU></ACCESO><EMISIONES></EMISIONES><MAILUSER></MAILUSER><PEOPLESOFTEC>0</PEOPLESOFTEC><USUARNOMEC>Daniel Bujia</USUARNOMEC><MAILEC>daniel.bujia@hsbc.com.ar</MAILEC></Response>";

            wobjXMLStatusCanal.loadXML( wvarResponse );

            wvarStep = 924;
            if( XmlDomExtended.getText( wobjXMLStatusCanal.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
            {
              wvarCodErr.set( -198 );
              wvarMensaje.set( XmlDomExtended.getText( wobjXMLStatusCanal.selectSingleNode( "//Response/Estado/@mensaje" )  ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            else
            {
              wvarBancoCod = "0";
              if( ! (wobjXMLStatusCanal.selectSingleNode( "//BANCOCOD" )  == (org.w3c.dom.Node) null) )
              {
                wvarBancoCod = XmlDomExtended.getText( wobjXMLStatusCanal.selectSingleNode( "//BANCOCOD" )  );
              }
              if( ! (Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EmpresaCodigo )  ) ) == Obj.toInt( wvarBancoCod )) )
              {
                wvarCodErr.set( -399 );
                wvarMensaje.set( "Nro. de Canal incorrecto para el Broker" );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }

            }
            // Fin valido CANAL
            //AM Valido el CANAL_SUCURSAL Msg1018
            wvarStep = 925;
            mvarRequest = "<Request><DEFINICION>1018_ListadoSucursalesxBanco.xml</DEFINICION>";
            mvarRequest = mvarRequest + "<BANCOCOD>" + wvarBancoCod + "</BANCOCOD></Request>";

            wvarStep = 926;
            wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
            wobjClass.Execute( mvarRequest, wvarResponse, "" );
            wobjClass = (Object) null;

            wvarStep = 927;
            wobjXMLStatusCanal = new XmlDomExtended();

            wvarResponse = "<Response>";
            wvarResponse = wvarResponse + "<Estado resultado='true' mensaje=''/>";
            wvarResponse = wvarResponse + "<CAMPOS>";
            wvarResponse = wvarResponse + "<CANT>211</CANT>";
            wvarResponse = wvarResponse + "<SUCURSALES>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0001</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[25 DE MAYO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0013</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[25 DE MAYO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0039</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[25 DE MAYO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0042</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CABALLITO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0047</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[NEUQUEN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0051</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MENDOZA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0052</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[TUCUMAN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0053</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CONCEPCION]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0054</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[AVELLANEDA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0055</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CORDOBA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0056</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[BELGRANO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0057</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CENTRO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0058</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAAVEDRA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0059</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[AV.DE MAYO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0060</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CONSTITUCION]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0061</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SANTA ROSA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0063</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ONCE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0064</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[VILLA CRESPO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0065</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[FLORIDA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0066</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LA PLATA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0067</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SALTA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0068</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[S.SOLEIL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0069</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PALERMO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0070</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[TRIBUNALES]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0071</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ROSARIO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0072</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SANTA FE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0073</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ALTO PALERMO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0074</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LA PLATA CATEDRAL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0075</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MAR DEL PLATA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0076</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MUNRO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0077</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[VICENTE LOPEZ]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0078</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN ISIDRO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0079</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LOMAS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0080</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[OLIVOS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0081</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CABILDO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0082</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[NU#EZ]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0083</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN FERNANDO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0084</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LAS HERAS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0085</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN RAFAEL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0086</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN MARTIN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0087</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LINIERS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0088</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[RESISTENCIA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0090</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[FLORES]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0094</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ALEM]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0110</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MORON]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0111</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[TUCUMAN NORTE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0112</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CATAMARCA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0113</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MALARGUE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0114</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CONC. DEL URUGUAY]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0115</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[JUNIN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0116</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN JUSTO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0117</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PILAR]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0118</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ADROGUE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0119</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LUJAN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0120</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PARANA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0121</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CORRIENTES]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0122</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN JUAN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0123</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[BAHIA BLANCA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0124</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[RECOLETA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0125</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN MIGUEL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0127</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[QUILMES]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0128</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[POSADAS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0130</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SANTIAGO DEL ESTERO2]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0131</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PLAZA ESPA�A]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0132</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PARQUE LEZAMA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0133</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAGRADA FAMILIA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0134</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CARREFOUR (S. FERNAN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0136</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CERRO LAS ROSAS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0139</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[YERBA BUENA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0140</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PLAZA DELA REPUBLICA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0141</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CIPOLLETTI]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0142</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[NUEVA NEUQUEN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0143</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[BORGES]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0144</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[RINCON DE LOS SAUCES]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0145</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[BARRIO PARQUE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0146</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ROSARIO SUR]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0147</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LEDESMA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0148</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN JUAN TERMINAL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0300</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CENTRAL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0301</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[BARRIO NORTE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0302</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PATERNAL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0303</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[DIAGONAL NORTE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0304</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[25 DE MAYO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0305</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CHARCAS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0306</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[POMPEYA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0307</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CENTRO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0308</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[FLORES SUD]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0309</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ESMERALDA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0310</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ABASTO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0312</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[FORMOSA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0313</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CABALLITO II]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0314</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN MARTIN II]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0315</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PERGAMINO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0316</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[VILLA MERCEDES]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0317</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PLAZA SAN MARTIN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0319</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[DEVOTO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0320</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LUGANO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0321</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[NAZCA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0322</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[VILLA PUEYRREDON]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0325</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ROSARIO II]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0326</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LOMAS DE ZAMORA II]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0327</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LANUS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0400</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CASA CENTRAL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0452</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[YERBA BUENA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0511</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[BARRIO BOMBAL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0512</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN MARTIN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0513</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LUJAN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0516</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[COLEGIO NOTARIAL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0518</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN JOSE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0600</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ONCE MISERERE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0601</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[BARRACAS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0602</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[URUGUAY]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0603</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[DEL SALVADOR]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0604</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[VILLA CRESPO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0605</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ENTRE RIOS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0606</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CONSTITUCION]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0607</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ALMAGRO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0608</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PATERNAL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0609</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SALGUERO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0610</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[VILLA URQUIZA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0611</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CABALLITO PARRAL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0612</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[BELGRANO PAMPA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0613</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MONSERRAT]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0614</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[NORTE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0615</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[VILLA DEVOTO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0616</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LA PIEDAD]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0617</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LAS HERAS PUEYRREDON]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0618</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MATADEROS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0619</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[FLORES CAMACUA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0620</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LINIERS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0621</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MAURE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0622</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CATALINAS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0623</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MICROCENTRO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0624</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[HOSPITAL ITALIANO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0625</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SUIPACHA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0626</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[AVELLANEDA BELGRANO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0627</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[VTE LOPEZ ALSINA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0628</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LANUS LAVALLOL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0629</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CASEROS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0630</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MARTINEZ]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0631</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[AZUL SAN MARTIN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0632</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN JUSTO ROSAS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0633</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[VILLA BALLESTER]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0634</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LOMAS DE SAN ISIDRO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0635</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[AZUL QUILMES]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0636</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MORON BROWN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0637</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[AZUL L DE ZAMORA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0638</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[NUNRO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0639</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[RAMOS MEJIA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0640</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[AZUL SAN JUSTO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0641</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PILAR PARQUE IND.]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0642</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[TORTUGUITAS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0643</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ZARATE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0644</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PACHECO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0645</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PILAR PANAMERICANA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0646</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN MIGUEL CONESA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0647</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ESCOBAR]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0648</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MONTE GRANDE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0649</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[LA PLATA DIAGONAL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0650</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[BAHIA BLANCA PLAZA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0651</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MAR DEL PLATA DIAG.]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0652</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[TANDIL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0653</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[BALCARCE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0654</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[OLAVARRIA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0655</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ROSARIO SAN MARTIN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0656</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SANTA FE COSTANERA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0657</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[RAFAELA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0658</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[ROSARIO P.PRINGLES]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0659</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CORDOBA CINE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0660</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[RIO CUARTO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0661</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[FERREYRA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0662</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[VILLA MARIA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0663</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CORDOBA H.LIBERTAD]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0664</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN FRANCISCO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0665</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CORDOBA MERCADO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0666</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[AGENCIA DE TUCUMAN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0667</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[AGENCIA DE TUCUMAN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0668</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CORRIENTES JUNIN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0669</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PARANA URQUIZA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0670</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MENDOZA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0671</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[GODOY CRUZ]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0672</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MENDOZA CIRC.MEDICO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0673</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MENDOZA COLON]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0674</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MENDOZA SAN MARTIN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0675</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[MENDOZA R. PE�A]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0676</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN JUAN ACHA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0677</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[AGENCIA G VIRASORO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0678</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SALTA PEATONAL]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0679</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[RESISTENCIA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0680</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[JUJUY]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0681</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[BARILOCHE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0682</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[GENERAL ROCA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0683</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[TRELEW]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0684</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[COMODORO RIVADAVIA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0685</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CALETA OLIVA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0686</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[RIO GALLEGOS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0687</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SAN LUIS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0688</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[NEUQUEN]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0689</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[USHUAIA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0690</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[RIO GRANDE]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0691</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PEATONAL FLORIDA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0777</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[PERSONAL DEL HSBC]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0995</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[TLMK BANCO]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0996</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[TLMK (CAP. Y GBA)]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0998</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[BANCOS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>0999</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[VENTA VENDEDORES]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>1000</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[RECUPERO DE PRENDAS]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>4511</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CASANFER]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>5000</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CALL CENTER]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>8888</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[SUCURSAL GENERICA]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "<SUCU>";
            wvarResponse = wvarResponse + "<CODISUCU>8889</CODISUCU>";
            wvarResponse = wvarResponse + "<DESCSUCU><![CDATA[CANAL ATM]]></DESCSUCU>";
            wvarResponse = wvarResponse + "</SUCU>";
            wvarResponse = wvarResponse + "</SUCURSALES>";
            wvarResponse = wvarResponse + "</CAMPOS>";
            wvarResponse = wvarResponse + "</Response>";

            wobjXMLStatusCanal.loadXML( wvarResponse );

            wvarStep = 928;
            if( XmlDomExtended.getText( wobjXMLStatusCanal.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
            {
              wvarCodErr.set( -198 );
              wvarMensaje.set( XmlDomExtended.getText( wobjXMLStatusCanal.selectSingleNode( "//Response/Estado/@mensaje" )  ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            else
            {
              wvarStep = 929;
              wobjXMLNodeList = wobjXMLStatusCanal.selectNodes( "//Response/CAMPOS/SUCURSALES/SUCU" ) ;
              wvarExisteSucu = "N";

              for( wvarcounter = 0; wvarcounter <= (wobjXMLNodeList.getLength() - 1); wvarcounter++ )
              {
                wvarStep = 931;
                if( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SucursalCodigo )  ).equals( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNodeList.item(wvarcounter), "CODISUCU" )  ) ) )
                {
                  wvarExisteSucu = "S";
                }
              }

              wvarStep = 932;
              if( wvarExisteSucu.equals( "N" ) )
              {
                wvarCodErr.set( -399 );
                wvarMensaje.set( "Nro. de Sucursal invalido para el Broker" );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
            // Fin valido CANAL_SUCURSAL
          }
          //TODO: Inicio Cambio
          //Agrego Estructura de la p�liza
          if( ! (invoke( "fncIntegracionAIS", new Variant[] { new Variant(wobjXMLRequest.getDocument().getDocumentElement().toString()), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes.toString()) } )) )
          {
            //wvarCodErr = -499
            wobjXMLAux = new XmlDomExtended();
            wobjXMLAux.loadXML( wvarMensaje.toString() );

            wobjXMLRes = new XmlDomExtended();
            wobjXMLRes.loadXML( pvarRes.toString() );

            /*unsup wobjXMLRes.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRes.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "INTEGRACION", "" ) */ );
            /*unsup wobjXMLRes.selectSingleNode( "//Request/INTEGRACION" ) */.appendChild( null /*unsup wobjXMLRes.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "SOLICITUDGRABADA", "" ) */ );
            /*unsup wobjXMLRes.selectSingleNode( "//Request/INTEGRACION" ) */.appendChild( null /*unsup wobjXMLRes.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "SOLICITUDOK", "" ) */ );
            /*unsup wobjXMLRes.selectSingleNode( "//Request/INTEGRACION" ) */.appendChild( null /*unsup wobjXMLRes.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "MENSAJE", "" ) */ );



            XmlDomExtended.setText( wobjXMLRes.selectSingleNode( "//SOLICITUDGRABADA" ) , XmlDomExtended.getText( wobjXMLAux.selectSingleNode( "//SOLICITUDGRABADA" )  ) );
            XmlDomExtended.setText( wobjXMLRes.selectSingleNode( "//SOLICITUDOK" ) , XmlDomExtended.getText( wobjXMLAux.selectSingleNode( "//SOLICITUDOK" )  ) );
            XmlDomExtended.setText( wobjXMLRes.selectSingleNode( "//MENSAJE" ) , XmlDomExtended.getText( wobjXMLAux.selectSingleNode( "//TEXTO" )  ) );
            //wobjXMLRes = ""
            pvarRes.set( wobjXMLRes.getDocument().getDocumentElement().toString() );
            //Exit Function
          }
        //TODO: Fin Cambio
          fncGetAll = true;
        }
        else
        {
          fncGetAll = false;
        }
      }
      else
      {
        fncGetAll = false;
      }

      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = (com.qbe.services.HSBCInterfaces.IDBConnection) null;
      wobjXMLRequest = null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + mcteErrorInesperadoDescr + "\" /></Response>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }
//TODO: Inicio Cambio
  private boolean fncIntegracionAIS( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, String pvarRes ) throws Exception
  {
    boolean fncIntegracionAIS = false;
    int wvarStep = 0;
    boolean wvarError = false;
    boolean mvarExcluido = false;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXML = null;
    XmlDomExtended wobjXMLAux = null;
    XmlDomExtended wobjXSL = null;
    XmlDomExtended wvarXMLACCESORIOS = null;
    XmlDomExtended wvarXMLHijos = null;
    XmlDomExtended wvarXMLASEGURADOS = null;
    XmlDomExtended wvarXMLGrabacion = null;
    XmlDomExtended wvarXMLASEGADIC = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    String wvarXSL = "";
    com.qbe.services.HSBCInterfaces.IDBConnection wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Object wobjClass = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    String mvarRequest = "";
    String mvarResponse = "";
    String wvarResponse = "";
    int wvarContChild = 0;
    int wvarContNode = 0;
    int wvarcounter = 0;
    String wvarErrorCodigo = "";
    String wvarTarjeta = "";
    String wvarStatusTarjeta = "";
    String wvarBancoCod = "";
    String wvarExisteSucu = "";
    String mvarWDB_USUARCOD = "";
    String mvarCUENNUME = "";
    String mvarMsgError = "";
    String mvarError = "";
    int CodigoEstado = 0;
    int i = 0;
    int y = 0;
    org.w3c.dom.Node x = null;
    int mvarLargo = 0;
    Variant mvarMensaje = new Variant();













    wvarStep = 10;
    wobjXMLRequest = new XmlDomExtended();
    wobjXMLRequest.loadXML( pvarRequest );


    mvarRequest = "";

    mvarRequest = mvarRequest + "<POLVENCIANN>0000</POLVENCIANN>";
    mvarRequest = mvarRequest + "<POLVENCIMES>00</POLVENCIMES>";
    mvarRequest = mvarRequest + "<POLVENVIDIA>00</POLVENVIDIA>";

    mvarRequest = mvarRequest + "<RAMOPCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//RAMOPCOD" )  ) + "</RAMOPCOD>";
    mvarRequest = mvarRequest + "<POLIZANN>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//POLIZANN" )  ) + "</POLIZANN>";
    mvarRequest = mvarRequest + "<POLIZSEC>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//POLIZSEC" )  ) + "</POLIZSEC>";
    mvarRequest = mvarRequest + "<CERTIPOL>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CERTIPOL" )  ) + "</CERTIPOL>";
    mvarRequest = mvarRequest + "<CERTIANN>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CERTIANN" )  ) + "</CERTIANN>";
    mvarRequest = mvarRequest + "<SUPLENUM>0</SUPLENUM>";

    //Verificar el campo al cual hace referencia
    mvarRequest = mvarRequest + "<FRANQCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//FRANQCOD" )  ) + "</FRANQCOD>";

    mvarRequest = mvarRequest + "<PQTDES>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PQTDES" )  ) + "</PQTDES>";
    mvarRequest = mvarRequest + "<PLANCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PLANNCOD" )  ) + "</PLANCOD>";
    //<HIJOS1416>0</HIJOS1416>
    //<HIJOS1729>1</HIJOS1729>
    //<HIJOS1729ND>1</HIJOS1729ND>
    //mvarRequest = mvarRequest & "<PQTDES>" & Left(Request.Form("PLANDES"), 120) & "</PQTDES>" ' NO SE UTILIZA
    //PLANCOD es la cobertura que eligio
    //mvarRequest = mvarRequest & "<PLANCOD>" & Left(mvarDATOSPLAND, Len(mvarDATOSPLAND) - 2) & "</PLANCOD>"  ' NO SE UTILIZA
    //mvarRequest = mvarRequest & "<HIJOS1416>" & OwnIif(Request.Form("HIJOS1416") = "1", "1", "0") & "</HIJOS1416>" NO SE UTILIZA
    //mvarRequest = mvarRequest & "<HIJOS1729>" & OwnIif(Request.Form("CONDADIC") = "1", "1", "0") & "</HIJOS1729>" NO SE UTILIZA
    //mvarRequest = mvarRequest & "<HIJOS1729ND>" & OwnIif(Request.Form("HIJOS1729ND") = "1", "1", "0") & "</HIJOS1729ND>" NO SE UTILIZA
    mvarRequest = mvarRequest + "<ZONA>0</ZONA>";

    mvarRequest = mvarRequest + "<CODPROV>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PROVI" )  ) + "</CODPROV>";
    // Se calcula en el cte del Put
    mvarRequest = mvarRequest + "<SUMALBA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//SUMAASEG" )  ) + "</SUMALBA>";

    mvarRequest = mvarRequest + "<CLIENIVA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//IVA" )  ) + "</CLIENIVA>";
    mvarRequest = mvarRequest + "<SUMASEG>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//SUMAASEG" )  ) + "</SUMASEG>";
    mvarRequest = mvarRequest + "<CLUB_LBA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CLUBLBA" )  ) + "</CLUB_LBA>";

    mvarRequest = mvarRequest + "<DESTRUCCION_80>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DESTRUCCION_80" )  ) + "</DESTRUCCION_80>";
    mvarRequest = mvarRequest + "<CPAANO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CPAANO" )  ) + "</CPAANO>";


    //mvarRequest = mvarRequest & "<DESTRUCCION_80>" & Request.Form("DEST80") & "</DESTRUCCION_80>"  NO SE USA...
    //mvarRequest = mvarRequest & "<CPAANO>" & Request.Form("CPAANOANN") & "</CPAANO>" NO SE USA...
    // VERIFICAR CAMPO
    mvarRequest = mvarRequest + "<CTAKMS>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//KMSRNGCOD" )  ) + "</CTAKMS>";
    mvarRequest = mvarRequest + "<ESCERO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//ESCERO" )  ) + "</ESCERO>";

    //<TIENEPLAN>N</TIENEPLAN>
    //<COND_ADIC>1</COND_ADIC>
    //<ASEG_ADIC>S</ASEG_ADIC>
    //<FH_NAC>19800101</FH_NAC>
    //mvarRequest = mvarRequest & "<TIENEPLAN>" & Request.Form("PLAN") & "</TIENEPLAN>" NO SE USA...
    //mvarRequest = mvarRequest & "<COND_ADIC>" & Request.Form("CONDADIC") & "</COND_ADIC>" NO SE USA...
    //mvarRequest = mvarRequest & "<ASEG_ADIC>" & Request.Form("ASEGUADICSINO") & "</ASEG_ADIC>" NO SE USA...
    //mvarRequest = mvarRequest & "<FH_NAC>" & Request.Form("NACIMANN") & Request.Form("NACIMMES") & Request.Form("NACIMDIA") & "</FH_NAC>"  NO SE USA...
    mvarRequest = mvarRequest + "<SEXO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//SEXO" )  ) + "</SEXO>";
    mvarRequest = mvarRequest + "<ESTCIV>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//ESTADO" )  ) + "</ESTCIV>";
    //MHC : Agregado de CDATA en la descripcion del vehiculo
    //mvarRequest =  mvarRequest & "<SIFMVEHI_DES>" & OwnIif(left(Request.Form("MODEAUTCOT"),50) = "", left(Request.Form("MODEAUT"),50), left(Request.Form("MODEAUTCOT"),50)) & "</SIFMVEHI_DES>"
    //mvarRequest =  mvarRequest & "<SIFMVEHI_DES><![CDATA[" & OwnIif(left(Request.Form("MODEAUTCOT"),50) = "", left(Request.Form("MODEAUT"),50), left(Request.Form("MODEAUTCOT"),50)) & "]]></SIFMVEHI_DES>"
    mvarRequest = mvarRequest + "<SIFMVEHI_DES><![CDATA[" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VEHDES" )  ) + "]]></SIFMVEHI_DES>";

    mvarRequest = mvarRequest + "<PROFECOD>000000</PROFECOD>";

    //<SIACCESORIOS>N</SIACCESORIOS>
    //mvarRequest = mvarRequest & "<SIACCESORIOS>" & Request.Form("SIACCESORIOS") & "</SIACCESORIOS>" NO SE USA
    mvarRequest = mvarRequest + "<REFERIDO>0</REFERIDO>";
    mvarRequest = mvarRequest + "<MOTORNUM>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//MOTORNUM" )  ) + "</MOTORNUM>";
    mvarRequest = mvarRequest + "<CHASINUM>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CHASINUM" )  ) + "</CHASINUM>";
    mvarRequest = mvarRequest + "<PATENNUM>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PATENNUM" )  ) + "</PATENNUM>";
    //mvarRequest =  mvarRequest & "<MOTORNUM>D547851125</MOTORNUM>"
    //mvarRequest =  mvarRequest & "<CHASINUM>CH04943245</CHASINUM>"
    //mvarRequest =  mvarRequest & "<PATENNUM>DFP252</PATENNUM>"
    mvarRequest = mvarRequest + "<AUMARCOD>" + Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//MODEAUTCOD" )  ), 1, 5 ) + "</AUMARCOD>";
    mvarRequest = mvarRequest + "<AUMODCOD>" + Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//MODEAUTCOD" )  ), 6, 5 ) + "</AUMODCOD>";
    mvarRequest = mvarRequest + "<AUSUBCOD>" + Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//MODEAUTCOD" )  ), 11, 5 ) + "</AUSUBCOD>";
    mvarRequest = mvarRequest + "<AUADICOD>" + Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//MODEAUTCOD" )  ), 16, 5 ) + "</AUADICOD>";
    mvarRequest = mvarRequest + "<AUMODORI>" + Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//MODEAUTCOD" )  ), 21, 1 ) + "</AUMODORI>";
    // VERIFICAR CAMPO (ASUMIMOS USO PARTICULAR)
    mvarRequest = mvarRequest + "<AUUSOCOD>1</AUUSOCOD>";
    mvarRequest = mvarRequest + "<AUVTVCOD>N</AUVTVCOD>";
    mvarRequest = mvarRequest + "<AUVTVDIA>0</AUVTVDIA>";
    mvarRequest = mvarRequest + "<AUVTVMES>0</AUVTVMES>";
    mvarRequest = mvarRequest + "<AUVTVANN>0</AUVTVANN>";
    mvarRequest = mvarRequest + "<VEHCLRCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VEHCLRCOD" )  ) + "</VEHCLRCOD>";
    mvarRequest = mvarRequest + "<AUKLMNUM>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//KMSRNGCOD" )  ) + "</AUKLMNUM>";
    mvarRequest = mvarRequest + "<FABRICAN>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//EFECTANN" )  ) + "</FABRICAN>";
    mvarRequest = mvarRequest + "<FABRICMES>1</FABRICMES>";
    mvarRequest = mvarRequest + "<GUGARAGE>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//SIGARAGE" )  ) + "</GUGARAGE>";
    mvarRequest = mvarRequest + "<GUDOMICI>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//GUDOMICI" )  ) + "</GUDOMICI>";
    mvarRequest = mvarRequest + "<AUCATCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//AUCATCOD" )  ) + "</AUCATCOD>";
    mvarRequest = mvarRequest + "<AUTIPCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//AUTIPCOD" )  ) + "</AUTIPCOD>";
    mvarRequest = mvarRequest + "<AUCIASAN> </AUCIASAN>";
    mvarRequest = mvarRequest + "<AUANTANN>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//AUANTANN" )  ) + "</AUANTANN>";
    // SINIESTROS
    mvarRequest = mvarRequest + "<AUNUMSIN>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//SINIESTROS" )  ) + "</AUNUMSIN>";

    //Se pone el kilometraje de la inspecci�n en dicho lugar.
    //VERIFICAR CAMPO
    mvarRequest = mvarRequest + "<AUNUMKMT>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//KMSRNGCOD" )  ) + "</AUNUMKMT>";
    mvarRequest = mvarRequest + "<AUUSOGNC>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//GAS" )  ) + "</AUUSOGNC>";
    //VERIFICAR CAMPO (SE ASUME CODINST)
    mvarRequest = mvarRequest + "<USUARCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CODINST" )  ) + "</USUARCOD>";
    //VERIFICAR CAMPO (SE ASUME 0)
    mvarRequest = mvarRequest + "<SITUCPOL>0</SITUCPOL>";
    //VERIFICAR CAMPO (SE ASUME QUE NO SE USA)
    mvarRequest = mvarRequest + "<CLIENSEC></CLIENSEC>";
    mvarRequest = mvarRequest + "<NUMEDOCU>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//NUMEDOCU" )  ) + "</NUMEDOCU>";
    mvarRequest = mvarRequest + "<TIPODOCU>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//TIPODOCU" )  ) + "</TIPODOCU>";
    //VERIFICAR CAMPO (se asume 1)
    mvarRequest = mvarRequest + "<DOMICSEC>1</DOMICSEC>";
    //VERIFICAR CAMPO (se asume 1)
    mvarRequest = mvarRequest + "<CUENTSEC>1</CUENTSEC>";
    mvarRequest = mvarRequest + "<COBROFOR>1</COBROFOR>";

    //<EDADACTU>33</EDADACTU>
    //mvarEdad = CalcularEdad(Request.Form("NACIMANN") & Request.Form("NACIMMES") & Request.Form("NACIMDIA"))
    //mvarRequest = mvarRequest & "<EDADACTU>" & mvarEdad & "</EDADACTU>"
    mvarRequest = mvarRequest + "<COBERTURAS></COBERTURAS>";


    //<ASEGURADOS>
    //    <ASEGURADO>
    //        <DOCUMDAT>20457856</DOCUMDAT>
    //        <NOMBREAS><![CDATA[PRUEBAADIC,ITDADIC]]></NOMBREAS>
    //    </ASEGURADO>
    //</ASEGURADOS>
    if( !wobjXMLRequest.selectSingleNode( "//ASEGURADOS-ADIC" ) .toString().equals( "<ASEGURADOS-ADIC/>" ) )
    {

      wvarXMLASEGADIC = new XmlDomExtended();
      wvarXMLASEGADIC.loadXML( /*unsup wobjXMLRequest.selectSingleNode( "//ASEGURADOS-ADIC" ) */.toString() );

      i = 0;
      mvarRequest = mvarRequest + "<ASEGURADOS>";
      for( int nx = 0; nx < wvarXMLASEGADIC.selectNodes( "//ASEGURADO-ADIC" ) .getLength(); nx++ )
      {
        x = wvarXMLASEGADIC.selectNodes( "//ASEGURADO-ADIC" ) .item( nx );
        mvarRequest = mvarRequest + "<ASEGURADO>";
        mvarRequest = mvarRequest + "<DOCUMDAT>" + XmlDomExtended.getText( x.selectNodes( "//DOCUMASEG" ) .item( i ) ) + "</DOCUMDAT>";
        mvarRequest = mvarRequest + "<NOMBREAS><![CDATA[" + XmlDomExtended.getText( x.selectNodes( "//NOMBREASEG" ) .item( i ) ) + "]]></NOMBREAS>";
        mvarRequest = mvarRequest + "</ASEGURADO>";
        i = i + 1;
      }
      mvarRequest = mvarRequest + "</ASEGURADOS>";
    }

    //mvarRequest = mvarRequest & "Request.Form('XMLASEGUADIC')" 'VERIFICAR CAMPO
    mvarRequest = mvarRequest + "<CAMP_CODIGO>" + Strings.right( ("0000" + Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CAMPACOD" )  ) )), 4 ) + "</CAMP_CODIGO>";
    mvarRequest = mvarRequest + "<NRO_PROD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//AGECOD" )  ) + "</NRO_PROD>";
    //MHC: Soluci�n T�ctica Etapa III - Bancos
    if( ! (wobjXMLRequest.selectSingleNode( "//CANAL" )  == (org.w3c.dom.Node) null) )
    {
      if( Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CANAL" )  ) ) == Obj.toInt( "150" ) )
      {
        mvarRequest = mvarRequest + "<SUCURSAL_CODIGO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CANAL_SUCURSAL" )  ) + "</SUCURSAL_CODIGO>";
        mvarRequest = mvarRequest + "<LEGAJO_VEND>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LEGAJO_VEND" )  ) + "</LEGAJO_VEND>";
        mvarRequest = mvarRequest + "<EMPRESA_CODIGO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LEGAJCIA" )  ) + "</EMPRESA_CODIGO>";
        mvarRequest = mvarRequest + "<EMPL></EMPL>";
        mvarRequest = mvarRequest + "<LEGAJO_GTE></LEGAJO_GTE>";
      }
      else
      {
        mvarRequest = mvarRequest + "<SUCURSAL_CODIGO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CANAL_SUCURSAL" )  ) + "</SUCURSAL_CODIGO>";
        mvarRequest = mvarRequest + "<LEGAJO_VEND>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LEGAJO_VEND" )  ) + "</LEGAJO_VEND>";
        mvarRequest = mvarRequest + "<EMPRESA_CODIGO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LEGAJCIA" )  ) + "</EMPRESA_CODIGO>";
        mvarRequest = mvarRequest + "<EMPL></EMPL>";
        mvarRequest = mvarRequest + "<LEGAJO_GTE></LEGAJO_GTE>";
      }
    }
    else
    {
      mvarRequest = mvarRequest + "<SUCURSAL_CODIGO>8888</SUCURSAL_CODIGO>";
      mvarRequest = mvarRequest + "<LEGAJO_VEND></LEGAJO_VEND>";
      mvarRequest = mvarRequest + "<EMPRESA_CODIGO>0</EMPRESA_CODIGO>";
      mvarRequest = mvarRequest + "<EMPL></EMPL>";
      mvarRequest = mvarRequest + "<LEGAJO_GTE></LEGAJO_GTE>";
    }
    // VERIFICAR CAMPOS
    mvarRequest = mvarRequest + wobjXMLRequest.selectSingleNode( "//HIJOS" ) .toString();

    //ACCESORIOS
    //If Request.Form("XMLACCESORIOS") <> "" Then
    //<EMPL_NOMYAPE/>
    //<CLIENTE_NOMYAPE>PRUEBA ITD</CLIENTE_NOMYAPE>
    if( !wobjXMLRequest.selectSingleNode( "//ACCESORIOS" ) .toString().equals( "<ACCESORIOS/>" ) )
    {

      wvarXMLACCESORIOS = new XmlDomExtended();
      wvarXMLACCESORIOS.loadXML( /*unsup wobjXMLRequest.selectSingleNode( "//ACCESORIOS" ) */.toString() );

      i = 1;
      for( int nx = 0; nx < wvarXMLACCESORIOS.selectNodes( "//ACCESORIO" ) .getLength(); nx++ )
      {
        x = wvarXMLACCESORIOS.selectNodes( "//ACCESORIO" ) .item( nx );
        wobjXML = (diamondedge.util.XmlDom) wvarXMLACCESORIOS.getDocument().createElement( "VEACCSEC" );
        if( x.selectSingleNode( "./DEPRECIA" )  == (org.w3c.dom.Node) null )
        {
          wobjXMLAux = (diamondedge.util.XmlDom) wvarXMLACCESORIOS.getDocument().createElement( "DEPRECIA" );
          /*unsup x.appendChild( wobjXMLAux ) */;
          XmlDomExtended.setText( wobjXMLAux.getDocument(), "N" );
        }
        /*unsup x.appendChild( wobjXML ) */;
        XmlDomExtended.setText( wobjXML.getDocument(), String.valueOf( i ) );
        i = i + 1;
      }
    }
    else
    {
      mvarRequest = mvarRequest + "<ACCESORIOS/>";
    }
    //mvarRequest = mvarRequest & wvarXMLACCESORIOS.xml
    //inspeccion
    //mvarRequest =  mvarRequest & "<SITUCEST>" & owniif(Request.Form("RB0KM")="S","S","D") & "</SITUCEST>"
    //VERIFICAR CAMPO (Se asume 3)
    mvarRequest = mvarRequest + "<ESTAINSP>3</ESTAINSP>";
    //mvarRequest =  mvarRequest & "<ESTAINSP>3</ESTAINSP>"
    mvarRequest = mvarRequest + "<INSPECOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//INSPECOD" )  ) + "</INSPECOD>";
    //VERIFICAR CAMPO
    mvarRequest = mvarRequest + "<INSPECTOR></INSPECTOR>";
    mvarRequest = mvarRequest + "<INSPEADOM>S</INSPEADOM>";
    //VERIFICAR CAMPO
    mvarRequest = mvarRequest + "<OBSERTXT></OBSERTXT>";
    //mvarRequest =  mvarRequest & "<INSPENUM>0</INSPENUM>"
    //VERIFICAR CAMPOS
    mvarRequest = mvarRequest + "<INSPEOBLEA></INSPEOBLEA>";
    //VERIFICAR CAMPOS
    mvarRequest = mvarRequest + "<INSPEKMS></INSPEKMS>";
    //VERIFICAR CAMPOS
    mvarRequest = mvarRequest + "<CENTRCOD_INS></CENTRCOD_INS>";
    //VERIFICAR CAMPOS
    mvarRequest = mvarRequest + "<CENTRDES></CENTRDES>";
    //mvarRequest =  mvarRequest & "<INSPECC>" & owniif(trim(mvarDATOS_INSPECCION")) <> "",mvarDATOS_INSPECCION"),"0") & "</INSPECC>"
    mvarRequest = mvarRequest + "<RASTREO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//RASTREO" )  ) + "</RASTREO>";
    //mvarRequest =  mvarRequest & "<ALARMCOD>" & owniif(trim(mvarALARMAS")) <> "",mvarALARMAS"),"0") & "</ALARMCOD>"
    mvarRequest = mvarRequest + "<ALARMCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//ALARMCOD" )  ) + "</ALARMCOD>";
    mvarRequest = mvarRequest + "<INSPEANN>0</INSPEANN>";
    mvarRequest = mvarRequest + "<INSPEMES>0</INSPEMES>";
    mvarRequest = mvarRequest + "<INSPEDIA>0</INSPEDIA>";
    //fin inspeccion
    //If Request.form("PRENDASINO") <> "S" Then
    if( ! (wobjXMLRequest.selectSingleNode( mcteParam_ACREPREN )  == (org.w3c.dom.Node) null) )
    {
      if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ACREPREN )  ) ).equals( "S" ) )
      {
        mvarRequest = mvarRequest + "<NUMEDOCU_ACRE>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCUACRE )  ) + "</NUMEDOCU_ACRE>";
        mvarRequest = mvarRequest + "<TIPODOCU_ACRE>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCUACRE )  ) + "</TIPODOCU_ACRE>";
        mvarRequest = mvarRequest + "<APELLIDO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_APELLIDOACRE )  ) + "</APELLIDO>";
        mvarRequest = mvarRequest + "<NOMBRES>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NOMBRESACRE )  ) + "</NOMBRES>";
        mvarRequest = mvarRequest + "<DOMICDOM>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOMACRE )  ) + "</DOMICDOM>";
        mvarRequest = mvarRequest + "<DOMICDNU>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICDNUACRE )  ) + "</DOMICDNU>";
        mvarRequest = mvarRequest + "<DOMICESC> </DOMICESC>";
        mvarRequest = mvarRequest + "<DOMICPIS>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICPISACRE )  ) + "</DOMICPIS>";
        mvarRequest = mvarRequest + "<DOMICPTA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICPTAACRE )  ) + "</DOMICPTA>";
        mvarRequest = mvarRequest + "<DOMICPOB>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOBACRE )  ) + "</DOMICPOB>";
        mvarRequest = mvarRequest + "<DOMICCPO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICCPOACRE )  ) + "</DOMICCPO>";
        mvarRequest = mvarRequest + "<PROVICOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PROVICODACRE )  ) + "</PROVICOD>";
        mvarRequest = mvarRequest + "<PAISSCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PAISSCODACRE )  ) + "</PAISSCOD>";
      }
      else
      {
        mvarRequest = mvarRequest + "<NUMEDOCU_ACRE> </NUMEDOCU_ACRE>";
        mvarRequest = mvarRequest + "<TIPODOCU_ACRE>0</TIPODOCU_ACRE>";
        mvarRequest = mvarRequest + "<APELLIDO> </APELLIDO>";
        mvarRequest = mvarRequest + "<NOMBRES> </NOMBRES>";
        mvarRequest = mvarRequest + "<DOMICDOM> </DOMICDOM>";
        mvarRequest = mvarRequest + "<DOMICDNU> </DOMICDNU>";
        mvarRequest = mvarRequest + "<DOMICESC> </DOMICESC>";
        mvarRequest = mvarRequest + "<DOMICPIS> </DOMICPIS>";
        mvarRequest = mvarRequest + "<DOMICPTA> </DOMICPTA>";
        mvarRequest = mvarRequest + "<DOMICPOB> </DOMICPOB>";
        mvarRequest = mvarRequest + "<DOMICCPO>0</DOMICCPO>";
        mvarRequest = mvarRequest + "<PROVICOD>0</PROVICOD>";
        mvarRequest = mvarRequest + "<PAISSCOD> </PAISSCOD>";
      }
    }
    else
    {
      mvarRequest = mvarRequest + "<NUMEDOCU_ACRE> </NUMEDOCU_ACRE>";
      mvarRequest = mvarRequest + "<TIPODOCU_ACRE>0</TIPODOCU_ACRE>";
      mvarRequest = mvarRequest + "<APELLIDO> </APELLIDO>";
      mvarRequest = mvarRequest + "<NOMBRES> </NOMBRES>";
      mvarRequest = mvarRequest + "<DOMICDOM> </DOMICDOM>";
      mvarRequest = mvarRequest + "<DOMICDNU> </DOMICDNU>";
      mvarRequest = mvarRequest + "<DOMICESC> </DOMICESC>";
      mvarRequest = mvarRequest + "<DOMICPIS> </DOMICPIS>";
      mvarRequest = mvarRequest + "<DOMICPTA> </DOMICPTA>";
      mvarRequest = mvarRequest + "<DOMICPOB> </DOMICPOB>";
      mvarRequest = mvarRequest + "<DOMICCPO>0</DOMICCPO>";
      mvarRequest = mvarRequest + "<PROVICOD>0</PROVICOD>";
      mvarRequest = mvarRequest + "<PAISSCOD> </PAISSCOD>";
    }
    //MHC: Soluci�n T�ctica Etapa III - Bancos
    //mvarRequest =  mvarRequest & "<BANCOCOD>9000</BANCOCOD>"
    mvarRequest = mvarRequest + "<BANCOCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CODINST" )  ) + "</BANCOCOD>";
    //mvarRequest =  mvarRequest & "<BANCOCOD>"  & Request.form("BANCOCOD") & "</BANCOCOD>"
    mvarRequest = mvarRequest + "<COBROCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//COBROCOD" )  ) + "</COBROCOD>";
    //mvarRequest =  mvarRequest & "<COBROCOD>4</COBROCOD>"
    mvarRequest = mvarRequest + "<EFECTANN2>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VIGENANN" )  ) + "</EFECTANN2>";
    mvarRequest = mvarRequest + "<EFECTMES>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VIGENMES" )  ) + "</EFECTMES>";
    mvarRequest = mvarRequest + "<EFECTDIA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VIGENDIA" )  ) + "</EFECTDIA>";


    //<INSPEANN>0</INSPEANN>
    //<INSPEMES>0</INSPEMES>
    //<INSPEDIA>0</INSPEDIA>
    //<ENTDOMSC>2</ENTDOMSC>
    //<ANOTACIO><![CDATA[]]></ANOTACIO>
    //mvarRequest = mvarRequest & "<ENTDOMSC>" & mvarDOMICENTREGA & "</ENTDOMSC>" NO SE UTILIZA
    //mvarRequest = mvarRequest & "<ANOTACIO><![CDATA[" & Request.Form("ANOTACIONES") & "]]></ANOTACIO>" NO SE UTILIZA
    mvarRequest = mvarRequest + "<PRECIO_MENSUAL>" + Strings.replace( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PRECIO" )  ), ",", "." ) + "</PRECIO_MENSUAL>";


    //<MODEAUTCOD>00009000870000103481N</MODEAUTCOD>
    //mvarRequest = mvarRequest & "<MODEAUTCOD>" & Request.Form("MODEAUTCOD") & "</MODEAUTCOD>"  NO SE UTILIZA
    mvarRequest = mvarRequest + "<EFECTANN>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//EFECTANN" )  ) + "</EFECTANN>";
    mvarRequest = mvarRequest + "<NACIMANN>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//NACIMANN" )  ) + "</NACIMANN>";
    mvarRequest = mvarRequest + "<NACIMMES>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//NACIMMES" )  ) + "</NACIMMES>";
    mvarRequest = mvarRequest + "<NACIMDIA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//NACIMDIA" )  ) + "</NACIMDIA>";

    mvarRequest = mvarRequest + "<IVA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//IVA" )  ) + "</IVA>";
    if( ! (wobjXMLRequest.selectSingleNode( "//IBB" )  == (org.w3c.dom.Node) null) )
    {
      mvarRequest = mvarRequest + "<IBB>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//IBB" )  ) + "</IBB>";
    }
    mvarRequest = mvarRequest + "<ESTADO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//ESTADO" )  ) + "</ESTADO>";
    mvarRequest = mvarRequest + "<KMSRNGCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//KMSRNGCOD" )  ) + "</KMSRNGCOD>";
    mvarRequest = mvarRequest + "<SIGARAGE>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//SIGARAGE" )  ) + "</SIGARAGE>";
    mvarRequest = mvarRequest + "<SINIESTROS>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//SINIESTROS" )  ) + "</SINIESTROS>";
    mvarRequest = mvarRequest + "<GAS>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//GAS" )  ) + "</GAS>";
    mvarRequest = mvarRequest + "<PROVI>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PROVI" )  ) + "</PROVI>";

    mvarRequest = mvarRequest + "<LOCALIDADCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LOCALIDADCOD" )  ) + "</LOCALIDADCOD>";
    mvarRequest = mvarRequest + "<CLUBLBA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CLUBLBA" )  ) + "</CLUBLBA>";

    //MHC: se agregan nuevas marcas al cotizador
    mvarRequest = mvarRequest + "<CLUBECO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CLUBECO" )  ) + "</CLUBECO>";
    mvarRequest = mvarRequest + "<GRANIZO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//GRANIZO" )  ) + "</GRANIZO>";
    mvarRequest = mvarRequest + "<ROBOCONT>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//ROBOCONT" )  ) + "</ROBOCONT>";


    //<DATOSPLAN>01503</DATOSPLAN>
    //<CAMPACOD>0900</CAMPACOD>
    //<CAMP_DESC><![CDATA[BAJA POR ALTA 2005]]></CAMP_DESC>
    //<AGECLA>PR</AGECLA>
    //<AGENTCLA>PR</AGENTCLA>
    //mvarRequest = mvarRequest & "<DATOSPLAN>" & mvarDATOSPLAND & "</DATOSPLAN>" NO SE UTILIZA
    //mvarRequest = mvarRequest  & "<DATOSPLAN>00099</DATOSPLAN>"
    //mvarRequest = mvarRequest & "<CAMPACOD>" & Right("0000" + Trim(Request.Form("CAMPACOD")), 4) & "</CAMPACOD>" NO SE UTILIZA
    //DA - 21/02/2007: Se manda con CDATA porque si tiene caracteres especiales pincha.
    //mvarRequest = mvarRequest & "<CAMP_DESC><![CDATA[" & Request.Form("CAMP_DESC") & "]]></CAMP_DESC>" NO SE UTILIZA
    //mvarRequest = mvarRequest  & "<CAMP_DESC>"    & Request.Form("CAMP_DESC")      & "</CAMP_DESC>"
    if( ! (wobjXMLRequest.selectSingleNode( "//AGECLA" )  == (org.w3c.dom.Node) null) )
    {
      //MMC 2012-12-05 - Verificar Campo
      mvarRequest = mvarRequest + "<AGECLA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//AGECLA" )  ) + "</AGECLA>";
      //mvarRequest = mvarRequest & "<AGENTCLA>" & Request.Form("AGENTCLA") & "</AGENTCLA>" ' NO SE UTILIZA
    }
    mvarRequest = mvarRequest + "<AGECOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//AGECOD" )  ) + "</AGECOD>";

    //<PORTAL>LBA_PRODUCTORES</PORTAL>
    //mvarRequest = mvarRequest & "<PORTAL>" & "LBA_PRODUCTORES" & "</PORTAL>" NO SE UTILIZA
    mvarRequest = mvarRequest + "<COBROTIP>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//COBROTIP" )  ) + "</COBROTIP>";
    //mvarRequest = mvarRequest  & "<COBROTIP>AC</COBROTIP>"
    //<CUITNUME/>
    //<RAZONSOC><![CDATA[]]></RAZONSOC>
    //<NROFACTU/>
    //mvarRequest = mvarRequest & "<CUITNUME>" & Request.Form("NUMCUIT") & "</CUITNUME>" NO SE UTILIZA
    //mvarRequest = mvarRequest & "<RAZONSOC><![CDATA[" & Request.Form("RAZON_SOCIAL") & "]]></RAZONSOC>" NO SE UTILIZA
    //verificar campo
    mvarRequest = mvarRequest + "<CLIEIBTP>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//IBB" )  ) + "</CLIEIBTP>";
    mvarRequest = mvarRequest + "<NROIIBB>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//NROIBB" )  ) + "</NROIIBB>";
    mvarRequest = mvarRequest + "<LUNETA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LUNETA" )  ) + "</LUNETA>";

    //AGREGO YO - JP
    //If Request.Form("FACT0KM") = "N" Then
    //    mvarRequest = mvarRequest & "<NROFACTU></NROFACTU>"
    //Else
    //    If Trim(Request.Form("FACT0KM")) = "" Then
    if( ! (wobjXMLRequest.selectSingleNode( "//NROFACTU" )  == (org.w3c.dom.Node) null) )
    {
      //verificar campo
      mvarRequest = mvarRequest + "<NROFACTU>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//NROFACTU" )  ) + "</NROFACTU>";
    }
    else
    {
      mvarRequest = mvarRequest + "<NROFACTU/>";
    }

    // VERIFICAR CAMPO
    mvarRequest = mvarRequest + "<DDJJ_STRING></DDJJ_STRING>";

    //   VERIFICAR CAMPOS
    mvarRequest = mvarRequest + "<DERIVACI></DERIVACI>";
    //   VERIFICAR CAMPOS
    mvarRequest = mvarRequest + "<ALEATORIO></ALEATORIO>";
    //   VERIFICAR CAMPOS
    mvarRequest = mvarRequest + "<ESTAIDES></ESTAIDES>";
    //   VERIFICAR CAMPOS
    mvarRequest = mvarRequest + "<DISPODES></DISPODES>";

    //NO SE UTILIZA
    mvarRequest = mvarRequest + "<INSTALADP>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//INSTALADP" )  ) + "</INSTALADP>";
    //NO SE UTILIZA
    mvarRequest = mvarRequest + "<POSEEDISP>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//POSEEDISP" )  ) + "</POSEEDISP>";


    //<AUTIPGAMA>B</AUTIPGAMA>
    //<SN_NBWS>S</SN_NBWS>
    //<EMAIL_NBWS>MARTIN.CABRERA@HSBC.COM.AR</EMAIL_NBWS>
    //<SN_EPOLIZA>N</SN_EPOLIZA>
    //<EMAIL_EPOLIZA/>
    //<SN_PRESTAMAIL>N</SN_PRESTAMAIL>
    //<PASSEPOL/>
    //mvarRequest = mvarRequest & "<AUTIPGAMA>" & Request.Form("AUTIPGAMA") & "</AUTIPGAMA>" 'NO SE UTILIZA
    //mvarRequest = mvarRequest & "<SN_NBWS>" & mvarSuscripto & "</SN_NBWS>" NO LO UTILIZA
    //mvarRequest = mvarRequest & "<email_NBWS>" & mvarMailSuscripto & "</email_NBWS>" NO LO UTILIZA
    //mvarRequest = mvarRequest & "<SN_EPOLIZA>" & Request.Form("SN_EPOLIZA") & "</SN_EPOLIZA>"  NO LO UTILIZA
    //mvarRequest = mvarRequest & "<email_EPOLIZA>" & Request.Form("email_EPOLIZA") & "</email_EPOLIZA>" NO LO UTILIZA
    //mvarRequest = mvarRequest & "<SN_PRESTAMAIL>" & Request.Form("SN_PRESTAMAIL") & "</SN_PRESTAMAIL>" NO LO UTILIZA
    //13/10/2009 Se agrega Password de EPoliza por nueva definicion: AUS1 = (3 ult dig doc) + (3 ult dig patente)
    //Este Password se utilizara solo para Integracion, en Emision Online el pass es generado por el back.-
    //Las patentes A/D no pueden suscribirse a EPoliza. Por lo tanto, en este caso no guardo pass.-
    //mvarPATENTE =  trim(Request.Form("PATENTE")) Esto esta definido arriba
    //Dim mvarNUMEDOCU
    //Dim mvarPASSEPOL
    //mvarNUMEDOCU = Request.Form("NUMEDOCU")
    //If UCase(Trim(mvarPATENTE)) <> "A/D" And Request.Form("SN_EPOLIZA") = "S" Then
    //    mvarPASSEPOL = Right("000" + Trim(mvarNUMEDOCU), 3) & Right("000" + Trim(mvarPATENTE), 3)
    //Else
    //    mvarPASSEPOL = ""
    //End If
    //mvarRequest = mvarRequest & "<PASSEPOL>" & mvarPASSEPOL & "</PASSEPOL>" NO SE UTILIZA
    //Dim y
    //Dim mvarEMAIL_NBWS
    //Dim mvarEMAIL_EPOLIZA
    //Dim mvarMEDCOCOD_NBWS
    //Dim mvarMEDCOCOD_EPOL
    mvarRequest = mvarRequest + "<MEDIOSCONTACTO>";

    //Por default mvarSuscripto y Request.Form("SN_EPOLIZA") = N
    //El ciclo se lo toma por 10 sin tener  en cuenta los nodos 9 y 10
    for( y = 1; y <= 10; y++ )
    {
      //Ciclo 10 veces en blanco
      mvarRequest = mvarRequest + "<MEDIO>";
      mvarRequest = mvarRequest + "<MEDCOSEC></MEDCOSEC>";
      mvarRequest = mvarRequest + "<MEDCOCOD></MEDCOCOD>";
      mvarRequest = mvarRequest + "<MEDCODAT></MEDCODAT>";
      mvarRequest = mvarRequest + "<MEDCOSYS></MEDCOSYS>";
      mvarRequest = mvarRequest + "</MEDIO>";
    }

    //LR - ANEXO I: En el nodo 9 agrego los datos para Seguros Online(NBWS)
    //GD - ANEXO I: Se modifica el origen de la marca y el mail
    //If (mvarSuscripto = "S") Then
    //    mvarEMAIL_NBWS = mvarMailSuscripto
    //    mvarMEDCOCOD_NBWS = "EMA"
    //Else
    //    mvarEMAIL_NBWS = ""
    //    mvarMEDCOCOD_NBWS = ""
    //End If
    //mvarRequest = mvarRequest & "<MEDIO>"
    //    mvarRequest = mvarRequest & "<MEDCOSEC>0</MEDCOSEC>" 'va en blanco
    //    mvarRequest = mvarRequest & "<MEDCOCOD>" & mvarMEDCOCOD_NBWS & "</MEDCOCOD>" 'me mand�s "EMA".
    //    mvarRequest = mvarRequest & "<MEDCODAT>" & mvarEMAIL_NBWS & "</MEDCODAT>" 'el mail en caso que el cliente se suscribe.
    //    mvarRequest = mvarRequest & "<MEDCOSYS></MEDCOSYS>" 'va en blanco
    //mvarRequest = mvarRequest & "</MEDIO>"
    //Por ultimo en el nodo decimo agrego los datos para EPoliza
    //If (Request.Form("SN_EPOLIZA") = "S") Then
    //    mvarEMAIL_EPOLIZA = Request.Form("email_EPOLIZA")
    //    mvarMEDCOCOD_EPOL = "EMA"
    //Else
    //    mvarEMAIL_EPOLIZA = ""
    //    mvarMEDCOCOD_EPOL = ""
    //End If
    //mvarRequest = mvarRequest & "<MEDIO>"
    //    mvarRequest = mvarRequest & "<MEDCOSEC>0</MEDCOSEC>" 'va en blanco
    //    mvarRequest = mvarRequest & "<MEDCOCOD>" & mvarMEDCOCOD_EPOL & "</MEDCOCOD>" 'me mand�s "EPO".
    //    mvarRequest = mvarRequest & "<MEDCODAT>" & mvarEMAIL_EPOLIZA & "</MEDCODAT>" 'el mail en caso que el cliente se suscribe.
    //    mvarRequest = mvarRequest & "<MEDCOSYS></MEDCOSYS>" 'va en blanco
    //mvarRequest = mvarRequest & "</MEDIO>"
    mvarRequest = mvarRequest + "</MEDIOSCONTACTO>";


    //<CERTISEC>861</CERTISEC>
    //Datos para AIS
    mvarRequest = mvarRequest + "<TIPOOPERACION>8</TIPOOPERACION>";
    mvarRequest = mvarRequest + "<OPCION>01</OPCION>";
    mvarRequest = mvarRequest + "<FUNCION>1</FUNCION>";

    mvarRequest = mvarRequest + "<GENERARLOG></GENERARLOG>";

    //Datos Cliente
    mvarRequest = mvarRequest + "<CLICLIENAP1><![CDATA[" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CLIENAP1" )  ) + "]]></CLICLIENAP1>";
    mvarRequest = mvarRequest + "<AISCLICLIENNOM><![CDATA[" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CLIENNOM" )  ) + "]]></AISCLICLIENNOM>";
    mvarRequest = mvarRequest + "<CLICLIENNOM><![CDATA[" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CLIENNOM" )  ) + "]]></CLICLIENNOM>";

    //FJO - 2009-02-11
    //Persona fisica
    mvarRequest = mvarRequest + "<CLICLIENTIP>00</CLICLIENTIP>";
    //mvarRequest = mvarRequest & "<CLICLIENTIP>" & Right("00" & Request.Form("TIPOPERSONA"), 2) & "</CLICLIENTIP>" 'Tipo de Persona
    //FJO - 2009-02-11
    //DA - 20/02/2007: Datos exigido x la UIF
    //VERIFICAR CAMPO
    mvarRequest = mvarRequest + "<UIFCUIT></UIFCUIT>";

    //Domicilio Cliente
    mvarRequest = mvarRequest + "<CLIDOMICCAL>PAR</CLIDOMICCAL>";
    mvarRequest = mvarRequest + "<CLIDOMICDOM><![CDATA[" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DOMICDOM" )  ) + "]]></CLIDOMICDOM>";


    mvarRequest = mvarRequest + "<CLIDOMICDNU>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DOMICDNU" )  ) + "</CLIDOMICDNU>";
    mvarRequest = mvarRequest + "<CLIDOMICESC></CLIDOMICESC>";
    mvarRequest = mvarRequest + "<CLIDOMICPIS>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DOMICPIS" )  ) + "</CLIDOMICPIS>";
    mvarRequest = mvarRequest + "<CLIDOMICPTA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DOMICPTA" )  ) + "</CLIDOMICPTA>";
    mvarRequest = mvarRequest + "<CLIDOMICPOB><![CDATA[" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DOMICPOB" )  ) + "]]></CLIDOMICPOB>";
    mvarRequest = mvarRequest + "<CLIDOMICCPO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LOCALIDADCOD" )  ) + "</CLIDOMICCPO>";
    mvarRequest = mvarRequest + "<CLIDOMPROVICOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PROVI" )  ) + "</CLIDOMPROVICOD>";


    //If Not wobjXMLRequest.selectSingleNode("//CLIDOMICDNU") Is Nothing Then
    //    mvarRequest = mvarRequest & "<CLIDOMICDNU>" & wobjXMLRequest.selectSingleNode("//CLIDOMICDNU").Text & "</CLIDOMICDNU>"
    //    mvarRequest = mvarRequest & "<CLIDOMICESC>" & wobjXMLRequest.selectSingleNode("//CLIDOMICESC").Text & "</CLIDOMICESC>"
    //    mvarRequest = mvarRequest & "<CLIDOMICPIS>" & wobjXMLRequest.selectSingleNode("//CLIDOMICPIS").Text & "</CLIDOMICPIS>"
    //    mvarRequest = mvarRequest & "<CLIDOMICPTA>" & wobjXMLRequest.selectSingleNode("//CLIDOMICCPTA").Text & "</CLIDOMICPTA>"
    //    mvarRequest = mvarRequest & "<CLIDOMICPOB><![CDATA[" & wobjXMLRequest.selectSingleNode("//CLIDOMICPOB").Text & "]]></CLIDOMICPOB>"
    //mvarRequest =  mvarRequest & "<CLIDOMICPOB>" & Request.form("LOCALIDAD_C")     & "</CLIDOMICPOB>"
    //mvarRequest =  mvarRequest & "<CLIDOMICPOB><![CDATA[" & Request.form("LOCALIDAD_DES")     & "]]></CLIDOMICPOB>"
    //Si no funciona probar con LOCALIDADDSP
    //    mvarRequest = mvarRequest & "<CLIDOMICCPO>" & wobjXMLRequest.selectSingleNode("//CLIDOMICCPO").Text & "</CLIDOMICCPO>"    'VERIFICAR CAMPO
    //mvarRequest =  mvarRequest & "<CLIDOMICCPO>" & Request.form("CODPOSTAL")     & "</CLIDOMICCPO>"
    //    mvarRequest = mvarRequest & "<CLIDOMPROVICOD>" & wobjXMLRequest.selectSingleNode("//CLIDOMPROVICOD").Text & "</CLIDOMPROVICOD>"
    //End If
    mvarRequest = mvarRequest + "<CLIDOMPAISSCOD>00</CLIDOMPAISSCOD>";

    //Datos de tarjeta / CBU
    mvarRequest = mvarRequest + "<CUENTDC>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//COBROTIP" )  ) + "</CUENTDC>";
    if( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//COBROTIP" )  ).equals( "DB" ) )
    {
      mvarRequest = mvarRequest + "<CUENNUME>" + Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CUENNUME" )  ), 1, 16 ) + "</CUENNUME>";
    }
    else if( (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//COBROTIP" )  ).equals( "CC" )) || (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//COBROTIP" )  ).equals( "CA" )) )
    {
      //MHC: Debito en cuenta CC o CA
      if( Strings.len( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CUENNUME" )  ) ) > 16 )
      {
        mvarRequest = mvarRequest + "<CUENNUME>" + Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CUENNUME" )  ), 1, 16 ) + "</CUENNUME>";
      }
      else
      {
        mvarRequest = mvarRequest + "<CUENNUME>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CUENNUME" )  ) + "</CUENNUME>";
      }
      mvarRequest = mvarRequest + "<BANCOCODCUE>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CANAL" )  ) + "</BANCOCODCUE>";
      mvarRequest = mvarRequest + "<SUCURCODCUE>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CANAL_SUCURSAL" )  ) + "</SUCURCODCUE>";
    }
    else
    {
      mvarRequest = mvarRequest + "<CUENNUME>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CUENNUME" )  ) + "</CUENNUME>";
    }

    // ASUMO QUE ES PLANNCOD
    if( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PLANNCOD" )  ).equals( "4" ) )
    {
      mvarRequest = mvarRequest + "<VENCIANN>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VENCIANN" )  ) + "</VENCIANN>";
      mvarRequest = mvarRequest + "<VENCIMES>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VENCIMES" )  ) + "</VENCIMES>";
    }
    else if( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//COBROTIP" )  ).equals( "DB" ) )
    {
      // VERIFICAR CAMPO
      mvarRequest = mvarRequest + "<VENCIMES>" + Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VENCIMES" )  ), 17, 2 ) + "</VENCIMES>";
      // VERIFICAR CAMPO
      mvarRequest = mvarRequest + "<VENCIANN>" + Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VENCIANN" )  ), 19, 4 ) + "</VENCIANN>";
    }
    else if( (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//COBROTIP" )  ).equals( "CA" )) || (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//COBROTIP" )  ).equals( "CC" )) )
    {
      //MHC: Debito en cuenta CC o CA
      mvarRequest = mvarRequest + "<VENCIMES></VENCIMES>";
      if( Strings.len( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CUENNUME" )  ) ) > 16 )
      {
        mvarLargo = Strings.len( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CUENNUME" )  ) ) - 16;
        mvarRequest = mvarRequest + "<VENCIANN>" + Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CUENNUME" )  ), 17, mvarLargo ) + "</VENCIANN>";
      }
      else
      {
        mvarRequest = mvarRequest + "<VENCIANN></VENCIANN>";
      }
    }
    //       mvarRequest = mvarRequest & "<TARJECOD>4</TARJECOD>"
    //Hijos / Conductores
    mvarRequest = mvarRequest + "<CONDUCTORES>";
    wvarXMLHijos = new XmlDomExtended();
    wvarXMLHijos.loadXML( /*unsup wobjXMLRequest.selectSingleNode( "//HIJOS" ) */.toString() );
    y = 1;
    //Primero doy de alta al due�o y despues itero a los hijos
    mvarRequest = mvarRequest + "<CONDUCTOR>";
    mvarRequest = mvarRequest + "<CONDUSEC>" + y + "</CONDUSEC>";
    mvarRequest = mvarRequest + "<CONDUTDO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//NUMEDOCU" )  ) + "</CONDUTDO>";
    mvarRequest = mvarRequest + "<CONDUDOC>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//TIPODOCU" )  ) + "</CONDUDOC>";
    mvarRequest = mvarRequest + "<APELLIDOHIJO><![CDATA[" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CLIENAP1" )  ) + "]]></APELLIDOHIJO>";
    mvarRequest = mvarRequest + "<NOMBREHIJO><![CDATA[" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CLIENNOM" )  ) + "]]></NOMBREHIJO>";
    mvarRequest = mvarRequest + "<SEXOHIJO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//SEXO" )  ) + "</SEXOHIJO>";
    mvarRequest = mvarRequest + "<ESTADOHIJO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//ESTADO" )  ) + "</ESTADOHIJO>";
    mvarRequest = mvarRequest + "<NACIMHIJO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//NACIMDIA" )  ) + "/" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//NACIMMES" )  ) + "/" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//NACIMANN" )  ) + "</NACIMHIJO>";
    mvarRequest = mvarRequest + "<EXCLUIDO>N</EXCLUIDO>";
    mvarRequest = mvarRequest + "</CONDUCTOR>";

    for( int nx = 0; nx < wvarXMLHijos.selectNodes( "//HIJO" ) .getLength(); nx++ )
    {
      x = wvarXMLHijos.selectNodes( "//HIJO" ) .item( nx );
      y = y + 1;
      mvarRequest = mvarRequest + "<CONDUCTOR>";
      mvarRequest = mvarRequest + "<CONDUSEC>" + y + "</CONDUSEC>";
      mvarRequest = mvarRequest + "<CONDUTDO>1</CONDUTDO>";
      mvarRequest = mvarRequest + "<CONDUDOC>0</CONDUDOC>";
      mvarRequest = mvarRequest + "<APELLIDOHIJO><![CDATA[" + XmlDomExtended.getText( x.selectSingleNode( "./APELLIDOHIJO" )  ) + "]]></APELLIDOHIJO>";
      mvarRequest = mvarRequest + "<NOMBREHIJO><![CDATA[" + XmlDomExtended.getText( x.selectSingleNode( "./NOMBREHIJO" )  ) + "]]></NOMBREHIJO>";
      mvarRequest = mvarRequest + "<SEXOHIJO>" + XmlDomExtended.getText( x.selectSingleNode( "./SEXOHIJO" )  ) + "</SEXOHIJO>";
      mvarRequest = mvarRequest + "<ESTADOHIJO>" + XmlDomExtended.getText( x.selectSingleNode( "./ESTADOHIJO" )  ) + "</ESTADOHIJO>";
      mvarRequest = mvarRequest + "<NACIMHIJO>" + Strings.mid( XmlDomExtended.getText( x.selectSingleNode( "./NACIMHIJO" )  ), 1, 2 ) + "/" + Strings.mid( XmlDomExtended.getText( x.selectSingleNode( "./NACIMHIJO" )  ), 3, 2 ) + "/" + Strings.mid( XmlDomExtended.getText( x.selectSingleNode( "./NACIMHIJO" )  ), 5, 4 ) + "</NACIMHIJO>";
      mvarRequest = mvarRequest + "<EXCLUIDO>N</EXCLUIDO>";
      //            If x.selectSingleNode("./INCLUIDO").Text = "0" Then 'VERIFICAR CAMPO
      //                mvarExcluido = "S"
      //            Else
      //                mvarExcluido = "N"
      //            End If
      //            mvarRequest = mvarRequest & "<EXCLUIDO>" & mvarExcluido & "</EXCLUIDO>"
      mvarRequest = mvarRequest + "<CONDUVIN>HI</CONDUVIN>";
      mvarRequest = mvarRequest + "</CONDUCTOR>";
    }


    wvarXMLASEGURADOS = new XmlDomExtended();
    //Call wvarXMLASEGURADOS.loadXML(Request.Form("XMLASEGURADOS"))
    if( ! (wobjXMLRequest.selectSingleNode( "//ASEGURADOS-ADIC" )  == (org.w3c.dom.Node) null) )
    {

      wvarXMLASEGURADOS.loadXML( /*unsup wobjXMLRequest.selectSingleNode( "//ASEGURADOS-ADIC" ) */.toString() );

      for( int nx = 0; nx < wvarXMLASEGURADOS.selectNodes( "//ASEGURADO-ADIC" ) .getLength(); nx++ )
      {
        x = wvarXMLASEGURADOS.selectNodes( "//ASEGURADO-ADIC" ) .item( nx );
        y = y + 1;
        mvarRequest = mvarRequest + "<CONDUCTOR>";
        mvarRequest = mvarRequest + "<CONDUSEC>" + y + "</CONDUSEC>";
        mvarRequest = mvarRequest + "<CONDUTDO>1</CONDUTDO>";
        mvarRequest = mvarRequest + "<CONDUDOC>" + XmlDomExtended.getText( x.selectSingleNode( "./DOCUMASEG" )  ) + "</CONDUDOC>";
        mvarRequest = mvarRequest + "<APELLIDOHIJO><![CDATA[" + Strings.mid( XmlDomExtended.getText( x.selectSingleNode( "./NOMBREASEG" )  ), 1, (Strings.find( 1, XmlDomExtended.getText( x.selectSingleNode( "./NOMBREASEG" )  ), "," ) - 1) ) + "]]></APELLIDOHIJO>";
        mvarRequest = mvarRequest + "<SEXOHIJO></SEXOHIJO>";
        mvarRequest = mvarRequest + "<ESTADOHIJO></ESTADOHIJO>";
        mvarRequest = mvarRequest + "<NACIMHIJO></NACIMHIJO>";
        mvarRequest = mvarRequest + "<EXCLUIDO>N</EXCLUIDO>";
        mvarRequest = mvarRequest + "<CONDUVIN>AD</CONDUVIN>";
        mvarRequest = mvarRequest + "</CONDUCTOR>";
      }
    }
    wvarXMLASEGURADOS = null;
    wvarXMLHijos = null;


    mvarRequest = mvarRequest + "</CONDUCTORES>";

    //Fecha Solicitud
    mvarRequest = mvarRequest + "<FESOLANN>" + DateTime.year( DateTime.now() ) + "</FESOLANN>";
    mvarRequest = mvarRequest + "<FESOLMES>" + DateTime.month( DateTime.now() ) + "</FESOLMES>";
    mvarRequest = mvarRequest + "<FESOLDIA>" + DateTime.day( DateTime.now() ) + "</FESOLDIA>";

    mvarRequest = mvarRequest + "<FEINGANN>" + DateTime.year( DateTime.now() ) + "</FEINGANN>";
    mvarRequest = mvarRequest + "<FEINGMES>" + DateTime.month( DateTime.now() ) + "</FEINGMES>";
    mvarRequest = mvarRequest + "<FEINGDIA>" + DateTime.day( DateTime.now() ) + "</FEINGDIA>";

    //Datos productor
    //mvarRequest = mvarRequest & "<AGECLA>" & session("PERFIL_AS") & "</AGECLA>" ' corregir
    // FJO --------
    // Agregado el 06/11/2007 para que el componente tome el DATO. Anteriormente, por motivos desconocidos
    // no se estaba mandando al componente. El componente del mensaje 1540 espera
    // AGECLA, AGECOD.
    // FJO --------
    if( ! (wobjXMLRequest.selectSingleNode( "//AGECLA" )  == (org.w3c.dom.Node) null) )
    {
      //VERIFICAR CAMPO
      mvarRequest = mvarRequest + "<AGECLA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//AGECLA" )  ) + "</AGECLA>";
    }

    //<AGENTCLA>PR</AGENTCLA>
    // corregir
    mvarRequest = mvarRequest + "<AGENTCLA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//AGECLA" )  ) + "</AGENTCLA>";
    // corregir
    mvarRequest = mvarRequest + "<AGECOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//AGECOD" )  ) + "</AGECOD>";

    //harcodeo coberturas VERIFICAR CAMPOS
    //mvarRequest = mvarRequest & mvarCoberturas
    //<COBERTURAS>
    mvarRequest = mvarRequest + wobjXMLRequest.selectSingleNode( "//COBERTURAS" ) .toString();
    //harcodeo LO QUE FALTA
    mvarRequest = mvarRequest + "<AUPAISCOD>00</AUPAISCOD>";


    //Provincia del domicilio del cliente/auto (siempre)
    //mvarRequest = mvarRequest & "<AUPROVICOD>" & Request.form("PROVI_C") & "</AUPROVICOD>"
    mvarRequest = mvarRequest + "<AUPROVICOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PROVI" )  ) + "</AUPROVICOD>";
    //Codigo Postal del domicilio del cliente/auto (siempre)
    //VERIFICAR CAMPO
    mvarRequest = mvarRequest + "<AUDOMCPACODPO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LOCALIDADCOD" )  ) + "</AUDOMCPACODPO>";

    mvarRequest = mvarRequest + "<CENTRCOD>0010</CENTRCOD>";
    //VERIFICAR CAMPO (ASUMO 003)
    mvarRequest = mvarRequest + "<PLANPOLICOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PLANNCOD" )  ) + "</PLANPOLICOD>";
    //mvarRequest =  mvarRequest & "<PLANPOLICOD>003</PLANPOLICOD>"
    //mvarRequest =  mvarRequest & "<PRECIOFT>" & mvarPRECIOMENSUAL & "</PRECIOFT>"
    mvarRequest = mvarRequest + "<ASEG_ADIC>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//ASEG-ADIC" )  ) + "</ASEG_ADIC>";

    //<CLIDOMCPACODPO>1073</CLIDOMCPACODPO>
    //<CLIDOMICPRE>011</CLIDOMICPRE>
    //mvarRequest =  mvarRequest & "<CLIDOMCPACODPO>" & Request.Form("LOCALIDADCOD") & "</CLIDOMCPACODPO>"
    //mvarRequest =  mvarRequest & "<CLIDOMCPACODPO>" & Request.Form("CODPOSTAL") & "</CLIDOMCPACODPO>"
    //If Not wobjXMLRequest.selectSingleNode("//CLIDOMLOCALCOD") Is Nothing Then
    //VERIFICAR CAMPO
    mvarRequest = mvarRequest + "<CLIDOMCPACODPO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LOCALIDADCOD" )  ) + "</CLIDOMCPACODPO>";
    //End If
    // VERIFICAR NO SE UTILIZA
    mvarRequest = mvarRequest + "<CLIDOMICPRE>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//TELCODCOR" )  ) + "</CLIDOMICPRE>";
    //VERIFICAR CAMPO
    mvarRequest = mvarRequest + "<CLIDOMICTLF>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//TELCODCOR" )  ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//TELNROCOR" )  ) + "</CLIDOMICTLF>";
    //mvarRequest =  mvarRequest & "<CLIDOMICTLF>" & Request.Form("PREFTELEFNUM1") & Request.Form("TELEFNUM1") & "</CLIDOMICTLF>"
    //PRENDASINO = N VERIFICAR
    //If Request.Form("PRENDASINO") = "S" Then
    //    mvarRequest = mvarRequest & "<SWACREPR>1</SWACREPR>"
    //Else
    mvarRequest = mvarRequest + "<SWACREPR>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//ACRE-PREN" )  ) + "</SWACREPR>";
    //End If
    if( wobjXMLRequest.selectSingleNode( "//CANAL_SUCURSAL" )  == (org.w3c.dom.Node) null )
    {
      mvarRequest = mvarRequest + "<SUCURCOD>8888</SUCURCOD>";
    }
    else
    {
      mvarRequest = mvarRequest + "<SUCURCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CANAL_SUCURSAL" )  ) + "</SUCURCOD>";
    }
    //mvarRequest =  mvarRequest & "<SUCURCOD>" & Request.form("SUCURCOD") & "</SUCURCOD>"
    mvarRequest = mvarRequest + "<VIENEDMS>1</VIENEDMS>";
    mvarRequest = mvarRequest + "<PLANPAGOCOD>998</PLANPAGOCOD>";

    //DA - 20/02/2007: mando los datos del apoderado (exigidos por la UIF).
    //<CLIENIVA>3</CLIENIVA><COBROCOD>1</COBROCOD>
    //wvarDomiApodAIS = Strings.replace(wvarDomiApodAIS, "<DOMICCAL> </DOMICCAL>", "<DOMICCAL>PAR</DOMICCAL>") 'VERIFICAR CAMPO (ASUMO PAR)
    //mvarRequest = mvarRequest & "<DOMICCAL>PAR</DOMICCAL>" 'VERIFICAR CAMPO (ASUMO <DOMICCAL>PAR</DOMICCAL>)
    //mvarRequest = mvarRequest & "<APOD>" & wvarPersApodAIS & wvarDomiApodAIS & "<OCUPACOD>" & Request.Form("OCUPACODAPOD") & "</OCUPACOD></APOD>"
    //VERIFICAR CAMPO (OCUPACOD ASUMO VACIO)
    mvarRequest = mvarRequest + "<APOD>00<OCUPACOD/></APOD>";

    mvarRequest = "<Request>" + Strings.toUpperCase( mvarRequest ) + "</Request>";
    wobjClass = new lbawA_OVMQEmision.lbaw_OVAisPutSolicAUS();
    wobjClass.Execute( "<Request>" + Strings.toUpperCase( mvarRequest ) + "</Request>", wvarResponse, "" );

    wvarResponse = "<Response><Estado resultado='true' mensaje=''/><RESULTADO><SOLICITUDGRABADA>NO</SOLICITUDGRABADA><SOLICITUDOK>NO</SOLICITUDOK><MENSAJE/><RAMOPCOD>AUS1</RAMOPCOD><POLIZANN>00</POLIZANN><POLIZSEC>000001</POLIZSEC><CERTIPOL>7995</CERTIPOL><CERTIANN>0000</CERTIANN><CERTISEC>000000</CERTISEC><SUPLENUM>0000</SUPLENUM><TEXTOS><TEXTO>CONVENIO INEXISTENTE</TEXTO><TEXTO>CONVENIO INEXISTENTE</TEXTO></TEXTOS></RESULTADO></Response>";
    //wvarResponse = "<Response><Estado resultado='true' mensaje=''/><RESULTADO><SOLICITUDGRABADA>NO</SOLICITUDGRABADA><SOLICITUDOK>NO</SOLICITUDOK><MENSAJE/><RAMOPCOD>AUS1</RAMOPCOD><POLIZANN>00</POLIZANN><POLIZSEC>000001</POLIZSEC><CERTIPOL>9000</CERTIPOL><CERTIANN>0000</CERTIANN><CERTISEC>000864</CERTISEC><SUPLENUM>0000</SUPLENUM><TEXTOS><TEXTO>Existen varias personas con los datos especificado</TEXTO></TEXTOS></RESULTADO></Response>"
    // *********************************************
    if( !Strings.trim( wvarResponse ).equals( "" ) )
    {
      wvarXMLGrabacion = new XmlDomExtended();
      wvarXMLGrabacion.loadXML( wvarResponse );

      if( !Strings.toUpperCase( XmlDomExtended.getText( wvarXMLGrabacion.selectSingleNode( "//Estado/@resultado" )  ) ).equals( "FALSE" ) )
      {
        if( XmlDomExtended.getText( wvarXMLGrabacion.selectSingleNode( "//RESULTADO/SOLICITUDOK" )  ).equals( "NO" ) )
        {
          if( !XmlDomExtended.getText( wvarXMLGrabacion.selectSingleNode( "//RESULTADO/TEXTOS/TEXTO" )  ).equals( "" ) )
          {
            if( XmlDomExtended.getText( wvarXMLGrabacion.selectSingleNode( "//RESULTADO/SOLICITUDGRABADA" )  ).equals( "SI" ) )
            {
              //mvarMsgError = ""
              wvarCodErr.set( 4 );
            }
            else
            {
              wvarCodErr.set( 5 );
              //For i = 0 To wvarXMLGrabacion.selectNodes("//RESULTADO/TEXTOS/TEXTO").length - 1
              //mvarMensaje = mvarMensaje & wvarXMLGrabacion.selectNodes("//RESULTADO/TEXTOS/TEXTO").Item(i).Text & "\n"
              mvarMensaje.set( mvarMensaje + wvarXMLGrabacion.selectSingleNode( "//RESULTADO/TEXTOS" ) .toString() );
              //Next
            }

            if( Strings.len( Strings.trim( mvarMensaje.toString() ) ) != 0 )
            {
              //If Len(Trim(mvarMensaje)) > 0 Then
              //wvarMensaje = "No se pudo realizar la Emisi�n On Line por la siguiente causa:\n" & mvarMensaje
              //wvarMensaje = mvarMensaje
              //Else
              //wvarMensaje = "No se pudo realizar la Emisi�n On Line."
              wvarMensaje.set( "<TEXTOS><TEXTO>NO SE PUDO REALIZAR LA EMISION ON LINE<TEXTO><TEXTOS>" );
            }
          }
          //mvarError = "true"
          //mvarErrorCod = "1"
          //wvarCodErr = 1
          wvarMensaje.set( "<RESULTADO><SOLICITUDGRABADA>" + XmlDomExtended.getText( wvarXMLGrabacion.selectSingleNode( "//RESULTADO/SOLICITUDGRABADA" )  ) + "</SOLICITUDGRABADA><SOLICITUDOK> " + XmlDomExtended.getText( wvarXMLGrabacion.selectSingleNode( "//RESULTADO/SOLICITUDOK" )  ) + "</SOLICITUDOK>" + wvarMensaje + "</RESULTADO>" );
          fncIntegracionAIS = false;
          //wvarMensaje = "Fall� Integraci�n al AIS"
          //pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
          return fncIntegracionAIS;
        }
        else
        {
          if( XmlDomExtended.getText( wvarXMLGrabacion.selectSingleNode( "//RESULTADO/MENSAJE" )  ).equals( "PROPUESTA EMITIDA" ) )
          {
            //mvarEMISION = Strings.replace(wvarXMLGrabacion.xml, """", "'")
            //XML_AIS = mvarResponse
            //mvarPoli_CERTIPOL = wvarXMLGrabacion.selectSingleNode("//CERTIPOL").Text
            //mvarPoli_CERTIANN = wvarXMLGrabacion.selectSingleNode("//CERTIANN").Text
            //mvarPoli_CERTISEC = wvarXMLGrabacion.selectSingleNode("//CERTISEC").Text
          }
        }
      }
      else
      {
        CodigoEstado = 6;
        fncIntegracionAIS = true;
      }
    }
    else
    {
      CodigoEstado = 6;
      fncIntegracionAIS = false;
    }
    // *******************************************************
    return fncIntegracionAIS;
  }
//TODO: Fin Cambio

  private boolean fncValidaABase( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncValidaABase = false;
    Variant vbLogEventTypeError = new Variant();
    Variant mcteErrorInesperadoDescr = new Variant();
    Variant mcteDB = new Variant();
    int wvarStep = 0;
    boolean wvarError = false;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXML = null;
    XmlDomExtended wobjXSL = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    String wvarXSL = "";
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarContNode = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_NRO_OPERACION_BROKER = "";
    String mvarWDB_CODPROVCO = "";
    String mvarWDB_CODPOSTCO = "";
    String mvarWDB_COBROCOD = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_SUMAASEG = "";
    String mvarWDB_SUMALBA = "";
    String mvarWDB_COLOR = "";
    String mvarWDB_DOCUMTIP_TIT = "";
    String mvarWDB_PLANNCOD = "";
    String mvarWDB_FRANQCOD = "";
    String mvarWDB_INSPECCION = "";
    String mvarWDB_RASTREO = "";
    String mvarWDB_CODIGO_ALARMA = "";
    String mvarWDB_CODIZONA = "";
    String mvarWDB_CATEGO = "";
    String pvarReq = "";
    String mvarseltest = "";
    int wvarCount = 0;
    String CampoNOmbre = "";





    try 
    {


      wvarError = false;
      wvarStep = 10;

      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      mvarWDB_CODINST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST )  );
      mvarWDB_NRO_OPERACION_BROKER = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NRO_OPERACION_BROKER )  );
      mvarWDB_CODPROVCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODPROVCO )  );
      mvarWDB_CODPOSTCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODPOSTCO )  );
      mvarWDB_COBROCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD )  );
      mvarWDB_COBROTIP = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP )  ) );
      mvarWDB_CODIZONA = "0";
      mvarWDB_SUMAASEG = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SUMAASEG )  );
      mvarWDB_COLOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COLOR )  );
      mvarWDB_DOCUMTIP_TIT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOCUMTIP_TIT )  );
      mvarWDB_PLANNCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PLANNCOD )  );
      mvarWDB_FRANQCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_FRANQCOD )  );
      mvarWDB_INSPECCION = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_INSPECCION )  );
      mvarWDB_RASTREO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_RASTREO )  );
      mvarWDB_CODIGO_ALARMA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODIGO_ALARMA )  );
      mvarWDB_CATEGO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CATEGO )  );

      wvarStep = 20;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(mcteDB);
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 30;
      wobjDBParm = new Parameter( "@WDB_CODINST", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_CODINST ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 40;
      wobjDBParm = new Parameter( "@WDB_PRODUCTOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_NRO_OPERACION_BROKER ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 50;
      wobjDBParm = new Parameter( "@WDB_CODPROVCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_CODPROVCO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 60;
      wobjDBParm = new Parameter( "@WDB_CODPOSTCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_CODPOSTCO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 70;
      wobjDBParm = new Parameter( "@WDB_COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_COBROCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 80;
      wobjDBParm = new Parameter( "@WDB_COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarWDB_COBROTIP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 90;
      wobjDBParm = new Parameter( "@WDB_SUMAASEG", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Strings.split( mvarWDB_SUMAASEG, ".", -1 )[0] ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 15 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 100;
      wobjDBParm = new Parameter( "@WDB_COLOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_COLOR ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 110;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP_TIT", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_DOCUMTIP_TIT ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 120;
      wobjDBParm = new Parameter( "@WDB_PLANNCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_PLANNCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 130;
      wobjDBParm = new Parameter( "@WDB_FRANQCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarWDB_FRANQCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 140;
      wobjDBParm = new Parameter( "@WDB_INSPECCION", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_INSPECCION ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      //jc 09/2010 actualmente de 4
      //wobjDBParm.Precision = 2
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 150;
      wobjDBParm = new Parameter( "@WDB_RASTREO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_RASTREO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 160;
      //mvarWDB_CODIGO_ALARMA tiene longitud 4 , se saca de la validacion
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CODIGO_ALARMA", adNumeric, adParamInput, , mvarWDB_CODIGO_ALARMA)
      wobjDBParm = new Parameter( "@WDB_CODIGO_ALARMA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 170;
      wobjDBParm = new Parameter( "@WDB_CODIZONA", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( mvarWDB_CODIZONA ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 180;
      wobjDBParm = new Parameter( "@WDB_CATEGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_CATEGO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 190;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP_1", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP_2", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP_3", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP_4", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 230;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP_5", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 235;
      //jc 09/2010 el @WDB_NROCOT al cambiar el componente de cotiz. no se obtiene mas en la sp viene en 0
      wobjDBParm = new Parameter( "@WDB_NROCOT", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 238;
      wobjDBParm = new Parameter( "@WDB_SUMA_MINIMA", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 18 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 239;
      wobjDBParm = new Parameter( "@WDB_SUMALBA", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 15 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 2399;
      wobjDBParm = new Parameter( "@WDB_USUARCOD", AdoConst.adChar, AdoConst.adParamInputOutput, 10, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 240;
      mvarseltest = mcteStoreProc + " ";
      for( wvarCount = 0; wvarCount <= (wobjDBCmd.getParameters().getCount() - 1); wvarCount++ )
      {
        if( (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adChar) || (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adVarChar) )
        {
          mvarseltest = mvarseltest + "'" + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + "',";
        }
        else
        {
          mvarseltest = mvarseltest + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + ",";
        }
      }

      wvarStep = 250;
      if( ! (wvarError) )
      {

        wrstRes = new Recordset();
        wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
        wrstRes.setActiveConnection( (Connection) null );

        wobjXML = new XmlDomExtended();
        wobjXML.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteArchivoAUSSOL_XML));


        //SI NO HUBO ERROR AGREGO LOS DATOS AL XML DE ENTRADA PARA LA RESPUESTA
        wvarStep = 260;
        if( ! (wrstRes.isEOF()) )
        {
          if( Strings.toUpperCase( Strings.trim( wrstRes.getFields().getField(0).getValue().toString() ) ).equals( "OK" ) )
          {
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CODZONA", "" ) */ );
            XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//CODZONA" ) , wobjDBCmd.getParameters().getParameter("@WDB_CODIZONA").getValue().toString() );

            //Agrega el NroCot y el nodo de CERTISEC en blanco
            //jc 09/2010 el @WDB_NROCOT al cambiar el componente no se obtiene mas de la sp viene en 0
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "WDB_NROCOT", "" ) */ );
            XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//WDB_NROCOT" ) , wobjDBCmd.getParameters().getParameter("@WDB_NROCOT").getValue().toString() );
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "SUMA_MINIMA", "" ) */ );
            XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//SUMA_MINIMA" ) , wobjDBCmd.getParameters().getParameter("@WDB_SUMA_MINIMA").getValue().multiply( new Variant( 100 ) ).toString() );
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CERTISEC", "" ) */ );
            XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//CERTISEC" ) , "0" );
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "SUMALBA", "" ) */ );
            XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//SUMALBA" ) , wobjDBCmd.getParameters().getParameter("@WDB_SUMALBA").getValue().toString() );
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "USUARCOD", "" ) */ );
            XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//USUARCOD" ) , wobjDBCmd.getParameters().getParameter("@WDB_USUARCOD").getValue().toString() );


            for( wvarContNode = 0; wvarContNode <= (wrstRes.getFields().getCount() - 1); wvarContNode++ )
            {
              if( wrstRes.getFields().getField(wvarContNode).getName().equals( "" ) )
              {
                CampoNOmbre = "RESULTADO" + String.valueOf( wvarContNode + 1 );
              }
              else
              {
                CampoNOmbre = wrstRes.getFields().getField(wvarContNode).getName();
              }
              /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), Strings.toUpperCase( CampoNOmbre ), "" ) */ );
              if( (wrstRes.getFields().getField(wvarContNode).getType() == AdoConst.adNumeric) && (wrstRes.getFields().getField(wvarContNode).getPrecision() == 18) )
              {
                XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//" + Strings.toUpperCase( CampoNOmbre ) ) , wrstRes.getFields().getField(wvarContNode).getValue().multiply( new Variant( 100 ) ).toString() );
              }
              else
              {
                XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//" + Strings.toUpperCase( CampoNOmbre ) ) , Strings.trim( wrstRes.getFields().getField(wvarContNode).getValue().toString() ) );
              }
            }
            pvarRes.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
            if( ! (invoke( "fncComparaConCotizacion", new Variant[] { new Variant(wobjXMLRequest.getDocument().getDocumentElement().toString()), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
            {
              wvarError = true;
            }
            else
            {
              wvarError = false;
            }

          }
          else
          {
            //error
            wvarError = true;
            if( wobjXML.selectNodes( ("//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']") ) .getLength() > 0 )
            {
              wvarMensaje.set( XmlDomExtended.getText( wobjXML.selectSingleNode( "//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/TEXTOERROR" )  ) );
              wvarCodErr.set( XmlDomExtended.getText( wobjXML.selectSingleNode( "//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/VALORRESPUESTA" )  ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg = \"" + wvarMensaje + "\"></LBA_WS>" );
            }
            else
            {
              wvarCodErr.set( wrstRes.getFields().getField(0).getValue() );
              wvarMensaje.set( "No existe descripcion para este error" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg = \"" + wvarMensaje + "\"></LBA_WS>" );
            }
          }
        }
        else
        {
          //error
          wvarError = true;
          wvarCodErr.set( -16 );
          wvarMensaje.set( "No se retornaron datos de la base" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        }
      }
      //
      pvarReq = XmlDomExtended.marshal(wobjXMLRequest.getDocument().getDocumentElement());
      //
      wobjXML = null;
      if( wvarError )
      {
        fncValidaABase = false;
      }
      else
      {
        fncValidaABase = true;
      }


      wrstRes.close();
      wobjDBCnn.close();
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = null;
      return fncValidaABase;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + mcteErrorInesperadoDescr + "\" /></Response>" );
        wvarCodErr.set( -1000 );
        wvarMensaje.set( mcteErrorInesperadoDescr );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncValidaABase = false;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncValidaABase;
  }

  private boolean fncComparaConCotizacion( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncComparaConCotizacion = false;
    Variant vbLogEventTypeError = new Variant();
    Variant mcteErrorInesperadoDescr = new Variant();
    Variant mcteDB = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLRequestCotis = null;
    XmlDomExtended wobjXMLCotizacion = null;
    XmlDomExtended wobjXMLValid = null;
    org.w3c.dom.NodeList wobjXMLNodeList = null;
    org.w3c.dom.NodeList wobjXMLListCotiz = null;
    org.w3c.dom.Node wobjXMLNode = null;
    org.w3c.dom.Node wobjXMLNodeCotiz = null;
    Object wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    boolean wvarChequeonuevas = false;
    boolean wvarChequeoLocalidad = false;
    boolean wvarChequeoTarjeta = false;
    String wvarTieneHijos = "";
    String wvarNombrePlan = "";
    boolean wvarChequeoHijos = false;
    boolean wvarChequeoAccesorios = false;
    int wvarcounter = 0;
    String wvarResponse = "";
    int wvarStep = 0;
    String wvarNuevoHijos = "";
    String mvarWDB_CODINST = "";
    String mvarWDB_NRO_OPERACION_BROKER = "";
  //TODO: Inicio Cambio
    String mvarRAMOPCOD = "";
    String mvarPOLIZANN = "";
    String mvarPOLIZSEC = "";
    String mvarCERTIPOL = "";
    String mvarCERTIANN = "";
    String mvarCertiSec = "";
  //TODO: Fin Cambio
    String mvarDatosPlan = "";
    String mvarResponse = "";
    Variant mvarResponse1 = new Variant();
    Variant mvarResponse2 = new Variant();
    Variant mvarResponse3 = new Variant();
    Variant mvarResponse4 = new Variant();
    Variant mvarResponse5 = new Variant();
    Variant mvarResponse6 = new Variant();
    Variant mvarResponse7 = new Variant();
    Variant mvarResponse8 = new Variant();
    Variant mvarResponse9 = new Variant();
    Variant mvarResponse10 = new Variant();



    //jc 09/2010 nvas. coberturas
    wvarStep = 0;


    //Agrego estructura de la p�liza
    //
    try 
    {

      wvarStep = 10;

      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 20;
      mvarWDB_CODINST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST )  );
      mvarWDB_NRO_OPERACION_BROKER = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NRO_OPERACION_BROKER )  );

      wvarStep = 30;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(mcteDB);
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProcSelect );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      //Comparo con el XML guardado de Cotizacion
      //Busco el registro guardado de la cotizacion
      wvarStep = 40;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 50;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NRO_OPERACION_BROKER.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_NRO_OPERACION_BROKER)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 60;
      wobjDBParm = new Parameter( "@WDB_TIPO_OPERACION ", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "C" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 70;
      wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "OK" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wrstRes = new Recordset();
      wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
      wrstRes.setActiveConnection( (Connection) null );
      //
      //Cargo el campo de XML de cotizacion
      wvarStep = 80;
      wobjXMLCotizacion = new XmlDomExtended();
      wobjXMLCotizacion.loadXML( wrstRes.getFields().getField(14).getValue().toString() );
      //
    //TODO: Inicio Cambio
      //Cargo los campos de estructura de p�liza
      mvarRAMOPCOD = wrstRes.getFields().getField(7).getValue().toString();
      mvarPOLIZANN = wrstRes.getFields().getField(8).getValue().toString();
      mvarPOLIZSEC = wrstRes.getFields().getField(9).getValue().toString();
      mvarCERTIPOL = wrstRes.getFields().getField(10).getValue().toString();
      mvarCERTIANN = wrstRes.getFields().getField(11).getValue().toString();
      mvarCertiSec = wrstRes.getFields().getField(12).getValue().toString();
    //TODO: Fin Cambio
      //
      //Si es PROVI = 1 no compara el Codigo Postal
      if( Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) )  ) ) != 1 )
      {
        if( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_LocalidadCod) )  ) ) )
        {
          wvarChequeoLocalidad = true;
        }
        else
        {
          wvarChequeoLocalidad = false;
        }
      }
      else
      {
        wvarChequeoLocalidad = true;
      }

      //Si es COBROCOD = 4 no compara el Tipo de Tarjeta
      wvarStep = 90;
      if( Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) )  ) ) != 4 )
      {
        if( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_COBROTIP) )  ) ) )
        {
          wvarChequeoTarjeta = true;
        }
        else
        {
          wvarChequeoTarjeta = false;
        }
      }
      else
      {
        wvarChequeoTarjeta = true;
      }

      //Chequeo los nodos de hijos
      wvarStep = 100;
      wobjXMLNodeList = wobjXMLRequest.selectNodes( mcteNodos_Hijos ) ;
      wobjXMLListCotiz = wobjXMLCotizacion.selectNodes( mcteNodos_Hijos ) ;
      wvarChequeoHijos = true;
      if( ! ((wobjXMLNodeList == (org.w3c.dom.NodeList) null)) && ! ((wobjXMLListCotiz == (org.w3c.dom.NodeList) null)) )
      {
        for( wvarcounter = 0; wvarcounter <= (wobjXMLNodeList.getLength() - 1); wvarcounter++ )
        {
          wobjXMLNode = wobjXMLNodeList.item( wvarcounter );
          wobjXMLNodeCotiz = wobjXMLCotizacion.selectNodes( mcteNodos_Hijos ) .item( wvarcounter );
          if( wobjXMLNodeList.getLength() != wobjXMLListCotiz.getLength() )
          {
            wvarCodErr.set( -165 );
            wvarMensaje.set( "Datos de Hijos: existe una diferencia entre la Cotizacion y la Solicitud" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            fncComparaConCotizacion = false;
            return fncComparaConCotizacion;
          }

          if( ! ((wobjXMLNode.selectSingleNode( "NACIMHIJO" )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLNodeCotiz.selectSingleNode( "NACIMHIJO" )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLNode.selectSingleNode( "SEXOHIJO" )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLNodeCotiz.selectSingleNode( "SEXOHIJO" )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLNode.selectSingleNode( "ESTADOHIJO" )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLNodeCotiz.selectSingleNode( "ESTADOHIJO" )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLNode.selectSingleNode( "EDADHIJO" )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLNodeCotiz.selectSingleNode( "EDADHIJO" )  == (org.w3c.dom.Node) null)) )
          {
            if( ! ((XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "NACIMHIJO" )  ).equals( XmlDomExtended.getText( wobjXMLNodeCotiz.selectSingleNode( "NACIMHIJO" )  ) )) && (XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "SEXOHIJO" )  ).equals( XmlDomExtended.getText( wobjXMLNodeCotiz.selectSingleNode( "SEXOHIJO" )  ) )) && (XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "ESTADOHIJO" )  ).equals( XmlDomExtended.getText( wobjXMLNodeCotiz.selectSingleNode( "ESTADOHIJO" )  ) )) && (XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "EDADHIJO" )  ).equals( XmlDomExtended.getText( wobjXMLNodeCotiz.selectSingleNode( "EDADHIJO" )  ) ))) )
            {
              wvarChequeoHijos = false;
            }
          }
          else
          {
            wvarChequeoHijos = false;
          }
          wobjXMLNode = (org.w3c.dom.Node) null;
          wobjXMLNodeCotiz = (org.w3c.dom.Node) null;
        }
        wobjXMLNodeList = (org.w3c.dom.NodeList) null;
        wobjXMLListCotiz = (org.w3c.dom.NodeList) null;
      }


      //Chequeo si viene al menos un accesorio
      //  wvarStep = 111
      //  Set wobjXMLNodeList = wobjXMLRequest.selectNodes(mcteNodos_Accesorios)
      //  wvarStep = 112
      //  Set wobjXMLListCotiz = wobjXMLCotizacion.selectNodes(mcteNodos_Accesorios)
      //  wvarStep = 113
      wvarChequeoAccesorios = true;
      //  If Not wobjXMLNodeList Is Nothing And Not wobjXMLListCotiz Is Nothing Then
      //    wvarStep = 114
      //    For wvarcounter = 0 To (wobjXMLNodeList.length - 1)
      //      wvarStep = 115
      //      Set wobjXMLNode = wobjXMLNodeList(wvarcounter)
      //      wvarStep = 116
      //      Set wobjXMLNodeCotiz = wobjXMLCotizacion.selectNodes(mcteNodos_Accesorios).Item(wvarcounter)
      //
      //         wvarStep = 117
      //         If Not (wobjXMLNode.selectSingleNode("CODIGOACC") Is Nothing) And Not (wobjXMLNodeCotiz.selectSingleNode("CODIGOACC") Is Nothing) And _
      // '           Not (wobjXMLNode.selectSingleNode("DESCRIPCIONACC") Is Nothing) And Not (wobjXMLNodeCotiz.selectSingleNode("DESCRIPCIONACC") Is Nothing) And _
      //           Not (wobjXMLNode.selectSingleNode("PRECIOACC") Is Nothing) And Not (wobjXMLNodeCotiz.selectSingleNode("PRECIOACC") Is Nothing) _
      // '         Then
      //           wvarStep = 118
      //           If Not wobjXMLNode.selectSingleNode("PRECIOACC").Text = "0" And Not wobjXMLNode.selectSingleNode("PRECIOACC").Text = "" Then
      //            wvarStep = 119
      //            If Not (wobjXMLNode.selectSingleNode("CODIGOACC").Text = wobjXMLNodeCotiz.selectSingleNode("CODIGOACC").Text And _
      // '                  wobjXMLNode.selectSingleNode("DESCRIPCIONACC").Text = wobjXMLNodeCotiz.selectSingleNode("DESCRIPCIONACC").Text And _
      //                  wobjXMLNode.selectSingleNode("PRECIOACC").Text = wobjXMLNodeCotiz.selectSingleNode("PRECIOACC").Text) Then
      //                  wvarChequeoAccesorios = False
      //             End If
      //            End If
      //           Set wobjXMLNode = Nothing
      //           Set wobjXMLNodeCotiz = Nothing
      //         End If
      //    Next
      //  End If
      //Controla que la ultima grabacion tenga todos los nodos para comparar
      //jc 09/2010 se agregan coberturas nuevas y se cambian nombres de los tag productor e iva
      wvarStep = 120;
      if( (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_NacimAnn) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_NacimMes) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_NacimDia) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Sexo) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Estado) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_PRODUCTOR) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CampaCod) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_ModeAutCod) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_SUMAASEG) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_KMsrngCod) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_EfectAnn) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_SiGarage) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Siniestros) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Gas) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_ClubLBA) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Provi) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_COBROCOD) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CLIENIVA) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_VEHDES) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_DESTRUCCION_80) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Luneta) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_ClubEco) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Robocont) )  == (org.w3c.dom.Node) null) || (wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Granizo) )  == (org.w3c.dom.Node) null) )
      {
        wvarCodErr.set( -96 );
        wvarMensaje.set( "La ultima cotizacion se guardo con error debido a que falta un campo" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        fncComparaConCotizacion = false;
        return fncComparaConCotizacion;
      }
      //jc 09/2010 valida nuevas coberturas
      //jc como hay planes que no soportan las coberturas se debe validar el plan contra la marca, esta fijo como en OV
      //jc ojo que esta al limite de los or encadenados
      wvarStep = 125;
      //jc 08/2011 ANEXO I, x nvo. comp+ no se puede validar, se asume solo validacion x precio
      wvarChequeonuevas = true;
      // RC y RC+Robo no pueden luneta-granizo-robocont-dt80
      //        If wobjXMLRequest.selectSingleNode("//" & mcteParam_PLANNCOD).Text = "001" Or wobjXMLRequest.selectSingleNode("//" & mcteParam_PLANNCOD).Text = "005" Then
      //           If UCase(wobjXMLRequest.selectSingleNode("//" & mcteParam_DESTRUCCION_80).Text) = "S" Or _
      //  '              UCase(wobjXMLRequest.selectSingleNode("//" & mcteParam_Luneta).Text) = "S" Or _
      //              UCase(wobjXMLRequest.selectSingleNode("//" & mcteParam_Robocont).Text) = "S" Or _
      //  '              UCase(wobjXMLRequest.selectSingleNode("//" & mcteParam_Granizo).Text) = "S" _
      //           Then
      //              wvarChequeonuevas = False
      //           Else
      //              wvarChequeonuevas = True
      //           End If
      //        Else
      // TRCG y TT no puede dt80
      //           If wobjXMLRequest.selectSingleNode("//" & mcteParam_PLANNCOD).Text = "009" Or wobjXMLRequest.selectSingleNode("//" & mcteParam_PLANNCOD).Text = "003" Then
      //              If UCase(wobjXMLRequest.selectSingleNode("//" & mcteParam_DESTRUCCION_80).Text) = "S" Then
      //                   wvarChequeonuevas = False
      //              Else
      //                   wvarChequeonuevas = True
      //              End If
      //           Else
      //el resto puede todas las opcionales
      //              wvarChequeonuevas = True
      //           End If
      //        End If
      //fin jc se agregan en el paso 130
      wvarStep = 130;
      wvarCodErr.set( 0 );
      //A pedido de Inworx, si existe una diferencia entre la cotizacion y la
      //solicitud, debo especificar el campo de la diferencia
      if( ! ((XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimAnn) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_NacimAnn) )  ) )) && (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimMes) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_NacimMes) )  ) )) && (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimDia) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_NacimDia) )  ) ))) )
      {
        wvarCodErr.set( -150 );
        wvarMensaje.set( "Fecha de Nacimiento: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 132;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Sexo) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Sexo) )  ) )) )
      {
        wvarCodErr.set( -151 );
        wvarMensaje.set( "Sexo: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 133;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Estado) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Estado) )  ) )) )
      {
        wvarCodErr.set( -152 );
        wvarMensaje.set( "Estado Civil: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 134;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PRODUCTOR) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_PRODUCTOR) )  ) )) )
      {
        wvarCodErr.set( -153 );
        wvarMensaje.set( "Cod. de Productor: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 135;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CampaCod) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CampaCod) )  ) )) )
      {
        wvarCodErr.set( -154 );
        wvarMensaje.set( "Cod. de Campania: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 136;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ModeAutCod) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_ModeAutCod) )  ) )) )
      {
        wvarCodErr.set( -155 );
        wvarMensaje.set( "Modelo de Vehiculo: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 137;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SUMAASEG) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_SUMAASEG) )  ) )) )
      {
        wvarCodErr.set( -156 );
        wvarMensaje.set( "Suma Asegurada: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 138;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_KMsrngCod) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_KMsrngCod) )  ) )) )
      {
        wvarCodErr.set( -157 );
        wvarMensaje.set( "Rango de Kms.: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 139;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EfectAnn) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_EfectAnn) )  ) )) )
      {
        wvarCodErr.set( -158 );
        wvarMensaje.set( "Anio del Vehiculo: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 140;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SiGarage) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_SiGarage) )  ) )) )
      {
        wvarCodErr.set( -159 );
        wvarMensaje.set( "Guarda en Garage: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 141;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Siniestros) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Siniestros) )  ) )) )
      {
        wvarCodErr.set( -160 );
        wvarMensaje.set( "Cantidad de Siniestros: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 142;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Gas) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Gas) )  ) )) )
      {
        wvarCodErr.set( -161 );
        wvarMensaje.set( "Usa GNC: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 143;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClubLBA) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_ClubLBA) )  ) )) )
      {
        wvarCodErr.set( -162 );
        wvarMensaje.set( "Club LBA: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 144;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Provi) )  ) )) )
      {
        wvarCodErr.set( -163 );
        wvarMensaje.set( "Cod. de Provincia: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 145;
      if( ! (wvarChequeoLocalidad) )
      {
        wvarCodErr.set( -164 );
        wvarMensaje.set( "Cod. de Localidad: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 146;
      if( ! (wvarChequeoHijos) )
      {
        wvarCodErr.set( -165 );
        wvarMensaje.set( "Datos de Hijos: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 147;
      if( ! (wvarChequeoAccesorios) )
      {
        wvarCodErr.set( -166 );
        wvarMensaje.set( "Datos de Accesorios: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 148;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_COBROCOD) )  ) )) )
      {
        wvarCodErr.set( -167 );
        wvarMensaje.set( "Cod. de Cobro: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 149;
      if( ! (wvarChequeoTarjeta) )
      {
        wvarCodErr.set( -168 );
        wvarMensaje.set( "Datos de Tarjeta: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 150;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CLIENIVA) )  ) )) )
      {
        wvarCodErr.set( -169 );
        wvarMensaje.set( "Responsabilidad frente al IVA: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 151;
      if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VEHDES) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_VEHDES) )  ) )) )
      {
        wvarCodErr.set( -170 );
        wvarMensaje.set( "Descripcion del Vehiculo: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 152;
      //jc 09/2010 se agregan nuevas cob solo puede caer aqui si no correspondio al plan
      if( ! (wvarChequeonuevas) )
      {
        if( Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DESTRUCCION_80) )  ) ).equals( "S" ) )
        {
          wvarCodErr.set( -171 );
          wvarMensaje.set( "Destruc.80: El plan seleccionado no permite esta cobertura opcional" );
        }
        else
        {
          wvarStep = 153;
          if( Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Luneta) )  ) ).equals( "S" ) )
          {
            wvarCodErr.set( -172 );
            wvarMensaje.set( "Luneta: El plan seleccionado no permite esta cobertura opcional" );
          }
          else
          {
            wvarStep = 154;
            if( Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Robocont) )  ) ).equals( "S" ) )
            {
              wvarCodErr.set( -174 );
              wvarMensaje.set( "Robo Cont.: El plan seleccionado no permite esta cobertura opcional" );
            }
            else
            {
              wvarStep = 155;
              if( Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Granizo) )  ) ).equals( "S" ) )
              {
                wvarCodErr.set( -175 );
                wvarMensaje.set( "Granizo: El plan seleccionado no permite esta cobertura opcional" );
              }
            }
          }
        }
      }
      if( wvarCodErr.toInt() < 0 )
      {
        wvarStep = 156;
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        fncComparaConCotizacion = false;
        return fncComparaConCotizacion;
      }
      //fin jc
      wvarStep = 157;
      //11/2010 se agregan validaciones x distinto iva, nodos no obligatorios en la cotiz.
      if( ! ((wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENTIP) )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CLIENTIP) )  == (org.w3c.dom.Node) null)) )
      {
        wvarStep = 158;
        if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENTIP) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CLIENTIP) )  ) )) )
        {
          wvarCodErr.set( -181 );
          wvarMensaje.set( "Tipo de Persona: existe una diferencia entre la Cotizacion y la Solicitud" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          fncComparaConCotizacion = false;
          return fncComparaConCotizacion;
        }
      }
      wvarStep = 159;
      if( ! ((wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_IBB) )  == (org.w3c.dom.Node) null)) && ! ((wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_IBB) )  == (org.w3c.dom.Node) null)) )
      {
        if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_IBB) )  ).equals( XmlDomExtended.getText( wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_IBB) )  ) )) )
        {
          wvarCodErr.set( -181 );
          wvarMensaje.set( "Tipo de IBB: existe una diferencia entre la Cotizacion y la Solicitud" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          fncComparaConCotizacion = false;
          return fncComparaConCotizacion;
        }
      }
      //fin distintos iva
      pvarRes.set( pvarRequest );
      //
      wrstRes.close();
      wobjDBCnn.close();

      //Llama al cotizador
      //Cotiza nuevamente
      //jc 09/2010 se llama al componente actual y ademas hay que agregar nodo
      wvarStep = 160;
      mvarDatosPlan = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PLANNCOD )  ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_FRANQCOD) )  );
      pvarRequest = Strings.replace( pvarRequest, "</Request>", "<DATOSPLAN>" + mvarDatosPlan + "</DATOSPLAN></Request>" );
      //Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVLBAMQ.lbaws_GetCotisAUS")
      wobjClass = new lbawA_OVMQCotizar.lbaw_OVGetCotiAUS();
      wobjClass.Execute( pvarRequest, wvarResponse, "" );
      wobjClass = null;

      wobjXMLRequestCotis = new XmlDomExtended();
      wobjXMLRequestCotis.loadXML( wvarResponse );



      mvarResponse = "<Response>";
      mvarResponse = mvarResponse + "<Estado resultado='true' mensaje=''/>";
      mvarResponse = mvarResponse + "<COT_NRO>2049428</COT_NRO>";
      mvarResponse = mvarResponse + "<CAMPA_COD>0888</CAMPA_COD>";
      mvarResponse = mvarResponse + "<AGE_COD>4850</AGE_COD>";
      mvarResponse = mvarResponse + "<AGE_CLA>PR</AGE_CLA>";
      mvarResponse = mvarResponse + "<CAMPA_TEL/>";
      mvarResponse = mvarResponse + "<CAMPA_TEL_FORM/>";
      mvarResponse = mvarResponse + "<SUMASEG>00009000000</SUMASEG>";
      mvarResponse = mvarResponse + "<SUMALBA>00009000000</SUMALBA>";
      mvarResponse = mvarResponse + "<SILUNETA>S</SILUNETA>";
      mvarResponse = mvarResponse + "<TIENEHIJOS>S</TIENEHIJOS>";
      mvarResponse = mvarResponse + "<PLANES>";
      mvarResponse = mvarResponse + "<PLAN>";
      mvarResponse = mvarResponse + "<PLANNCOD>01199</PLANNCOD>";
      mvarResponse = mvarResponse + "<PLANNDES><![CDATA[RESPONSABILIDAD CIVIL/FRANQUICIA NO APLICABLE]]></PLANNDES>";
      mvarResponse = mvarResponse + "<ORDEN>0006</ORDEN>";
      mvarResponse = mvarResponse + "<LUNPAR>N</LUNPAR>";
      mvarResponse = mvarResponse + "<GRANIZO>N</GRANIZO>";
      mvarResponse = mvarResponse + "<ROBOCON>N</ROBOCON>";
      mvarResponse = mvarResponse + "<ESRC>S</ESRC>";
      mvarResponse = mvarResponse + "<CON_HIJOS>";
      mvarResponse = mvarResponse + "<PRIMA>784,21</PRIMA>";
      mvarResponse = mvarResponse + "<RECARGOS>0,00</RECARGOS>";
      mvarResponse = mvarResponse + "<IVAIMPOR>164,68</IVAIMPOR>";
      mvarResponse = mvarResponse + "<IVAIMPOA>0,00</IVAIMPOA>";
      mvarResponse = mvarResponse + "<IVARETEN>0,00</IVARETEN>";
      mvarResponse = mvarResponse + "<DEREMI>0,00</DEREMI>";
      mvarResponse = mvarResponse + "<SELLADO>11,73</SELLADO>";
      mvarResponse = mvarResponse + "<INGBRU>0,00</INGBRU>";
      mvarResponse = mvarResponse + "<IMPUES>17,25</IMPUES>";
      mvarResponse = mvarResponse + "<PRECIO>977,87</PRECIO>";
      mvarResponse = mvarResponse + "</CON_HIJOS>";
      mvarResponse = mvarResponse + "<SIN_HIJOS>";
      mvarResponse = mvarResponse + "<PRIMA>667,40</PRIMA>";
      mvarResponse = mvarResponse + "<RECARGOS>0,00</RECARGOS>";
      mvarResponse = mvarResponse + "<IVAIMPOR>140,15</IVAIMPOR>";
      mvarResponse = mvarResponse + "<IVAIMPOA>0,00</IVAIMPOA>";
      mvarResponse = mvarResponse + "<IVARETEN>0,00</IVARETEN>";
      mvarResponse = mvarResponse + "<DEREMI>0,00</DEREMI>";
      mvarResponse = mvarResponse + "<SELLADO>9,99</SELLADO>";
      mvarResponse = mvarResponse + "<INGBRU>0,00</INGBRU>";
      mvarResponse = mvarResponse + "<IMPUES>14,68</IMPUES>";
      mvarResponse = mvarResponse + "<PRECIO>832,22</PRECIO>";
      mvarResponse = mvarResponse + "</SIN_HIJOS>";
      mvarResponse = mvarResponse + "</PLAN>";
      mvarResponse = mvarResponse + "<PLAN>";
      mvarResponse = mvarResponse + "<PLANNCOD>01499</PLANNCOD>";
      mvarResponse = mvarResponse + "<PLANNDES><![CDATA[TERCEROS COMPLETOS (SIN DA�OS PARCIALES)/FRANQUICIA NO APLICABLE]]></PLANNDES>";
      mvarResponse = mvarResponse + "<ORDEN>0001</ORDEN>";
      mvarResponse = mvarResponse + "<LUNPAR>S</LUNPAR>";
      mvarResponse = mvarResponse + "<GRANIZO>S</GRANIZO>";
      mvarResponse = mvarResponse + "<ROBOCON>N</ROBOCON>";
      mvarResponse = mvarResponse + "<ESRC>N</ESRC>";
      mvarResponse = mvarResponse + "<CON_HIJOS>";
      mvarResponse = mvarResponse + "<PRIMA>0,00</PRIMA>";
      mvarResponse = mvarResponse + "<RECARGOS>0,00</RECARGOS>";
      mvarResponse = mvarResponse + "<IVAIMPOR>0,00</IVAIMPOR>";
      mvarResponse = mvarResponse + "<IVAIMPOA>0,00</IVAIMPOA>";
      mvarResponse = mvarResponse + "<IVARETEN>0,00</IVARETEN>";
      mvarResponse = mvarResponse + "<DEREMI>0,00</DEREMI>";
      mvarResponse = mvarResponse + "<SELLADO>0,00</SELLADO>";
      mvarResponse = mvarResponse + "<INGBRU>0,00</INGBRU>";
      mvarResponse = mvarResponse + "<IMPUES>0,00</IMPUES>";
      mvarResponse = mvarResponse + "<PRECIO>0,00</PRECIO>";
      mvarResponse = mvarResponse + "</CON_HIJOS>";
      mvarResponse = mvarResponse + "<SIN_HIJOS>";
      mvarResponse = mvarResponse + "<PRIMA>0,00</PRIMA>";
      mvarResponse = mvarResponse + "<RECARGOS>0,00</RECARGOS>";
      mvarResponse = mvarResponse + "<IVAIMPOR>0,00</IVAIMPOR>";
      mvarResponse = mvarResponse + "<IVAIMPOA>0,00</IVAIMPOA>";
      mvarResponse = mvarResponse + "<IVARETEN>0,00</IVARETEN>";
      mvarResponse = mvarResponse + "<DEREMI>0,00</DEREMI>";
      mvarResponse = mvarResponse + "<SELLADO>0,00</SELLADO>";
      mvarResponse = mvarResponse + "<INGBRU>0,00</INGBRU>";
      mvarResponse = mvarResponse + "<IMPUES>0,00</IMPUES>";
      mvarResponse = mvarResponse + "<PRECIO>0,00</PRECIO>";
      mvarResponse = mvarResponse + "</SIN_HIJOS>";
      mvarResponse = mvarResponse + "</PLAN>";
      mvarResponse = mvarResponse + "<PLAN>";
      mvarResponse = mvarResponse + "<PLANNCOD>01604</PLANNCOD>";
      mvarResponse = mvarResponse + "<PLANNDES><![CDATA[TODO RIESGO REPARACION GARANTIZADA/NAC. $ 1.600.- / IMP. $ 3.800.]]></PLANNDES>";
      mvarResponse = mvarResponse + "<ORDEN>0005</ORDEN>";
      mvarResponse = mvarResponse + "<LUNPAR>S</LUNPAR>";
      mvarResponse = mvarResponse + "<GRANIZO>S</GRANIZO>";
      mvarResponse = mvarResponse + "<ROBOCON>N</ROBOCON>";
      mvarResponse = mvarResponse + "<ESRC>N</ESRC>";
      mvarResponse = mvarResponse + "<CON_HIJOS>";
      mvarResponse = mvarResponse + "<PRIMA>0,00</PRIMA>";
      mvarResponse = mvarResponse + "<RECARGOS>0,00</RECARGOS>";
      mvarResponse = mvarResponse + "<IVAIMPOR>0,00</IVAIMPOR>";
      mvarResponse = mvarResponse + "<IVAIMPOA>0,00</IVAIMPOA>";
      mvarResponse = mvarResponse + "<IVARETEN>0,00</IVARETEN>";
      mvarResponse = mvarResponse + "<DEREMI>0,00</DEREMI>";
      mvarResponse = mvarResponse + "<SELLADO>0,00</SELLADO>";
      mvarResponse = mvarResponse + "<INGBRU>0,00</INGBRU>";
      mvarResponse = mvarResponse + "<IMPUES>0,00</IMPUES>";
      mvarResponse = mvarResponse + "<PRECIO>0,00</PRECIO>";
      mvarResponse = mvarResponse + "</CON_HIJOS>";
      mvarResponse = mvarResponse + "<SIN_HIJOS>";
      mvarResponse = mvarResponse + "<PRIMA>0,00</PRIMA>";
      mvarResponse = mvarResponse + "<RECARGOS>0,00</RECARGOS>";
      mvarResponse = mvarResponse + "<IVAIMPOR>0,00</IVAIMPOR>";
      mvarResponse = mvarResponse + "<IVAIMPOA>0,00</IVAIMPOA>";
      mvarResponse = mvarResponse + "<IVARETEN>0,00</IVARETEN>";
      mvarResponse = mvarResponse + "<DEREMI>0,00</DEREMI>";
      mvarResponse = mvarResponse + "<SELLADO>0,00</SELLADO>";
      mvarResponse = mvarResponse + "<INGBRU>0,00</INGBRU>";
      mvarResponse = mvarResponse + "<IMPUES>0,00</IMPUES>";
      mvarResponse = mvarResponse + "<PRECIO>0,00</PRECIO>";
      mvarResponse = mvarResponse + "</SIN_HIJOS>";
      mvarResponse = mvarResponse + "</PLAN>";
      mvarResponse = mvarResponse + "<PLAN>";
      mvarResponse = mvarResponse + "<PLANNCOD>01501</PLANNCOD>";
      mvarResponse = mvarResponse + "<PLANNDES><![CDATA[TODO RIESGO CON FRANQUICIA/NAC. $ 1.300.- / IMP. $ 3.500.]]></PLANNDES>";
      mvarResponse = mvarResponse + "<ORDEN>0002</ORDEN>";
      mvarResponse = mvarResponse + "<LUNPAR>S</LUNPAR>";
      mvarResponse = mvarResponse + "<GRANIZO>S</GRANIZO>";
      mvarResponse = mvarResponse + "<ROBOCON>N</ROBOCON>";
      mvarResponse = mvarResponse + "<ESRC>N</ESRC>";
      mvarResponse = mvarResponse + "<CON_HIJOS>";
      mvarResponse = mvarResponse + "<PRIMA>0,00</PRIMA>";
      mvarResponse = mvarResponse + "<RECARGOS>0,00</RECARGOS>";
      mvarResponse = mvarResponse + "<IVAIMPOR>0,00</IVAIMPOR>";
      mvarResponse = mvarResponse + "<IVAIMPOA>0,00</IVAIMPOA>";
      mvarResponse = mvarResponse + "<IVARETEN>0,00</IVARETEN>";
      mvarResponse = mvarResponse + "<DEREMI>0,00</DEREMI>";
      mvarResponse = mvarResponse + "<SELLADO>0,00</SELLADO>";
      mvarResponse = mvarResponse + "<INGBRU>0,00</INGBRU>";
      mvarResponse = mvarResponse + "<IMPUES>0,00</IMPUES>";
      mvarResponse = mvarResponse + "<PRECIO>0,00</PRECIO>";
      mvarResponse = mvarResponse + "</CON_HIJOS>";
      mvarResponse = mvarResponse + "<SIN_HIJOS>";
      mvarResponse = mvarResponse + "<PRIMA>0,00</PRIMA>";
      mvarResponse = mvarResponse + "<RECARGOS>0,00</RECARGOS>";
      mvarResponse = mvarResponse + "<IVAIMPOR>0,00</IVAIMPOR>";
      mvarResponse = mvarResponse + "<IVAIMPOA>0,00</IVAIMPOA>";
      mvarResponse = mvarResponse + "<IVARETEN>0,00</IVARETEN>";
      mvarResponse = mvarResponse + "<DEREMI>0,00</DEREMI>";
      mvarResponse = mvarResponse + "<SELLADO>0,00</SELLADO>";
      mvarResponse = mvarResponse + "<INGBRU>0,00</INGBRU>";
      mvarResponse = mvarResponse + "<IMPUES>0,00</IMPUES>";
      mvarResponse = mvarResponse + "<PRECIO>0,00</PRECIO>";
      mvarResponse = mvarResponse + "</SIN_HIJOS>";
      mvarResponse = mvarResponse + "</PLAN>";
      mvarResponse = mvarResponse + "<PLAN>";
      mvarResponse = mvarResponse + "<PLANNCOD>01502</PLANNCOD>";
      mvarResponse = mvarResponse + "<PLANNDES><![CDATA[TODO RIESGO CON FRANQUICIA/NAC. $ 2.500.- / IMP. $ 5.000.]]></PLANNDES>";
      mvarResponse = mvarResponse + "<ORDEN>0003</ORDEN>";
      mvarResponse = mvarResponse + "<LUNPAR>S</LUNPAR>";
      mvarResponse = mvarResponse + "<GRANIZO>S</GRANIZO>";
      mvarResponse = mvarResponse + "<ROBOCON>N</ROBOCON>";
      mvarResponse = mvarResponse + "<ESRC>N</ESRC>";
      mvarResponse = mvarResponse + "<CON_HIJOS>";
      mvarResponse = mvarResponse + "<PRIMA>0,00</PRIMA>";
      mvarResponse = mvarResponse + "<RECARGOS>0,00</RECARGOS>";
      mvarResponse = mvarResponse + "<IVAIMPOR>0,00</IVAIMPOR>";
      mvarResponse = mvarResponse + "<IVAIMPOA>0,00</IVAIMPOA>";
      mvarResponse = mvarResponse + "<IVARETEN>0,00</IVARETEN>";
      mvarResponse = mvarResponse + "<DEREMI>0,00</DEREMI>";
      mvarResponse = mvarResponse + "<SELLADO>0,00</SELLADO>";
      mvarResponse = mvarResponse + "<INGBRU>0,00</INGBRU>";
      mvarResponse = mvarResponse + "<IMPUES>0,00</IMPUES>";
      mvarResponse = mvarResponse + "<PRECIO>0,00</PRECIO>";
      mvarResponse = mvarResponse + "</CON_HIJOS>";
      mvarResponse = mvarResponse + "<SIN_HIJOS>";
      mvarResponse = mvarResponse + "<PRIMA>0,00</PRIMA>";
      mvarResponse = mvarResponse + "<RECARGOS>0,00</RECARGOS>";
      mvarResponse = mvarResponse + "<IVAIMPOR>0,00</IVAIMPOR>";
      mvarResponse = mvarResponse + "<IVAIMPOA>0,00</IVAIMPOA>";
      mvarResponse = mvarResponse + "<IVARETEN>0,00</IVARETEN>";
      mvarResponse = mvarResponse + "<DEREMI>0,00</DEREMI>";
      mvarResponse = mvarResponse + "<SELLADO>0,00</SELLADO>";
      mvarResponse = mvarResponse + "<INGBRU>0,00</INGBRU>";
      mvarResponse = mvarResponse + "<IMPUES>0,00</IMPUES>";
      mvarResponse = mvarResponse + "<PRECIO>0,00</PRECIO>";
      mvarResponse = mvarResponse + "</SIN_HIJOS>";
      mvarResponse = mvarResponse + "</PLAN>";
      mvarResponse = mvarResponse + "<PLAN>";
      mvarResponse = mvarResponse + "<PLANNCOD>01503</PLANNCOD>";
      mvarResponse = mvarResponse + "<PLANNDES><![CDATA[TODO RIESGO CON FRANQUICIA/NAC. $ 3.500.- / IMP. $ 7.000.]]></PLANNDES>";
      mvarResponse = mvarResponse + "<ORDEN>0004</ORDEN>";
      mvarResponse = mvarResponse + "<LUNPAR>S</LUNPAR>";
      mvarResponse = mvarResponse + "<GRANIZO>S</GRANIZO>";
      mvarResponse = mvarResponse + "<ROBOCON>N</ROBOCON>";
      mvarResponse = mvarResponse + "<ESRC>N</ESRC>";
      mvarResponse = mvarResponse + "<CON_HIJOS>";
      mvarResponse = mvarResponse + "<PRIMA>0,00</PRIMA>";
      mvarResponse = mvarResponse + "<RECARGOS>0,00</RECARGOS>";
      mvarResponse = mvarResponse + "<IVAIMPOR>0,00</IVAIMPOR>";
      mvarResponse = mvarResponse + "<IVAIMPOA>0,00</IVAIMPOA>";
      mvarResponse = mvarResponse + "<IVARETEN>0,00</IVARETEN>";
      mvarResponse = mvarResponse + "<DEREMI>0,00</DEREMI>";
      mvarResponse = mvarResponse + "<SELLADO>0,00</SELLADO>";
      mvarResponse = mvarResponse + "<INGBRU>0,00</INGBRU>";
      mvarResponse = mvarResponse + "<IMPUES>0,00</IMPUES>";
      mvarResponse = mvarResponse + "<PRECIO>0,00</PRECIO>";
      mvarResponse = mvarResponse + "</CON_HIJOS>";
      mvarResponse = mvarResponse + "<SIN_HIJOS>";
      mvarResponse = mvarResponse + "<PRIMA>0,00</PRIMA>";
      mvarResponse = mvarResponse + "<RECARGOS>0,00</RECARGOS>";
      mvarResponse = mvarResponse + "<IVAIMPOR>0,00</IVAIMPOR>";
      mvarResponse = mvarResponse + "<IVAIMPOA>0,00</IVAIMPOA>";
      mvarResponse = mvarResponse + "<IVARETEN>0,00</IVARETEN>";
      mvarResponse = mvarResponse + "<DEREMI>0,00</DEREMI>";
      mvarResponse = mvarResponse + "<SELLADO>0,00</SELLADO>";
      mvarResponse = mvarResponse + "<INGBRU>0,00</INGBRU>";
      mvarResponse = mvarResponse + "<IMPUES>0,00</IMPUES>";
      mvarResponse = mvarResponse + "<PRECIO>0,00</PRECIO>";
      mvarResponse = mvarResponse + "</SIN_HIJOS>";
      mvarResponse = mvarResponse + "</PLAN>";
      mvarResponse = mvarResponse + "<PLAN>";
      mvarResponse = mvarResponse + "<PLANNCOD>01299</PLANNCOD>";
      mvarResponse = mvarResponse + "<PLANNDES><![CDATA[RESPONSABILIDAD CIVIL + ROBO O HURTO TOTAL/FRANQUICIA NO APLICABLE]]></PLANNDES>";
      mvarResponse = mvarResponse + "<ORDEN>0007</ORDEN>";
      mvarResponse = mvarResponse + "<LUNPAR>N</LUNPAR>";
      mvarResponse = mvarResponse + "<GRANIZO>N</GRANIZO>";
      mvarResponse = mvarResponse + "<ROBOCON>N</ROBOCON>";
      mvarResponse = mvarResponse + "<ESRC>N</ESRC>";
      mvarResponse = mvarResponse + "<CON_HIJOS>";
      mvarResponse = mvarResponse + "<PRIMA>0,00</PRIMA>";
      mvarResponse = mvarResponse + "<RECARGOS>0,00</RECARGOS>";
      mvarResponse = mvarResponse + "<IVAIMPOR>0,00</IVAIMPOR>";
      mvarResponse = mvarResponse + "<IVAIMPOA>0,00</IVAIMPOA>";
      mvarResponse = mvarResponse + "<IVARETEN>0,00</IVARETEN>";
      mvarResponse = mvarResponse + "<DEREMI>0,00</DEREMI>";
      mvarResponse = mvarResponse + "<SELLADO>0,00</SELLADO>";
      mvarResponse = mvarResponse + "<INGBRU>0,00</INGBRU>";
      mvarResponse = mvarResponse + "<IMPUES>0,00</IMPUES>";
      mvarResponse = mvarResponse + "<PRECIO>0,00</PRECIO>";
      mvarResponse = mvarResponse + "</CON_HIJOS>";
      mvarResponse = mvarResponse + "<SIN_HIJOS>";
      mvarResponse = mvarResponse + "<PRIMA>0,00</PRIMA>";
      mvarResponse = mvarResponse + "<RECARGOS>0,00</RECARGOS>";
      mvarResponse = mvarResponse + "<IVAIMPOR>0,00</IVAIMPOR>";
      mvarResponse = mvarResponse + "<IVAIMPOA>0,00</IVAIMPOA>";
      mvarResponse = mvarResponse + "<IVARETEN>0,00</IVARETEN>";
      mvarResponse = mvarResponse + "<DEREMI>0,00</DEREMI>";
      mvarResponse = mvarResponse + "<SELLADO>0,00</SELLADO>";
      mvarResponse = mvarResponse + "<INGBRU>0,00</INGBRU>";
      mvarResponse = mvarResponse + "<IMPUES>0,00</IMPUES>";
      mvarResponse = mvarResponse + "<PRECIO>0,00</PRECIO>";
      mvarResponse = mvarResponse + "</SIN_HIJOS>";
      mvarResponse = mvarResponse + "</PLAN>";
      mvarResponse = mvarResponse + "</PLANES>";
      mvarResponse = mvarResponse + "<COBERCOD1>002</COBERCOD1>";
      mvarResponse = mvarResponse + "<COBERORD1>02</COBERORD1>";
      mvarResponse = mvarResponse + "<CAPITASG1>300000,00</CAPITASG1>";
      mvarResponse = mvarResponse + "<CAPITIMP1>300000,00</CAPITIMP1>";
      mvarResponse = mvarResponse + "<COBERCOD2>004</COBERCOD2>";
      mvarResponse = mvarResponse + "<COBERORD2>03</COBERORD2>";
      mvarResponse = mvarResponse + "<CAPITASG2>90000,00</CAPITASG2>";
      mvarResponse = mvarResponse + "<CAPITIMP2>900,00</CAPITIMP2>";
      mvarResponse = mvarResponse + "<COBERCOD3>009</COBERCOD3>";
      mvarResponse = mvarResponse + "<COBERORD3>05</COBERORD3>";
      mvarResponse = mvarResponse + "<CAPITASG3>90000,00</CAPITASG3>";
      mvarResponse = mvarResponse + "<CAPITIMP3>900,00</CAPITIMP3>";
      mvarResponse = mvarResponse + "<COBERCOD4>013</COBERCOD4>";
      mvarResponse = mvarResponse + "<COBERORD4>07</COBERORD4>";
      mvarResponse = mvarResponse + "<CAPITASG4>90000,00</CAPITASG4>";
      mvarResponse = mvarResponse + "<CAPITIMP4>900,00</CAPITIMP4>";
      mvarResponse = mvarResponse + "<COBERCOD5>003</COBERCOD5>";
      mvarResponse = mvarResponse + "<COBERORD5>09</COBERORD5>";
      mvarResponse = mvarResponse + "<CAPITASG5>6,00</CAPITASG5>";
      mvarResponse = mvarResponse + "<CAPITIMP5>6,00</CAPITIMP5>";
      mvarResponse = mvarResponse + "<COBERCOD6>905</COBERCOD6>";
      mvarResponse = mvarResponse + "<COBERORD6>17</COBERORD6>";
      mvarResponse = mvarResponse + "<CAPITASG6>20000,00</CAPITASG6>";
      mvarResponse = mvarResponse + "<CAPITIMP6>20000,00</CAPITIMP6>";
      mvarResponse = mvarResponse + "<COBERCOD7>024</COBERCOD7>";
      mvarResponse = mvarResponse + "<COBERORD7>18</COBERORD7>";
      mvarResponse = mvarResponse + "<CAPITASG7>90000,00</CAPITASG7>";
      mvarResponse = mvarResponse + "<CAPITIMP7>900,00</CAPITIMP7>";
      mvarResponse = mvarResponse + "<COBERCOD8>185</COBERCOD8>";
      mvarResponse = mvarResponse + "<COBERORD8>21</COBERORD8>";
      mvarResponse = mvarResponse + "<CAPITASG8>9000,00</CAPITASG8>";
      mvarResponse = mvarResponse + "<CAPITIMP8>9000,00</CAPITIMP8>";
      mvarResponse = mvarResponse + "<COBERCOD9>027</COBERCOD9>";
      mvarResponse = mvarResponse + "<COBERORD9>22</COBERORD9>";
      mvarResponse = mvarResponse + "<CAPITASG9>90000,00</CAPITASG9>";
      mvarResponse = mvarResponse + "<CAPITIMP9>900,00</CAPITIMP9>";
      mvarResponse = mvarResponse + "<COBERCOD10>028</COBERCOD10>";
      mvarResponse = mvarResponse + "<COBERORD10>23</COBERORD10>";
      mvarResponse = mvarResponse + "<CAPITASG10>90000,00</CAPITASG10>";
      mvarResponse = mvarResponse + "<CAPITIMP10>900,00</CAPITIMP10>";
      mvarResponse = mvarResponse + "<COBERCOD11>030</COBERCOD11>";
      mvarResponse = mvarResponse + "<COBERORD11>26</COBERORD11>";
      mvarResponse = mvarResponse + "<CAPITASG11>120000,00</CAPITASG11>";
      mvarResponse = mvarResponse + "<CAPITIMP11>120000,00</CAPITIMP11>";
      mvarResponse = mvarResponse + "<COBERCOD12>031</COBERCOD12>";
      mvarResponse = mvarResponse + "<COBERORD12>27</COBERORD12>";
      mvarResponse = mvarResponse + "<CAPITASG12>2880000,00</CAPITASG12>";
      mvarResponse = mvarResponse + "<CAPITIMP12>2880000,00</CAPITIMP12>";
      mvarResponse = mvarResponse + "<COBERCOD13>032</COBERCOD13>";
      mvarResponse = mvarResponse + "<COBERORD13>28</COBERORD13>";
      mvarResponse = mvarResponse + "<CAPITASG13>60000,00</CAPITASG13>";
      mvarResponse = mvarResponse + "<CAPITIMP13>60000,00</CAPITIMP13>";
      mvarResponse = mvarResponse + "<COBERCOD14>033</COBERCOD14>";
      mvarResponse = mvarResponse + "<COBERORD14>29</COBERORD14>";
      mvarResponse = mvarResponse + "<CAPITASG14>60000,00</CAPITASG14>";
      mvarResponse = mvarResponse + "<CAPITIMP14>60000,00</CAPITIMP14>";
      mvarResponse = mvarResponse + "<COBERCOD15>040</COBERCOD15>";
      mvarResponse = mvarResponse + "<COBERORD15>31</COBERORD15>";
      mvarResponse = mvarResponse + "<CAPITASG15>3000,00</CAPITASG15>";
      mvarResponse = mvarResponse + "<CAPITIMP15>3000,00</CAPITIMP15>";
      mvarResponse = mvarResponse + "<COBERCOD16>041</COBERCOD16>";
      mvarResponse = mvarResponse + "<COBERORD16>32</COBERORD16>";
      mvarResponse = mvarResponse + "<CAPITASG16>90000,00</CAPITASG16>";
      mvarResponse = mvarResponse + "<CAPITIMP16>900,00</CAPITIMP16>";
      mvarResponse = mvarResponse + "<COBERCOD17>039</COBERCOD17>";
      mvarResponse = mvarResponse + "<COBERORD17>33</COBERORD17>";
      mvarResponse = mvarResponse + "<CAPITASG17>30000,00</CAPITASG17>";
      mvarResponse = mvarResponse + "<CAPITIMP17>30000,00</CAPITIMP17>";
      mvarResponse = mvarResponse + "<COBERCOD18>810</COBERCOD18>";
      mvarResponse = mvarResponse + "<COBERORD18>35</COBERORD18>";
      mvarResponse = mvarResponse + "<CAPITASG18>3000,00</CAPITASG18>";
      mvarResponse = mvarResponse + "<CAPITIMP18>3000,00</CAPITIMP18>";
      mvarResponse = mvarResponse + "<COBERCOD19>000</COBERCOD19>";
      mvarResponse = mvarResponse + "<COBERORD19>00</COBERORD19>";
      mvarResponse = mvarResponse + "<CAPITASG19>0,00</CAPITASG19>";
      mvarResponse = mvarResponse + "<CAPITIMP19>0,00</CAPITIMP19>";
      mvarResponse = mvarResponse + "<COBERCOD20>000</COBERCOD20>";
      mvarResponse = mvarResponse + "<COBERORD20>00</COBERORD20>";
      mvarResponse = mvarResponse + "<CAPITASG20>0,00</CAPITASG20>";
      mvarResponse = mvarResponse + "<CAPITIMP20>0,00</CAPITIMP20>";
      mvarResponse = mvarResponse + "<COBERCOD21>000</COBERCOD21>";
      mvarResponse = mvarResponse + "<COBERORD21>00</COBERORD21>";
      mvarResponse = mvarResponse + "<CAPITASG21>0,00</CAPITASG21>";
      mvarResponse = mvarResponse + "<CAPITIMP21>0,00</CAPITIMP21>";
      mvarResponse = mvarResponse + "<COBERCOD22>000</COBERCOD22>";
      mvarResponse = mvarResponse + "<COBERORD22>00</COBERORD22>";
      mvarResponse = mvarResponse + "<CAPITASG22>0,00</CAPITASG22>";
      mvarResponse = mvarResponse + "<CAPITIMP22>0,00</CAPITIMP22>";
      mvarResponse = mvarResponse + "<COBERCOD23>000</COBERCOD23>";
      mvarResponse = mvarResponse + "<COBERORD23>00</COBERORD23>";
      mvarResponse = mvarResponse + "<CAPITASG23>0,00</CAPITASG23>";
      mvarResponse = mvarResponse + "<CAPITIMP23>0,00</CAPITIMP23>";
      mvarResponse = mvarResponse + "<COBERCOD24>000</COBERCOD24>";
      mvarResponse = mvarResponse + "<COBERORD24>00</COBERORD24>";
      mvarResponse = mvarResponse + "<CAPITASG24>0,00</CAPITASG24>";
      mvarResponse = mvarResponse + "<CAPITIMP24>0,00</CAPITIMP24>";
      mvarResponse = mvarResponse + "<COBERCOD25>000</COBERCOD25>";
      mvarResponse = mvarResponse + "<COBERORD25>00</COBERORD25>";
      mvarResponse = mvarResponse + "<CAPITASG25>0,00</CAPITASG25>";
      mvarResponse = mvarResponse + "<CAPITIMP25>0,00</CAPITIMP25>";
      mvarResponse = mvarResponse + "<COBERCOD26>000</COBERCOD26>";
      mvarResponse = mvarResponse + "<COBERORD26>00</COBERORD26>";
      mvarResponse = mvarResponse + "<CAPITASG26>0,00</CAPITASG26>";
      mvarResponse = mvarResponse + "<CAPITIMP26>0,00</CAPITIMP26>";
      mvarResponse = mvarResponse + "<COBERCOD27>000</COBERCOD27>";
      mvarResponse = mvarResponse + "<COBERORD27>00</COBERORD27>";
      mvarResponse = mvarResponse + "<CAPITASG27>0,00</CAPITASG27>";
      mvarResponse = mvarResponse + "<CAPITIMP27>0,00</CAPITIMP27>";
      mvarResponse = mvarResponse + "<COBERCOD28>000</COBERCOD28>";
      mvarResponse = mvarResponse + "<COBERORD28>00</COBERORD28>";
      mvarResponse = mvarResponse + "<CAPITASG28>0,00</CAPITASG28>";
      mvarResponse = mvarResponse + "<CAPITIMP28>0,00</CAPITIMP28>";
      mvarResponse = mvarResponse + "<COBERCOD29>000</COBERCOD29>";
      mvarResponse = mvarResponse + "<COBERORD29>00</COBERORD29>";
      mvarResponse = mvarResponse + "<CAPITASG29>0,00</CAPITASG29>";
      mvarResponse = mvarResponse + "<CAPITIMP29>0,00</CAPITIMP29>";
      mvarResponse = mvarResponse + "<COBERCOD30>000</COBERCOD30>";
      mvarResponse = mvarResponse + "<COBERORD30>00</COBERORD30>";
      mvarResponse = mvarResponse + "<CAPITASG30>0,00</CAPITASG30>";
      mvarResponse = mvarResponse + "<CAPITIMP30>0,00</CAPITIMP30>";
      mvarResponse = mvarResponse + "</Response>";




      wobjXMLRequestCotis.loadXML( mvarResponse );

      wvarStep = 170;
      //jc 09/2010 cambia la salida del componente
      //If wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_code").Text = "OK" Then
      if( XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {

        wobjXMLValid = new XmlDomExtended();
        wobjXMLValid.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteArchivoAUSSOL_XML));

        //Chequea si busca el precio con Hijos o sin Hijos
        //08/2011 ANEXO I cambia el formato del componente de cotiz.
        wvarStep = 2180;
        if( wobjXMLRequest.selectNodes( "//HIJOS/HIJO" ) .getLength() > 0 )
        {
          wvarTieneHijos = "_CH";
          wvarNuevoHijos = "CON_HIJOS";
        }
        else
        {
          wvarTieneHijos = "_SH";
          wvarNuevoHijos = "SIN_HIJOS";
        }

        //Si existe el plan
        //Chequea si tiene el mismo importe la Solicitud con la Cotizacion
        wvarStep = 2181;
        //jc 09/2010 cambia la salida del componente (para cualquier plan el importe viene en RC_SH RC_CH
        //If Not Val(wobjXMLRequest.selectSingleNode("//PRECIO").Text) = Val(wobjXMLRequestCotis.selectSingleNode("//PRECIOPLAN" & wvarTieneHijos).Text) Then
        //08/2011 ANEXO I
        //If Not Val(wobjXMLRequest.selectSingleNode("//PRECIO").Text) = Val(wobjXMLRequestCotis.selectSingleNode("//RC" & wvarTieneHijos).Text) Then
        if( ! (VB.val( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PRECIO" )  ) ) == VB.val( XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( ("//" + wvarNuevoHijos + "/PRECIO") )  ) )) )
        {
          fncComparaConCotizacion = false;
          pvarRes.set( "<LBA_WS res_code=\"-550\" res_msg=\"Existe una diferencia de precio entre la Solicitud(" + VB.val( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PRECIO" )  ) ) + ")y la Cotizacion(" + VB.val( XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( ("//" + wvarNuevoHijos + "/PRECIO") )  ) ) + ")\"></LBA_WS>" );
          return fncComparaConCotizacion;
        }
        else
        {
          wvarStep = 2182;
          //Lea agrega los valores de la cotizacion
          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "PRECIOPLAN" + wvarTieneHijos, "" ) */ );
          //jc 09/2010 cambia componente
          //wobjXMLRequest.selectSingleNode("//PRECIOPLAN" & wvarTieneHijos).Text = wobjXMLRequestCotis.selectSingleNode("//PRECIOPLAN" & wvarTieneHijos).Text
          wvarStep = 2183;
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//PRECIOPLAN" + wvarTieneHijos ) , XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( "//" + wvarNuevoHijos + "/PRECIO" )  ) );

        //TODO: Inicio Cambio
          //Se agrega Descripci�n del Plan
          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "PLANDES", "" ) */ );
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//PLANDES" ) , XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( "//PLANNDES" )  ) );

          //Se agrega AGECLA...
          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "AGECLA", "" ) */ );
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//AGECLA" ) , XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( "//AGE_CLA" )  ) );
        //TODO: Fin Cambio

          for( wvarcounter = 1; wvarcounter <= 30; wvarcounter++ )
          {
            wvarStep = 2184;

            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "COBERCOD" + wvarcounter, "" ) */ );
            XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//COBERCOD" + wvarcounter ) , XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( "//COBERCOD" + wvarcounter )  ) );

            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "COBERORD" + wvarcounter, "" ) */ );
            XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//COBERORD" + wvarcounter ) , XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( "//COBERORD" + wvarcounter )  ) );

            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CAPITASG" + wvarcounter, "" ) */ );
            XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//CAPITASG" + wvarcounter ) , XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( "//CAPITASG" + wvarcounter )  ) );

            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CAPITIMP" + wvarcounter, "" ) */ );
            XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//CAPITIMP" + wvarcounter ) , XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( "//CAPITIMP" + wvarcounter )  ) );
          }
        //TODO: Inicio Cambio
          //Se le agregan datos para la Integraci�n al AIS
          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "RAMOPCOD", "" ) */ );
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//RAMOPCOD" ) , mvarRAMOPCOD );

          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "POLIZANN", "" ) */ );
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//POLIZANN" ) , mvarPOLIZANN );

          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "POLIZSEC", "" ) */ );
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//POLIZSEC" ) , mvarPOLIZSEC );

          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CERTIPOL", "" ) */ );
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//CERTIPOL" ) , mvarCERTIPOL );

          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CERTIANN", "" ) */ );
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//CERTIANN" ) , mvarCERTIANN );

          if( wobjXMLRequest.selectSingleNode( "//CERTISEC" )  == (org.w3c.dom.Node) null )
          {
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CERTISEC", "" ) */ );
          }
          XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//CERTISEC" ) , mvarCertiSec );

        //TODO: Fin Cambio
          pvarRes.set( "<Response><Estado resultado=\"true\" mensaje=\"\"/>" + wobjXMLRequest.getDocument().getDocumentElement().toString() + "</Response>" );
        }
      }

      wvarStep = 2190;
      if( ! (wobjXMLRequestCotis == (diamondedge.util.XmlDom) null) )
      {
        //jc 09/2010 cambia la salida del componente
        //If Not UCase(wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_code").Text) = "OK" Then
        if( XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
        {
          fncComparaConCotizacion = false;
        }
        else
        {
          fncComparaConCotizacion = true;
        }
      }
      else
      {
        fncComparaConCotizacion = true;
      }


      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = null;
      return fncComparaConCotizacion;


      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + mcteErrorInesperadoDescr + "\" /></Response>" );
        wvarCodErr.set( -1000 );
        wvarMensaje.set( mcteErrorInesperadoDescr );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncComparaConCotizacion = false;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncComparaConCotizacion;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
