package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class GetValidacionCotHO implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetValidacionCotHO";
  /**
   * Archivo de Errores
   */
  static final String mcteArchivoHOM_XML = "LBA_VALIDACION_COT_HO.XML";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  /**
   * EG 14-10 cambia constante
   * Const mcteParam_COBROCOD            As String = "COBROCOD"
   */
  static final String mcteParam_COBROCOD = "FPAGO";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_TIPOHOGAR = "TIPOHOGAR";
  /**
   * EG 14-10 cambia constante
   * Const mcteParam_PLANNCOD            As String = "PLANNCOD"
   */
  static final String mcteParam_PLANNCOD = "PLAN";
  /**
   * EG 14-10 cambia constante
   * Const mcteParam_Provi                As String = "PROVI"
   */
  static final String mcteParam_Provi = "PROVCOD";
  /**
   * EG 14-10 cambia constante
   * Const mcteParam_LocalidadCod         As String = "LOCALIDADCOD"
   */
  static final String mcteParam_LocalidadCod = "LOCALIDAD";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS/COBERTURA";
  /**
   * DATOS DE LAS CAMPA�AS
   */
  static final String mcteNodos_Campa = "//Request/CAMPANIAS/CAMPANIA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncValidaABase
   */
  private final String wcteFnName = "fncValidaABase";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant pvarRes = new Variant();
    Variant wvarCodErr = new Variant();
    //
    //
    //Declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      pvarRes.set( "" );
      wvarCodErr.set( "" );
      wvarMensaje.set( "" );
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*unsup mobjCOM_Context.SetAbort() */;
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*unsup mobjCOM_Context.SetComplete() */;
        }
        return IAction_Execute;
      }
      //
      pvarResponse.set( pvarRes );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        IAction_Execute = 1;
        //mobjCOM_Context.SetAbort
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes ) throws Exception
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLCheck = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLError = null;
    int wvarContChild = 0;
    int wvarContNode = 0;



    wvarStep = 10;
    //Chequeo que los datos esten OK
    //Chequeo que no venga nada en blanco
    wobjXMLRequest = new diamondedge.util.XmlDom();
    //unsup wobjXMLRequest.async = false;
    wobjXMLRequest.loadXML( pvarRequest );
    //
    wobjXMLCheck = new diamondedge.util.XmlDom();
    //unsup wobjXMLCheck.async = false;
    wobjXMLCheck.load( System.getProperty("user.dir") + "\\" + mcteArchivoHOM_XML );
    //
    wvarStep = 20;
    wobjXMLList = null /*unsup wobjXMLCheck.selectNodes( "//ERRORES/HOGAR/VACIOS/CAMPOS/ERROR" ) */;
    for( wvarContChild = 0; wvarContChild <= (wobjXMLList.getLength() - 1); wvarContChild++ )
    {
      wobjXMLError = wobjXMLList.item( wvarContChild );
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLError.selectSingleNode( "CAMPO" ) *) )) ) */ == (org.w3c.dom.Node) null) )
      {
        wvarStep = 30;
        if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLError.selectSingleNode( "CAMPO" ) *) )) ) */ ) ).equals( "" ) )
        {
          wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "VALORRESPUESTA" ) */ ) );
          wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "TEXTOERROR" ) */ ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      else
      {
        //error
        wvarStep = 40;
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "VALORRESPUESTA" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "TEXTOERROR" ) */ ) );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      wobjXMLError = (org.w3c.dom.Node) null;
    }


    //Chequeo Tipo y Longitud de Datos para el XML Padre
    wvarStep = 50;

    for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().getLength() - 1); wvarContChild++ )
    {
      if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
      {
        //Chequeo la longitud y si tiene Largo Fijo
        wvarStep = 60;
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
        {
          wvarStep = 70;
          if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") ) */ ) )) )
          {
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
        else
        {
          wvarStep = 80;
          if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") ) */ ) ) )
          {
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }

        //Chequeo el Tipo
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
        {
          wvarStep = 90;
          if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ).isNumeric()) )
          {
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
          else
          {
            //Chequeo que el dato numerico no sea 0
            //Excluye los nodos POLIZANN
            wvarStep = 100;
            if( (Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) == 0) && ! ((null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ).getNodeName().equals( "POLIZANN" ))) )
            {
              wvarCodErr.set( -199 );
              wvarMensaje.set( "El valor del dato numerico no puede estar en 0" );
              pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
              return fncGetAll;
            }
          }
        }
      }
    }

    //Chequeo Tipo y Longitud de Datos para Coberturas
    for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) */.getLength() - 1); wvarContChild++ )
    {
      wvarStep = 110;
      for( wvarContNode = 0; wvarContNode <= (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) */.item( wvarContChild ).getChildNodes().getLength() - 1); wvarContNode++ )
      {
        wvarStep = 120;
        if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
        {
          //Chequeo la longitud y si tiene Largo Fijo
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
          {
            wvarStep = 130;
            if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) )) )
            {
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
          else
          {
            if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) ) )
            {
              wvarStep = 140;
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }

          //Chequeo el Tipo
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
          {
            wvarStep = 150;
            if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ).isNumeric()) )
            {
              wvarStep = 160;
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            else
            {
              //Chequeo que el dato numerico no sea 0
              if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == 0 )
              {
                wvarStep = 160;
                wvarCodErr.set( -199 );
                wvarMensaje.set( "El valor del dato numerico no puede estar en 0" );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
          }
        }
      }
    }


    //Chequeo Tipo y Longitud de Datos para Campa�as
    for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) */.getLength() - 1); wvarContChild++ )
    {
      wvarStep = 170;
      for( wvarContNode = 0; wvarContNode <= (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) */.item( wvarContChild ).getChildNodes().getLength() - 1); wvarContNode++ )
      {
        if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
        {
          wvarStep = 180;
          //Chequeo la longitud y si tiene Largo Fijo
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
          {
            if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) )) )
            {
              wvarStep = 190;
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
          else
          {
            if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) ) )
            {
              wvarStep = 200;
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }

          //Chequeo el Tipo
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
          {
            wvarStep = 210;
            if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ).isNumeric()) )
            {
              wvarStep = 220;
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
        }
      }
    }

    //Chequeo los datos FIJOS
    wvarStep = 230;
    if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_CLIENIVA + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) *) ) + "']") ) */.getLength() == 0 )
    {
      //error
      wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_CLIENIVA + "/N_ERROR" ) */ ) );
      wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_CLIENIVA + "/TEXTOERROR" ) */ ) );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }

    wobjXMLCheck = (diamondedge.util.XmlDom) null;

    //Cheque que vengan hasta 50 coberturas
    wvarStep = 240;
    wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) */;
    if( wobjXMLList.getLength() > 50 )
    {
      wvarCodErr.set( -48 );
      wvarMensaje.set( "Existen mas coberturas de las permitidas" );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }

    //Chequeo que no vengan las coberturas en blanco
    wobjXMLList = (org.w3c.dom.NodeList) null;

    //Cheque que vengan hasta 10 campanias
    wvarStep = 250;
    wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) */;
    if( wobjXMLList.getLength() > 10 )
    {
      wvarCodErr.set( -49 );
      wvarMensaje.set( "Existen mas campanias de las permitidas" );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }

    //Chequeo que no vengan las campanias en blanco
    wvarStep = 270;
    for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
    {
      if( wobjXMLList.item( wvarContNode ).getChildNodes().item( 0 ) == (org.w3c.dom.Node) null )
      {
        //error
        wvarCodErr.set( -1 );
        wvarMensaje.set( "Falta el dato de la Campania" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
    }
    wobjXMLList = (org.w3c.dom.NodeList) null;

    wvarStep = 280;
    if( invoke( "fncValidaABase", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } ) )
    {
      fncGetAll = true;
    }
    else
    {
      fncGetAll = false;
    }

    fin: 
    return fncGetAll;

    //~~~~~~~~~~~~~~~
    ErrorHandler: 
    //~~~~~~~~~~~~~~~
    pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
    wvarCodErr.set( General.mcteErrorInesperadoCod );
    wvarMensaje.set( General.mcteErrorInesperadoDescr );

    mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

    fncGetAll = false;
    /*unsup mobjCOM_Context.SetComplete() */;
    //unsup GoTo fin
    return fncGetAll;
  }

  private boolean fncValidaABase( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncValidaABase = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Object wobjClass = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;

    //
    //
    try 
    {
      //
      wvarStep = 10;
      wobjClass = new LBA_InterWSBrok.GetNroCotHO();
      wobjClass.Execute( pvarRequest, pvarRes.toString(), "" );
      //
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRes.toString() );
      //
      wvarStep = 20;
      wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" ) */ ) );
      wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) );
      if( ! (Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" ) */ ) ) == 0) )
      {
        fncValidaABase = false;
      }
      else
      {
        fncValidaABase = true;
      }

      fin: 
      wobjClass = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return fncValidaABase;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        fncValidaABase = false;
        /*unsup mobjCOM_Context.SetComplete() */;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncValidaABase;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
