package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class PutCotizacionHO implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  static final String mcteClassName = "LBA_InterWSBrok.PutCotizacionHO";
  static final String mcteStoreProc = "SPSNCV_BRO_COTI_HOM1_GRABA";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  /**
   * EG 10-2010 se cambia la constante
   * Const mcteParam_COBROCOD             As String = "COBROCOD"
   */
  static final String mcteParam_COBROCOD = "FPAGO";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_TIPOHOGAR = "TIPOHOGAR";
  /**
   * EG 10-2010 se cambia la constante
   * Const mcteParam_PLANNCOD             As String = "PLANNCOD"
   */
  static final String mcteParam_PLANNCOD = "PLAN";
  /**
   * EG 10-2010 se cambia la constante
   * Const mcteParam_Provi                As String = "PROVI"
   */
  static final String mcteParam_Provi = "PROVCOD";
  /**
   * EG 10-2010 se cambia la constante
   * Const mcteParam_LocalidadCod         As String = "LOCALIDADCOD"
   */
  static final String mcteParam_LocalidadCod = "LOCALIDAD";
  static final String mcteParam_TipoOperac = "TIPOOPERAC";
  static final String mcteParam_RAMOPCOD = "RAMOPCOD";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  static final String mcteParam_NROCOT = "COT_NRO";
  /**
   * Const mcteParam_Estado               As String = "ESTADO"
   */
  static final String mcteParam_PedidoAno = "MSGANO";
  static final String mcteParam_PedidoMes = "MSGMES";
  static final String mcteParam_PedidoDia = "MSGDIA";
  static final String mcteParam_PedidoHora = "MSGHORA";
  static final String mcteParam_PedidoMinuto = "MSGMINUTO";
  static final String mcteParam_PedidoSegundo = "MSGSEGUNDO";
  static final String mcteParam_CERTISEC = "CERTISEC";
  static final String mcteParam_DOMICCPO = "DOMICCPO";
  static final String mcteParam_ZONA = "CODZONA";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS/COBERTURA";
  static final String mcteParam_COBERCOD = "COBERCOD";
  static final String mcteParam_CONTRMOD = "CONTRMOD";
  static final String mcteParam_CAPITASG = "CAPITASG";
  /**
   * DATOS DE LAS CAMPA�AS
   */
  static final String mcteNodos_Campa = "//Request/CAMPANIAS/CAMPANIA";
  static final String mcteParam_CampaCod = "CAMPACOD";
  /**
   * PRODUCTOR
   */
  static final String mctePORTAL = "LBA";
  /**
   * Horas de Ingreso y Salida
   */
  static final String mcteParam_Recepcion = "RECEPCION";
  static final String mcteParam_Envio = "ENVIO";
  /**
   * Objetos del FrameWork
   */
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncPutAll
   */
  private final String wcteFnName = "fncPutAll";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarMensaje = "";
    Variant wvarCodErr = new Variant();
    Variant pvarRes = new Variant();
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wvarMensaje = "";
      wvarCodErr.set( "" );
      pvarRes.set( "" );
      if( ! (invoke( "fncPutAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        if( !wvarCodErr.toString().equals( General.mcteErrorInesperadoCod ) )
        {
          pvarResponse.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        }
        else
        {
          pvarResponse.set( pvarRes );
        }

        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*unsup mobjCOM_Context.SetAbort() */;
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*unsup mobjCOM_Context.SetComplete() */;
        }
        return IAction_Execute;
      }
      //
      pvarResponse.set( pvarRes );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncPutAll( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarcounter = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_USUARCOD = "";
    String mvarWDB_EFECTANN = "";
    String mvarWDB_EFECTMES = "";
    String mvarWDB_EFECTDIA = "";
    String mvarWDB_CLIENTIP = "";
    String mvarWDB_DOMICCPO = "";
    String mvarWDB_PROVICOD = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_P_CIAASCOD = "";
    String mvarWDB_P_EMISIANN = "";
    String mvarWDB_P_EMISIMES = "";
    String mvarWDB_P_EMISIDIA = "";
    String mvarWDB_P_COBROCOD = "";
    String mvarWDB_P_COBROFOR = "";
    String mvarWDB_P_TIVIVCOD = "";
    String mvarWDB_P_PLANNCOD = "";
    String mvarWDB_P_ZONA = "";
    String mvarWDB_P_CLIENIVA = "";

    //
    //
    //
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_CODINST = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }
      //
      wvarStep = 30;
      mvarWDB_RAMOPCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAMOPCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_RAMOPCOD ) */ );
      }
      //
      wvarStep = 40;
      mvarWDB_POLIZANN = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN ) */ );
      }
      //
      wvarStep = 50;
      mvarWDB_POLIZSEC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC ) */ );
      }
      //
      wvarStep = 60;
      mvarWDB_CERTISEC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CERTISEC) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTISEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CERTISEC ) */ );
      }
      //
      wvarStep = 70;
      mvarWDB_USUARCOD = mvarWDB_CODINST;
      //
      wvarStep = 80;
      mvarWDB_DOMICCPO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICCPO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCod ) */ );
      }
      //
      wvarStep = 90;
      mvarWDB_PROVICOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi ) */ );
      }
      //
      wvarStep = 100;
      mvarWDB_P_EMISIANN = Strings.right( Strings.fill( 4, "0" ) + DateTime.year( DateTime.now() ), 4 );
      mvarWDB_P_EMISIMES = Strings.right( Strings.fill( 2, "0" ) + DateTime.month( DateTime.now() ), 2 );
      mvarWDB_P_EMISIDIA = Strings.right( Strings.fill( 2, "0" ) + DateTime.day( DateTime.now() ), 2 );
      //
      mvarWDB_EFECTANN = Strings.right( Strings.fill( 4, "0" ) + DateTime.year( DateTime.now() ), 4 );
      mvarWDB_EFECTMES = Strings.right( Strings.fill( 2, "0" ) + DateTime.month( DateTime.now() ), 2 );
      mvarWDB_EFECTDIA = Strings.right( Strings.fill( 2, "0" ) + DateTime.day( DateTime.now() ), 2 );
      //
      wvarStep = 110;
      mvarWDB_COBROTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROTIP = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP ) */ ) );
      }
      //
      wvarStep = 120;
      mvarWDB_P_COBROCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_COBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD ) */ );
      }
      //
      wvarStep = 130;
      mvarWDB_P_TIVIVCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TIPOHOGAR) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_TIVIVCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TIPOHOGAR ) */ );
      }
      //
      wvarStep = 140;
      mvarWDB_P_PLANNCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PLANNCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_PLANNCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PLANNCOD ) */ );
      }
      //
      wvarStep = 150;
      mvarWDB_P_ZONA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ZONA) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_ZONA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ZONA ) */ );
      }
      //
      wvarStep = 160;
      mvarWDB_P_CLIENIVA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CLIENIVA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENIVA ) */ );
      }
      //
      wvarStep = 170;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      //
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 180;
      //
      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, (mvarWDB_RAMOPCOD.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_RAMOPCOD)) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 190;
      wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_POLIZANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 200;
      wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_POLIZSEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 210;
      wobjDBParm = new Parameter( "@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_CODINST ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 220;
      wobjDBParm = new Parameter( "@CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 230;
      wobjDBParm = new Parameter( "@CERTISEC", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, (mvarWDB_CERTISEC.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_CERTISEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 240;
      wobjDBParm = new Parameter( "@SUPLENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 250;
      wobjDBParm = new Parameter( "@USUARCOD", AdoConst.adChar, AdoConst.adParamInput, 10, (mvarWDB_USUARCOD.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_USUARCOD)) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 260;
      wobjDBParm = new Parameter( "@NUMEDOCU", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( Strings.fill( 11, "0" ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 270;
      wobjDBParm = new Parameter( "@TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 280;
      wobjDBParm = new Parameter( "@CLIENAP1", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( "Cotiz. Innominada" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 290;
      wobjDBParm = new Parameter( "@CLIENAP2", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 300;
      wobjDBParm = new Parameter( "@CLIENNOM", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 310;
      wobjDBParm = new Parameter( "@NACIMANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 320;
      wobjDBParm = new Parameter( "@NACIMMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 330;
      wobjDBParm = new Parameter( "@NACIMDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 340;
      wobjDBParm = new Parameter( "@CLIENSEX", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 350;
      wobjDBParm = new Parameter( "@CLIENEST", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 360;
      wobjDBParm = new Parameter( "@PAISSCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "00" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 370;
      wobjDBParm = new Parameter( "@IDIOMCOD", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 380;
      wobjDBParm = new Parameter( "@NUMHIJOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 390;
      wobjDBParm = new Parameter( "@EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTANN.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_EFECTANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 400;
      wobjDBParm = new Parameter( "@EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTMES.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_EFECTMES)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 410;
      wobjDBParm = new Parameter( "@EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTDIA.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_EFECTDIA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 420;
      //10/2010 no es mas F persona f�sica
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTIP", adChar, adParamInput, 2, "F")
      wobjDBParm = new Parameter( "@CLIENTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "00" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 430;
      wobjDBParm = new Parameter( "@CLIENFUM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 440;
      wobjDBParm = new Parameter( "@CLIENDOZ", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 450;
      wobjDBParm = new Parameter( "@ABRIDTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 460;
      wobjDBParm = new Parameter( "@ABRIDNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 470;
      wobjDBParm = new Parameter( "@CLIENORG", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 480;
      wobjDBParm = new Parameter( "@PERSOTIP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "F" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 490;
      wobjDBParm = new Parameter( "@CLIENCLA", AdoConst.adChar, AdoConst.adParamInput, 9, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 500;
      wobjDBParm = new Parameter( "@FALLEANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 510;
      wobjDBParm = new Parameter( "@FALLEMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 520;
      wobjDBParm = new Parameter( "@FALLEDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 530;
      wobjDBParm = new Parameter( "@FBAJAANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 540;
      wobjDBParm = new Parameter( "@FBAJAMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 550;
      wobjDBParm = new Parameter( "@FBAJADIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 560;
      wobjDBParm = new Parameter( "@EMAIL", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 570;
      wobjDBParm = new Parameter( "@DOMICCAL", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 580;
      wobjDBParm = new Parameter( "@DOMICDOM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 590;
      wobjDBParm = new Parameter( "@DOMICDNU", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 600;
      wobjDBParm = new Parameter( "@DOMICESC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 610;
      wobjDBParm = new Parameter( "@DOMICPIS", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 620;
      wobjDBParm = new Parameter( "@DOMICPTA", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 630;
      wobjDBParm = new Parameter( "@DOMICPOB", AdoConst.adVarChar, AdoConst.adParamInput, 60, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 640;
      wobjDBParm = new Parameter( "@DOMICCPO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_DOMICCPO.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_DOMICCPO)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 650;
      wobjDBParm = new Parameter( "@PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_PROVICOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 660;
      wobjDBParm = new Parameter( "@TELCOD", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 670;
      wobjDBParm = new Parameter( "@TELNRO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 680;
      wobjDBParm = new Parameter( "@PRINCIPAL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 690;
      wobjDBParm = new Parameter( "@TIPOCUEN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 700;
      wobjDBParm = new Parameter( "@COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, (mvarWDB_COBROTIP.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_COBROTIP)) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 710;
      wobjDBParm = new Parameter( "@BANCOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 720;
      wobjDBParm = new Parameter( "@SUCURCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 730;
      wobjDBParm = new Parameter( "@CUENTDC", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 740;
      wobjDBParm = new Parameter( "@CUENNUME", AdoConst.adChar, AdoConst.adParamInput, 16, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 750;
      wobjDBParm = new Parameter( "@TARJECOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 760;
      wobjDBParm = new Parameter( "@VENCIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 770;
      wobjDBParm = new Parameter( "@VENCIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 780;
      wobjDBParm = new Parameter( "@VENCIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 790;
      wobjDBParm = new Parameter( "@P_CIAASCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "0001" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 800;
      wobjDBParm = new Parameter( "@P_EMISIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIANN.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_P_EMISIANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 810;
      wobjDBParm = new Parameter( "@P_EMISIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIMES.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_P_EMISIMES)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 820;
      wobjDBParm = new Parameter( "@P_EMISIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIDIA.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_P_EMISIDIA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 830;
      wobjDBParm = new Parameter( "@P_EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTANN.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_EFECTANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 840;
      wobjDBParm = new Parameter( "@P_EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTMES.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_EFECTMES)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 850;
      wobjDBParm = new Parameter( "@P_EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTDIA.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_EFECTDIA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 860;
      wobjDBParm = new Parameter( "@P_USUARCOD", AdoConst.adChar, AdoConst.adParamInput, 10, (mvarWDB_USUARCOD.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_USUARCOD)) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 870;
      wobjDBParm = new Parameter( "@P_SITUCPOL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "A" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 880;
      wobjDBParm = new Parameter( "@P_CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 890;
      wobjDBParm = new Parameter( "@P_NUMEDOCU", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( Strings.fill( 11, "0" ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 900;
      wobjDBParm = new Parameter( "@P_TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 910;
      wobjDBParm = new Parameter( "@P_DOMICSEC1", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 920;
      wobjDBParm = new Parameter( "@P_DOMICSEC2", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 930;
      wobjDBParm = new Parameter( "@P_CUENTSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 940;
      wobjDBParm = new Parameter( "@P_COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_COBROCOD.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_P_COBROCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 950;
      wobjDBParm = new Parameter( "@P_COBROFOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 960;
      wobjDBParm = new Parameter( "@P_COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, (mvarWDB_COBROTIP.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_COBROTIP)) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 970;
      wobjDBParm = new Parameter( "@P_TIVIVCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_TIVIVCOD.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_P_TIVIVCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 980;
      wobjDBParm = new Parameter( "@P_ALTURNUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 990;
      wobjDBParm = new Parameter( "@P_USOTIPOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1000;
      wobjDBParm = new Parameter( "@P_SWCALDER", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1010;
      wobjDBParm = new Parameter( "@P_SWASCENS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1020;
      wobjDBParm = new Parameter( "@P_ALARMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1030;
      wobjDBParm = new Parameter( "@P_GUARDTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1040;
      wobjDBParm = new Parameter( "@P_SWCREJAS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1050;
      wobjDBParm = new Parameter( "@P_SWPBLIND", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1060;
      wobjDBParm = new Parameter( "@P_SWDISYUN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1070;
      wobjDBParm = new Parameter( "@P_SWPROPIE", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1080;
      wobjDBParm = new Parameter( "@P_PLANNCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_PLANNCOD.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_P_PLANNCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1090;
      wobjDBParm = new Parameter( "@P_PLANNDAB", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1100;
      wobjDBParm = new Parameter( "@P_BARRIOCOUNT", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1110;
      wobjDBParm = new Parameter( "@P_ZONA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_ZONA.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_P_ZONA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1120;
      wobjDBParm = new Parameter( "@P_CLIENIVA", AdoConst.adChar, AdoConst.adParamInput, 1, (mvarWDB_P_CLIENIVA.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_P_CLIENIVA)) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1130;
      wobjDBParm = new Parameter( "@P_ASEG_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1140;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) */;
      //
      for( wvarcounter = 1; wvarcounter <= 10; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
          wobjDBParm = new Parameter( "@P_CAMPACOD_" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_CampaCod ) */ ) ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          wobjDBParm = new Parameter( "@P_CAMPACOD_" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
        }
      }
      //
      wobjXMLList = (org.w3c.dom.NodeList) null;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) */;
      //
      wvarStep = 1150;
      // AM se cambio de 20 a 30
      for( wvarcounter = 1; wvarcounter <= 30; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
          wobjDBParm = new Parameter( "@P_COBERCOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_COBERCOD ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 3 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_COBERORD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CAPITASG" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_CAPITASG ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CAPITIMP" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 100000 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CONTRMOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_CONTRMOD ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          wobjDBParm = new Parameter( "@P_COBERCOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 3 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_COBERORD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CAPITASG" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CAPITIMP" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CONTRMOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
        }
      }
      //
      wvarStep = 1160;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      wobjDBCnn.close();
      fncPutAll = true;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//CERTISEC" ) */, wobjDBCmd.getParameters().getParameter("@CERTISEC").getValue().toString() );
      pvarRes.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );

      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
      return fncPutAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );


        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncPutAll = false;
        /*unsup mobjCOM_Context.SetComplete() */;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutAll;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
