package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class GetValidacionVehiculo implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetValidacionVehiculo";
  static final String mcteStoreProcSelect = "SPSNCV_BRO_SELECT_OPER_ESP";
  static final String mcteStoreProcValidacion = "SPSNCV_BRO_VALIDA_VEHICULO";
  /**
   * Archivo de Errores
   */
  static final String mcteArchivoVehiculo_XML = "LBA_VALIDACION_VEHICULO.XML";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_Marca = "MARCA";
  static final String mcteParam_Modelo = "MODELO";
  static final String mcteParam_EfectAnn = "EFECTANN";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncValidaABase
   */
  private final String wcteFnName = "fncValidaABase";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant pvarRes = new Variant();
    Variant wvarCodErr = new Variant();
    //
    //
    //Declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      pvarRes.set( "" );
      wvarCodErr.set( "" );
      wvarMensaje.set( "" );
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*unsup mobjCOM_Context.SetAbort() */;
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*unsup mobjCOM_Context.SetComplete() */;
        }
        return IAction_Execute;
      }

      pvarResponse.set( pvarRes );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        IAction_Execute = 1;
        //mobjCOM_Context.SetAbort
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes ) throws Exception
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLCheck = null;
    diamondedge.util.XmlDom wobjXMLParams = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLError = null;
    diamondedge.util.XmlDom wobjXMLStatusTarjeta = null;
    int wvarContChild = 0;
    int wvarContNode = 0;
    String wvarTarjeta = "";
    String wvarStatusTarjeta = "";
    java.util.Date wvarFechaActual = DateTime.EmptyDate;
    java.util.Date wvarFechaVigencia = DateTime.EmptyDate;
    int mvarDif = 0;









    wvarStep = 10;
    //Chequeo que los datos esten OK
    //Chequeo que no venga nada en blanco
    wobjXMLRequest = new diamondedge.util.XmlDom();
    //unsup wobjXMLRequest.async = false;
    wobjXMLRequest.loadXML( pvarRequest );

    wobjXMLCheck = new diamondedge.util.XmlDom();
    //unsup wobjXMLCheck.async = false;
    wobjXMLCheck.load( System.getProperty("user.dir") + "\\" + mcteArchivoVehiculo_XML );
    //
    wvarStep = 20;

    wobjXMLList = null /*unsup wobjXMLCheck.selectNodes( "//ERRORES/VEHICULO/VACIOS/CAMPOS/ERROR" ) */;
    for( wvarContChild = 0; wvarContChild <= (wobjXMLList.getLength() - 1); wvarContChild++ )
    {
      wobjXMLError = wobjXMLList.item( wvarContChild );
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLError.selectSingleNode( "CAMPO" ) *) )) ) */ == (org.w3c.dom.Node) null) )
      {
        wvarStep = 30;
        if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLError.selectSingleNode( "CAMPO" ) *) )) ) */ ) ).equals( "" ) )
        {
          wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "VALORRESPUESTA" ) */ ) );
          wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "TEXTOERROR" ) */ ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      else
      {
        //error
        wvarStep = 40;
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "VALORRESPUESTA" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "TEXTOERROR" ) */ ) );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      wobjXMLError = (org.w3c.dom.Node) null;
    }

    //Chequeo Tipo y Longitud de Datos para el XML Padre
    wvarStep = 50;

    for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().getLength() - 1); wvarContChild++ )
    {
      if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/VEHICULO/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
      {

        //Chequeo la longitud y si tiene Largo Fijo
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/VEHICULO/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
        {
          wvarStep = 60;
          if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/VEHICULO/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") ) */ ) )) )
          {
            wvarStep = 70;
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/VEHICULO/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/VEHICULO/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
        else
        {
          if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/VEHICULO/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") ) */ ) ) )
          {
            wvarStep = 80;
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/VEHICULO/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/VEHICULO/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }

        //Chequeo el Tipo
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/VEHICULO/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
        {
          if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ).isNumeric()) )
          {
            wvarStep = 90;
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/VEHICULO/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/VEHICULO/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
          else
          {
            //Chequeo que el dato numerico no sea 0
            wvarStep = 100;
            if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) == 0 )
            {
              wvarMensaje.set( "El valor del dato numerico no puede estar en 0" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
        }


      }
      Siguiente: 
        ;
    }
    //
    //Valido el a�o del Veh�culo
    mvarDif = DateTime.year( DateTime.now() ) - Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EfectAnn) ) */ ) );

    //If mvarDif > 20 Or mvarDif < 0 Then
    //        wvarMensaje = "Fecha del vehiculo invalida (0-20)"
    //MMC 17-12-2012 Se modifica para permitir veh�culos del a�o 2013.
    if( (mvarDif > 20) || (mvarDif < -1) )
    {
      wvarMensaje.set( "A�o del veh�culo inv�lido." );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }

    wobjXMLCheck = (diamondedge.util.XmlDom) null;
    wobjXMLList = (org.w3c.dom.NodeList) null;
    //
    wvarStep = 110;
    if( invoke( "fncValidaABase", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } ) )
    {
      fncGetAll = true;
    }
    else
    {
      fncGetAll = false;
    }

    fin: 

    return fncGetAll;

    //~~~~~~~~~~~~~~~
    ErrorHandler: 
    //~~~~~~~~~~~~~~~
    pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
    wvarCodErr.set( General.mcteErrorInesperadoCod );
    wvarMensaje.set( General.mcteErrorInesperadoDescr );

    mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

    fncGetAll = false;
    /*unsup mobjCOM_Context.SetComplete() */;
    //unsup GoTo fin
    return fncGetAll;
  }

  private boolean fncValidaABase( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncValidaABase = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    boolean wvarError = false;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLCotizacion = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXMLReturnVal = null;
    diamondedge.util.XmlDom wobjXML = null;
    diamondedge.util.XmlDom wobjXMLError = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarContChild = 0;
    int wvarContNode = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_MARCA = "";
    String mvarWDB_MODELO = "";
    String wvarMensajeStoreProc = "";
    String wvarMensajeNroCot = "";

    //
    //
    //
    //
    //
    //
    try 
    {

      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_REQUESTID = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID ) */ );
      }
      //
      wvarStep = 30;
      mvarWDB_CODINST = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }
      //
      wvarStep = 40;
      mvarWDB_MARCA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Marca) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_MARCA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Marca ) */ );
      }
      //
      wvarStep = 50;
      mvarWDB_MODELO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Modelo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_MODELO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Modelo ) */ );
      }
      //
      //Valido los campos de Solicitud
      //
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProcValidacion );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 130;
      wobjDBParm = new Parameter( "@WDB_CODINST", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 140;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 150;
      wobjDBParm = new Parameter( "@WDB_MARCA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_MARCA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_MARCA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 160;
      wobjDBParm = new Parameter( "@WDB_USUARCOD", AdoConst.adChar, AdoConst.adParamInputOutput, 10, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 230;
      wrstRes = new Recordset();
      wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
      wrstRes.setActiveConnection( (Connection) null );
      //
      wobjXML = new diamondedge.util.XmlDom();
      //unsup wobjXML.async = false;
      wobjXML.load( System.getProperty("user.dir") + "\\" + mcteArchivoVehiculo_XML );
      //
      //SI NO HUBO ERROR CONTINUO
      wvarStep = 240;
      if( ! (wrstRes.isEOF()) )
      {
        if( ! (Strings.toUpperCase( Strings.trim( wrstRes.getFields().getField(0).getValue().toString() ) ).equals( "OK" )) )
        {
          //error
          if( null /*unsup wobjXML.selectNodes( ("//ERRORES/VEHICULO/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']") ) */.getLength() > 0 )
          {
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXML.selectSingleNode( "//ERRORES/VEHICULO/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/TEXTOERROR" ) */ ) );
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXML.selectSingleNode( "//ERRORES/VEHICULO/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/VALORRESPUESTA" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          }
          else
          {
            wvarCodErr.set( wrstRes.getFields().getField(0).getValue() );
            wvarMensaje.set( "Error de validacion en base de datos" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          }
          //
          fncValidaABase = false;
          return fncValidaABase;
        }
        else
        {
        }
      }
      else
      {
        //error
        wvarCodErr.set( -60 );
        wvarMensaje.set( "No se retornaron datos de la base" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncValidaABase;
      }
      //
      wrstRes.close();
      wobjDBCnn.close();

      wobjXML = (diamondedge.util.XmlDom) null;
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      //
      fncValidaABase = true;
      //
      pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + wobjXMLRequest.getDocument().getDocumentElement().toString() + "</LBA_WS>" );
      fin: 
      return fncValidaABase;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );
        wvarMensaje.set( General.mcteErrorInesperadoDescr );

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        fncValidaABase = false;
        /*unsup mobjCOM_Context.SetComplete() */;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncValidaABase;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
