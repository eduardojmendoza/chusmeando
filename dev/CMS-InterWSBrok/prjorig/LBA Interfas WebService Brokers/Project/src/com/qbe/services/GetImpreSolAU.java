package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class GetImpreSolAU implements Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  static final String mcteClassName = "LBA_InterWSBrok.GetImpreSolAU";
  static final String mcteStoreProc = "SPSNCV_BRO_SOLI_AUS1_IMPRE";
  /**
   *  Parametros XML de Entrada
   */
  static final String mcteParam_WDB_IDENTIFICACION_BROKER = "//CODINST";
  static final String mcteParam_WDB_NRO_OPERACION_BROKER = "//REQUESTID";
  static final String mcteParam_WDB_NRO_COTIZACION_LBA = "//COT_NRO";
  static final String mcteParam_WDB_TIPO_OPERACION = "//TIPOOPERACION";
  static final String mcteParam_PRODUCTOR = "//AGECOD";
  static final String mcteParam_NumeDocu = "//NUMEDOCU";
  static final String mcteParam_TipoDocu = "//TIPODOCU";
  static final String mcteParam_ClienApe1 = "//CLIENAP1";
  static final String mcteParam_ClienApe2 = "//CLIENAP2";
  static final String mcteParam_ClienNom1 = "//CLIENNOM";
  static final String mcteParam_NacimAnn = "//NACIMANN";
  static final String mcteParam_NacimMes = "//NACIMMES";
  static final String mcteParam_NacimDia = "//NACIMDIA";
  static final String mcteParam_CLIENSEX = "//SEXO";
  static final String mcteParam_CLIENEST = "//ESTADO";
  static final String mcteParam_EMAIL = "//EMAIL";
  /**
   * Domicilio de riesgo
   */
  static final String mcteParam_DOMICDOM = "//DOMICDOM";
  static final String mcteParam_DomicDnu = "//DOMICDNU";
  static final String mcteParam_DOMICPIS = "//DOMICPIS";
  static final String mcteParam_DomicPta = "//DOMICPTA";
  static final String mcteParam_DOMICPOB = "//DOMICPOB";
  static final String mcteParam_DOMICCPO = "//LOCALIDADCOD";
  static final String mcteParam_PROVICOD = "//PROVI";
  static final String mcteParam_TELCOD = "//TELCOD";
  static final String mcteParam_TELNRO = "//TELNRO";
  /**
   * datos del domicilio de correspondencia
   */
  static final String mcteParam_DOMICDOM_CO = "//DOMICDOMCOR";
  static final String mcteParam_DomicDnu_CO = "//DOMICDNUCOR";
  static final String mcteParam_DOMICPIS_CO = "//DOMICPISCOR";
  static final String mcteParam_DomicPta_CO = "//DOMICPTACOR";
  static final String mcteParam_DOMICPOB_CO = "//DOMICPOBCOR";
  static final String mcteParam_DOMICCPO_CO = "//LOCALIDADCODCOR";
  static final String mcteParam_PROVICOD_CO = "//PROVICOR";
  static final String mcteParam_TELCOD_CO = "//TELCODCOR";
  static final String mcteParam_TELNRO_CO = "//TELNROCOR";
  /**
   * datos de la cuenta
   */
  static final String mcteParam_COBROTIP = "//COBROTIP";
  static final String mcteParam_CUENNUME = "//CUENNUME";
  static final String mcteParam_VENCIANN = "//VENCIANN";
  static final String mcteParam_VENCIMES = "//VENCIMES";
  static final String mcteParam_VENCIDIA = "//VENCIDIA";
  /**
   * datos de la operacion
   */
  static final String mcteParam_FRANQCOD = "//FRANQCOD";
  static final String mcteParam_PLANCOD = "//PLANNCOD";
  static final String mcteParam_ZONA = "//CODZONA";
  static final String mcteParam_CLIENIVA = "//IVA";
  static final String mcteParam_SUMAASEG = "//SUMAASEG";
  static final String mcteParam_CLUB_LBA = "//CLUBLBA";
  static final String mcteParam_CPAANO = "//CPAANO";
  static final String mcteParam_CTAKMS = "//KMSRNGCOD";
  static final String mcteParam_Escero = "//ESCERO";
  static final String mcteParam_SIFMVEHI_DES = "//VEHDES";
  static final String mcteParam_MOTORNUM = "//MOTORNUM";
  static final String mcteParam_CHASINUM = "//CHASINUM";
  static final String mcteParam_PATENNUM = "//PATENNUM";
  /**
   * AUMARCOD+AUMODCOD+AUSUBCOD+AUADICODAUMODORI
   */
  static final String mcteParam_ModeAutCod = "//MODEAUTCOD";
  static final String mcteParam_VEHCLRCOD = "//VEHCLRCOD";
  static final String mcteParam_EfectAnn = "//EFECTANN";
  static final String mcteParam_GUGARAGE = "//SIGARAGE";
  static final String mcteParam_GUDOMICI = "//GUDOMICI";
  static final String mcteParam_AUCATCOD = "//AUCATCOD";
  static final String mcteParam_AUTIPCOD = "//AUTIPCOD";
  static final String mcteParam_AUANTANN = "//AUANTANN";
  static final String mcteParam_AUNUMSIN = "//SINIESTROS";
  static final String mcteParam_AUUSOGNC = "//GAS";
  static final String mcteParam_CampaCod = "//CAMPACOD";
  static final String mcteParam_CONDUCTORES = "//HIJOS/HIJO";
  static final String mcteParam_CONDUAPE = "APELLIDOHIJO";
  static final String mcteParam_CONDUNOM = "NOMBREHIJO";
  static final String mcteParam_CONDUFEC = "NACIMHIJO";
  static final String mcteParam_CONDUSEX = "SEXOHIJO";
  static final String mcteParam_CONDUEST = "ESTADOHIJO";
  static final String mcteParam_CONDUEXC = "INCLUIDO";
  static final String mcteParam_ACCESORIOS = "//ACCESORIOS/ACCESORIO";
  static final String mcteParam_AUACCCOD = "CODIGOACC";
  static final String mcteParam_AUVEASUM = "PRECIOACC";
  static final String mcteParam_AUVEADES = "DESCRIPCIONACC";
  static final String mcteParam_INSPECOD = "//INSPECOD";
  static final String mcteParam_RASTREO = "//RASTREO";
  static final String mcteParam_ALARMCOD = "//ALARMCOD";
  static final String mcteParam_COBROCOD = "//COBROCOD";
  static final String mcteParam_ANOTACIO = "//ANOTACIONES";
  static final String mcteParam_VIGENANN = "//VIGENANN";
  static final String mcteParam_VIGENMES = "//VIGENMES";
  static final String mcteParam_VIGENDIA = "//VIGENDIA";
  /**
   * hora en que empezo la transaccion
   */
  static final String mcteParam_MSGANO = "//MSGANO";
  static final String mcteParam_MSGMES = "//MSGMES";
  static final String mcteParam_MSGDIA = "//MSGDIA";
  static final String mcteParam_MSGHORA = "//MSGHORA";
  static final String mcteParam_MSGMINUTO = "//MSGMINUTO";
  static final String mcteParam_MSGSEGUNDO = "//MSGSEGUNDO";
  /**
   * Datos de la cotizacion
   */
  static final String mcteParm_TIEMPO = "//TIEMPO";
  static final String mcteParam_DESTRUCCION_80 = "//DESTRUCCION_80";
  static final String mcteParam_Luneta = "//LUNETA";
  static final String mcteParam_ClubEco = "//CLUBECO";
  static final String mcteParam_Robocont = "//ROBOCONT";
  static final String mcteParam_Granizo = "//GRANIZO";
  static final String mcteParam_IBB = "//IBB";
  static final String mcteParam_NROIBB = "//NROIBB";
  static final String mcteParam_INSTALADP = "//INSTALADP";
  static final String mcteParam_POSEEDISP = "//POSEEDISP";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_PRECIO = "//PRECIO";
  /**
   *  11/2010 se permiten distintos tipos de iva
   */
  static final String mcteParam_CUITNUME = "//CUITNUME";
  static final String mcteParam_RAZONSOC = "//RAZONSOC";
  static final String mcteParam_CLIENTIP = "//CLIENTIP";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   */
  private final String wcteFnName = "fncGetAll";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarMensaje = "";
    Variant pvarRes = new Variant();
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      pvarRes.set( "" );
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        wvarStep = 20;
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }
      wvarStep = 30;
      pvarResponse.set( pvarRes );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"-2000\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( String pvarRequest, String wvarMensaje, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarMensaje2 = "";
    String wvarMensaje3 = "";
    LBAA_MWGenerico.lbaw_MQMW wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
    Object wobjHSBC_DBCnn = null;
    diamondedge.util.XmlDom wobjXMLRequestSolis = null;
    diamondedge.util.XmlDom wobjXMLError = null;
    diamondedge.util.XmlDom mobjXMLDoc = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstDBResult = null;
    String mvarAUX = "";
    String mvarAUMARCOD = "";
    String mvarNUMECOLO = "";
    String mvarESTADCOD = "";
    String mvarPLANNCOD = "";
    String mvarFRANQCOD = "";
    String mvarDOCUMTIP = "";
    String mvarCOBROTIP = "";
    String mvarAGENTCOD = "";
    String mvarCAMPACOD = "";
    String mvarMARCA = "";
    String mvarCOLOR = "";
    String mvarINSPEDES = "";
    String mvarPLANNDES = "";
    String mvarFRANQDES = "";
    String mvarDOCUDES = "";
    String mvarCOBRODES = "";
    String mvarCAMPADES = "";
    String mvarAGENTDES = "";
    String mvarDatosImpreso = "";
    String mvarNodoImpreso = "";
    String mvarImpreso64 = "";
    org.w3c.dom.Node oModelo = null;
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarOPT_MODELOS = "";
    String mvarOrigen = "";
    String mvarGas = "";
    String mvarCero = "";
    String mvarRastreo = "";
    String mvarZONA = "";
    String mvarZonaDes = "";
    String mvarGarage = "";
    String mvarStros = "";
    String mvarSEXO = "";
    String mvarEC = "";
    String mvarNUMHIJOS = "";
    String mvarHEC = "";
    int wvariCounter = 0;
    org.w3c.dom.NodeList wobjXMLList = null;
    String mvarOPT_IVA = "";
    String mvarCLIENTIP = "";
    String mvarOPT_IBB = "";
    String mvarCUENNUME = "";
    String mvarRAZONSOC = "";
    String mvarCUITNUME = "";
    String mvarseltest = "";
    int wvarCount = 0;






    //11/2010 se agregan distintos IVA
    try 
    {

      wvarStep = 10;
      wobjXMLRequestSolis = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequestSolis.async = false;
      wobjXMLRequestSolis.loadXML( pvarRequest );

      //ejecuta la sp para rescatar literales
      mvarAUX = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_ModeAutCod ) */ );
      mvarAUMARCOD = Strings.left( mvarAUX, 5 );
      mvarNUMECOLO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_VEHCLRCOD ) */ );
      mvarESTADCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_INSPECOD ) */ );
      mvarPLANNCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_PLANCOD ) */ );
      mvarFRANQCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_FRANQCOD ) */ );
      mvarDOCUMTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_TipoDocu ) */ );
      mvarCOBROTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_COBROTIP ) */ );
      mvarAGENTCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_PRODUCTOR ) */ );
      mvarCAMPACOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CampaCod ) */ );

      wvarStep = 100;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 110;
      wobjDBParm = new Parameter( "@AUMARCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUMARCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 120;
      wobjDBParm = new Parameter( "@NUMECOLO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNUMECOLO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 130;
      wobjDBParm = new Parameter( "@ESTADCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarESTADCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 140;
      wobjDBParm = new Parameter( "@PLANNCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPLANNCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 150;
      wobjDBParm = new Parameter( "@FRANQCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarFRANQCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 160;
      wobjDBParm = new Parameter( "@DOCUMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarDOCUMTIP ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 170;
      wobjDBParm = new Parameter( "@COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarCOBROTIP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 180;
      wobjDBParm = new Parameter( "@AGENTCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarAGENTCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 190;
      wobjDBParm = new Parameter( "@CAMPACOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarCAMPACOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 200;
      mvarseltest = mcteStoreProc + " ";
      for( wvarCount = 0; wvarCount <= (wobjDBCmd.getParameters().getCount() - 1); wvarCount++ )
      {
        if( (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adChar) || (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adVarChar) )
        {
          mvarseltest = mvarseltest + "'" + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + "',";
        }
        else
        {
          mvarseltest = mvarseltest + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + ",";
        }
      }

      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );

      wvarStep = 210;
      if( ! (wrstDBResult.isEOF()) )
      {
        mvarMARCA = wrstDBResult.getFields().getField("MARCA").getValue().toString();
        mvarCOLOR = wrstDBResult.getFields().getField("COLOR").getValue().toString();
        mvarINSPEDES = wrstDBResult.getFields().getField("INSPEDES").getValue().toString();
        mvarPLANNDES = wrstDBResult.getFields().getField("PLANNDES").getValue().toString();
        mvarFRANQDES = wrstDBResult.getFields().getField("FRANQDES").getValue().toString();
        mvarDOCUDES = wrstDBResult.getFields().getField("DOCUDES").getValue().toString();
        mvarCOBRODES = wrstDBResult.getFields().getField("COBRODES").getValue().toString();
        //12/2010 se obtiene x componente
        //  mvarCAMPADES = wrstDBResult.Fields.Item("CAMPADES").Value
        //  mvarAGENTDES = wrstDBResult.Fields.Item("AGENTDES").Value
      }
      else
      {
        mvarMARCA = "";
        mvarCOLOR = "";
        mvarINSPEDES = "";
        mvarPLANNDES = "";
        mvarFRANQDES = "";
        mvarDOCUDES = "";
        mvarCOBRODES = "";
        mvarCAMPADES = "";
        mvarAGENTDES = "";
      }

      wvarStep = 220;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResult.close();
        }
      }

      wvarStep = 230;
      wrstDBResult = (Recordset) null;

      wobjDBCnn.close();

      //busca modelo
      wvarStep = 240;
      mvarRequest = "<Request><AUMARCOD>" + mvarAUMARCOD + "</AUMARCOD><AUVEHDES></AUVEHDES></Request>";
      wobjClass = new lbawA_OVLBAMQ.lbaw_GetVehiculos();
      wobjClass.Execute( new Variant( mvarRequest ), new Variant( mvarResponse ), "" );
      wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );


      if( null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.getLength() > 0 )
      {
        for( int noModelo = 0; noModelo < null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.getLength(); noModelo++ )
        {
          oModelo = null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.item( noModelo );
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_ModeAutCod ) */ ).equals( diamondedge.util.XmlDom.getText( oModelo.getAttributes().getNamedItem( "value" ) ) ) )
          {
            mvarOPT_MODELOS = diamondedge.util.XmlDom.getText( oModelo );
          }
        }
      }
      else
      {
        mvarOPT_MODELOS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_SIFMVEHI_DES ) */ );
      }

      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      //11/2010 se agregan distintos tipos de IVA
      wvarStep = 241;
      mvarRequest = "<Request></Request>";
      wobjClass = new lbawA_OfVirtualLBA.lbaw_GetTiposIVA();
      wobjClass.Execute( mvarRequest, mvarResponse, "" );
      wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );

      mvarOPT_IVA = "";
      if( null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.getLength() > 0 )
      {
        for( int noModelo = 0; noModelo < null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.getLength(); noModelo++ )
        {
          oModelo = null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.item( noModelo );
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CLIENIVA ) */ ).equals( diamondedge.util.XmlDom.getText( oModelo.getAttributes().getNamedItem( "value" ) ) ) )
          {
            mvarOPT_IVA = diamondedge.util.XmlDom.getText( oModelo );
          }
        }
      }

      mobjXMLDoc = (diamondedge.util.XmlDom) null;

      mvarCLIENTIP = "F";
      if( ! (null /*unsup wobjXMLRequestSolis.selectNodes( mcteParam_CLIENTIP ) */ == (org.w3c.dom.NodeList) null) )
      {
        if( null /*unsup wobjXMLRequestSolis.selectNodes( mcteParam_CLIENTIP ) */.getLength() == 0 )
        {
          mvarCLIENTIP = "F";
        }
        else
        {
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CLIENTIP ) */ ).equals( "15" ) )
          {
            mvarCLIENTIP = "J";
          }
          else
          {
            mvarCLIENTIP = "F";
          }
        }
      }

      wvarStep = 242;
      mvarOPT_IBB = "";
      if( !diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_IBB ) */ ).equals( "0" ) )
      {
        mvarRequest = "<Request><NUMERO>0</NUMERO></Request>";
        wobjClass = new lbawA_OfVirtualLBA.lbaw_GetTipIngBrut();
        wobjClass.Execute( new Variant( mvarRequest ), new Variant( mvarResponse ), "" );
        wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
        mobjXMLDoc = new diamondedge.util.XmlDom();
        //unsup mobjXMLDoc.async = false;
        mobjXMLDoc.loadXML( mvarResponse );

        if( null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.getLength() > 0 )
        {
          for( int noModelo = 0; noModelo < null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.getLength(); noModelo++ )
          {
            oModelo = null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.item( noModelo );
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_IBB ) */ ).equals( diamondedge.util.XmlDom.getText( oModelo.getAttributes().getNamedItem( "value" ) ) ) )
            {
              mvarOPT_IBB = diamondedge.util.XmlDom.getText( oModelo );
            }
          }
        }
      }

      mobjXMLDoc = (diamondedge.util.XmlDom) null;

      //11/2010 fin iva
      //12/2010 productor y campa�a
      //Nombre productor
      wvarStep = 243;
      mvarRequest = "<Request><DEFINICION>GetNombreProductor.xml</DEFINICION>";
      mvarRequest = mvarRequest + "<USUARCOD></USUARCOD><AGENTCLA>PR</AGENTCLA>";
      mvarRequest = mvarRequest + "<AGENTCOD>" + mvarAGENTCOD + "</AGENTCOD></Request>";

      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( mvarRequest, mvarResponse, "" );
      wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );

      if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {
          mvarAGENTDES = diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//PRODUCTOR/NOMBRE" ) */ );
        }
        else
        {
          mvarAGENTDES = "";
        }
      }
      else
      {
        mvarAGENTDES = "";
      }

      mobjXMLDoc = (diamondedge.util.XmlDom) null;

      //nombre campania
      wvarStep = 244;
      mvarRequest = "<Request><DEFINICION>CampanasHabilitadasProductor.xml</DEFINICION>";
      //no hace falta el valor del usuarcod, solo informar el campo
      mvarRequest = mvarRequest + "<USUARCOD>AUS1</USUARCOD>";
      mvarRequest = mvarRequest + "<RAMOPCOD>AUS1</RAMOPCOD><AGENTCLA>PR</AGENTCLA>";
      mvarRequest = mvarRequest + "<AGENTCOD>" + mvarAGENTCOD + "</AGENTCOD></Request>";

      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( mvarRequest, mvarResponse, "" );
      wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );

      if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {

          wobjXMLList = null /*unsup mobjXMLDoc.selectNodes( "//CAMPOS/T-CAMPANAS-SAL/CAMPANA" ) */;
          for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
          {
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( "CAMPACOD" ) */ ).equals( mvarCAMPACOD ) )
            {
              mvarCAMPADES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( "CAMPADES" ) */ );
            }
          }
        }
        else
        {
          mvarCAMPADES = "";
        }
      }
      else
      {
        mvarCAMPADES = "";
      }

      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      //fin
      //obtiene otros literales
      wvarStep = 250;
      mvarAUX = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_ModeAutCod ) */ );
      mvarAUX = Strings.mid( mvarAUX, 21, 1 );
      if( mvarAUX.equals( "N" ) )
      {
        mvarOrigen = "NACIONAL";
      }
      else
      {
        mvarOrigen = "IMPORTADO";
      }

      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_AUUSOGNC ) */ ) ).equals( "S" ) )
      {
        mvarGas = "SI";
      }
      else
      {
        mvarGas = "NO";
      }

      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_Escero ) */ ) ).equals( "S" ) )
      {
        mvarCero = "SI";
      }
      else
      {
        mvarCero = "NO";
      }

      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_INSTALADP ) */ ) ).equals( "S" ) )
      {
        mvarRastreo = "SOLICITA";
      }
      else
      {
        if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_POSEEDISP ) */ ) ).equals( "S" ) )
        {
          mvarRastreo = "YA POSEE";
        }
        else
        {
          mvarRastreo = "";
        }
      }

      wvarStep = 260;
      mvarRequest = "<Request><RAMOPCOD>AUS1</RAMOPCOD><PROVICOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_PROVICOD ) */ ) + "</PROVICOD><CPACODPO>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_DOMICCPO ) */ ) + "</CPACODPO></Request>";
      wobjClass = new lbawA_OVLBAMQ.lbaw_GetZona();
      wobjClass.Execute( new Variant( mvarRequest ), new Variant( mvarResponse ), "" );
      wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );
      if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
      {
        mvarZONA = diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//CODIZONA" ) */ );
        mvarZonaDes = diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//ZONASDES" ) */ );
      }
      else
      {
        mvarZONA = "";
        mvarZonaDes = "";
      }
      mobjXMLDoc = (diamondedge.util.XmlDom) null;

      wvarStep = 270;
      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_GUGARAGE ) */ ) ).equals( "S" ) )
      {
        mvarGarage = "SI";
      }
      else
      {
        mvarGarage = "NO";
      }

      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_AUNUMSIN ) */ ).equals( "0" ) )
      {
        mvarStros = "NINGUNO";
      }
      else
      {
        mvarStros = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_AUNUMSIN ) */ );
      }

      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CLIENSEX ) */ ) ).equals( "M" ) )
      {
        mvarSEXO = "MASCULINO";
      }
      else
      {
        mvarSEXO = "FEMENINO";
      }

      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CLIENEST ) */ ) ).equals( "S" ) )
      {
        mvarEC = "SOLTERO/A";
      }
      else
      {
        if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CLIENEST ) */ ) ).equals( "C" ) )
        {
          mvarEC = "CASADO/A";
        }
        else
        {
          if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CLIENEST ) */ ) ).equals( "E" ) )
          {
            mvarEC = "SEPARADO/A";
          }
          else
          {
            if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CLIENEST ) */ ) ).equals( "V" ) )
            {
              mvarEC = "VIUDO/A";
            }
            else
            {
              if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CLIENEST ) */ ) ).equals( "D" ) )
              {
                mvarEC = "DIVORCIADO/A";
              }
              else
              {
                mvarEC = "";
              }
            }
          }
        }
      }

      //conforma xml para la impresi�n
      wvarStep = 300;
      mvarDatosImpreso = "<Request>";
      mvarDatosImpreso = mvarDatosImpreso + "<PROCEANN>" + DateTime.year( DateTime.now() ) + "</PROCEANN>";
      mvarDatosImpreso = mvarDatosImpreso + "<PROCEMES>" + DateTime.month( DateTime.now() ) + "</PROCEMES>";
      mvarDatosImpreso = mvarDatosImpreso + "<PROCEDIA>" + DateTime.day( DateTime.now() ) + "</PROCEDIA>";
      mvarDatosImpreso = mvarDatosImpreso + "<POLIZANN>00</POLIZANN>";
      mvarDatosImpreso = mvarDatosImpreso + "<POLIZSEC>1</POLIZSEC>";
      mvarDatosImpreso = mvarDatosImpreso + "<CERTIPOL>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_WDB_IDENTIFICACION_BROKER ) */ ) + "</CERTIPOL>";
      wvarStep = 301;
      mvarDatosImpreso = mvarDatosImpreso + "<CERTIANN>0000</CERTIANN>";
      wvarStep = 302;
      mvarDatosImpreso = mvarDatosImpreso + "<CERTISEC>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CERTISEC ) */ ) + "</CERTISEC>";
      wvarStep = 303;
      mvarDatosImpreso = mvarDatosImpreso + "<VIGENANN>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_VIGENANN ) */ ) + "</VIGENANN>";
      wvarStep = 304;
      mvarDatosImpreso = mvarDatosImpreso + "<VIGENMES>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_VIGENMES ) */ ) + "</VIGENMES>";
      wvarStep = 305;
      mvarDatosImpreso = mvarDatosImpreso + "<VIGENDIA>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_VIGENDIA ) */ ) + "</VIGENDIA>";
      wvarStep = 306;
      mvarDatosImpreso = mvarDatosImpreso + "<CLIENNOM>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_ClienNom1 ) */ ) + "</CLIENNOM>";
      wvarStep = 307;
      mvarDatosImpreso = mvarDatosImpreso + "<CLIENAP1>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_ClienApe1 ) */ ) + "</CLIENAP1>";
      wvarStep = 308;
      mvarDatosImpreso = mvarDatosImpreso + "<CLIENAP2>" + ((null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_ClienApe2 ) */ == (org.w3c.dom.Node) null) ? "" : diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_ClienApe2 ) */ )) + "</CLIENAP2>";
      wvarStep = 309;
      mvarDatosImpreso = mvarDatosImpreso + "<DOMICDOMCOR>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_DOMICDOM_CO ) */ ) + "</DOMICDOMCOR>";
      wvarStep = 310;
      mvarDatosImpreso = mvarDatosImpreso + "<DOMICDNUCOR>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_DomicDnu_CO ) */ ) + "</DOMICDNUCOR>";
      wvarStep = 311;
      mvarDatosImpreso = mvarDatosImpreso + "<DOMICPISCOR>" + ((null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_DOMICPIS_CO ) */ == (org.w3c.dom.Node) null) ? "" : diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_DOMICPIS_CO ) */ )) + "</DOMICPISCOR>";
      wvarStep = 312;
      mvarDatosImpreso = mvarDatosImpreso + "<DOMICPTACOR>" + ((null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_DomicPta_CO ) */ == (org.w3c.dom.Node) null) ? "" : diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_DomicPta_CO ) */ )) + "</DOMICPTACOR>";
      wvarStep = 313;
      mvarDatosImpreso = mvarDatosImpreso + "<DOMICPOBCOR>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_DOMICPOB_CO ) */ ) + "</DOMICPOBCOR>";
      wvarStep = 314;
      mvarDatosImpreso = mvarDatosImpreso + "<LOCALIDADCODCOR>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_DOMICCPO_CO ) */ ) + "</LOCALIDADCODCOR>";
      wvarStep = 315;
      mvarDatosImpreso = mvarDatosImpreso + "<TELCODCOR>" + ((null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_TELCOD_CO ) */ == (org.w3c.dom.Node) null) ? "" : diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_TELCOD_CO ) */ )) + "</TELCODCOR>";
      wvarStep = 316;
      mvarDatosImpreso = mvarDatosImpreso + "<TELNROCOR>" + ((null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_TELNRO_CO ) */ == (org.w3c.dom.Node) null) ? "" : diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_TELNRO_CO ) */ )) + "</TELNROCOR>";
      wvarStep = 317;
      mvarDatosImpreso = mvarDatosImpreso + "<EMAIL>" + ((null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_EMAIL ) */ == (org.w3c.dom.Node) null) ? "" : diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_EMAIL ) */ )) + "</EMAIL>";
      wvarStep = 318;
      mvarDatosImpreso = mvarDatosImpreso + "<MARCA>" + mvarMARCA + "</MARCA>";
      wvarStep = 319;
      mvarDatosImpreso = mvarDatosImpreso + "<VEHDES>" + mvarOPT_MODELOS + "</VEHDES>";
      wvarStep = 320;
      mvarDatosImpreso = mvarDatosImpreso + "<PATENNUM>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_PATENNUM ) */ ) + "</PATENNUM>";
      wvarStep = 321;
      mvarDatosImpreso = mvarDatosImpreso + "<MOTORNUM>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_MOTORNUM ) */ ) + "</MOTORNUM>";
      wvarStep = 322;
      mvarDatosImpreso = mvarDatosImpreso + "<CHASINUM>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CHASINUM ) */ ) + "</CHASINUM>";
      wvarStep = 323;
      mvarDatosImpreso = mvarDatosImpreso + "<EFECTANN>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_EfectAnn ) */ ) + "</EFECTANN>";
      wvarStep = 324;
      mvarDatosImpreso = mvarDatosImpreso + "<SUMASEG>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_SUMAASEG ) */ ) + "</SUMASEG>";
      wvarStep = 325;
      mvarDatosImpreso = mvarDatosImpreso + "<VEHCLRCOD>" + mvarCOLOR + "</VEHCLRCOD>";
      mvarDatosImpreso = mvarDatosImpreso + "<VEHORIGEN>" + mvarOrigen + "</VEHORIGEN>";
      mvarDatosImpreso = mvarDatosImpreso + "<GAS>" + mvarGas + "</GAS>";
      mvarDatosImpreso = mvarDatosImpreso + "<ESCERO>" + mvarCero + "</ESCERO>";
      mvarDatosImpreso = mvarDatosImpreso + "<INSPEDES>" + mvarINSPEDES + "</INSPEDES>";
      mvarDatosImpreso = mvarDatosImpreso + "<RASTREO>" + mvarRastreo + "</RASTREO>";
      mvarDatosImpreso = mvarDatosImpreso + "<PLANNCOD>" + mvarPLANNCOD + "</PLANNCOD>";
      mvarDatosImpreso = mvarDatosImpreso + "<FRANQCOD>" + mvarFRANQCOD + "</FRANQCOD>";
      mvarDatosImpreso = mvarDatosImpreso + "<PLANNDES>" + mvarPLANNDES + "</PLANNDES>";
      mvarDatosImpreso = mvarDatosImpreso + "<FRANQDES>" + mvarFRANQDES + "</FRANQDES>";
      mvarDatosImpreso = mvarDatosImpreso + "<ZONASDES>" + mvarZonaDes + "</ZONASDES>";
      mvarDatosImpreso = mvarDatosImpreso + "<ZONATARI>" + mvarZONA + "</ZONATARI>";
      wvarStep = 326;
      mvarDatosImpreso = mvarDatosImpreso + "<KMSRNGCOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CTAKMS ) */ ) + "</KMSRNGCOD>";
      wvarStep = 327;
      mvarDatosImpreso = mvarDatosImpreso + "<CPAANO>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CPAANO ) */ ) + "</CPAANO>";
      mvarDatosImpreso = mvarDatosImpreso + "<SIGARAGE>" + mvarGarage + "</SIGARAGE>";
      mvarDatosImpreso = mvarDatosImpreso + "<SINIESTROS>" + mvarStros + "</SINIESTROS>";
      wvarStep = 328;
      mvarDatosImpreso = mvarDatosImpreso + "<NACIMANN>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_NacimAnn ) */ ) + "</NACIMANN>";
      wvarStep = 329;
      mvarDatosImpreso = mvarDatosImpreso + "<NACIMMES>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_NacimMes ) */ ) + "</NACIMMES>";
      wvarStep = 330;
      mvarDatosImpreso = mvarDatosImpreso + "<NACIMDIA>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_NacimDia ) */ ) + "</NACIMDIA>";
      mvarDatosImpreso = mvarDatosImpreso + "<TIPODOCU>" + mvarDOCUDES + "</TIPODOCU>";
      wvarStep = 331;
      mvarDatosImpreso = mvarDatosImpreso + "<NUMEDOCU>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_NumeDocu ) */ ) + "</NUMEDOCU>";
      wvarStep = 332;
      mvarDatosImpreso = mvarDatosImpreso + "<EDAD>" + DateTime.diff( "YYYY", DateTime.dateSerial( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_NacimAnn ) */ ) ), Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_NacimMes ) */ ) ), Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_NacimDia ) */ ) ) ), DateTime.now() ) + "</EDAD>";
      mvarDatosImpreso = mvarDatosImpreso + "<SEXO>" + mvarSEXO + "</SEXO>";
      mvarDatosImpreso = mvarDatosImpreso + "<ESTADO>" + mvarEC + "</ESTADO>";
      wvarStep = 333;
      mvarDatosImpreso = mvarDatosImpreso + "<AUANTANN>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_AUANTANN ) */ ) + "</AUANTANN>";
      mvarDatosImpreso = mvarDatosImpreso + "<HIJOS>";

      //ciclo de hijos
      wvarStep = 334;
      if( ! (null /*unsup wobjXMLRequestSolis.selectNodes( mcteParam_CONDUCTORES ) */ == (org.w3c.dom.NodeList) null) )
      {
        wvarStep = 335;
        if( null /*unsup wobjXMLRequestSolis.selectNodes( mcteParam_CONDUCTORES ) */.getLength() > 0 )
        {
          mvarNUMHIJOS = String.valueOf( null /*unsup wobjXMLRequestSolis.selectNodes( mcteParam_CONDUCTORES ) */.getLength() );
          wobjXMLList = null /*unsup wobjXMLRequestSolis.selectNodes( mcteParam_CONDUCTORES ) */;
          wvarStep = 336;
          for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
          {
            mvarDatosImpreso = mvarDatosImpreso + "<HIJO>";
            mvarDatosImpreso = mvarDatosImpreso + "<CANTHIJOS>" + mvarNUMHIJOS + "</CANTHIJOS>";
            mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJOAA>" + Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CONDUFEC ) */ ), 5, 4 ) + "</NACIMHIJOAA>";
            mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJOMM>" + Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CONDUFEC ) */ ), 3, 2 ) + "</NACIMHIJOMM>";
            mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJODD>" + Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CONDUFEC ) */ ), 1, 2 ) + "</NACIMHIJODD>";
            wvarStep = 337;
            if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CONDUEST ) */ ) ).equals( "S" ) )
            {
              mvarHEC = "SOLTERO/A";
            }
            else
            {
              wvarStep = 338;
              if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CONDUEST ) */ ) ).equals( "C" ) )
              {
                mvarHEC = "CASADO/A";
              }
              else
              {
                wvarStep = 339;
                if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CONDUEST ) */ ) ).equals( "E" ) )
                {
                  mvarHEC = "SEPARADO/A";
                }
                else
                {
                  wvarStep = 340;
                  if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CONDUEST ) */ ) ).equals( "V" ) )
                  {
                    mvarHEC = "VIUDO/A";
                  }
                  else
                  {
                    wvarStep = 341;
                    if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CONDUEST ) */ ) ).equals( "D" ) )
                    {
                      mvarHEC = "DIVORCIADO/A";
                    }
                    else
                    {
                      mvarHEC = "";
                    }
                  }
                }
              }
            }
            wvarStep = 342;
            mvarDatosImpreso = mvarDatosImpreso + "<ESTADOHIJO>" + mvarHEC + "</ESTADOHIJO>";
            wvarStep = 343;
            mvarDatosImpreso = mvarDatosImpreso + "<APELLIDOHIJO>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CONDUAPE ) */ ) + "</APELLIDOHIJO>";
            wvarStep = 344;
            mvarDatosImpreso = mvarDatosImpreso + "<NOMBREHIJO>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CONDUNOM ) */ ) + "</NOMBREHIJO>";

            mvarDatosImpreso = mvarDatosImpreso + "</HIJO>";
          }
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<HIJO>";
          mvarDatosImpreso = mvarDatosImpreso + "<CANTHIJOS>0</CANTHIJOS>";
          mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJOAA></NACIMHIJOAA>";
          mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJOMM></NACIMHIJOMM>";
          mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJODD></NACIMHIJODD>";
          mvarDatosImpreso = mvarDatosImpreso + "<ESTADOHIJO></ESTADOHIJO>";
          mvarDatosImpreso = mvarDatosImpreso + "<APELLIDOHIJO></APELLIDOHIJO>";
          mvarDatosImpreso = mvarDatosImpreso + "<NOMBREHIJO></NOMBREHIJO>";
          mvarDatosImpreso = mvarDatosImpreso + "</HIJO>";
        }
      }
      else
      {
        mvarDatosImpreso = mvarDatosImpreso + "<HIJO>";
        mvarDatosImpreso = mvarDatosImpreso + "<CANTHIJOS>0</CANTHIJOS>";
        mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJOAA></NACIMHIJOAA>";
        mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJOMM></NACIMHIJOMM>";
        mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJODD></NACIMHIJODD>";
        mvarDatosImpreso = mvarDatosImpreso + "<ESTADOHIJO></ESTADOHIJO>";
        mvarDatosImpreso = mvarDatosImpreso + "<APELLIDOHIJO></APELLIDOHIJO>";
        mvarDatosImpreso = mvarDatosImpreso + "<NOMBREHIJO></NOMBREHIJO>";
        mvarDatosImpreso = mvarDatosImpreso + "</HIJO>";
      }
      mvarDatosImpreso = mvarDatosImpreso + "</HIJOS>";

      //ciclo de accesorios
      mvarDatosImpreso = mvarDatosImpreso + "<ACCESORIOS>";
      wvarStep = 345;
      if( ! (null /*unsup wobjXMLRequestSolis.selectNodes( mcteParam_ACCESORIOS ) */ == (org.w3c.dom.NodeList) null) )
      {
        if( null /*unsup wobjXMLRequestSolis.selectNodes( mcteParam_ACCESORIOS ) */.getLength() > 0 )
        {
          wobjXMLList = null /*unsup wobjXMLRequestSolis.selectNodes( mcteParam_ACCESORIOS ) */;
          wvarStep = 346;
          for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
          {
            mvarDatosImpreso = mvarDatosImpreso + "<ACCESORIO>";
            wvarStep = 347;
            mvarDatosImpreso = mvarDatosImpreso + "<PRECIOACC>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_AUVEASUM ) */ ) + "</PRECIOACC>";
            wvarStep = 348;
            mvarDatosImpreso = mvarDatosImpreso + "<CODIGOACC>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_AUACCCOD ) */ ) + "</CODIGOACC>";
            wvarStep = 349;
            mvarDatosImpreso = mvarDatosImpreso + "<DESCRIPCIONACC>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_AUVEADES ) */ ) + "</DESCRIPCIONACC>";
            mvarDatosImpreso = mvarDatosImpreso + "</ACCESORIO>";
          }
          mvarDatosImpreso = mvarDatosImpreso + "</ACCESORIOS>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<ACCESORIO>";
          mvarDatosImpreso = mvarDatosImpreso + "<PRECIOACC></PRECIOACC>";
          mvarDatosImpreso = mvarDatosImpreso + "<CODIGOACC></CODIGOACC>";
          mvarDatosImpreso = mvarDatosImpreso + "<DESCRIPCIONACC></DESCRIPCIONACC>";
          mvarDatosImpreso = mvarDatosImpreso + "</ACCESORIO>";
          mvarDatosImpreso = mvarDatosImpreso + "</ACCESORIOS>";
        }
      }
      else
      {
        mvarDatosImpreso = mvarDatosImpreso + "<ACCESORIO>";
        mvarDatosImpreso = mvarDatosImpreso + "<PRECIOACC></PRECIOACC>";
        mvarDatosImpreso = mvarDatosImpreso + "<CODIGOACC></CODIGOACC>";
        mvarDatosImpreso = mvarDatosImpreso + "<DESCRIPCIONACC></DESCRIPCIONACC>";
        mvarDatosImpreso = mvarDatosImpreso + "</ACCESORIO>";
        mvarDatosImpreso = mvarDatosImpreso + "</ACCESORIOS>";
      }

      //fin accesorios
      wvarStep = 350;
      mvarDatosImpreso = mvarDatosImpreso + "<PRECIO>" + ((null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_PRECIO ) */ == (org.w3c.dom.Node) null) ? "" : diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_PRECIO ) */ )) + "</PRECIO>";
      wvarStep = 351;
      if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_COBROCOD ) */ ) ) == 4 )
      {
        mvarDatosImpreso = mvarDatosImpreso + "<COBROFOR>TARJETA " + mvarCOBRODES + "</COBROFOR>";
      }
      else
      {
        mvarDatosImpreso = mvarDatosImpreso + "<COBROFOR>" + mvarCOBRODES + "</COBROFOR>";
      }

      //  mvarDatosImpreso = mvarDatosImpreso & "<CUENNUME>" & wobjXMLRequestSolis.selectSingleNode(mcteParam_CUENNUME).Text & "</CUENNUME>"
      //cbu
      wvarStep = 352;
      if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_COBROCOD ) */ ) ) == 5 )
      {
        if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_COBROTIP ) */ ) ).equals( "DB" ) )
        {
          wvarStep = 353;
          mvarDatosImpreso = mvarDatosImpreso + "<CUENNUME>" + ((null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CUENNUME ) */ == (org.w3c.dom.Node) null) ? "" : diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CUENNUME ) */ )) + ((null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_VENCIMES ) */ == (org.w3c.dom.Node) null) ? "" : diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_VENCIMES ) */ )) + ((null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_VENCIANN ) */ == (org.w3c.dom.Node) null) ? "" : diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_VENCIANN ) */ )) + "</CUENNUME>";
        }
        else
        {
          wvarStep = 354;
          mvarDatosImpreso = mvarDatosImpreso + "<CUENNUME>" + ((null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CUENNUME ) */ == (org.w3c.dom.Node) null) ? "" : diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CUENNUME ) */ )) + "</CUENNUME>";
        }
      }
      else
      {
        wvarStep = 355;
        mvarCUENNUME = "";
        if( ! (null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CUENNUME ) */ == (org.w3c.dom.Node) null) )
        {
          mvarCUENNUME = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CUENNUME ) */ );
        }
        mvarDatosImpreso = mvarDatosImpreso + "<CUENNUME>" + mvarCUENNUME + "</CUENNUME>";
      }
      wvarStep = 356;
      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CLUB_LBA ) */ ) ).equals( "S" ) )
      {
        mvarDatosImpreso = mvarDatosImpreso + "<CLUBLBA>SI</CLUBLBA>";
      }
      else
      {
        mvarDatosImpreso = mvarDatosImpreso + "<CLUBLBA>NO</CLUBLBA>";
      }
      wvarStep = 357;
      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_DESTRUCCION_80 ) */ ) ).equals( "S" ) )
      {
        mvarDatosImpreso = mvarDatosImpreso + "<DESTRUCCION_80>SI</DESTRUCCION_80>";
      }
      else
      {
        mvarDatosImpreso = mvarDatosImpreso + "<DESTRUCCION_80>NO</DESTRUCCION_80>";
      }
      wvarStep = 358;
      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_Luneta ) */ ) ).equals( "S" ) )
      {
        mvarDatosImpreso = mvarDatosImpreso + "<LUNETA>SI</LUNETA>";
      }
      else
      {
        mvarDatosImpreso = mvarDatosImpreso + "<LUNETA>NO</LUNETA>";
      }
      wvarStep = 359;
      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_ClubEco ) */ ) ).equals( "S" ) )
      {
        mvarDatosImpreso = mvarDatosImpreso + "<CLUBECO>SI</CLUBECO>";
      }
      else
      {
        mvarDatosImpreso = mvarDatosImpreso + "<CLUBECO>NO</CLUBECO>";
      }
      wvarStep = 360;
      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_Robocont ) */ ) ).equals( "S" ) )
      {
        mvarDatosImpreso = mvarDatosImpreso + "<ROBOCONT>SI</ROBOCONT>";
      }
      else
      {
        mvarDatosImpreso = mvarDatosImpreso + "<ROBOCONT>NO</ROBOCONT>";
      }
      wvarStep = 361;
      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_Granizo ) */ ) ).equals( "S" ) )
      {
        mvarDatosImpreso = mvarDatosImpreso + "<GRANIZO>SI</GRANIZO>";
      }
      else
      {
        mvarDatosImpreso = mvarDatosImpreso + "<GRANIZO>NO</GRANIZO>";
      }
      wvarStep = 362;
      mvarDatosImpreso = mvarDatosImpreso + "<AGECOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_PRODUCTOR ) */ ) + "</AGECOD>";
      mvarDatosImpreso = mvarDatosImpreso + "<AGEDES>" + mvarAGENTDES + "</AGEDES>";
      wvarStep = 363;
      mvarDatosImpreso = mvarDatosImpreso + "<CAMPACOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CampaCod ) */ ) + "</CAMPACOD>";
      mvarDatosImpreso = mvarDatosImpreso + "<CAMPADES>" + mvarCAMPADES + "</CAMPADES>";
      //11/2010 se aceptan distintos IVA
      wvarStep = 364;
      mvarDatosImpreso = mvarDatosImpreso + "<IVA>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CLIENIVA ) */ ) + "</IVA>";
      mvarDatosImpreso = mvarDatosImpreso + "<DESCIVA>" + mvarOPT_IVA + "</DESCIVA>";
      wvarStep = 365;
      mvarRAZONSOC = "";
      if( ! (null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_RAZONSOC ) */ == (org.w3c.dom.Node) null) )
      {
        mvarRAZONSOC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_RAZONSOC ) */ );
      }
      mvarDatosImpreso = mvarDatosImpreso + "<RAZONSOC>" + mvarRAZONSOC + "</RAZONSOC>";
      wvarStep = 366;
      mvarCUITNUME = "";
      if( ! (null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CUITNUME ) */ == (org.w3c.dom.Node) null) )
      {
        mvarCUITNUME = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_CUITNUME ) */ );
      }
      mvarDatosImpreso = mvarDatosImpreso + "<CUITNUME>" + mvarCUITNUME + "</CUITNUME>";
      wvarStep = 367;
      mvarDatosImpreso = mvarDatosImpreso + "<DESCIBB>" + mvarOPT_IBB + "</DESCIBB>";
      mvarDatosImpreso = mvarDatosImpreso + "<NROIBB>" + ((null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_NROIBB ) */ == (org.w3c.dom.Node) null) ? "" : diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_NROIBB ) */ )) + "</NROIBB>";

      mvarDatosImpreso = mvarDatosImpreso + "<CLIENTIP>" + mvarCLIENTIP + "</CLIENTIP>";
      //fin iva
      mvarDatosImpreso = mvarDatosImpreso + "</Request>";

      //Se generan los pdf en base 64, son 4 pdf, corresponden a 4 nodos a enviar
      //BRO_SolicitudAUS1 va siempre
      //BRO_CertificadoProvisorioAUS1.rptdesign va siempre
      //BRO_AutorizacionDebitoTarjAUS1.rptdesign solo cuando es tarjeta
      //BRO_AutorizacionDebitoAUS1.rptdesign solo cuando es cbu
      //mvarImpreso64 = "<Request>"
      mvarImpreso64 = "";

      wvarStep = 400;
      mvarRequest = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>" + mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_SolicitudAUS1</reportId>" + "</Request>";

      // cmdp_ExecuteTrn("lbaw_OVMWGen", , mvarRequest, mvarResponse)
      wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
      wobjClass.Execute( mvarRequest, mvarResponse, "" );
      wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );
      //Verifica que haya respuesta de MQ
      if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Estado" ) */.getAttributes().getNamedItem( "resultado" ) ).equals( "true" ) )
      {
        //Verifica que exista el PDF si no error Middle
        if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//generatePdfReportReturn" ) */ == (org.w3c.dom.Node) null) )
        {
          mvarNodoImpreso = diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.getDocument().getDocumentElement().selectSingleNode( "//generatePdfReportReturn" ) */ );
        }
        else
        {
          //error de mdw
          mvarNodoImpreso = "No se pudo obtener el impreso MDW";
        }
      }
      else
      {
        mvarNodoImpreso = "No se pudo obtener el impreso de Solicitud (MQ)";
      }

      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      mvarImpreso64 = mvarImpreso64 + "<IMPSOLI>" + mvarNodoImpreso + "</IMPSOLI>";

      wvarStep = 410;
      mvarRequest = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>" + mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_CertificadoProvisorioAUS1</reportId>" + "</Request>";

      // cmdp_ExecuteTrn("lbaw_OVMWGen", , mvarRequest, mvarResponse)
      wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
      wobjClass.Execute( mvarRequest, mvarResponse, "" );
      wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );
      //Verifica que haya respuesta de MQ
      if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Estado" ) */.getAttributes().getNamedItem( "resultado" ) ).equals( "true" ) )
      {
        //Verifica que exista el PDF si no error Middle
        if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//generatePdfReportReturn" ) */ == (org.w3c.dom.Node) null) )
        {
          mvarNodoImpreso = diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.getDocument().getDocumentElement().selectSingleNode( "//generatePdfReportReturn" ) */ );
        }
        else
        {
          //error de mdw
          mvarNodoImpreso = "No se pudo obtener el impreso MDW";
        }
      }
      else
      {
        mvarNodoImpreso = "No se pudo obtener el impreso de Certificado (MQ)";
      }

      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      mvarImpreso64 = mvarImpreso64 + "<IMPCERTI>" + mvarNodoImpreso + "</IMPCERTI>";
      //Autorizaci�n debito en cuenta o tarjeta
      wvarStep = 420;
      if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_COBROCOD ) */ ) ) == 4 )
      {
        mvarRequest = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>" + mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_AutorizacionDebitoTarjAUS1</reportId>" + "</Request>";
      }
      else
      {
        mvarRequest = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>" + mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_AutorizacionDebitoAUS1</reportId>" + "</Request>";
      }
      wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
      wobjClass.Execute( mvarRequest, mvarResponse, "" );
      wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );
      //Verifica que haya respuesta de MQ
      if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Estado" ) */.getAttributes().getNamedItem( "resultado" ) ).equals( "true" ) )
      {
        //Verifica que exista el PDF si no error Middle
        if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//generatePdfReportReturn" ) */ == (org.w3c.dom.Node) null) )
        {
          mvarNodoImpreso = diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.getDocument().getDocumentElement().selectSingleNode( "//generatePdfReportReturn" ) */ );
        }
        else
        {
          //error de mdw
          mvarNodoImpreso = "No se pudo obtener el impreso MDW";
        }
      }
      else
      {
        mvarNodoImpreso = "No se pudo obtener el impreso Autoriz. Debito (MQ)";
      }

      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      mvarImpreso64 = mvarImpreso64 + "<IMPAUTORI>" + mvarNodoImpreso + "</IMPAUTORI>";

      //jose
      //mvarImpreso64 = mvarImpreso64 & "</Request>"
      pvarRes.set( mvarImpreso64 );

      fncGetAll = true;
      fin: 
      //libero los objectos
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequestSolis = (diamondedge.util.XmlDom) null;
      wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"-2000\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;



        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
