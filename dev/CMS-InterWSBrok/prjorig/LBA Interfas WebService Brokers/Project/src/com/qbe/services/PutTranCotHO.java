package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class PutTranCotHO implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  static final String mcteClassName = "LBA_InterWSBrok.PutTranCotHO";
  static final String mcteStoreProc = "SPSNCV_BRO_ALTA_OPER";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  static final String mcteParam_COBROCOD = "COBROCOD";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_TIPOHOGAR = "TIPOHOGAR";
  static final String mcteParam_PLANNCOD = "PLANNCOD";
  static final String mcteParam_Provi = "PROVI";
  static final String mcteParam_LocalidadCod = "LOCALIDADCOD";
  static final String mcteParam_TipoOperac = "TIPOOPERAC";
  static final String mcteParam_RAMOPCOD = "RAMOPCOD";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  static final String mcteParam_NROCOT = "COT_NRO";
  static final String mcteParam_EstadoProc = "ESTADOPROC";
  static final String mcteParam_PedidoAno = "MSGANO";
  static final String mcteParam_PedidoMes = "MSGMES";
  static final String mcteParam_PedidoDia = "MSGDIA";
  static final String mcteParam_PedidoHora = "MSGHORA";
  static final String mcteParam_PedidoMinuto = "MSGMINUTO";
  static final String mcteParam_PedidoSegundo = "MSGSEGUNDO";
  static final String mcteParam_TiempoProceso = "TIEMPOPROCESO";
  static final String mcteParam_CERTISEC = "CERTISEC";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS";
  static final String mcteParam_COBERCOD = "COBERCOD";
  static final String mcteParam_CONTRMOD = "CONTRMOD";
  static final String mcteParam_CAPITASG = "CAPITASG";
  /**
   * DATOS DE LAS CAMPA�AS
   */
  static final String mcteNodos_Campa = "//Request/CAMPANIAS";
  static final String mcteParam_CampaCod = "CAMPACOD";
  /**
   * PRODUCTOR
   */
  static final String mctePORTAL = "LBA";
  /**
   * Objetos del FrameWork
   */
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncPutAll
   */
  private final String wcteFnName = "fncPutAll";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarMensaje = "";
    Variant wvarCodErr = new Variant();
    Variant pvarRes = new Variant();
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wvarMensaje = "";
      wvarCodErr.set( "" );
      pvarRes.set( "" );
      if( ! (invoke( "fncPutAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        if( !wvarCodErr.toString().equals( General.mcteErrorInesperadoCod ) )
        {
          pvarResponse.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        }
        else
        {
          pvarResponse.set( pvarRes );
        }

        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*unsup mobjCOM_Context.SetAbort() */;
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*unsup mobjCOM_Context.SetComplete() */;
        }
        return IAction_Execute;
      }
      //
      pvarResponse.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + pvarRes + "</LBA_WS>" );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        //mobjCOM_Context.SetAbort
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncPutAll( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_NROCOT = "";
    String mvarWDB_TIPOOPERAC = "";
    String mvarWDB_ESTADO = "";
    String mvarWDB_RECEPCION_PEDIDOANO = "";
    String mvarWDB_RECEPCION_PEDIDOMES = "";
    String mvarWDB_RECEPCION_PEDIDODIA = "";
    String mvarWDB_RECEPCION_PEDIDOHORA = "";
    String mvarWDB_RECEPCION_PEDIDOMINUTO = "";
    String mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_TIEMPOPROCESO = "";
    String mvarWDB_XML = "";

    //
    //
    //
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_CODINST = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }
      //
      wvarStep = 30;
      mvarWDB_REQUESTID = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID ) */ );
      }
      //
      wvarStep = 40;
      mvarWDB_NROCOT = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROCOT) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NROCOT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NROCOT ) */ );
      }
      //
      wvarStep = 50;
      mvarWDB_TIPOOPERAC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoOperac) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPOOPERAC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoOperac ) */ );
      }
      //
      wvarStep = 60;
      mvarWDB_ESTADO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EstadoProc) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_ESTADO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_EstadoProc ) */ );
      }
      //
      wvarStep = 70;
      mvarWDB_RECEPCION_PEDIDOANO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoAno) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoAno ) */ );
      }
      //
      wvarStep = 80;
      mvarWDB_RECEPCION_PEDIDOMES = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMes) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMes ) */ );
      }
      //
      wvarStep = 90;
      mvarWDB_RECEPCION_PEDIDODIA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoDia) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDODIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoDia ) */ );
      }
      //
      wvarStep = 100;
      mvarWDB_RECEPCION_PEDIDOHORA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoHora) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOHORA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoHora ) */ );
      }
      //
      wvarStep = 110;
      mvarWDB_RECEPCION_PEDIDOMINUTO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMinuto) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMINUTO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMinuto ) */ );
      }
      //
      wvarStep = 120;
      mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoSegundo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOSEGUNDO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoSegundo ) */ );
      }
      //
      wvarStep = 130;
      mvarWDB_RAMOPCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAMOPCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_RAMOPCOD ) */ );
      }
      //
      wvarStep = 140;
      mvarWDB_POLIZANN = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ == (org.w3c.dom.Node) null) )
      {
        if( new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN ) */ ) ).isNumeric() )
        {
          if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ ) ) < 100 )
          {
            mvarWDB_POLIZANN = String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN ) */ ) ) );
          }
        }
      }
      //
      wvarStep = 150;
      mvarWDB_POLIZSEC = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ == (org.w3c.dom.Node) null) )
      {
        if( new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC ) */ ) ).isNumeric() )
        {
          if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ ) ) < 1000000 )
          {
            mvarWDB_POLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC ) */ );
          }
        }
      }
      //
      wvarStep = 160;
      mvarWDB_CERTISEC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CERTISEC) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTISEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CERTISEC ) */ );
      }
      //
      wvarStep = 170;
      mvarWDB_TIEMPOPROCESO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TiempoProceso) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIEMPOPROCESO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TiempoProceso ) */ );
      }
      //
      mvarWDB_XML = "";
      mvarWDB_XML = pvarRequest;
      //
      wvarStep = 180;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      //
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 190;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_NRO_COTIZACION_LBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NROCOT.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NROCOT)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 18 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_TIPO_OPERACION", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_TIPOOPERAC.equals( "" ) ? "" : mvarWDB_TIPOOPERAC) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 230;
      wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_ESTADO.equals( "" ) ? "" : mvarWDB_ESTADO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 240;
      wobjDBParm = new Parameter( "@WDB_RECEPCION_PEDIDO", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant( mvarWDB_RECEPCION_PEDIDOANO + "-" + mvarWDB_RECEPCION_PEDIDOMES + "-" + mvarWDB_RECEPCION_PEDIDODIA + " " + mvarWDB_RECEPCION_PEDIDOHORA + ":" + mvarWDB_RECEPCION_PEDIDOMINUTO + ":" + mvarWDB_RECEPCION_PEDIDOSEGUNDO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 250;
      wobjDBParm = new Parameter( "@WDB_ENVIO_RESPUESTA", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant( DateTime.year( DateTime.now() ) + "-" + DateTime.month( DateTime.now() ) + "-" + DateTime.day( DateTime.now() ) + " " + DateTime.hour( DateTime.now() ) + ":" + DateTime.minute( DateTime.now() ) + ":" + DateTime.second( DateTime.now() ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 260;
      wobjDBParm = new Parameter( "@WDB_RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_RAMOPCOD.equals( "" ) ? "" : mvarWDB_RAMOPCOD) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 270;
      wobjDBParm = new Parameter( "@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 280;
      wobjDBParm = new Parameter( "@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 290;
      wobjDBParm = new Parameter( "@WDB_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 300;
      wobjDBParm = new Parameter( "@WDB_CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 310;
      wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CERTISEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CERTISEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 320;
      wobjDBParm = new Parameter( "@WDB_TIEMPO_PROCESO_AIS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIEMPOPROCESO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIEMPOPROCESO)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 330;
      wobjDBParm = new Parameter( "@WDB_OPERACION_XML", AdoConst.adVarChar, AdoConst.adParamInput, 8000, new Variant( (mvarWDB_XML.equals( "" ) ? "" : Strings.left( mvarWDB_XML, 8000 )) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 340;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      //
      wobjDBCnn.close();
      //
      fncPutAll = true;

      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
      return fncPutAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );


        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncPutAll = false;
        /*unsup mobjCOM_Context.SetComplete() */;


        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutAll;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
