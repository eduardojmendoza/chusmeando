package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class PutSolicitudHO implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  static final String mcteClassName = "LBA_InterWSBrok.PutSolicitudHO";
  static final String mcteStoreProc = "SPSNCV_BRO_SOLI_HOM1_GRABA";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  static final String mcteParam_COBROCOD = "COBROCOD";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_TIPOHOGAR = "TIPOHOGAR";
  static final String mcteParam_PLANNCOD = "PLANNCOD";
  static final String mcteParam_Provi = "PROVI";
  static final String mcteParam_LocalidadCod = "LOCALIDADCOD";
  static final String mcteParam_RAMOPCOD = "RAMOPCOD";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  static final String mcteParam_NROCOT = "COT_NRO";
  static final String mcteParam_CERTISEC = "CERTISEC";
  static final String mcteParam_ClienAp1 = "CLIENAP1";
  static final String mcteParam_ClienAp2 = "CLIENAP2";
  static final String mcteParam_ClienNom = "CLIENNOM";
  static final String mcteParam_CLIENSEX = "SEXO";
  static final String mcteParam_CLIENEST = "ESTADO";
  static final String mcteParam_NacimAnn = "NACIMANN";
  static final String mcteParam_NacimMes = "NACIMMES";
  static final String mcteParam_NacimDia = "NACIMDIA";
  static final String mcteParam_EMAIL = "EMAIL";
  static final String mcteParam_DOMICDOM = "DOMICDOM";
  static final String mcteParam_DomicDnu = "DOMICDNU";
  static final String mcteParam_DOMICPIS = "DOMICPIS";
  static final String mcteParam_DomicPta = "DOMICPTA";
  static final String mcteParam_DOMICPOB = "DOMICPOB";
  static final String mcteParam_BARRIOCOUNT = "BARRIOCOUNT";
  static final String mcteParam_TELNRO = "TELNRO";
  static final String mcteParam_DomicDomCo = "DOMICDOMCOR";
  static final String mcteParam_DomicDNUCo = "DOMICDNUCOR";
  static final String mcteParam_DomicPisCo = "DOMICPISCOR";
  static final String mcteParam_DomicPtaCo = "DOMICPTACOR";
  static final String mcteParam_DOMICPOBCo = "DOMICPOBCOR";
  static final String mcteParam_ProviCo = "PROVICOR";
  static final String mcteParam_LocalidadCodCo = "LOCALIDADCODCOR";
  static final String mcteParam_TelNroCo = "TELNROCOR";
  /**
   * Const mcteParam_TipoCuen             As String = "TIPOCUEN"
   */
  static final String mcteParam_CUENNUME = "CUENNUME";
  /**
   * Const mcteParam_TarjeCod             As String = "TARJECOD"
   */
  static final String mcteParam_VENCIANN = "VENCIANN";
  static final String mcteParam_VENCIMES = "VENCIMES";
  static final String mcteParam_VENCIDIA = "VENCIDIA";
  static final String mcteParam_UsoTipos = "USOTIPOS";
  static final String mcteParam_ALARMTIP = "ALARMTIP";
  static final String mcteParam_GUARDTIP = "GUARDTIP";
  static final String mcteParam_SWCALDER = "CALDERA";
  static final String mcteParam_SWASCENS = "ASCENSOR";
  static final String mcteParam_SWCREJAS = "REJAS";
  static final String mcteParam_SWPBLIND = "PUERTABLIND";
  static final String mcteParam_SWDISYUN = "DISYUNTOR";
  static final String mcteParam_SWPROPIE = "PROPIEDAD";
  static final String mcteParam_ZONA = "CODIZONA";
  static final String mcteParam_TipoDocu = "TIPODOCU";
  static final String mcteParam_NumeDocu = "NUMEDOCU";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS/COBERTURA";
  static final String mcteParam_COBERCOD = "COBERCOD";
  static final String mcteParam_CONTRMOD = "CONTRMOD";
  static final String mcteParam_CAPITASG = "CAPITASG";
  /**
   * DATOS DE LAS CAMPA�AS
   */
  static final String mcteNodos_Campa = "//Request/CAMPANIAS/CAMPANIA";
  static final String mcteParam_CampaCod = "CAMPACOD";
  /**
   * DATOS DE LOS ELECTRODOMESTICOS
   */
  static final String mcteNodos_Elect = "//Request/ELECTRODOMESTICOS/ELECTRODOMESTICO";
  static final String mcteParam_Tipo = "TIPO";
  static final String mcteParam_Marca = "MARCA";
  static final String mcteParam_Modelo = "MODELO";
  /**
   * PRODUCTOR
   */
  static final String mctePORTAL = "LBA";
  /**
   * Objetos del FrameWork
   */
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncPutAll
   */
  private final String wcteFnName = "fncPutAll";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarMensaje = "";
    Variant wvarCodErr = new Variant();
    Variant pvarRes = new Variant();
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wvarMensaje = "";
      wvarCodErr.set( "" );
      pvarRes.set( "" );
      if( ! (invoke( "fncPutAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*unsup mobjCOM_Context.SetAbort() */;
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*unsup mobjCOM_Context.SetComplete() */;
        }
        return IAction_Execute;
      }
      //
      pvarResponse.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + pvarRes + "</LBA_WS>" );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        //mobjCOM_Context.SetAbort
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncPutAll( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarcounter = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTIPOL = "";
    String mvarWDB_CERTIANN = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_SUPLENUM = "";
    String mvarWDB_NUMEDOCU = "";
    String mvarWDB_TIPODOCU = "";
    String mvarWDB_CLIENAP1 = "";
    String mvarWDB_CLIENAP2 = "";
    String mvarWDB_CLIENNOM = "";
    String mvarWDB_NACIMANN = "";
    String mvarWDB_NACIMMES = "";
    String mvarWDB_NACIMDIA = "";
    String mvarWDB_CLIENSEX = "";
    String mvarWDB_CLIENEST = "";
    String mvarWDB_EFECTANN = "";
    String mvarWDB_EFECTMES = "";
    String mvarWDB_EFECTDIA = "";
    String mvarWDB_EMAIL = "";
    String mvarWDB_DOMICDOM = "";
    String mvarWDB_DOMICDNU = "";
    String mvarWDB_DOMICPIS = "";
    String mvarWDB_DOMICPTA = "";
    String mvarWDB_DOMICPOB = "";
    String mvarWDB_BARRIOCOUNT = "";
    String mvarWDB_DOMICCPO = "";
    String mvarWDB_PROVICOD = "";
    String mvarWDB_TELNRO = "";
    String mvarWDB_DOMICDOM_CO = "";
    String mvarWDB_DOMICDNU_CO = "";
    String mvarWDB_DOMICPIS_CO = "";
    String mvarWDB_DOMICPTA_CO = "";
    String mvarWDB_DOMICPOB_CO = "";
    String mvarWDB_DOMICCPO_CO = "";
    String mvarWDB_PROVICOD_CO = "";
    String mvarWDB_TELNRO_CO = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_CUENNUME = "";
    String mvarWDB_VENCIANN = "";
    String mvarWDB_VENCIMES = "";
    String mvarWDB_VENCIDIA = "";
    String mvarWDB_BANCOCOD = "";
    String mvarWDB_SUCURCOD = "";
    String mvarWDB_P_EMISIANN = "";
    String mvarWDB_P_EMISIMES = "";
    String mvarWDB_P_EMISIDIA = "";
    String mvarWDB_P_COBROCOD = "";
    String mvarWDB_P_TIVIVCOD = "";
    String mvarWDB_P_USOTIPOS = "";
    String mvarWDB_P_SWCALDER = "";
    String mvarWDB_P_SWASCENS = "";
    String mvarWDB_P_ALARMTIP = "";
    String mvarWDB_P_GUARDTIP = "";
    String mvarWDB_P_SWCREJAS = "";
    String mvarWDB_P_SWPBLIND = "";
    String mvarWDB_P_SWDISYUN = "";
    String mvarWDB_P_SWPROPIE = "";
    String mvarWDB_P_PLANNCOD = "";
    String mvarWDB_P_BARRIOCOUNT = "";
    String mvarWDB_P_ZONA = "";
    String mvarWDB_P_CLIENIVA = "";
    String mvarWDB_P_COBERCOD = "";
    String mvarWDB_P_CONTRMOD = "";
    String mvarWDB_P_CAPITASG = "";
    String mvarWDB_P_CAMPACOD = "";
    String mvarWDB_P_TIPO = "";
    String mvarWDB_P_MARCA = "";
    String mvarWDB_P_MODELO = "";

    //
    //
    //
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      //
      mvarWDB_CODINST = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }
      //
      wvarStep = 30;
      //
      mvarWDB_RAMOPCOD = "HOM1";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAMOPCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_RAMOPCOD ) */ );
      }
      //
      wvarStep = 40;
      //
      mvarWDB_POLIZANN = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN ) */ );
      }
      //
      wvarStep = 50;
      //
      mvarWDB_POLIZSEC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC ) */ );
      }
      //
      wvarStep = 60;
      //
      mvarWDB_CERTISEC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CERTISEC) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTISEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CERTISEC ) */ );
      }
      //
      wvarStep = 70;
      //
      mvarWDB_P_EMISIANN = Strings.right( Strings.fill( 4, "0" ) + DateTime.year( DateTime.now() ), 4 );
      mvarWDB_P_EMISIMES = Strings.right( Strings.fill( 2, "0" ) + DateTime.month( DateTime.now() ), 2 );
      mvarWDB_P_EMISIDIA = Strings.right( Strings.fill( 2, "0" ) + DateTime.day( DateTime.now() ), 2 );
      //
      mvarWDB_EFECTANN = Strings.right( Strings.fill( 4, "0" ) + DateTime.year( DateTime.now() ), 4 );
      mvarWDB_EFECTMES = Strings.right( Strings.fill( 2, "0" ) + DateTime.month( DateTime.now() ), 2 );
      mvarWDB_EFECTDIA = Strings.right( Strings.fill( 2, "0" ) + DateTime.day( DateTime.now() ), 2 );
      //
      wvarStep = 80;
      //
      mvarWDB_NUMEDOCU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NumeDocu) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NUMEDOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NumeDocu ) */ );
      }
      //
      wvarStep = 90;
      //
      mvarWDB_TIPODOCU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoDocu) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPODOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoDocu ) */ );
      }
      //
      wvarStep = 100;
      //
      mvarWDB_CLIENAP1 = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienAp1) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENAP1 = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienAp1 ) */ ) );
      }
      //
      wvarStep = 110;
      //
      mvarWDB_CLIENAP2 = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienAp2) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENAP2 = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienAp2 ) */ ) );
      }
      //
      wvarStep = 120;
      //
      mvarWDB_CLIENNOM = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienNom) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENNOM = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienNom ) */ ) );
      }
      //
      wvarStep = 130;
      //
      mvarWDB_NACIMANN = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimAnn) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimAnn ) */ );
      }
      //
      wvarStep = 140;
      //
      mvarWDB_NACIMMES = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimMes) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimMes ) */ );
      }
      //
      wvarStep = 150;
      //
      mvarWDB_NACIMDIA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimDia) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimDia ) */ );
      }
      //
      wvarStep = 160;
      //
      mvarWDB_CLIENSEX = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENSEX) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENSEX = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENSEX ) */ ) );
      }
      //
      wvarStep = 170;
      //
      mvarWDB_CLIENEST = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENEST = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENEST ) */ ) );
      }
      //
      wvarStep = 180;
      //
      mvarWDB_EMAIL = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EMAIL) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_EMAIL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_EMAIL ) */ );
      }
      //
      wvarStep = 190;
      //
      mvarWDB_DOMICDOM = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICDOM) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDOM = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICDOM ) */ ) );
      }
      //
      wvarStep = 200;
      //
      mvarWDB_DOMICDNU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDnu) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDNU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDnu ) */ );
      }
      //
      wvarStep = 210;
      //
      mvarWDB_DOMICPIS = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICPIS) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPIS = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICPIS ) */ ) );
      }
      //
      wvarStep = 220;
      //
      mvarWDB_DOMICPTA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPta) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPTA = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPta ) */ ) );
      }
      //
      wvarStep = 230;
      //
      mvarWDB_DOMICPOB = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICPOB) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPOB = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICPOB ) */ ) );
      }
      //
      wvarStep = 240;
      //
      mvarWDB_DOMICCPO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICCPO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCod ) */ );
      }
      //
      wvarStep = 250;
      //
      mvarWDB_PROVICOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi ) */ );
      }
      //
      wvarStep = 260;
      //
      mvarWDB_TELNRO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TELNRO) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TELNRO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TELNRO ) */ );
      }
      //
      wvarStep = 270;
      //
      mvarWDB_BARRIOCOUNT = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_BARRIOCOUNT) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_BARRIOCOUNT = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_BARRIOCOUNT ) */ ) );
      }
      //
      wvarStep = 280;
      //
      mvarWDB_DOMICDOM_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDomCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDOM_CO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDomCo ) */ ) );
      }
      //
      wvarStep = 290;
      //
      mvarWDB_DOMICDNU_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDNUCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDNU_CO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDNUCo ) */ );
      }
      //
      wvarStep = 300;
      //
      mvarWDB_DOMICPIS_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPisCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPIS_CO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPisCo ) */ ) );
      }
      //
      wvarStep = 310;
      //
      mvarWDB_DOMICPTA_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPtaCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPTA_CO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPtaCo ) */ ) );
      }
      //
      wvarStep = 320;
      //
      mvarWDB_DOMICPOB_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICPOBCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPOB_CO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICPOBCo ) */ ) );
      }
      //
      wvarStep = 330;
      //
      mvarWDB_DOMICCPO_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCodCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICCPO_CO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCodCo ) */ );
      }
      //
      wvarStep = 340;
      //
      mvarWDB_PROVICOD_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ProviCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICOD_CO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ProviCo ) */ );
      }
      //
      wvarStep = 350;
      //
      mvarWDB_TELNRO_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TelNroCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TELNRO_CO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TelNroCo ) */ );
      }
      //
      wvarStep = 360;
      //
      mvarWDB_COBROTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROTIP = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP ) */ ) );
      }
      //
      wvarStep = 370;
      //
      mvarWDB_CUENNUME = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CUENNUME = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CUENNUME ) */ );
      }
      //
      wvarStep = 380;
      //
      mvarWDB_VENCIANN = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VENCIANN) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_VENCIANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_VENCIANN ) */ );
      }
      //
      wvarStep = 390;
      //
      mvarWDB_VENCIMES = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VENCIMES) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_VENCIMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_VENCIMES ) */ );
      }
      //
      wvarStep = 400;
      //
      mvarWDB_VENCIDIA = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VENCIDIA) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_VENCIDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_VENCIDIA ) */ );
      }
      //
      wvarStep = 410;
      if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) == 4 )
      {
        mvarWDB_BANCOCOD = "0";
        mvarWDB_SUCURCOD = "0000";
      }
      else
      {
        mvarWDB_BANCOCOD = "9000";
        mvarWDB_SUCURCOD = "8888";
      }
      wvarStep = 420;
      mvarWDB_P_COBROCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_COBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD ) */ );
      }
      //
      wvarStep = 430;
      mvarWDB_P_TIVIVCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TIPOHOGAR) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_TIVIVCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TIPOHOGAR ) */ );
      }
      //
      wvarStep = 440;
      mvarWDB_P_USOTIPOS = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_UsoTipos) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_USOTIPOS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_UsoTipos ) */ );
      }
      //
      wvarStep = 450;
      mvarWDB_P_SWCALDER = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWCALDER) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWCALDER = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWCALDER ) */ ) );
      }
      //
      wvarStep = 460;
      mvarWDB_P_SWASCENS = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWASCENS) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWASCENS = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWASCENS ) */ ) );
      }
      //
      wvarStep = 470;
      mvarWDB_P_ALARMTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ALARMTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_ALARMTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ALARMTIP ) */ );
      }
      //
      wvarStep = 480;
      mvarWDB_P_GUARDTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_GUARDTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_GUARDTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_GUARDTIP ) */ );
      }
      //
      wvarStep = 490;
      mvarWDB_P_SWCREJAS = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWCREJAS) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWCREJAS = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWCREJAS ) */ ) );
      }
      //
      wvarStep = 500;
      mvarWDB_P_SWPBLIND = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWPBLIND) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWPBLIND = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWPBLIND ) */ ) );
      }
      //
      wvarStep = 510;
      mvarWDB_P_SWDISYUN = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWDISYUN) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWDISYUN = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWDISYUN ) */ ) );
      }
      //
      wvarStep = 520;
      mvarWDB_P_SWPROPIE = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWPROPIE) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWPROPIE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWPROPIE ) */ );
      }
      //
      wvarStep = 530;
      mvarWDB_P_PLANNCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PLANNCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_PLANNCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PLANNCOD ) */ );
      }
      //
      wvarStep = 540;
      mvarWDB_P_ZONA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ZONA) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_ZONA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ZONA ) */ );
      }
      //
      wvarStep = 550;
      mvarWDB_P_CLIENIVA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CLIENIVA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENIVA ) */ );
      }
      //
      wvarStep = 560;
      mvarWDB_P_COBERCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBERCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_COBERCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBERCOD ) */ );
      }
      //
      wvarStep = 570;
      mvarWDB_P_CONTRMOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CONTRMOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CONTRMOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CONTRMOD ) */ );
      }
      //
      wvarStep = 580;
      mvarWDB_P_CAPITASG = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CAPITASG) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CAPITASG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CAPITASG ) */ );
      }
      //
      wvarStep = 590;
      mvarWDB_P_CAMPACOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CampaCod) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CAMPACOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CampaCod ) */ );
      }
      //
      wvarStep = 600;
      mvarWDB_P_TIPO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Tipo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_TIPO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Tipo ) */ ) );
      }
      //
      wvarStep = 610;
      mvarWDB_P_MARCA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Marca) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_MARCA = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Marca ) */ ) );
      }
      //
      wvarStep = 620;
      mvarWDB_P_MODELO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Modelo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_MODELO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Modelo ) */ ) );
      }
      //
      wvarStep = 630;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      //
      wvarStep = 640;
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 650;
      //
      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_RAMOPCOD.equals( "" ) ? "" : mvarWDB_RAMOPCOD) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 660;
      wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 670;
      wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 680;
      wobjDBParm = new Parameter( "@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_CODINST ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 690;
      wobjDBParm = new Parameter( "@CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 700;
      wobjDBParm = new Parameter( "@CERTISEC", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 710;
      wobjDBParm = new Parameter( "@SUPLENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 720;
      wobjDBParm = new Parameter( "@USUARCOD", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_CODINST ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 730;
      wobjDBParm = new Parameter( "@NUMEDOCU", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( (mvarWDB_NUMEDOCU.equals( "" ) ? "" : mvarWDB_NUMEDOCU) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 740;
      wobjDBParm = new Parameter( "@TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPODOCU.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPODOCU)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 750;
      wobjDBParm = new Parameter( "@CLIENAP1", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( (mvarWDB_CLIENAP1.equals( "" ) ? "" : mvarWDB_CLIENAP1) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 760;
      wobjDBParm = new Parameter( "@CLIENAP2", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( (mvarWDB_CLIENAP2.equals( "" ) ? "" : mvarWDB_CLIENAP2) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 770;
      wobjDBParm = new Parameter( "@CLIENNOM", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( (mvarWDB_CLIENNOM.equals( "" ) ? "" : mvarWDB_CLIENNOM) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 780;
      wobjDBParm = new Parameter( "@NACIMANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NACIMANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NACIMANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 790;
      wobjDBParm = new Parameter( "@NACIMMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NACIMMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NACIMMES)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 800;
      wobjDBParm = new Parameter( "@NACIMDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NACIMDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NACIMDIA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 810;
      wobjDBParm = new Parameter( "@CLIENSEX", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_CLIENSEX.equals( "" ) ? "" : mvarWDB_CLIENSEX) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 820;
      wobjDBParm = new Parameter( "@CLIENEST", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_CLIENEST.equals( "" ) ? "" : mvarWDB_CLIENEST) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 830;
      wobjDBParm = new Parameter( "@PAISSCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "00" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 840;
      wobjDBParm = new Parameter( "@IDIOMCOD", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 850;
      wobjDBParm = new Parameter( "@NUMHIJOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 860;
      wobjDBParm = new Parameter( "@EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 870;
      wobjDBParm = new Parameter( "@EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTMES)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 880;
      wobjDBParm = new Parameter( "@EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTDIA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 890;
      wobjDBParm = new Parameter( "@CLIENTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "F" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 900;
      wobjDBParm = new Parameter( "@CLIENFUM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 910;
      wobjDBParm = new Parameter( "@CLIENDOZ", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 920;
      wobjDBParm = new Parameter( "@ABRIDTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 930;
      wobjDBParm = new Parameter( "@ABRIDNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 940;
      wobjDBParm = new Parameter( "@CLIENORG", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 950;
      wobjDBParm = new Parameter( "@PERSOTIP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "F" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 960;
      wobjDBParm = new Parameter( "@DOMICSEC", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 970;
      wobjDBParm = new Parameter( "@CLIENCLA", AdoConst.adChar, AdoConst.adParamInput, 9, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 980;
      wobjDBParm = new Parameter( "@FALLEANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 990;
      wobjDBParm = new Parameter( "@FALLEMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1000;
      wobjDBParm = new Parameter( "@FALLEDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1010;
      wobjDBParm = new Parameter( "@FBAJAANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1020;
      wobjDBParm = new Parameter( "@FBAJAMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1030;
      wobjDBParm = new Parameter( "@FBAJADIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1040;
      wobjDBParm = new Parameter( "@EMAIL", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant( (mvarWDB_EMAIL.equals( "" ) ? "" : mvarWDB_EMAIL) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1050;
      wobjDBParm = new Parameter( "@DOMICCAL", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1060;
      wobjDBParm = new Parameter( "@DOMICDOM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( (mvarWDB_DOMICDOM.equals( "" ) ? "" : mvarWDB_DOMICDOM) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1070;
      wobjDBParm = new Parameter( "@DOMICDNU", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( (mvarWDB_DOMICDNU.equals( "" ) ? "" : mvarWDB_DOMICDNU) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1080;
      wobjDBParm = new Parameter( "@DOMICESC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1090;
      wobjDBParm = new Parameter( "@DOMICPIS", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_DOMICPIS.equals( "" ) ? "" : mvarWDB_DOMICPIS) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1100;
      wobjDBParm = new Parameter( "@DOMICPTA", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_DOMICPTA.equals( "" ) ? "" : mvarWDB_DOMICPTA) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1120;
      wobjDBParm = new Parameter( "@DOMICPOB", AdoConst.adVarChar, AdoConst.adParamInput, 60, new Variant( (mvarWDB_DOMICPOB.equals( "" ) ? "" : mvarWDB_DOMICPOB) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1130;
      wobjDBParm = new Parameter( "@DOMICCPO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_DOMICCPO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_DOMICCPO)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1140;
      wobjDBParm = new Parameter( "@PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1150;
      wobjDBParm = new Parameter( "@TELCOD", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1160;
      wobjDBParm = new Parameter( "@TELNRO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( (mvarWDB_TELNRO.equals( "" ) ? "" : mvarWDB_TELNRO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1170;
      wobjDBParm = new Parameter( "@PRINCIPAL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "S" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1180;
      wobjDBParm = new Parameter( "@DOMICCAL_CO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1190;
      wobjDBParm = new Parameter( "@DOMICDOM_CO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( (mvarWDB_DOMICDOM_CO.equals( "" ) ? "" : mvarWDB_DOMICDOM_CO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1200;
      wobjDBParm = new Parameter( "@DOMICDNU_CO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( (mvarWDB_DOMICDNU_CO.equals( "" ) ? "" : mvarWDB_DOMICDNU_CO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1210;
      wobjDBParm = new Parameter( "@DOMICESC_CO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1220;
      wobjDBParm = new Parameter( "@DOMICPIS_CO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_DOMICPIS_CO.equals( "" ) ? "" : mvarWDB_DOMICPIS_CO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1230;
      wobjDBParm = new Parameter( "@DOMICPTA_CO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_DOMICPTA_CO.equals( "" ) ? "" : mvarWDB_DOMICPTA_CO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1240;
      wobjDBParm = new Parameter( "@DOMICPOB_CO", AdoConst.adVarChar, AdoConst.adParamInput, 60, new Variant( (mvarWDB_DOMICPOB_CO.equals( "" ) ? "" : mvarWDB_DOMICPOB_CO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1250;
      wobjDBParm = new Parameter( "@DOMICCPO_CO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_DOMICCPO_CO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_DOMICCPO_CO)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1260;
      wobjDBParm = new Parameter( "@PROVICOD_CO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD_CO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICOD_CO)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1270;
      wobjDBParm = new Parameter( "@TELCOD_CO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1280;
      wobjDBParm = new Parameter( "@TELNRO_CO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( (mvarWDB_TELNRO_CO.equals( "" ) ? "" : mvarWDB_TELNRO_CO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1290;
      wobjDBParm = new Parameter( "@PRINCIPAL_CO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1300;
      wobjDBParm = new Parameter( "@TIPOCUEN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1310;
      wobjDBParm = new Parameter( "@COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_COBROTIP.equals( "" ) ? "" : mvarWDB_COBROTIP) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1320;
      wobjDBParm = new Parameter( "@BANCOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_BANCOCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1330;
      wobjDBParm = new Parameter( "@SUCURCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Math.floor( Obj.toDouble( mvarWDB_SUCURCOD ) ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1340;
      wobjDBParm = new Parameter( "@CUENTDC", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1350;
      wobjDBParm = new Parameter( "@CUENNUME", AdoConst.adChar, AdoConst.adParamInput, 16, new Variant( (mvarWDB_CUENNUME.equals( "" ) ? "" : mvarWDB_CUENNUME) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1360;
      wobjDBParm = new Parameter( "@TARJECOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1370;
      wobjDBParm = new Parameter( "@VENCIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VENCIANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VENCIANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1380;
      wobjDBParm = new Parameter( "@VENCIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VENCIMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VENCIMES)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1390;
      wobjDBParm = new Parameter( "@VENCIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VENCIDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VENCIDIA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1400;
      wobjDBParm = new Parameter( "@P_CIAASCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "0001" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1410;
      wobjDBParm = new Parameter( "@P_EMISIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_EMISIANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1420;
      wobjDBParm = new Parameter( "@P_EMISIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_EMISIMES)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1430;
      wobjDBParm = new Parameter( "@P_EMISIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_EMISIDIA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1440;
      wobjDBParm = new Parameter( "@P_EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1450;
      wobjDBParm = new Parameter( "@P_EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTMES)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1460;
      wobjDBParm = new Parameter( "@P_EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTDIA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1470;
      wobjDBParm = new Parameter( "@P_USUARCOD", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_CODINST ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1480;
      wobjDBParm = new Parameter( "@P_SITUCPOL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "A" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1490;
      wobjDBParm = new Parameter( "@P_CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1500;
      wobjDBParm = new Parameter( "@P_NUMEDOCU", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( (mvarWDB_NUMEDOCU.equals( "" ) ? "" : mvarWDB_NUMEDOCU) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1510;
      wobjDBParm = new Parameter( "@P_TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPODOCU.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPODOCU)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1520;
      wobjDBParm = new Parameter( "@P_DOMICSEC1", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1530;
      wobjDBParm = new Parameter( "@P_DOMICSEC2", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1540;
      wobjDBParm = new Parameter( "@P_CUENTSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1550;
      wobjDBParm = new Parameter( "@P_COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_COBROCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_COBROCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1560;
      wobjDBParm = new Parameter( "@P_COBROFOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1570;
      wobjDBParm = new Parameter( "@P_COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_COBROTIP.equals( "" ) ? "" : mvarWDB_COBROTIP) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1580;
      wobjDBParm = new Parameter( "@P_TIVIVCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_TIVIVCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_TIVIVCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1590;
      wobjDBParm = new Parameter( "@P_ALTURNUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1600;
      wobjDBParm = new Parameter( "@P_USOTIPOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_USOTIPOS.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_USOTIPOS)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1610;
      wobjDBParm = new Parameter( "@P_SWCALDER", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_P_SWCALDER.equals( "" ) ? "" : mvarWDB_P_SWCALDER) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1620;
      wobjDBParm = new Parameter( "@P_SWASCENS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_P_SWASCENS.equals( "" ) ? "" : mvarWDB_P_SWASCENS) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1630;
      wobjDBParm = new Parameter( "@P_ALARMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_ALARMTIP.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_ALARMTIP)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1640;
      wobjDBParm = new Parameter( "@P_GUARDTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_GUARDTIP.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_GUARDTIP)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1650;
      wobjDBParm = new Parameter( "@P_SWCREJAS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_P_SWCREJAS.equals( "" ) ? "" : mvarWDB_P_SWCREJAS) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1660;
      wobjDBParm = new Parameter( "@P_SWPBLIND", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_P_SWPBLIND.equals( "" ) ? "" : mvarWDB_P_SWPBLIND) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1670;
      wobjDBParm = new Parameter( "@P_SWDISYUN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_P_SWDISYUN.equals( "" ) ? "" : mvarWDB_P_SWDISYUN) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1680;
      wobjDBParm = new Parameter( "@P_SWPROPIE", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_P_SWPROPIE.equals( "" ) ? "" : mvarWDB_P_SWPROPIE) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1690;
      wobjDBParm = new Parameter( "@P_PLANNCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_PLANNCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_PLANNCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1700;
      wobjDBParm = new Parameter( "@P_PLANNDAB", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1710;
      wobjDBParm = new Parameter( "@P_BARRIOCOUNT", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( (mvarWDB_P_BARRIOCOUNT.equals( "" ) ? "" : mvarWDB_P_BARRIOCOUNT) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1720;
      wobjDBParm = new Parameter( "@P_ZONA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_ZONA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_ZONA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1730;
      wobjDBParm = new Parameter( "@P_CLIENIVA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_P_CLIENIVA.equals( "" ) ? "" : mvarWDB_P_CLIENIVA) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1740;
      wobjDBParm = new Parameter( "@P_ASEG_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1750;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) */;
      //
      for( wvarcounter = 1; wvarcounter <= 10; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
          wobjDBParm = new Parameter( "@P_CAMPACOD_" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_CampaCod ) */ ) ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          wobjDBParm = new Parameter( "@P_CAMPACOD_" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
        }
      }
      //
      wvarStep = 1760;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) */;
      //
      // AM se cambio de 20 a 30
      for( wvarcounter = 1; wvarcounter <= 30; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
          wobjDBParm = new Parameter( "@P_COBERCOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_COBERCOD ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 3 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_COBERORD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CAPITASG" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_CAPITASG ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CAPITIMP" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 100000 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CONTRMOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_CONTRMOD ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          wobjDBParm = new Parameter( "@P_COBERCOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 3 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_COBERORD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CAPITASG" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CAPITIMP" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CONTRMOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
        }
      }
      //
      wvarStep = 1770;
      wobjDBParm = new Parameter( "@P_SUCURSAL_CODIGO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarWDB_SUCURCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1780;
      wobjDBParm = new Parameter( "@P_SUCURSAL_DESCRI", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1790;
      wobjDBParm = new Parameter( "@P_LEGAJO_VEND", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1800;
      wobjDBParm = new Parameter( "@P_APENOM_VEND", AdoConst.adChar, AdoConst.adParamInput, 50, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1810;
      wobjDBParm = new Parameter( "@P_LEGAJO_GTE", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1820;
      wobjDBParm = new Parameter( "@P_APENOM_GTE", AdoConst.adChar, AdoConst.adParamInput, 50, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1830;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) */;
      //
      for( wvarcounter = 1; wvarcounter <= 10; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
          wobjDBParm = new Parameter( "@P_TIPO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_Tipo ) */ ) ) ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_MARCA" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_Marca ) */ ) ) ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_MODELO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_Modelo ) */ ) ) ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          wobjDBParm = new Parameter( "@P_TIPO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( "" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_MARCA" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( "" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_MODELO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( "" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
        }
      }
      //
      wvarStep = 1840;
      wobjDBParm = new Parameter( "@P_SITUCEST", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      //Como no vienen asegurados, guardo todos estos datos en blanco
      for( wvarcounter = 1; wvarcounter <= 10; wvarcounter++ )
      {
        wobjDBParm = new Parameter( "@P_DOCUMDAT" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 9 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wobjDBParm = new Parameter( "@P_NOMBREAS" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 49, new Variant( "" ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }
      //
      wvarStep = 1850;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      wobjDBCnn.close();

      pvarRes.set( "<Response>" + "<REQUESTID>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ ) + "</REQUESTID>" + "<CERTISEC>" + wobjDBCmd.getParameters().getParameter("@CERTISEC").getValue() + "</CERTISEC>" + "</Response>" );

      fncPutAll = true;

      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
      return fncPutAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );


        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncPutAll = false;
        /*unsup mobjCOM_Context.SetComplete() */;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutAll;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
