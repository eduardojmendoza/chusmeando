package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class GetValidacionSolAU implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  static final String mcteClassName = "LBA_InterWSBrok.GetValidacionSolAU";
  static final String mcteStoreProc = "SPSNCV_BRO_VALIDA_SOL_SCO";
  static final String mcteStoreProcSelect = "SPSNCV_BRO_SELECT_OPER_ESP";
  static final String mcteArchivoAUSSOL_XML = "LBA_VALIDACION_SOL_AU.XML";
  /**
   *  Parametros XML de Entrada
   */
  static final String mcteParam_Cliensec = "CERTISEC";
  static final String mcteParam_NacimAnn = "NACIMANN";
  static final String mcteParam_NacimMes = "NACIMMES";
  static final String mcteParam_NacimDia = "NACIMDIA";
  static final String mcteParam_Sexo = "SEXO";
  static final String mcteParam_Estado = "ESTADO";
  /**
   * jc 09/2010 en la cotizaci�n tiene otro nombre el tag
   * Const mcteParam_CLIENIVA       As String = "CLIENIVA"
   */
  static final String mcteParam_CLIENIVA = "IVA";
  static final String mcteParam_ModeAutCod = "MODEAUTCOD";
  static final String mcteParam_KMsrngCod = "KMSRNGCOD";
  static final String mcteParam_EfectAnn = "EFECTANN";
  static final String mcteParam_SiGarage = "SIGARAGE";
  static final String mcteParam_SumAseg = "SUMAASEG";
  static final String mcteParam_Siniestros = "SINIESTROS";
  static final String mcteParam_Gas = "GAS";
  static final String mcteParam_Provi = "PROVI";
  static final String mcteParam_LocalidadCod = "LOCALIDADCOD";
  /**
   * Revisar
   */
  static final String mcteParam_CampaCod = "CAMPACOD";
  /**
   * Revisar
   */
  static final String mcteParam_DatosPlan = "DATOSPLAN";
  static final String mcteParam_ClubLBA = "CLUBLBA";
  /**
   * Revisar
   */
  static final String mcteParam_Portal = "PORTAL";
  /**
   * jc 09/2010 en la cotizaci�n tiene otro nombre el tag
   * Const mcteParam_PRODUCTOR       As String = "PRODUCTOR"
   */
  static final String mcteParam_PRODUCTOR = "AGECOD";
  static final String mcteParam_Permite_GNC = "PERMITE_GNC";
  static final String mcteParam_VEHDES = "VEHDES";
  static final String mcteParam_CODZONA = "CODZONA";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_NRO_OPERACION_BROKER = "REQUESTID";
  static final String mcteParam_CODPROVCO = "PROVI";
  static final String mcteParam_CODPOSTCO = "LOCALIDADCOD";
  static final String mcteParam_COBROCOD = "COBROCOD";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_CUENNUME = "CUENNUME";
  static final String mcteParam_SUMAASEG = "SUMAASEG";
  static final String mcteParam_COLOR = "VEHCLRCOD";
  static final String mcteParam_DOCUMTIP_TIT = "TIPODOCU";
  static final String mcteParam_PLANNCOD = "PLANNCOD";
  static final String mcteParam_FRANQCOD = "FRANQCOD";
  static final String mcteParam_INSPECCION = "INSPECOD";
  static final String mcteParam_RASTREO = "RASTREO";
  static final String mcteParam_CODIGO_ALARMA = "ALARMCOD";
  static final String mcteParam_CATEGO = "AUCATCOD";
  /**
   * Accesorios
   */
  static final String mcteNodos_Accesorios = "//Request/ACCESORIOS/ACCESORIO";
  static final String mcteParam_PrecioAcc = "PRECIOACC";
  static final String mcteParam_DescripcionAcc = "DESCRIPCIONACC";
  static final String mcteParam_CodigoAcc = "CODIGOACC";
  /**
   * Hijos
   */
  static final String mcteNodos_Hijos = "//Request/HIJOS/HIJO";
  static final String mcteParam_SexoHijo = "SEXOHIJO";
  static final String mcteParam_EstadoHijo = "ESTADOHIJO";
  static final String mcteParam_NacimHijo = "NACIMHIJO";
  static final String mcteParam_NombreHijo = "NOMBREHIJO";
  static final String mcteParam_ApellidoHijo = "APELLIDOHIJO";
  static final String mcteParam_EdadHijo = "EDADHIJO";
  static final String mcteParam_TipoDocuHijo = "TIPODOCUHIJO";
  static final String mcteParam_NroDocuHijo = "NRODOCUHIJO";
  /**
   * jc 8/2010 agregado xml
   */
  static final String mcteParam_DESTRUCCION_80 = "DESTRUCCION_80";
  static final String mcteParam_Luneta = "LUNETA";
  static final String mcteParam_ClubEco = "CLUBECO";
  static final String mcteParam_Robocont = "ROBOCONT";
  static final String mcteParam_Granizo = "GRANIZO";
  static final String mcteParam_INSTALADP = "INSTALADP";
  static final String mcteParam_POSEEDISP = "POSEEDISP";
  /**
   * JC 2010-11 ch.r.
   */
  static final String mcteParam_CLIENTIP = "CLIENTIP";
  /**
   * JC 2010-11 ch.r.
   */
  static final String mcteParam_IBB = "IBB";
  /**
   * JC 2010-11 ch.r.
   */
  static final String mcteParam_CUITNUME = "CUITNUME";
  /**
   * JC 2010-11 ch.r.
   */
  static final String mcteParam_NROIBB = "NROIBB";
  /**
   * JC 2010-11 ch.r.
   */
  static final String mcteParam_RAZONSOC = "RAZONSOC";
  /**
   * AM
   */
  static final String mcteParam_ACREPREN = "//ACRE-PREN";
  static final String mcteParam_NUMEDOCUACRE = "//NUMEDOCU-ACRE";
  static final String mcteParam_TIPODOCUACRE = "//TIPODOCU-ACRE";
  static final String mcteParam_APELLIDOACRE = "//APELLIDO-ACRE";
  static final String mcteParam_NOMBRESACRE = "//NOMBRES-ACRE";
  static final String mcteParam_DOMICDOMACRE = "//DOMICDOM-ACRE";
  static final String mcteParam_DOMICDNUACRE = "//DOMICDNU-ACRE";
  static final String mcteParam_DOMICPISACRE = "//DOMICPIS-ACRE";
  static final String mcteParam_DOMICPTAACRE = "//DOMICPTA-ACRE";
  static final String mcteParam_DOMICPOBACRE = "//DOMICPOB-ACRE";
  static final String mcteParam_DOMICCPOACRE = "//DOMICCPO-ACRE";
  static final String mcteParam_PROVICODACRE = "//PROVICOD-ACRE";
  static final String mcteParam_PAISSCODACRE = "//PAISSCOD-ACRE";
  static final String mcteParam_EmpresaCodigo = "//CANAL";
  static final String mcteParam_SucursalCodigo = "//CANAL_SUCURSAL";
  static final String mcteParam_LegajoVend = "//LEGAJO_VEND";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncValidaABase
   * static variable for method: fncComparaConCotizacion
   */
  private final String wcteFnName = "fncComparaConCotizacion";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarCodErr = new Variant();
    Variant wvarMensaje = new Variant();
    Variant pvarRes = new Variant();
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      pvarRes.set( "" );
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        wvarStep = 20;
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }
      wvarStep = 30;
      pvarResponse.set( pvarRes );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    Object wobjClass = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLRes = null;
    diamondedge.util.XmlDom wobjXMLCheck = null;
    diamondedge.util.XmlDom wobjXMLStatusTarjeta = null;
    diamondedge.util.XmlDom wobjXMLStatusCanal = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.NodeList wobjXMLNodeList = null;
    org.w3c.dom.Node wobjXMLNodeError = null;
    org.w3c.dom.Node wobjXMLNodeSucu = null;
    com.qbe.services.HSBCInterfaces.IDBConnection wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarContChild = 0;
    int wvarContNode = 0;
    int wvarcounter = 0;
    String wvarErrorCodigo = "";
    int wvarStep = 0;
    String wvarTarjeta = "";
    String wvarStatusTarjeta = "";
    String wvarBancoCod = "";
    String wvarExisteSucu = "";
    String mvarRequest = "";
    String wvarResponse = "";
    String mvarWDB_USUARCOD = "";
    String mvarCUENNUME = "";
    java.util.Date wvarFechaActual = DateTime.EmptyDate;
    java.util.Date wvarFechaVigencia = DateTime.EmptyDate;





    wvarStep = 0;
    wvarCodErr.set( 0 );

    try 
    {
      //Chequeo que los datos esten OK
      //Chequeo que no venga nada en blanco
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );

      wobjXMLCheck = new diamondedge.util.XmlDom();
      //unsup wobjXMLCheck.async = false;
      wobjXMLCheck.load( System.getProperty("user.dir") + "\\" + mcteArchivoAUSSOL_XML );

      //Chequeo nodo padre
      wobjXMLNodeError = null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/VACIOS/CAMPOS" ) */;
      //Tomo codigo de error de Vacios
      wvarErrorCodigo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/VACIOS/N_ERROR" ) */ );

      wvarStep = 20;
      for( wvarContChild = 0; wvarContChild <= (wobjXMLNodeError.getChildNodes().getLength() - 1); wvarContChild++ )
      {
        if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + wobjXMLNodeError.getChildNodes().item( wvarContChild ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
        {
          if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + wobjXMLNodeError.getChildNodes().item( wvarContChild ).getNodeName()) ) */ ) ).equals( "" ) )
          {
            //error
            wvarCodErr.set( wvarErrorCodigo );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( wobjXMLNodeError.getChildNodes().item( wvarContChild ) ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
        else
        {
          //error
          wvarCodErr.set( -1 );
          wvarMensaje.set( diamondedge.util.XmlDom.getText( wobjXMLNodeError.getChildNodes().item( wvarContChild ) ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }


      //Chequeo Tipo y Longitud de Datos para el XML Padre
      wvarStep = 30;
      for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().getLength() - 1); wvarContChild++ )
      {
        if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
        {
          //Si el Codigo de Cobro no es tarjeta, no chequea el Largo de Tarjeta y su Vencimiento
          if( ! ((VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) == 4)) && ((null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "CUENNUME" )) || (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "VENCIANN" )) || (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "VENCIMES" )) || (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "VENCIDIA" ))) )
          {
            //unsup GoTo Siguiente
          }
          //Chequeo la longitud y si tiene Largo Fijo
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
          {
            if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") ) */ ) )) )
            {
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
          else
          {
            if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") ) */ ) ) )
            {
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }

          //Chequeo el Tipo
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
          {
            if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ).isNumeric()) )
            {
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            else
            {
              //Chequeo que el dato numerico no sea 0
              //Excluye los nodos COT_NRO, SINIESTROS, RASTREO, INSPECOD Y ALARMCOD
              if( (Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) == 0) && ! (((null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ).getNodeName().equals( "COT_NRO" )) || (null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ).getNodeName().equals( "ALARMCOD" )) || (null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ).getNodeName().equals( "RASTREO" )) || (null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ).getNodeName().equals( "INSPECOD" )) || (null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ).getNodeName().equals( "SINIESTROS" )))) )
              {
                wvarCodErr.set( -199 );
                wvarMensaje.set( "El valor del dato numerico no puede estar en 0" );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
          }
        }
        Siguiente: 
          ;
      }

      //Chequeo Tipo y Longitud de Datos para Accesorios
      for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */.getLength() - 1); wvarContChild++ )
      {
        wvarStep = 40;
        for( wvarContNode = 0; wvarContNode <= (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */.item( wvarContChild ).getChildNodes().getLength() - 1); wvarContNode++ )
        {
          if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
          {
            //Chequeo la longitud y si tiene Largo Fijo
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
            {
              if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) )) )
              {
                wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
            else
            {
              if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) ) )
              {
                wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }

            //Chequeo el Tipo
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
            {
              if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ).isNumeric()) )
              {
                wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
          }
        }
      }

      //Chequeo Tipo y Longitud de Datos de Hijos
      wvarStep = 50;
      for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */.getLength() - 1); wvarContChild++ )
      {
        for( wvarContNode = 0; wvarContNode <= (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */.item( wvarContChild ).getChildNodes().getLength() - 1); wvarContNode++ )
        {
          if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
          {
            //Chequeo la longitud y si tiene Largo Fijo
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
            {
              if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) )) )
              {
                wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
            else
            {
              if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) ) )
              {
                wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }

            //Chequeo el Tipo
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
            {
              if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ).isNumeric()) )
              {
                wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
          }
        }
      }

      //valido los datos
      wvarStep = 60;
      wobjXMLNodeError = null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS" ) */;
      for( wvarContChild = 0; wvarContChild <= (wobjXMLNodeError.getChildNodes().getLength() - 1); wvarContChild++ )
      {
        if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + wobjXMLNodeError.getChildNodes().item( wvarContChild ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
        {
          if( null /*unsup wobjXMLNodeError.getChildNodes().item( wvarContChild ).selectNodes( ("VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( "//" + wobjXMLNodeError.getChildNodes().item( wvarContChild ).getNodeName() ) *) ) + "']") ) */.getLength() == 0 )
          {
            //error
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNodeError.getChildNodes().item( wvarContChild ).selectSingleNode( "N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNodeError.getChildNodes().item( wvarContChild ).selectSingleNode( "TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }

      //Chequeo EDAD
      wvarStep = 70;
      if( ! (Funciones.ValidarFechayRango( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimDia ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimMes) ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimAnn) ) */ ), 17, 86, wvarMensaje )) )
      {
        //error
        wvarCodErr.set( -9 );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }

      //Valida la fecha de la Tarjeta (si Viene)
      wvarStep = 80;
      if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) == 4 )
      {
        if( ! ((null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIDIA" ) */ == (org.w3c.dom.Node) null) && (null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ == (org.w3c.dom.Node) null) && (null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ == (org.w3c.dom.Node) null)) )
        {
          if( ! (Funciones.validarFecha( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIDIA" ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ ), wvarMensaje )) )
          {
            //error
            wvarCodErr.set( -13 );
            wvarMensaje.set( "Fecha de Tarjeta Invalida" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }


        //Valida el nro. de Tarjeta
        wvarStep = 92;

        mvarCUENNUME = "0";
        if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) ) */ == (org.w3c.dom.Node) null) )
        {
          mvarCUENNUME = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CUENNUME ) */ );
        }

        wvarTarjeta = "<Request>" + "<USUARIO/>" + "<COBROTIP>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ ) + "</COBROTIP>" + "<CTANUM>" + mvarCUENNUME + "</CTANUM>" + "<VENANN>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ ) + "</VENANN>" + "<VENMES>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ ) + "</VENMES>" + "</Request>";

        wvarStep = 94;
        //jc 09/2010 se reemplaza componente
        wobjClass = new lbawA_OVMQCotizar.lbaw_OVValidaCuentas();
        wobjClass.Execute( wvarTarjeta, wvarStatusTarjeta, "" );
        wobjClass = null;

        wvarStep = 96;
        wobjXMLStatusTarjeta = new diamondedge.util.XmlDom();
        //unsup wobjXMLStatusTarjeta.async = false;
        wobjXMLStatusTarjeta.loadXML( wvarStatusTarjeta );

        wvarStep = 98;
        //jc 09/2010 se reemplaza componente
        //If Not UCase(wobjXMLStatusTarjeta.selectSingleNode("//VERIFICACION").Text) = "TRUE" Then
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
        {
          wvarCodErr.set( -198 );
          //jc 09/2010
          //wvarMensaje = "Numero de Tarjeta de Cr�dito invalido"
          wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@mensaje" ) */ ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      //jc 10/2010 se agrega CBU
      //El nro. de CBU(22) se conforma asi en cuennume(0,16) en  VENCIMES(16,2) y VENCIANN(18, 4)
      wvarStep = 800;
      if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) == 5 )
      {
        if( !Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ ) ).equals( "DB" ) )
        {
          wvarCodErr.set( -13 );
          wvarMensaje.set( "Para CBU campo COBROTIP debe valer DB" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        if( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -13 );
          wvarMensaje.set( "Nro. CBU incompleto (cuennume)" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        if( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -13 );
          wvarMensaje.set( "Nro. CBU incompleto (vencimes)" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        if( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -13 );
          wvarMensaje.set( "Nro. CBU incompleto (venciann)" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        //Valida el nro. de CBU
        wvarStep = 920;
        wvarTarjeta = "<Request>" + "<USUARIO/>" + "<COBROTIP>" + Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ ) ) + "</COBROTIP>" + "<CTANUM>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) ) */ ) + "</CTANUM>" + "<VENANN>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ ) + "</VENANN>" + "<VENMES>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ ) + "</VENMES>" + "</Request>";

        wvarStep = 940;
        wobjClass = new lbawA_OVMQCotizar.lbaw_OVValidaCuentas();
        wobjClass.Execute( wvarTarjeta, wvarStatusTarjeta, "" );
        wobjClass = (Object) null;

        wvarStep = 960;
        wobjXMLStatusTarjeta = new diamondedge.util.XmlDom();
        //unsup wobjXMLStatusTarjeta.async = false;
        wobjXMLStatusTarjeta.loadXML( wvarStatusTarjeta );

        wvarStep = 980;
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
        {
          wvarCodErr.set( -198 );
          wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@mensaje" ) */ ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      //JC FIN CBU
      //Chequeo el Nro. de Patente
      wvarStep = 100;
      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//ESCERO" ) */ ) ).equals( "S" ) )
      {
        if( ! (Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//PATENNUM" ) */ ) ).equals( "A/D" )) )
        {
          wvarCodErr.set( -14 );
          wvarMensaje.set( "Nro. de Patente Invalida" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        }
      }
      if( (Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//ESCERO" ) */ ) ).equals( "N" )) || ((Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//ESCERO" ) */ ) ).equals( "S" )) && ! ((Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//PATENNUM" ) */ ) ).equals( "A/D" )))) )
      {
        wvarCodErr.set( "0" );
        wvarMensaje.set( "" );
        pvarRes.set( "" );
        for( wvarcounter = 1; wvarcounter <= 3; wvarcounter++ )
        {
          if( new Variant( Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//PATENNUM" ) */ ), wvarcounter, 1 ) ).isNumeric() )
          {
            wvarCodErr.set( -14 );
            wvarMensaje.set( "Nro. de Patente Invalida" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          }
        }

        wvarStep = 105;
        for( wvarcounter = 4; wvarcounter <= 6; wvarcounter++ )
        {
          if( ! (new Variant( Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//PATENNUM" ) */ ), wvarcounter, 1 ) ).isNumeric()) )
          {
            wvarCodErr.set( -14 );
            wvarMensaje.set( "Nro. de Patente Invalida" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          }
        }
      }
      if( wvarCodErr.toInt() != 0 )
      {
        return fncGetAll;
      }
      //jc 09/2010 valida dispositivo de rastreo
      // 12-11-2012 Si el plan es RC no requeire dispositivo
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//PLANNCOD" ) */ ).equals( "011" ) )
      {

        if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//INSTALADP" ) */ ) ).equals( "S" ) )
        {
          wvarCodErr.set( -298 );
          wvarMensaje.set( "No requiere instalar dispositivo de rastreo" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      else
      {

        wvarStep = 107;
        wvarTarjeta = "<Request>" + "<USUARIO/>" + "<MODEAUTCOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ModeAutCod) ) */ ) + "</MODEAUTCOD>" + "<SUMASEG>" + String.valueOf( (Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SumAseg) ) */ ) ) * 100) ) + "</SUMASEG>" + "<PROVI>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ ) + "</PROVI>" + "<LOCALIDADCOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) ) */ ) + "</LOCALIDADCOD>" + "<OPCION/><OPERACION/><PRESCOD/><INSTALA/>" + "</Request>";
        wobjClass = new lbawA_OVMQCotizar.lbaw_OVGetDispRastreo();
        wobjClass.Execute( wvarTarjeta, wvarStatusTarjeta, "" );
        wobjClass = (Object) null;

        wobjXMLStatusTarjeta = new diamondedge.util.XmlDom();
        //unsup wobjXMLStatusTarjeta.async = false;
        wobjXMLStatusTarjeta.loadXML( wvarStatusTarjeta );

        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
        {
          wvarCodErr.set( -298 );
          wvarMensaje.set( "No se puede determinar si requiere dispositivo de rastreo" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusTarjeta.selectSingleNode( "//REQ" ) */ ).equals( "S" ) )
          {
            if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//INSTALADP" ) */ ) ).equals( "N" ) )
            {
              wvarCodErr.set( -298 );
              wvarMensaje.set( "Se requiere instalar dispositivo de rastreo" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            else
            {
              if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//RASTREO" ) */ ).equals( "0" ) )
              {
                wvarCodErr.set( -298 );
                wvarMensaje.set( "Falta informar dispositivo de rastreo" );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
          }
          else
          {
            if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//INSTALADP" ) */ ) ).equals( "S" ) )
            {
              wvarCodErr.set( -298 );
              wvarMensaje.set( "NO requiere instalar dispositivo de rastreo" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
        }
      }

      //Valida la fecha de Vigencia
      wvarStep = 110;
      if( ! (Funciones.validarFecha( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENDIA" ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENMES" ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENANN" ) */ ), wvarMensaje )) )
      {
        //error
        wvarCodErr.set( -15 );
        wvarMensaje.set( "Fecha de Vigencia Invalida" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;

      }

      //Chequeo que la fecha de Vigencia sea mayor a hoy
      wvarStep = 112;
      wvarFechaActual = DateTime.dateSerial( DateTime.year( DateTime.now() ), DateTime.month( DateTime.now() ), DateTime.day( DateTime.now() ) );
      wvarFechaVigencia = DateTime.dateSerial( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENANN" ) */ ) ), Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENMES" ) */ ) ), Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENDIA" ) */ ) ) );

      if( wvarFechaVigencia.compareTo( wvarFechaActual ) < 0 )
      {
        wvarCodErr.set( -197 );
        wvarMensaje.set( "Fecha de Vigencia anterior a la fecha actual" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      //Chequeo que la fecha no sea mayor a 30 d�as a partir de hoy
      wvarStep = 114;
      if( wvarFechaVigencia.compareTo( DateTime.add( wvarFechaActual, 30 ) ) > 0 )
      {
        wvarCodErr.set( -196 );
        wvarMensaje.set( "Fecha de Vigencia posterior a 30 dias de la fecha actual" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;

      }
      //Chequeo los hijos
      wvarStep = 120;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */;
      for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
      {
        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_SexoHijo ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Sexo de Hijo en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_SexoHijo ) *) ) + "']") ) */.getLength() == 0 )
          {
            //error
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
        wvarStep = 130;
        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_NacimHijo ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Fecha de Nacimiento de Hijo en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( ! (Funciones.ValidarFechayRango( Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_NacimHijo ) */ ), 2 ) + "/" + Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_NacimHijo ) */ ), 3, 2 ) + "/" + Strings.right( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_NacimHijo ) */ ), 4 ), 17, 29, wvarMensaje )) )
          {
            //error
            wvarCodErr.set( -10 );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
        wvarStep = 150;
        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_EstadoHijo ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Estado Civil del Hijo en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_EstadoHijo ) *) ) + "']") ) */.getLength() == 0 )
          {
            //error
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
        wvarStep = 160;

        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_NombreHijo ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Nombre del Hijo en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_NombreHijo ) */ ).equals( "" ) )
          {
            //error
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Nombre del Hijo en blanco" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }

        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_ApellidoHijo ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Apellido del Hijo en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_ApellidoHijo ) */ ).equals( "" ) )
          {
            //error
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Apellido del Hijo en blanco" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }

        wvarStep = 170;
        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_EdadHijo ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Edad del Hijo en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_EdadHijo ) */ ) ) == 0 )
          {
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Edad del Hijo con Valor 0" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;

      //Chequeo los Accesorios
      wvarStep = 180;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */;
      for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
      {
        wvarStep = 190;
        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_PrecioAcc ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Precio de Accesorio en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
          //Else
          //   If Val(wobjXMLList(wvarContNode).selectSingleNode(mcteParam_PrecioAcc).Text) = 0 Then
          //     wvarCodErr = -1
          //     wvarMensaje = "Campo de Precio de Accesorio en cero"
          //     pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
          //     Exit Function
          //   End If
        }

        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_CodigoAcc ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Codigo de Accesorio en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_CodigoAcc ) */ ) ) == 0 )
          {
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Codigo de Accesorio en blanco" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }

        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_DescripcionAcc ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Descripcion de Accesorio en blanco" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_DescripcionAcc ) */ ).equals( "" ) )
          {
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Descripcion de Accesorio en blanco" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;

      //11/2010 se permiten distintos tipos de iva
      wvarStep = 195;
      // distino consumidor final valida cuit
      if( !diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ).equals( "3" ) )
      {
        if( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUITNUME) ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -398 );
          wvarMensaje.set( "Debe informar Nro. de CUIT" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          wvarStep = 197;
          wvarTarjeta = "<Request>" + "<USUARIO/> " + "<DOCUMTIP>4</DOCUMTIP> " + "<DOCUMNRO>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUITNUME) ) */ ) + "</DOCUMNRO> " + "</Request>";
          wvarStep = 198;
          wobjClass = new lbawA_OVMQCotizar.lbaw_OVValidaNumDoc();
          wobjClass.Execute( wvarTarjeta, wvarStatusTarjeta, "" );
          wobjClass = (Object) null;
          wvarStep = 199;
          wobjXMLStatusTarjeta = new diamondedge.util.XmlDom();
          //unsup wobjXMLStatusTarjeta.async = false;
          wobjXMLStatusTarjeta.loadXML( wvarStatusTarjeta );
          wvarStep = 200;
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
          {
            wvarCodErr.set( -398 );
            wvarMensaje.set( "Nro. de CUIT incorrecto o esta mal el formato" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }
      else
      {
        wvarStep = 201;

        if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUITNUME) ) */ == (org.w3c.dom.Node) null) )
        {
          if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUITNUME) ) */ ).equals( "0" )) )
          {
            wvarCodErr.set( -398 );
            wvarMensaje.set( "NO Debe informar Nro. de CUIT" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }

      //11/2010 se permiten distintos tipos de iva implica tambien ibb
      wvarStep = 202;
      // distino consumidor final valida nro.ibb
      if( !diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ).equals( "3" ) )
      {
        if( (null /*unsup wobjXMLRequest.selectNodes( ("//" + mcteParam_NROIBB) ) */ == (org.w3c.dom.NodeList) null) || (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_IBB) ) */ == (org.w3c.dom.Node) null) )
        {
          wvarCodErr.set( -398 );
          wvarMensaje.set( "Debe informar Nro. de Ingresos Brutos" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          wvarStep = 203;
          wvarTarjeta = "<Request>" + "<USUARIO/> " + "<IIBBTIP>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_IBB) ) */ ) + "</IIBBTIP> " + "<IIBBNUM>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROIBB) ) */ ) + "</IIBBNUM> " + "</Request>";
          wvarStep = 204;
          wobjClass = new lbawA_OVMQCotizar.lbaw_OVValidaIIBB();
          wobjClass.Execute( wvarTarjeta, wvarStatusTarjeta, "" );
          wobjClass = (Object) null;
          wvarStep = 205;
          wobjXMLStatusTarjeta = new diamondedge.util.XmlDom();
          //unsup wobjXMLStatusTarjeta.async = false;
          wobjXMLStatusTarjeta.loadXML( wvarStatusTarjeta );
          wvarStep = 206;
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
          {
            wvarCodErr.set( -398 );
            wvarMensaje.set( "Nro. de Ingresos Brutos incorrecto o no corresponde a Tipo de Ingresos Brutos" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }
      else
      {
        wvarStep = 207;
        if( null /*unsup wobjXMLRequest.selectNodes( ("//" + mcteParam_NROIBB) ) */.getLength() > 0 )
        {
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROIBB) ) */ == (org.w3c.dom.Node) null) )
          {
            if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROIBB) ) */ ).equals( "0" )) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "NO Debe informar Nro. de Ingresos Brutos" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
        }
      }
      // se valida razon social si corresponde x iva
      wvarStep = 208;
      // distino consumidor final valida cuit
      if( !diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ).equals( "3" ) )
      {
        if( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAZONSOC) ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -398 );
          wvarMensaje.set( "Debe informar Razon Social" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      else
      {
        if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAZONSOC) ) */ == (org.w3c.dom.Node) null) )
        {
          if( !Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAZONSOC) ) */ ) ).equals( "" ) )
          {
            wvarCodErr.set( -398 );
            wvarMensaje.set( "No debe informar Razon Social" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }

      //fin 11/2010
      // Valido campos del Acreedor Prendario
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ACREPREN ) */ == (org.w3c.dom.Node) null) )
      {
        if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ACREPREN ) */ ) ).equals( "S" ) )
        {

          if( ! ((null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCUACRE ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCUACRE ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_APELLIDOACRE ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NOMBRESACRE ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOMACRE ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDNUACRE ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPISACRE ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPTAACRE ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOBACRE ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICCPOACRE ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROVICODACRE ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAISSCODACRE ) */ == (org.w3c.dom.Node) null)) )
          {
            if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCUACRE ) */ ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Numero de Documento del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCUACRE ) */ ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Tipo de Documento del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_APELLIDOACRE ) */ ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Apellido del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NOMBRESACRE ) */ ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Nombre del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOMACRE ) */ ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Domicilio del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDNUACRE ) */ ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Domicilio Numero del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPISACRE ) */ ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Domicilio Piso del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPTAACRE ) */ ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Domicilio Puerta del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOBACRE ) */ ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Domicilio POB del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICCPOACRE ) */ ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Numero de Documento del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROVICODACRE ) */ ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Provincia del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAISSCODACRE ) */ ) ).equals( "" ) )
            {
              wvarCodErr.set( -398 );
              wvarMensaje.set( "Debe informar Pais del Acreedor Prendario" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
          else
          {
            wvarCodErr.set( -399 );
            wvarMensaje.set( "Debe informar todos los datos del Acreedor Prendario" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
      }

      wvarStep = 200;
      if( wvarCodErr.toInt() == 0 )
      {
        if( invoke( "fncValidaABase", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } ) )
        {

          //AM Valido CANAL en caso de que se informe
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//CANAL" ) */ == (org.w3c.dom.Node) null) )
          {
            // Obtengo el USUARCOD de la respuesta de la BD
            wvarStep = 111;
            wobjXMLRes = new diamondedge.util.XmlDom();
            //unsup wobjXMLRes.async = false;
            wobjXMLRes.loadXML( pvarRes.toString() );
            //
            wvarStep = 112;
            mvarWDB_USUARCOD = "";
            if( ! (null /*unsup wobjXMLRes.selectSingleNode( "//USUARCOD" ) */ == (org.w3c.dom.Node) null) )
            {
              mvarWDB_USUARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRes.selectSingleNode( "//USUARCOD" ) */ );
            }
            //
            // Si se informa el CANAL valido los otros dos campos
            if( null /*unsup wobjXMLRequest.selectSingleNode( "//CANAL_SUCURSAL" ) */ == (org.w3c.dom.Node) null )
            {
              wvarCodErr.set( -14 );
              wvarMensaje.set( "Debe informar Canal Sucursal" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            if( null /*unsup wobjXMLRequest.selectSingleNode( "//LEGAJO_VEND" ) */ == (org.w3c.dom.Node) null )
            {
              wvarCodErr.set( -14 );
              wvarMensaje.set( "Debe informar Legajo Vendedor" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }

            //AM Valido el CANAL Msg1000
            wvarStep = 921;
            mvarRequest = "<Request><DEFINICION>1000_PerfilUsuario.xml</DEFINICION>";
            mvarRequest = mvarRequest + "<AplicarXSL>1000_PerfilUsuario.xsl</AplicarXSL>";
            mvarRequest = mvarRequest + "<USUARIO>" + Strings.toUpperCase( mvarWDB_USUARCOD ) + "</USUARIO></Request>";


            wvarStep = 922;
            wobjClass = new lbawA_OVMQGen.lbaw_MQMensajeGestion();
            wobjClass.Execute( mvarRequest, wvarResponse, "" );
            wobjClass = (Object) null;

            wvarStep = 923;
            wobjXMLStatusCanal = new diamondedge.util.XmlDom();
            //unsup wobjXMLStatusCanal.async = false;
            wobjXMLStatusCanal.loadXML( wvarResponse );

            wvarStep = 924;
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusCanal.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
            {
              wvarCodErr.set( -198 );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusCanal.selectSingleNode( "//Response/Estado/@mensaje" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            else
            {
              wvarBancoCod = "0";
              if( ! (null /*unsup wobjXMLStatusCanal.selectSingleNode( "//BANCOCOD" ) */ == (org.w3c.dom.Node) null) )
              {
                wvarBancoCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusCanal.selectSingleNode( "//BANCOCOD" ) */ );
              }
              if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EmpresaCodigo ) */ ).equals( wvarBancoCod )) )
              {
                wvarCodErr.set( -399 );
                wvarMensaje.set( "Nro. de Canal incorrecto para el Broker" );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }

            }
            // Fin valido CANAL
            //AM Valido el CANAL_SUCURSAL Msg1018
            wvarStep = 925;
            mvarRequest = "<Request><DEFINICION>1018_ListadoSucursalesxBanco.xml</DEFINICION>";
            mvarRequest = mvarRequest + "<BANCOCOD>" + wvarBancoCod + "</BANCOCOD></Request>";

            wvarStep = 926;
            wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
            wobjClass.Execute( mvarRequest, wvarResponse, "" );
            wobjClass = (Object) null;

            wvarStep = 927;
            wobjXMLStatusCanal = new diamondedge.util.XmlDom();
            //unsup wobjXMLStatusCanal.async = false;
            wobjXMLStatusCanal.loadXML( wvarResponse );

            wvarStep = 928;
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusCanal.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
            {
              wvarCodErr.set( -198 );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusCanal.selectSingleNode( "//Response/Estado/@mensaje" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            else
            {
              wvarStep = 929;
              wobjXMLNodeList = null /*unsup wobjXMLStatusCanal.selectNodes( "//Response/CAMPOS/SUCURSALES/SUCU" ) */;
              wvarExisteSucu = "N";

              for( wvarcounter = 0; wvarcounter <= (wobjXMLNodeList.getLength() - 1); wvarcounter++ )
              {
                wvarStep = 931;
                if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SucursalCodigo ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNodeList.item( wvarcounter ).selectSingleNode( "CODISUCU" ) */ ) ) )
                {
                  wvarExisteSucu = "S";
                }
              }

              wvarStep = 932;
              if( wvarExisteSucu.equals( "N" ) )
              {
                wvarCodErr.set( -399 );
                wvarMensaje.set( "Nro. de Sucursal invalido para el Broker" );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
            // Fin valido CANAL_SUCURSAL
          }
          fncGetAll = true;
        }
        else
        {
          fncGetAll = false;
        }
      }
      else
      {
        fncGetAll = false;
      }

      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = (com.qbe.services.HSBCInterfaces.IDBConnection) null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }

  private boolean fncValidaABase( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncValidaABase = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    boolean wvarError = false;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXML = null;
    diamondedge.util.XmlDom wobjXSL = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    String wvarXSL = "";
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarContNode = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_NRO_OPERACION_BROKER = "";
    String mvarWDB_CODPROVCO = "";
    String mvarWDB_CODPOSTCO = "";
    String mvarWDB_COBROCOD = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_SUMAASEG = "";
    String mvarWDB_SUMALBA = "";
    String mvarWDB_COLOR = "";
    String mvarWDB_DOCUMTIP_TIT = "";
    String mvarWDB_PLANNCOD = "";
    String mvarWDB_FRANQCOD = "";
    String mvarWDB_INSPECCION = "";
    String mvarWDB_RASTREO = "";
    String mvarWDB_CODIGO_ALARMA = "";
    String mvarWDB_CODIZONA = "";
    String mvarWDB_CATEGO = "";
    String pvarReq = "";
    String mvarseltest = "";
    int wvarCount = 0;
    String CampoNOmbre = "";





    try 
    {


      wvarError = false;
      wvarStep = 10;

      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );

      mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      mvarWDB_NRO_OPERACION_BROKER = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NRO_OPERACION_BROKER ) */ );
      mvarWDB_CODPROVCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODPROVCO ) */ );
      mvarWDB_CODPOSTCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODPOSTCO ) */ );
      mvarWDB_COBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD ) */ );
      mvarWDB_COBROTIP = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP ) */ ) );
      mvarWDB_CODIZONA = "0";
      mvarWDB_SUMAASEG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SUMAASEG ) */ );
      mvarWDB_COLOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COLOR ) */ );
      mvarWDB_DOCUMTIP_TIT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOCUMTIP_TIT ) */ );
      mvarWDB_PLANNCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PLANNCOD ) */ );
      mvarWDB_FRANQCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_FRANQCOD ) */ );
      mvarWDB_INSPECCION = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_INSPECCION ) */ );
      mvarWDB_RASTREO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_RASTREO ) */ );
      mvarWDB_CODIGO_ALARMA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODIGO_ALARMA ) */ );
      mvarWDB_CATEGO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CATEGO ) */ );

      wvarStep = 20;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 30;
      wobjDBParm = new Parameter( "@WDB_CODINST", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_CODINST ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 40;
      wobjDBParm = new Parameter( "@WDB_PRODUCTOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_NRO_OPERACION_BROKER ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 50;
      wobjDBParm = new Parameter( "@WDB_CODPROVCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_CODPROVCO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 60;
      wobjDBParm = new Parameter( "@WDB_CODPOSTCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_CODPOSTCO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 70;
      wobjDBParm = new Parameter( "@WDB_COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_COBROCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 80;
      wobjDBParm = new Parameter( "@WDB_COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarWDB_COBROTIP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 90;
      wobjDBParm = new Parameter( "@WDB_SUMAASEG", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Strings.split( mvarWDB_SUMAASEG, ".", -1 )[0] ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 15 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 100;
      wobjDBParm = new Parameter( "@WDB_COLOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_COLOR ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 110;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP_TIT", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_DOCUMTIP_TIT ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 120;
      wobjDBParm = new Parameter( "@WDB_PLANNCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_PLANNCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 130;
      wobjDBParm = new Parameter( "@WDB_FRANQCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarWDB_FRANQCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 140;
      wobjDBParm = new Parameter( "@WDB_INSPECCION", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_INSPECCION ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      //jc 09/2010 actualmente de 4
      //wobjDBParm.Precision = 2
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 150;
      wobjDBParm = new Parameter( "@WDB_RASTREO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_RASTREO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 160;
      //mvarWDB_CODIGO_ALARMA tiene longitud 4 , se saca de la validacion
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CODIGO_ALARMA", adNumeric, adParamInput, , mvarWDB_CODIGO_ALARMA)
      wobjDBParm = new Parameter( "@WDB_CODIGO_ALARMA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 170;
      wobjDBParm = new Parameter( "@WDB_CODIZONA", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( mvarWDB_CODIZONA ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 180;
      wobjDBParm = new Parameter( "@WDB_CATEGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_CATEGO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 190;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP_1", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP_2", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP_3", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP_4", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 230;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP_5", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 235;
      //jc 09/2010 el @WDB_NROCOT al cambiar el componente de cotiz. no se obtiene mas en la sp viene en 0
      wobjDBParm = new Parameter( "@WDB_NROCOT", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 238;
      wobjDBParm = new Parameter( "@WDB_SUMA_MINIMA", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 18 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 239;
      wobjDBParm = new Parameter( "@WDB_SUMALBA", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 15 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 2399;
      wobjDBParm = new Parameter( "@WDB_USUARCOD", AdoConst.adChar, AdoConst.adParamInputOutput, 10, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 240;
      mvarseltest = mcteStoreProc + " ";
      for( wvarCount = 0; wvarCount <= (wobjDBCmd.getParameters().getCount() - 1); wvarCount++ )
      {
        if( (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adChar) || (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adVarChar) )
        {
          mvarseltest = mvarseltest + "'" + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + "',";
        }
        else
        {
          mvarseltest = mvarseltest + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + ",";
        }
      }

      wvarStep = 250;
      if( ! (wvarError) )
      {

        wrstRes = new Recordset();
        wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
        wrstRes.setActiveConnection( (Connection) null );

        wobjXML = new diamondedge.util.XmlDom();
        //unsup wobjXML.async = false;
        wobjXML.load( System.getProperty("user.dir") + "\\" + mcteArchivoAUSSOL_XML );


        //SI NO HUBO ERROR AGREGO LOS DATOS AL XML DE ENTRADA PARA LA RESPUESTA
        wvarStep = 260;
        if( ! (wrstRes.isEOF()) )
        {
          if( Strings.toUpperCase( Strings.trim( wrstRes.getFields().getField(0).getValue().toString() ) ).equals( "OK" ) )
          {
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CODZONA", "" ) */ );
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//CODZONA" ) */, wobjDBCmd.getParameters().getParameter("@WDB_CODIZONA").getValue().toString() );

            //Agrega el NroCot y el nodo de CERTISEC en blanco
            //jc 09/2010 el @WDB_NROCOT al cambiar el componente no se obtiene mas de la sp viene en 0
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "WDB_NROCOT", "" ) */ );
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//WDB_NROCOT" ) */, wobjDBCmd.getParameters().getParameter("@WDB_NROCOT").getValue().toString() );
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "SUMA_MINIMA", "" ) */ );
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//SUMA_MINIMA" ) */, wobjDBCmd.getParameters().getParameter("@WDB_SUMA_MINIMA").getValue().multiply( new Variant( 100 ) ).toString() );
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CERTISEC", "" ) */ );
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//CERTISEC" ) */, "0" );
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "SUMALBA", "" ) */ );
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//SUMALBA" ) */, wobjDBCmd.getParameters().getParameter("@WDB_SUMALBA").getValue().toString() );
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "USUARCOD", "" ) */ );
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//USUARCOD" ) */, wobjDBCmd.getParameters().getParameter("@WDB_USUARCOD").getValue().toString() );


            for( wvarContNode = 0; wvarContNode <= (wrstRes.getFields().getCount() - 1); wvarContNode++ )
            {
              if( wrstRes.getFields().getField(wvarContNode).getName().equals( "" ) )
              {
                CampoNOmbre = "RESULTADO" + String.valueOf( wvarContNode + 1 );
              }
              else
              {
                CampoNOmbre = wrstRes.getFields().getField(wvarContNode).getName();
              }
              /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), Strings.toUpperCase( CampoNOmbre ), "" ) */ );
              if( (wrstRes.getFields().getField(wvarContNode).getType() == AdoConst.adNumeric) && (wrstRes.getFields().getField(wvarContNode).getPrecision() == 18) )
              {
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + Strings.toUpperCase( CampoNOmbre ) ) */, wrstRes.getFields().getField(wvarContNode).getValue().multiply( new Variant( 100 ) ).toString() );
              }
              else
              {
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + Strings.toUpperCase( CampoNOmbre ) ) */, Strings.trim( wrstRes.getFields().getField(wvarContNode).getValue().toString() ) );
              }
            }
            pvarRes.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
            if( ! (invoke( "fncComparaConCotizacion", new Variant[] { new Variant(wobjXMLRequest.getDocument().getDocumentElement().toString()), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
            {
              wvarError = true;
            }
            else
            {
              wvarError = false;
            }

          }
          else
          {
            //error
            wvarError = true;
            if( null /*unsup wobjXML.selectNodes( ("//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']") ) */.getLength() > 0 )
            {
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXML.selectSingleNode( "//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/TEXTOERROR" ) */ ) );
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXML.selectSingleNode( "//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/VALORRESPUESTA" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg = \"" + wvarMensaje + "\"></LBA_WS>" );
            }
            else
            {
              wvarCodErr.set( wrstRes.getFields().getField(0).getValue() );
              wvarMensaje.set( "No existe descripcion para este error" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg = \"" + wvarMensaje + "\"></LBA_WS>" );
            }
          }
        }
        else
        {
          //error
          wvarError = true;
          wvarCodErr.set( -16 );
          wvarMensaje.set( "No se retornaron datos de la base" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        }
      }
      //
      pvarReq = wobjXMLRequest.getDocument().getDocumentElement().toString();
      //
      wobjXML = (diamondedge.util.XmlDom) null;
      if( wvarError )
      {
        fncValidaABase = false;
      }
      else
      {
        fncValidaABase = true;
      }


      wrstRes.close();
      wobjDBCnn.close();
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return fncValidaABase;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );
        wvarCodErr.set( -1000 );
        wvarMensaje.set( General.mcteErrorInesperadoDescr );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncValidaABase = false;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncValidaABase;
  }

  private boolean fncComparaConCotizacion( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncComparaConCotizacion = false;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLRequestCotis = null;
    diamondedge.util.XmlDom wobjXMLCotizacion = null;
    diamondedge.util.XmlDom wobjXMLValid = null;
    org.w3c.dom.NodeList wobjXMLNodeList = null;
    org.w3c.dom.NodeList wobjXMLListCotiz = null;
    org.w3c.dom.Node wobjXMLNode = null;
    org.w3c.dom.Node wobjXMLNodeCotiz = null;
    Object wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    boolean wvarChequeonuevas = false;
    boolean wvarChequeoLocalidad = false;
    boolean wvarChequeoTarjeta = false;
    String wvarTieneHijos = "";
    String wvarNombrePlan = "";
    boolean wvarChequeoHijos = false;
    boolean wvarChequeoAccesorios = false;
    int wvarcounter = 0;
    String wvarResponse = "";
    int wvarStep = 0;
    String wvarNuevoHijos = "";
    String mvarWDB_CODINST = "";
    String mvarWDB_NRO_OPERACION_BROKER = "";
    String mvarDatosPlan = "";



    //jc 09/2010 nvas. coberturas
    wvarStep = 0;


    try 
    {

      wvarStep = 10;

      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 20;
      mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      mvarWDB_NRO_OPERACION_BROKER = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NRO_OPERACION_BROKER ) */ );

      wvarStep = 30;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProcSelect );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      //Comparo con el XML guardado de Cotizacion
      //Busco el registro guardado de la cotizacion
      wvarStep = 40;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 50;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NRO_OPERACION_BROKER.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_NRO_OPERACION_BROKER)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 60;
      wobjDBParm = new Parameter( "@WDB_TIPO_OPERACION ", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "C" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 70;
      wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "OK" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wrstRes = new Recordset();
      wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
      wrstRes.setActiveConnection( (Connection) null );
      //
      //Cargo el campo de XML de cotizacion
      wvarStep = 80;
      wobjXMLCotizacion = new diamondedge.util.XmlDom();
      //unsup wobjXMLCotizacion.async = false;
      wobjXMLCotizacion.loadXML( wrstRes.getFields().getField(14).getValue().toString() );
      //
      //Si es PROVI = 1 no compara el Codigo Postal
      if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ ) ) != 1 )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_LocalidadCod) ) */ ) ) )
        {
          wvarChequeoLocalidad = true;
        }
        else
        {
          wvarChequeoLocalidad = false;
        }
      }
      else
      {
        wvarChequeoLocalidad = true;
      }

      //Si es COBROCOD = 4 no compara el Tipo de Tarjeta
      wvarStep = 90;
      if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) != 4 )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ ) ) )
        {
          wvarChequeoTarjeta = true;
        }
        else
        {
          wvarChequeoTarjeta = false;
        }
      }
      else
      {
        wvarChequeoTarjeta = true;
      }

      //Chequeo los nodos de hijos
      wvarStep = 100;
      wobjXMLNodeList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */;
      wobjXMLListCotiz = null /*unsup wobjXMLCotizacion.selectNodes( mcteNodos_Hijos ) */;
      wvarChequeoHijos = true;
      if( ! ((wobjXMLNodeList == (org.w3c.dom.NodeList) null)) && ! ((wobjXMLListCotiz == (org.w3c.dom.NodeList) null)) )
      {
        for( wvarcounter = 0; wvarcounter <= (wobjXMLNodeList.getLength() - 1); wvarcounter++ )
        {
          wobjXMLNode = wobjXMLNodeList.item( wvarcounter );
          wobjXMLNodeCotiz = null /*unsup wobjXMLCotizacion.selectNodes( mcteNodos_Hijos ) */.item( wvarcounter );
          if( wobjXMLNodeList.getLength() != wobjXMLListCotiz.getLength() )
          {
            wvarCodErr.set( -165 );
            wvarMensaje.set( "Datos de Hijos: existe una diferencia entre la Cotizacion y la Solicitud" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            fncComparaConCotizacion = false;
            return fncComparaConCotizacion;
          }

          if( ! ((null /*unsup wobjXMLNode.selectSingleNode( "NACIMHIJO" ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLNodeCotiz.selectSingleNode( "NACIMHIJO" ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLNode.selectSingleNode( "SEXOHIJO" ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLNodeCotiz.selectSingleNode( "SEXOHIJO" ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLNode.selectSingleNode( "ESTADOHIJO" ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLNodeCotiz.selectSingleNode( "ESTADOHIJO" ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLNode.selectSingleNode( "EDADHIJO" ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLNodeCotiz.selectSingleNode( "EDADHIJO" ) */ == (org.w3c.dom.Node) null)) )
          {
            if( ! ((diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( "NACIMHIJO" ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNodeCotiz.selectSingleNode( "NACIMHIJO" ) */ ) )) && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( "SEXOHIJO" ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNodeCotiz.selectSingleNode( "SEXOHIJO" ) */ ) )) && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( "ESTADOHIJO" ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNodeCotiz.selectSingleNode( "ESTADOHIJO" ) */ ) )) && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( "EDADHIJO" ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNodeCotiz.selectSingleNode( "EDADHIJO" ) */ ) ))) )
            {
              wvarChequeoHijos = false;
            }
          }
          else
          {
            wvarChequeoHijos = false;
          }
          wobjXMLNode = (org.w3c.dom.Node) null;
          wobjXMLNodeCotiz = (org.w3c.dom.Node) null;
        }
        wobjXMLNodeList = (org.w3c.dom.NodeList) null;
        wobjXMLListCotiz = (org.w3c.dom.NodeList) null;
      }


      //Chequeo si viene al menos un accesorio
      //  wvarStep = 111
      //  Set wobjXMLNodeList = wobjXMLRequest.selectNodes(mcteNodos_Accesorios)
      //  wvarStep = 112
      //  Set wobjXMLListCotiz = wobjXMLCotizacion.selectNodes(mcteNodos_Accesorios)
      //  wvarStep = 113
      wvarChequeoAccesorios = true;
      //  If Not wobjXMLNodeList Is Nothing And Not wobjXMLListCotiz Is Nothing Then
      //    wvarStep = 114
      //    For wvarcounter = 0 To (wobjXMLNodeList.length - 1)
      //      wvarStep = 115
      //      Set wobjXMLNode = wobjXMLNodeList(wvarcounter)
      //      wvarStep = 116
      //      Set wobjXMLNodeCotiz = wobjXMLCotizacion.selectNodes(mcteNodos_Accesorios).Item(wvarcounter)
      //
      //         wvarStep = 117
      //         If Not (wobjXMLNode.selectSingleNode("CODIGOACC") Is Nothing) And Not (wobjXMLNodeCotiz.selectSingleNode("CODIGOACC") Is Nothing) And _
      // '           Not (wobjXMLNode.selectSingleNode("DESCRIPCIONACC") Is Nothing) And Not (wobjXMLNodeCotiz.selectSingleNode("DESCRIPCIONACC") Is Nothing) And _
      //           Not (wobjXMLNode.selectSingleNode("PRECIOACC") Is Nothing) And Not (wobjXMLNodeCotiz.selectSingleNode("PRECIOACC") Is Nothing) _
      // '         Then
      //           wvarStep = 118
      //           If Not wobjXMLNode.selectSingleNode("PRECIOACC").Text = "0" And Not wobjXMLNode.selectSingleNode("PRECIOACC").Text = "" Then
      //            wvarStep = 119
      //            If Not (wobjXMLNode.selectSingleNode("CODIGOACC").Text = wobjXMLNodeCotiz.selectSingleNode("CODIGOACC").Text And _
      // '                  wobjXMLNode.selectSingleNode("DESCRIPCIONACC").Text = wobjXMLNodeCotiz.selectSingleNode("DESCRIPCIONACC").Text And _
      //                  wobjXMLNode.selectSingleNode("PRECIOACC").Text = wobjXMLNodeCotiz.selectSingleNode("PRECIOACC").Text) Then
      //                  wvarChequeoAccesorios = False
      //             End If
      //            End If
      //           Set wobjXMLNode = Nothing
      //           Set wobjXMLNodeCotiz = Nothing
      //         End If
      //    Next
      //  End If
      //Controla que la ultima grabacion tenga todos los nodos para comparar
      //jc 09/2010 se agregan coberturas nuevas y se cambian nombres de los tag productor e iva
      wvarStep = 120;
      if( (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_NacimAnn) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_NacimMes) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_NacimDia) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Sexo) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Estado) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_PRODUCTOR) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CampaCod) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_ModeAutCod) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_SUMAASEG) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_KMsrngCod) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_EfectAnn) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_SiGarage) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Siniestros) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Gas) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_ClubLBA) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Provi) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_VEHDES) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_DESTRUCCION_80) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Luneta) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_ClubEco) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Robocont) ) */ == (org.w3c.dom.Node) null) || (null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Granizo) ) */ == (org.w3c.dom.Node) null) )
      {
        wvarCodErr.set( -96 );
        wvarMensaje.set( "La ultima cotizacion se guardo con error debido a que falta un campo" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        fncComparaConCotizacion = false;
        return fncComparaConCotizacion;
      }
      //jc 09/2010 valida nuevas coberturas
      //jc como hay planes que no soportan las coberturas se debe validar el plan contra la marca, esta fijo como en OV
      //jc ojo que esta al limite de los or encadenados
      wvarStep = 125;
      //jc 08/2011 ANEXO I, x nvo. comp+ no se puede validar, se asume solo validacion x precio
      wvarChequeonuevas = true;
      // RC y RC+Robo no pueden luneta-granizo-robocont-dt80
      //        If wobjXMLRequest.selectSingleNode("//" & mcteParam_PLANNCOD).Text = "001" Or wobjXMLRequest.selectSingleNode("//" & mcteParam_PLANNCOD).Text = "005" Then
      //           If UCase(wobjXMLRequest.selectSingleNode("//" & mcteParam_DESTRUCCION_80).Text) = "S" Or _
      //  '              UCase(wobjXMLRequest.selectSingleNode("//" & mcteParam_Luneta).Text) = "S" Or _
      //              UCase(wobjXMLRequest.selectSingleNode("//" & mcteParam_Robocont).Text) = "S" Or _
      //  '              UCase(wobjXMLRequest.selectSingleNode("//" & mcteParam_Granizo).Text) = "S" _
      //           Then
      //              wvarChequeonuevas = False
      //           Else
      //              wvarChequeonuevas = True
      //           End If
      //        Else
      // TRCG y TT no puede dt80
      //           If wobjXMLRequest.selectSingleNode("//" & mcteParam_PLANNCOD).Text = "009" Or wobjXMLRequest.selectSingleNode("//" & mcteParam_PLANNCOD).Text = "003" Then
      //              If UCase(wobjXMLRequest.selectSingleNode("//" & mcteParam_DESTRUCCION_80).Text) = "S" Then
      //                   wvarChequeonuevas = False
      //              Else
      //                   wvarChequeonuevas = True
      //              End If
      //           Else
      //el resto puede todas las opcionales
      //              wvarChequeonuevas = True
      //           End If
      //        End If
      //fin jc se agregan en el paso 130
      wvarStep = 130;
      wvarCodErr.set( 0 );
      //A pedido de Inworx, si existe una diferencia entre la cotizacion y la
      //solicitud, debo especificar el campo de la diferencia
      if( ! ((diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimAnn) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_NacimAnn) ) */ ) )) && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimMes) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_NacimMes) ) */ ) )) && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimDia) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_NacimDia) ) */ ) ))) )
      {
        wvarCodErr.set( -150 );
        wvarMensaje.set( "Fecha de Nacimiento: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 132;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Sexo) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Sexo) ) */ ) )) )
      {
        wvarCodErr.set( -151 );
        wvarMensaje.set( "Sexo: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 133;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Estado) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Estado) ) */ ) )) )
      {
        wvarCodErr.set( -152 );
        wvarMensaje.set( "Estado Civil: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 134;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PRODUCTOR) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_PRODUCTOR) ) */ ) )) )
      {
        wvarCodErr.set( -153 );
        wvarMensaje.set( "Cod. de Productor: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 135;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CampaCod) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CampaCod) ) */ ) )) )
      {
        wvarCodErr.set( -154 );
        wvarMensaje.set( "Cod. de Campania: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 136;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ModeAutCod) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_ModeAutCod) ) */ ) )) )
      {
        wvarCodErr.set( -155 );
        wvarMensaje.set( "Modelo de Vehiculo: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 137;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SUMAASEG) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_SUMAASEG) ) */ ) )) )
      {
        wvarCodErr.set( -156 );
        wvarMensaje.set( "Suma Asegurada: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 138;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_KMsrngCod) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_KMsrngCod) ) */ ) )) )
      {
        wvarCodErr.set( -157 );
        wvarMensaje.set( "Rango de Kms.: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 139;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EfectAnn) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_EfectAnn) ) */ ) )) )
      {
        wvarCodErr.set( -158 );
        wvarMensaje.set( "Anio del Vehiculo: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 140;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SiGarage) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_SiGarage) ) */ ) )) )
      {
        wvarCodErr.set( -159 );
        wvarMensaje.set( "Guarda en Garage: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 141;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Siniestros) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Siniestros) ) */ ) )) )
      {
        wvarCodErr.set( -160 );
        wvarMensaje.set( "Cantidad de Siniestros: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 142;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Gas) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Gas) ) */ ) )) )
      {
        wvarCodErr.set( -161 );
        wvarMensaje.set( "Usa GNC: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 143;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClubLBA) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_ClubLBA) ) */ ) )) )
      {
        wvarCodErr.set( -162 );
        wvarMensaje.set( "Club LBA: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 144;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Provi) ) */ ) )) )
      {
        wvarCodErr.set( -163 );
        wvarMensaje.set( "Cod. de Provincia: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 145;
      if( ! (wvarChequeoLocalidad) )
      {
        wvarCodErr.set( -164 );
        wvarMensaje.set( "Cod. de Localidad: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 146;
      if( ! (wvarChequeoHijos) )
      {
        wvarCodErr.set( -165 );
        wvarMensaje.set( "Datos de Hijos: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 147;
      if( ! (wvarChequeoAccesorios) )
      {
        wvarCodErr.set( -166 );
        wvarMensaje.set( "Datos de Accesorios: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 148;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) )) )
      {
        wvarCodErr.set( -167 );
        wvarMensaje.set( "Cod. de Cobro: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 149;
      if( ! (wvarChequeoTarjeta) )
      {
        wvarCodErr.set( -168 );
        wvarMensaje.set( "Datos de Tarjeta: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 150;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ) )) )
      {
        wvarCodErr.set( -169 );
        wvarMensaje.set( "Responsabilidad frente al IVA: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 151;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VEHDES) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_VEHDES) ) */ ) )) )
      {
        wvarCodErr.set( -170 );
        wvarMensaje.set( "Descripcion del Vehiculo: existe una diferencia entre la Cotizacion y la Solicitud" );
      }
      wvarStep = 152;
      //jc 09/2010 se agregan nuevas cob solo puede caer aqui si no correspondio al plan
      if( ! (wvarChequeonuevas) )
      {
        if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DESTRUCCION_80) ) */ ) ).equals( "S" ) )
        {
          wvarCodErr.set( -171 );
          wvarMensaje.set( "Destruc.80: El plan seleccionado no permite esta cobertura opcional" );
        }
        else
        {
          wvarStep = 153;
          if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Luneta) ) */ ) ).equals( "S" ) )
          {
            wvarCodErr.set( -172 );
            wvarMensaje.set( "Luneta: El plan seleccionado no permite esta cobertura opcional" );
          }
          else
          {
            wvarStep = 154;
            if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Robocont) ) */ ) ).equals( "S" ) )
            {
              wvarCodErr.set( -174 );
              wvarMensaje.set( "Robo Cont.: El plan seleccionado no permite esta cobertura opcional" );
            }
            else
            {
              wvarStep = 155;
              if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Granizo) ) */ ) ).equals( "S" ) )
              {
                wvarCodErr.set( -175 );
                wvarMensaje.set( "Granizo: El plan seleccionado no permite esta cobertura opcional" );
              }
            }
          }
        }
      }
      if( wvarCodErr.toInt() < 0 )
      {
        wvarStep = 156;
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        fncComparaConCotizacion = false;
        return fncComparaConCotizacion;
      }
      //fin jc
      wvarStep = 157;
      //11/2010 se agregan validaciones x distinto iva, nodos no obligatorios en la cotiz.
      if( ! ((null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENTIP) ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CLIENTIP) ) */ == (org.w3c.dom.Node) null)) )
      {
        wvarStep = 158;
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENTIP) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CLIENTIP) ) */ ) )) )
        {
          wvarCodErr.set( -181 );
          wvarMensaje.set( "Tipo de Persona: existe una diferencia entre la Cotizacion y la Solicitud" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          fncComparaConCotizacion = false;
          return fncComparaConCotizacion;
        }
      }
      wvarStep = 159;
      if( ! ((null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_IBB) ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_IBB) ) */ == (org.w3c.dom.Node) null)) )
      {
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_IBB) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_IBB) ) */ ) )) )
        {
          wvarCodErr.set( -181 );
          wvarMensaje.set( "Tipo de IBB: existe una diferencia entre la Cotizacion y la Solicitud" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          fncComparaConCotizacion = false;
          return fncComparaConCotizacion;
        }
      }
      //fin distintos iva
      pvarRes.set( pvarRequest );
      //
      wrstRes.close();
      wobjDBCnn.close();

      //Llama al cotizador
      //Cotiza nuevamente
      //jc 09/2010 se llama al componente actual y ademas hay que agregar nodo
      wvarStep = 160;
      mvarDatosPlan = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PLANNCOD ) */ ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_FRANQCOD) ) */ );
      pvarRequest = Strings.replace( pvarRequest, "</Request>", "<DATOSPLAN>" + mvarDatosPlan + "</DATOSPLAN></Request>" );
      //Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVLBAMQ.lbaws_GetCotisAUS")
      wobjClass = new lbawA_OVMQCotizar.lbaw_OVGetCotiAUS();
      wobjClass.Execute( pvarRequest, wvarResponse, "" );
      wobjClass = null;

      wobjXMLRequestCotis = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequestCotis.async = false;
      wobjXMLRequestCotis.loadXML( wvarResponse );

      wvarStep = 170;
      //jc 09/2010 cambia la salida del componente
      //If wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_code").Text = "OK" Then
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestCotis.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
      {

        wobjXMLValid = new diamondedge.util.XmlDom();
        //unsup wobjXMLValid.async = false;
        wobjXMLValid.load( System.getProperty("user.dir") + "\\" + mcteArchivoAUSSOL_XML );

        //Chequea si busca el precio con Hijos o sin Hijos
        //08/2011 ANEXO I cambia el formato del componente de cotiz.
        wvarStep = 2180;
        if( null /*unsup wobjXMLRequest.selectNodes( "//HIJOS/HIJO" ) */.getLength() > 0 )
        {
          wvarTieneHijos = "_CH";
          wvarNuevoHijos = "CON_HIJOS";
        }
        else
        {
          wvarTieneHijos = "_SH";
          wvarNuevoHijos = "SIN_HIJOS";
        }

        //Si existe el plan
        //Chequea si tiene el mismo importe la Solicitud con la Cotizacion
        wvarStep = 2181;
        //jc 09/2010 cambia la salida del componente (para cualquier plan el importe viene en RC_SH RC_CH
        //If Not Val(wobjXMLRequest.selectSingleNode("//PRECIO").Text) = Val(wobjXMLRequestCotis.selectSingleNode("//PRECIOPLAN" & wvarTieneHijos).Text) Then
        //08/2011 ANEXO I
        //If Not Val(wobjXMLRequest.selectSingleNode("//PRECIO").Text) = Val(wobjXMLRequestCotis.selectSingleNode("//RC" & wvarTieneHijos).Text) Then
        if( ! (VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//PRECIO" ) */ ) ) == VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestCotis.selectSingleNode( ("//" + wvarNuevoHijos + "/PRECIO") ) */ ) )) )
        {
          fncComparaConCotizacion = false;
          pvarRes.set( "<LBA_WS res_code=\"-550\" res_msg=\"Existe una diferencia de precio entre la Solicitud(" + VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//PRECIO" ) */ ) ) + ")y la Cotizacion(" + VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestCotis.selectSingleNode( ("//" + wvarNuevoHijos + "/PRECIO") ) */ ) ) + ")\"></LBA_WS>" );
          return fncComparaConCotizacion;
        }
        else
        {
          wvarStep = 2182;
          //Lea agrega los valores de la cotizacion
          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "PRECIOPLAN" + wvarTieneHijos, "" ) */ );
          //jc 09/2010 cambia componente
          //wobjXMLRequest.selectSingleNode("//PRECIOPLAN" & wvarTieneHijos).Text = wobjXMLRequestCotis.selectSingleNode("//PRECIOPLAN" & wvarTieneHijos).Text
          wvarStep = 2183;
          diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//PRECIOPLAN" + wvarTieneHijos ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestCotis.selectSingleNode( "//" + wvarNuevoHijos + "/PRECIO" ) */ ) );

          for( wvarcounter = 1; wvarcounter <= 30; wvarcounter++ )
          {
            wvarStep = 2184;

            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "COBERCOD" + wvarcounter, "" ) */ );
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//COBERCOD" + wvarcounter ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestCotis.selectSingleNode( "//COBERCOD" + wvarcounter ) */ ) );

            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "COBERORD" + wvarcounter, "" ) */ );
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//COBERORD" + wvarcounter ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestCotis.selectSingleNode( "//COBERORD" + wvarcounter ) */ ) );

            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CAPITASG" + wvarcounter, "" ) */ );
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//CAPITASG" + wvarcounter ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestCotis.selectSingleNode( "//CAPITASG" + wvarcounter ) */ ) );

            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CAPITIMP" + wvarcounter, "" ) */ );
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//CAPITIMP" + wvarcounter ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestCotis.selectSingleNode( "//CAPITIMP" + wvarcounter ) */ ) );
          }
          pvarRes.set( "<Response><Estado resultado=\"true\" mensaje=\"\"/>" + wobjXMLRequest.getDocument().getDocumentElement().toString() + "</Response>" );
        }
      }

      wvarStep = 2190;
      if( ! (wobjXMLRequestCotis == (diamondedge.util.XmlDom) null) )
      {
        //jc 09/2010 cambia la salida del componente
        //If Not UCase(wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_code").Text) = "OK" Then
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestCotis.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
        {
          fncComparaConCotizacion = false;
        }
        else
        {
          fncComparaConCotizacion = true;
        }
      }
      else
      {
        fncComparaConCotizacion = true;
      }


      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return fncComparaConCotizacion;


      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );
        wvarCodErr.set( -1000 );
        wvarMensaje.set( General.mcteErrorInesperadoDescr );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncComparaConCotizacion = false;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncComparaConCotizacion;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
