package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class GetSolicitudAU implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  static final String mcteClassName = "LBA_InterWSBrok.GetSolicitudAU";
  static final String mcteStoreProc = "SPSNCV_BRO_SOLI_AUS1_GRABA";
  static final String mcteStoreProcSelect = "SPSNCV_BRO_SELECT_OPER_ESP";
  static final String mcteStoreProcAltaOperacion = "SPSNCV_BRO_ALTA_OPER";
  /**
   *  Parametros XML de Entrada
   */
  static final String mcteParam_WDB_IDENTIFICACION_BROKER = "//CODINST";
  static final String mcteParam_WDB_NRO_OPERACION_BROKER = "//REQUESTID";
  static final String mcteParam_WDB_NRO_COTIZACION_LBA = "//COT_NRO";
  static final String mcteParam_WDB_TIPO_OPERACION = "//TIPOOPERACION";
  /**
   * jc 09/2010 al re-utlizar el componente nuevo cambia el nombre de los tag
   * Const mcteParam_PRODUCTOR                   As String = "//PRODUCTOR"
   */
  static final String mcteParam_PRODUCTOR = "//AGECOD";
  static final String mcteParam_NumeDocu = "//NUMEDOCU";
  static final String mcteParam_TipoDocu = "//TIPODOCU";
  static final String mcteParam_ClienApe1 = "//CLIENAP1";
  static final String mcteParam_ClienApe2 = "//CLIENAP2";
  static final String mcteParam_ClienNom1 = "//CLIENNOM";
  static final String mcteParam_NacimAnn = "//NACIMANN";
  static final String mcteParam_NacimMes = "//NACIMMES";
  static final String mcteParam_NacimDia = "//NACIMDIA";
  static final String mcteParam_CLIENSEX = "//SEXO";
  static final String mcteParam_CLIENEST = "//ESTADO";
  static final String mcteParam_EMAIL = "//EMAIL";
  /**
   * Domicilio de riesgo
   */
  static final String mcteParam_DOMICDOM = "//DOMICDOM";
  static final String mcteParam_DomicDnu = "//DOMICDNU";
  static final String mcteParam_DOMICPIS = "//DOMICPIS";
  static final String mcteParam_DomicPta = "//DOMICPTA";
  static final String mcteParam_DOMICPOB = "//DOMICPOB";
  static final String mcteParam_DOMICCPO = "//LOCALIDADCOD";
  static final String mcteParam_PROVICOD = "//PROVI";
  /**
   * jc 09/2010 se agrega codigo de �rea
   */
  static final String mcteParam_TELCOD = "//TELCOD";
  static final String mcteParam_TELNRO = "//TELNRO";
  /**
   * datos del domicilio de correspondencia
   */
  static final String mcteParam_DOMICDOM_CO = "//DOMICDOMCOR";
  static final String mcteParam_DomicDnu_CO = "//DOMICDNUCOR";
  static final String mcteParam_DOMICPIS_CO = "//DOMICPISCOR";
  static final String mcteParam_DomicPta_CO = "//DOMICPTACOR";
  static final String mcteParam_DOMICPOB_CO = "//DOMICPOBCOR";
  static final String mcteParam_DOMICCPO_CO = "//LOCALIDADCODCOR";
  static final String mcteParam_PROVICOD_CO = "//PROVICOR";
  /**
   * jc 09/2010 se agrega codigo de �rea
   */
  static final String mcteParam_TELCOD_CO = "//TELCODCOR";
  static final String mcteParam_TELNRO_CO = "//TELNROCOR";
  /**
   * datos de la cuenta
   */
  static final String mcteParam_COBROTIP = "//COBROTIP";
  static final String mcteParam_CUENNUME = "//CUENNUME";
  static final String mcteParam_VENCIANN = "//VENCIANN";
  static final String mcteParam_VENCIMES = "//VENCIMES";
  static final String mcteParam_VENCIDIA = "//VENCIDIA";
  /**
   * datos de la operacion
   */
  static final String mcteParam_FRANQCOD = "//FRANQCOD";
  static final String mcteParam_PLANCOD = "//PLANNCOD";
  static final String mcteParam_ZONA = "//CODZONA";
  /**
   * jc 09/2010 al re-utlizar el componente nuevo cambia el nombre de los tag
   * Const mcteParam_CLIENIVA                    As String = "//CLIENIVA"
   */
  static final String mcteParam_CLIENIVA = "//IVA";
  static final String mcteParam_SUMAASEG = "//SUMAASEG";
  static final String mcteParam_CLUB_LBA = "//CLUBLBA";
  static final String mcteParam_CPAANO = "//CPAANO";
  static final String mcteParam_CTAKMS = "//KMSRNGCOD";
  static final String mcteParam_Escero = "//ESCERO";
  static final String mcteParam_SIFMVEHI_DES = "//VEHDES";
  static final String mcteParam_MOTORNUM = "//MOTORNUM";
  static final String mcteParam_CHASINUM = "//CHASINUM";
  static final String mcteParam_PATENNUM = "//PATENNUM";
  /**
   * AUMARCOD+AUMODCOD+AUSUBCOD+AUADICODAUMODORI
   */
  static final String mcteParam_ModeAutCod = "//MODEAUTCOD";
  static final String mcteParam_VEHCLRCOD = "//VEHCLRCOD";
  static final String mcteParam_EfectAnn = "//EFECTANN";
  static final String mcteParam_GUGARAGE = "//SIGARAGE";
  static final String mcteParam_GUDOMICI = "//GUDOMICI";
  static final String mcteParam_AUCATCOD = "//AUCATCOD";
  static final String mcteParam_AUTIPCOD = "//AUTIPCOD";
  static final String mcteParam_AUANTANN = "//AUANTANN";
  static final String mcteParam_AUNUMSIN = "//SINIESTROS";
  static final String mcteParam_AUUSOGNC = "//GAS";
  static final String mcteParam_CampaCod = "//CAMPACOD";
  static final String mcteParam_CONDUCTORES = "//HIJOS/HIJO";
  static final String mcteParam_CONDUAPE = "APELLIDOHIJO";
  static final String mcteParam_CONDUNOM = "NOMBREHIJO";
  static final String mcteParam_CONDUFEC = "NACIMHIJO";
  static final String mcteParam_CONDUSEX = "SEXOHIJO";
  static final String mcteParam_CONDUEST = "ESTADOHIJO";
  static final String mcteParam_CONDUEXC = "INCLUIDO";
  static final String mcteParam_ACCESORIOS = "//ACCESORIOS/ACCESORIO";
  static final String mcteParam_AUACCCOD = "CODIGOACC";
  static final String mcteParam_AUVEASUM = "PRECIOACC";
  static final String mcteParam_AUVEADES = "DESCRIPCIONACC";
  static final String mcteParam_INSPECOD = "//INSPECOD";
  static final String mcteParam_RASTREO = "//RASTREO";
  static final String mcteParam_ALARMCOD = "//ALARMCOD";
  static final String mcteParam_COBROCOD = "//COBROCOD";
  static final String mcteParam_ANOTACIO = "//ANOTACIONES";
  static final String mcteParam_VIGENANN = "//VIGENANN";
  static final String mcteParam_VIGENMES = "//VIGENMES";
  static final String mcteParam_VIGENDIA = "//VIGENDIA";
  /**
   * hora en que empezo la transaccion
   */
  static final String mcteParam_MSGANO = "//MSGANO";
  static final String mcteParam_MSGMES = "//MSGMES";
  static final String mcteParam_MSGDIA = "//MSGDIA";
  static final String mcteParam_MSGHORA = "//MSGHORA";
  static final String mcteParam_MSGMINUTO = "//MSGMINUTO";
  static final String mcteParam_MSGSEGUNDO = "//MSGSEGUNDO";
  /**
   * Datos de la cotizacion
   */
  static final String mcteParm_TIEMPO = "//TIEMPO";
  /**
   * jc 8/2010 agregado xml
   */
  static final String mcteParam_DESTRUCCION_80 = "//DESTRUCCION_80";
  static final String mcteParam_Luneta = "//LUNETA";
  static final String mcteParam_ClubEco = "//CLUBECO";
  static final String mcteParam_Robocont = "//ROBOCONT";
  static final String mcteParam_Granizo = "//GRANIZO";
  static final String mcteParam_IBB = "//IBB";
  static final String mcteParam_NROIBB = "//NROIBB";
  static final String mcteParam_INSTALADP = "//INSTALADP";
  static final String mcteParam_POSEEDISP = "//POSEEDISP";
  /**
   *  11/2010 se permiten distintos tipos de iva
   */
  static final String mcteParam_CUITNUME = "//CUITNUME";
  static final String mcteParam_RAZONSOC = "//RAZONSOC";
  static final String mcteParam_CLIENTIP = "//CLIENTIP";
  static final String mcteParam_ACREPREN = "//ACRE-PREN";
  static final String mcteParam_NUMEDOCUACRE = "//NUMEDOCU-ACRE";
  static final String mcteParam_TIPODOCUACRE = "//TIPODOCU-ACRE";
  static final String mcteParam_APELLIDOACRE = "//APELLIDO-ACRE";
  static final String mcteParam_NOMBRESACRE = "//NOMBRES-ACRE";
  static final String mcteParam_DOMICDOMACRE = "//DOMICDOM-ACRE";
  static final String mcteParam_DOMICDNUACRE = "//DOMICDNU-ACRE";
  static final String mcteParam_DOMICPISACRE = "//DOMICPIS-ACRE";
  static final String mcteParam_DOMICPTAACRE = "//DOMICPTA-ACRE";
  static final String mcteParam_DOMICPOBACRE = "//DOMICPOB-ACRE";
  static final String mcteParam_DOMICCPOACRE = "//DOMICCPO-ACRE";
  static final String mcteParam_PROVICODACRE = "//PROVICOD-ACRE";
  static final String mcteParam_PAISSCODACRE = "//PAISSCOD-ACRE";
  /**
   * AM
   */
  static final String mcteParam_BANCOCOD = "//BANCOCOD";
  static final String mcteParam_EmpresaCodigo = "//CANAL";
  static final String mcteParam_SucursalCodigo = "//CANAL_SUCURSAL";
  static final String mcteParam_LegajoVend = "//LEGAJO_VEND";
  static final String mcteParam_LegajoCia = "//LEGAJCIA";
  /**
   * Nro NewSas para mcteParam_EmpresaCodigo(CANAL) = 150 (HSBC)
   */
  static final String mcteParam_NROSOLI = "//NROSOLI";
  /**
   * Asegurados Adicionales
   */
  static final String mcteParam_ASEGADIC = "//ASEG-ADIC";
  static final String mcteParam_ASEGURADOS = "//ASEGURADOS-ADIC/ASEGURADO-ADIC";
  static final String mcteParam_DOCUMASEG = "DOCUMASEG";
  static final String mcteParam_NOMBREASEG = "NOMBREASEG";
  static final String mcteParam_NROFACTU = "//NROFACTU";
  static final String mcteParam_CENSICOD = "//CENSICOD";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncGrabaSolicitud
   */
  private final String wcteFnName = "fncGrabaSolicitud";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    Variant mcteErrorInesperadoDescr = new Variant();
    int wvarStep = 0;
    String wvarMensaje = "";
    Variant pvarRes = new Variant();
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      pvarRequest = Strings.mid( pvarRequest, 1, Strings.find( 1, pvarRequest, ">" ) ) + "<MSGANO>" + DateTime.year( DateTime.now() ) + "</MSGANO>" + "<MSGMES>" + DateTime.month( DateTime.now() ) + "</MSGMES>" + "<MSGDIA>" + DateTime.day( DateTime.now() ) + "</MSGDIA>" + "<MSGHORA>" + DateTime.hour( DateTime.now() ) + "</MSGHORA>" + "<MSGMINUTO>" + DateTime.minute( DateTime.now() ) + "</MSGMINUTO>" + "<MSGSEGUNDO>" + DateTime.second( DateTime.now() ) + "</MSGSEGUNDO>" + "<TIPOOPERACION>S</TIPOOPERACION>" + Strings.mid( pvarRequest, (Strings.find( 1, pvarRequest, ">" ) + 1) );
      pvarRes.set( "" );
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        wvarStep = 20;
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }
      wvarStep = 30;
      pvarResponse.set( pvarRes );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"-1000\" res_msg=\"" + mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( String pvarRequest, String wvarMensaje, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    Variant mcteErrorInesperadoDescr = new Variant();
    Variant mcteDB = new Variant();
    int wvarStep = 0;
    String wvarMensaje2 = "";
    Variant wvarMensaje3 = new Variant();
    String wvarMensajeI = "";
    String wvarInputImpre = "";
    Object wobjClass = null;
    HSBC.DBConnection wobjHSBC_DBCnn = new HSBC.DBConnection();
    diamondedge.util.XmlDom wobjXMLRequestExists = null;
    diamondedge.util.XmlDom wobjXMLRequestSolis = null;
    diamondedge.util.XmlDom wobjXMLError = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    Variant mvarCertiSec = new Variant();
    String mvarWDB_ESTADO = "";
    String mvarWDB_IDENTIFICACION_BROKER = "";
    String mvarWDB_NRO_OPERACION_BROKER = "";
    String mvarWDB_NRO_COTIZACION_LBA = "";
    String mvarWDB_TIPO_OPERACION = "";
    String mvarWDB_CERTIANN = "";
    String mvarWDB_RECEPCION_PEDIDOANO = "";
    String mvarWDB_RECEPCION_PEDIDOMES = "";
    String mvarWDB_RECEPCION_PEDIDODIA = "";
    String mvarWDB_RECEPCION_PEDIDOHORA = "";
    String mvarWDB_RECEPCION_PEDIDOMINUTO = "";
    String mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
    String mvarWDB_ENVIO_RESPUESTA = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_TIEMPO_PROCESO_AIS = "";
    String mvarWDB_OPERACION_XML = "";
    String mvarEmpresaCodigo = "";
    String mvarSucursalCodigo = "";
    String mvarLegajoVend = "";
    String mvarLegajoCia = "";
    String mvarNROSOLI = "";
    Variant mvarseltest = new Variant();
    int wvarCount = 0;

    //jc 09/2010 variables para la clase de impresion
    //AM
    try 
    {

      wvarStep = 10;
      //jc 09/2010
      wvarInputImpre = pvarRequest;

      mvarCertiSec.set( 0 );
      //Llama a validacion
      wobjClass = new Variant( new com.qbe.services.GetValidacionSolAU() )new com.qbe.services.GetValidacionSolAU().toObject();
      wobjClass.Execute( pvarRequest, wvarMensaje2, "" );
      wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;

      wvarStep = 20;
      //Si esta todo OK Llama a la grabacion de solicitud
      wobjXMLRequestSolis = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequestSolis.async = false;
      wobjXMLRequestSolis.loadXML( wvarMensaje2 );

      wvarStep = 30;
      //Si el mensaje viene con error y tiene otro encabezado, lo fuerza a ER
      if( null /*unsup wobjXMLRequestSolis.selectSingleNode( "//Estado/@resultado" ) */ == (org.w3c.dom.Node) null )
      {
        mvarWDB_ESTADO = "ER";
      }
      else
      {
        mvarWDB_ESTADO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( "//Estado/@resultado" ) */ );
      }

      if( ! (Strings.toUpperCase( mvarWDB_ESTADO ).equals( "TRUE" )) )
      {
        wvarStep = 40;
        wobjXMLRequestExists = new diamondedge.util.XmlDom();
        //unsup wobjXMLRequestExists.async = false;
        wobjXMLRequestExists.loadXML( pvarRequest );
        //Si el Error es C010, entoces es una Solicitud que est� Ok.
        //Busco el CertiSec y Guardo la Operacion unicamente
        //Sino, lo trato como error
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( "//LBA_WS/@res_code" ) */ ).equals( "C010" ) )
        {
          wobjHSBC_DBCnn = new HSBC.DBConnection();
          //error: function 'GetDBConnection' was not found.
          //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
          wobjDBCmd = new Command();
          wobjDBCmd.setActiveConnection( wobjDBCnn );
          wobjDBCmd.setCommandText( mcteStoreProcSelect );
          wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
          //
          wvarStep = 50;
          wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestExists.selectSingleNode( "//CODINST" ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 4 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wvarStep = 51;
          wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestExists.selectSingleNode( "//REQUESTID" ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 14 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wvarStep = 52;
          wobjDBParm = new Parameter( "@WDB_TIPO_OPERACION", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "S" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wvarStep = 53;
          wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "OK" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wvarStep = 54;
          wrstRes = new Recordset();
          wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
          pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + "<Response>" + "<REQUESTID>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestExists.selectSingleNode( "//REQUESTID" ) */ ) + "</REQUESTID>" + "<CERTISEC>" + wrstRes.getFields().getField(12).getValue() + "</CERTISEC>" + "</Response>" + "</LBA_WS>" );
          mvarCertiSec.set( wrstRes.getFields().getField(12).getValue() );
          mvarWDB_ESTADO = "OK";
          wobjXMLRequestExists = (diamondedge.util.XmlDom) null;
          wobjDBCmd = (Command) null;
          wobjDBCnn = (Connection) null;
          wobjHSBC_DBCnn = (HSBC.DBConnection) null;
        }
        else
        {
          if( Strings.left( mvarWDB_ESTADO, 1 ).equals( "-" ) )
          {
            mvarWDB_ESTADO = "ER";
          }
          pvarRes.set( wvarMensaje2 );
        }
        //Es un error distinto de C010
        wobjXMLRequestSolis = (diamondedge.util.XmlDom) null;
        wobjXMLRequestSolis = new diamondedge.util.XmlDom();
        //unsup wobjXMLRequestSolis.async = false;
        wobjXMLRequestSolis.loadXML( pvarRequest );
      }
      else
      {
        wvarStep = 50;
        if( Strings.toUpperCase( mvarWDB_ESTADO ).equals( "TRUE" ) )
        {
          //Grabo la solicitud
          mvarWDB_ESTADO = "OK";
          wvarStep = 70;

          mvarCertiSec.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( "//CERTISEC" ) */ ) );

          if( ! (invoke( "fncGrabaSolicitud", new Variant[] { new Variant(wvarMensaje2), new Variant(wvarMensaje3), new Variant(mvarCertiSec) } )) )
          {
            wvarStep = 71;
            pvarRes.set( "<LBA_WS res_code=\"-215\" res_msg=\"Error al intentar obtener el numero de poliza\"></LBA_WS>" );
            mvarWDB_ESTADO = "ER";
          }
          else
          {
            wvarStep = 72;
            if( mvarCertiSec.toInt() >= 0 )
            {

              mvarEmpresaCodigo = "0";
              if( ! (null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_EmpresaCodigo ) */ == (org.w3c.dom.Node) null) )
              {
                mvarEmpresaCodigo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_EmpresaCodigo ) */ );
              }
              mvarSucursalCodigo = "0";
              if( ! (null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_SucursalCodigo ) */ == (org.w3c.dom.Node) null) )
              {
                mvarSucursalCodigo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_SucursalCodigo ) */ );
              }

              //AM se saca el impreso para HSBC
              if( ! (Obj.toInt( mvarEmpresaCodigo ) == 150) )
              {
                //jc 09/2010 se agrega llamado a la generaci�n del impreso
                wvarInputImpre = Strings.replace( wvarInputImpre, "</Request>", "<CERTISEC>" + mvarCertiSec + "</CERTISEC></Request>" );
                wvarStep = 73;
                wobjClass = new LBA_InterWSBrok.GetImpreSolAU();
                wvarStep = 74;
                //error: function 'Execute' was not found.
                //unsup: Call wobjClass.Execute(wvarInputImpre, wvarMensajeI, "")
                wvarStep = 75;
                wobjClass = null;
                //jc fin impreso
              }
              else
              {
                wvarMensajeI = "";
              }
              pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + "<Response>" + "<REQUESTID>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( "//REQUESTID" ) */ ) + "</REQUESTID>" + "<CERTISEC>" + mvarCertiSec + "</CERTISEC>" + "<PDF64>" + wvarMensajeI + "</PDF64>" + "</Response>" + "</LBA_WS>" );
            }
            else
            {
              pvarRes.set( "<LBA_WS res_code=\"-215\" res_msg=\"Error al intentar obtener el numero de poliza\"></LBA_WS>" );
            }
          }
        }
      }
      //Grabo la operacion
      wvarStep = 80;
      mvarWDB_IDENTIFICACION_BROKER = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_WDB_IDENTIFICACION_BROKER ) */ );
      mvarWDB_NRO_OPERACION_BROKER = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_WDB_NRO_OPERACION_BROKER ) */ );
      //jc 09/2010 se lleva igual que los otros campos
      mvarWDB_NRO_COTIZACION_LBA = "0";

      if( ! (null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_WDB_NRO_COTIZACION_LBA ) */ == (org.w3c.dom.Node) null) )
      {
        if( new Variant( (Object) null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_WDB_NRO_COTIZACION_LBA ) */ ).isNumeric() )
        {
          mvarWDB_NRO_COTIZACION_LBA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_WDB_NRO_COTIZACION_LBA ) */ );
        }
      }

      mvarWDB_TIPO_OPERACION = "S";

      wvarStep = 90;
      mvarWDB_RECEPCION_PEDIDOANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_MSGANO ) */ );
      mvarWDB_RECEPCION_PEDIDOMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_MSGMES ) */ );
      mvarWDB_RECEPCION_PEDIDODIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_MSGDIA ) */ );
      mvarWDB_RECEPCION_PEDIDOHORA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_MSGHORA ) */ );
      mvarWDB_RECEPCION_PEDIDOMINUTO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_MSGMINUTO ) */ );
      mvarWDB_RECEPCION_PEDIDOSEGUNDO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParam_MSGSEGUNDO ) */ );
      mvarWDB_TIEMPO_PROCESO_AIS = "0";
      if( ! (null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParm_TIEMPO ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIEMPO_PROCESO_AIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestSolis.selectSingleNode( mcteParm_TIEMPO ) */ );
      }

      wvarStep = 100;
      mvarWDB_OPERACION_XML = pvarRequest;

      wobjHSBC_DBCnn = new HSBC.DBConnection();
      wobjDBCnn = (Connection) (Connection)( wobjHSBC_DBCnn.GetDBConnection( mcteDB ) );
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProcAltaOperacion );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 110;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_IDENTIFICACION_BROKER ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 120;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_NRO_OPERACION_BROKER ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 130;
      wobjDBParm = new Parameter( "@WDB_NRO_COTIZACION_LBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_NRO_COTIZACION_LBA ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 18 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 140;
      wobjDBParm = new Parameter( "@WDB_TIPO_OPERACION", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarWDB_TIPO_OPERACION ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 150;
      if( (Strings.len( mvarWDB_ESTADO ) >= 3) || (Strings.left( mvarWDB_ESTADO, 1 ).equals( "-" )) )
      {
        mvarWDB_ESTADO = "ER";
      }

      wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarWDB_ESTADO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 160;
      wobjDBParm = new Parameter( "@WDB_RECEPCION_PEDIDO", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant( mvarWDB_RECEPCION_PEDIDOANO + "-" + mvarWDB_RECEPCION_PEDIDOMES + "-" + mvarWDB_RECEPCION_PEDIDODIA + " " + mvarWDB_RECEPCION_PEDIDOHORA + ":" + mvarWDB_RECEPCION_PEDIDOMINUTO + ":" + mvarWDB_RECEPCION_PEDIDOSEGUNDO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 170;
      wobjDBParm = new Parameter( "@WDB_ENVIO_RESPUESTA", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant( DateTime.year( DateTime.now() ) + "-" + DateTime.month( DateTime.now() ) + "-" + DateTime.day( DateTime.now() ) + " " + DateTime.hour( DateTime.now() ) + ":" + DateTime.minute( DateTime.now() ) + ":" + DateTime.second( DateTime.now() ) ) );

      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 180;
      wobjDBParm = new Parameter( "@WDB_RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "AUS1" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 190;
      wobjDBParm = new Parameter( "@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 201;
      // Solo para el HSBC
      mvarWDB_CERTIANN = "0";
      if( mvarEmpresaCodigo.equals( "150" ) )
      {
        mvarWDB_IDENTIFICACION_BROKER = "150";
        mvarWDB_CERTIANN = mvarSucursalCodigo;
      }


      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toInt( mvarWDB_IDENTIFICACION_BROKER ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toInt( mvarWDB_CERTIANN ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 230;
      wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, mvarCertiSec );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 240;
      wobjDBParm = new Parameter( "@WDB_TIEMPO_PROCESO_AIS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_TIEMPO_PROCESO_AIS ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 250;
      wobjDBParm = new Parameter( "@WDB_OPERACION_XML", AdoConst.adVarChar, AdoConst.adParamInput, 8000, (mvarWDB_OPERACION_XML.equals( "" ) ? new Variant((Object)null) : new Variant(Strings.left( mvarWDB_OPERACION_XML, 8000 ))) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 260;
      mvarseltest.set( mcteStoreProc + " " );
      for( wvarCount = 0; wvarCount <= (wobjDBCmd.getParameters().getCount() - 1); wvarCount++ )
      {
        if( (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adChar) || (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adVarChar) )
        {
          mvarseltest.set( mvarseltest + "'" + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + "'," );
        }
        else
        {
          mvarseltest.set( String.valueOf( mvarseltest ) + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + "," );
        }
      }
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );

      wobjDBCnn.close();

      fncGetAll = true;
      fin: 
      //libero los objectos
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = (HSBC.DBConnection) null;
      wobjXMLRequestSolis = (diamondedge.util.XmlDom) null;
      wobjXMLRequestExists = (diamondedge.util.XmlDom) null;
      wobjClass = null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"-1000\" res_msg=\"" + mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;



        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }

  private boolean fncGrabaSolicitud( String pvarXMLrequest, Variant pvarXMLresponse, Variant pvarCertiSec )
  {
    boolean fncGrabaSolicitud = false;
    Variant vbLogEventTypeError = new Variant();
    Variant mcteErrorInesperadoDescr = new Variant();
    Variant mcteDB = new Variant();
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Object wobjClass = null;
    diamondedge.util.XmlDom mobjXMLDoc = null;
    String mvarRequest = "";
    String mvarResponse = "";
    int wvarCount = 0;
    String mvarWDB_IDENTIFICACION_BROKER = "";
    String mvarWDB_NRO_OPERACION_BROKER = "";
    String mvarWDB_NRO_COTIZACION_LBA = "";
    String mvarWDB_TIPO_OPERACION = "";
    String mvarPRODUCTOR = "";
    String mvarNUMEDOCU = "";
    String mvarTipoDocu = "";
    String mvarClienApe1 = "";
    String mvarClienApe2 = "";
    String mvarClienNom1 = "";
    String mvarNacimAnn = "";
    String mvarNacimMes = "";
    String mvarNacimDia = "";
    String mvarCLIENSEX = "";
    String mvarCLIENEST = "";
    String mvarNUMHIJOS = "";
    String mvarEMAIL = "";
    String mvarDOMICDOM = "";
    String mvarDomicDnu = "";
    String mvarDOMICPIS = "";
    String mvarDomicPta = "";
    String mvarDOMICPOB = "";
    String mvarDOMICCPO = "";
    String mvarPROVICOD = "";
    String mvarTELCOD = "";
    String mvarTELNRO = "";
    String mvarPRINCIPAL = "";
    String mvarDOMICDOM_CO = "";
    String mvarDOMICDNU_CO = "";
    String mvarDOMICPIS_CO = "";
    String mvarDOMICPTA_CO = "";
    String mvarDOMICPOB_CO = "";
    String mvarDOMICCPO_CO = "";
    String mvarPROVICOD_CO = "";
    String mvarTELCOD_CO = "";
    String mvarTELNRO_CO = "";
    String mvarPRINCIPAL_CO = "";
    String mvarCOBROTIP = "";
    String mvarCUENNUME = "";
    String mvarVENCIANN = "";
    String mvarVENCIMES = "";
    String mvarVENCIDIA = "";
    String mvarBANCOCOD = "";
    String mvarSUCURCOD = "";
    String mvarFRANQCOD = "";
    String mvarPLANCOD = "";
    String mvarZONA = "";
    String mvarCLIENIVA = "";
    String mvarSUMAASEG = "";
    String mvarCLUB_LBA = "";
    String mvarDESTRUCCION_80 = "";
    String mvarLUNETA = "";
    String mvarCLUBECO = "";
    String mvarROBOCONT = "";
    String mvarGRANIZO = "";
    String mvarIBB = "";
    String mvarNROIBB = "";
    String mvarINSTALADP = "";
    String mvarPOSEEDISP = "";
    String mvarCUITNUME = "";
    String mvarRAZONSOC = "";
    String mvarCLIENTIP = "";
    String mvarCPAANO = "";
    String mvarCTAKMS = "";
    String mvarESCERO = "";
    String mvarSIFMVEHI_DES = "";
    String mvarMOTORNUM = "";
    String mvarCHASINUM = "";
    String mvarPATENNUM = "";
    String mvarMODEAUTCOD = "";
    String mvarVEHCLRCOD = "";
    String mvarEFECTANN = "";
    String mvarGUGARAGE = "";
    String mvarGUDOMICI = "";
    String mvarAUCATCOD = "";
    String mvarAUTIPCOD = "";
    String mvarAUANTANN = "";
    String mvarAUNUMSIN = "";
    String mvarAUUSOGNC = "";
    String mvarCAMPACOD = "";
    String mvarCONDUCTORES = "";
    String mvarCONDUAPE = "";
    String mvarCONDUNOM = "";
    String mvarCONDUFEC = "";
    String mvarCONDUSEX = "";
    String mvarCONDUEST = "";
    String mvarCONDUEXC = "";
    String mvarACCESORIOS = "";
    String mvarAUACCCOD = "";
    String mvarAUVEASUM = "";
    String mvarAUVEADES = "";
    String mvarAUVEADEP = "";
    String mvarINSPECOD = "";
    String mvarRastreo = "";
    String mvarALARMCOD = "";
    String mvarCOBROCOD = "";
    String mvarANOTACIO = "";
    String mvarVIGENANN = "";
    String mvarVIGENMES = "";
    String mvarVIGENDIA = "";
    String mvarSUMALBA = "";
    String mvarCOBERCOD = "";
    String mvarCOBERORD = "";
    String mvarCAPITASG = "";
    String mvarCAPITIMP = "";
    String mvarPRECIO_MENSUAL = "";
    String mvarseltest = "";
    String mvarEmpresaCodigo = "";
    String mvarSucursalCodigo = "";
    String mvarLegajoVend = "";
    String mvarLegajoCia = "";
    String mvarNROSOLI = "";
    String mvarACREPREN = "";
    String mvarNUMEDOCUACRE = "";
    String mvarTIPODOCUACRE = "";
    String mvarAPELLIDOACRE = "";
    String mvarNOMBRESACRE = "";
    String mvarDOMICDOMACRE = "";
    String mvarDOMICDNUACRE = "";
    String mvarDOMICPISACRE = "";
    String mvarDOMICPTAACRE = "";
    String mvarDOMICPOBACRE = "";
    String mvarDOMICCPOACRE = "";
    String mvarPROVICODACRE = "";
    String mvarPAISSCODACRE = "";
    String mvarWDB_CERTIANN = "";
    String mvarDOCUMASEG = "";
    String mvarNOMBREASEG = "";
    String mvarNROFACTU = "";
    String mvarCENSICOD = "";


    //jc 08/2010 creado para el componente de zona
    //jc fin
    //Domicilio de riesgo
    //jc 09/2010 agrega area
    //datos del domicilio de correspondencia
    //jc 09/2010 agrega area
    //datos de la cuenta
    //datos de la operacion
    //jc 09/2010 se agregan nuevas coberturas
    //jc 11/2011 se aceptan distintos tipos de iva
    //jc fin
    //AUMARCOD+AUMODCOD+AUSUBCOD+AUADICODAUMODORI
    //DATOS DE LA COTIZACION
    //AM 07/2012 Banca Seguros
    // Acreedor prendario
    try 
    {

      wvarStep = 10;
      fncGrabaSolicitud = true;
      mvarseltest = "";

      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarXMLrequest );

      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( pvarXMLresponse.toString() );



      wvarStep = 20;
      mvarWDB_IDENTIFICACION_BROKER = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_IDENTIFICACION_BROKER ) */ );
      wvarStep = 21;
      mvarWDB_NRO_OPERACION_BROKER = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_NRO_OPERACION_BROKER ) */ );
      wvarStep = 22;
      mvarWDB_NRO_COTIZACION_LBA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_NRO_COTIZACION_LBA ) */ );
      wvarStep = 23;
      mvarWDB_TIPO_OPERACION = "S";
      wvarStep = 24;
      mvarSUMALBA = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//SUMALBA" ) */ == (org.w3c.dom.Node) null) )
      {
        mvarSUMALBA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//SUMALBA" ) */ );
      }
      wvarStep = 25;
      mvarPRODUCTOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PRODUCTOR ) */ );
      wvarStep = 26;

      //AM HSBC
      wvarStep = 27;
      mvarEmpresaCodigo = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EmpresaCodigo ) */ == (org.w3c.dom.Node) null) )
      {
        mvarEmpresaCodigo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EmpresaCodigo ) */ );
      }
      //
      wvarStep = 28;
      mvarSucursalCodigo = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SucursalCodigo ) */ == (org.w3c.dom.Node) null) )
      {
        mvarSucursalCodigo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SucursalCodigo ) */ );
      }
      //
      wvarStep = 29;
      mvarLegajoVend = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LegajoVend ) */ == (org.w3c.dom.Node) null) )
      {
        mvarLegajoVend = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LegajoVend ) */ );
      }
      //
      wvarStep = 30;
      mvarLegajoCia = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LegajoCia ) */ == (org.w3c.dom.Node) null) )
      {
        mvarLegajoCia = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LegajoCia ) */ );
      }
      //
      wvarStep = 31;
      mvarNROSOLI = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NROSOLI ) */ == (org.w3c.dom.Node) null) )
      {
        mvarNROSOLI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NROSOLI ) */ );
      }
      //Fin AM HSBC
      wvarStep = 31;
      mvarNUMEDOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NumeDocu ) */ );
      wvarStep = 32;
      mvarTipoDocu = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TipoDocu ) */ );
      wvarStep = 33;
      mvarClienApe1 = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienApe1 ) */ ) );
      wvarStep = 34;
      mvarClienApe2 = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienApe2 ) */ == (org.w3c.dom.Node) null) )
      {
        mvarClienApe2 = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienApe2 ) */ ) );
      }
      wvarStep = 35;
      mvarClienNom1 = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienNom1 ) */ ) );
      wvarStep = 36;
      mvarNacimAnn = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NacimAnn ) */ );
      wvarStep = 37;
      mvarNacimMes = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NacimMes ) */ );
      wvarStep = 38;
      mvarNacimDia = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NacimDia ) */ );
      wvarStep = 39;
      mvarCLIENSEX = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEX ) */ ) );
      wvarStep = 40;
      mvarCLIENEST = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENEST ) */ ) );
      wvarStep = 41;
      mvarNUMHIJOS = "0";
      if( ! (null /*unsup wobjXMLRequest.selectNodes( mcteParam_CONDUCTORES ) */ == (org.w3c.dom.NodeList) null) )
      {
        mvarNUMHIJOS = String.valueOf( null /*unsup wobjXMLRequest.selectNodes( mcteParam_CONDUCTORES ) */.getLength() );
      }
      wvarStep = 42;
      mvarEMAIL = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMAIL ) */ == (org.w3c.dom.Node) null) )
      {
        mvarEMAIL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMAIL ) */ );
      }
      wvarStep = 43;
      //Domicilio de riesgo
      mvarDOMICDOM = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOM ) */ ) );
      wvarStep = 44;
      mvarDomicDnu = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DomicDnu ) */ );
      wvarStep = 45;
      mvarDOMICPIS = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPIS ) */ == (org.w3c.dom.Node) null) )
      {
        mvarDOMICPIS = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPIS ) */ ) );
      }
      wvarStep = 46;
      mvarDomicPta = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DomicPta ) */ == (org.w3c.dom.Node) null) )
      {
        mvarDomicPta = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DomicPta ) */ ) );
      }
      wvarStep = 47;
      mvarDOMICPOB = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOB ) */ ) );
      wvarStep = 48;
      mvarDOMICCPO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICCPO ) */ );
      wvarStep = 49;
      mvarPROVICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROVICOD ) */ );
      //jc 09/2010 se agrega area
      wvarStep = 50;
      mvarTELCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TELCOD ) */ );
      wvarStep = 51;
      mvarTELNRO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TELNRO ) */ );
      mvarPRINCIPAL = "";
      //datos del domicilio de correspondencia
      wvarStep = 52;
      mvarDOMICDOM_CO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOM_CO ) */ ) );
      wvarStep = 53;
      mvarDOMICDNU_CO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DomicDnu_CO ) */ );
      wvarStep = 54;
      mvarDOMICPIS_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPIS_CO ) */ == (org.w3c.dom.Node) null) )
      {
        mvarDOMICPIS_CO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPIS_CO ) */ ) );
      }
      wvarStep = 55;
      mvarDOMICPTA_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DomicPta_CO ) */ == (org.w3c.dom.Node) null) )
      {
        mvarDOMICPTA_CO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DomicPta_CO ) */ ) );
      }
      wvarStep = 56;
      mvarDOMICPOB_CO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOB_CO ) */ ) );
      wvarStep = 57;
      mvarDOMICCPO_CO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICCPO_CO ) */ );
      wvarStep = 58;
      mvarPROVICOD_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROVICOD_CO ) */ == (org.w3c.dom.Node) null) )
      {
        mvarPROVICOD_CO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROVICOD_CO ) */ );
      }
      wvarStep = 59;
      //jc 09/2010 se agrega area
      mvarTELCOD_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TELCOD_CO ) */ == (org.w3c.dom.Node) null) )
      {
        mvarTELCOD_CO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TELCOD_CO ) */ );
      }
      wvarStep = 60;
      mvarTELNRO_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TELNRO_CO ) */ == (org.w3c.dom.Node) null) )
      {
        mvarTELNRO_CO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TELNRO_CO ) */ );
      }
      wvarStep = 61;
      mvarPRINCIPAL_CO = "";
      //datos de la cuenta
      mvarCOBROTIP = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COBROTIP ) */ ) );

      wvarStep = 62;
      mvarCUENNUME = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CUENNUME ) */ == (org.w3c.dom.Node) null) )
      {
        mvarCUENNUME = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CUENNUME ) */ );
      }

      wvarStep = 63;

      mvarVENCIANN = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VENCIANN ) */ == (org.w3c.dom.Node) null) )
      {
        mvarVENCIANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VENCIANN ) */ );
      }
      wvarStep = 64;
      mvarVENCIMES = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VENCIMES ) */ == (org.w3c.dom.Node) null) )
      {
        mvarVENCIMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VENCIMES ) */ );
      }
      wvarStep = 65;
      mvarVENCIDIA = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VENCIDIA ) */ == (org.w3c.dom.Node) null) )
      {
        mvarVENCIDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VENCIDIA ) */ );
      }

      wvarStep = 66;
      //Si Pago con Tarjeta de Credito, no va ni el Banco ni la Sucursal
      mvarBANCOCOD = "0";
      if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COBROCOD ) */ ) ) == 4 )
      {
        //jc 09/2010 en la sp forzaba los valores a 9000 se fuerza aqui para ser mas claro
        mvarBANCOCOD = "0";
        mvarSUCURCOD = "0";
      }
      else
      {
        mvarBANCOCOD = "9000";
        mvarSUCURCOD = "8888";
      }

      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_BANCOCOD ) */ == (org.w3c.dom.Node) null) )
      {
        mvarBANCOCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_BANCOCOD ) */ );
      }

      wvarStep = 67;
      //datos de la operacion
      mvarFRANQCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FRANQCOD ) */ );
      wvarStep = 68;
      mvarPLANCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PLANCOD ) */ );
      //jc 08/2010 el XML no trae la zona.
      //mvarZONA = wobjXMLRequest.selectSingleNode(mcteParam_ZONA).Text
      mvarRequest = "<Request><RAMOPCOD>AUS1</RAMOPCOD><PROVICOD>" + mvarPROVICOD + "</PROVICOD><CPACODPO>" + mvarDOMICCPO + "</CPACODPO></Request>";
      wobjClass = new lbawA_OVLBAMQ.lbaw_GetZona();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(mvarRequest, mvarResponse, "")
      wobjClass = null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mvarResponse = "<Response><Estado resultado='true' mensaje=''/><CODIZONA>0001</CODIZONA><ZONASDES>CAPITAL FEDERAL</ZONASDES></Response>";
      mobjXMLDoc.loadXML( mvarResponse );
      wvarStep = 69;
      if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
      {
        mvarZONA = diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//CODIZONA" ) */ );
      }
      else
      {
        mvarZONA = "";
      }
      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      //fin jc
      wvarStep = 70;
      mvarCLIENIVA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENIVA ) */ );
      wvarStep = 71;
      mvarSUMAASEG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUMAASEG ) */ );
      wvarStep = 72;
      mvarCLUB_LBA = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLUB_LBA ) */ ) );
      //jc 09/2010 nuevas
      wvarStep = 73;
      mvarDESTRUCCION_80 = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DESTRUCCION_80 ) */ ) );
      wvarStep = 74;
      mvarLUNETA = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Luneta ) */ ) );
      wvarStep = 75;
      mvarCLUBECO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClubEco ) */ ) );
      wvarStep = 76;
      mvarROBOCONT = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Robocont ) */ ) );
      wvarStep = 77;
      mvarGRANIZO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Granizo ) */ ) );
      wvarStep = 78;
      mvarIBB = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_IBB ) */ == (org.w3c.dom.Node) null) )
      {
        mvarIBB = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_IBB ) */ ) );
      }
      wvarStep = 79;
      mvarNROIBB = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NROIBB ) */ == (org.w3c.dom.Node) null) )
      {
        mvarNROIBB = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NROIBB ) */ ) );
      }
      wvarStep = 80;
      mvarINSTALADP = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSTALADP ) */ ) );
      wvarStep = 81;
      mvarPOSEEDISP = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POSEEDISP ) */ ) );
      //11/2010 se agregan distintos iva
      wvarStep = 82;
      mvarCUITNUME = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CUITNUME ) */ == (org.w3c.dom.Node) null) )
      {
        mvarCUITNUME = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CUITNUME ) */ ) );
      }
      wvarStep = 83;
      mvarRAZONSOC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAZONSOC ) */ == (org.w3c.dom.Node) null) )
      {
        mvarRAZONSOC = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAZONSOC ) */ ) );
      }
      wvarStep = 84;
      mvarCLIENTIP = "00";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENTIP ) */ == (org.w3c.dom.Node) null) )
      {
        mvarCLIENTIP = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENTIP ) */ ) );
      }
      wvarStep = 85;
      //jc fin
      mvarCPAANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CPAANO ) */ );
      wvarStep = 86;
      mvarCTAKMS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CTAKMS ) */ );
      wvarStep = 87;
      mvarESCERO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Escero ) */ ) );
      wvarStep = 88;
      mvarSIFMVEHI_DES = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SIFMVEHI_DES ) */ ) );
      wvarStep = 89;
      mvarMOTORNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MOTORNUM ) */ );
      wvarStep = 90;
      mvarCHASINUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CHASINUM ) */ );
      wvarStep = 91;
      mvarPATENNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PATENNUM ) */ );
      //AUMARCOD+AUMODCOD+AUSUBCOD+AUADICODAUMODORI
      wvarStep = 92;
      mvarMODEAUTCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ModeAutCod ) */ );
      wvarStep = 93;
      mvarVEHCLRCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VEHCLRCOD ) */ );
      wvarStep = 94;
      mvarEFECTANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn ) */ );
      wvarStep = 95;
      mvarGUGARAGE = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GUGARAGE ) */ ) );
      wvarStep = 96;
      mvarGUDOMICI = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GUDOMICI ) */ ) );
      wvarStep = 97;
      mvarAUCATCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUCATCOD ) */ );
      wvarStep = 98;
      mvarAUTIPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUTIPCOD ) */ );
      wvarStep = 99;
      mvarAUANTANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUANTANN ) */ );
      wvarStep = 100;
      mvarAUNUMSIN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUNUMSIN ) */ );
      wvarStep = 101;
      mvarAUUSOGNC = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUUSOGNC ) */ ) );
      wvarStep = 102;
      mvarCAMPACOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CampaCod ) */ );
      wvarStep = 103;

      mvarCONDUCTORES = "N";
      if( ! (null /*unsup wobjXMLRequest.selectNodes( mcteParam_CONDUCTORES ) */ == (org.w3c.dom.NodeList) null) )
      {
        mvarCONDUCTORES = (null /*unsup wobjXMLRequest.selectNodes( mcteParam_CONDUCTORES ) */.getLength() == 0 ? "N" : "S");
      }
      wvarStep = 104;
      mvarACCESORIOS = "N";
      if( ! (null /*unsup wobjXMLRequest.selectNodes( mcteParam_ACCESORIOS ) */ == (org.w3c.dom.NodeList) null) )
      {
        mvarACCESORIOS = (null /*unsup wobjXMLRequest.selectNodes( mcteParam_ACCESORIOS ) */.getLength() == 0 ? "N" : "S");
      }

      wvarStep = 105;
      mvarINSPECOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPECOD ) */ );
      wvarStep = 106;
      mvarRastreo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RASTREO ) */ );
      wvarStep = 107;
      mvarALARMCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ALARMCOD ) */ );
      wvarStep = 108;
      mvarCOBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COBROCOD ) */ );
      wvarStep = 109;
      mvarANOTACIO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ANOTACIO ) */ == (org.w3c.dom.Node) null) )
      {
        mvarANOTACIO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ANOTACIO ) */ );
      }
      wvarStep = 110;
      mvarVIGENANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VIGENANN ) */ );
      wvarStep = 111;
      mvarVIGENMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VIGENMES ) */ );
      wvarStep = 112;
      mvarVIGENDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VIGENDIA ) */ );

      wvarStep = 113;
      mvarPRECIO_MENSUAL = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//PRECIOPLAN_SH" ) */ == (org.w3c.dom.Node) null) )
      {
        mvarPRECIO_MENSUAL = String.valueOf( Obj.toDouble( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//PRECIOPLAN_SH" ) */ ) ) );
      }
      wvarStep = 114;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//PRECIOPLAN_CH" ) */ == (org.w3c.dom.Node) null) )
      {
        mvarPRECIO_MENSUAL = String.valueOf( Obj.toDouble( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//PRECIOPLAN_CH" ) */ ) ) );
      }

      wvarStep = 1141;
      mvarNROFACTU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NROFACTU ) */ == (org.w3c.dom.Node) null) )
      {
        mvarNROFACTU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NROFACTU ) */ );
      }

      wvarStep = 1142;
      mvarCENSICOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CENSICOD ) */ == (org.w3c.dom.Node) null) )
      {
        mvarCENSICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CENSICOD ) */ );
      }


      wvarStep = 115;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 116;
      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "AUS1" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 117;
      wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 130;
      wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 131;
      // Solo para el HSBC
      mvarWDB_CERTIANN = "0";
      if( Obj.toInt( mvarEmpresaCodigo ) == 150 )
      {
        mvarWDB_IDENTIFICACION_BROKER = "150";
        mvarWDB_CERTIANN = mvarSucursalCodigo;
      }
      wvarStep = 132;
      if( Obj.toInt( mvarEmpresaCodigo ) > 0 )
      {
        mvarBANCOCOD = mvarEmpresaCodigo;
      }

      wvarStep = 140;
      wobjDBParm = new Parameter( "@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toInt( mvarWDB_IDENTIFICACION_BROKER ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 150;
      wobjDBParm = new Parameter( "@CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toInt( mvarWDB_CERTIANN ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 160;
      wobjDBParm = new Parameter( "@CERTISEC", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, pvarCertiSec );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 170;
      wobjDBParm = new Parameter( "@SUPLENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 180;
      wobjDBParm = new Parameter( "@USUARCOD", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_IDENTIFICACION_BROKER ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 190;
      wobjDBParm = new Parameter( "@NUMEDOCU", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( mvarNUMEDOCU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 200;
      wobjDBParm = new Parameter( "@TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarTipoDocu ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 210;
      wobjDBParm = new Parameter( "@CLIENAP1", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( mvarClienApe1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 220;
      wobjDBParm = new Parameter( "@CLIENAP2", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( mvarClienApe2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 230;
      wobjDBParm = new Parameter( "@CLIENNOM", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( mvarClienNom1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 240;
      wobjDBParm = new Parameter( "@NACIMANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNacimAnn ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 250;
      wobjDBParm = new Parameter( "@NACIMMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNacimMes ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 260;
      wobjDBParm = new Parameter( "@NACIMDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNacimDia ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 270;
      wobjDBParm = new Parameter( "@CLIENSEX", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLIENSEX ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 280;
      wobjDBParm = new Parameter( "@CLIENEST", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLIENEST ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 290;
      wobjDBParm = new Parameter( "@PAISSCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "00" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 300;
      wobjDBParm = new Parameter( "@IDIOMCOD", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 310;
      wobjDBParm = new Parameter( "@NUMHIJOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNUMHIJOS ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 320;
      wobjDBParm = new Parameter( "@EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( DateTime.year( DateTime.now() ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 330;
      wobjDBParm = new Parameter( "@EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( DateTime.month( DateTime.now() ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 340;
      wobjDBParm = new Parameter( "@EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( DateTime.day( DateTime.now() ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 350;
      //jc 09/2010 cambio la codificacion
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTIP", adChar, adParamInput, 2, "F")
      //11/2010
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTIP", adChar, adParamInput, 2, "00")
      wobjDBParm = new Parameter( "@CLIENTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarCLIENTIP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 360;
      wobjDBParm = new Parameter( "@CLIENFUM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 370;
      wobjDBParm = new Parameter( "@CLIENDOZ", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 380;
      wobjDBParm = new Parameter( "@ABRIDTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 390;
      wobjDBParm = new Parameter( "@ABRIDNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 400;
      wobjDBParm = new Parameter( "@CLIENORG", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 410;
      //11/2010
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@PERSOTIP", adChar, adParamInput, 1, "F")
      if( mvarCLIENTIP.equals( "00" ) )
      {
        wobjDBParm = new Parameter( "@PERSOTIP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "F" ) );
      }
      else
      {
        wobjDBParm = new Parameter( "@PERSOTIP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "J" ) );
      }
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 420;
      wobjDBParm = new Parameter( "@DOMICSEC", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( "0" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 430;
      wobjDBParm = new Parameter( "@CLIENCLA", AdoConst.adChar, AdoConst.adParamInput, 9, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 440;
      wobjDBParm = new Parameter( "@FALLEANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 450;
      wobjDBParm = new Parameter( "@FALLEMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 460;
      wobjDBParm = new Parameter( "@FALLEDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 470;
      wobjDBParm = new Parameter( "@FBAJAANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 480;
      wobjDBParm = new Parameter( "@FBAJAMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 490;
      wobjDBParm = new Parameter( "@FBAJADIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 500;
      wobjDBParm = new Parameter( "@EMAIL", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant( mvarEMAIL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 510;
      wobjDBParm = new Parameter( "@DOMICCAL", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 520;
      wobjDBParm = new Parameter( "@DOMICDOM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarDOMICDOM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 530;
      wobjDBParm = new Parameter( "@DOMICDNU", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( mvarDomicDnu ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 540;
      wobjDBParm = new Parameter( "@DOMICESC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 550;
      wobjDBParm = new Parameter( "@DOMICPIS", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarDOMICPIS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 560;
      wobjDBParm = new Parameter( "@DOMICPTA", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarDomicPta ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 570;
      wobjDBParm = new Parameter( "@DOMICPOB", AdoConst.adVarChar, AdoConst.adParamInput, 60, new Variant( mvarDOMICPOB ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 580;
      wobjDBParm = new Parameter( "@DOMICCPO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarDOMICCPO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 590;
      wobjDBParm = new Parameter( "@PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPROVICOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 600;
      wobjDBParm = new Parameter( "@TELCOD", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( mvarTELCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 610;
      wobjDBParm = new Parameter( "@TELNRO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarTELNRO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 620;
      wobjDBParm = new Parameter( "@PRINCIPAL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarPRINCIPAL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 630;
      wobjDBParm = new Parameter( "@DOMICCAL_CO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 640;
      wobjDBParm = new Parameter( "@DOMICDOM_CO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarDOMICDOM_CO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 650;
      wobjDBParm = new Parameter( "@DOMICDNU_CO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( mvarDOMICDNU_CO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 660;
      wobjDBParm = new Parameter( "@DOMICESC_CO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 670;
      wobjDBParm = new Parameter( "@DOMICPIS_CO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarDOMICPIS_CO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 680;
      wobjDBParm = new Parameter( "@DOMICPTA_CO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarDOMICPTA_CO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 690;
      wobjDBParm = new Parameter( "@DOMICPOB_CO", AdoConst.adVarChar, AdoConst.adParamInput, 60, new Variant( mvarDOMICPOB_CO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 700;
      wobjDBParm = new Parameter( "@DOMICCPO_CO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarDOMICCPO_CO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 710;
      wobjDBParm = new Parameter( "@PROVICOD_CO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPROVICOD_CO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 720;
      wobjDBParm = new Parameter( "@TELCOD_CO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( mvarTELCOD_CO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 730;
      wobjDBParm = new Parameter( "@TELNRO_CO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarTELNRO_CO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 740;
      wobjDBParm = new Parameter( "@PRINCIPAL_CO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarPRINCIPAL_CO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 750;
      wobjDBParm = new Parameter( "@TIPOCUEN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 760;
      wobjDBParm = new Parameter( "@COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarCOBROTIP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 770;
      wobjDBParm = new Parameter( "@BANCOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarBANCOCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 780;
      wobjDBParm = new Parameter( "@SUCURCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Math.floor( Obj.toDouble( mvarSUCURCOD ) ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 790;
      wobjDBParm = new Parameter( "@CUENTDC", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 800;
      wobjDBParm = new Parameter( "@CUENNUME", AdoConst.adChar, AdoConst.adParamInput, 16, new Variant( mvarCUENNUME ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 810;
      wobjDBParm = new Parameter( "@TARJECOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 820;
      wobjDBParm = new Parameter( "@VENCIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarVENCIANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarVENCIANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 830;
      wobjDBParm = new Parameter( "@VENCIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarVENCIMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarVENCIMES)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 840;
      wobjDBParm = new Parameter( "@VENCIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarVENCIDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarVENCIDIA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 850;
      wobjDBParm = new Parameter( "@FRANQCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarFRANQCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 860;
      //jc 09/2010 por actualizaci�n sp
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@PQTDES", adChar, adParamInput, 60, "")
      wobjDBParm = new Parameter( "@PQTDES", AdoConst.adChar, AdoConst.adParamInput, 120, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 870;
      wobjDBParm = new Parameter( "@PLANCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPLANCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 880;
      wobjDBParm = new Parameter( "@HIJOS1416", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 890;
      wobjDBParm = new Parameter( "@HIJOS1729", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNUMHIJOS ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 900;
      //jc 09/2010
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@ZONA", adNumeric, adParamInput, , 0) 'mvarZONA)
      wobjDBParm = new Parameter( "@ZONA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarZONA ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 910;
      wobjDBParm = new Parameter( "@CODPROV", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPROVICOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 920;
      wobjDBParm = new Parameter( "@SUMALBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarSUMALBA ) );
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@SUMALBA", adNumeric, adParamInput, , (mvarSUMALBA / 100))
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 930;
      wobjDBParm = new Parameter( "@CLIENIVA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLIENIVA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 940;
      wobjDBParm = new Parameter( "@SUMAASEG", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarSUMAASEG ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 950;
      wobjDBParm = new Parameter( "@CLUB_LBA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLUB_LBA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 960;
      wobjDBParm = new Parameter( "@CPAANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCPAANO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 8 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 970;
      wobjDBParm = new Parameter( "@CTAKMS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCTAKMS ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 980;
      wobjDBParm = new Parameter( "@ESCERO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarESCERO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 990;
      wobjDBParm = new Parameter( "@TIENEPLAN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1000;
      wobjDBParm = new Parameter( "@COND_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1010;
      wobjDBParm = new Parameter( "@ASEG_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1020;
      //jc 09/2010 grababa en cero mes y a�o
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@FH_NAC", adNumeric, adParamInput, , CLng(mvarNacimAnn & Left("0" & mvarNacimMes, 2) & Left("0" & mvarNacimDia, 2)))
      wobjDBParm = new Parameter( "@FH_NAC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toInt( mvarNacimAnn + mvarNacimMes + mvarNacimDia ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 8 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1030;
      wobjDBParm = new Parameter( "@SEXO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLIENSEX ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1040;
      wobjDBParm = new Parameter( "@ESTCIV", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLIENEST ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1050;
      //jc 09/2010 por actualizaci�n sp
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@SIFMVEHI_DES", adChar, adParamInput, 35, Left(mvarSIFMVEHI_DES, 35))
      wobjDBParm = new Parameter( "@SIFMVEHI_DES", AdoConst.adChar, AdoConst.adParamInput, 80, new Variant( mvarSIFMVEHI_DES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1060;
      //jc 09/2010 por actualizaci�n sp
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@PROFECOD", adChar, adParamInput, 6, "")
      wobjDBParm = new Parameter( "@PROFECOD", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( "000000" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1070;
      wobjDBParm = new Parameter( "@ACCESORIOS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarACCESORIOS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1080;
      wobjDBParm = new Parameter( "@REFERIDO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1090;
      wobjDBParm = new Parameter( "@MOTORNUM", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarMOTORNUM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1100;
      wobjDBParm = new Parameter( "@CHASINUM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarCHASINUM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1110;
      wobjDBParm = new Parameter( "@PATENNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarPATENNUM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1120;
      wobjDBParm = new Parameter( "@AUMARCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Strings.mid( mvarMODEAUTCOD, 1, 5 ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1130;
      wobjDBParm = new Parameter( "@AUMODCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Strings.mid( mvarMODEAUTCOD, 6, 5 ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1140;
      wobjDBParm = new Parameter( "@AUSUBCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Strings.mid( mvarMODEAUTCOD, 11, 5 ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1150;
      wobjDBParm = new Parameter( "@AUADICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Strings.mid( mvarMODEAUTCOD, 16, 5 ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1160;
      wobjDBParm = new Parameter( "@AUMODORI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( Strings.mid( mvarMODEAUTCOD, 21, 1 ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1170;
      wobjDBParm = new Parameter( "@AUUSOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1180;
      wobjDBParm = new Parameter( "@AUVTVCOD", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1190;
      wobjDBParm = new Parameter( "@AUVTVDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1200;
      wobjDBParm = new Parameter( "@AUVTVMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1205;
      wobjDBParm = new Parameter( "@AUVTVANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1210;
      wobjDBParm = new Parameter( "@VEHCLRCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarVEHCLRCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1215;
      wobjDBParm = new Parameter( "@AUKLMNUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCTAKMS ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1220;
      wobjDBParm = new Parameter( "@FABRICAN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarEFECTANN ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1225;
      wobjDBParm = new Parameter( "@FABRICMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1230;
      wobjDBParm = new Parameter( "@GUGARAGE", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarGUGARAGE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1235;
      wobjDBParm = new Parameter( "@GUDOMICI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarGUDOMICI ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1240;
      wobjDBParm = new Parameter( "@AUCATCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUCATCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1245;
      wobjDBParm = new Parameter( "@AUTIPCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUTIPCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1250;
      wobjDBParm = new Parameter( "@AUCIASAN", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarAUTIPCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1255;
      wobjDBParm = new Parameter( "@AUANTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUANTANN ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1260;
      wobjDBParm = new Parameter( "@AUNUMSIN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUNUMSIN ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1270;
      wobjDBParm = new Parameter( "@AUNUMKMT", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCTAKMS ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1280;
      wobjDBParm = new Parameter( "@AUUSOGNC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarAUUSOGNC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1290;
      wobjDBParm = new Parameter( "@SITUCPOL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "A" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1300;
      wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1310;
      wobjDBParm = new Parameter( "@CUENTSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1320;
      wobjDBParm = new Parameter( "@COBROFOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1330;
      wobjDBParm = new Parameter( "@EDADACTU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( DateTime.diff( "YYYY", DateTime.dateSerial( Obj.toInt( mvarNacimAnn ), Obj.toInt( mvarNacimMes ), Obj.toInt( mvarNacimDia ) ), DateTime.now() ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1340;
      for( wvarCount = 1; wvarCount <= 30; wvarCount++ )
      {
        mvarCOBERCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//COBERCOD" + wvarCount ) */ );
        mvarCOBERORD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//COBERORD" + wvarCount ) */ );
        //mvarCAPITASG = wobjXMLRequest.selectSingleNode("//CAPITASG" & wvarCount).Text * 100
        //mvarCAPITIMP = wobjXMLRequest.selectSingleNode("//CAPITIMP" & wvarCount).Text * 100
        mvarCAPITASG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//CAPITASG" + wvarCount ) */ );
        mvarCAPITIMP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//CAPITIMP" + wvarCount ) */ );

        wobjDBParm = new Parameter( "@COBERCOD" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCOBERCOD ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 3 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@COBERORD" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCOBERORD ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CAPITASG" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCAPITASG ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CAPITIMP" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCAPITIMP ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      //Asegurados adicionales
      wvarStep = 1350;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_ASEGURADOS ) */;
      wvarStep = 1351;
      for( wvarCount = 1; wvarCount <= 10; wvarCount++ )
      {
        mvarDOCUMASEG = "0";
        mvarNOMBREASEG = "";

        if( (wvarCount <= wobjXMLList.getLength()) && ! ((null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ASEGADIC ) */ == (org.w3c.dom.Node) null)) )
        {
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ASEGADIC ) */ ).equals( "S" ) )
          {
            if( ! ((null /*unsup wobjXMLList.item( (wvarCount - 1) ).selectSingleNode( mcteParam_DOCUMASEG ) */ == (org.w3c.dom.Node) null)) && ! ((null /*unsup wobjXMLList.item( (wvarCount - 1) ).selectSingleNode( mcteParam_NOMBREASEG ) */ == (org.w3c.dom.Node) null)) )
            {
              wvarStep = 1352;
              mvarDOCUMASEG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCount - 1 ).selectSingleNode( mcteParam_DOCUMASEG ) */ );
              wvarStep = 1353;
              mvarNOMBREASEG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCount - 1 ).selectSingleNode( mcteParam_NOMBREASEG ) */ );
            }
          }
        }
        wvarStep = 1354;
        wobjDBParm = new Parameter( "@DOCUMDAT" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarDOCUMASEG ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 11 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        wvarStep = 1355;
        wobjDBParm = new Parameter( "@NOMBREAS" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 49, new Variant( mvarNOMBREASEG ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }


      wvarStep = 1360;
      wobjDBParm = new Parameter( "@CAMP_CODIGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCAMPACOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1370;
      wobjDBParm = new Parameter( "@CAMP_DESC", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1380;
      wobjDBParm = new Parameter( "@LEGAJO_GTE", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1390;
      //jc 09/2010 no llevaba productor
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@NRO_PROD", adNumeric, adParamInput, , 0)
      wobjDBParm = new Parameter( "@NRO_PROD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPRODUCTOR ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //AM
      wvarStep = 1395;
      wobjDBParm = new Parameter( "@EMPRESA_CODIGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarLegajoCia ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 1400;
      wobjDBParm = new Parameter( "@SUCURSAL_CODIGO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarSucursalCodigo ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1410;
      wobjDBParm = new Parameter( "@LEGAJO_VEND", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarLegajoVend ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      // AM fin
      //jc 09/2010 agregado x cambio sp
      wvarStep = 1420;
      wobjDBParm = new Parameter( "@EMPL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //jc fin
      wvarStep = 1420;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_CONDUCTORES ) */;
      for( wvarCount = 1; wvarCount <= 10; wvarCount++ )
      {

        if( wvarCount <= wobjXMLList.getLength() )
        {
          mvarCONDUAPE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCount - 1 ).selectSingleNode( mcteParam_CONDUAPE ) */ );
          mvarCONDUNOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCount - 1 ).selectSingleNode( mcteParam_CONDUNOM ) */ );
          mvarCONDUFEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCount - 1 ).selectSingleNode( mcteParam_CONDUFEC ) */ );
          mvarCONDUSEX = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCount - 1 ).selectSingleNode( mcteParam_CONDUSEX ) */ ) );
          mvarCONDUEST = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCount - 1 ).selectSingleNode( mcteParam_CONDUEST ) */ ) );
          //jc 09/2010 cambio el valor
          //mvarCONDUEXC = "S"
          mvarCONDUEXC = "1";
        }
        else
        {
          mvarCONDUAPE = "";
          mvarCONDUNOM = "";
          mvarCONDUFEC = "0";
          mvarCONDUSEX = "";
          mvarCONDUEST = "";
          //mvarCONDUEXC = "N"
          mvarCONDUEXC = "0";
        }
        wobjDBParm = new Parameter( "@CONDUAPE" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( mvarCONDUAPE ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CONDUNOM" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarCONDUNOM ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CONDUFEC" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCONDUFEC ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 8 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CONDUSEX" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCONDUSEX ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CONDUEST" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCONDUEST ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CONDUEXC" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCONDUEXC ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      wvarStep = 1430;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_ACCESORIOS ) */;
      for( wvarCount = 1; wvarCount <= 10; wvarCount++ )
      {

        if( wvarCount <= wobjXMLList.getLength() )
        {
          mvarAUACCCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCount - 1 ).selectSingleNode( mcteParam_AUACCCOD ) */ );
          mvarAUVEASUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCount - 1 ).selectSingleNode( mcteParam_AUVEASUM ) */ );
          mvarAUVEADES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCount - 1 ).selectSingleNode( mcteParam_AUVEADES ) */ );
          mvarAUVEADEP = "S";
        }
        else
        {
          mvarAUACCCOD = "0";
          mvarAUVEASUM = "0";
          mvarAUVEADES = "";
          mvarAUVEADEP = "N";
        }

        wobjDBParm = new Parameter( "@AUACCCOD" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUACCCOD ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@AUVEASUM" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUVEASUM ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 14 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@AUVEADES" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( Strings.toUpperCase( mvarAUVEADES ) ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@AUVEADEP" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarAUVEADEP ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      //jc 09/2010 nuevos campos en sp
      wvarStep = 1435;
      wobjDBParm = new Parameter( "@ESTAINSP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPECOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@INSPECOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@INSPEDES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@INSPADOM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@INSPEOBS", AdoConst.adChar, AdoConst.adParamInput, 50, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@INSPEKMS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 8 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@CENSICOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarCENSICOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@CENSIDES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@DISPOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarRastreo ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@PRESTCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarALARMCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //jc fin
      //Acreedor prendario
      mvarACREPREN = "N";
      mvarNUMEDOCUACRE = "";
      mvarTIPODOCUACRE = "0";
      mvarAPELLIDOACRE = "";
      mvarNOMBRESACRE = "";
      mvarDOMICDOMACRE = "";
      mvarDOMICDNUACRE = "";
      mvarDOMICPISACRE = "";
      mvarDOMICPTAACRE = "";
      mvarDOMICPOBACRE = "";
      mvarDOMICCPOACRE = "0";
      mvarPROVICODACRE = "0";
      mvarPAISSCODACRE = "";

      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ACREPREN ) */ == (org.w3c.dom.Node) null) )
      {
        mvarACREPREN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ACREPREN ) */ );
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ACREPREN ) */ ).equals( "S" ) )
        {
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCUACRE ) */ == (org.w3c.dom.Node) null) )
          {
            mvarNUMEDOCUACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCUACRE ) */ );
          }
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCUACRE ) */ == (org.w3c.dom.Node) null) )
          {
            mvarTIPODOCUACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCUACRE ) */ );
          }
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_APELLIDOACRE ) */ == (org.w3c.dom.Node) null) )
          {
            mvarAPELLIDOACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_APELLIDOACRE ) */ );
          }
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NOMBRESACRE ) */ == (org.w3c.dom.Node) null) )
          {
            mvarNOMBRESACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NOMBRESACRE ) */ );
          }
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOMACRE ) */ == (org.w3c.dom.Node) null) )
          {
            mvarDOMICDOMACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOMACRE ) */ );
          }
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDNUACRE ) */ == (org.w3c.dom.Node) null) )
          {
            mvarDOMICDNUACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDNUACRE ) */ );
          }
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPISACRE ) */ == (org.w3c.dom.Node) null) )
          {
            mvarDOMICPISACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPISACRE ) */ );
          }
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPTAACRE ) */ == (org.w3c.dom.Node) null) )
          {
            mvarDOMICPTAACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPTAACRE ) */ );
          }
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOBACRE ) */ == (org.w3c.dom.Node) null) )
          {
            mvarDOMICPOBACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOBACRE ) */ );
          }
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICCPOACRE ) */ == (org.w3c.dom.Node) null) )
          {
            mvarDOMICCPOACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICCPOACRE ) */ );
          }
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROVICODACRE ) */ == (org.w3c.dom.Node) null) )
          {
            mvarPROVICODACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROVICODACRE ) */ );
          }
          if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAISSCODACRE ) */ == (org.w3c.dom.Node) null) )
          {
            mvarPAISSCODACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAISSCODACRE ) */ );
          }
        }
      }

      wvarStep = 1550;
      wobjDBParm = new Parameter( "@NUMEDOCU_ACRE", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( mvarNUMEDOCUACRE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1560;
      wobjDBParm = new Parameter( "@TIPODOCU_ACRE", AdoConst.adNumeric, AdoConst.adParamInput, 0, (Strings.trim( mvarTIPODOCUACRE ).equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarTIPODOCUACRE)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1570;
      wobjDBParm = new Parameter( "@APELLIDO_ACRE", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( mvarAPELLIDOACRE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1580;
      wobjDBParm = new Parameter( "@NOMBRES_ACRE", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( mvarNOMBRESACRE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1590;
      wobjDBParm = new Parameter( "@DOMICDOM_ACRE", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarDOMICDOMACRE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1600;
      wobjDBParm = new Parameter( "@DOMICDNU_ACRE", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( mvarDOMICDNUACRE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1610;
      wobjDBParm = new Parameter( "@DOMICESC_ACRE", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1620;
      wobjDBParm = new Parameter( "@DOMICPIS_ACRE", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarDOMICPISACRE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1630;
      wobjDBParm = new Parameter( "@DOMICPTA_ACRE", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarDOMICPTAACRE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1640;
      wobjDBParm = new Parameter( "@DOMICPOB_ACRE", AdoConst.adVarChar, AdoConst.adParamInput, 20, new Variant( mvarDOMICPOBACRE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1650;
      wobjDBParm = new Parameter( "@DOMICCPO_ACRE", AdoConst.adNumeric, AdoConst.adParamInput, 0, (Strings.trim( mvarDOMICCPOACRE ).equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarDOMICCPOACRE)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1660;
      wobjDBParm = new Parameter( "@PROVICOD_ACRE", AdoConst.adNumeric, AdoConst.adParamInput, 0, (Strings.trim( mvarPROVICODACRE ).equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarPROVICODACRE)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1670;
      wobjDBParm = new Parameter( "@PAISSCOD_ACRE", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarPAISSCODACRE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1680;
      wobjDBParm = new Parameter( "@COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCOBROCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1690;
      wobjDBParm = new Parameter( "@INSPEANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1700;
      wobjDBParm = new Parameter( "@INSPEMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1710;
      wobjDBParm = new Parameter( "@INSPEDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1720;
      wobjDBParm = new Parameter( "@ENTDOMSC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1730;
      wobjDBParm = new Parameter( "@ANOTACIO", AdoConst.adChar, AdoConst.adParamInput, 100, new Variant( mvarANOTACIO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1740;
      //jc 09/2010 cambio sp
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@PRECIO_MENSUAL", adNumeric, adParamInput, , Format(mvarPRECIO_MENSUAL, "0.00"))
      wobjDBParm = new Parameter( "@PRECIO_MENSUAL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toDouble( mvarPRECIO_MENSUAL ) / 100 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBParm.setPrecision( (byte)( 13 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1750;
      wobjDBParm = new Parameter( "@EFECTANN_SOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarVIGENANN ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1760;
      wobjDBParm = new Parameter( "@EFECTMES_SOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarVIGENMES ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1770;
      wobjDBParm = new Parameter( "@EFECTDIA_SOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarVIGENDIA ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //jc 09/2010 se tienen que agregar todos los campos por quedar la sp desactualizada
      wvarStep = 17710;
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@CUITNUME", adChar, adParamInput, 11, "") 11/2010
      wobjDBParm = new Parameter( "@CUITNUME", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( mvarCUITNUME ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17711;
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@RAZONSOC", adChar, adParamInput, 60, "")
      wobjDBParm = new Parameter( "@RAZONSOC", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant( mvarRAZONSOC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17712;
      wobjDBParm = new Parameter( "@CLIEIBTP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarIBB ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17713;
      wobjDBParm = new Parameter( "@NROIIBB", AdoConst.adChar, AdoConst.adParamInput, 15, new Variant( mvarNROIBB ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17714;
      wobjDBParm = new Parameter( "@LUNETA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarLUNETA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17715;
      wobjDBParm = new Parameter( "@HIJOS1729_EXC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17716;
      wobjDBParm = new Parameter( "@NROFACTU", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( mvarNROFACTU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17717;
      wobjDBParm = new Parameter( "@INS_DOMICDOM", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17718;
      wobjDBParm = new Parameter( "@INS_CPACODPO", AdoConst.adChar, AdoConst.adParamInput, 8, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17719;
      wobjDBParm = new Parameter( "@INS_DOMICPOB", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17720;
      wobjDBParm = new Parameter( "@INS_PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17721;
      wobjDBParm = new Parameter( "@INS_DOMICTLF", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17722;
      wobjDBParm = new Parameter( "@INS_RANGOHOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 8 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17723;
      wobjDBParm = new Parameter( "@DDJJ", AdoConst.adVarChar, AdoConst.adParamInput, 140, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17724;
      wobjDBParm = new Parameter( "@DERIVACI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17725;
      wobjDBParm = new Parameter( "@ALEATORIO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17726;
      wobjDBParm = new Parameter( "@ESTAIDES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17727;
      wobjDBParm = new Parameter( "@DISPODES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 17728;
      wobjDBParm = new Parameter( "@INSTALADP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarINSTALADP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17729;
      wobjDBParm = new Parameter( "@POSEEDISP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarPOSEEDISP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17730;
      wobjDBParm = new Parameter( "@AUTIPGAMA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17731;
      wobjDBParm = new Parameter( "@SWDT80", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarDESTRUCCION_80 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17732;
      wobjDBParm = new Parameter( "@TIPO_PROD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "PR" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17733;
      wobjDBParm = new Parameter( "@SWAPODER", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17734;
      wobjDBParm = new Parameter( "@OCUPACODCLIE", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17735;
      wobjDBParm = new Parameter( "@OCUPACODAPOD", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17736;
      wobjDBParm = new Parameter( "@CLIENSECAPOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17737;
      wobjDBParm = new Parameter( "@DOMISECAPOD", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17738;
      wobjDBParm = new Parameter( "@GREEN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLUBECO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17739;
      wobjDBParm = new Parameter( "@GRANIZO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarGRANIZO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17740;
      wobjDBParm = new Parameter( "@ROBOCONT", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarROBOCONT ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17741;
      wobjDBParm = new Parameter( "@SN_NBWS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17742;
      wobjDBParm = new Parameter( "@EMAIL_NBWS", AdoConst.adVarChar, AdoConst.adParamInput, 50, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17743;
      wobjDBParm = new Parameter( "@SN_EPOLIZA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17744;
      wobjDBParm = new Parameter( "@EMAIL_EPOLIZA", AdoConst.adVarChar, AdoConst.adParamInput, 50, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17745;
      wobjDBParm = new Parameter( "@SN_PRESTAMAIL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 17746;
      wobjDBParm = new Parameter( "@PASSEPOL", AdoConst.adVarChar, AdoConst.adParamInput, 10, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 17747;
      wobjDBParm = new Parameter( "@CERTISECNEWSAS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNROSOLI ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //jc fin
      wvarStep = 1780;
      mvarseltest = mcteStoreProc + " ";
      for( wvarCount = 0; wvarCount <= (wobjDBCmd.getParameters().getCount() - 1); wvarCount++ )
      {
        if( (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adChar) || (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adVarChar) )
        {
          mvarseltest = mvarseltest + "'" + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + "',";
        }
        else
        {
          mvarseltest = mvarseltest + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + ",";
        }
      }

      wvarStep = 1790;

      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      pvarCertiSec.set( wobjDBCmd.getParameters().getParameter("@CERTISEC").getValue().toString() );


      fin: 
      //libero los objectos
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLResponse = (diamondedge.util.XmlDom) null;


      return fncGrabaSolicitud;
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarXMLresponse.set( "<LBA_WS res_code=\"-1000\" res_msg=\"" + mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        //AM Debug
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGrabaSolicitud = false;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGrabaSolicitud;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
