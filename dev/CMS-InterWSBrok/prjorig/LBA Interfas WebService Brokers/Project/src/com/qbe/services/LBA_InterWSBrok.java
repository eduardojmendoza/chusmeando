package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
import java.applet.*;

public class LBA_InterWSBrok extends JApplet
{
  static {
    try {
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (Exception e) { System.out.println(e); }
  }
  public static String Title = "LBA_InterWSBrok";
  public static String ProductName = "";
  public static int MajorVersion = 1;
  public static int MinorVersion = 0;
  public static int Revision = 0;
  public static String HelpFile = "";
  public static String Comments = "";
  public static String FileDescription = "";
  public static String CompanyName = "HSBC Group";
  public static String LegalCopyright = "";
  public static String LegalTrademarks = "";

  public LBA_InterWSBrok()
  {
  }

  // called only when running as an applet
  public void init()
  {
    getContentPane().setLayout( new java.awt.BorderLayout() );
    Application app = new Application( "LBA_InterWSBrok" );
  }

  public String getAppletInfo()
  {
    return "LBA_InterWSBrok" + " " + LegalCopyright;
  }

  // called only when running as a stand-alone application
  public static void main( String args[] )
  {
    final Application app = new Application( "LBA_InterWSBrok" );
    app.setApplication( new LBA_InterWSBrok(), args );
    try
    {
    }
    catch(Exception e) { Err.set(e); }
    app.endApplication();
    javax.swing.SwingUtilities.invokeLater( new Runnable() {
      public void run() {
        Application.setApplication( app );
      }
    });
  }
}
