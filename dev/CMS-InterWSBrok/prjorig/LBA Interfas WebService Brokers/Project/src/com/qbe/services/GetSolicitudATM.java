package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Implementacion de los objetos
 * Objetos del FrameWork
 */

public class GetSolicitudATM implements Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetSolicitudATM";
  static final String mcteStoreProcSelect = "SPSNCV_BRO_SELECT_OPER_ESP";
  static final String mcteStoreProc = "SPSNCV_BRO_ALTA_OPER";
  /**
   * 10/2010 sp para descripciones del impreso
   */
  static final String mcteStoreProcImpre = "SPSNCV_BRO_SOLI_ATM_IMPRE";
  /**
   * Archivo de Configuracion
   */
  static final String mcteArchivoConfATM_XML = "LBA_PARAM_ATM.XML";
  static final String mcteArchivoHOM_XML = "LBA_VALIDACION_COT_ATM.XML";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_NROCOT = "COT_NRO";
  static final String mcteParam_VALOR = "VALOR";
  static final String mcteParam_DOMICDOM = "DOMICDOM";
  static final String mcteParam_DomicDnu = "DOMICDNU";
  static final String mcteParam_DOMICPIS = "DOMICPIS";
  static final String mcteParam_DomicPta = "DOMICPTA";
  static final String mcteParam_DOMICPOB = "DOMICPOB";
  static final String mcteParam_Provi = "PROVCOD";
  static final String mcteParam_LocalidadCod = "LOCALIDAD";
  static final String mcteParam_TELCOD = "TELCOD";
  static final String mcteParam_TELNRO = "TELNRO";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  static final String mcteParam_BANELCO = "BANELCO";
  static final String mcteParam_PLANNCOD = "TPLANATM";
  static final String mcteParam_COBROCOD = "FPAGO";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_CUENNUME = "CUENNUME";
  static final String mcteParam_VENCIANN = "VENCIANN";
  static final String mcteParam_VENCIMES = "VENCIMES";
  static final String mcteParam_VENCIDIA = "VENCIDIA";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  static final String mcteParam_ClienAp1 = "CLIENAP1";
  static final String mcteParam_ClienAp2 = "CLIENAP2";
  static final String mcteParam_ClienNom = "CLIENNOM";
  static final String mcteParam_NacimAnn = "NACIMANN";
  static final String mcteParam_NacimMes = "NACIMMES";
  static final String mcteParam_NacimDia = "NACIMDIA";
  static final String mcteParam_CLIENSEX = "SEXO";
  static final String mcteParam_CLIENEST = "ESTADO";
  static final String mcteParam_EMAIL = "EMAIL";
  static final String mcteParam_TipoDocu = "TIPODOCU";
  static final String mcteParam_NumeDocu = "NUMEDOCU";
  static final String mcteParam_DomicDomCo = "DOMICDOMCOR";
  static final String mcteParam_DomicDNUCo = "DOMICDNUCOR";
  static final String mcteParam_DomicPisCo = "DOMICPISCOR";
  static final String mcteParam_DomicPtaCo = "DOMICPTACOR";
  static final String mcteParam_DOMICPOBCo = "DOMICPOBCOR";
  static final String mcteParam_ProviCo = "PROVICOR";
  static final String mcteParam_LocalidadCodCo = "LOCALIDADCODCOR";
  static final String mcteParam_TELCOD_CO = "TELCODCOR";
  static final String mcteParam_TelNroCo = "TELNROCOR";
  static final String mcteParam_RAMOPCOD = "RAMOPCOD";
  static final String mcteParam_CERTIPOL = "CERTIPOL";
  static final String mcteParam_CERTIANN = "CERTIANN";
  static final String mcteParam_CERTISEC = "CERTISEC";
  static final String mcteParam_SUPLENUM = "SUPLENUM";
  static final String mcteParam_TipoOperac = "TIPOOPERAC";
  static final String mcteParam_EstadoProc = "ESTADOPROC";
  static final String mcteParam_TiempoProceso = "TIEMPOPROCESO";
  static final String mcteParam_PedidoAno = "MSGANO";
  static final String mcteParam_PedidoMes = "MSGMES";
  static final String mcteParam_PedidoDia = "MSGDIA";
  static final String mcteParam_PedidoHora = "MSGHORA";
  static final String mcteParam_PedidoMinuto = "MSGMINUTO";
  static final String mcteParam_PedidoSegundo = "MSGSEGUNDO";
  static final String mcteParam_USUARCOD = "USUARCOD";
  /**
   * AM
   */
  static final String mcteParam_EmpresaCodigo = "//CANAL";
  static final String mcteParam_SucursalCodigo = "//CANAL_SUCURSAL";
  static final String mcteParam_LegajoVend = "//LEGAJO_VEND";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncInsertNode
   * static variable for method: fncPutSolHO
   * static variable for method: fncPutSolicitud
   * static variable for method: fncIntegrarSolicitudMQ
   */
  private final String wcteFnName = "fncIntegrarSolicitudMQ";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant wvarCodErr = new Variant();
    Variant pvarRes = new Variant();
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      pvarRequest = Strings.mid( pvarRequest, 1, Strings.find( 1, pvarRequest, ">" ) ) + "<MSGANO>" + DateTime.year( DateTime.now() ) + "</MSGANO>" + "<MSGMES>" + DateTime.month( DateTime.now() ) + "</MSGMES>" + "<MSGDIA>" + DateTime.day( DateTime.now() ) + "</MSGDIA>" + "<MSGHORA>" + DateTime.hour( DateTime.now() ) + "</MSGHORA>" + "<MSGMINUTO>" + DateTime.minute( DateTime.now() ) + "</MSGMINUTO>" + "<MSGSEGUNDO>" + DateTime.second( DateTime.now() ) + "</MSGSEGUNDO>" + Strings.mid( pvarRequest, (Strings.find( 1, pvarRequest, ">" ) + 1) );
      wvarStep = 10;
      wvarMensaje.set( "" );
      wvarCodErr.set( "" );
      pvarRes.set( "" );
      //
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(new Variant( pvarRequest )), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*unsup mobjCOM_Context.SetAbort() */;
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*unsup mobjCOM_Context.SetComplete() */;
        }
        return IAction_Execute;
      }

      pvarResponse.set( pvarRes );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( Variant pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLParams = null;
    diamondedge.util.XmlDom wobjXMLReturnVal = null;
    diamondedge.util.XmlDom wobjXMLError = null;
    diamondedge.util.XmlDom wobjXMLRequestExists = null;
    Object wobjClass = null;
    Variant wvarMensajeStoreProc = new Variant();
    Variant wvarMensajePutTran = new Variant();
    String mvar_Estado = "";
    String mvarCertiSec = "";
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    Variant pTiempoAIS = new Variant();

    //
    //
    //
    try 
    {
      //Le agrega los Nodos de Valor Fijo
      wvarStep = 10;
      wobjXMLParams = new diamondedge.util.XmlDom();
      //unsup wobjXMLParams.async = false;
      wobjXMLParams.load( System.getProperty("user.dir") + "\\" + mcteArchivoConfATM_XML );
      //
      invoke( "fncInsertNode", new Variant[] { new Variant(pvarRequest), new Variant("//Request"), new Variant(diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( new Variant("//BROKERS/ATM/SOLICITUD") ) */ )), new Variant("TIPOOPERAC") } );
      invoke( "fncInsertNode", new Variant[] { new Variant(pvarRequest), new Variant("//Request"), new Variant(diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( new Variant("//BROKERS/ATM/RAMOPCOD") ) */ )), new Variant("RAMOPCOD") } );
      //
      //Instancia la clase de validacion
      wvarMensaje.set( "" );
      wvarMensajeStoreProc.set( "" );
      //
      wvarStep = 20;
      wobjClass = new LBA_InterWSBrok.GetValidacionSolATM();
      wobjClass.Execute( pvarRequest, wvarMensajeStoreProc, "" );
      wobjClass = null;
      //
      //Analizo la respuesta del validador
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( wvarMensajeStoreProc.toString() );
      //
      //Si la respuesta viene con error
      wvarStep = 30;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" ) */ ).equals( "0" )) )
      {
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) );
        
        if( wvarCodErr.toString().equals( "-2" ) )
        {
          //Este es el caso que no venga el cod. de broker o el nro. de operacion
          wvarStep = 35;
          mvar_Estado = "NI";
          pvarRes.set( wvarMensajeStoreProc );
          wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
        }
        else if( wvarCodErr.toString().equals( "H010" ) )
        {
          //Este es el caso que ya existe la solicitud
          wobjXMLRequestExists = new diamondedge.util.XmlDom();
          //unsup wobjXMLRequestExists.async = false;
          wobjXMLRequestExists.loadXML( pvarRequest.toString() );

          wvarStep = 40;
          wobjHSBC_DBCnn = new HSBC.DBConnection();
          //error: function 'GetDBConnection' was not found.
          //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
          wobjDBCmd = new Command();
          wobjDBCmd.setActiveConnection( wobjDBCnn );
          wobjDBCmd.setCommandText( mcteStoreProcSelect );
          wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
          //
          wvarStep = 50;
          wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestExists.selectSingleNode( "//CODINST" ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 4 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wvarStep = 52;
          wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestExists.selectSingleNode( "//REQUESTID" ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 14 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wvarStep = 54;
          wobjDBParm = new Parameter( "@WDB_TIPO_OPERACION", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "S" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wvarStep = 56;
          wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "OK" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wvarStep = 58;
          wrstRes = new Recordset();
          wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
          wvarCodErr.set( "0" );
          wvarMensaje.set( "" );

          pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + "<Response>" + "<REQUESTID>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestExists.selectSingleNode( "//REQUESTID" ) */ ) + "</REQUESTID>" + "<POLIZANN>0</POLIZANN>" + "<POLIZSEC>0</POLIZSEC>" + "<CERTIPOL>0</CERTIPOL>" + "<CERTIANN>0</CERTIANN>" + "<CERTISEC>" + wrstRes.getFields().getField(12).getValue() + "</CERTISEC>" + "<SUPLENUM>0</SUPLENUM>" + "<MENSAJE>PROPUESTA YA EMITIDA</MENSAJE>" + "<SITUACION>R</SITUACION>" + "<PDF64><IMPCERTI/><IMPSOLI/><IMPPOLI/></PDF64>" + "</Response>" + "</LBA_WS>" );

          mvarCertiSec = wrstRes.getFields().getField(12).getValue().toString();
          mvar_Estado = "OK";
          wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
          //Le agrego el Nodo de CertiSec al XML original
          invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvarCertiSec), new Variant("CERTISEC") } );
          wobjXMLRequestExists = (diamondedge.util.XmlDom) null;
          wobjDBCmd = (Command) null;
          wobjDBCnn = (Connection) null;
          wobjHSBC_DBCnn = null;
        }
        else
        {
          mvar_Estado = "ER";
          pvarRes.set( wvarMensajeStoreProc );
          wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );

        }

        // si no viene con error de la BD
      }
      else
      {

        //Guarda la Solicitud en el SP
        wvarStep = 60;

        if( invoke( "fncPutSolicitud", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes), new Variant(pTiempoAIS) } ) )
        {

          wobjXMLReturnVal = new diamondedge.util.XmlDom();
          //unsup wobjXMLReturnVal.async = false;
          wobjXMLReturnVal.loadXML( pvarRes.toString() );
          //
          if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" ) */ ).equals( "0" )) )
          {
            //hubo un error
            wvarStep = 65;
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) );
            wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
            mvar_Estado = "ER";

            //Este es el caso en que no haya contestado el SP o sea un error no identificado
          }
          else
          {
            wvarStep = 70;
            mvar_Estado = "OK";
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//CERTISEC" ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//CERTISEC" ) */ ) );
            wvarMensajeStoreProc.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
          }
          //
          wobjXMLReturnVal = (diamondedge.util.XmlDom) null;

        }


        //
      }

      invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvar_Estado), new Variant("ESTADOPROC") } );

      // Agrego el tiempo de impresion
      invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(pTiempoAIS.toString()), new Variant("TIEMPOPROCESO") } );

      //Alta de la OPERACION
      wvarStep = 75;
      if( invoke( "fncPutSolHO", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje.toString()), new Variant(wvarCodErr), new Variant(wvarMensajePutTran) } ) )
      {
        //
        wobjXMLReturnVal = new diamondedge.util.XmlDom();
        //unsup wobjXMLReturnVal.async = false;
        wobjXMLReturnVal.loadXML( wvarMensajePutTran.toString() );
        //
        wvarStep = 80;
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" ) */ ).equals( "0" )) )
        {
          wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" ) */ ) );
          wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          fncGetAll = false;
          return fncGetAll;
        }
        //
        fncGetAll = true;
        //
      }
      fin: 
      wobjClass = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;
        /*unsup mobjCOM_Context.SetComplete() */;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }

  private boolean fncInsertNode( Variant pvarRequest, String pvarHeader, String pvarNodeValue, String pvarNodeName )
  {
    boolean fncInsertNode = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLRequest = null;


    try 
    {

      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest.toString() );
      //
      wvarStep = 20;
      /*unsup wobjXMLRequest.selectSingleNode( pvarHeader ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), pvarNodeName, "" ) */ );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + pvarNodeName ) */, pvarNodeValue );
      //
      pvarRequest.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      fncInsertNode = true;
      return fncInsertNode;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncInsertNode = false;
        /*unsup mobjCOM_Context.SetComplete() */;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncInsertNode;
  }

  private boolean fncPutSolHO( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutSolHO = false;
    int wvarStep = 0;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_NROCOT = "";
    String mvarWDB_TIPOOPERAC = "";
    String mvarWDB_ESTADO = "";
    String mvarWDB_RECEPCION_PEDIDOANO = "";
    String mvarWDB_RECEPCION_PEDIDOMES = "";
    String mvarWDB_RECEPCION_PEDIDODIA = "";
    String mvarWDB_RECEPCION_PEDIDOHORA = "";
    String mvarWDB_RECEPCION_PEDIDOMINUTO = "";
    String mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_TIEMPOPROCESO = "";
    String mvarWDB_XML = "";

    //
    //
    //
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_CODINST = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }
      //
      wvarStep = 30;
      mvarWDB_REQUESTID = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID ) */ );
      }
      //
      wvarStep = 40;
      mvarWDB_NROCOT = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROCOT) ) */ == (org.w3c.dom.Node) null) )
      {
        if( new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NROCOT ) */ ) ).isNumeric() )
        {
          if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROCOT) ) */ ) ) < 19 )
          {
            mvarWDB_NROCOT = String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NROCOT ) */ ) ) );
          }
        }
      }
      //
      wvarStep = 50;
      mvarWDB_TIPOOPERAC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoOperac) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPOOPERAC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoOperac ) */ );
      }
      //
      wvarStep = 60;
      mvarWDB_ESTADO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EstadoProc) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_ESTADO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_EstadoProc ) */ );
      }
      //
      wvarStep = 70;
      mvarWDB_RECEPCION_PEDIDOANO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoAno) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoAno ) */ );
      }
      //
      wvarStep = 80;
      mvarWDB_RECEPCION_PEDIDOMES = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMes) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMes ) */ );
      }
      //
      wvarStep = 90;
      mvarWDB_RECEPCION_PEDIDODIA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoDia) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDODIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoDia ) */ );
      }
      //
      wvarStep = 100;
      mvarWDB_RECEPCION_PEDIDOHORA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoHora) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOHORA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoHora ) */ );
      }
      //
      wvarStep = 110;
      mvarWDB_RECEPCION_PEDIDOMINUTO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMinuto) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMINUTO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMinuto ) */ );
      }
      //
      wvarStep = 120;
      mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoSegundo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOSEGUNDO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoSegundo ) */ );
      }
      //
      wvarStep = 140;
      mvarWDB_POLIZANN = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ == (org.w3c.dom.Node) null) )
      {
        if( new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN ) */ ) ).isNumeric() )
        {
          if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ ) ) < 100 )
          {
            mvarWDB_POLIZANN = String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN ) */ ) ) );
          }
        }
      }
      //
      wvarStep = 150;
      mvarWDB_POLIZSEC = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ == (org.w3c.dom.Node) null) )
      {
        if( new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC ) */ ) ).isNumeric() )
        {
          if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ ) ) < 1000000 )
          {
            mvarWDB_POLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC ) */ );
          }
        }
      }
      //
      wvarStep = 170;
      mvarWDB_TIEMPOPROCESO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TiempoProceso) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIEMPOPROCESO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TiempoProceso ) */ );
      }



      wvarStep = 180;
      mvarWDB_XML = "";
      mvarWDB_XML = pvarRequest;
      //
      wvarStep = 190;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      //
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_NRO_COTIZACION_LBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NROCOT.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NROCOT)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 18 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 230;
      wobjDBParm = new Parameter( "@WDB_TIPO_OPERACION", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_TIPOOPERAC.equals( "" ) ? "" : mvarWDB_TIPOOPERAC) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 240;
      wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_ESTADO.equals( "" ) ? "ER" : mvarWDB_ESTADO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 250;
      wobjDBParm = new Parameter( "@WDB_RECEPCION_PEDIDO", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant( mvarWDB_RECEPCION_PEDIDOANO + "-" + mvarWDB_RECEPCION_PEDIDOMES + "-" + mvarWDB_RECEPCION_PEDIDODIA + " " + mvarWDB_RECEPCION_PEDIDOHORA + ":" + mvarWDB_RECEPCION_PEDIDOMINUTO + ":" + mvarWDB_RECEPCION_PEDIDOSEGUNDO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 260;
      wobjDBParm = new Parameter( "@WDB_ENVIO_RESPUESTA", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant( DateTime.year( DateTime.now() ) + "-" + DateTime.month( DateTime.now() ) + "-" + DateTime.day( DateTime.now() ) + " " + DateTime.hour( DateTime.now() ) + ":" + DateTime.minute( DateTime.now() ) + ":" + DateTime.second( DateTime.now() ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 270;
      wobjDBParm = new Parameter( "@WDB_RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_RAMOPCOD.equals( "" ) ? "ATD1" : mvarWDB_RAMOPCOD) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 280;
      wobjDBParm = new Parameter( "@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 290;
      wobjDBParm = new Parameter( "@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 300;
      wobjDBParm = new Parameter( "@WDB_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 310;
      wobjDBParm = new Parameter( "@WDB_CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 320;
      wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CERTISEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CERTISEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 330;
      wobjDBParm = new Parameter( "@WDB_TIEMPO_PROCESO_AIS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIEMPOPROCESO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIEMPOPROCESO)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 340;
      wobjDBParm = new Parameter( "@WDB_OPERACION_XML", AdoConst.adVarChar, AdoConst.adParamInput, 8000, new Variant( (mvarWDB_XML.equals( "" ) ? "" : Strings.left( mvarWDB_XML, 8000 )) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 350;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      wobjDBCnn.close();

      fncPutSolHO = true;
      pvarRes.set( "<LBA_WS res_code=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" ) */ ) + "\" res_msg=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) + "\"></LBA_WS>" );
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
      return fncPutSolHO;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        fncPutSolHO = false;
        //mobjCOM_Context.SetComplete
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutSolHO;
  }

  private boolean fncPutSolicitud( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes, Variant pTiempoAIS )
  {
    boolean fncPutSolicitud = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Object wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLRequest1 = null;
    diamondedge.util.XmlDom wobjXMLPdf = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarcounter = 0;
    String wvarSitu = "";
    String mvarWDB_CODINST = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTIPOL = "";
    String mvarWDB_CERTIANN = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_SUPLENUM = "";
    String mvarWDB_NUMEDOCU = "";
    String mvarWDB_TIPODOCU = "";
    String mvarWDB_CLIENAP1 = "";
    String mvarWDB_CLIENAP2 = "";
    String mvarWDB_CLIENNOM = "";
    String mvarWDB_NACIMANN = "";
    String mvarWDB_NACIMMES = "";
    String mvarWDB_NACIMDIA = "";
    String mvarWDB_CLIENSEX = "";
    String mvarWDB_CLIENEST = "";
    String mvarWDB_EFECTANN = "";
    String mvarWDB_EFECTMES = "";
    String mvarWDB_EFECTDIA = "";
    String mvarWDB_EMAIL = "";
    String mvarWDB_DOMICDOM = "";
    String mvarWDB_DOMICDNU = "";
    String mvarWDB_DOMICPIS = "";
    String mvarWDB_DOMICPTA = "";
    String mvarWDB_DOMICPOB = "";
    String mvarWDB_BARRIOCOUNT = "";
    String mvarWDB_DOMICCPO = "";
    String mvarWDB_PROVICOD = "";
    String mvarTELCOD = "";
    String mvarWDB_TELNRO = "";
    String mvarWDB_DOMICDOMCO = "";
    String mvarWDB_DOMICDNUCO = "";
    String mvarWDB_DOMICPISCO = "";
    String mvarWDB_DOMICPTACO = "";
    String mvarWDB_DOMICPOBCO = "";
    String mvarWDB_DOMICCPOCO = "";
    String mvarWDB_PROVICODCO = "";
    String mvarTELCOD_CO = "";
    String mvarWDB_TELNRO_CO = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_CUENNUME = "";
    String mvarWDB_VENCIANN = "";
    String mvarWDB_VENCIMES = "";
    String mvarWDB_VENCIDIA = "";
    String mvarWDB_BANCOCOD = "";
    String mvarWDB_SUCURCOD = "";
    String mvarWDB_P_EMISIANN = "";
    String mvarWDB_P_EMISIMES = "";
    String mvarWDB_P_EMISIDIA = "";
    String mvarWDB_P_COBROCOD = "";
    String mvarWDB_P_PLANNCOD = "";
    String mvarWDB_P_BARRIOCOUNT = "";
    String mvarWDB_P_ZONA = "";
    String mvarWDB_P_CLIENIVA = "";
    String mvarWDB_BANELCO = "";
    String mvarWDB_USUARCOD = "";
    String mvarWDB_MENSAJE = "";
    String mvarNroCertiSec = "";
    String mvarDatosImpreso = "";
    String mvarNodoImpreso = "";
    String mvarImpreso64 = "";
    String mvarRequestImp = "";
    String mvarResponseImp = "";
    Recordset wrstDBResult = null;
    String mvarDOCUDES = "";
    String mvarCOBRODES = "";
    String mvarCAMPADES = "";
    String mvarNOMBROKER = "";
    String mvarPROVIDES = "";
    String mvarPROVIDESCO = "";
    String mvarWDB_VALOR = "";
    String mvarRequest = "";
    String mvarResponse = "";
    diamondedge.util.XmlDom mobjXMLDoc = null;
    String mvarRequestCob = "";
    String mvarResponseCob = "";
    diamondedge.util.XmlDom mobjXMLDocCob = null;
    String mvarPLANDES = "";
    String mvarCOBERDES = "";
    String mvarCUENNUME = "";
    String mvarCobOk = "";
    String hdatos = "";
    String vFECINIVG = "";
    String vFECFINVG = "";
    String vNROREFERENCIA = "";
    String mvarseltest = "";
    String mvarNomArch = "";
    String wvarPdfReq = "";
    String wvarPdfResp = "";
    int wvarCount = 0;
    int mvarSumaCob = 0;
    int wvariCounter = 0;
    String mvarEmpresaCodigo = "";
    String mvarSucursalCodigo = "";
    String mvarLegajoVend = "";
    String vOk = "";
    String mvar_TiempoProceso = "";
    String mvarInicioAIS = "";
    String mvarFinAIS = "";
    String mvarTiempoAIS = "";

    //
    //
    //
    //10/2010 variables para armar el impreso
    //AM
    // variables para calcular tiempo proceso de la impresi�n
    try 
    {

      vOk = "OK";

      // **************   INTEGRACION MSG 2210 y 2219**************************
      if( ! (invoke( "fncIntegrarSolicitudMQ", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje.toString()), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        return fncPutSolicitud;
      }

      // *****************   IMPRESO     **************************************
      //Recupero datos del request y el response
      wvarStep = 100;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 110;
      wobjXMLRequest1 = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest1.async = false;
      wobjXMLRequest1.loadXML( pvarRes.toString() );
      //
      wvarStep = 120;
      mvarWDB_VALOR = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VALOR) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_VALOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_VALOR ) */ );
      }
      //
      wvarStep = 130;
      mvarWDB_TIPODOCU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoDocu) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPODOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoDocu ) */ );
      }
      //
      wvarStep = 140;
      mvarWDB_COBROTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROTIP = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP ) */ ) );
      }
      //
      wvarStep = 150;
      mvarWDB_CODINST = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }
      //
      wvarStep = 160;
      mvarWDB_PROVICOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi ) */ );
      }
      //
      wvarStep = 170;
      mvarWDB_PROVICODCO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ProviCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICODCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ProviCo ) */ );
      }
      //
      wvarStep = 180;
      mvarWDB_P_PLANNCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PLANNCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_PLANNCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PLANNCOD ) */ );
      }
      //
      wvarStep = 190;
      mvarWDB_POLIZANN = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN ) */ );
      }
      //
      wvarStep = 200;
      mvarWDB_POLIZSEC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC ) */ );
      }
      //
      wvarStep = 210;
      mvarWDB_BANELCO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_BANELCO) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_BANELCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_BANELCO ) */ );
      }
      //
      wvarStep = 220;
      mvarWDB_CUENNUME = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CUENNUME = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CUENNUME ) */ );
      }
      //
      wvarStep = 230;
      mvarWDB_RAMOPCOD = "";
      if( ! (null /*unsup wobjXMLRequest1.selectSingleNode( ("//" + mcteParam_RAMOPCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest1.selectSingleNode( "//" + mcteParam_RAMOPCOD ) */ );
      }
      //
      wvarStep = 235;
      mvarWDB_MENSAJE = "";
      if( ! (null /*unsup wobjXMLRequest1.selectSingleNode( "//MENSAJE" ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_MENSAJE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest1.selectSingleNode( "//MENSAJE" ) */ );
      }
      //
      wvarStep = 240;
      mvarWDB_CERTIPOL = "";
      if( ! (null /*unsup wobjXMLRequest1.selectSingleNode( ("//" + mcteParam_CERTIPOL) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTIPOL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest1.selectSingleNode( "//" + mcteParam_CERTIPOL ) */ );
      }
      //
      wvarStep = 250;
      mvarWDB_CERTIANN = "";
      if( ! (null /*unsup wobjXMLRequest1.selectSingleNode( ("//" + mcteParam_CERTIANN) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTIANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest1.selectSingleNode( "//" + mcteParam_CERTIANN ) */ );
      }
      //
      wvarStep = 260;
      mvarWDB_SUPLENUM = "";
      if( ! (null /*unsup wobjXMLRequest1.selectSingleNode( ("//" + mcteParam_SUPLENUM) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_SUPLENUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest1.selectSingleNode( "//" + mcteParam_SUPLENUM ) */ );
      }
      //
      wvarStep = 270;
      mvarWDB_CERTISEC = "";
      if( ! (null /*unsup wobjXMLRequest1.selectSingleNode( ("//" + mcteParam_CERTISEC) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTISEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest1.selectSingleNode( "//" + mcteParam_CERTISEC ) */ );
      }
      //
      wvarStep = 280;
      mvarWDB_CLIENAP1 = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienAp1) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENAP1 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienAp1 ) */ );
      }
      //
      wvarStep = 290;
      mvarWDB_CLIENAP2 = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienAp2) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENAP2 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienAp2 ) */ );
      }
      //
      wvarStep = 300;
      mvarWDB_CLIENNOM = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienNom) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENNOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienNom ) */ );
      }
      //
      wvarStep = 310;
      mvarWDB_NUMEDOCU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NumeDocu) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NUMEDOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NumeDocu ) */ );
      }
      //
      wvarStep = 320;
      mvarWDB_TELNRO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TELNRO) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TELNRO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TELNRO ) */ );
      }
      //
      wvarStep = 330;
      mvarWDB_DOMICDOMCO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDomCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDOMCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDomCo ) */ );
      }
      //
      wvarStep = 340;
      mvarWDB_DOMICDNUCO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDNUCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDNUCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDNUCo ) */ );
      }
      //
      wvarStep = 350;
      mvarWDB_DOMICDNUCO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDNUCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDNUCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDNUCo ) */ );
      }
      //
      wvarStep = 360;
      mvarWDB_DOMICPISCO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPisCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPISCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPisCo ) */ );
      }
      //
      wvarStep = 370;
      mvarWDB_DOMICPTACO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPtaCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPTACO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPtaCo ) */ );
      }
      //
      wvarStep = 380;
      mvarWDB_DOMICPOBCO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICPOBCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPOBCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICPOBCo ) */ );
      }
      //
      wvarStep = 390;
      mvarWDB_NACIMDIA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimDia) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimDia ) */ );
      }
      //
      wvarStep = 400;
      mvarWDB_NACIMMES = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimMes) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimMes ) */ );
      }
      //
      wvarStep = 410;
      mvarWDB_NACIMANN = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimAnn) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimAnn ) */ );
      }
      //
      wvarStep = 420;
      mvarWDB_USUARCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_USUARCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_USUARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_USUARCOD ) */ );
      }
      //
      //Ejectua sp para descripciones
      wvarStep = 430;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProcImpre );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );


      wvarStep = 440;
      wobjDBParm = new Parameter( "@DOCUMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPODOCU.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPODOCU)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 450;
      wobjDBParm = new Parameter( "@COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_COBROTIP.equals( "" ) ? "" : mvarWDB_COBROTIP) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 460;
      wobjDBParm = new Parameter( "@CODINST", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarWDB_CODINST ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 470;
      wobjDBParm = new Parameter( "@CODPROV", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 480;
      wobjDBParm = new Parameter( "@CODPROVCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICODCO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICODCO)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 490;
      mvarseltest = mcteStoreProcImpre + " ";
      for( wvarCount = 0; wvarCount <= (wobjDBCmd.getParameters().getCount() - 1); wvarCount++ )
      {
        if( (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adChar) || (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adVarChar) )
        {
          mvarseltest = mvarseltest + "'" + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + "',";
        }
        else
        {
          mvarseltest = mvarseltest + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + ",";
        }
      }

      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );

      wvarStep = 500;
      if( ! (wrstDBResult.isEOF()) )
      {
        mvarDOCUDES = wrstDBResult.getFields().getField("DOCUDES").getValue().toString();
        mvarCOBRODES = wrstDBResult.getFields().getField("COBRODES").getValue().toString();
        mvarPROVIDES = wrstDBResult.getFields().getField("PROVIDES").getValue().toString();
        mvarPROVIDESCO = wrstDBResult.getFields().getField("PROVIDESCO").getValue().toString();
      }
      else
      {
        mvarDOCUDES = "";
        mvarCOBRODES = "";
        mvarNOMBROKER = "";
        mvarPROVIDES = "";
        mvarPROVIDESCO = "";
      }

      wvarStep = 510;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResult.close();
        }
      }

      wvarStep = 520;
      wrstDBResult = (Recordset) null;

      wobjDBCnn.close();

      // Detalle de Coberturas
      wvarStep = 530;
      mvarRequestCob = "<Request><DEFINICION>2330_CoberturasParaImpresion.xml</DEFINICION>";
      mvarRequestCob = mvarRequestCob + "<RAMOPCOD>ATD1</RAMOPCOD>";
      mvarRequestCob = mvarRequestCob + "<POLIZANN>" + mvarWDB_POLIZANN + "</POLIZANN>";
      mvarRequestCob = mvarRequestCob + "<POLIZSEC>" + mvarWDB_POLIZSEC + "</POLIZSEC>";
      mvarRequestCob = mvarRequestCob + "<PLANNCOD>" + mvarWDB_P_PLANNCOD + "</PLANNCOD>";
      mvarRequestCob = mvarRequestCob + "<AUUSOCOD>0</AUUSOCOD></Request>";

      wvarStep = 540;
      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( new Variant( mvarRequestCob ), new Variant( mvarResponseCob ), "" );
      wobjClass = null;
      mobjXMLDocCob = new diamondedge.util.XmlDom();
      //unsup mobjXMLDocCob.async = false;
      mobjXMLDocCob.loadXML( mvarResponseCob );

      wvarStep = 550;
      mvarCobOk = "ER";
      if( ! (null /*unsup mobjXMLDocCob.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDocCob.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {
          mvarCobOk = "OK";

          wvarStep = 560;
          wobjXMLList = null /*unsup mobjXMLDocCob.selectNodes( "//CAMPOS/TEXTOS/TEXTO" ) */;
          //
          for( wvarcounter = 1; wvarcounter <= Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDocCob.selectSingleNode( "//CANTIDAD" ) */ ) ); wvarcounter++ )
          {
            if( wobjXMLList.getLength() >= wvarcounter )
            {
              wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
              wvarStep = 570;
              hdatos = "<DESCCOBERTURAS>";
              hdatos = hdatos + "<COBERDESC>![CDATA[";
              hdatos = hdatos + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( "//COBERDES" ) */ );
              hdatos = hdatos + "]]</COBERDESC>";
              hdatos = hdatos + "<COBERADIC>![CDATA[";
              hdatos = hdatos + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( "//PLANNDES" ) */ );
              hdatos = hdatos + "]]</COBERADIC>";
              hdatos = hdatos + "</DESCCOBERTURAS>";

              wobjXMLNode = (org.w3c.dom.Node) null;
            }
          }
        }
      }
      else
      {
        hdatos = "<DESCCOBERTURAS>";
        hdatos = hdatos + "<COBERDESC/>";
        hdatos = hdatos + "<COBERADIC/>";
        hdatos = hdatos + "</DESCCOBERTURAS>";
      }

      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      wvarStep = 580;
      if( mvarCobOk.equals( "ER" ) )
      {
        wvarCodErr.set( -53 );
        pvarRes.set( "<LBA_WS res_code=\"-53\" res_msg=\"Error\"><Response><Estado resultado=\"false\" mensaje=\"Poliza sin Coberturas\" /></Response></LBA_WS>" );
        return fncPutSolicitud;
      }
      // Fin Detalle de coberturas
      wvarStep = 590;
      //Calculo fechas de vigencia
      //AM la fecha de vigencia debe ser el 1 del mes actual.
      vFECINIVG = invoke( "fechaVige", new Variant[] { new Variant(new Variant( DateTime.dateSerial( new Variant(DateTime.year( new Variant(DateTime.now()) )), new Variant(DateTime.month( new Variant(DateTime.now()) )), new Variant(1) ) ))/*warning: ByRef value change will be lost.*/ } );
      vFECFINVG = invoke( "fechaVige", new Variant[] { new Variant(new Variant( DateTime.dateSerial( new Variant(DateTime.year( new Variant(DateTime.now()) )), new Variant(DateTime.month( new Variant(DateTime.now()) ) + 1), new Variant(1) ) ))/*warning: ByRef value change will be lost.*/ } );
      //vFECINIVG = fechaVige(DateSerial(CInt(wobjXMLRequest.selectSingleNode("//VIGENANN").Text), CInt(wobjXMLRequest.selectSingleNode("//VIGENMES").Text), CInt(wobjXMLRequest.selectSingleNode("//VIGENDIA").Text)))
      //vFECFINVG = fechaVige(DateSerial(CInt(wobjXMLRequest.selectSingleNode("//VIGENANN").Text), CInt(wobjXMLRequest.selectSingleNode("//VIGENMES").Text), CInt(wobjXMLRequest.selectSingleNode("//VIGENDIA").Text)) + 15)
      vNROREFERENCIA = mvarWDB_RAMOPCOD + "-" + mvarWDB_CERTIPOL + "-" + mvarWDB_CERTISEC;

      //conforma xml para la impresi�n
      wvarStep = 600;
      mvarDatosImpreso = "<DATOS>";
      mvarDatosImpreso = mvarDatosImpreso + "<DATOSCLIENTE>";
      mvarDatosImpreso = mvarDatosImpreso + "<APELLIDORAZONSOCIAL><![CDATA[" + mvarWDB_CLIENAP1 + " " + mvarWDB_CLIENAP2 + "]]></APELLIDORAZONSOCIAL>";
      mvarDatosImpreso = mvarDatosImpreso + "<NOMBRE><![CDATA[" + mvarWDB_CLIENNOM + "]]></NOMBRE>";
      mvarDatosImpreso = mvarDatosImpreso + "<TIPODOC>" + mvarDOCUDES + "</TIPODOC>";
      mvarDatosImpreso = mvarDatosImpreso + "<NRODOCUMENTO>" + mvarWDB_NUMEDOCU + "</NRODOCUMENTO>";
      mvarDatosImpreso = mvarDatosImpreso + "<TEL>" + mvarWDB_TELNRO + "</TEL>";
      mvarDatosImpreso = mvarDatosImpreso + "<EMAIL/>";
      mvarDatosImpreso = mvarDatosImpreso + "<DOMICORRESP>" + mvarWDB_DOMICDOMCO + " " + mvarWDB_DOMICDNUCO + " " + mvarWDB_DOMICPISCO + " " + mvarWDB_DOMICPTACO + " " + mvarWDB_DOMICPOBCO + "</DOMICORRESP>";
      mvarDatosImpreso = mvarDatosImpreso + "<DOMICORRESP>" + mvarWDB_DOMICDOMCO + "</DOMICORRESP>";
      mvarDatosImpreso = mvarDatosImpreso + "<DOMINRO>" + mvarWDB_DOMICDNUCO + "</DOMINRO>";
      mvarDatosImpreso = mvarDatosImpreso + "<LOCALIDAD>" + mvarWDB_DOMICPOBCO + "</LOCALIDAD>";
      mvarDatosImpreso = mvarDatosImpreso + "<PROV>" + mvarPROVIDESCO + "</PROV>";
      mvarDatosImpreso = mvarDatosImpreso + "<FECNACIMIENTO>" + mvarWDB_NACIMDIA + "/" + mvarWDB_NACIMMES + "/" + mvarWDB_NACIMANN + "</FECNACIMIENTO>";
      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) ) */ ) ).equals( "S" ) )
      {
        mvarDatosImpreso = mvarDatosImpreso + "<ESTADOCIVIL>SOLTERO/A</ESTADOCIVIL>";
      }
      else
      {
        if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) ) */ ) ).equals( "C" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<ESTADOCIVIL>CASADO/A</ESTADOCIVIL>";
        }
        else
        {
          if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) ) */ ) ).equals( "E" ) )
          {
            mvarDatosImpreso = mvarDatosImpreso + "<ESTADOCIVIL>SEPARADO/A</ESTADOCIVIL>";
          }
          else
          {
            if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) ) */ ) ).equals( "V" ) )
            {
              mvarDatosImpreso = mvarDatosImpreso + "<ESTADOCIVIL>VIUDO/A</ESTADOCIVIL>";
            }
            else
            {
              if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) ) */ ) ).equals( "D" ) )
              {
                mvarDatosImpreso = mvarDatosImpreso + "<ESTADOCIVIL>DIVORCIADO/A</ESTADOCIVIL>";
              }
              else
              {
                mvarDatosImpreso = mvarDatosImpreso + "<ESTADOCIVIL></ESTADOCIVIL>";
              }
            }
          }
        }
      }
      mvarDatosImpreso = mvarDatosImpreso + "<PAISNACIMIENTO>ARGENTINA</PAISNACIMIENTO>";
      if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENSEX) ) */ ) ).equals( "M" ) )
      {
        mvarDatosImpreso = mvarDatosImpreso + "<SEXO>MASCULINO</SEXO>";
      }
      else
      {
        mvarDatosImpreso = mvarDatosImpreso + "<SEXO>FEMENINO</SEXO>";
      }
      mvarDatosImpreso = mvarDatosImpreso + "</DATOSCLIENTE>";
      mvarDatosImpreso = mvarDatosImpreso + "<CONDICIONESCOTIZACION>";
      mvarDatosImpreso = mvarDatosImpreso + "<POLIZACOL>" + mvarWDB_POLIZSEC + "</POLIZACOL>";
      mvarDatosImpreso = mvarDatosImpreso + "<PRODUCTOR></PRODUCTOR>";
      mvarDatosImpreso = mvarDatosImpreso + "<NROREFERENCIA></NROREFERENCIA>";
      mvarDatosImpreso = mvarDatosImpreso + "<PRECIOMENS>" + mvarWDB_VALOR + "</PRECIOMENS>";
      mvarDatosImpreso = mvarDatosImpreso + "<FORMADEPAGO>" + mvarCOBRODES + "</FORMADEPAGO>";
      mvarDatosImpreso = mvarDatosImpreso + "<DESC_FP1/>";
      mvarDatosImpreso = mvarDatosImpreso + "<DESC_FP2/>";
      mvarDatosImpreso = mvarDatosImpreso + "<BANCO></BANCO>";
      mvarDatosImpreso = mvarDatosImpreso + "<VIGENCIADESDE>" + vFECINIVG + " (desde las 12 hs.)</VIGENCIADESDE>";
      mvarDatosImpreso = mvarDatosImpreso + "<VIGENCIAHASTA>" + vFECFINVG + " (hasta las 12 hs.)</VIGENCIAHASTA>";
      mvarDatosImpreso = mvarDatosImpreso + "</CONDICIONESCOTIZACION>";
      mvarDatosImpreso = mvarDatosImpreso + "<DATOSRIESGO>";
      mvarDatosImpreso = mvarDatosImpreso + "<NROBANELCO>" + mvarWDB_BANELCO + "</NROBANELCO>";
      mvarDatosImpreso = mvarDatosImpreso + "<CAJAAHORRO>" + mvarWDB_CUENNUME + "</CAJAAHORRO>";
      mvarDatosImpreso = mvarDatosImpreso + "</DATOSRIESGO>";
      mvarDatosImpreso = mvarDatosImpreso + "<DESCRIPCIONESCOB>";
      mvarDatosImpreso = mvarDatosImpreso + hdatos;
      mvarDatosImpreso = mvarDatosImpreso + "</DESCRIPCIONESCOB>";
      mvarDatosImpreso = mvarDatosImpreso + "<DATOSCOTIZACION>";
      mvarDatosImpreso = mvarDatosImpreso + "<MONEDA>Pesos</MONEDA>";
      mvarDatosImpreso = mvarDatosImpreso + "<NROSOLI>" + vNROREFERENCIA + "</NROSOLI>";
      mvarDatosImpreso = mvarDatosImpreso + "<FECHACOTIZACION>" + DateTime.format( DateTime.now(), "dd MMMM, yyyy" ) + "</FECHACOTIZACION>";
      mvarDatosImpreso = mvarDatosImpreso + "</DATOSCOTIZACION>";
      mvarDatosImpreso = mvarDatosImpreso + "</DATOS>";
      // Fin xml de impresion
      //imprimo siempre  IMPSOLI SOLI_ATD por BIRT(MW)
      wvarStep = 610;
      mvarRequestImp = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>" + mvarDatosImpreso + "</xmlDataSource>" + "<reportId>SOLI_ATD</reportId>" + "</Request>";
      wvarStep = 620;
      mvarNodoImpreso = "";
      //Set wobjClass = mobjCOM_Context.CreateInstance("LBAA_MWGenerico.lbaw_MQMW")
      //Call wobjClass.Execute(mvarRequestImp, mvarResponseImp, "")
      //Set wobjClass = Nothing
      //Set mobjXMLDoc = CreateObject("MSXML2.DOMDocument")
      //mobjXMLDoc.async = False
      //Call mobjXMLDoc.loadXML(mvarResponseImp)
      //wvarStep = 630
      //'Verifica que haya respuesta de MQ
      //If mobjXMLDoc.selectSingleNode("//Estado").Attributes.getNamedItem("resultado").Text = "true" Then
      //    'Verifica que exista el PDF si no error Middle
      //    If Not mobjXMLDoc.selectSingleNode("//generatePdfReportReturn") Is Nothing Then
      //           mvarNodoImpreso = mobjXMLDoc.documentElement.selectSingleNode("//generatePdfReportReturn").Text
      //    Else
      //           mvarNodoImpreso = "No se pudo obtener el impreso MDW" 'error de mdw
      //    End If
      //Else
      //    mvarNodoImpreso = "No se pudo obtener el impreso MQ"
      //End If
      //Set mobjXMLDoc = Nothing
      mvarImpreso64 = mvarImpreso64 + "<IMPSOLI>" + mvarNodoImpreso + "</IMPSOLI>";
      //wvarStep = 640
      // Si la p�liza fue Emitida imprimo MPOLI por MSJ 1710 y despues recupero el binary por lbaw_ovGetBinaryFile
      mvarInicioAIS = DateTime.format( DateTime.now() );
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest1.selectSingleNode( "//SITUACION" ) */ ).equals( "E" ) )
      {

        wvarSitu = "E";
        if( mvarWDB_MENSAJE.equals( "" ) )
        {
          wvarMensaje.set( "PROPUESTA EMITIDA" );
        }
        else
        {
          wvarMensaje.set( mvarWDB_MENSAJE );
        }

        mvarNodoImpreso = "";
        //
        mvarRequest = "<Request>";
        mvarRequest = mvarRequest + "<USUARIO>" + mvarWDB_USUARCOD + "</USUARIO>";
        mvarRequest = mvarRequest + "<RAMOPCOD>" + mvarWDB_RAMOPCOD + "</RAMOPCOD>";
        mvarRequest = mvarRequest + "<POLIZANN>" + mvarWDB_POLIZANN + "</POLIZANN>";
        mvarRequest = mvarRequest + "<POLIZSEC>" + mvarWDB_POLIZSEC + "</POLIZSEC>";
        mvarRequest = mvarRequest + "<CERTIPOL>" + mvarWDB_CERTIPOL + "</CERTIPOL>";
        mvarRequest = mvarRequest + "<CERTIANN>" + mvarWDB_CERTIANN + "</CERTIANN>";
        mvarRequest = mvarRequest + "<CERTISEC>" + mvarWDB_CERTISEC + "</CERTISEC>";
        mvarRequest = mvarRequest + "<SUPLENUM>" + mvarWDB_SUPLENUM + "</SUPLENUM>";
        mvarRequest = mvarRequest + "<OPERAPOL>0</OPERAPOL>";
        mvarRequest = mvarRequest + "<TIPODOCU>PC</TIPODOCU>";
        mvarRequest = mvarRequest + "<TIPOIMPR>CO</TIPOIMPR>";
        mvarRequest = mvarRequest + "<MANTENERARCHIVO></MANTENERARCHIVO>";
        mvarRequest = mvarRequest + "</Request>";
        //
        wvarStep = 650;
        wobjClass = new lbawA_OVMQEmision.lbaw_OVImprimirPoliza();
        wobjClass.Execute( mvarRequest, mvarResponse, "" );
        wobjClass = (Object) null;
        mobjXMLDoc = new diamondedge.util.XmlDom();
        //unsup mobjXMLDoc.async = false;
        mobjXMLDoc.loadXML( mvarResponse );
        //Verifica que haya respuesta
        wvarStep = 660;
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Estado" ) */.getAttributes().getNamedItem( "resultado" ) ).equals( "true" ) )
        {
          //Verifica que exista el PDF si no error
          wvarStep = 670;
          if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//FILES/FILE/RUTA" ) */ == (org.w3c.dom.Node) null) )
          {
            mvarNomArch = diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.getDocument().getDocumentElement().selectSingleNode( "//FILES/FILE/RUTA" ) */ );

            //Recupero el binaryFile
            wvarStep = 430;
            wvarPdfReq = "<Request><RUTA>" + mvarNomArch + "</RUTA></Request>";

            wvarStep = 680;
            wobjClass = new lbawA_OVMQEmision.lbaw_OVGetBinaryFile();
            wobjClass.Execute( new Variant( wvarPdfReq ), new Variant( wvarPdfResp ), "" );
            wobjClass = (Object) null;

            wvarStep = 690;
            wobjXMLPdf = new diamondedge.util.XmlDom();
            //unsup wobjXMLPdf.async = false;
            wobjXMLPdf.loadXML( wvarPdfResp );


            wvarStep = 700;
            if( ! (null /*unsup wobjXMLPdf.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
            {
              if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLPdf.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
              {
                mvarNodoImpreso = "No se pudo obtener el impreso";
              }
              else
              {
                mvarNodoImpreso = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLPdf.selectSingleNode( "//BinData" ) */ );
              }
            }
            else
            {
              mvarNodoImpreso = "";
            }
          }
          else
          {
            mvarNodoImpreso = "";
          }
        }
        else
        {
          mvarNodoImpreso = "";
        }

        mobjXMLDoc = (diamondedge.util.XmlDom) null;
        mvarImpreso64 = mvarImpreso64 + "<IMPCERTI></IMPCERTI><IMPPOLI>" + mvarNodoImpreso + "</IMPPOLI>";

        //imprimo IMPCERTI por BIRT(MW)
      }
      else if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest1.selectSingleNode( "//SITUACION" ) */ ).equals( "I" ) )
      {
        wvarSitu = "I";
        if( mvarWDB_MENSAJE.equals( "" ) )
        {
          wvarMensaje.set( "PROPUESTA INGRESADA" );
        }
        else
        {
          wvarMensaje.set( mvarWDB_MENSAJE );
        }

        mvarNodoImpreso = "";
        //
        //   wvarStep = 710
        //   mvarRequestImp = "<Request>" & _
        //  '              "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" & _
        //              "<Raiz>share:generatePdfReport</Raiz>" & _
        //  '              "<xmlDataSource>" & mvarDatosImpreso & "</xmlDataSource>" & _
        //              "<reportId>CERT_PROV_ATD</reportId>" & _
        //  '              "</Request>"
        //   wvarStep = 720
        //   Set wobjClass = mobjCOM_Context.CreateInstance("LBAA_MWGenerico.lbaw_MQMW")
        //   Call wobjClass.Execute(mvarRequestImp, mvarResponseImp, "")
        //   Set wobjClass = Nothing
        //   Set mobjXMLDoc = CreateObject("MSXML2.DOMDocument")
        //   mobjXMLDoc.async = False
        //   Call mobjXMLDoc.loadXML(mvarResponseImp)
        //   'Verifica que haya respuesta de MQ
        //   wvarStep = 730
        //   If mobjXMLDoc.selectSingleNode("//Estado").Attributes.getNamedItem("resultado").Text = "true" Then
        //       'Verifica que exista el PDF si no error
        //       If Not mobjXMLDoc.selectSingleNode("//generatePdfReportReturn") Is Nothing Then
        //              mvarNodoImpreso = mobjXMLDoc.documentElement.selectSingleNode("//generatePdfReportReturn").Text
        //       Else
        //              mvarNodoImpreso = "No se pudo obtener el impreso"
        //       End If
        //   Else
        //       mvarNodoImpreso = "No se pudo obtener el impreso"
        //      End If
        //
        //   Set mobjXMLDoc = Nothing
        mvarImpreso64 = mvarImpreso64 + "<IMPCERTI>" + mvarNodoImpreso + "</IMPCERTI><IMPPOLI></IMPPOLI>";

      }
      else
      {
        vOk = "ER";
      }

      mvarFinAIS = DateTime.format( DateTime.now() );
      pTiempoAIS.set( DateTime.diff( "S", DateTime.toDate( mvarInicioAIS ), DateTime.toDate( mvarFinAIS ) ) );

      //fin impreso, Se agrega en pvarRes el nodo <PDF64>
      if( vOk.equals( "OK" ) )
      {
        wvarStep = 740;
        pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + "<Response>" + "<REQUESTID>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ ) + "</REQUESTID>" + "<POLIZANN>" + mvarWDB_POLIZANN + "</POLIZANN>" + "<POLIZSEC>" + mvarWDB_POLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + mvarWDB_CERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + mvarWDB_CERTIANN + "</CERTIANN>" + "<CERTISEC>" + mvarWDB_CERTISEC + "</CERTISEC>" + "<SUPLENUM>" + mvarWDB_SUPLENUM + "</SUPLENUM>" + "<MENSAJE>" + wvarMensaje + "</MENSAJE>" + "<SITUACION>" + wvarSitu + "</SITUACION>" + "<PDF64>" + mvarImpreso64 + "</PDF64>" + "</Response>" + "</LBA_WS>" );
        fncPutSolicitud = true;
      }
      else
      {
        wvarMensaje.set( mvarWDB_MENSAJE );
        wvarStep = 750;
        pvarRes.set( "<LBA_WS res_code=\"-54\" res_msg=\"Error\"><Response><Estado resultado=\"false\" mensaje=" + wvarMensaje + " /></Response></LBA_WS>" );
      }

      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjClass = (Object) null;
      return fncPutSolicitud;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + "<Response>" + "<REQUESTID>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ ) + "</REQUESTID>" + "<POLIZANN>" + mvarWDB_POLIZANN + "</POLIZANN>" + "<POLIZSEC>" + mvarWDB_POLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + mvarWDB_CERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + mvarWDB_CERTIANN + "</CERTIANN>" + "<CERTISEC>" + mvarWDB_CERTISEC + "</CERTISEC>" + "<SUPLENUM>" + mvarWDB_SUPLENUM + "</SUPLENUM>" + "<MENSAJE>" + wvarMensaje + "</MENSAJE>" + "<SITUACION>" + wvarSitu + "</SITUACION>" + "<PDF64><IMPCERTI/><IMPSOLI/><IMPPOLI/></PDF64>" + "</Response>" + "</LBA_WS>" );

        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        //fncPutSolicitud = False
        //mobjCOM_Context.SetComplete
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutSolicitud;
  }

  private boolean fncIntegrarSolicitudMQ( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncIntegrarSolicitudMQ = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String mvarRequest = "";
    String mvarRequest1 = "";
    String mvarResponse = "";
    Variant oProd = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom mobjXMLDoc = null;
    lbawA_OVMQGen.lbaw_MQMensaje wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
    String mvarProdok = "";
    org.w3c.dom.NodeList wobjXMLList = null;
    int wvarContNode = 0;
    String vNROCOTI = "";
    Variant vMES = new Variant();
    Variant vANO = new Variant();
    String vFECINIVG = "";
    Variant vMESsig = new Variant();
    String vANOsig = "";
    String vFECFINVG = "";
    String vTPLANATM = "";
    String vPOLIZANN = "";
    String vPOLIZSEC = "";
    String vREQUESTID = "";
    String vUSUARCOD = "";
    String vNROSOLTMP = "";
    String vCANAL = "";
    String vNROSOLI = "";
    String vSOLICITU = "";
    String vCOBROCOD = "";
    Variant vCOPROTIP = new Variant();
    String vCUENNUME = "";
    String vCOBROTIP = "";
    String vCLIENNOM = "";
    String vCLIENAP1 = "";
    String vCLIENAP2 = "";
    String vSEXO = "";
    String vNACIMANN = "";
    String vNACIMMES = "";
    String vNACIMDIA = "";
    String vESTADO = "";
    String vEMAIL = "";
    String vTIPODOCU = "";
    String vNUMEDOCU = "";
    String vCLIENIVA = "";
    String vDOMICDOM = "";
    String vDOMICDNU = "";
    String vDOMICPIS = "";
    String vDOMICPTA = "";
    String vDOMICPOB = "";
    String vLOCALIDAD = "";
    String vPROVCOD = "";
    String vTELNRO = "";
    String vBANELCO = "";
    String vCANAL_SUCURSAL = "";
    String vLEGAJCIA = "";
    String vLEGAJO_VEND = "";
    String vLEGAJAPEAP = "";
    String vLEGAJNOMAP = "";
    String vDOMICDOMCOR = "";
    String vDOMICDNUCOR = "";
    String vDOMICPISCOR = "";
    String vDOMICPTACOR = "";
    String vDOMICPOBCOR = "";
    String vLOCALIDADCODCOR = "";
    String vPROVICOR = "";




    wvarStep = 0;
    wvarCodErr.set( 0 );
    try 
    {

      wvarStep = 2;
      // Traigo los par�metros del request
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 3;
      vNROCOTI = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/COT_NRO" ) */ == (org.w3c.dom.Node) null) )
      {
        vNROCOTI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/COT_NRO" ) */ );
      }

      wvarStep = 4;
      vREQUESTID = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/REQUESTID" ) */ == (org.w3c.dom.Node) null) )
      {
        vREQUESTID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/REQUESTID" ) */ );
      }

      wvarStep = 5;
      vUSUARCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/USUARCOD" ) */ == (org.w3c.dom.Node) null) )
      {
        vUSUARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/USUARCOD" ) */ );
      }

      wvarStep = 6;
      vFECINIVG = Funciones.convertirFecha( new Variant( DateTime.dateSerial( DateTime.year( DateTime.now() ), DateTime.month( DateTime.now() ), 1 ) )/*warning: ByRef value change will be lost.*/ );

      wvarStep = 7;
      vFECFINVG = Funciones.convertirFecha( new Variant( DateTime.dateSerial( DateTime.year( DateTime.now() ), DateTime.month( DateTime.now() ) + 1, 1 ) )/*warning: ByRef value change will be lost.*/ );

      wvarStep = 8;
      vCANAL = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CANAL" ) */ == (org.w3c.dom.Node) null) )
      {
        vCANAL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CANAL" ) */ );
      }

      wvarStep = 9;
      vCANAL_SUCURSAL = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CANAL_SUCURSAL" ) */ == (org.w3c.dom.Node) null) )
      {
        vCANAL_SUCURSAL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CANAL_SUCURSAL" ) */ );
      }

      wvarStep = 10;
      vNROSOLI = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NROSOLI" ) */ == (org.w3c.dom.Node) null) )
      {
        vNROSOLI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NROSOLI" ) */ );
      }


      wvarStep = 12;
      vPOLIZANN = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/POLIZANN" ) */ == (org.w3c.dom.Node) null) )
      {
        vPOLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/POLIZANN" ) */ );
      }

      wvarStep = 13;
      vPOLIZSEC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/POLIZSEC" ) */ == (org.w3c.dom.Node) null) )
      {
        vPOLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/POLIZSEC" ) */ );
      }

      wvarStep = 14;
      vSOLICITU = Strings.right( "0000" + vCANAL, 4 ) + Strings.right( ("000000" + vNROSOLI), 6 ) + "ATD1" + Strings.right( ("00" + vPOLIZANN), 2 ) + Strings.right( ("000000" + vPOLIZSEC), 6 );

      wvarStep = 15;
      vTPLANATM = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/TPLANATM" ) */ == (org.w3c.dom.Node) null) )
      {
        vTPLANATM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/TPLANATM" ) */ );
      }

      wvarStep = 16;
      vCOBROCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/FPAGO" ) */ == (org.w3c.dom.Node) null) )
      {
        vCOBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/FPAGO" ) */ );
      }

      wvarStep = 17;
      vCOBROTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/COBROTIP" ) */ == (org.w3c.dom.Node) null) )
      {
        vCOBROTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/COBROTIP" ) */ );
      }

      wvarStep = 18;
      vCUENNUME = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CUENNUME" ) */ == (org.w3c.dom.Node) null) )
      {
        vCUENNUME = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CUENNUME" ) */ );
      }

      wvarStep = 19;
      vCLIENNOM = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENNOM" ) */ == (org.w3c.dom.Node) null) )
      {
        vCLIENNOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENNOM" ) */ );
      }

      wvarStep = 20;
      vCLIENAP1 = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENAP1" ) */ == (org.w3c.dom.Node) null) )
      {
        vCLIENAP1 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENAP1" ) */ );
      }

      wvarStep = 21;
      vCLIENAP2 = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENAP2" ) */ == (org.w3c.dom.Node) null) )
      {
        vCLIENAP2 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENAP2" ) */ );
      }

      wvarStep = 22;
      vSEXO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/SEXO" ) */ == (org.w3c.dom.Node) null) )
      {
        vSEXO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/SEXO" ) */ );
      }

      wvarStep = 23;
      vNACIMANN = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NACIMANN" ) */ == (org.w3c.dom.Node) null) )
      {
        vNACIMANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NACIMANN" ) */ );
      }

      wvarStep = 24;
      vNACIMMES = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NACIMMES" ) */ == (org.w3c.dom.Node) null) )
      {
        vNACIMMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NACIMMES" ) */ );
      }

      wvarStep = 25;
      vNACIMDIA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NACIMDIA" ) */ == (org.w3c.dom.Node) null) )
      {
        vNACIMDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NACIMDIA" ) */ );
      }

      wvarStep = 26;
      vESTADO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/ESTADO" ) */ == (org.w3c.dom.Node) null) )
      {
        vESTADO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/ESTADO" ) */ );
      }

      wvarStep = 27;
      vEMAIL = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/EMAIL" ) */ == (org.w3c.dom.Node) null) )
      {
        vEMAIL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/EMAIL" ) */ );
      }

      wvarStep = 28;
      vTIPODOCU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/TIPODOCU" ) */ == (org.w3c.dom.Node) null) )
      {
        vTIPODOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/TIPODOCU" ) */ );
      }

      wvarStep = 29;
      vNUMEDOCU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NUMEDOCU" ) */ == (org.w3c.dom.Node) null) )
      {
        vNUMEDOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NUMEDOCU" ) */ );
      }

      wvarStep = 30;
      vCLIENIVA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENIVA" ) */ == (org.w3c.dom.Node) null) )
      {
        vCLIENIVA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENIVA" ) */ );
      }

      wvarStep = 31;
      vDOMICDOM = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDOM" ) */ == (org.w3c.dom.Node) null) )
      {
        vDOMICDOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDOM" ) */ );
      }

      wvarStep = 32;
      vDOMICDNU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDNU" ) */ == (org.w3c.dom.Node) null) )
      {
        vDOMICDNU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDNU" ) */ );
      }

      wvarStep = 33;
      vDOMICPIS = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPIS" ) */ == (org.w3c.dom.Node) null) )
      {
        vDOMICPIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPIS" ) */ );
      }

      wvarStep = 34;
      vDOMICPTA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPTA" ) */ == (org.w3c.dom.Node) null) )
      {
        vDOMICPTA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPTA" ) */ );
      }

      wvarStep = 35;
      vDOMICPOB = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPOB" ) */ == (org.w3c.dom.Node) null) )
      {
        vDOMICPOB = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPOB" ) */ );
      }

      wvarStep = 36;
      vLOCALIDAD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LOCALIDAD" ) */ == (org.w3c.dom.Node) null) )
      {
        vLOCALIDAD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LOCALIDAD" ) */ );
      }

      wvarStep = 37;
      vPROVCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/PROVCOD" ) */ == (org.w3c.dom.Node) null) )
      {
        vPROVCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/PROVCOD" ) */ );
      }

      wvarStep = 311;
      vDOMICDOMCOR = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDOMCOR" ) */ == (org.w3c.dom.Node) null) )
      {
        vDOMICDOMCOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDOMCOR" ) */ );
      }

      wvarStep = 321;
      vDOMICDNUCOR = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDNUCOR" ) */ == (org.w3c.dom.Node) null) )
      {
        vDOMICDNUCOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDNUCOR" ) */ );
      }

      wvarStep = 331;
      vDOMICPISCOR = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPISCOR" ) */ == (org.w3c.dom.Node) null) )
      {
        vDOMICPISCOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPISCOR" ) */ );
      }

      wvarStep = 341;
      vDOMICPTACOR = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPTACOR" ) */ == (org.w3c.dom.Node) null) )
      {
        vDOMICPTACOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPTACOR" ) */ );
      }

      wvarStep = 351;
      vDOMICPOBCOR = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPOBCOR" ) */ == (org.w3c.dom.Node) null) )
      {
        vDOMICPOBCOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPOBCOR" ) */ );
      }

      wvarStep = 361;
      vLOCALIDADCODCOR = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LOCALIDADCODCOR" ) */ == (org.w3c.dom.Node) null) )
      {
        vLOCALIDADCODCOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LOCALIDADCODCOR" ) */ );
      }

      wvarStep = 371;
      vPROVICOR = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/PROVICOR" ) */ == (org.w3c.dom.Node) null) )
      {
        vPROVICOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/PROVICOR" ) */ );
      }

      wvarStep = 38;
      vTELNRO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/TELNRO" ) */ == (org.w3c.dom.Node) null) )
      {
        vTELNRO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/TELNRO" ) */ );
      }

      wvarStep = 39;
      vBANELCO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/BANELCO" ) */ == (org.w3c.dom.Node) null) )
      {
        vBANELCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/BANELCO" ) */ );
      }

      wvarStep = 40;
      vLEGAJCIA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJCIA" ) */ == (org.w3c.dom.Node) null) )
      {
        vLEGAJCIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJCIA" ) */ );
      }

      wvarStep = 41;
      vLEGAJO_VEND = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJO_VEND" ) */ == (org.w3c.dom.Node) null) )
      {
        vLEGAJO_VEND = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJO_VEND" ) */ );
      }

      wvarStep = 42;
      vLEGAJAPEAP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJAPEAP" ) */ == (org.w3c.dom.Node) null) )
      {
        vLEGAJAPEAP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJAPEAP" ) */ );
      }

      wvarStep = 43;
      vLEGAJNOMAP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJNOMAP" ) */ == (org.w3c.dom.Node) null) )
      {
        vLEGAJNOMAP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJNOMAP" ) */ );
      }

      wvarStep = 44;
      //Armo el Request para integrar
      mvarRequest = "<Request><DEFINICION>2210_IntegracionGral.xml</DEFINICION>";
      mvarRequest = mvarRequest + "<INTERSEC>NOBK</INTERSEC>";
      mvarRequest = mvarRequest + "<USUARCOD>" + vUSUARCOD + "</USUARCOD>";
      mvarRequest = mvarRequest + "<SECUECOD>" + Funciones.convertirFecha( new Variant( DateTime.now() )/*warning: ByRef value change will be lost.*/ ) + "</SECUECOD>";
      mvarRequest = mvarRequest + "<SOLICITU>" + vSOLICITU + "</SOLICITU>";
      mvarRequest = mvarRequest + "<FESOLANN>" + DateTime.year( DateTime.now() ) + "</FESOLANN>";
      mvarRequest = mvarRequest + "<FESOLMES>" + Strings.right( ("0" + DateTime.month( DateTime.now() )), 2 ) + "</FESOLMES>";
      mvarRequest = mvarRequest + "<FESOLDIA>" + Strings.right( ("0" + DateTime.day( DateTime.now() )), 2 ) + "</FESOLDIA>";
      mvarRequest = mvarRequest + "<EFECTANN>" + DateTime.year( DateTime.now() ) + "</EFECTANN>";
      mvarRequest = mvarRequest + "<EFECTMES>" + Strings.right( ("0" + DateTime.month( DateTime.now() )), 2 ) + "</EFECTMES>";
      mvarRequest = mvarRequest + "<EFECTDIA>01</EFECTDIA>";
      mvarRequest = mvarRequest + "<VENCIANN>" + DateTime.year( DateTime.dateSerial( DateTime.year( DateTime.now() ), (DateTime.month( DateTime.now() ) + 1), 1 ) ) + "</VENCIANN>";
      mvarRequest = mvarRequest + "<VENCIMES>" + Strings.right( ("0" + DateTime.month( DateTime.dateSerial( DateTime.year( DateTime.now() ), DateTime.month( DateTime.now() ) + 1, 1 ) )), 2 ) + "</VENCIMES>";
      mvarRequest = mvarRequest + "<VENCIDIA>01</VENCIDIA>";
      mvarRequest = mvarRequest + "<AGENTCLAPR>PR</AGENTCLAPR>";
      mvarRequest = mvarRequest + "<AGENTCODPR></AGENTCODPR>";
      mvarRequest = mvarRequest + "<AGENTCLAPR2/>";
      mvarRequest = mvarRequest + "<AGENTCODPR2>0</AGENTCODPR2>";
      mvarRequest = mvarRequest + "<AGENTNOMPR2/>";
      mvarRequest = mvarRequest + " <AGENTCLAOR>OR</AGENTCLAOR>";
      mvarRequest = mvarRequest + "<AGENTCODOR></AGENTCODOR>";
      mvarRequest = mvarRequest + "<COMISIONPR1/>";
      mvarRequest = mvarRequest + "<COMISIONPR2>0</COMISIONPR2>";
      mvarRequest = mvarRequest + "<COMISIONOR/>";
      mvarRequest = mvarRequest + "<PRIMA>0</PRIMA>";
      mvarRequest = mvarRequest + "<RECARGOADM>0</RECARGOADM>";
      mvarRequest = mvarRequest + "<RECARGOFIN>0</RECARGOFIN>";
      mvarRequest = mvarRequest + "<DERPOIMP>0</DERPOIMP>";
      mvarRequest = mvarRequest + "<PLANPCOD>0</PLANPCOD>";
      mvarRequest = mvarRequest + "<COBROCOD>" + vCOBROCOD + "</COBROCOD>";
      mvarRequest = mvarRequest + "<COBROTIP>" + vCOBROTIP + "</COBROTIP>";
      mvarRequest = mvarRequest + "<CUENNUME>" + vCUENNUME + "</CUENNUME>";
      mvarRequest = mvarRequest + "<VENCIANNT></VENCIANNT>";
      mvarRequest = mvarRequest + "<VENCIMEST></VENCIMEST>";
      mvarRequest = mvarRequest + "<MONEDA>1</MONEDA>";
      mvarRequest = mvarRequest + "<ACTIVPPAL/>";
      mvarRequest = mvarRequest + "<TIPOOPER>AP</TIPOOPER>";
      mvarRequest = mvarRequest + "<MARCOPER/>";
      mvarRequest = mvarRequest + "<CIAASCODANT/>";
      mvarRequest = mvarRequest + "<RAMOPCODANT>ATD1</RAMOPCODANT>";
      mvarRequest = mvarRequest + "<POLIZANNANT>" + vPOLIZANN + "</POLIZANNANT>";
      mvarRequest = mvarRequest + "<POLIZSECANT>" + vPOLIZSEC + "</POLIZSECANT>";
      mvarRequest = mvarRequest + "<CAUSAEND/>";
      mvarRequest = mvarRequest + "<ORIOPERA>OV</ORIOPERA>";
      mvarRequest = mvarRequest + "<CLIENTES>";
      mvarRequest = mvarRequest + "<CLIENTE>";
      mvarRequest = mvarRequest + "<TIPOCLIE>P</TIPOCLIE>";
      mvarRequest = mvarRequest + "<CLIENTIP>00</CLIENTIP>";
      mvarRequest = mvarRequest + "<CLIENNOM><![CDATA[" + vCLIENNOM + "]]></CLIENNOM>";
      mvarRequest = mvarRequest + "<CLIENAP1><![CDATA[" + vCLIENAP1 + "]]></CLIENAP1>";
      mvarRequest = mvarRequest + "<CLIENAP2><![CDATA[" + vCLIENAP2 + "]]></CLIENAP2>";
      mvarRequest = mvarRequest + "<CLIENSEX>" + vSEXO + "</CLIENSEX>";
      mvarRequest = mvarRequest + "<NACIMANN>" + vNACIMANN + "</NACIMANN>";
      mvarRequest = mvarRequest + "<NACIMMES>" + vNACIMMES + "</NACIMMES>";
      mvarRequest = mvarRequest + "<NACIMDIA>" + vNACIMDIA + "</NACIMDIA>";
      mvarRequest = mvarRequest + "<PAISSCODNAC>00</PAISSCODNAC>";
      mvarRequest = mvarRequest + "<CLIENEST>" + vESTADO + "</CLIENEST>";
      mvarRequest = mvarRequest + "<DOCUMTIP>" + vTIPODOCU + "</DOCUMTIP>";
      mvarRequest = mvarRequest + "<DOCUMDAT>" + vNUMEDOCU + "</DOCUMDAT>";
      mvarRequest = mvarRequest + "<CLIENIVA>" + vCLIENIVA + "</CLIENIVA>";
      mvarRequest = mvarRequest + "<CUITL>0</CUITL>";
      mvarRequest = mvarRequest + "<CLIEIBTP/>";
      mvarRequest = mvarRequest + "<CLIEIBNU>0</CLIEIBNU>";
      mvarRequest = mvarRequest + "<DOMICDOM>" + vDOMICDOM + "</DOMICDOM>";
      mvarRequest = mvarRequest + "<DOMICDNU>" + vDOMICDNU + "</DOMICDNU>";
      mvarRequest = mvarRequest + "<DOMICPIS>" + vDOMICPIS + "</DOMICPIS>";
      mvarRequest = mvarRequest + "<DOMICPTA>" + vDOMICPTA + "</DOMICPTA>";
      mvarRequest = mvarRequest + "<DOMICCPO>" + vLOCALIDAD + "</DOMICCPO>";
      mvarRequest = mvarRequest + "<DOMICPOB>" + vDOMICPOB + "</DOMICPOB>";
      mvarRequest = mvarRequest + "<PROVICOD>" + vPROVCOD + "</PROVICOD>";
      mvarRequest = mvarRequest + "<PAISSCODDOM>00</PAISSCODDOM>";
      mvarRequest = mvarRequest + "<TELPPAL>" + vTELNRO + "</TELPPAL>";
      mvarRequest = mvarRequest + "<TELTRAB/>";
      mvarRequest = mvarRequest + "<TELCELU/>";
      mvarRequest = mvarRequest + "<TELOTRO/>";
      mvarRequest = mvarRequest + "<EMAIL>" + vEMAIL + "</EMAIL>";
      mvarRequest = mvarRequest + "<DOMICCPOBNACAML/>";
      mvarRequest = mvarRequest + "<CATEGCLIEAML>0</CATEGCLIEAML>";
      mvarRequest = mvarRequest + "<CLIENSEC>0</CLIENSEC>";
      mvarRequest = mvarRequest + "</CLIENTE>";
      mvarRequest = mvarRequest + "</CLIENTES>";
      mvarRequest = mvarRequest + "<ANOTACIONES>";
      mvarRequest = mvarRequest + "<ANOTACION>";
      mvarRequest = mvarRequest + "<TIPOANOTA/>";
      mvarRequest = mvarRequest + "<ORDENTEXT>0</ORDENTEXT>";
      mvarRequest = mvarRequest + "<TEXTO/>";
      mvarRequest = mvarRequest + "</ANOTACION>";
      mvarRequest = mvarRequest + "</ANOTACIONES>";
      mvarRequest = mvarRequest + "</Request>";
      wvarStep = 45;
      //Armo el Request1 para la SolicitudMQ
      mvarRequest1 = "<Request><DEFINICION>2219_IntegracionATM.xml</DEFINICION>";
      mvarRequest1 = mvarRequest1 + "<INTERSEC>NOBK</INTERSEC>";
      mvarRequest1 = mvarRequest1 + "<USUARCOD>" + vUSUARCOD + "</USUARCOD>";
      mvarRequest1 = mvarRequest1 + "<SECUECOD>" + Funciones.convertirFecha( new Variant( DateTime.now() )/*warning: ByRef value change will be lost.*/ ) + "</SECUECOD>";
      mvarRequest1 = mvarRequest1 + "<SOLICITU>" + vSOLICITU + "</SOLICITU>";
      mvarRequest1 = mvarRequest1 + "<FINPOLIZA>S</FINPOLIZA>";
      mvarRequest1 = mvarRequest1 + "<RAMO>ATD1</RAMO>";
      mvarRequest1 = mvarRequest1 + "<ORIOPERA>OV</ORIOPERA>";
      mvarRequest1 = mvarRequest1 + "<ITEM>1</ITEM>";
      mvarRequest1 = mvarRequest1 + "<RIESGTIP>27</RIESGTIP>";
      mvarRequest1 = mvarRequest1 + "<TIPOMOVI>I</TIPOMOVI>";
      mvarRequest1 = mvarRequest1 + "<CERTIPOLANT>0</CERTIPOLANT>";
      mvarRequest1 = mvarRequest1 + "<CERTIANNANT>0</CERTIANNANT>";
      mvarRequest1 = mvarRequest1 + "<CERTISECANT>0</CERTISECANT>";
      mvarRequest1 = mvarRequest1 + "<CLAUSAJUST>0</CLAUSAJUST>";
      mvarRequest1 = mvarRequest1 + "<TNROTARJ>" + vBANELCO + "</TNROTARJ>";
      mvarRequest1 = mvarRequest1 + "<TPLANATM>" + vTPLANATM + "</TPLANATM>";
      mvarRequest1 = mvarRequest1 + "<RDOMICDOM>" + vDOMICDOMCOR + "</RDOMICDOM>";
      mvarRequest1 = mvarRequest1 + "<RDOMICDNU>" + vDOMICDNUCOR + "</RDOMICDNU>";
      mvarRequest1 = mvarRequest1 + "<RDOMICPIS>" + vDOMICPISCOR + "</RDOMICPIS>";
      mvarRequest1 = mvarRequest1 + "<RDOMICPTA>" + vDOMICPTACOR + "</RDOMICPTA>";
      mvarRequest1 = mvarRequest1 + "<RDOMICCPO>" + vLOCALIDADCODCOR + "</RDOMICCPO>";
      mvarRequest1 = mvarRequest1 + "<RDOMICPOB>" + vDOMICPOBCOR + "</RDOMICPOB>";
      mvarRequest1 = mvarRequest1 + "<RPROVICOD>" + vPROVICOR + "</RPROVICOD>";
      mvarRequest1 = mvarRequest1 + "<RPAISSCOD>00</RPAISSCOD>";
      mvarRequest1 = mvarRequest1 + "<BANCOCOD>" + vCANAL + "</BANCOCOD>";
      mvarRequest1 = mvarRequest1 + "<SUCURCOD>" + vCANAL_SUCURSAL + "</SUCURCOD>";
      mvarRequest1 = mvarRequest1 + "<LEGAJAPERT>0000009991</LEGAJAPERT>";
      mvarRequest1 = mvarRequest1 + "<LEGAJCIERRE>0000009992</LEGAJCIERRE>";
      mvarRequest1 = mvarRequest1 + "<BANCOCODEM>" + vCANAL + "</BANCOCODEM>";
      mvarRequest1 = mvarRequest1 + "<LEGAJCIA>" + vLEGAJCIA + "</LEGAJCIA>";
      mvarRequest1 = mvarRequest1 + "<LEGAJNUMAP>" + vLEGAJO_VEND + "</LEGAJNUMAP>";
      mvarRequest1 = mvarRequest1 + "<COMISIONAP>S</COMISIONAP>";
      mvarRequest1 = mvarRequest1 + "<LEGAJAPEAP>" + vLEGAJAPEAP + "</LEGAJAPEAP>";
      mvarRequest1 = mvarRequest1 + "<LEGAJNOMAP>" + vLEGAJNOMAP + "</LEGAJNOMAP>";
      mvarRequest1 = mvarRequest1 + "<LEGAJNUMCR>" + vLEGAJO_VEND + "</LEGAJNUMCR>";
      mvarRequest1 = mvarRequest1 + "<LEGAJAPECR>" + vLEGAJAPEAP + "</LEGAJAPECR>";
      mvarRequest1 = mvarRequest1 + "<LEGAJNOMCR>" + vLEGAJNOMAP + "</LEGAJNOMCR>";
      mvarRequest1 = mvarRequest1 + "<COMISIONCR>S</COMISIONCR>";
      mvarRequest1 = mvarRequest1 + "<ANOTACIONES></ANOTACIONES>";

      wvarStep = 46;
      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( new Variant( mvarRequest ), new Variant( mvarResponse ), "" );
      wobjClass = (lbawA_OVMQGen.lbaw_MQMensaje) null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );
      wvarStep = 47;
      mvarProdok = "ER";
      if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {
          // Obtengo el vNROSOLTMP
          wvarStep = 48;
          vNROSOLTMP = diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//NROSOLTMP" ) */ );
          //Agrego vNROSOLTMP al request1 de la SolicitudMQ
          wvarStep = 49;
          mvarRequest1 = mvarRequest1 + "<NROSOLTMP>" + vNROSOLTMP + "</NROSOLTMP></Request>";
          wvarStep = 50;
          //Llamo al mensaje de SolicitudMQ
          wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
          wobjClass.Execute( mvarRequest1, mvarResponse, "" );
          wobjClass = (lbawA_OVMQGen.lbaw_MQMensaje) null;
          mobjXMLDoc = new diamondedge.util.XmlDom();
          //unsup mobjXMLDoc.async = false;
          mobjXMLDoc.loadXML( mvarResponse );
          wvarStep = 51;
          if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
          {
            if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
            {
              //             If (mobjXMLDoc.selectSingleNode("//MENSAJE").Text) <> "" Then
              //                wvarCodErr = -111
              //                pvarRes = "<Response><Estado resultado=""false"" mensaje=" & mobjXMLDoc.selectSingleNode("//MENSAJE").Text & "/></Response>"
              //                mvarProdok = "ER"
              //                Exit Function
              //             Else
              pvarRes.set( mvarResponse );
              mvarProdok = "OK";
              //              End If
            }
          }
        }
      }
      wvarStep = 53;
      if( mvarProdok.equals( "ER" ) )
      {
        wvarCodErr.set( -53 );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=Poliza  no habilitada /></Response>" );
        return fncIntegrarSolicitudMQ;
      }


      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLList = (org.w3c.dom.NodeList) null;

      wvarStep = 54;
      if( wvarCodErr.toInt() == 0 )
      {
        fncIntegrarSolicitudMQ = true;
      }
      else
      {
        fncIntegrarSolicitudMQ = false;
      }

      fin: 
      return fncIntegrarSolicitudMQ;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncIntegrarSolicitudMQ = false;

        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncIntegrarSolicitudMQ;
  }

  public String fechaVige( Variant pvarStrFecha ) throws Exception
  {
    String fechaVige = "";
    String wvarIntAnio = "";
    String wvarIntMes = "";
    String wvarIntDia = "";
    String[] warrSplit = null;


    pvarStrFecha.set( Strings.format( pvarStrFecha.toString(), "General Date" ) );

    wvarIntAnio = String.valueOf( DateTime.year( pvarStrFecha.toDate() ) );
    wvarIntMes = Strings.right( "0" + DateTime.month( pvarStrFecha.toDate() ), 2 );
    wvarIntDia = Strings.right( "0" + DateTime.day( pvarStrFecha.toDate() ), 2 );

    fechaVige = wvarIntDia + "/" + wvarIntMes + "/" + wvarIntAnio;

    return fechaVige;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
