package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Implementacion de los objetos
 * Objetos del FrameWork
 */

public class GetProductos implements Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetProductos";
  static final String mcteStoreProc = "SPSNCV_BRO_ALTA_OPER";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_TipoDocu = "TIPODOCU";
  static final String mcteParam_NumeDocu = "NUMEDOCU";
  static final String mcteParam_EstadoProc = "ESTADOPROC";
  static final String mcteParam_TiempoProceso = "TIEMPOPROCESO";
  static final String mcteParam_PedidoAno = "MSGANO";
  static final String mcteParam_PedidoMes = "MSGMES";
  static final String mcteParam_PedidoDia = "MSGDIA";
  static final String mcteParam_PedidoHora = "MSGHORA";
  static final String mcteParam_PedidoMinuto = "MSGMINUTO";
  static final String mcteParam_PedidoSegundo = "MSGSEGUNDO";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncInsertNode
   * static variable for method: fncPutSolHO
   * static variable for method: fncPutSolicitud
   */
  private final String wcteFnName = "fncPutSolicitud";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant wvarCodErr = new Variant();
    Variant pvarRes = new Variant();
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      pvarRequest = Strings.mid( pvarRequest, 1, Strings.find( 1, pvarRequest, ">" ) ) + "<MSGANO>" + DateTime.year( DateTime.now() ) + "</MSGANO>" + "<MSGMES>" + DateTime.month( DateTime.now() ) + "</MSGMES>" + "<MSGDIA>" + DateTime.day( DateTime.now() ) + "</MSGDIA>" + "<MSGHORA>" + DateTime.hour( DateTime.now() ) + "</MSGHORA>" + "<MSGMINUTO>" + DateTime.minute( DateTime.now() ) + "</MSGMINUTO>" + "<MSGSEGUNDO>" + DateTime.second( DateTime.now() ) + "</MSGSEGUNDO>" + Strings.mid( pvarRequest, (Strings.find( 1, pvarRequest, ">" ) + 1) );
      wvarStep = 10;
      wvarMensaje.set( "" );
      wvarCodErr.set( "" );
      pvarRes.set( "" );
      //
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*unsup mobjCOM_Context.SetAbort() */;
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*unsup mobjCOM_Context.SetComplete() */;
        }
        return IAction_Execute;
      }
      pvarResponse.set( pvarRes );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLReturnVal = null;
    diamondedge.util.XmlDom wobjXMLError = null;
    diamondedge.util.XmlDom wobjXMLRequestExists = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    String wvarMensajeStoreProc = "";
    String wvarMensajePutTran = "";
    String mvar_Estado = "";
    String mvarCertiSec = "";
    com.qbe.services.HSBCInterfaces.IDBConnection wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    String pTiempoAIS = "";

    //
    //
    //
    try 
    {

      //Instancia la clase de validacion
      wvarMensaje.set( "" );
      wvarMensajeStoreProc = "";
      //
      //  wvarStep = 20
      //  Set wobjClass = mobjCOM_Context.CreateInstance("LBA_InterWSBrok.GetValidacionVehiculo")
      //  Call wobjClass.Execute(pvarRequest, wvarMensajeStoreProc, "")
      //  Set wobjClass = Nothing
      //  '
      //'Analizo la respuesta del validador
      //  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
      //  wobjXMLRequest.async = False
      // Call wobjXMLRequest.loadXML(wvarMensajeStoreProc)
      // '
      //'Si la respuesta viene con error
      //' wvarStep = 30
      // If Not wobjXMLRequest.selectSingleNode("//LBA_WS/@res_code").Text = "0" Then
      //   wvarCodErr = wobjXMLRequest.selectSingleNode("//LBA_WS/@res_code").Text
      //   wvarMensaje = wobjXMLRequest.selectSingleNode("//LBA_WS/@res_msg").Text
      //
      //            mvar_Estado = "ER"
      //            pvarRes = wvarMensajeStoreProc
      //            wvarMensajeStoreProc = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """>" & _
      // '                                    pvarRequest & _
      //                                    "</LBA_WS>"
      //  ' si no viene con error de la BD
      //  Else
      //Guarda la Solicitud en el SP
      wvarStep = 60;

      //If fncPutSolicitud(wvarMensajeStoreProc, wvarMensaje, wvarCodErr, pvarRes, pTiempoAIS) Then
      if( invoke( "fncPutSolicitud", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes), new Variant(pTiempoAIS) } ) )
      {

        wobjXMLReturnVal = new diamondedge.util.XmlDom();
        //unsup wobjXMLReturnVal.async = false;
        wobjXMLReturnVal.loadXML( pvarRes.toString() );
        //
        if( ! (null /*unsup wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
        {
          if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" )) )
          {

            //If Not wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text = "0" Then
            //hubo un error
            wvarStep = 65;
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) );
            wvarMensajeStoreProc = "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>";
            mvar_Estado = "ER";

            //Este es el caso en que no haya contestado el SP o sea un error no identificado
          }
          else
          {
            wvarStep = 70;
            mvar_Estado = "OK";
            // wvarMensajeStoreProc = wobjXMLRequest.xml
            wvarMensajeStoreProc = pvarRes.toString();
          }
        }
        //
        wobjXMLReturnVal = (diamondedge.util.XmlDom) null;

      }


      //
      // End If
      //Call fncInsertNode(wvarMensajeStoreProc, "//LBA_WS/Request", mvar_Estado, "ESTADOPROC")
      // Agrego el tiempo de impresion
      //Call fncInsertNode(wvarMensajeStoreProc, "//LBA_WS/Request", pTiempoAIS, "TIEMPOPROCESO")
      //Alta de la OPERACION
      wvarStep = 75;
      //  If fncPutSolHO(wvarMensajeStoreProc, wvarMensaje, wvarCodErr, wvarMensajePutTran) Then
      wobjXMLReturnVal = new diamondedge.util.XmlDom();
      //unsup wobjXMLReturnVal.async = false;
      //     Call wobjXMLReturnVal.loadXML(wvarMensajePutTran)
      wobjXMLReturnVal.loadXML( wvarMensajeStoreProc );
      //
      //       wvarStep = 80
      //      If Not wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text = "0" Then
      //          wvarCodErr = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text
      //         wvarMensaje = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_msg").Text
      //         pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
      //         fncGetAll = False
      //         Exit Function
      //     End If
      //
      fncGetAll = true;
      //
      // End If
      fin: 
      wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;
        /*unsup mobjCOM_Context.SetComplete() */;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }

  private boolean fncInsertNode( Variant pvarRequest, Variant pvarHeader, Variant pvarNodeValue, Variant pvarNodeName )
  {
    boolean fncInsertNode = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLRequest = null;


    try 
    {

      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest.toString() );
      //
      wvarStep = 20;
      /*unsup wobjXMLRequest.selectSingleNode( pvarHeader.toString() ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), pvarNodeName.toString(), "" ) */ );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + pvarNodeName ) */, pvarNodeValue.toString() );
      //
      pvarRequest.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      fncInsertNode = true;
      return fncInsertNode;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncInsertNode = false;
        /*unsup mobjCOM_Context.SetComplete() */;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncInsertNode;
  }

  private boolean fncPutSolHO( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutSolHO = false;
    int wvarStep = 0;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_NROCOT = "";
    String mvarWDB_TIPOOPERAC = "";
    String mvarWDB_ESTADO = "";
    String mvarWDB_RECEPCION_PEDIDOANO = "";
    String mvarWDB_RECEPCION_PEDIDOMES = "";
    String mvarWDB_RECEPCION_PEDIDODIA = "";
    String mvarWDB_RECEPCION_PEDIDOHORA = "";
    String mvarWDB_RECEPCION_PEDIDOMINUTO = "";
    String mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_TIEMPOPROCESO = "";
    String mvarWDB_XML = "";

    //
    //
    //
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_CODINST = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }
      //
      wvarStep = 30;
      mvarWDB_REQUESTID = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID ) */ );
      }
      //
      wvarStep = 60;
      mvarWDB_ESTADO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EstadoProc) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_ESTADO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_EstadoProc ) */ );
      }
      //
      wvarStep = 70;
      mvarWDB_RECEPCION_PEDIDOANO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoAno) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoAno ) */ );
      }
      //
      wvarStep = 80;
      mvarWDB_RECEPCION_PEDIDOMES = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMes) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMes ) */ );
      }
      //
      wvarStep = 90;
      mvarWDB_RECEPCION_PEDIDODIA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoDia) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDODIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoDia ) */ );
      }
      //
      wvarStep = 100;
      mvarWDB_RECEPCION_PEDIDOHORA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoHora) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOHORA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoHora ) */ );
      }
      //
      wvarStep = 110;
      mvarWDB_RECEPCION_PEDIDOMINUTO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMinuto) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMINUTO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMinuto ) */ );
      }
      //
      wvarStep = 120;
      mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoSegundo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOSEGUNDO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoSegundo ) */ );
      }
      //
      wvarStep = 170;
      mvarWDB_TIEMPOPROCESO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TiempoProceso) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIEMPOPROCESO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TiempoProceso ) */ );
      }



      wvarStep = 180;
      mvarWDB_XML = "";
      mvarWDB_XML = pvarRequest;
      //
      wvarStep = 190;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      //
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_NRO_COTIZACION_LBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NROCOT.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NROCOT)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 18 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 230;
      wobjDBParm = new Parameter( "@WDB_TIPO_OPERACION", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_TIPOOPERAC.equals( "" ) ? "P" : mvarWDB_TIPOOPERAC) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 240;
      wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_ESTADO.equals( "" ) ? "ER" : mvarWDB_ESTADO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 250;
      wobjDBParm = new Parameter( "@WDB_RECEPCION_PEDIDO", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant( mvarWDB_RECEPCION_PEDIDOANO + "-" + mvarWDB_RECEPCION_PEDIDOMES + "-" + mvarWDB_RECEPCION_PEDIDODIA + " " + mvarWDB_RECEPCION_PEDIDOHORA + ":" + mvarWDB_RECEPCION_PEDIDOMINUTO + ":" + mvarWDB_RECEPCION_PEDIDOSEGUNDO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 260;
      wobjDBParm = new Parameter( "@WDB_ENVIO_RESPUESTA", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant( DateTime.year( DateTime.now() ) + "-" + DateTime.month( DateTime.now() ) + "-" + DateTime.day( DateTime.now() ) + " " + DateTime.hour( DateTime.now() ) + ":" + DateTime.minute( DateTime.now() ) + ":" + DateTime.second( DateTime.now() ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 270;
      wobjDBParm = new Parameter( "@WDB_RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_RAMOPCOD.equals( "" ) ? "VHI1" : mvarWDB_RAMOPCOD) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 280;
      wobjDBParm = new Parameter( "@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 290;
      wobjDBParm = new Parameter( "@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 300;
      wobjDBParm = new Parameter( "@WDB_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 310;
      wobjDBParm = new Parameter( "@WDB_CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 320;
      wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CERTISEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CERTISEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 330;
      wobjDBParm = new Parameter( "@WDB_TIEMPO_PROCESO_AIS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIEMPOPROCESO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIEMPOPROCESO)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 340;
      wobjDBParm = new Parameter( "@WDB_OPERACION_XML", AdoConst.adVarChar, AdoConst.adParamInput, 8000, new Variant( (mvarWDB_XML.equals( "" ) ? "" : Strings.left( mvarWDB_XML, 8000 )) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 350;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      wobjDBCnn.close();

      fncPutSolHO = true;
      pvarRes.set( "<LBA_WS res_code=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" ) */ ) + "\" res_msg=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) + "\"></LBA_WS>" );
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
      return fncPutSolHO;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        fncPutSolHO = false;
        //mobjCOM_Context.SetComplete
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutSolHO;
  }

  private boolean fncPutSolicitud( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes, String pTiempoAIS )
  {
    boolean fncPutSolicitud = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Object wobjClass = null;
    com.qbe.services.HSBCInterfaces.IDBConnection wobjHSBC_DBCnn = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom mobjXMLDoc = null;
    org.w3c.dom.NodeList wobjXMLNodeList = null;
    int wvarcounter = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_TIPODOCU = "";
    String mvarWDB_NUMEDOCU = "";
    String mvarWDB_EFECTANN = "";
    String mvarWDB_USUARCOD = "";
    String mvarWDB_MENSAJE = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarOk = "";
    String mvar_TiempoProceso = "";
    String mvarInicioAIS = "";
    String mvarFinAIS = "";
    String mvarTiempoAIS = "";

    //
    // variables para calcular tiempo proceso de la impresión
    try 
    {


      //Recupero datos del request
      wvarStep = 100;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 160;
      mvarWDB_TIPODOCU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoDocu) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPODOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoDocu ) */ );
      }
      //
      wvarStep = 170;
      mvarWDB_NUMEDOCU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NumeDocu) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NUMEDOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NumeDocu ) */ );
      }
      //
      //  wvarStep = 420
      //  mvarWDB_USUARCOD = ""
      //  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_USUARCOD) Is Nothing Then
      //     mvarWDB_USUARCOD = wobjXMLRequest.selectSingleNode("//" & mcteParam_USUARCOD).Text
      //  End If
      //conforma xml para el request
      wvarStep = 610;
      mvarRequest = "<Request>" + "<DEFINICION>1189_ProductosPorCuit.xml</DEFINICION>" + "<AplicarXSL>1189_ProductosPorCuit.xsl</AplicarXSL>" + "<TIPODOCU>" + mvarWDB_TIPODOCU + "</TIPODOCU>" + "<NUMEDOCU>" + mvarWDB_NUMEDOCU + "</NUMEDOCU>" + "</Request>";

      wvarStep = 620;


      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( new Variant( mvarRequest ), new Variant( mvarResponse ), "" );
      wobjClass = null;

      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );
      //Call mobjXMLDoc.Load(App.Path & "\ResponseProductos.xml")
      mvarOk = "ER";
      if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {
          mvarOk = "OK";
        }
        else
        {
          mvarOk = "ER";
        }
      }
      else
      {
        mvarOk = "ER";
      }

      if( mvarOk.equals( "ER" ) )
      {
        wvarCodErr.set( -101 );
        wvarMensaje.set( "No se pudieron recuperar los Productos " );
        pvarRes.set( "<LBA_WS res_code=\"-54\" res_msg=\"Error\"><Response><Estado resultado=\"false\" mensaje=" + wvarMensaje + " /></Response></LBA_WS>" );
        return fncPutSolicitud;
      }
      else
      {
        fncPutSolicitud = true;
        //pvarRes = mvarResponse
        pvarRes.set( mobjXMLDoc.getDocument().getDocumentElement().toString() );
      }

      fin: 
      //libero los objectos
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjClass = null;
      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      return fncPutSolicitud;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"-54\" res_msg=\"Error\"><Response><Estado resultado=\"false\" mensaje=" + wvarMensaje + " /></Response></LBA_WS>" );

        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncPutSolicitud = false;
        //mobjCOM_Context.SetComplete
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutSolicitud;
  }

  public String fechaVige( Variant pvarStrFecha ) throws Exception
  {
    String fechaVige = "";
    String wvarIntAnio = "";
    String wvarIntMes = "";
    String wvarIntDia = "";
    String[] warrSplit = null;


    pvarStrFecha.set( Strings.format( pvarStrFecha.toString(), "General Date" ) );

    wvarIntAnio = String.valueOf( DateTime.year( pvarStrFecha.toDate() ) );
    wvarIntMes = Strings.right( "0" + DateTime.month( pvarStrFecha.toDate() ), 2 );
    wvarIntDia = Strings.right( "0" + DateTime.day( pvarStrFecha.toDate() ), 2 );

    fechaVige = wvarIntDia + "/" + wvarIntMes + "/" + wvarIntAnio;

    return fechaVige;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
