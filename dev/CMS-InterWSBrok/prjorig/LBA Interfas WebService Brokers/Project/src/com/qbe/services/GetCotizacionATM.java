package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Implementacion de los objetos
 * Objetos del FrameWork
 */

public class GetCotizacionATM implements Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetCotizacionATM";
  static final String mcteStoreProc = "SPSNCV_BRO_ALTA_OPER";
  /**
   * Archivo de Configuracion
   */
  static final String mcteArchivoConfATM_XML = "LBA_PARAM_ATM.XML";
  static final String mcteArchivoATM_XML = "LBA_VALIDACION_COT_ATM.XML";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_Provi = "PROVCOD";
  static final String mcteParam_LocalidadCod = "LOCALIDAD";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  static final String mcteParam_COBROCOD = "FPAGO";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  /**
   * Parámetros agregados
   */
  static final String mcteParam_NROCOT = "COT_NRO";
  static final String mcteParam_TipoOperac = "TIPOOPERAC";
  static final String mcteParam_EstadoProc = "ESTADOPROC";
  static final String mcteParam_PedidoAno = "MSGANO";
  static final String mcteParam_PedidoMes = "MSGMES";
  static final String mcteParam_PedidoDia = "MSGDIA";
  static final String mcteParam_PedidoHora = "MSGHORA";
  static final String mcteParam_PedidoMinuto = "MSGMINUTO";
  static final String mcteParam_PedidoSegundo = "MSGSEGUNDO";
  static final String mcteParam_TiempoProceso = "TIEMPOPROCESO";
  static final String mcteParam_CERTISEC = "CERTISEC";
  static final String mcteParam_RAMOPCOD = "RAMOPCOD";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncInsertNode
   * static variable for method: fncPutCotHO
   * static variable for method: fncCotizarAtmMQ
   */
  private final String wcteFnName = "fncCotizarAtmMQ";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant wvarCodErr = new Variant();
    Variant pvarRes = new Variant();
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      pvarRequest = Strings.mid( pvarRequest, 1, Strings.find( 1, pvarRequest, ">" ) ) + "<MSGANO>" + DateTime.year( DateTime.now() ) + "</MSGANO>" + "<MSGMES>" + DateTime.month( DateTime.now() ) + "</MSGMES>" + "<MSGDIA>" + DateTime.day( DateTime.now() ) + "</MSGDIA>" + "<MSGHORA>" + DateTime.hour( DateTime.now() ) + "</MSGHORA>" + "<MSGMINUTO>" + DateTime.minute( DateTime.now() ) + "</MSGMINUTO>" + "<MSGSEGUNDO>" + DateTime.second( DateTime.now() ) + "</MSGSEGUNDO>" + Strings.mid( pvarRequest, (Strings.find( 1, pvarRequest, ">" ) + 1) );
      wvarStep = 10;
      pvarRes.set( "" );
      wvarMensaje.set( "" );
      wvarCodErr.set( "" );
      //
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(new Variant( pvarRequest )), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*unsup mobjCOM_Context.SetAbort() */;
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*unsup mobjCOM_Context.SetComplete() */;
        }
        return IAction_Execute;
      }
      //
      pvarResponse.set( pvarRes );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( Variant pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLParams = null;
    diamondedge.util.XmlDom wobjXMLReturnVal = null;
    diamondedge.util.XmlDom wobjXMLError = null;
    org.w3c.dom.Node wobjXMLChildNode = null;
    Object wobjClass = null;
    Variant wvarMensajeStoreProc = new Variant();
    Variant wvarMensajePutTran = new Variant();
    String mvar_Estado = "";
    String mvar_precio = "";
    String mvar_CodZona = "";
    String mvar_TiempoProceso = "";
    String mvarInicioAIS = "";
    String mvarFinAIS = "";
    String mvarTiempoAIS = "";

    //
    //
    // variables para calcular tiempo proceso AIS
    try 
    {

      //Le agrega los Nodos de Valor Fijo
      wvarStep = 10;
      wobjXMLParams = new diamondedge.util.XmlDom();
      //unsup wobjXMLParams.async = false;
      wobjXMLParams.load( System.getProperty("user.dir") + "\\" + mcteArchivoConfATM_XML );
      //
      wvarStep = 15;
      invoke( "fncInsertNode", new Variant[] { new Variant(pvarRequest), new Variant("//Request"), new Variant(diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( new Variant("//BROKERS/ATM/COTIZACION") ) */ )), new Variant("TIPOOPERAC") } );
      invoke( "fncInsertNode", new Variant[] { new Variant(pvarRequest), new Variant("//Request"), new Variant(diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( new Variant("//BROKERS/ATM/RAMOPCOD") ) */ )), new Variant("RAMOPCOD") } );
      //
      //Instancia la clase de validacion
      wvarMensaje.set( "" );
      wvarMensajeStoreProc.set( "" );
      //
      wvarStep = 20;
      wobjClass = new LBA_InterWSBrok.GetValidacionCotATM();
      wobjClass.Execute( pvarRequest, wvarMensajeStoreProc, "" );
      wobjClass = null;
      //
      //Analizo la respuesta del validador
      wvarStep = 25;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( wvarMensajeStoreProc.toString() );
      //
      //Si la respuesta viene con error
      wvarStep = 30;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" ) */ ).equals( "0" )) )
      {
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) );
        if( wvarCodErr.toString().equals( "-2" ) )
        {
          mvar_Estado = "NI";
        }
        else
        {
          mvar_Estado = "ER";
        }
        //En el caso de que no llegue a cotizar, tomo el XML original para el ALTA OPER
        wvarStep = 35;
        pvarRes.set( wvarMensajeStoreProc );
        wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
      }
      else
      {

        //En el caso de que la Validacion esté Ok, obtengo la Cotizacion
        //De la validación vienen los datos de NROCOTI , CERTISEC = 0 , PLANDEFEC/TPLANATM
        wvarStep = 40;

        mvarInicioAIS = DateTime.format( DateTime.now() );
        //Cotizo . Llamo al msg genérico 2200
        if( invoke( "fncCotizarAtmMQ", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje.toString()), new Variant(wvarCodErr), new Variant(pvarRes) } ) )
        {
          mvarFinAIS = DateTime.format( DateTime.now() );
          // devuelvo pvarRes con precio y prima
          wvarStep = 45;
          wobjXMLReturnVal = new diamondedge.util.XmlDom();
          //unsup wobjXMLReturnVal.async = false;
          wobjXMLReturnVal.loadXML( pvarRes.toString() );
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
          {
            mvar_Estado = "ER";
          }
          else
          {
            mvar_Estado = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@resultado" ) */ );
          }
          wvarStep = 50;
          if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" )) )
          {
            //hubo un error
            pvarRes.set( "<LBA_WS res_code=\"-300\" res_msg=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@mensaje" ) */ ) + "\"></LBA_WS>" );
          }
          else
          {

            // Devuelvo el mensaje de salida
            pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + pvarRes + "</LBA_WS>" );

            //Tomo el valor del Precio
            wvarStep = 90;
            //EG 10/2010 No se toma en cuenta  la zona
            //  mvar_precio = wobjXMLReturnVal.selectSingleNode("//RECTOIMP").Text
            mvar_CodZona = "0";

            mvar_TiempoProceso = String.valueOf( DateTime.diff( "S", DateTime.toDate( mvarInicioAIS ), DateTime.toDate( mvarFinAIS ) ) );

            wvarStep = 96;
            wobjXMLReturnVal = (diamondedge.util.XmlDom) null;
            //
          }
        }

        // paso el estado para guardar en la tabla BRO_OPERACIONES
        invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvar_Estado), new Variant("ESTADOPROC") } );

        // paso el tiempo de proceso AIS para guardar en la tabla BRO_OPERACIONES
        invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvar_TiempoProceso), new Variant("TIEMPOPROCESO") } );

        //Alta de la OPERACION . SPSNCV_BRO_ALTA_OPER
        wvarStep = 130;
        //
        if( invoke( "fncPutCotHO", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje.toString()), new Variant(wvarCodErr), new Variant(wvarMensajePutTran) } ) )
        {
          wobjXMLReturnVal = new diamondedge.util.XmlDom();
          //unsup wobjXMLReturnVal.async = false;
          wobjXMLReturnVal.loadXML( wvarMensajePutTran.toString() );
          wvarStep = 140;
          if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" ) */ ).equals( "0" )) )
          {
            wvarStep = 150;
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) );
            pvarRes.set( wvarMensajePutTran );
            return fncGetAll;
          }
          wobjXMLReturnVal = (diamondedge.util.XmlDom) null;
          wobjClass = null;
          fncGetAll = true;
        }
        //
      }


      fin: 
      wobjClass = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLParams = (diamondedge.util.XmlDom) null;
      wobjXMLReturnVal = (diamondedge.util.XmlDom) null;
      wobjXMLError = (diamondedge.util.XmlDom) null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;
        /*unsup mobjCOM_Context.SetComplete() */;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }

  private boolean fncInsertNode( Variant pvarRequest, String pvarHeader, String pvarNodeValue, String pvarNodeName )
  {
    boolean fncInsertNode = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLRequest = null;


    try 
    {
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest.toString() );
      //
      wvarStep = 20;
      /*unsup wobjXMLRequest.selectSingleNode( pvarHeader ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), pvarNodeName, "" ) */ );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + pvarNodeName ) */, pvarNodeValue );
      //
      pvarRequest.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      fncInsertNode = true;
      //
      fin: 
      return fncInsertNode;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //AM Debug
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncInsertNode = false;
        /*unsup mobjCOM_Context.SetComplete() */;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncInsertNode;
  }

  private boolean fncPutCotHO( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutCotHO = false;
    int wvarStep = 0;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_NROCOT = "";
    String mvarWDB_TIPOOPERAC = "";
    String mvarWDB_ESTADO = "";
    String mvarWDB_RECEPCION_PEDIDOANO = "";
    String mvarWDB_RECEPCION_PEDIDOMES = "";
    String mvarWDB_RECEPCION_PEDIDODIA = "";
    String mvarWDB_RECEPCION_PEDIDOHORA = "";
    String mvarWDB_RECEPCION_PEDIDOMINUTO = "";
    String mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_TIEMPOPROCESO = "";
    String mvarWDB_XML = "";

    //
    //
    //
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_CODINST = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }
      //
      wvarStep = 30;
      mvarWDB_REQUESTID = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID ) */ );
      }
      //
      wvarStep = 40;
      mvarWDB_NROCOT = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROCOT) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NROCOT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NROCOT ) */ );
      }
      //
      wvarStep = 50;
      mvarWDB_TIPOOPERAC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoOperac) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPOOPERAC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoOperac ) */ );
      }
      //
      wvarStep = 60;
      mvarWDB_ESTADO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EstadoProc) ) */ == (org.w3c.dom.Node) null) )
      {
        //EG 10/2010 cambia el valor del tag al actualizarce componente
        mvarWDB_ESTADO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_EstadoProc ) */ );
        if( mvarWDB_ESTADO.equals( "true" ) )
        {
          mvarWDB_ESTADO = "OK";
        }
        else
        {
          mvarWDB_ESTADO = "ER";
        }
      }
      //
      wvarStep = 70;
      mvarWDB_RECEPCION_PEDIDOANO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoAno) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoAno ) */ );
      }
      //
      wvarStep = 80;
      mvarWDB_RECEPCION_PEDIDOMES = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMes) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMes ) */ );
      }
      //
      wvarStep = 90;
      mvarWDB_RECEPCION_PEDIDODIA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoDia) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDODIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoDia ) */ );
      }
      //
      wvarStep = 100;
      mvarWDB_RECEPCION_PEDIDOHORA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoHora) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOHORA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoHora ) */ );
      }
      //
      wvarStep = 110;
      mvarWDB_RECEPCION_PEDIDOMINUTO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMinuto) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMINUTO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMinuto ) */ );
      }
      //
      wvarStep = 120;
      mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoSegundo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOSEGUNDO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoSegundo ) */ );
      }
      //
      wvarStep = 130;
      mvarWDB_RAMOPCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAMOPCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_RAMOPCOD ) */ );
      }
      //
      wvarStep = 140;
      mvarWDB_POLIZANN = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ == (org.w3c.dom.Node) null) )
      {
        if( new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN ) */ ) ).isNumeric() )
        {
          if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ ) ) < 100 )
          {
            mvarWDB_POLIZANN = String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN ) */ ) ) );
          }
        }
      }
      //
      wvarStep = 150;
      mvarWDB_POLIZSEC = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ == (org.w3c.dom.Node) null) )
      {
        if( new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC ) */ ) ).isNumeric() )
        {
          if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ ) ) < 1000000 )
          {
            mvarWDB_POLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC ) */ );
          }
        }
      }
      //
      wvarStep = 160;
      mvarWDB_CERTISEC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CERTISEC) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTISEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CERTISEC ) */ );
      }
      //
      wvarStep = 170;
      mvarWDB_TIEMPOPROCESO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TiempoProceso) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIEMPOPROCESO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TiempoProceso ) */ );
      }
      //
      wvarStep = 180;
      mvarWDB_XML = "";
      mvarWDB_XML = pvarRequest;
      //
      wvarStep = 190;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      //
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_NRO_COTIZACION_LBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NROCOT.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NROCOT)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 18 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 230;
      wobjDBParm = new Parameter( "@WDB_TIPO_OPERACION", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_TIPOOPERAC.equals( "" ) ? "" : mvarWDB_TIPOOPERAC) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 240;
      wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_ESTADO.equals( "" ) ? "" : mvarWDB_ESTADO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 250;
      wobjDBParm = new Parameter( "@WDB_RECEPCION_PEDIDO", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant( mvarWDB_RECEPCION_PEDIDOANO + "-" + mvarWDB_RECEPCION_PEDIDOMES + "-" + mvarWDB_RECEPCION_PEDIDODIA + " " + mvarWDB_RECEPCION_PEDIDOHORA + ":" + mvarWDB_RECEPCION_PEDIDOMINUTO + ":" + mvarWDB_RECEPCION_PEDIDOSEGUNDO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 260;
      wobjDBParm = new Parameter( "@WDB_ENVIO_RESPUESTA", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant( DateTime.year( DateTime.now() ) + "-" + DateTime.month( DateTime.now() ) + "-" + DateTime.day( DateTime.now() ) + " " + DateTime.hour( DateTime.now() ) + ":" + DateTime.minute( DateTime.now() ) + ":" + DateTime.second( DateTime.now() ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 270;
      wobjDBParm = new Parameter( "@WDB_RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_RAMOPCOD.equals( "" ) ? "" : mvarWDB_RAMOPCOD) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 280;
      wobjDBParm = new Parameter( "@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 290;
      wobjDBParm = new Parameter( "@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 300;
      wobjDBParm = new Parameter( "@WDB_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 310;
      wobjDBParm = new Parameter( "@WDB_CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 320;
      wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CERTISEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CERTISEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 330;
      wobjDBParm = new Parameter( "@WDB_TIEMPO_PROCESO_AIS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIEMPOPROCESO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIEMPOPROCESO)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 340;
      wobjDBParm = new Parameter( "@WDB_OPERACION_XML", AdoConst.adVarChar, AdoConst.adParamInput, 8000, new Variant( (mvarWDB_XML.equals( "" ) ? "" : Strings.left( mvarWDB_XML, 8000 )) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 350;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      //
      wobjDBCnn.close();
      //
      wvarStep = 360;
      fncPutCotHO = true;
      pvarRes.set( "<LBA_WS res_code=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" ) */ ) + "\" res_msg=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) + "\"></LBA_WS>" );
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
      return fncPutCotHO;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        fncPutCotHO = false;
        /*unsup mobjCOM_Context.SetComplete() */;


        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutCotHO;
  }

  private boolean fncCotizarAtmMQ( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncCotizarAtmMQ = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String mvarRequest = "";
    String mvarResponse = "";
    Variant oProd = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom mobjXMLDoc = null;
    Object wobjClass = null;
    String mvarProdok = "";
    org.w3c.dom.NodeList wobjXMLList = null;
    int wvarContNode = 0;
    String vNROCOTI = "";
    Variant vMES = new Variant();
    Variant vANO = new Variant();
    String vFECINIVG = "";
    Variant vMESsig = new Variant();
    String vANOsig = "";
    String vFECFINVG = "";
    String vTPLANATM = "";
    String vPOLIZANN = "";
    String vPOLIZSEC = "";
    String vREQUESTID = "";
    String vUSUARCOD = "";
    String Primer = "";





    wvarStep = 0;
    wvarCodErr.set( 0 );
    try 
    {

      wvarStep = 2;
      // Traigo los parámetros del request
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 3;
      vNROCOTI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/COT_NRO" ) */ );
      vREQUESTID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/REQUESTID" ) */ );


      vFECINIVG = Funciones.convertirFecha( new Variant( DateTime.dateSerial( DateTime.year( DateTime.now() ), DateTime.month( DateTime.now() ), 1 ) )/*warning: ByRef value change will be lost.*/ );
      vFECFINVG = Funciones.convertirFecha( new Variant( DateTime.dateSerial( DateTime.year( DateTime.now() ), DateTime.month( DateTime.now() ) + 1, 1 ) )/*warning: ByRef value change will be lost.*/ );
      vPOLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/POLIZANN" ) */ );
      vPOLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/POLIZSEC" ) */ );
      vTPLANATM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/TPLANATM" ) */ );
      vUSUARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/USUARCOD" ) */ );

      //Armo el Request para cotizar
      mvarRequest = "<Request><DEFINICION>2200_OVMsjGralCotizacion.xml</DEFINICION>";
      mvarRequest = mvarRequest + "  <NROCOTI>" + vNROCOTI + "</NROCOTI>";
      mvarRequest = mvarRequest + "  <RAMOPCOD>ATD1</RAMOPCOD>";
      mvarRequest = mvarRequest + "  <POLIZANN>" + vPOLIZANN + "</POLIZANN>";
      mvarRequest = mvarRequest + "  <POLIZSEC>" + vPOLIZSEC + "</POLIZSEC>";
      mvarRequest = mvarRequest + "  <FECSOLIC>0</FECSOLIC>";
      mvarRequest = mvarRequest + "  <FECINIVG>" + vFECINIVG + "</FECINIVG>";
      mvarRequest = mvarRequest + "  <FECFINVG>" + vFECFINVG + "</FECFINVG>";
      mvarRequest = mvarRequest + "  <AGENTCLAPR1>PR</AGENTCLAPR1>";
      mvarRequest = mvarRequest + "  <AGENTCODPR1>0</AGENTCODPR1>";
      mvarRequest = mvarRequest + "  <AGENTCLAPR2/>";
      mvarRequest = mvarRequest + "  <AGENTCODPR2>0</AGENTCODPR2>";
      mvarRequest = mvarRequest + "  <AGENTCLAOR>OR</AGENTCLAOR>";
      mvarRequest = mvarRequest + "  <AGENTCODOR>0</AGENTCODOR>";
      mvarRequest = mvarRequest + "  <COMISIONPR1/>";
      mvarRequest = mvarRequest + "  <COMISIONPR2>0</COMISIONPR2>";
      mvarRequest = mvarRequest + "  <COMISIONOR/>";
      mvarRequest = mvarRequest + "  <PLANPCOD>0</PLANPCOD>";
      mvarRequest = mvarRequest + "  <COBROCOD>0</COBROCOD>";
      mvarRequest = mvarRequest + "  <COBROTIP/>";
      mvarRequest = mvarRequest + "  <PROFECOD/>";
      mvarRequest = mvarRequest + "  <OPERATIP>AP</OPERATIP>";
      mvarRequest = mvarRequest + "  <COBROFOR>0</COBROFOR>";
      mvarRequest = mvarRequest + "  <CAUSAEND>0</CAUSAEND>";
      mvarRequest = mvarRequest + "  <CLIENTIP>0</CLIENTIP>";
      mvarRequest = mvarRequest + "  <CLIENSEX/>";
      mvarRequest = mvarRequest + "  <FECNACIM>0</FECNACIM>";
      mvarRequest = mvarRequest + "  <CLIENEST/>";
      mvarRequest = mvarRequest + "  <NACIONAL>00</NACIONAL>";
      mvarRequest = mvarRequest + "  <CLIENIVA>3</CLIENIVA>";
      mvarRequest = mvarRequest + "  <CLIEIBTP>0</CLIEIBTP>";
      mvarRequest = mvarRequest + "  <CLAUAJUS>0</CLAUAJUS>";
      mvarRequest = mvarRequest + "  <TPLANATM>" + vTPLANATM + "</TPLANATM>";
      mvarRequest = mvarRequest + "  <PARTICOD>0</PARTICOD>";
      mvarRequest = mvarRequest + "  <LOCALCOD>0</LOCALCOD>";
      mvarRequest = mvarRequest + "  <DOMICCPO>X0000</DOMICCPO>";
      mvarRequest = mvarRequest + "  <PROVICOD>1</PROVICOD>";
      mvarRequest = mvarRequest + "  <PAISSCOD>00</PAISSCOD></Request>";

      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( new Variant( mvarRequest ), new Variant( mvarResponse ), "" );
      wobjClass = null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );

      mvarProdok = "ER";
      if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {
          //Armo la salida
          pvarRes.set( "" );
          pvarRes.set( pvarRes + "<Response><Estado resultado='true' mensaje='' />" );
          pvarRes.set( pvarRes + "<REQUESTID>" + vREQUESTID + "</REQUESTID>" );
          pvarRes.set( pvarRes + "<COT_NRO>" + vNROCOTI + "</COT_NRO>" );
          pvarRes.set( pvarRes + "<TPLANATM>" + vTPLANATM + "</TPLANATM>" );
          pvarRes.set( pvarRes + "<USUARCOD>" + vUSUARCOD + "</USUARCOD>" );
          pvarRes.set( pvarRes + "<COMP_PRECIO>" );
          pvarRes.set( pvarRes + "<PRIMA>" + diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//PRIMAIMP" ) */ ) + "</PRIMA>" );
          pvarRes.set( pvarRes + "<RECARGOS>" + diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//RECAIMPO" ) */ ) + "</RECARGOS>" );
          pvarRes.set( pvarRes + "<IVAIMPOR>" + diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//IVAIMPOR" ) */ ) + "</IVAIMPOR>" );
          pvarRes.set( pvarRes + "<IVAIMPOA>" + diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//IVAIMPOA" ) */ ) + "</IVAIMPOA>" );
          pvarRes.set( pvarRes + "<IVARETEN>" + diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//IVARETEN" ) */ ) + "</IVARETEN>" );
          pvarRes.set( pvarRes + "<DEREMI>" + diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//DEREMIMP" ) */ ) + "</DEREMI>" );
          pvarRes.set( pvarRes + "<SELLADO>" + diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//SELLAIMP" ) */ ) + "</SELLADO>" );
          pvarRes.set( pvarRes + "<INGBRU>" + diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//INGBRIMP" ) */ ) + "</INGBRU>" );
          pvarRes.set( pvarRes + "<IMPUES>" + diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//IMPUEIMP" ) */ ) + "</IMPUES>" );
          pvarRes.set( pvarRes + "<PRECIO>" + diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//RECTOIMP" ) */ ) + "</PRECIO>" );
          pvarRes.set( pvarRes + "</COMP_PRECIO></Response>" );

          //pvarRes = mvarResponse
          mvarProdok = "OK";
        }
      }

      if( mvarProdok.equals( "ER" ) )
      {
        wvarCodErr.set( -53 );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=Poliza  no habilitada /></Response>" );
        return fncCotizarAtmMQ;
      }


      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLList = (org.w3c.dom.NodeList) null;

      wvarStep = 240;
      if( wvarCodErr.toInt() == 0 )
      {
        fncCotizarAtmMQ = true;
      }
      else
      {
        fncCotizarAtmMQ = false;
      }

      fin: 
      return fncCotizarAtmMQ;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );
        wvarCodErr.set( -1000 );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        //fncCotizarAtmMQ False
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncCotizarAtmMQ;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
