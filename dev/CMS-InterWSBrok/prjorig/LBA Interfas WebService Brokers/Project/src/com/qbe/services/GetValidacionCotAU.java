package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class GetValidacionCotAU implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  static final String mcteClassName = "LBA_InterWSBrok.GetValidacionCotAU";
  static final String mcteStoreProc = "SPSNCV_BRO_COTIZACION_SCO";
  static final String mcteArchivoAUSCOT_XML = "LBA_VALIDACION_COT_AU.XML";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_PRODUCTOR = "AGECOD";
  static final String mcteParam_CampaCod = "CAMPACOD";
  static final String mcteParam_KMsrngCod = "KMSRNGCOD";
  static final String mcteParam_Provi = "PROVI";
  static final String mcteParam_LocalidadCod = "LOCALIDADCOD";
  static final String mcteParam_COBROCOD = "COBROCOD";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_SUMAASEG = "SUMAASEG";
  static final String mcteParam_ModeAutCod = "MODEAUTCOD";
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_NacimAnn = "NACIMANN";
  static final String mcteParam_NacimMes = "NACIMMES";
  static final String mcteParam_NacimDia = "NACIMDIA";
  static final String mcteParam_Sexo = "SEXO";
  static final String mcteParam_Estado = "ESTADO";
  static final String mcteParam_EfectAnn = "EFECTANN";
  static final String mcteParam_SiGarage = "SIGARAGE";
  static final String mcteParam_Siniestros = "SINIESTROS";
  static final String mcteParam_Gas = "GAS";
  static final String mcteParam_ClubLBA = "CLUBLBA";
  /**
   *  Se agregan las nuevas coberturas adic. jc 08/2010
   */
  static final String mcteParam_Dt80 = "DESTRUCCION_80";
  static final String mcteParam_Luneta = "LUNETA";
  static final String mcteParam_ClubEco = "CLUBECO";
  static final String mcteParam_Robocont = "ROBOCONT";
  static final String mcteParam_Granizo = "GRANIZO";
  static final String mcteParam_Escero = "ESCERO";
  /**
   *  fin jc
   * JC 2010-11 ch.r.
   */
  static final String mcteParam_CLIENTIP = "CLIENTIP";
  /**
   * JC 2010-11 ch.r.
   */
  static final String mcteParam_IBB = "IBB";
  static final String mcteParam_CLIENIVA = "IVA";
  /**
   *  DATOS DE LOS HIJOS
   */
  static final String mcteNodos_Hijos = "//Request/HIJOS/HIJO";
  static final String mcteParam_NacimHijo = "NACIMHIJO";
  static final String mcteParam_SexoHijo = "SEXOHIJO";
  static final String mcteParam_EstadoHijo = "ESTADOHIJO";
  static final String mcteParam_EdadHijo = "EDADHIJO";
  /**
   *  DATOS DE LOS ACCESORIOS
   */
  static final String mcteNodos_Accesorios = "//Request/ACCESORIOS/ACCESORIO";
  static final String mcteParam_PrecioAcc = "PRECIOACC";
  static final String mcteParam_DescripcionAcc = "DESCRIPCIONACC";
  static final String mcteParam_CodigoAcc = "CODIGOACC";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncValidaABase
   * static variable for method: fncValidaMQ
   */
  private final String wcteFnName = "fncValidaMQ";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    int wvarCodErr = 0;
    Variant wvarMensaje = new Variant();
    Variant pvarRes = new Variant();
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      pvarRes.set( "" );
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(new Variant( wvarCodErr ))/*warning: ByRef value change will be lost.*/, new Variant(pvarRes) } )) )
      {
        if( wvarCodErr == Obj.toInt( "0" ) )
        {
          wvarCodErr = -1000;
        }
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + wvarCodErr + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + wvarMensaje + String.valueOf( (char)(34) ) + " /></Response>" );
        wvarStep = 20;
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }
      wvarStep = 30;
      pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + pvarRes + "</Response>" );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjxmlRequestChild = null;
    diamondedge.util.XmlDom wobjXMLCheck = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNodeErrorVacios = null;
    com.qbe.services.HSBCInterfaces.IDBConnection wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarContChild = 0;
    int wvarContNode = 0;
    String wvarErrorCodigo = "";
    int wvarStep = 0;
    String wvarCLIENTIP = "";
    String wvarCABA = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarIVAok = "";
    org.w3c.dom.Node oTipoiva = null;
    diamondedge.util.XmlDom mobjXMLDoc = null;
    Object wobjClass = null;
    String mvarREQ_IIBB = "";
    String mvarIBBok = "";
    org.w3c.dom.Node oTipoibb = null;



    //11/2010 ch.r
    //f ch.r
    wvarStep = 0;
    wvarCodErr.set( 0 );
    try 
    {
      //Chequeo que los datos esten OK
      //Chequeo que no venga nada en blanco
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 20;
      wobjXMLCheck = new diamondedge.util.XmlDom();
      //unsup wobjXMLCheck.async = false;
      wobjXMLCheck.load( System.getProperty("user.dir") + "\\" + mcteArchivoAUSCOT_XML );

      wvarStep = 30;
      //Chequeo nodo padre
      wobjXMLNodeErrorVacios = null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/VACIOS/CAMPOS" ) */;
      //Tomo codigo de error de vacios
      wvarStep = 40;
      wvarErrorCodigo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/VACIOS/N_ERROR" ) */ );
      for( wvarContChild = 0; wvarContChild <= (wobjXMLNodeErrorVacios.getChildNodes().getLength() - 1); wvarContChild++ )
      {
        if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + wobjXMLNodeErrorVacios.getChildNodes().item( wvarContChild ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
        {
          if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + wobjXMLNodeErrorVacios.getChildNodes().item( wvarContChild ).getNodeName()) ) */ ) ).equals( "" ) )
          {
            //error
            wvarStep = 50;
            wvarCodErr.set( -1 );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( wobjXMLNodeErrorVacios.getChildNodes().item( wvarContChild ) ) );
            pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );
            return fncGetAll;
          }
        }
        else
        {
          //error
          wvarStep = 60;
          wvarCodErr.set( -1 );
          wvarMensaje.set( diamondedge.util.XmlDom.getText( wobjXMLNodeErrorVacios.getChildNodes().item( wvarContChild ) ) );
          pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
          return fncGetAll;
        }
      }


      //Chequeo Tipo y Longitud de Datos para el XML Padre
      wvarStep = 70;

      for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().getLength() - 1); wvarContChild++ )
      {
        if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
        {
          //Chequeo la longitud y si tiene Largo Fijo
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
          {
            if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") ) */ ) )) )
            {
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
              return fncGetAll;
            }
          }
          else
          {
            if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") ) */ ) ) )
            {
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
              return fncGetAll;
            }
          }

          //Chequeo el Tipo
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
          {
            if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ).isNumeric()) )
            {
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
              return fncGetAll;
            }
            else
            {
              //Chequeo que el dato numerico no sea 0
              //Excluye los nodos COT_NRO, SINIESTROS
              if( (Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) == 0) && ! (((null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ).getNodeName().equals( "COT_NRO" )) || (null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ).getNodeName().equals( "SINIESTROS" )))) )
              {
                wvarCodErr.set( -199 );
                wvarMensaje.set( "El valor del dato numerico no puede estar en 0" );
                pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
                return fncGetAll;
              }
            }
          }
        }
      }

      //Chequeo Tipo y Longitud de Datos para Coberturas
      for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */.getLength() - 1); wvarContChild++ )
      {
        wvarStep = 80;
        for( wvarContNode = 0; wvarContNode <= (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */.item( wvarContChild ).getChildNodes().getLength() - 1); wvarContNode++ )
        {
          if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
          {
            //Chequeo la longitud y si tiene Largo Fijo
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
            {
              if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) )) )
              {
                wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
                return fncGetAll;
              }
            }
            else
            {
              if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) ) )
              {
                wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
                return fncGetAll;
              }
            }

            //Chequeo el Tipo
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
            {
              if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ).isNumeric()) )
              {
                wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
                return fncGetAll;
              }
            }
          }
        }
      }


      //Chequeo Tipo y Longitud de Datos de Hijos
      wvarStep = 90;
      for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */.getLength() - 1); wvarContChild++ )
      {
        for( wvarContNode = 0; wvarContNode <= (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */.item( wvarContChild ).getChildNodes().getLength() - 1); wvarContNode++ )
        {
          if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
          {
            //Chequeo la longitud y si tiene Largo Fijo
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
            {
              if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) )) )
              {
                wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
                return fncGetAll;
              }
            }
            else
            {
              if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) ) )
              {
                wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
                return fncGetAll;
              }
            }

            //Chequeo el Tipo
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
            {
              if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ).isNumeric()) )
              {
                wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
                wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
                pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
                return fncGetAll;
              }
            }
          }
        }
      }


      //Chequeo EDAD
      wvarStep = 100;
      if( ! (Funciones.ValidarFechayRango( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimDia ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimMes) ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimAnn) ) */ ), 17, 86, wvarMensaje )) )
      {
        //error
        wvarCodErr.set( -9 );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;

      }

      //No se permite cotizar con GNC - Adriana 21-12-2012
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Gas) ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Gas) ) */ ).equals( "S" ) )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "No se permite cotizar veh�culos con GNC" );
          pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
          return fncGetAll;
        }

      }

      // If Not wobjXMLList(wvarContNode).selectSingleNode(mcteParam_Gas) Is Nothing Then
      //    If wobjXMLList(wvarContNode).selectSingleNode(mcteParam_Gas).Text = "S" Then
      //    wvarCodErr = -1
      //    wvarMensaje = "No se permite cotizar veh�culos con GNC"
      //    pvarRes = "<Response><Estado resultado=""false"" mensaje=""" & wvarMensaje & """ /></Response>"
      //    Exit Function
      //    End If
      //End If
      //Chequeo los datos FIJOS
      wvarStep = 110;
      if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Sexo + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Sexo) ) *) ) + "']") ) */.getLength() == 0 )
      {
        //error
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Sexo + "/N_ERROR" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Sexo + "/TEXTOERROR" ) */ ) );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }

      wvarStep = 120;
      if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Estado + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Estado) ) *) ) + "']") ) */.getLength() == 0 )
      {
        //error
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Estado + "/N_ERROR" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Estado + "/TEXTOERROR" ) */ ) );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }

      wvarStep = 130;
      if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Siniestros + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Siniestros) ) *) ) + "']") ) */.getLength() == 0 )
      {
        //error
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Siniestros + "/N_ERROR" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Siniestros + "/TEXTOERROR" ) */ ) );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }

      wvarStep = 140;
      if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SiGarage + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SiGarage) ) *) ) + "']") ) */.getLength() == 0 )
      {
        //error
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SiGarage + "/N_ERROR" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SiGarage + "/TEXTOERROR" ) */ ) );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }

      wvarStep = 150;
      if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Gas + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Gas) ) *) ) + "']") ) */.getLength() == 0 )
      {
        //error
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Gas + "/N_ERROR" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Gas + "/TEXTOERROR" ) */ ) );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }

      wvarStep = 160;
      if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_ClubLBA + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClubLBA) ) *) ) + "']") ) */.getLength() == 0 )
      {
        //error
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_ClubLBA + "/N_ERROR" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_ClubLBA + "/TEXTOERROR" ) */ ) );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }
      //Se agregan nuevas coberturas adic. jc 08/2010
      wvarStep = 161;
      if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Dt80 + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Dt80) ) *) ) + "']") ) */.getLength() == 0 )
      {
        //error
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Dt80 + "/N_ERROR" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Dt80 + "/TEXTOERROR" ) */ ) );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }
      wvarStep = 162;
      if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Luneta + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Luneta) ) *) ) + "']") ) */.getLength() == 0 )
      {
        //error
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Luneta + "/N_ERROR" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Luneta + "/TEXTOERROR" ) */ ) );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }
      wvarStep = 163;
      if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_ClubEco + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClubEco) ) *) ) + "']") ) */.getLength() == 0 )
      {
        //error
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_ClubEco + "/N_ERROR" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_ClubEco + "/TEXTOERROR" ) */ ) );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }
      wvarStep = 164;
      if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Robocont + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Robocont) ) *) ) + "']") ) */.getLength() == 0 )
      {
        //error
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Robocont + "/N_ERROR" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Robocont + "/TEXTOERROR" ) */ ) );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }
      wvarStep = 165;
      if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Granizo + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Granizo) ) *) ) + "']") ) */.getLength() == 0 )
      {
        //error
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Granizo + "/N_ERROR" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Granizo + "/TEXTOERROR" ) */ ) );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }
      wvarStep = 166;
      if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Escero + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Escero) ) *) ) + "']") ) */.getLength() == 0 )
      {
        //error
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Escero + "/N_ERROR" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Escero + "/TEXTOERROR" ) */ ) );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }
      //fin jc
      wvarStep = 170;
      //Chequeo los hijos
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */;
      for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
      {
        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_SexoHijo ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Sexo de Hijo en blanco" );
          pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
          return fncGetAll;
        }
        else
        {
          if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_SexoHijo ) *) ) + "']") ) */.getLength() == 0 )
          {
            //error
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
            return fncGetAll;
          }
        }

        wvarStep = 180;
        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_NacimHijo ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Fecha de Nacimiento de Hijo en blanco" );
          pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
          return fncGetAll;
        }
        else
        {
          if( ! (Funciones.ValidarFechayRango( Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_NacimHijo ) */ ), 2 ) + "/" + Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_NacimHijo ) */ ), 3, 2 ) + "/" + Strings.right( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_NacimHijo ) */ ), 4 ), 17, 29, wvarMensaje )) )
          {
            //error
            wvarCodErr.set( -10 );
            pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
            return fncGetAll;
          }
        }

        wvarStep = 190;
        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_EstadoHijo ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Estado Civil del Hijo en blanco" );
          pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
          return fncGetAll;
        }
        else
        {
          if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_EstadoHijo ) *) ) + "']") ) */.getLength() == 0 )
          {
            //error
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
            return fncGetAll;
          }
        }
        wvarStep = 200;
        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_EdadHijo ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Edad del Hijo en blanco" );
          pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
          return fncGetAll;
        }
        else
        {
          if( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_EdadHijo ) */ ) ) == 0 )
          {
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Edad del Hijo con Valor 0" );
            pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
            return fncGetAll;
          }
        }
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;

      //Chequeo los Accesorios
      wvarStep = 210;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */;
      for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
      {
        wvarStep = 220;
        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_PrecioAcc ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Precio de Accesorio en blanco" );
          pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
          return fncGetAll;
          //Else
          //   If Val(wobjXMLList(wvarContNode).selectSingleNode(mcteParam_PrecioAcc).Text) = 0 Then
          //     wvarCodErr = -1
          //     wvarMensaje = "Campo de Precio de Accesorio en 0"
          //     pvarRes = "<Response><Estado resultado=""false"" mensaje=""" & wvarMensaje & """ /></Response>"
          //     Exit Function
          //   End If
        }

        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_CodigoAcc ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Codigo de Accesorio en blanco" );
          pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
          return fncGetAll;
        }
        else
        {
          if( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_CodigoAcc ) */ ) ) == 0 )
          {
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Codigo de Accesorio en 0" );
            pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
            return fncGetAll;
          }
        }

        if( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_DescripcionAcc ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -1 );
          wvarMensaje.set( "Campo de Descripcion de Accesorio en blanco" );
          pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
          return fncGetAll;
        }
        else
        {
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( mcteParam_DescripcionAcc ) */ ).equals( "" ) )
          {
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Descripcion de Accesorio en blanco" );
            pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
            return fncGetAll;
          }
        }
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;

      //jc ch.r. se permiten varios ivas, se valida x componente
      //wvarStep = 230
      //If wobjXMLCheck.selectNodes("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" & mcteParam_CLIENIVA & "/VALOR[.='" & wobjXMLRequest.selectSingleNode("//" & mcteParam_CLIENIVA).Text & "']").length = 0 Then
      //error
      //    wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" & mcteParam_CLIENIVA & "/N_ERROR").Text
      //    wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" & mcteParam_CLIENIVA & "/TEXTOERROR").Text
      //    pvarRes = "<Response><Estado resultado=""false"" mensaje=""" & wvarMensaje & """ /></Response>"
      //    Exit Function
      //End If
      //jc ch.r.
      //valida persona fisica/juridica contra iva
      wvarStep = 235;

      wvarCABA = Strings.right( "00" + Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi ) */ ) ), 2 );
      if( null /*unsup wobjXMLRequest.selectNodes( ("//" + mcteParam_CLIENTIP) ) */.getLength() == 0 )
      {
        //Para forzar la persona f�sica si no informa el nodo
        wvarCLIENTIP = "00";
      }
      else
      {
        wvarCLIENTIP = Strings.right( "00" + Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENTIP ) */ ) ), 2 );
      }
      if( wvarCABA.equals( "01" ) )
      {
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ).equals( "3" )) )
        {
          if( ! (wvarCLIENTIP.equals( "15" )) )
          {
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo Tipo de Persona no valido" );
            pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
            return fncGetAll;
          }
        }
        else
        {
          if( ! (wvarCLIENTIP.equals( "00" )) )
          {
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo Tipo de Persona no valido" );
            pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
            return fncGetAll;
          }
        }
      }

      //11/2010 se permiten distintos iva
      wvarStep = 236;


      mvarRequest = "<Request></Request>";
      wobjClass = new lbawA_OfVirtualLBA.lbaw_GetTiposIVA();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(mvarRequest, mvarResponse, "")
      wobjClass = null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );

      mvarIVAok = "ER";
      if( null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.getLength() > 0 )
      {
        for( int noTipoiva = 0; noTipoiva < null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.getLength(); noTipoiva++ )
        {
          oTipoiva = null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.item( noTipoiva );
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ).equals( diamondedge.util.XmlDom.getText( oTipoiva.getAttributes().getNamedItem( "value" ) ) ) )
          {
            mvarIVAok = "OK";
          }
        }
      }
      else
      {
        mvarIVAok = "FC";
      }

      if( mvarIVAok.equals( "ER" ) )
      {
        wvarCodErr.set( -12 );
        wvarMensaje.set( "Valor de Responsabilidad frente al IVA invalido" );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }

      if( mvarIVAok.equals( "FC" ) )
      {
        wvarCodErr.set( -12 );
        wvarMensaje.set( "No se pudo recuperar el Valor del IVA desde el equipo central" );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }

      mobjXMLDoc = (diamondedge.util.XmlDom) null;

      // valida ingresos brutos
      mvarRequest = "<Request><PAISSCOD>00</PAISSCOD><V_PROVICOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi ) */ ) + "</V_PROVICOD><RAMOPCOD>AUS1</RAMOPCOD><CLIENIVA>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ) + "</CLIENIVA></Request>";
      wobjClass = new lbawA_OfVirtualLBA.lbaw_GetZonaIngBrut();
      wobjClass.Execute( mvarRequest, mvarResponse, "" );
      wobjClass = (Object) null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );
      //
      if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
      {
        mvarREQ_IIBB = "S";
      }
      else
      {
        mvarREQ_IIBB = "N";
      }
      //
      mobjXMLDoc = (diamondedge.util.XmlDom) null;

      if( mvarREQ_IIBB.equals( "S" ) )
      {
        //
        mvarRequest = "<Request><NUMERO>0</NUMERO></Request>";
        wobjClass = new lbawA_OfVirtualLBA.lbaw_GetTipIngBrut();
        wobjClass.Execute( mvarRequest, mvarResponse, "" );
        wobjClass = (Object) null;
        mobjXMLDoc = new diamondedge.util.XmlDom();
        //unsup mobjXMLDoc.async = false;
        mobjXMLDoc.loadXML( mvarResponse );

        mvarIBBok = "ER";
        if( null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.getLength() > 0 )
        {
          for( int noTipoibb = 0; noTipoibb < null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.getLength(); noTipoibb++ )
          {
            oTipoibb = null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.item( noTipoibb );
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_IBB) ) */ ).equals( diamondedge.util.XmlDom.getText( oTipoibb.getAttributes().getNamedItem( "value" ) ) ) )
            {
              mvarIBBok = "OK";
            }
          }
        }
        else
        {
          mvarIBBok = "FC";
        }
      }
      else
      {
        if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_IBB) ) */ ) ) == 0 )
        {
          mvarIBBok = "OK";
        }
        else
        {
          mvarIBBok = "ER";
        }
      }

      if( mvarIBBok.equals( "ER" ) )
      {
        wvarCodErr.set( -12 );
        wvarMensaje.set( "Valor de Codigo de Ingresos Brutos invalido" );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }

      if( mvarIBBok.equals( "FC" ) )
      {
        wvarCodErr.set( -12 );
        wvarMensaje.set( "No se pudo recuperar el Valor del IBB desde el equipo central" );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncGetAll;
      }

      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      //fin ch.r
      wvarStep = 240;
      if( wvarCodErr.toInt() == 0 )
      {
        if( invoke( "fncValidaABase", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } ) )
        {
          fncGetAll = true;
        }
        else
        {
          fncGetAll = false;
        }
      }
      else
      {
        fncGetAll = false;
      }

      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = (com.qbe.services.HSBCInterfaces.IDBConnection) null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );
        wvarMensaje.set( General.mcteErrorInesperadoDescr );
        wvarCodErr.set( -1000 );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }

  private boolean fncValidaABase( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncValidaABase = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    boolean wvarError = false;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXML = null;
    diamondedge.util.XmlDom wobjXSL = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    String wvarXSL = "";
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarContNode = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_PRODUCTOR = "";
    String mvarWDB_CAMPANA = "";
    String mvarWDB_KILOMETROS = "";
    String mvarWDB_CODPROV = "";
    String mvarWDB_CODPOST = "";
    String mvarWDB_COBROCOD = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_SUMAASEG = "";
    String mvarWDB_CODIGO_ACCESORIO = "";
    String mvarWDB_SUMA_ACCESORIO = "";
    String mvarWDB_AUMARCOD = "";
    String mvarWDB_AUMODCOD = "";
    String mvarWDB_AUSUBCOD = "";
    String mvarWDB_AUADICOD = "";
    String mvarWDB_AUMODORI = "";
    String mvarWDA_REQUESTID = "";





    try 
    {

      wvarStep = 10;
      wvarError = false;

      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 20;
      mvarWDB_CODINST = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }

      wvarStep = 30;
      mvarWDB_PRODUCTOR = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PRODUCTOR) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PRODUCTOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PRODUCTOR ) */ );
      }

      wvarStep = 40;
      mvarWDB_CAMPANA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CampaCod) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CAMPANA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CampaCod ) */ );
      }

      wvarStep = 50;
      mvarWDB_KILOMETROS = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_KMsrngCod) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_KILOMETROS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_KMsrngCod ) */ );
      }

      wvarStep = 60;
      mvarWDB_CODPROV = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODPROV = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi ) */ );
      }

      wvarStep = 70;
      mvarWDB_CODPOST = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODPOST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCod ) */ );
      }

      wvarStep = 80;
      mvarWDB_COBROCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD ) */ );
      }

      wvarStep = 90;
      mvarWDB_COBROTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROTIP = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP ) */ ) );
      }

      wvarStep = 100;
      mvarWDB_SUMAASEG = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SUMAASEG) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_SUMAASEG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SUMAASEG ) */ );
      }

      wvarStep = 110;
      mvarWDB_AUMODCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ModeAutCod) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_AUMODCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ModeAutCod ) */ );
      }

      wvarStep = 120;
      if( Strings.len( mvarWDB_AUMODCOD ) == 21 )
      {
        mvarWDB_AUMARCOD = Strings.mid( mvarWDB_AUMODCOD, 1, 5 );
        mvarWDB_AUSUBCOD = Strings.mid( mvarWDB_AUMODCOD, 11, 5 );
        mvarWDB_AUADICOD = Strings.mid( mvarWDB_AUMODCOD, 16, 5 );
        mvarWDB_AUMODORI = Strings.mid( mvarWDB_AUMODCOD, 21, 1 );
        mvarWDB_AUMODCOD = Strings.mid( mvarWDB_AUMODCOD, 6, 5 );
      }

      wvarStep = 130;
      mvarWDA_REQUESTID = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDA_REQUESTID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID ) */ );
      }

      wvarStep = 140;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 150;
      wobjDBParm = new Parameter( "@WDB_CODINST", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 160;
      wobjDBParm = new Parameter( "@WDB_PRODUCTOR", AdoConst.adChar, AdoConst.adParamInput, 4, (mvarWDB_PRODUCTOR.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_PRODUCTOR)) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 170;
      wobjDBParm = new Parameter( "@WDB_CAMPANA", AdoConst.adChar, AdoConst.adParamInput, 4, (mvarWDB_CAMPANA.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_CAMPANA)) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 180;
      wobjDBParm = new Parameter( "@WDB_KILOMETROS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_KILOMETROS.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_KILOMETROS)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 190;
      wobjDBParm = new Parameter( "@WDB_CODPROV", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODPROV.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_CODPROV)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_CODPOST", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODPOST.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_CODPOST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_COBROCOD.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_COBROCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, (mvarWDB_COBROTIP.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_COBROTIP)) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 230;
      wobjDBParm = new Parameter( "@WDB_SUMAASEG", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_SUMAASEG.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_SUMAASEG)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 15 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 240;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */;
      for( wvarContNode = 0; wvarContNode <= 4; wvarContNode++ )
      {
        wobjXMLNode = wobjXMLList.item( wvarContNode );
        if( wvarContNode <= (wobjXMLList.getLength() - 1) )
        {
          mvarWDB_SUMA_ACCESORIO = "0";
          wvarStep = 250;
          if( ! (null /*unsup wobjXMLNode.selectSingleNode( mcteParam_PrecioAcc ) */ == (org.w3c.dom.Node) null) )
          {
            mvarWDB_SUMA_ACCESORIO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_PrecioAcc ) */ );
          }
          else
          {
            wvarError = true;
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Precio de Accesorios en blanco" );
          }
          mvarWDB_CODIGO_ACCESORIO = "0";
          wvarStep = 260;
          if( ! (null /*unsup wobjXMLNode.selectSingleNode( mcteParam_CodigoAcc ) */ == (org.w3c.dom.Node) null) )
          {
            mvarWDB_CODIGO_ACCESORIO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_CodigoAcc ) */ );
          }
          else
          {
            wvarError = true;
            wvarCodErr.set( -1 );
            wvarMensaje.set( "Campo de Codigo de Accesorios en blanco" );
          }
          wvarStep = 270;
          wobjDBParm = new Parameter( "@WDB_CODIGO_ACCESORIO_" + String.valueOf( wvarContNode + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODIGO_ACCESORIO.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_CODIGO_ACCESORIO)) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 3 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 280;
          wobjDBParm = new Parameter( "@WDB_SUMA_ACCESORIO_" + String.valueOf( wvarContNode + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_SUMA_ACCESORIO.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_SUMA_ACCESORIO)) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          wvarStep = 290;
          wobjDBParm = new Parameter( "@WDB_CODIGO_ACCESORIO_" + String.valueOf( wvarContNode + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 3 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 300;
          wobjDBParm = new Parameter( "@WDB_SUMA_ACCESORIO_" + String.valueOf( wvarContNode + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
        }
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;

      wvarStep = 310;
      wobjDBParm = new Parameter( "@WDB_AUMARCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_AUMARCOD.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_AUMARCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 320;
      wobjDBParm = new Parameter( "@WDB_AUMODCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_AUMODCOD.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_AUMODCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 330;
      wobjDBParm = new Parameter( "@WDB_AUSUBCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_AUSUBCOD.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_AUSUBCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 340;
      wobjDBParm = new Parameter( "@WDB_AUADICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_AUADICOD.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_AUADICOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 350;
      wobjDBParm = new Parameter( "@WDB_AUMODORI", AdoConst.adChar, AdoConst.adParamInput, 1, (mvarWDB_AUMODORI.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_AUMODORI)) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 360;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 1, new Variant( mvarWDA_REQUESTID ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //jc 08/2010 el @WDB_NROCOT al cambiar el componente de cotiz. no se obtiene mas en la sp SPSNCV_BRO_COTIZACION_SCO viene en 0
      wvarStep = 370;
      wobjDBParm = new Parameter( "@WDB_NROCOT", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 380;
      wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 390;
      if( ! (wvarError) )
      {
        wrstRes = new Recordset();
        wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
        wrstRes.setActiveConnection( (Connection) null );

        wvarStep = 400;
        wobjXML = new diamondedge.util.XmlDom();
        //unsup wobjXML.async = false;
        wobjXML.load( System.getProperty("user.dir") + "\\" + mcteArchivoAUSCOT_XML );

        wvarStep = 410;
        //SI NO HUBO ERROR AGREGO LOS DATOS AL XML DE ENTRADA PARA LA RESPUESTA
        if( ! (wrstRes.isEOF()) )
        {
          if( Strings.toUpperCase( Strings.trim( wrstRes.getFields().getField(0).getValue().toString() ) ).equals( "OK" ) )
          {
            wvarStep = 420;
            //jc 08/2010 el @WDB_NROCOT al cambiar el componente de cotiz. no se obtiene mas en la sp SPSNCV_BRO_COTIZACION_SCO viene en 0
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "WDB_NROCOT", "" ) */ );
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//WDB_NROCOT" ) */, wobjDBCmd.getParameters().getParameter("@WDB_NROCOT").getValue().toString() );
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CERTISEC", "" ) */ );
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//CERTISEC" ) */, wobjDBCmd.getParameters().getParameter("@WDB_CERTISEC").getValue().toString() );
            wvarStep = 430;
            for( wvarContNode = 0; wvarContNode <= (wrstRes.getFields().getCount() - 1); wvarContNode++ )
            {
              /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), Strings.toUpperCase( wrstRes.getFields().getField(wvarContNode).getName() ), "" ) */ );
              wvarStep = 440;
              if( (wrstRes.getFields().getField(wvarContNode).getType() == AdoConst.adNumeric) && (wrstRes.getFields().getField(wvarContNode).getPrecision() == 18) )
              {
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + Strings.toUpperCase( wrstRes.getFields().getField(wvarContNode).getName() ) ) */, wrstRes.getFields().getField(wvarContNode).getValue().multiply( new Variant( 100 ) ).toString() );
              }
              else
              {
                diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + Strings.toUpperCase( wrstRes.getFields().getField(wvarContNode).getName() ) ) */, Strings.trim( wrstRes.getFields().getField(wvarContNode).getValue().toString() ) );
              }
            }
            pvarRes.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
          }
          else
          {
            //error
            wvarStep = 450;
            wvarError = true;
            if( null /*unsup wobjXML.selectNodes( ("//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']") ) */.getLength() > 0 )
            {
              wvarStep = 460;
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXML.selectSingleNode( "//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/TEXTOERROR" ) */ ) );
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXML.selectSingleNode( "//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/VALORRESPUESTA" ) */ ) );
            }
            else
            {
              wvarCodErr.set( wrstRes.getFields().getField(0).getValue() );
              wvarMensaje.set( "No existe descripcion para este error" );
            }
          }
        }
        else
        {
          //error
          wvarError = true;
          wvarCodErr.set( -16 );
          wvarMensaje.set( "No se retornaron datos de la base" );
        }
      }

      wvarStep = 470;

      wobjXML = (diamondedge.util.XmlDom) null;
      fncValidaABase = true;
      if( wvarError )
      {
        fncValidaABase = false;
        // End If12/2010 se cambian validaciones ch.request
      }
      else
      {
        if( invoke( "fncValidaMQ", new Variant[] { new Variant(wrstRes.getFields().getField("USUARCOD").getValue().toString()), new Variant(mvarWDB_PRODUCTOR), new Variant(mvarWDB_CAMPANA), new Variant(mvarWDB_COBROCOD), new Variant(mvarWDB_COBROTIP), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } ) )
        {
          fncValidaABase = true;
        }
        else
        {
          fncValidaABase = false;
        }
      }


      wrstRes.close();
      wobjDBCnn.close();
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return fncValidaABase;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );
        wvarMensaje.set( General.mcteErrorInesperadoDescr );
        wvarCodErr.set( -1000 );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncValidaABase = false;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncValidaABase;
  }

  private boolean fncValidaMQ( String varUsuarcod, String varProductor, String varCampana, String varCobrocod, String varCobrotip, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncValidaMQ = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String mvarRequest = "";
    String mvarResponse = "";
    org.w3c.dom.Node oProd = null;
    diamondedge.util.XmlDom mobjXMLDoc = null;
    lbawA_OVMQGen.lbaw_MQMensaje wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
    String mvarProdok = "";
    org.w3c.dom.NodeList wobjXMLList = null;
    int wvarContNode = 0;



    //f ch.r
    wvarStep = 0;
    wvarCodErr.set( 0 );
    try 
    {
      //valida productor
      wvarStep = 1;
      mvarRequest = "<Request><DEFINICION>ProductoresHabilitadosParaCotizar.xml</DEFINICION><AplicarXSL>ProductoresHabilitadosParaCotizarXML.xsl</AplicarXSL>";
      mvarRequest = mvarRequest + "<USUARCOD>" + varUsuarcod + "</USUARCOD>";
      mvarRequest = mvarRequest + "<CLIENSECAS></CLIENSECAS><NIVELCLAS></NIVELCLAS><RAMOPCOD>AUS1</RAMOPCOD></Request>";

      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( mvarRequest, mvarResponse, "" );
      wobjClass = (lbawA_OVMQGen.lbaw_MQMensaje) null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );

      mvarProdok = "ER";
      if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {
          for( int noProd = 0; noProd < null /*unsup mobjXMLDoc.selectNodes( "//AGENTE" ) */.getLength(); noProd++ )
          {
            oProd = null /*unsup mobjXMLDoc.selectNodes( "//AGENTE" ) */.item( noProd );
            if( varProductor.equals( diamondedge.util.XmlDom.getText( oProd.getAttributes().getNamedItem( "AGENTCOD" ) ) ) )
            {
              mvarProdok = "OK";
            }
          }
        }
        else
        {
          mvarProdok = "ER";
        }
      }
      else
      {
        mvarProdok = "FC";
      }

      if( mvarProdok.equals( "ER" ) )
      {
        wvarCodErr.set( -101 );
        wvarMensaje.set( "Productor no habilitado" );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncValidaMQ;
      }

      if( mvarProdok.equals( "FC" ) )
      {
        wvarCodErr.set( -101 );
        wvarMensaje.set( "No se pudo recuperar el codigo de productor desde el equipo central" );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncValidaMQ;
      }

      mobjXMLDoc = (diamondedge.util.XmlDom) null;

      // valida campa�as
      wvarStep = 2;
      mvarRequest = "<Request><DEFINICION>CampanasHabilitadasProductor.xml</DEFINICION>";
      mvarRequest = mvarRequest + "<USUARCOD>" + varUsuarcod + "</USUARCOD>";
      mvarRequest = mvarRequest + "<RAMOPCOD>AUS1</RAMOPCOD><AGENTCLA>PR</AGENTCLA>";
      mvarRequest = mvarRequest + "<AGENTCOD>" + varProductor + "</AGENTCOD></Request>";

      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( mvarRequest, mvarResponse, "" );
      wobjClass = (lbawA_OVMQGen.lbaw_MQMensaje) null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );

      mvarProdok = "ER";
      if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {

          wobjXMLList = null /*unsup mobjXMLDoc.selectNodes( "//CAMPOS/T-CAMPANAS-SAL/CAMPANA" ) */;
          for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
          {
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( "CAMPACOD" ) */ ).equals( varCampana ) )
            {
              mvarProdok = "OK";
            }
          }
        }
        else
        {
          mvarProdok = "ER";
        }
      }
      else
      {
        mvarProdok = "FC";
      }

      if( mvarProdok.equals( "ER" ) )
      {
        wvarCodErr.set( -101 );
        wvarMensaje.set( "Campania no habilitada" );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncValidaMQ;
      }

      if( mvarProdok.equals( "FC" ) )
      {
        wvarCodErr.set( -101 );
        wvarMensaje.set( "No se pudo recuperar el codigo de Campania desde el equipo central" );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncValidaMQ;
      }

      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      // valida forma de pago
      wvarStep = 3;
      mvarRequest = "<Request><DEFINICION>FormasDePagoxCampana.xml</DEFINICION><AplicarXSL>FormasDePagoxCampanaOut.xsl</AplicarXSL>";
      mvarRequest = mvarRequest + "<USUARCOD></USUARCOD><RAMOPCOD>AUS1</RAMOPCOD>";
      mvarRequest = mvarRequest + "<POLIZANN>0</POLIZANN><POLIZSEC>1</POLIZSEC>";
      mvarRequest = mvarRequest + "<CAMPACOD>" + varCampana + "</CAMPACOD></Request>";

      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( mvarRequest, mvarResponse, "" );
      wobjClass = (lbawA_OVMQGen.lbaw_MQMensaje) null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );

      mvarProdok = "ER";
      if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {
          for( int noProd = 0; noProd < null /*unsup mobjXMLDoc.selectNodes( "//option" ) */.getLength(); noProd++ )
          {
            oProd = null /*unsup mobjXMLDoc.selectNodes( "//option" ) */.item( noProd );
            if( varCobrocod.equals( diamondedge.util.XmlDom.getText( oProd.getAttributes().getNamedItem( "value" ) ) ) )
            {
              if( varCobrocod.equals( "4" ) )
              {
                mvarProdok = "OK";
              }
              else
              {
                if( varCobrotip.equals( diamondedge.util.XmlDom.getText( oProd.getAttributes().getNamedItem( "cobrotip" ) ) ) )
                {
                  mvarProdok = "OK";
                }
              }
            }
          }
        }
        else
        {
          mvarProdok = "ER";
        }
      }
      else
      {
        mvarProdok = "FC";
      }

      if( mvarProdok.equals( "ER" ) )
      {
        wvarCodErr.set( -104 );
        wvarMensaje.set( "Forma de Pago invalida" );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncValidaMQ;
      }

      if( mvarProdok.equals( "FC" ) )
      {
        wvarCodErr.set( -104 );
        wvarMensaje.set( "No se pudo recuperar la forma de pago desde el equipo central" );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncValidaMQ;
      }

      mobjXMLDoc = (diamondedge.util.XmlDom) null;

      wvarStep = 240;
      if( wvarCodErr.toInt() == 0 )
      {
        fncValidaMQ = true;
      }
      else
      {
        fncValidaMQ = false;
      }

      fin: 
      return fncValidaMQ;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );
        wvarMensaje.set( General.mcteErrorInesperadoDescr );
        wvarCodErr.set( -1000 );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncValidaMQ = false;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncValidaMQ;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
