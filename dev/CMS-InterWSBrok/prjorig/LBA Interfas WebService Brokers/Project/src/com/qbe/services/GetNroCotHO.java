package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class GetNroCotHO implements Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetNroCotHO";
  static final String mcteStoreProc = "SPSNCV_BRO_COTIZACION_HOM1";
  /**
   * Archivo de Errores
   */
  static final String mcteArchivoHOM_XML = "LBA_VALIDACION_COT_HO.XML";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  /**
   *  EG 14/10 cambia nombre constante
   * Const mcteParam_COBROCOD            As String = "COBROCOD"
   */
  static final String mcteParam_COBROCOD = "FPAGO";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_TIPOHOGAR = "TIPOHOGAR";
  /**
   *  EG 14/10 cambia nombre constante
   * Const mcteParam_PLANNCOD            As String = "PLANNCOD"
   */
  static final String mcteParam_PLANNCOD = "PLAN";
  /**
   *  EG 14/10 cambia nombre constante
   * Const mcteParam_Provi               As String = "PROVI"
   */
  static final String mcteParam_Provi = "PROVCOD";
  /**
   *  EG 14/10 cambia nombre constante
   * Const mcteParam_LocalidadCod        As String = "LOCALIDADCOD"
   */
  static final String mcteParam_LocalidadCod = "LOCALIDAD";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS/COBERTURA";
  /**
   * DATOS DE LAS CAMPA�AS
   */
  static final String mcteNodos_Campa = "//Request/CAMPANIAS/CAMPANIA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetNroCot
   * static variable for method: fncValidaMQ
   */
  private final String wcteFnName = "fncValidaMQ";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant pvarRes = new Variant();
    Variant wvarCodErr = new Variant();
    //
    //
    //Declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      pvarRes.set( "" );
      wvarCodErr.set( "" );
      wvarMensaje.set( "" );
      if( ! (invoke( "fncGetNroCot", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*unsup mobjCOM_Context.SetAbort() */;
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*unsup mobjCOM_Context.SetComplete() */;
        }
        return IAction_Execute;
      }
      //
      pvarResponse.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + pvarRes + "</LBA_WS>" );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetNroCot( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncGetNroCot = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXML = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarContChild = 0;
    int wvarContNode = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_TIPOHOGAR = "";
    String mvarWDB_PROVI = "";
    String mvarWDB_LOCALIDADCOD = "";
    String mvarWDB_COBROCOD = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_REQUESTID = "";

    //
    //
    //
    //
    //
    try 
    {
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_REQUESTID = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID ) */ );
      }
      //
      wvarStep = 30;
      mvarWDB_CODINST = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }
      //
      wvarStep = 40;
      mvarWDB_POLIZANN = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN ) */ );
      }
      //
      wvarStep = 50;
      mvarWDB_POLIZSEC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC ) */ );
      }
      //
      wvarStep = 60;
      mvarWDB_TIPOHOGAR = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TIPOHOGAR) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPOHOGAR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TIPOHOGAR ) */ );
      }
      //
      wvarStep = 70;
      mvarWDB_PROVI = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi ) */ );
      }
      //
      wvarStep = 80;
      mvarWDB_LOCALIDADCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_LOCALIDADCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCod ) */ );
      }
      //
      wvarStep = 90;
      mvarWDB_COBROCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD ) */ );
      }
      //
      wvarStep = 100;
      mvarWDB_COBROTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP ) */ );
      }
      //
      wvarStep = 110;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) */;
      wvarStep = 20;
      for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
      {
        //Por si falta un nodo dentro de las coberturas
        for( wvarContChild = 0; wvarContChild <= 2; wvarContChild++ )
        {
          if( wobjXMLList.item( wvarContNode ).getChildNodes().item( wvarContChild ) == (org.w3c.dom.Node) null )
          {
            wvarCodErr.set( -42 );
            wvarMensaje.set( "Falta un nodo dentro de las coberturas" );
            return fncGetNroCot;
          }
        }
      }
      //
      //Error si viene sin Coberturas
      wvarStep = 120;
      if( wvarContNode == 0 )
      {
        wvarCodErr.set( -43 );
        wvarMensaje.set( "No existen coberturas" );
        return fncGetNroCot;
      }
      //
      wobjXMLList = (org.w3c.dom.NodeList) null;
      //
      wvarStep = 130;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 140;
      wobjDBParm = new Parameter( "@WDB_CODINST", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 150;
      wobjDBParm = new Parameter( "@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 160;
      wobjDBParm = new Parameter( "@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 170;
      wobjDBParm = new Parameter( "@WDB_TIPOVIV", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPOHOGAR.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPOHOGAR)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 180;
      wobjDBParm = new Parameter( "@WDB_PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVI.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVI)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 190;
      wobjDBParm = new Parameter( "@WDB_CODPOST", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_LOCALIDADCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_LOCALIDADCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_COBROCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_COBROCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_COBROTIP.equals( "" ) ? "" : mvarWDB_COBROTIP) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 230;
      wobjDBParm = new Parameter( "@WDB_NROCOT", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 240;
      wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 250;
      wrstRes = new Recordset();
      wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
      wrstRes.setActiveConnection( (Connection) null );

      wobjXML = new diamondedge.util.XmlDom();
      //unsup wobjXML.async = false;
      wobjXML.load( System.getProperty("user.dir") + "\\" + mcteArchivoHOM_XML );

      wvarStep = 260;
      //SI NO HUBO ERROR AGREGO LOS DATOS AL XML DE ENTRADA PARA LA RESPUESTA
      if( ! (wrstRes.isEOF()) )
      {
        if( Strings.toUpperCase( Strings.trim( wrstRes.getFields().getField(0).getValue().toString() ) ).equals( "OK" ) )
        {
          if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request/TIPOOPERAC" ) */ ) ).equals( "C" ) )
          {
            /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "COT_NRO", "" ) */ );
          }
          wvarStep = 270;
          diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//COT_NRO" ) */, wobjDBCmd.getParameters().getParameter("@WDB_NROCOT").getValue().toString() );
          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CERTISEC", "" ) */ );
          diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//CERTISEC" ) */, wobjDBCmd.getParameters().getParameter("@WDB_CERTISEC").getValue().toString() );
          pvarRes.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
          wvarCodErr.set( 0 );
          wvarMensaje.set( "" );
        }
        else
        {
          //error
          wvarStep = 280;
          if( null /*unsup wobjXML.selectNodes( ("//ERRORES/HOGAR/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']") ) */.getLength() > 0 )
          {
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXML.selectSingleNode( "//ERRORES/HOGAR/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/TEXTOERROR" ) */ ) );
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXML.selectSingleNode( "//ERRORES/HOGAR/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/VALORRESPUESTA" ) */ ) );
          }
          else
          {
            wvarCodErr.set( General.mcteErrorInesperadoCod );
            wvarMensaje.set( General.mcteErrorInesperadoDescr );
          }
          //
          fncGetNroCot = false;
          return fncGetNroCot;
        }
      }
      else
      {
        //error
        wvarCodErr.set( -100 );
        wvarMensaje.set( "No se retornaron datos de la base" );
      }
      //
      wvarStep = 290;
      wobjXML = (diamondedge.util.XmlDom) null;
      // 12/2010 se cambian validaciones ch.request
      // fncGetNroCot = True
      if( invoke( "fncValidaMQ", new Variant[] { new Variant(mvarWDB_POLIZANN), new Variant(mvarWDB_POLIZSEC), new Variant(mvarWDB_COBROCOD), new Variant(mvarWDB_COBROTIP), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } ) )
      {
        fncGetNroCot = true;
      }
      else
      {
        fncGetNroCot = false;
      }
      //
      wrstRes.close();
      wobjDBCnn.close();
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return fncGetNroCot;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wvarCodErr.set( General.mcteErrorInesperadoCod );
        wvarMensaje.set( General.mcteErrorInesperadoDescr );
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        fncGetNroCot = false;
        /*unsup mobjCOM_Context.SetComplete() */;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetNroCot;
  }

  private boolean fncValidaMQ( String varPolizann, String varPolizsec, String varCobrocod, String varCobrotip, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncValidaMQ = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String mvarRequest = "";
    String mvarResponse = "";
    Variant oProd = new Variant();
    diamondedge.util.XmlDom mobjXMLDoc = null;
    Object wobjClass = null;
    String mvarProdok = "";
    org.w3c.dom.NodeList wobjXMLList = null;
    int wvarContNode = 0;


    //f ch.r
    wvarStep = 0;
    wvarCodErr.set( 0 );
    try 
    {
      // valida campa�as
      wvarStep = 2;

      mvarRequest = "<Request><DEFINICION>FormasDePagoxPolizaColectiva.xml</DEFINICION>";
      mvarRequest = mvarRequest + "<USUARCOD></USUARCOD>";
      mvarRequest = mvarRequest + "<RAMOPCOD>HOM1</RAMOPCOD>";
      mvarRequest = mvarRequest + "<POLIZANN>" + varPolizann + "</POLIZANN>";
      mvarRequest = mvarRequest + "<POLIZSEC>" + varPolizsec + "</POLIZSEC></Request>";

      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( new Variant( mvarRequest ), new Variant( mvarResponse ), "" );
      wobjClass = null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );

      mvarProdok = "ER";
      if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ == (org.w3c.dom.Node) null) )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {

          wobjXMLList = null /*unsup mobjXMLDoc.selectNodes( "//CAMPOS/T-PAGOS-SAL/FORMADEPAGO" ) */;
          for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
          {
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( "COBROCOD" ) */ ).equals( varCobrocod ) )
            {
              if( varCobrocod.equals( "4" ) )
              {
                mvarProdok = "OK";
              }
              else
              {
                if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarContNode ).selectSingleNode( "COBROTIP" ) */ ).equals( varCobrotip ) )
                {
                  mvarProdok = "OK";
                }
              }
            }
          }
        }
        else
        {
          mvarProdok = "ER";
        }
      }
      else
      {
        mvarProdok = "ER";
      }

      if( mvarProdok.equals( "ER" ) )
      {
        wvarCodErr.set( -53 );
        wvarMensaje.set( "Poliza colectiva no habilitada / Forma de Pago invalida" );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarMensaje + "\" /></Response>" );
        return fncValidaMQ;
      }


      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      wobjXMLList = (org.w3c.dom.NodeList) null;


      wvarStep = 240;
      if( wvarCodErr.toInt() == 0 )
      {
        fncValidaMQ = true;
      }
      else
      {
        fncValidaMQ = false;
      }

      fin: 
      return fncValidaMQ;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );
        wvarMensaje.set( General.mcteErrorInesperadoDescr );
        wvarCodErr.set( -1000 );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        fncValidaMQ = false;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncValidaMQ;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
