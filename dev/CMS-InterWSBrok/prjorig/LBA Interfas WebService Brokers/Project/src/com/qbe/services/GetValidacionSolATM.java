package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class GetValidacionSolATM implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetValidacionSolATM";
  static final String mcteStoreProcSelect = "SPSNCV_BRO_SELECT_OPER_ESP";
  static final String mcteStoreProcValidacion = "SPSNCV_BRO_VALIDA_SOL_ATM";
  /**
   * Archivo de Errores
   */
  static final String mcteArchivoATM_XML = "LBA_VALIDACION_SOL_ATM.XML";
  static final String mcteParamATM_XML = "LBA_PARAM_ATM.XML";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_VALOR = "VALOR";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  static final String mcteParam_CERTISEC = "CERTISEC";
  static final String mcteParam_BANELCO = "BANELCO";
  static final String mcteParam_COBROCOD = "FPAGO";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_PLANNCOD = "TPLANATM";
  static final String mcteParam_Provi = "PROVCOD";
  static final String mcteParam_LocalidadCod = "LOCALIDAD";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  static final String mcteParam_NacimAnn = "NACIMANN";
  static final String mcteParam_NacimMes = "NACIMMES";
  static final String mcteParam_NacimDia = "NACIMDIA";
  static final String mcteParam_EmisiAnn = "EMISIANN";
  static final String mcteParam_EmisiMes = "EMISIMES";
  static final String mcteParam_EmisiDia = "EMISIDIA";
  static final String mcteParam_EfectAnn = "EFECTANN";
  static final String mcteParam_EfectMes = "EFECTMES";
  static final String mcteParam_EfectDia = "EFECTDIA";
  static final String mcteParam_NumeDocu = "NUMEDOCU";
  static final String mcteParam_TipoDocu = "TIPODOCU";
  static final String mcteParam_DomicNro = "DOMICNRO";
  static final String mcteParam_DomicPso = "DOMICPSO";
  static final String mcteParam_DomicPta = "DOMICPTA";
  static final String mcteParam_TelefNro = "TELEFNRO";
  static final String mcteParam_DomicDnu = "DOMICDNU";
  static final String mcteParam_Sexo = "SEXO";
  static final String mcteParam_Estado = "ESTADO";
  static final String mcteParam_CUENNUME = "CUENNUME";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncValidaABase
   */
  private final String wcteFnName = "fncValidaABase";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant pvarRes = new Variant();
    Variant wvarCodErr = new Variant();
    //
    //
    //Declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      pvarRes.set( "" );
      wvarCodErr.set( "" );
      wvarMensaje.set( "" );
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*unsup mobjCOM_Context.SetAbort() */;
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*unsup mobjCOM_Context.SetComplete() */;
        }
        return IAction_Execute;
      }

      pvarResponse.set( pvarRes );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        IAction_Execute = 1;
        //mobjCOM_Context.SetAbort
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes ) throws Exception
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Object wobjClass = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLCheck = null;
    diamondedge.util.XmlDom wobjXMLParams = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLError = null;
    diamondedge.util.XmlDom wobjXMLStatusTarjeta = null;
    int wvarContChild = 0;
    int wvarContNode = 0;
    String wvarTarjeta = "";
    String wvarStatusTarjeta = "";
    java.util.Date wvarFechaActual = DateTime.EmptyDate;
    java.util.Date wvarFechaVigencia = DateTime.EmptyDate;







    wvarStep = 10;
    //Chequeo que los datos esten OK
    //Chequeo que no venga nada en blanco
    wobjXMLRequest = new diamondedge.util.XmlDom();
    //unsup wobjXMLRequest.async = false;
    wobjXMLRequest.loadXML( pvarRequest );

    wobjXMLCheck = new diamondedge.util.XmlDom();
    //unsup wobjXMLCheck.async = false;
    wobjXMLCheck.load( System.getProperty("user.dir") + "\\" + mcteArchivoATM_XML );
    //
    wvarStep = 20;

    wobjXMLList = null /*unsup wobjXMLCheck.selectNodes( "//ERRORES/ATM/VACIOS/CAMPOS/ERROR" ) */;
    for( wvarContChild = 0; wvarContChild <= (wobjXMLList.getLength() - 1); wvarContChild++ )
    {
      wobjXMLError = wobjXMLList.item( wvarContChild );
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLError.selectSingleNode( "CAMPO" ) *) )) ) */ == (org.w3c.dom.Node) null) )
      {
        wvarStep = 30;
        if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLError.selectSingleNode( "CAMPO" ) *) )) ) */ ) ).equals( "" ) )
        {
          wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "VALORRESPUESTA" ) */ ) );
          wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "TEXTOERROR" ) */ ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      else
      {
        //error
        wvarStep = 40;
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "VALORRESPUESTA" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "TEXTOERROR" ) */ ) );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      wobjXMLError = (org.w3c.dom.Node) null;
    }

    //Chequeo Tipo y Longitud de Datos para el XML Padre
    wvarStep = 50;

    for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().getLength() - 1); wvarContChild++ )
    {
      if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
      {
        //Si el Codigo de Cobro no es tarjeta, no chequea el Largo de Tarjeta y su Vencimiento
        if( ! ((VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) == 4)) && ((null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "CUENNUME" )) || (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "VENCIANN" )) || (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "VENCIMES" )) || (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "VENCIDIA" ))) )
        {
          //unsup GoTo Siguiente
        }

        //Chequeo la longitud y si tiene Largo Fijo
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
        {
          wvarStep = 60;
          if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") ) */ ) )) )
          {
            wvarStep = 70;
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
        else
        {
          if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") ) */ ) ) )
          {
            wvarStep = 80;
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }

        //Chequeo el Tipo
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
        {
          if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ).isNumeric()) )
          {
            wvarStep = 90;
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/ATM/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
          else
          {
            //Chequeo que el dato numerico no sea 0
            //Excluye los nodos POLIZANN
            wvarStep = 100;
            if( (Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) == 0) && ! (((null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ).getNodeName().equals( "POLIZANN" )) || (null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ).getNodeName().equals( "COT_NRO" )))) )
            {
              wvarCodErr.set( -199 );
              wvarMensaje.set( "El valor del dato numerico no puede estar en 0" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
        }
      }
      Siguiente: 
        ;
    }


    //Chequeo los datos FIJOS
    wvarStep = 280;
    //CLIENIVA
    if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/ATM/DATOS/CAMPOS/" + mcteParam_CLIENIVA + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) *) ) + "']") ) */.getLength() == 0 )
    {
      //error
      wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/ATM/DATOS/CAMPOS/" + mcteParam_CLIENIVA + "/N_ERROR" ) */ ) );
      wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/ATM/DATOS/CAMPOS/" + mcteParam_CLIENIVA + "/TEXTOERROR" ) */ ) );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }

    wvarStep = 290;
    //SEXO
    if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/ATM/DATOS/CAMPOS/" + mcteParam_Sexo + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Sexo) ) *) ) + "']") ) */.getLength() == 0 )
    {
      //error
      wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/ATM/DATOS/CAMPOS/" + mcteParam_Sexo + "/N_ERROR" ) */ ) );
      wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/ATM/DATOS/CAMPOS/" + mcteParam_Sexo + "/TEXTOERROR" ) */ ) );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }
    //
    wvarStep = 300;
    //ESTADO
    if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/ATM/DATOS/CAMPOS/" + mcteParam_Estado + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Estado) ) *) ) + "']") ) */.getLength() == 0 )
    {
      //error
      wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/ATM/DATOS/CAMPOS/" + mcteParam_Estado + "/N_ERROR" ) */ ) );
      wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/ATM/DATOS/CAMPOS/" + mcteParam_Estado + "/TEXTOERROR" ) */ ) );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }
    //
    wobjXMLCheck = (diamondedge.util.XmlDom) null;
    wobjXMLList = (org.w3c.dom.NodeList) null;
    //
    //Chequeo EDAD
    wvarStep = 410;
    if( ! (Funciones.ValidarFechayRango( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimDia ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimMes) ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimAnn) ) */ ), 17, 99, wvarMensaje )) )
    {
      //error
      wvarCodErr.set( -9 );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }

    //Valida la fecha de la Tarjeta (si Viene)
    wvarStep = 420;
    if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) == 4 )
    {
      if( ! ((null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIDIA" ) */ == (org.w3c.dom.Node) null) && (null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ == (org.w3c.dom.Node) null) && (null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ == (org.w3c.dom.Node) null)) )
      {
        if( ! (Funciones.validarFecha( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIDIA" ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ ), wvarMensaje )) )
        {
          //error
          wvarCodErr.set( -13 );
          wvarMensaje.set( "Fecha de Tarjeta Invalida" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      if( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ == (org.w3c.dom.Node) null )
      {
        wvarCodErr.set( -13 );
        wvarMensaje.set( "Fecha de Tarjeta Invalida" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      if( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ == (org.w3c.dom.Node) null )
      {
        wvarCodErr.set( -13 );
        wvarMensaje.set( "Fecha de Tarjeta Invalida" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      if( (VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ ) ) < DateTime.year( DateTime.now() )) || ((VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ ) ) == DateTime.year( DateTime.now() )) && (VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ ) ) <= DateTime.month( DateTime.now() ))) )
      {
        wvarCodErr.set( -18 );
        wvarMensaje.set( "Tarjeta de Credito vencida" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }

      //Valida el nro. de Tarjeta
      wvarStep = 430;
      wvarTarjeta = "<Request>" + "<USUARIO/>" + "<COBROTIP>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ ) + "</COBROTIP>" + "<CTANUM>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) ) */ ) + "</CTANUM>" + "<VENANN>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ ) + "</VENANN>" + "<VENMES>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ ) + "</VENMES>" + "</Request>";

      wvarStep = 440;
      wobjClass = new lbawA_OVMQCotizar.lbaw_OVValidaCuentas();
      wobjClass.Execute( wvarTarjeta, wvarStatusTarjeta, "" );
      wobjClass = null;

      wvarStep = 450;
      wobjXMLStatusTarjeta = new diamondedge.util.XmlDom();
      //unsup wobjXMLStatusTarjeta.async = false;
      wobjXMLStatusTarjeta.loadXML( wvarStatusTarjeta );

      wvarStep = 460;
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
      {
        wvarCodErr.set( -198 );
        //wvarMensaje = "Numero de Tarjeta de Cr�dito invalido"
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@mensaje" ) */ ) );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
    }

    wvarStep = 461;
    if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) == 5 )
    {
      if( !Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ ) ).equals( "DB" ) )
      {
        wvarCodErr.set( -13 );
        wvarMensaje.set( "Para CBU campo COBROTIP debe valer DB" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      //Se saca validacion de CBU
      //If wobjXMLRequest.selectSingleNode("//VENCIMES") Is Nothing Then
      //       wvarCodErr = -13
      //       wvarMensaje = "Nro. CBU incompleto (vencimes)"
      //       pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
      //       Exit Function
      //End If
      //If wobjXMLRequest.selectSingleNode("//VENCIANN") Is Nothing Then
      //       wvarCodErr = -13
      //       wvarMensaje = "Nro. CBU incompleto (venciann)"
      //       pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
      //       Exit Function
      //End If
      //Valida el nro. de CBU
      //wvarStep = 462
      //    wvarTarjeta = "<Request>" & _
      //     '              "<USUARIO/>" & _
      //              "<COBROTIP>" & UCase(wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROTIP).Text) & "</COBROTIP>" & _
      //     '              "<CTANUM>" & wobjXMLRequest.selectSingleNode("//" & mcteParam_CUENNUME).Text & "</CTANUM>" & _
      //              "<VENANN>" & wobjXMLRequest.selectSingleNode("//VENCIANN").Text & "</VENANN>" & _
      //     '              "<VENMES>" & wobjXMLRequest.selectSingleNode("//VENCIMES").Text & "</VENMES>" & _
      //              "</Request>"
      // wvarStep = 463
      // Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVMQCotizar.lbaw_OVValidaCuentas")
      // Call wobjClass.Execute(wvarTarjeta, wvarStatusTarjeta, "")
      // Set wobjClass = Nothing
      // wvarStep = 464
      // Set wobjXMLStatusTarjeta = CreateObject("MSXML2.DOMDocument")
      // wobjXMLStatusTarjeta.async = False
      // Call wobjXMLStatusTarjeta.loadXML(wvarStatusTarjeta)
      // wvarStep = 465
      // If wobjXMLStatusTarjeta.selectSingleNode("//Response/Estado/@resultado").Text = "false" Then
      //        wvarCodErr = -198
      //        wvarMensaje = wobjXMLStatusTarjeta.selectSingleNode("//Response/Estado/@mensaje").Text
      //        pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
      //        Exit Function
      // End If
    }
    //JC FIN CBU
    // No valido la fecha de vigencia porque debe ser el 1 del mes actual para ATM
    //
    wvarStep = 110;
    if( invoke( "fncValidaABase", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } ) )
    {
      fncGetAll = true;
    }
    else
    {
      fncGetAll = false;
    }

    fin: 

    return fncGetAll;

    //~~~~~~~~~~~~~~~
    ErrorHandler: 
    //~~~~~~~~~~~~~~~
    pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
    wvarCodErr.set( General.mcteErrorInesperadoCod );
    wvarMensaje.set( General.mcteErrorInesperadoDescr );

    mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

    fncGetAll = false;
    /*unsup mobjCOM_Context.SetComplete() */;
    //unsup GoTo fin
    return fncGetAll;
  }

  private boolean fncValidaABase( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncValidaABase = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    boolean wvarError = false;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLCotizacion = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXMLReturnVal = null;
    diamondedge.util.XmlDom wobjXML = null;
    diamondedge.util.XmlDom wobjXMLError = null;
    Object wobjClass = null;
    HSBC.DBConnection wobjHSBC_DBCnn = new HSBC.DBConnection();
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarContChild = 0;
    int wvarContNode = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_PROVI = "";
    String mvarWDB_LOCALIDADCOD = "";
    String mvarWDB_COBROCOD = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_TIPODOCU = "";
    boolean wvarChequeoLocalidad = false;
    boolean wvarChequeoTarjeta = false;
    String wvarMensajeStoreProc = "";
    String wvarMensajeNroCot = "";
    String pvarCot = "";

    //
    //
    //
    //
    //
    //
    //
    // 10/2010 se agrega una nueva variable para re-cotizar
    try 
    {

      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_REQUESTID = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID ) */ );
      }
      //
      wvarStep = 30;
      mvarWDB_CODINST = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }
      //
      wvarStep = 40;
      mvarWDB_PROVI = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi ) */ );
      }
      //
      wvarStep = 50;
      mvarWDB_LOCALIDADCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_LOCALIDADCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCod ) */ );
      }
      //
      wvarStep = 60;
      mvarWDB_COBROCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD ) */ );
      }
      //
      wvarStep = 70;
      mvarWDB_COBROTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP ) */ );
      }
      //
      wvarStep = 80;
      mvarWDB_TIPODOCU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoDocu) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPODOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoDocu ) */ );
      }
      //
      wvarStep = 120;
      //
      //Valido los campos de Solicitud
      //
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProcValidacion );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 130;
      wobjDBParm = new Parameter( "@WDB_CODINST", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 140;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 150;
      wobjDBParm = new Parameter( "@WDB_CODPROVCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVI.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVI)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 160;
      wobjDBParm = new Parameter( "@WDB_CODPOSTCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_LOCALIDADCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_LOCALIDADCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 170;
      wobjDBParm = new Parameter( "@WDB_COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_COBROCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_COBROCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 180;
      wobjDBParm = new Parameter( "@WDB_COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_COBROTIP.equals( "" ) ? "" : mvarWDB_COBROTIP) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 190;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP_TIT", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPODOCU.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPODOCU)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 225;
      wobjDBParm = new Parameter( "@WDB_NROCOT", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 230;
      //
      wrstRes = new Recordset();
      wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
      wrstRes.setActiveConnection( (Connection) null );
      //
      wobjXML = new diamondedge.util.XmlDom();
      //unsup wobjXML.async = false;
      wobjXML.load( System.getProperty("user.dir") + "\\" + mcteArchivoATM_XML );
      //
      //SI NO HUBO ERROR CONTINUO
      wvarStep = 240;
      if( ! (wrstRes.isEOF()) )
      {
        if( ! (Strings.toUpperCase( Strings.trim( wrstRes.getFields().getField(0).getValue().toString() ) ).equals( "OK" )) )
        {
          //error
          if( null /*unsup wobjXML.selectNodes( ("//ERRORES/ATM/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']") ) */.getLength() > 0 )
          {
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXML.selectSingleNode( "//ERRORES/ATM/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/TEXTOERROR" ) */ ) );
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXML.selectSingleNode( "//ERRORES/ATM/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/VALORRESPUESTA" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          }
          else
          {
            wvarCodErr.set( wrstRes.getFields().getField(0).getValue() );
            wvarMensaje.set( "No existe descripcion para este error" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          }
          //
          fncValidaABase = false;
          return fncValidaABase;
        }
        else
        {
        }
      }
      else
      {
        //error
        wvarCodErr.set( -60 );
        wvarMensaje.set( "No se retornaron datos de la base" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncValidaABase;
      }
      //
      wobjXML = (diamondedge.util.XmlDom) null;
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = (HSBC.DBConnection) null;
      //
      wvarStep = 250;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      wobjDBCnn = (Connection) (Connection)( wobjHSBC_DBCnn.GetDBConnection( General.mcteDB ) );
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProcSelect );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      //Comparo con el XML guardado de Cotizacion
      //Busco el registro guardado de la cotizacion
      wvarStep = 270;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 280;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_REQUESTID)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 290;
      wobjDBParm = new Parameter( "@WDB_TIPO_OPERACION", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "C" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 300;
      wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "OK" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 310;
      wrstRes = new Recordset();
      wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
      //
      wvarStep = 320;
      wrstRes.setActiveConnection( (Connection) null );
      //
      //Cargo el campo de XML de cotizacion
      wvarStep = 330;
      wobjXMLCotizacion = new diamondedge.util.XmlDom();
      //unsup wobjXMLCotizacion.async = false;
      wobjXMLCotizacion.loadXML( wrstRes.getFields().getField(14).getValue().toString() );
      //
      //no compara el Codigo Postal
      wvarStep = 340;
      wvarChequeoLocalidad = true;

      //Si es COBROCOD = 4 no compara el Tipo de Tarjeta
      wvarStep = 350;
      if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) != 4 )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ ) ) )
        {
          wvarChequeoTarjeta = true;
        }
        else
        {
          wvarChequeoTarjeta = false;
        }
      }
      else
      {
        wvarChequeoTarjeta = true;
      }

      wvarStep = 360;
      if( ! ((diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ ) )) && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ ) )) && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) )) && wvarChequeoTarjeta && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PLANNCOD) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_PLANNCOD) ) */ ) )) && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ) ))) )
      {
        //If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_Provi).Text = wobjXMLCotizacion.selectSingleNode("//" & mcteParam_Provi).Text Then
        //    wvarCodErr = -163
        //    wvarMensaje = "Cod. de Provincia: existe una diferencia entre la Cotizacion y la Solicitud"
        //End If
        //If Not wvarChequeoLocalidad Then
        //    wvarCodErr = -164
        //    wvarMensaje = "Cod. de Localidad: existe una diferencia entre la Cotizacion y la Solicitud"
        //End If
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) )) )
        {
          wvarCodErr.set( -167 );
          wvarMensaje.set( "Cod. de Cobro: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (wvarChequeoTarjeta) )
        {
          wvarCodErr.set( -168 );
          wvarMensaje.set( "Tipo de cobro: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ) )) )
        {
          wvarCodErr.set( -169 );
          wvarMensaje.set( "Responsabilidad frente al IVA: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ ) )) )
        {
          wvarCodErr.set( -250 );
          wvarMensaje.set( "Poliza Anual: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ ) )) )
        {
          wvarCodErr.set( -251 );
          wvarMensaje.set( "Poliza Colectiva: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PLANNCOD) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_PLANNCOD) ) */ ) )) )
        {
          wvarCodErr.set( -253 );
          wvarMensaje.set( "Cod. de Plan: existe una diferencia entre la Cotizacion y la Solicitud" );
        }


        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        fncValidaABase = false;
        return fncValidaABase;

      }
      //
      //Le agrego el Nodo de CERTISEC, y los encabezados para mandar el XML a cotizar
      wvarStep = 380;
      /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CERTISEC", "" ) */ );
      pvarRequest = "<LBA_WS res_code=\"0\" res_msg=\"\">" + wobjXMLRequest.getDocument().getDocumentElement().toString() + "</LBA_WS>";
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( pvarRequest );

      //Cotizo nuevamente
      wvarStep = 385;
      pvarCot = wobjXMLCotizacion.getDocument().getDocumentElement().toString();

      wobjClass = new LBA_InterWSBrok.GetCotizacionATM();
      wobjClass.Execute( pvarCot, wvarMensajeStoreProc, "" );

      wobjClass = null;
      //
      wobjXMLReturnVal = new diamondedge.util.XmlDom();
      //unsup wobjXMLReturnVal.async = false;
      wobjXMLReturnVal.loadXML( wvarMensajeStoreProc );

      if( ! (Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" ) */ ) ) == 0) )
      {
        pvarRes.set( "<LBA_WS res_code=\"-300\" res_msg=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) + "\"></LBA_WS>" );
        return fncValidaABase;
      }
      else
      {
        wvarCodErr.set( "0" );
        wvarMensaje.set( "" );
      }


      wvarStep = 420;
      if( ! (VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VALOR) ) */ ) ) == VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//PRECIO" ) */ ) )) )
      {
        wvarCodErr.set( -97 );
        wvarMensaje.set( "Precio: existe una diferencia entre la COTIZACION y la SOLICITUD" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );

      }
      //
      wvarStep = 430;
      /*unsup wobjXMLResponse.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLReturnVal.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CODZONA", "" ) */ );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLResponse.selectSingleNode( "//CODZONA" ) */, "0" );

      //
      wvarStep = 440;
      /*unsup wobjXMLResponse.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLResponse.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "TIEMPOPROCESO", "" ) */ );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLResponse.selectSingleNode( "//TIEMPOPROCESO" ) */, "0" );

      //
      //
      wvarStep = 440;
      /*unsup wobjXMLResponse.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLResponse.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "USUARCOD", "" ) */ );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLResponse.selectSingleNode( "//USUARCOD" ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//USUARCOD" ) */ ) );

      //
      pvarRes.set( wobjXMLResponse.getDocument().getDocumentElement().toString() );

      fncValidaABase = true;
      //
      wrstRes.close();
      wobjDBCnn.close();

      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = (HSBC.DBConnection) null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return fncValidaABase;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );
        wvarMensaje.set( General.mcteErrorInesperadoDescr );

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        fncValidaABase = false;
        /*unsup mobjCOM_Context.SetComplete() */;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncValidaABase;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
