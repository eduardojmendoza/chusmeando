package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class GetValidacionSolHO implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetValidacionSolHO";
  static final String mcteStoreProcSelect = "SPSNCV_BRO_SELECT_OPER_ESP";
  static final String mcteStoreProcValidacion = "SPSNCV_BRO_VALIDA_SOL_HOM";
  /**
   * Archivo de Errores
   */
  static final String mcteArchivoHOM_XML = "LBA_VALIDACION_SOL_HO.XML";
  static final String mcteParamHOM_XML = "LBA_PARAM_HO.XML";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_VALOR = "VALOR";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  static final String mcteParam_CERTISEC = "CERTISEC";
  /**
   * 11/2010 se cambia nombre nodos al actualizar mensaje ais
   * Const mcteParam_COBROCOD             As String = "COBROCOD"
   */
  static final String mcteParam_COBROCOD = "FPAGO";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_TIPOHOGAR = "TIPOHOGAR";
  /**
   * Const mcteParam_PLANNCOD             As String = "PLANNCOD"
   */
  static final String mcteParam_PLANNCOD = "PLAN";
  /**
   * Const mcteParam_Provi                As String = "PROVI"
   */
  static final String mcteParam_Provi = "PROVCOD";
  /**
   * Const mcteParam_LocalidadCod         As String = "LOCALIDADCOD"
   */
  static final String mcteParam_LocalidadCod = "LOCALIDAD";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  static final String mcteParam_NacimAnn = "NACIMANN";
  static final String mcteParam_NacimMes = "NACIMMES";
  static final String mcteParam_NacimDia = "NACIMDIA";
  static final String mcteParam_EmisiAnn = "EMISIANN";
  static final String mcteParam_EmisiMes = "EMISIMES";
  static final String mcteParam_EmisiDia = "EMISIDIA";
  static final String mcteParam_EfectAnn = "EFECTANN";
  static final String mcteParam_EfectMes = "EFECTMES";
  static final String mcteParam_EfectDia = "EFECTDIA";
  static final String mcteParam_NumeDocu = "NUMEDOCU";
  static final String mcteParam_TipoDocu = "TIPODOCU";
  static final String mcteParam_UsoTipos = "USOTIPOS";
  static final String mcteParam_CALDERA = "CALDERA";
  static final String mcteParam_ASCENSOR = "ASCENSOR";
  static final String mcteParam_ALARMTIP = "ALARMTIP";
  static final String mcteParam_GUARDTIP = "GUARDTIP";
  static final String mcteParam_REJAS = "REJAS";
  static final String mcteParam_PUERTABLIND = "PUERTABLIND";
  static final String mcteParam_DISYUNTOR = "DISYUNTOR";
  static final String mcteParam_DomicNro = "DOMICNRO";
  static final String mcteParam_DomicPso = "DOMICPSO";
  static final String mcteParam_DomicPta = "DOMICPTA";
  static final String mcteParam_TelefNro = "TELEFNRO";
  static final String mcteParam_DomicDnu = "DOMICDNU";
  static final String mcteParam_Sexo = "SEXO";
  static final String mcteParam_Estado = "ESTADO";
  static final String mcteParam_CUENNUME = "CUENNUME";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS";
  /**
   * DATOS DE LAS CAMPA�AS
   */
  static final String mcteNodos_Campa = "//Request/CAMPANIAS";
  /**
   * DATOS DE LOS ELECTRODOMESTICOS
   */
  static final String mcteNodos_Elect = "//Request/ELECTRODOMESTICOS/ELECTRODOMESTICO";
  static final String mcteParam_Tipo = "TIPO";
  static final String mcteParam_Marca = "MARCA";
  static final String mcteParam_Modelo = "MODELO";
  /**
   * AM
   */
  static final String mcteParam_EmpresaCodigo = "//CANAL";
  static final String mcteParam_SucursalCodigo = "//CANAL_SUCURSAL";
  static final String mcteParam_LegajoVend = "//LEGAJO_VEND";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncValidaABase
   */
  private final String wcteFnName = "fncValidaABase";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant pvarRes = new Variant();
    Variant wvarCodErr = new Variant();
    //
    //
    //Declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      pvarRes.set( "" );
      wvarCodErr.set( "" );
      wvarMensaje.set( "" );
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*unsup mobjCOM_Context.SetAbort() */;
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*unsup mobjCOM_Context.SetComplete() */;
        }
        return IAction_Execute;
      }

      pvarResponse.set( pvarRes );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        IAction_Execute = 1;
        //mobjCOM_Context.SetAbort
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes ) throws Exception
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    lbawA_OVMQGen.lbaw_MQMensaje wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLRes = null;
    diamondedge.util.XmlDom wobjXMLCheck = null;
    diamondedge.util.XmlDom wobjXMLParams = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.NodeList wobjXMLNodeList = null;
    org.w3c.dom.Node wobjXMLError = null;
    diamondedge.util.XmlDom wobjXMLStatusTarjeta = null;
    diamondedge.util.XmlDom wobjXMLStatusCanal = null;
    org.w3c.dom.Node wobjXMLNodeSucu = null;
    int wvarContChild = 0;
    int wvarContNode = 0;
    int wvarcounter = 0;
    String wvarTarjeta = "";
    String wvarStatusTarjeta = "";
    String wvarBancoCod = "";
    String mvarRequest = "";
    String wvarResponse = "";
    String wvarExisteSucu = "";
    String mvarWDB_USUARCOD = "";
    java.util.Date wvarFechaActual = DateTime.EmptyDate;
    java.util.Date wvarFechaVigencia = DateTime.EmptyDate;







    wvarStep = 10;
    //Chequeo que los datos esten OK
    //Chequeo que no venga nada en blanco
    wobjXMLRequest = new diamondedge.util.XmlDom();
    //unsup wobjXMLRequest.async = false;
    wobjXMLRequest.loadXML( pvarRequest );

    wobjXMLCheck = new diamondedge.util.XmlDom();
    //unsup wobjXMLCheck.async = false;
    wobjXMLCheck.load( System.getProperty("user.dir") + "\\" + mcteArchivoHOM_XML );
    //
    wvarStep = 20;

    wobjXMLList = null /*unsup wobjXMLCheck.selectNodes( "//ERRORES/HOGAR/VACIOS/CAMPOS/ERROR" ) */;
    for( wvarContChild = 0; wvarContChild <= (wobjXMLList.getLength() - 1); wvarContChild++ )
    {
      wobjXMLError = wobjXMLList.item( wvarContChild );
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLError.selectSingleNode( "CAMPO" ) *) )) ) */ == (org.w3c.dom.Node) null) )
      {
        wvarStep = 30;
        if( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLError.selectSingleNode( "CAMPO" ) *) )) ) */ ) ).equals( "" ) )
        {
          wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "VALORRESPUESTA" ) */ ) );
          wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "TEXTOERROR" ) */ ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      else
      {
        //error
        wvarStep = 40;
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "VALORRESPUESTA" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( "TEXTOERROR" ) */ ) );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      wobjXMLError = (org.w3c.dom.Node) null;
    }

    //Chequeo Tipo y Longitud de Datos para el XML Padre
    wvarStep = 50;

    for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().getLength() - 1); wvarContChild++ )
    {
      if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
      {
        //Si el Codigo de Cobro no es tarjeta, no chequea el Largo de Tarjeta y su Vencimiento
        if( ! ((VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) == 4)) && ((null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "CUENNUME" )) || (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "VENCIANN" )) || (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "VENCIMES" )) || (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName()) ) */.getNodeName().equals( "VENCIDIA" ))) )
        {
          //unsup GoTo Siguiente
        }

        //Chequeo la longitud y si tiene Largo Fijo
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
        {
          wvarStep = 60;
          if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") ) */ ) )) )
          {
            wvarStep = 70;
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }
        else
        {
          if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") ) */ ) ) )
          {
            wvarStep = 80;
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
        }

        //Chequeo el Tipo
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
        {
          if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ).isNumeric()) )
          {
            wvarStep = 90;
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectSingleNode( "//Request" ) *).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }
          else
          {
            //Chequeo que el dato numerico no sea 0
            //Excluye los nodos POLIZANN
            wvarStep = 100;
            if( (Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ) ) ) == 0) && ! (((null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ).getNodeName().equals( "POLIZANN" )) || (null /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.getChildNodes().item( wvarContChild ).getNodeName().equals( "COT_NRO" )))) )
            {
              wvarCodErr.set( -199 );
              wvarMensaje.set( "El valor del dato numerico no puede estar en 0" );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
        }
      }
      Siguiente: 
        ;
    }

    //Chequeo Tipo y Longitud de Datos para Coberturas
    for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectNodes( (mcteNodos_Cober + "/COBERTURA") ) */.getLength() - 1); wvarContChild++ )
    {
      wvarStep = 110;
      for( wvarContNode = 0; wvarContNode <= (null /*unsup wobjXMLRequest.selectNodes( (mcteNodos_Cober + "/COBERTURA") ) */.item( wvarContChild ).getChildNodes().getLength() - 1); wvarContNode++ )
      {
        wvarStep = 120;
        if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober + "/COBERTURA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
        {
          //Chequeo la longitud y si tiene Largo Fijo
          wvarStep = 130;
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober + "/COBERTURA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
          {
            if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( (mcteNodos_Cober + "/COBERTURA") ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober + "/COBERTURA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) )) )
            {
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober + "/COBERTURA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober + "/COBERTURA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
          else
          {
            wvarStep = 140;
            if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( (mcteNodos_Cober + "/COBERTURA") ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober + "/COBERTURA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) ) )
            {
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober + "/COBERTURA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober + "/COBERTURA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }

          //Chequeo el Tipo
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober + "/COBERTURA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
          {
            wvarStep = 150;
            if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober + "/COBERTURA" ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ).isNumeric()) )
            {
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober + "/COBERTURA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober + "/COBERTURA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
            else
            {
              //Chequeo que el dato numerico no sea 0
              wvarStep = 160;
              if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( (mcteNodos_Cober + "/COBERTURA") ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == 0 )
              {
                wvarCodErr.set( -199 );
                wvarMensaje.set( "El valor del dato numerico no puede estar en 0" );
                pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
                return fncGetAll;
              }
            }
          }
        }
      }
    }


    //Chequeo Tipo y Longitud de Datos para Campa�as
    for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectNodes( (mcteNodos_Campa + "/CAMPANIA") ) */.getLength() - 1); wvarContChild++ )
    {
      wvarStep = 170;
      for( wvarContNode = 0; wvarContNode <= (null /*unsup wobjXMLRequest.selectNodes( (mcteNodos_Campa + "/CAMPANIA") ) */.item( wvarContChild ).getChildNodes().getLength() - 1); wvarContNode++ )
      {
        if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa + "/CAMPANIA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
        {
          //Chequeo la longitud y si tiene Largo Fijo
          wvarStep = 180;
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa + "/CAMPANIA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
          {
            if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( (mcteNodos_Campa + "/CAMPANIA") ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa + "/CAMPANIA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) )) )
            {
              wvarStep = 190;
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa + "/CAMPANIA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa + "/CAMPANIA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
          else
          {
            wvarStep = 200;
            if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( (mcteNodos_Campa + "/CAMPANIA") ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa + "/CAMPANIA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) ) )
            {
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa + "/CAMPANIA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa + "/CAMPANIA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }

          //Chequeo el Tipo
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa + "/CAMPANIA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
          {
            if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa + "/CAMPANIA" ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ).isNumeric()) )
            {
              wvarStep = 210;
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa + "/CAMPANIA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa + "/CAMPANIA" ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
        }
      }
    }

    //Chequeo Tipo y Longitud de Datos para Electrodomesticos
    for( wvarContChild = 0; wvarContChild <= (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) */.getLength() - 1); wvarContChild++ )
    {
      wvarStep = 220;
      for( wvarContNode = 0; wvarContNode <= (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) */.item( wvarContChild ).getChildNodes().getLength() - 1); wvarContNode++ )
      {
        if( ! (null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName()) ) */ == (org.w3c.dom.Node) null) )
        {
          //Chequeo la longitud y si tiene Largo Fijo
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONG_VARIABLE") ) */ ).equals( "N" ) )
          {
            wvarStep = 230;
            if( ! (Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) )) )
            {
              wvarStep = 240;
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
          else
          {
            wvarStep = 250;
            if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) > Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") ) */ ) ) )
            {
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }

          //Chequeo el Tipo
          if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( ("//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TIPO") ) */ ).equals( "NUMERICO" ) )
          {
            wvarStep = 260;
            if( ! (new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) */.item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ).isNumeric()) )
            {
              wvarStep = 270;
              wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) */ ) );
              wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/TIPODEDATO/" + null (*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) *).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) */ ) );
              pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
              return fncGetAll;
            }
          }
        }
      }
    }



    //Chequeo los datos FIJOS
    wvarStep = 280;
    //CLIENIVA
    if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_CLIENIVA + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) *) ) + "']") ) */.getLength() == 0 )
    {
      //error
      wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_CLIENIVA + "/N_ERROR" ) */ ) );
      wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_CLIENIVA + "/TEXTOERROR" ) */ ) );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }

    wvarStep = 290;
    //SEXO
    if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_Sexo + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Sexo) ) *) ) + "']") ) */.getLength() == 0 )
    {
      //error
      wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_Sexo + "/N_ERROR" ) */ ) );
      wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_Sexo + "/TEXTOERROR" ) */ ) );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }
    //
    wvarStep = 300;
    //ESTADO
    if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_Estado + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Estado) ) *) ) + "']") ) */.getLength() == 0 )
    {
      //error
      wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_Estado + "/N_ERROR" ) */ ) );
      wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_Estado + "/TEXTOERROR" ) */ ) );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }

    wvarStep = 310;
    //REJAS
    if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_REJAS + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REJAS) ) *) ) + "']") ) */.getLength() == 0 )
    {
      //error
      wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_REJAS + "/N_ERROR" ) */ ) );
      wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_REJAS + "/TEXTOERROR" ) */ ) );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }
    //
    wvarStep = 320;
    //PUERTA BLINDADA
    if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_PUERTABLIND + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PUERTABLIND) ) *) ) + "']") ) */.getLength() == 0 )
    {
      //error
      wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_PUERTABLIND + "/N_ERROR" ) */ ) );
      wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_PUERTABLIND + "/TEXTOERROR" ) */ ) );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }
    //
    //Cheque que vengan hasta 50 coberturas
    wvarStep = 330;
    wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober + "/COBERTURA" ) */;
    if( wobjXMLList.getLength() > 50 )
    {
      wvarCodErr.set( -48 );
      wvarMensaje.set( "Existen mas coberturas de las permitidas" );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }

    //Chequeo que no falte un nodo en las coberturas
    wvarStep = 340;
    for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
    {
      //Por si falta un nodo dentro de las coberturas
      for( wvarContChild = 0; wvarContChild <= 2; wvarContChild++ )
      {
        if( wobjXMLList.item( wvarContNode ).getChildNodes().item( wvarContChild ) == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -42 );
          wvarMensaje.set( "Falta un nodo dentro de las coberturas" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
    }
    //
    //Error si viene sin Coberturas
    wvarStep = 350;
    if( wvarContNode == 0 )
    {
      wvarCodErr.set( -43 );
      wvarMensaje.set( "No existen coberturas" );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }

    wobjXMLList = (org.w3c.dom.NodeList) null;

    //Cheque que vengan hasta 10 campanias
    wvarStep = 360;
    wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa + "/CAMPANIA" ) */;
    if( wobjXMLList.getLength() > 10 )
    {
      wvarCodErr.set( -49 );
      wvarMensaje.set( "Existen mas campanias de las permitidas" );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }

    //Chequeo que no vengan las campanias en blanco
    wvarStep = 370;
    for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
    {
      if( wobjXMLList.item( wvarContNode ).getChildNodes().item( 0 ) == (org.w3c.dom.Node) null )
      {
        //error
        wvarCodErr.set( -1 );
        wvarMensaje.set( "Falta el dato de la Campania" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
    }
    wobjXMLList = (org.w3c.dom.NodeList) null;

    //Chequeo que vengan hasta 10 electrodomesticos
    wvarStep = 380;
    wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) */;
    if( wobjXMLList.getLength() > 10 )
    {
      wvarCodErr.set( -46 );
      wvarMensaje.set( "Existen mas electrodomesticos de los permitidos" );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }
    //
    //Chequeo que no vengan los electrodomesticos en blanco ni mal el codigo de TIPO
    wvarStep = 390;
    for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
    {
      wobjXMLError = (org.w3c.dom.Node) null;
      wobjXMLError = wobjXMLList.item( wvarContNode );
      if( null /*unsup wobjXMLError.selectSingleNode( mcteParam_Tipo ) */ == (org.w3c.dom.Node) null )
      {
        wvarCodErr.set( -45 );
        wvarMensaje.set( "Falta el Tipo de Electrodomestico" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      else
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( mcteParam_Tipo ) */ ).equals( "" ) )
        {
          wvarCodErr.set( -45 );
          wvarMensaje.set( "Falta el Tipo de Electrodomestico" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      if( null /*unsup wobjXMLError.selectSingleNode( mcteParam_Marca ) */ == (org.w3c.dom.Node) null )
      {
        wvarCodErr.set( -47 );
        wvarMensaje.set( "Falta la Marca del Electrodomestico" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      else
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLError.selectSingleNode( mcteParam_Marca ) */ ).equals( "" ) )
        {
          wvarCodErr.set( -47 );
          wvarMensaje.set( "Falta la Marca del Electrodomestico" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      if( null /*unsup wobjXMLCheck.selectNodes( ("//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_Tipo + "/VALOR[.='" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLError.selectSingleNode( mcteParam_Tipo ) *) ) + "']") ) */.getLength() == 0 )
      {
        //error
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_Tipo + "/N_ERROR" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCheck.selectSingleNode( "//ERRORES/HOGAR/DATOS/CAMPOS/" + mcteParam_Tipo + "/TEXTOERROR" ) */ ) );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      wobjXMLError = (org.w3c.dom.Node) null;
    }
    //
    wobjXMLCheck = (diamondedge.util.XmlDom) null;
    wobjXMLList = (org.w3c.dom.NodeList) null;
    //
    //Chequeo que no falte ningun nodo en electrodomesticos
    wvarStep = 400;

    //Chequeo EDAD
    //  wvarStep = 410
    //  If Not ValidarFechayRango(wobjXMLRequest.selectSingleNode("//" & mcteParam_NacimDia).Text & _
    // '                        "/" & wobjXMLRequest.selectSingleNode("//" & mcteParam_NacimMes).Text & _
    //                        "/" & wobjXMLRequest.selectSingleNode("//" & mcteParam_NacimAnn).Text, 17, 86, wvarMensaje) Then
    //    'error
    //      wvarCodErr = -9
    //      pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
    //      Exit Function
    //  End If
    //Valida la fecha de la Tarjeta (si Viene)
    wvarStep = 420;
    if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) == 4 )
    {
      if( ! ((null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIDIA" ) */ == (org.w3c.dom.Node) null) && (null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ == (org.w3c.dom.Node) null) && (null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ == (org.w3c.dom.Node) null)) )
      {
        if( ! (Funciones.validarFecha( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIDIA" ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ ), wvarMensaje )) )
        {
          //error
          wvarCodErr.set( -13 );
          wvarMensaje.set( "Fecha de Tarjeta Invalida" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
      }
      if( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ == (org.w3c.dom.Node) null )
      {
        wvarCodErr.set( -13 );
        wvarMensaje.set( "Fecha de Tarjeta Invalida" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      if( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ == (org.w3c.dom.Node) null )
      {
        wvarCodErr.set( -13 );
        wvarMensaje.set( "Fecha de Tarjeta Invalida" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      if( (VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ ) ) < DateTime.year( DateTime.now() )) || ((VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ ) ) == DateTime.year( DateTime.now() )) && (VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ ) ) <= DateTime.month( DateTime.now() ))) )
      {
        wvarCodErr.set( -18 );
        wvarMensaje.set( "Tarjeta de Credito vencida" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }

      //Valida el nro. de Tarjeta
      wvarStep = 430;
      // 10/2010 se reemplaza componente
      //wvarTarjeta = "<Request>" & _
      //     '              "<TARJETACOD>" & wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROTIP).Text & "</TARJETACOD>" & _
      //              "<TARJETANUM>" & wobjXMLRequest.selectSingleNode("//" & mcteParam_CUENNUME).Text & "</TARJETANUM>" & _
      //     '              "</Request>"
      wvarTarjeta = "<Request>" + "<USUARIO/>" + "<COBROTIP>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ ) + "</COBROTIP>" + "<CTANUM>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) ) */ ) + "</CTANUM>" + "<VENANN>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ ) + "</VENANN>" + "<VENMES>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ ) + "</VENMES>" + "</Request>";

      wvarStep = 440;
      //Set wobjClass = mobjCOM_Context.CreateInstance("Valida_Tarjetas.GetSeleccionTarjeta")
      wobjClass = new lbawA_OVMQCotizar.lbaw_OVValidaCuentas();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarTarjeta, wvarStatusTarjeta, "")
      wobjClass = (lbawA_OVMQGen.lbaw_MQMensaje) null;

      wvarStep = 450;
      wobjXMLStatusTarjeta = new diamondedge.util.XmlDom();
      //unsup wobjXMLStatusTarjeta.async = false;
      wobjXMLStatusTarjeta.loadXML( wvarStatusTarjeta );

      wvarStep = 460;
      //If Not UCase(wobjXMLStatusTarjeta.selectSingleNode("//VERIFICACION").Text) = "TRUE" Then
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
      {
        wvarCodErr.set( -198 );
        //wvarMensaje = "Numero de Tarjeta de Cr�dito invalido"
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@mensaje" ) */ ) );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
    }
    //10/2010 se agrega CBU
    //El nro. de CBU(22) se conforma asi en cuennume(0,16) en  VENCIMES(16,2) y VENCIANN(18, 4)
    wvarStep = 461;
    if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) == 5 )
    {
      if( !Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ ) ).equals( "DB" ) )
      {
        wvarCodErr.set( -13 );
        wvarMensaje.set( "Para CBU campo COBROTIP debe valer DB" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      if( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ == (org.w3c.dom.Node) null )
      {
        wvarCodErr.set( -13 );
        wvarMensaje.set( "Nro. CBU incompleto (vencimes)" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      if( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ == (org.w3c.dom.Node) null )
      {
        wvarCodErr.set( -13 );
        wvarMensaje.set( "Nro. CBU incompleto (venciann)" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
      //Valida el nro. de CBU
      wvarStep = 462;
      wvarTarjeta = "<Request>" + "<USUARIO/>" + "<COBROTIP>" + Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ ) ) + "</COBROTIP>" + "<CTANUM>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) ) */ ) + "</CTANUM>" + "<VENANN>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIANN" ) */ ) + "</VENANN>" + "<VENMES>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VENCIMES" ) */ ) + "</VENMES>" + "</Request>";

      wvarStep = 463;
      wobjClass = new lbawA_OVMQCotizar.lbaw_OVValidaCuentas();
      wobjClass.Execute( wvarTarjeta, wvarStatusTarjeta, "" );
      wobjClass = (lbawA_OVMQGen.lbaw_MQMensaje) null;

      wvarStep = 464;
      wobjXMLStatusTarjeta = new diamondedge.util.XmlDom();
      //unsup wobjXMLStatusTarjeta.async = false;
      wobjXMLStatusTarjeta.loadXML( wvarStatusTarjeta );

      wvarStep = 465;
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
      {
        wvarCodErr.set( -198 );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusTarjeta.selectSingleNode( "//Response/Estado/@mensaje" ) */ ) );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncGetAll;
      }
    }
    //JC FIN CBU
    //Valida la fecha de Vigencia
    wvarStep = 470;
    if( ! (Funciones.validarFecha( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENDIA" ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENMES" ) */ ) + "/" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENANN" ) */ ), wvarMensaje )) )
    {
      //error
      wvarCodErr.set( -15 );
      wvarMensaje.set( "Fecha de Vigencia Invalida" );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;

    }

    //Chequeo que la fecha de Vigencia sea mayor a hoy
    wvarStep = 480;
    wvarFechaActual = DateTime.dateSerial( DateTime.year( DateTime.now() ), DateTime.month( DateTime.now() ), DateTime.day( DateTime.now() ) );
    wvarFechaVigencia = DateTime.dateSerial( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENANN" ) */ ) ), Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENMES" ) */ ) ), Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENDIA" ) */ ) ) );

    if( wvarFechaVigencia.compareTo( wvarFechaActual ) < 0 )
    {
      wvarCodErr.set( -197 );
      wvarMensaje.set( "Fecha de Vigencia anterior a la fecha actual" );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;
    }
    //Chequeo que la fecha no sea mayor a 30 d�as a partir de hoy
    wvarStep = 490;
    if( wvarFechaVigencia.compareTo( DateTime.add( wvarFechaActual, 30 ) ) > 0 )
    {
      wvarCodErr.set( -196 );
      wvarMensaje.set( "Fecha de Vigencia posterior a 30 dias de la fecha actual" );
      pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
      return fncGetAll;

    }
    //
    //Cheque que si la forma de Pago es 4 (Tarjeta) venga el Nro. de Cuenta y Vto.
    wobjXMLParams = new diamondedge.util.XmlDom();
    //unsup wobjXMLParams.async = false;
    wobjXMLParams.load( System.getProperty("user.dir") + "\\" + mcteParamHOM_XML );
    //
    //
    wvarStep = 110;
    if( invoke( "fncValidaABase", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } ) )
    {
      //AM Valido CANAL en caso de que se informe
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( "//CANAL" ) */ == (org.w3c.dom.Node) null) )
      {

        // Obtengo el USUARCOD de la respuesta de la BD
        wvarStep = 111;
        wobjXMLRes = new diamondedge.util.XmlDom();
        //unsup wobjXMLRes.async = false;
        wobjXMLRes.loadXML( pvarRes.toString() );
        //
        wvarStep = 112;
        mvarWDB_USUARCOD = "";
        if( ! (null /*unsup wobjXMLRes.selectSingleNode( "//USUARCOD" ) */ == (org.w3c.dom.Node) null) )
        {
          mvarWDB_USUARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRes.selectSingleNode( "//USUARCOD" ) */ );
        }
        //
        // Si se informa el CANAL valido los otros dos campos
        if( null /*unsup wobjXMLRequest.selectSingleNode( "//CANAL_SUCURSAL" ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -14 );
          wvarMensaje.set( "Debe informar Canal Sucursal" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        if( null /*unsup wobjXMLRequest.selectSingleNode( "//LEGAJO_VEND" ) */ == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( -14 );
          wvarMensaje.set( "Debe informar Legajo Vendedor" );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }

        //AM Valido el CANAL Msg1000
        wvarStep = 921;
        mvarRequest = "<Request><DEFINICION>1000_PerfilUsuario.xml</DEFINICION>";
        mvarRequest = mvarRequest + "<AplicarXSL>1000_PerfilUsuario.xsl</AplicarXSL>";
        mvarRequest = mvarRequest + "<USUARIO>" + Strings.toUpperCase( mvarWDB_USUARCOD ) + "</USUARIO></Request>";


        wvarStep = 922;
        wobjClass = new lbawA_OVMQGen.lbaw_MQMensajeGestion();
        wobjClass.Execute( mvarRequest, wvarResponse, "" );
        wobjClass = (lbawA_OVMQGen.lbaw_MQMensaje) null;

        wvarStep = 923;
        wobjXMLStatusCanal = new diamondedge.util.XmlDom();
        //unsup wobjXMLStatusCanal.async = false;
        wobjXMLStatusCanal.loadXML( wvarResponse );

        wvarStep = 924;
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusCanal.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
        {
          wvarCodErr.set( -198 );
          wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusCanal.selectSingleNode( "//Response/Estado/@mensaje" ) */ ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          wvarBancoCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusCanal.selectSingleNode( "//BANCOCOD" ) */ );
          if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EmpresaCodigo ) */ ).equals( wvarBancoCod )) )
          {
            wvarCodErr.set( -399 );
            wvarMensaje.set( "Nro. de Canal incorrecto para el Broker" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }

        }
        // Fin valido CANAL
        //AM Valido el CANAL_SUCURSAL Msg1018
        wvarStep = 925;
        mvarRequest = "<Request><DEFINICION>1018_ListadoSucursalesxBanco.xml</DEFINICION>";
        mvarRequest = mvarRequest + "<BANCOCOD>" + wvarBancoCod + "</BANCOCOD></Request>";

        wvarStep = 926;
        wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
        wobjClass.Execute( mvarRequest, wvarResponse, "" );
        wobjClass = (lbawA_OVMQGen.lbaw_MQMensaje) null;

        wvarStep = 927;
        wobjXMLStatusCanal = new diamondedge.util.XmlDom();
        //unsup wobjXMLStatusCanal.async = false;
        wobjXMLStatusCanal.loadXML( wvarResponse );

        wvarStep = 928;
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusCanal.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
        {
          wvarCodErr.set( -198 );
          wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLStatusCanal.selectSingleNode( "//Response/Estado/@mensaje" ) */ ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          return fncGetAll;
        }
        else
        {
          wvarStep = 929;
          wobjXMLNodeList = null /*unsup wobjXMLStatusCanal.selectNodes( "//Response/CAMPOS/SUCURSALES/SUCU" ) */;
          wvarExisteSucu = "N";

          for( wvarcounter = 0; wvarcounter <= (wobjXMLNodeList.getLength() - 1); wvarcounter++ )
          {
            wvarStep = 931;
            if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SucursalCodigo ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNodeList.item( wvarcounter ).selectSingleNode( "CODISUCU" ) */ ) ) )
            {
              wvarExisteSucu = "S";
            }
          }
          wvarStep = 932;
          if( wvarExisteSucu.equals( "N" ) )
          {
            wvarCodErr.set( -399 );
            wvarMensaje.set( "Nro. de Sucursal invalido para el Broker" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
            return fncGetAll;
          }

        }
        // Fin valido CANAL_SUCURSAL
      }
      fncGetAll = true;
    }
    else
    {
      fncGetAll = false;
    }

    fin: 

    return fncGetAll;

    //~~~~~~~~~~~~~~~
    ErrorHandler: 
    //~~~~~~~~~~~~~~~
    //pvarRes = "<Response><Estado resultado=""false"" mensaje=""" & mcteErrorInesperadoDescr & """ /></Response>"
    pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
    wvarCodErr.set( General.mcteErrorInesperadoCod );
    wvarMensaje.set( General.mcteErrorInesperadoDescr );

    mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

    fncGetAll = false;
    /*unsup mobjCOM_Context.SetComplete() */;
    //unsup GoTo fin
    return fncGetAll;
  }

  private boolean fncValidaABase( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncValidaABase = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    boolean wvarError = false;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLCotizacion = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXMLReturnVal = null;
    diamondedge.util.XmlDom wobjXML = null;
    diamondedge.util.XmlDom wobjXMLError = null;
    Object wobjClass = null;
    HSBC.DBConnection wobjHSBC_DBCnn = new HSBC.DBConnection();
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarContChild = 0;
    int wvarContNode = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_PROVI = "";
    String mvarWDB_LOCALIDADCOD = "";
    String mvarWDB_COBROCOD = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_TIPODOCU = "";
    String mvarWDB_USOTIPOS = "";
    String mvarWDB_ALARMTIP = "";
    String mvarWDB_GUARDTIP = "";
    boolean wvarChequeoLocalidad = false;
    boolean wvarChequeoTarjeta = false;
    String wvarMensajeStoreProc = "";
    String wvarMensajeNroCot = "";
    String mvarWDB_USUARCOD = "";
    String pvarCot = "";

    //
    //
    //
    //
    //
    //
    //
    // 10/2010 se agrega una nueva variable para re-cotizar
    try 
    {

      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_REQUESTID = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID ) */ );
      }
      //
      wvarStep = 30;
      mvarWDB_CODINST = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }
      //
      wvarStep = 40;
      mvarWDB_PROVI = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi ) */ );
      }
      //
      wvarStep = 50;
      mvarWDB_LOCALIDADCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_LOCALIDADCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCod ) */ );
      }
      //
      wvarStep = 60;
      mvarWDB_COBROCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD ) */ );
      }
      //
      wvarStep = 70;
      mvarWDB_COBROTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP ) */ );
      }
      //
      wvarStep = 80;
      mvarWDB_TIPODOCU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoDocu) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPODOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoDocu ) */ );
      }
      //
      wvarStep = 90;
      mvarWDB_USOTIPOS = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_UsoTipos) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_USOTIPOS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_UsoTipos ) */ );
      }
      //
      wvarStep = 100;
      mvarWDB_ALARMTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ALARMTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_ALARMTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ALARMTIP ) */ );
      }
      //
      wvarStep = 110;
      mvarWDB_GUARDTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_GUARDTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_GUARDTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_GUARDTIP ) */ );
      }
      //
      wvarStep = 120;
      //
      //Valido los campos de Solicitud
      //
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProcValidacion );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 130;
      wobjDBParm = new Parameter( "@WDB_CODINST", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 140;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 150;
      wobjDBParm = new Parameter( "@WDB_CODPROVCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVI.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVI)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 160;
      wobjDBParm = new Parameter( "@WDB_CODPOSTCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_LOCALIDADCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_LOCALIDADCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 170;
      wobjDBParm = new Parameter( "@WDB_COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_COBROCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_COBROCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 180;
      wobjDBParm = new Parameter( "@WDB_COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_COBROTIP.equals( "" ) ? "" : mvarWDB_COBROTIP) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 190;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP_TIT", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPODOCU.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPODOCU)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_USOTIPOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_USOTIPOS.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_USOTIPOS)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_ALARMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_ALARMTIP.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_ALARMTIP)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_GUARDTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_GUARDTIP.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_GUARDTIP)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 225;
      wobjDBParm = new Parameter( "@WDB_NROCOT", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 226;
      wobjDBParm = new Parameter( "@WDB_USUARCOD", AdoConst.adChar, AdoConst.adParamInputOutput, 10, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 230;
      //
      wrstRes = new Recordset();
      wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
      wrstRes.setActiveConnection( (Connection) null );
      //
      wobjXML = new diamondedge.util.XmlDom();
      //unsup wobjXML.async = false;
      wobjXML.load( System.getProperty("user.dir") + "\\" + mcteArchivoHOM_XML );
      //
      //SI NO HUBO ERROR CONTINUO
      wvarStep = 240;
      if( ! (wrstRes.isEOF()) )
      {
        if( ! (Strings.toUpperCase( Strings.trim( wrstRes.getFields().getField(0).getValue().toString() ) ).equals( "OK" )) )
        {
          //error
          if( null /*unsup wobjXML.selectNodes( ("//ERRORES/HOGAR/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']") ) */.getLength() > 0 )
          {
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXML.selectSingleNode( "//ERRORES/HOGAR/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/TEXTOERROR" ) */ ) );
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXML.selectSingleNode( "//ERRORES/HOGAR/RESPUESTAS/ERROR[CODIGO='" + wrstRes.getFields().getField(0).getValue() + "']/VALORRESPUESTA" ) */ ) );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          }
          else
          {
            wvarCodErr.set( wrstRes.getFields().getField(0).getValue() );
            wvarMensaje.set( "No existe descripcion para este error" );
            pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          }
          //
          fncValidaABase = false;
          return fncValidaABase;
        }
        else
        {
          //Le agrego el nro. de Cotizacion.
          diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//COT_NRO" ) */, wobjDBCmd.getParameters().getParameter("@WDB_NROCOT").getValue().toString() );
          //Le agrego el nro. de UsuarCod
          /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "USUARCOD", "" ) */ );
          mvarWDB_USUARCOD = wobjDBCmd.getParameters().getParameter("@WDB_USUARCOD").getValue().toString();
          diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//USUARCOD" ) */, mvarWDB_USUARCOD );
        }
      }
      else
      {
        //error
        wvarCodErr.set( -60 );
        wvarMensaje.set( "No se retornaron datos de la base" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncValidaABase;
      }
      //
      wobjXML = (diamondedge.util.XmlDom) null;
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = (HSBC.DBConnection) null;
      //
      wvarStep = 250;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      wobjDBCnn = (Connection) (Connection)( wobjHSBC_DBCnn.GetDBConnection( General.mcteDB ) );
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProcSelect );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      //Comparo con el XML guardado de Cotizacion
      //Busco el registro guardado de la cotizacion
      wvarStep = 270;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 280;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_REQUESTID)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 290;
      wobjDBParm = new Parameter( "@WDB_TIPO_OPERACION", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "C" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 300;
      wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "OK" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 310;
      wrstRes = new Recordset();
      wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
      //
      wvarStep = 320;
      wrstRes.setActiveConnection( (Connection) null );
      //
      //Cargo el campo de XML de cotizacion
      wvarStep = 330;
      wobjXMLCotizacion = new diamondedge.util.XmlDom();
      //unsup wobjXMLCotizacion.async = false;
      wobjXMLCotizacion.loadXML( wrstRes.getFields().getField(14).getValue().toString() );
      //
      //Si es PROVI = 1 no compara el Codigo Postal
      wvarStep = 340;
      if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ ) ) != 1 )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_LocalidadCod) ) */ ) ) )
        {
          wvarChequeoLocalidad = true;
        }
        else
        {
          wvarChequeoLocalidad = false;
        }
      }
      else
      {
        wvarChequeoLocalidad = true;
      }

      //Si es COBROCOD = 4 no compara el Tipo de Tarjeta
      wvarStep = 350;
      if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) != 4 )
      {
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ ) ) )
        {
          wvarChequeoTarjeta = true;
        }
        else
        {
          wvarChequeoTarjeta = false;
        }
      }
      else
      {
        wvarChequeoTarjeta = true;
      }

      wvarStep = 360;
      if( ! ((diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ ) )) && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ ) )) && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) )) && wvarChequeoTarjeta && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TIPOHOGAR) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_TIPOHOGAR) ) */ ) )) && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PLANNCOD) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_PLANNCOD) ) */ ) )) && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Provi) ) */ ) )) && wvarChequeoLocalidad && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteNodos_Cober ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( mcteNodos_Cober ) */ ) )) && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteNodos_Campa ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( mcteNodos_Campa ) */ ) )) && (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ) ))) )
      {
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_Provi) ) */ ) )) )
        {
          wvarCodErr.set( -163 );
          wvarMensaje.set( "Cod. de Provincia: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (wvarChequeoLocalidad) )
        {
          wvarCodErr.set( -164 );
          wvarMensaje.set( "Cod. de Localidad: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) )) )
        {
          wvarCodErr.set( -167 );
          wvarMensaje.set( "Cod. de Cobro: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (wvarChequeoTarjeta) )
        {
          wvarCodErr.set( -168 );
          wvarMensaje.set( "Datos de Tarjeta: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ ) )) )
        {
          wvarCodErr.set( -169 );
          wvarMensaje.set( "Responsabilidad frente al IVA: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ ) )) )
        {
          wvarCodErr.set( -250 );
          wvarMensaje.set( "Poliza Anual: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ ) )) )
        {
          wvarCodErr.set( -251 );
          wvarMensaje.set( "Poliza Colectiva: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TIPOHOGAR) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_TIPOHOGAR) ) */ ) )) )
        {
          wvarCodErr.set( -252 );
          wvarMensaje.set( "Tipo de Hogar: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PLANNCOD) ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( ("//" + mcteParam_PLANNCOD) ) */ ) )) )
        {
          wvarCodErr.set( -253 );
          wvarMensaje.set( "Cod. de Plan: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteNodos_Cober ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( mcteNodos_Cober ) */ ) )) )
        {
          wvarCodErr.set( -254 );
          wvarMensaje.set( "Coberturas: existe una diferencia entre la Cotizacion y la Solicitud" );
        }
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteNodos_Campa ) */ ).equals( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCotizacion.selectSingleNode( mcteNodos_Campa ) */ ) )) )
        {
          wvarCodErr.set( -255 );
          wvarMensaje.set( "Campanias: existe una diferencia entre la Cotizacion y la Solicitud" );
        }


        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        fncValidaABase = false;
        return fncValidaABase;

      }
      //
      //Le agrego el Nodo de CERTISEC, y los encabezados para mandar el XML a cotizar
      wvarStep = 380;
      /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CERTISEC", "" ) */ );
      pvarRequest = "<LBA_WS res_code=\"0\" res_msg=\"\">" + wobjXMLRequest.getDocument().getDocumentElement().toString() + "</LBA_WS>";
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( pvarRequest );

      //Cotizo nuevamente
      wvarStep = 385;
      // 10/2010 el mensaje quedo obsoleto, se cambia por el actual de ov, como requiere de otros tag, y la cotizacion original ya fue comparada con la solicitud, se utiliza el xml de la cotizaci�n anterior para comparar precio
      pvarCot = wobjXMLCotizacion.getDocument().getDocumentElement().toString();
      //Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVLBAMQ.lbawS_GetSolicHOM")
      //Call wobjClass.Execute(pvarRequest, wvarMensajeStoreProc, "")
      wobjClass = new lbawA_ProductorHgMQ.lbaw_GetCotiz();
      wobjClass.Execute( pvarCot, wvarMensajeStoreProc, "" );

      wobjClass = null;
      //
      wobjXMLReturnVal = new diamondedge.util.XmlDom();
      //unsup wobjXMLReturnVal.async = false;
      wobjXMLReturnVal.loadXML( wvarMensajeStoreProc );
      //
      //10/2010 x cambio componente
      //wvarCodErr = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
      {
        pvarRes.set( "<LBA_WS res_code=\"-300\" res_msg=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@mensaje" ) */ ) + "\"></LBA_WS>" );
      }
      else
      {
        wvarCodErr.set( "0" );
        wvarMensaje.set( "" );
      }

      //wvarStep = 390
      //If Not wvarCodErr = "OK" Then
      //    Set wobjXMLError = CreateObject("MSXML2.DOMDocument")
      //    wobjXMLError.async = False
      //    Call wobjXMLError.Load(App.Path & "\" & mcteArchivoHOM_XML)
      //Completo los valores del error
      //
      //    If Not wobjXMLError.selectSingleNode("//ERRORES/HOGAR/RESPUESTAS/ERROR[CODIGO='" & wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text & "']") Is Nothing Then
      //      wvarStep = 400
      //      wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_msg").Text = wobjXMLError.selectSingleNode("//ERRORES/HOGAR/RESPUESTAS/ERROR[CODIGO='" & wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text & "']/TEXTOERROR").Text
      //      wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text = wobjXMLError.selectSingleNode("//ERRORES/HOGAR/RESPUESTAS/ERROR[CODIGO='" & wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text & "']/VALORRESPUESTA").Text
      //      wvarCodErr = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text
      //      wvarMensaje = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_msg").Text
      //      pvarRes = wobjXMLReturnVal.xml
      //    Else
      //      wvarStep = 410
      //      wvarCodErr = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text
      //      wvarMensaje = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_msg").Text
      //      pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
      //Este es el cado en que no haya contestado el MQ o sea un error
      //no identificado. No esta grabando esta salida
      //      fncValidaABase = False
      //      Exit Function
      //    End If
      //Else
      //  wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_msg").Text = ""
      //  wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text = 0
      //  wvarCodErr = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text
      //  wvarMensaje = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_msg").Text
      //End If
      //
      wvarStep = 420;
      //10/2010 x cambio componente
      //If Not (wobjXMLRequest.selectSingleNode("//" & mcteParam_VALOR).Text = wobjXMLReturnVal.selectSingleNode("//LBA_WS/Response/PRECIOS/PRECIO/" & mcteParam_VALOR).Text) Then
      if( ! (VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VALOR) ) */ ) ) == VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//COTIZACION" ) */ ) )) )
      {
        wvarCodErr.set( -97 );
        wvarMensaje.set( "Precio: existe una diferencia entre la COTIZACION y la SOLICITUD" );
        pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        return fncValidaABase;
      }
      //
      wvarStep = 430;
      /*unsup wobjXMLResponse.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLReturnVal.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "CODZONA", "" ) */ );
      //10/2010 x cambio componente
      //wobjXMLResponse.selectSingleNode("//CODZONA").Text = wobjXMLReturnVal.selectSingleNode("LBA_WS/Response/CODIZONA").Text
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLResponse.selectSingleNode( "//CODZONA" ) */, "0" );

      //
      wvarStep = 440;
      /*unsup wobjXMLResponse.selectSingleNode( "//Request" ) */.appendChild( null /*unsup wobjXMLResponse.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), "TIEMPOPROCESO", "" ) */ );
      //10/2010 x cambio componente
      //wobjXMLResponse.selectSingleNode("//TIEMPOPROCESO").Text = wobjXMLReturnVal.selectSingleNode("LBA_WS/Response/TIEMPOPROCESO").Text
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLResponse.selectSingleNode( "//TIEMPOPROCESO" ) */, "0" );

      pvarRes.set( wobjXMLResponse.getDocument().getDocumentElement().toString() );

      fncValidaABase = true;
      //
      wrstRes.close();
      wobjDBCnn.close();

      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = (HSBC.DBConnection) null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return fncValidaABase;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );
        wvarMensaje.set( General.mcteErrorInesperadoDescr );

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        fncValidaABase = false;
        /*unsup mobjCOM_Context.SetComplete() */;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncValidaABase;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
