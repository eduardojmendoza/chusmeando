package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class Funciones
{

  static boolean ValidarFechayRango( String pvarStrFecha, int pvarIntervaloMenor, int pvarIntervaloMayor, Variant pvarMensaje )
  {
    boolean ValidarFechayRango = false;
    java.util.Date wvarDatFecha = DateTime.EmptyDate;
    int wvarIntAnio = 0;
    int wvarIntMes = 0;
    int wvarIntDia = 0;
    String[] warrSplit = null;




    try 
    {

      if( ! (validarFecha( pvarStrFecha, pvarMensaje )) )
      {
        ValidarFechayRango = false;
        return ValidarFechayRango;
      }

      if( Strings.len( pvarStrFecha ) != 10 )
      {
        pvarMensaje.set( "Formato de fecha invalido" );
        ValidarFechayRango = false;
        return ValidarFechayRango;
      }

      warrSplit = Strings.split( pvarStrFecha, "/", -1 );
      wvarIntAnio = Obj.toInt( warrSplit[2] );
      wvarIntMes = Obj.toInt( warrSplit[1] );
      wvarIntDia = Obj.toInt( warrSplit[0] );

      wvarDatFecha = DateTime.dateSerial( wvarIntAnio, wvarIntMes, wvarIntDia );

      if( (wvarDatFecha.compareTo( DateTime.add( "yyyy", -pvarIntervaloMenor, DateTime.now() ) ) > 0) || (wvarDatFecha.compareTo( DateTime.add( "yyyy", -pvarIntervaloMayor, DateTime.now() ) ) < 0) )
      {
        pvarMensaje.set( "La edad permitida comprende entre " + pvarIntervaloMenor + " - " + pvarIntervaloMayor + " Anos" );
        ValidarFechayRango = false;
        return ValidarFechayRango;
      }

      ValidarFechayRango = true;
      return ValidarFechayRango;
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        ValidarFechayRango = false;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return ValidarFechayRango;
  }

  static boolean validarFecha( String pvarStrFecha, Variant pvarMensaje ) throws Exception
  {
    boolean validarFecha = false;
    int wvarIntAnio = 0;
    int wvarIntMes = 0;
    int wvarIntDia = 0;
    String[] warrSplit = null;



    warrSplit = Strings.split( pvarStrFecha, "/", -1 );
    wvarIntAnio = Obj.toInt( warrSplit[2] );
    wvarIntMes = Obj.toInt( warrSplit[1] );
    wvarIntDia = Obj.toInt( warrSplit[0] );

    pvarMensaje.set( "" );
    validarFecha = true;

    if( Math.floor( (wvarIntAnio / 4) ) == (wvarIntAnio / 4) )
    {
      if( wvarIntMes == 2 )
      {
        if( wvarIntDia > 29 )
        {
          validarFecha = false;
        }
      }
    }
    else
    {
      if( wvarIntMes == 2 )
      {
        if( wvarIntDia > 28 )
        {
          validarFecha = false;
        }
      }
    }
    if( wvarIntMes == 4 )
    {
      if( wvarIntDia > 30 )
      {
        validarFecha = false;
      }
    }
    if( wvarIntMes == 6 )
    {
      if( wvarIntDia > 30 )
      {
        validarFecha = false;
      }
    }
    if( wvarIntMes == 9 )
    {
      if( wvarIntDia > 30 )
      {
        validarFecha = false;
      }
    }
    if( wvarIntMes == 11 )
    {
      if( wvarIntDia > 30 )
      {
        validarFecha = false;
      }
    }
    if( (wvarIntMes == 1) || (wvarIntMes == 3) || (wvarIntMes == 5) || (wvarIntMes == 7) || (wvarIntMes == 8) || (wvarIntMes == 10) || (wvarIntMes == 12) )
    {
      if( wvarIntDia > 31 )
      {
        validarFecha = false;
      }
    }
    if( wvarIntMes > 12 )
    {
      validarFecha = false;
    }

    if( ! (validarFecha) )
    {
      pvarMensaje.set( "Formato de fecha invalido" );
    }

    return validarFecha;
  }

  static String convertirFecha( Variant pvarStrFecha ) throws Exception
  {
    String convertirFecha = "";
    String wvarIntAnio = "";
    String wvarIntMes = "";
    String wvarIntDia = "";
    String[] warrSplit = null;


    pvarStrFecha.set( Strings.format( pvarStrFecha.toString(), "General Date" ) );

    wvarIntAnio = String.valueOf( DateTime.year( pvarStrFecha.toDate() ) );
    wvarIntMes = Strings.right( "0" + DateTime.month( pvarStrFecha.toDate() ), 2 );
    wvarIntDia = Strings.right( "0" + DateTime.day( pvarStrFecha.toDate() ), 2 );

    convertirFecha = wvarIntAnio + wvarIntMes + wvarIntDia;

    return convertirFecha;
  }
}
