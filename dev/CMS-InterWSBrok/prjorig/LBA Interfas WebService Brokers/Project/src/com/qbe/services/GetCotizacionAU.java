package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class GetCotizacionAU implements Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  static final String mcteClassName = "LBA_InterWSBrok.GetCotizacionAU";
  static final String mcteStoreProcAltaOperacion = "SPSNCV_BRO_ALTA_OPER";
  static final String mcteStoreProcInsertCot = "SPSNCV_BRO_COTI_AUS1_GRABA";
  static final String mcteArchivoAUSCOT_XML = "LBA_VALIDACION_COT_AU.XML";
  /**
   * jc 08/2010 se agrega xsl de formateo a la nueva salida del ws
   */
  static final String mcteFormatoAUSCOT_XSL = "LBA_FORMAT_COT_AU.xsl";
  static final String mcteParam_WDB_IDENTIFICACION_BROKER = "//CODINST";
  static final String mcteParam_WDB_NRO_OPERACION_BROKER = "//REQUESTID";
  /**
   * jc 08/2010 el nuevo componente ya calcula el nro de cotizacion, no como lo hacia antes
   * Const mcteParam_WDB_NRO_COTIZACION_LBA      As String = "//WDB_NROCOT"
   */
  static final String mcteParam_WDB_NRO_COTIZACION_LBA = "//COT_NRO";
  static final String mcteParam_WDB_TIPO_OPERACION = "//TIPOOPERACION";
  static final String mcteParam_MSGANO = "//MSGANO";
  static final String mcteParam_MSGMES = "//MSGMES";
  static final String mcteParam_MSGDIA = "//MSGDIA";
  static final String mcteParam_MSGHORA = "//MSGHORA";
  static final String mcteParam_MSGMINUTO = "//MSGMINUTO";
  static final String mcteParam_MSGSEGUNDO = "//MSGSEGUNDO";
  static final String mcteParam_CODINST = "//CODINST";
  static final String mcteParam_Cliensec = "CERTISEC";
  static final String mcteParam_NacimAnn = "NACIMANN";
  static final String mcteParam_NacimMes = "NACIMMES";
  static final String mcteParam_NacimDia = "NACIMDIA";
  static final String mcteParam_Sexo = "SEXO";
  static final String mcteParam_Estado = "ESTADO";
  /**
   * jc 08/2010 cambio xml
   * Const mcteParam_CLIENIVA                    As String = "CLIENIVA"
   */
  static final String mcteParam_CLIENIVA = "IVA";
  static final String mcteParam_ModeAutCod = "MODEAUTCOD";
  static final String mcteParam_KMsrngCod = "KMSRNGCOD";
  static final String mcteParam_EfectAnn = "EFECTANN";
  static final String mcteParam_SiGarage = "SIGARAGE";
  static final String mcteParam_SumAseg = "SUMAASEG";
  static final String mcteParam_Siniestros = "SINIESTROS";
  static final String mcteParam_Gas = "GAS";
  static final String mcteParam_Provi = "PROVI";
  static final String mcteParam_LocalidadCod = "LOCALIDADCOD";
  /**
   * Revisar
   */
  static final String mcteParam_CampaCod = "CAMPACOD";
  /**
   * Revisar
   */
  static final String mcteParam_DatosPlan = "DATOSPLAN";
  static final String mcteParam_ClubLBA = "CLUBLBA";
  /**
   * jc 8/2010 agregado xml
   */
  static final String mcteParam_DESTRUCCION_80 = "DESTRUCCION_80";
  static final String mcteParam_Luneta = "LUNETA";
  static final String mcteParam_ClubEco = "CLUBECO";
  static final String mcteParam_Robocont = "ROBOCONT";
  static final String mcteParam_Granizo = "GRANIZO";
  static final String mcteParam_IBB = "IBB";
  static final String mcteParam_Escero = "ESCERO";
  /**
   * JC 2010-11 ch.r.
   */
  static final String mcteParam_CLIENTIP = "CLIENTIP";
  /**
   * fin jc
   * Revisar
   */
  static final String mcteParam_Portal = "PORTAL";
  /**
   * jc 8/2010 cambio xml
   * Const mcteParam_PRODUCTOR                   As String = "PRODUCTOR"
   */
  static final String mcteParam_PRODUCTOR = "AGECOD";
  static final String mcteParam_COBROCOD = "COBROCOD";
  static final String mcteParam_COBROTIP = "COBROTIP";
  /**
   * jc 08/2010 el nuevo componente ya calcula el nro de cotizacion, no como lo hacia antes
   * Const mcteParam_NROCOT                      As String = "//WDB_NROCOT"
   */
  static final String mcteParam_NROCOT = "//COT_NRO";
  static final String mcteParam_SumaMin = "//SUMA_MINIMA";
  static final String mcteParam_SumaMax_GBA = "//SUMA_MAXIMA_GBA";
  static final String mcteParam_SumaMax_INT = "//SUMA_MAXIMA_INT";
  static final String mcteParam_Control_GNC_GBA = "//CONTROL_GNC_GBA";
  static final String mcteParam_Control_GNC_INT = "//CONTROL_GNC_INT";
  static final String mcteParam_Permite_GNC = "//PERMITE_GNC";
  static final String mcteParam_FRANQCOD = "//FRANQCOD";
  static final String mcteParam_PLANCOD = "//PLANCOD";
  static final String mcteParam_PLANDES = "//PLANDES";
  static final String mcteParam_CODZONA = "//CODZONA";
  /**
   *  DATOS DE LOS HIJOS
   */
  static final String mcteNodos_Hijos = "//Request/HIJOS/HIJO";
  static final String mcteParam_NacimHijo = "NACIMHIJO";
  static final String mcteParam_SexoHijo = "SEXOHIJO";
  static final String mcteParam_EstadoHijo = "ESTADOHIJO";
  static final String mcteParam_EdadHijo = "EDADHIJO";
  /**
   *  DATOS DE LOS ACCESORIOS
   */
  static final String mcteNodos_Accesorios = "//Request/ACCESORIOS/ACCESORIO";
  static final String mcteParam_PrecioAcc = "PRECIOACC";
  static final String mcteParam_DescripcionAcc = "DESCRIPCIONACC";
  static final String mcteParam_CodigoAcc = "CODIGOACC";
  /**
   * Datos del mensaje
   */
  static final String mcteParam_REQUESTID = "//REQUESTID";
  static final String mcteParam_VEHDES = "//VEHDES";
  static final String mcteParm_TIEMPO = "//TIEMPO";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncGrabaCotizacion
   */
  private final String wcteFnName = "fncGrabaCotizacion";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarMensaje = "";
    Variant pvarRes = new Variant();
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      pvarRequest = Strings.mid( pvarRequest, 1, Strings.find( 1, pvarRequest, ">" ) ) + "<MSGANO>" + DateTime.year( DateTime.now() ) + "</MSGANO>" + "<MSGMES>" + DateTime.month( DateTime.now() ) + "</MSGMES>" + "<MSGDIA>" + DateTime.day( DateTime.now() ) + "</MSGDIA>" + "<MSGHORA>" + DateTime.hour( DateTime.now() ) + "</MSGHORA>" + "<MSGMINUTO>" + DateTime.minute( DateTime.now() ) + "</MSGMINUTO>" + "<MSGSEGUNDO>" + DateTime.second( DateTime.now() ) + "</MSGSEGUNDO>" + Strings.mid( pvarRequest, (Strings.find( 1, pvarRequest, ">" ) + 1) );

      //Reemplazado
      pvarRes.set( "" );
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        wvarStep = 20;
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }
      wvarStep = 30;
      pvarResponse.set( pvarRes );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"-1000\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( String pvarRequest, String wvarMensaje, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarMensaje2 = "";
    lbawA_OVMQCotizar.lbaw_OVGetCotiAUS wobjClass = new lbawA_OVMQCotizar.lbaw_OVGetCotiAUS();
    Object wobjHSBC_DBCnn = null;
    diamondedge.util.XmlDom wobjXMLRequestValid = null;
    diamondedge.util.XmlDom wobjXMLRequestCotis = null;
    diamondedge.util.XmlDom wobjXMLError = null;
    diamondedge.util.XmlDom mobjLocXSL = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    String mvarWDB_IDENTIFICACION_BROKER = "";
    String mvarWDB_NRO_OPERACION_BROKER = "";
    String mvarWDB_NRO_COTIZACION_LBA = "";
    String mvarWDB_TIPO_OPERACION = "";
    String mvarWDB_ESTADO = "";
    String mvarWDB_CERTIPOL = "";
    Variant mvarWDB_CERTISEC = new Variant();
    String mvarWDB_RECEPCION_PEDIDOANO = "";
    String mvarWDB_RECEPCION_PEDIDOMES = "";
    String mvarWDB_RECEPCION_PEDIDODIA = "";
    String mvarWDB_RECEPCION_PEDIDOHORA = "";
    String mvarWDB_RECEPCION_PEDIDOMINUTO = "";
    String mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
    String mvarWDB_ENVIO_RESPUESTA = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_TIEMPO_PROCESO_AIS = "";
    String mvarWDB_OPERACION_XML = "";
    String mvarInicioAIS = "";
    String mvarFinAIS = "";
    String mvarTiempoAIS = "";

    //jc 08/2010 para invocar el xsl
    //jc 09/2010 se agregan variables para calcular tiempo proceso AIS
    try 
    {
      //Llama a validacion
      wvarStep = 10;


      wobjClass = new LBA_InterWSBrok.GetValidacionCotAU();
      wobjClass.Execute( new Variant( pvarRequest ), new Variant( wvarMensaje2 ), "" );
      wobjClass = (lbawA_OVMQCotizar.lbaw_OVGetCotiAUS) null;

      //Analizo la respuesta del validador
      wvarStep = 20;
      wobjXMLRequestValid = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequestValid.async = false;
      wobjXMLRequestValid.loadXML( wvarMensaje2 );

      wobjXMLRequestCotis = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequestCotis.async = false;

      mvarWDB_ESTADO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( "//Response/Estado/@resultado" ) */ );
      //Si viene con errores, no continuo con la cotizacion. Grabo el error y muestro
      //el resultado
      wvarStep = 30;
      if( ! (Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( "//Response/Estado/@resultado" ) */ ) ).equals( "TRUE" )) )
      {
        pvarRes.set( "<LBA_WS res_code=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( "//Response/Estado/@resultado" ) */ ) + "\"  res_msg=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( "//Response/Estado/@mensaje" ) */ ) + "\"></LBA_WS>" );
        wobjXMLRequestValid = (diamondedge.util.XmlDom) null;
        wobjXMLRequestValid = new diamondedge.util.XmlDom();
        //unsup wobjXMLRequestValid.async = false;
        wobjXMLRequestValid.loadXML( pvarRequest );
      }
      else
      {
        //Llama al cotizador
        wvarStep = 40;
        //jc 08/2010 se llama al componente actual ademas se toma el tiempo de proceso ya que no vendr� en el mensaje
        //Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVLBAMQ.lbaws_GetCotisAUS")
        mvarInicioAIS = DateTime.format( DateTime.now() );

        wobjClass = new lbawA_OVMQCotizar.lbaw_OVGetCotiAUS();
        wobjClass.Execute( wvarMensaje2, wvarMensaje, "" );
        wobjClass = (lbawA_OVMQCotizar.lbaw_OVGetCotiAUS) null;

        wobjXMLRequestCotis.loadXML( wvarMensaje );

        //jc 08/2010 cambia la salida del componente y se agrega tiempo proceso
        mvarFinAIS = DateTime.format( DateTime.now() );
        mvarTiempoAIS = String.valueOf( DateTime.diff( "s", DateTime.toDate( mvarInicioAIS ), DateTime.toDate( mvarFinAIS ) ) );
        // mvarWDB_ESTADO = wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_code").Text
        mvarWDB_ESTADO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestCotis.selectSingleNode( "//Response/Estado/@resultado" ) */ );
        //Controlo si no hubo ningun error cuando fue a buscar la cotizacion al MQ
        wvarStep = 50;
        //If wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_code").Text <> "OK" Then
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestCotis.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
        {
          //hubo un error
          //jc 08/2010 la codificaci�n respondia al mensaje viejo, se utiliza el error que devuelve el componente actual
          pvarRes.set( "<LBA_WS res_code=\"-300\" res_msg=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestCotis.selectSingleNode( "//Response/Estado/@mensaje" ) */ ) + "\"></LBA_WS>" );

          //Set wobjXMLError = CreateObject("MSXML2.DOMDocument")
          //wobjXMLError.async = False
          //Call wobjXMLError.Load(App.Path & "\" & mcteArchivoAUSCOT_XML)
          //Completo los valores del error
          //wvarStep = 60
          //If Not wobjXMLError.selectSingleNode("//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" & wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_code").Text & "']") Is Nothing Then
          //wvarStep = 62
          //wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_msg").Text = wobjXMLError.selectSingleNode("//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" & wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_code").Text & "']/TEXTOERROR").Text
          //wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_code").Text = wobjXMLError.selectSingleNode("//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" & wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_code").Text & "']/VALORRESPUESTA").Text
          //pvarRes = wobjXMLRequestCotis.xml
          //Else
          //wvarStep = 64
          //wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_code").Text = "-200"
          //wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_msg").Text = "No hay texto para ese error"
          //pvarRes = "<LBA_WS res_code=""-200"" res_msg=""No hay texto para ese error""></LBA_WS>"
          //End If
        }
        else
        {
          //La Cotizacion esta OK
          //jc 08/2010 no existe el nodo
          //wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_code").Text = "0"
          //Grabo la cotizacion en la base de datos de SQL
          //si falla la grabacion la respuesta se la envio igual al broker
          wvarStep = 70;
          if( null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_TIPO_OPERACION ) */ == (org.w3c.dom.Node) null )
          {
            if( ! (invoke( "fncGrabaCotizacion", new Variant[] { new Variant(wvarMensaje2), new Variant(wvarMensaje), new Variant(mvarWDB_CERTISEC) } )) )
            {
              //wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_code").Text = "-552"
              //wobjXMLRequestCotis.selectSingleNode("//LBA_WS/@res_msg").Text = "No se grab� correctamente el registro"
              pvarRes.set( "<LBA_WS res_code=\"-552\" res_msg=\"No se grabo correctamente el registro\"></LBA_WS>" );
              mvarWDB_ESTADO = "ER";
            }
            else
            {
              diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequestValid.selectSingleNode( "//" + mcteParam_Cliensec ) */, mvarWDB_CERTISEC.toString() );
              //jc 08/2010 se agrega nuevo formateo de la salida xsl + 2 tag
              //pvarRes = wobjXMLRequestCotis.xml
              /*unsup wobjXMLRequestCotis.selectSingleNode( "//Response" ) */.appendChild( wobjXMLRequestCotis.getDocument().createElement( "REQUESTID" ) );
              diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequestCotis.selectSingleNode( "//REQUESTID" ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_NRO_OPERACION_BROKER ) */ ) );

              /*unsup wobjXMLRequestCotis.selectSingleNode( "//Response" ) */.appendChild( wobjXMLRequestCotis.getDocument().createElement( "VEHDES" ) );
              diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequestCotis.selectSingleNode( "//VEHDES" ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_VEHDES ) */ ) );

              mobjLocXSL = new diamondedge.util.XmlDom();
              //unsup mobjLocXSL.async = false;
              mobjLocXSL.load( System.getProperty("user.dir") + "\\" + mcteFormatoAUSCOT_XSL );
              wobjXMLRequestCotis.loadXML( /*unsup wobjXMLRequestCotis.transformNode( mobjLocXSL ) */.toString() );
              pvarRes.set( wobjXMLRequestCotis.getDocument().getDocumentElement().toString() );

              //fin formato
            }
          }
        }
      }
      //Graba todas las Operaciones inclu�das las que vienen con error
      wvarStep = 80;
      if( null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_TIPO_OPERACION ) */ == (org.w3c.dom.Node) null )
      {
        //Inicializa las variables por el caso de que venga con error
        wvarStep = 90;
        mvarWDB_IDENTIFICACION_BROKER = "0";
        if( ! (null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_IDENTIFICACION_BROKER ) */ == (org.w3c.dom.Node) null) )
        {
          mvarWDB_IDENTIFICACION_BROKER = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_IDENTIFICACION_BROKER ) */ );
        }
        wvarStep = 100;
        mvarWDB_NRO_OPERACION_BROKER = "0";
        if( ! (null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_NRO_OPERACION_BROKER ) */ == (org.w3c.dom.Node) null) )
        {
          mvarWDB_NRO_OPERACION_BROKER = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_NRO_OPERACION_BROKER ) */ );
        }
        wvarStep = 110;
        mvarWDB_NRO_COTIZACION_LBA = "0";
        //jc 08/2010 al cambiar el componente de cotiz. cambia el origen del nodo
        //If Not wobjXMLRequestValid.selectSingleNode(mcteParam_WDB_NRO_COTIZACION_LBA) Is Nothing Then
        //mvarWDB_NRO_COTIZACION_LBA = wobjXMLRequestValid.selectSingleNode(mcteParam_WDB_NRO_COTIZACION_LBA).Text
        if( ! (null /*unsup wobjXMLRequestCotis.selectSingleNode( mcteParam_WDB_NRO_COTIZACION_LBA ) */ == (org.w3c.dom.Node) null) )
        {
          mvarWDB_NRO_COTIZACION_LBA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestCotis.selectSingleNode( mcteParam_WDB_NRO_COTIZACION_LBA ) */ );
        }
        wvarStep = 120;
        mvarWDB_TIPO_OPERACION = "C";
        if( ! (null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_TIPO_OPERACION ) */ == (org.w3c.dom.Node) null) )
        {
          mvarWDB_TIPO_OPERACION = "S";
        }
        wvarStep = 130;
        mvarWDB_CERTIPOL = "0";
        if( ! (null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_IDENTIFICACION_BROKER ) */ == (org.w3c.dom.Node) null) )
        {
          mvarWDB_CERTIPOL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_IDENTIFICACION_BROKER ) */ );
        }
        wvarStep = 140;
        mvarWDB_CERTISEC.set( 0 );
        if( ! (null /*unsup wobjXMLRequestValid.selectSingleNode( ("//" + mcteParam_Cliensec) ) */ == (org.w3c.dom.Node) null) )
        {
          mvarWDB_CERTISEC.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( "//" + mcteParam_Cliensec ) */ ) );
        }
        wvarStep = 150;
        mvarWDB_RECEPCION_PEDIDOANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_MSGANO ) */ );
        mvarWDB_RECEPCION_PEDIDOMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_MSGMES ) */ );
        mvarWDB_RECEPCION_PEDIDODIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_MSGDIA ) */ );
        mvarWDB_RECEPCION_PEDIDOHORA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_MSGHORA ) */ );
        mvarWDB_RECEPCION_PEDIDOMINUTO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_MSGMINUTO ) */ );
        mvarWDB_RECEPCION_PEDIDOSEGUNDO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestValid.selectSingleNode( mcteParam_MSGSEGUNDO ) */ );
        mvarWDB_TIEMPO_PROCESO_AIS = "0";
        //jc 08/2010 esto ya no viene en el mensaje del AIS
        //If Not wobjXMLRequestCotis.selectSingleNode(mcteParm_TIEMPO) Is Nothing Then
        //   mvarWDB_TIEMPO_PROCESO_AIS = CLng(wobjXMLRequestCotis.selectSingleNode(mcteParm_TIEMPO).Text)
        //End If
        wvarStep = 160;
        //Fuerza los errores no codificados con 2 caracteres a ER
        //jc 08/2010 el componente devuelve "true" antes "ok"
        //If Left(mvarWDB_ESTADO, 1) = "-" Or Len(mvarWDB_ESTADO) = 4 Then
        if( mvarWDB_ESTADO.equals( "true" ) )
        {
          mvarWDB_ESTADO = "OK";
        }
        else
        {
          mvarWDB_ESTADO = "ER";
        }
        //
        mvarWDB_OPERACION_XML = pvarRequest;
        wvarStep = 170;
        //
        wobjXMLRequestCotis = (diamondedge.util.XmlDom) null;
        wobjHSBC_DBCnn = new HSBC.DBConnection();
        //error: function 'GetDBConnection' was not found.
        //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
        wobjDBCmd = new Command();
        wobjDBCmd.setActiveConnection( wobjDBCnn );
        wobjDBCmd.setCommandText( mcteStoreProcAltaOperacion );
        wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
        //
        wvarStep = 180;
        wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_IDENTIFICACION_BROKER ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 190;
        wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_NRO_OPERACION_BROKER ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 14 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 200;
        wobjDBParm = new Parameter( "@WDB_NRO_COTIZACION_LBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_NRO_COTIZACION_LBA ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 18 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 210;
        wobjDBParm = new Parameter( "@WDB_TIPO_OPERACION", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarWDB_TIPO_OPERACION ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 220;
        wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarWDB_ESTADO ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 230;
        wobjDBParm = new Parameter( "@WDB_RECEPCION_PEDIDO", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant( mvarWDB_RECEPCION_PEDIDOANO + "-" + mvarWDB_RECEPCION_PEDIDOMES + "-" + mvarWDB_RECEPCION_PEDIDODIA + " " + mvarWDB_RECEPCION_PEDIDOHORA + ":" + mvarWDB_RECEPCION_PEDIDOMINUTO + ":" + mvarWDB_RECEPCION_PEDIDOSEGUNDO ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 240;
        wobjDBParm = new Parameter( "@WDB_ENVIO_RESPUESTA", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant( DateTime.year( DateTime.now() ) + "-" + DateTime.month( DateTime.now() ) + "-" + DateTime.day( DateTime.now() ) + " " + DateTime.hour( DateTime.now() ) + ":" + DateTime.minute( DateTime.now() ) + ":" + DateTime.second( DateTime.now() ) ) );

        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 250;
        wobjDBParm = new Parameter( "@WDB_RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "AUS1" ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 260;
        wobjDBParm = new Parameter( "@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 270;
        wobjDBParm = new Parameter( "@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 6 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 280;
        wobjDBParm = new Parameter( "@WDB_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_CERTIPOL ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 290;
        wobjDBParm = new Parameter( "@WDB_CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 300;
        wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, mvarWDB_CERTISEC );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 6 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 310;
        //jc 09/2010 el tiempo no viene en el mensaje, se calcula
        //si vino con error de la validaci�n como viene vacio se fuerza 1.
        if( mvarTiempoAIS.equals( "" ) )
        {
          mvarTiempoAIS = "1";
        }

        //Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_TIEMPO_PROCESO_AIS", adNumeric, adParamInput, , mvarWDB_TIEMPO_PROCESO_AIS)
        wobjDBParm = new Parameter( "@WDB_TIEMPO_PROCESO_AIS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarTiempoAIS ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 6 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 320;
        wobjDBParm = new Parameter( "@WDB_OPERACION_XML", AdoConst.adVarChar, AdoConst.adParamInput, 8000, (mvarWDB_OPERACION_XML.equals( "" ) ? new Variant((Object)null) : new Variant(Strings.left( mvarWDB_OPERACION_XML, 8000 ))) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 330;
        wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
        wobjDBCnn.close();
      }
      fncGetAll = true;
      fin: 
      //libero los objectos
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequestValid = (diamondedge.util.XmlDom) null;
      wobjXMLRequestCotis = (diamondedge.util.XmlDom) null;
      wobjClass = (lbawA_OVMQCotizar.lbaw_OVGetCotiAUS) null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"-1000\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }

  private boolean fncGrabaCotizacion( String pvarXMLrequest, String pvarXMLresponse, Variant pvarCertiSec )
  {
    boolean fncGrabaCotizacion = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Object wobjClass = null;
    diamondedge.util.XmlDom mobjXMLDoc = null;
    int wvarCount = 0;
    String mvarCODINT = "";
    String mvarCertiSec = "";
    String mvarSUPLENUM = "";
    String mvarNacimAnn = "";
    String mvarNacimMes = "";
    String mvarNacimDia = "";
    String mvarCLIENEST = "";
    String mvarNUMHIJOS = "";
    String mvarDOMICCPO = "";
    String mvarPROVICOD = "";
    String mvarTIPOCUEN = "";
    String mvarCOBROTIP = "";
    String mvarFRANQCOD = "";
    String mvarPQTDES = "";
    String mvarPLANCOD = "";
    String mvarHIJOS1729 = "";
    String mvarZONA = "";
    String mvarCLIENIVA = "";
    String mvarSUMAASEG = "";
    String mvarSUMALBA = "";
    String mvarCLUB_LBA = "";
    String mvarDESTRUCCION_80 = "";
    String mvarLUNETA = "";
    String mvarCLUBECO = "";
    String mvarROBOCONT = "";
    String mvarGRANIZO = "";
    String mvarIBB = "";
    String mvarESCERO = "";
    String wvarCLIENTIP = "";
    String wvarCLIENTIP2 = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarSEXO = "";
    String mvarESTCIV = "";
    String mvarSIFMVEHI_DES = "";
    String mvarACCESORIOS = "";
    String mvarMOTORNUM = "";
    String mvarCHASINUM = "";
    String mvarPATENNUM = "";
    String mvarAUMARCOD = "";
    String mvarAUMODCOD = "";
    String mvarAUSUBCOD = "";
    String mvarAUADICOD = "";
    String mvarAUMODORI = "";
    String mvarAUKLMNUM = "";
    String mvarFABRICAN = "";
    String mvarGUGARAGE = "";
    String mvarAUNUMSIN = "";
    String mvarAUNUMKMT = "";
    String mvarAUUSOGNC = "";
    String mvarCAMP_CODIGO = "";
    String mvarNRO_PROD = "";
    String mvarCONDUAPE = "";
    String mvarCONDUNOM = "";
    String mvarCONDUFEC = "";
    String mvarCONDUSEX = "";
    String mvarCONDUEST = "";
    String mvarCONDUEXC = "";
    String mvarAUACCCOD = "";
    String mvarAUVEASUM = "";
    String mvarAUVEADES = "";
    String mvarAUVEADEP = "";
    String mvarCOBROCOD = "";

    //jc 08/2010 creado para el componente de zona
    //jc fin
    //jc 08/2010 se agregan nuevos campos
    //11/2010 ch.r.
    //11/2010 ch.r.
    //existentes en componente viejo y no el el actual
    //jc fin
    try 
    {
      //Llama a validacion
      wvarStep = 10;
      fncGrabaCotizacion = true;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarXMLrequest );

      wvarStep = 20;
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( pvarXMLresponse );

      wvarStep = 30;
      mvarNRO_PROD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PRODUCTOR ) */ );
      mvarCODINT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CODINST ) */ );
      mvarCertiSec = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Cliensec ) */ );
      mvarNacimAnn = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimAnn ) */ );
      mvarNacimMes = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimMes ) */ );
      mvarNacimDia = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimDia ) */ );
      mvarCLIENEST = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Estado ) */ ) );
      mvarNUMHIJOS = String.valueOf( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */.getLength() );
      mvarDOMICCPO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCod ) */ );
      mvarPROVICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi ) */ );
      mvarCOBROTIP = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP ) */ ) );
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO_OPERACION ) */ == (org.w3c.dom.Node) null) )
      {
        mvarFRANQCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( mcteParam_FRANQCOD ) */ );
        mvarPQTDES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( mcteParam_PLANDES ) */ );
        mvarPLANCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( mcteParam_PLANCOD ) */ );
      }
      else
      {
        mvarFRANQCOD = " ";
        mvarPQTDES = " ";
        mvarPLANCOD = "0";
      }
      mvarHIJOS1729 = String.valueOf( null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */.getLength() );
      //jc 08/2010 el componente de cotiz nuevo no trae la zona.
      //mvarZONA = CLng(wobjXMLResponse.selectSingleNode(mcteParam_CODZONA).Text)
      mvarRequest = "<Request><RAMOPCOD>AUS1</RAMOPCOD><PROVICOD>" + mvarPROVICOD + "</PROVICOD><CPACODPO>" + mvarDOMICCPO + "</CPACODPO></Request>";
      wobjClass = new lbawA_OVLBAMQ.lbaw_GetZona();
      wobjClass.Execute( new Variant( mvarRequest ), new Variant( mvarResponse ), "" );
      wobjClass = null;
      mobjXMLDoc = new diamondedge.util.XmlDom();
      //unsup mobjXMLDoc.async = false;
      mobjXMLDoc.loadXML( mvarResponse );
      if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
      {
        mvarZONA = diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//CODIZONA" ) */ );
      }
      else
      {
        mvarZONA = "";
      }
      mobjXMLDoc = (diamondedge.util.XmlDom) null;
      //fin jc
      mvarCLIENIVA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENIVA ) */ );
      mvarSUMAASEG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SumAseg ) */ );
      mvarSUMALBA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//SUMALBA" ) */ );
      mvarCLUB_LBA = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClubLBA ) */ ) );
      mvarSEXO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Sexo ) */ ) );
      mvarSIFMVEHI_DES = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VEHDES ) */ ) );
      mvarACCESORIOS = (null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */.getLength() > 0 ? "S" : "N");
      mvarAUMARCOD = Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ModeAutCod ) */ ), 1, 5 );
      mvarAUMODCOD = Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ModeAutCod ) */ ), 6, 5 );
      mvarAUSUBCOD = Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ModeAutCod ) */ ), 11, 5 );
      mvarAUADICOD = Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ModeAutCod ) */ ), 16, 5 );
      mvarAUMODORI = Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ModeAutCod ) */ ), 21, 1 );
      mvarAUKLMNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_KMsrngCod ) */ );
      mvarFABRICAN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_EfectAnn ) */ );
      mvarGUGARAGE = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SiGarage ) */ ) );
      mvarAUNUMSIN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Siniestros ) */ );
      mvarAUUSOGNC = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Gas ) */ ) );
      mvarCAMP_CODIGO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CampaCod ) */ );
      mvarCOBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD ) */ );
      //jc 08/2010 se agregan nuevos campos
      mvarDESTRUCCION_80 = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DESTRUCCION_80 ) */ ) );
      mvarLUNETA = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Luneta ) */ ) );
      mvarCLUBECO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClubEco ) */ ) );
      mvarROBOCONT = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Robocont ) */ ) );
      mvarGRANIZO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Granizo ) */ ) );
      mvarIBB = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_IBB ) */ ) );
      mvarESCERO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Escero ) */ ) );
      //11/2010 ch.r.
      if( null /*unsup wobjXMLRequest.selectNodes( ("//" + mcteParam_CLIENTIP) ) */.getLength() == 0 )
      {
        //Para forzar la persona f�sica si no informa el nodo
        wvarCLIENTIP = "00";
      }
      else
      {
        wvarCLIENTIP = Strings.right( "00" + Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENTIP ) */ ) ), 2 );
      }

      if( wvarCLIENTIP.equals( "00" ) )
      {
        wvarCLIENTIP2 = "F";
      }
      else
      {
        wvarCLIENTIP2 = "J";
      }

      //fin jc
      wvarStep = 40;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProcInsertCot );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 50;
      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "AUS1" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 60;
      wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 70;
      wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 80;
      wobjDBParm = new Parameter( "@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCODINT ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 90;
      wobjDBParm = new Parameter( "@CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 100;
      wobjDBParm = new Parameter( "@CERTISEC", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( mvarCertiSec ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 110;
      wobjDBParm = new Parameter( "@SUPLENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 120;
      wobjDBParm = new Parameter( "@USUARCOD", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarCODINT ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 130;
      wobjDBParm = new Parameter( "@NUMEDOCU", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( "0" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 140;
      wobjDBParm = new Parameter( "@TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 150;
      wobjDBParm = new Parameter( "@CLIENAP1", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( "INNOMINADA" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 160;
      //Reemplazado
      wobjDBParm = new Parameter( "@CLIENAP2", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 170;
      //Reemplazado
      wobjDBParm = new Parameter( "@CLIENNOM", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 180;
      wobjDBParm = new Parameter( "@NACIMANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNacimAnn ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 190;
      wobjDBParm = new Parameter( "@NACIMMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNacimMes ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 200;
      wobjDBParm = new Parameter( "@NACIMDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNacimDia ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 210;
      wobjDBParm = new Parameter( "@CLIENSEX", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarSEXO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 220;
      wobjDBParm = new Parameter( "@CLIENEST", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLIENEST ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 230;
      wobjDBParm = new Parameter( "@PAISSCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "00" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 240;
      //Reemplazado
      wobjDBParm = new Parameter( "@IDIOMCOD", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 250;
      wobjDBParm = new Parameter( "@NUMHIJOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNUMHIJOS ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 260;
      wobjDBParm = new Parameter( "@EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( DateTime.year( DateTime.now() ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 270;
      wobjDBParm = new Parameter( "@EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( DateTime.month( DateTime.now() ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 280;
      wobjDBParm = new Parameter( "@EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( DateTime.day( DateTime.now() ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 290;
      // jc 10/2010 no es mas F persona f�sica
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTIP", adChar, adParamInput, 2, "F")
      //11/2010 ch.r.
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTIP", adChar, adParamInput, 2, "00")
      wobjDBParm = new Parameter( "@CLIENTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarCLIENTIP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 300;
      //Reemplazado
      wobjDBParm = new Parameter( "@CLIENFUM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 310;
      //Reemplazado
      wobjDBParm = new Parameter( "@CLIENDOZ", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 320;
      //Reemplazado
      wobjDBParm = new Parameter( "@ABRIDTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 330;
      //Reemplazado
      wobjDBParm = new Parameter( "@ABRIDNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 340;
      //Reemplazado
      wobjDBParm = new Parameter( "@CLIENORG", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 350;
      //11/2010 ch.r.
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@PERSOTIP", adChar, adParamInput, 1, "F")
      wobjDBParm = new Parameter( "@PERSOTIP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarCLIENTIP2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 360;
      //Reemplazado
      wobjDBParm = new Parameter( "@CLIENCLA", AdoConst.adChar, AdoConst.adParamInput, 9, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 370;
      wobjDBParm = new Parameter( "@FALLEANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 380;
      wobjDBParm = new Parameter( "@FALLEMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 390;
      wobjDBParm = new Parameter( "@FALLEDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 400;
      wobjDBParm = new Parameter( "@FBAJAANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 410;
      wobjDBParm = new Parameter( "@FBAJAMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 420;
      wobjDBParm = new Parameter( "@FBAJADIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 430;
      //Reemplazado
      wobjDBParm = new Parameter( "@EMAIL", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 440;
      //Reemplazado
      wobjDBParm = new Parameter( "@DOMICCAL", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 450;
      //Reemplazado
      wobjDBParm = new Parameter( "@DOMICDOM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 460;
      //Reemplazado
      wobjDBParm = new Parameter( "@DOMICDNU", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 470;
      //Reemplazado
      wobjDBParm = new Parameter( "@DOMICESC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 480;
      //Reemplazado
      wobjDBParm = new Parameter( "@DOMICPIS", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 490;
      //Reemplazado
      wobjDBParm = new Parameter( "@DOMICPTA", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 500;
      //Reemplazado
      wobjDBParm = new Parameter( "@DOMICPOB", AdoConst.adVarChar, AdoConst.adParamInput, 60, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 510;
      wobjDBParm = new Parameter( "@DOMICCPO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarDOMICCPO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 520;
      wobjDBParm = new Parameter( "@PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPROVICOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 530;
      //Reemplazado
      wobjDBParm = new Parameter( "@TELCOD", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 540;
      //Reemplazado
      wobjDBParm = new Parameter( "@TELNRO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 550;
      //Reemplazado
      wobjDBParm = new Parameter( "@PRINCIPAL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 560;
      wobjDBParm = new Parameter( "@TIPOCUEN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarTIPOCUEN ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 570;
      wobjDBParm = new Parameter( "@COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarCOBROTIP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 580;
      wobjDBParm = new Parameter( "@BANCOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 9000 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 590;
      wobjDBParm = new Parameter( "@SUCURCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 8888 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 600;
      //Reemplazado
      wobjDBParm = new Parameter( "@CUENTDC", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 610;
      //Reemplazado
      wobjDBParm = new Parameter( "@CUENNUME", AdoConst.adChar, AdoConst.adParamInput, 16, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 620;
      wobjDBParm = new Parameter( "@TARJECOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 630;
      wobjDBParm = new Parameter( "@VENCIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 640;
      wobjDBParm = new Parameter( "@VENCIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 650;
      wobjDBParm = new Parameter( "@VENCIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 660;
      wobjDBParm = new Parameter( "@FRANQCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarFRANQCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 670;
      wobjDBParm = new Parameter( "@PQTDES", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant( mvarPQTDES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 680;
      wobjDBParm = new Parameter( "@PLANCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPLANCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 690;
      wobjDBParm = new Parameter( "@HIJOS1416", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 700;
      wobjDBParm = new Parameter( "@HIJOS1729", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarHIJOS1729 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 710;
      wobjDBParm = new Parameter( "@ZONA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarZONA ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 720;
      wobjDBParm = new Parameter( "@CODPROV", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPROVICOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 730;
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@SUMALBA", adNumeric, adParamInput, , mvarSUMALBA)
      wobjDBParm = new Parameter( "@SUMALBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toDouble( mvarSUMALBA ) / 100 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 740;
      wobjDBParm = new Parameter( "@CLIENIVA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLIENIVA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 750;
      wobjDBParm = new Parameter( "@SUMAASEG", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarSUMAASEG ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 760;
      wobjDBParm = new Parameter( "@CLUB_LBA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLUB_LBA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 770;
      wobjDBParm = new Parameter( "@CPAANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 8 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 780;
      wobjDBParm = new Parameter( "@CTAKMS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 790;
      //jc 08/2010 ahora se informa valor
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@ESCERO", adChar, adParamInput, 1, " ") 'Reemplazado
      wobjDBParm = new Parameter( "@ESCERO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarESCERO ) );

      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 800;
      wobjDBParm = new Parameter( "@TIENEPLAN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 810;
      wobjDBParm = new Parameter( "@COND_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 820;
      wobjDBParm = new Parameter( "@ASEG_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 830;
      wobjDBParm = new Parameter( "@FH_NAC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toInt( mvarNacimAnn + mvarNacimMes + mvarNacimDia ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 8 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 840;
      wobjDBParm = new Parameter( "@SEXO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarSEXO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 850;
      wobjDBParm = new Parameter( "@ESTCIV", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLIENEST ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 860;
      wobjDBParm = new Parameter( "@SIFMVEHI_DES", AdoConst.adChar, AdoConst.adParamInput, 35, new Variant( Strings.left( mvarSIFMVEHI_DES, 35 ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 870;
      //Reemplazado
      wobjDBParm = new Parameter( "@PROFECOD", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 880;
      wobjDBParm = new Parameter( "@ACCESORIOS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarACCESORIOS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 890;
      wobjDBParm = new Parameter( "@REFERIDO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 900;
      //Reemplazado
      wobjDBParm = new Parameter( "@MOTORNUM", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 910;
      //Reemplazado
      wobjDBParm = new Parameter( "@CHASINUM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 920;
      //Reemplazado
      wobjDBParm = new Parameter( "@PATENNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 930;
      wobjDBParm = new Parameter( "@AUMARCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toInt( mvarAUMARCOD ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 940;
      wobjDBParm = new Parameter( "@AUMODCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toInt( mvarAUMODCOD ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 950;
      wobjDBParm = new Parameter( "@AUSUBCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toInt( mvarAUSUBCOD ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 960;
      wobjDBParm = new Parameter( "@AUADICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toInt( mvarAUADICOD ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 970;
      wobjDBParm = new Parameter( "@AUMODORI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarAUMODORI ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 980;
      wobjDBParm = new Parameter( "@AUUSOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 990;
      wobjDBParm = new Parameter( "@AUVTVCOD", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1000;
      wobjDBParm = new Parameter( "@AUVTVDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1010;
      wobjDBParm = new Parameter( "@AUVTVMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1020;
      wobjDBParm = new Parameter( "@AUVTVANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1030;
      wobjDBParm = new Parameter( "@VEHCLRCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1040;
      wobjDBParm = new Parameter( "@AUKLMNUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUKLMNUM ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1050;
      wobjDBParm = new Parameter( "@FABRICAN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarFABRICAN ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1060;
      wobjDBParm = new Parameter( "@FABRICMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1070;
      wobjDBParm = new Parameter( "@GUGARAGE", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarGUGARAGE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1080;
      //Reemplazado
      wobjDBParm = new Parameter( "@GUDOMICI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1090;
      wobjDBParm = new Parameter( "@AUCATCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1100;
      wobjDBParm = new Parameter( "@AUTIPCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1110;
      //Reemplazado
      wobjDBParm = new Parameter( "@AUCIASAN", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1120;
      wobjDBParm = new Parameter( "@AUANTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1130;
      wobjDBParm = new Parameter( "@AUNUMSIN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUNUMSIN ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1140;
      wobjDBParm = new Parameter( "@AUNUMKMT", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUKLMNUM ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1150;
      wobjDBParm = new Parameter( "@AUUSOGNC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarAUUSOGNC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1160;
      //Reemplazado
      wobjDBParm = new Parameter( "@SITUCPOL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "A" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1170;
      wobjDBParm = new Parameter( "@COBROFOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1180;
      wobjDBParm = new Parameter( "@EDADACTU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( DateTime.diff( "YYYY", DateTime.dateSerial( Obj.toInt( mvarNacimAnn ), Obj.toInt( mvarNacimMes ), Obj.toInt( mvarNacimDia ) ), DateTime.now() ) ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1190;
      for( wvarCount = 1; wvarCount <= 20; wvarCount++ )
      {
        wobjDBParm = new Parameter( "@COBERCOD" + String.valueOf( wvarCount ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 3 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@COBERORD" + String.valueOf( wvarCount ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CAPITASG" + String.valueOf( wvarCount ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CAPITIMP" + String.valueOf( wvarCount ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      wvarStep = 1200;
      for( wvarCount = 1; wvarCount <= 10; wvarCount++ )
      {
        wobjDBParm = new Parameter( "@DOCUMDAT" + String.valueOf( wvarCount ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 9 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        //Reemplazado
        wobjDBParm = new Parameter( "@NOMBREAS" + String.valueOf( wvarCount ), AdoConst.adChar, AdoConst.adParamInput, 49, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      wvarStep = 1210;
      wobjDBParm = new Parameter( "@CAMP_CODIGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCAMP_CODIGO ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1220;
      //Reemplazado
      wobjDBParm = new Parameter( "@CAMP_DESC", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1230;
      //Reemplazado
      wobjDBParm = new Parameter( "@LEGAJO_GTE", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1240;
      wobjDBParm = new Parameter( "@NRO_PROD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNRO_PROD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1250;
      //Reemplazado
      wobjDBParm = new Parameter( "@SUCURSAL_CODIGO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1260;
      //Reemplazado
      wobjDBParm = new Parameter( "@LEGAJO_VEND", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1270;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */;
      for( wvarCount = 1; wvarCount <= 10; wvarCount++ )
      {
        //Reemplazado
        wobjDBParm = new Parameter( "@CONDUAPE" + String.valueOf( wvarCount ), AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        //Reemplazado
        wobjDBParm = new Parameter( "@CONDUNOM" + String.valueOf( wvarCount ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        if( wvarCount <= Obj.toInt( mvarNUMHIJOS ) )
        {
          wobjXMLNode = wobjXMLList.item( wvarCount - 1 );
          mvarCONDUFEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( "//" + mcteParam_NacimHijo ) */ );
          mvarCONDUSEX = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( "//" + mcteParam_SexoHijo ) */ ) );
          mvarCONDUEST = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( "//" + mcteParam_EstadoHijo ) */ ) );
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          mvarCONDUFEC = "0";
          mvarCONDUSEX = " ";
          mvarCONDUEST = " ";
        }

        wobjDBParm = new Parameter( "@CONDUFEC" + String.valueOf( wvarCount ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCONDUFEC ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 8 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CONDUSEX" + String.valueOf( wvarCount ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCONDUSEX ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CONDUEST" + String.valueOf( wvarCount ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCONDUEST ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CONDUEXC" + String.valueOf( wvarCount ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "S" ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;

      wvarStep = 1280;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */;
      for( wvarCount = 1; wvarCount <= 10; wvarCount++ )
      {
        if( wvarCount <= wobjXMLList.getLength() )
        {
          wobjXMLNode = wobjXMLList.item( wvarCount - 1 );
          mvarAUACCCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( "//" + mcteParam_CodigoAcc ) */ );
          mvarAUVEASUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( "//" + mcteParam_PrecioAcc ) */ );
          mvarAUVEADES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( "//" + mcteParam_DescripcionAcc ) */ );
          mvarAUVEADEP = "S";
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          mvarAUACCCOD = "0";
          mvarAUVEASUM = "0";
          mvarAUVEADES = " ";
          mvarAUVEADEP = " ";
        }
        wobjDBParm = new Parameter( "@AUACCCOD" + String.valueOf( wvarCount ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUACCCOD ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@AUVEASUM" + String.valueOf( wvarCount ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUVEASUM ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 14 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@AUVEADES" + String.valueOf( wvarCount ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarAUVEADES ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@AUVEADEP" + String.valueOf( wvarCount ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarAUVEADEP ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;

      wvarStep = 1290;
      wobjDBParm = new Parameter( "@COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCOBROCOD ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //jc 08/2010 se agregan nuevos campos
      wvarStep = 1291;
      wobjDBParm = new Parameter( "@DESTRUCCION_80", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarDESTRUCCION_80 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1292;
      wobjDBParm = new Parameter( "@LUNETA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarLUNETA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1293;
      wobjDBParm = new Parameter( "@CLUBECO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLUBECO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1294;
      wobjDBParm = new Parameter( "@ROBOCONT", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarROBOCONT ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1295;
      wobjDBParm = new Parameter( "@GRANIZO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarGRANIZO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1296;
      wobjDBParm = new Parameter( "@IBB", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarIBB ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //jc fin
      wvarStep = 1300;
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      wobjDBCnn.close();
      pvarCertiSec.set( wobjDBCmd.getParameters().getParameter("@CERTISEC").getValue() );

      fin: 
      //libero los objectos
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLResponse = (diamondedge.util.XmlDom) null;


      return fncGrabaCotizacion;
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        fncGrabaCotizacion = false;
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGrabaCotizacion;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
