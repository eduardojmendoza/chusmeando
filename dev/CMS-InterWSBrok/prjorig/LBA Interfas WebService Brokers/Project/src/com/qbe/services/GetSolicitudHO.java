package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Implementacion de los objetos
 * Objetos del FrameWork
 */

public class GetSolicitudHO implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetSolicitudHO";
  static final String mcteStoreProcSelect = "SPSNCV_BRO_SELECT_OPER_ESP";
  static final String mcteStoreProc = "SPSNCV_BRO_ALTA_OPER";
  static final String mcteStoreProcAlta = "SPSNCV_BRO_SOLI_HOM1_GRABA";
  /**
   * 10/2010 sp para descripciones del impreso
   */
  static final String mcteStoreProcImpre = "SPSNCV_BRO_SOLI_HOM1_IMPRE";
  static final String mcteStorePImpreCob = "SPSNCV_BRO_SOLI_HOM1_IMP_COB";
  /**
   * Archivo de Configuracion
   */
  static final String mcteArchivoConfHOM_XML = "LBA_PARAM_HO.XML";
  static final String mcteArchivoHOM_XML = "LBA_VALIDACION_COT_HO.XML";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_TipoOperac = "TIPOOPERAC";
  static final String mcteParam_EstadoProc = "ESTADOPROC";
  static final String mcteParam_TiempoProceso = "TIEMPOPROCESO";
  static final String mcteParam_PedidoAno = "MSGANO";
  static final String mcteParam_PedidoMes = "MSGMES";
  static final String mcteParam_PedidoDia = "MSGDIA";
  static final String mcteParam_PedidoHora = "MSGHORA";
  static final String mcteParam_PedidoMinuto = "MSGMINUTO";
  static final String mcteParam_PedidoSegundo = "MSGSEGUNDO";
  static final String mcteParam_DOMICCPO = "DOMICCPO";
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  /**
   *  Se cambian los 4 nodos x utilizarse el nuevo componente de cotizacion
   * Const mcteParam_COBROCOD             As String = "COBROCOD"
   */
  static final String mcteParam_COBROCOD = "FPAGO";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_TIPOHOGAR = "TIPOHOGAR";
  /**
   * Const mcteParam_PLANNCOD             As String = "PLANNCOD"
   */
  static final String mcteParam_PLANNCOD = "PLAN";
  /**
   * Const mcteParam_Provi                As String = "PROVI"
   */
  static final String mcteParam_Provi = "PROVCOD";
  /**
   * Const mcteParam_LocalidadCod         As String = "LOCALIDADCOD"
   */
  static final String mcteParam_LocalidadCod = "LOCALIDAD";
  static final String mcteParam_RAMOPCOD = "RAMOPCOD";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  static final String mcteParam_NROCOT = "COT_NRO";
  static final String mcteParam_CERTISEC = "CERTISEC";
  static final String mcteParam_ClienAp1 = "CLIENAP1";
  static final String mcteParam_ClienAp2 = "CLIENAP2";
  static final String mcteParam_ClienNom = "CLIENNOM";
  static final String mcteParam_CLIENSEX = "SEXO";
  static final String mcteParam_CLIENEST = "ESTADO";
  static final String mcteParam_NacimAnn = "NACIMANN";
  static final String mcteParam_NacimMes = "NACIMMES";
  static final String mcteParam_NacimDia = "NACIMDIA";
  static final String mcteParam_EMAIL = "EMAIL";
  static final String mcteParam_DOMICDOM = "DOMICDOM";
  static final String mcteParam_DomicDnu = "DOMICDNU";
  static final String mcteParam_DOMICPIS = "DOMICPIS";
  static final String mcteParam_DomicPta = "DOMICPTA";
  static final String mcteParam_DOMICPOB = "DOMICPOB";
  static final String mcteParam_BARRIOCOUNT = "BARRIOCOUNT";
  /**
   * 10/2010 se agrega codigo de �rea
   */
  static final String mcteParam_TELCOD = "TELCOD";
  static final String mcteParam_TELNRO = "TELNRO";
  static final String mcteParam_DomicDomCo = "DOMICDOMCOR";
  static final String mcteParam_DomicDNUCo = "DOMICDNUCOR";
  static final String mcteParam_DomicPisCo = "DOMICPISCOR";
  static final String mcteParam_DomicPtaCo = "DOMICPTACOR";
  static final String mcteParam_DOMICPOBCo = "DOMICPOBCOR";
  static final String mcteParam_ProviCo = "PROVICOR";
  static final String mcteParam_LocalidadCodCo = "LOCALIDADCODCOR";
  /**
   * 10/2010 se agrega codigo de �rea
   */
  static final String mcteParam_TELCOD_CO = "TELCODCOR";
  static final String mcteParam_TelNroCo = "TELNROCOR";
  /**
   * Const mcteParam_TipoCuen             As String = "TIPOCUEN"
   */
  static final String mcteParam_CUENNUME = "CUENNUME";
  /**
   * Const mcteParam_TarjeCod             As String = "TARJECOD"
   */
  static final String mcteParam_VENCIANN = "VENCIANN";
  static final String mcteParam_VENCIMES = "VENCIMES";
  static final String mcteParam_VENCIDIA = "VENCIDIA";
  static final String mcteParam_UsoTipos = "USOTIPOS";
  static final String mcteParam_ALARMTIP = "ALARMTIP";
  static final String mcteParam_GUARDTIP = "GUARDTIP";
  static final String mcteParam_SWCALDER = "CALDERA";
  static final String mcteParam_SWASCENS = "ASCENSOR";
  static final String mcteParam_SWCREJAS = "REJAS";
  static final String mcteParam_SWPBLIND = "PUERTABLIND";
  static final String mcteParam_SWDISYUN = "DISYUNTOR";
  static final String mcteParam_SWPROPIE = "PROPIEDAD";
  static final String mcteParam_ZONA = "CODIZONA";
  static final String mcteParam_TipoDocu = "TIPODOCU";
  static final String mcteParam_NumeDocu = "NUMEDOCU";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS/COBERTURA";
  static final String mcteParam_COBERCOD = "COBERCOD";
  static final String mcteParam_CONTRMOD = "CONTRMOD";
  static final String mcteParam_CAPITASG = "CAPITASG";
  /**
   * DATOS DE LAS CAMPA�AS
   */
  static final String mcteNodos_Campa = "//Request/CAMPANIAS/CAMPANIA";
  static final String mcteParam_CampaCod = "CAMPACOD";
  /**
   * DATOS DE LOS ELECTRODOMESTICOS
   */
  static final String mcteNodos_Elect = "//Request/ELECTRODOMESTICOS/ELECTRODOMESTICO";
  static final String mcteParam_Tipo = "TIPO";
  static final String mcteParam_Marca = "MARCA";
  static final String mcteParam_Modelo = "MODELO";
  /**
   * AM
   */
  static final String mcteParam_BANCOCOD = "//BANCOCOD";
  static final String mcteParam_EmpresaCodigo = "//CANAL";
  static final String mcteParam_SucursalCodigo = "//CANAL_SUCURSAL";
  static final String mcteParam_LegajoVend = "//LEGAJO_VEND";
  static final String mcteParam_LegajoCia = "//LEGAJCIA";
  /**
   * Nro NewSas para mcteParam_EmpresaCodigo(CANAL) = 150 (HSBC)
   */
  static final String mcteParam_NROSOLI = "//NROSOLI";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncInsertNode
   * static variable for method: fncPutSolHO
   * static variable for method: fncPutSolicitud
   */
  private final String wcteFnName = "fncPutSolicitud";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant wvarCodErr = new Variant();
    Variant pvarRes = new Variant();
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      pvarRequest = Strings.mid( pvarRequest, 1, Strings.find( 1, pvarRequest, ">" ) ) + "<MSGANO>" + DateTime.year( DateTime.now() ) + "</MSGANO>" + "<MSGMES>" + DateTime.month( DateTime.now() ) + "</MSGMES>" + "<MSGDIA>" + DateTime.day( DateTime.now() ) + "</MSGDIA>" + "<MSGHORA>" + DateTime.hour( DateTime.now() ) + "</MSGHORA>" + "<MSGMINUTO>" + DateTime.minute( DateTime.now() ) + "</MSGMINUTO>" + "<MSGSEGUNDO>" + DateTime.second( DateTime.now() ) + "</MSGSEGUNDO>" + Strings.mid( pvarRequest, (Strings.find( 1, pvarRequest, ">" ) + 1) );
      wvarStep = 10;
      wvarMensaje.set( "" );
      wvarCodErr.set( "" );
      pvarRes.set( "" );
      //
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(new Variant( pvarRequest )), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*unsup mobjCOM_Context.SetAbort() */;
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*unsup mobjCOM_Context.SetComplete() */;
        }
        return IAction_Execute;
      }

      pvarResponse.set( pvarRes );
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetComplete() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( Variant pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLParams = null;
    diamondedge.util.XmlDom wobjXMLReturnVal = null;
    diamondedge.util.XmlDom wobjXMLError = null;
    diamondedge.util.XmlDom wobjXMLRequestExists = null;
    Object wobjClass = null;
    Variant wvarMensajeStoreProc = new Variant();
    Variant wvarMensajePutTran = new Variant();
    String mvar_Estado = "";
    String mvarCertiSec = "";
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;

    //
    //
    //
    try 
    {
      //Le agrega los Nodos de Valor Fijo
      wvarStep = 10;
      wobjXMLParams = new diamondedge.util.XmlDom();
      //unsup wobjXMLParams.async = false;
      wobjXMLParams.load( System.getProperty("user.dir") + "\\" + mcteArchivoConfHOM_XML );
      //
      invoke( "fncInsertNode", new Variant[] { new Variant(pvarRequest), new Variant("//Request"), new Variant(diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( new Variant("//BROKERS/HOGAR/SOLICITUD") ) */ )), new Variant("TIPOOPERAC") } );
      invoke( "fncInsertNode", new Variant[] { new Variant(pvarRequest), new Variant("//Request"), new Variant(diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( new Variant("//BROKERS/HOGAR/RAMOPCOD") ) */ )), new Variant("RAMOPCOD") } );
      //
      //Instancia la clase de validacion
      wvarMensaje.set( "" );
      wvarMensajeStoreProc.set( "" );
      //
      wvarStep = 20;
      wobjClass = new LBA_InterWSBrok.GetValidacionSolHO();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(pvarRequest, wvarMensajeStoreProc, "")
      wobjClass = null;
      //
      //Analizo la respuesta del validador
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( wvarMensajeStoreProc.toString() );
      //
      //Si la respuesta viene con error
      wvarStep = 30;
      if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" ) */ ).equals( "0" )) )
      {
        wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" ) */ ) );
        wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) );
        
        if( wvarCodErr.toString().equals( "-2" ) )
        {
          //Este es el caso que no venga el cod. de broker o el nro. de operacion
          wvarStep = 35;
          mvar_Estado = "NI";
          pvarRes.set( wvarMensajeStoreProc );
          wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
        }
        else if( wvarCodErr.toString().equals( "H010" ) )
        {
          //Este es el caso que ya existe la solicitud
          wobjXMLRequestExists = new diamondedge.util.XmlDom();
          //unsup wobjXMLRequestExists.async = false;
          wobjXMLRequestExists.loadXML( pvarRequest.toString() );

          wvarStep = 40;
          wobjHSBC_DBCnn = new HSBC.DBConnection();
          //error: function 'GetDBConnection' was not found.
          //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
          wobjDBCmd = new Command();
          wobjDBCmd.setActiveConnection( wobjDBCnn );
          wobjDBCmd.setCommandText( mcteStoreProcSelect );
          wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
          //
          wvarStep = 50;
          wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestExists.selectSingleNode( "//CODINST" ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 4 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wvarStep = 52;
          wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestExists.selectSingleNode( "//REQUESTID" ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 14 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wvarStep = 54;
          wobjDBParm = new Parameter( "@WDB_TIPO_OPERACION", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "S" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wvarStep = 56;
          wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "OK" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wvarStep = 58;
          wrstRes = new Recordset();
          wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
          wvarCodErr.set( "0" );
          wvarMensaje.set( "" );
          pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + "<Response>" + "<REQUESTID>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequestExists.selectSingleNode( "//REQUESTID" ) */ ) + "</REQUESTID>" + "<CERTISEC>" + wrstRes.getFields().getField(12).getValue() + "</CERTISEC>" + "</Response>" + "</LBA_WS>" );
          mvarCertiSec = wrstRes.getFields().getField(12).getValue().toString();
          mvar_Estado = "OK";
          wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
          //Le agrego el Nodo de CertiSec al XML original
          invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvarCertiSec), new Variant("CERTISEC") } );
          wobjXMLRequestExists = (diamondedge.util.XmlDom) null;
          wobjDBCmd = (Command) null;
          wobjDBCnn = (Connection) null;
          wobjHSBC_DBCnn = null;
        }
        else
        {
          mvar_Estado = "ER";
          pvarRes.set( wvarMensajeStoreProc );
          wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );

        }

      }
      else
      {
        //Guarda la Solicitud en el SP
        wvarStep = 60;
        //Nueva funcion
        if( invoke( "fncPutSolicitud", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje.toString()), new Variant(wvarCodErr), new Variant(pvarRes) } ) )
        {

          wobjXMLReturnVal = new diamondedge.util.XmlDom();
          //unsup wobjXMLReturnVal.async = false;
          wobjXMLReturnVal.loadXML( pvarRes.toString() );
          //
          if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" ) */ ).equals( "0" )) )
          {
            //hubo un error
            wvarStep = 65;
            wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" ) */ ) );
            wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) );
            wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
            mvar_Estado = "ER";

            //Este es el cado en que no haya contestado el SP o sea un error no identificado
          }
          else
          {
            wvarStep = 70;
            mvar_Estado = "OK";
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//CERTISEC" ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//CERTISEC" ) */ ) );
            wvarMensajeStoreProc.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
          }
          //
          wobjXMLReturnVal = (diamondedge.util.XmlDom) null;

        }
        //
      }
      invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvar_Estado), new Variant("ESTADOPROC") } );
      //Alta de la OPERACION
      wvarStep = 75;
      if( invoke( "fncPutSolHO", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje.toString()), new Variant(wvarCodErr), new Variant(wvarMensajePutTran) } ) )
      {
        //
        wobjXMLReturnVal = new diamondedge.util.XmlDom();
        //unsup wobjXMLReturnVal.async = false;
        wobjXMLReturnVal.loadXML( wvarMensajePutTran.toString() );
        //
        wvarStep = 80;
        if( ! (diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" ) */ ).equals( "0" )) )
        {
          wvarCodErr.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" ) */ ) );
          wvarMensaje.set( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          fncGetAll = false;
          return fncGetAll;
        }
        //
        fncGetAll = true;
        //
      }
      fin: 
      wobjClass = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;
        /*unsup mobjCOM_Context.SetComplete() */;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }

  private boolean fncInsertNode( Variant pvarRequest, String pvarHeader, String pvarNodeValue, String pvarNodeName )
  {
    boolean fncInsertNode = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    diamondedge.util.XmlDom wobjXMLRequest = null;


    try 
    {

      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest.toString() );
      //
      wvarStep = 20;
      /*unsup wobjXMLRequest.selectSingleNode( pvarHeader ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), pvarNodeName, "" ) */ );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + pvarNodeName ) */, pvarNodeValue );
      //
      pvarRequest.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      fncInsertNode = true;
      return fncInsertNode;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncInsertNode = false;
        /*unsup mobjCOM_Context.SetComplete() */;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncInsertNode;
  }

  private boolean fncPutSolHO( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutSolHO = false;
    int wvarStep = 0;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_NROCOT = "";
    String mvarWDB_TIPOOPERAC = "";
    String mvarWDB_ESTADO = "";
    String mvarWDB_RECEPCION_PEDIDOANO = "";
    String mvarWDB_RECEPCION_PEDIDOMES = "";
    String mvarWDB_RECEPCION_PEDIDODIA = "";
    String mvarWDB_RECEPCION_PEDIDOHORA = "";
    String mvarWDB_RECEPCION_PEDIDOMINUTO = "";
    String mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_TIEMPOPROCESO = "";
    String mvarWDB_XML = "";
    String mvarWDB_CERTIANN = "";
    String mvarEmpresaCodigo = "";
    String mvarSucursalCodigo = "";

    //
    //
    //
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_CODINST = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }
      //
      wvarStep = 30;
      mvarWDB_REQUESTID = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID ) */ );
      }
      //
      wvarStep = 40;
      mvarWDB_NROCOT = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROCOT) ) */ == (org.w3c.dom.Node) null) )
      {
        if( new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NROCOT ) */ ) ).isNumeric() )
        {
          if( Strings.len( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROCOT) ) */ ) ) < 19 )
          {
            mvarWDB_NROCOT = String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NROCOT ) */ ) ) );
          }
        }
      }
      //
      wvarStep = 50;
      mvarWDB_TIPOOPERAC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoOperac) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPOOPERAC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoOperac ) */ );
      }
      //
      wvarStep = 60;
      mvarWDB_ESTADO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EstadoProc) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_ESTADO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_EstadoProc ) */ );
      }
      //
      wvarStep = 70;
      mvarWDB_RECEPCION_PEDIDOANO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoAno) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoAno ) */ );
      }
      //
      wvarStep = 80;
      mvarWDB_RECEPCION_PEDIDOMES = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMes) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMes ) */ );
      }
      //
      wvarStep = 90;
      mvarWDB_RECEPCION_PEDIDODIA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoDia) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDODIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoDia ) */ );
      }
      //
      wvarStep = 100;
      mvarWDB_RECEPCION_PEDIDOHORA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoHora) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOHORA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoHora ) */ );
      }
      //
      wvarStep = 110;
      mvarWDB_RECEPCION_PEDIDOMINUTO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMinuto) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMINUTO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMinuto ) */ );
      }
      //
      wvarStep = 120;
      mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoSegundo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOSEGUNDO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoSegundo ) */ );
      }
      //
      wvarStep = 130;
      mvarWDB_RAMOPCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAMOPCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_RAMOPCOD ) */ );
      }
      //
      wvarStep = 140;
      mvarWDB_POLIZANN = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ == (org.w3c.dom.Node) null) )
      {
        if( new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN ) */ ) ).isNumeric() )
        {
          if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ ) ) < 100 )
          {
            mvarWDB_POLIZANN = String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN ) */ ) ) );
          }
        }
      }
      //
      wvarStep = 150;
      mvarWDB_POLIZSEC = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ == (org.w3c.dom.Node) null) )
      {
        if( new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC ) */ ) ).isNumeric() )
        {
          if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ ) ) < 1000000 )
          {
            mvarWDB_POLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC ) */ );
          }
        }
      }
      //
      wvarStep = 160;
      mvarWDB_CERTISEC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CERTISEC) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTISEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CERTISEC ) */ );
      }
      //
      wvarStep = 170;
      mvarWDB_TIEMPOPROCESO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TiempoProceso) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIEMPOPROCESO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TiempoProceso ) */ );
      }
      //
      wvarStep = 171;
      mvarEmpresaCodigo = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EmpresaCodigo ) */ == (org.w3c.dom.Node) null) )
      {
        mvarEmpresaCodigo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EmpresaCodigo ) */ );
      }
      //
      wvarStep = 172;
      mvarSucursalCodigo = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SucursalCodigo ) */ == (org.w3c.dom.Node) null) )
      {
        mvarSucursalCodigo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SucursalCodigo ) */ );
      }


      wvarStep = 180;
      mvarWDB_XML = "";
      mvarWDB_XML = pvarRequest;
      //
      wvarStep = 190;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      //
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 14 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_NRO_COTIZACION_LBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NROCOT.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NROCOT)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 18 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 230;
      wobjDBParm = new Parameter( "@WDB_TIPO_OPERACION", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_TIPOOPERAC.equals( "" ) ? "" : mvarWDB_TIPOOPERAC) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 240;
      wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_ESTADO.equals( "" ) ? "" : mvarWDB_ESTADO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 250;
      wobjDBParm = new Parameter( "@WDB_RECEPCION_PEDIDO", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant( mvarWDB_RECEPCION_PEDIDOANO + "-" + mvarWDB_RECEPCION_PEDIDOMES + "-" + mvarWDB_RECEPCION_PEDIDODIA + " " + mvarWDB_RECEPCION_PEDIDOHORA + ":" + mvarWDB_RECEPCION_PEDIDOMINUTO + ":" + mvarWDB_RECEPCION_PEDIDOSEGUNDO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 260;
      wobjDBParm = new Parameter( "@WDB_ENVIO_RESPUESTA", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant( DateTime.year( DateTime.now() ) + "-" + DateTime.month( DateTime.now() ) + "-" + DateTime.day( DateTime.now() ) + " " + DateTime.hour( DateTime.now() ) + ":" + DateTime.minute( DateTime.now() ) + ":" + DateTime.second( DateTime.now() ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 270;
      wobjDBParm = new Parameter( "@WDB_RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_RAMOPCOD.equals( "" ) ? "" : mvarWDB_RAMOPCOD) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 280;
      wobjDBParm = new Parameter( "@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 290;
      wobjDBParm = new Parameter( "@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      // Solo para el HSBC
      mvarWDB_CERTIANN = "0";
      if( Obj.toInt( mvarEmpresaCodigo ) == 150 )
      {
        mvarWDB_CODINST = "150";
        mvarWDB_CERTIANN = mvarSucursalCodigo;
        //mvarWDB_CERTISEC = 0
      }
      //
      wvarStep = 300;
      wobjDBParm = new Parameter( "@WDB_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 310;
      wobjDBParm = new Parameter( "@WDB_CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_CERTIANN ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 320;
      wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CERTISEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CERTISEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 330;
      wobjDBParm = new Parameter( "@WDB_TIEMPO_PROCESO_AIS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIEMPOPROCESO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIEMPOPROCESO)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 340;
      wobjDBParm = new Parameter( "@WDB_OPERACION_XML", AdoConst.adVarChar, AdoConst.adParamInput, 8000, new Variant( (mvarWDB_XML.equals( "" ) ? "" : Strings.left( mvarWDB_XML, 8000 )) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 350;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      wobjDBCnn.close();

      fncPutSolHO = true;
      pvarRes.set( "<LBA_WS res_code=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" ) */ ) + "\" res_msg=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" ) */ ) + "\"></LBA_WS>" );

      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
      return fncPutSolHO;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        fncPutSolHO = false;
        //mobjCOM_Context.SetComplete
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutSolHO;
  }

  private boolean fncPutSolicitud( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutSolicitud = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    LBAA_MWGenerico.lbaw_MQMW wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
    HSBC.DBConnection wobjHSBC_DBCnn = new HSBC.DBConnection();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarcounter = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTIPOL = "";
    String mvarWDB_CERTIANN = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_SUPLENUM = "";
    String mvarWDB_NUMEDOCU = "";
    String mvarWDB_TIPODOCU = "";
    String mvarWDB_CLIENAP1 = "";
    String mvarWDB_CLIENAP2 = "";
    String mvarWDB_CLIENNOM = "";
    String mvarWDB_NACIMANN = "";
    String mvarWDB_NACIMMES = "";
    String mvarWDB_NACIMDIA = "";
    String mvarWDB_CLIENSEX = "";
    String mvarWDB_CLIENEST = "";
    String mvarWDB_EFECTANN = "";
    String mvarWDB_EFECTMES = "";
    String mvarWDB_EFECTDIA = "";
    String mvarWDB_EMAIL = "";
    String mvarWDB_DOMICDOM = "";
    String mvarWDB_DOMICDNU = "";
    String mvarWDB_DOMICPIS = "";
    String mvarWDB_DOMICPTA = "";
    String mvarWDB_DOMICPOB = "";
    String mvarWDB_BARRIOCOUNT = "";
    String mvarWDB_DOMICCPO = "";
    String mvarWDB_PROVICOD = "";
    String mvarTELCOD = "";
    String mvarWDB_TELNRO = "";
    String mvarWDB_DOMICDOM_CO = "";
    String mvarWDB_DOMICDNU_CO = "";
    String mvarWDB_DOMICPIS_CO = "";
    String mvarWDB_DOMICPTA_CO = "";
    String mvarWDB_DOMICPOB_CO = "";
    String mvarWDB_DOMICCPO_CO = "";
    String mvarWDB_PROVICOD_CO = "";
    String mvarTELCOD_CO = "";
    String mvarWDB_TELNRO_CO = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_CUENNUME = "";
    String mvarWDB_VENCIANN = "";
    String mvarWDB_VENCIMES = "";
    String mvarWDB_VENCIDIA = "";
    String mvarWDB_BANCOCOD = "";
    String mvarWDB_SUCURCOD = "";
    String mvarWDB_P_EMISIANN = "";
    String mvarWDB_P_EMISIMES = "";
    String mvarWDB_P_EMISIDIA = "";
    String mvarWDB_P_COBROCOD = "";
    String mvarWDB_P_TIVIVCOD = "";
    String mvarWDB_P_USOTIPOS = "";
    String mvarWDB_P_SWCALDER = "";
    String mvarWDB_P_SWASCENS = "";
    String mvarWDB_P_ALARMTIP = "";
    String mvarWDB_P_GUARDTIP = "";
    String mvarWDB_P_SWCREJAS = "";
    String mvarWDB_P_SWPBLIND = "";
    String mvarWDB_P_SWDISYUN = "";
    String mvarWDB_P_SWPROPIE = "";
    String mvarWDB_P_PLANNCOD = "";
    String mvarWDB_P_BARRIOCOUNT = "";
    String mvarWDB_P_ZONA = "";
    String mvarWDB_P_CLIENIVA = "";
    String mvarWDB_P_COBERCOD = "";
    String mvarWDB_P_CONTRMOD = "";
    String mvarWDB_P_CAPITASG = "";
    String mvarWDB_P_CAMPACOD = "";
    String mvarWDB_P_TIPO = "";
    String mvarWDB_P_MARCA = "";
    String mvarWDB_P_MODELO = "";
    String mvarNroCertiSec = "";
    String mvarDatosImpreso = "";
    String mvarNodoImpreso = "";
    String mvarImpreso64 = "";
    String mvarRequestImp = "";
    String mvarResponseImp = "";
    Recordset wrstDBResult = null;
    String mvarDOCUDES = "";
    String mvarCOBRODES = "";
    String mvarCAMPADES = "";
    String mvarNOMBROKER = "";
    String mvarPROVIDES = "";
    String mvarPROVIDESCO = "";
    String mvarRequest = "";
    String mvarResponse = "";
    diamondedge.util.XmlDom mobjXMLDoc = null;
    String mvarPLANDES = "";
    String mvarUSOTIDES = "";
    String mvarALARMDES = "";
    String mvarGUARDDES = "";
    String mvarCOBERDES = "";
    String mvarseltest = "";
    int wvarCount = 0;
    int mvarSumaCob = 0;
    int wvariCounter = 0;
    String mvarEmpresaCodigo = "";
    String mvarSucursalCodigo = "";
    String mvarLegajoVend = "";
    String mvarLegajoCia = "";
    String mvarNROSOLI = "";
    org.w3c.dom.Node oPlan = null;

    //
    //
    //
    //10/2010 agrega area
    //10/2010 agrega area
    //10/2010 variables para armar el impreso
    //fin impreso
    //AM
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_CODINST = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST ) */ );
      }
      //
      wvarStep = 30;
      mvarWDB_RAMOPCOD = "HOM1";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAMOPCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_RAMOPCOD ) */ );
      }
      //
      wvarStep = 40;
      mvarWDB_POLIZANN = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN ) */ );
      }
      //
      wvarStep = 50;
      mvarWDB_POLIZSEC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC ) */ );
      }
      //
      wvarStep = 60;
      mvarWDB_CERTISEC = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CERTISEC) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTISEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CERTISEC ) */ );
      }
      //
      wvarStep = 70;
      mvarWDB_P_EMISIANN = Strings.right( Strings.fill( 4, "0" ) + DateTime.year( DateTime.now() ), 4 );
      mvarWDB_P_EMISIMES = Strings.right( Strings.fill( 2, "0" ) + DateTime.month( DateTime.now() ), 2 );
      mvarWDB_P_EMISIDIA = Strings.right( Strings.fill( 2, "0" ) + DateTime.day( DateTime.now() ), 2 );
      //
      mvarWDB_EFECTANN = Strings.right( Strings.fill( 4, "0" ) + DateTime.year( DateTime.now() ), 4 );
      mvarWDB_EFECTMES = Strings.right( Strings.fill( 2, "0" ) + DateTime.month( DateTime.now() ), 2 );
      mvarWDB_EFECTDIA = Strings.right( Strings.fill( 2, "0" ) + DateTime.day( DateTime.now() ), 2 );
      //
      wvarStep = 80;
      mvarWDB_NUMEDOCU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NumeDocu) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NUMEDOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NumeDocu ) */ );
      }
      //
      wvarStep = 90;
      mvarWDB_TIPODOCU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoDocu) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPODOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoDocu ) */ );
      }
      //
      wvarStep = 100;
      mvarWDB_CLIENAP1 = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienAp1) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENAP1 = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienAp1 ) */ ) );
      }
      //
      wvarStep = 110;
      mvarWDB_CLIENAP2 = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienAp2) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENAP2 = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienAp2 ) */ ) );
      }
      //
      wvarStep = 120;
      mvarWDB_CLIENNOM = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienNom) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENNOM = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienNom ) */ ) );
      }
      //
      wvarStep = 130;
      mvarWDB_NACIMANN = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimAnn) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimAnn ) */ );
      }
      //
      wvarStep = 140;
      mvarWDB_NACIMMES = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimMes) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimMes ) */ );
      }
      //
      wvarStep = 150;
      mvarWDB_NACIMDIA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimDia) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimDia ) */ );
      }
      //
      wvarStep = 160;
      mvarWDB_CLIENSEX = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENSEX) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENSEX = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENSEX ) */ ) );
      }
      //
      wvarStep = 170;
      mvarWDB_CLIENEST = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENEST = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENEST ) */ ) );
      }

      //AM
      wvarStep = 171;
      mvarEmpresaCodigo = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EmpresaCodigo ) */ == (org.w3c.dom.Node) null) )
      {
        mvarEmpresaCodigo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EmpresaCodigo ) */ );
      }

      wvarStep = 172;
      mvarSucursalCodigo = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SucursalCodigo ) */ == (org.w3c.dom.Node) null) )
      {
        mvarSucursalCodigo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SucursalCodigo ) */ );
      }

      wvarStep = 173;
      mvarLegajoVend = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LegajoVend ) */ == (org.w3c.dom.Node) null) )
      {
        mvarLegajoVend = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LegajoVend ) */ );
      }

      wvarStep = 174;
      mvarLegajoCia = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LegajoCia ) */ == (org.w3c.dom.Node) null) )
      {
        mvarLegajoCia = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LegajoCia ) */ );
      }
      // FIN AM
      //
      wvarStep = 180;
      mvarWDB_EMAIL = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EMAIL) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_EMAIL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_EMAIL ) */ );
      }
      //
      wvarStep = 190;
      mvarWDB_DOMICDOM = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICDOM) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDOM = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICDOM ) */ ) );
      }
      //
      wvarStep = 200;
      mvarWDB_DOMICDNU = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDnu) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDNU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDnu ) */ );
      }
      //
      wvarStep = 210;
      mvarWDB_DOMICPIS = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICPIS) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPIS = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICPIS ) */ ) );
      }
      //
      wvarStep = 220;
      mvarWDB_DOMICPTA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPta) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPTA = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPta ) */ ) );
      }
      //
      wvarStep = 230;
      mvarWDB_DOMICPOB = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICPOB) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPOB = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICPOB ) */ ) );
      }
      //
      wvarStep = 240;
      mvarWDB_DOMICCPO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICCPO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCod ) */ );
      }
      //
      wvarStep = 250;
      mvarWDB_PROVICOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi ) */ );
      }
      //
      wvarStep = 260;
      //10/2010 se agrega area
      mvarTELCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TELCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarTELCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TELCOD ) */ );
      }

      mvarWDB_TELNRO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TELNRO) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TELNRO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TELNRO ) */ );
      }
      //
      wvarStep = 270;
      mvarWDB_BARRIOCOUNT = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_BARRIOCOUNT) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_BARRIOCOUNT = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_BARRIOCOUNT ) */ ) );
      }
      //
      wvarStep = 280;
      mvarWDB_DOMICDOM_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDomCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDOM_CO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDomCo ) */ ) );
      }
      //
      wvarStep = 290;
      mvarWDB_DOMICDNU_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDNUCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDNU_CO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDNUCo ) */ );
      }
      //
      wvarStep = 300;
      mvarWDB_DOMICPIS_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPisCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPIS_CO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPisCo ) */ ) );
      }
      //
      wvarStep = 310;
      mvarWDB_DOMICPTA_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPtaCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPTA_CO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPtaCo ) */ ) );
      }
      //
      wvarStep = 320;
      mvarWDB_DOMICPOB_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICPOBCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPOB_CO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICPOBCo ) */ ) );
      }
      //
      wvarStep = 330;
      mvarWDB_DOMICCPO_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCodCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICCPO_CO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCodCo ) */ );
      }
      //
      wvarStep = 340;
      mvarWDB_PROVICOD_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ProviCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICOD_CO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ProviCo ) */ );
      }
      //
      wvarStep = 350;
      //10/2010 se agrega area
      mvarTELCOD_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TELCOD_CO) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarTELCOD_CO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TELCOD_CO ) */ );
      }

      mvarWDB_TELNRO_CO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TelNroCo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TELNRO_CO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TelNroCo ) */ );
      }
      //
      wvarStep = 360;
      mvarWDB_COBROTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROTIP = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP ) */ ) );
      }
      //
      wvarStep = 370;
      mvarWDB_CUENNUME = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CUENNUME = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CUENNUME ) */ );
      }
      //
      wvarStep = 380;
      mvarWDB_VENCIANN = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VENCIANN) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_VENCIANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_VENCIANN ) */ );
      }
      //
      wvarStep = 390;
      mvarWDB_VENCIMES = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VENCIMES) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_VENCIMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_VENCIMES ) */ );
      }
      //
      wvarStep = 400;
      mvarWDB_VENCIDIA = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VENCIDIA) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_VENCIDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_VENCIDIA ) */ );
      }
      //
      wvarStep = 410;

      //AM  Verificar esta l�gica
      mvarWDB_BANCOCOD = "0";
      if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) == 4 )
      {
        mvarWDB_BANCOCOD = "0";
        mvarWDB_SUCURCOD = "0";
      }
      else
      {
        mvarWDB_BANCOCOD = "9000";
        mvarWDB_SUCURCOD = "8888";
      }
      // FIN AM
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_BANCOCOD ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_BANCOCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_BANCOCOD ) */ );
      }

      wvarStep = 420;
      mvarWDB_P_COBROCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_COBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD ) */ );
      }
      //
      wvarStep = 430;
      mvarWDB_P_TIVIVCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TIPOHOGAR) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_TIVIVCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_TIPOHOGAR ) */ );
      }
      //
      wvarStep = 440;
      mvarWDB_P_USOTIPOS = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_UsoTipos) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_USOTIPOS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_UsoTipos ) */ );
      }
      //
      wvarStep = 450;
      mvarWDB_P_SWCALDER = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWCALDER) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWCALDER = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWCALDER ) */ ) );
      }
      //
      wvarStep = 460;
      mvarWDB_P_SWASCENS = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWASCENS) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWASCENS = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWASCENS ) */ ) );
      }
      //
      wvarStep = 470;
      mvarWDB_P_ALARMTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ALARMTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_ALARMTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ALARMTIP ) */ );
      }
      //
      wvarStep = 480;
      mvarWDB_P_GUARDTIP = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_GUARDTIP) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_GUARDTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_GUARDTIP ) */ );
      }
      //
      wvarStep = 490;
      mvarWDB_P_SWCREJAS = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWCREJAS) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWCREJAS = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWCREJAS ) */ ) );
      }
      //
      wvarStep = 500;
      mvarWDB_P_SWPBLIND = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWPBLIND) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWPBLIND = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWPBLIND ) */ ) );
      }
      //
      wvarStep = 510;
      mvarWDB_P_SWDISYUN = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWDISYUN) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWDISYUN = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWDISYUN ) */ ) );
      }
      //
      wvarStep = 520;
      mvarWDB_P_SWPROPIE = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWPROPIE) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWPROPIE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWPROPIE ) */ );
      }
      //
      wvarStep = 530;
      mvarWDB_P_PLANNCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PLANNCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_PLANNCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_PLANNCOD ) */ );
      }
      //
      wvarStep = 540;
      mvarWDB_P_ZONA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ZONA) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_ZONA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_ZONA ) */ );
      }
      //
      wvarStep = 550;
      mvarWDB_P_CLIENIVA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CLIENIVA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENIVA ) */ );
      }
      //
      wvarStep = 560;
      mvarWDB_P_COBERCOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBERCOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_COBERCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBERCOD ) */ );
      }
      //
      wvarStep = 570;
      mvarWDB_P_CONTRMOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CONTRMOD) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CONTRMOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CONTRMOD ) */ );
      }
      //
      wvarStep = 580;
      mvarWDB_P_CAPITASG = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CAPITASG) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CAPITASG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CAPITASG ) */ );
      }
      //
      wvarStep = 590;
      mvarWDB_P_CAMPACOD = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CampaCod) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CAMPACOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_CampaCod ) */ );
        if( Obj.toInt( mvarWDB_P_CAMPACOD ) < 1 )
        {
          mvarWDB_P_CAMPACOD = "";
        }
      }
      //
      wvarStep = 600;
      mvarWDB_P_TIPO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Tipo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_TIPO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Tipo ) */ ) );
      }
      //
      wvarStep = 610;
      mvarWDB_P_MARCA = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Marca) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_MARCA = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Marca ) */ ) );
      }
      //
      wvarStep = 620;
      mvarWDB_P_MODELO = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Modelo) ) */ == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_MODELO = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//" + mcteParam_Modelo ) */ ) );
      }
      //
      wvarStep = 621;
      mvarNROSOLI = "0";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NROSOLI ) */ == (org.w3c.dom.Node) null) )
      {
        mvarNROSOLI = Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NROSOLI ) */ ) );
      }
      //
      wvarStep = 630;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      //
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProcAlta );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 640;
      //
      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_RAMOPCOD.equals( "" ) ? "" : mvarWDB_RAMOPCOD) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 650;
      wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 660;
      wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      // Solo para el HSBC
      mvarWDB_CERTIANN = "0";
      if( Obj.toInt( mvarEmpresaCodigo ) == 150 )
      {
        mvarWDB_CODINST = "150";
        mvarWDB_CERTIANN = mvarSucursalCodigo;
      }

      wvarStep = 670;
      wobjDBParm = new Parameter( "@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 680;
      wobjDBParm = new Parameter( "@CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_CERTIANN ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 690;
      wobjDBParm = new Parameter( "@CERTISEC", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 700;
      wobjDBParm = new Parameter( "@SUPLENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 710;
      wobjDBParm = new Parameter( "@USUARCOD", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_CODINST ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 720;
      wobjDBParm = new Parameter( "@NUMEDOCU", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( (mvarWDB_NUMEDOCU.equals( "" ) ? "" : mvarWDB_NUMEDOCU) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 730;
      wobjDBParm = new Parameter( "@TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPODOCU.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPODOCU)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 740;
      wobjDBParm = new Parameter( "@CLIENAP1", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( (mvarWDB_CLIENAP1.equals( "" ) ? "" : mvarWDB_CLIENAP1) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 750;
      wobjDBParm = new Parameter( "@CLIENAP2", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( (mvarWDB_CLIENAP2.equals( "" ) ? "" : mvarWDB_CLIENAP2) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 760;
      wobjDBParm = new Parameter( "@CLIENNOM", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( (mvarWDB_CLIENNOM.equals( "" ) ? "" : mvarWDB_CLIENNOM) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 770;
      wobjDBParm = new Parameter( "@NACIMANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NACIMANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NACIMANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 780;
      wobjDBParm = new Parameter( "@NACIMMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NACIMMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NACIMMES)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 790;
      wobjDBParm = new Parameter( "@NACIMDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NACIMDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NACIMDIA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 800;
      wobjDBParm = new Parameter( "@CLIENSEX", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_CLIENSEX.equals( "" ) ? "" : mvarWDB_CLIENSEX) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 810;
      wobjDBParm = new Parameter( "@CLIENEST", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_CLIENEST.equals( "" ) ? "" : mvarWDB_CLIENEST) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 820;
      wobjDBParm = new Parameter( "@PAISSCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "00" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 830;
      wobjDBParm = new Parameter( "@IDIOMCOD", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 840;
      wobjDBParm = new Parameter( "@NUMHIJOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 850;
      wobjDBParm = new Parameter( "@EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 860;
      wobjDBParm = new Parameter( "@EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTMES)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 870;
      wobjDBParm = new Parameter( "@EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTDIA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 880;
      // 10/2010 no es mas F persona f�sica
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTIP", adChar, adParamInput, 2, "F")
      wobjDBParm = new Parameter( "@CLIENTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "00" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 890;
      wobjDBParm = new Parameter( "@CLIENFUM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 900;
      wobjDBParm = new Parameter( "@CLIENDOZ", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 910;
      wobjDBParm = new Parameter( "@ABRIDTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 920;
      wobjDBParm = new Parameter( "@ABRIDNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 930;
      wobjDBParm = new Parameter( "@CLIENORG", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 940;
      wobjDBParm = new Parameter( "@PERSOTIP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "F" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 950;
      wobjDBParm = new Parameter( "@DOMICSEC", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 960;
      wobjDBParm = new Parameter( "@CLIENCLA", AdoConst.adChar, AdoConst.adParamInput, 9, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 970;
      wobjDBParm = new Parameter( "@FALLEANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 980;
      wobjDBParm = new Parameter( "@FALLEMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 990;
      wobjDBParm = new Parameter( "@FALLEDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1000;
      wobjDBParm = new Parameter( "@FBAJAANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1010;
      wobjDBParm = new Parameter( "@FBAJAMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1020;
      wobjDBParm = new Parameter( "@FBAJADIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1030;
      wobjDBParm = new Parameter( "@EMAIL", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant( (mvarWDB_EMAIL.equals( "" ) ? "" : mvarWDB_EMAIL) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1040;
      wobjDBParm = new Parameter( "@DOMICCAL", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1050;
      wobjDBParm = new Parameter( "@DOMICDOM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( (mvarWDB_DOMICDOM.equals( "" ) ? "" : mvarWDB_DOMICDOM) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1060;
      wobjDBParm = new Parameter( "@DOMICDNU", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( (mvarWDB_DOMICDNU.equals( "" ) ? "" : mvarWDB_DOMICDNU) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1070;
      wobjDBParm = new Parameter( "@DOMICESC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1080;
      wobjDBParm = new Parameter( "@DOMICPIS", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_DOMICPIS.equals( "" ) ? "" : mvarWDB_DOMICPIS) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1090;
      wobjDBParm = new Parameter( "@DOMICPTA", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_DOMICPTA.equals( "" ) ? "" : mvarWDB_DOMICPTA) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1100;
      wobjDBParm = new Parameter( "@DOMICPOB", AdoConst.adVarChar, AdoConst.adParamInput, 60, new Variant( (mvarWDB_DOMICPOB.equals( "" ) ? "" : mvarWDB_DOMICPOB) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1110;
      wobjDBParm = new Parameter( "@DOMICCPO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_DOMICCPO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_DOMICCPO)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1120;
      wobjDBParm = new Parameter( "@PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1130;
      wobjDBParm = new Parameter( "@TELCOD", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( mvarTELCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1140;
      wobjDBParm = new Parameter( "@TELNRO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( (mvarWDB_TELNRO.equals( "" ) ? "" : mvarWDB_TELNRO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1150;
      wobjDBParm = new Parameter( "@PRINCIPAL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "S" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1160;
      wobjDBParm = new Parameter( "@DOMICCAL_CO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1170;
      wobjDBParm = new Parameter( "@DOMICDOM_CO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( (mvarWDB_DOMICDOM_CO.equals( "" ) ? "" : mvarWDB_DOMICDOM_CO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1180;
      wobjDBParm = new Parameter( "@DOMICDNU_CO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( (mvarWDB_DOMICDNU_CO.equals( "" ) ? "" : mvarWDB_DOMICDNU_CO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1190;
      wobjDBParm = new Parameter( "@DOMICESC_CO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1200;
      wobjDBParm = new Parameter( "@DOMICPIS_CO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_DOMICPIS_CO.equals( "" ) ? "" : mvarWDB_DOMICPIS_CO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1210;
      wobjDBParm = new Parameter( "@DOMICPTA_CO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_DOMICPTA_CO.equals( "" ) ? "" : mvarWDB_DOMICPTA_CO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1220;
      wobjDBParm = new Parameter( "@DOMICPOB_CO", AdoConst.adVarChar, AdoConst.adParamInput, 60, new Variant( (mvarWDB_DOMICPOB_CO.equals( "" ) ? "" : mvarWDB_DOMICPOB_CO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1230;
      wobjDBParm = new Parameter( "@DOMICCPO_CO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_DOMICCPO_CO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_DOMICCPO_CO)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1240;
      wobjDBParm = new Parameter( "@PROVICOD_CO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD_CO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICOD_CO)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1250;
      wobjDBParm = new Parameter( "@TELCOD_CO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( mvarTELCOD_CO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1260;
      wobjDBParm = new Parameter( "@TELNRO_CO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( (mvarWDB_TELNRO_CO.equals( "" ) ? "" : mvarWDB_TELNRO_CO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1270;
      wobjDBParm = new Parameter( "@PRINCIPAL_CO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1280;
      wobjDBParm = new Parameter( "@TIPOCUEN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1290;
      wobjDBParm = new Parameter( "@COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_COBROTIP.equals( "" ) ? "" : mvarWDB_COBROTIP) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1300;
      wobjDBParm = new Parameter( "@BANCOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_BANCOCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_BANCOCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1310;
      wobjDBParm = new Parameter( "@SUCURCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_SUCURCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_SUCURCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1320;
      wobjDBParm = new Parameter( "@CUENTDC", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1330;
      wobjDBParm = new Parameter( "@CUENNUME", AdoConst.adChar, AdoConst.adParamInput, 16, new Variant( (mvarWDB_CUENNUME.equals( "" ) ? "" : mvarWDB_CUENNUME) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1340;
      wobjDBParm = new Parameter( "@TARJECOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1350;
      wobjDBParm = new Parameter( "@VENCIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VENCIANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VENCIANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1360;
      wobjDBParm = new Parameter( "@VENCIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VENCIMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VENCIMES)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1370;
      wobjDBParm = new Parameter( "@VENCIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VENCIDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VENCIDIA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1380;
      wobjDBParm = new Parameter( "@P_CIAASCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "0001" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1390;
      wobjDBParm = new Parameter( "@P_EMISIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_EMISIANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1400;
      wobjDBParm = new Parameter( "@P_EMISIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_EMISIMES)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1410;
      wobjDBParm = new Parameter( "@P_EMISIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_EMISIDIA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1420;
      wobjDBParm = new Parameter( "@P_EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTANN)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1430;
      wobjDBParm = new Parameter( "@P_EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTMES)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1440;
      wobjDBParm = new Parameter( "@P_EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTDIA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1450;
      wobjDBParm = new Parameter( "@P_USUARCOD", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_CODINST ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1460;
      wobjDBParm = new Parameter( "@P_SITUCPOL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "A" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1470;
      wobjDBParm = new Parameter( "@P_CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1480;
      wobjDBParm = new Parameter( "@P_NUMEDOCU", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( (mvarWDB_NUMEDOCU.equals( "" ) ? "" : mvarWDB_NUMEDOCU) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1490;
      wobjDBParm = new Parameter( "@P_TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPODOCU.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPODOCU)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1500;
      wobjDBParm = new Parameter( "@P_DOMICSEC1", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1510;
      wobjDBParm = new Parameter( "@P_DOMICSEC2", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1520;
      wobjDBParm = new Parameter( "@P_CUENTSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1530;
      wobjDBParm = new Parameter( "@P_COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_COBROCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_COBROCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1540;
      wobjDBParm = new Parameter( "@P_COBROFOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1550;
      wobjDBParm = new Parameter( "@P_COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_COBROTIP.equals( "" ) ? "" : mvarWDB_COBROTIP) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1560;
      wobjDBParm = new Parameter( "@P_TIVIVCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_TIVIVCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_TIVIVCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1570;
      wobjDBParm = new Parameter( "@P_ALTURNUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1580;
      wobjDBParm = new Parameter( "@P_USOTIPOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_USOTIPOS.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_USOTIPOS)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1590;
      wobjDBParm = new Parameter( "@P_SWCALDER", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_P_SWCALDER.equals( "" ) ? "" : mvarWDB_P_SWCALDER) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1600;
      wobjDBParm = new Parameter( "@P_SWASCENS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_P_SWASCENS.equals( "" ) ? "" : mvarWDB_P_SWASCENS) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1610;
      wobjDBParm = new Parameter( "@P_ALARMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_ALARMTIP.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_ALARMTIP)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1620;
      wobjDBParm = new Parameter( "@P_GUARDTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_GUARDTIP.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_GUARDTIP)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1630;
      wobjDBParm = new Parameter( "@P_SWCREJAS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_P_SWCREJAS.equals( "" ) ? "" : mvarWDB_P_SWCREJAS) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1640;
      wobjDBParm = new Parameter( "@P_SWPBLIND", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_P_SWPBLIND.equals( "" ) ? "" : mvarWDB_P_SWPBLIND) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1650;
      wobjDBParm = new Parameter( "@P_SWDISYUN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_P_SWDISYUN.equals( "" ) ? "" : mvarWDB_P_SWDISYUN) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1660;
      wobjDBParm = new Parameter( "@P_SWPROPIE", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_P_SWPROPIE.equals( "" ) ? "" : mvarWDB_P_SWPROPIE) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1670;
      wobjDBParm = new Parameter( "@P_PLANNCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_PLANNCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_PLANNCOD)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1680;
      wobjDBParm = new Parameter( "@P_PLANNDAB", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1690;
      wobjDBParm = new Parameter( "@P_BARRIOCOUNT", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( (mvarWDB_P_BARRIOCOUNT.equals( "" ) ? "" : mvarWDB_P_BARRIOCOUNT) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1700;
      wobjDBParm = new Parameter( "@P_ZONA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_ZONA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_ZONA)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1710;
      wobjDBParm = new Parameter( "@P_CLIENIVA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_P_CLIENIVA.equals( "" ) ? "" : mvarWDB_P_CLIENIVA) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1720;
      wobjDBParm = new Parameter( "@P_ASEG_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1730;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) */;
      //
      for( wvarcounter = 1; wvarcounter <= 10; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );

          if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_CampaCod ) */ ) ) < 1 )
          {
            diamondedge.util.XmlDom.setText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_CampaCod ) */, "" );
          }

          wobjDBParm = new Parameter( "@P_CAMPACOD_" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_CampaCod ) */ ) ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          wobjDBParm = new Parameter( "@P_CAMPACOD_" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
        }
      }
      //
      wobjXMLList = (org.w3c.dom.NodeList) null;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) */;
      //
      wvarStep = 1740;
      for( wvarcounter = 1; wvarcounter <= 30; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
          wobjDBParm = new Parameter( "@P_COBERCOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_COBERCOD ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 3 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_COBERORD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 1 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CAPITASG" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_CAPITASG ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CAPITIMP" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 100000 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CONTRMOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_CONTRMOD ) */ ) ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          wobjDBParm = new Parameter( "@P_COBERCOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 3 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_COBERORD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CAPITASG" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CAPITIMP" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_CONTRMOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
        }
      }
      //AM
      wvarStep = 1750;
      wobjDBParm = new Parameter( "@P_SUCURSAL_CODIGO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarSucursalCodigo ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1760;
      wobjDBParm = new Parameter( "@P_SUCURSAL_DESCRI", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      //AM
      wvarStep = 1770;
      wobjDBParm = new Parameter( "@P_LEGAJO_VEND", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarLegajoVend ) );
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@P_LEGAJO_VEND", adChar, adParamInput, 10, "")
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1780;
      wobjDBParm = new Parameter( "@P_APENOM_VEND", AdoConst.adChar, AdoConst.adParamInput, 50, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //11/2010 se agrega variable x actualizaci�n sp
      wvarStep = 1781;
      wobjDBParm = new Parameter( "@P_EMPL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //fin
      wvarStep = 1790;
      wobjDBParm = new Parameter( "@P_LEGAJO_GTE", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1800;
      wobjDBParm = new Parameter( "@P_APENOM_GTE", AdoConst.adChar, AdoConst.adParamInput, 50, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1810;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Elect ) */;
      //
      wvarStep = 1820;
      for( wvarcounter = 1; wvarcounter <= 10; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
          wobjDBParm = new Parameter( "@P_TIPO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_Tipo ) */ ) ) ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_MARCA" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_Marca ) */ ) ) ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_MODELO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLNode.selectSingleNode( mcteParam_Modelo ) */ ) ) ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          wobjDBParm = new Parameter( "@P_TIPO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( "" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_MARCA" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( "" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wobjDBParm = new Parameter( "@P_MODELO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( "" ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
        }
      }
      //
      wvarStep = 1830;
      wobjDBParm = new Parameter( "@P_SITUCEST", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      //Como no vienen asegurados, guardo todos estos datos en blanco
      for( wvarcounter = 1; wvarcounter <= 10; wvarcounter++ )
      {
        wobjDBParm = new Parameter( "@P_DOCUMDAT" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 9 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wobjDBParm = new Parameter( "@P_NOMBREAS" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 49, new Variant( "" ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }
      //
      //AM
      wvarStep = 1831;
      //LEGAJCIA
      wobjDBParm = new Parameter( "@P_EMPRESA_CODIGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarLegajoCia.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarLegajoCia)) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1832;
      //CANAL
      wobjDBParm = new Parameter( "@P_BANCOCOD_C", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarEmpresaCodigo ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1833;
      wobjDBParm = new Parameter( "@TIPO_PROD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( "PR" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1834;
      //mvarPRODUCTOR)
      wobjDBParm = new Parameter( "@NRO_PROD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1835;
      wobjDBParm = new Parameter( "@SN_EPOLIZA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1836;
      wobjDBParm = new Parameter( "@EMAIL_EPOLIZA", AdoConst.adVarChar, AdoConst.adParamInput, 50, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1837;
      wobjDBParm = new Parameter( "@SN_PRESTAMAIL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1838;
      wobjDBParm = new Parameter( "@PASSEPOL", AdoConst.adVarChar, AdoConst.adParamInput, 10, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1839;
      wobjDBParm = new Parameter( "@SWAPODER", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "N" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1840;
      wobjDBParm = new Parameter( "@OCUPACODCLIE", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1841;
      wobjDBParm = new Parameter( "@OCUPACODAPOD", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1842;
      wobjDBParm = new Parameter( "@CLIENSECAPOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1843;
      wobjDBParm = new Parameter( "@DOMISECAPOD", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( "" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 1844;
      wobjDBParm = new Parameter( "@CERTISECNEWSAS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNROSOLI ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //eg fin'
      //
      wvarStep = 1845;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      wobjDBCnn.close();

      //10/2010 se agrega el armado del tag con el impreso en base 64
      //se guarda el certisec otorgado por la sp, en una variable,ya que se ejecutara otra sp para obtener descripciones para el impreso.
      mvarNroCertiSec = wobjDBCmd.getParameters().getParameter("@CERTISEC").getValue().toString();

      //AM150 se saca el armado del impreso para HSBC
      if( ! (Obj.toInt( mvarEmpresaCodigo ) == 150) )
      {

        //Ejectua sp para descripciones
        wvarStep = 1890;
        wobjHSBC_DBCnn = new HSBC.DBConnection();
        wobjDBCnn = (Connection) (Connection)( wobjHSBC_DBCnn.GetDBConnection( General.mcteDB ) );
        wobjDBCmd = new Command();
        wobjDBCmd.setActiveConnection( wobjDBCnn );
        wobjDBCmd.setCommandText( mcteStoreProcImpre );
        wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

        wvarStep = 1901;
        wobjDBParm = new Parameter( "@USOTIPOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_USOTIPOS.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_USOTIPOS)) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1902;
        wobjDBParm = new Parameter( "@ALARMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_ALARMTIP.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_ALARMTIP)) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1903;
        wobjDBParm = new Parameter( "@GUARDTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_GUARDTIP.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_GUARDTIP)) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1910;
        wobjDBParm = new Parameter( "@DOCUMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPODOCU.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPODOCU)) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1920;
        wobjDBParm = new Parameter( "@COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_COBROTIP.equals( "" ) ? "" : mvarWDB_COBROTIP) ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1930;
        wobjDBParm = new Parameter( "@CODINST", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarWDB_CODINST ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1935;
        wobjDBParm = new Parameter( "@CODPROV", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICOD)) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1936;
        wobjDBParm = new Parameter( "@CODPROVCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD_CO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICOD_CO)) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1940;
        mvarseltest = mcteStoreProcImpre + " ";
        for( wvarCount = 0; wvarCount <= (wobjDBCmd.getParameters().getCount() - 1); wvarCount++ )
        {
          if( (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adChar) || (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adVarChar) )
          {
            mvarseltest = mvarseltest + "'" + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + "',";
          }
          else
          {
            mvarseltest = mvarseltest + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + ",";
          }
        }

        wrstDBResult = wobjDBCmd.execute();
        wrstDBResult.setActiveConnection( (Connection) null );

        wvarStep = 1950;
        if( ! (wrstDBResult.isEOF()) )
        {
          mvarDOCUDES = wrstDBResult.getFields().getField("DOCUDES").getValue().toString();
          mvarCOBRODES = wrstDBResult.getFields().getField("COBRODES").getValue().toString();
          //12/2010 se elimina tabla PROD_LBATCAMP
          //mvarNOMBROKER = wrstDBResult.Fields.Item("NOMBROKER").Value
          mvarPROVIDES = wrstDBResult.getFields().getField("PROVIDES").getValue().toString();
          mvarPROVIDESCO = wrstDBResult.getFields().getField("PROVIDESCO").getValue().toString();
          mvarUSOTIDES = wrstDBResult.getFields().getField("USOTIDES").getValue().toString();
          mvarALARMDES = wrstDBResult.getFields().getField("ALARMDES").getValue().toString();
          mvarGUARDDES = wrstDBResult.getFields().getField("GUARDDES").getValue().toString();
        }
        else
        {
          mvarDOCUDES = "";
          mvarCOBRODES = "";
          mvarNOMBROKER = "";
          mvarPROVIDES = "";
          mvarPROVIDESCO = "";
          mvarUSOTIDES = "";
          mvarALARMDES = "";
          mvarGUARDDES = "";
        }

        wvarStep = 1960;
        if( ! (wrstDBResult == (Recordset) null) )
        {
          if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
          {
            wrstDBResult.close();
          }
        }


        wvarStep = 1970;
        wrstDBResult = (Recordset) null;

        wobjDBCnn.close();
        //descripci�n del plan
        wvarStep = 1980;
        mvarRequest = "<Request><MSGCOD>0021</MSGCOD><CIAASCOD>0001</CIAASCOD><RAMOPCOD>HOM1</RAMOPCOD>";
        mvarRequest = mvarRequest + "<POLIZANN>" + mvarWDB_POLIZANN + "</POLIZANN>";
        mvarRequest = mvarRequest + "<POLIZSEC>" + mvarWDB_POLIZSEC + "</POLIZSEC>";
        mvarRequest = mvarRequest + "<EFECTANN>" + DateTime.year( DateTime.now() ) + "</EFECTANN>";
        mvarRequest = mvarRequest + "<EFECTMES>" + DateTime.month( DateTime.now() ) + "</EFECTMES>";
        mvarRequest = mvarRequest + "<EFECTDIA>" + DateTime.day( DateTime.now() ) + "</EFECTDIA></Request>";

        wobjClass = new lbawA_ProductorHgMQ.lbaw_GetPolPlanes();
        //error: function 'Execute' was not found.
        //unsup: Call wobjClass.Execute(mvarRequest, mvarResponse, "")
        wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
        mobjXMLDoc = new diamondedge.util.XmlDom();
        //unsup mobjXMLDoc.async = false;
        mobjXMLDoc.loadXML( mvarResponse );
        if( null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.getLength() > 0 )
        {
          for( int noPlan = 0; noPlan < null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.getLength(); noPlan++ )
          {
            oPlan = null /*unsup mobjXMLDoc.selectNodes( "//OPTION" ) */.item( noPlan );
            if( mvarWDB_P_PLANNCOD.equals( diamondedge.util.XmlDom.getText( oPlan.getAttributes().getNamedItem( "value" ) ) ) )
            {
              mvarPLANDES = diamondedge.util.XmlDom.getText( oPlan );
            }
          }
        }
        else
        {
          mvarPLANDES = "";
        }

        mobjXMLDoc = (diamondedge.util.XmlDom) null;

        //conforma xml para la impresi�n
        wvarStep = 2000;
        mvarDatosImpreso = "<Request>";
        mvarDatosImpreso = mvarDatosImpreso + "<REQUESTID>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ ) + "</REQUESTID>";
        mvarDatosImpreso = mvarDatosImpreso + "<CODINST>" + mvarWDB_CODINST + "</CODINST>";
        //12/2010 se elimina tabla PROD_LBATCAMP
        //mvarDatosImpreso = mvarDatosImpreso & "<USUARIO>" & mvarNOMBROKER & "</USUARIO>"
        mvarDatosImpreso = mvarDatosImpreso + "<USUARIO>" + mvarWDB_CODINST + "</USUARIO>";
        mvarDatosImpreso = mvarDatosImpreso + "<COT_NRO>0</COT_NRO>";
        mvarDatosImpreso = mvarDatosImpreso + "<PROCEANN>" + DateTime.year( DateTime.now() ) + "</PROCEANN>";
        mvarDatosImpreso = mvarDatosImpreso + "<PROCEMES>" + DateTime.month( DateTime.now() ) + "</PROCEMES>";
        mvarDatosImpreso = mvarDatosImpreso + "<PROCEDIA>" + DateTime.day( DateTime.now() ) + "</PROCEDIA>";
        mvarDatosImpreso = mvarDatosImpreso + "<POLIZANN>" + mvarWDB_POLIZANN + "</POLIZANN>";
        mvarDatosImpreso = mvarDatosImpreso + "<POLIZSEC>" + mvarWDB_POLIZSEC + "</POLIZSEC>";
        mvarDatosImpreso = mvarDatosImpreso + "<CERTIPOL>" + mvarWDB_CODINST + "</CERTIPOL>";
        mvarDatosImpreso = mvarDatosImpreso + "<CERTIANN>0000</CERTIANN>";
        mvarDatosImpreso = mvarDatosImpreso + "<CERTISEC>" + mvarNroCertiSec + "</CERTISEC>";
        mvarDatosImpreso = mvarDatosImpreso + "<PRECIO>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VALOR" ) */ ) + "</PRECIO>";
        mvarDatosImpreso = mvarDatosImpreso + "<TIPOHOGAR>" + mvarWDB_P_TIVIVCOD + "</TIPOHOGAR>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICDOM>" + mvarWDB_DOMICDOM + "</DOMICDOM>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICDNU>" + mvarWDB_DOMICDNU + "</DOMICDNU>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICPIS>" + mvarWDB_DOMICPIS + "</DOMICPIS>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICPTA>" + mvarWDB_DOMICPTA + "</DOMICPTA>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICPOB>" + mvarWDB_DOMICPOB + "</DOMICPOB>";
        mvarDatosImpreso = mvarDatosImpreso + "<PROVI>" + mvarWDB_PROVICOD + "</PROVI>";
        mvarDatosImpreso = mvarDatosImpreso + "<DESC_PROVI>" + mvarPROVIDES + "</DESC_PROVI>";
        mvarDatosImpreso = mvarDatosImpreso + "<LOCALIDADCOD>" + mvarWDB_DOMICCPO + "</LOCALIDADCOD>";
        mvarDatosImpreso = mvarDatosImpreso + "<TELCOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//TELCOD" ) */ ) + "</TELCOD>";
        mvarDatosImpreso = mvarDatosImpreso + "<TELNRO>" + mvarWDB_TELNRO + "</TELNRO>";
        //ciclo de coberturas
        mvarSumaCob = 0;
        mvarDatosImpreso = mvarDatosImpreso + "<COBERTURAS>";
        if( null /*unsup wobjXMLRequest.selectNodes( "//COBERTURAS/COBERTURA" ) */.getLength() > 0 )
        {
          wobjXMLList = (org.w3c.dom.NodeList) null;
          wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( "//COBERTURAS/COBERTURA" ) */;
          for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
          {
            //Ejectua sp para descripciones
            wvarStep = 20001;
            wobjHSBC_DBCnn = new HSBC.DBConnection();
            wobjDBCnn = (Connection) (Connection)( wobjHSBC_DBCnn.GetDBConnection( General.mcteDB ) );
            wobjDBCmd = new Command();
            wobjDBCmd.setActiveConnection( wobjDBCnn );
            wobjDBCmd.setCommandText( mcteStorePImpreCob );
            wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

            wvarStep = 20002;
            wobjDBParm = new Parameter( "@COBERCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_COBERCOD ) */ ) ) );
            wobjDBParm.setScale( (byte)( 0 ) );
            wobjDBParm.setPrecision( (byte)( 3 ) );
            wobjDBCmd.getParameters().append( wobjDBParm );
            wobjDBParm = (Parameter) null;

            wvarStep = 20003;
            mvarseltest = mcteStorePImpreCob + " ";
            for( wvarCount = 0; wvarCount <= (wobjDBCmd.getParameters().getCount() - 1); wvarCount++ )
            {
              if( (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adChar) || (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adVarChar) )
              {
                mvarseltest = mvarseltest + "'" + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + "',";
              }
              else
              {
                mvarseltest = mvarseltest + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + ",";
              }
            }

            wrstDBResult = wobjDBCmd.execute();
            wrstDBResult.setActiveConnection( (Connection) null );

            wvarStep = 20004;
            if( ! (wrstDBResult.isEOF()) )
            {
              mvarCOBERDES = wrstDBResult.getFields().getField("COBERDES").getValue().toString();
            }
            else
            {
              mvarCOBERDES = "";
            }

            wvarStep = 20005;
            if( ! (wrstDBResult == (Recordset) null) )
            {
              if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
              {
                wrstDBResult.close();
              }
            }

            wvarStep = 20006;
            wrstDBResult = (Recordset) null;
            wobjDBCnn.close();

            mvarDatosImpreso = mvarDatosImpreso + "<COBERTURA>";
            mvarDatosImpreso = mvarDatosImpreso + "<COBERCOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_COBERCOD ) */ ) + "</COBERCOD>";
            mvarDatosImpreso = mvarDatosImpreso + "<CONTRMOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CONTRMOD ) */ ) + "</CONTRMOD>";
            mvarDatosImpreso = mvarDatosImpreso + "<COBERDAB>" + mvarCOBERDES + "</COBERDAB>";
            mvarDatosImpreso = mvarDatosImpreso + "<CAPITASG>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CAPITASG ) */ ) + "</CAPITASG>";
            mvarSumaCob = mvarSumaCob + Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CAPITASG ) */ ) );
            mvarDatosImpreso = mvarDatosImpreso + "</COBERTURA>";
          }
          mvarDatosImpreso = mvarDatosImpreso + "</COBERTURAS>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<COBERTURA>";
          mvarDatosImpreso = mvarDatosImpreso + "<COBERCOD></COBERCOD>";
          mvarDatosImpreso = mvarDatosImpreso + "<CONTRMOD></CONTRMOD>";
          mvarDatosImpreso = mvarDatosImpreso + "<COBERDAB></COBERDAB>";
          mvarDatosImpreso = mvarDatosImpreso + "<CAPITASG></CAPITASG>";
          mvarDatosImpreso = mvarDatosImpreso + "</COBERTURA>";
          mvarDatosImpreso = mvarDatosImpreso + "</COBERTURAS>";
        }

        mvarDatosImpreso = mvarDatosImpreso + "<SUMATOT>" + mvarSumaCob + "</SUMATOT>";

        //fin coberturas
        mvarDatosImpreso = mvarDatosImpreso + "<PLANNCOD>" + mvarWDB_P_PLANNCOD + "</PLANNCOD>";
        mvarDatosImpreso = mvarDatosImpreso + "<PLANNDES>" + mvarPLANDES + "</PLANNDES>";
        mvarDatosImpreso = mvarDatosImpreso + "<COBROFOR>" + mvarCOBRODES + "</COBROFOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<COBROCOD>" + mvarWDB_P_COBROCOD + "</COBROCOD>";
        mvarDatosImpreso = mvarDatosImpreso + "<COBROTIP>" + mvarWDB_COBROTIP + "</COBROTIP>";
        // mvarDatosImpreso = mvarDatosImpreso & "<CUENNUME>" & mvarWDB_CUENNUME & "</CUENNUME>"
        //cbu
        if( Obj.toInt( mvarWDB_P_COBROCOD ) == 5 )
        {
          if( mvarWDB_COBROTIP.equals( "DB" ) )
          {
            mvarDatosImpreso = mvarDatosImpreso + "<CUENNUME>" + mvarWDB_CUENNUME + mvarWDB_VENCIMES + mvarWDB_VENCIANN + "</CUENNUME>";
          }
          else
          {
            mvarDatosImpreso = mvarDatosImpreso + "<CUENNUME>" + mvarWDB_CUENNUME + "</CUENNUME>";
          }
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<CUENNUME>" + mvarWDB_CUENNUME + "</CUENNUME>";
        }

        mvarDatosImpreso = mvarDatosImpreso + "<VENCIANN>" + mvarWDB_VENCIANN + "</VENCIANN>";
        mvarDatosImpreso = mvarDatosImpreso + "<VENCIMES>" + mvarWDB_VENCIMES + "</VENCIMES>";
        mvarDatosImpreso = mvarDatosImpreso + "<VENCIDIA>" + mvarWDB_VENCIDIA + "</VENCIDIA>";

        //ciclo de campa�as
        mvarDatosImpreso = mvarDatosImpreso + "<CAMPANIAS>";
        wvarStep = 2010;
        if( null /*unsup wobjXMLRequest.selectNodes( "//CAMPANIAS/CAMPANIA" ) */.getLength() > 0 )
        {
          wobjXMLList = (org.w3c.dom.NodeList) null;
          wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( "//CAMPANIAS/CAMPANIA" ) */;
          for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
          {
            mvarDatosImpreso = mvarDatosImpreso + "<CAMPANIA>";
            mvarDatosImpreso = mvarDatosImpreso + "<CAMPACOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( "//CAMPACOD" ) */ ) + "</CAMPACOD>";
            mvarDatosImpreso = mvarDatosImpreso + "</CAMPANIA>";
          }
          mvarDatosImpreso = mvarDatosImpreso + "</CAMPANIAS>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<CAMPANIA>";
          mvarDatosImpreso = mvarDatosImpreso + "<CAMPACOD></CAMPACOD>";
          mvarDatosImpreso = mvarDatosImpreso + "</CAMPANIA>";
          mvarDatosImpreso = mvarDatosImpreso + "</CAMPANIAS>";
        }
        //fin campa�as
        mvarDatosImpreso = mvarDatosImpreso + "<CLIENIVA>" + mvarWDB_P_CLIENIVA + "</CLIENIVA>";
        mvarDatosImpreso = mvarDatosImpreso + "<CLIENAP1>" + mvarWDB_CLIENAP1 + "</CLIENAP1>";
        mvarDatosImpreso = mvarDatosImpreso + "<CLIENAP2>" + mvarWDB_CLIENAP2 + "</CLIENAP2>";
        mvarDatosImpreso = mvarDatosImpreso + "<CLIENNOM>" + mvarWDB_CLIENNOM + "</CLIENNOM>";
        mvarDatosImpreso = mvarDatosImpreso + "<NACIMANN>" + mvarWDB_NACIMANN + "</NACIMANN>";
        mvarDatosImpreso = mvarDatosImpreso + "<NACIMMES>" + mvarWDB_NACIMMES + "</NACIMMES>";
        mvarDatosImpreso = mvarDatosImpreso + "<NACIMDIA>" + mvarWDB_NACIMDIA + "</NACIMDIA>";
        if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENSEX) ) */ ) ).equals( "M" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<SEXO>MASCULINO</SEXO>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<SEXO>FEMENINO</SEXO>";
        }
        if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) ) */ ) ).equals( "S" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<ESTADO>SOLTERO/A</ESTADO>";
        }
        else
        {
          if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) ) */ ) ).equals( "C" ) )
          {
            mvarDatosImpreso = mvarDatosImpreso + "<ESTADO>CASADO/A</ESTADO>";
          }
          else
          {
            if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) ) */ ) ).equals( "E" ) )
            {
              mvarDatosImpreso = mvarDatosImpreso + "<ESTADO>SEPARADO/A</ESTADO>";
            }
            else
            {
              if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) ) */ ) ).equals( "V" ) )
              {
                mvarDatosImpreso = mvarDatosImpreso + "<ESTADO>VIUDO/A</ESTADO>";
              }
              else
              {
                if( Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) ) */ ) ).equals( "D" ) )
                {
                  mvarDatosImpreso = mvarDatosImpreso + "<ESTADO>DIVORCIADO/A</ESTADO>";
                }
                else
                {
                  mvarDatosImpreso = mvarDatosImpreso + "<ESTADO></ESTADO>";
                }
              }
            }
          }
        }
        mvarDatosImpreso = mvarDatosImpreso + "<EMAIL>" + mvarWDB_EMAIL + "</EMAIL>";
        mvarDatosImpreso = mvarDatosImpreso + "<VIGENANN>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENANN" ) */ ) + "</VIGENANN>";
        mvarDatosImpreso = mvarDatosImpreso + "<VIGENMES>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENMES" ) */ ) + "</VIGENMES>";
        mvarDatosImpreso = mvarDatosImpreso + "<VIGENDIA>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//VIGENDIA" ) */ ) + "</VIGENDIA>";
        mvarDatosImpreso = mvarDatosImpreso + "<TIPODOCU>" + mvarDOCUDES + "</TIPODOCU>";
        mvarDatosImpreso = mvarDatosImpreso + "<NUMEDOCU>" + mvarWDB_NUMEDOCU + "</NUMEDOCU>";
        mvarDatosImpreso = mvarDatosImpreso + "<USOTIPOS>" + mvarWDB_P_USOTIPOS + "</USOTIPOS>";
        mvarDatosImpreso = mvarDatosImpreso + "<USOTIPODESC>" + mvarUSOTIDES + "</USOTIPODESC>";
        if( Strings.toUpperCase( mvarWDB_P_SWCALDER ).equals( "N" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<CALDERA>NO</CALDERA>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<CALDERA>SI</CALDERA>";
        }
        if( Strings.toUpperCase( mvarWDB_P_SWASCENS ).equals( "N" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<ASCENSOR>NO</ASCENSOR>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<ASCENSOR>SI</ASCENSOR>";
        }
        mvarDatosImpreso = mvarDatosImpreso + "<ALARMTIP>" + mvarWDB_P_ALARMTIP + "</ALARMTIP>";
        mvarDatosImpreso = mvarDatosImpreso + "<ALARMTIPDES>" + mvarALARMDES + "</ALARMTIPDES>";
        mvarDatosImpreso = mvarDatosImpreso + "<GUARDTIP>" + mvarWDB_P_GUARDTIP + "</GUARDTIP>";
        mvarDatosImpreso = mvarDatosImpreso + "<GUARDDES>" + mvarGUARDDES + "</GUARDDES>";
        if( Strings.toUpperCase( mvarWDB_P_SWCREJAS ).equals( "N" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<REJAS>NO</REJAS>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<REJAS>SI</REJAS>";
        }
        if( Strings.toUpperCase( mvarWDB_P_SWPBLIND ).equals( "N" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<PUERTABLIND>NO</PUERTABLIND>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<PUERTABLIND>SI</PUERTABLIND>";
        }
        if( Strings.toUpperCase( mvarWDB_P_SWDISYUN ).equals( "N" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<DISYUNTOR>NO</DISYUNTOR>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<DISYUNTOR>SI</DISYUNTOR>";
        }
        mvarDatosImpreso = mvarDatosImpreso + "<BARRIOCOUNT>" + mvarWDB_BARRIOCOUNT + "</BARRIOCOUNT>";
        //ciclo electrodomesticos
        mvarDatosImpreso = mvarDatosImpreso + "<ELECTRODOMESTICOS>";
        wvarStep = 2020;
        if( null /*unsup wobjXMLRequest.selectNodes( "//ELECTRODOMESTICOS/ELECTRODOMESTICO" ) */.getLength() > 0 )
        {
          wobjXMLList = (org.w3c.dom.NodeList) null;
          wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( "//ELECTRODOMESTICOS/ELECTRODOMESTICO" ) */;
          for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
          {
            mvarDatosImpreso = mvarDatosImpreso + "<ELECTRODOMESTICO>";
            mvarDatosImpreso = mvarDatosImpreso + "<TIPO>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( "TIPO" ) */ ) + "</TIPO>";
            mvarDatosImpreso = mvarDatosImpreso + "<MARCA>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( "MARCA" ) */ ) + "</MARCA>";
            mvarDatosImpreso = mvarDatosImpreso + "<MODELO>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( "MODELO" ) */ ) + "</MODELO>";
            mvarDatosImpreso = mvarDatosImpreso + "</ELECTRODOMESTICO>";
          }
          mvarDatosImpreso = mvarDatosImpreso + "</ELECTRODOMESTICOS>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<ELECTRODOMESTICO>";
          mvarDatosImpreso = mvarDatosImpreso + "<TIPO></TIPO>";
          mvarDatosImpreso = mvarDatosImpreso + "<MARCA></MARCA>";
          mvarDatosImpreso = mvarDatosImpreso + "<MODELO></MODELO>";
          mvarDatosImpreso = mvarDatosImpreso + "</ELECTRODOMESTICO>";
          mvarDatosImpreso = mvarDatosImpreso + "</ELECTRODOMESTICOS>";
        }
        //fin electrodomesticos
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICDOMCOR>" + mvarWDB_DOMICDOM_CO + "</DOMICDOMCOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICDNUCOR>" + mvarWDB_DOMICDNU_CO + "</DOMICDNUCOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICPISCOR>" + mvarWDB_DOMICPIS_CO + "</DOMICPISCOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICPTACOR>" + mvarWDB_DOMICPTA_CO + "</DOMICPTACOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICPOBCOR>" + mvarWDB_DOMICPOB_CO + "</DOMICPOBCOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<PROVICOR>" + mvarWDB_PROVICOD_CO + "</PROVICOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<DESC_PROVICOR>" + mvarPROVIDESCO + "</DESC_PROVICOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<LOCALIDADCODCOR>" + mvarWDB_DOMICCPO_CO + "</LOCALIDADCODCOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<TELCODCOR>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//TELCOD" ) */ ) + "</TELCODCOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<TELNROCOR>" + mvarWDB_TELNRO_CO + "</TELNROCOR>";
        mvarDatosImpreso = mvarDatosImpreso + "</Request>";
        //fin armado xml para el llamado a la impresion
        //Se generan los pdf en base 64, son 4 pdf, corresponden a 4 nodos a enviar
        //BRO_Solicitud_HOM1 va siempre
        //BRO_CertificadoProvisorioHOM1 va siempre
        //BRO_AutorizacionDebitoTarjHOM1 solo cuando es tarjeta
        //BRO_BRO_AutorizacionDebitoHOM1 solo cuando es cbu
        mvarImpreso64 = "";

        wvarStep = 2030;
        mvarRequestImp = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>" + mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_Solicitud_HOM1</reportId>" + "</Request>";

        wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
        wobjClass.Execute( mvarRequestImp, mvarResponseImp, "" );
        wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
        mobjXMLDoc = new diamondedge.util.XmlDom();
        //unsup mobjXMLDoc.async = false;
        mobjXMLDoc.loadXML( mvarResponseImp );
        //Verifica que haya respuesta de MQ
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Estado" ) */.getAttributes().getNamedItem( "resultado" ) ).equals( "true" ) )
        {
          //Verifica que exista el PDF si no error Middle
          if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//generatePdfReportReturn" ) */ == (org.w3c.dom.Node) null) )
          {
            mvarNodoImpreso = diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.getDocument().getDocumentElement().selectSingleNode( "//generatePdfReportReturn" ) */ );
          }
          else
          {
            //error de mdw
            mvarNodoImpreso = "No se pudo obtener el impreso MDW";
          }
        }
        else
        {
          mvarNodoImpreso = "No se pudo obtener el impreso MQ";
        }

        mobjXMLDoc = (diamondedge.util.XmlDom) null;
        mvarImpreso64 = mvarImpreso64 + "<IMPSOLI>" + mvarNodoImpreso + "</IMPSOLI>";
        wvarStep = 2040;
        mvarRequestImp = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>" + mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_CertificadoProvisorioHOM1</reportId>" + "</Request>";

        wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
        wobjClass.Execute( mvarRequestImp, mvarResponseImp, "" );
        wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
        mobjXMLDoc = new diamondedge.util.XmlDom();
        //unsup mobjXMLDoc.async = false;
        mobjXMLDoc.loadXML( mvarResponseImp );
        //Verifica que haya respuesta de MQ
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Estado" ) */.getAttributes().getNamedItem( "resultado" ) ).equals( "true" ) )
        {
          //Verifica que exista el PDF si no error Middle
          if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//generatePdfReportReturn" ) */ == (org.w3c.dom.Node) null) )
          {
            mvarNodoImpreso = diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.getDocument().getDocumentElement().selectSingleNode( "//generatePdfReportReturn" ) */ );
          }
          else
          {
            //error de mdw
            mvarNodoImpreso = "No se pudo obtener el impreso MDW";
          }
        }
        else
        {
          mvarNodoImpreso = "No se pudo obtener el impreso MQ";
        }

        mobjXMLDoc = (diamondedge.util.XmlDom) null;
        mvarImpreso64 = mvarImpreso64 + "<IMPCERTI>" + mvarNodoImpreso + "</IMPCERTI>";
        wvarStep = 2050;
        if( Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) ) */ ) ) == 4 )
        {
          mvarRequestImp = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>" + mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_AutorizacionDebitoTarjHOM1</reportId>" + "</Request>";
        }
        else
        {
          mvarRequestImp = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>" + mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_AutorizacionDebitoHOM1</reportId>" + "</Request>";
        }
        wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
        wobjClass.Execute( mvarRequestImp, mvarResponseImp, "" );
        wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
        mobjXMLDoc = new diamondedge.util.XmlDom();
        //unsup mobjXMLDoc.async = false;
        mobjXMLDoc.loadXML( mvarResponseImp );
        //Verifica que haya respuesta de MQ
        if( diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.selectSingleNode( "//Estado" ) */.getAttributes().getNamedItem( "resultado" ) ).equals( "true" ) )
        {
          //Verifica que exista el PDF si no error Middle
          if( ! (null /*unsup mobjXMLDoc.selectSingleNode( "//generatePdfReportReturn" ) */ == (org.w3c.dom.Node) null) )
          {
            mvarNodoImpreso = diamondedge.util.XmlDom.getText( null /*unsup mobjXMLDoc.getDocument().getDocumentElement().selectSingleNode( "//generatePdfReportReturn" ) */ );
          }
          else
          {
            //error de mdw
            mvarNodoImpreso = "No se pudo obtener el impreso MDW";
          }
        }
        else
        {
          mvarNodoImpreso = "No se pudo obtener el impreso MQ";
        }

        mobjXMLDoc = (diamondedge.util.XmlDom) null;
        mvarImpreso64 = mvarImpreso64 + "<IMPAUTORI>" + mvarNodoImpreso + "</IMPAUTORI>";

        // Libero los objetos
        wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;

      }
      else
      {

        mvarImpreso64 = "";
      }
      //fin impreso, Se agrega en pvarRes el nodo <PDF64>
      pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + "<Response>" + "<REQUESTID>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) ) */ ) + "</REQUESTID>" + "<CERTISEC>" + mvarNroCertiSec + "</CERTISEC>" + "<PDF64>" + mvarImpreso64 + "</PDF64>" + "</Response>" + "</LBA_WS>" );
      fncPutSolicitud = true;

      fin: 
      //libero los objectos
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = (HSBC.DBConnection) null;
      wrstRes = (Recordset) null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;

      return fncPutSolicitud;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncPutSolicitud = false;
        //mobjCOM_Context.SetComplete
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutSolicitud;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
