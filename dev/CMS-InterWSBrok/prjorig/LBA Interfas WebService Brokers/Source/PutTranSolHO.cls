VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "PutTranSolHO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements ObjectControl
Implements HSBCInterfaces.IAction

Const mcteClassName         As String = "LBA_InterWSBrok.PutTranSolHO"
Const mcteStoreProc         As String = "SPSNCV_BRO_ALTA_OPER"

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Parametros XML de Entrada
Const mcteParam_REQUESTID            As String = "REQUESTID"
Const mcteParam_CODINST              As String = "CODINST"
Const mcteParam_POLIZANN             As String = "POLIZANN"
Const mcteParam_POLIZSEC             As String = "POLIZSEC"
Const mcteParam_COBROCOD             As String = "COBROCOD"
Const mcteParam_COBROTIP             As String = "COBROTIP"
Const mcteParam_TIPOHOGAR            As String = "TIPOHOGAR"
Const mcteParam_PLANNCOD             As String = "PLANNCOD"
Const mcteParam_Provi                As String = "PROVI"
Const mcteParam_LocalidadCod         As String = "LOCALIDADCOD"
Const mcteParam_TipoOperac           As String = "TIPOOPERAC"
Const mcteParam_RAMOPCOD             As String = "RAMOPCOD"
Const mcteParam_CLIENIVA             As String = "CLIENIVA"
Const mcteParam_NROCOT               As String = "COT_NRO"
Const mcteParam_Estado               As String = "ESTADO"
Const mcteParam_PedidoAno            As String = "MSGANO"
Const mcteParam_PedidoMes            As String = "MSGMES"
Const mcteParam_PedidoDia            As String = "MSGDIA"
Const mcteParam_PedidoHora           As String = "MSGHORA"
Const mcteParam_PedidoMinuto         As String = "MSGMINUTO"
Const mcteParam_PedidoSegundo        As String = "MSGSEGUNDO"
Const mcteParam_TiempoProceso        As String = "TIEMPOPROCESO"
Const mcteParam_CERTISEC             As String = "CERTISEC"
Const mcteParam_EstadoProc           As String = "ESTADOPROC"
'DATOS DE LAS COBERTURAS
Const mcteNodos_Cober                As String = "//Request/COBERTURAS"
Const mcteParam_COBERCOD             As String = "COBERCOD"
Const mcteParam_CONTRMOD             As String = "CONTRMOD"
Const mcteParam_CAPITASG             As String = "CAPITASG"
'DATOS DE LAS CAMPA�AS
Const mcteNodos_Campa                As String = "//Request/CAMPANIAS"
Const mcteParam_CampaCod             As String = "CAMPACOD"
'PRODUCTOR
Const mctePORTAL                     As String = "LBA"
Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    'declaracion de variables
    Dim wvarMensaje         As String
    Dim wvarCodErr          As String
    Dim pvarRes             As String

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    wvarMensaje = ""
    wvarCodErr = ""
    pvarRes = ""
    pvarResponse = "<LBA_WS res_code=""0"" res_msg="""">" & pvarRes & "</LBA_WS>"
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarResponse = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    IAction_Execute = 1

    mobjCOM_Context.SetComplete
End Function

