VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "PutCotizacionHO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements ObjectControl
Implements HSBCInterfaces.IAction

Const mcteClassName         As String = "LBA_InterWSBrok.PutCotizacionHO"
Const mcteStoreProc         As String = "SPSNCV_BRO_COTI_HOM1_GRABA"

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Parametros XML de Entrada
Const mcteParam_REQUESTID            As String = "REQUESTID"
Const mcteParam_CODINST              As String = "CODINST"
Const mcteParam_POLIZANN             As String = "POLIZANN"
Const mcteParam_POLIZSEC             As String = "POLIZSEC"
'EG 10-2010 se cambia la constante
'Const mcteParam_COBROCOD             As String = "COBROCOD"
Const mcteParam_COBROCOD             As String = "FPAGO"
Const mcteParam_COBROTIP             As String = "COBROTIP"
Const mcteParam_TIPOHOGAR            As String = "TIPOHOGAR"
'EG 10-2010 se cambia la constante
'Const mcteParam_PLANNCOD             As String = "PLANNCOD"
Const mcteParam_PLANNCOD             As String = "PLAN"
'EG 10-2010 se cambia la constante
'Const mcteParam_Provi                As String = "PROVI"
Const mcteParam_Provi                As String = "PROVCOD"
'EG 10-2010 se cambia la constante
'Const mcteParam_LocalidadCod         As String = "LOCALIDADCOD"
Const mcteParam_LocalidadCod         As String = "LOCALIDAD"
Const mcteParam_TipoOperac           As String = "TIPOOPERAC"
Const mcteParam_RAMOPCOD             As String = "RAMOPCOD"
Const mcteParam_CLIENIVA             As String = "CLIENIVA"
Const mcteParam_NROCOT               As String = "COT_NRO"
'Const mcteParam_Estado               As String = "ESTADO"
Const mcteParam_PedidoAno            As String = "MSGANO"
Const mcteParam_PedidoMes            As String = "MSGMES"
Const mcteParam_PedidoDia            As String = "MSGDIA"
Const mcteParam_PedidoHora           As String = "MSGHORA"
Const mcteParam_PedidoMinuto         As String = "MSGMINUTO"
Const mcteParam_PedidoSegundo        As String = "MSGSEGUNDO"
Const mcteParam_CERTISEC             As String = "CERTISEC"
Const mcteParam_DOMICCPO             As String = "DOMICCPO"
Const mcteParam_ZONA                 As String = "CODZONA"
'DATOS DE LAS COBERTURAS
Const mcteNodos_Cober                As String = "//Request/COBERTURAS/COBERTURA"
Const mcteParam_COBERCOD             As String = "COBERCOD"
Const mcteParam_CONTRMOD             As String = "CONTRMOD"
Const mcteParam_CAPITASG             As String = "CAPITASG"
'DATOS DE LAS CAMPA�AS
Const mcteNodos_Campa                As String = "//Request/CAMPANIAS/CAMPANIA"
Const mcteParam_CampaCod             As String = "CAMPACOD"
'PRODUCTOR
Const mctePORTAL                     As String = "LBA"
'Horas de Ingreso y Salida
Const mcteParam_Recepcion            As String = "RECEPCION"
Const mcteParam_Envio                As String = "ENVIO"
Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    'declaracion de variables
    Dim wvarMensaje         As String
    Dim wvarCodErr          As String
    Dim pvarRes             As String

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    wvarMensaje = ""
    wvarCodErr = ""
    pvarRes = ""
    If Not fncPutAll(pvarRequest, wvarMensaje, wvarCodErr, pvarRes) Then
        If wvarCodErr <> mcteErrorInesperadoCod Then
            pvarResponse = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
        Else
            pvarResponse = pvarRes
        End If
        
        If mcteIfFunctionFailed_failClass Then
            wvarStep = 20
            IAction_Execute = 1
            mobjCOM_Context.SetAbort
        Else
            wvarStep = 30
            IAction_Execute = 0
            mobjCOM_Context.SetComplete
        End If
        Exit Function
    End If
    '
    pvarResponse = pvarRes
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarResponse = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    IAction_Execute = 1
    mobjCOM_Context.SetComplete
End Function
Private Function fncPutAll(ByVal pvarRequest As String, _
                           ByRef wvarMensaje As String, _
                           ByRef wvarCodErr As String, _
                           ByRef pvarRes As String) As Boolean

  Const wcteFnName                     As String = "fncPutAll"
  Dim wvarStep                         As Long
  '
  Dim wobjClass                        As HSBCInterfaces.IAction
  Dim wobjHSBC_DBCnn                   As HSBCInterfaces.IDBConnection
  Dim wobjXMLRequest                   As MSXML2.DOMDocument
  Dim wobjXMLList                      As MSXML2.IXMLDOMNodeList
  Dim wobjXMLNode                      As MSXML2.IXMLDOMNode
  
  '
  Dim wobjDBCnn                        As ADODB.Connection
  Dim wobjDBCmd                        As ADODB.Command
  Dim wobjDBParm                       As ADODB.Parameter
  Dim wrstRes                          As ADODB.Recordset
  '
  Dim wvarcounter                      As Integer
  Dim mvarWDB_CODINST                  As String
  Dim mvarWDB_RAMOPCOD                 As String
  Dim mvarWDB_POLIZANN                 As String
  Dim mvarWDB_POLIZSEC                 As String
  Dim mvarWDB_CERTISEC                 As String
  Dim mvarWDB_USUARCOD                 As String
  Dim mvarWDB_EFECTANN                 As String
  Dim mvarWDB_EFECTMES                 As String
  Dim mvarWDB_EFECTDIA                 As String
  Dim mvarWDB_CLIENTIP                 As String
  Dim mvarWDB_DOMICCPO                 As String
  Dim mvarWDB_PROVICOD                 As String
  Dim mvarWDB_COBROTIP                 As String
  Dim mvarWDB_P_CIAASCOD               As String
  Dim mvarWDB_P_EMISIANN               As String
  Dim mvarWDB_P_EMISIMES               As String
  Dim mvarWDB_P_EMISIDIA               As String
  Dim mvarWDB_P_COBROCOD               As String
  Dim mvarWDB_P_COBROFOR               As String
  Dim mvarWDB_P_TIVIVCOD               As String
  Dim mvarWDB_P_PLANNCOD               As String
  Dim mvarWDB_P_ZONA                   As String
  Dim mvarWDB_P_CLIENIVA               As String
                         
On Error GoTo ErrorHandler

'Alta de la OPERACION
  wvarStep = 10
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRequest)
  '
  wvarStep = 20
  mvarWDB_CODINST = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST) Is Nothing Then
     mvarWDB_CODINST = wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST).Text
  End If
  '
  wvarStep = 30
  mvarWDB_RAMOPCOD = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_RAMOPCOD) Is Nothing Then
     mvarWDB_RAMOPCOD = wobjXMLRequest.selectSingleNode("//" & mcteParam_RAMOPCOD).Text
  End If
  '
  wvarStep = 40
  mvarWDB_POLIZANN = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZANN) Is Nothing Then
     mvarWDB_POLIZANN = wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZANN).Text
  End If
  '
  wvarStep = 50
  mvarWDB_POLIZSEC = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZSEC) Is Nothing Then
     mvarWDB_POLIZSEC = wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZSEC).Text
  End If
  '
  wvarStep = 60
  mvarWDB_CERTISEC = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_CERTISEC) Is Nothing Then
     mvarWDB_CERTISEC = wobjXMLRequest.selectSingleNode("//" & mcteParam_CERTISEC).Text
  End If
  '
  wvarStep = 70
  mvarWDB_USUARCOD = mvarWDB_CODINST
  '
  wvarStep = 80
  mvarWDB_DOMICCPO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_LocalidadCod) Is Nothing Then
     mvarWDB_DOMICCPO = wobjXMLRequest.selectSingleNode("//" & mcteParam_LocalidadCod).Text
  End If
  '
  wvarStep = 90
  mvarWDB_PROVICOD = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_Provi) Is Nothing Then
     mvarWDB_PROVICOD = wobjXMLRequest.selectSingleNode("//" & mcteParam_Provi).Text
  End If
  '
  wvarStep = 100
  mvarWDB_P_EMISIANN = Right(String(4, "0") & Year(Now), 4)
  mvarWDB_P_EMISIMES = Right(String(2, "0") & Month(Now), 2)
  mvarWDB_P_EMISIDIA = Right(String(2, "0") & Day(Now), 2)
  '
  mvarWDB_EFECTANN = Right(String(4, "0") & Year(Now), 4)
  mvarWDB_EFECTMES = Right(String(2, "0") & Month(Now), 2)
  mvarWDB_EFECTDIA = Right(String(2, "0") & Day(Now), 2)
  '
  wvarStep = 110
  mvarWDB_COBROTIP = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROTIP) Is Nothing Then
     mvarWDB_COBROTIP = UCase(wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROTIP).Text)
  End If
  '
  wvarStep = 120
  mvarWDB_P_COBROCOD = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROCOD) Is Nothing Then
     mvarWDB_P_COBROCOD = wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROCOD).Text
  End If
  '
  wvarStep = 130
  mvarWDB_P_TIVIVCOD = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_TIPOHOGAR) Is Nothing Then
     mvarWDB_P_TIVIVCOD = wobjXMLRequest.selectSingleNode("//" & mcteParam_TIPOHOGAR).Text
  End If
  '
  wvarStep = 140
  mvarWDB_P_PLANNCOD = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PLANNCOD) Is Nothing Then
     mvarWDB_P_PLANNCOD = wobjXMLRequest.selectSingleNode("//" & mcteParam_PLANNCOD).Text
  End If
  '
  wvarStep = 150
  mvarWDB_P_ZONA = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_ZONA) Is Nothing Then
     mvarWDB_P_ZONA = wobjXMLRequest.selectSingleNode("//" & mcteParam_ZONA).Text
  End If
  '
  wvarStep = 160
  mvarWDB_P_CLIENIVA = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_CLIENIVA) Is Nothing Then
     mvarWDB_P_CLIENIVA = wobjXMLRequest.selectSingleNode("//" & mcteParam_CLIENIVA).Text
  End If
  '
  wvarStep = 170
  Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
  Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
  '
  Set wobjDBCmd = CreateObject("ADODB.Command")
  Set wobjDBCmd.ActiveConnection = wobjDBCnn
  wobjDBCmd.CommandText = mcteStoreProc
  wobjDBCmd.CommandType = adCmdStoredProc
  '
  wvarStep = 180
  '
  Set wobjDBParm = wobjDBCmd.CreateParameter("@RAMOPCOD", adChar, adParamInput, 4, IIf(mvarWDB_RAMOPCOD = "", Null, mvarWDB_RAMOPCOD))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 190
  Set wobjDBParm = wobjDBCmd.CreateParameter("@POLIZANN", adNumeric, adParamInput, , IIf(mvarWDB_POLIZANN = "", Null, mvarWDB_POLIZANN))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 200
  Set wobjDBParm = wobjDBCmd.CreateParameter("@POLIZSEC", adNumeric, adParamInput, , IIf(mvarWDB_POLIZSEC = "", Null, mvarWDB_POLIZSEC))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 6
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 210
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIPOL", adNumeric, adParamInput, , mvarWDB_CODINST)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 220
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIANN", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 230
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTISEC", adNumeric, adParamInputOutput, , IIf(mvarWDB_CERTISEC = "", Null, mvarWDB_CERTISEC))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 6
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 240
  Set wobjDBParm = wobjDBCmd.CreateParameter("@SUPLENUM", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 250
  Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARCOD", adChar, adParamInput, 10, IIf(mvarWDB_USUARCOD = "", Null, mvarWDB_USUARCOD))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 260
  Set wobjDBParm = wobjDBCmd.CreateParameter("@NUMEDOCU", adChar, adParamInput, 11, String(11, "0"))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 270
  Set wobjDBParm = wobjDBCmd.CreateParameter("@TIPODOCU", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 280
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENAP1", adChar, adParamInput, 20, "Cotiz. Innominada")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 290
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENAP2", adChar, adParamInput, 20, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 300
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENNOM", adChar, adParamInput, 20, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 310
  Set wobjDBParm = wobjDBCmd.CreateParameter("@NACIMANN", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 320
  Set wobjDBParm = wobjDBCmd.CreateParameter("@NACIMMES", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 330
  Set wobjDBParm = wobjDBCmd.CreateParameter("@NACIMDIA", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 340
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEX", adChar, adParamInput, 1, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 350
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENEST", adChar, adParamInput, 1, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 360
  Set wobjDBParm = wobjDBCmd.CreateParameter("@PAISSCOD", adChar, adParamInput, 2, "00")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 370
  Set wobjDBParm = wobjDBCmd.CreateParameter("@IDIOMCOD", adChar, adParamInput, 3, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 380
  Set wobjDBParm = wobjDBCmd.CreateParameter("@NUMHIJOS", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 390
  Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTANN", adNumeric, adParamInput, , IIf(mvarWDB_EFECTANN = "", Null, mvarWDB_EFECTANN))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 400
  Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTMES", adNumeric, adParamInput, , IIf(mvarWDB_EFECTMES = "", Null, mvarWDB_EFECTMES))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 410
  Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTDIA", adNumeric, adParamInput, , IIf(mvarWDB_EFECTDIA = "", Null, mvarWDB_EFECTDIA))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 420
   '10/2010 no es mas F persona f�sica
  'Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTIP", adChar, adParamInput, 2, "F")
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTIP", adChar, adParamInput, 2, "00")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 430
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENFUM", adChar, adParamInput, 1, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 440
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENDOZ", adChar, adParamInput, 1, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 450
  Set wobjDBParm = wobjDBCmd.CreateParameter("@ABRIDTIP", adChar, adParamInput, 2, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 460
  Set wobjDBParm = wobjDBCmd.CreateParameter("@ABRIDNUM", adChar, adParamInput, 10, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 470
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENORG", adChar, adParamInput, 3, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 480
  Set wobjDBParm = wobjDBCmd.CreateParameter("@PERSOTIP", adChar, adParamInput, 1, "F")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 490
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENCLA", adChar, adParamInput, 9, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 500
  Set wobjDBParm = wobjDBCmd.CreateParameter("@FALLEANN", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 510
  Set wobjDBParm = wobjDBCmd.CreateParameter("@FALLEMES", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 520
  Set wobjDBParm = wobjDBCmd.CreateParameter("@FALLEDIA", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 530
  Set wobjDBParm = wobjDBCmd.CreateParameter("@FBAJAANN", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 540
  Set wobjDBParm = wobjDBCmd.CreateParameter("@FBAJAMES", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 550
  Set wobjDBParm = wobjDBCmd.CreateParameter("@FBAJADIA", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 560
  Set wobjDBParm = wobjDBCmd.CreateParameter("@EMAIL", adChar, adParamInput, 60, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 570
  Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICCAL", adChar, adParamInput, 5, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 580
  Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICDOM", adChar, adParamInput, 30, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 590
  Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICDNU", adChar, adParamInput, 5, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 600
  Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICESC", adChar, adParamInput, 1, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 610
  Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICPIS", adChar, adParamInput, 4, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 620
  Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICPTA", adChar, adParamInput, 4, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 630
  Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICPOB", adVarChar, adParamInput, 60, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 640
  Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICCPO", adNumeric, adParamInput, , IIf(mvarWDB_DOMICCPO = "", Null, mvarWDB_DOMICCPO))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 5
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 650
  Set wobjDBParm = wobjDBCmd.CreateParameter("@PROVICOD", adNumeric, adParamInput, , IIf(mvarWDB_PROVICOD = "", Null, mvarWDB_PROVICOD))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 660
  Set wobjDBParm = wobjDBCmd.CreateParameter("@TELCOD", adChar, adParamInput, 5, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 670
  Set wobjDBParm = wobjDBCmd.CreateParameter("@TELNRO", adChar, adParamInput, 30, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 680
  Set wobjDBParm = wobjDBCmd.CreateParameter("@PRINCIPAL", adChar, adParamInput, 1, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 690
  Set wobjDBParm = wobjDBCmd.CreateParameter("@TIPOCUEN", adChar, adParamInput, 1, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 700
  Set wobjDBParm = wobjDBCmd.CreateParameter("@COBROTIP", adChar, adParamInput, 2, IIf(mvarWDB_COBROTIP = "", Null, mvarWDB_COBROTIP))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 710
  Set wobjDBParm = wobjDBCmd.CreateParameter("@BANCOCOD", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 720
  Set wobjDBParm = wobjDBCmd.CreateParameter("@SUCURCOD", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 730
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CUENTDC", adChar, adParamInput, 2, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 740
  Set wobjDBParm = wobjDBCmd.CreateParameter("@CUENNUME", adChar, adParamInput, 16, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 750
  Set wobjDBParm = wobjDBCmd.CreateParameter("@TARJECOD", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 760
  Set wobjDBParm = wobjDBCmd.CreateParameter("@VENCIANN", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 770
  Set wobjDBParm = wobjDBCmd.CreateParameter("@VENCIMES", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 780
  Set wobjDBParm = wobjDBCmd.CreateParameter("@VENCIDIA", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 790
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_CIAASCOD", adChar, adParamInput, 4, "0001")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 800
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_EMISIANN", adNumeric, adParamInput, , IIf(mvarWDB_P_EMISIANN = "", Null, mvarWDB_P_EMISIANN))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 810
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_EMISIMES", adNumeric, adParamInput, , IIf(mvarWDB_P_EMISIMES = "", Null, mvarWDB_P_EMISIMES))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 820
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_EMISIDIA", adNumeric, adParamInput, , IIf(mvarWDB_P_EMISIDIA = "", Null, mvarWDB_P_EMISIDIA))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 830
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_EFECTANN", adNumeric, adParamInput, , IIf(mvarWDB_EFECTANN = "", Null, mvarWDB_EFECTANN))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 840
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_EFECTMES", adNumeric, adParamInput, , IIf(mvarWDB_EFECTMES = "", Null, mvarWDB_EFECTMES))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 850
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_EFECTDIA", adNumeric, adParamInput, , IIf(mvarWDB_EFECTDIA = "", Null, mvarWDB_EFECTDIA))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 860
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_USUARCOD", adChar, adParamInput, 10, IIf(mvarWDB_USUARCOD = "", Null, mvarWDB_USUARCOD))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 870
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_SITUCPOL", adChar, adParamInput, 1, "A")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 880
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_CLIENSEC", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 9
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 890
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_NUMEDOCU", adChar, adParamInput, 11, String(11, "0"))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 900
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_TIPODOCU", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 910
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_DOMICSEC1", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 3
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 920
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_DOMICSEC2", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 3
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 930
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_CUENTSEC", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 3
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 940
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_COBROCOD", adNumeric, adParamInput, , IIf(mvarWDB_P_COBROCOD = "", Null, mvarWDB_P_COBROCOD))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 950
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_COBROFOR", adNumeric, adParamInput, , 1)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 1
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 960
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_COBROTIP", adChar, adParamInput, 2, IIf(mvarWDB_COBROTIP = "", Null, mvarWDB_COBROTIP))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 970
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_TIVIVCOD", adNumeric, adParamInput, , IIf(mvarWDB_P_TIVIVCOD = "", Null, mvarWDB_P_TIVIVCOD))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 980
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_ALTURNUM", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 990
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_USOTIPOS", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1000
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_SWCALDER", adChar, adParamInput, 1, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1010
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_SWASCENS", adChar, adParamInput, 1, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1020
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_ALARMTIP", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1030
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_GUARDTIP", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1040
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_SWCREJAS", adChar, adParamInput, 1, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1050
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_SWPBLIND", adChar, adParamInput, 1, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1060
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_SWDISYUN", adChar, adParamInput, 1, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1070
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_SWPROPIE", adChar, adParamInput, 1, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1080
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_PLANNCOD", adNumeric, adParamInput, , IIf(mvarWDB_P_PLANNCOD = "", Null, mvarWDB_P_PLANNCOD))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 3
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1090
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_PLANNDAB", adChar, adParamInput, 20, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1100
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_BARRIOCOUNT", adChar, adParamInput, 40, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1110
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_ZONA", adNumeric, adParamInput, , IIf(mvarWDB_P_ZONA = "", Null, mvarWDB_P_ZONA))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1120
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_CLIENIVA", adChar, adParamInput, 1, IIf(mvarWDB_P_CLIENIVA = "", Null, mvarWDB_P_CLIENIVA))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1130
  Set wobjDBParm = wobjDBCmd.CreateParameter("@P_ASEG_ADIC", adChar, adParamInput, 1, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 1140
  Set wobjXMLList = wobjXMLRequest.selectNodes(mcteNodos_Campa)
  '
  For wvarcounter = 1 To 10
  If wobjXMLList.length >= wvarcounter Then
    Set wobjXMLNode = wobjXMLList.Item(wvarcounter - 1)
    Set wobjDBParm = wobjDBCmd.CreateParameter("@P_CAMPACOD_" & CStr(wvarcounter), adChar, adParamInput, 4, wobjXMLNode.selectSingleNode(mcteParam_CampaCod).Text)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    Set wobjXMLNode = Nothing
  Else
    Set wobjDBParm = wobjDBCmd.CreateParameter("@P_CAMPACOD_" & CStr(wvarcounter), adChar, adParamInput, 4, "")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
  End If
  Next
  '
  Set wobjXMLList = Nothing
  Set wobjXMLList = wobjXMLRequest.selectNodes(mcteNodos_Cober)
  '
  wvarStep = 1150
  ' AM se cambio de 20 a 30
  For wvarcounter = 1 To 30
  If wobjXMLList.length >= wvarcounter Then
    Set wobjXMLNode = wobjXMLList.Item(wvarcounter - 1)
    Set wobjDBParm = wobjDBCmd.CreateParameter("@P_COBERCOD" & CStr(wvarcounter), adNumeric, adParamInput, , wobjXMLNode.selectSingleNode(mcteParam_COBERCOD).Text)
    wobjDBParm.NumericScale = 0
    wobjDBParm.Precision = 3
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@P_COBERORD" & CStr(wvarcounter), adNumeric, adParamInput, , 1)
    wobjDBParm.NumericScale = 0
    wobjDBParm.Precision = 2
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@P_CAPITASG" & CStr(wvarcounter), adNumeric, adParamInput, , wobjXMLNode.selectSingleNode(mcteParam_CAPITASG).Text)
    wobjDBParm.NumericScale = 0
    wobjDBParm.Precision = 15
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@P_CAPITIMP" & CStr(wvarcounter), adNumeric, adParamInput, , 100000)
    wobjDBParm.NumericScale = 0
    wobjDBParm.Precision = 15
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@P_CONTRMOD" & CStr(wvarcounter), adNumeric, adParamInput, , wobjXMLNode.selectSingleNode(mcteParam_CONTRMOD).Text)
    wobjDBParm.NumericScale = 0
    wobjDBParm.Precision = 2
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjXMLNode = Nothing
  Else
    Set wobjDBParm = wobjDBCmd.CreateParameter("@P_COBERCOD" & CStr(wvarcounter), adNumeric, adParamInput, , 0)
    wobjDBParm.NumericScale = 0
    wobjDBParm.Precision = 3
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@P_COBERORD" & CStr(wvarcounter), adNumeric, adParamInput, , 0)
    wobjDBParm.NumericScale = 0
    wobjDBParm.Precision = 2
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@P_CAPITASG" & CStr(wvarcounter), adNumeric, adParamInput, , 0)
    wobjDBParm.NumericScale = 0
    wobjDBParm.Precision = 15
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@P_CAPITIMP" & CStr(wvarcounter), adNumeric, adParamInput, , 0)
    wobjDBParm.NumericScale = 0
    wobjDBParm.Precision = 15
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@P_CONTRMOD" & CStr(wvarcounter), adNumeric, adParamInput, , 0)
    wobjDBParm.NumericScale = 0
    wobjDBParm.Precision = 2
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
  End If
  Next
  '
  wvarStep = 1160
  '
  wobjDBCmd.Execute adExecuteNoRecords
  wobjDBCnn.Close
  fncPutAll = True
  wobjXMLRequest.selectSingleNode("//CERTISEC").Text = wobjDBCmd.Parameters("@CERTISEC").Value
  pvarRes = wobjXMLRequest.xml

fin:
'libero los objectos
  Set wrstRes = Nothing
  Set wobjDBCmd = Nothing
  Set wobjDBCnn = Nothing
  Set wobjHSBC_DBCnn = Nothing
  Set wobjXMLRequest = Nothing
  Set wobjClass = Nothing
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
wvarCodErr = mcteErrorInesperadoCod


    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    fncPutAll = False
    mobjCOM_Context.SetComplete

End Function




