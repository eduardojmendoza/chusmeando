VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "GetNroCotATM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "LBA_InterWSBrok.GetNroCotATM"
Const mcteStoreProc             As String = "SPSNCV_BRO_COTIZACION_ATM"

'Archivo de Errores
Const mcteArchivoATM_XML            As String = "LBA_VALIDACION_COT_ATM.XML"

'Parametros XML de Entrada
Const mcteParam_REQUESTID            As String = "REQUESTID"
Const mcteParam_CODINST              As String = "CODINST"
Const mcteParam_POLIZANN             As String = "POLIZANN"
Const mcteParam_POLIZSEC             As String = "POLIZSEC"
Const mcteParam_COBROCOD             As String = "FPAGO"
Const mcteParam_COBROTIP             As String = "COBROTIP"
Const mcteParam_Provi                As String = "PROVCOD"
Const mcteParam_LocalidadCod         As String = "LOCALIDAD"
Const mcteParam_CLIENIVA             As String = "CLIENIVA"


Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    
    'Declaracion de variables
    Dim wvarMensaje         As String
    Dim pvarRes             As String
    Dim wvarCodErr          As String

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    pvarRes = ""
    wvarCodErr = ""
    wvarMensaje = ""
    If Not fncGetNroCot(pvarRequest, wvarMensaje, wvarCodErr, pvarRes) Then
            pvarResponse = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
        If mcteIfFunctionFailed_failClass Then
            wvarStep = 20
            IAction_Execute = 1
            mobjCOM_Context.SetAbort
        Else
            wvarStep = 30
            IAction_Execute = 0
            mobjCOM_Context.SetComplete
        End If
        Exit Function
    End If
    '
    pvarResponse = "<LBA_WS res_code=""0"" res_msg="""">" & pvarRes & "</LBA_WS>"
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarResponse = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"

    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    IAction_Execute = 1
    mobjCOM_Context.SetComplete
End Function

Private Function fncGetNroCot(ByVal pvarRequest As String, _
                                ByRef wvarMensaje As String, _
                                ByRef wvarCodErr As String, _
                                ByRef pvarRes As String) As Boolean

  Const wcteFnName          As String = "fncGetNroCot"
  Dim wvarStep              As Long
  '
  Dim wobjXMLRequest        As MSXML2.DOMDocument
  Dim wobjXML               As MSXML2.DOMDocument
  Dim wobjXMLList           As MSXML2.IXMLDOMNodeList
  '
  Dim wobjHSBC_DBCnn        As HSBCInterfaces.IDBConnection
  Dim wobjDBCnn             As ADODB.Connection
  Dim wobjDBCmd             As ADODB.Command
  Dim wobjDBParm            As ADODB.Parameter
  Dim wrstRes               As ADODB.Recordset
  '
  Dim wvarContChild         As Long
  Dim wvarContNode          As Long
  '
  Dim mvarWDB_CODINST       As String
  Dim mvarWDB_POLIZANN      As String
  Dim mvarWDB_POLIZSEC      As String
  Dim mvarWDB_PROVI         As String
  Dim mvarWDB_LOCALIDADCOD  As String
  Dim mvarWDB_COBROCOD      As String
  Dim mvarWDB_COBROTIP      As String
  Dim mvarWDB_REQUESTID     As String
  Dim mvarWDB_USUARCOD      As String
  
  Dim varPlanDefec          As String
    '
  On Error GoTo ErrorHandler
  '
  wvarStep = 10
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRequest)
  '
  wvarStep = 20
  mvarWDB_REQUESTID = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_REQUESTID) Is Nothing Then
     mvarWDB_REQUESTID = wobjXMLRequest.selectSingleNode("//" & mcteParam_REQUESTID).Text
  End If
  '
  wvarStep = 30
  mvarWDB_CODINST = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST) Is Nothing Then
     mvarWDB_CODINST = wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST).Text
  End If
  '
  wvarStep = 40
  mvarWDB_POLIZANN = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZANN) Is Nothing Then
     mvarWDB_POLIZANN = wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZANN).Text
  End If
  '
  wvarStep = 50
  mvarWDB_POLIZSEC = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZSEC) Is Nothing Then
     mvarWDB_POLIZSEC = wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZSEC).Text
  End If
  '
  wvarStep = 70
  mvarWDB_PROVI = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_Provi) Is Nothing Then
     mvarWDB_PROVI = wobjXMLRequest.selectSingleNode("//" & mcteParam_Provi).Text
  End If
  '
  wvarStep = 80
  mvarWDB_LOCALIDADCOD = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_LocalidadCod) Is Nothing Then
     mvarWDB_LOCALIDADCOD = wobjXMLRequest.selectSingleNode("//" & mcteParam_LocalidadCod).Text
  End If
  '
  wvarStep = 90
  mvarWDB_COBROCOD = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROCOD) Is Nothing Then
     mvarWDB_COBROCOD = wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROCOD).Text
  End If
  '
  wvarStep = 100
  mvarWDB_COBROTIP = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROTIP) Is Nothing Then
     mvarWDB_COBROTIP = wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROTIP).Text
  End If
  '

  Set wobjXMLList = Nothing
  '
  wvarStep = 130
  Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
  Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
  Set wobjDBCmd = CreateObject("ADODB.Command")
  Set wobjDBCmd.ActiveConnection = wobjDBCnn
  wobjDBCmd.CommandText = mcteStoreProc
  wobjDBCmd.CommandType = adCmdStoredProc
  '
  wvarStep = 140
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CODINST", adNumeric, adParamInput, , IIf(mvarWDB_CODINST = "", 0, mvarWDB_CODINST))
  'wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 150
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_POLIZANN", adNumeric, adParamInput, , IIf(mvarWDB_POLIZANN = "", 0, mvarWDB_POLIZANN))
  'wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 160
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_POLIZSEC", adNumeric, adParamInput, , IIf(mvarWDB_POLIZSEC = "", 0, mvarWDB_POLIZSEC))
  'wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 6
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 180
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_PROVICOD", adNumeric, adParamInput, , IIf(mvarWDB_PROVI = "", 0, mvarWDB_PROVI))
  'wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 190
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CODPOST", adNumeric, adParamInput, , IIf(mvarWDB_LOCALIDADCOD = "", 0, mvarWDB_LOCALIDADCOD))
  'wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 5
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 200
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_COBROCOD", adNumeric, adParamInput, , IIf(mvarWDB_COBROCOD = "", 0, mvarWDB_COBROCOD))
  'wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 210
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_COBROTIP", adChar, adParamInput, 2, IIf(mvarWDB_COBROTIP = "", "", mvarWDB_COBROTIP))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 220
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_NRO_OPERACION_BROKER", adNumeric, adParamInput, , IIf(mvarWDB_REQUESTID = "", 0, mvarWDB_REQUESTID))
  'wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 14
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 230
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_NROCOT", adNumeric, adParamInputOutput, , 0)
  'wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 7
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 240
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CERTISEC", adNumeric, adParamInputOutput, , 0)
  'wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 6
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 245
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_USUARCOD", adChar, adParamInputOutput, 10, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 250
  Set wrstRes = CreateObject("ADODB.Recordset")
  wrstRes.Open wobjDBCmd
  Set wrstRes.ActiveConnection = Nothing
          
  Set wobjXML = CreateObject("MSXML2.DOMDocument")
  wobjXML.async = False
  Call wobjXML.Load(App.Path & "\" & mcteArchivoATM_XML)
    
  wvarStep = 260
  'SI NO HUBO ERROR AGREGO LOS DATOS AL XML DE ENTRADA PARA LA RESPUESTA
  If Not wrstRes.EOF Then
      If UCase(Trim(wrstRes.Fields(0).Value)) = "OK" Then
        If UCase(wobjXMLRequest.selectSingleNode("//Request/TIPOOPERAC").Text) = "C" Then
            Call wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(NODE_ELEMENT, "COT_NRO", ""))
        End If
        wvarStep = 270
        wobjXMLRequest.selectSingleNode("//COT_NRO").Text = wobjDBCmd.Parameters("@WDB_NROCOT").Value
        
        Call wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(NODE_ELEMENT, "CERTISEC", ""))
        wobjXMLRequest.selectSingleNode("//CERTISEC").Text = wobjDBCmd.Parameters("@WDB_CERTISEC").Value
        
        Call wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(NODE_ELEMENT, "USUARCOD", ""))
        mvarWDB_USUARCOD = wobjDBCmd.Parameters("@WDB_USUARCOD").Value
        wobjXMLRequest.selectSingleNode("//USUARCOD").Text = mvarWDB_USUARCOD
        
        'pvarRes = wobjXMLRequest.xml
        wvarCodErr = 0
        wvarMensaje = ""
      Else
        'error
        wvarStep = 280
        If wobjXML.selectNodes("//ERRORES/ATM/RESPUESTAS/ERROR[CODIGO='" & wrstRes.Fields(0).Value & "']").length > 0 Then
          wvarMensaje = wobjXML.selectSingleNode("//ERRORES/ATM/RESPUESTAS/ERROR[CODIGO='" & wrstRes.Fields(0).Value & "']/TEXTOERROR").Text
          wvarCodErr = wobjXML.selectSingleNode("//ERRORES/ATM/RESPUESTAS/ERROR[CODIGO='" & wrstRes.Fields(0).Value & "']/VALORRESPUESTA").Text
        Else
          wvarCodErr = mcteErrorInesperadoCod
          wvarMensaje = mcteErrorInesperadoDescr
        End If
        '
        fncGetNroCot = False
        Exit Function
      End If
  Else
      'error
      wvarCodErr = -100
      wvarMensaje = "No se retornaron datos de la base"
  End If
  '
  wvarStep = 290
  Set wobjXML = Nothing

  If fncValidaMQ(mvarWDB_POLIZANN, mvarWDB_POLIZSEC, mvarWDB_COBROCOD, mvarWDB_COBROTIP, wvarMensaje, wvarCodErr, pvarRes) Then
    If fncValidaPolizasMQ(mvarWDB_USUARCOD, mvarWDB_POLIZSEC, mvarWDB_POLIZANN, varPlanDefec, wvarCodErr, wvarMensaje, pvarRes) Then
     'agrego el PlanDefec
     Call wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(NODE_ELEMENT, "TPLANATM", ""))
        wobjXMLRequest.selectSingleNode("//TPLANATM").Text = varPlanDefec
        pvarRes = wobjXMLRequest.xml
      fncGetNroCot = True
    Else
     fncGetNroCot = False
    End If
  Else
    fncGetNroCot = False
  End If
  '
  wrstRes.Close
  wobjDBCnn.Close
fin:
'libero los objectos
  Set wrstRes = Nothing
  Set wobjDBCmd = Nothing
  Set wobjDBCnn = Nothing
  Set wobjHSBC_DBCnn = Nothing
  Set wobjXMLRequest = Nothing
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

wvarCodErr = mcteErrorInesperadoCod
wvarMensaje = mcteErrorInesperadoDescr
pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
  
  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  fncGetNroCot = False
  mobjCOM_Context.SetComplete
  GoTo fin
End Function

Private Function fncValidaMQ(varPolizann As String, varPolizsec As String, varCobrocod As String, varCobrotip As String, ByRef wvarMensaje As String, wvarCodErr As String, pvarRes As String) As Boolean
  Const wcteFnName              As String = "fncValidaMQ"
  Dim wvarStep    As Long
  
  Dim mvarRequest As String
  Dim mvarResponse As String
  
  Dim oProd
  Dim mobjXMLDoc   As MSXML2.DOMDocument
  Dim wobjClass    As HSBCInterfaces.IAction
  Dim mvarProdok As String
  Dim wobjXMLList               As MSXML2.IXMLDOMNodeList
  Dim wvarContNode              As Long
  'f ch.r
  
  wvarStep = 0
  wvarCodErr = 0
  On Error GoTo ErrorHandler
' valida cobro
  wvarStep = 2

  mvarRequest = "<Request><DEFINICION>FormasDePagoxPolizaColectiva.xml</DEFINICION>"
  mvarRequest = mvarRequest & "<USUARCOD></USUARCOD>"
  mvarRequest = mvarRequest & "<RAMOPCOD>ATD1</RAMOPCOD>"
  mvarRequest = mvarRequest & "<POLIZANN>" & varPolizann & "</POLIZANN>"
  mvarRequest = mvarRequest & "<POLIZSEC>" & varPolizsec & "</POLIZSEC></Request>"

  Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVMQGen.lbaw_MQMensaje")
  Call wobjClass.Execute(mvarRequest, mvarResponse, "")
  Set wobjClass = Nothing
  Set mobjXMLDoc = CreateObject("MSXML2.DOMDocument")
  mobjXMLDoc.async = False
  Call mobjXMLDoc.loadXML(mvarResponse)
    
  mvarProdok = "ER"
  
  If Not mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
    If (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").Text) = "true" Then
        
      Set wobjXMLList = mobjXMLDoc.selectNodes("//CAMPOS/T-PAGOS-SAL/FORMADEPAGO")
      For wvarContNode = 0 To wobjXMLList.length - 1
        If wobjXMLList(wvarContNode).selectSingleNode("COBROCOD").Text = varCobrocod Then
         If wobjXMLList(wvarContNode).selectSingleNode("COBROTIP").Text = varCobrotip Then
            mvarProdok = "OK"
         End If
        End If
      Next
    End If
  End If
        
  If mvarProdok = "ER" Then
     wvarCodErr = -53
     wvarMensaje = "Poliza colectiva no habilitada / Forma de Pago invalida"
     pvarRes = "<Response><Estado resultado=""false"" mensaje=""" & wvarMensaje & """ /></Response>"
     fncValidaMQ = False
     Exit Function
  End If

        
  Set mobjXMLDoc = Nothing
  Set wobjXMLList = Nothing
        
  wvarStep = 240
  If wvarCodErr = 0 Then
      fncValidaMQ = True
  Else
      fncValidaMQ = False
  End If
  
fin:
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<Response><Estado resultado=""false"" mensaje=""" & mcteErrorInesperadoDescr & """ /></Response>"
wvarMensaje = mcteErrorInesperadoDescr
wvarCodErr = -1000
  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  fncValidaMQ = False
  GoTo fin
End Function

' Devuelve el BANCOCOD o INSTACOD a partir del USUARCOD
Private Function fncGetInstaCodMQ(varUsuarcod As String, wvarCodErr As String, ByRef wvarMensaje As String, pvarRes As String) As String

  Const wcteFnName              As String = "fncGetInstaCodMQ"
  Dim wvarStep    As Long
  
  Dim mvarRequest As String
  Dim mvarResponse As String
  
  Dim oProd
  Dim mobjXMLDoc   As MSXML2.DOMDocument
  Dim wobjClass    As HSBCInterfaces.IAction
  Dim mvarProdok As String
  Dim wobjXMLList               As MSXML2.IXMLDOMNodeList
  Dim wvarContNode              As Long
  'f ch.r
  
  wvarStep = 0
  wvarCodErr = 0
  On Error GoTo ErrorHandler
' valida cobro
  wvarStep = 2


  mvarRequest = "<Request><DEFINICION>1000_PerfilUsuario.xml</DEFINICION>"
  mvarRequest = mvarRequest & "<AplicarXSL>1000_PerfilUsuario.xsl</AplicarXSL>"
  mvarRequest = mvarRequest & "<USUARIO>" & varUsuarcod & "</USUARIO></Request>"


  Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVMQGen.lbaw_MQMensajeGestion")
  Call wobjClass.Execute(mvarRequest, mvarResponse, "")
  Set wobjClass = Nothing
  Set mobjXMLDoc = CreateObject("MSXML2.DOMDocument")
  mobjXMLDoc.async = False
  Call mobjXMLDoc.loadXML(mvarResponse)
    
  mvarProdok = "ER"
  If Not mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
    If (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").Text) = "true" Then
      fncGetInstaCodMQ = mobjXMLDoc.selectSingleNode("//Response/BANCOCOD").Text
      mvarProdok = "OK"
    Else
     mvarProdok = "ER"
    End If
  End If
        
  If mvarProdok = "ER" Then
     wvarCodErr = -53
     wvarMensaje = "Usuario Incorrecto / No posee banco asignado"
     pvarRes = "<Response><Estado resultado=""false"" mensaje=""" & wvarMensaje & """ /></Response>"
     Exit Function
  End If

        
  Set mobjXMLDoc = Nothing
  Set wobjXMLList = Nothing
        
  wvarStep = 240

fin:
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<Response><Estado resultado=""false"" mensaje=""" & mcteErrorInesperadoDescr & """ /></Response>"
wvarCodErr = -1000
  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  'fncValidaMQ = False
  GoTo fin
End Function

Private Function fncValidaPolizasMQ(varUsuarcod As String, varPolizsec As String, varPolizann As String, ByRef varPlanDefec As String, ByRef wvarCodErr As String, ByRef wvarMensaje As String, ByRef pvarRes As String) As Boolean
  Const wcteFnName              As String = "fncValidaPolizasMQ"
  Dim wvarStep    As Long
  
  Dim mvarRequest As String
  Dim mvarResponse As String
  
  Dim varInstaCod As String
  
  Dim oProd
  Dim mobjXMLDoc   As MSXML2.DOMDocument
  Dim wobjClass    As HSBCInterfaces.IAction
  Dim mvarProdok As String
  Dim wobjXMLList               As MSXML2.IXMLDOMNodeList
  Dim wvarContNode              As Long
  'f ch.r
  
  wvarStep = 0
  wvarCodErr = 0
  On Error GoTo ErrorHandler
' valida cobro
  wvarStep = 2
  
  ' Obtengo el varInstaCod
  varInstaCod = fncGetInstaCodMQ(varUsuarcod, wvarCodErr, wvarMensaje, pvarRes)

  mvarRequest = "<Request><DEFINICION>2130_PolizasxCanalHabilOv.xml</DEFINICION>"
  mvarRequest = mvarRequest & "<USUARCOD>" & varUsuarcod & "</USUARCOD>"
  mvarRequest = mvarRequest & "<INSTACOD>" & varInstaCod & "</INSTACOD>"
  mvarRequest = mvarRequest & "<RAMOPCOD>ATD1</RAMOPCOD></Request>"

  Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVMQGen.lbaw_MQMensaje")
  Call wobjClass.Execute(mvarRequest, mvarResponse, "")
  Set wobjClass = Nothing
  Set mobjXMLDoc = CreateObject("MSXML2.DOMDocument")
  mobjXMLDoc.async = False
  Call mobjXMLDoc.loadXML(mvarResponse)
    
  mvarProdok = "ER"
  If Not mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
    If (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").Text) = "true" Then
      If (mobjXMLDoc.selectSingleNode("//CANTIDAD").Text) > 0 Then
            Set wobjXMLList = mobjXMLDoc.selectNodes("//CAMPOS/COBERTURAS/COBERTURA")
            
            For wvarContNode = 0 To wobjXMLList.length - 1
              If wobjXMLList(wvarContNode).selectSingleNode("POLIZANN").Text = varPolizann Then
                 If wobjXMLList(wvarContNode).selectSingleNode("POLIZSEC").Text = varPolizsec Then
                        mvarProdok = "OK"
                        ' obtengo el plan por defecto
                        varPlanDefec = wobjXMLList(wvarContNode).selectSingleNode("PLANDEFEC").Text
                 End If
              End If
            Next
       End If
    End If
  End If
        
  If mvarProdok = "ER" Then
     wvarCodErr = -53
     wvarMensaje = "Poliza  no habilitada para el usuario"
     pvarRes = "<Response><Estado resultado=""false"" mensaje=""" & wvarMensaje & """ /></Response>"
     fncValidaPolizasMQ = False
     Exit Function
  End If

        
  Set mobjXMLDoc = Nothing
  Set wobjXMLList = Nothing
        
  wvarStep = 240
  If wvarCodErr = 0 Then
      fncValidaPolizasMQ = True
  Else
      fncValidaPolizasMQ = False
  End If
  
fin:
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<Response><Estado resultado=""false"" mensaje=""" & mcteErrorInesperadoDescr & """ /></Response>"
wvarCodErr = -1000
  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  fncValidaPolizasMQ = False
  GoTo fin
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub



