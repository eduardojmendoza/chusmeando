package com.qbe.services.fewebservices.jaxws;

import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.interWSBrok.impl.Utils;

public class ComposedResponseTest {

	@Test
	@Ignore
	public void testCR() throws Exception {
		String mensaje = "<Error><LBA_WS res_code=\"-1000\" res_msg=\"Error inesperado [mensaje] - TS-tiempoXXXX - CI - contextinfoXXX\"></LBA_WS></Error>";
		ComposedResponse cr = new ComposedResponse(1, AnyXmlElement.newWithRootChildrenNodes(mensaje));
		System.out.println(cr.getResponse());
	}

	@Test
	public void testCreateLBA_WS() throws Exception {
	
		String msg = Utils.createLBA_WS_XML(-1000, "Error inesperado [" + "Pincho XXX" + "] - TS-" + System.currentTimeMillis() + " CI - " + "ctx");
		System.out.println(msg);
	}
}
