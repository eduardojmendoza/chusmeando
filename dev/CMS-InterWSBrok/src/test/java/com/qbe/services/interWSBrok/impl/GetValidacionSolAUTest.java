package com.qbe.services.interWSBrok.impl;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.text.MessageFormat;

import javax.xml.transform.TransformerException;
import javax.xml.ws.WebServiceException;

import org.apache.commons.lang.StringUtils;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.util.StreamUtils;
import org.w3c.dom.Node;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.services.exutils.ExceptionUtils;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

public class GetValidacionSolAUTest {

	@Test
	public void testCreateIntegrationRequestTicket1435BancoCod() throws IOException, XmlDomExtendedException, TransformerException {
		String requestId = getRequestId();
		String cotiRequestTemplate = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("GetCotizacion1.xml"), Charset.forName("UTF-8"));
		String cotiRequest = MessageFormat.format(cotiRequestTemplate, requestId);

		GetValidacionSolAU vali = new GetValidacionSolAU();
		XmlDomExtended wobjXMLRequest = new XmlDomExtended();
		wobjXMLRequest.loadXML(cotiRequest);
		String integrationReq = vali.createIntegrationRequest(wobjXMLRequest);
		assertTrue(integrationReq.contains("<BANCOCOD>9001</BANCOCOD>"));
	}


	protected String getRequestId() {
		String requestId = "" + System.currentTimeMillis();
		requestId = StringUtils.right(requestId, 9);
		return requestId;
	}

	
	/**
	 * Si mando un CANAL tiene que venir como BANCOCOD
	 * 
	 * @throws IOException
	 * @throws XmlDomExtendedException
	 * @throws TransformerException
	 */
	@Test
	public void testCreateIntegrationRequestTicket1435Canal() throws IOException, XmlDomExtendedException, TransformerException {
		String requestId = getRequestId();
		String cotiRequestTemplate = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
			.getResourceAsStream("GetCotizacionCanal999.xml"), Charset.forName("UTF-8"));
		String cotiRequest = MessageFormat.format(cotiRequestTemplate, requestId);

		GetValidacionSolAU vali = new GetValidacionSolAU();
		XmlDomExtended wobjXMLRequest = new XmlDomExtended();
		wobjXMLRequest.loadXML(cotiRequest);
		String integrationReq = vali.createIntegrationRequest(wobjXMLRequest);
		assertTrue(integrationReq.contains("<BANCOCOD>999</BANCOCOD>"));
	}

	/**
	 * Si mando un NROSOLI tiene que venir como CERTISEC
	 * 
	 * @throws IOException
	 * @throws XmlDomExtendedException
	 * @throws TransformerException
	 */
	@Test
	public void testCreateIntegrationRequestTicket1435NroSoli() throws IOException, XmlDomExtendedException, TransformerException {
		String requestId = getRequestId();
		String cotiRequestTemplate = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
			.getResourceAsStream("GetCotizacionNroSoli.xml"), Charset.forName("UTF-8"));
		String cotiRequest = MessageFormat.format(cotiRequestTemplate, requestId);

		GetValidacionSolAU vali = new GetValidacionSolAU();
		XmlDomExtended wobjXMLRequest = new XmlDomExtended();
		wobjXMLRequest.loadXML(cotiRequest);
		String integrationReq = vali.createIntegrationRequest(wobjXMLRequest);
		assertTrue(integrationReq.contains("<CERTISEC>123456</CERTISEC>"));
	}

	/**
	 * Si mando un CERTISEC tiene que venir como CERTISEC
	 * 
	 * @throws IOException
	 * @throws XmlDomExtendedException
	 * @throws TransformerException
	 */
	@Test
	public void testCreateIntegrationRequestTicket1435Certisec() throws IOException, XmlDomExtendedException, TransformerException {
		String requestId = getRequestId();
		String cotiRequestTemplate = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
			.getResourceAsStream("GetCotizacionCertisec.xml"), Charset.forName("UTF-8"));
		String cotiRequest = MessageFormat.format(cotiRequestTemplate, requestId);

		GetValidacionSolAU vali = new GetValidacionSolAU();
		XmlDomExtended wobjXMLRequest = new XmlDomExtended();
		wobjXMLRequest.loadXML(cotiRequest);
		String integrationReq = vali.createIntegrationRequest(wobjXMLRequest);
		assertTrue(integrationReq.contains("<CERTISEC>123456</CERTISEC>"));
	}

	/**
	 * Si mando CERTISEC en 0 tiene que volver de la base
	 * 
	 * @throws IOException
	 * @throws XmlDomExtendedException
	 * @throws TransformerException
	 */
	@Test
	@Ignore //Lo usamos durante el desarrollo, agregando una linea que devuelve siempre el mismo valor sin ir a la base
	public void testCreateIntegrationRequestTicket1435Certisec0Cableado() throws IOException, XmlDomExtendedException, TransformerException {
		String requestId = getRequestId();
		String cotiRequestTemplate = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
			.getResourceAsStream("GetCotizacionCertisec0.xml"), Charset.forName("UTF-8"));
		String cotiRequest = MessageFormat.format(cotiRequestTemplate, requestId);

		GetValidacionSolAU vali = new GetValidacionSolAU();
		XmlDomExtended wobjXMLRequest = new XmlDomExtended();
		wobjXMLRequest.loadXML(cotiRequest);
		String integrationReq = vali.createIntegrationRequest(wobjXMLRequest);
		assertTrue(integrationReq.contains("<CERTISEC>1608</CERTISEC>"));
	}


	/**
	 * Si mando CERTISEC en 0 tiene que volver de la base
	 * 
	 * @throws IOException
	 * @throws XmlDomExtendedException
	 * @throws TransformerException
	 * @throws CurrentProfileException 
	 */
	@Test
	public void testCreateIntegrationRequestTicket1435Certisec0() throws IOException, XmlDomExtendedException, TransformerException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		String requestId = getRequestId();
		String cotiRequestTemplate = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
			.getResourceAsStream("GetCotizacionCertisec0.xml"), Charset.forName("UTF-8"));
		String cotiRequest = MessageFormat.format(cotiRequestTemplate, requestId);

		GetValidacionSolAU vali = new GetValidacionSolAU();
		XmlDomExtended wobjXMLRequest = new XmlDomExtended();
		wobjXMLRequest.loadXML(cotiRequest);
		String integrationReq = vali.createIntegrationRequest(wobjXMLRequest);
		XmlDomExtended integrationReqXml = new XmlDomExtended();
		integrationReqXml.loadXML(integrationReq);
		Node certisecNode = integrationReqXml.selectSingleNode("//CERTISEC");
		String certisec = XmlDomExtended.getText(certisecNode);
		assertFalse(certisec.equals("0"));
		System.out.println(certisec);
	}
	
	@Test
	public void testExceptionCauses() {
		SocketTimeoutException rootSte = new SocketTimeoutException("Read timed out");
		SocketTimeoutException ste2 = new SocketTimeoutException("SocketTimeoutException invoking http://ard116vlncdb.qbe-ar.localdomain:8011/OV/Proxy/QBESvc: Read timed out");
		org.apache.commons.lang.exception.ExceptionUtils.setCause(ste2, rootSte);
		WebServiceException wse = new WebServiceException("Could not send Message", ste2);
		OSBConnectorException oce = new OSBConnectorException("javax.xml.ws.WebServiceException: Could not send Message.", wse);
		
		String causes = ExceptionUtils.getCauses(oce);
		String response = "<LBA_WS res_code=\"-1000\" res_msg=\"" + "Error inesperado [" + oce.getMessage() + "] - TS-" + System.currentTimeMillis() + " CI - " + null
				+ " \">" + "<![CDATA[" + causes + "]]>" + "</LBA_WS>";
		System.out.println(response);
		
	}
}
