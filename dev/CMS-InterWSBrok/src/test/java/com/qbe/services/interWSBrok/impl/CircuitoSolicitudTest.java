package com.qbe.services.interWSBrok.impl;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.TimeZone;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.util.StreamUtils;
import org.w3c.dom.DOMException;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

import diamondedge.util.DateTime;
import diamondedge.util.Strings;

public class CircuitoSolicitudTest {

	protected static ApplicationContext context; 

	protected synchronized static ApplicationContext getContext() {

		if ( context == null) {
			context = new ClassPathXmlApplicationContext("classpath*:spring-beans.xml");
		}
		return context;
	}

	private OSBConnector getOSBConnector() {
		OSBConnector eng = (OSBConnector)getContext().getBean("osbConnector");
		return eng;
	}


	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Ignore //Falla por timeout
	public void testCotizaYSolicita() throws IOException, CurrentProfileException, DOMException, XmlDomExtendedException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		
		String requestId = "" + System.currentTimeMillis();
		requestId = StringUtils.right(requestId, 9);
		String cotiRequestTemplate = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("GetCotizacion1.xml"), Charset.forName("UTF-8"));
		String cotiRequest = MessageFormat.format(cotiRequestTemplate, requestId);
		
		GetCotizacionAU getCoti = new GetCotizacionAU();
		getCoti.setOsbConnector(getOSBConnector());
		StringHolder respSH = new StringHolder();
		getCoti.IAction_Execute(cotiRequest, respSH, null);
		assertNotNull(respSH);
		
		XmlDomExtended respuestaCotiXml = new XmlDomExtended();
		respuestaCotiXml.loadXML(respSH.getValue());
		
		String respCotiRequestID = XmlDomExtended.getText(respuestaCotiXml.selectSingleNode("//Response_WS/LBA_WS/Response/REQUESTID"));
		assertNotNull("respCotiRequestID es null!", respCotiRequestID);
		assertEquals("Volvió un requestID distinto al que enviamos", respCotiRequestID, requestId);
		
		String cotNro = XmlDomExtended.getText(respuestaCotiXml.selectSingleNode("//Response_WS/LBA_WS/Response/COT_NRO"));
		assertNotNull("cotNro es null!", cotNro);
		
		String precio = XmlDomExtended.getText(respuestaCotiXml.selectSingleNode("//Response_WS/LBA_WS/Response/PLANES/PLAN[PLAN_COD=\"01503\"]/COMP_PRECIO/PRECIO"));
		assertNotNull("precio es null!", precio);

		String solicitudRequestTemplate = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("GetSolicitud1.xml"), Charset.forName("UTF-8"));

        Calendar localCalendar = Calendar.getInstance(TimeZone.getDefault());
        
        int currentDay = localCalendar.get(Calendar.DATE);
        int currentMonth = localCalendar.get(Calendar.MONTH) + 1;
        int currentYear = localCalendar.get(Calendar.YEAR);
   
		String vigenann = "" + currentYear;
		String vigenmes = "" + currentMonth;
		String vigendia = "" + currentDay;

		String id = "" + System.currentTimeMillis();
		//Ej 1402495428535
		String motornum = id.substring(1, 12);
		String chasinum = "9BF" + id;
		String patennum = "KKK" + id.substring(9, 12);
//		
//		  <MOTORNUM>CBR1E002993</MOTORNUM>
//		  <CHASINUM>9BFZK53BEB002993</CHASINUM>
//		  <PATENNUM>NLJ213</PATENNUM>


		String solicitudRequest = MessageFormat.format(solicitudRequestTemplate, respCotiRequestID, 
				cotNro, vigenann, vigenmes, vigendia, precio,
				motornum, chasinum, patennum);
		
		
		
		GetSolicitudAU getSoli = new GetSolicitudAU();
		getSoli.setOsbConnector(getOSBConnector());
		StringHolder respSoliSH = new StringHolder();
		getSoli.IAction_Execute(solicitudRequest, respSoliSH, null);
		assertNotNull(respSoliSH);
		
	}

	@Test
	public void testCotiza() throws IOException, CurrentProfileException, DOMException, XmlDomExtendedException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		
		String requestId = "" + System.currentTimeMillis();
		requestId = StringUtils.right(requestId, 9);
		String cotiRequestTemplate = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("GetCotizacion1.xml"), Charset.forName("UTF-8"));
		String cotiRequest = MessageFormat.format(cotiRequestTemplate, requestId);
		
		GetCotizacionAU getCoti = new GetCotizacionAU();
		getCoti.setOsbConnector(getOSBConnector());
		StringHolder respSH = new StringHolder();
		getCoti.IAction_Execute(cotiRequest, respSH, null);
		assertNotNull(respSH);
		System.out.println("Respuesta de GetCotizacionAU:");
		System.out.println(respSH.toString());
		
		XmlDomExtended respuestaCotiXml = new XmlDomExtended();
		respuestaCotiXml.loadXML(respSH.getValue());

		String res_code = XmlDomExtended.getText(respuestaCotiXml.selectSingleNode("//Response_WS/LBA_WS/@res_code"));
		if ( "-300".equalsIgnoreCase(res_code)) {
			//Cancelo el test, -300 significa "El servicio de consulta no se encuentra disponible"
			return;
		}
		String respCotiRequestID = XmlDomExtended.getText(respuestaCotiXml.selectSingleNode("//Response_WS/LBA_WS/Response/REQUESTID"));
		
		
		assertNotNull("respCotiRequestID es null!", respCotiRequestID);
		assertEquals("Volvió un requestID distinto al que enviamos", respCotiRequestID, requestId);
		
		String cotNro = XmlDomExtended.getText(respuestaCotiXml.selectSingleNode("//Response_WS/LBA_WS/Response/COT_NRO"));
		assertNotNull("cotNro es null!", cotNro);
		
		String precio = XmlDomExtended.getText(respuestaCotiXml.selectSingleNode("//Response_WS/LBA_WS/Response/PLANES/PLAN[PLAN_COD=\"01503\"]/COMP_PRECIO/PRECIO"));
		assertNotNull("precio es null!", precio);
	}	
}
