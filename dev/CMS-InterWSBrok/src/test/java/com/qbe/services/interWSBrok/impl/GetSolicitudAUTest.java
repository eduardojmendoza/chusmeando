package com.qbe.services.interWSBrok.impl;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.MessageFormat;

import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.util.StreamUtils;
import org.w3c.dom.Node;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

public class GetSolicitudAUTest {
	protected static ApplicationContext context; 

	protected synchronized static ApplicationContext getContext() {

		if ( context == null) {
			context = new ClassPathXmlApplicationContext("classpath*:spring-beans.xml");
		}
		return context;
	}

	private OSBConnector getOSBConnector() {
		OSBConnector eng = (OSBConnector)getContext().getBean("osbConnector");
		return eng;
	}


	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testBug1453_1() throws IOException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		String cotiRequest = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
			.getResourceAsStream("Bug1435_1.xml"), Charset.forName("UTF-8"));

		GetSolicitudAU soli = new GetSolicitudAU();
		soli.setOsbConnector(getOSBConnector());
		StringHolder responseSH = new StringHolder();
		int result = soli.IAction_Execute(cotiRequest, responseSH, null);
		System.out.println(responseSH.getValue());
//		XmlDomExtended integrationReqXml = new XmlDomExtended();
//		integrationReqXml.loadXML(integrationReq);
//		Node certisecNode = integrationReqXml.selectSingleNode("//CERTISEC");
//		String certisec = XmlDomExtended.getText(certisecNode);
//		assertFalse(certisec.equals("0"));
//		System.out.println(certisec);
	}

}
