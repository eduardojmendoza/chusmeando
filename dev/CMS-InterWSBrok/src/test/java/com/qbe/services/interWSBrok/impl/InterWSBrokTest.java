package com.qbe.services.interWSBrok.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.interWSBrok.impl.GetCotizacionAU;
import com.qbe.services.interWSBrok.impl.GetImpreSolAU;
import com.qbe.services.interWSBrok.impl.GetValidacionSolAU;
import com.qbe.services.testutils.TestUtils;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;

public class InterWSBrokTest  {
		
	public static final String TEST_SERVICE = "http://10.1.10.98:8011/OV/Proxy/QBESvc?wsdl";


		public InterWSBrokTest() {
			super();
		}
		
		/**
		 * Testea la ejecución del mensaje, y que no vuelva un código de error. Valida contra la respuesta esperada
		 * 
		 * @param msgCode
		 * @throws Exception 
		 */
		protected void testMessageFullRequest (String msgCode, VBObjectClass comp) throws Exception {
			String responseText = this.testMessageJustExecution(msgCode, comp);
		
			Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
			XMLUnit.setIgnoreWhitespace(true);
			String expectedResponse = TestUtils.loadResponse(msgCode);
			if ( expectedResponse == null || expectedResponse.length()==0) throw new Exception("No se pudo cargar el request");
			DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(expectedResponse, responseText));
			List allDifferences = myDiff.getAllDifferences();
			assertEquals(myDiff.toString(), 0, allDifferences.size());
			//Eliminar los &lt; y &gt;
			XMLAssert.assertXMLEqual("No volvió la respuesta esperada", expectedResponse, responseText);
		
		}
		
		/**
		 * Testea la ejecución del mensaje, y que no vuelva un código de error. No valida contra la respuesta esperada, la devuelve solamente
		 * 
		 * @param msgCode
		 * @throws Exception 
		 */
		protected String testMessageJustExecution (String msgCode, VBObjectClass comp) throws Exception {
			String request = TestUtils.loadRequest(msgCode);
			if ( request == null || request.length()==0 || request.trim().equals("")) throw new Exception("No se pudo cargar el request");
			StringHolder responseHolder = new StringHolder();
			int result = comp.IAction_Execute(request, responseHolder, null);
//			System.out.println("Response: " + responseHolder);
			if ( result == 1) Assert.fail ("Abortó la ejecución, ver el log");
			Assert.assertTrue("Result no es 1 ni 0, es un error",result == 0);
			try {
				TestUtils.parseResultado(responseHolder.getValue());
				TestUtils.parseMensaje(responseHolder.getValue());
			} catch (Exception e){
				return responseHolder.getValue();
			}
			String responseText = responseHolder.getValue();
			if (( responseText == null) || ( responseText.length()==0)) {
				fail("responseText vacío");
			}
			return responseText;
		}
	
		//------------------------------ T E S T ------------------------------------------	
		@Ignore
		@Test
		public void testGetImpreSolAU() throws Exception {
			Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
			GetImpreSolAU comp = new GetImpreSolAU();
			OSBConnector conn = new OSBConnector();
			conn.setServiceURL(TEST_SERVICE);
			comp.setOsbConnector(conn);
			testMessageJustExecution("_LBA_InterWSBrok.GetImpreSolAU", comp);
		}

		@Ignore
		@Test
		public void testGetCotizacionAU() throws Exception {
			Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
			GetCotizacionAU comp = new GetCotizacionAU();
			OSBConnector conn = new OSBConnector();
			conn.setServiceURL(TEST_SERVICE);
			comp.setOsbConnector(conn);
			testMessageJustExecution("_LBA_InterWSBrok.GetCotizacionAU", comp);
		}
		
		@Ignore
		@Test
		public void testGetValidacionSolAU() throws Exception {
			Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
			GetValidacionSolAU comp = new GetValidacionSolAU();
			OSBConnector conn = new OSBConnector();
			conn.setServiceURL(TEST_SERVICE);
			comp.setOsbConnector(conn);

			testMessageFullRequest("_LBA_InterWSBrok.GetValidacionSolAU_2", comp);
		}
		
		@Test
		public void testGetSolicitudAU() throws Exception {
			Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
			GetSolicitudAU comp = new GetSolicitudAU();
			OSBConnector conn = new OSBConnector();
			conn.setServiceURL(TEST_SERVICE);
			comp.setOsbConnector(conn);

			testMessageFullRequest("_LBA_InterWSBrok.GetSolicitudAU", comp);
		}
}