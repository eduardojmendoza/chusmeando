package com.qbe.services.interWSBrok.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.interWSBrok.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Implementacion de los objetos
 * Objetos del FrameWork
 */

public class GetCotizacionHO implements VBObjectClass
{
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetCotizacionHO";
  static final String mcteStoreProc = "SPSNCV_BRO_ALTA_OPER";
  /**
   * Archivo de Configuracion
   */
  static final String mcteArchivoConfHOM_XML = "LBA_PARAM_HO.XML";
  static final String mcteArchivoHOM_XML = "LBA_VALIDACION_COT_HO.XML";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  /**
   *  EG 14/10 cambia nombre constante
   * Const mcteParam_COBROCOD            As String = "COBROCOD"
   */
  static final String mcteParam_COBROCOD = "FPAGO";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_TIPOHOGAR = "TIPOHOGAR";
  /**
   *  EG 14/10 cambia nombre constante
   * Const mcteParam_PLANNCOD            As String = "PLANNCOD"
   */
  static final String mcteParam_PLANNCOD = "PLAN";
  /**
   *  EG 14/10 cambia nombre constante
   * Const mcteParam_Provi               As String = "PROVI"
   */
  static final String mcteParam_Provi = "PROVCOD";
  /**
   *  EG 14/10 cambia nombre constante
   * Const mcteParam_LocalidadCod        As String = "LOCALIDADCOD"
   */
  static final String mcteParam_LocalidadCod = "LOCALIDAD";
  static final String mcteParam_TipoOperac = "TIPOOPERAC";
  static final String mcteParam_RAMOPCOD = "RAMOPCOD";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  static final String mcteParam_NROCOT = "COT_NRO";
  static final String mcteParam_EstadoProc = "ESTADOPROC";
  static final String mcteParam_PedidoAno = "MSGANO";
  static final String mcteParam_PedidoMes = "MSGMES";
  static final String mcteParam_PedidoDia = "MSGDIA";
  static final String mcteParam_PedidoHora = "MSGHORA";
  static final String mcteParam_PedidoMinuto = "MSGMINUTO";
  static final String mcteParam_PedidoSegundo = "MSGSEGUNDO";
  static final String mcteParam_TiempoProceso = "TIEMPOPROCESO";
  static final String mcteParam_CERTISEC = "CERTISEC";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS";
  static final String mcteParam_COBERCOD = "COBERCOD";
  static final String mcteParam_CONTRMOD = "CONTRMOD";
  static final String mcteParam_CAPITASG = "CAPITASG";
  /**
   * DATOS DE LAS CAMPA�AS
   */
  static final String mcteNodos_Campa = "//Request/CAMPANIAS";
  static final String mcteParam_CampaCod = "CAMPACOD";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncInsertNode
   * static variable for method: fncPutCotHO
   */
  private final String wcteFnName = "fncPutCotHO";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant wvarCodErr = new Variant();
    Variant pvarRes = new Variant();
    HSBCInterfaces.IAction wobjClass = null;
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      pvarRequest = Strings.mid( pvarRequest, 1, Strings.find( 1, pvarRequest, ">" ) ) + "<MSGANO>" + DateTime.year( DateTime.now() ) + "</MSGANO>" + "<MSGMES>" + DateTime.month( DateTime.now() ) + "</MSGMES>" + "<MSGDIA>" + DateTime.day( DateTime.now() ) + "</MSGDIA>" + "<MSGHORA>" + DateTime.hour( DateTime.now() ) + "</MSGHORA>" + "<MSGMINUTO>" + DateTime.minute( DateTime.now() ) + "</MSGMINUTO>" + "<MSGSEGUNDO>" + DateTime.second( DateTime.now() ) + "</MSGSEGUNDO>" + Strings.mid( pvarRequest, (Strings.find( 1, pvarRequest, ">" ) + 1) );
      wvarStep = 10;
      pvarRes.set( "" );
      wvarMensaje.set( "" );
      wvarCodErr.set( "" );
      //
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(new Variant( pvarRequest )), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*TBD mobjCOM_Context.SetAbort() ;*/
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*TBD mobjCOM_Context.SetComplete() ;*/
        }
        return IAction_Execute;
      }
      //
      pvarResponse.set( pvarRes );
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetComplete() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( Variant pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParams = null;
    XmlDomExtended wobjXMLReturnVal = null;
    XmlDomExtended wobjXMLError = null;
    org.w3c.dom.Node wobjXMLChildNode = null;
    Object wobjClass = null;
    Variant wvarMensajeStoreProc = new Variant();
    Variant wvarMensajePutTran = new Variant();
    String mvar_Estado = "";
    String mvar_precio = "";
    String mvar_TiempoProceso = "";
    String mvar_CodZona = "";

    //
    //
    try 
    {

      //Le agrega los Nodos de Valor Fijo
      wvarStep = 10;
      wobjXMLParams = new XmlDomExtended();
      wobjXMLParams.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteArchivoConfHOM_XML));
      //
      wvarStep = 15;
      invoke( "fncInsertNode", new Variant[] { new Variant(pvarRequest), new Variant("//Request"), new Variant( XmlDomExtended .getText( wobjXMLParams.selectSingleNode( new Variant("//BROKERS/HOGAR/COTIZACION") )  )), new Variant("TIPOOPERAC") } );
      invoke( "fncInsertNode", new Variant[] { new Variant(pvarRequest), new Variant("//Request"), new Variant( XmlDomExtended .getText( wobjXMLParams.selectSingleNode( new Variant("//BROKERS/HOGAR/RAMOPCOD") )  )), new Variant("RAMOPCOD") } );
      //
      //Instancia la clase de validacion
      wvarMensaje.set( "" );
      wvarMensajeStoreProc.set( "" );
      //
      wvarStep = 20;
      wobjClass = new GetValidacionCotHO();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(pvarRequest, wvarMensajeStoreProc, "")
      wobjClass = null;
      //
      //Analizo la respuesta del validador
      wvarStep = 25;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( wvarMensajeStoreProc.toString() );
      //
      //Si la respuesta viene con error
      wvarStep = 30;
      if( ! ( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ).equals( "0" )) )
      {
        wvarCodErr.set( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ) );
        wvarMensaje.set( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" )  ) );
        if( wvarCodErr.toString().equals( "-2" ) )
        {
          mvar_Estado = "NI";
        }
        else
        {
          mvar_Estado = "ER";
        }
        //En el caso de que no llegue a cotizar, tomo el XML original para el ALTA OPER
        wvarStep = 35;
        pvarRes.set( wvarMensajeStoreProc );
        wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
      }
      else
      {
        wvarStep = 40;
        //En el caso que la Validacion est� Ok, obtengo la Cotizacion
        //EG 10/2010 Cambio el componente viejo x el nuevo
        //Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVLBAMQ.lbawS_GetCotizHOM")
        wobjClass = new lbawA_ProductorHgMQ.lbaw_GetCotiz();
        wobjClass.Execute( wvarMensajeStoreProc.toString(), pvarRes, "" );
        wobjClass = (Object) null;
        //
        wvarStep = 45;
        wobjXMLReturnVal = new XmlDomExtended();
        wobjXMLReturnVal.loadXML( pvarRes.toString() );
        //If Val(wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text) < 0 Then
        if( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
        {
          mvar_Estado = "ER";
        }
        else
        {
          //mvar_Estado = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text
          mvar_Estado = XmlDomExtended.getText( wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@resultado" )  );
        }
        wvarStep = 50;
        //If Not wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text = "OK" Then
        if( ! ( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" )) )
        {
          //hubo un error
          //eg 10/2010 la codificaci�n respondia al mensaje viejo, se utiliza el error que devuelve el componente actual
          pvarRes.set( "<LBA_WS res_code=\"-300\" res_msg=\"" + XmlDomExtended.getText( wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@mensaje" )  ) + "\"></LBA_WS>" );


          //Set wobjXMLError = CreateObject("MSXML2.DOMDocument")
          //wobjXMLError.async = False
          //Call wobjXMLError.Load(App.Path & "\" & mcteArchivoHOM_XML)
          //Completo los valores del error
          //wvarStep = 60
          //If Not wobjXMLError.selectSingleNode("//ERRORES/HOGAR/RESPUESTAS/ERROR[CODIGO='" & wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text & "']") Is Nothing Then
          //  wvarStep = 70
          //  wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_msg").Text = wobjXMLError.selectSingleNode("//ERRORES/HOGAR/RESPUESTAS/ERROR[CODIGO='" & wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text & "']/TEXTOERROR").Text
          //  wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text = wobjXMLError.selectSingleNode("//ERRORES/HOGAR/RESPUESTAS/ERROR[CODIGO='" & wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text & "']/VALORRESPUESTA").Text
          //  wvarCodErr = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text
          //  wvarMensaje = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_msg").Text
          //  pvarRes = wobjXMLReturnVal.xml
          // Else
          //  wvarStep = 80
          //  wvarCodErr = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text
          //  wvarMensaje = wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_msg").Text
          //Este es el cado en que no haya contestado el MQ
          // End If
        }
        else
        {
          //Tomo el valor del Precio
          wvarStep = 90;
          //EG 10/2010 No se toma en cuenta ni la zona ni el tiempo de proceso del AIS
          mvar_precio = XmlDomExtended.getText( wobjXMLReturnVal.selectSingleNode( "//COTIZACION" )  );
          mvar_CodZona = "0";
          mvar_TiempoProceso = "0";

          //mvar_precio = wobjXMLReturnVal.selectSingleNode("//LBA_WS/Response/PRECIOS/PRECIO/VALOR").Text
          //mvar_CodZona = wobjXMLReturnVal.selectSingleNode("//LBA_WS/Response/CODIZONA").Text
          //mvar_TiempoProceso = wobjXMLReturnVal.selectSingleNode("//LBA_WS/Response/TIEMPOPROCESO").Text
          //Call fncInsertNode(wvarMensajeStoreProc, "//LBA_WS/Request", mvar_precio, "VALOR")
          invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvar_CodZona), new Variant("CODZONA") } );
          invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvar_TiempoProceso), new Variant("TIEMPOPROCESO") } );
          invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvar_precio), new Variant("VALOR") } );

          //Le agregamos numero de cotizacion
          wvarMensajeStoreProc.set( Strings.replace( wvarMensajeStoreProc.toString(), "<COT_NRO>0</COT_NRO>", "<COT_NRO>" + XmlDomExtended.getText( wobjXMLReturnVal.selectSingleNode( "//COT_NRO" )  ) + "</COT_NRO>" ) );
          //Elimino del XML de Salida el CodZona y TiempoProceso
          wvarStep = 92;
          //Set wobjXMLChildNode = wobjXMLReturnVal.selectSingleNode("//LBA_WS/Response/CODIZONA")
          //Call wobjXMLReturnVal.selectSingleNode("//LBA_WS/Response").removeChild(wobjXMLChildNode)
          //Set wobjXMLChildNode = Nothing
          //
          wvarStep = 94;
          //Set wobjXMLChildNode = wobjXMLReturnVal.selectSingleNode("//LBA_WS/Response/TIEMPOPROCESO")
          //Call wobjXMLReturnVal.selectSingleNode("//LBA_WS/Response").removeChild(wobjXMLChildNode)
          //Set wobjXMLChildNode = Nothing
          //
          //
          //Le asigno el Valor de Res_code a 0 porque significa que tiene OK
          wvarStep = 96;
          //wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text = 0
          pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" );
          pvarRes.set( pvarRes + wobjXMLReturnVal.getDocument().getDocumentElement().toString() );
          pvarRes.set( pvarRes + "</LBA_WS>" );
          //
          wobjXMLReturnVal = null;
          //
          //Guarda la Solicitud en el SP
          //Envio el mismo XML y lo unico que puede variar es el Certisec
          wvarStep = 100;
          wobjClass = new PutCotizacionHO();
          wobjClass.Execute( wvarMensajeStoreProc, wvarMensajeStoreProc, "" );
          wobjClass = (Object) null;
          //
          wobjXMLReturnVal = new XmlDomExtended();
          wobjXMLReturnVal.loadXML( wvarMensajeStoreProc.toString() );
          //
          wvarStep = 110;
          if( ! ( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ).equals( "0" )) )
          {
            wvarStep = 120;
            wvarCodErr.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ) );
            wvarMensaje.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" )  ) );
            pvarRes.set( wvarMensajeStoreProc );
            wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
            mvar_Estado = "ER";
            //Este es el cado en que no haya contestado el SP o sea un error no identificado
          }
          //
          wobjXMLReturnVal = null;
          //
        }
      }
      invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvar_Estado), new Variant("ESTADOPROC") } );
      //Alta de la OPERACION
      wvarStep = 130;
      //
      if( invoke( "fncPutCotHO", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje.toString()), new Variant(wvarCodErr), new Variant(wvarMensajePutTran) } ) )
      {
        wobjXMLReturnVal = new XmlDomExtended();
        wobjXMLReturnVal.loadXML( wvarMensajePutTran.toString() );
        wvarStep = 140;
        if( ! ( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ).equals( "0" )) )
        {
          wvarStep = 150;
          wvarCodErr.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ) );
          wvarMensaje.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" )  ) );
          pvarRes.set( wvarMensajePutTran );
          return fncGetAll;
        }
        wobjXMLReturnVal = null;
        wobjClass = (Object) null;
        fncGetAll = true;
      }
      //
      fin: 
      wobjClass = (Object) null;
      wobjXMLRequest = null;
      wobjXMLParams = null;
      wobjXMLReturnVal = null;
      wobjXMLError = null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;
        /*TBD mobjCOM_Context.SetComplete() ;*/

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }

  private boolean fncInsertNode( Variant pvarRequest, String pvarHeader, String pvarNodeValue, String pvarNodeName )
  {
    boolean fncInsertNode = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;


    try 
    {
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest.toString() );
      //
      wvarStep = 20;
      /*unsup wobjXMLRequest.selectSingleNode( pvarHeader ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), pvarNodeName, "" ) */ );
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//" + pvarNodeName ) , pvarNodeValue );
      //
      pvarRequest.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
      wobjXMLRequest = null;
      fncInsertNode = true;
      //
      fin: 
      return fncInsertNode;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //AM Debug
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncInsertNode = false;
        /*TBD mobjCOM_Context.SetComplete() ;*/

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncInsertNode;
  }

  private boolean fncPutCotHO( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutCotHO = false;
    int wvarStep = 0;
    HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    XmlDomExtended wobjXMLRequest = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_NROCOT = "";
    String mvarWDB_TIPOOPERAC = "";
    String mvarWDB_ESTADO = "";
    String mvarWDB_RECEPCION_PEDIDOANO = "";
    String mvarWDB_RECEPCION_PEDIDOMES = "";
    String mvarWDB_RECEPCION_PEDIDODIA = "";
    String mvarWDB_RECEPCION_PEDIDOHORA = "";
    String mvarWDB_RECEPCION_PEDIDOMINUTO = "";
    String mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_TIEMPOPROCESO = "";
    String mvarWDB_XML = "";

    //
    //
    //
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_CODINST = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST )  );
      }
      //
      wvarStep = 30;
      mvarWDB_REQUESTID = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID )  );
      }
      //
      wvarStep = 40;
      mvarWDB_NROCOT = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROCOT) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NROCOT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NROCOT )  );
      }
      //
      wvarStep = 50;
      mvarWDB_TIPOOPERAC = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoOperac) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPOOPERAC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoOperac )  );
      }
      //
      wvarStep = 60;
      mvarWDB_ESTADO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EstadoProc) )  == (org.w3c.dom.Node) null) )
      {
        //EG 10/2010 cambia el valor del tag al actualizarce componente
        mvarWDB_ESTADO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_EstadoProc )  );
        if( mvarWDB_ESTADO.equals( "true" ) )
        {
          mvarWDB_ESTADO = "OK";
        }
        else
        {
          mvarWDB_ESTADO = "ER";
        }
      }
      //
      wvarStep = 70;
      mvarWDB_RECEPCION_PEDIDOANO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoAno) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOANO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoAno )  );
      }
      //
      wvarStep = 80;
      mvarWDB_RECEPCION_PEDIDOMES = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMes) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMes )  );
      }
      //
      wvarStep = 90;
      mvarWDB_RECEPCION_PEDIDODIA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoDia) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDODIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoDia )  );
      }
      //
      wvarStep = 100;
      mvarWDB_RECEPCION_PEDIDOHORA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoHora) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOHORA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoHora )  );
      }
      //
      wvarStep = 110;
      mvarWDB_RECEPCION_PEDIDOMINUTO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMinuto) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMINUTO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMinuto )  );
      }
      //
      wvarStep = 120;
      mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoSegundo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOSEGUNDO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoSegundo )  );
      }
      //
      wvarStep = 130;
      mvarWDB_RAMOPCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAMOPCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_RAMOPCOD )  );
      }
      //
      wvarStep = 140;
      mvarWDB_POLIZANN = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) )  == (org.w3c.dom.Node) null) )
      {
        if( new Variant( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN )  ) ).isNumeric() )
        {
          if( Obj.toInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) )  ) ) < 100 )
          {
            mvarWDB_POLIZANN = String.valueOf( VB.val( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN )  ) ) );
          }
        }
      }
      //
      wvarStep = 150;
      mvarWDB_POLIZSEC = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) )  == (org.w3c.dom.Node) null) )
      {
        if( new Variant( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC )  ) ).isNumeric() )
        {
          if( Obj.toInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) )  ) ) < 1000000 )
          {
            mvarWDB_POLIZSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC )  );
          }
        }
      }
      //
      wvarStep = 160;
      mvarWDB_CERTISEC = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CERTISEC) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTISEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CERTISEC )  );
      }
      //
      wvarStep = 170;
      mvarWDB_TIEMPOPROCESO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TiempoProceso) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIEMPOPROCESO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TiempoProceso )  );
      }
      //
      wvarStep = 180;
      mvarWDB_XML = "";
      mvarWDB_XML = pvarRequest;
      //
      wvarStep = 190;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.mcteDB);
      //
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      
      //
      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      
      //
      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_NRO_COTIZACION_LBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NROCOT.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NROCOT)) );
      
      //
      wvarStep = 230;
      
      
      //
      wvarStep = 240;
      
      
      //
      wvarStep = 250;
      
      
      //
      wvarStep = 260;
      
      
      //
      wvarStep = 270;
      
      
      //
      wvarStep = 280;
      wobjDBParm = new Parameter( "@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      
      //
      wvarStep = 290;
      wobjDBParm = new Parameter( "@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      
      //
      wvarStep = 300;
      wobjDBParm = new Parameter( "@WDB_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      
      //
      wvarStep = 310;
      
      
      //
      wvarStep = 320;
      wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CERTISEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CERTISEC)) );
      
      //
      wvarStep = 330;
      wobjDBParm = new Parameter( "@WDB_TIEMPO_PROCESO_AIS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIEMPOPROCESO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIEMPOPROCESO)) );
      
      //
      wvarStep = 340;
      
      
      //
      wvarStep = 350;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      //
      wobjDBCnn.close();
      //
      wvarStep = 360;
      fncPutCotHO = true;
      pvarRes.set( "<LBA_WS res_code=\"" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ) + "\" res_msg=\"" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" )  ) + "\"></LBA_WS>" );
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = null;
      wobjClass = (HSBCInterfaces.IAction) null;
      return fncPutCotHO;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        fncPutCotHO = false;
        /*TBD mobjCOM_Context.SetComplete() ;*/


        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutCotHO;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
