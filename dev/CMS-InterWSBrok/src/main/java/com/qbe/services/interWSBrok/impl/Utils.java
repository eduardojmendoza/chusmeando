package com.qbe.services.interWSBrok.impl;

import java.text.MessageFormat;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import com.qbe.services.mqgeneric.impl.LBAW_MQMensaje;
import com.qbe.services.mqgeneric.impl.LBAW_MQMensajeGestion;
import com.qbe.vbcompat.string.StringHolder;

public class Utils {

	/**
	 * Formatea una respuesta, reemplazando <PDF64>...</PDF64> por <PDF64>...omitidos...</PDF64>
	 * @param pvarRes
	 * @return
	 */
	public static String formatLogResponseChompPDF64(StringHolder pvarRes) {
		// Para loguear saco los PDFs kilométricos
		String loggedResponse = pvarRes.getValue(); 
		int start = loggedResponse.indexOf("<PDF64>");
		int end = loggedResponse.indexOf("</PDF64>");
		while ( start != -1 && end != -1 ) {
			loggedResponse = StringUtils.overlay(loggedResponse, "<PDF64_LOG>...omitidos...</PDF64_LOG>", start, end);
			start = loggedResponse.indexOf("<PDF64>");
			end = loggedResponse.indexOf("</PDF64>");
		}
		return loggedResponse;
	}

	/**
	 * ActionCode lbaw_GetConsultaMQ es implementado por LBAW_MQMensaje 
	 * @param mvarRequest
	 * @return
	 */
	public static String getConsultaMQ(String mvarRequest) {
		String mvarResponse;
		LBAW_MQMensaje consultaMQ = new LBAW_MQMensaje();
		StringHolder consultaMQSH = new StringHolder();
		consultaMQ.IAction_Execute(mvarRequest, consultaMQSH, "");
		mvarResponse = consultaMQSH.getValue();
		return mvarResponse;
	}

	/**
	 * ActionCode lbaw_GetConsultaMQGestion es implementado por LBAW_MQMensajeGestion 
	 * @param mvarRequest
	 * @return
	 */
	public static String getConsultaMQGestion(String mvarRequest) {
		String mvarResponse;
		LBAW_MQMensajeGestion consultaMQ = new LBAW_MQMensajeGestion();
		StringHolder consultaMQSH = new StringHolder();
		consultaMQ.IAction_Execute(mvarRequest, consultaMQSH, "");
		mvarResponse = consultaMQSH.getValue();
		return mvarResponse;
	}
	
	/**
	 * Devuelve <LBA_WS res_code="code" res_msg="message"></LBA_WS> teniendo cuidado de escapear los atributos
	 * 
	 * 		 
	 * @param code
	 * @param message
	 * @return
	 */
	public static String createLBA_WS_XML(String code, String message) {
		String pattern = "<LBA_WS res_code=\"{0}\" res_msg=\"{1}\"></LBA_WS>";
		return MessageFormat.format(pattern, StringEscapeUtils.escapeXml(code), StringEscapeUtils.escapeXml(message) );
	}

	/**
	 * Devuelve <LBA_WS res_code="code" res_msg="message"></LBA_WS> teniendo cuidado de escapear los atributos
	 * 
	 * 		 
	 * @param code como int
	 * @param message
	 * @return
	 */
	public static String createLBA_WS_XML(int code, String message) {
		String pattern = "<LBA_WS res_code=\"{0}\" res_msg=\"{1}\"></LBA_WS>";
		return MessageFormat.format(pattern, StringEscapeUtils.escapeXml(Integer.toString(code)), StringEscapeUtils.escapeXml(message) );
	}

	protected static String createResponseXmlFalseWithMessage(String message) {
		return "<Response><Estado resultado=\"false\" mensaje=\"" + StringEscapeUtils.escapeXml(message) + "\" /></Response>";
	}
}
