package com.qbe.services.interWSBrok.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.interWSBrok.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Implementacion de los objetos
 * Objetos del FrameWork
 */

public class GetCotizacionATM implements VBObjectClass
{
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetCotizacionATM";
  static final String mcteStoreProc = "SPSNCV_BRO_ALTA_OPER";
  /**
   * Archivo de Configuracion
   */
  static final String mcteArchivoConfATM_XML = "LBA_PARAM_ATM.XML";
  static final String mcteArchivoATM_XML = "LBA_VALIDACION_COT_ATM.XML";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_Provi = "PROVCOD";
  static final String mcteParam_LocalidadCod = "LOCALIDAD";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  static final String mcteParam_COBROCOD = "FPAGO";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  /**
   * Par�metros agregados
   */
  static final String mcteParam_NROCOT = "COT_NRO";
  static final String mcteParam_TipoOperac = "TIPOOPERAC";
  static final String mcteParam_EstadoProc = "ESTADOPROC";
  static final String mcteParam_PedidoAno = "MSGANO";
  static final String mcteParam_PedidoMes = "MSGMES";
  static final String mcteParam_PedidoDia = "MSGDIA";
  static final String mcteParam_PedidoHora = "MSGHORA";
  static final String mcteParam_PedidoMinuto = "MSGMINUTO";
  static final String mcteParam_PedidoSegundo = "MSGSEGUNDO";
  static final String mcteParam_TiempoProceso = "TIEMPOPROCESO";
  static final String mcteParam_CERTISEC = "CERTISEC";
  static final String mcteParam_RAMOPCOD = "RAMOPCOD";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncInsertNode
   * static variable for method: fncPutCotHO
   * static variable for method: fncCotizarAtmMQ
   */
  private final String wcteFnName = "fncCotizarAtmMQ";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant wvarCodErr = new Variant();
    Variant pvarRes = new Variant();
    HSBCInterfaces.IAction wobjClass = null;
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      pvarRequest = Strings.mid( pvarRequest, 1, Strings.find( 1, pvarRequest, ">" ) ) + "<MSGANO>" + DateTime.year( DateTime.now() ) + "</MSGANO>" + "<MSGMES>" + DateTime.month( DateTime.now() ) + "</MSGMES>" + "<MSGDIA>" + DateTime.day( DateTime.now() ) + "</MSGDIA>" + "<MSGHORA>" + DateTime.hour( DateTime.now() ) + "</MSGHORA>" + "<MSGMINUTO>" + DateTime.minute( DateTime.now() ) + "</MSGMINUTO>" + "<MSGSEGUNDO>" + DateTime.second( DateTime.now() ) + "</MSGSEGUNDO>" + Strings.mid( pvarRequest, (Strings.find( 1, pvarRequest, ">" ) + 1) );
      wvarStep = 10;
      pvarRes.set( "" );
      wvarMensaje.set( "" );
      wvarCodErr.set( "" );
      //
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(new Variant( pvarRequest )), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*TBD mobjCOM_Context.SetAbort() ;*/
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*TBD mobjCOM_Context.SetComplete() ;*/
        }
        return IAction_Execute;
      }
      //
      pvarResponse.set( pvarRes );
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetComplete() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( Variant pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParams = null;
    XmlDomExtended wobjXMLReturnVal = null;
    XmlDomExtended wobjXMLError = null;
    org.w3c.dom.Node wobjXMLChildNode = null;
    Object wobjClass = null;
    Variant wvarMensajeStoreProc = new Variant();
    Variant wvarMensajePutTran = new Variant();
    String mvar_Estado = "";
    String mvar_precio = "";
    String mvar_CodZona = "";
    String mvar_TiempoProceso = "";
    String mvarInicioAIS = "";
    String mvarFinAIS = "";
    String mvarTiempoAIS = "";

    //
    //
    // variables para calcular tiempo proceso AIS
    try 
    {

      //Le agrega los Nodos de Valor Fijo
      wvarStep = 10;
      wobjXMLParams = new XmlDomExtended();
      wobjXMLParams.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteArchivoConfATM_XML));
      //
      wvarStep = 15;
      invoke( "fncInsertNode", new Variant[] { new Variant(pvarRequest), new Variant("//Request"), new Variant( XmlDomExtended .getText( wobjXMLParams.selectSingleNode( new Variant("//BROKERS/ATM/COTIZACION") )  )), new Variant("TIPOOPERAC") } );
      invoke( "fncInsertNode", new Variant[] { new Variant(pvarRequest), new Variant("//Request"), new Variant( XmlDomExtended .getText( wobjXMLParams.selectSingleNode( new Variant("//BROKERS/ATM/RAMOPCOD") )  )), new Variant("RAMOPCOD") } );
      //
      //Instancia la clase de validacion
      wvarMensaje.set( "" );
      wvarMensajeStoreProc.set( "" );
      //
      wvarStep = 20;
      wobjClass = new GetValidacionCotATM();
      wobjClass.Execute( pvarRequest, wvarMensajeStoreProc, "" );
      wobjClass = null;
      //
      //Analizo la respuesta del validador
      wvarStep = 25;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( wvarMensajeStoreProc.toString() );
      //
      //Si la respuesta viene con error
      wvarStep = 30;
      if( ! ( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ).equals( "0" )) )
      {
        wvarCodErr.set( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ) );
        wvarMensaje.set( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" )  ) );
        if( wvarCodErr.toString().equals( "-2" ) )
        {
          mvar_Estado = "NI";
        }
        else
        {
          mvar_Estado = "ER";
        }
        //En el caso de que no llegue a cotizar, tomo el XML original para el ALTA OPER
        wvarStep = 35;
        pvarRes.set( wvarMensajeStoreProc );
        wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
      }
      else
      {

        //En el caso de que la Validacion est� Ok, obtengo la Cotizacion
        //De la validaci�n vienen los datos de NROCOTI , CERTISEC = 0 , PLANDEFEC/TPLANATM
        wvarStep = 40;

        mvarInicioAIS = DateTime.format( DateTime.now() );
        //Cotizo . Llamo al msg gen�rico 2200
        if( invoke( "fncCotizarAtmMQ", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje.toString()), new Variant(wvarCodErr), new Variant(pvarRes) } ) )
        {
          mvarFinAIS = DateTime.format( DateTime.now() );
          // devuelvo pvarRes con precio y prima
          wvarStep = 45;
          wobjXMLReturnVal = new XmlDomExtended();
          wobjXMLReturnVal.loadXML( pvarRes.toString() );
          if( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
          {
            mvar_Estado = "ER";
          }
          else
          {
            mvar_Estado = XmlDomExtended.getText( wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@resultado" )  );
          }
          wvarStep = 50;
          if( ! ( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" )) )
          {
            //hubo un error
            pvarRes.set( "<LBA_WS res_code=\"-300\" res_msg=\"" + XmlDomExtended.getText( wobjXMLReturnVal.selectSingleNode( "//Response/Estado/@mensaje" )  ) + "\"></LBA_WS>" );
          }
          else
          {

            // Devuelvo el mensaje de salida
            pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + pvarRes + "</LBA_WS>" );

            //Tomo el valor del Precio
            wvarStep = 90;
            //EG 10/2010 No se toma en cuenta  la zona
            //  mvar_precio = wobjXMLReturnVal.selectSingleNode("//RECTOIMP").Text
            mvar_CodZona = "0";

            mvar_TiempoProceso = String.valueOf( DateTime.diff( "S", DateTime.toDate( mvarInicioAIS ), DateTime.toDate( mvarFinAIS ) ) );

            wvarStep = 96;
            wobjXMLReturnVal = null;
            //
          }
        }

        // paso el estado para guardar en la tabla BRO_OPERACIONES
        invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvar_Estado), new Variant("ESTADOPROC") } );

        // paso el tiempo de proceso AIS para guardar en la tabla BRO_OPERACIONES
        invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvar_TiempoProceso), new Variant("TIEMPOPROCESO") } );

        //Alta de la OPERACION . SPSNCV_BRO_ALTA_OPER
        wvarStep = 130;
        //
        if( invoke( "fncPutCotHO", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje.toString()), new Variant(wvarCodErr), new Variant(wvarMensajePutTran) } ) )
        {
          wobjXMLReturnVal = new XmlDomExtended();
          wobjXMLReturnVal.loadXML( wvarMensajePutTran.toString() );
          wvarStep = 140;
          if( ! ( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ).equals( "0" )) )
          {
            wvarStep = 150;
            wvarCodErr.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ) );
            wvarMensaje.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" )  ) );
            pvarRes.set( wvarMensajePutTran );
            return fncGetAll;
          }
          wobjXMLReturnVal = null;
          wobjClass = null;
          fncGetAll = true;
        }
        //
      }


      fin: 
      wobjClass = null;
      wobjXMLRequest = null;
      wobjXMLParams = null;
      wobjXMLReturnVal = null;
      wobjXMLError = null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;
        /*TBD mobjCOM_Context.SetComplete() ;*/

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }

  private boolean fncInsertNode( Variant pvarRequest, String pvarHeader, String pvarNodeValue, String pvarNodeName )
  {
    boolean fncInsertNode = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;


    try 
    {
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest.toString() );
      //
      wvarStep = 20;
      /*unsup wobjXMLRequest.selectSingleNode( pvarHeader ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), pvarNodeName, "" ) */ );
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//" + pvarNodeName ) , pvarNodeValue );
      //
      pvarRequest.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
      wobjXMLRequest = null;
      fncInsertNode = true;
      //
      fin: 
      return fncInsertNode;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //AM Debug
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncInsertNode = false;
        /*TBD mobjCOM_Context.SetComplete() ;*/

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncInsertNode;
  }

  private boolean fncPutCotHO( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutCotHO = false;
    int wvarStep = 0;
    HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    XmlDomExtended wobjXMLRequest = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_NROCOT = "";
    String mvarWDB_TIPOOPERAC = "";
    String mvarWDB_ESTADO = "";
    String mvarWDB_RECEPCION_PEDIDOANO = "";
    String mvarWDB_RECEPCION_PEDIDOMES = "";
    String mvarWDB_RECEPCION_PEDIDODIA = "";
    String mvarWDB_RECEPCION_PEDIDOHORA = "";
    String mvarWDB_RECEPCION_PEDIDOMINUTO = "";
    String mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_TIEMPOPROCESO = "";
    String mvarWDB_XML = "";

    //
    //
    //
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_CODINST = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST )  );
      }
      //
      wvarStep = 30;
      mvarWDB_REQUESTID = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID )  );
      }
      //
      wvarStep = 40;
      mvarWDB_NROCOT = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROCOT) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NROCOT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NROCOT )  );
      }
      //
      wvarStep = 50;
      mvarWDB_TIPOOPERAC = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoOperac) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPOOPERAC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoOperac )  );
      }
      //
      wvarStep = 60;
      mvarWDB_ESTADO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EstadoProc) )  == (org.w3c.dom.Node) null) )
      {
        //EG 10/2010 cambia el valor del tag al actualizarce componente
        mvarWDB_ESTADO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_EstadoProc )  );
        if( mvarWDB_ESTADO.equals( "true" ) )
        {
          mvarWDB_ESTADO = "OK";
        }
        else
        {
          mvarWDB_ESTADO = "ER";
        }
      }
      //
      wvarStep = 70;
      mvarWDB_RECEPCION_PEDIDOANO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoAno) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOANO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoAno )  );
      }
      //
      wvarStep = 80;
      mvarWDB_RECEPCION_PEDIDOMES = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMes) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMes )  );
      }
      //
      wvarStep = 90;
      mvarWDB_RECEPCION_PEDIDODIA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoDia) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDODIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoDia )  );
      }
      //
      wvarStep = 100;
      mvarWDB_RECEPCION_PEDIDOHORA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoHora) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOHORA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoHora )  );
      }
      //
      wvarStep = 110;
      mvarWDB_RECEPCION_PEDIDOMINUTO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMinuto) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMINUTO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMinuto )  );
      }
      //
      wvarStep = 120;
      mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoSegundo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOSEGUNDO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoSegundo )  );
      }
      //
      wvarStep = 130;
      mvarWDB_RAMOPCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAMOPCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_RAMOPCOD )  );
      }
      //
      wvarStep = 140;
      mvarWDB_POLIZANN = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) )  == (org.w3c.dom.Node) null) )
      {
        if( new Variant( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN )  ) ).isNumeric() )
        {
          if( Obj.toInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) )  ) ) < 100 )
          {
            mvarWDB_POLIZANN = String.valueOf( VB.val( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN )  ) ) );
          }
        }
      }
      //
      wvarStep = 150;
      mvarWDB_POLIZSEC = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) )  == (org.w3c.dom.Node) null) )
      {
        if( new Variant( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC )  ) ).isNumeric() )
        {
          if( Obj.toInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) )  ) ) < 1000000 )
          {
            mvarWDB_POLIZSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC )  );
          }
        }
      }
      //
      wvarStep = 160;
      mvarWDB_CERTISEC = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CERTISEC) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTISEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CERTISEC )  );
      }
      //
      wvarStep = 170;
      mvarWDB_TIEMPOPROCESO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TiempoProceso) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIEMPOPROCESO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TiempoProceso )  );
      }
      //
      wvarStep = 180;
      mvarWDB_XML = "";
      mvarWDB_XML = pvarRequest;
      //
      wvarStep = 190;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.mcteDB);
      //
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      
      //
      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      
      //
      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_NRO_COTIZACION_LBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NROCOT.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NROCOT)) );
      
      //
      wvarStep = 230;
      
      
      //
      wvarStep = 240;
      
      
      //
      wvarStep = 250;
      
      
      //
      wvarStep = 260;
      
      
      //
      wvarStep = 270;
      
      
      //
      wvarStep = 280;
      wobjDBParm = new Parameter( "@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      
      //
      wvarStep = 290;
      wobjDBParm = new Parameter( "@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      
      //
      wvarStep = 300;
      wobjDBParm = new Parameter( "@WDB_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      
      //
      wvarStep = 310;
      
      
      //
      wvarStep = 320;
      wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CERTISEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CERTISEC)) );
      
      //
      wvarStep = 330;
      wobjDBParm = new Parameter( "@WDB_TIEMPO_PROCESO_AIS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIEMPOPROCESO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIEMPOPROCESO)) );
      
      //
      wvarStep = 340;
      
      
      //
      wvarStep = 350;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      //
      wobjDBCnn.close();
      //
      wvarStep = 360;
      fncPutCotHO = true;
      pvarRes.set( "<LBA_WS res_code=\"" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ) + "\" res_msg=\"" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" )  ) + "\"></LBA_WS>" );
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = null;
      wobjClass = (HSBCInterfaces.IAction) null;
      return fncPutCotHO;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        fncPutCotHO = false;
        /*TBD mobjCOM_Context.SetComplete() ;*/


        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutCotHO;
  }

  private boolean fncCotizarAtmMQ( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncCotizarAtmMQ = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String mvarRequest = "";
    String mvarResponse = "";
    Variant oProd = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended mobjXMLDoc = null;
    Object wobjClass = null;
    String mvarProdok = "";
    org.w3c.dom.NodeList wobjXMLList = null;
    int wvarContNode = 0;
    String vNROCOTI = "";
    Variant vMES = new Variant();
    Variant vANO = new Variant();
    String vFECINIVG = "";
    Variant vMESsig = new Variant();
    String vANOsig = "";
    String vFECFINVG = "";
    String vTPLANATM = "";
    String vPOLIZANN = "";
    String vPOLIZSEC = "";
    String vREQUESTID = "";
    String vUSUARCOD = "";
    String Primer = "";





    wvarStep = 0;
    wvarCodErr.set( 0 );
    try 
    {

      wvarStep = 2;
      // Traigo los par�metros del request
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 3;
      vNROCOTI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/COT_NRO" )  );
      vREQUESTID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/REQUESTID" )  );


      vFECINIVG = Funciones.convertirFecha( new Variant( DateTime.dateSerial( DateTime.year( DateTime.now() ), DateTime.month( DateTime.now() ), 1 ) )/*warning: ByRef value change will be lost.*/ );
      vFECFINVG = Funciones.convertirFecha( new Variant( DateTime.dateSerial( DateTime.year( DateTime.now() ), DateTime.month( DateTime.now() ) + 1, 1 ) )/*warning: ByRef value change will be lost.*/ );
      vPOLIZANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/POLIZANN" )  );
      vPOLIZSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/POLIZSEC" )  );
      vTPLANATM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/TPLANATM" )  );
      vUSUARCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/USUARCOD" )  );

      //Armo el Request para cotizar
      mvarRequest = "<Request><DEFINICION>2200_OVMsjGralCotizacion.xml</DEFINICION>";
      mvarRequest = mvarRequest + "  <NROCOTI>" + vNROCOTI + "</NROCOTI>";
      mvarRequest = mvarRequest + "  <RAMOPCOD>ATA1</RAMOPCOD>";
      mvarRequest = mvarRequest + "  <POLIZANN>" + vPOLIZANN + "</POLIZANN>";
      mvarRequest = mvarRequest + "  <POLIZSEC>" + vPOLIZSEC + "</POLIZSEC>";
      mvarRequest = mvarRequest + "  <FECSOLIC>0</FECSOLIC>";
      mvarRequest = mvarRequest + "  <FECINIVG>" + vFECINIVG + "</FECINIVG>";
      mvarRequest = mvarRequest + "  <FECFINVG>" + vFECFINVG + "</FECFINVG>";
      mvarRequest = mvarRequest + "  <AGENTCLAPR1>PR</AGENTCLAPR1>";
      mvarRequest = mvarRequest + "  <AGENTCODPR1>0</AGENTCODPR1>";
      mvarRequest = mvarRequest + "  <AGENTCLAPR2/>";
      mvarRequest = mvarRequest + "  <AGENTCODPR2>0</AGENTCODPR2>";
      mvarRequest = mvarRequest + "  <AGENTCLAOR>OR</AGENTCLAOR>";
      mvarRequest = mvarRequest + "  <AGENTCODOR>0</AGENTCODOR>";
      mvarRequest = mvarRequest + "  <COMISIONPR1/>";
      mvarRequest = mvarRequest + "  <COMISIONPR2>0</COMISIONPR2>";
      mvarRequest = mvarRequest + "  <COMISIONOR/>";
      mvarRequest = mvarRequest + "  <PLANPCOD>0</PLANPCOD>";
      mvarRequest = mvarRequest + "  <COBROCOD>0</COBROCOD>";
      mvarRequest = mvarRequest + "  <COBROTIP/>";
      mvarRequest = mvarRequest + "  <PROFECOD/>";
      mvarRequest = mvarRequest + "  <OPERATIP>AP</OPERATIP>";
      mvarRequest = mvarRequest + "  <COBROFOR>0</COBROFOR>";
      mvarRequest = mvarRequest + "  <CAUSAEND>0</CAUSAEND>";
      mvarRequest = mvarRequest + "  <CLIENTIP>0</CLIENTIP>";
      mvarRequest = mvarRequest + "  <CLIENSEX/>";
      mvarRequest = mvarRequest + "  <FECNACIM>0</FECNACIM>";
      mvarRequest = mvarRequest + "  <CLIENEST/>";
      mvarRequest = mvarRequest + "  <NACIONAL>00</NACIONAL>";
      mvarRequest = mvarRequest + "  <CLIENIVA>3</CLIENIVA>";
      mvarRequest = mvarRequest + "  <CLIEIBTP>0</CLIEIBTP>";
      mvarRequest = mvarRequest + "  <CLAUAJUS>0</CLAUAJUS>";
      mvarRequest = mvarRequest + "  <TPLANATM>" + vTPLANATM + "</TPLANATM>";
      mvarRequest = mvarRequest + "  <PARTICOD>0</PARTICOD>";
      mvarRequest = mvarRequest + "  <LOCALCOD>0</LOCALCOD>";
      mvarRequest = mvarRequest + "  <DOMICCPO>X0000</DOMICCPO>";
      mvarRequest = mvarRequest + "  <PROVICOD>1</PROVICOD>";
      mvarRequest = mvarRequest + "  <PAISSCOD>00</PAISSCOD></Request>";

      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( new Variant( mvarRequest ), new Variant( mvarResponse ), "" );
      wobjClass = null;
      mobjXMLDoc = new XmlDomExtended();
      mobjXMLDoc.loadXML( mvarResponse );

      mvarProdok = "ER";
      if( ! (mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended .getText( mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          //Armo la salida
          pvarRes.set( "" );
          pvarRes.set( pvarRes + "<Response><Estado resultado='true' mensaje='' />" );
          pvarRes.set( pvarRes + "<REQUESTID>" + vREQUESTID + "</REQUESTID>" );
          pvarRes.set( pvarRes + "<COT_NRO>" + vNROCOTI + "</COT_NRO>" );
          pvarRes.set( pvarRes + "<TPLANATM>" + vTPLANATM + "</TPLANATM>" );
          pvarRes.set( pvarRes + "<USUARCOD>" + vUSUARCOD + "</USUARCOD>" );
          pvarRes.set( pvarRes + "<COMP_PRECIO>" );
          pvarRes.set( pvarRes + "<PRIMA>" + XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//PRIMAIMP" )  ) + "</PRIMA>" );
          pvarRes.set( pvarRes + "<RECARGOS>" + XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//RECAIMPO" )  ) + "</RECARGOS>" );
          pvarRes.set( pvarRes + "<IVAIMPOR>" + XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//IVAIMPOR" )  ) + "</IVAIMPOR>" );
          pvarRes.set( pvarRes + "<IVAIMPOA>" + XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//IVAIMPOA" )  ) + "</IVAIMPOA>" );
          pvarRes.set( pvarRes + "<IVARETEN>" + XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//IVARETEN" )  ) + "</IVARETEN>" );
          pvarRes.set( pvarRes + "<DEREMI>" + XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//DEREMIMP" )  ) + "</DEREMI>" );
          pvarRes.set( pvarRes + "<SELLADO>" + XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//SELLAIMP" )  ) + "</SELLADO>" );
          pvarRes.set( pvarRes + "<INGBRU>" + XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//INGBRIMP" )  ) + "</INGBRU>" );
          pvarRes.set( pvarRes + "<IMPUES>" + XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//IMPUEIMP" )  ) + "</IMPUES>" );
          pvarRes.set( pvarRes + "<PRECIO>" + XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//RECTOIMP" )  ) + "</PRECIO>" );
          pvarRes.set( pvarRes + "</COMP_PRECIO></Response>" );

          //pvarRes = mvarResponse
          mvarProdok = "OK";
        }
      }

      if( mvarProdok.equals( "ER" ) )
      {
        wvarCodErr.set( -53 );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=Poliza  no habilitada /></Response>" );
        return fncCotizarAtmMQ;
      }


      mobjXMLDoc = null;
      wobjXMLRequest = null;
      wobjXMLList = (org.w3c.dom.NodeList) null;

      wvarStep = 240;
      if( wvarCodErr.toInt() == 0 )
      {
        fncCotizarAtmMQ = true;
      }
      else
      {
        fncCotizarAtmMQ = false;
      }

      fin: 
      return fncCotizarAtmMQ;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=\"" + General.mcteErrorInesperadoDescr + "\" /></Response>" );
        wvarCodErr.set( -1000 );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        //fncCotizarAtmMQ False
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncCotizarAtmMQ;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
