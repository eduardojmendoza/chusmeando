package com.qbe.services.interWSBrok.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.interWSBrok.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Implementacion de los objetos
 * Objetos del FrameWork
 */

public class GetSolicitudHO implements VBObjectClass
{
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetSolicitudHO";
  static final String mcteStoreProcSelect = "SPSNCV_BRO_SELECT_OPER_ESP";
  static final String mcteStoreProc = "SPSNCV_BRO_ALTA_OPER";
  static final String mcteStoreProcAlta = "SPSNCV_BRO_SOLI_HOM1_GRABA";
  /**
   * 10/2010 sp para descripciones del impreso
   */
  static final String mcteStoreProcImpre = "SPSNCV_BRO_SOLI_HOM1_IMPRE";
  static final String mcteStorePImpreCob = "SPSNCV_BRO_SOLI_HOM1_IMP_COB";
  /**
   * Archivo de Configuracion
   */
  static final String mcteArchivoConfHOM_XML = "LBA_PARAM_HO.XML";
  static final String mcteArchivoHOM_XML = "LBA_VALIDACION_COT_HO.XML";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_TipoOperac = "TIPOOPERAC";
  static final String mcteParam_EstadoProc = "ESTADOPROC";
  static final String mcteParam_TiempoProceso = "TIEMPOPROCESO";
  static final String mcteParam_PedidoAno = "MSGANO";
  static final String mcteParam_PedidoMes = "MSGMES";
  static final String mcteParam_PedidoDia = "MSGDIA";
  static final String mcteParam_PedidoHora = "MSGHORA";
  static final String mcteParam_PedidoMinuto = "MSGMINUTO";
  static final String mcteParam_PedidoSegundo = "MSGSEGUNDO";
  static final String mcteParam_DOMICCPO = "DOMICCPO";
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  /**
   *  Se cambian los 4 nodos x utilizarse el nuevo componente de cotizacion
   * Const mcteParam_COBROCOD             As String = "COBROCOD"
   */
  static final String mcteParam_COBROCOD = "FPAGO";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_TIPOHOGAR = "TIPOHOGAR";
  /**
   * Const mcteParam_PLANNCOD             As String = "PLANNCOD"
   */
  static final String mcteParam_PLANNCOD = "PLAN";
  /**
   * Const mcteParam_Provi                As String = "PROVI"
   */
  static final String mcteParam_Provi = "PROVCOD";
  /**
   * Const mcteParam_LocalidadCod         As String = "LOCALIDADCOD"
   */
  static final String mcteParam_LocalidadCod = "LOCALIDAD";
  static final String mcteParam_RAMOPCOD = "RAMOPCOD";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  static final String mcteParam_NROCOT = "COT_NRO";
  static final String mcteParam_CERTISEC = "CERTISEC";
  static final String mcteParam_ClienAp1 = "CLIENAP1";
  static final String mcteParam_ClienAp2 = "CLIENAP2";
  static final String mcteParam_ClienNom = "CLIENNOM";
  static final String mcteParam_CLIENSEX = "SEXO";
  static final String mcteParam_CLIENEST = "ESTADO";
  static final String mcteParam_NacimAnn = "NACIMANN";
  static final String mcteParam_NacimMes = "NACIMMES";
  static final String mcteParam_NacimDia = "NACIMDIA";
  static final String mcteParam_EMAIL = "EMAIL";
  static final String mcteParam_DOMICDOM = "DOMICDOM";
  static final String mcteParam_DomicDnu = "DOMICDNU";
  static final String mcteParam_DOMICPIS = "DOMICPIS";
  static final String mcteParam_DomicPta = "DOMICPTA";
  static final String mcteParam_DOMICPOB = "DOMICPOB";
  static final String mcteParam_BARRIOCOUNT = "BARRIOCOUNT";
  /**
   * 10/2010 se agrega codigo de �rea
   */
  static final String mcteParam_TELCOD = "TELCOD";
  static final String mcteParam_TELNRO = "TELNRO";
  static final String mcteParam_DomicDomCo = "DOMICDOMCOR";
  static final String mcteParam_DomicDNUCo = "DOMICDNUCOR";
  static final String mcteParam_DomicPisCo = "DOMICPISCOR";
  static final String mcteParam_DomicPtaCo = "DOMICPTACOR";
  static final String mcteParam_DOMICPOBCo = "DOMICPOBCOR";
  static final String mcteParam_ProviCo = "PROVICOR";
  static final String mcteParam_LocalidadCodCo = "LOCALIDADCODCOR";
  /**
   * 10/2010 se agrega codigo de �rea
   */
  static final String mcteParam_TELCOD_CO = "TELCODCOR";
  static final String mcteParam_TelNroCo = "TELNROCOR";
  /**
   * Const mcteParam_TipoCuen             As String = "TIPOCUEN"
   */
  static final String mcteParam_CUENNUME = "CUENNUME";
  /**
   * Const mcteParam_TarjeCod             As String = "TARJECOD"
   */
  static final String mcteParam_VENCIANN = "VENCIANN";
  static final String mcteParam_VENCIMES = "VENCIMES";
  static final String mcteParam_VENCIDIA = "VENCIDIA";
  static final String mcteParam_UsoTipos = "USOTIPOS";
  static final String mcteParam_ALARMTIP = "ALARMTIP";
  static final String mcteParam_GUARDTIP = "GUARDTIP";
  static final String mcteParam_SWCALDER = "CALDERA";
  static final String mcteParam_SWASCENS = "ASCENSOR";
  static final String mcteParam_SWCREJAS = "REJAS";
  static final String mcteParam_SWPBLIND = "PUERTABLIND";
  static final String mcteParam_SWDISYUN = "DISYUNTOR";
  static final String mcteParam_SWPROPIE = "PROPIEDAD";
  static final String mcteParam_ZONA = "CODIZONA";
  static final String mcteParam_TipoDocu = "TIPODOCU";
  static final String mcteParam_NumeDocu = "NUMEDOCU";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS/COBERTURA";
  static final String mcteParam_COBERCOD = "COBERCOD";
  static final String mcteParam_CONTRMOD = "CONTRMOD";
  static final String mcteParam_CAPITASG = "CAPITASG";
  /**
   * DATOS DE LAS CAMPA�AS
   */
  static final String mcteNodos_Campa = "//Request/CAMPANIAS/CAMPANIA";
  static final String mcteParam_CampaCod = "CAMPACOD";
  /**
   * DATOS DE LOS ELECTRODOMESTICOS
   */
  static final String mcteNodos_Elect = "//Request/ELECTRODOMESTICOS/ELECTRODOMESTICO";
  static final String mcteParam_Tipo = "TIPO";
  static final String mcteParam_Marca = "MARCA";
  static final String mcteParam_Modelo = "MODELO";
  /**
   * AM
   */
  static final String mcteParam_BANCOCOD = "//BANCOCOD";
  static final String mcteParam_EmpresaCodigo = "//CANAL";
  static final String mcteParam_SucursalCodigo = "//CANAL_SUCURSAL";
  static final String mcteParam_LegajoVend = "//LEGAJO_VEND";
  static final String mcteParam_LegajoCia = "//LEGAJCIA";
  /**
   * Nro NewSas para mcteParam_EmpresaCodigo(CANAL) = 150 (HSBC)
   */
  static final String mcteParam_NROSOLI = "//NROSOLI";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncInsertNode
   * static variable for method: fncPutSolHO
   * static variable for method: fncPutSolicitud
   */
  private final String wcteFnName = "fncPutSolicitud";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant wvarCodErr = new Variant();
    Variant pvarRes = new Variant();
    HSBCInterfaces.IAction wobjClass = null;
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      pvarRequest = Strings.mid( pvarRequest, 1, Strings.find( 1, pvarRequest, ">" ) ) + "<MSGANO>" + DateTime.year( DateTime.now() ) + "</MSGANO>" + "<MSGMES>" + DateTime.month( DateTime.now() ) + "</MSGMES>" + "<MSGDIA>" + DateTime.day( DateTime.now() ) + "</MSGDIA>" + "<MSGHORA>" + DateTime.hour( DateTime.now() ) + "</MSGHORA>" + "<MSGMINUTO>" + DateTime.minute( DateTime.now() ) + "</MSGMINUTO>" + "<MSGSEGUNDO>" + DateTime.second( DateTime.now() ) + "</MSGSEGUNDO>" + Strings.mid( pvarRequest, (Strings.find( 1, pvarRequest, ">" ) + 1) );
      wvarStep = 10;
      wvarMensaje.set( "" );
      wvarCodErr.set( "" );
      pvarRes.set( "" );
      //
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(new Variant( pvarRequest )), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*TBD mobjCOM_Context.SetAbort() ;*/
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*TBD mobjCOM_Context.SetComplete() ;*/
        }
        return IAction_Execute;
      }

      pvarResponse.set( pvarRes );
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetComplete() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( Variant pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParams = null;
    XmlDomExtended wobjXMLReturnVal = null;
    XmlDomExtended wobjXMLError = null;
    XmlDomExtended wobjXMLRequestExists = null;
    Object wobjClass = null;
    Variant wvarMensajeStoreProc = new Variant();
    Variant wvarMensajePutTran = new Variant();
    String mvar_Estado = "";
    String mvarCertiSec = "";
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;

    //
    //
    //
    try 
    {
      //Le agrega los Nodos de Valor Fijo
      wvarStep = 10;
      wobjXMLParams = new XmlDomExtended();
      wobjXMLParams.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteArchivoConfHOM_XML));
      //
      invoke( "fncInsertNode", new Variant[] { new Variant(pvarRequest), new Variant("//Request"), new Variant( XmlDomExtended .getText( wobjXMLParams.selectSingleNode( new Variant("//BROKERS/HOGAR/SOLICITUD") )  )), new Variant("TIPOOPERAC") } );
      invoke( "fncInsertNode", new Variant[] { new Variant(pvarRequest), new Variant("//Request"), new Variant( XmlDomExtended .getText( wobjXMLParams.selectSingleNode( new Variant("//BROKERS/HOGAR/RAMOPCOD") )  )), new Variant("RAMOPCOD") } );
      //
      //Instancia la clase de validacion
      wvarMensaje.set( "" );
      wvarMensajeStoreProc.set( "" );
      //
      wvarStep = 20;
      wobjClass = new GetValidacionSolHO();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(pvarRequest, wvarMensajeStoreProc, "")
      wobjClass = null;
      //
      //Analizo la respuesta del validador
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( wvarMensajeStoreProc.toString() );
      //
      //Si la respuesta viene con error
      wvarStep = 30;
      if( ! ( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ).equals( "0" )) )
      {
        wvarCodErr.set( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ) );
        wvarMensaje.set( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" )  ) );
        
        if( wvarCodErr.toString().equals( "-2" ) )
        {
          //Este es el caso que no venga el cod. de broker o el nro. de operacion
          wvarStep = 35;
          mvar_Estado = "NI";
          pvarRes.set( wvarMensajeStoreProc );
          wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
        }
        else if( wvarCodErr.toString().equals( "H010" ) )
        {
          //Este es el caso que ya existe la solicitud
          wobjXMLRequestExists = new XmlDomExtended();
          wobjXMLRequestExists.loadXML( pvarRequest.toString() );

          wvarStep = 40;
          com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
          //error: function 'GetDBConnection' was not found.
          wobjDBCnn = connFactory.getDBConnection(ModGeneral.mcteDB);
          wobjDBCmd = new Command();
          wobjDBCmd.setActiveConnection( wobjDBCnn );
          wobjDBCmd.setCommandText( mcteStoreProcSelect );
          wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
          //
          wvarStep = 50;
          
          
          //
          wvarStep = 52;
          
          
          //
          wvarStep = 54;
          
          
          //
          wvarStep = 56;
          
          
          //
          wvarStep = 58;
          wrstRes = new Recordset();
          wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
          wvarCodErr.set( "0" );
          wvarMensaje.set( "" );
          pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + "<Response>" + "<REQUESTID>" + XmlDomExtended.getText( wobjXMLRequestExists.selectSingleNode( "//REQUESTID" )  ) + "</REQUESTID>" + "<CERTISEC>" + wrstRes.getFields().getField(12).getValue() + "</CERTISEC>" + "</Response>" + "</LBA_WS>" );
          mvarCertiSec = wrstRes.getFields().getField(12).getValue().toString();
          mvar_Estado = "OK";
          wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
          //Le agrego el Nodo de CertiSec al XML original
          invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvarCertiSec), new Variant("CERTISEC") } );
          wobjXMLRequestExists = null;
          wobjDBCmd = (Command) null;
          wobjDBCnn = (Connection) null;
          wobjHSBC_DBCnn = null;
        }
        else
        {
          mvar_Estado = "ER";
          pvarRes.set( wvarMensajeStoreProc );
          wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );

        }

      }
      else
      {
        //Guarda la Solicitud en el SP
        wvarStep = 60;
        //Nueva funcion
        if( invoke( "fncPutSolicitud", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje.toString()), new Variant(wvarCodErr), new Variant(pvarRes) } ) )
        {

          wobjXMLReturnVal = new XmlDomExtended();
          wobjXMLReturnVal.loadXML( pvarRes.toString() );
          //
          if( ! ( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ).equals( "0" )) )
          {
            //hubo un error
            wvarStep = 65;
            wvarCodErr.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ) );
            wvarMensaje.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" )  ) );
            wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
            mvar_Estado = "ER";

            //Este es el cado en que no haya contestado el SP o sea un error no identificado
          }
          else
          {
            wvarStep = 70;
            mvar_Estado = "OK";
            XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//CERTISEC" ) , XmlDomExtended.getText( wobjXMLReturnVal.selectSingleNode( "//CERTISEC" )  ) );
            wvarMensajeStoreProc.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
          }
          //
          wobjXMLReturnVal = null;

        }
        //
      }
      invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvar_Estado), new Variant("ESTADOPROC") } );
      //Alta de la OPERACION
      wvarStep = 75;
      if( invoke( "fncPutSolHO", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje.toString()), new Variant(wvarCodErr), new Variant(wvarMensajePutTran) } ) )
      {
        //
        wobjXMLReturnVal = new XmlDomExtended();
        wobjXMLReturnVal.loadXML( wvarMensajePutTran.toString() );
        //
        wvarStep = 80;
        if( ! ( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ).equals( "0" )) )
        {
          wvarCodErr.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ) );
          wvarMensaje.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" )  ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          fncGetAll = false;
          return fncGetAll;
        }
        //
        fncGetAll = true;
        //
      }
      fin: 
      wobjClass = null;
      wobjXMLRequest = null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;
        /*TBD mobjCOM_Context.SetComplete() ;*/

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }

  private boolean fncInsertNode( Variant pvarRequest, String pvarHeader, String pvarNodeValue, String pvarNodeName )
  {
    boolean fncInsertNode = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;


    try 
    {

      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest.toString() );
      //
      wvarStep = 20;
      /*unsup wobjXMLRequest.selectSingleNode( pvarHeader ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), pvarNodeName, "" ) */ );
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//" + pvarNodeName ) , pvarNodeValue );
      //
      pvarRequest.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
      wobjXMLRequest = null;
      fncInsertNode = true;
      return fncInsertNode;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncInsertNode = false;
        /*TBD mobjCOM_Context.SetComplete() ;*/

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncInsertNode;
  }

  private boolean fncPutSolHO( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutSolHO = false;
    int wvarStep = 0;
    HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    XmlDomExtended wobjXMLRequest = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_NROCOT = "";
    String mvarWDB_TIPOOPERAC = "";
    String mvarWDB_ESTADO = "";
    String mvarWDB_RECEPCION_PEDIDOANO = "";
    String mvarWDB_RECEPCION_PEDIDOMES = "";
    String mvarWDB_RECEPCION_PEDIDODIA = "";
    String mvarWDB_RECEPCION_PEDIDOHORA = "";
    String mvarWDB_RECEPCION_PEDIDOMINUTO = "";
    String mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_TIEMPOPROCESO = "";
    String mvarWDB_XML = "";
    String mvarWDB_CERTIANN = "";
    String mvarEmpresaCodigo = "";
    String mvarSucursalCodigo = "";

    //
    //
    //
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_CODINST = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST )  );
      }
      //
      wvarStep = 30;
      mvarWDB_REQUESTID = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID )  );
      }
      //
      wvarStep = 40;
      mvarWDB_NROCOT = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROCOT) )  == (org.w3c.dom.Node) null) )
      {
        if( new Variant( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NROCOT )  ) ).isNumeric() )
        {
          if( Strings.len( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROCOT) )  ) ) < 19 )
          {
            mvarWDB_NROCOT = String.valueOf( VB.val( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NROCOT )  ) ) );
          }
        }
      }
      //
      wvarStep = 50;
      mvarWDB_TIPOOPERAC = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoOperac) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPOOPERAC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoOperac )  );
      }
      //
      wvarStep = 60;
      mvarWDB_ESTADO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EstadoProc) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_ESTADO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_EstadoProc )  );
      }
      //
      wvarStep = 70;
      mvarWDB_RECEPCION_PEDIDOANO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoAno) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOANO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoAno )  );
      }
      //
      wvarStep = 80;
      mvarWDB_RECEPCION_PEDIDOMES = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMes) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMes )  );
      }
      //
      wvarStep = 90;
      mvarWDB_RECEPCION_PEDIDODIA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoDia) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDODIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoDia )  );
      }
      //
      wvarStep = 100;
      mvarWDB_RECEPCION_PEDIDOHORA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoHora) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOHORA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoHora )  );
      }
      //
      wvarStep = 110;
      mvarWDB_RECEPCION_PEDIDOMINUTO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMinuto) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMINUTO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMinuto )  );
      }
      //
      wvarStep = 120;
      mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoSegundo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOSEGUNDO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoSegundo )  );
      }
      //
      wvarStep = 130;
      mvarWDB_RAMOPCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAMOPCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_RAMOPCOD )  );
      }
      //
      wvarStep = 140;
      mvarWDB_POLIZANN = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) )  == (org.w3c.dom.Node) null) )
      {
        if( new Variant( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN )  ) ).isNumeric() )
        {
          if( Obj.toInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) )  ) ) < 100 )
          {
            mvarWDB_POLIZANN = String.valueOf( VB.val( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN )  ) ) );
          }
        }
      }
      //
      wvarStep = 150;
      mvarWDB_POLIZSEC = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) )  == (org.w3c.dom.Node) null) )
      {
        if( new Variant( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC )  ) ).isNumeric() )
        {
          if( Obj.toInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) )  ) ) < 1000000 )
          {
            mvarWDB_POLIZSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC )  );
          }
        }
      }
      //
      wvarStep = 160;
      mvarWDB_CERTISEC = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CERTISEC) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTISEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CERTISEC )  );
      }
      //
      wvarStep = 170;
      mvarWDB_TIEMPOPROCESO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TiempoProceso) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIEMPOPROCESO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TiempoProceso )  );
      }
      //
      wvarStep = 171;
      mvarEmpresaCodigo = "";
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_EmpresaCodigo )  == (org.w3c.dom.Node) null) )
      {
        mvarEmpresaCodigo = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EmpresaCodigo )  );
      }
      //
      wvarStep = 172;
      mvarSucursalCodigo = "";
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_SucursalCodigo )  == (org.w3c.dom.Node) null) )
      {
        mvarSucursalCodigo = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SucursalCodigo )  );
      }


      wvarStep = 180;
      mvarWDB_XML = "";
      mvarWDB_XML = pvarRequest;
      //
      wvarStep = 190;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.mcteDB);
      //
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      
      //
      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      
      //
      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_NRO_COTIZACION_LBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NROCOT.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NROCOT)) );
      
      //
      wvarStep = 230;
      
      
      //
      wvarStep = 240;
      
      
      //
      wvarStep = 250;
      
      
      //
      wvarStep = 260;
      
      
      //
      wvarStep = 270;
      
      
      //
      wvarStep = 280;
      wobjDBParm = new Parameter( "@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      
      //
      wvarStep = 290;
      wobjDBParm = new Parameter( "@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      
      //
      // Solo para el HSBC
      mvarWDB_CERTIANN = "0";
      if( Obj.toInt( mvarEmpresaCodigo ) == 150 )
      {
        mvarWDB_CODINST = "150";
        mvarWDB_CERTIANN = mvarSucursalCodigo;
        //mvarWDB_CERTISEC = 0
      }
      //
      wvarStep = 300;
      wobjDBParm = new Parameter( "@WDB_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      
      //
      wvarStep = 310;
      
      
      //
      wvarStep = 320;
      wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CERTISEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CERTISEC)) );
      
      //
      wvarStep = 330;
      wobjDBParm = new Parameter( "@WDB_TIEMPO_PROCESO_AIS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIEMPOPROCESO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIEMPOPROCESO)) );
      
      //
      wvarStep = 340;
      
      
      //
      wvarStep = 350;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      wobjDBCnn.close();

      fncPutSolHO = true;
      pvarRes.set( "<LBA_WS res_code=\"" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ) + "\" res_msg=\"" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" )  ) + "\"></LBA_WS>" );

      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = null;
      wobjClass = (HSBCInterfaces.IAction) null;
      return fncPutSolHO;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        fncPutSolHO = false;
        //mobjCOM_Context.SetComplete
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutSolHO;
  }

  private boolean fncPutSolicitud( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutSolicitud = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    LBAA_MWGenerico.lbaw_MQMW wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
    HSBC.DBConnection com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    XmlDomExtended wobjXMLRequest = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarcounter = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTIPOL = "";
    String mvarWDB_CERTIANN = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_SUPLENUM = "";
    String mvarWDB_NUMEDOCU = "";
    String mvarWDB_TIPODOCU = "";
    String mvarWDB_CLIENAP1 = "";
    String mvarWDB_CLIENAP2 = "";
    String mvarWDB_CLIENNOM = "";
    String mvarWDB_NACIMANN = "";
    String mvarWDB_NACIMMES = "";
    String mvarWDB_NACIMDIA = "";
    String mvarWDB_CLIENSEX = "";
    String mvarWDB_CLIENEST = "";
    String mvarWDB_EFECTANN = "";
    String mvarWDB_EFECTMES = "";
    String mvarWDB_EFECTDIA = "";
    String mvarWDB_EMAIL = "";
    String mvarWDB_DOMICDOM = "";
    String mvarWDB_DOMICDNU = "";
    String mvarWDB_DOMICPIS = "";
    String mvarWDB_DOMICPTA = "";
    String mvarWDB_DOMICPOB = "";
    String mvarWDB_BARRIOCOUNT = "";
    String mvarWDB_DOMICCPO = "";
    String mvarWDB_PROVICOD = "";
    String mvarTELCOD = "";
    String mvarWDB_TELNRO = "";
    String mvarWDB_DOMICDOM_CO = "";
    String mvarWDB_DOMICDNU_CO = "";
    String mvarWDB_DOMICPIS_CO = "";
    String mvarWDB_DOMICPTA_CO = "";
    String mvarWDB_DOMICPOB_CO = "";
    String mvarWDB_DOMICCPO_CO = "";
    String mvarWDB_PROVICOD_CO = "";
    String mvarTELCOD_CO = "";
    String mvarWDB_TELNRO_CO = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_CUENNUME = "";
    String mvarWDB_VENCIANN = "";
    String mvarWDB_VENCIMES = "";
    String mvarWDB_VENCIDIA = "";
    String mvarWDB_BANCOCOD = "";
    String mvarWDB_SUCURCOD = "";
    String mvarWDB_P_EMISIANN = "";
    String mvarWDB_P_EMISIMES = "";
    String mvarWDB_P_EMISIDIA = "";
    String mvarWDB_P_COBROCOD = "";
    String mvarWDB_P_TIVIVCOD = "";
    String mvarWDB_P_USOTIPOS = "";
    String mvarWDB_P_SWCALDER = "";
    String mvarWDB_P_SWASCENS = "";
    String mvarWDB_P_ALARMTIP = "";
    String mvarWDB_P_GUARDTIP = "";
    String mvarWDB_P_SWCREJAS = "";
    String mvarWDB_P_SWPBLIND = "";
    String mvarWDB_P_SWDISYUN = "";
    String mvarWDB_P_SWPROPIE = "";
    String mvarWDB_P_PLANNCOD = "";
    String mvarWDB_P_BARRIOCOUNT = "";
    String mvarWDB_P_ZONA = "";
    String mvarWDB_P_CLIENIVA = "";
    String mvarWDB_P_COBERCOD = "";
    String mvarWDB_P_CONTRMOD = "";
    String mvarWDB_P_CAPITASG = "";
    String mvarWDB_P_CAMPACOD = "";
    String mvarWDB_P_TIPO = "";
    String mvarWDB_P_MARCA = "";
    String mvarWDB_P_MODELO = "";
    String mvarNroCertiSec = "";
    String mvarDatosImpreso = "";
    String mvarNodoImpreso = "";
    String mvarImpreso64 = "";
    String mvarRequestImp = "";
    String mvarResponseImp = "";
    Recordset wrstDBResult = null;
    String mvarDOCUDES = "";
    String mvarCOBRODES = "";
    String mvarCAMPADES = "";
    String mvarNOMBROKER = "";
    String mvarPROVIDES = "";
    String mvarPROVIDESCO = "";
    String mvarRequest = "";
    String mvarResponse = "";
    XmlDomExtended mobjXMLDoc = null;
    String mvarPLANDES = "";
    String mvarUSOTIDES = "";
    String mvarALARMDES = "";
    String mvarGUARDDES = "";
    String mvarCOBERDES = "";
    String mvarseltest = "";
    int wvarCount = 0;
    int mvarSumaCob = 0;
    int wvariCounter = 0;
    String mvarEmpresaCodigo = "";
    String mvarSucursalCodigo = "";
    String mvarLegajoVend = "";
    String mvarLegajoCia = "";
    String mvarNROSOLI = "";
    org.w3c.dom.Node oPlan = null;

    //
    //
    //
    //10/2010 agrega area
    //10/2010 agrega area
    //10/2010 variables para armar el impreso
    //fin impreso
    //AM
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_CODINST = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST )  );
      }
      //
      wvarStep = 30;
      mvarWDB_RAMOPCOD = "HOM1";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAMOPCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_RAMOPCOD )  );
      }
      //
      wvarStep = 40;
      mvarWDB_POLIZANN = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN )  );
      }
      //
      wvarStep = 50;
      mvarWDB_POLIZSEC = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC )  );
      }
      //
      wvarStep = 60;
      mvarWDB_CERTISEC = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CERTISEC) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTISEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CERTISEC )  );
      }
      //
      wvarStep = 70;
      mvarWDB_P_EMISIANN = Strings.right( Strings.fill( 4, "0" ) + DateTime.year( DateTime.now() ), 4 );
      mvarWDB_P_EMISIMES = Strings.right( Strings.fill( 2, "0" ) + DateTime.month( DateTime.now() ), 2 );
      mvarWDB_P_EMISIDIA = Strings.right( Strings.fill( 2, "0" ) + DateTime.day( DateTime.now() ), 2 );
      //
      mvarWDB_EFECTANN = Strings.right( Strings.fill( 4, "0" ) + DateTime.year( DateTime.now() ), 4 );
      mvarWDB_EFECTMES = Strings.right( Strings.fill( 2, "0" ) + DateTime.month( DateTime.now() ), 2 );
      mvarWDB_EFECTDIA = Strings.right( Strings.fill( 2, "0" ) + DateTime.day( DateTime.now() ), 2 );
      //
      wvarStep = 80;
      mvarWDB_NUMEDOCU = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NumeDocu) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NUMEDOCU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NumeDocu )  );
      }
      //
      wvarStep = 90;
      mvarWDB_TIPODOCU = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoDocu) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPODOCU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoDocu )  );
      }
      //
      wvarStep = 100;
      mvarWDB_CLIENAP1 = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienAp1) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENAP1 = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienAp1 )  ) );
      }
      //
      wvarStep = 110;
      mvarWDB_CLIENAP2 = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienAp2) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENAP2 = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienAp2 )  ) );
      }
      //
      wvarStep = 120;
      mvarWDB_CLIENNOM = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienNom) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENNOM = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienNom )  ) );
      }
      //
      wvarStep = 130;
      mvarWDB_NACIMANN = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimAnn) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimAnn )  );
      }
      //
      wvarStep = 140;
      mvarWDB_NACIMMES = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimMes) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimMes )  );
      }
      //
      wvarStep = 150;
      mvarWDB_NACIMDIA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimDia) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMDIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimDia )  );
      }
      //
      wvarStep = 160;
      mvarWDB_CLIENSEX = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENSEX) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENSEX = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENSEX )  ) );
      }
      //
      wvarStep = 170;
      mvarWDB_CLIENEST = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENEST = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENEST )  ) );
      }

      //AM
      wvarStep = 171;
      mvarEmpresaCodigo = "0";
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_EmpresaCodigo )  == (org.w3c.dom.Node) null) )
      {
        mvarEmpresaCodigo = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EmpresaCodigo )  );
      }

      wvarStep = 172;
      mvarSucursalCodigo = "";
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_SucursalCodigo )  == (org.w3c.dom.Node) null) )
      {
        mvarSucursalCodigo = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SucursalCodigo )  );
      }

      wvarStep = 173;
      mvarLegajoVend = "";
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_LegajoVend )  == (org.w3c.dom.Node) null) )
      {
        mvarLegajoVend = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_LegajoVend )  );
      }

      wvarStep = 174;
      mvarLegajoCia = "";
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_LegajoCia )  == (org.w3c.dom.Node) null) )
      {
        mvarLegajoCia = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_LegajoCia )  );
      }
      // FIN AM
      //
      wvarStep = 180;
      mvarWDB_EMAIL = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EMAIL) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_EMAIL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_EMAIL )  );
      }
      //
      wvarStep = 190;
      mvarWDB_DOMICDOM = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICDOM) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDOM = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICDOM )  ) );
      }
      //
      wvarStep = 200;
      mvarWDB_DOMICDNU = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDnu) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDNU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDnu )  );
      }
      //
      wvarStep = 210;
      mvarWDB_DOMICPIS = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICPIS) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPIS = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICPIS )  ) );
      }
      //
      wvarStep = 220;
      mvarWDB_DOMICPTA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPta) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPTA = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPta )  ) );
      }
      //
      wvarStep = 230;
      mvarWDB_DOMICPOB = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICPOB) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPOB = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICPOB )  ) );
      }
      //
      wvarStep = 240;
      mvarWDB_DOMICCPO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICCPO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCod )  );
      }
      //
      wvarStep = 250;
      mvarWDB_PROVICOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi )  );
      }
      //
      wvarStep = 260;
      //10/2010 se agrega area
      mvarTELCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TELCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarTELCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TELCOD )  );
      }

      mvarWDB_TELNRO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TELNRO) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TELNRO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TELNRO )  );
      }
      //
      wvarStep = 270;
      mvarWDB_BARRIOCOUNT = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_BARRIOCOUNT) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_BARRIOCOUNT = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_BARRIOCOUNT )  ) );
      }
      //
      wvarStep = 280;
      mvarWDB_DOMICDOM_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDomCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDOM_CO = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDomCo )  ) );
      }
      //
      wvarStep = 290;
      mvarWDB_DOMICDNU_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDNUCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDNU_CO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDNUCo )  );
      }
      //
      wvarStep = 300;
      mvarWDB_DOMICPIS_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPisCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPIS_CO = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPisCo )  ) );
      }
      //
      wvarStep = 310;
      mvarWDB_DOMICPTA_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPtaCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPTA_CO = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPtaCo )  ) );
      }
      //
      wvarStep = 320;
      mvarWDB_DOMICPOB_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICPOBCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPOB_CO = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICPOBCo )  ) );
      }
      //
      wvarStep = 330;
      mvarWDB_DOMICCPO_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCodCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICCPO_CO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCodCo )  );
      }
      //
      wvarStep = 340;
      mvarWDB_PROVICOD_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ProviCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICOD_CO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ProviCo )  );
      }
      //
      wvarStep = 350;
      //10/2010 se agrega area
      mvarTELCOD_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TELCOD_CO) )  == (org.w3c.dom.Node) null) )
      {
        mvarTELCOD_CO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TELCOD_CO )  );
      }

      mvarWDB_TELNRO_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TelNroCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TELNRO_CO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TelNroCo )  );
      }
      //
      wvarStep = 360;
      mvarWDB_COBROTIP = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROTIP = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP )  ) );
      }
      //
      wvarStep = 370;
      mvarWDB_CUENNUME = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CUENNUME = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CUENNUME )  );
      }
      //
      wvarStep = 380;
      mvarWDB_VENCIANN = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VENCIANN) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_VENCIANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_VENCIANN )  );
      }
      //
      wvarStep = 390;
      mvarWDB_VENCIMES = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VENCIMES) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_VENCIMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_VENCIMES )  );
      }
      //
      wvarStep = 400;
      mvarWDB_VENCIDIA = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VENCIDIA) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_VENCIDIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_VENCIDIA )  );
      }
      //
      wvarStep = 410;

      //AM  Verificar esta l�gica
      mvarWDB_BANCOCOD = "0";
      if( Obj.toInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) )  ) ) == 4 )
      {
        mvarWDB_BANCOCOD = "0";
        mvarWDB_SUCURCOD = "0";
      }
      else
      {
        mvarWDB_BANCOCOD = "9000";
        mvarWDB_SUCURCOD = "8888";
      }
      // FIN AM
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_BANCOCOD )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_BANCOCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_BANCOCOD )  );
      }

      wvarStep = 420;
      mvarWDB_P_COBROCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_COBROCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD )  );
      }
      //
      wvarStep = 430;
      mvarWDB_P_TIVIVCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TIPOHOGAR) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_TIVIVCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TIPOHOGAR )  );
      }
      //
      wvarStep = 440;
      mvarWDB_P_USOTIPOS = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_UsoTipos) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_USOTIPOS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_UsoTipos )  );
      }
      //
      wvarStep = 450;
      mvarWDB_P_SWCALDER = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWCALDER) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWCALDER = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWCALDER )  ) );
      }
      //
      wvarStep = 460;
      mvarWDB_P_SWASCENS = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWASCENS) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWASCENS = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWASCENS )  ) );
      }
      //
      wvarStep = 470;
      mvarWDB_P_ALARMTIP = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ALARMTIP) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_ALARMTIP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ALARMTIP )  );
      }
      //
      wvarStep = 480;
      mvarWDB_P_GUARDTIP = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_GUARDTIP) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_GUARDTIP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_GUARDTIP )  );
      }
      //
      wvarStep = 490;
      mvarWDB_P_SWCREJAS = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWCREJAS) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWCREJAS = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWCREJAS )  ) );
      }
      //
      wvarStep = 500;
      mvarWDB_P_SWPBLIND = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWPBLIND) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWPBLIND = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWPBLIND )  ) );
      }
      //
      wvarStep = 510;
      mvarWDB_P_SWDISYUN = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWDISYUN) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWDISYUN = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWDISYUN )  ) );
      }
      //
      wvarStep = 520;
      mvarWDB_P_SWPROPIE = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWPROPIE) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWPROPIE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWPROPIE )  );
      }
      //
      wvarStep = 530;
      mvarWDB_P_PLANNCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PLANNCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_PLANNCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PLANNCOD )  );
      }
      //
      wvarStep = 540;
      mvarWDB_P_ZONA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ZONA) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_ZONA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ZONA )  );
      }
      //
      wvarStep = 550;
      mvarWDB_P_CLIENIVA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CLIENIVA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENIVA )  );
      }
      //
      wvarStep = 560;
      mvarWDB_P_COBERCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBERCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_COBERCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBERCOD )  );
      }
      //
      wvarStep = 570;
      mvarWDB_P_CONTRMOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CONTRMOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CONTRMOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CONTRMOD )  );
      }
      //
      wvarStep = 580;
      mvarWDB_P_CAPITASG = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CAPITASG) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CAPITASG = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CAPITASG )  );
      }
      //
      wvarStep = 590;
      mvarWDB_P_CAMPACOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CampaCod) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CAMPACOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CampaCod )  );
        if( Obj.toInt( mvarWDB_P_CAMPACOD ) < 1 )
        {
          mvarWDB_P_CAMPACOD = "";
        }
      }
      //
      wvarStep = 600;
      mvarWDB_P_TIPO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Tipo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_TIPO = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Tipo )  ) );
      }
      //
      wvarStep = 610;
      mvarWDB_P_MARCA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Marca) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_MARCA = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Marca )  ) );
      }
      //
      wvarStep = 620;
      mvarWDB_P_MODELO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Modelo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_MODELO = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Modelo )  ) );
      }
      //
      wvarStep = 621;
      mvarNROSOLI = "0";
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_NROSOLI )  == (org.w3c.dom.Node) null) )
      {
        mvarNROSOLI = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_NROSOLI )  ) );
      }
      //
      wvarStep = 630;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.mcteDB);
      //
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProcAlta );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 640;
      //
      
      
      //
      wvarStep = 650;
      wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      
      //
      wvarStep = 660;
      wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      
      //
      // Solo para el HSBC
      mvarWDB_CERTIANN = "0";
      if( Obj.toInt( mvarEmpresaCodigo ) == 150 )
      {
        mvarWDB_CODINST = "150";
        mvarWDB_CERTIANN = mvarSucursalCodigo;
      }

      wvarStep = 670;
      wobjDBParm = new Parameter( "@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      
      //
      wvarStep = 680;
      
      
      //
      wvarStep = 690;
      
      
      //
      wvarStep = 700;
      
      
      //
      wvarStep = 710;
      
      
      //
      wvarStep = 720;
      
      
      //
      wvarStep = 730;
      wobjDBParm = new Parameter( "@TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPODOCU.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPODOCU)) );
      
      //
      wvarStep = 740;
      
      
      //
      wvarStep = 750;
      
      
      //
      wvarStep = 760;
      
      
      //
      wvarStep = 770;
      wobjDBParm = new Parameter( "@NACIMANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NACIMANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NACIMANN)) );
      
      //
      wvarStep = 780;
      wobjDBParm = new Parameter( "@NACIMMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NACIMMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NACIMMES)) );
      
      //
      wvarStep = 790;
      wobjDBParm = new Parameter( "@NACIMDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NACIMDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NACIMDIA)) );
      
      //
      wvarStep = 800;
      
      
      //
      wvarStep = 810;
      
      
      //
      wvarStep = 820;
      
      
      //
      wvarStep = 830;
      
      
      //
      wvarStep = 840;
      
      
      //
      wvarStep = 850;
      wobjDBParm = new Parameter( "@EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTANN)) );
      
      //
      wvarStep = 860;
      wobjDBParm = new Parameter( "@EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTMES)) );
      
      //
      wvarStep = 870;
      wobjDBParm = new Parameter( "@EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTDIA)) );
      
      //
      wvarStep = 880;
      // 10/2010 no es mas F persona f�sica
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTIP", adChar, adParamInput, 2, "F")
      
      
      //
      wvarStep = 890;
      
      
      //
      wvarStep = 900;
      
      
      //
      wvarStep = 910;
      
      
      //
      wvarStep = 920;
      
      
      //
      wvarStep = 930;
      
      
      //
      wvarStep = 940;
      
      
      //
      wvarStep = 950;
      
      
      //
      wvarStep = 960;
      
      
      //
      wvarStep = 970;
      
      
      //
      wvarStep = 980;
      
      
      //
      wvarStep = 990;
      
      
      //
      wvarStep = 1000;
      
      
      //
      wvarStep = 1010;
      
      
      //
      wvarStep = 1020;
      
      
      //
      wvarStep = 1030;
      
      
      //
      wvarStep = 1040;
      
      
      //
      wvarStep = 1050;
      
      
      //
      wvarStep = 1060;
      
      
      //
      wvarStep = 1070;
      
      
      //
      wvarStep = 1080;
      
      
      //
      wvarStep = 1090;
      
      
      //
      wvarStep = 1100;
      
      
      //
      wvarStep = 1110;
      wobjDBParm = new Parameter( "@DOMICCPO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_DOMICCPO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_DOMICCPO)) );
      
      //
      wvarStep = 1120;
      wobjDBParm = new Parameter( "@PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICOD)) );
      
      //
      wvarStep = 1130;
      
      
      //
      wvarStep = 1140;
      
      
      //
      wvarStep = 1150;
      
      
      //
      wvarStep = 1160;
      
      
      //
      wvarStep = 1170;
      
      
      //
      wvarStep = 1180;
      
      
      //
      wvarStep = 1190;
      
      
      //
      wvarStep = 1200;
      
      
      //
      wvarStep = 1210;
      
      
      //
      wvarStep = 1220;
      
      
      //
      wvarStep = 1230;
      wobjDBParm = new Parameter( "@DOMICCPO_CO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_DOMICCPO_CO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_DOMICCPO_CO)) );
      
      //
      wvarStep = 1240;
      wobjDBParm = new Parameter( "@PROVICOD_CO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD_CO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICOD_CO)) );
      
      //
      wvarStep = 1250;
      
      
      //
      wvarStep = 1260;
      
      
      //
      wvarStep = 1270;
      
      
      //
      wvarStep = 1280;
      
      
      //
      wvarStep = 1290;
      
      
      //
      wvarStep = 1300;
      wobjDBParm = new Parameter( "@BANCOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_BANCOCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_BANCOCOD)) );
      
      //
      wvarStep = 1310;
      wobjDBParm = new Parameter( "@SUCURCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_SUCURCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_SUCURCOD)) );
      
      //
      wvarStep = 1320;
      
      
      //
      wvarStep = 1330;
      
      
      //
      wvarStep = 1340;
      
      
      //
      wvarStep = 1350;
      wobjDBParm = new Parameter( "@VENCIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VENCIANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VENCIANN)) );
      
      //
      wvarStep = 1360;
      wobjDBParm = new Parameter( "@VENCIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VENCIMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VENCIMES)) );
      
      //
      wvarStep = 1370;
      wobjDBParm = new Parameter( "@VENCIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VENCIDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VENCIDIA)) );
      
      //
      wvarStep = 1380;
      
      
      //
      wvarStep = 1390;
      wobjDBParm = new Parameter( "@P_EMISIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_EMISIANN)) );
      
      //
      wvarStep = 1400;
      wobjDBParm = new Parameter( "@P_EMISIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_EMISIMES)) );
      
      //
      wvarStep = 1410;
      wobjDBParm = new Parameter( "@P_EMISIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_EMISIDIA)) );
      
      //
      wvarStep = 1420;
      wobjDBParm = new Parameter( "@P_EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTANN)) );
      
      //
      wvarStep = 1430;
      wobjDBParm = new Parameter( "@P_EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTMES)) );
      
      //
      wvarStep = 1440;
      wobjDBParm = new Parameter( "@P_EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTDIA)) );
      
      //
      wvarStep = 1450;
      
      
      //
      wvarStep = 1460;
      
      
      //
      wvarStep = 1470;
      
      
      //
      wvarStep = 1480;
      
      
      //
      wvarStep = 1490;
      wobjDBParm = new Parameter( "@P_TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPODOCU.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPODOCU)) );
      
      //
      wvarStep = 1500;
      
      
      //
      wvarStep = 1510;
      
      
      //
      wvarStep = 1520;
      
      
      //
      wvarStep = 1530;
      wobjDBParm = new Parameter( "@P_COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_COBROCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_COBROCOD)) );
      
      //
      wvarStep = 1540;
      
      
      //
      wvarStep = 1550;
      
      
      //
      wvarStep = 1560;
      wobjDBParm = new Parameter( "@P_TIVIVCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_TIVIVCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_TIVIVCOD)) );
      
      //
      wvarStep = 1570;
      
      
      //
      wvarStep = 1580;
      wobjDBParm = new Parameter( "@P_USOTIPOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_USOTIPOS.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_USOTIPOS)) );
      
      //
      wvarStep = 1590;
      
      
      //
      wvarStep = 1600;
      
      
      //
      wvarStep = 1610;
      wobjDBParm = new Parameter( "@P_ALARMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_ALARMTIP.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_ALARMTIP)) );
      
      //
      wvarStep = 1620;
      wobjDBParm = new Parameter( "@P_GUARDTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_GUARDTIP.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_GUARDTIP)) );
      
      //
      wvarStep = 1630;
      
      
      //
      wvarStep = 1640;
      
      
      //
      wvarStep = 1650;
      
      
      //
      wvarStep = 1660;
      
      
      //
      wvarStep = 1670;
      wobjDBParm = new Parameter( "@P_PLANNCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_PLANNCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_PLANNCOD)) );
      
      //
      wvarStep = 1680;
      
      
      //
      wvarStep = 1690;
      
      
      //
      wvarStep = 1700;
      wobjDBParm = new Parameter( "@P_ZONA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_ZONA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_ZONA)) );
      
      //
      wvarStep = 1710;
      
      
      //
      wvarStep = 1720;
      
      
      //
      wvarStep = 1730;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Campa ) ;
      //
      for( wvarcounter = 1; wvarcounter <= 10; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );

          if( Obj.toInt( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_CampaCod )  ) ) < 1 )
          {
            XmlDomExtended.setText( wobjXMLNode.selectSingleNode( mcteParam_CampaCod ) , "" );
          }

          wobjDBParm = new Parameter( "@P_CAMPACOD_" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_CampaCod )  ) ) );
          
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          wobjDBParm = new Parameter( "@P_CAMPACOD_" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "" ) );
          
        }
      }
      //
      wobjXMLList = (org.w3c.dom.NodeList) null;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Cober ) ;
      //
      wvarStep = 1740;
      for( wvarcounter = 1; wvarcounter <= 30; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
          wobjDBParm = new Parameter( "@P_COBERCOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_COBERCOD )  ) ) );
          
          //
          
          
          //
          wobjDBParm = new Parameter( "@P_CAPITASG" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_CAPITASG )  ) ) );
          
          //
          
          
          //
          wobjDBParm = new Parameter( "@P_CONTRMOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_CONTRMOD )  ) ) );
          
          //
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          
          
          //
          
          
          //
          
          
          //
          
          
          //
          
          
        }
      }
      //AM
      wvarStep = 1750;
      
      
      //
      wvarStep = 1760;
      
      
      //
      //AM
      wvarStep = 1770;
      
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@P_LEGAJO_VEND", adChar, adParamInput, 10, "")
      
      //
      wvarStep = 1780;
      
      

      //11/2010 se agrega variable x actualizaci�n sp
      wvarStep = 1781;
      
      
      //fin
      wvarStep = 1790;
      
      
      //
      wvarStep = 1800;
      
      
      //
      wvarStep = 1810;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Elect ) ;
      //
      wvarStep = 1820;
      for( wvarcounter = 1; wvarcounter <= 10; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
          wobjDBParm = new Parameter( "@P_TIPO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_Tipo )  ) ) ) );
          
          //
          wobjDBParm = new Parameter( "@P_MARCA" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_Marca )  ) ) ) );
          
          //
          wobjDBParm = new Parameter( "@P_MODELO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_Modelo )  ) ) ) );
          
          //
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          wobjDBParm = new Parameter( "@P_TIPO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( "" ) );
          
          //
          wobjDBParm = new Parameter( "@P_MARCA" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( "" ) );
          
          //
          wobjDBParm = new Parameter( "@P_MODELO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( "" ) );
          
          //
        }
      }
      //
      wvarStep = 1830;
      
      
      //
      //Como no vienen asegurados, guardo todos estos datos en blanco
      for( wvarcounter = 1; wvarcounter <= 10; wvarcounter++ )
      {
        
        
        //
        wobjDBParm = new Parameter( "@P_NOMBREAS" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 49, new Variant( "" ) );
        
      }
      //
      //AM
      wvarStep = 1831;
      //LEGAJCIA
      wobjDBParm = new Parameter( "@P_EMPRESA_CODIGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarLegajoCia.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarLegajoCia)) );
      

      wvarStep = 1832;
      //CANAL
      
      
      //
      wvarStep = 1833;
      
      
      //
      wvarStep = 1834;
      //mvarPRODUCTOR)
      
      

      wvarStep = 1835;
      
      
      //
      wvarStep = 1836;
      
      
      //
      wvarStep = 1837;
      
      
      //
      wvarStep = 1838;
      
      
      //
      wvarStep = 1839;
      
      
      //
      wvarStep = 1840;
      
      
      //
      wvarStep = 1841;
      
      
      //
      wvarStep = 1842;
      
      
      
      //
      wvarStep = 1843;
      
      
      //
      wvarStep = 1844;
      
      
      

      //eg fin'
      //
      wvarStep = 1845;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      wobjDBCnn.close();

      //10/2010 se agrega el armado del tag con el impreso en base 64
      //se guarda el certisec otorgado por la sp, en una variable,ya que se ejecutara otra sp para obtener descripciones para el impreso.
      mvarNroCertiSec = wobjDBCmd.getParameters().getParameter("@CERTISEC").getValue().toString();

      //AM150 se saca el armado del impreso para HSBC
      if( ! (Obj.toInt( mvarEmpresaCodigo ) == 150) )
      {

        //Ejectua sp para descripciones
        wvarStep = 1890;
        com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
        wobjDBCnn = (Connection) (Connection)( wobjHSBC_DBCnn.GetDBConnection( General.mcteDB ) );
        wobjDBCmd = new Command();
        wobjDBCmd.setActiveConnection( wobjDBCnn );
        wobjDBCmd.setCommandText( mcteStoreProcImpre );
        wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

        wvarStep = 1901;
        wobjDBParm = new Parameter( "@USOTIPOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_USOTIPOS.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_USOTIPOS)) );
        

        wvarStep = 1902;
        wobjDBParm = new Parameter( "@ALARMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_ALARMTIP.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_ALARMTIP)) );
        

        wvarStep = 1903;
        wobjDBParm = new Parameter( "@GUARDTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_GUARDTIP.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_GUARDTIP)) );
        

        wvarStep = 1910;
        wobjDBParm = new Parameter( "@DOCUMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPODOCU.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPODOCU)) );
        

        wvarStep = 1920;
        
        

        wvarStep = 1930;
        
        

        wvarStep = 1935;
        wobjDBParm = new Parameter( "@CODPROV", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICOD)) );
        

        wvarStep = 1936;
        wobjDBParm = new Parameter( "@CODPROVCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD_CO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICOD_CO)) );
        

        wvarStep = 1940;
        mvarseltest = mcteStoreProcImpre + " ";
        for( wvarCount = 0; wvarCount <= (wobjDBCmd.getParameters().getCount() - 1); wvarCount++ )
        {
          if( (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adChar) || (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adVarChar) )
          {
            mvarseltest = mvarseltest + "'" + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + "',";
          }
          else
          {
            mvarseltest = mvarseltest + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + ",";
          }
        }

        wrstDBResult = wobjDBCmd.execute();
        wrstDBResult.setActiveConnection( (Connection) null );

        wvarStep = 1950;
        if( ! (wrstDBResult.isEOF()) )
        {
          mvarDOCUDES = wrstDBResult.getFields().getField("DOCUDES").getValue().toString();
          mvarCOBRODES = wrstDBResult.getFields().getField("COBRODES").getValue().toString();
          //12/2010 se elimina tabla PROD_LBATCAMP
          //mvarNOMBROKER = wrstDBResult.Fields.Item("NOMBROKER").Value
          mvarPROVIDES = wrstDBResult.getFields().getField("PROVIDES").getValue().toString();
          mvarPROVIDESCO = wrstDBResult.getFields().getField("PROVIDESCO").getValue().toString();
          mvarUSOTIDES = wrstDBResult.getFields().getField("USOTIDES").getValue().toString();
          mvarALARMDES = wrstDBResult.getFields().getField("ALARMDES").getValue().toString();
          mvarGUARDDES = wrstDBResult.getFields().getField("GUARDDES").getValue().toString();
        }
        else
        {
          mvarDOCUDES = "";
          mvarCOBRODES = "";
          mvarNOMBROKER = "";
          mvarPROVIDES = "";
          mvarPROVIDESCO = "";
          mvarUSOTIDES = "";
          mvarALARMDES = "";
          mvarGUARDDES = "";
        }

        wvarStep = 1960;
        if( ! (wrstDBResult == (Recordset) null) )
        {
          
          {
            wrstDBResult.close();
          }
        }


        wvarStep = 1970;
        wrstDBResult = (Recordset) null;

        wobjDBCnn.close();
        //descripci�n del plan
        wvarStep = 1980;
        mvarRequest = "<Request><MSGCOD>0021</MSGCOD><CIAASCOD>0001</CIAASCOD><RAMOPCOD>HOM1</RAMOPCOD>";
        mvarRequest = mvarRequest + "<POLIZANN>" + mvarWDB_POLIZANN + "</POLIZANN>";
        mvarRequest = mvarRequest + "<POLIZSEC>" + mvarWDB_POLIZSEC + "</POLIZSEC>";
        mvarRequest = mvarRequest + "<EFECTANN>" + DateTime.year( DateTime.now() ) + "</EFECTANN>";
        mvarRequest = mvarRequest + "<EFECTMES>" + DateTime.month( DateTime.now() ) + "</EFECTMES>";
        mvarRequest = mvarRequest + "<EFECTDIA>" + DateTime.day( DateTime.now() ) + "</EFECTDIA></Request>";

        wobjClass = new lbawA_ProductorHgMQ.lbaw_GetPolPlanes();
        //error: function 'Execute' was not found.
        //unsup: Call wobjClass.Execute(mvarRequest, mvarResponse, "")
        wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
        mobjXMLDoc = new XmlDomExtended();
        mobjXMLDoc.loadXML( mvarResponse );
        if( mobjXMLDoc.selectNodes( "//OPTION" ) .getLength() > 0 )
        {
          for( int noPlan = 0; noPlan < mobjXMLDoc.selectNodes( "//OPTION" ) .getLength(); noPlan++ )
          {
            oPlan = mobjXMLDoc.selectNodes( "//OPTION" ) .item( noPlan );
            if( mvarWDB_P_PLANNCOD.equals( XmlDomExtended .getText( oPlan.getAttributes().getNamedItem( "value" ) ) ) )
            {
              mvarPLANDES = XmlDomExtended.getText( oPlan );
            }
          }
        }
        else
        {
          mvarPLANDES = "";
        }

        mobjXMLDoc = null;

        //conforma xml para la impresi�n
        wvarStep = 2000;
        mvarDatosImpreso = "<Request>";
        mvarDatosImpreso = mvarDatosImpreso + "<REQUESTID>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) )  ) + "</REQUESTID>";
        mvarDatosImpreso = mvarDatosImpreso + "<CODINST>" + mvarWDB_CODINST + "</CODINST>";
        //12/2010 se elimina tabla PROD_LBATCAMP
        //mvarDatosImpreso = mvarDatosImpreso & "<USUARIO>" & mvarNOMBROKER & "</USUARIO>"
        mvarDatosImpreso = mvarDatosImpreso + "<USUARIO>" + mvarWDB_CODINST + "</USUARIO>";
        mvarDatosImpreso = mvarDatosImpreso + "<COT_NRO>0</COT_NRO>";
        mvarDatosImpreso = mvarDatosImpreso + "<PROCEANN>" + DateTime.year( DateTime.now() ) + "</PROCEANN>";
        mvarDatosImpreso = mvarDatosImpreso + "<PROCEMES>" + DateTime.month( DateTime.now() ) + "</PROCEMES>";
        mvarDatosImpreso = mvarDatosImpreso + "<PROCEDIA>" + DateTime.day( DateTime.now() ) + "</PROCEDIA>";
        mvarDatosImpreso = mvarDatosImpreso + "<POLIZANN>" + mvarWDB_POLIZANN + "</POLIZANN>";
        mvarDatosImpreso = mvarDatosImpreso + "<POLIZSEC>" + mvarWDB_POLIZSEC + "</POLIZSEC>";
        mvarDatosImpreso = mvarDatosImpreso + "<CERTIPOL>" + mvarWDB_CODINST + "</CERTIPOL>";
        mvarDatosImpreso = mvarDatosImpreso + "<CERTIANN>0000</CERTIANN>";
        mvarDatosImpreso = mvarDatosImpreso + "<CERTISEC>" + mvarNroCertiSec + "</CERTISEC>";
        mvarDatosImpreso = mvarDatosImpreso + "<PRECIO>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VALOR" )  ) + "</PRECIO>";
        mvarDatosImpreso = mvarDatosImpreso + "<TIPOHOGAR>" + mvarWDB_P_TIVIVCOD + "</TIPOHOGAR>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICDOM>" + mvarWDB_DOMICDOM + "</DOMICDOM>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICDNU>" + mvarWDB_DOMICDNU + "</DOMICDNU>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICPIS>" + mvarWDB_DOMICPIS + "</DOMICPIS>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICPTA>" + mvarWDB_DOMICPTA + "</DOMICPTA>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICPOB>" + mvarWDB_DOMICPOB + "</DOMICPOB>";
        mvarDatosImpreso = mvarDatosImpreso + "<PROVI>" + mvarWDB_PROVICOD + "</PROVI>";
        mvarDatosImpreso = mvarDatosImpreso + "<DESC_PROVI>" + mvarPROVIDES + "</DESC_PROVI>";
        mvarDatosImpreso = mvarDatosImpreso + "<LOCALIDADCOD>" + mvarWDB_DOMICCPO + "</LOCALIDADCOD>";
        mvarDatosImpreso = mvarDatosImpreso + "<TELCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//TELCOD" )  ) + "</TELCOD>";
        mvarDatosImpreso = mvarDatosImpreso + "<TELNRO>" + mvarWDB_TELNRO + "</TELNRO>";
        //ciclo de coberturas
        mvarSumaCob = 0;
        mvarDatosImpreso = mvarDatosImpreso + "<COBERTURAS>";
        if( wobjXMLRequest.selectNodes( "//COBERTURAS/COBERTURA" ) .getLength() > 0 )
        {
          wobjXMLList = (org.w3c.dom.NodeList) null;
          wobjXMLList = wobjXMLRequest.selectNodes( "//COBERTURAS/COBERTURA" ) ;
          for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
          {
            //Ejectua sp para descripciones
            wvarStep = 20001;
            com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
            wobjDBCnn = (Connection) (Connection)( wobjHSBC_DBCnn.GetDBConnection( General.mcteDB ) );
            wobjDBCmd = new Command();
            wobjDBCmd.setActiveConnection( wobjDBCnn );
            wobjDBCmd.setCommandText( mcteStorePImpreCob );
            wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

            wvarStep = 20002;
            
            

            wvarStep = 20003;
            mvarseltest = mcteStorePImpreCob + " ";
            for( wvarCount = 0; wvarCount <= (wobjDBCmd.getParameters().getCount() - 1); wvarCount++ )
            {
              if( (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adChar) || (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adVarChar) )
              {
                mvarseltest = mvarseltest + "'" + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + "',";
              }
              else
              {
                mvarseltest = mvarseltest + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + ",";
              }
            }

            wrstDBResult = wobjDBCmd.execute();
            wrstDBResult.setActiveConnection( (Connection) null );

            wvarStep = 20004;
            if( ! (wrstDBResult.isEOF()) )
            {
              mvarCOBERDES = wrstDBResult.getFields().getField("COBERDES").getValue().toString();
            }
            else
            {
              mvarCOBERDES = "";
            }

            wvarStep = 20005;
            if( ! (wrstDBResult == (Recordset) null) )
            {
              
              {
                wrstDBResult.close();
              }
            }

            wvarStep = 20006;
            wrstDBResult = (Recordset) null;
            wobjDBCnn.close();

            mvarDatosImpreso = mvarDatosImpreso + "<COBERTURA>";
            mvarDatosImpreso = mvarDatosImpreso + "<COBERCOD>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_COBERCOD )  ) + "</COBERCOD>";
            mvarDatosImpreso = mvarDatosImpreso + "<CONTRMOD>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CONTRMOD )  ) + "</CONTRMOD>";
            mvarDatosImpreso = mvarDatosImpreso + "<COBERDAB>" + mvarCOBERDES + "</COBERDAB>";
            mvarDatosImpreso = mvarDatosImpreso + "<CAPITASG>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CAPITASG )  ) + "</CAPITASG>";
            mvarSumaCob = mvarSumaCob + Obj.toInt( XmlDomExtended .getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CAPITASG )  ) );
            mvarDatosImpreso = mvarDatosImpreso + "</COBERTURA>";
          }
          mvarDatosImpreso = mvarDatosImpreso + "</COBERTURAS>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<COBERTURA>";
          mvarDatosImpreso = mvarDatosImpreso + "<COBERCOD></COBERCOD>";
          mvarDatosImpreso = mvarDatosImpreso + "<CONTRMOD></CONTRMOD>";
          mvarDatosImpreso = mvarDatosImpreso + "<COBERDAB></COBERDAB>";
          mvarDatosImpreso = mvarDatosImpreso + "<CAPITASG></CAPITASG>";
          mvarDatosImpreso = mvarDatosImpreso + "</COBERTURA>";
          mvarDatosImpreso = mvarDatosImpreso + "</COBERTURAS>";
        }

        mvarDatosImpreso = mvarDatosImpreso + "<SUMATOT>" + mvarSumaCob + "</SUMATOT>";

        //fin coberturas
        mvarDatosImpreso = mvarDatosImpreso + "<PLANNCOD>" + mvarWDB_P_PLANNCOD + "</PLANNCOD>";
        mvarDatosImpreso = mvarDatosImpreso + "<PLANNDES>" + mvarPLANDES + "</PLANNDES>";
        mvarDatosImpreso = mvarDatosImpreso + "<COBROFOR>" + mvarCOBRODES + "</COBROFOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<COBROCOD>" + mvarWDB_P_COBROCOD + "</COBROCOD>";
        mvarDatosImpreso = mvarDatosImpreso + "<COBROTIP>" + mvarWDB_COBROTIP + "</COBROTIP>";
        // mvarDatosImpreso = mvarDatosImpreso & "<CUENNUME>" & mvarWDB_CUENNUME & "</CUENNUME>"
        //cbu
        if( Obj.toInt( mvarWDB_P_COBROCOD ) == 5 )
        {
          if( mvarWDB_COBROTIP.equals( "DB" ) )
          {
            mvarDatosImpreso = mvarDatosImpreso + "<CUENNUME>" + mvarWDB_CUENNUME + mvarWDB_VENCIMES + mvarWDB_VENCIANN + "</CUENNUME>";
          }
          else
          {
            mvarDatosImpreso = mvarDatosImpreso + "<CUENNUME>" + mvarWDB_CUENNUME + "</CUENNUME>";
          }
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<CUENNUME>" + mvarWDB_CUENNUME + "</CUENNUME>";
        }

        mvarDatosImpreso = mvarDatosImpreso + "<VENCIANN>" + mvarWDB_VENCIANN + "</VENCIANN>";
        mvarDatosImpreso = mvarDatosImpreso + "<VENCIMES>" + mvarWDB_VENCIMES + "</VENCIMES>";
        mvarDatosImpreso = mvarDatosImpreso + "<VENCIDIA>" + mvarWDB_VENCIDIA + "</VENCIDIA>";

        //ciclo de campa�as
        mvarDatosImpreso = mvarDatosImpreso + "<CAMPANIAS>";
        wvarStep = 2010;
        if( wobjXMLRequest.selectNodes( "//CAMPANIAS/CAMPANIA" ) .getLength() > 0 )
        {
          wobjXMLList = (org.w3c.dom.NodeList) null;
          wobjXMLList = wobjXMLRequest.selectNodes( "//CAMPANIAS/CAMPANIA" ) ;
          for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
          {
            mvarDatosImpreso = mvarDatosImpreso + "<CAMPANIA>";
            mvarDatosImpreso = mvarDatosImpreso + "<CAMPACOD>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), "//CAMPACOD" )  ) + "</CAMPACOD>";
            mvarDatosImpreso = mvarDatosImpreso + "</CAMPANIA>";
          }
          mvarDatosImpreso = mvarDatosImpreso + "</CAMPANIAS>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<CAMPANIA>";
          mvarDatosImpreso = mvarDatosImpreso + "<CAMPACOD></CAMPACOD>";
          mvarDatosImpreso = mvarDatosImpreso + "</CAMPANIA>";
          mvarDatosImpreso = mvarDatosImpreso + "</CAMPANIAS>";
        }
        //fin campa�as
        mvarDatosImpreso = mvarDatosImpreso + "<CLIENIVA>" + mvarWDB_P_CLIENIVA + "</CLIENIVA>";
        mvarDatosImpreso = mvarDatosImpreso + "<CLIENAP1>" + mvarWDB_CLIENAP1 + "</CLIENAP1>";
        mvarDatosImpreso = mvarDatosImpreso + "<CLIENAP2>" + mvarWDB_CLIENAP2 + "</CLIENAP2>";
        mvarDatosImpreso = mvarDatosImpreso + "<CLIENNOM>" + mvarWDB_CLIENNOM + "</CLIENNOM>";
        mvarDatosImpreso = mvarDatosImpreso + "<NACIMANN>" + mvarWDB_NACIMANN + "</NACIMANN>";
        mvarDatosImpreso = mvarDatosImpreso + "<NACIMMES>" + mvarWDB_NACIMMES + "</NACIMMES>";
        mvarDatosImpreso = mvarDatosImpreso + "<NACIMDIA>" + mvarWDB_NACIMDIA + "</NACIMDIA>";
        if( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENSEX) )  ) ).equals( "M" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<SEXO>MASCULINO</SEXO>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<SEXO>FEMENINO</SEXO>";
        }
        if( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) )  ) ).equals( "S" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<ESTADO>SOLTERO/A</ESTADO>";
        }
        else
        {
          if( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) )  ) ).equals( "C" ) )
          {
            mvarDatosImpreso = mvarDatosImpreso + "<ESTADO>CASADO/A</ESTADO>";
          }
          else
          {
            if( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) )  ) ).equals( "E" ) )
            {
              mvarDatosImpreso = mvarDatosImpreso + "<ESTADO>SEPARADO/A</ESTADO>";
            }
            else
            {
              if( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) )  ) ).equals( "V" ) )
              {
                mvarDatosImpreso = mvarDatosImpreso + "<ESTADO>VIUDO/A</ESTADO>";
              }
              else
              {
                if( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) )  ) ).equals( "D" ) )
                {
                  mvarDatosImpreso = mvarDatosImpreso + "<ESTADO>DIVORCIADO/A</ESTADO>";
                }
                else
                {
                  mvarDatosImpreso = mvarDatosImpreso + "<ESTADO></ESTADO>";
                }
              }
            }
          }
        }
        mvarDatosImpreso = mvarDatosImpreso + "<EMAIL>" + mvarWDB_EMAIL + "</EMAIL>";
        mvarDatosImpreso = mvarDatosImpreso + "<VIGENANN>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VIGENANN" )  ) + "</VIGENANN>";
        mvarDatosImpreso = mvarDatosImpreso + "<VIGENMES>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VIGENMES" )  ) + "</VIGENMES>";
        mvarDatosImpreso = mvarDatosImpreso + "<VIGENDIA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//VIGENDIA" )  ) + "</VIGENDIA>";
        mvarDatosImpreso = mvarDatosImpreso + "<TIPODOCU>" + mvarDOCUDES + "</TIPODOCU>";
        mvarDatosImpreso = mvarDatosImpreso + "<NUMEDOCU>" + mvarWDB_NUMEDOCU + "</NUMEDOCU>";
        mvarDatosImpreso = mvarDatosImpreso + "<USOTIPOS>" + mvarWDB_P_USOTIPOS + "</USOTIPOS>";
        mvarDatosImpreso = mvarDatosImpreso + "<USOTIPODESC>" + mvarUSOTIDES + "</USOTIPODESC>";
        if( Strings.toUpperCase( mvarWDB_P_SWCALDER ).equals( "N" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<CALDERA>NO</CALDERA>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<CALDERA>SI</CALDERA>";
        }
        if( Strings.toUpperCase( mvarWDB_P_SWASCENS ).equals( "N" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<ASCENSOR>NO</ASCENSOR>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<ASCENSOR>SI</ASCENSOR>";
        }
        mvarDatosImpreso = mvarDatosImpreso + "<ALARMTIP>" + mvarWDB_P_ALARMTIP + "</ALARMTIP>";
        mvarDatosImpreso = mvarDatosImpreso + "<ALARMTIPDES>" + mvarALARMDES + "</ALARMTIPDES>";
        mvarDatosImpreso = mvarDatosImpreso + "<GUARDTIP>" + mvarWDB_P_GUARDTIP + "</GUARDTIP>";
        mvarDatosImpreso = mvarDatosImpreso + "<GUARDDES>" + mvarGUARDDES + "</GUARDDES>";
        if( Strings.toUpperCase( mvarWDB_P_SWCREJAS ).equals( "N" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<REJAS>NO</REJAS>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<REJAS>SI</REJAS>";
        }
        if( Strings.toUpperCase( mvarWDB_P_SWPBLIND ).equals( "N" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<PUERTABLIND>NO</PUERTABLIND>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<PUERTABLIND>SI</PUERTABLIND>";
        }
        if( Strings.toUpperCase( mvarWDB_P_SWDISYUN ).equals( "N" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<DISYUNTOR>NO</DISYUNTOR>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<DISYUNTOR>SI</DISYUNTOR>";
        }
        mvarDatosImpreso = mvarDatosImpreso + "<BARRIOCOUNT>" + mvarWDB_BARRIOCOUNT + "</BARRIOCOUNT>";
        //ciclo electrodomesticos
        mvarDatosImpreso = mvarDatosImpreso + "<ELECTRODOMESTICOS>";
        wvarStep = 2020;
        if( wobjXMLRequest.selectNodes( "//ELECTRODOMESTICOS/ELECTRODOMESTICO" ) .getLength() > 0 )
        {
          wobjXMLList = (org.w3c.dom.NodeList) null;
          wobjXMLList = wobjXMLRequest.selectNodes( "//ELECTRODOMESTICOS/ELECTRODOMESTICO" ) ;
          for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
          {
            mvarDatosImpreso = mvarDatosImpreso + "<ELECTRODOMESTICO>";
            mvarDatosImpreso = mvarDatosImpreso + "<TIPO>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), "TIPO" )  ) + "</TIPO>";
            mvarDatosImpreso = mvarDatosImpreso + "<MARCA>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), "MARCA" )  ) + "</MARCA>";
            mvarDatosImpreso = mvarDatosImpreso + "<MODELO>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), "MODELO" )  ) + "</MODELO>";
            mvarDatosImpreso = mvarDatosImpreso + "</ELECTRODOMESTICO>";
          }
          mvarDatosImpreso = mvarDatosImpreso + "</ELECTRODOMESTICOS>";
        }
        else
        {
          mvarDatosImpreso = mvarDatosImpreso + "<ELECTRODOMESTICO>";
          mvarDatosImpreso = mvarDatosImpreso + "<TIPO></TIPO>";
          mvarDatosImpreso = mvarDatosImpreso + "<MARCA></MARCA>";
          mvarDatosImpreso = mvarDatosImpreso + "<MODELO></MODELO>";
          mvarDatosImpreso = mvarDatosImpreso + "</ELECTRODOMESTICO>";
          mvarDatosImpreso = mvarDatosImpreso + "</ELECTRODOMESTICOS>";
        }
        //fin electrodomesticos
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICDOMCOR>" + mvarWDB_DOMICDOM_CO + "</DOMICDOMCOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICDNUCOR>" + mvarWDB_DOMICDNU_CO + "</DOMICDNUCOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICPISCOR>" + mvarWDB_DOMICPIS_CO + "</DOMICPISCOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICPTACOR>" + mvarWDB_DOMICPTA_CO + "</DOMICPTACOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<DOMICPOBCOR>" + mvarWDB_DOMICPOB_CO + "</DOMICPOBCOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<PROVICOR>" + mvarWDB_PROVICOD_CO + "</PROVICOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<DESC_PROVICOR>" + mvarPROVIDESCO + "</DESC_PROVICOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<LOCALIDADCODCOR>" + mvarWDB_DOMICCPO_CO + "</LOCALIDADCODCOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<TELCODCOR>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//TELCOD" )  ) + "</TELCODCOR>";
        mvarDatosImpreso = mvarDatosImpreso + "<TELNROCOR>" + mvarWDB_TELNRO_CO + "</TELNROCOR>";
        mvarDatosImpreso = mvarDatosImpreso + "</Request>";
        //fin armado xml para el llamado a la impresion
        //Se generan los pdf en base 64, son 4 pdf, corresponden a 4 nodos a enviar
        //BRO_Solicitud_HOM1 va siempre
        //BRO_CertificadoProvisorioHOM1 va siempre
        //BRO_AutorizacionDebitoTarjHOM1 solo cuando es tarjeta
        //BRO_BRO_AutorizacionDebitoHOM1 solo cuando es cbu
        mvarImpreso64 = "";

        wvarStep = 2030;
        mvarRequestImp = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>" + mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_Solicitud_HOM1</reportId>" + "</Request>";

        wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
        wobjClass.Execute( mvarRequestImp, mvarResponseImp, "" );
        wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
        mobjXMLDoc = new XmlDomExtended();
        mobjXMLDoc.loadXML( mvarResponseImp );
        //Verifica que haya respuesta de MQ
        if( XmlDomExtended .getText( mobjXMLDoc.selectSingleNode( "//Estado" ) .getAttributes().getNamedItem( "resultado" ) ).equals( "true" ) )
        {
          //Verifica que exista el PDF si no error Middle
          if( ! (mobjXMLDoc.selectSingleNode( "//generatePdfReportReturn" )  == (org.w3c.dom.Node) null) )
          {
            mvarNodoImpreso = XmlDomExtended.getText( null /*unsup mobjXMLDoc.getDocument().getDocumentElement().selectSingleNode( "//generatePdfReportReturn" ) */ );
          }
          else
          {
            //error de mdw
            mvarNodoImpreso = "No se pudo obtener el impreso MDW";
          }
        }
        else
        {
          mvarNodoImpreso = "No se pudo obtener el impreso MQ";
        }

        mobjXMLDoc = null;
        mvarImpreso64 = mvarImpreso64 + "<IMPSOLI>" + mvarNodoImpreso + "</IMPSOLI>";
        wvarStep = 2040;
        mvarRequestImp = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>" + mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_CertificadoProvisorioHOM1</reportId>" + "</Request>";

        wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
        wobjClass.Execute( mvarRequestImp, mvarResponseImp, "" );
        wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
        mobjXMLDoc = new XmlDomExtended();
        mobjXMLDoc.loadXML( mvarResponseImp );
        //Verifica que haya respuesta de MQ
        if( XmlDomExtended .getText( mobjXMLDoc.selectSingleNode( "//Estado" ) .getAttributes().getNamedItem( "resultado" ) ).equals( "true" ) )
        {
          //Verifica que exista el PDF si no error Middle
          if( ! (mobjXMLDoc.selectSingleNode( "//generatePdfReportReturn" )  == (org.w3c.dom.Node) null) )
          {
            mvarNodoImpreso = XmlDomExtended.getText( null /*unsup mobjXMLDoc.getDocument().getDocumentElement().selectSingleNode( "//generatePdfReportReturn" ) */ );
          }
          else
          {
            //error de mdw
            mvarNodoImpreso = "No se pudo obtener el impreso MDW";
          }
        }
        else
        {
          mvarNodoImpreso = "No se pudo obtener el impreso MQ";
        }

        mobjXMLDoc = null;
        mvarImpreso64 = mvarImpreso64 + "<IMPCERTI>" + mvarNodoImpreso + "</IMPCERTI>";
        wvarStep = 2050;
        if( Obj.toInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) )  ) ) == 4 )
        {
          mvarRequestImp = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>" + mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_AutorizacionDebitoTarjHOM1</reportId>" + "</Request>";
        }
        else
        {
          mvarRequestImp = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>" + mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_AutorizacionDebitoHOM1</reportId>" + "</Request>";
        }
        wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
        wobjClass.Execute( mvarRequestImp, mvarResponseImp, "" );
        wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;
        mobjXMLDoc = new XmlDomExtended();
        mobjXMLDoc.loadXML( mvarResponseImp );
        //Verifica que haya respuesta de MQ
        if( XmlDomExtended .getText( mobjXMLDoc.selectSingleNode( "//Estado" ) .getAttributes().getNamedItem( "resultado" ) ).equals( "true" ) )
        {
          //Verifica que exista el PDF si no error Middle
          if( ! (mobjXMLDoc.selectSingleNode( "//generatePdfReportReturn" )  == (org.w3c.dom.Node) null) )
          {
            mvarNodoImpreso = XmlDomExtended.getText( null /*unsup mobjXMLDoc.getDocument().getDocumentElement().selectSingleNode( "//generatePdfReportReturn" ) */ );
          }
          else
          {
            //error de mdw
            mvarNodoImpreso = "No se pudo obtener el impreso MDW";
          }
        }
        else
        {
          mvarNodoImpreso = "No se pudo obtener el impreso MQ";
        }

        mobjXMLDoc = null;
        mvarImpreso64 = mvarImpreso64 + "<IMPAUTORI>" + mvarNodoImpreso + "</IMPAUTORI>";

        // Libero los objetos
        wobjClass = (LBAA_MWGenerico.lbaw_MQMW) null;

      }
      else
      {

        mvarImpreso64 = "";
      }
      //fin impreso, Se agrega en pvarRes el nodo <PDF64>
      pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + "<Response>" + "<REQUESTID>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) )  ) + "</REQUESTID>" + "<CERTISEC>" + mvarNroCertiSec + "</CERTISEC>" + "<PDF64>" + mvarImpreso64 + "</PDF64>" + "</Response>" + "</LBA_WS>" );
      fncPutSolicitud = true;

      fin: 
      //libero los objectos
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = (HSBC.DBConnection) null;
      wrstRes = (Recordset) null;
      wobjXMLRequest = null;

      return fncPutSolicitud;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncPutSolicitud = false;
        //mobjCOM_Context.SetComplete
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutSolicitud;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
