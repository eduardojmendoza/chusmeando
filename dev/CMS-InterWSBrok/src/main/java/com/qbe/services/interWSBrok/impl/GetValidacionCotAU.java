package com.qbe.services.interWSBrok.impl;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringEscapeUtils;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.ofVirtualLBA.impl.lbaw_GetTipIngBrut;
import com.qbe.services.ofVirtualLBA.impl.lbaw_GetTiposIVA;
import com.qbe.services.ofVirtualLBA.impl.lbaw_GetZonaIngBrut;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;

/**
 * 
 * Componente interno, no se publica como WebService a los brokers
 * 
 * A diferencia de GetValidacionSolAU y GetCotizacionAU retorna el resultado con formato //Response/Estado/@resultado
 * en vez de <LBA_WS res_code= res_msg= >
 * 
 * PERO en resultado puede poner true, false ***o un código de error*** !!!!!
 * 
 * @author ramiro
 *
 */
public class GetValidacionCotAU extends BaseOSBClient implements VBObjectClass
{
  protected static Logger logger = Logger.getLogger( GetValidacionCotAU.class.getName());

  static final String mcteClassName = "LBA_InterWSBrok.GetValidacionCotAU";
  static final String mcteStoreProc = "SPSNCV_BRO_COTIZACION_SCO";
  static final String mcteArchivoAUSCOT_XML = "LBA_VALIDACION_COT_AU.xml";
  
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_PRODUCTOR = "AGECOD";
  static final String mcteParam_CampaCod = "CAMPACOD";
  static final String mcteParam_KMsrngCod = "KMSRNGCOD";
  static final String mcteParam_Provi = "PROVI";
  static final String mcteParam_LocalidadCod = "LOCALIDADCOD";
  static final String mcteParam_COBROCOD = "COBROCOD";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_SUMAASEG = "SUMAASEG";
  static final String mcteParam_ModeAutCod = "MODEAUTCOD";
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_NacimAnn = "NACIMANN";
  static final String mcteParam_NacimMes = "NACIMMES";
  static final String mcteParam_NacimDia = "NACIMDIA";
  static final String mcteParam_Sexo = "SEXO";
  static final String mcteParam_Estado = "ESTADO";
  static final String mcteParam_EfectAnn = "EFECTANN";
  static final String mcteParam_SiGarage = "SIGARAGE";
  static final String mcteParam_Siniestros = "SINIESTROS";
  static final String mcteParam_Gas = "GAS";
  static final String mcteParam_ClubLBA = "CLUBLBA";

  static final String mcteParam_Dt80 = "DESTRUCCION_80";
  static final String mcteParam_Luneta = "LUNETA";
  static final String mcteParam_ClubEco = "CLUBECO";
  static final String mcteParam_Robocont = "ROBOCONT";
  static final String mcteParam_Granizo = "GRANIZO";
  static final String mcteParam_Escero = "ESCERO";

  static final String mcteParam_CLIENTIP = "CLIENTIP";

  static final String mcteParam_IBB = "IBB";
  static final String mcteParam_CLIENIVA = "IVA";
  /**
   *  DATOS DE LOS HIJOS
   */
  static final String mcteNodos_Hijos = "//Request/HIJOS/HIJO";
  static final String mcteParam_NacimHijo = "NACIMHIJO";
  static final String mcteParam_SexoHijo = "SEXOHIJO";
  static final String mcteParam_EstadoHijo = "ESTADOHIJO";
  static final String mcteParam_EdadHijo = "EDADHIJO";
  /**
   *  DATOS DE LOS ACCESORIOS
   */
  static final String mcteNodos_Accesorios = "//Request/ACCESORIOS/ACCESORIO";
  static final String mcteParam_PrecioAcc = "PRECIOACC";
  static final String mcteParam_DescripcionAcc = "DESCRIPCIONACC";
  static final String mcteParam_CodigoAcc = "CODIGOACC";


  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;

    int wvarStep = 0;
    StringHolder wvarCodErr = new StringHolder("0");
    Variant wvarMensaje = new Variant();
    Variant pvarRes = new Variant();

    try {
      pvarRes.set( "" );
      if( ! fncGetAll(pvarRequest, wvarMensaje, wvarCodErr, pvarRes))
      {
        if( wvarCodErr.isNumeric() &&  Integer.valueOf(wvarCodErr.getValue()).intValue() == 0)
        {
          wvarCodErr.set("-1000");
        }
        pvarResponse.set( "<Response><Estado resultado=\"" + StringEscapeUtils.escapeXml(wvarCodErr.getValue()) + "\" mensaje=\"" + StringEscapeUtils.escapeXml(wvarMensaje.toString()) + "\" /></Response>" );
        wvarStep = 20;
        IAction_Execute = 0;
        return IAction_Execute;
      }
      wvarStep = 30;
      //Acá no lo escapeo porque es contenido XML, no el valor de un atributo
      pvarResponse.set( "<Response><Estado resultado=\"true\" mensaje=\"\"/>"  + pvarRes + "</Response>" );
      IAction_Execute = 0;
      return IAction_Execute;
    }
    catch( Exception _e_ )
    {
    	String errorMsg = "Error inesperado [" + _e_.getMessage() + "] - TS-" + System.currentTimeMillis() + " CI - " + pvarContextInfo;
		logger.log(Level.SEVERE, this.getClass().getName() + "-" + errorMsg, _e_);
    	errorMsg = StringEscapeUtils.escapeXml(errorMsg);
        pvarResponse.set( "<Response><Estado resultado=\"false\" mensaje=\"" + errorMsg	+ "\" /></Response>" );
        IAction_Execute = 1;
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( String pvarRequest, Variant wvarMensaje, StringHolder wvarCodErr, Variant pvarRes ) throws IOException, XmlDomExtendedException, SQLException
  {
    boolean fncGetAll = false;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLCheck = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNodeErrorVacios = null;
    java.sql.ResultSet cursor;
    int wvarContChild = 0;
    int wvarContNode = 0;
    String wvarErrorCodigo = "";
    int wvarStep = 0;
    String wvarCLIENTIP = "";
    String wvarCABA = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarIVAok = "";
    org.w3c.dom.Node oTipoiva = null;
    XmlDomExtended mobjXMLDoc = null;
    String mvarREQ_IIBB = "";
    String mvarIBBok = "";
    org.w3c.dom.Node oTipoibb = null;

    wvarStep = 0;
    wvarCodErr.set("0");

      //Chequeo que los datos esten OK
      //Chequeo que no venga nada en blanco
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 20;
      wobjXMLCheck = new XmlDomExtended();
      wobjXMLCheck.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteArchivoAUSCOT_XML));

      wvarStep = 30;
      //Chequeo nodo padre
      wobjXMLNodeErrorVacios = wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/VACIOS/CAMPOS" ) ;
      //Tomo codigo de error de vacios
      wvarStep = 40;
      wvarErrorCodigo = XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/VACIOS/N_ERROR" )  );
      for( wvarContChild = 0; wvarContChild <= (wobjXMLNodeErrorVacios.getChildNodes().getLength() - 1); wvarContChild++ )
      {
        if( ! (wobjXMLRequest.selectSingleNode( ("//" + wobjXMLNodeErrorVacios.getChildNodes().item( wvarContChild ).getNodeName()) )  == (org.w3c.dom.Node) null) )
        {
          if( Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + wobjXMLNodeErrorVacios.getChildNodes().item( wvarContChild ).getNodeName()) )  ) ).equals( "" ) )
          {
            //error
            wvarStep = 50;
            wvarCodErr.set("-1");
            wvarMensaje.set( XmlDomExtended.getText( wobjXMLNodeErrorVacios.getChildNodes().item( wvarContChild ) ) );
            pvarRes.set( Utils.createResponseXmlFalseWithMessage(General.mcteErrorInesperadoDescr) );
            return fncGetAll;
          }
        }
        else
        {
          //error
          wvarStep = 60;
          wvarCodErr.set("-1");
          wvarMensaje.set( XmlDomExtended.getText( wobjXMLNodeErrorVacios.getChildNodes().item( wvarContChild ) ) );
          pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
          return fncGetAll;
        }
      }

      //Chequeo Tipo y Longitud de Datos para el XML Padre
      wvarStep = 70;

      for( wvarContChild = 0; wvarContChild <= (wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().getLength() - 1); wvarContChild++ )
      {
        if( wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectSingleNode( "//Request" ).getChildNodes().item( wvarContChild ).getNodeName()) != null)
        {
          //Chequeo la longitud y si tiene Largo Fijo
          if(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectSingleNode( "//Request" ).getChildNodes().item(wvarContChild).getNodeName() + "/LONG_VARIABLE") ).equals( "N" ) )
          {
            if(!(Strings.len(XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ) ) ) == Obj.toInt( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" +wobjXMLRequest.selectSingleNode( "//Request" ).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") )) )) )
            {
                wvarCodErr.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectSingleNode( "//Request" ).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" )) );
                wvarMensaje.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectSingleNode( "//Request" ).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) ));
                pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
              return fncGetAll;
            }
          }
          else
          {
            if( Strings.len( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ) ) ) > Obj.toInt( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" +wobjXMLRequest.selectSingleNode( "//Request" ).getChildNodes().item( wvarContChild ).getNodeName() + "/LONGITUD") )) ) )
            {
                wvarCodErr.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectSingleNode( "//Request" ).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR" )));
                wvarMensaje.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectSingleNode( "//Request" ).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) ) );
              pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
              return fncGetAll;
            }
          }

          //Chequeo el Tipo
          if( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectSingleNode( "//Request").getChildNodes().item( wvarContChild ).getNodeName() + "/TIPO") ) ).equals( "NUMERICO" ) )
          {
            if( ! (new Variant( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ) ) ).isNumeric()) )
            {
                wvarCodErr.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectSingleNode( "//Request" ).getChildNodes().item( wvarContChild ).getNodeName() + "/N_ERROR") ));
                wvarMensaje.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectSingleNode( "//Request" ).getChildNodes().item( wvarContChild ).getNodeName() + "/TEXTOERROR" ) ));
              pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
              return fncGetAll;
            }
            else
            {
              //Chequeo que el dato numerico no sea 0
              //Excluye los nodos COT_NRO, SINIESTROS
              if( (Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ) ) ) == 0) && ! (((wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ).getNodeName().equals( "COT_NRO" )) || (wobjXMLRequest.selectSingleNode( "//Request" ) .getChildNodes().item( wvarContChild ).getNodeName().equals( "SINIESTROS" )))) )
              {
                  wvarCodErr.set("-199");
                wvarMensaje.set( "El valor del dato numerico no puede estar en 0" );
                pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
                return fncGetAll;
              }
            }
          }
        }
      }

      //Chequeo Tipo y Longitud de Datos para Coberturas
      for( wvarContChild = 0; wvarContChild <= (wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) .getLength() - 1); wvarContChild++ )
      {
        wvarStep = 80;
        for( wvarContNode = 0; wvarContNode <= (wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) .item( wvarContChild ).getChildNodes().getLength() - 1); wvarContNode++ )
        {
          if( ! (wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Accesorios ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName()) )  == (org.w3c.dom.Node) null) )
          {
            //Chequeo la longitud y si tiene Largo Fijo
            if( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Accesorios ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONG_VARIABLE") )  ).equals( "N" ) )
            {
              if( ! (Strings.len( XmlDomExtended.getText( wobjXMLRequest.selectNodes( mcteNodos_Accesorios ).item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == Obj.toInt( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Accesorios ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") )  ) )) )
              {
                wvarCodErr.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Accesorios ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) ) );
                wvarMensaje.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Accesorios ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) ) );
                pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
                return fncGetAll;
              }
            }
            else
            {
              if( Strings.len( XmlDomExtended.getText( wobjXMLRequest.selectNodes( mcteNodos_Accesorios ).item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) > Obj.toInt( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Accesorios ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") )  ) ) )
              {
                wvarCodErr.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Accesorios ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) ) );
                wvarMensaje.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Accesorios ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) ) );
                pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
                return fncGetAll;
              }
            }

            //Chequeo el Tipo
            if( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Accesorios ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TIPO") ) ).equals( "NUMERICO" ) )
            {
              if( ! (new Variant( XmlDomExtended.getText( wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) .item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ).isNumeric()) )
              {
                wvarCodErr.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Accesorios ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) ) );
                wvarMensaje.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Accesorios ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) ) );
                pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
                return fncGetAll;
              }
            }
          }
        }
      }


      //Chequeo Tipo y Longitud de Datos de Hijos
      wvarStep = 90;
      for( wvarContChild = 0; wvarContChild <= (wobjXMLRequest.selectNodes( mcteNodos_Hijos ) .getLength() - 1); wvarContChild++ )
      {
        for( wvarContNode = 0; wvarContNode <= (wobjXMLRequest.selectNodes( mcteNodos_Hijos ) .item( wvarContChild ).getChildNodes().getLength() - 1); wvarContNode++ )
        {
          if( ! (wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Hijos ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName()) ) == (org.w3c.dom.Node) null) )
          {
            //Chequeo la longitud y si tiene Largo Fijo
            if( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Hijos ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONG_VARIABLE") ) ).equals( "N" ) )
            {
              if( ! (Strings.len( XmlDomExtended.getText( wobjXMLRequest.selectNodes( mcteNodos_Hijos ).item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) == Obj.toInt( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Hijos ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") )  ) )) )
              {
                wvarCodErr.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Hijos ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) ) );
                wvarMensaje.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Hijos ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) ) );
                pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
                return fncGetAll;
              }
            }
            else
            {
              if( Strings.len( XmlDomExtended.getText( wobjXMLRequest.selectNodes( mcteNodos_Hijos ).item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ) > Obj.toInt( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Hijos ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/LONGITUD") )  ) ) )
              {
                wvarCodErr.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Hijos ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) ) );
                wvarMensaje.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Hijos ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) ) );
                pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
                return fncGetAll;
              }
            }

            //Chequeo el Tipo
            if( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( ("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Hijos ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TIPO") ) ).equals( "NUMERICO" ) )
            {
              if( ! (new Variant( XmlDomExtended.getText( wobjXMLRequest.selectNodes( mcteNodos_Hijos ) .item( wvarContChild ).getChildNodes().item( wvarContNode ) ) ).isNumeric()) )
              {
                wvarCodErr.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Hijos ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/N_ERROR" ) ) );
                wvarMensaje.set( XmlDomExtended.getText(wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes( mcteNodos_Hijos ).item( wvarContChild ).getChildNodes().item( wvarContNode ).getNodeName() + "/TEXTOERROR" ) ) );
                pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
                return fncGetAll;
              }
            }
          }
        }
      }


      //Chequeo EDAD
      wvarStep = 100;
      if( ! (Funciones.ValidarFechayRango( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimDia )  ) + "/" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimMes) )  ) + "/" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimAnn) )  ), 17, 86, wvarMensaje )) )
      {
        //error
        wvarCodErr.set("-9");
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;

      }

      //No se permite cotizar con GNC - Adriana 21-12-2012
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Gas) )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Gas) )  ).equals( "S" ) )
        {
          wvarCodErr.set("-1");
          wvarMensaje.set( "No se permite cotizar vehículos con GNC" );
          pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
          return fncGetAll;
        }

      }

      wvarStep = 110;
      if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Sexo + "/VALOR[.='" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Sexo) ) ) + "']") ) .getLength() == 0 )
      {
        //error
        wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Sexo + "/N_ERROR" )  ) );
        wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Sexo + "/TEXTOERROR" )  ) );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }

      wvarStep = 120;
      if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Estado + "/VALOR[.='" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Estado) ) ) + "']") ) .getLength() == 0 )
      {
        //error
        wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Estado + "/N_ERROR" )  ) );
        wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Estado + "/TEXTOERROR" )  ) );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }

      wvarStep = 130;
      if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Siniestros + "/VALOR[.='" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Siniestros) ) ) + "']") ) .getLength() == 0 )
      {
        //error
        wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Siniestros + "/N_ERROR" )  ) );
        wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Siniestros + "/TEXTOERROR" )  ) );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }

      wvarStep = 140;
      if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SiGarage + "/VALOR[.='" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SiGarage) ) ) + "']") ) .getLength() == 0 )
      {
        //error
        wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SiGarage + "/N_ERROR" )  ) );
        wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SiGarage + "/TEXTOERROR" )  ) );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }

      wvarStep = 150;
      if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Gas + "/VALOR[.='" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Gas) ) ) + "']") ) .getLength() == 0 )
      {
        //error
        wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Gas + "/N_ERROR" )  ) );
        wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Gas + "/TEXTOERROR" )  ) );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }

      wvarStep = 160;
      if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_ClubLBA + "/VALOR[.='" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClubLBA) ) ) + "']") ) .getLength() == 0 )
      {
        //error
        wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_ClubLBA + "/N_ERROR" )  ) );
        wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_ClubLBA + "/TEXTOERROR" )  ) );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }
      wvarStep = 161;
      if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Dt80 + "/VALOR[.='" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Dt80) ) ) + "']") ) .getLength() == 0 )
      {
        //error
        wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Dt80 + "/N_ERROR" )  ) );
        wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Dt80 + "/TEXTOERROR" )  ) );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }
      wvarStep = 162;
      if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Luneta + "/VALOR[.='" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Luneta) ) ) + "']") ) .getLength() == 0 )
      {
        //error
        wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Luneta + "/N_ERROR" )  ) );
        wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Luneta + "/TEXTOERROR" )  ) );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }
      wvarStep = 163;
      if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_ClubEco + "/VALOR[.='" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClubEco) ) ) + "']") ) .getLength() == 0 )
      {
        //error
        wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_ClubEco + "/N_ERROR" )  ) );
        wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_ClubEco + "/TEXTOERROR" )  ) );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }
      wvarStep = 164;
      if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Robocont + "/VALOR[.='" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Robocont) ) ) + "']") ) .getLength() == 0 )
      {
        //error
        wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Robocont + "/N_ERROR" )  ) );
        wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Robocont + "/TEXTOERROR" )  ) );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }
      wvarStep = 165;
      if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Granizo + "/VALOR[.='" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Granizo) ) ) + "']") ) .getLength() == 0 )
      {
        //error
        wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Granizo + "/N_ERROR" )  ) );
        wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Granizo + "/TEXTOERROR" )  ) );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }
      wvarStep = 166;
      if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Escero + "/VALOR[.='" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Escero) ) ) + "']") ) .getLength() == 0 )
      {
        //error
        wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Escero + "/N_ERROR" )  ) );
        wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_Escero + "/TEXTOERROR" )  ) );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }
      wvarStep = 170;
      //Chequeo los hijos
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Hijos ) ;
      for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
      {
        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_SexoHijo )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set("-1");
          wvarMensaje.set( "Campo de Sexo de Hijo en blanco" );
          pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
          return fncGetAll;
        }
        else
        {
          if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo + "/VALOR[.='" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item( wvarContNode ), mcteParam_SexoHijo ) ) + "']") ) .getLength() == 0 )
          {
            //error
            wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo + "/N_ERROR" )  ) );
            wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo + "/TEXTOERROR" )  ) );
            pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
            return fncGetAll;
          }
        }

        wvarStep = 180;
        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_NacimHijo )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set("-1");
          wvarMensaje.set( "Campo de Fecha de Nacimiento de Hijo en blanco" );
          pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
          return fncGetAll;
        }
        else
        {
          if( ! (Funciones.ValidarFechayRango( Strings.left( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_NacimHijo )  ), 2 ) + "/" + Strings.mid( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_NacimHijo )  ), 3, 2 ) + "/" + Strings.right( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_NacimHijo )  ), 4 ), 17, 29, wvarMensaje )) )
          {
            //error
            wvarCodErr.set("-10");
            pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
            return fncGetAll;
          }
        }

        wvarStep = 190;
        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_EstadoHijo )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( "-1" );
          wvarMensaje.set( "Campo de Estado Civil del Hijo en blanco" );
          pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
          return fncGetAll;
        }
        else
        {
          if( wobjXMLCheck.selectNodes( ("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo + "/VALOR[.='" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item( wvarContNode), mcteParam_EstadoHijo ) ) + "']") ) .getLength() == 0 )
          {
            //error
            wvarCodErr.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo + "/N_ERROR" )  ) );
            wvarMensaje.set( XmlDomExtended.getText( wobjXMLCheck.selectSingleNode( "//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo + "/TEXTOERROR" )  ) );
            pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
            return fncGetAll;
          }
        }
        wvarStep = 200;
        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_EdadHijo )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( "-1" );
          wvarMensaje.set( "Campo de Edad del Hijo en blanco" );
          pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
          return fncGetAll;
        }
        else
        {
          if( Integer.valueOf(XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_EdadHijo )) ) == 0 )
          {
            wvarCodErr.set( "-1" );
            wvarMensaje.set( "Campo de Edad del Hijo con Valor 0" );
            pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
            return fncGetAll;
          }
        }
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;

      //Chequeo los Accesorios
      wvarStep = 210;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) ;
      for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
      {
        wvarStep = 220;
        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_PrecioAcc )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( "-1" );
          wvarMensaje.set( "Campo de Precio de Accesorio en blanco" );
          pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
          return fncGetAll;
        }

        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_CodigoAcc )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( "-1" );
          wvarMensaje.set( "Campo de Codigo de Accesorio en blanco" );
          pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
          return fncGetAll;
        }
        else
        {
          if( Integer.valueOf( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_CodigoAcc )  ) ) == 0 )
          {
            wvarCodErr.set( "-1" );
            wvarMensaje.set( "Campo de Codigo de Accesorio en 0" );
            pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
            return fncGetAll;
          }
        }

        if( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_DescripcionAcc )  == (org.w3c.dom.Node) null )
        {
          wvarCodErr.set( "-1" );
          wvarMensaje.set( "Campo de Descripcion de Accesorio en blanco" );
          pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
          return fncGetAll;
        }
        else
        {
          if( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_DescripcionAcc )  ).equals( "" ) )
          {
            wvarCodErr.set( "-1" );
            wvarMensaje.set( "Campo de Descripcion de Accesorio en blanco" );
            pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
            return fncGetAll;
          }
        }
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;

      wvarStep = 235;

      wvarCABA = Strings.right( "00" + Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi )  ) ), 2 );
      if( wobjXMLRequest.selectNodes( ("//" + mcteParam_CLIENTIP) ) .getLength() == 0 )
      {
        //Para forzar la persona física si no informa el nodo
        wvarCLIENTIP = "00";
      }
      else
      {
        wvarCLIENTIP = Strings.right( "00" + Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENTIP )  ) ), 2 );
      }
      if( wvarCABA.equals( "01" ) )
      {
        if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) )  ).equals( "3" )) )
        {
          if( ! (wvarCLIENTIP.equals( "15" )) )
          {
            wvarCodErr.set( "-1" );
            wvarMensaje.set( "Campo Tipo de Persona no valido" );
            pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
            return fncGetAll;
          }
        }
        else
        {
          if( ! (wvarCLIENTIP.equals( "00" )) )
          {
            wvarCodErr.set( "-1" );
            wvarMensaje.set( "Campo Tipo de Persona no valido" );
            pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
            return fncGetAll;
          }
        }
      }

      wvarStep = 236;

      mvarRequest = "<Request></Request>";
      lbaw_GetTiposIVA wobjGetTiposIVA = new lbaw_GetTiposIVA();
      StringHolder shMvarResponse = new StringHolder(mvarResponse);
      wobjGetTiposIVA.IAction_Execute(mvarRequest, shMvarResponse, "");
      mvarResponse = shMvarResponse.getValue();
      mobjXMLDoc = new XmlDomExtended();
      mobjXMLDoc.loadXML( mvarResponse );

      mvarIVAok = "ER";
      if( mobjXMLDoc.selectNodes( "//OPTION" ) .getLength() > 0 )
      {
        for( int noTipoiva = 0; noTipoiva < mobjXMLDoc.selectNodes( "//OPTION" ) .getLength(); noTipoiva++ )
        {
          oTipoiva = mobjXMLDoc.selectNodes( "//OPTION" ) .item( noTipoiva );
          if( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) )  ).equals( XmlDomExtended.getText( oTipoiva.getAttributes().getNamedItem( "value" ) ) ) )
          {
            mvarIVAok = "OK";
          }
        }
      }
      else
      {
        mvarIVAok = "FC";
      }

      if( mvarIVAok.equals( "ER" ) )
      {
        wvarCodErr.set( "-12" );
        wvarMensaje.set( "Valor de Responsabilidad frente al IVA invalido" );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }

      if( mvarIVAok.equals( "FC" ) )
      {
        wvarCodErr.set( "-12" );
        wvarMensaje.set( "No se pudo recuperar el Valor del IVA desde el equipo central" );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }

      mobjXMLDoc = null;

      // valida ingresos brutos
      mvarRequest = "<Request><PAISSCOD>00</PAISSCOD><V_PROVICOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi )  ) + "</V_PROVICOD><RAMOPCOD>AUS1</RAMOPCOD><CLIENIVA>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) )  ) + "</CLIENIVA></Request>";
      lbaw_GetZonaIngBrut wobjGetZonaIngBrut = new lbaw_GetZonaIngBrut();
      shMvarResponse = new StringHolder(mvarResponse);
      wobjGetZonaIngBrut.IAction_Execute(mvarRequest, shMvarResponse, "");
      mvarResponse = shMvarResponse.getValue();
      mobjXMLDoc = new XmlDomExtended();
      mobjXMLDoc.loadXML( mvarResponse );
      //
      if( XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        mvarREQ_IIBB = "S";
      }
      else
      {
        mvarREQ_IIBB = "N";
      }
      //
      mobjXMLDoc = null;

      if( mvarREQ_IIBB.equals( "S" ) )
      {
        //
        mvarRequest = "<Request><NUMERO>0</NUMERO></Request>";
        lbaw_GetTipIngBrut wobjGetTipIngBrut = new lbaw_GetTipIngBrut();
        shMvarResponse = new StringHolder(mvarResponse);
        wobjGetTipIngBrut.IAction_Execute(mvarRequest, shMvarResponse, "");
        mvarResponse = shMvarResponse.getValue();
        mobjXMLDoc = new XmlDomExtended();
        mobjXMLDoc.loadXML( mvarResponse );

        mvarIBBok = "ER";
        if( mobjXMLDoc.selectNodes( "//OPTION" ) .getLength() > 0 )
        {
          for( int noTipoibb = 0; noTipoibb < mobjXMLDoc.selectNodes( "//OPTION" ) .getLength(); noTipoibb++ )
          {
            oTipoibb = mobjXMLDoc.selectNodes( "//OPTION" ) .item( noTipoibb );
            if( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_IBB) )  ).equals( XmlDomExtended.getText( oTipoibb.getAttributes().getNamedItem( "value" ) ) ) )
            {
              mvarIBBok = "OK";
            }
          }
        }
        else
        {
          mvarIBBok = "FC";
        }
      }
      else
      {
        if( Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_IBB) )  ) ) == 0 )
        {
          mvarIBBok = "OK";
        }
        else
        {
          mvarIBBok = "ER";
        }
      }

      if( mvarIBBok.equals( "ER" ) )
      {
        wvarCodErr.set( "-12" );
        wvarMensaje.set( "Valor de Codigo de Ingresos Brutos invalido" );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }

      if( mvarIBBok.equals( "FC" ) )
      {
        wvarCodErr.set( "-12" );
        wvarMensaje.set( "No se pudo recuperar el Valor del IBB desde el equipo central" );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncGetAll;
      }

      wvarStep = 240;
      if( wvarCodErr.isNumeric() && Integer.valueOf(wvarCodErr.getValue()).intValue() == 0 ) {
    	  fncGetAll = fncValidaABase(pvarRequest, wvarMensaje, wvarCodErr, pvarRes);
      } else {
        fncGetAll = false;
      }


      return fncGetAll;
  }

private boolean fncValidaABase( String pvarRequest, Variant wvarMensaje, StringHolder wvarCodErr, Variant pvarRes ) throws XmlDomExtendedException, SQLException, IOException
  {
    boolean fncValidaABase = false;
    int wvarStep = 0;
    boolean wvarError = false;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXML = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    java.sql.ResultSet cursor = null;
    int wvarContNode = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_PRODUCTOR = "";
    String mvarWDB_CAMPANA = "";
    String mvarWDB_KILOMETROS = "";
    String mvarWDB_CODPROV = "";
    String mvarWDB_CODPOST = "";
    String mvarWDB_COBROCOD = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_SUMAASEG = "";
    String mvarWDB_CODIGO_ACCESORIO = "";
    String mvarWDB_SUMA_ACCESORIO = "";
    String mvarWDB_AUMARCOD = "";
    String mvarWDB_AUMODCOD = "";
    String mvarWDB_AUSUBCOD = "";
    String mvarWDB_AUADICOD = "";
    String mvarWDB_AUMODORI = "";
    String mvarWDA_REQUESTID = "";
    String curUsuarCod = null;

      wvarStep = 10;
      wvarError = false;

      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 20;
      mvarWDB_CODINST = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST )  );
      }

      wvarStep = 30;
      mvarWDB_PRODUCTOR = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PRODUCTOR) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PRODUCTOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PRODUCTOR )  );
      }

      wvarStep = 40;
      mvarWDB_CAMPANA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CampaCod) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CAMPANA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CampaCod )  );
      }

      wvarStep = 50;
      mvarWDB_KILOMETROS = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_KMsrngCod) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_KILOMETROS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_KMsrngCod )  );
      }

      wvarStep = 60;
      mvarWDB_CODPROV = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODPROV = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi )  );
      }

      wvarStep = 70;
      mvarWDB_CODPOST = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODPOST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCod )  );
      }

      wvarStep = 80;
      mvarWDB_COBROCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD )  );
      }

      wvarStep = 90;
      mvarWDB_COBROTIP = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROTIP = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP )  ) );
      }

      wvarStep = 100;
      mvarWDB_SUMAASEG = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SUMAASEG) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_SUMAASEG = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SUMAASEG )  );
      }

      wvarStep = 110;
      mvarWDB_AUMODCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ModeAutCod) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_AUMODCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ModeAutCod )  );
      }

      wvarStep = 120;
      if( Strings.len( mvarWDB_AUMODCOD ) == 21 )
      {
        mvarWDB_AUMARCOD = Strings.mid( mvarWDB_AUMODCOD, 1, 5 );
        mvarWDB_AUSUBCOD = Strings.mid( mvarWDB_AUMODCOD, 11, 5 );
        mvarWDB_AUADICOD = Strings.mid( mvarWDB_AUMODCOD, 16, 5 );
        mvarWDB_AUMODORI = Strings.mid( mvarWDB_AUMODCOD, 21, 1 );
        mvarWDB_AUMODCOD = Strings.mid( mvarWDB_AUMODCOD, 6, 5 );
      }

      wvarStep = 130;
      mvarWDA_REQUESTID = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDA_REQUESTID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID )  );
      }

      wvarStep = 140;
      
		java.sql.Connection jdbcConn = null;
		try {
			JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
			jdbcConn = connFactory.getJDBCConnection(General.mcteDB);

			String sp = "{? = call SPSNCV_BRO_COTIZACION_SCO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		      CallableStatement ps = jdbcConn.prepareCall(sp);
		      int posiSP = 1;
		      wvarStep = 150;
		      ps.registerOutParameter(posiSP++, Types.INTEGER);
		      ps.registerOutParameter(27, Types.INTEGER);
		      ps.registerOutParameter(28, Types.INTEGER);
		
		      ps.setInt(posiSP++, Integer.parseInt(mvarWDB_CODINST.equals( "" ) ? null : mvarWDB_CODINST));
		
		      wvarStep = 160;
		      
		      
		      ps.setString(posiSP++, String.valueOf(mvarWDB_PRODUCTOR.equals( "" ) ? null : mvarWDB_PRODUCTOR));
		
		      wvarStep = 170;
		      
		      
		      ps.setString(posiSP++, String.valueOf(mvarWDB_CAMPANA.equals( "" ) ? null : mvarWDB_CAMPANA));
		
		      wvarStep = 180;
		      
		      
		      ps.setInt(posiSP++, Integer.parseInt(mvarWDB_KILOMETROS.equals( "" ) ? null : mvarWDB_KILOMETROS));
		
		      wvarStep = 190;
		      
		      
		      ps.setInt(posiSP++, Integer.parseInt(mvarWDB_CODPROV.equals( "" ) ? null : mvarWDB_CODPROV));
		
		      wvarStep = 200;
		      
		      
		      ps.setInt(posiSP++, Integer.parseInt(mvarWDB_CODPOST.equals( "" ) ? null : mvarWDB_CODPOST));
		
		      wvarStep = 210;
		      
		      
		      ps.setInt(posiSP++, Integer.parseInt(mvarWDB_COBROCOD.equals( "" ) ? null : mvarWDB_COBROCOD));
		
		      wvarStep = 220;
		      
		      
		      ps.setString(posiSP++, String.valueOf(mvarWDB_COBROTIP.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_COBROTIP)));
		
		      wvarStep = 230;
		      
		      
		      
		      String sumaAsegConver = null;
		      if (mvarWDB_SUMAASEG.indexOf(".") != -1) {
		          sumaAsegConver = mvarWDB_SUMAASEG.substring(0, mvarWDB_SUMAASEG.indexOf("."));
		      } else {
		          sumaAsegConver = mvarWDB_SUMAASEG;
		      }
		      ps.setInt(posiSP++, Integer.parseInt(mvarWDB_SUMAASEG.equals( "" ) ? null : sumaAsegConver));
		
		      wvarStep = 240;
		      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) ;
		      for( wvarContNode = 0; wvarContNode <= 4; wvarContNode++ )
		      {
		        wobjXMLNode = wobjXMLList.item( wvarContNode );
		        if( wvarContNode <= (wobjXMLList.getLength() - 1) )
		        {
		          mvarWDB_SUMA_ACCESORIO = "0";
		          wvarStep = 250;
		          if( ! (XmlDomExtended.Node_selectSingleNode(wobjXMLNode, mcteParam_PrecioAcc)  == (org.w3c.dom.Node) null) )
		          {
		            mvarWDB_SUMA_ACCESORIO = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode, mcteParam_PrecioAcc )  );
		          }
		          else
		          {
		            wvarError = true;
		            wvarCodErr.set( "-1" );
		            wvarMensaje.set( "Campo de Precio de Accesorios en blanco" );
		          }
		          mvarWDB_CODIGO_ACCESORIO = "0";
		          wvarStep = 260;
		          if( ! (XmlDomExtended.Node_selectSingleNode(wobjXMLNode, mcteParam_CodigoAcc )  == (org.w3c.dom.Node) null) )
		          {
		            mvarWDB_CODIGO_ACCESORIO = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode, mcteParam_CodigoAcc ));
		          }
		          else
		          {
		            wvarError = true;
		            wvarCodErr.set( "-1" );
		            wvarMensaje.set( "Campo de Codigo de Accesorios en blanco" );
		          }
		          wvarStep = 270;
		          
		          ps.setInt(posiSP++, Integer.parseInt(mvarWDB_CODIGO_ACCESORIO.equals( "" ) ? null : mvarWDB_CODIGO_ACCESORIO));
		          wvarStep = 280;
		          
		          wobjXMLNode = (org.w3c.dom.Node) null;
		          ps.setInt(posiSP ++, Integer.parseInt(mvarWDB_SUMA_ACCESORIO.equals( "" ) ? null : mvarWDB_SUMA_ACCESORIO));
		        }
		        else
		        {
		          wvarStep = 290;
		          ps.setInt(posiSP++, 0);
		
		          wvarStep = 300;
		          
		          ps.setInt(posiSP++, 0);
		        }
		      }
		      wobjXMLList = (org.w3c.dom.NodeList) null;
		
		      wvarStep = 310;
		      
		      
		      ps.setInt(posiSP++, Integer.parseInt(mvarWDB_AUMARCOD.equals( "" ) ? null : mvarWDB_AUMARCOD));
		
		      wvarStep = 320;
		      
		      
		      ps.setInt(posiSP++, Integer.parseInt(mvarWDB_AUMODCOD.equals( "" ) ? null : mvarWDB_AUMODCOD));
		
		      wvarStep = 330;
		      
		      
		      ps.setInt(posiSP++, Integer.parseInt(mvarWDB_AUSUBCOD.equals( "" ) ? null : mvarWDB_AUSUBCOD));
		
		      wvarStep = 340;
		      
		      
		      ps.setInt(posiSP++, Integer.parseInt(mvarWDB_AUADICOD.equals( "" ) ? null : mvarWDB_AUADICOD));
		
		      wvarStep = 350;
		      
		      
		      ps.setString(posiSP++, String.valueOf(mvarWDB_AUMODORI.equals( "" ) ? null : mvarWDB_AUMODORI));
		
		      wvarStep = 360;
		      
		      
		      ps.setBigDecimal(posiSP++, new BigDecimal(mvarWDA_REQUESTID));
		      //jc 08/2010 el @WDB_NROCOT al cambiar el componente de cotiz. no se obtiene mas en la sp SPSNCV_BRO_COTIZACION_SCO viene en 0
		      wvarStep = 370;
		      
		      ps.setInt(posiSP++, 0);
		      wvarStep = 380;
		      ps.setInt(posiSP++, 0);
		
		      wvarStep = 390;
		      if( ! (wvarError) )
		      {
		        boolean result = ps.execute();
		
		        if (result) {
		          //El primero es un ResultSet
		        } else {
		          //Salteo el "updateCount"
		          int uc = ps.getUpdateCount();
		          if (uc  == -1) {
		              //System.out.println("No hay resultados");
		          }
		          result = ps.getMoreResults();
		        }
		
		        cursor = ps.getResultSet();
		        
		
		        wvarStep = 400;
		        wobjXML = new XmlDomExtended();
		        wobjXML.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteArchivoAUSCOT_XML));
		        wvarStep = 410;
		
		        //SI NO HUBO ERROR AGREGO LOS DATOS AL XML DE ENTRADA PARA LA RESPUESTA
		        if(cursor.next())
		        {
		          if( Strings.toUpperCase(cursor.getString(1).trim()).equals( "OK" ))
		          {
		            wvarStep = 420;
		            //jc 08/2010 el @WDB_NROCOT al cambiar el componente de cotiz. no se obtiene mas en la sp SPSNCV_BRO_COTIZACION_SCO viene en 0
		            wvarStep = 430;
		            for( wvarContNode = 0; wvarContNode <= (cursor.getMetaData().getColumnCount() - 1); wvarContNode++ )
		            {
		              wobjXMLRequest.selectSingleNode( "//Request" ).appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, Strings.toUpperCase(cursor.getMetaData().getColumnName(wvarContNode + 1)), "" ));
		              wvarStep = 440;
		              if( (cursor.getMetaData().getColumnType(wvarContNode + 1) == java.sql.Types.NUMERIC) && (cursor.getMetaData().getPrecision(wvarContNode + 1) == 18) )
		              {
		                XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//" + Strings.toUpperCase( cursor.getMetaData().getColumnName(wvarContNode + 1) ) ) , String.valueOf(cursor.getInt(wvarContNode + 1) * 100));
		              }
		              else
		              {
		                XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//" + Strings.toUpperCase( cursor.getMetaData().getColumnName(wvarContNode + 1) ) ) , Strings.trim( cursor.getString(wvarContNode + 1) ) );
		              }
		            }
		            curUsuarCod = cursor.getString("USUARCOD");
		            int nroCot = ps.getInt(27);
		            int certiSec = ps.getInt(28);
		            wobjXMLRequest.selectSingleNode( "//Request" ).appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "WDB_NROCOT", "" ));
		            XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//WDB_NROCOT" ) , String.valueOf(nroCot));
		            wobjXMLRequest.selectSingleNode( "//Request" ).appendChild(wobjXMLRequest.createNode( org.w3c.dom.Node.ELEMENT_NODE, "CERTISEC", "" ));
		            XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//CERTISEC" ) , String.valueOf(certiSec) );
		            pvarRes.set(XmlDomExtended.marshal(wobjXMLRequest.getDocument().getDocumentElement()));
		          }
		          else
		          {
		            //error
		            wvarStep = 450;
		            wvarError = true;
		            if( wobjXML.selectNodes( ("//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + cursor.getString(1) + "']") ) .getLength() > 0 )
		            {
		              wvarStep = 460;
		              wvarMensaje.set( XmlDomExtended.getText( wobjXML.selectSingleNode( "//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + cursor.getString(1) + "']/TEXTOERROR" )  ) );
		              wvarCodErr.set( XmlDomExtended.getText( wobjXML.selectSingleNode( "//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + cursor.getString(1) + "']/VALORRESPUESTA" )  ) );
		            }
		            else
		            {
		              wvarCodErr.set( cursor.getString(wvarContNode + 1));
		              wvarMensaje.set( "No existe descripcion para este error" );
		            }
		          }
		        }
		        else
		        {
		          //error
		          wvarError = true;
		          wvarCodErr.set( "-16" );
		          wvarMensaje.set( "No se retornaron datos de la base" );
		        }
		      }
		} finally {
			try {
				if (jdbcConn != null)
					jdbcConn.close();
			} catch (Exception e) {
				logger.log(Level.WARNING, "Exception al hacer un close", e);
			}
		}

      wvarStep = 470;

      wobjXML = null;
      fncValidaABase = true;
      if( wvarError ) {
        fncValidaABase = false;
      } else {
    	fncValidaABase = fncValidaMQ(curUsuarCod, mvarWDB_PRODUCTOR, mvarWDB_CAMPANA, mvarWDB_COBROCOD, mvarWDB_COBROTIP, wvarMensaje, wvarCodErr, pvarRes);
      }
      return fncValidaABase;
  }

  private boolean fncValidaMQ( String varUsuarcod, String varProductor, String varCampana, String varCobrocod, String varCobrotip, Variant wvarMensaje, StringHolder wvarCodErr, Variant pvarRes ) throws XmlDomExtendedException
  {
    boolean fncValidaMQ = false;
    int wvarStep = 0;
    String mvarRequest = "";
    String mvarResponse = "";
    org.w3c.dom.Node oProd = null;
    XmlDomExtended mobjXMLDoc = null;
    String mvarProdok = "";
    org.w3c.dom.NodeList wobjXMLList = null;
    int wvarContNode = 0;

    wvarStep = 0;
    wvarCodErr.set("0");

    //valida productor
      wvarStep = 1;
      mvarRequest = "<Request><DEFINICION>ProductoresHabilitadosParaCotizar.xml</DEFINICION><AplicarXSL>ProductoresHabilitadosParaCotizarXML.xsl</AplicarXSL>";
      mvarRequest = mvarRequest + "<USUARCOD>" + varUsuarcod + "</USUARCOD>";
      mvarRequest = mvarRequest + "<CLIENSECAS></CLIENSECAS><NIVELCLAS></NIVELCLAS><RAMOPCOD>AUS1</RAMOPCOD></Request>";

      mvarResponse = Utils.getConsultaMQ(mvarRequest);
      mobjXMLDoc = new XmlDomExtended();
      mobjXMLDoc.loadXML( mvarResponse );

      mvarProdok = "ER";
      if( ! (mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          for( int noProd = 0; noProd < mobjXMLDoc.selectNodes( "//AGENTE" ) .getLength(); noProd++ )
          {
            oProd = mobjXMLDoc.selectNodes( "//AGENTE" ) .item( noProd );
            if( varProductor.equals( XmlDomExtended.getText( oProd.getAttributes().getNamedItem( "AGENTCOD" ) ) ) )
            {
              mvarProdok = "OK";
            }
          }
        }
        else
        {
          mvarProdok = "ER";
        }
      }
      else
      {
        mvarProdok = "FC";
      }

      if( mvarProdok.equals( "ER" ) )
      {
        wvarCodErr.set( "-101" );
        wvarMensaje.set( "Productor no habilitado" );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncValidaMQ;
      }

      if( mvarProdok.equals( "FC" ) )
      {
        wvarCodErr.set( "-101" );
        wvarMensaje.set( "No se pudo recuperar el codigo de productor desde el equipo central" );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncValidaMQ;
      }

      mobjXMLDoc = null;

      wvarStep = 2;
      mvarRequest = "<Request><DEFINICION>CampanasHabilitadasProductor.xml</DEFINICION>";
      mvarRequest = mvarRequest + "<USUARCOD>" + varUsuarcod + "</USUARCOD>";
      mvarRequest = mvarRequest + "<RAMOPCOD>AUS1</RAMOPCOD><AGENTCLA>PR</AGENTCLA>";
      mvarRequest = mvarRequest + "<AGENTCOD>" + varProductor + "</AGENTCOD></Request>";

      mvarResponse = Utils.getConsultaMQ(mvarRequest);
      mobjXMLDoc = new XmlDomExtended();
      mobjXMLDoc.loadXML( mvarResponse );

      mvarProdok = "ER";
      if( ! (mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {

          wobjXMLList = mobjXMLDoc.selectNodes( "//CAMPOS/T-CAMPANAS-SAL/CAMPANA" ) ;
          for( wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++ )
          {
            if( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), "CAMPACOD" )  ).equals( varCampana ) )
            {
              mvarProdok = "OK";
            }
          }
        }
        else
        {
          mvarProdok = "ER";
        }
      }
      else
      {
        mvarProdok = "FC";
      }

      if( mvarProdok.equals( "ER" ) )
      {
        wvarCodErr.set( "-101" );
        wvarMensaje.set( "Campania no habilitada" );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncValidaMQ;
      }

      if( mvarProdok.equals( "FC" ) )
      {
        wvarCodErr.set( "-101" );
        wvarMensaje.set( "No se pudo recuperar el codigo de Campania desde el equipo central" );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncValidaMQ;
      }

      mobjXMLDoc = null;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      // valida forma de pago
      wvarStep = 3;
      mvarRequest = "<Request><DEFINICION>FormasDePagoxCampana.xml</DEFINICION><AplicarXSL>FormasDePagoxCampanaOut.xsl</AplicarXSL>";
      mvarRequest = mvarRequest + "<USUARCOD></USUARCOD><RAMOPCOD>AUS1</RAMOPCOD>";
      mvarRequest = mvarRequest + "<POLIZANN>0</POLIZANN><POLIZSEC>1</POLIZSEC>";
      mvarRequest = mvarRequest + "<CAMPACOD>" + varCampana + "</CAMPACOD></Request>";

      mvarResponse = Utils.getConsultaMQ(mvarRequest);
      mobjXMLDoc = new XmlDomExtended();
      mobjXMLDoc.loadXML( mvarResponse );

      mvarProdok = "ER";
      if( ! (mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          for( int noProd = 0; noProd < mobjXMLDoc.selectNodes( "//option" ) .getLength(); noProd++ )
          {
            oProd = mobjXMLDoc.selectNodes( "//option" ) .item( noProd );
            if( varCobrocod.equals( XmlDomExtended.getText( oProd.getAttributes().getNamedItem( "value" ) ) ) )
            {
              if( varCobrocod.equals( "4" ) )
              {
                mvarProdok = "OK";
              }
              else
              {
                if( varCobrotip.equals( XmlDomExtended.getText( oProd.getAttributes().getNamedItem( "cobrotip" ) ) ) )
                {
                  mvarProdok = "OK";
                }
              }
            }
          }
        }
        else
        {
          mvarProdok = "ER";
        }
      }
      else
      {
        mvarProdok = "FC";
      }

      if( mvarProdok.equals( "ER" ) )
      {
        wvarCodErr.set( "-104" );
        wvarMensaje.set( "Forma de Pago invalida" );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncValidaMQ;
      }

      if( mvarProdok.equals( "FC" ) )
      {
        wvarCodErr.set( "-104" );
        wvarMensaje.set( "No se pudo recuperar la forma de pago desde el equipo central" );
        pvarRes.set( Utils.createResponseXmlFalseWithMessage(wvarMensaje.toString()) );
        return fncValidaMQ;
      }

      mobjXMLDoc = null;

      wvarStep = 240;
      if( wvarCodErr.isNumeric() && Integer.valueOf(wvarCodErr.getValue()).intValue() == 0 )
      {
        fncValidaMQ = true;
      }
      else
      {
        fncValidaMQ = false;
      }

      return fncValidaMQ;
  }

  public void Activate() throws Exception
  {
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
