package com.qbe.services.interWSBrok.impl;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.ado.AdoConst;
import diamondedge.ado.Field;
import diamondedge.ado.Recordset;
import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.Variant;

public class General
{
  public static final String mcteErrorInesperadoDescr = "Ocurrio un error inesperado en la ejecucion del componente";
  public static final String mcteErrorInesperadoCod = "-50";
  public static final String mcteDB = "lbawA_InterWSBrok.udl";
  public static final boolean mcteIfFunctionFailed_failClass = true;
  public static final String mcteArchivoAUSCOT_XML = "LBA_VALIDACION_COT_AU.xml";
  public static final String mcteArchivoAUSSOL_XML = "LBA_VALIDACION_SOL_AU.xml";
  public static final String mcteArchivoHOM_XML = "LBA_VALIDACION_COT_HO.xml";
  public static final String mcteArchivoATM_XML = "LBA_VALIDACION_COT_ATM.xml";
  /**
   * static variable for method: fncTransformXSL
   * static variable for method: fncTransformXSLADO
   */
  private static final String wcteFnName = "fncTransformXSLADO";

  public static boolean fncTransformXSL( Variant pvarMensaje, String wvarXML_IN, String pvarNodos, String pvarNodo, Variant wvarXML_OUT )
  {
    boolean fncTransformXSL = false;
    int wvarStep = 0;
    XmlDomExtended wobjXML = null;
    XmlDomExtended wobjXSL = null;
    org.w3c.dom.Node wobjXMLNode = null;
    String wvarStrXSL = "";
    try 
    {

      //declaracion de objetos
      //declaracion de variables
      wvarStrXSL = "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\" xmlns:rs=\"urn:schemas-microsoft-com:rowset\" xmlns:z=\"#RowsetSchema\">";
      wvarStrXSL = wvarStrXSL + " <xsl:template match='rs:data'>";
      wvarStrXSL = wvarStrXSL + "  <xsl:element name='" + pvarNodos + "'>";
      wvarStrXSL = wvarStrXSL + "   <xsl:apply-templates />";
      wvarStrXSL = wvarStrXSL + "  </xsl:element>";
      wvarStrXSL = wvarStrXSL + " </xsl:template>";

      wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
      wvarStrXSL = wvarStrXSL + "   <xsl:element name='" + pvarNodo + "'>";

      wobjXML = new XmlDomExtended();
      wobjXML.loadXML( wvarXML_IN );

      for( int nwobjXMLNode = 0; nwobjXMLNode < wobjXML.selectSingleNode( "//z:row" ) .getAttributes().getLength(); nwobjXMLNode++ )
      {
        wobjXMLNode = wobjXML.selectSingleNode( "//z:row" ) .getAttributes().item( nwobjXMLNode );
        wvarStrXSL = wvarStrXSL + "     <xsl:element name='" + Strings.toUpperCase( wobjXMLNode.getNodeName() ) + "'><xsl:value-of select='@" + wobjXMLNode.getNodeName() + "'/></xsl:element>";
      }

      wvarStrXSL = wvarStrXSL + "  </xsl:element>";
      wvarStrXSL = wvarStrXSL + "  <xsl:apply-templates />";
      wvarStrXSL = wvarStrXSL + " </xsl:template>";
      wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";


      wobjXSL = new XmlDomExtended();
      wobjXSL.loadXML( wvarStrXSL );


      wvarXML_OUT.set( wobjXML.transformNode( wobjXSL ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" ) );

      fncTransformXSL = true;
      return fncTransformXSL;
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        pvarMensaje.set( mcteErrorInesperadoDescr );

        fncTransformXSL = false;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncTransformXSL;
  }

  public static boolean fncTransformXSLADO( Variant pvarMensaje, Recordset pobjRecordset, String pvarNodos, String pvarNodo, Variant wvarXSL_OUT )
  {
    boolean fncTransformXSLADO = false;
    int wvarStep = 0;
    Field wobjFieldAdo = null;
    String wvarStrXSL = "";
    try 
    {

      //declaracion de objetos
      //declaracion de variables
      wvarStrXSL = "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\" xmlns:rs=\"urn:schemas-microsoft-com:rowset\" xmlns:z=\"#RowsetSchema\">";
      wvarStrXSL = wvarStrXSL + " <xsl:template match='rs:data'>";
      wvarStrXSL = wvarStrXSL + "  <xsl:element name='" + pvarNodos + "'>";
      wvarStrXSL = wvarStrXSL + "   <xsl:apply-templates />";
      wvarStrXSL = wvarStrXSL + "  </xsl:element>";
      wvarStrXSL = wvarStrXSL + " </xsl:template>";

      wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
      wvarStrXSL = wvarStrXSL + "   <xsl:element name='" + pvarNodo + "'>";

      for( int nwobjFieldAdo = 0; nwobjFieldAdo < pobjRecordset.getFields().getCount(); nwobjFieldAdo++ )
      {
        wobjFieldAdo = pobjRecordset.getFields().getField(nwobjFieldAdo);
        wvarStrXSL = wvarStrXSL + "     <xsl:element name='" + Strings.toUpperCase( wobjFieldAdo.getName() ) + "' ><xsl:value-of select='@" + wobjFieldAdo.getName() + "'/></xsl:element>";
      }

      wvarStrXSL = wvarStrXSL + "  </xsl:element>";
      wvarStrXSL = wvarStrXSL + "  <xsl:apply-templates />";
      wvarStrXSL = wvarStrXSL + " </xsl:template>";

      for( int nwobjFieldAdo = 0; nwobjFieldAdo < pobjRecordset.getFields().getCount(); nwobjFieldAdo++ )
      {
        wobjFieldAdo = pobjRecordset.getFields().getField(nwobjFieldAdo);
        if( (wobjFieldAdo.getType() == AdoConst.adChar) || (wobjFieldAdo.getType() == AdoConst.adLongVarChar) || (wobjFieldAdo.getType() == AdoConst.adLongVarWChar) || (wobjFieldAdo.getType() == AdoConst.adVarChar) || (wobjFieldAdo.getType() == AdoConst.adVarWChar) || (wobjFieldAdo.getType() == AdoConst.adWChar) )
        {
          wvarStrXSL = wvarStrXSL + "  <xsl:output cdata-section-elements='" + Strings.toUpperCase( wobjFieldAdo.getName() ) + "'/>";
        }
      }


      wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";




      wvarXSL_OUT.set( wvarStrXSL );

      fncTransformXSLADO = true;
      fin: 
      //libero los objetos
      return fncTransformXSLADO;
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {



        pvarMensaje.set( mcteErrorInesperadoDescr );

        fncTransformXSLADO = false;
        //unsup Resume fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncTransformXSLADO;
  }
}
