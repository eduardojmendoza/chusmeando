package com.qbe.services.interWSBrok.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.interWSBrok.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class PutSolicitudHO implements VBObjectClass
{
  static final String mcteClassName = "LBA_InterWSBrok.PutSolicitudHO";
  static final String mcteStoreProc = "SPSNCV_BRO_SOLI_HOM1_GRABA";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  static final String mcteParam_COBROCOD = "COBROCOD";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_TIPOHOGAR = "TIPOHOGAR";
  static final String mcteParam_PLANNCOD = "PLANNCOD";
  static final String mcteParam_Provi = "PROVI";
  static final String mcteParam_LocalidadCod = "LOCALIDADCOD";
  static final String mcteParam_RAMOPCOD = "RAMOPCOD";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  static final String mcteParam_NROCOT = "COT_NRO";
  static final String mcteParam_CERTISEC = "CERTISEC";
  static final String mcteParam_ClienAp1 = "CLIENAP1";
  static final String mcteParam_ClienAp2 = "CLIENAP2";
  static final String mcteParam_ClienNom = "CLIENNOM";
  static final String mcteParam_CLIENSEX = "SEXO";
  static final String mcteParam_CLIENEST = "ESTADO";
  static final String mcteParam_NacimAnn = "NACIMANN";
  static final String mcteParam_NacimMes = "NACIMMES";
  static final String mcteParam_NacimDia = "NACIMDIA";
  static final String mcteParam_EMAIL = "EMAIL";
  static final String mcteParam_DOMICDOM = "DOMICDOM";
  static final String mcteParam_DomicDnu = "DOMICDNU";
  static final String mcteParam_DOMICPIS = "DOMICPIS";
  static final String mcteParam_DomicPta = "DOMICPTA";
  static final String mcteParam_DOMICPOB = "DOMICPOB";
  static final String mcteParam_BARRIOCOUNT = "BARRIOCOUNT";
  static final String mcteParam_TELNRO = "TELNRO";
  static final String mcteParam_DomicDomCo = "DOMICDOMCOR";
  static final String mcteParam_DomicDNUCo = "DOMICDNUCOR";
  static final String mcteParam_DomicPisCo = "DOMICPISCOR";
  static final String mcteParam_DomicPtaCo = "DOMICPTACOR";
  static final String mcteParam_DOMICPOBCo = "DOMICPOBCOR";
  static final String mcteParam_ProviCo = "PROVICOR";
  static final String mcteParam_LocalidadCodCo = "LOCALIDADCODCOR";
  static final String mcteParam_TelNroCo = "TELNROCOR";
  /**
   * Const mcteParam_TipoCuen             As String = "TIPOCUEN"
   */
  static final String mcteParam_CUENNUME = "CUENNUME";
  /**
   * Const mcteParam_TarjeCod             As String = "TARJECOD"
   */
  static final String mcteParam_VENCIANN = "VENCIANN";
  static final String mcteParam_VENCIMES = "VENCIMES";
  static final String mcteParam_VENCIDIA = "VENCIDIA";
  static final String mcteParam_UsoTipos = "USOTIPOS";
  static final String mcteParam_ALARMTIP = "ALARMTIP";
  static final String mcteParam_GUARDTIP = "GUARDTIP";
  static final String mcteParam_SWCALDER = "CALDERA";
  static final String mcteParam_SWASCENS = "ASCENSOR";
  static final String mcteParam_SWCREJAS = "REJAS";
  static final String mcteParam_SWPBLIND = "PUERTABLIND";
  static final String mcteParam_SWDISYUN = "DISYUNTOR";
  static final String mcteParam_SWPROPIE = "PROPIEDAD";
  static final String mcteParam_ZONA = "CODIZONA";
  static final String mcteParam_TipoDocu = "TIPODOCU";
  static final String mcteParam_NumeDocu = "NUMEDOCU";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS/COBERTURA";
  static final String mcteParam_COBERCOD = "COBERCOD";
  static final String mcteParam_CONTRMOD = "CONTRMOD";
  static final String mcteParam_CAPITASG = "CAPITASG";
  /**
   * DATOS DE LAS CAMPA�AS
   */
  static final String mcteNodos_Campa = "//Request/CAMPANIAS/CAMPANIA";
  static final String mcteParam_CampaCod = "CAMPACOD";
  /**
   * DATOS DE LOS ELECTRODOMESTICOS
   */
  static final String mcteNodos_Elect = "//Request/ELECTRODOMESTICOS/ELECTRODOMESTICO";
  static final String mcteParam_Tipo = "TIPO";
  static final String mcteParam_Marca = "MARCA";
  static final String mcteParam_Modelo = "MODELO";
  /**
   * PRODUCTOR
   */
  static final String mctePORTAL = "LBA";
  /**
   * Objetos del FrameWork
   */
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncPutAll
   */
  private final String wcteFnName = "fncPutAll";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarMensaje = "";
    Variant wvarCodErr = new Variant();
    Variant pvarRes = new Variant();
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wvarMensaje = "";
      wvarCodErr.set( "" );
      pvarRes.set( "" );
      if( ! (invoke( "fncPutAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*TBD mobjCOM_Context.SetAbort() ;*/
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*TBD mobjCOM_Context.SetComplete() ;*/
        }
        return IAction_Execute;
      }
      //
      pvarResponse.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + pvarRes + "</LBA_WS>" );
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        //mobjCOM_Context.SetAbort
        /*TBD mobjCOM_Context.SetComplete() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncPutAll( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    XmlDomExtended wobjXMLRequest = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarcounter = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTIPOL = "";
    String mvarWDB_CERTIANN = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_SUPLENUM = "";
    String mvarWDB_NUMEDOCU = "";
    String mvarWDB_TIPODOCU = "";
    String mvarWDB_CLIENAP1 = "";
    String mvarWDB_CLIENAP2 = "";
    String mvarWDB_CLIENNOM = "";
    String mvarWDB_NACIMANN = "";
    String mvarWDB_NACIMMES = "";
    String mvarWDB_NACIMDIA = "";
    String mvarWDB_CLIENSEX = "";
    String mvarWDB_CLIENEST = "";
    String mvarWDB_EFECTANN = "";
    String mvarWDB_EFECTMES = "";
    String mvarWDB_EFECTDIA = "";
    String mvarWDB_EMAIL = "";
    String mvarWDB_DOMICDOM = "";
    String mvarWDB_DOMICDNU = "";
    String mvarWDB_DOMICPIS = "";
    String mvarWDB_DOMICPTA = "";
    String mvarWDB_DOMICPOB = "";
    String mvarWDB_BARRIOCOUNT = "";
    String mvarWDB_DOMICCPO = "";
    String mvarWDB_PROVICOD = "";
    String mvarWDB_TELNRO = "";
    String mvarWDB_DOMICDOM_CO = "";
    String mvarWDB_DOMICDNU_CO = "";
    String mvarWDB_DOMICPIS_CO = "";
    String mvarWDB_DOMICPTA_CO = "";
    String mvarWDB_DOMICPOB_CO = "";
    String mvarWDB_DOMICCPO_CO = "";
    String mvarWDB_PROVICOD_CO = "";
    String mvarWDB_TELNRO_CO = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_CUENNUME = "";
    String mvarWDB_VENCIANN = "";
    String mvarWDB_VENCIMES = "";
    String mvarWDB_VENCIDIA = "";
    String mvarWDB_BANCOCOD = "";
    String mvarWDB_SUCURCOD = "";
    String mvarWDB_P_EMISIANN = "";
    String mvarWDB_P_EMISIMES = "";
    String mvarWDB_P_EMISIDIA = "";
    String mvarWDB_P_COBROCOD = "";
    String mvarWDB_P_TIVIVCOD = "";
    String mvarWDB_P_USOTIPOS = "";
    String mvarWDB_P_SWCALDER = "";
    String mvarWDB_P_SWASCENS = "";
    String mvarWDB_P_ALARMTIP = "";
    String mvarWDB_P_GUARDTIP = "";
    String mvarWDB_P_SWCREJAS = "";
    String mvarWDB_P_SWPBLIND = "";
    String mvarWDB_P_SWDISYUN = "";
    String mvarWDB_P_SWPROPIE = "";
    String mvarWDB_P_PLANNCOD = "";
    String mvarWDB_P_BARRIOCOUNT = "";
    String mvarWDB_P_ZONA = "";
    String mvarWDB_P_CLIENIVA = "";
    String mvarWDB_P_COBERCOD = "";
    String mvarWDB_P_CONTRMOD = "";
    String mvarWDB_P_CAPITASG = "";
    String mvarWDB_P_CAMPACOD = "";
    String mvarWDB_P_TIPO = "";
    String mvarWDB_P_MARCA = "";
    String mvarWDB_P_MODELO = "";

    //
    //
    //
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      //
      mvarWDB_CODINST = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST )  );
      }
      //
      wvarStep = 30;
      //
      mvarWDB_RAMOPCOD = "HOM1";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAMOPCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_RAMOPCOD )  );
      }
      //
      wvarStep = 40;
      //
      mvarWDB_POLIZANN = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN )  );
      }
      //
      wvarStep = 50;
      //
      mvarWDB_POLIZSEC = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC )  );
      }
      //
      wvarStep = 60;
      //
      mvarWDB_CERTISEC = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CERTISEC) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTISEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CERTISEC )  );
      }
      //
      wvarStep = 70;
      //
      mvarWDB_P_EMISIANN = Strings.right( Strings.fill( 4, "0" ) + DateTime.year( DateTime.now() ), 4 );
      mvarWDB_P_EMISIMES = Strings.right( Strings.fill( 2, "0" ) + DateTime.month( DateTime.now() ), 2 );
      mvarWDB_P_EMISIDIA = Strings.right( Strings.fill( 2, "0" ) + DateTime.day( DateTime.now() ), 2 );
      //
      mvarWDB_EFECTANN = Strings.right( Strings.fill( 4, "0" ) + DateTime.year( DateTime.now() ), 4 );
      mvarWDB_EFECTMES = Strings.right( Strings.fill( 2, "0" ) + DateTime.month( DateTime.now() ), 2 );
      mvarWDB_EFECTDIA = Strings.right( Strings.fill( 2, "0" ) + DateTime.day( DateTime.now() ), 2 );
      //
      wvarStep = 80;
      //
      mvarWDB_NUMEDOCU = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NumeDocu) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NUMEDOCU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NumeDocu )  );
      }
      //
      wvarStep = 90;
      //
      mvarWDB_TIPODOCU = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoDocu) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPODOCU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoDocu )  );
      }
      //
      wvarStep = 100;
      //
      mvarWDB_CLIENAP1 = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienAp1) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENAP1 = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienAp1 )  ) );
      }
      //
      wvarStep = 110;
      //
      mvarWDB_CLIENAP2 = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienAp2) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENAP2 = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienAp2 )  ) );
      }
      //
      wvarStep = 120;
      //
      mvarWDB_CLIENNOM = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienNom) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENNOM = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienNom )  ) );
      }
      //
      wvarStep = 130;
      //
      mvarWDB_NACIMANN = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimAnn) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimAnn )  );
      }
      //
      wvarStep = 140;
      //
      mvarWDB_NACIMMES = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimMes) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimMes )  );
      }
      //
      wvarStep = 150;
      //
      mvarWDB_NACIMDIA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimDia) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMDIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimDia )  );
      }
      //
      wvarStep = 160;
      //
      mvarWDB_CLIENSEX = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENSEX) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENSEX = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENSEX )  ) );
      }
      //
      wvarStep = 170;
      //
      mvarWDB_CLIENEST = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENEST = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENEST )  ) );
      }
      //
      wvarStep = 180;
      //
      mvarWDB_EMAIL = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EMAIL) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_EMAIL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_EMAIL )  );
      }
      //
      wvarStep = 190;
      //
      mvarWDB_DOMICDOM = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICDOM) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDOM = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICDOM )  ) );
      }
      //
      wvarStep = 200;
      //
      mvarWDB_DOMICDNU = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDnu) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDNU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDnu )  );
      }
      //
      wvarStep = 210;
      //
      mvarWDB_DOMICPIS = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICPIS) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPIS = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICPIS )  ) );
      }
      //
      wvarStep = 220;
      //
      mvarWDB_DOMICPTA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPta) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPTA = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPta )  ) );
      }
      //
      wvarStep = 230;
      //
      mvarWDB_DOMICPOB = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICPOB) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPOB = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICPOB )  ) );
      }
      //
      wvarStep = 240;
      //
      mvarWDB_DOMICCPO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICCPO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCod )  );
      }
      //
      wvarStep = 250;
      //
      mvarWDB_PROVICOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi )  );
      }
      //
      wvarStep = 260;
      //
      mvarWDB_TELNRO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TELNRO) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TELNRO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TELNRO )  );
      }
      //
      wvarStep = 270;
      //
      mvarWDB_BARRIOCOUNT = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_BARRIOCOUNT) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_BARRIOCOUNT = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_BARRIOCOUNT )  ) );
      }
      //
      wvarStep = 280;
      //
      mvarWDB_DOMICDOM_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDomCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDOM_CO = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDomCo )  ) );
      }
      //
      wvarStep = 290;
      //
      mvarWDB_DOMICDNU_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDNUCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDNU_CO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDNUCo )  );
      }
      //
      wvarStep = 300;
      //
      mvarWDB_DOMICPIS_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPisCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPIS_CO = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPisCo )  ) );
      }
      //
      wvarStep = 310;
      //
      mvarWDB_DOMICPTA_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPtaCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPTA_CO = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPtaCo )  ) );
      }
      //
      wvarStep = 320;
      //
      mvarWDB_DOMICPOB_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICPOBCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPOB_CO = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICPOBCo )  ) );
      }
      //
      wvarStep = 330;
      //
      mvarWDB_DOMICCPO_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCodCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICCPO_CO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCodCo )  );
      }
      //
      wvarStep = 340;
      //
      mvarWDB_PROVICOD_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ProviCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICOD_CO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ProviCo )  );
      }
      //
      wvarStep = 350;
      //
      mvarWDB_TELNRO_CO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TelNroCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TELNRO_CO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TelNroCo )  );
      }
      //
      wvarStep = 360;
      //
      mvarWDB_COBROTIP = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROTIP = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP )  ) );
      }
      //
      wvarStep = 370;
      //
      mvarWDB_CUENNUME = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CUENNUME = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CUENNUME )  );
      }
      //
      wvarStep = 380;
      //
      mvarWDB_VENCIANN = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VENCIANN) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_VENCIANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_VENCIANN )  );
      }
      //
      wvarStep = 390;
      //
      mvarWDB_VENCIMES = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VENCIMES) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_VENCIMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_VENCIMES )  );
      }
      //
      wvarStep = 400;
      //
      mvarWDB_VENCIDIA = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VENCIDIA) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_VENCIDIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_VENCIDIA )  );
      }
      //
      wvarStep = 410;
      if( Obj.toInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) )  ) ) == 4 )
      {
        mvarWDB_BANCOCOD = "0";
        mvarWDB_SUCURCOD = "0000";
      }
      else
      {
        mvarWDB_BANCOCOD = "9000";
        mvarWDB_SUCURCOD = "8888";
      }
      wvarStep = 420;
      mvarWDB_P_COBROCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_COBROCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD )  );
      }
      //
      wvarStep = 430;
      mvarWDB_P_TIVIVCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TIPOHOGAR) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_TIVIVCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TIPOHOGAR )  );
      }
      //
      wvarStep = 440;
      mvarWDB_P_USOTIPOS = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_UsoTipos) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_USOTIPOS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_UsoTipos )  );
      }
      //
      wvarStep = 450;
      mvarWDB_P_SWCALDER = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWCALDER) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWCALDER = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWCALDER )  ) );
      }
      //
      wvarStep = 460;
      mvarWDB_P_SWASCENS = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWASCENS) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWASCENS = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWASCENS )  ) );
      }
      //
      wvarStep = 470;
      mvarWDB_P_ALARMTIP = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ALARMTIP) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_ALARMTIP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ALARMTIP )  );
      }
      //
      wvarStep = 480;
      mvarWDB_P_GUARDTIP = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_GUARDTIP) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_GUARDTIP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_GUARDTIP )  );
      }
      //
      wvarStep = 490;
      mvarWDB_P_SWCREJAS = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWCREJAS) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWCREJAS = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWCREJAS )  ) );
      }
      //
      wvarStep = 500;
      mvarWDB_P_SWPBLIND = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWPBLIND) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWPBLIND = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWPBLIND )  ) );
      }
      //
      wvarStep = 510;
      mvarWDB_P_SWDISYUN = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWDISYUN) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWDISYUN = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWDISYUN )  ) );
      }
      //
      wvarStep = 520;
      mvarWDB_P_SWPROPIE = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_SWPROPIE) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_SWPROPIE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SWPROPIE )  );
      }
      //
      wvarStep = 530;
      mvarWDB_P_PLANNCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PLANNCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_PLANNCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PLANNCOD )  );
      }
      //
      wvarStep = 540;
      mvarWDB_P_ZONA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ZONA) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_ZONA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ZONA )  );
      }
      //
      wvarStep = 550;
      mvarWDB_P_CLIENIVA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CLIENIVA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENIVA )  );
      }
      //
      wvarStep = 560;
      mvarWDB_P_COBERCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBERCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_COBERCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBERCOD )  );
      }
      //
      wvarStep = 570;
      mvarWDB_P_CONTRMOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CONTRMOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CONTRMOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CONTRMOD )  );
      }
      //
      wvarStep = 580;
      mvarWDB_P_CAPITASG = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CAPITASG) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CAPITASG = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CAPITASG )  );
      }
      //
      wvarStep = 590;
      mvarWDB_P_CAMPACOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CampaCod) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CAMPACOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CampaCod )  );
      }
      //
      wvarStep = 600;
      mvarWDB_P_TIPO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Tipo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_TIPO = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Tipo )  ) );
      }
      //
      wvarStep = 610;
      mvarWDB_P_MARCA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Marca) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_MARCA = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Marca )  ) );
      }
      //
      wvarStep = 620;
      mvarWDB_P_MODELO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Modelo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_MODELO = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Modelo )  ) );
      }
      //
      wvarStep = 630;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.mcteDB);
      //
      wvarStep = 640;
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 650;
      //
      
      
      //
      wvarStep = 660;
      wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      
      //
      wvarStep = 670;
      wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      
      //
      wvarStep = 680;
      
      
      //
      wvarStep = 690;
      
      
      //
      wvarStep = 700;
      
      
      //
      wvarStep = 710;
      
      
      //
      wvarStep = 720;
      
      
      //
      wvarStep = 730;
      
      
      //
      wvarStep = 740;
      wobjDBParm = new Parameter( "@TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPODOCU.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPODOCU)) );
      
      //
      wvarStep = 750;
      
      
      //
      wvarStep = 760;
      
      
      //
      wvarStep = 770;
      
      
      //
      wvarStep = 780;
      wobjDBParm = new Parameter( "@NACIMANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NACIMANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NACIMANN)) );
      
      //
      wvarStep = 790;
      wobjDBParm = new Parameter( "@NACIMMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NACIMMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NACIMMES)) );
      
      //
      wvarStep = 800;
      wobjDBParm = new Parameter( "@NACIMDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NACIMDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NACIMDIA)) );
      
      //
      wvarStep = 810;
      
      
      //
      wvarStep = 820;
      
      
      //
      wvarStep = 830;
      
      
      //
      wvarStep = 840;
      
      
      //
      wvarStep = 850;
      
      
      //
      wvarStep = 860;
      wobjDBParm = new Parameter( "@EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTANN)) );
      
      //
      wvarStep = 870;
      wobjDBParm = new Parameter( "@EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTMES)) );
      
      //
      wvarStep = 880;
      wobjDBParm = new Parameter( "@EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTDIA)) );
      
      //
      wvarStep = 890;
      
      
      //
      wvarStep = 900;
      
      
      //
      wvarStep = 910;
      
      
      //
      wvarStep = 920;
      
      
      //
      wvarStep = 930;
      
      
      //
      wvarStep = 940;
      
      
      //
      wvarStep = 950;
      
      
      //
      wvarStep = 960;
      
      
      //
      wvarStep = 970;
      
      
      //
      wvarStep = 980;
      
      
      //
      wvarStep = 990;
      
      
      //
      wvarStep = 1000;
      
      
      //
      wvarStep = 1010;
      
      
      //
      wvarStep = 1020;
      
      
      //
      wvarStep = 1030;
      
      
      //
      wvarStep = 1040;
      
      
      //
      wvarStep = 1050;
      
      
      //
      wvarStep = 1060;
      
      
      //
      wvarStep = 1070;
      
      
      //
      wvarStep = 1080;
      
      
      //
      wvarStep = 1090;
      
      
      //
      wvarStep = 1100;
      
      
      //
      wvarStep = 1120;
      
      
      //
      wvarStep = 1130;
      wobjDBParm = new Parameter( "@DOMICCPO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_DOMICCPO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_DOMICCPO)) );
      
      //
      wvarStep = 1140;
      wobjDBParm = new Parameter( "@PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICOD)) );
      
      //
      wvarStep = 1150;
      
      
      //
      wvarStep = 1160;
      
      
      //
      wvarStep = 1170;
      
      
      //
      wvarStep = 1180;
      
      
      //
      wvarStep = 1190;
      
      
      //
      wvarStep = 1200;
      
      
      //
      wvarStep = 1210;
      
      
      //
      wvarStep = 1220;
      
      
      //
      wvarStep = 1230;
      
      
      //
      wvarStep = 1240;
      
      
      //
      wvarStep = 1250;
      wobjDBParm = new Parameter( "@DOMICCPO_CO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_DOMICCPO_CO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_DOMICCPO_CO)) );
      
      //
      wvarStep = 1260;
      wobjDBParm = new Parameter( "@PROVICOD_CO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD_CO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICOD_CO)) );
      
      //
      wvarStep = 1270;
      
      
      //
      wvarStep = 1280;
      
      
      //
      wvarStep = 1290;
      
      
      //
      wvarStep = 1300;
      
      
      //
      wvarStep = 1310;
      
      
      //
      wvarStep = 1320;
      
      
      //
      wvarStep = 1330;
      
      
      //
      wvarStep = 1340;
      
      
      //
      wvarStep = 1350;
      
      
      //
      wvarStep = 1360;
      
      
      //
      wvarStep = 1370;
      wobjDBParm = new Parameter( "@VENCIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VENCIANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VENCIANN)) );
      
      //
      wvarStep = 1380;
      wobjDBParm = new Parameter( "@VENCIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VENCIMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VENCIMES)) );
      
      //
      wvarStep = 1390;
      wobjDBParm = new Parameter( "@VENCIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VENCIDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VENCIDIA)) );
      
      //
      wvarStep = 1400;
      
      
      //
      wvarStep = 1410;
      wobjDBParm = new Parameter( "@P_EMISIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_EMISIANN)) );
      
      //
      wvarStep = 1420;
      wobjDBParm = new Parameter( "@P_EMISIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_EMISIMES)) );
      
      //
      wvarStep = 1430;
      wobjDBParm = new Parameter( "@P_EMISIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_EMISIDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_EMISIDIA)) );
      
      //
      wvarStep = 1440;
      wobjDBParm = new Parameter( "@P_EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTANN)) );
      
      //
      wvarStep = 1450;
      wobjDBParm = new Parameter( "@P_EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTMES)) );
      
      //
      wvarStep = 1460;
      wobjDBParm = new Parameter( "@P_EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EFECTDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EFECTDIA)) );
      
      //
      wvarStep = 1470;
      
      
      //
      wvarStep = 1480;
      
      
      //
      wvarStep = 1490;
      
      
      //
      wvarStep = 1500;
      
      
      //
      wvarStep = 1510;
      wobjDBParm = new Parameter( "@P_TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPODOCU.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPODOCU)) );
      
      //
      wvarStep = 1520;
      
      
      //
      wvarStep = 1530;
      
      
      //
      wvarStep = 1540;
      
      
      //
      wvarStep = 1550;
      wobjDBParm = new Parameter( "@P_COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_COBROCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_COBROCOD)) );
      
      //
      wvarStep = 1560;
      
      
      //
      wvarStep = 1570;
      
      
      //
      wvarStep = 1580;
      wobjDBParm = new Parameter( "@P_TIVIVCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_TIVIVCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_TIVIVCOD)) );
      
      //
      wvarStep = 1590;
      
      
      //
      wvarStep = 1600;
      wobjDBParm = new Parameter( "@P_USOTIPOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_USOTIPOS.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_USOTIPOS)) );
      
      //
      wvarStep = 1610;
      
      
      //
      wvarStep = 1620;
      
      
      //
      wvarStep = 1630;
      wobjDBParm = new Parameter( "@P_ALARMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_ALARMTIP.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_ALARMTIP)) );
      
      //
      wvarStep = 1640;
      wobjDBParm = new Parameter( "@P_GUARDTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_GUARDTIP.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_GUARDTIP)) );
      
      //
      wvarStep = 1650;
      
      
      //
      wvarStep = 1660;
      
      
      //
      wvarStep = 1670;
      
      
      //
      wvarStep = 1680;
      
      
      //
      wvarStep = 1690;
      wobjDBParm = new Parameter( "@P_PLANNCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_PLANNCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_PLANNCOD)) );
      
      //
      wvarStep = 1700;
      
      
      //
      wvarStep = 1710;
      
      
      //
      wvarStep = 1720;
      wobjDBParm = new Parameter( "@P_ZONA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_P_ZONA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_P_ZONA)) );
      
      //
      wvarStep = 1730;
      
      
      //
      wvarStep = 1740;
      
      
      //
      wvarStep = 1750;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Campa ) ;
      //
      for( wvarcounter = 1; wvarcounter <= 10; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
          wobjDBParm = new Parameter( "@P_CAMPACOD_" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_CampaCod )  ) ) );
          
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          wobjDBParm = new Parameter( "@P_CAMPACOD_" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "" ) );
          
        }
      }
      //
      wvarStep = 1760;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Cober ) ;
      //
      // AM se cambio de 20 a 30
      for( wvarcounter = 1; wvarcounter <= 30; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
          wobjDBParm = new Parameter( "@P_COBERCOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_COBERCOD )  ) ) );
          
          //
          
          
          //
          wobjDBParm = new Parameter( "@P_CAPITASG" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_CAPITASG )  ) ) );
          
          //
          
          
          //
          wobjDBParm = new Parameter( "@P_CONTRMOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_CONTRMOD )  ) ) );
          
          //
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          
          
          //
          
          
          //
          
          
          //
          
          
          //
          
          
        }
      }
      //
      wvarStep = 1770;
      
      
      //
      wvarStep = 1780;
      
      
      //
      wvarStep = 1790;
      
      
      //
      wvarStep = 1800;
      
      
      //
      wvarStep = 1810;
      
      
      //
      wvarStep = 1820;
      
      
      //
      wvarStep = 1830;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Elect ) ;
      //
      for( wvarcounter = 1; wvarcounter <= 10; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
          wobjDBParm = new Parameter( "@P_TIPO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_Tipo )  ) ) ) );
          
          //
          wobjDBParm = new Parameter( "@P_MARCA" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_Marca )  ) ) ) );
          
          //
          wobjDBParm = new Parameter( "@P_MODELO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_Modelo )  ) ) ) );
          
          //
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          wobjDBParm = new Parameter( "@P_TIPO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( "" ) );
          
          //
          wobjDBParm = new Parameter( "@P_MARCA" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( "" ) );
          
          //
          wobjDBParm = new Parameter( "@P_MODELO" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( "" ) );
          
          //
        }
      }
      //
      wvarStep = 1840;
      
      
      //
      //Como no vienen asegurados, guardo todos estos datos en blanco
      for( wvarcounter = 1; wvarcounter <= 10; wvarcounter++ )
      {
        
        
        //
        wobjDBParm = new Parameter( "@P_NOMBREAS" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 49, new Variant( "" ) );
        
      }
      //
      wvarStep = 1850;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      wobjDBCnn.close();

      pvarRes.set( "<Response>" + "<REQUESTID>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) )  ) + "</REQUESTID>" + "<CERTISEC>" + wobjDBCmd.getParameters().getParameter("@CERTISEC").getValue() + "</CERTISEC>" + "</Response>" );

      fncPutAll = true;

      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = null;
      wobjClass = (HSBCInterfaces.IAction) null;
      return fncPutAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );


        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncPutAll = false;
        /*TBD mobjCOM_Context.SetComplete() ;*/

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutAll;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
