package com.qbe.services.interWSBrok.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.TransformerException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.exutils.ExceptionUtils;
import com.qbe.services.lbavirtualsqlgenerico.OVLBASQLGen;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVGetDispRastreo;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVValidaCuentas;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVValidaIIBB;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVValidaNumDoc;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

import diamondedge.util.DateTime;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import com.qbe.vbcompat.format.VBFixesUtil;

/**
 * 
 * Componente interno, no se publica como WebService a los brokers
 * 
 * TODO Especificar interface
 * 
 * TODO Además de la validación, integra y emite. Esto último debería ir en otro lugar
 * 
 * @author ramiro
 *
 */
public class GetValidacionSolAU extends BaseOSBClient implements VBObjectClass {

	protected static Logger logger = Logger.getLogger(GetValidacionSolAU.class.getName());

	static final String mcteClassName = "LBA_InterWSBrok.GetValidacionSolAU";
	static final String mcteStoreProc = "SPSNCV_BRO_VALIDA_SOL_SCO";
	static final String mcteStoreProcSelect = "SPSNCV_BRO_SELECT_OPER_ESP";
	static final String mcteArchivoAUSSOL_XML = "LBA_VALIDACION_SOL_AU.xml";
	/**
	 * Parametros XML de Entrada
	 */
	static final String mcteParam_Cliensec = "CERTISEC";
	static final String mcteParam_NacimAnn = "NACIMANN";
	static final String mcteParam_NacimMes = "NACIMMES";
	static final String mcteParam_NacimDia = "NACIMDIA";
	static final String mcteParam_Sexo = "SEXO";
	static final String mcteParam_Estado = "ESTADO";
	/**
	 * jc 09/2010 en la cotización tiene otro nombre el tag Const
	 * mcteParam_CLIENIVA As String = "CLIENIVA"
	 */
	static final String mcteParam_CLIENIVA = "IVA";
	static final String mcteParam_ModeAutCod = "MODEAUTCOD";
	static final String mcteParam_KMsrngCod = "KMSRNGCOD";
	static final String mcteParam_EfectAnn = "EFECTANN";
	static final String mcteParam_SiGarage = "SIGARAGE";
	static final String mcteParam_SumAseg = "SUMAASEG";
	static final String mcteParam_Siniestros = "SINIESTROS";
	static final String mcteParam_Gas = "GAS";
	static final String mcteParam_Provi = "PROVI";
	static final String mcteParam_LocalidadCod = "LOCALIDADCOD";
	/**
	 * Revisar
	 */
	static final String mcteParam_CampaCod = "CAMPACOD";
	/**
	 * Revisar
	 */
	static final String mcteParam_DatosPlan = "DATOSPLAN";
	static final String mcteParam_ClubLBA = "CLUBLBA";
	/**
	 * Revisar
	 */
	static final String mcteParam_Portal = "PORTAL";
	/**
	 * jc 09/2010 en la cotización tiene otro nombre el tag Const
	 * mcteParam_PRODUCTOR As String = "PRODUCTOR"
	 */
	static final String mcteParam_PRODUCTOR = "AGECOD";
	static final String mcteParam_Permite_GNC = "PERMITE_GNC";
	static final String mcteParam_VEHDES = "VEHDES";
	static final String mcteParam_CODZONA = "CODZONA";
	static final String mcteParam_CODINST = "CODINST";
	static final String mcteParam_NRO_OPERACION_BROKER = "REQUESTID";
	static final String mcteParam_CODPROVCO = "PROVI";
	static final String mcteParam_CODPOSTCO = "LOCALIDADCOD";
	static final String mcteParam_COBROCOD = "COBROCOD";
	static final String mcteParam_COBROTIP = "COBROTIP";
	static final String mcteParam_CUENNUME = "CUENNUME";
	static final String mcteParam_SUMAASEG = "SUMAASEG";
	static final String mcteParam_COLOR = "VEHCLRCOD";
	static final String mcteParam_DOCUMTIP_TIT = "TIPODOCU";
	static final String mcteParam_PLANNCOD = "PLANNCOD";
	static final String mcteParam_FRANQCOD = "FRANQCOD";
	static final String mcteParam_INSPECCION = "INSPECOD";
	static final String mcteParam_RASTREO = "RASTREO";
	static final String mcteParam_CODIGO_ALARMA = "ALARMCOD";
	static final String mcteParam_CATEGO = "AUCATCOD";
	/**
	 * Accesorios
	 */
	static final String mcteNodos_Accesorios = "//Request/ACCESORIOS/ACCESORIO";
	static final String mcteParam_PrecioAcc = "PRECIOACC";
	static final String mcteParam_DescripcionAcc = "DESCRIPCIONACC";
	static final String mcteParam_CodigoAcc = "CODIGOACC";
	/**
	 * Hijos
	 */
	static final String mcteNodos_Hijos = "//Request/HIJOS/HIJO";
	static final String mcteParam_SexoHijo = "SEXOHIJO";
	static final String mcteParam_EstadoHijo = "ESTADOHIJO";
	static final String mcteParam_NacimHijo = "NACIMHIJO";
	static final String mcteParam_NombreHijo = "NOMBREHIJO";
	static final String mcteParam_ApellidoHijo = "APELLIDOHIJO";
	static final String mcteParam_EdadHijo = "EDADHIJO";
	static final String mcteParam_TipoDocuHijo = "TIPODOCUHIJO";
	static final String mcteParam_NroDocuHijo = "NRODOCUHIJO";
	/**
	 * jc 8/2010 agregado xml
	 */
	static final String mcteParam_DESTRUCCION_80 = "DESTRUCCION_80";
	static final String mcteParam_Luneta = "LUNETA";
	static final String mcteParam_ClubEco = "CLUBECO";
	static final String mcteParam_Robocont = "ROBOCONT";
	static final String mcteParam_Granizo = "GRANIZO";
	static final String mcteParam_INSTALADP = "INSTALADP";
	static final String mcteParam_POSEEDISP = "POSEEDISP";
	/**
	 * JC 2010-11 ch.r.
	 */
	static final String mcteParam_CLIENTIP = "CLIENTIP";
	/**
	 * JC 2010-11 ch.r.
	 */
	static final String mcteParam_IBB = "IBB";
	/**
	 * JC 2010-11 ch.r.
	 */
	static final String mcteParam_CUITNUME = "CUITNUME";
	/**
	 * JC 2010-11 ch.r.
	 */
	static final String mcteParam_NROIBB = "NROIBB";
	/**
	 * JC 2010-11 ch.r.
	 */
	static final String mcteParam_RAZONSOC = "RAZONSOC";
	/**
	 * AM
	 */
	static final String mcteParam_ACREPREN = "//ACRE-PREN";
	static final String mcteParam_NUMEDOCUACRE = "//NUMEDOCU-ACRE";
	static final String mcteParam_TIPODOCUACRE = "//TIPODOCU-ACRE";
	static final String mcteParam_APELLIDOACRE = "//APELLIDO-ACRE";
	static final String mcteParam_NOMBRESACRE = "//NOMBRES-ACRE";
	static final String mcteParam_DOMICDOMACRE = "//DOMICDOM-ACRE";
	static final String mcteParam_DOMICDNUACRE = "//DOMICDNU-ACRE";
	static final String mcteParam_DOMICPISACRE = "//DOMICPIS-ACRE";
	static final String mcteParam_DOMICPTAACRE = "//DOMICPTA-ACRE";
	static final String mcteParam_DOMICPOBACRE = "//DOMICPOB-ACRE";
	static final String mcteParam_DOMICCPOACRE = "//DOMICCPO-ACRE";
	static final String mcteParam_PROVICODACRE = "//PROVICOD-ACRE";
	static final String mcteParam_PAISSCODACRE = "//PAISSCOD-ACRE";
	static final String mcteParam_EmpresaCodigo = "//CANAL";
	static final String mcteParam_SucursalCodigo = "//CANAL_SUCURSAL";
	static final String mcteParam_LegajoVend = "//LEGAJO_VEND";

	public int IAction_Execute(String pvarRequest, StringHolder pvarResponse, String pvarContextInfo) {
		int IAction_Execute = 0;
		Variant wvarCodErr = new Variant();
		Variant wvarMensaje = new Variant();
		StringHolder pvarRes = new StringHolder();
		try {
			pvarRes.set("");
			validarIntegrarYEmitir(pvarRequest, wvarMensaje, wvarCodErr, pvarRes);
			pvarResponse.set("<Response_WS>" + pvarRes.getValue() + "</Response_WS>");
			logger.log(Level.FINEST, "GetValidacionSolAU response es: " + pvarResponse.getValue());
			IAction_Execute = 0;
			return IAction_Execute;
		} catch (Exception _e_) {
			logger.log(Level.SEVERE, "en GetValidacionSolAU", _e_);
			IAction_Execute = 1;

			String causes = ExceptionUtils.getCauses(_e_);
			// Logueo el timestamp para que si el usuario reporta el error
			// podamos usarlo para buscar en los logs
			String resultResponse = "<LBA_WS res_code=\"-1000\" res_msg=\"" + "Error inesperado [" + _e_.getMessage() + "] - TS-" + System.currentTimeMillis() + " CI - " + pvarContextInfo + " \">" + "<![CDATA[" + causes + "]]>" + "</LBA_WS>";
			pvarResponse.set("<Response_WS>" + resultResponse + "</Response_WS>");
		}
		return IAction_Execute;
	}

	private boolean validarIntegrarYEmitir(String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, StringHolder pvarRes) throws IOException, XmlDomExtendedException,
			OSBConnectorException, TransformerException, NumberFormatException, DOMException, SQLException {
		boolean validarIntegrarYEmitir = false;
		XmlDomExtended wobjXMLRequest = null;
		XmlDomExtended wobjXMLRes = null;
		XmlDomExtended wobjXMLCheck = null;
		XmlDomExtended wobjXMLStatusTarjeta = null;
		XmlDomExtended wobjXMLStatusCanal = null;
		org.w3c.dom.NodeList wobjXMLList = null;
		org.w3c.dom.NodeList wobjXMLNodeList = null;
		org.w3c.dom.Node wobjXMLNodeError = null;
		int wvarContChild = 0;
		int wvarContNode = 0;
		int wvarcounter = 0;
		String wvarErrorCodigo = "";
		String wvarTarjeta = "";
		String wvarStatusTarjeta = "";
		String wvarBancoCod = "";
		String wvarExisteSucu = "";
		String mvarRequest = "";
		String wvarResponse = "";
		String mvarWDB_USUARCOD = "";
		String mvarCUENNUME = "";
		java.util.Date wvarFechaActual = DateTime.EmptyDate;
		java.util.Date wvarFechaVigencia = DateTime.EmptyDate;

		wvarCodErr.set(0);

		// Chequeo que los datos esten OK
		// Chequeo que no venga nada en blanco
		wobjXMLRequest = new XmlDomExtended();
		wobjXMLRequest.loadXML(pvarRequest);

		wobjXMLCheck = new XmlDomExtended();
		wobjXMLCheck.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteArchivoAUSSOL_XML));

		// Chequeo nodo padre
		wobjXMLNodeError = wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/VACIOS/CAMPOS");
		// Tomo codigo de error de Vacios
		wvarErrorCodigo = XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/VACIOS/N_ERROR"));
		for (wvarContChild = 0; wvarContChild <= (wobjXMLNodeError.getChildNodes().getLength() - 1); wvarContChild++) {
			if (!(wobjXMLRequest.selectSingleNode(("//" + wobjXMLNodeError.getChildNodes().item(wvarContChild).getNodeName())) == (org.w3c.dom.Node) null)) {
				if (Strings.trim(
						XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + wobjXMLNodeError.getChildNodes().item(wvarContChild).getNodeName()))))
						.equals("")) {
					// error
					wvarCodErr.set(wvarErrorCodigo);
					wvarMensaje.set(XmlDomExtended.getText(wobjXMLNodeError.getChildNodes().item(wvarContChild)));
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			} else {
				// error
				wvarCodErr.set(-1);
				wvarMensaje.set(XmlDomExtended.getText(wobjXMLNodeError.getChildNodes().item(wvarContChild)));
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			}
		}

		// Chequeo Tipo y Longitud de Datos para el XML Padre
		for (wvarContChild = 0; wvarContChild <= (wobjXMLRequest.selectSingleNode("//Request").getChildNodes().getLength() - 1); wvarContChild++) {
			if (!(wobjXMLCheck.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectSingleNode("//Request").getChildNodes()
					.item(wvarContChild).getNodeName())) == null)) {
				// Si el Codigo de Cobro no es tarjeta, no chequea el Largo de
				// Tarjeta y su Vencimiento
				if (!(VBFixesUtil.val(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_COBROCOD)))) == 4)
						&& ((wobjXMLCheck.selectSingleNode(
								"//ERRORES/AUTOSCORING/TIPODEDATO/"
										+ wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName()).getNodeName()
								.equals("CUENNUME"))
								|| (wobjXMLCheck.selectSingleNode(
										("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild)
												.getNodeName())).getNodeName().equals("VENCIANN")) || wobjXMLCheck
								.selectSingleNode(
										("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild)
												.getNodeName())).getNodeName().equals("VENCIMES"))
						|| wobjXMLCheck
								.selectSingleNode(
										("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild)
												.getNodeName())).getNodeName().equals("VENCIDIA")) {
					// unsup GoTo Siguiente
				}
				// Chequeo la longitud y si tiene Largo Fijo
				if (XmlDomExtended.getText(
						wobjXMLCheck.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/"
								+ wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName() + "/LONG_VARIABLE"))).equals(
						"N")) {
					if (!(Strings.len(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild))) == Obj
							.toInt(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/"
									+ wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName() + "/LONGITUD")))))) {
						wvarCodErr.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
								+ wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName() + "/N_ERROR")));
						wvarMensaje.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
								+ wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName() + "/TEXTOERROR")));
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
				} else {
					if (Strings.len(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild))) > Obj
							.toInt(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/"
									+ wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName() + "/LONGITUD"))))) {
						wvarCodErr.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
								+ wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName() + "/N_ERROR")));
						wvarMensaje.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
								+ wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName() + "/TEXTOERROR")));
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
				}

				// Chequeo el Tipo
				if (XmlDomExtended.getText(
						wobjXMLCheck.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/"
								+ wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName() + "/TIPO")))
						.equals("NUMERICO")) {
					if (!(new Variant(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild))).isNumeric())) {
						wvarCodErr.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
								+ wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName() + "/N_ERROR")));
						wvarMensaje.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
								+ wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName() + "/TEXTOERROR")));
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					} else {
						// Chequeo que el dato numerico no sea 0
						// Excluye los nodos COT_NRO, SINIESTROS, RASTREO,
						// INSPECOD Y ALARMCOD
						if ((Obj.toInt(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild))) == 0)
								&& !(((wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName().equals("COT_NRO"))
										|| (wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName().equals("ALARMCOD"))
										|| (wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName().equals("RASTREO"))
										|| (wobjXMLRequest.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName().equals("INSPECOD")) || (wobjXMLRequest
										.selectSingleNode("//Request").getChildNodes().item(wvarContChild).getNodeName().equals("SINIESTROS"))))) {
							wvarCodErr.set(-199);
							wvarMensaje.set("El valor del dato numerico no puede estar en 0");
							pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
							return validarIntegrarYEmitir;
						}
					}
				}
			}
		}

		// Chequeo Tipo y Longitud de Datos para Accesorios
		for (wvarContChild = 0; wvarContChild <= (wobjXMLRequest.selectNodes(mcteNodos_Accesorios).getLength() - 1); wvarContChild++) {
			for (wvarContNode = 0; wvarContNode <= (wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild).getChildNodes().getLength() - 1); wvarContNode++) {
				if (!(wobjXMLCheck.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild)
						.getChildNodes().item(wvarContNode).getNodeName())) == (org.w3c.dom.Node) null)) {
					// Chequeo la longitud y si tiene Largo Fijo
					if (XmlDomExtended.getText(
							wobjXMLCheck
									.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/"
											+ wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild).getChildNodes().item(wvarContNode)
													.getNodeName() + "/LONG_VARIABLE"))).equals("N")) {
						if (!(Strings.len(XmlDomExtended.getText(wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild).getChildNodes()
								.item(wvarContNode))) == Obj
								.toInt(XmlDomExtended.getText(wobjXMLCheck
										.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/"
												+ wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild).getChildNodes().item(wvarContNode)
														.getNodeName() + "/LONGITUD")))))) {
							wvarCodErr.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
									+ wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild).getChildNodes().item(wvarContNode).getNodeName()
									+ "/N_ERROR")));
							wvarMensaje.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
									+ wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild).getChildNodes().item(wvarContNode).getNodeName()
									+ "/TEXTOERROR")));
							pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
							return validarIntegrarYEmitir;
						}
					} else {
						if (Strings.len(XmlDomExtended.getText(wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild).getChildNodes()
								.item(wvarContNode))) > Obj
								.toInt(XmlDomExtended.getText(wobjXMLCheck
										.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/"
												+ wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild).getChildNodes().item(wvarContNode)
														.getNodeName() + "/LONGITUD"))))) {
							wvarCodErr.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
									+ wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild).getChildNodes().item(wvarContNode).getNodeName()
									+ "/N_ERROR")));
							wvarMensaje.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
									+ wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild).getChildNodes().item(wvarContNode).getNodeName()
									+ "/TEXTOERROR")));
							pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
							return validarIntegrarYEmitir;
						}
					}

					// Chequeo el Tipo
					if (XmlDomExtended.getText(
							wobjXMLCheck
									.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/"
											+ wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild).getChildNodes().item(wvarContNode)
													.getNodeName() + "/TIPO"))).equals("NUMERICO")) {
						if (!(new Variant(XmlDomExtended.getText(wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild).getChildNodes()
								.item(wvarContNode))).isNumeric())) {
							wvarCodErr.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
									+ wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild).getChildNodes().item(wvarContNode).getNodeName()
									+ "/N_ERROR")));
							wvarMensaje.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
									+ wobjXMLRequest.selectNodes(mcteNodos_Accesorios).item(wvarContChild).getChildNodes().item(wvarContNode).getNodeName()
									+ "/TEXTOERROR")));
							pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
							return validarIntegrarYEmitir;
						}
					}
				}
			}
		}

		// Chequeo Tipo y Longitud de Datos de Hijos
		for (wvarContChild = 0; wvarContChild <= (wobjXMLRequest.selectNodes(mcteNodos_Hijos).getLength() - 1); wvarContChild++) {
			for (wvarContNode = 0; wvarContNode <= (wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild).getChildNodes().getLength() - 1); wvarContNode++) {
				if (!(wobjXMLCheck.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/" + wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild)
						.getChildNodes().item(wvarContNode).getNodeName())) == (org.w3c.dom.Node) null)) {
					// Chequeo la longitud y si tiene Largo Fijo
					if (XmlDomExtended
							.getText(
									wobjXMLCheck
											.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/"
													+ wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild).getChildNodes().item(wvarContNode)
															.getNodeName() + "/LONG_VARIABLE"))).equals("N")) {
						if (!(Strings.len(XmlDomExtended.getText(wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild).getChildNodes()
								.item(wvarContNode))) == Obj
								.toInt(XmlDomExtended.getText(wobjXMLCheck
										.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/"
												+ wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild).getChildNodes().item(wvarContNode)
														.getNodeName() + "/LONGITUD")))))) {
							wvarCodErr.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
									+ wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild).getChildNodes().item(wvarContNode).getNodeName()
									+ "/N_ERROR")));
							wvarMensaje.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
									+ wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild).getChildNodes().item(wvarContNode).getNodeName()
									+ "/TEXTOERROR")));
							pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
							return validarIntegrarYEmitir;
						}
					} else {
						if (Strings.len(XmlDomExtended.getText(wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild).getChildNodes()
								.item(wvarContNode))) > Obj
								.toInt(XmlDomExtended.getText(wobjXMLCheck
										.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/"
												+ wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild).getChildNodes().item(wvarContNode)
														.getNodeName() + "/LONGITUD"))))) {
							wvarCodErr.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
									+ wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild).getChildNodes().item(wvarContNode).getNodeName()
									+ "/N_ERROR")));
							wvarMensaje.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
									+ wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild).getChildNodes().item(wvarContNode).getNodeName()
									+ "/TEXTOERROR")));
							pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
							return validarIntegrarYEmitir;
						}
					}

					// Chequeo el Tipo
					if (XmlDomExtended
							.getText(
									wobjXMLCheck
											.selectSingleNode(("//ERRORES/AUTOSCORING/TIPODEDATO/"
													+ wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild).getChildNodes().item(wvarContNode)
															.getNodeName() + "/TIPO"))).equals("NUMERICO")) {
						if (!(new Variant(XmlDomExtended.getText(wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild).getChildNodes()
								.item(wvarContNode))).isNumeric())) {
							wvarCodErr.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
									+ wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild).getChildNodes().item(wvarContNode).getNodeName()
									+ "/N_ERROR")));
							wvarMensaje.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/TIPODEDATO/"
									+ wobjXMLRequest.selectNodes(mcteNodos_Hijos).item(wvarContChild).getChildNodes().item(wvarContNode).getNodeName()
									+ "/TEXTOERROR")));
							pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
							return validarIntegrarYEmitir;
						}
					}
				}
			}
		}

		// valido los datos
		wobjXMLNodeError = wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/DATOS/CAMPOS");
		for (wvarContChild = 0; wvarContChild <= (wobjXMLNodeError.getChildNodes().getLength() - 1); wvarContChild++) {
			if (!(wobjXMLRequest.selectSingleNode(("//" + wobjXMLNodeError.getChildNodes().item(wvarContChild).getNodeName())) == (org.w3c.dom.Node) null)) {
				if (XmlDomExtended
						.Node_selectSingleNode(
								wobjXMLNodeError.getChildNodes().item(wvarContChild),
								"VALOR[.='"
										+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//"
												+ wobjXMLNodeError.getChildNodes().item(wvarContChild).getNodeName())) + "']").getChildNodes().getLength() == 0) {
					// error
					wvarCodErr
							.set(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLNodeError.getChildNodes().item(wvarContChild), "N_ERROR")));
					wvarMensaje.set(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLNodeError.getChildNodes().item(wvarContChild),
							"TEXTOERROR")));
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}
		}

		// Chequeo EDAD
		if (!(Funciones.ValidarFechayRango(
				XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_NacimDia)) + "/"
						+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_NacimMes))) + "/"
						+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_NacimAnn))), 17, 86, wvarMensaje))) {
			// error
			wvarCodErr.set(-9);
			pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
			return validarIntegrarYEmitir;
		}

		// Valida la fecha de la Tarjeta (si Viene)
		if (Obj.toInt(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_COBROCOD)))) == 4) {
			if (!((wobjXMLRequest.selectSingleNode("//VENCIDIA") == (org.w3c.dom.Node) null)
					&& (wobjXMLRequest.selectSingleNode("//VENCIMES") == (org.w3c.dom.Node) null) && (wobjXMLRequest.selectSingleNode("//VENCIANN") == (org.w3c.dom.Node) null))) {
				if (!(Funciones.validarFecha(
						XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VENCIDIA")) + "/"
								+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VENCIMES")) + "/"
								+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VENCIANN")), wvarMensaje))) {
					// error
					wvarCodErr.set(-13);
					wvarMensaje.set("Fecha de Tarjeta Invalida");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}

			// Valida el nro. de Tarjeta

			mvarCUENNUME = "0";
			if (!(wobjXMLRequest.selectSingleNode(("//" + mcteParam_CUENNUME)) == (org.w3c.dom.Node) null)) {
				mvarCUENNUME = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_CUENNUME));
			}

			wvarTarjeta = "<Request>" + "<USUARIO/>" + "<COBROTIP>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_COBROTIP)))
					+ "</COBROTIP>" + "<CTANUM>" + mvarCUENNUME + "</CTANUM>" + "<VENANN>"
					+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VENCIANN")) + "</VENANN>" + "<VENMES>"
					+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VENCIMES")) + "</VENMES>" + "</Request>";

			logger.log(Level.FINE, "Llamando localmente a lbaw_OVValidaCuentas");
			lbaw_OVValidaCuentas validaCuentas = new lbaw_OVValidaCuentas();
			StringHolder validaCuentasSH = new StringHolder();
			validaCuentas.IAction_Execute(wvarTarjeta, validaCuentasSH, "");
			wvarStatusTarjeta = validaCuentasSH.getValue();

			
			wobjXMLStatusTarjeta = new XmlDomExtended();
			wobjXMLStatusTarjeta.loadXML(wvarStatusTarjeta);

			if (XmlDomExtended.getText(wobjXMLStatusTarjeta.selectSingleNode("//Response/Estado/@resultado")).equals("false")) {
				wvarCodErr.set(-198);
				wvarMensaje.set(XmlDomExtended.getText(wobjXMLStatusTarjeta.selectSingleNode("//Response/Estado/@mensaje")));
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			}
		}

		// El nro. de CBU(22) se conforma asi en cuennume(0,16) en
		// VENCIMES(16,2) y VENCIANN(18, 4)
		if (Obj.toInt(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_COBROCOD)))) == 5) {
			if (!Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_COBROTIP)))).equals("DB")) {
				wvarCodErr.set(-13);
				wvarMensaje.set("Para CBU campo COBROTIP debe valer DB");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			}
			if (wobjXMLRequest.selectSingleNode(("//" + mcteParam_CUENNUME)) == (org.w3c.dom.Node) null) {
				wvarCodErr.set(-13);
				wvarMensaje.set("Nro. CBU incompleto (cuennume)");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			}
			if (wobjXMLRequest.selectSingleNode("//VENCIMES") == (org.w3c.dom.Node) null) {
				wvarCodErr.set(-13);
				wvarMensaje.set("Nro. CBU incompleto (vencimes)");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			}
			if (wobjXMLRequest.selectSingleNode("//VENCIANN") == (org.w3c.dom.Node) null) {
				wvarCodErr.set(-13);
				wvarMensaje.set("Nro. CBU incompleto (venciann)");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			}
			// Valida el nro. de CBU
			wvarTarjeta = "<Request>" + "<USUARIO/>" + "<COBROTIP>"
					+ Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_COBROTIP)))) + "</COBROTIP>" + "<CTANUM>"
					+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_CUENNUME))) + "</CTANUM>" + "<VENANN>"
					+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VENCIANN")) + "</VENANN>" + "<VENMES>"
					+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VENCIMES")) + "</VENMES>" + "</Request>";

			logger.log(Level.FINE, "Llamando localmente a lbaw_OVValidaCuentas");
			lbaw_OVValidaCuentas validaCuentas = new lbaw_OVValidaCuentas();
			StringHolder validaCuentasSH = new StringHolder();
			validaCuentas.IAction_Execute(wvarTarjeta, validaCuentasSH, "");
			wvarStatusTarjeta = validaCuentasSH.getValue();

			wobjXMLStatusTarjeta = new XmlDomExtended();
			wobjXMLStatusTarjeta.loadXML(wvarStatusTarjeta);
			if (XmlDomExtended.getText(wobjXMLStatusTarjeta.selectSingleNode("//Response/Estado/@resultado")).equals("false")) {
				wvarCodErr.set(-198);
				wvarMensaje.set(XmlDomExtended.getText(wobjXMLStatusTarjeta.selectSingleNode("//Response/Estado/@mensaje")));
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			}
		}

		// Chequeo el Nro. de Patente
		if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//ESCERO"))).equals("S")) {
			if (!(Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PATENNUM"))).equals("A/D"))) {
				wvarCodErr.set(-14);
				wvarMensaje.set("Nro. de Patente Invalida");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
			}
		}
		if ((Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//ESCERO"))).equals("N"))
				|| ((Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//ESCERO"))).equals("S")) && !((Strings
						.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PATENNUM"))).equals("A/D"))))) {
			wvarCodErr.set("0");
			wvarMensaje.set("");
			pvarRes.set("");
			for (wvarcounter = 1; wvarcounter <= 3; wvarcounter++) {
				if (new Variant(Strings.mid(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PATENNUM")), wvarcounter, 1)).isNumeric()) {
					wvarCodErr.set(-14);
					wvarMensaje.set("Nro. de Patente Invalida");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				}
			}
			for (wvarcounter = 4; wvarcounter <= 6; wvarcounter++) {
				if (!(new Variant(Strings.mid(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PATENNUM")), wvarcounter, 1)).isNumeric())) {
					wvarCodErr.set(-14);
					wvarMensaje.set("Nro. de Patente Invalida");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				}
			}
		}
		if (wvarCodErr.toInt() != 0) {
			return validarIntegrarYEmitir;
		}

		if (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PLANNCOD")).equals("011")) {

			if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//INSTALADP"))).equals("S")) {
				wvarCodErr.set(-298);
				wvarMensaje.set("No requiere instalar dispositivo de rastreo");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			}
		} else {
			wvarTarjeta = "<Request>" + "<USUARIO/>" + "<MODEAUTCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_ModeAutCod)))
					+ "</MODEAUTCOD>" + "<SUMASEG>"
					+ String.valueOf((Obj.toInt(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_SumAseg)))) * 100)) + "</SUMASEG>"
					+ "<PROVI>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_Provi))) + "</PROVI>" + "<LOCALIDADCOD>"
					+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_LocalidadCod))) + "</LOCALIDADCOD>"
					+ "<OPCION/><OPERACION/><PRESCOD/><INSTALA/>" + "</Request>";

			logger.log(Level.FINE, "Llamando localmente a lbaw_OVGetDispRastreo");
			lbaw_OVGetDispRastreo dispRastreo = new lbaw_OVGetDispRastreo();
			StringHolder dispRastreoSH = new StringHolder();
			dispRastreo.IAction_Execute(wvarTarjeta, dispRastreoSH, "");
			wvarStatusTarjeta = dispRastreoSH.getValue();
			
			wobjXMLStatusTarjeta = new XmlDomExtended();
			wobjXMLStatusTarjeta.loadXML(wvarStatusTarjeta);

			if (XmlDomExtended.getText(wobjXMLStatusTarjeta.selectSingleNode("//Response/Estado/@resultado")).equals("false")) {
				wvarCodErr.set(-298);
				wvarMensaje.set("No se puede determinar si requiere dispositivo de rastreo");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			} else {
				if (XmlDomExtended.getText(wobjXMLStatusTarjeta.selectSingleNode("//REQ")).equals("S")) {
					if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//INSTALADP"))).equals("N")) {
						wvarCodErr.set(-298);
						wvarMensaje.set("Se requiere instalar dispositivo de rastreo");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					} else {
						if (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//RASTREO")).equals("0")) {
							wvarCodErr.set(-298);
							wvarMensaje.set("Falta informar dispositivo de rastreo");
							pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
							return validarIntegrarYEmitir;
						}
					}
				} else {
					if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//INSTALADP"))).equals("S")) {
						wvarCodErr.set(-298);
						wvarMensaje.set("NO requiere instalar dispositivo de rastreo");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
				}
			}
		}

		// Valida la fecha de Vigencia
		if (!(Funciones.validarFecha(
				XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VIGENDIA")) + "/"
						+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VIGENMES")) + "/"
						+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VIGENANN")), wvarMensaje))) {
			// error
			wvarCodErr.set(-15);
			wvarMensaje.set("Fecha de Vigencia Invalida");
			pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
			return validarIntegrarYEmitir;

		}

		// Chequeo que la fecha de Vigencia sea mayor a hoy
		wvarFechaActual = DateTime.dateSerial(DateTime.year(DateTime.now()), DateTime.month(DateTime.now()), DateTime.day(DateTime.now()));
		wvarFechaVigencia = DateTime.dateSerial(Obj.toInt(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VIGENANN"))),
				Obj.toInt(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VIGENMES"))),
				Obj.toInt(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VIGENDIA"))));

		if (wvarFechaVigencia.compareTo(wvarFechaActual) < 0) {
			wvarCodErr.set(-197);
			wvarMensaje.set("Fecha de Vigencia anterior a la fecha actual");
			pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
			return validarIntegrarYEmitir;
		}
		// Chequeo que la fecha no sea mayor a 30 días a partir de hoy
		if (wvarFechaVigencia.compareTo(DateTime.add(wvarFechaActual, 30)) > 0) {
			wvarCodErr.set(-196);
			wvarMensaje.set("Fecha de Vigencia posterior a 30 dias de la fecha actual");
			pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
			return validarIntegrarYEmitir;

		}
		// Chequeo los hijos
		wobjXMLList = wobjXMLRequest.selectNodes(mcteNodos_Hijos);
		for (wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++) {
			if (XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_SexoHijo) == (org.w3c.dom.Node) null) {
				wvarCodErr.set(-1);
				wvarMensaje.set("Campo de Sexo de Hijo en blanco");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			} else {
				if (wobjXMLCheck.selectNodes(
						("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo + "/VALOR[.='"
								+ XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_SexoHijo)) + "']"))
						.getLength() == 0) {
					// error
					wvarCodErr.set(XmlDomExtended.getText(wobjXMLCheck
							.selectSingleNode("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo + "/N_ERROR")));
					wvarMensaje.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_SexoHijo
							+ "/TEXTOERROR")));
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}
			if (XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_NacimHijo) == (org.w3c.dom.Node) null) {
				wvarCodErr.set(-1);
				wvarMensaje.set("Campo de Fecha de Nacimiento de Hijo en blanco");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			} else {
				if (!(Funciones
						.ValidarFechayRango(
								Strings.left(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_NacimHijo)),
										2)
										+ "/"
										+ Strings.mid(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode),
												mcteParam_NacimHijo)), 3, 2)
										+ "/"
										+ Strings.right(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode),
												mcteParam_NacimHijo)), 4), 17, 29, wvarMensaje))) {
					// error
					wvarCodErr.set(-10);
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}
			if (XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_EstadoHijo) == (org.w3c.dom.Node) null) {
				wvarCodErr.set(-1);
				wvarMensaje.set("Campo de Estado Civil del Hijo en blanco");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			} else {
				if (wobjXMLCheck.selectNodes(
						("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo + "/VALOR[.='"
								+ XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_EstadoHijo)) + "']"))
						.getLength() == 0) {
					// error
					wvarCodErr.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo
							+ "/N_ERROR")));
					wvarMensaje.set(XmlDomExtended.getText(wobjXMLCheck.selectSingleNode("//ERRORES/AUTOSCORING/DATOS/CAMPOS/" + mcteParam_EstadoHijo
							+ "/TEXTOERROR")));
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}

			if (XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_NombreHijo) == (org.w3c.dom.Node) null) {
				wvarCodErr.set(-1);
				wvarMensaje.set("Campo de Nombre del Hijo en blanco");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			} else {
				if (XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_NombreHijo)).equals("")) {
					// error
					wvarCodErr.set(-1);
					wvarMensaje.set("Campo de Nombre del Hijo en blanco");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}

			if (XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_ApellidoHijo) == (org.w3c.dom.Node) null) {
				wvarCodErr.set(-1);
				wvarMensaje.set("Campo de Apellido del Hijo en blanco");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			} else {
				if (XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_ApellidoHijo)).equals("")) {
					// error
					wvarCodErr.set(-1);
					wvarMensaje.set("Campo de Apellido del Hijo en blanco");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}
			if (XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_EdadHijo) == (org.w3c.dom.Node) null) {
				wvarCodErr.set(-1);
				wvarMensaje.set("Campo de Edad del Hijo en blanco");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			} else {
				if (VBFixesUtil.val(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_EdadHijo))) == 0) {
					wvarCodErr.set(-1);
					wvarMensaje.set("Campo de Edad del Hijo con Valor 0");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}
		}
		wobjXMLList = (org.w3c.dom.NodeList) null;

		// Chequeo los Accesorios
		wobjXMLList = wobjXMLRequest.selectNodes(mcteNodos_Accesorios);
		for (wvarContNode = 0; wvarContNode <= (wobjXMLList.getLength() - 1); wvarContNode++) {
			if (XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_PrecioAcc) == (org.w3c.dom.Node) null) {
				wvarCodErr.set(-1);
				wvarMensaje.set("Campo de Precio de Accesorio en blanco");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			}

			if (XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_CodigoAcc) == (org.w3c.dom.Node) null) {
				wvarCodErr.set(-1);
				wvarMensaje.set("Campo de Codigo de Accesorio en blanco");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			} else {
				if (VBFixesUtil.val(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_CodigoAcc))) == 0) {
					wvarCodErr.set(-1);
					wvarMensaje.set("Campo de Codigo de Accesorio en blanco");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}

			if (XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_DescripcionAcc) == (org.w3c.dom.Node) null) {
				wvarCodErr.set(-1);
				wvarMensaje.set("Campo de Descripcion de Accesorio en blanco");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			} else {
				if (XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarContNode), mcteParam_DescripcionAcc)).equals("")) {
					wvarCodErr.set(-1);
					wvarMensaje.set("Campo de Descripcion de Accesorio en blanco");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}
		}
		wobjXMLList = (org.w3c.dom.NodeList) null;

		// 11/2010 se permiten distintos tipos de iva
		// distino consumidor final valida cuit
		if (!XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_CLIENIVA))).equals("3")) {
			if (wobjXMLRequest.selectSingleNode(("//" + mcteParam_CUITNUME)) == (org.w3c.dom.Node) null) {
				wvarCodErr.set(-398);
				wvarMensaje.set("Debe informar Nro. de CUIT");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			} else {
				wvarTarjeta = "<Request>" + "<USUARIO/> " + "<DOCUMTIP>4</DOCUMTIP> " + "<DOCUMNRO>"
						+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_CUITNUME))) + "</DOCUMNRO> " + "</Request>";

				logger.log(Level.FINE, "Llamando localmente a lbaw_OVValidaNumDoc");
				lbaw_OVValidaNumDoc validaNumDoc = new lbaw_OVValidaNumDoc();
				StringHolder validaNumDocSH = new StringHolder();
				validaNumDoc.IAction_Execute(wvarTarjeta, validaNumDocSH, "");
				wvarStatusTarjeta = validaNumDocSH.getValue();

				wobjXMLStatusTarjeta = new XmlDomExtended();
				wobjXMLStatusTarjeta.loadXML(wvarStatusTarjeta);
				if (XmlDomExtended.getText(wobjXMLStatusTarjeta.selectSingleNode("//Response/Estado/@resultado")).equals("false")) {
					wvarCodErr.set(-398);
					wvarMensaje.set("Nro. de CUIT incorrecto o esta mal el formato");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}
		} else {

			if (!(wobjXMLRequest.selectSingleNode(("//" + mcteParam_CUITNUME)) == (org.w3c.dom.Node) null)) {
				if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_CUITNUME))).equals("0"))) {
					wvarCodErr.set(-398);
					wvarMensaje.set("NO Debe informar Nro. de CUIT");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}
		}

		// 11/2010 se permiten distintos tipos de iva implica tambien ibb
		// distino consumidor final valida nro.ibb
		if (!XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_CLIENIVA))).equals("3")) {
			if ((wobjXMLRequest.selectNodes(("//" + mcteParam_NROIBB)) == (org.w3c.dom.NodeList) null)
					|| (wobjXMLRequest.selectSingleNode(("//" + mcteParam_IBB)) == (org.w3c.dom.Node) null)) {
				wvarCodErr.set(-398);
				wvarMensaje.set("Debe informar Nro. de Ingresos Brutos");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			} else {
				wvarTarjeta = "<Request>" + "<USUARIO/> " + "<IIBBTIP>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_IBB)))
						+ "</IIBBTIP> " + "<IIBBNUM>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_NROIBB))) + "</IIBBNUM> "
						+ "</Request>";

				logger.log(Level.FINE, "Llamando localmente a lbaw_OVValidaIIBB");
				lbaw_OVValidaIIBB validaIIBB = new lbaw_OVValidaIIBB();
				StringHolder validaIIBBSH = new StringHolder();
				validaIIBB.IAction_Execute(wvarTarjeta, validaIIBBSH, "");
				wvarStatusTarjeta = validaIIBBSH.getValue();

				wobjXMLStatusTarjeta = new XmlDomExtended();
				wobjXMLStatusTarjeta.loadXML(wvarStatusTarjeta);
				if (XmlDomExtended.getText(wobjXMLStatusTarjeta.selectSingleNode("//Response/Estado/@resultado")).equals("false")) {
					wvarCodErr.set(-398);
					wvarMensaje.set("Nro. de Ingresos Brutos incorrecto o no corresponde a Tipo de Ingresos Brutos");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}
		} else {
			if (wobjXMLRequest.selectNodes(("//" + mcteParam_NROIBB)).getLength() > 0) {
				if (!(wobjXMLRequest.selectSingleNode(("//" + mcteParam_NROIBB)) == (org.w3c.dom.Node) null)) {
					if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_NROIBB))).equals("0"))) {
						wvarCodErr.set(-398);
						wvarMensaje.set("NO Debe informar Nro. de Ingresos Brutos");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
				}
			}
		}
		// se valida razon social si corresponde x iva
		// distino consumidor final valida cuit
		if (!XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_CLIENIVA))).equals("3")) {
			if (wobjXMLRequest.selectSingleNode(("//" + mcteParam_RAZONSOC)) == (org.w3c.dom.Node) null) {
				wvarCodErr.set(-398);
				wvarMensaje.set("Debe informar Razon Social");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				return validarIntegrarYEmitir;
			}
		} else {
			if (!(wobjXMLRequest.selectSingleNode(("//" + mcteParam_RAZONSOC)) == (org.w3c.dom.Node) null)) {
				if (!Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_RAZONSOC)))).equals("")) {
					wvarCodErr.set(-398);
					wvarMensaje.set("No debe informar Razon Social");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}
		}

		// fin 11/2010
		// Valido campos del Acreedor Prendario
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_ACREPREN) == (org.w3c.dom.Node) null)) {
			if (Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ACREPREN))).equals("S")) {

				if (!((wobjXMLRequest.selectSingleNode(mcteParam_NUMEDOCUACRE) == (org.w3c.dom.Node) null))
						&& !((wobjXMLRequest.selectSingleNode(mcteParam_TIPODOCUACRE) == (org.w3c.dom.Node) null))
						&& !((wobjXMLRequest.selectSingleNode(mcteParam_APELLIDOACRE) == (org.w3c.dom.Node) null))
						&& !((wobjXMLRequest.selectSingleNode(mcteParam_NOMBRESACRE) == (org.w3c.dom.Node) null))
						&& !((wobjXMLRequest.selectSingleNode(mcteParam_DOMICDOMACRE) == (org.w3c.dom.Node) null))
						&& !((wobjXMLRequest.selectSingleNode(mcteParam_DOMICDNUACRE) == (org.w3c.dom.Node) null))
						&& !((wobjXMLRequest.selectSingleNode(mcteParam_DOMICPISACRE) == (org.w3c.dom.Node) null))
						&& !((wobjXMLRequest.selectSingleNode(mcteParam_DOMICPTAACRE) == (org.w3c.dom.Node) null))
						&& !((wobjXMLRequest.selectSingleNode(mcteParam_DOMICPOBACRE) == (org.w3c.dom.Node) null))
						&& !((wobjXMLRequest.selectSingleNode(mcteParam_DOMICCPOACRE) == (org.w3c.dom.Node) null))
						&& !((wobjXMLRequest.selectSingleNode(mcteParam_PROVICODACRE) == (org.w3c.dom.Node) null))
						&& !((wobjXMLRequest.selectSingleNode(mcteParam_PAISSCODACRE) == (org.w3c.dom.Node) null))) {
					if (Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_NUMEDOCUACRE))).equals("")) {
						wvarCodErr.set(-398);
						wvarMensaje.set("Debe informar Numero de Documento del Acreedor Prendario");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
					if (Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_TIPODOCUACRE))).equals("")) {
						wvarCodErr.set(-398);
						wvarMensaje.set("Debe informar Tipo de Documento del Acreedor Prendario");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
					if (Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_APELLIDOACRE))).equals("")) {
						wvarCodErr.set(-398);
						wvarMensaje.set("Debe informar Apellido del Acreedor Prendario");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
					if (Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_NOMBRESACRE))).equals("")) {
						wvarCodErr.set(-398);
						wvarMensaje.set("Debe informar Nombre del Acreedor Prendario");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
					if (Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICDOMACRE))).equals("")) {
						wvarCodErr.set(-398);
						wvarMensaje.set("Debe informar Domicilio del Acreedor Prendario");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
					if (Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICDNUACRE))).equals("")) {
						wvarCodErr.set(-398);
						wvarMensaje.set("Debe informar Domicilio Numero del Acreedor Prendario");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
					if (Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPISACRE))).equals("")) {
						wvarCodErr.set(-398);
						wvarMensaje.set("Debe informar Domicilio Piso del Acreedor Prendario");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
					if (Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPTAACRE))).equals("")) {
						wvarCodErr.set(-398);
						wvarMensaje.set("Debe informar Domicilio Puerta del Acreedor Prendario");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
					if (Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPOBACRE))).equals("")) {
						wvarCodErr.set(-398);
						wvarMensaje.set("Debe informar Domicilio POB del Acreedor Prendario");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
					if (Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICCPOACRE))).equals("")) {
						wvarCodErr.set(-398);
						wvarMensaje.set("Debe informar Numero de Documento del Acreedor Prendario");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
					if (Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PROVICODACRE))).equals("")) {
						wvarCodErr.set(-398);
						wvarMensaje.set("Debe informar Provincia del Acreedor Prendario");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
					if (Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PAISSCODACRE))).equals("")) {
						wvarCodErr.set(-398);
						wvarMensaje.set("Debe informar Pais del Acreedor Prendario");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
				} else {
					wvarCodErr.set(-399);
					wvarMensaje.set("Debe informar todos los datos del Acreedor Prendario");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					return validarIntegrarYEmitir;
				}
			}
		}
		
		
		if (wvarCodErr.toInt() == 0) {
			if (validarEnBase(pvarRequest, wvarMensaje, wvarCodErr, pvarRes)) {
				logger.log(Level.FINEST, "validarEnBase: true");
				wobjXMLRes = new XmlDomExtended();
				wobjXMLRes.loadXML(pvarRes.toString());

				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "RAMOPCOD", ""));
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//RAMOPCOD"), XmlDomExtended.getText(wobjXMLRes.selectSingleNode("//RAMOPCOD")));

				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "POLIZANN", ""));
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//POLIZANN"), XmlDomExtended.getText(wobjXMLRes.selectSingleNode("//POLIZANN")));

				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "POLIZSEC", ""));
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//POLIZSEC"), XmlDomExtended.getText(wobjXMLRes.selectSingleNode("//POLIZSEC")));

				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "CERTIPOL", ""));
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//CERTIPOL"), XmlDomExtended.getText(wobjXMLRes.selectSingleNode("//CERTIPOL")));

				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "CERTIANN", ""));
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//CERTIANN"), XmlDomExtended.getText(wobjXMLRes.selectSingleNode("//CERTIANN")));

				if (wobjXMLRequest.selectSingleNode("//CERTISEC") == (org.w3c.dom.Node) null) {
					wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "CERTISEC", ""));
				}
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//CERTISEC"), XmlDomExtended.getText(wobjXMLRes.selectSingleNode("//CERTISEC")));

				// Se agregan las Coberturas y descripción del Plan para el
				// AIS...
				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "PQTDES", ""));
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//PQTDES"), XmlDomExtended.getText(wobjXMLRes.selectSingleNode("//PLANDES")));

				// Se agrega el AGECLA...
				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "AGECLA", ""));
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//AGECLA"), XmlDomExtended.getText(wobjXMLRes.selectSingleNode("//AGECLA")));

				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "COBERTURAS", ""));

				for (wvarcounter = 1; wvarcounter <= 30; wvarcounter++) {

					if (!(Obj.toInt(XmlDomExtended.getText(wobjXMLRes.selectSingleNode(("//COBERCOD" + wvarcounter)))) == 0)) {

						wobjXMLRequest.selectSingleNode("//Request/COBERTURAS").appendChild(
								wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "COBERTURA", ""));

						wobjXMLRequest.selectNodes("//Request/COBERTURAS/COBERTURA").item(wvarcounter - 1)
								.appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "COBERCOD", ""));
						XmlDomExtended.setText(wobjXMLRequest.selectNodes("//Request/COBERTURAS/COBERTURA/COBERCOD").item(wvarcounter - 1),
								XmlDomExtended.getText(wobjXMLRes.selectSingleNode("//COBERCOD" + wvarcounter)));

						wobjXMLRequest.selectNodes("//Request/COBERTURAS/COBERTURA").item(wvarcounter - 1)
								.appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "COBERORD", ""));
						XmlDomExtended.setText(wobjXMLRequest.selectNodes("//Request/COBERTURAS/COBERTURA/COBERORD").item(wvarcounter - 1),
								XmlDomExtended.getText(wobjXMLRes.selectSingleNode("//COBERORD" + wvarcounter)));

						wobjXMLRequest.selectNodes("//Request/COBERTURAS/COBERTURA").item(wvarcounter - 1)
								.appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "CAPITASG", ""));
						XmlDomExtended.setText(wobjXMLRequest.selectNodes("//Request/COBERTURAS/COBERTURA/CAPITASG").item(wvarcounter - 1),
								XmlDomExtended.getText(wobjXMLRes.selectSingleNode("//CAPITASG" + wvarcounter)));

						wobjXMLRequest.selectNodes("//Request/COBERTURAS/COBERTURA").item(wvarcounter - 1)
								.appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "CAPITIMP", ""));
						XmlDomExtended.setText(wobjXMLRequest.selectNodes("//Request/COBERTURAS/COBERTURA/CAPITIMP").item(wvarcounter - 1),
								XmlDomExtended.getText(wobjXMLRes.selectSingleNode("//CAPITIMP" + wvarcounter)));

					}
				}
				// AM Valido CANAL en caso de que se informe
				if (!(wobjXMLRequest.selectSingleNode("//CANAL") == (org.w3c.dom.Node) null)) {

					mvarWDB_USUARCOD = "";
					if (!(wobjXMLRes.selectSingleNode("//USUARCOD") == (org.w3c.dom.Node) null)) {
						mvarWDB_USUARCOD = XmlDomExtended.getText(wobjXMLRes.selectSingleNode("//USUARCOD"));
					}
					//
					// Si se informa el CANAL, valido los otros dos campos
					if (wobjXMLRequest.selectSingleNode("//CANAL_SUCURSAL") == (org.w3c.dom.Node) null) {
						wvarCodErr.set(-14);
						wvarMensaje.set("Debe informar Canal Sucursal");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}
					if (wobjXMLRequest.selectSingleNode("//LEGAJO_VEND") == (org.w3c.dom.Node) null) {
						wvarCodErr.set(-14);
						wvarMensaje.set("Debe informar Legajo Vendedor");
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					}

					// AM Valido el CANAL Msg1000
					mvarRequest = "<Request><DEFINICION>1000_PerfilUsuario.xml</DEFINICION>";
					mvarRequest = mvarRequest + "<AplicarXSL>1000_PerfilUsuario.xsl</AplicarXSL>";
					mvarRequest = mvarRequest + "<USUARIO>" + Strings.toUpperCase(mvarWDB_USUARCOD) + "</USUARIO></Request>";

					wvarResponse = Utils.getConsultaMQGestion(mvarRequest);

					wobjXMLStatusCanal = new XmlDomExtended();
					wobjXMLStatusCanal.loadXML(wvarResponse);
					if (XmlDomExtended.getText(wobjXMLStatusCanal.selectSingleNode("//Response/Estado/@resultado")).equals("false")) {
						wvarCodErr.set(-198);
						wvarMensaje.set(XmlDomExtended.getText(wobjXMLStatusCanal.selectSingleNode("//Response/Estado/@mensaje")));
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					} else {
						wvarBancoCod = "0";
						if (!(wobjXMLStatusCanal.selectSingleNode("//BANCOCOD") == (org.w3c.dom.Node) null)) {
							wvarBancoCod = XmlDomExtended.getText(wobjXMLStatusCanal.selectSingleNode("//BANCOCOD"));
						}
						if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_EmpresaCodigo)).equals(Integer.valueOf(wvarBancoCod).toString()))) {
							wvarCodErr.set(-399);
							wvarMensaje.set("Nro. de Canal incorrecto para el Broker");
							pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
							return validarIntegrarYEmitir;
						}

					}
					// Fin valido CANAL
					// AM Valido el CANAL_SUCURSAL Msg1018
					mvarRequest = "<Request><DEFINICION>1018_ListadoSucursalesxBanco.xml</DEFINICION>";
					mvarRequest = mvarRequest + "<BANCOCOD>" + wvarBancoCod + "</BANCOCOD></Request>";

			      wvarResponse = Utils.getConsultaMQ(mvarRequest);

					wobjXMLStatusCanal = new XmlDomExtended();
					wobjXMLStatusCanal.loadXML(wvarResponse);
					if (XmlDomExtended.getText(wobjXMLStatusCanal.selectSingleNode("//Response/Estado/@resultado")).equals("false")) {
						wvarCodErr.set(-198);
						wvarMensaje.set(XmlDomExtended.getText(wobjXMLStatusCanal.selectSingleNode("//Response/Estado/@mensaje")));
						pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
						return validarIntegrarYEmitir;
					} else {
						wobjXMLNodeList = wobjXMLStatusCanal.selectNodes("//Response/CAMPOS/SUCURSALES/SUCU");
						wvarExisteSucu = "N";

						for (wvarcounter = 0; wvarcounter <= (wobjXMLNodeList.getLength() - 1); wvarcounter++) {
							if (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_SucursalCodigo)).equals(
									XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLNodeList.item(wvarcounter), "CODISUCU")))) {
								wvarExisteSucu = "S";
							}
						}
						if (wvarExisteSucu.equals("N")) {
							wvarCodErr.set(-399);
							wvarMensaje.set("Nro. de Sucursal invalido para el Broker");
							pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
							return validarIntegrarYEmitir;
						}
					}
					// Fin valido CANAL_SUCURSAL
				}

				// Agrego Estructura de la póliza
				wobjXMLRes = new XmlDomExtended();
				wobjXMLRes.loadXML(pvarRes.toString());

				logger.log(Level.FINEST, "Por cargar pvarRes = " + pvarRes);
				wobjXMLRes.selectSingleNode("//Request").appendChild(wobjXMLRes.createNode(org.w3c.dom.Node.ELEMENT_NODE, "INTEGRACION", ""));
				wobjXMLRes.selectSingleNode("//Request/INTEGRACION").appendChild(wobjXMLRes.createNode(org.w3c.dom.Node.ELEMENT_NODE, "RESULTADO", ""));
				wobjXMLRes.selectSingleNode("//Request/INTEGRACION").appendChild(wobjXMLRes.createNode(org.w3c.dom.Node.ELEMENT_NODE, "MENSAJE", ""));

				StringHolder respuestaIntegracionSH = new StringHolder();
				StringHolder respuestaEmisionSH = new StringHolder();
				String requestAIntegrar = XmlDomExtended.marshal(wobjXMLRequest.getDocument().getDocumentElement());
				if (!(integrarYEmitirPropuesta(requestAIntegrar, respuestaIntegracionSH, respuestaEmisionSH, wvarCodErr))) {

					int codErr = wvarCodErr.toInt();
					String errorMessage = "Error genérico al realizar la integración y emisión";
					switch (codErr) {
					case -402:
						errorMessage = "Error en la emisión [ " + respuestaEmisionSH.getValue() + " ]";
						break;
					case -403:
						errorMessage = "Error en la integración";
						break;
					case -401:
						errorMessage = "Error en la integración - error en la ejecución";
						break;
					default:
						break;
					}
					
					wvarMensaje.set(errorMessage);
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");

					logger.log(Level.WARNING, String.format("Error en integrarYEmitirPropuesta desde %s. Request: %s , respuesta integracion: %s, respuesta Emision: %s", this.getClass().getName(),
							requestAIntegrar, respuestaIntegracionSH.getValue(), respuestaEmisionSH.getValue()));
					validarIntegrarYEmitir = false;

				} else {
					//Meto el tag EMISION de respuestaEmisionSH en el request
					XmlDomExtended resultadoEmisionXml = new XmlDomExtended();
					resultadoEmisionXml.loadXML(respuestaEmisionSH.getValue());
					logger.log(Level.FINEST, "resultadoEmisionXml cargado desde: " + respuestaEmisionSH.getValue());
					Node nodeIntegracion = wobjXMLRes.selectSingleNode("//Request/INTEGRACION");
					Node nodeEmision = resultadoEmisionXml.selectSingleNode("//EMISION");
					XmlDomExtended.nodeImportAsChildNode(nodeIntegracion, nodeEmision);
					pvarRes.set(XmlDomExtended.marshal(wobjXMLRes.getDocument().getDocumentElement()));
					validarIntegrarYEmitir = true;
				}
			} else {
				validarIntegrarYEmitir = false;
			}
		} else {
			validarIntegrarYEmitir = false;
		}

		//Chequeo formato respuestas, es un assert para detectar casos no contemplados
		if ( !validarIntegrarYEmitir && !pvarRes.getValue().startsWith("<LBA_WS res_code")) {
			throw new ComponentExecutionException("GetValidacionSolAU fncGetAll es false y no devuelve una respuesta de error con formato válido. Devuelve: " + pvarRes.getValue());
		}
		return validarIntegrarYEmitir;
	}

	/**
	 * Arma el request y realiza la integración ( lbaw_AisPutSolicAUS 1540 ) y 
	 * emisión ( lbaw_EmitirPropuesta 1526 ) via OSB
	 * 
	 * TODO Se podría emprolijar este engendro mutante de armado de XMLs a mano concatenando Strings
	 * está todo cableado. Ver que omite tags y los anota con comentarios
	 * "NO SE USA"
	 * 
	 * 
	 * @param pvarRequest
	 * @param respuestaIntegracionSH
	 * @param respuestaIntegracionSH
	 * @param wvarCodErr
	 * @return
	 * @throws XmlDomExtendedException
	 * @throws TransformerException
	 * @throws OSBConnectorException
	 * @throws MalformedURLException
	 */
	private boolean integrarYEmitirPropuesta(String pvarRequest, StringHolder respuestaIntegracionSH, StringHolder respuestaEmisionSH, Variant wvarCodErr) throws XmlDomExtendedException,
			TransformerException, MalformedURLException, OSBConnectorException {
		XmlDomExtended wobjXMLRequest = new XmlDomExtended();
		wobjXMLRequest.loadXML(pvarRequest);

		String requestIntegracion = createIntegrationRequest(wobjXMLRequest);

		logger.log(Level.FINE, String.format("GetValidacionSolAU por enviar a lbaw_AisPutSolicAUS: [%s]", requestIntegracion));
		String respuestaIntegracion = getOsbConnector().executeRequest("lbaw_AisPutSolicAUS", requestIntegracion);
		logger.log(Level.FINE, String.format("GetValidacionSolAU respuesta de lbaw_AisPutSolicAUS: [%s]", respuestaIntegracion));
		respuestaIntegracionSH.set(respuestaIntegracion);
		
		XmlDomExtended putSolicAusResponseXML = new XmlDomExtended();
		putSolicAusResponseXML.loadXML(respuestaIntegracion);

		if (!(putSolicAusResponseXML.selectSingleNode("//Response/Estado/@resultado") == null)) {
			if (XmlDomExtended.getText(putSolicAusResponseXML.selectSingleNode("//Response/Estado/@resultado")).equals("true")) {

				// Emisión
				String mvarRAMOPCOD = XmlDomExtended.getText(putSolicAusResponseXML.selectSingleNode("//RESULTADO/RAMOPCOD"));
				String mvarPOLIZANN = XmlDomExtended.getText(putSolicAusResponseXML.selectSingleNode("//RESULTADO/POLIZANN"));
				String mvarPOLIZSEC = XmlDomExtended.getText(putSolicAusResponseXML.selectSingleNode("//RESULTADO/POLIZSEC"));
				String mvarCERTIPOL = XmlDomExtended.getText(putSolicAusResponseXML.selectSingleNode("//RESULTADO/CERTIPOL"));
				String mvarCERTIANN = XmlDomExtended.getText(putSolicAusResponseXML.selectSingleNode("//RESULTADO/CERTIANN"));
				String mvarCertiSec = XmlDomExtended.getText(putSolicAusResponseXML.selectSingleNode("//RESULTADO/CERTISEC"));
				String mvarSUPLENUM = XmlDomExtended.getText(putSolicAusResponseXML.selectSingleNode("//RESULTADO/SUPLENUM"));
				String usuarcod = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//USUARCOD"));

				String mvarProdok = emitirPropuesta(respuestaEmisionSH, mvarRAMOPCOD, mvarPOLIZANN, mvarPOLIZSEC, mvarCERTIPOL, mvarCERTIANN, mvarCertiSec, mvarSUPLENUM,
						usuarcod);
				if (mvarProdok.equals("ER")) {
					wvarCodErr.set(-402); //Error en la emisión
					return false;
				} else {
					wvarCodErr.set(0); //Alles gut
					return true;
				}
			} else {
				wvarCodErr.set(-403); //resultado false en la integración
				return false;
			}
		} else {
			wvarCodErr.set(-401); // error genérico en la integración, ni vino el tag Estado/@resultado
			return false;
		}
	}

	protected String createIntegrationRequest(XmlDomExtended wobjXMLRequest) throws XmlDomExtendedException, TransformerException {
		XmlDomExtended wvarXMLACCESORIOS;
		XmlDomExtended wvarXMLHijos;
		XmlDomExtended wvarXMLASEGURADOS;
		XmlDomExtended wvarXMLASEGADIC;
		String mvarRequest;
		int i;
		int y;
		org.w3c.dom.Node x;
		int mvarLargo;
		mvarRequest = "";

		mvarRequest = mvarRequest + "<POLVENCIANN>0000</POLVENCIANN>";
		mvarRequest = mvarRequest + "<POLVENCIMES>00</POLVENCIMES>";
		mvarRequest = mvarRequest + "<POLVENVIDIA>00</POLVENVIDIA>";

		mvarRequest = mvarRequest + "<RAMOPCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//RAMOPCOD")) + "</RAMOPCOD>";
		mvarRequest = mvarRequest + "<POLIZANN>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//POLIZANN")) + "</POLIZANN>";
		mvarRequest = mvarRequest + "<POLIZSEC>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//POLIZSEC")) + "</POLIZSEC>";

		//Ticket 1435 cambio #4

	    if (wobjXMLRequest.selectSingleNode("//CANAL") == null) {
			mvarRequest = mvarRequest + "<CERTIPOL>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CERTIPOL")) + "</CERTIPOL>";
			mvarRequest = mvarRequest + "<CERTIANN>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CERTIANN")) + "</CERTIANN>";
	    } else {
			mvarRequest = mvarRequest + "<CERTIPOL>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CANAL")) + "</CERTIPOL>";
			mvarRequest = mvarRequest + "<CERTIANN>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CANAL_SUCURSAL")) + "</CERTIANN>";
	    }
	    // fin ticket 1435
		
		
		mvarRequest = mvarRequest + "<SUPLENUM>0</SUPLENUM>";

		mvarRequest = mvarRequest + "<FRANQCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//FRANQCOD")) + "</FRANQCOD>";

		mvarRequest = mvarRequest + "<PQTDES>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PQTDES")) + "</PQTDES>";
		mvarRequest = mvarRequest + "<PLANCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PLANNCOD")) + "</PLANCOD>";
		mvarRequest = mvarRequest + "<ZONA>0</ZONA>";

		mvarRequest = mvarRequest + "<CODPROV>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PROVI")) + "</CODPROV>";
		mvarRequest = mvarRequest + "<SUMALBA>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//SUMAASEG")) + "</SUMALBA>";

		mvarRequest = mvarRequest + "<CLIENIVA>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//IVA")) + "</CLIENIVA>";
		mvarRequest = mvarRequest + "<SUMASEG>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//SUMAASEG")) + "</SUMASEG>";
		mvarRequest = mvarRequest + "<CLUB_LBA>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CLUBLBA")) + "</CLUB_LBA>";

		mvarRequest = mvarRequest + "<DESTRUCCION_80>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//DESTRUCCION_80")) + "</DESTRUCCION_80>";
		mvarRequest = mvarRequest + "<CPAANO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CPAANO")) + "</CPAANO>";

		// mvarRequest = mvarRequest & "<DESTRUCCION_80>" &
		// Request.Form("DEST80") & "</DESTRUCCION_80>" NO SE USA...
		// mvarRequest = mvarRequest & "<CPAANO>" & Request.Form("CPAANOANN") &
		// "</CPAANO>" NO SE USA...
		// VERIFICAR CAMPO
		mvarRequest = mvarRequest + "<CTAKMS>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//KMSRNGCOD")) + "</CTAKMS>";
		mvarRequest = mvarRequest + "<ESCERO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//ESCERO")) + "</ESCERO>";

		// <TIENEPLAN>N</TIENEPLAN>
		// <COND_ADIC>1</COND_ADIC>
		// <ASEG_ADIC>S</ASEG_ADIC>
		// <FH_NAC>19800101</FH_NAC>
		// mvarRequest = mvarRequest & "<TIENEPLAN>" & Request.Form("PLAN") &
		// "</TIENEPLAN>" NO SE USA...
		// mvarRequest = mvarRequest & "<COND_ADIC>" & Request.Form("CONDADIC")
		// & "</COND_ADIC>" NO SE USA...
		// mvarRequest = mvarRequest & "<ASEG_ADIC>" &
		// Request.Form("ASEGUADICSINO") & "</ASEG_ADIC>" NO SE USA...
		// mvarRequest = mvarRequest & "<FH_NAC>" & Request.Form("NACIMANN") &
		// Request.Form("NACIMMES") & Request.Form("NACIMDIA") & "</FH_NAC>" NO
		// SE USA...
		mvarRequest = mvarRequest + "<SEXO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//SEXO")) + "</SEXO>";
		mvarRequest = mvarRequest + "<ESTCIV>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//ESTADO")) + "</ESTCIV>";
		// MHC : Agregado de CDATA en la descripcion del vehiculo
		// mvarRequest = mvarRequest & "<SIFMVEHI_DES>" &
		// OwnIif(left(Request.Form("MODEAUTCOT"),50) = "",
		// left(Request.Form("MODEAUT"),50),
		// left(Request.Form("MODEAUTCOT"),50)) & "</SIFMVEHI_DES>"
		// mvarRequest = mvarRequest & "<SIFMVEHI_DES><![CDATA[" &
		// OwnIif(left(Request.Form("MODEAUTCOT"),50) = "",
		// left(Request.Form("MODEAUT"),50),
		// left(Request.Form("MODEAUTCOT"),50)) & "]]></SIFMVEHI_DES>"
		mvarRequest = mvarRequest + "<SIFMVEHI_DES><![CDATA[" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VEHDES")) + "]]></SIFMVEHI_DES>";

		mvarRequest = mvarRequest + "<PROFECOD>000000</PROFECOD>";

		// <SIACCESORIOS>N</SIACCESORIOS>
		// mvarRequest = mvarRequest & "<SIACCESORIOS>" &
		// Request.Form("SIACCESORIOS") & "</SIACCESORIOS>" NO SE USA
		mvarRequest = mvarRequest + "<REFERIDO>0</REFERIDO>";
		mvarRequest = mvarRequest + "<MOTORNUM>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//MOTORNUM")) + "</MOTORNUM>";
		mvarRequest = mvarRequest + "<CHASINUM>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CHASINUM")) + "</CHASINUM>";
		mvarRequest = mvarRequest + "<PATENNUM>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PATENNUM")) + "</PATENNUM>";
		// mvarRequest = mvarRequest & "<MOTORNUM>D547851125</MOTORNUM>"
		// mvarRequest = mvarRequest & "<CHASINUM>CH04943245</CHASINUM>"
		// mvarRequest = mvarRequest & "<PATENNUM>DFP252</PATENNUM>"
		mvarRequest = mvarRequest + "<AUMARCOD>" + Strings.mid(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//MODEAUTCOD")), 1, 5) + "</AUMARCOD>";
		mvarRequest = mvarRequest + "<AUMODCOD>" + Strings.mid(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//MODEAUTCOD")), 6, 5) + "</AUMODCOD>";
		mvarRequest = mvarRequest + "<AUSUBCOD>" + Strings.mid(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//MODEAUTCOD")), 11, 5) + "</AUSUBCOD>";
		mvarRequest = mvarRequest + "<AUADICOD>" + Strings.mid(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//MODEAUTCOD")), 16, 5) + "</AUADICOD>";
		mvarRequest = mvarRequest + "<AUMODORI>" + Strings.mid(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//MODEAUTCOD")), 21, 1) + "</AUMODORI>";
		// VERIFICAR CAMPO (ASUMIMOS USO PARTICULAR)
		mvarRequest = mvarRequest + "<AUUSOCOD>1</AUUSOCOD>";
		mvarRequest = mvarRequest + "<AUVTVCOD>N</AUVTVCOD>";
		mvarRequest = mvarRequest + "<AUVTVDIA>0</AUVTVDIA>";
		mvarRequest = mvarRequest + "<AUVTVMES>0</AUVTVMES>";
		mvarRequest = mvarRequest + "<AUVTVANN>0</AUVTVANN>";

		String vehclrcod = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VEHCLRCOD"));
		// Si viene con 0s adelante ( 004 ) se los saco
		// The ^ anchor will make sure that the 0+ being matched is at the
		// beginning of the input. The (?!$) negative lookahead ensures that not
		// the entire string will be matched.
		// http://stackoverflow.com/questions/2800739/how-to-remove-leading-zeros-from-alphanumeric-text
		vehclrcod = vehclrcod.replaceFirst("^0+(?!$)", "");
		mvarRequest = mvarRequest + "<VEHCLRCOD>" + vehclrcod + "</VEHCLRCOD>";

		mvarRequest = mvarRequest + "<AUKLMNUM>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//KMSRNGCOD")) + "</AUKLMNUM>";
		mvarRequest = mvarRequest + "<FABRICAN>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//EFECTANN")) + "</FABRICAN>";
		mvarRequest = mvarRequest + "<FABRICMES>1</FABRICMES>";
		mvarRequest = mvarRequest + "<GUGARAGE>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//SIGARAGE")) + "</GUGARAGE>";
		mvarRequest = mvarRequest + "<GUDOMICI>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//GUDOMICI")) + "</GUDOMICI>";
		mvarRequest = mvarRequest + "<AUCATCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//AUCATCOD")) + "</AUCATCOD>";
		mvarRequest = mvarRequest + "<AUTIPCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//AUTIPCOD")) + "</AUTIPCOD>";
		mvarRequest = mvarRequest + "<AUCIASAN> </AUCIASAN>";
		mvarRequest = mvarRequest + "<AUANTANN>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//AUANTANN")) + "</AUANTANN>";
		// SINIESTROS
		mvarRequest = mvarRequest + "<AUNUMSIN>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//SINIESTROS")) + "</AUNUMSIN>";

		// Se pone el kilometraje de la inspección en dicho lugar.
		// VERIFICAR CAMPO
		mvarRequest = mvarRequest + "<AUNUMKMT>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//KMSRNGCOD")) + "</AUNUMKMT>";
		mvarRequest = mvarRequest + "<AUUSOGNC>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//GAS")) + "</AUUSOGNC>";
		// VERIFICAR CAMPO (SE ASUME CODINST)
		mvarRequest = mvarRequest + "<USUARCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CODINST")) + "</USUARCOD>";
		// VERIFICAR CAMPO (SE ASUME 0)
		mvarRequest = mvarRequest + "<SITUCPOL>0</SITUCPOL>";
		// VERIFICAR CAMPO (SE ASUME QUE NO SE USA)
		mvarRequest = mvarRequest + "<CLIENSEC></CLIENSEC>";
		mvarRequest = mvarRequest + "<NUMEDOCU>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//NUMEDOCU")) + "</NUMEDOCU>";
		mvarRequest = mvarRequest + "<TIPODOCU>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//TIPODOCU")) + "</TIPODOCU>";
		// VERIFICAR CAMPO (se asume 1)
		mvarRequest = mvarRequest + "<DOMICSEC>1</DOMICSEC>";
		// VERIFICAR CAMPO (se asume 1)
		mvarRequest = mvarRequest + "<CUENTSEC>1</CUENTSEC>";
		mvarRequest = mvarRequest + "<COBROFOR>1</COBROFOR>";

		// <EDADACTU>33</EDADACTU>
		// mvarEdad = CalcularEdad(Request.Form("NACIMANN") &
		// Request.Form("NACIMMES") & Request.Form("NACIMDIA"))
		// mvarRequest = mvarRequest & "<EDADACTU>" & mvarEdad & "</EDADACTU>"
		mvarRequest = mvarRequest + "<COBERTURAS></COBERTURAS>";

		// <ASEGURADOS>
		// <ASEGURADO>
		// <DOCUMDAT>20457856</DOCUMDAT>
		// <NOMBREAS><![CDATA[PRUEBAADIC,ITDADIC]]></NOMBREAS>
		// </ASEGURADO>
		// </ASEGURADOS>
		if (wobjXMLRequest.selectSingleNode("//ASEGURADOS-ADIC") != null
				&& !XmlDomExtended.prettyPrint(wobjXMLRequest.selectSingleNode("//ASEGURADOS-ADIC")).equals("<ASEGURADOS-ADIC/>")) {

			wvarXMLASEGADIC = new XmlDomExtended();
			wvarXMLASEGADIC.loadXML(XmlDomExtended.prettyPrint(wobjXMLRequest.selectSingleNode("//ASEGURADOS-ADIC")));

			i = 0;
			mvarRequest = mvarRequest + "<ASEGURADOS>";
			for (int nx = 0; nx < wvarXMLASEGADIC.selectNodes("//ASEGURADO-ADIC").getLength(); nx++) {
				x = wvarXMLASEGADIC.selectNodes("//ASEGURADO-ADIC").item(nx);
				mvarRequest = mvarRequest + "<ASEGURADO>";
				// Antes: mvarRequest = mvarRequest + "<DOCUMDAT>" +
				// XmlDomExtended.getText( x.selectNodes( "//DOCUMASEG" ) .item(
				// i ) ) + "</DOCUMDAT>";
				XmlDomExtended wvarXMLASEGURADO_ADIC = new XmlDomExtended();
				wvarXMLASEGURADO_ADIC.loadXML(XmlDomExtended.prettyPrint(x));
				mvarRequest = mvarRequest + "<DOCUMDAT>" + XmlDomExtended.getText(wvarXMLASEGURADO_ADIC.selectNodes("//DOCUMASEG").item(i)) + "</DOCUMDAT>";

				mvarRequest = mvarRequest + "<NOMBREAS><![CDATA[" + XmlDomExtended.getText(wvarXMLASEGURADO_ADIC.selectNodes("//NOMBREASEG").item(i))
						+ "]]></NOMBREAS>";
				mvarRequest = mvarRequest + "</ASEGURADO>";
				i = i + 1;
			}
			mvarRequest = mvarRequest + "</ASEGURADOS>";
		}

		// mvarRequest = mvarRequest & "Request.Form('XMLASEGUADIC')" 'VERIFICAR
		// CAMPO
		mvarRequest = mvarRequest + "<CAMP_CODIGO>"
				+ Strings.right(("0000" + Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CAMPACOD")))), 4) + "</CAMP_CODIGO>";
		mvarRequest = mvarRequest + "<NRO_PROD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//AGECOD")) + "</NRO_PROD>";
		// MHC: Solución Táctica Etapa III - Bancos
		// Ticket 1435, cambio #5
		if (!(wobjXMLRequest.selectSingleNode("//CANAL") == (org.w3c.dom.Node) null)) {
			if (Obj.toInt(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CANAL"))) == 150 ) {
				mvarRequest = mvarRequest + "<SUCURSAL_CODIGO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CANAL_SUCURSAL")) + "</SUCURSAL_CODIGO>";
				mvarRequest = mvarRequest + "<LEGAJO_VEND>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//LEGAJO_VEND")) + "</LEGAJO_VEND>";
				mvarRequest = mvarRequest + "<EMPRESA_CODIGO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//LEGAJCIA")) + "</EMPRESA_CODIGO>";
				mvarRequest = mvarRequest + "<EMPL></EMPL>";
				mvarRequest = mvarRequest + "<LEGAJO_GTE>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//LEGAJO_VEND")) + "</LEGAJO_GTE>";
			} else {
				mvarRequest = mvarRequest + "<SUCURSAL_CODIGO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CANAL_SUCURSAL")) + "</SUCURSAL_CODIGO>";
				mvarRequest = mvarRequest + "<LEGAJO_VEND>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//LEGAJO_VEND")) + "</LEGAJO_VEND>";
				mvarRequest = mvarRequest + "<EMPRESA_CODIGO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//LEGAJCIA")) + "</EMPRESA_CODIGO>";
				mvarRequest = mvarRequest + "<EMPL></EMPL>";
				mvarRequest = mvarRequest + "<LEGAJO_GTE>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//LEGAJO_VEND")) + "</LEGAJO_GTE>";
			}
		} else {
			mvarRequest = mvarRequest + "<SUCURSAL_CODIGO>8888</SUCURSAL_CODIGO>";
			mvarRequest = mvarRequest + "<LEGAJO_VEND></LEGAJO_VEND>";
			mvarRequest = mvarRequest + "<EMPRESA_CODIGO>0</EMPRESA_CODIGO>";
			mvarRequest = mvarRequest + "<EMPL></EMPL>";
			mvarRequest = mvarRequest + "<LEGAJO_GTE></LEGAJO_GTE>";
		}
		
		// Fin ticket 1435
		
		// VERIFICAR CAMPOS
		Node hijosNode = wobjXMLRequest.selectSingleNode("//HIJOS");
		if ( hijosNode != null ) {
			mvarRequest = mvarRequest + XmlDomExtended.prettyPrint(hijosNode);
		}

		// ACCESORIOS
		// If Request.Form("XMLACCESORIOS") <> "" Then
		// <EMPL_NOMYAPE/>
		// <CLIENTE_NOMYAPE>PRUEBA ITD</CLIENTE_NOMYAPE>
		if (wobjXMLRequest.selectSingleNode("//ACCESORIOS") != null
				&& !XmlDomExtended.prettyPrint(wobjXMLRequest.selectSingleNode("//ACCESORIOS")).equals("<ACCESORIOS/>")) {

			wvarXMLACCESORIOS = new XmlDomExtended();
			wvarXMLACCESORIOS.loadXML(XmlDomExtended.prettyPrint(wobjXMLRequest.selectSingleNode("//ACCESORIOS")));

			i = 1;
			for (int nx = 0; nx < wvarXMLACCESORIOS.selectNodes("//ACCESORIO").getLength(); nx++) {
				x = wvarXMLACCESORIOS.selectNodes("//ACCESORIO").item(nx);
				XmlDomExtended wvarXMLACCESORIO = new XmlDomExtended();
				wvarXMLACCESORIO.loadXML(XmlDomExtended.prettyPrint(x));

				Element wobjXML = wvarXMLACCESORIOS.getDocument().createElement("VEACCSEC");
				if (wvarXMLACCESORIO.selectSingleNode("./DEPRECIA") == (org.w3c.dom.Node) null) {
					Element wobjXMLAux = wvarXMLACCESORIOS.getDocument().createElement("DEPRECIA");
					x.appendChild(wobjXMLAux);
					XmlDomExtended.setText(wobjXMLAux, "N");
				}
				x.appendChild(wobjXML);
				XmlDomExtended.setText(wobjXML, String.valueOf(i));
				i = i + 1;
			}
		} else {
			mvarRequest = mvarRequest + "<ACCESORIOS/>";
		}
		// mvarRequest = mvarRequest & wvarXMLACCESORIOS.xml
		// inspeccion
		// mvarRequest = mvarRequest & "<SITUCEST>" &
		// owniif(Request.Form("RB0KM")="S","S","D") & "</SITUCEST>"
		// VERIFICAR CAMPO (Se asume 3)
		mvarRequest = mvarRequest + "<ESTAINSP>3</ESTAINSP>";
		// mvarRequest = mvarRequest & "<ESTAINSP>3</ESTAINSP>"
		mvarRequest = mvarRequest + "<INSPECOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//INSPECOD")) + "</INSPECOD>";
		// VERIFICAR CAMPO
		mvarRequest = mvarRequest + "<INSPECTOR></INSPECTOR>";
		mvarRequest = mvarRequest + "<INSPEADOM>S</INSPEADOM>";
		// VERIFICAR CAMPO
		mvarRequest = mvarRequest + "<OBSERTXT></OBSERTXT>";
		// mvarRequest = mvarRequest & "<INSPENUM>0</INSPENUM>"
		// VERIFICAR CAMPOS
		mvarRequest = mvarRequest + "<INSPEOBLEA></INSPEOBLEA>";
		// VERIFICAR CAMPOS
		mvarRequest = mvarRequest + "<INSPEKMS></INSPEKMS>";
		// VERIFICAR CAMPOS
		mvarRequest = mvarRequest + "<CENTRCOD_INS></CENTRCOD_INS>";
		// VERIFICAR CAMPOS
		mvarRequest = mvarRequest + "<CENTRDES></CENTRDES>";
		// mvarRequest = mvarRequest & "<INSPECC>" &
		// owniif(trim(mvarDATOS_INSPECCION")) <> "",mvarDATOS_INSPECCION"),"0")
		// & "</INSPECC>"
		mvarRequest = mvarRequest + "<RASTREO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//RASTREO")) + "</RASTREO>";
		// mvarRequest = mvarRequest & "<ALARMCOD>" &
		// owniif(trim(mvarALARMAS")) <> "",mvarALARMAS"),"0") & "</ALARMCOD>"
		mvarRequest = mvarRequest + "<ALARMCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//ALARMCOD")) + "</ALARMCOD>";
		mvarRequest = mvarRequest + "<INSPEANN>0</INSPEANN>";
		mvarRequest = mvarRequest + "<INSPEMES>0</INSPEMES>";
		mvarRequest = mvarRequest + "<INSPEDIA>0</INSPEDIA>";
		// fin inspeccion
		// If Request.form("PRENDASINO") <> "S" Then
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_ACREPREN) == (org.w3c.dom.Node) null)) {
			if (Strings.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ACREPREN))).equals("S")) {
				mvarRequest = mvarRequest + "<NUMEDOCU_ACRE>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_NUMEDOCUACRE))
						+ "</NUMEDOCU_ACRE>";
				mvarRequest = mvarRequest + "<TIPODOCU_ACRE>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_TIPODOCUACRE))
						+ "</TIPODOCU_ACRE>";
				mvarRequest = mvarRequest + "<APELLIDO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_APELLIDOACRE)) + "</APELLIDO>";
				mvarRequest = mvarRequest + "<NOMBRES>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_NOMBRESACRE)) + "</NOMBRES>";
				mvarRequest = mvarRequest + "<DOMICDOM>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICDOMACRE)) + "</DOMICDOM>";
				mvarRequest = mvarRequest + "<DOMICDNU>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICDNUACRE)) + "</DOMICDNU>";
				mvarRequest = mvarRequest + "<DOMICESC> </DOMICESC>";
				mvarRequest = mvarRequest + "<DOMICPIS>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPISACRE)) + "</DOMICPIS>";
				mvarRequest = mvarRequest + "<DOMICPTA>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPTAACRE)) + "</DOMICPTA>";
				mvarRequest = mvarRequest + "<DOMICPOB>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPOBACRE)) + "</DOMICPOB>";
				mvarRequest = mvarRequest + "<DOMICCPO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICCPOACRE)) + "</DOMICCPO>";
				mvarRequest = mvarRequest + "<PROVICOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PROVICODACRE)) + "</PROVICOD>";
				mvarRequest = mvarRequest + "<PAISSCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PAISSCODACRE)) + "</PAISSCOD>";
			} else {
				mvarRequest = mvarRequest + "<NUMEDOCU_ACRE> </NUMEDOCU_ACRE>";
				mvarRequest = mvarRequest + "<TIPODOCU_ACRE>0</TIPODOCU_ACRE>";
				mvarRequest = mvarRequest + "<APELLIDO> </APELLIDO>";
				mvarRequest = mvarRequest + "<NOMBRES> </NOMBRES>";
				mvarRequest = mvarRequest + "<DOMICDOM> </DOMICDOM>";
				mvarRequest = mvarRequest + "<DOMICDNU> </DOMICDNU>";
				mvarRequest = mvarRequest + "<DOMICESC> </DOMICESC>";
				mvarRequest = mvarRequest + "<DOMICPIS> </DOMICPIS>";
				mvarRequest = mvarRequest + "<DOMICPTA> </DOMICPTA>";
				mvarRequest = mvarRequest + "<DOMICPOB> </DOMICPOB>";
				mvarRequest = mvarRequest + "<DOMICCPO>0</DOMICCPO>";
				mvarRequest = mvarRequest + "<PROVICOD>0</PROVICOD>";
				mvarRequest = mvarRequest + "<PAISSCOD> </PAISSCOD>";
			}
		} else {
			mvarRequest = mvarRequest + "<NUMEDOCU_ACRE> </NUMEDOCU_ACRE>";
			mvarRequest = mvarRequest + "<TIPODOCU_ACRE>0</TIPODOCU_ACRE>";
			mvarRequest = mvarRequest + "<APELLIDO> </APELLIDO>";
			mvarRequest = mvarRequest + "<NOMBRES> </NOMBRES>";
			mvarRequest = mvarRequest + "<DOMICDOM> </DOMICDOM>";
			mvarRequest = mvarRequest + "<DOMICDNU> </DOMICDNU>";
			mvarRequest = mvarRequest + "<DOMICESC> </DOMICESC>";
			mvarRequest = mvarRequest + "<DOMICPIS> </DOMICPIS>";
			mvarRequest = mvarRequest + "<DOMICPTA> </DOMICPTA>";
			mvarRequest = mvarRequest + "<DOMICPOB> </DOMICPOB>";
			mvarRequest = mvarRequest + "<DOMICCPO>0</DOMICCPO>";
			mvarRequest = mvarRequest + "<PROVICOD>0</PROVICOD>";
			mvarRequest = mvarRequest + "<PAISSCOD> </PAISSCOD>";
		}

		
		//Ticket 1435
		// "Modificar el tag BANCOCOD, según CANAL 150"
		// Antes era:
//		if (wobjXMLRequest.selectSingleNode("//CANAL") == null) {
//			mvarRequest = mvarRequest + "<BANCOCOD>9000</BANCOCOD>";
//		} else {
//			mvarRequest = mvarRequest + "<BANCOCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CODINST")) + "</BANCOCOD>";
//		}

		// Cambio pedido:
		//		If Not wobjXMLRequest.selectSingleNode("//CANAL") Is Nothing Then
//        mvarRequest = mvarRequest & "<BANCOCOD>" &
//wobjXMLRequest.selectSingleNode("//CANAL").Text & "</BANCOCOD>"
//    Else
//        mvarRequest = mvarRequest & "<BANCOCOD>9001</BANCOCOD>"
//    End If

	    if (wobjXMLRequest.selectSingleNode("//CANAL") == null) {
			mvarRequest = mvarRequest + "<BANCOCOD>9001</BANCOCOD>";
		} else {
			mvarRequest = mvarRequest + "<BANCOCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CANAL")) + "</BANCOCOD>";
		}
		// Fin 1435
		
		// mvarRequest = mvarRequest & "<BANCOCOD>" & Request.form("BANCOCOD") &
		// "</BANCOCOD>"
		mvarRequest = mvarRequest + "<COBROCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//COBROCOD")) + "</COBROCOD>";
		// mvarRequest = mvarRequest & "<COBROCOD>4</COBROCOD>"
		mvarRequest = mvarRequest + "<EFECTANN2>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VIGENANN")) + "</EFECTANN2>";
		mvarRequest = mvarRequest + "<EFECTMES>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VIGENMES")) + "</EFECTMES>";
		mvarRequest = mvarRequest + "<EFECTDIA>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VIGENDIA")) + "</EFECTDIA>";

		// <INSPEANN>0</INSPEANN>
		// <INSPEMES>0</INSPEMES>
		// <INSPEDIA>0</INSPEDIA>
		// <ENTDOMSC>2</ENTDOMSC>
		// <ANOTACIO><![CDATA[]]></ANOTACIO>
		// mvarRequest = mvarRequest & "<ENTDOMSC>" & mvarDOMICENTREGA &
		// "</ENTDOMSC>" NO SE UTILIZA
		// mvarRequest = mvarRequest & "<ANOTACIO><![CDATA[" &
		// Request.Form("ANOTACIONES") & "]]></ANOTACIO>" NO SE UTILIZA
		mvarRequest = mvarRequest + "<PRECIO_MENSUAL>" + Strings.replace(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PRECIO")), ",", ".")
				+ "</PRECIO_MENSUAL>";

		// <MODEAUTCOD>00009000870000103481N</MODEAUTCOD>
		// mvarRequest = mvarRequest & "<MODEAUTCOD>" &
		// Request.Form("MODEAUTCOD") & "</MODEAUTCOD>" NO SE UTILIZA
		mvarRequest = mvarRequest + "<EFECTANN>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//EFECTANN")) + "</EFECTANN>";
		mvarRequest = mvarRequest + "<NACIMANN>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//NACIMANN")) + "</NACIMANN>";
		mvarRequest = mvarRequest + "<NACIMMES>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//NACIMMES")) + "</NACIMMES>";
		mvarRequest = mvarRequest + "<NACIMDIA>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//NACIMDIA")) + "</NACIMDIA>";

		mvarRequest = mvarRequest + "<IVA>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//IVA")) + "</IVA>";
		if (!(wobjXMLRequest.selectSingleNode("//IBB") == (org.w3c.dom.Node) null)) {
			mvarRequest = mvarRequest + "<IBB>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//IBB")) + "</IBB>";
		}
		mvarRequest = mvarRequest + "<ESTADO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//ESTADO")) + "</ESTADO>";
		mvarRequest = mvarRequest + "<KMSRNGCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//KMSRNGCOD")) + "</KMSRNGCOD>";
		mvarRequest = mvarRequest + "<SIGARAGE>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//SIGARAGE")) + "</SIGARAGE>";
		mvarRequest = mvarRequest + "<SINIESTROS>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//SINIESTROS")) + "</SINIESTROS>";
		mvarRequest = mvarRequest + "<GAS>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//GAS")) + "</GAS>";
		mvarRequest = mvarRequest + "<PROVI>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PROVI")) + "</PROVI>";

		mvarRequest = mvarRequest + "<LOCALIDADCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//LOCALIDADCOD")) + "</LOCALIDADCOD>";
		mvarRequest = mvarRequest + "<CLUBLBA>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CLUBLBA")) + "</CLUBLBA>";

		// MHC: se agregan nuevas marcas al cotizador
		mvarRequest = mvarRequest + "<CLUBECO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CLUBECO")) + "</CLUBECO>";
		mvarRequest = mvarRequest + "<GRANIZO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//GRANIZO")) + "</GRANIZO>";
		mvarRequest = mvarRequest + "<ROBOCONT>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//ROBOCONT")) + "</ROBOCONT>";

		// <DATOSPLAN>01503</DATOSPLAN>
		// <CAMPACOD>0900</CAMPACOD>
		// <CAMP_DESC><![CDATA[BAJA POR ALTA 2005]]></CAMP_DESC>
		// <AGECLA>PR</AGECLA>
		// <AGENTCLA>PR</AGENTCLA>
		// mvarRequest = mvarRequest & "<DATOSPLAN>" & mvarDATOSPLAND &
		// "</DATOSPLAN>" NO SE UTILIZA
		// mvarRequest = mvarRequest & "<DATOSPLAN>00099</DATOSPLAN>"
		// mvarRequest = mvarRequest & "<CAMPACOD>" & Right("0000" +
		// Trim(Request.Form("CAMPACOD")), 4) & "</CAMPACOD>" NO SE UTILIZA
		// DA - 21/02/2007: Se manda con CDATA porque si tiene caracteres
		// especiales pincha.
		// mvarRequest = mvarRequest & "<CAMP_DESC><![CDATA[" &
		// Request.Form("CAMP_DESC") & "]]></CAMP_DESC>" NO SE UTILIZA
		// mvarRequest = mvarRequest & "<CAMP_DESC>" & Request.Form("CAMP_DESC")
		// & "</CAMP_DESC>"
		if (!(wobjXMLRequest.selectSingleNode("//AGECLA") == (org.w3c.dom.Node) null)) {
			// MMC 2012-12-05 - Verificar Campo
			mvarRequest = mvarRequest + "<AGECLA>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//AGECLA")) + "</AGECLA>";
			// mvarRequest = mvarRequest & "<AGENTCLA>" &
			// Request.Form("AGENTCLA") & "</AGENTCLA>" ' NO SE UTILIZA
		}
		mvarRequest = mvarRequest + "<AGECOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//AGECOD")) + "</AGECOD>";

		// <PORTAL>LBA_PRODUCTORES</PORTAL>
		// mvarRequest = mvarRequest & "<PORTAL>" & "LBA_PRODUCTORES" &
		// "</PORTAL>" NO SE UTILIZA
		mvarRequest = mvarRequest + "<COBROTIP>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//COBROTIP")) + "</COBROTIP>";
		// mvarRequest = mvarRequest & "<COBROTIP>AC</COBROTIP>"
		// <CUITNUME/>
		// <RAZONSOC><![CDATA[]]></RAZONSOC>
		// <NROFACTU/>
		// mvarRequest = mvarRequest & "<CUITNUME>" & Request.Form("NUMCUIT") &
		// "</CUITNUME>" NO SE UTILIZA
		// mvarRequest = mvarRequest & "<RAZONSOC><![CDATA[" &
		// Request.Form("RAZON_SOCIAL") & "]]></RAZONSOC>" NO SE UTILIZA
		// verificar campo
		mvarRequest = mvarRequest + "<CLIEIBTP>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//IBB")) + "</CLIEIBTP>";
		mvarRequest = mvarRequest + "<NROIIBB>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//NROIBB")) + "</NROIIBB>";
		mvarRequest = mvarRequest + "<LUNETA>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//LUNETA")) + "</LUNETA>";

		// AGREGO YO - JP
		// If Request.Form("FACT0KM") = "N" Then
		// mvarRequest = mvarRequest & "<NROFACTU></NROFACTU>"
		// Else
		// If Trim(Request.Form("FACT0KM")) = "" Then
		if (!(wobjXMLRequest.selectSingleNode("//NROFACTU") == (org.w3c.dom.Node) null)) {
			// verificar campo
			mvarRequest = mvarRequest + "<NROFACTU>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//NROFACTU")) + "</NROFACTU>";
		} else {
			mvarRequest = mvarRequest + "<NROFACTU/>";
		}

		// VERIFICAR CAMPO
		mvarRequest = mvarRequest + "<DDJJ_STRING></DDJJ_STRING>";

		// VERIFICAR CAMPOS
		mvarRequest = mvarRequest + "<DERIVACI></DERIVACI>";
		// VERIFICAR CAMPOS
		mvarRequest = mvarRequest + "<ALEATORIO></ALEATORIO>";
		// VERIFICAR CAMPOS
		mvarRequest = mvarRequest + "<ESTAIDES></ESTAIDES>";
		// VERIFICAR CAMPOS
		mvarRequest = mvarRequest + "<DISPODES></DISPODES>";

		// NO SE UTILIZA
		mvarRequest = mvarRequest + "<INSTALADP>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//INSTALADP")) + "</INSTALADP>";
		// NO SE UTILIZA
		mvarRequest = mvarRequest + "<POSEEDISP>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//POSEEDISP")) + "</POSEEDISP>";

		// <AUTIPGAMA>B</AUTIPGAMA>
		// <SN_NBWS>S</SN_NBWS>
		// <EMAIL_NBWS>MARTIN.CABRERA@HSBC.COM.AR</EMAIL_NBWS>
		// <SN_EPOLIZA>N</SN_EPOLIZA>
		// <EMAIL_EPOLIZA/>
		// <SN_PRESTAMAIL>N</SN_PRESTAMAIL>
		// <PASSEPOL/>
		// mvarRequest = mvarRequest & "<AUTIPGAMA>" & Request.Form("AUTIPGAMA")
		// & "</AUTIPGAMA>" 'NO SE UTILIZA
		// mvarRequest = mvarRequest & "<SN_NBWS>" & mvarSuscripto &
		// "</SN_NBWS>" NO LO UTILIZA
		// mvarRequest = mvarRequest & "<email_NBWS>" & mvarMailSuscripto &
		// "</email_NBWS>" NO LO UTILIZA
		// mvarRequest = mvarRequest & "<SN_EPOLIZA>" &
		// Request.Form("SN_EPOLIZA") & "</SN_EPOLIZA>" NO LO UTILIZA
		// mvarRequest = mvarRequest & "<email_EPOLIZA>" &
		// Request.Form("email_EPOLIZA") & "</email_EPOLIZA>" NO LO UTILIZA
		// mvarRequest = mvarRequest & "<SN_PRESTAMAIL>" &
		// Request.Form("SN_PRESTAMAIL") & "</SN_PRESTAMAIL>" NO LO UTILIZA
		// 13/10/2009 Se agrega Password de EPoliza por nueva definicion: AUS1 =
		// (3 ult dig doc) + (3 ult dig patente)
		// Este Password se utilizara solo para Integracion, en Emision Online
		// el pass es generado por el back.-
		// Las patentes A/D no pueden suscribirse a EPoliza. Por lo tanto, en
		// este caso no guardo pass.-
		// mvarPATENTE = trim(Request.Form("PATENTE")) Esto esta definido arriba
		// Dim mvarNUMEDOCU
		// Dim mvarPASSEPOL
		// mvarNUMEDOCU = Request.Form("NUMEDOCU")
		// If UCase(Trim(mvarPATENTE)) <> "A/D" And Request.Form("SN_EPOLIZA") =
		// "S" Then
		// mvarPASSEPOL = Right("000" + Trim(mvarNUMEDOCU), 3) & Right("000" +
		// Trim(mvarPATENTE), 3)
		// Else
		// mvarPASSEPOL = ""
		// End If
		// mvarRequest = mvarRequest & "<PASSEPOL>" & mvarPASSEPOL &
		// "</PASSEPOL>" NO SE UTILIZA
		// Dim y
		// Dim mvarEMAIL_NBWS
		// Dim mvarEMAIL_EPOLIZA
		// Dim mvarMEDCOCOD_NBWS
		// Dim mvarMEDCOCOD_EPOL
		mvarRequest = mvarRequest + "<MEDIOSCONTACTO>";

		// Por default mvarSuscripto y Request.Form("SN_EPOLIZA") = N
		// El ciclo se lo toma por 10 sin tener en cuenta los nodos 9 y 10
		for (y = 1; y <= 10; y++) {
			// Ciclo 10 veces en blanco
			mvarRequest = mvarRequest + "<MEDIO>";
			mvarRequest = mvarRequest + "<MEDCOSEC></MEDCOSEC>";
			mvarRequest = mvarRequest + "<MEDCOCOD></MEDCOCOD>";
			mvarRequest = mvarRequest + "<MEDCODAT></MEDCODAT>";
			mvarRequest = mvarRequest + "<MEDCOSYS></MEDCOSYS>";
			mvarRequest = mvarRequest + "</MEDIO>";
		}

		// LR - ANEXO I: En el nodo 9 agrego los datos para Seguros Online(NBWS)
		// GD - ANEXO I: Se modifica el origen de la marca y el mail
		// If (mvarSuscripto = "S") Then
		// mvarEMAIL_NBWS = mvarMailSuscripto
		// mvarMEDCOCOD_NBWS = "EMA"
		// Else
		// mvarEMAIL_NBWS = ""
		// mvarMEDCOCOD_NBWS = ""
		// End If
		// mvarRequest = mvarRequest & "<MEDIO>"
		// mvarRequest = mvarRequest & "<MEDCOSEC>0</MEDCOSEC>" 'va en blanco
		// mvarRequest = mvarRequest & "<MEDCOCOD>" & mvarMEDCOCOD_NBWS &
		// "</MEDCOCOD>" 'me mand�s "EMA".
		// mvarRequest = mvarRequest & "<MEDCODAT>" & mvarEMAIL_NBWS &
		// "</MEDCODAT>" 'el mail en caso que el cliente se suscribe.
		// mvarRequest = mvarRequest & "<MEDCOSYS></MEDCOSYS>" 'va en blanco
		// mvarRequest = mvarRequest & "</MEDIO>"
		// Por ultimo en el nodo decimo agrego los datos para EPoliza
		// If (Request.Form("SN_EPOLIZA") = "S") Then
		// mvarEMAIL_EPOLIZA = Request.Form("email_EPOLIZA")
		// mvarMEDCOCOD_EPOL = "EMA"
		// Else
		// mvarEMAIL_EPOLIZA = ""
		// mvarMEDCOCOD_EPOL = ""
		// End If
		// mvarRequest = mvarRequest & "<MEDIO>"
		// mvarRequest = mvarRequest & "<MEDCOSEC>0</MEDCOSEC>" 'va en blanco
		// mvarRequest = mvarRequest & "<MEDCOCOD>" & mvarMEDCOCOD_EPOL &
		// "</MEDCOCOD>" 'me mand�s "EPO".
		// mvarRequest = mvarRequest & "<MEDCODAT>" & mvarEMAIL_EPOLIZA &
		// "</MEDCODAT>" 'el mail en caso que el cliente se suscribe.
		// mvarRequest = mvarRequest & "<MEDCOSYS></MEDCOSYS>" 'va en blanco
		// mvarRequest = mvarRequest & "</MEDIO>"
		mvarRequest = mvarRequest + "</MEDIOSCONTACTO>";

		// Ticket 1435:
	    if (wobjXMLRequest.selectSingleNode("//NROSOLI") == null) {
			 String mvarCertiSec = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CERTISEC"));
			 
			 if ( mvarCertiSec != null && mvarCertiSec.length() != 0 ) {
				 try {
					int ncertisec = Integer.parseInt(mvarCertiSec);
					if ( ncertisec == 0 ) {
						 String mvarRAMOPCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//RAMOPCOD"));
						 String mvarPOLIZANN = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//POLIZANN"));
						 String mvarPOLIZSEC = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//POLIZSEC"));
						 String mvarCERTIPOL = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CERTIPOL"));
						 String mvarCERTIANN = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CERTIANN"));
						 if (mvarRAMOPCOD.equals("")) {
							 throw new ComponentExecutionException("RAMOPCOD es vacío o no especificado en el request");
						 }
						 if (mvarPOLIZANN.equals("")) {
							 throw new ComponentExecutionException("POLIZANN es vacío o no especificado en el request");
						 }
						 if (mvarPOLIZSEC.equals("")) {
							 throw new ComponentExecutionException("POLIZSEC es vacío o no especificado en el request");
						 }
						 if (mvarCERTIPOL.equals("")) {
							 throw new ComponentExecutionException("CERTIPOL es vacío o no especificado en el request");
						 }
						 if (mvarCERTIANN.equals("")) {
							 throw new ComponentExecutionException("CERTIANN es vacío o no especificado en el request");
						 }
						mvarCertiSec = fncObtenerCertisecSQL(mvarRAMOPCOD, mvarPOLIZANN, mvarPOLIZSEC, mvarCERTIPOL, mvarCERTIANN); 
					}
				} catch (NumberFormatException e) {
					// Si no lo puede convertir es lo mismo que si no lo hubiera encontrado, o sea != 0, lo dejo para que copie lo que vino
				}
			 }
			mvarRequest = mvarRequest + "<CERTISEC>" + mvarCertiSec + "</CERTISEC>";
	    } else {
	    	mvarRequest = mvarRequest + "<CERTISEC>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//NROSOLI")) + "</CERTISEC>";	    	
	    }

	    // Fin Ticket 1435
		
		
		// Datos para AIS
		mvarRequest = mvarRequest + "<TIPOOPERACION>8</TIPOOPERACION>";
		mvarRequest = mvarRequest + "<OPCION>01</OPCION>";
		mvarRequest = mvarRequest + "<FUNCION>1</FUNCION>";

		mvarRequest = mvarRequest + "<GENERARLOG></GENERARLOG>";

		// Datos Cliente
		mvarRequest = mvarRequest + "<CLICLIENAP1><![CDATA[" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CLIENAP1")) + "]]></CLICLIENAP1>";
		mvarRequest = mvarRequest + "<AISCLICLIENNOM><![CDATA[" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CLIENNOM"))
				+ "]]></AISCLICLIENNOM>";
		mvarRequest = mvarRequest + "<CLICLIENNOM><![CDATA[" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CLIENNOM")) + "]]></CLICLIENNOM>";

		// FJO - 2009-02-11
		// Persona fisica
		mvarRequest = mvarRequest + "<CLICLIENTIP>00</CLICLIENTIP>";
		// mvarRequest = mvarRequest & "<CLICLIENTIP>" & Right("00" &
		// Request.Form("TIPOPERSONA"), 2) & "</CLICLIENTIP>" 'Tipo de Persona
		// FJO - 2009-02-11
		// DA - 20/02/2007: Datos exigido x la UIF
		// VERIFICAR CAMPO
		mvarRequest = mvarRequest + "<UIFCUIT></UIFCUIT>";

		// Domicilio Cliente
		mvarRequest = mvarRequest + "<CLIDOMICCAL>PAR</CLIDOMICCAL>";
		mvarRequest = mvarRequest + "<CLIDOMICDOM><![CDATA[" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//DOMICDOM")) + "]]></CLIDOMICDOM>";

		mvarRequest = mvarRequest + "<CLIDOMICDNU>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//DOMICDNU")) + "</CLIDOMICDNU>";
		mvarRequest = mvarRequest + "<CLIDOMICESC></CLIDOMICESC>";
		mvarRequest = mvarRequest + "<CLIDOMICPIS>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//DOMICPIS")) + "</CLIDOMICPIS>";
		mvarRequest = mvarRequest + "<CLIDOMICPTA>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//DOMICPTA")) + "</CLIDOMICPTA>";
		mvarRequest = mvarRequest + "<CLIDOMICPOB><![CDATA[" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//DOMICPOB")) + "]]></CLIDOMICPOB>";
		mvarRequest = mvarRequest + "<CLIDOMICCPO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//LOCALIDADCOD")) + "</CLIDOMICCPO>";
		mvarRequest = mvarRequest + "<CLIDOMPROVICOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PROVI")) + "</CLIDOMPROVICOD>";

		// If Not wobjXMLRequest.selectSingleNode("//CLIDOMICDNU") Is Nothing
		// Then
		// mvarRequest = mvarRequest & "<CLIDOMICDNU>" &
		// wobjXMLRequest.selectSingleNode("//CLIDOMICDNU").Text &
		// "</CLIDOMICDNU>"
		// mvarRequest = mvarRequest & "<CLIDOMICESC>" &
		// wobjXMLRequest.selectSingleNode("//CLIDOMICESC").Text &
		// "</CLIDOMICESC>"
		// mvarRequest = mvarRequest & "<CLIDOMICPIS>" &
		// wobjXMLRequest.selectSingleNode("//CLIDOMICPIS").Text &
		// "</CLIDOMICPIS>"
		// mvarRequest = mvarRequest & "<CLIDOMICPTA>" &
		// wobjXMLRequest.selectSingleNode("//CLIDOMICCPTA").Text &
		// "</CLIDOMICPTA>"
		// mvarRequest = mvarRequest & "<CLIDOMICPOB><![CDATA[" &
		// wobjXMLRequest.selectSingleNode("//CLIDOMICPOB").Text &
		// "]]></CLIDOMICPOB>"
		// mvarRequest = mvarRequest & "<CLIDOMICPOB>" &
		// Request.form("LOCALIDAD_C") & "</CLIDOMICPOB>"
		// mvarRequest = mvarRequest & "<CLIDOMICPOB><![CDATA[" &
		// Request.form("LOCALIDAD_DES") & "]]></CLIDOMICPOB>"
		// Si no funciona probar con LOCALIDADDSP
		// mvarRequest = mvarRequest & "<CLIDOMICCPO>" &
		// wobjXMLRequest.selectSingleNode("//CLIDOMICCPO").Text &
		// "</CLIDOMICCPO>" 'VERIFICAR CAMPO
		// mvarRequest = mvarRequest & "<CLIDOMICCPO>" &
		// Request.form("CODPOSTAL") & "</CLIDOMICCPO>"
		// mvarRequest = mvarRequest & "<CLIDOMPROVICOD>" &
		// wobjXMLRequest.selectSingleNode("//CLIDOMPROVICOD").Text &
		// "</CLIDOMPROVICOD>"
		// End If
		mvarRequest = mvarRequest + "<CLIDOMPAISSCOD>00</CLIDOMPAISSCOD>";

		// Datos de tarjeta / CBU
		mvarRequest = mvarRequest + "<CUENTDC>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//COBROTIP")) + "</CUENTDC>";
		if (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//COBROTIP")).equals("DB")) {
			mvarRequest = mvarRequest + "<CUENNUME>" + Strings.mid(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CUENNUME")), 1, 16)
					+ "</CUENNUME>";
		} else if ((XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//COBROTIP")).equals("CC"))
				|| (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//COBROTIP")).equals("CA"))) {
			// MHC: Debito en cuenta CC o CA
			if (Strings.len(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CUENNUME"))) > 16) {
				mvarRequest = mvarRequest + "<CUENNUME>" + Strings.mid(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CUENNUME")), 1, 16)
						+ "</CUENNUME>";
			} else {
				mvarRequest = mvarRequest + "<CUENNUME>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CUENNUME")) + "</CUENNUME>";
			}
			mvarRequest = mvarRequest + "<BANCOCODCUE>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CANAL")) + "</BANCOCODCUE>";
			mvarRequest = mvarRequest + "<SUCURCODCUE>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CANAL_SUCURSAL")) + "</SUCURCODCUE>";
		} else {
			mvarRequest = mvarRequest + "<CUENNUME>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CUENNUME")) + "</CUENNUME>";
		}

		// ASUMO QUE ES PLANNCOD
		if (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PLANNCOD")).equals("4")) {
			mvarRequest = mvarRequest + "<VENCIANN>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VENCIANN")) + "</VENCIANN>";
			mvarRequest = mvarRequest + "<VENCIMES>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VENCIMES")) + "</VENCIMES>";
		} else if (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//COBROTIP")).equals("DB")) {
			// VERIFICAR CAMPO
			mvarRequest = mvarRequest + "<VENCIMES>" + Strings.mid(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VENCIMES")), 17, 2)
					+ "</VENCIMES>";
			// VERIFICAR CAMPO
			mvarRequest = mvarRequest + "<VENCIANN>" + Strings.mid(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//VENCIANN")), 19, 4)
					+ "</VENCIANN>";
		} else if ((XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//COBROTIP")).equals("CA"))
				|| (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//COBROTIP")).equals("CC"))) {
			// MHC: Debito en cuenta CC o CA
			mvarRequest = mvarRequest + "<VENCIMES></VENCIMES>";
			if (Strings.len(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CUENNUME"))) > 16) {
				mvarLargo = Strings.len(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CUENNUME"))) - 16;
				mvarRequest = mvarRequest + "<VENCIANN>" + Strings.mid(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CUENNUME")), 17, mvarLargo)
						+ "</VENCIANN>";
			} else {
				mvarRequest = mvarRequest + "<VENCIANN></VENCIANN>";
			}
		}
		// mvarRequest = mvarRequest & "<TARJECOD>4</TARJECOD>"
		// Hijos / Conductores
		mvarRequest = mvarRequest + "<CONDUCTORES>";
		wvarXMLHijos = new XmlDomExtended();
		if ( hijosNode != null ) {
			wvarXMLHijos.loadXML(XmlDomExtended.prettyPrint(hijosNode));
		}
		y = 1;
		// Primero doy de alta al dueño y despues itero a los hijos
		mvarRequest = mvarRequest + "<CONDUCTOR>";
		mvarRequest = mvarRequest + "<CONDUSEC>" + y + "</CONDUSEC>";
		mvarRequest = mvarRequest + "<CONDUDOC>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//NUMEDOCU")) + "</CONDUDOC>";
		mvarRequest = mvarRequest + "<CONDUTDO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//TIPODOCU")) + "</CONDUTDO>";
		mvarRequest = mvarRequest + "<APELLIDOHIJO><![CDATA[" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CLIENAP1")) + "]]></APELLIDOHIJO>";
		mvarRequest = mvarRequest + "<NOMBREHIJO><![CDATA[" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CLIENNOM")) + "]]></NOMBREHIJO>";
		mvarRequest = mvarRequest + "<SEXOHIJO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//SEXO")) + "</SEXOHIJO>";
		mvarRequest = mvarRequest + "<ESTADOHIJO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//ESTADO")) + "</ESTADOHIJO>";
		mvarRequest = mvarRequest + "<NACIMHIJO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//NACIMDIA")) + "/"
				+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//NACIMMES")) + "/"
				+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//NACIMANN")) + "</NACIMHIJO>";
		mvarRequest = mvarRequest + "<EXCLUIDO>N</EXCLUIDO>";
		mvarRequest = mvarRequest + "</CONDUCTOR>";

		for (int nx = 0; nx < wvarXMLHijos.selectNodes("//HIJO").getLength(); nx++) {
			x = wvarXMLHijos.selectNodes("//HIJO").item(nx);
			XmlDomExtended wvarXMLHIJO = new XmlDomExtended();
			wvarXMLHIJO.loadXML(XmlDomExtended.prettyPrint(x));
			y = y + 1;
			mvarRequest = mvarRequest + "<CONDUCTOR>";
			mvarRequest = mvarRequest + "<CONDUSEC>" + y + "</CONDUSEC>";
			mvarRequest = mvarRequest + "<CONDUTDO>1</CONDUTDO>";
			mvarRequest = mvarRequest + "<CONDUDOC>0</CONDUDOC>";
			mvarRequest = mvarRequest + "<APELLIDOHIJO><![CDATA[" + XmlDomExtended.getText(wvarXMLHIJO.selectSingleNode("./APELLIDOHIJO"))
					+ "]]></APELLIDOHIJO>";
			mvarRequest = mvarRequest + "<NOMBREHIJO><![CDATA[" + XmlDomExtended.getText(wvarXMLHIJO.selectSingleNode("./NOMBREHIJO")) + "]]></NOMBREHIJO>";
			mvarRequest = mvarRequest + "<SEXOHIJO>" + XmlDomExtended.getText(wvarXMLHIJO.selectSingleNode("./SEXOHIJO")) + "</SEXOHIJO>";
			mvarRequest = mvarRequest + "<ESTADOHIJO>" + XmlDomExtended.getText(wvarXMLHIJO.selectSingleNode("./ESTADOHIJO")) + "</ESTADOHIJO>";
			mvarRequest = mvarRequest + "<NACIMHIJO>" + Strings.mid(XmlDomExtended.getText(wvarXMLHIJO.selectSingleNode("./NACIMHIJO")), 1, 2) + "/"
					+ Strings.mid(XmlDomExtended.getText(wvarXMLHIJO.selectSingleNode("./NACIMHIJO")), 3, 2) + "/"
					+ Strings.mid(XmlDomExtended.getText(wvarXMLHIJO.selectSingleNode("./NACIMHIJO")), 5, 4) + "</NACIMHIJO>";
			mvarRequest = mvarRequest + "<EXCLUIDO>N</EXCLUIDO>";
			// If x.selectSingleNode("./INCLUIDO").Text = "0" Then 'VERIFICAR
			// CAMPO
			// mvarExcluido = "S"
			// Else
			// mvarExcluido = "N"
			// End If
			// mvarRequest = mvarRequest & "<EXCLUIDO>" & mvarExcluido &
			// "</EXCLUIDO>"
			mvarRequest = mvarRequest + "<CONDUVIN>HI</CONDUVIN>";
			mvarRequest = mvarRequest + "</CONDUCTOR>";
		}

		wvarXMLASEGURADOS = new XmlDomExtended();
		// Call wvarXMLASEGURADOS.loadXML(Request.Form("XMLASEGURADOS"))
		if (!(wobjXMLRequest.selectSingleNode("//ASEGURADOS-ADIC") == (org.w3c.dom.Node) null)) {

			wvarXMLASEGURADOS.loadXML(XmlDomExtended.prettyPrint(wobjXMLRequest.selectSingleNode("//ASEGURADOS-ADIC")));

			for (int nx = 0; nx < wvarXMLASEGURADOS.selectNodes("//ASEGURADO-ADIC").getLength(); nx++) {
				x = wvarXMLASEGURADOS.selectNodes("//ASEGURADO-ADIC").item(nx);
				y = y + 1;
				XmlDomExtended wvarXMLASEGURADO_ADIC = new XmlDomExtended();
				wvarXMLASEGURADO_ADIC.loadXML(XmlDomExtended.prettyPrint(x));
				mvarRequest = mvarRequest + "<CONDUCTOR>";
				mvarRequest = mvarRequest + "<CONDUSEC>" + y + "</CONDUSEC>";
				mvarRequest = mvarRequest + "<CONDUTDO>1</CONDUTDO>";
				mvarRequest = mvarRequest + "<CONDUDOC>" + XmlDomExtended.getText(wvarXMLASEGURADO_ADIC.selectSingleNode("./DOCUMASEG")) + "</CONDUDOC>";
				mvarRequest = mvarRequest
						+ "<APELLIDOHIJO><![CDATA["
						+ Strings.mid(XmlDomExtended.getText(wvarXMLASEGURADO_ADIC.selectSingleNode("./NOMBREASEG")), 1,
								(Strings.find(1, XmlDomExtended.getText(wvarXMLASEGURADO_ADIC.selectSingleNode("./NOMBREASEG")), ",") - 1))
						+ "]]></APELLIDOHIJO>";
				mvarRequest = mvarRequest + "<SEXOHIJO></SEXOHIJO>";
				mvarRequest = mvarRequest + "<ESTADOHIJO></ESTADOHIJO>";
				mvarRequest = mvarRequest + "<NACIMHIJO></NACIMHIJO>";
				mvarRequest = mvarRequest + "<EXCLUIDO>N</EXCLUIDO>";
				mvarRequest = mvarRequest + "<CONDUVIN>AD</CONDUVIN>";
				mvarRequest = mvarRequest + "</CONDUCTOR>";
			}
		}
		wvarXMLASEGURADOS = null;
		wvarXMLHijos = null;

		mvarRequest = mvarRequest + "</CONDUCTORES>";

		// Fecha Solicitud
		mvarRequest = mvarRequest + "<FESOLANN>" + DateTime.year(DateTime.now()) + "</FESOLANN>";
		mvarRequest = mvarRequest + "<FESOLMES>" + DateTime.month(DateTime.now()) + "</FESOLMES>";
		mvarRequest = mvarRequest + "<FESOLDIA>" + DateTime.day(DateTime.now()) + "</FESOLDIA>";

		mvarRequest = mvarRequest + "<FEINGANN>" + DateTime.year(DateTime.now()) + "</FEINGANN>";
		mvarRequest = mvarRequest + "<FEINGMES>" + DateTime.month(DateTime.now()) + "</FEINGMES>";
		mvarRequest = mvarRequest + "<FEINGDIA>" + DateTime.day(DateTime.now()) + "</FEINGDIA>";

		// Datos productor
		// mvarRequest = mvarRequest & "<AGECLA>" & session("PERFIL_AS") &
		// "</AGECLA>" ' corregir
		// FJO --------
		// Agregado el 06/11/2007 para que el componente tome el DATO.
		// Anteriormente, por motivos desconocidos
		// no se estaba mandando al componente. El componente del mensaje 1540
		// espera
		// AGECLA, AGECOD.
		// FJO --------
		if (!(wobjXMLRequest.selectSingleNode("//AGECLA") == (org.w3c.dom.Node) null)) {
			// VERIFICAR CAMPO
			mvarRequest = mvarRequest + "<AGECLA>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//AGECLA")) + "</AGECLA>";
		}

		// <AGENTCLA>PR</AGENTCLA>
		// corregir
		mvarRequest = mvarRequest + "<AGENTCLA>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//AGECLA")) + "</AGENTCLA>";
		// corregir
		mvarRequest = mvarRequest + "<AGECOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//AGECOD")) + "</AGECOD>";

		// harcodeo coberturas VERIFICAR CAMPOS
		// mvarRequest = mvarRequest & mvarCoberturas
		// <COBERTURAS>
		Node coberturasNode = wobjXMLRequest.selectSingleNode("//COBERTURAS");
		if ( coberturasNode != null) {
			mvarRequest = mvarRequest + XmlDomExtended.prettyPrint(coberturasNode);
		}
		// harcodeo LO QUE FALTA
		mvarRequest = mvarRequest + "<AUPAISCOD>00</AUPAISCOD>";

		// Provincia del domicilio del cliente/auto (siempre)
		// mvarRequest = mvarRequest & "<AUPROVICOD>" & Request.form("PROVI_C")
		// & "</AUPROVICOD>"
		mvarRequest = mvarRequest + "<AUPROVICOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PROVI")) + "</AUPROVICOD>";
		// Codigo Postal del domicilio del cliente/auto (siempre)
		// VERIFICAR CAMPO
		mvarRequest = mvarRequest + "<AUDOMCPACODPO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//LOCALIDADCOD")) + "</AUDOMCPACODPO>";

		mvarRequest = mvarRequest + "<CENTRCOD>0010</CENTRCOD>";
		// VERIFICAR CAMPO (ASUMO 003)
		mvarRequest = mvarRequest + "<PLANPOLICOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PLANNCOD")) + "</PLANPOLICOD>";
		// mvarRequest = mvarRequest & "<PLANPOLICOD>003</PLANPOLICOD>"
		// mvarRequest = mvarRequest & "<PRECIOFT>" & mvarPRECIOMENSUAL &
		// "</PRECIOFT>"
		mvarRequest = mvarRequest + "<ASEG_ADIC>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//ASEG-ADIC")) + "</ASEG_ADIC>";

		// <CLIDOMCPACODPO>1073</CLIDOMCPACODPO>
		// <CLIDOMICPRE>011</CLIDOMICPRE>
		// mvarRequest = mvarRequest & "<CLIDOMCPACODPO>" &
		// Request.Form("LOCALIDADCOD") & "</CLIDOMCPACODPO>"
		// mvarRequest = mvarRequest & "<CLIDOMCPACODPO>" &
		// Request.Form("CODPOSTAL") & "</CLIDOMCPACODPO>"
		// If Not wobjXMLRequest.selectSingleNode("//CLIDOMLOCALCOD") Is Nothing
		// Then
		// VERIFICAR CAMPO
		mvarRequest = mvarRequest + "<CLIDOMCPACODPO>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//LOCALIDADCOD")) + "</CLIDOMCPACODPO>";
		// End If
		// VERIFICAR NO SE UTILIZA
		mvarRequest = mvarRequest + "<CLIDOMICPRE>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//TELCODCOR")) + "</CLIDOMICPRE>";
		// VERIFICAR CAMPO
		mvarRequest = mvarRequest + "<CLIDOMICTLF>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//TELCODCOR"))
				+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//TELNROCOR")) + "</CLIDOMICTLF>";
		// mvarRequest = mvarRequest & "<CLIDOMICTLF>" &
		// Request.Form("PREFTELEFNUM1") & Request.Form("TELEFNUM1") &
		// "</CLIDOMICTLF>"
		// PRENDASINO = N VERIFICAR
		// If Request.Form("PRENDASINO") = "S" Then
		// mvarRequest = mvarRequest & "<SWACREPR>1</SWACREPR>"
		// Else
		mvarRequest = mvarRequest + "<SWACREPR>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//ACRE-PREN")) + "</SWACREPR>";
		// End If
		if (wobjXMLRequest.selectSingleNode("//CANAL_SUCURSAL") == (org.w3c.dom.Node) null) {
			mvarRequest = mvarRequest + "<SUCURCOD>8888</SUCURCOD>";
		} else {
			mvarRequest = mvarRequest + "<SUCURCOD>" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CANAL_SUCURSAL")) + "</SUCURCOD>";
		}
		// mvarRequest = mvarRequest & "<SUCURCOD>" & Request.form("SUCURCOD") &
		// "</SUCURCOD>"
		mvarRequest = mvarRequest + "<VIENEDMS>1</VIENEDMS>";
		mvarRequest = mvarRequest + "<PLANPAGOCOD>998</PLANPAGOCOD>";

		// DA - 20/02/2007: mando los datos del apoderado (exigidos por la UIF).
		// <CLIENIVA>3</CLIENIVA><COBROCOD>1</COBROCOD>
		// wvarDomiApodAIS = Strings.replace(wvarDomiApodAIS,
		// "<DOMICCAL> </DOMICCAL>", "<DOMICCAL>PAR</DOMICCAL>") 'VERIFICAR
		// CAMPO (ASUMO PAR)
		// mvarRequest = mvarRequest & "<DOMICCAL>PAR</DOMICCAL>" 'VERIFICAR
		// CAMPO (ASUMO <DOMICCAL>PAR</DOMICCAL>)
		// mvarRequest = mvarRequest & "<APOD>" & wvarPersApodAIS &
		// wvarDomiApodAIS & "<OCUPACOD>" & Request.Form("OCUPACODAPOD") &
		// "</OCUPACOD></APOD>"
		// VERIFICAR CAMPO (OCUPACOD ASUMO VACIO)
		mvarRequest = mvarRequest + "<APOD>00<OCUPACOD/></APOD>";
		// Inicio - Actualización 2
		mvarRequest = mvarRequest + "<PORTAL>WS</PORTAL>";
		mvarRequest = "<Request>" + Strings.toUpperCase(mvarRequest) + "</Request>";
		String mvarDatosPlan = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_PLANNCOD))
				+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_FRANQCOD)));
		mvarRequest = Strings.replace(mvarRequest, "</Request>", "<DATOSPLAN>" + mvarDatosPlan + "</DATOSPLAN></Request>");
		return mvarRequest;
	}

	private String fncObtenerCertisecSQL(String pvarRamopcod, String pvarPolizann, String pvarPolizsec,
			String pvarCertipol, String pvarCertiann) throws XmlDomExtendedException {

		String mvarRequest = "<Request>" +
				"<DEFINICION>SPSNCV_PROD_NUMERADOR_COTIZACION_BROKER.xml</DEFINICION>" +
				"<RAMOPCOD>" + pvarRamopcod + "</RAMOPCOD>" +
				"<POLIZANN>" + pvarPolizann + "</POLIZANN>" +
				"<POLIZSEC>" + pvarPolizsec + "</POLIZSEC>" +
				"<CERTIPOL>" + pvarCertipol	+ "</CERTIPOL>" +
				"<CERTIANN>" + pvarCertiann + "</CERTIANN>" +
				"</Request>";

        logger.log(Level.FINE, getClass().getName() + ": por llamar a SP SPSNCV_PROD_NUMERADOR_COTIZACION_BROKER: " + mvarRequest);
        OVLBASQLGen sql = new OVLBASQLGen();
        StringHolder mvarResponseSH = new StringHolder();
        sql.IAction_Execute(mvarRequest, mvarResponseSH, "" );
        String wvarResponse = mvarResponseSH.getValue();
        logger.log(Level.FINE, getClass().getName() + ": resultado de SP SPSNCV_PROD_NUMERADOR_COTIZACION_BROKER: " + wvarResponse);
        
        // Para pruebas
//        wvarResponse = "<Response><Estado resultado='true'/><REGS><REG><CERTISEC>1608</CERTISEC></REG></REGS></Response>";

        XmlDomExtended wobjXMLResponse = new XmlDomExtended();
        wobjXMLResponse.loadXML( wvarResponse );

        //Analiza resultado del SP
        if( ! (wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) ) {
          if( XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) ) {
        		  return XmlDomExtended.getText(wobjXMLResponse.selectSingleNode("//CERTISEC"));
          } 
        }
        
        return "0";
	}

	/**
	 * Emite la propuesta, armando el request a partir de los parámetros marcados con EMI abajo.
	 * 
	 * 
	 * @param respuestaEmisionSH Parámetro de salida, contiene la respuesta de la emisión
	 * @param mvarRAMOPCOD EMI
	 * @param mvarPOLIZANN EMI
	 * @param mvarPOLIZSEC EMI
	 * @param mvarCERTIPOL EMI
	 * @param mvarCERTIANN EMI
	 * @param mvarCertiSec EMI
	 * @param mvarSUPLENUM EMI
	 * @param usuarcod EMI
	 * @return OK si la propuesta fue emitida o retenida, ER en otro caso
	 * 
	 * @throws OSBConnectorException
	 * @throws MalformedURLException
	 * @throws XmlDomExtendedException
	 */
	protected String emitirPropuesta(StringHolder respuestaEmisionSH, String mvarRAMOPCOD, String mvarPOLIZANN, String mvarPOLIZSEC, String mvarCERTIPOL,
			String mvarCERTIANN, String mvarCertiSec, String mvarSUPLENUM, String usuarcod) throws OSBConnectorException, MalformedURLException,
			XmlDomExtendedException {

		String mvarRequestEmision = "";
		String mvarProdok = null;
		
		mvarRequestEmision = mvarRequestEmision + "<Request>";
		mvarRequestEmision = mvarRequestEmision + "<USUARIO>" + usuarcod + "</USUARIO>";
		mvarRequestEmision = mvarRequestEmision + "<RAMOPCOD>" + mvarRAMOPCOD + "</RAMOPCOD>";
		mvarRequestEmision = mvarRequestEmision + "<POLIZANN>" + mvarPOLIZANN + "</POLIZANN>";
		mvarRequestEmision = mvarRequestEmision + "<POLIZSEC>" + mvarPOLIZSEC + "</POLIZSEC>";
		mvarRequestEmision = mvarRequestEmision + "<CERTIPOL>" + mvarCERTIPOL + "</CERTIPOL>";
		mvarRequestEmision = mvarRequestEmision + "<CERTIANN>" + mvarCERTIANN + "</CERTIANN>";
		mvarRequestEmision = mvarRequestEmision + "<CERTISEC>" + mvarCertiSec + "</CERTISEC>";
		mvarRequestEmision = mvarRequestEmision + "<SUPLENUM>" + mvarSUPLENUM + "</SUPLENUM>";
		mvarRequestEmision = mvarRequestEmision + "</Request>";

		logger.log(Level.FINE, String.format("GetValidacionSolAU por enviar a lbaw_EmitirPropuesta: [%s]", mvarRequestEmision));
		String mvarResponseEmision = getOsbConnector().executeRequest("lbaw_EmitirPropuesta", mvarRequestEmision);
		logger.log(Level.FINE, String.format("GetValidacionSolAU respuesta de lbaw_EmitirPropuesta: [%s]", mvarResponseEmision));

		String responseEmisionOKTemplate = "<EMISION>" +
				"<Estpol>%s</Estpol>" +
				"<MotivoRet>%s</MotivoRet>" +
				"<PolizaNo>%s</PolizaNo>" +
				"<TipoCerti>%s</TipoCerti>" +
				"<TipoInspe>%s</TipoInspe>" +
				"<TipoRastreo>%s</TipoRastreo>" +
				"</EMISION>";
		
		
		XmlDomExtended mobjXMLDocEmision = new XmlDomExtended();
		mobjXMLDocEmision.loadXML(mvarResponseEmision);

		if (mobjXMLDocEmision.selectSingleNode("//Estado/@resultado") != null)  {
			if (XmlDomExtended.getText(mobjXMLDocEmision.selectSingleNode("//Estado/@resultado")).equals("true")) {
				String estPol = "";
				String nroPoliza = "0001" + 
						XmlDomExtended.getText(mobjXMLDocEmision.selectSingleNode("//POLIZA/RAMOPCOD")) +
						XmlDomExtended.getText(mobjXMLDocEmision.selectSingleNode("//POLIZA/POLIZANN")) +
						XmlDomExtended.getText(mobjXMLDocEmision.selectSingleNode("//POLIZA/POLIZSEC")) +
						XmlDomExtended.getText(mobjXMLDocEmision.selectSingleNode("//POLIZA/CERTIPOL")) +
						XmlDomExtended.getText(mobjXMLDocEmision.selectSingleNode("//POLIZA/CERTIANN")) +
						XmlDomExtended.getText(mobjXMLDocEmision.selectSingleNode("//POLIZA/CERTISEC"));
						
				String tipoCerti = XmlDomExtended.getText(mobjXMLDocEmision.selectSingleNode("//TIPOCERTI"));
				String inspeccion = XmlDomExtended.getText(mobjXMLDocEmision.selectSingleNode("//INSPECCION")).equalsIgnoreCase("S") ? "1" : "0";
				String dispositivo = XmlDomExtended.getText(mobjXMLDocEmision.selectSingleNode("//DISPOSITIVO")).equalsIgnoreCase("S") ? "1" : "0";
	
				Node n = mobjXMLDocEmision.selectSingleNode("//ERRORES");
				//Pido getTextContent por si viene más de un tag "ERROR", así concatena todos.
				String motivoRet = n == null ? "" : n.getTextContent();
				if (XmlDomExtended.getText(mobjXMLDocEmision.selectSingleNode("//TEXTO")).trim().equals("PROPUESTA EMITIDA")) {
					estPol = "E";
					mvarProdok = "OK";
				} else if (XmlDomExtended.getText(mobjXMLDocEmision.selectSingleNode("//TEXTO")).equals("PROPUESTA RETENIDA")) {
					estPol = "R";
					mvarProdok = "OK";
				} else {
					Node nt = mobjXMLDocEmision.selectSingleNode("//TEXTO");
					estPol = nt == null ? "" : XmlDomExtended.getText(nt);
					logger.log(Level.WARNING, "TEXTO desconocido en la emisión: " + estPol);
					mvarProdok = "ER";
				}
				String responseEmitida = String.format(responseEmisionOKTemplate,estPol, motivoRet, nroPoliza,tipoCerti, inspeccion, dispositivo );
				respuestaEmisionSH.set(responseEmitida);
			} else {
				mvarProdok = "ER";
				String message = XmlDomExtended.getText(mobjXMLDocEmision.selectSingleNode("//Estado/@mensaje"));
				respuestaEmisionSH.set(message);
			}
		} else {
			mvarProdok = "ER";
			String message = "Respuesta con formato inválido";
			respuestaEmisionSH.set(message);
		}
		logger.log(Level.FINEST, "En emitirPropuesta respuestaEmisionSH es " + respuestaEmisionSH.getValue());
		return mvarProdok;
	}

	private boolean validarEnBase(String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, StringHolder pvarRes) throws XmlDomExtendedException,
			SQLException, IOException, OSBConnectorException {
		boolean wvarError = false;
		XmlDomExtended wobjXMLRequest = null;
		XmlDomExtended wobjXML = null;
		int wvarContNode = 0;
		String mvarWDB_CODINST = "";
		String mvarWDB_NRO_OPERACION_BROKER = "";
		String mvarWDB_CODPROVCO = "";
		String mvarWDB_CODPOSTCO = "";
		String mvarWDB_COBROCOD = "";
		String mvarWDB_COBROTIP = "";
		String mvarWDB_SUMAASEG = "";
		String mvarWDB_COLOR = "";
		String mvarWDB_DOCUMTIP_TIT = "";
		String mvarWDB_PLANNCOD = "";
		String mvarWDB_FRANQCOD = "";
		String mvarWDB_INSPECCION = "";
		String mvarWDB_RASTREO = "";
		String mvarWDB_CODIZONA = "";
		String mvarWDB_CATEGO = "";

		String campoNombre = "";

		wvarError = false;

		wobjXMLRequest = new XmlDomExtended();
		wobjXMLRequest.loadXML(pvarRequest);

		mvarWDB_CODINST = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_CODINST));
		mvarWDB_NRO_OPERACION_BROKER = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_NRO_OPERACION_BROKER));
		mvarWDB_CODPROVCO = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_CODPROVCO));
		mvarWDB_CODPOSTCO = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_CODPOSTCO));
		mvarWDB_COBROCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_COBROCOD));
		mvarWDB_COBROTIP = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_COBROTIP)));
		mvarWDB_CODIZONA = "0";
		mvarWDB_SUMAASEG = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_SUMAASEG));
		mvarWDB_COLOR = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_COLOR));
		mvarWDB_DOCUMTIP_TIT = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_DOCUMTIP_TIT));
		mvarWDB_PLANNCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_PLANNCOD));
		mvarWDB_FRANQCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_FRANQCOD));
		mvarWDB_INSPECCION = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_INSPECCION));
		mvarWDB_RASTREO = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_RASTREO));
		mvarWDB_CATEGO = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_CATEGO));

		java.sql.Connection jdbcConn = null;
		try {
			JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
			jdbcConn = connFactory.getJDBCConnection(General.mcteDB);
			String sp = "{? = call SPSNCV_BRO_VALIDA_SOL_SCO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			CallableStatement ps = jdbcConn.prepareCall(sp);
			int posiSP = 1;

			// Preparo los parámetros que van a estar de salida tambien
			ps.registerOutParameter(posiSP++, Types.VARCHAR);
			ps.registerOutParameter(16, Types.INTEGER);
			ps.registerOutParameter(23, Types.INTEGER);
			ps.registerOutParameter(24, Types.INTEGER);
			ps.registerOutParameter(25, Types.INTEGER);
			ps.registerOutParameter(26, Types.VARCHAR);
			ps.setInt(posiSP++, Integer.parseInt(mvarWDB_CODINST));
			ps.setInt(posiSP++, Integer.parseInt(mvarWDB_NRO_OPERACION_BROKER));
			ps.setInt(posiSP++, Integer.parseInt(mvarWDB_CODPROVCO));
			ps.setInt(posiSP++, Integer.parseInt(mvarWDB_CODPOSTCO));
			ps.setInt(posiSP++, Integer.parseInt(mvarWDB_COBROCOD));
			ps.setString(posiSP++, String.valueOf(mvarWDB_COBROTIP));
			ps.setInt(posiSP++, Integer.parseInt(Strings.split(mvarWDB_SUMAASEG, ".", -1)[0]));
			ps.setInt(posiSP++, Integer.parseInt(mvarWDB_COLOR));
			ps.setInt(posiSP++, Integer.parseInt(mvarWDB_DOCUMTIP_TIT));
			ps.setInt(posiSP++, Integer.parseInt(mvarWDB_PLANNCOD));
			ps.setString(posiSP++, String.valueOf(mvarWDB_FRANQCOD));
			ps.setInt(posiSP++, Integer.parseInt(mvarWDB_INSPECCION));
			ps.setInt(posiSP++, Integer.parseInt(mvarWDB_RASTREO));
			ps.setInt(posiSP++, 0);
			ps.setInt(posiSP++, Integer.parseInt(mvarWDB_CODIZONA));
			ps.setInt(posiSP++, Integer.parseInt(mvarWDB_CATEGO));
			ps.setInt(posiSP++, 0);
			ps.setInt(posiSP++, 0);
			ps.setInt(posiSP++, 0);
			ps.setInt(posiSP++, 0);
			ps.setInt(posiSP++, 0);
			ps.setInt(posiSP++, 0);
			ps.setInt(posiSP++, 0);
			ps.setInt(posiSP++, 0);
			ps.setString(posiSP++, "");
			boolean result = ps.execute();

			if (result) {
				// El primero es un ResultSet
			} else {
				// Salteo el "updateCount"
				int uc = ps.getUpdateCount();
				if (uc == -1) {
					// System.out.println("No hay resultados");
				}
				result = ps.getMoreResults();
			}
			java.sql.ResultSet cursor = ps.getResultSet();

			if (!(wvarError)) {
				wobjXML = new XmlDomExtended();
				wobjXML.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteArchivoAUSSOL_XML));
				// SI NO HUBO ERROR AGREGO LOS DATOS AL XML DE ENTRADA PARA LA
				// RESPUESTA
				if (cursor.next()) {
					String rtado = cursor.getString(1);
					if (Strings.toUpperCase(Strings.trim(rtado.trim())).equals("OK")) {

						for (wvarContNode = 0; wvarContNode <= cursor.getMetaData().getColumnCount() - 1; wvarContNode++) {
							if (cursor.getMetaData().getColumnName(wvarContNode + 1).equals("")) {
								campoNombre = "RESULTADO" + String.valueOf(wvarContNode + 1);
							} else {
								campoNombre = cursor.getMetaData().getColumnName(wvarContNode + 1);
							}
							wobjXMLRequest.selectSingleNode("//Request").appendChild(
									wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, Strings.toUpperCase(campoNombre), ""));
							if (cursor.getMetaData().getColumnType(wvarContNode + 1) == java.sql.Types.NUMERIC
									&& cursor.getMetaData().getPrecision(wvarContNode + 1) == 18) {
								XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//" + Strings.toUpperCase(campoNombre)),
										String.valueOf(cursor.getInt(wvarContNode + 1) * 100));
							} else {
								XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//" + Strings.toUpperCase(campoNombre)),
										Strings.trim(cursor.getString(wvarContNode + 1)));
							}
						}
						// SE MUEVE DESDE EL PRINCIPIO DEL IF A ACÁ... PORQUE SE
						// CIERRA EL CURSOR
						// Obtengo los parámetros modificados
						Integer codiZon = ps.getInt(16);
						Integer nroCot = ps.getInt(23);
						Integer sumaMinima = ps.getInt(24);
						Integer sumaLBA = ps.getInt(25);
						String usuarCod = ps.getString(26);

						wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "CODZONA", ""));
						XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//CODZONA"), String.valueOf(codiZon));

						wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "WDB_NROCOT", ""));
						XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//WDB_NROCOT"), nroCot.toString());
						wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "SUMA_MINIMA", ""));
						XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//SUMA_MINIMA"), String.valueOf(sumaMinima * 100));
						wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "CERTISEC", ""));
						XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//CERTISEC"), "0");
						wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "SUMALBA", ""));
						XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//SUMALBA"), String.valueOf(sumaLBA));
						wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "USUARCOD", ""));
						XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//USUARCOD"), String.valueOf(usuarCod));

						pvarRes.set(XmlDomExtended.marshal(wobjXMLRequest.getDocument().getDocumentElement()));
						wvarError = !validaContraCotizacion(XmlDomExtended.marshal(wobjXMLRequest.getDocument().getDocumentElement()), wvarMensaje, wvarCodErr,
								pvarRes);
						logger.log(Level.FINEST, "wvarError después de llamada a validaContraCotizacion es: " + wvarError);
					} else {
						// error
						wvarError = true;
						if (wobjXML.selectNodes(("//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + rtado + "']")).getLength() > 0) {
							wvarMensaje.set(XmlDomExtended.getText(wobjXML.selectSingleNode("//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + rtado
									+ "']/TEXTOERROR")));
							wvarCodErr.set(XmlDomExtended.getText(wobjXML.selectSingleNode("//ERRORES/AUTOSCORING/RESPUESTA/ERROR[CODIGO='" + rtado
									+ "']/VALORRESPUESTA")));
							pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg = \"" + wvarMensaje + "\"></LBA_WS>");
						} else {
							wvarCodErr.set(cursor.getString(1));
							wvarMensaje.set("No existe descripcion para este error");
							pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg = \"" + wvarMensaje + "\"></LBA_WS>");
						}
					}
				} else {
					// error
					wvarError = true;
					wvarCodErr.set(-16);
					wvarMensaje.set("No se retornaron datos de la base");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				}
			}
			return !wvarError;
		} finally {
			try {
				if (jdbcConn != null)
					jdbcConn.close();
			} catch (Exception e) {
				logger.log(Level.WARNING, "Exception al hacer un close", e);
			}
		}
	}

	private boolean validaContraCotizacion(String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, StringHolder pvarRes) throws XmlDomExtendedException,
			SQLException, OSBConnectorException, IOException {
		boolean validaContraCotizacion = false;
		Variant vbLogEventTypeError = new Variant();
		XmlDomExtended wobjXMLRequest = null;
		XmlDomExtended wobjXMLRequestCotis = null;
		XmlDomExtended wobjXMLCotizacion = null;
		XmlDomExtended wobjXMLValid = null;
		org.w3c.dom.NodeList wobjXMLNodeList = null;
		org.w3c.dom.NodeList wobjXMLListCotiz = null;
		org.w3c.dom.Node wobjXMLNode = null;
		org.w3c.dom.Node wobjXMLNodeCotiz = null;
		Object wobjClass = null;
		// Recordset wrstRes = null;
		boolean wvarChequeonuevas = false;
		boolean wvarChequeoLocalidad = false;
		boolean wvarChequeoTarjeta = false;
		String wvarTieneHijos = "";
		String wvarNombrePlan = "";
		boolean wvarChequeoHijos = false;
		boolean wvarChequeoAccesorios = false;
		int wvarcounter = 0;
		String wvarResponse = "";
		int wvarStep = 0;
		String wvarNuevoHijos = "";
		String mvarWDB_CODINST = "";
		String mvarWDB_NRO_OPERACION_BROKER = "";
		String mvarRAMOPCOD = "";
		String mvarPOLIZANN = "";
		String mvarPOLIZSEC = "";
		String mvarCERTIPOL = "";
		String mvarCERTIANN = "";
		String mvarCertiSec = "";
		String mvarDatosPlan = "";

		wobjXMLRequest = new XmlDomExtended();
		wobjXMLRequest.loadXML(pvarRequest);
		mvarWDB_CODINST = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_CODINST));
		mvarWDB_NRO_OPERACION_BROKER = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_NRO_OPERACION_BROKER));

		java.sql.Connection jdbcConn = null;

		try {
			JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
			jdbcConn = connFactory.getJDBCConnection(General.mcteDB);
			String sp = "{call SPSNCV_BRO_SELECT_OPER_ESP(?,?,?,?)}";
			CallableStatement ps = jdbcConn.prepareCall(sp);
			ps.setInt(1, Integer.parseInt(mvarWDB_CODINST.equals("") ? null : mvarWDB_CODINST));
			ps.setInt(2, Integer.parseInt(mvarWDB_NRO_OPERACION_BROKER.equals("") ? null : mvarWDB_NRO_OPERACION_BROKER));
			ps.setString(3, String.valueOf("C"));
			ps.setString(4, String.valueOf("OK"));

			boolean result = ps.execute();

			if (result) {
				// El primero es un ResultSet
			} else {
				// Salteo el "updateCount"
				int uc = ps.getUpdateCount();
				if (uc == -1) {
					// System.out.println("No hay resultados");
				}
				result = ps.getMoreResults();
			}

			java.sql.ResultSet cursor = ps.getResultSet();
			cursor.next();
			// Cargo el campo de XML de cotizacion
			wobjXMLCotizacion = new XmlDomExtended();
			wobjXMLCotizacion.loadXML(cursor.getString(15));
			mvarRAMOPCOD = cursor.getString(8);
			mvarPOLIZANN = cursor.getString(9);
			mvarPOLIZSEC = cursor.getString(10);
			mvarCERTIPOL = cursor.getString(11);
			mvarCERTIANN = cursor.getString(12);
			mvarCertiSec = cursor.getString(13);
		} finally {
			try {
				if (jdbcConn != null)
					jdbcConn.close();
			} catch (Exception e) {
				logger.log(Level.WARNING, "Exception al hacer un close", e);
			}
		}

		if (Obj.toInt(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_Provi)))) != 1) {
			if (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_LocalidadCod))).equals(
					XmlDomExtended.getText(wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_LocalidadCod))))) {
				wvarChequeoLocalidad = true;
			} else {
				wvarChequeoLocalidad = false;
			}
		} else {
			wvarChequeoLocalidad = true;
		}

		// Si es COBROCOD = 4 no compara el Tipo de Tarjeta
		if (Obj.toInt(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_COBROCOD)))) != 4) {
			if (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_COBROTIP))).equals(
					XmlDomExtended.getText(wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_COBROTIP))))) {
				wvarChequeoTarjeta = true;
			} else {
				wvarChequeoTarjeta = false;
			}
		} else {
			wvarChequeoTarjeta = true;
		}

		// Chequeo los nodos de hijos
		wobjXMLNodeList = wobjXMLRequest.selectNodes(mcteNodos_Hijos);
		wobjXMLListCotiz = wobjXMLCotizacion.selectNodes(mcteNodos_Hijos);
		wvarChequeoHijos = true;
		if (!((wobjXMLNodeList == (org.w3c.dom.NodeList) null)) && !((wobjXMLListCotiz == (org.w3c.dom.NodeList) null))) {
			for (wvarcounter = 0; wvarcounter <= (wobjXMLNodeList.getLength() - 1); wvarcounter++) {
				wobjXMLNode = wobjXMLNodeList.item(wvarcounter);
				wobjXMLNodeCotiz = wobjXMLCotizacion.selectNodes(mcteNodos_Hijos).item(wvarcounter);
				if (wobjXMLNodeList.getLength() != wobjXMLListCotiz.getLength()) {
					wvarCodErr.set(-165);
					wvarMensaje.set("Datos de Hijos: existe una diferencia entre la Cotizacion y la Solicitud");
					pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
					validaContraCotizacion = false;
					return validaContraCotizacion;
				}

				if (!((XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "NACIMHIJO") == null))
						&& !((XmlDomExtended.Node_selectSingleNode(wobjXMLNodeCotiz, "NACIMHIJO") == null))
						&& !((XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "SEXOHIJO") == null))
						&& !((XmlDomExtended.Node_selectSingleNode(wobjXMLNodeCotiz, "SEXOHIJO") == (org.w3c.dom.Node) null))
						&& !((XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "ESTADOHIJO") == null))
						&& !((XmlDomExtended.Node_selectSingleNode(wobjXMLNodeCotiz, "ESTADOHIJO") == null))
						&& !((XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "EDADHIJO") == null))
						&& !((XmlDomExtended.Node_selectSingleNode(wobjXMLNodeCotiz, "EDADHIJO") == null))) {
					if (!((XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "NACIMHIJO")).equals(XmlDomExtended.getText(XmlDomExtended
							.Node_selectSingleNode(wobjXMLNodeCotiz, "NACIMHIJO"))))
							&& (XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "SEXOHIJO")).equals(XmlDomExtended
									.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLNodeCotiz, "SEXOHIJO"))))
							&& (XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "ESTADOHIJO")).equals(XmlDomExtended
									.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLNodeCotiz, "ESTADOHIJO")))) && (XmlDomExtended.getText(XmlDomExtended
							.Node_selectSingleNode(wobjXMLNode, "EDADHIJO")).equals(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(
							wobjXMLNodeCotiz, "EDADHIJO")))))) {
						wvarChequeoHijos = false;
					}
				} else {
					wvarChequeoHijos = false;
				}
				wobjXMLNode = (org.w3c.dom.Node) null;
				wobjXMLNodeCotiz = (org.w3c.dom.Node) null;
			}
			wobjXMLNodeList = (org.w3c.dom.NodeList) null;
			wobjXMLListCotiz = (org.w3c.dom.NodeList) null;
		}

		wvarChequeoAccesorios = true;
		if ((wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_NacimAnn)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_NacimMes)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_NacimDia)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_Sexo)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_Estado)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_PRODUCTOR)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_CampaCod)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_ModeAutCod)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_SUMAASEG)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_KMsrngCod)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_EfectAnn)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_SiGarage)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_Siniestros)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_Gas)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_ClubLBA)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_Provi)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_COBROCOD)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_CLIENIVA)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_VEHDES)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_DESTRUCCION_80)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_Luneta)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_ClubEco)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_Robocont)) == (org.w3c.dom.Node) null)
				|| (wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_Granizo)) == (org.w3c.dom.Node) null)) {
			wvarCodErr.set(-96);
			wvarMensaje.set("La ultima cotizacion se guardo con error debido a que falta un campo");
			pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
			validaContraCotizacion = false;
			return validaContraCotizacion;
		}
		wvarChequeonuevas = true;
		wvarCodErr.set(0);
		if (!((XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_NacimAnn))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_NacimAnn)))))
				&& (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_NacimMes))).equals(XmlDomExtended.getText(wobjXMLCotizacion
						.selectSingleNode(("//" + mcteParam_NacimMes))))) && (XmlDomExtended.getText(wobjXMLRequest
				.selectSingleNode(("//" + mcteParam_NacimDia))).equals(XmlDomExtended.getText(wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_NacimDia))))))) {
			wvarCodErr.set(-150);
			wvarMensaje.set("Fecha de Nacimiento: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_Sexo))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_Sexo)))))) {
			wvarCodErr.set(-151);
			wvarMensaje.set("Sexo: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_Estado))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_Estado)))))) {
			wvarCodErr.set(-152);
			wvarMensaje.set("Estado Civil: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_PRODUCTOR))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_PRODUCTOR)))))) {
			wvarCodErr.set(-153);
			wvarMensaje.set("Cod. de Productor: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_CampaCod))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_CampaCod)))))) {
			wvarCodErr.set(-154);
			wvarMensaje.set("Cod. de Campania: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_ModeAutCod))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_ModeAutCod)))))) {
			wvarCodErr.set(-155);
			wvarMensaje.set("Modelo de Vehiculo: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_SUMAASEG))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_SUMAASEG)))))) {
			wvarCodErr.set(-156);
			wvarMensaje.set("Suma Asegurada: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_KMsrngCod))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_KMsrngCod)))))) {
			wvarCodErr.set(-157);
			wvarMensaje.set("Rango de Kms.: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_EfectAnn))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_EfectAnn)))))) {
			wvarCodErr.set(-158);
			wvarMensaje.set("Anio del Vehiculo: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_SiGarage))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_SiGarage)))))) {
			wvarCodErr.set(-159);
			wvarMensaje.set("Guarda en Garage: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_Siniestros))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_Siniestros)))))) {
			wvarCodErr.set(-160);
			wvarMensaje.set("Cantidad de Siniestros: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_Gas))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_Gas)))))) {
			wvarCodErr.set(-161);
			wvarMensaje.set("Usa GNC: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_ClubLBA))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_ClubLBA)))))) {
			wvarCodErr.set(-162);
			wvarMensaje.set("Club LBA: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_Provi))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_Provi)))))) {
			wvarCodErr.set(-163);
			wvarMensaje.set("Cod. de Provincia: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(wvarChequeoLocalidad)) {
			wvarCodErr.set(-164);
			wvarMensaje.set("Cod. de Localidad: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(wvarChequeoHijos)) {
			wvarCodErr.set(-165);
			wvarMensaje.set("Datos de Hijos: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(wvarChequeoAccesorios)) {
			wvarCodErr.set(-166);
			wvarMensaje.set("Datos de Accesorios: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_COBROCOD))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_COBROCOD)))))) {
			wvarCodErr.set(-167);
			wvarMensaje.set("Cod. de Cobro: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(wvarChequeoTarjeta)) {
			wvarCodErr.set(-168);
			wvarMensaje.set("Datos de Tarjeta: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_CLIENIVA))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_CLIENIVA)))))) {
			wvarCodErr.set(-169);
			wvarMensaje.set("Responsabilidad frente al IVA: existe una diferencia entre la Cotizacion y la Solicitud");
		}
		if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_VEHDES))).equals(XmlDomExtended.getText(wobjXMLCotizacion
				.selectSingleNode(("//" + mcteParam_VEHDES)))))) {
			wvarCodErr.set(-170);
			wvarMensaje.set("Descripcion del Vehiculo: existe una diferencia entre la Cotizacion y la Solicitud");
		}

		if (!(wvarChequeonuevas)) {
			if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_DESTRUCCION_80)))).equals("S")) {
				wvarCodErr.set(-171);
				wvarMensaje.set("Destruc.80: El plan seleccionado no permite esta cobertura opcional");
			} else {
				if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_Luneta)))).equals("S")) {
					wvarCodErr.set(-172);
					wvarMensaje.set("Luneta: El plan seleccionado no permite esta cobertura opcional");
				} else {
					if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_Robocont)))).equals("S")) {
						wvarCodErr.set(-174);
						wvarMensaje.set("Robo Cont.: El plan seleccionado no permite esta cobertura opcional");
					} else {
						if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_Granizo)))).equals("S")) {
							wvarCodErr.set(-175);
							wvarMensaje.set("Granizo: El plan seleccionado no permite esta cobertura opcional");
						}
					}
				}
			}
		}
		if (wvarCodErr.toInt() < 0) {
			pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
			validaContraCotizacion = false;
			return validaContraCotizacion;
		}
		if (!((wobjXMLRequest.selectSingleNode(("//" + mcteParam_CLIENTIP)) == (org.w3c.dom.Node) null))
				&& !((wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_CLIENTIP)) == (org.w3c.dom.Node) null))) {
			if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_CLIENTIP))).equals(XmlDomExtended.getText(wobjXMLCotizacion
					.selectSingleNode(("//" + mcteParam_CLIENTIP)))))) {
				wvarCodErr.set(-181);
				wvarMensaje.set("Tipo de Persona: existe una diferencia entre la Cotizacion y la Solicitud");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				validaContraCotizacion = false;
				return validaContraCotizacion;
			}
		}
		if (!((wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_IBB)) == (org.w3c.dom.Node) null))
				&& !((wobjXMLCotizacion.selectSingleNode(("//" + mcteParam_IBB)) == (org.w3c.dom.Node) null))) {
			if (!(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_IBB))).equals(XmlDomExtended.getText(wobjXMLCotizacion
					.selectSingleNode(("//" + mcteParam_IBB)))))) {
				wvarCodErr.set(-181);
				wvarMensaje.set("Tipo de IBB: existe una diferencia entre la Cotizacion y la Solicitud");
				pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>");
				validaContraCotizacion = false;
				return validaContraCotizacion;
			}
		}
		pvarRes.set(pvarRequest);

		mvarDatosPlan = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//" + mcteParam_PLANNCOD))
				+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(("//" + mcteParam_FRANQCOD)));
		pvarRequest = Strings.replace(pvarRequest, "</Request>", "<DATOSPLAN>" + mvarDatosPlan + "</DATOSPLAN></Request>");
		
		
		wvarResponse = getOsbConnector().executeRequest("lbaw_OVGetCotiAUS", pvarRequest);
		
		wobjXMLRequestCotis = new XmlDomExtended();
		wobjXMLRequestCotis.loadXML(wvarResponse);
		if (XmlDomExtended.getText(wobjXMLRequestCotis.selectSingleNode("//Response/Estado/@resultado")).equals("true")) {

			wobjXMLValid = new XmlDomExtended();
			wobjXMLValid.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteArchivoAUSSOL_XML));

			if (wobjXMLRequest.selectNodes("//HIJOS/HIJO").getLength() > 0) {
				wvarTieneHijos = "_CH";
				wvarNuevoHijos = "CON_HIJOS";
			} else {
				wvarTieneHijos = "_SH";
				wvarNuevoHijos = "SIN_HIJOS";
			}

			//Saca los centavos de ambos para comparar
			String precioRequestCent = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PRECIO"));
			String precioRequest = null;
			if (precioRequestCent.indexOf(",") != -1) {
				precioRequest = precioRequestCent.substring(0, precioRequestCent.indexOf(","));
			} else {
				precioRequest = precioRequestCent;
			}

			String precioCotisCent = XmlDomExtended.getText(wobjXMLRequestCotis.selectSingleNode(("//" + wvarNuevoHijos + "/PRECIO")));
			String precioCotis;
			if (precioCotisCent.indexOf(",") != -1) {
				precioCotis = precioCotisCent.substring(0, precioCotisCent.indexOf(","));
			} else {
				precioCotis = precioCotisCent;
			}

			if (!precioRequest.equals(precioCotis)) {
				validaContraCotizacion = false;
				pvarRes.set("<LBA_WS res_code=\"-550\" res_msg=\"Existe una diferencia de precio entre la Solicitud("
						+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PRECIO")) + ")y la Cotizacion("
						+ XmlDomExtended.getText(wobjXMLRequestCotis.selectSingleNode(("//" + wvarNuevoHijos + "/PRECIO"))) + ")\"></LBA_WS>");
				return validaContraCotizacion;
			} else {
				wobjXMLRequest.selectSingleNode("//Request").appendChild(
						wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "PRECIOPLAN" + wvarTieneHijos, ""));
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//PRECIOPLAN" + wvarTieneHijos),
						XmlDomExtended.getText(wobjXMLRequestCotis.selectSingleNode("//" + wvarNuevoHijos + "/PRECIO")));

				// Se agrega Descripción del Plan
				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "PLANDES", ""));
				XmlDomExtended
						.setText(wobjXMLRequest.selectSingleNode("//PLANDES"), XmlDomExtended.getText(wobjXMLRequestCotis.selectSingleNode("//PLANNDES")));

				// Se agrega AGECLA...
				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "AGECLA", ""));
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//AGECLA"), XmlDomExtended.getText(wobjXMLRequestCotis.selectSingleNode("//AGE_CLA")));

				for (wvarcounter = 1; wvarcounter <= 30; wvarcounter++) {

					wobjXMLRequest.selectSingleNode("//Request").appendChild(
							wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "COBERCOD" + wvarcounter, ""));
					XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//COBERCOD" + wvarcounter),
							XmlDomExtended.getText(wobjXMLRequestCotis.selectSingleNode("//COBERCOD" + wvarcounter)));

					wobjXMLRequest.selectSingleNode("//Request").appendChild(
							wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "COBERORD" + wvarcounter, ""));
					XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//COBERORD" + wvarcounter),
							XmlDomExtended.getText(wobjXMLRequestCotis.selectSingleNode("//COBERORD" + wvarcounter)));

					wobjXMLRequest.selectSingleNode("//Request").appendChild(
							wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "CAPITASG" + wvarcounter, ""));
					XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//CAPITASG" + wvarcounter),
							XmlDomExtended.getText(wobjXMLRequestCotis.selectSingleNode("//CAPITASG" + wvarcounter)));

					wobjXMLRequest.selectSingleNode("//Request").appendChild(
							wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "CAPITIMP" + wvarcounter, ""));
					XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//CAPITIMP" + wvarcounter),
							XmlDomExtended.getText(wobjXMLRequestCotis.selectSingleNode("//CAPITIMP" + wvarcounter)));
				}

				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "RAMOPCOD", ""));
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//RAMOPCOD"), mvarRAMOPCOD);

				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "POLIZANN", ""));
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//POLIZANN"), mvarPOLIZANN);

				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "POLIZSEC", ""));
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//POLIZSEC"), mvarPOLIZSEC);

				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "CERTIPOL", ""));
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//CERTIPOL"), mvarCERTIPOL);

				wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "CERTIANN", ""));
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//CERTIANN"), mvarCERTIANN);

				if (wobjXMLRequest.selectSingleNode("//CERTISEC") == null) {
					wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(org.w3c.dom.Node.ELEMENT_NODE, "CERTISEC", ""));
				}
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode("//CERTISEC"), mvarCertiSec);
				// LNC - fin cabios de Actualización
				pvarRes.set("<Response><Estado resultado=\"true\" mensaje=\"\"/>" + XmlDomExtended.marshal(wobjXMLRequest.getDocument().getDocumentElement())
						+ "</Response>");
			}
			validaContraCotizacion = true;
		} else { // ! XmlDomExtended.getText(wobjXMLRequestCotis.selectSingleNode("//Response/Estado/@resultado")).equals("true")
			wvarCodErr.set(-404);
			wvarMensaje.set("Error interno al realizar la validación de la cotización. Por favor reintente.");
			Node responseNode = wobjXMLRequestCotis.selectSingleNode("//Response");
			String sourceError = XmlDomExtended.getText(responseNode).trim();
			pvarRes.set("<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg = \"" + wvarMensaje + "\">" + sourceError + "</LBA_WS>");
			validaContraCotizacion = false;
		} 
		return validaContraCotizacion;
	}

	public void Activate() throws Exception {
		//
		/* TBD mobjCOM_Context = this.GetObjectContext() ; */
		//
	}

	public boolean CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		//
		ObjectControl_CanBePooled = true;
		//
		return ObjectControl_CanBePooled;
	}

	public void Deactivate() throws Exception {
		//
	}
}
