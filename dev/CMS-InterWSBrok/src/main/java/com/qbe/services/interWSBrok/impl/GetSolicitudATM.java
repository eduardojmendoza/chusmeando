package com.qbe.services.interWSBrok.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.interWSBrok.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Implementacion de los objetos
 * Objetos del FrameWork
 */

public class GetSolicitudATM implements VBObjectClass
{
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetSolicitudATM";
  static final String mcteStoreProcSelect = "SPSNCV_BRO_SELECT_OPER_ESP";
  static final String mcteStoreProc = "SPSNCV_BRO_ALTA_OPER";
  /**
   * 10/2010 sp para descripciones del impreso
   */
  static final String mcteStoreProcImpre = "SPSNCV_BRO_SOLI_ATM_IMPRE";
  /**
   * Archivo de Configuracion
   */
  static final String mcteArchivoConfATM_XML = "LBA_PARAM_ATM.XML";
  static final String mcteArchivoHOM_XML = "LBA_VALIDACION_COT_ATM.XML";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_NROCOT = "COT_NRO";
  static final String mcteParam_VALOR = "VALOR";
  static final String mcteParam_DOMICDOM = "DOMICDOM";
  static final String mcteParam_DomicDnu = "DOMICDNU";
  static final String mcteParam_DOMICPIS = "DOMICPIS";
  static final String mcteParam_DomicPta = "DOMICPTA";
  static final String mcteParam_DOMICPOB = "DOMICPOB";
  static final String mcteParam_Provi = "PROVCOD";
  static final String mcteParam_LocalidadCod = "LOCALIDAD";
  static final String mcteParam_TELCOD = "TELCOD";
  static final String mcteParam_TELNRO = "TELNRO";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  static final String mcteParam_BANELCO = "BANELCO";
  static final String mcteParam_PLANNCOD = "TPLANATM";
  static final String mcteParam_COBROCOD = "FPAGO";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_CUENNUME = "CUENNUME";
  static final String mcteParam_VENCIANN = "VENCIANN";
  static final String mcteParam_VENCIMES = "VENCIMES";
  static final String mcteParam_VENCIDIA = "VENCIDIA";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  static final String mcteParam_ClienAp1 = "CLIENAP1";
  static final String mcteParam_ClienAp2 = "CLIENAP2";
  static final String mcteParam_ClienNom = "CLIENNOM";
  static final String mcteParam_NacimAnn = "NACIMANN";
  static final String mcteParam_NacimMes = "NACIMMES";
  static final String mcteParam_NacimDia = "NACIMDIA";
  static final String mcteParam_CLIENSEX = "SEXO";
  static final String mcteParam_CLIENEST = "ESTADO";
  static final String mcteParam_EMAIL = "EMAIL";
  static final String mcteParam_TipoDocu = "TIPODOCU";
  static final String mcteParam_NumeDocu = "NUMEDOCU";
  static final String mcteParam_DomicDomCo = "DOMICDOMCOR";
  static final String mcteParam_DomicDNUCo = "DOMICDNUCOR";
  static final String mcteParam_DomicPisCo = "DOMICPISCOR";
  static final String mcteParam_DomicPtaCo = "DOMICPTACOR";
  static final String mcteParam_DOMICPOBCo = "DOMICPOBCOR";
  static final String mcteParam_ProviCo = "PROVICOR";
  static final String mcteParam_LocalidadCodCo = "LOCALIDADCODCOR";
  static final String mcteParam_TELCOD_CO = "TELCODCOR";
  static final String mcteParam_TelNroCo = "TELNROCOR";
  static final String mcteParam_RAMOPCOD = "RAMOPCOD";
  static final String mcteParam_CERTIPOL = "CERTIPOL";
  static final String mcteParam_CERTIANN = "CERTIANN";
  static final String mcteParam_CERTISEC = "CERTISEC";
  static final String mcteParam_SUPLENUM = "SUPLENUM";
  static final String mcteParam_TipoOperac = "TIPOOPERAC";
  static final String mcteParam_EstadoProc = "ESTADOPROC";
  static final String mcteParam_TiempoProceso = "TIEMPOPROCESO";
  static final String mcteParam_PedidoAno = "MSGANO";
  static final String mcteParam_PedidoMes = "MSGMES";
  static final String mcteParam_PedidoDia = "MSGDIA";
  static final String mcteParam_PedidoHora = "MSGHORA";
  static final String mcteParam_PedidoMinuto = "MSGMINUTO";
  static final String mcteParam_PedidoSegundo = "MSGSEGUNDO";
  static final String mcteParam_USUARCOD = "USUARCOD";
  /**
   * AM
   */
  static final String mcteParam_EmpresaCodigo = "//CANAL";
  static final String mcteParam_SucursalCodigo = "//CANAL_SUCURSAL";
  static final String mcteParam_LegajoVend = "//LEGAJO_VEND";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncInsertNode
   * static variable for method: fncPutSolHO
   * static variable for method: fncPutSolicitud
   * static variable for method: fncIntegrarSolicitudMQ
   */
  private final String wcteFnName = "fncIntegrarSolicitudMQ";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant wvarCodErr = new Variant();
    Variant pvarRes = new Variant();
    HSBCInterfaces.IAction wobjClass = null;
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      pvarRequest = Strings.mid( pvarRequest, 1, Strings.find( 1, pvarRequest, ">" ) ) + "<MSGANO>" + DateTime.year( DateTime.now() ) + "</MSGANO>" + "<MSGMES>" + DateTime.month( DateTime.now() ) + "</MSGMES>" + "<MSGDIA>" + DateTime.day( DateTime.now() ) + "</MSGDIA>" + "<MSGHORA>" + DateTime.hour( DateTime.now() ) + "</MSGHORA>" + "<MSGMINUTO>" + DateTime.minute( DateTime.now() ) + "</MSGMINUTO>" + "<MSGSEGUNDO>" + DateTime.second( DateTime.now() ) + "</MSGSEGUNDO>" + Strings.mid( pvarRequest, (Strings.find( 1, pvarRequest, ">" ) + 1) );
      wvarStep = 10;
      wvarMensaje.set( "" );
      wvarCodErr.set( "" );
      pvarRes.set( "" );
      //
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(new Variant( pvarRequest )), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*TBD mobjCOM_Context.SetAbort() ;*/
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*TBD mobjCOM_Context.SetComplete() ;*/
        }
        return IAction_Execute;
      }

      pvarResponse.set( pvarRes );
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetComplete() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( Variant pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParams = null;
    XmlDomExtended wobjXMLReturnVal = null;
    XmlDomExtended wobjXMLError = null;
    XmlDomExtended wobjXMLRequestExists = null;
    Object wobjClass = null;
    Variant wvarMensajeStoreProc = new Variant();
    Variant wvarMensajePutTran = new Variant();
    String mvar_Estado = "";
    String mvarCertiSec = "";
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    Variant pTiempoAIS = new Variant();

    //
    //
    //
    try 
    {
      //Le agrega los Nodos de Valor Fijo
      wvarStep = 10;
      wobjXMLParams = new XmlDomExtended();
      wobjXMLParams.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteArchivoConfATM_XML));
      //
      invoke( "fncInsertNode", new Variant[] { new Variant(pvarRequest), new Variant("//Request"), new Variant( XmlDomExtended .getText( wobjXMLParams.selectSingleNode( new Variant("//BROKERS/ATM/SOLICITUD") )  )), new Variant("TIPOOPERAC") } );
      invoke( "fncInsertNode", new Variant[] { new Variant(pvarRequest), new Variant("//Request"), new Variant( XmlDomExtended .getText( wobjXMLParams.selectSingleNode( new Variant("//BROKERS/ATM/RAMOPCOD") )  )), new Variant("RAMOPCOD") } );
      //
      //Instancia la clase de validacion
      wvarMensaje.set( "" );
      wvarMensajeStoreProc.set( "" );
      //
      wvarStep = 20;
      wobjClass = new GetValidacionSolATM();
      wobjClass.Execute( pvarRequest, wvarMensajeStoreProc, "" );
      wobjClass = null;
      //
      //Analizo la respuesta del validador
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( wvarMensajeStoreProc.toString() );
      //
      //Si la respuesta viene con error
      wvarStep = 30;
      if( ! ( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ).equals( "0" )) )
      {
        wvarCodErr.set( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ) );
        wvarMensaje.set( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" )  ) );
        
        if( wvarCodErr.toString().equals( "-2" ) )
        {
          //Este es el caso que no venga el cod. de broker o el nro. de operacion
          wvarStep = 35;
          mvar_Estado = "NI";
          pvarRes.set( wvarMensajeStoreProc );
          wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
        }
        else if( wvarCodErr.toString().equals( "H010" ) )
        {
          //Este es el caso que ya existe la solicitud
          wobjXMLRequestExists = new XmlDomExtended();
          wobjXMLRequestExists.loadXML( pvarRequest.toString() );

          wvarStep = 40;
          com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
          //error: function 'GetDBConnection' was not found.
          wobjDBCnn = connFactory.getDBConnection(ModGeneral.mcteDB);
          wobjDBCmd = new Command();
          wobjDBCmd.setActiveConnection( wobjDBCnn );
          wobjDBCmd.setCommandText( mcteStoreProcSelect );
          wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
          //
          wvarStep = 50;
          
          
          //
          wvarStep = 52;
          
          
          //
          wvarStep = 54;
          
          
          //
          wvarStep = 56;
          
          
          //
          wvarStep = 58;
          wrstRes = new Recordset();
          wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );
          wvarCodErr.set( "0" );
          wvarMensaje.set( "" );

          pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + "<Response>" + "<REQUESTID>" + XmlDomExtended.getText( wobjXMLRequestExists.selectSingleNode( "//REQUESTID" )  ) + "</REQUESTID>" + "<POLIZANN>0</POLIZANN>" + "<POLIZSEC>0</POLIZSEC>" + "<CERTIPOL>0</CERTIPOL>" + "<CERTIANN>0</CERTIANN>" + "<CERTISEC>" + wrstRes.getFields().getField(12).getValue() + "</CERTISEC>" + "<SUPLENUM>0</SUPLENUM>" + "<MENSAJE>PROPUESTA YA EMITIDA</MENSAJE>" + "<SITUACION>R</SITUACION>" + "<PDF64><IMPCERTI/><IMPSOLI/><IMPPOLI/></PDF64>" + "</Response>" + "</LBA_WS>" );

          mvarCertiSec = wrstRes.getFields().getField(12).getValue().toString();
          mvar_Estado = "OK";
          wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
          //Le agrego el Nodo de CertiSec al XML original
          invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvarCertiSec), new Variant("CERTISEC") } );
          wobjXMLRequestExists = null;
          wobjDBCmd = (Command) null;
          wobjDBCnn = (Connection) null;
          wobjHSBC_DBCnn = null;
        }
        else
        {
          mvar_Estado = "ER";
          pvarRes.set( wvarMensajeStoreProc );
          wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );

        }

        // si no viene con error de la BD
      }
      else
      {

        //Guarda la Solicitud en el SP
        wvarStep = 60;

        if( invoke( "fncPutSolicitud", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes), new Variant(pTiempoAIS) } ) )
        {

          wobjXMLReturnVal = new XmlDomExtended();
          wobjXMLReturnVal.loadXML( pvarRes.toString() );
          //
          if( ! ( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ).equals( "0" )) )
          {
            //hubo un error
            wvarStep = 65;
            wvarCodErr.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ) );
            wvarMensaje.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" )  ) );
            wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
            mvar_Estado = "ER";

            //Este es el caso en que no haya contestado el SP o sea un error no identificado
          }
          else
          {
            wvarStep = 70;
            mvar_Estado = "OK";
            XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//CERTISEC" ) , XmlDomExtended.getText( wobjXMLReturnVal.selectSingleNode( "//CERTISEC" )  ) );
            wvarMensajeStoreProc.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
          }
          //
          wobjXMLReturnVal = null;

        }


        //
      }

      invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvar_Estado), new Variant("ESTADOPROC") } );

      // Agrego el tiempo de impresion
      invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(pTiempoAIS.toString()), new Variant("TIEMPOPROCESO") } );

      //Alta de la OPERACION
      wvarStep = 75;
      if( invoke( "fncPutSolHO", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje.toString()), new Variant(wvarCodErr), new Variant(wvarMensajePutTran) } ) )
      {
        //
        wobjXMLReturnVal = new XmlDomExtended();
        wobjXMLReturnVal.loadXML( wvarMensajePutTran.toString() );
        //
        wvarStep = 80;
        if( ! ( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ).equals( "0" )) )
        {
          wvarCodErr.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ) );
          wvarMensaje.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" )  ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          fncGetAll = false;
          return fncGetAll;
        }
        //
        fncGetAll = true;
        //
      }
      fin: 
      wobjClass = null;
      wobjXMLRequest = null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;
        /*TBD mobjCOM_Context.SetComplete() ;*/

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }

  private boolean fncInsertNode( Variant pvarRequest, String pvarHeader, String pvarNodeValue, String pvarNodeName )
  {
    boolean fncInsertNode = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;


    try 
    {

      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest.toString() );
      //
      wvarStep = 20;
      /*unsup wobjXMLRequest.selectSingleNode( pvarHeader ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), pvarNodeName, "" ) */ );
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//" + pvarNodeName ) , pvarNodeValue );
      //
      pvarRequest.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
      wobjXMLRequest = null;
      fncInsertNode = true;
      return fncInsertNode;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncInsertNode = false;
        /*TBD mobjCOM_Context.SetComplete() ;*/

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncInsertNode;
  }

  private boolean fncPutSolHO( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutSolHO = false;
    int wvarStep = 0;
    HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    XmlDomExtended wobjXMLRequest = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_NROCOT = "";
    String mvarWDB_TIPOOPERAC = "";
    String mvarWDB_ESTADO = "";
    String mvarWDB_RECEPCION_PEDIDOANO = "";
    String mvarWDB_RECEPCION_PEDIDOMES = "";
    String mvarWDB_RECEPCION_PEDIDODIA = "";
    String mvarWDB_RECEPCION_PEDIDOHORA = "";
    String mvarWDB_RECEPCION_PEDIDOMINUTO = "";
    String mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_TIEMPOPROCESO = "";
    String mvarWDB_XML = "";

    //
    //
    //
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_CODINST = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST )  );
      }
      //
      wvarStep = 30;
      mvarWDB_REQUESTID = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID )  );
      }
      //
      wvarStep = 40;
      mvarWDB_NROCOT = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROCOT) )  == (org.w3c.dom.Node) null) )
      {
        if( new Variant( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NROCOT )  ) ).isNumeric() )
        {
          if( Strings.len( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NROCOT) )  ) ) < 19 )
          {
            mvarWDB_NROCOT = String.valueOf( VB.val( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NROCOT )  ) ) );
          }
        }
      }
      //
      wvarStep = 50;
      mvarWDB_TIPOOPERAC = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoOperac) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPOOPERAC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoOperac )  );
      }
      //
      wvarStep = 60;
      mvarWDB_ESTADO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EstadoProc) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_ESTADO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_EstadoProc )  );
      }
      //
      wvarStep = 70;
      mvarWDB_RECEPCION_PEDIDOANO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoAno) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOANO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoAno )  );
      }
      //
      wvarStep = 80;
      mvarWDB_RECEPCION_PEDIDOMES = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMes) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMes )  );
      }
      //
      wvarStep = 90;
      mvarWDB_RECEPCION_PEDIDODIA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoDia) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDODIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoDia )  );
      }
      //
      wvarStep = 100;
      mvarWDB_RECEPCION_PEDIDOHORA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoHora) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOHORA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoHora )  );
      }
      //
      wvarStep = 110;
      mvarWDB_RECEPCION_PEDIDOMINUTO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMinuto) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMINUTO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMinuto )  );
      }
      //
      wvarStep = 120;
      mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoSegundo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOSEGUNDO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoSegundo )  );
      }
      //
      wvarStep = 140;
      mvarWDB_POLIZANN = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) )  == (org.w3c.dom.Node) null) )
      {
        if( new Variant( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN )  ) ).isNumeric() )
        {
          if( Obj.toInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) )  ) ) < 100 )
          {
            mvarWDB_POLIZANN = String.valueOf( VB.val( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN )  ) ) );
          }
        }
      }
      //
      wvarStep = 150;
      mvarWDB_POLIZSEC = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) )  == (org.w3c.dom.Node) null) )
      {
        if( new Variant( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC )  ) ).isNumeric() )
        {
          if( Obj.toInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) )  ) ) < 1000000 )
          {
            mvarWDB_POLIZSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC )  );
          }
        }
      }
      //
      wvarStep = 170;
      mvarWDB_TIEMPOPROCESO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TiempoProceso) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIEMPOPROCESO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TiempoProceso )  );
      }



      wvarStep = 180;
      mvarWDB_XML = "";
      mvarWDB_XML = pvarRequest;
      //
      wvarStep = 190;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.mcteDB);
      //
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      
      //
      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      
      //
      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_NRO_COTIZACION_LBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NROCOT.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NROCOT)) );
      
      //
      wvarStep = 230;
      
      
      //
      wvarStep = 240;
      
      
      //
      wvarStep = 250;
      
      
      //
      wvarStep = 260;
      
      
      //
      wvarStep = 270;
      
      
      //
      wvarStep = 280;
      wobjDBParm = new Parameter( "@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      
      //
      wvarStep = 290;
      wobjDBParm = new Parameter( "@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      
      //
      wvarStep = 300;
      wobjDBParm = new Parameter( "@WDB_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      
      //
      wvarStep = 310;
      
      
      //
      wvarStep = 320;
      wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CERTISEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CERTISEC)) );
      
      //
      wvarStep = 330;
      wobjDBParm = new Parameter( "@WDB_TIEMPO_PROCESO_AIS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIEMPOPROCESO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIEMPOPROCESO)) );
      
      //
      wvarStep = 340;
      
      
      //
      wvarStep = 350;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      wobjDBCnn.close();

      fncPutSolHO = true;
      pvarRes.set( "<LBA_WS res_code=\"" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ) + "\" res_msg=\"" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" )  ) + "\"></LBA_WS>" );
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = null;
      wobjClass = (HSBCInterfaces.IAction) null;
      return fncPutSolHO;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        fncPutSolHO = false;
        //mobjCOM_Context.SetComplete
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutSolHO;
  }

  private boolean fncPutSolicitud( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes, Variant pTiempoAIS )
  {
    boolean fncPutSolicitud = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Object wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLRequest1 = null;
    XmlDomExtended wobjXMLPdf = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarcounter = 0;
    String wvarSitu = "";
    String mvarWDB_CODINST = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTIPOL = "";
    String mvarWDB_CERTIANN = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_SUPLENUM = "";
    String mvarWDB_NUMEDOCU = "";
    String mvarWDB_TIPODOCU = "";
    String mvarWDB_CLIENAP1 = "";
    String mvarWDB_CLIENAP2 = "";
    String mvarWDB_CLIENNOM = "";
    String mvarWDB_NACIMANN = "";
    String mvarWDB_NACIMMES = "";
    String mvarWDB_NACIMDIA = "";
    String mvarWDB_CLIENSEX = "";
    String mvarWDB_CLIENEST = "";
    String mvarWDB_EFECTANN = "";
    String mvarWDB_EFECTMES = "";
    String mvarWDB_EFECTDIA = "";
    String mvarWDB_EMAIL = "";
    String mvarWDB_DOMICDOM = "";
    String mvarWDB_DOMICDNU = "";
    String mvarWDB_DOMICPIS = "";
    String mvarWDB_DOMICPTA = "";
    String mvarWDB_DOMICPOB = "";
    String mvarWDB_BARRIOCOUNT = "";
    String mvarWDB_DOMICCPO = "";
    String mvarWDB_PROVICOD = "";
    String mvarTELCOD = "";
    String mvarWDB_TELNRO = "";
    String mvarWDB_DOMICDOMCO = "";
    String mvarWDB_DOMICDNUCO = "";
    String mvarWDB_DOMICPISCO = "";
    String mvarWDB_DOMICPTACO = "";
    String mvarWDB_DOMICPOBCO = "";
    String mvarWDB_DOMICCPOCO = "";
    String mvarWDB_PROVICODCO = "";
    String mvarTELCOD_CO = "";
    String mvarWDB_TELNRO_CO = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_CUENNUME = "";
    String mvarWDB_VENCIANN = "";
    String mvarWDB_VENCIMES = "";
    String mvarWDB_VENCIDIA = "";
    String mvarWDB_BANCOCOD = "";
    String mvarWDB_SUCURCOD = "";
    String mvarWDB_P_EMISIANN = "";
    String mvarWDB_P_EMISIMES = "";
    String mvarWDB_P_EMISIDIA = "";
    String mvarWDB_P_COBROCOD = "";
    String mvarWDB_P_PLANNCOD = "";
    String mvarWDB_P_BARRIOCOUNT = "";
    String mvarWDB_P_ZONA = "";
    String mvarWDB_P_CLIENIVA = "";
    String mvarWDB_BANELCO = "";
    String mvarWDB_USUARCOD = "";
    String mvarWDB_MENSAJE = "";
    String mvarNroCertiSec = "";
    String mvarDatosImpreso = "";
    String mvarNodoImpreso = "";
    String mvarImpreso64 = "";
    String mvarRequestImp = "";
    String mvarResponseImp = "";
    Recordset wrstDBResult = null;
    String mvarDOCUDES = "";
    String mvarCOBRODES = "";
    String mvarCAMPADES = "";
    String mvarNOMBROKER = "";
    String mvarPROVIDES = "";
    String mvarPROVIDESCO = "";
    String mvarWDB_VALOR = "";
    String mvarRequest = "";
    String mvarResponse = "";
    XmlDomExtended mobjXMLDoc = null;
    String mvarRequestCob = "";
    String mvarResponseCob = "";
    XmlDomExtended mobjXMLDocCob = null;
    String mvarPLANDES = "";
    String mvarCOBERDES = "";
    String mvarCUENNUME = "";
    String mvarCobOk = "";
    String hdatos = "";
    String vFECINIVG = "";
    String vFECFINVG = "";
    String vNROREFERENCIA = "";
    String mvarseltest = "";
    String mvarNomArch = "";
    String wvarPdfReq = "";
    String wvarPdfResp = "";
    int wvarCount = 0;
    int mvarSumaCob = 0;
    int wvariCounter = 0;
    String mvarEmpresaCodigo = "";
    String mvarSucursalCodigo = "";
    String mvarLegajoVend = "";
    String vOk = "";
    String mvar_TiempoProceso = "";
    String mvarInicioAIS = "";
    String mvarFinAIS = "";
    String mvarTiempoAIS = "";

    //
    //
    //
    //10/2010 variables para armar el impreso
    //AM
    // variables para calcular tiempo proceso de la impresi�n
    try 
    {

      vOk = "OK";

      // **************   INTEGRACION MSG 2210 y 2219**************************
      if( ! (invoke( "fncIntegrarSolicitudMQ", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje.toString()), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        return fncPutSolicitud;
      }

      // *****************   IMPRESO     **************************************
      //Recupero datos del request y el response
      wvarStep = 100;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 110;
      wobjXMLRequest1 = new XmlDomExtended();
      wobjXMLRequest1.loadXML( pvarRes.toString() );
      //
      wvarStep = 120;
      mvarWDB_VALOR = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_VALOR) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_VALOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_VALOR )  );
      }
      //
      wvarStep = 130;
      mvarWDB_TIPODOCU = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TipoDocu) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIPODOCU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TipoDocu )  );
      }
      //
      wvarStep = 140;
      mvarWDB_COBROTIP = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROTIP = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP )  ) );
      }
      //
      wvarStep = 150;
      mvarWDB_CODINST = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST )  );
      }
      //
      wvarStep = 160;
      mvarWDB_PROVICOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi )  );
      }
      //
      wvarStep = 170;
      mvarWDB_PROVICODCO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ProviCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICODCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ProviCo )  );
      }
      //
      wvarStep = 180;
      mvarWDB_P_PLANNCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PLANNCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_PLANNCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PLANNCOD )  );
      }
      //
      wvarStep = 190;
      mvarWDB_POLIZANN = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN )  );
      }
      //
      wvarStep = 200;
      mvarWDB_POLIZSEC = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC )  );
      }
      //
      wvarStep = 210;
      mvarWDB_BANELCO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_BANELCO) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_BANELCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_BANELCO )  );
      }
      //
      wvarStep = 220;
      mvarWDB_CUENNUME = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CUENNUME) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CUENNUME = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CUENNUME )  );
      }
      //
      wvarStep = 230;
      mvarWDB_RAMOPCOD = "";
      if( ! (wobjXMLRequest1.selectSingleNode( ("//" + mcteParam_RAMOPCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RAMOPCOD = XmlDomExtended.getText( wobjXMLRequest1.selectSingleNode( "//" + mcteParam_RAMOPCOD )  );
      }
      //
      wvarStep = 235;
      mvarWDB_MENSAJE = "";
      if( ! (wobjXMLRequest1.selectSingleNode( "//MENSAJE" )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_MENSAJE = XmlDomExtended.getText( wobjXMLRequest1.selectSingleNode( "//MENSAJE" )  );
      }
      //
      wvarStep = 240;
      mvarWDB_CERTIPOL = "";
      if( ! (wobjXMLRequest1.selectSingleNode( ("//" + mcteParam_CERTIPOL) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTIPOL = XmlDomExtended.getText( wobjXMLRequest1.selectSingleNode( "//" + mcteParam_CERTIPOL )  );
      }
      //
      wvarStep = 250;
      mvarWDB_CERTIANN = "";
      if( ! (wobjXMLRequest1.selectSingleNode( ("//" + mcteParam_CERTIANN) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTIANN = XmlDomExtended.getText( wobjXMLRequest1.selectSingleNode( "//" + mcteParam_CERTIANN )  );
      }
      //
      wvarStep = 260;
      mvarWDB_SUPLENUM = "";
      if( ! (wobjXMLRequest1.selectSingleNode( ("//" + mcteParam_SUPLENUM) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_SUPLENUM = XmlDomExtended.getText( wobjXMLRequest1.selectSingleNode( "//" + mcteParam_SUPLENUM )  );
      }
      //
      wvarStep = 270;
      mvarWDB_CERTISEC = "";
      if( ! (wobjXMLRequest1.selectSingleNode( ("//" + mcteParam_CERTISEC) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTISEC = XmlDomExtended.getText( wobjXMLRequest1.selectSingleNode( "//" + mcteParam_CERTISEC )  );
      }
      //
      wvarStep = 280;
      mvarWDB_CLIENAP1 = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienAp1) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENAP1 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienAp1 )  );
      }
      //
      wvarStep = 290;
      mvarWDB_CLIENAP2 = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienAp2) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENAP2 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienAp2 )  );
      }
      //
      wvarStep = 300;
      mvarWDB_CLIENNOM = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ClienNom) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CLIENNOM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClienNom )  );
      }
      //
      wvarStep = 310;
      mvarWDB_NUMEDOCU = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NumeDocu) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NUMEDOCU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NumeDocu )  );
      }
      //
      wvarStep = 320;
      mvarWDB_TELNRO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TELNRO) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TELNRO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TELNRO )  );
      }
      //
      wvarStep = 330;
      mvarWDB_DOMICDOMCO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDomCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDOMCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDomCo )  );
      }
      //
      wvarStep = 340;
      mvarWDB_DOMICDNUCO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDNUCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDNUCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDNUCo )  );
      }
      //
      wvarStep = 350;
      mvarWDB_DOMICDNUCO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicDNUCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICDNUCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicDNUCo )  );
      }
      //
      wvarStep = 360;
      mvarWDB_DOMICPISCO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPisCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPISCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPisCo )  );
      }
      //
      wvarStep = 370;
      mvarWDB_DOMICPTACO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DomicPtaCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPTACO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DomicPtaCo )  );
      }
      //
      wvarStep = 380;
      mvarWDB_DOMICPOBCO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_DOMICPOBCo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICPOBCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DOMICPOBCo )  );
      }
      //
      wvarStep = 390;
      mvarWDB_NACIMDIA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimDia) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMDIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimDia )  );
      }
      //
      wvarStep = 400;
      mvarWDB_NACIMMES = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimMes) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimMes )  );
      }
      //
      wvarStep = 410;
      mvarWDB_NACIMANN = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_NacimAnn) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_NACIMANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimAnn )  );
      }
      //
      wvarStep = 420;
      mvarWDB_USUARCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_USUARCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_USUARCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_USUARCOD )  );
      }
      //
      //Ejectua sp para descripciones
      wvarStep = 430;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.mcteDB);
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProcImpre );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );


      wvarStep = 440;
      wobjDBParm = new Parameter( "@DOCUMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIPODOCU.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIPODOCU)) );
      

      wvarStep = 450;
      
      

      wvarStep = 460;
      
      

      wvarStep = 470;
      wobjDBParm = new Parameter( "@CODPROV", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICOD)) );
      

      wvarStep = 480;
      wobjDBParm = new Parameter( "@CODPROVCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICODCO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICODCO)) );
      


      wvarStep = 490;
      mvarseltest = mcteStoreProcImpre + " ";
      for( wvarCount = 0; wvarCount <= (wobjDBCmd.getParameters().getCount() - 1); wvarCount++ )
      {
        if( (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adChar) || (wobjDBCmd.getParameters().getParameter(wvarCount).getType() == AdoConst.adVarChar) )
        {
          mvarseltest = mvarseltest + "'" + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + "',";
        }
        else
        {
          mvarseltest = mvarseltest + wobjDBCmd.getParameters().getParameter(wvarCount).getValue() + ",";
        }
      }

      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );

      wvarStep = 500;
      if( ! (wrstDBResult.isEOF()) )
      {
        mvarDOCUDES = wrstDBResult.getFields().getField("DOCUDES").getValue().toString();
        mvarCOBRODES = wrstDBResult.getFields().getField("COBRODES").getValue().toString();
        mvarPROVIDES = wrstDBResult.getFields().getField("PROVIDES").getValue().toString();
        mvarPROVIDESCO = wrstDBResult.getFields().getField("PROVIDESCO").getValue().toString();
      }
      else
      {
        mvarDOCUDES = "";
        mvarCOBRODES = "";
        mvarNOMBROKER = "";
        mvarPROVIDES = "";
        mvarPROVIDESCO = "";
      }

      wvarStep = 510;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        
        {
          wrstDBResult.close();
        }
      }

      wvarStep = 520;
      wrstDBResult = (Recordset) null;

      wobjDBCnn.close();

      // Detalle de Coberturas
      wvarStep = 530;
      mvarRequestCob = "<Request><DEFINICION>2330_CoberturasParaImpresion.xml</DEFINICION>";
      mvarRequestCob = mvarRequestCob + "<RAMOPCOD>ATA1</RAMOPCOD>";
      mvarRequestCob = mvarRequestCob + "<POLIZANN>" + mvarWDB_POLIZANN + "</POLIZANN>";
      mvarRequestCob = mvarRequestCob + "<POLIZSEC>" + mvarWDB_POLIZSEC + "</POLIZSEC>";
      mvarRequestCob = mvarRequestCob + "<PLANNCOD>" + mvarWDB_P_PLANNCOD + "</PLANNCOD>";
      mvarRequestCob = mvarRequestCob + "<AUUSOCOD>0</AUUSOCOD></Request>";

      wvarStep = 540;
      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( new Variant( mvarRequestCob ), new Variant( mvarResponseCob ), "" );
      wobjClass = null;
      mobjXMLDocCob = new XmlDomExtended();
      mobjXMLDocCob.loadXML( mvarResponseCob );

      wvarStep = 550;
      mvarCobOk = "ER";
      if( ! (mobjXMLDocCob.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended .getText( mobjXMLDocCob.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          mvarCobOk = "OK";

          wvarStep = 560;
          wobjXMLList = mobjXMLDocCob.selectNodes( "//CAMPOS/TEXTOS/TEXTO" ) ;
          //
          for( wvarcounter = 1; wvarcounter <= Obj.toInt( XmlDomExtended .getText( mobjXMLDocCob.selectSingleNode( "//CANTIDAD" )  ) ); wvarcounter++ )
          {
            if( wobjXMLList.getLength() >= wvarcounter )
            {
              wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
              wvarStep = 570;
              hdatos = "<DESCCOBERTURAS>";
              hdatos = hdatos + "<COBERDESC>![CDATA[";
              hdatos = hdatos + XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "//COBERDES" )  );
              hdatos = hdatos + "]]</COBERDESC>";
              hdatos = hdatos + "<COBERADIC>![CDATA[";
              hdatos = hdatos + XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "//PLANNDES" )  );
              hdatos = hdatos + "]]</COBERADIC>";
              hdatos = hdatos + "</DESCCOBERTURAS>";

              wobjXMLNode = (org.w3c.dom.Node) null;
            }
          }
        }
      }
      else
      {
        hdatos = "<DESCCOBERTURAS>";
        hdatos = hdatos + "<COBERDESC/>";
        hdatos = hdatos + "<COBERADIC/>";
        hdatos = hdatos + "</DESCCOBERTURAS>";
      }

      mobjXMLDoc = null;
      wvarStep = 580;
      if( mvarCobOk.equals( "ER" ) )
      {
        wvarCodErr.set( -53 );
        pvarRes.set( "<LBA_WS res_code=\"-53\" res_msg=\"Error\"><Response><Estado resultado=\"false\" mensaje=\"Poliza sin Coberturas\" /></Response></LBA_WS>" );
        return fncPutSolicitud;
      }
      // Fin Detalle de coberturas
      wvarStep = 590;
      //Calculo fechas de vigencia
      //AM la fecha de vigencia debe ser el 1 del mes actual.
      vFECINIVG = invoke( "fechaVige", new Variant[] { new Variant(new Variant( DateTime.dateSerial( new Variant(DateTime.year( new Variant(DateTime.now()) )), new Variant(DateTime.month( new Variant(DateTime.now()) )), new Variant(1) ) ))/*warning: ByRef value change will be lost.*/ } );
      vFECFINVG = invoke( "fechaVige", new Variant[] { new Variant(new Variant( DateTime.dateSerial( new Variant(DateTime.year( new Variant(DateTime.now()) )), new Variant(DateTime.month( new Variant(DateTime.now()) ) + 1), new Variant(1) ) ))/*warning: ByRef value change will be lost.*/ } );
      //vFECINIVG = fechaVige(DateSerial(CInt(wobjXMLRequest.selectSingleNode("//VIGENANN").Text), CInt(wobjXMLRequest.selectSingleNode("//VIGENMES").Text), CInt(wobjXMLRequest.selectSingleNode("//VIGENDIA").Text)))
      //vFECFINVG = fechaVige(DateSerial(CInt(wobjXMLRequest.selectSingleNode("//VIGENANN").Text), CInt(wobjXMLRequest.selectSingleNode("//VIGENMES").Text), CInt(wobjXMLRequest.selectSingleNode("//VIGENDIA").Text)) + 15)
      vNROREFERENCIA = mvarWDB_RAMOPCOD + "-" + mvarWDB_CERTIPOL + "-" + mvarWDB_CERTISEC;

      //conforma xml para la impresi�n
      wvarStep = 600;
      mvarDatosImpreso = "<DATOS>";
      mvarDatosImpreso = mvarDatosImpreso + "<DATOSCLIENTE>";
      mvarDatosImpreso = mvarDatosImpreso + "<APELLIDORAZONSOCIAL><![CDATA[" + mvarWDB_CLIENAP1 + " " + mvarWDB_CLIENAP2 + "]]></APELLIDORAZONSOCIAL>";
      mvarDatosImpreso = mvarDatosImpreso + "<NOMBRE><![CDATA[" + mvarWDB_CLIENNOM + "]]></NOMBRE>";
      mvarDatosImpreso = mvarDatosImpreso + "<TIPODOC>" + mvarDOCUDES + "</TIPODOC>";
      mvarDatosImpreso = mvarDatosImpreso + "<NRODOCUMENTO>" + mvarWDB_NUMEDOCU + "</NRODOCUMENTO>";
      mvarDatosImpreso = mvarDatosImpreso + "<TEL>" + mvarWDB_TELNRO + "</TEL>";
      mvarDatosImpreso = mvarDatosImpreso + "<EMAIL/>";
      mvarDatosImpreso = mvarDatosImpreso + "<DOMICORRESP>" + mvarWDB_DOMICDOMCO + " " + mvarWDB_DOMICDNUCO + " " + mvarWDB_DOMICPISCO + " " + mvarWDB_DOMICPTACO + " " + mvarWDB_DOMICPOBCO + "</DOMICORRESP>";
      mvarDatosImpreso = mvarDatosImpreso + "<DOMICORRESP>" + mvarWDB_DOMICDOMCO + "</DOMICORRESP>";
      mvarDatosImpreso = mvarDatosImpreso + "<DOMINRO>" + mvarWDB_DOMICDNUCO + "</DOMINRO>";
      mvarDatosImpreso = mvarDatosImpreso + "<LOCALIDAD>" + mvarWDB_DOMICPOBCO + "</LOCALIDAD>";
      mvarDatosImpreso = mvarDatosImpreso + "<PROV>" + mvarPROVIDESCO + "</PROV>";
      mvarDatosImpreso = mvarDatosImpreso + "<FECNACIMIENTO>" + mvarWDB_NACIMDIA + "/" + mvarWDB_NACIMMES + "/" + mvarWDB_NACIMANN + "</FECNACIMIENTO>";
      if( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) )  ) ).equals( "S" ) )
      {
        mvarDatosImpreso = mvarDatosImpreso + "<ESTADOCIVIL>SOLTERO/A</ESTADOCIVIL>";
      }
      else
      {
        if( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) )  ) ).equals( "C" ) )
        {
          mvarDatosImpreso = mvarDatosImpreso + "<ESTADOCIVIL>CASADO/A</ESTADOCIVIL>";
        }
        else
        {
          if( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) )  ) ).equals( "E" ) )
          {
            mvarDatosImpreso = mvarDatosImpreso + "<ESTADOCIVIL>SEPARADO/A</ESTADOCIVIL>";
          }
          else
          {
            if( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) )  ) ).equals( "V" ) )
            {
              mvarDatosImpreso = mvarDatosImpreso + "<ESTADOCIVIL>VIUDO/A</ESTADOCIVIL>";
            }
            else
            {
              if( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENEST) )  ) ).equals( "D" ) )
              {
                mvarDatosImpreso = mvarDatosImpreso + "<ESTADOCIVIL>DIVORCIADO/A</ESTADOCIVIL>";
              }
              else
              {
                mvarDatosImpreso = mvarDatosImpreso + "<ESTADOCIVIL></ESTADOCIVIL>";
              }
            }
          }
        }
      }
      mvarDatosImpreso = mvarDatosImpreso + "<PAISNACIMIENTO>ARGENTINA</PAISNACIMIENTO>";
      if( Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENSEX) )  ) ).equals( "M" ) )
      {
        mvarDatosImpreso = mvarDatosImpreso + "<SEXO>MASCULINO</SEXO>";
      }
      else
      {
        mvarDatosImpreso = mvarDatosImpreso + "<SEXO>FEMENINO</SEXO>";
      }
      mvarDatosImpreso = mvarDatosImpreso + "</DATOSCLIENTE>";
      mvarDatosImpreso = mvarDatosImpreso + "<CONDICIONESCOTIZACION>";
      mvarDatosImpreso = mvarDatosImpreso + "<POLIZACOL>" + mvarWDB_POLIZSEC + "</POLIZACOL>";
      mvarDatosImpreso = mvarDatosImpreso + "<PRODUCTOR></PRODUCTOR>";
      mvarDatosImpreso = mvarDatosImpreso + "<NROREFERENCIA></NROREFERENCIA>";
      mvarDatosImpreso = mvarDatosImpreso + "<PRECIOMENS>" + mvarWDB_VALOR + "</PRECIOMENS>";
      mvarDatosImpreso = mvarDatosImpreso + "<FORMADEPAGO>" + mvarCOBRODES + "</FORMADEPAGO>";
      mvarDatosImpreso = mvarDatosImpreso + "<DESC_FP1/>";
      mvarDatosImpreso = mvarDatosImpreso + "<DESC_FP2/>";
      mvarDatosImpreso = mvarDatosImpreso + "<BANCO></BANCO>";
      mvarDatosImpreso = mvarDatosImpreso + "<VIGENCIADESDE>" + vFECINIVG + " (desde las 12 hs.)</VIGENCIADESDE>";
      mvarDatosImpreso = mvarDatosImpreso + "<VIGENCIAHASTA>" + vFECFINVG + " (hasta las 12 hs.)</VIGENCIAHASTA>";
      mvarDatosImpreso = mvarDatosImpreso + "</CONDICIONESCOTIZACION>";
      mvarDatosImpreso = mvarDatosImpreso + "<DATOSRIESGO>";
      mvarDatosImpreso = mvarDatosImpreso + "<NROBANELCO>" + mvarWDB_BANELCO + "</NROBANELCO>";
      mvarDatosImpreso = mvarDatosImpreso + "<CAJAAHORRO>" + mvarWDB_CUENNUME + "</CAJAAHORRO>";
      mvarDatosImpreso = mvarDatosImpreso + "</DATOSRIESGO>";
      mvarDatosImpreso = mvarDatosImpreso + "<DESCRIPCIONESCOB>";
      mvarDatosImpreso = mvarDatosImpreso + hdatos;
      mvarDatosImpreso = mvarDatosImpreso + "</DESCRIPCIONESCOB>";
      mvarDatosImpreso = mvarDatosImpreso + "<DATOSCOTIZACION>";
      mvarDatosImpreso = mvarDatosImpreso + "<MONEDA>Pesos</MONEDA>";
      mvarDatosImpreso = mvarDatosImpreso + "<NROSOLI>" + vNROREFERENCIA + "</NROSOLI>";
      mvarDatosImpreso = mvarDatosImpreso + "<FECHACOTIZACION>" + DateTime.format( DateTime.now(), "dd MMMM, yyyy" ) + "</FECHACOTIZACION>";
      mvarDatosImpreso = mvarDatosImpreso + "</DATOSCOTIZACION>";
      mvarDatosImpreso = mvarDatosImpreso + "</DATOS>";
      // Fin xml de impresion
      //imprimo siempre  IMPSOLI SOLI_ATD por BIRT(MW)
      wvarStep = 610;
      mvarRequestImp = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>" + mvarDatosImpreso + "</xmlDataSource>" + "<reportId>SOLI_ATD</reportId>" + "</Request>";
      wvarStep = 620;
      mvarNodoImpreso = "";
      //Set wobjClass = mobjCOM_Context.CreateInstance("LBAA_MWGenerico.lbaw_MQMW")
      //Call wobjClass.Execute(mvarRequestImp, mvarResponseImp, "")
      //Set wobjClass = Nothing
      //Set mobjXMLDoc = CreateObject("MSXML2.DOMDocument")
      //mobjXMLDoc.async = False
      //Call mobjXMLDoc.loadXML(mvarResponseImp)
      //wvarStep = 630
      //'Verifica que haya respuesta de MQ
      //If mobjXMLDoc.selectSingleNode("//Estado").Attributes.getNamedItem("resultado").Text = "true" Then
      //    'Verifica que exista el PDF si no error Middle
      //    If Not mobjXMLDoc.selectSingleNode("//generatePdfReportReturn") Is Nothing Then
      //           mvarNodoImpreso = mobjXMLDoc.documentElement.selectSingleNode("//generatePdfReportReturn").Text
      //    Else
      //           mvarNodoImpreso = "No se pudo obtener el impreso MDW" 'error de mdw
      //    End If
      //Else
      //    mvarNodoImpreso = "No se pudo obtener el impreso MQ"
      //End If
      //Set mobjXMLDoc = Nothing
      mvarImpreso64 = mvarImpreso64 + "<IMPSOLI>" + mvarNodoImpreso + "</IMPSOLI>";
      //wvarStep = 640
      // Si la p�liza fue Emitida imprimo MPOLI por MSJ 1710 y despues recupero el binary por lbaw_ovGetBinaryFile
      mvarInicioAIS = DateTime.format( DateTime.now() );
      if( XmlDomExtended .getText( wobjXMLRequest1.selectSingleNode( "//SITUACION" )  ).equals( "E" ) )
      {

        wvarSitu = "E";
        if( mvarWDB_MENSAJE.equals( "" ) )
        {
          wvarMensaje.set( "PROPUESTA EMITIDA" );
        }
        else
        {
          wvarMensaje.set( mvarWDB_MENSAJE );
        }

        mvarNodoImpreso = "";
        //
        mvarRequest = "<Request>";
        mvarRequest = mvarRequest + "<USUARIO>" + mvarWDB_USUARCOD + "</USUARIO>";
        mvarRequest = mvarRequest + "<RAMOPCOD>" + mvarWDB_RAMOPCOD + "</RAMOPCOD>";
        mvarRequest = mvarRequest + "<POLIZANN>" + mvarWDB_POLIZANN + "</POLIZANN>";
        mvarRequest = mvarRequest + "<POLIZSEC>" + mvarWDB_POLIZSEC + "</POLIZSEC>";
        mvarRequest = mvarRequest + "<CERTIPOL>" + mvarWDB_CERTIPOL + "</CERTIPOL>";
        mvarRequest = mvarRequest + "<CERTIANN>" + mvarWDB_CERTIANN + "</CERTIANN>";
        mvarRequest = mvarRequest + "<CERTISEC>" + mvarWDB_CERTISEC + "</CERTISEC>";
        mvarRequest = mvarRequest + "<SUPLENUM>" + mvarWDB_SUPLENUM + "</SUPLENUM>";
        mvarRequest = mvarRequest + "<OPERAPOL>0</OPERAPOL>";
        mvarRequest = mvarRequest + "<TIPODOCU>PC</TIPODOCU>";
        mvarRequest = mvarRequest + "<TIPOIMPR>CO</TIPOIMPR>";
        mvarRequest = mvarRequest + "<MANTENERARCHIVO></MANTENERARCHIVO>";
        mvarRequest = mvarRequest + "</Request>";
        //
        wvarStep = 650;
        wobjClass = new lbawA_OVMQEmision.lbaw_OVImprimirPoliza();
        wobjClass.Execute( mvarRequest, mvarResponse, "" );
        wobjClass = (Object) null;
        mobjXMLDoc = new XmlDomExtended();
        mobjXMLDoc.loadXML( mvarResponse );
        //Verifica que haya respuesta
        wvarStep = 660;
        if( XmlDomExtended .getText( mobjXMLDoc.selectSingleNode( "//Estado" ) .getAttributes().getNamedItem( "resultado" ) ).equals( "true" ) )
        {
          //Verifica que exista el PDF si no error
          wvarStep = 670;
          if( ! (mobjXMLDoc.selectSingleNode( "//FILES/FILE/RUTA" )  == (org.w3c.dom.Node) null) )
          {
            mvarNomArch = XmlDomExtended.getText( null /*unsup mobjXMLDoc.getDocument().getDocumentElement().selectSingleNode( "//FILES/FILE/RUTA" ) */ );

            //Recupero el binaryFile
            wvarStep = 430;
            wvarPdfReq = "<Request><RUTA>" + mvarNomArch + "</RUTA></Request>";

            wvarStep = 680;
            wobjClass = new lbawA_OVMQEmision.lbaw_OVGetBinaryFile();
            wobjClass.Execute( new Variant( wvarPdfReq ), new Variant( wvarPdfResp ), "" );
            wobjClass = (Object) null;

            wvarStep = 690;
            wobjXMLPdf = new XmlDomExtended();
            wobjXMLPdf.loadXML( wvarPdfResp );


            wvarStep = 700;
            if( ! (wobjXMLPdf.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
            {
              if( XmlDomExtended .getText( wobjXMLPdf.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
              {
                mvarNodoImpreso = "No se pudo obtener el impreso";
              }
              else
              {
                mvarNodoImpreso = XmlDomExtended.getText( wobjXMLPdf.selectSingleNode( "//BinData" )  );
              }
            }
            else
            {
              mvarNodoImpreso = "";
            }
          }
          else
          {
            mvarNodoImpreso = "";
          }
        }
        else
        {
          mvarNodoImpreso = "";
        }

        mobjXMLDoc = null;
        mvarImpreso64 = mvarImpreso64 + "<IMPCERTI></IMPCERTI><IMPPOLI>" + mvarNodoImpreso + "</IMPPOLI>";

        //imprimo IMPCERTI por BIRT(MW)
      }
      else if( XmlDomExtended .getText( wobjXMLRequest1.selectSingleNode( "//SITUACION" )  ).equals( "I" ) )
      {
        wvarSitu = "I";
        if( mvarWDB_MENSAJE.equals( "" ) )
        {
          wvarMensaje.set( "PROPUESTA INGRESADA" );
        }
        else
        {
          wvarMensaje.set( mvarWDB_MENSAJE );
        }

        mvarNodoImpreso = "";
        //
        //   wvarStep = 710
        //   mvarRequestImp = "<Request>" & _
        //  '              "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" & _
        //              "<Raiz>share:generatePdfReport</Raiz>" & _
        //  '              "<xmlDataSource>" & mvarDatosImpreso & "</xmlDataSource>" & _
        //              "<reportId>CERT_PROV_ATD</reportId>" & _
        //  '              "</Request>"
        //   wvarStep = 720
        //   Set wobjClass = mobjCOM_Context.CreateInstance("LBAA_MWGenerico.lbaw_MQMW")
        //   Call wobjClass.Execute(mvarRequestImp, mvarResponseImp, "")
        //   Set wobjClass = Nothing
        //   Set mobjXMLDoc = CreateObject("MSXML2.DOMDocument")
        //   mobjXMLDoc.async = False
        //   Call mobjXMLDoc.loadXML(mvarResponseImp)
        //   'Verifica que haya respuesta de MQ
        //   wvarStep = 730
        //   If mobjXMLDoc.selectSingleNode("//Estado").Attributes.getNamedItem("resultado").Text = "true" Then
        //       'Verifica que exista el PDF si no error
        //       If Not mobjXMLDoc.selectSingleNode("//generatePdfReportReturn") Is Nothing Then
        //              mvarNodoImpreso = mobjXMLDoc.documentElement.selectSingleNode("//generatePdfReportReturn").Text
        //       Else
        //              mvarNodoImpreso = "No se pudo obtener el impreso"
        //       End If
        //   Else
        //       mvarNodoImpreso = "No se pudo obtener el impreso"
        //      End If
        //
        //   Set mobjXMLDoc = Nothing
        mvarImpreso64 = mvarImpreso64 + "<IMPCERTI>" + mvarNodoImpreso + "</IMPCERTI><IMPPOLI></IMPPOLI>";

      }
      else
      {
        vOk = "ER";
      }

      mvarFinAIS = DateTime.format( DateTime.now() );
      pTiempoAIS.set( DateTime.diff( "S", DateTime.toDate( mvarInicioAIS ), DateTime.toDate( mvarFinAIS ) ) );

      //fin impreso, Se agrega en pvarRes el nodo <PDF64>
      if( vOk.equals( "OK" ) )
      {
        wvarStep = 740;
        pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + "<Response>" + "<REQUESTID>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) )  ) + "</REQUESTID>" + "<POLIZANN>" + mvarWDB_POLIZANN + "</POLIZANN>" + "<POLIZSEC>" + mvarWDB_POLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + mvarWDB_CERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + mvarWDB_CERTIANN + "</CERTIANN>" + "<CERTISEC>" + mvarWDB_CERTISEC + "</CERTISEC>" + "<SUPLENUM>" + mvarWDB_SUPLENUM + "</SUPLENUM>" + "<MENSAJE>" + wvarMensaje + "</MENSAJE>" + "<SITUACION>" + wvarSitu + "</SITUACION>" + "<PDF64>" + mvarImpreso64 + "</PDF64>" + "</Response>" + "</LBA_WS>" );
        fncPutSolicitud = true;
      }
      else
      {
        wvarMensaje.set( mvarWDB_MENSAJE );
        wvarStep = 750;
        pvarRes.set( "<LBA_WS res_code=\"-54\" res_msg=\"Error\"><Response><Estado resultado=\"false\" mensaje=" + wvarMensaje + " /></Response></LBA_WS>" );
      }

      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = null;
      wobjClass = (Object) null;
      return fncPutSolicitud;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\">" + "<Response>" + "<REQUESTID>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) )  ) + "</REQUESTID>" + "<POLIZANN>" + mvarWDB_POLIZANN + "</POLIZANN>" + "<POLIZSEC>" + mvarWDB_POLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + mvarWDB_CERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + mvarWDB_CERTIANN + "</CERTIANN>" + "<CERTISEC>" + mvarWDB_CERTISEC + "</CERTISEC>" + "<SUPLENUM>" + mvarWDB_SUPLENUM + "</SUPLENUM>" + "<MENSAJE>" + wvarMensaje + "</MENSAJE>" + "<SITUACION>" + wvarSitu + "</SITUACION>" + "<PDF64><IMPCERTI/><IMPSOLI/><IMPPOLI/></PDF64>" + "</Response>" + "</LBA_WS>" );

        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        //fncPutSolicitud = False
        //mobjCOM_Context.SetComplete
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutSolicitud;
  }

  private boolean fncIntegrarSolicitudMQ( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncIntegrarSolicitudMQ = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String mvarRequest = "";
    String mvarRequest1 = "";
    String mvarResponse = "";
    Variant oProd = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended mobjXMLDoc = null;
    lbawA_OVMQGen.lbaw_MQMensaje wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
    String mvarProdok = "";
    org.w3c.dom.NodeList wobjXMLList = null;
    int wvarContNode = 0;
    String vNROCOTI = "";
    Variant vMES = new Variant();
    Variant vANO = new Variant();
    String vFECINIVG = "";
    Variant vMESsig = new Variant();
    String vANOsig = "";
    String vFECFINVG = "";
    String vTPLANATM = "";
    String vPOLIZANN = "";
    String vPOLIZSEC = "";
    String vREQUESTID = "";
    String vUSUARCOD = "";
    String vNROSOLTMP = "";
    String vCANAL = "";
    String vNROSOLI = "";
    String vSOLICITU = "";
    String vCOBROCOD = "";
    Variant vCOPROTIP = new Variant();
    String vCUENNUME = "";
    String vCOBROTIP = "";
    String vCLIENNOM = "";
    String vCLIENAP1 = "";
    String vCLIENAP2 = "";
    String vSEXO = "";
    String vNACIMANN = "";
    String vNACIMMES = "";
    String vNACIMDIA = "";
    String vESTADO = "";
    String vEMAIL = "";
    String vTIPODOCU = "";
    String vNUMEDOCU = "";
    String vCLIENIVA = "";
    String vDOMICDOM = "";
    String vDOMICDNU = "";
    String vDOMICPIS = "";
    String vDOMICPTA = "";
    String vDOMICPOB = "";
    String vLOCALIDAD = "";
    String vPROVCOD = "";
    String vTELNRO = "";
    String vBANELCO = "";
    String vCANAL_SUCURSAL = "";
    String vLEGAJCIA = "";
    String vLEGAJO_VEND = "";
    String vLEGAJAPEAP = "";
    String vLEGAJNOMAP = "";
    String vDOMICDOMCOR = "";
    String vDOMICDNUCOR = "";
    String vDOMICPISCOR = "";
    String vDOMICPTACOR = "";
    String vDOMICPOBCOR = "";
    String vLOCALIDADCODCOR = "";
    String vPROVICOR = "";




    wvarStep = 0;
    wvarCodErr.set( 0 );
    try 
    {

      wvarStep = 2;
      // Traigo los par�metros del request
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 3;
      vNROCOTI = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/COT_NRO" )  == (org.w3c.dom.Node) null) )
      {
        vNROCOTI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/COT_NRO" )  );
      }

      wvarStep = 4;
      vREQUESTID = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/REQUESTID" )  == (org.w3c.dom.Node) null) )
      {
        vREQUESTID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/REQUESTID" )  );
      }

      wvarStep = 5;
      vUSUARCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/USUARCOD" )  == (org.w3c.dom.Node) null) )
      {
        vUSUARCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/USUARCOD" )  );
      }

      wvarStep = 6;
      vFECINIVG = Funciones.convertirFecha( new Variant( DateTime.dateSerial( DateTime.year( DateTime.now() ), DateTime.month( DateTime.now() ), 1 ) )/*warning: ByRef value change will be lost.*/ );

      wvarStep = 7;
      vFECFINVG = Funciones.convertirFecha( new Variant( DateTime.dateSerial( DateTime.year( DateTime.now() ), DateTime.month( DateTime.now() ) + 1, 1 ) )/*warning: ByRef value change will be lost.*/ );

      wvarStep = 8;
      vCANAL = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CANAL" )  == (org.w3c.dom.Node) null) )
      {
        vCANAL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CANAL" )  );
      }

      wvarStep = 9;
      vCANAL_SUCURSAL = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CANAL_SUCURSAL" )  == (org.w3c.dom.Node) null) )
      {
        vCANAL_SUCURSAL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CANAL_SUCURSAL" )  );
      }

      wvarStep = 10;
      vNROSOLI = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NROSOLI" )  == (org.w3c.dom.Node) null) )
      {
        vNROSOLI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NROSOLI" )  );
      }


      wvarStep = 12;
      vPOLIZANN = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/POLIZANN" )  == (org.w3c.dom.Node) null) )
      {
        vPOLIZANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/POLIZANN" )  );
      }

      wvarStep = 13;
      vPOLIZSEC = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/POLIZSEC" )  == (org.w3c.dom.Node) null) )
      {
        vPOLIZSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/POLIZSEC" )  );
      }

      wvarStep = 14;
      vSOLICITU = Strings.right( "0000" + vCANAL, 4 ) + Strings.right( ("000000" + vNROSOLI), 6 ) + "ATA1" + Strings.right( ("00" + vPOLIZANN), 2 ) + Strings.right( ("000000" + vPOLIZSEC), 6 );

      wvarStep = 15;
      vTPLANATM = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/TPLANATM" )  == (org.w3c.dom.Node) null) )
      {
        vTPLANATM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/TPLANATM" )  );
      }

      wvarStep = 16;
      vCOBROCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/FPAGO" )  == (org.w3c.dom.Node) null) )
      {
        vCOBROCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/FPAGO" )  );
      }

      wvarStep = 17;
      vCOBROTIP = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/COBROTIP" )  == (org.w3c.dom.Node) null) )
      {
        vCOBROTIP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/COBROTIP" )  );
      }

      wvarStep = 18;
      vCUENNUME = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CUENNUME" )  == (org.w3c.dom.Node) null) )
      {
        vCUENNUME = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CUENNUME" )  );
      }

      wvarStep = 19;
      vCLIENNOM = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENNOM" )  == (org.w3c.dom.Node) null) )
      {
        vCLIENNOM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENNOM" )  );
      }

      wvarStep = 20;
      vCLIENAP1 = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENAP1" )  == (org.w3c.dom.Node) null) )
      {
        vCLIENAP1 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENAP1" )  );
      }

      wvarStep = 21;
      vCLIENAP2 = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENAP2" )  == (org.w3c.dom.Node) null) )
      {
        vCLIENAP2 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENAP2" )  );
      }

      wvarStep = 22;
      vSEXO = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/SEXO" )  == (org.w3c.dom.Node) null) )
      {
        vSEXO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/SEXO" )  );
      }

      wvarStep = 23;
      vNACIMANN = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NACIMANN" )  == (org.w3c.dom.Node) null) )
      {
        vNACIMANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NACIMANN" )  );
      }

      wvarStep = 24;
      vNACIMMES = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NACIMMES" )  == (org.w3c.dom.Node) null) )
      {
        vNACIMMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NACIMMES" )  );
      }

      wvarStep = 25;
      vNACIMDIA = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NACIMDIA" )  == (org.w3c.dom.Node) null) )
      {
        vNACIMDIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NACIMDIA" )  );
      }

      wvarStep = 26;
      vESTADO = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/ESTADO" )  == (org.w3c.dom.Node) null) )
      {
        vESTADO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/ESTADO" )  );
      }

      wvarStep = 27;
      vEMAIL = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/EMAIL" )  == (org.w3c.dom.Node) null) )
      {
        vEMAIL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/EMAIL" )  );
      }

      wvarStep = 28;
      vTIPODOCU = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/TIPODOCU" )  == (org.w3c.dom.Node) null) )
      {
        vTIPODOCU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/TIPODOCU" )  );
      }

      wvarStep = 29;
      vNUMEDOCU = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NUMEDOCU" )  == (org.w3c.dom.Node) null) )
      {
        vNUMEDOCU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/NUMEDOCU" )  );
      }

      wvarStep = 30;
      vCLIENIVA = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENIVA" )  == (org.w3c.dom.Node) null) )
      {
        vCLIENIVA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/CLIENIVA" )  );
      }

      wvarStep = 31;
      vDOMICDOM = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDOM" )  == (org.w3c.dom.Node) null) )
      {
        vDOMICDOM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDOM" )  );
      }

      wvarStep = 32;
      vDOMICDNU = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDNU" )  == (org.w3c.dom.Node) null) )
      {
        vDOMICDNU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDNU" )  );
      }

      wvarStep = 33;
      vDOMICPIS = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPIS" )  == (org.w3c.dom.Node) null) )
      {
        vDOMICPIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPIS" )  );
      }

      wvarStep = 34;
      vDOMICPTA = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPTA" )  == (org.w3c.dom.Node) null) )
      {
        vDOMICPTA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPTA" )  );
      }

      wvarStep = 35;
      vDOMICPOB = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPOB" )  == (org.w3c.dom.Node) null) )
      {
        vDOMICPOB = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPOB" )  );
      }

      wvarStep = 36;
      vLOCALIDAD = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LOCALIDAD" )  == (org.w3c.dom.Node) null) )
      {
        vLOCALIDAD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LOCALIDAD" )  );
      }

      wvarStep = 37;
      vPROVCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/PROVCOD" )  == (org.w3c.dom.Node) null) )
      {
        vPROVCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/PROVCOD" )  );
      }

      wvarStep = 311;
      vDOMICDOMCOR = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDOMCOR" )  == (org.w3c.dom.Node) null) )
      {
        vDOMICDOMCOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDOMCOR" )  );
      }

      wvarStep = 321;
      vDOMICDNUCOR = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDNUCOR" )  == (org.w3c.dom.Node) null) )
      {
        vDOMICDNUCOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICDNUCOR" )  );
      }

      wvarStep = 331;
      vDOMICPISCOR = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPISCOR" )  == (org.w3c.dom.Node) null) )
      {
        vDOMICPISCOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPISCOR" )  );
      }

      wvarStep = 341;
      vDOMICPTACOR = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPTACOR" )  == (org.w3c.dom.Node) null) )
      {
        vDOMICPTACOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPTACOR" )  );
      }

      wvarStep = 351;
      vDOMICPOBCOR = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPOBCOR" )  == (org.w3c.dom.Node) null) )
      {
        vDOMICPOBCOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/DOMICPOBCOR" )  );
      }

      wvarStep = 361;
      vLOCALIDADCODCOR = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LOCALIDADCODCOR" )  == (org.w3c.dom.Node) null) )
      {
        vLOCALIDADCODCOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LOCALIDADCODCOR" )  );
      }

      wvarStep = 371;
      vPROVICOR = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/PROVICOR" )  == (org.w3c.dom.Node) null) )
      {
        vPROVICOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/PROVICOR" )  );
      }

      wvarStep = 38;
      vTELNRO = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/TELNRO" )  == (org.w3c.dom.Node) null) )
      {
        vTELNRO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/TELNRO" )  );
      }

      wvarStep = 39;
      vBANELCO = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/BANELCO" )  == (org.w3c.dom.Node) null) )
      {
        vBANELCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/BANELCO" )  );
      }

      wvarStep = 40;
      vLEGAJCIA = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJCIA" )  == (org.w3c.dom.Node) null) )
      {
        vLEGAJCIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJCIA" )  );
      }

      wvarStep = 41;
      vLEGAJO_VEND = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJO_VEND" )  == (org.w3c.dom.Node) null) )
      {
        vLEGAJO_VEND = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJO_VEND" )  );
      }

      wvarStep = 42;
      vLEGAJAPEAP = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJAPEAP" )  == (org.w3c.dom.Node) null) )
      {
        vLEGAJAPEAP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJAPEAP" )  );
      }

      wvarStep = 43;
      vLEGAJNOMAP = "";
      if( ! (wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJNOMAP" )  == (org.w3c.dom.Node) null) )
      {
        vLEGAJNOMAP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/Request/LEGAJNOMAP" )  );
      }

      wvarStep = 44;
      //Armo el Request para integrar
      mvarRequest = "<Request><DEFINICION>2210_IntegracionGral.xml</DEFINICION>";
      mvarRequest = mvarRequest + "<INTERSEC>NOBK</INTERSEC>";
      mvarRequest = mvarRequest + "<USUARCOD>" + vUSUARCOD + "</USUARCOD>";
      mvarRequest = mvarRequest + "<SECUECOD>" + Funciones.convertirFecha( new Variant( DateTime.now() )/*warning: ByRef value change will be lost.*/ ) + "</SECUECOD>";
      mvarRequest = mvarRequest + "<SOLICITU>" + vSOLICITU + "</SOLICITU>";
      mvarRequest = mvarRequest + "<FESOLANN>" + DateTime.year( DateTime.now() ) + "</FESOLANN>";
      mvarRequest = mvarRequest + "<FESOLMES>" + Strings.right( ("0" + DateTime.month( DateTime.now() )), 2 ) + "</FESOLMES>";
      mvarRequest = mvarRequest + "<FESOLDIA>" + Strings.right( ("0" + DateTime.day( DateTime.now() )), 2 ) + "</FESOLDIA>";
      mvarRequest = mvarRequest + "<EFECTANN>" + DateTime.year( DateTime.now() ) + "</EFECTANN>";
      mvarRequest = mvarRequest + "<EFECTMES>" + Strings.right( ("0" + DateTime.month( DateTime.now() )), 2 ) + "</EFECTMES>";
      mvarRequest = mvarRequest + "<EFECTDIA>01</EFECTDIA>";
      mvarRequest = mvarRequest + "<VENCIANN>" + DateTime.year( DateTime.dateSerial( DateTime.year( DateTime.now() ), (DateTime.month( DateTime.now() ) + 1), 1 ) ) + "</VENCIANN>";
      mvarRequest = mvarRequest + "<VENCIMES>" + Strings.right( ("0" + DateTime.month( DateTime.dateSerial( DateTime.year( DateTime.now() ), DateTime.month( DateTime.now() ) + 1, 1 ) )), 2 ) + "</VENCIMES>";
      mvarRequest = mvarRequest + "<VENCIDIA>01</VENCIDIA>";
      mvarRequest = mvarRequest + "<AGENTCLAPR>PR</AGENTCLAPR>";
      mvarRequest = mvarRequest + "<AGENTCODPR></AGENTCODPR>";
      mvarRequest = mvarRequest + "<AGENTCLAPR2/>";
      mvarRequest = mvarRequest + "<AGENTCODPR2>0</AGENTCODPR2>";
      mvarRequest = mvarRequest + "<AGENTNOMPR2/>";
      mvarRequest = mvarRequest + " <AGENTCLAOR>OR</AGENTCLAOR>";
      mvarRequest = mvarRequest + "<AGENTCODOR></AGENTCODOR>";
      mvarRequest = mvarRequest + "<COMISIONPR1/>";
      mvarRequest = mvarRequest + "<COMISIONPR2>0</COMISIONPR2>";
      mvarRequest = mvarRequest + "<COMISIONOR/>";
      mvarRequest = mvarRequest + "<PRIMA>0</PRIMA>";
      mvarRequest = mvarRequest + "<RECARGOADM>0</RECARGOADM>";
      mvarRequest = mvarRequest + "<RECARGOFIN>0</RECARGOFIN>";
      mvarRequest = mvarRequest + "<DERPOIMP>0</DERPOIMP>";
      mvarRequest = mvarRequest + "<PLANPCOD>0</PLANPCOD>";
      mvarRequest = mvarRequest + "<COBROCOD>" + vCOBROCOD + "</COBROCOD>";
      mvarRequest = mvarRequest + "<COBROTIP>" + vCOBROTIP + "</COBROTIP>";
      mvarRequest = mvarRequest + "<CUENNUME>" + vCUENNUME + "</CUENNUME>";
      mvarRequest = mvarRequest + "<VENCIANNT></VENCIANNT>";
      mvarRequest = mvarRequest + "<VENCIMEST></VENCIMEST>";
      mvarRequest = mvarRequest + "<MONEDA>1</MONEDA>";
      mvarRequest = mvarRequest + "<ACTIVPPAL/>";
      mvarRequest = mvarRequest + "<TIPOOPER>AP</TIPOOPER>";
      mvarRequest = mvarRequest + "<MARCOPER/>";
      mvarRequest = mvarRequest + "<CIAASCODANT/>";
      mvarRequest = mvarRequest + "<RAMOPCODANT>ATA1</RAMOPCODANT>";
      mvarRequest = mvarRequest + "<POLIZANNANT>" + vPOLIZANN + "</POLIZANNANT>";
      mvarRequest = mvarRequest + "<POLIZSECANT>" + vPOLIZSEC + "</POLIZSECANT>";
      mvarRequest = mvarRequest + "<CAUSAEND/>";
      mvarRequest = mvarRequest + "<ORIOPERA>OV</ORIOPERA>";
      mvarRequest = mvarRequest + "<CLIENTES>";
      mvarRequest = mvarRequest + "<CLIENTE>";
      mvarRequest = mvarRequest + "<TIPOCLIE>P</TIPOCLIE>";
      mvarRequest = mvarRequest + "<CLIENTIP>00</CLIENTIP>";
      mvarRequest = mvarRequest + "<CLIENNOM><![CDATA[" + vCLIENNOM + "]]></CLIENNOM>";
      mvarRequest = mvarRequest + "<CLIENAP1><![CDATA[" + vCLIENAP1 + "]]></CLIENAP1>";
      mvarRequest = mvarRequest + "<CLIENAP2><![CDATA[" + vCLIENAP2 + "]]></CLIENAP2>";
      mvarRequest = mvarRequest + "<CLIENSEX>" + vSEXO + "</CLIENSEX>";
      mvarRequest = mvarRequest + "<NACIMANN>" + vNACIMANN + "</NACIMANN>";
      mvarRequest = mvarRequest + "<NACIMMES>" + vNACIMMES + "</NACIMMES>";
      mvarRequest = mvarRequest + "<NACIMDIA>" + vNACIMDIA + "</NACIMDIA>";
      mvarRequest = mvarRequest + "<PAISSCODNAC>00</PAISSCODNAC>";
      mvarRequest = mvarRequest + "<CLIENEST>" + vESTADO + "</CLIENEST>";
      mvarRequest = mvarRequest + "<DOCUMTIP>" + vTIPODOCU + "</DOCUMTIP>";
      mvarRequest = mvarRequest + "<DOCUMDAT>" + vNUMEDOCU + "</DOCUMDAT>";
      mvarRequest = mvarRequest + "<CLIENIVA>" + vCLIENIVA + "</CLIENIVA>";
      mvarRequest = mvarRequest + "<CUITL>0</CUITL>";
      mvarRequest = mvarRequest + "<CLIEIBTP/>";
      mvarRequest = mvarRequest + "<CLIEIBNU>0</CLIEIBNU>";
      mvarRequest = mvarRequest + "<DOMICDOM>" + vDOMICDOM + "</DOMICDOM>";
      mvarRequest = mvarRequest + "<DOMICDNU>" + vDOMICDNU + "</DOMICDNU>";
      mvarRequest = mvarRequest + "<DOMICPIS>" + vDOMICPIS + "</DOMICPIS>";
      mvarRequest = mvarRequest + "<DOMICPTA>" + vDOMICPTA + "</DOMICPTA>";
      mvarRequest = mvarRequest + "<DOMICCPO>" + vLOCALIDAD + "</DOMICCPO>";
      mvarRequest = mvarRequest + "<DOMICPOB>" + vDOMICPOB + "</DOMICPOB>";
      mvarRequest = mvarRequest + "<PROVICOD>" + vPROVCOD + "</PROVICOD>";
      mvarRequest = mvarRequest + "<PAISSCODDOM>00</PAISSCODDOM>";
      mvarRequest = mvarRequest + "<TELPPAL>" + vTELNRO + "</TELPPAL>";
      mvarRequest = mvarRequest + "<TELTRAB/>";
      mvarRequest = mvarRequest + "<TELCELU/>";
      mvarRequest = mvarRequest + "<TELOTRO/>";
      mvarRequest = mvarRequest + "<EMAIL>" + vEMAIL + "</EMAIL>";
      mvarRequest = mvarRequest + "<DOMICCPOBNACAML/>";
      mvarRequest = mvarRequest + "<CATEGCLIEAML>0</CATEGCLIEAML>";
      mvarRequest = mvarRequest + "<CLIENSEC>0</CLIENSEC>";
      mvarRequest = mvarRequest + "</CLIENTE>";
      mvarRequest = mvarRequest + "</CLIENTES>";
      mvarRequest = mvarRequest + "<ANOTACIONES>";
      mvarRequest = mvarRequest + "<ANOTACION>";
      mvarRequest = mvarRequest + "<TIPOANOTA/>";
      mvarRequest = mvarRequest + "<ORDENTEXT>0</ORDENTEXT>";
      mvarRequest = mvarRequest + "<TEXTO/>";
      mvarRequest = mvarRequest + "</ANOTACION>";
      mvarRequest = mvarRequest + "</ANOTACIONES>";
      mvarRequest = mvarRequest + "</Request>";
      wvarStep = 45;
      //Armo el Request1 para la SolicitudMQ
      mvarRequest1 = "<Request><DEFINICION>2219_IntegracionATM.xml</DEFINICION>";
      mvarRequest1 = mvarRequest1 + "<INTERSEC>NOBK</INTERSEC>";
      mvarRequest1 = mvarRequest1 + "<USUARCOD>" + vUSUARCOD + "</USUARCOD>";
      mvarRequest1 = mvarRequest1 + "<SECUECOD>" + Funciones.convertirFecha( new Variant( DateTime.now() )/*warning: ByRef value change will be lost.*/ ) + "</SECUECOD>";
      mvarRequest1 = mvarRequest1 + "<SOLICITU>" + vSOLICITU + "</SOLICITU>";
      mvarRequest1 = mvarRequest1 + "<FINPOLIZA>S</FINPOLIZA>";
      mvarRequest1 = mvarRequest1 + "<RAMO>ATA1</RAMO>";
      mvarRequest1 = mvarRequest1 + "<ORIOPERA>OV</ORIOPERA>";
      mvarRequest1 = mvarRequest1 + "<ITEM>1</ITEM>";
      mvarRequest1 = mvarRequest1 + "<RIESGTIP>27</RIESGTIP>";
      mvarRequest1 = mvarRequest1 + "<TIPOMOVI>I</TIPOMOVI>";
      mvarRequest1 = mvarRequest1 + "<CERTIPOLANT>0</CERTIPOLANT>";
      mvarRequest1 = mvarRequest1 + "<CERTIANNANT>0</CERTIANNANT>";
      mvarRequest1 = mvarRequest1 + "<CERTISECANT>0</CERTISECANT>";
      mvarRequest1 = mvarRequest1 + "<CLAUSAJUST>0</CLAUSAJUST>";
      mvarRequest1 = mvarRequest1 + "<TNROTARJ>" + vBANELCO + "</TNROTARJ>";
      mvarRequest1 = mvarRequest1 + "<TPLANATM>" + vTPLANATM + "</TPLANATM>";
      mvarRequest1 = mvarRequest1 + "<RDOMICDOM>" + vDOMICDOMCOR + "</RDOMICDOM>";
      mvarRequest1 = mvarRequest1 + "<RDOMICDNU>" + vDOMICDNUCOR + "</RDOMICDNU>";
      mvarRequest1 = mvarRequest1 + "<RDOMICPIS>" + vDOMICPISCOR + "</RDOMICPIS>";
      mvarRequest1 = mvarRequest1 + "<RDOMICPTA>" + vDOMICPTACOR + "</RDOMICPTA>";
      mvarRequest1 = mvarRequest1 + "<RDOMICCPO>" + vLOCALIDADCODCOR + "</RDOMICCPO>";
      mvarRequest1 = mvarRequest1 + "<RDOMICPOB>" + vDOMICPOBCOR + "</RDOMICPOB>";
      mvarRequest1 = mvarRequest1 + "<RPROVICOD>" + vPROVICOR + "</RPROVICOD>";
      mvarRequest1 = mvarRequest1 + "<RPAISSCOD>00</RPAISSCOD>";
      mvarRequest1 = mvarRequest1 + "<BANCOCOD>" + vCANAL + "</BANCOCOD>";
      mvarRequest1 = mvarRequest1 + "<SUCURCOD>" + vCANAL_SUCURSAL + "</SUCURCOD>";
      mvarRequest1 = mvarRequest1 + "<LEGAJAPERT>0000009991</LEGAJAPERT>";
      mvarRequest1 = mvarRequest1 + "<LEGAJCIERRE>0000009992</LEGAJCIERRE>";
      mvarRequest1 = mvarRequest1 + "<BANCOCODEM>" + vCANAL + "</BANCOCODEM>";
      mvarRequest1 = mvarRequest1 + "<LEGAJCIA>" + vLEGAJCIA + "</LEGAJCIA>";
      mvarRequest1 = mvarRequest1 + "<LEGAJNUMAP>" + vLEGAJO_VEND + "</LEGAJNUMAP>";
      mvarRequest1 = mvarRequest1 + "<COMISIONAP>S</COMISIONAP>";
      mvarRequest1 = mvarRequest1 + "<LEGAJAPEAP>" + vLEGAJAPEAP + "</LEGAJAPEAP>";
      mvarRequest1 = mvarRequest1 + "<LEGAJNOMAP>" + vLEGAJNOMAP + "</LEGAJNOMAP>";
      mvarRequest1 = mvarRequest1 + "<LEGAJNUMCR>" + vLEGAJO_VEND + "</LEGAJNUMCR>";
      mvarRequest1 = mvarRequest1 + "<LEGAJAPECR>" + vLEGAJAPEAP + "</LEGAJAPECR>";
      mvarRequest1 = mvarRequest1 + "<LEGAJNOMCR>" + vLEGAJNOMAP + "</LEGAJNOMCR>";
      mvarRequest1 = mvarRequest1 + "<COMISIONCR>S</COMISIONCR>";
      mvarRequest1 = mvarRequest1 + "<ANOTACIONES></ANOTACIONES>";

      wvarStep = 46;
      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      wobjClass.Execute( new Variant( mvarRequest ), new Variant( mvarResponse ), "" );
      wobjClass = (lbawA_OVMQGen.lbaw_MQMensaje) null;
      mobjXMLDoc = new XmlDomExtended();
      mobjXMLDoc.loadXML( mvarResponse );
      wvarStep = 47;
      mvarProdok = "ER";
      if( ! (mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended .getText( mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          // Obtengo el vNROSOLTMP
          wvarStep = 48;
          vNROSOLTMP = XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//NROSOLTMP" )  );
          //Agrego vNROSOLTMP al request1 de la SolicitudMQ
          wvarStep = 49;
          mvarRequest1 = mvarRequest1 + "<NROSOLTMP>" + vNROSOLTMP + "</NROSOLTMP></Request>";
          wvarStep = 50;
          //Llamo al mensaje de SolicitudMQ
          wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
          wobjClass.Execute( mvarRequest1, mvarResponse, "" );
          wobjClass = (lbawA_OVMQGen.lbaw_MQMensaje) null;
          mobjXMLDoc = new XmlDomExtended();
          mobjXMLDoc.loadXML( mvarResponse );
          wvarStep = 51;
          if( ! (mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
          {
            if( XmlDomExtended .getText( mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
            {
              //             If (mobjXMLDoc.selectSingleNode("//MENSAJE").Text) <> "" Then
              //                wvarCodErr = -111
              //                pvarRes = "<Response><Estado resultado=""false"" mensaje=" & mobjXMLDoc.selectSingleNode("//MENSAJE").Text & "/></Response>"
              //                mvarProdok = "ER"
              //                Exit Function
              //             Else
              pvarRes.set( mvarResponse );
              mvarProdok = "OK";
              //              End If
            }
          }
        }
      }
      wvarStep = 53;
      if( mvarProdok.equals( "ER" ) )
      {
        wvarCodErr.set( -53 );
        pvarRes.set( "<Response><Estado resultado=\"false\" mensaje=Poliza  no habilitada /></Response>" );
        return fncIntegrarSolicitudMQ;
      }


      mobjXMLDoc = null;
      wobjXMLRequest = null;
      wobjXMLList = (org.w3c.dom.NodeList) null;

      wvarStep = 54;
      if( wvarCodErr.toInt() == 0 )
      {
        fncIntegrarSolicitudMQ = true;
      }
      else
      {
        fncIntegrarSolicitudMQ = false;
      }

      fin: 
      return fncIntegrarSolicitudMQ;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncIntegrarSolicitudMQ = false;

        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncIntegrarSolicitudMQ;
  }

  public String fechaVige( Variant pvarStrFecha ) throws Exception
  {
    String fechaVige = "";
    String wvarIntAnio = "";
    String wvarIntMes = "";
    String wvarIntDia = "";
    String[] warrSplit = null;


    pvarStrFecha.set( Strings.format( pvarStrFecha.toString(), "General Date" ) );

    wvarIntAnio = String.valueOf( DateTime.year( pvarStrFecha.toDate() ) );
    wvarIntMes = Strings.right( "0" + DateTime.month( pvarStrFecha.toDate() ), 2 );
    wvarIntDia = Strings.right( "0" + DateTime.day( pvarStrFecha.toDate() ), 2 );

    fechaVige = wvarIntDia + "/" + wvarIntMes + "/" + wvarIntAnio;

    return fechaVige;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
