package com.qbe.services.interWSBrok.impl;

import java.net.MalformedURLException;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.exutils.ExceptionUtils;
import com.qbe.services.webmq.impl.lbaw_GetZona;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

import diamondedge.util.DateTime;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;

/**
 * Implementa el actionCode GetSolicitudAU_broker
 * 
 * Componente público, usado por los brokers
 * 
 * TODO Definir interface
 * 
 * 
 * @see com.qbe.services.interWSBrok.impl.GetCotizacionAU
 * 
 *      Sobre el código C010 El número de operación es el número de solicitud
 *      que se envía en el tag <REQUESTID> , hay veces que se producen
 *      reintentos con los mismos códigos y en la validación se fija que para
 *      esa operación no haya una solicitud que esté OK. Está bien que no emita
 *      porque esa solicitud ya fue emitida.
 * 
 * 
 *      Ver Bug 899
 * 
 * 
 * 
 * @author ramiro
 * 
 */
public class GetSolicitudAU extends BaseOSBClient implements VBObjectClass {

	protected static Logger logger = Logger.getLogger(GetSolicitudAU.class.getName());

	static final String mcteClassName = "LBA_InterWSBrok.GetSolicitudAU";
	static final String mcteStoreProc = "SPSNCV_BRO_SOLI_AUS1_GRABA";
	static final String mcteStoreProcSelect = "SPSNCV_BRO_SELECT_OPER_ESP";
	static final String mcteStoreProcAltaOperacion = "SPSNCV_BRO_ALTA_OPER";
	/**
	 * Parametros XML de Entrada
	 */
	static final String mcteParam_WDB_IDENTIFICACION_BROKER = "//CODINST";
	static final String mcteParam_WDB_NRO_OPERACION_BROKER = "//REQUESTID";
	static final String mcteParam_WDB_NRO_COTIZACION_LBA = "//COT_NRO";
	static final String mcteParam_WDB_TIPO_OPERACION = "//TIPOOPERACION";

	static final String mcteParam_PRODUCTOR = "//AGECOD";
	static final String mcteParam_NumeDocu = "//NUMEDOCU";
	static final String mcteParam_TipoDocu = "//TIPODOCU";
	static final String mcteParam_ClienApe1 = "//CLIENAP1";
	static final String mcteParam_ClienApe2 = "//CLIENAP2";
	static final String mcteParam_ClienNom1 = "//CLIENNOM";
	static final String mcteParam_NacimAnn = "//NACIMANN";
	static final String mcteParam_NacimMes = "//NACIMMES";
	static final String mcteParam_NacimDia = "//NACIMDIA";
	static final String mcteParam_CLIENSEX = "//SEXO";
	static final String mcteParam_CLIENEST = "//ESTADO";
	static final String mcteParam_EMAIL = "//EMAIL";
	/**
	 * Domicilio de riesgo
	 */
	static final String mcteParam_DOMICDOM = "//DOMICDOM";
	static final String mcteParam_DomicDnu = "//DOMICDNU";
	static final String mcteParam_DOMICPIS = "//DOMICPIS";
	static final String mcteParam_DomicPta = "//DOMICPTA";
	static final String mcteParam_DOMICPOB = "//DOMICPOB";
	static final String mcteParam_DOMICCPO = "//LOCALIDADCOD";
	static final String mcteParam_PROVICOD = "//PROVI";
	static final String mcteParam_TELCOD = "//TELCOD";
	static final String mcteParam_TELNRO = "//TELNRO";
	/**
	 * datos del domicilio de correspondencia
	 */
	static final String mcteParam_DOMICDOM_CO = "//DOMICDOMCOR";
	static final String mcteParam_DomicDnu_CO = "//DOMICDNUCOR";
	static final String mcteParam_DOMICPIS_CO = "//DOMICPISCOR";
	static final String mcteParam_DomicPta_CO = "//DOMICPTACOR";
	static final String mcteParam_DOMICPOB_CO = "//DOMICPOBCOR";
	static final String mcteParam_DOMICCPO_CO = "//LOCALIDADCODCOR";
	static final String mcteParam_PROVICOD_CO = "//PROVICOR";
	static final String mcteParam_TELCOD_CO = "//TELCODCOR";
	static final String mcteParam_TELNRO_CO = "//TELNROCOR";
	/**
	 * datos de la cuenta
	 */
	static final String mcteParam_COBROTIP = "//COBROTIP";
	static final String mcteParam_CUENNUME = "//CUENNUME";
	static final String mcteParam_VENCIANN = "//VENCIANN";
	static final String mcteParam_VENCIMES = "//VENCIMES";
	static final String mcteParam_VENCIDIA = "//VENCIDIA";
	/**
	 * datos de la operacion
	 */
	static final String mcteParam_FRANQCOD = "//FRANQCOD";
	static final String mcteParam_PLANCOD = "//PLANNCOD";
	static final String mcteParam_ZONA = "//CODZONA";
	static final String mcteParam_CLIENIVA = "//IVA";
	static final String mcteParam_SUMAASEG = "//SUMAASEG";
	static final String mcteParam_CLUB_LBA = "//CLUBLBA";
	static final String mcteParam_CPAANO = "//CPAANO";
	static final String mcteParam_CTAKMS = "//KMSRNGCOD";
	static final String mcteParam_Escero = "//ESCERO";
	static final String mcteParam_SIFMVEHI_DES = "//VEHDES";
	static final String mcteParam_MOTORNUM = "//MOTORNUM";
	static final String mcteParam_CHASINUM = "//CHASINUM";
	static final String mcteParam_PATENNUM = "//PATENNUM";
	/**
	 * AUMARCOD+AUMODCOD+AUSUBCOD+AUADICODAUMODORI
	 */
	static final String mcteParam_ModeAutCod = "//MODEAUTCOD";
	static final String mcteParam_VEHCLRCOD = "//VEHCLRCOD";
	static final String mcteParam_EfectAnn = "//EFECTANN";
	static final String mcteParam_GUGARAGE = "//SIGARAGE";
	static final String mcteParam_GUDOMICI = "//GUDOMICI";
	static final String mcteParam_AUCATCOD = "//AUCATCOD";
	static final String mcteParam_AUTIPCOD = "//AUTIPCOD";
	static final String mcteParam_AUANTANN = "//AUANTANN";
	static final String mcteParam_AUNUMSIN = "//SINIESTROS";
	static final String mcteParam_AUUSOGNC = "//GAS";
	static final String mcteParam_CampaCod = "//CAMPACOD";
	static final String mcteParam_CONDUCTORES = "//HIJOS/HIJO";
	static final String mcteParam_CONDUAPE = "APELLIDOHIJO";
	static final String mcteParam_CONDUNOM = "NOMBREHIJO";
	static final String mcteParam_CONDUFEC = "NACIMHIJO";
	static final String mcteParam_CONDUSEX = "SEXOHIJO";
	static final String mcteParam_CONDUEST = "ESTADOHIJO";
	static final String mcteParam_CONDUEXC = "INCLUIDO";
	static final String mcteParam_ACCESORIOS = "//ACCESORIOS/ACCESORIO";
	static final String mcteParam_AUACCCOD = "CODIGOACC";
	static final String mcteParam_AUVEASUM = "PRECIOACC";
	static final String mcteParam_AUVEADES = "DESCRIPCIONACC";
	static final String mcteParam_INSPECOD = "//INSPECOD";
	static final String mcteParam_RASTREO = "//RASTREO";
	static final String mcteParam_ALARMCOD = "//ALARMCOD";
	static final String mcteParam_COBROCOD = "//COBROCOD";
	static final String mcteParam_ANOTACIO = "//ANOTACIONES";
	static final String mcteParam_VIGENANN = "//VIGENANN";
	static final String mcteParam_VIGENMES = "//VIGENMES";
	static final String mcteParam_VIGENDIA = "//VIGENDIA";
	/**
	 * hora en que empezo la transaccion
	 */
	static final String mcteParam_MSGANO = "//MSGANO";
	static final String mcteParam_MSGMES = "//MSGMES";
	static final String mcteParam_MSGDIA = "//MSGDIA";
	static final String mcteParam_MSGHORA = "//MSGHORA";
	static final String mcteParam_MSGMINUTO = "//MSGMINUTO";
	static final String mcteParam_MSGSEGUNDO = "//MSGSEGUNDO";
	/**
	 * Datos de la cotizacion
	 */
	static final String mcteParm_TIEMPO = "//TIEMPO";
	static final String mcteParam_DESTRUCCION_80 = "//DESTRUCCION_80";
	static final String mcteParam_Luneta = "//LUNETA";
	static final String mcteParam_ClubEco = "//CLUBECO";
	static final String mcteParam_Robocont = "//ROBOCONT";
	static final String mcteParam_Granizo = "//GRANIZO";
	static final String mcteParam_IBB = "//IBB";
	static final String mcteParam_NROIBB = "//NROIBB";
	static final String mcteParam_INSTALADP = "//INSTALADP";
	static final String mcteParam_POSEEDISP = "//POSEEDISP";
	static final String mcteParam_CUITNUME = "//CUITNUME";
	static final String mcteParam_RAZONSOC = "//RAZONSOC";
	static final String mcteParam_CLIENTIP = "//CLIENTIP";
	static final String mcteParam_ACREPREN = "//ACRE-PREN";
	static final String mcteParam_NUMEDOCUACRE = "//NUMEDOCU-ACRE";
	static final String mcteParam_TIPODOCUACRE = "//TIPODOCU-ACRE";
	static final String mcteParam_APELLIDOACRE = "//APELLIDO-ACRE";
	static final String mcteParam_NOMBRESACRE = "//NOMBRES-ACRE";
	static final String mcteParam_DOMICDOMACRE = "//DOMICDOM-ACRE";
	static final String mcteParam_DOMICDNUACRE = "//DOMICDNU-ACRE";
	static final String mcteParam_DOMICPISACRE = "//DOMICPIS-ACRE";
	static final String mcteParam_DOMICPTAACRE = "//DOMICPTA-ACRE";
	static final String mcteParam_DOMICPOBACRE = "//DOMICPOB-ACRE";
	static final String mcteParam_DOMICCPOACRE = "//DOMICCPO-ACRE";
	static final String mcteParam_PROVICODACRE = "//PROVICOD-ACRE";
	static final String mcteParam_PAISSCODACRE = "//PAISSCOD-ACRE";
	/**
	 * AM
	 */
	static final String mcteParam_BANCOCOD = "//BANCOCOD";
	static final String mcteParam_EmpresaCodigo = "//CANAL";
	static final String mcteParam_SucursalCodigo = "//CANAL_SUCURSAL";
	static final String mcteParam_LegajoVend = "//LEGAJO_VEND";
	static final String mcteParam_LegajoCia = "//LEGAJCIA";
	/**
	 * Nro NewSas para mcteParam_EmpresaCodigo(CANAL) = 150 (HSBC)
	 */
	static final String mcteParam_NROSOLI = "//NROSOLI";
	/**
	 * Asegurados Adicionales
	 */
	static final String mcteParam_ASEGADIC = "//ASEG-ADIC";
	static final String mcteParam_ASEGURADOS = "//ASEGURADOS-ADIC/ASEGURADO-ADIC";
	static final String mcteParam_DOCUMASEG = "DOCUMASEG";
	static final String mcteParam_NOMBREASEG = "NOMBREASEG";
	static final String mcteParam_NROFACTU = "//NROFACTU";
	static final String mcteParam_CENSICOD = "//CENSICOD";

	public int IAction_Execute(String pvarRequest, StringHolder pvarResponse, String pvarContextInfo) {
		int IAction_Execute = 0;
		String wvarMensaje = "";
		StringHolder pvarRes = new StringHolder();
		try {
			pvarRequest = Strings.mid(pvarRequest, 1, Strings.find(1, pvarRequest, ">")) 
					+ "<MSGANO>" + DateTime.year(DateTime.now()) + "</MSGANO>"
					+ "<MSGMES>" + DateTime.month(DateTime.now()) + "</MSGMES>" 
					+ "<MSGDIA>" + DateTime.day(DateTime.now()) + "</MSGDIA>" 
					+ "<MSGHORA>"+ DateTime.hour(DateTime.now()) + "</MSGHORA>" 
					+ "<MSGMINUTO>" + DateTime.minute(DateTime.now()) + "</MSGMINUTO>" 
					+ "<MSGSEGUNDO>" + DateTime.second(DateTime.now()) + "</MSGSEGUNDO>" 
					+ "<TIPOOPERACION>S</TIPOOPERACION>"
					+ Strings.mid(pvarRequest, (Strings.find(1, pvarRequest, ">") + 1));
			pvarRes.set("");
			boolean fncResult = fncGetAll(pvarRequest, wvarMensaje, pvarRes);
			pvarResponse.set("<Response_WS>" + pvarRes.getValue() + "</Response_WS>");
			String loggedResponse = Utils.formatLogResponseChompPDF64(pvarResponse);
			logger.log(Level.FINEST, this.getClass().getName() + " response es: " + loggedResponse);

			IAction_Execute = 0;
			return IAction_Execute;
		} catch (Exception _e_) {
			logger.log(Level.SEVERE, "en GetSolicitudAU", _e_);
			IAction_Execute = 1;
			String causes = ExceptionUtils.getCauses(_e_);

			// Logueo el timestamp para que si el usuario reporta el error
			// podamos usarlo para buscar en los logs
			String resultResponse = "<LBA_WS res_code=\"-1000\" res_msg=\"" + "Error inesperado [" + _e_.getMessage() + "] - TS-" + System.currentTimeMillis() + " CI - " + pvarContextInfo+ " \">" + "<![CDATA[" + causes + "]]>" + "</LBA_WS>";
			pvarResponse.set("<Response_WS>" + resultResponse + "</Response_WS>");
		}
		return IAction_Execute;
	}

	private boolean fncGetAll(String pvarRequest, String wvarMensaje, StringHolder pvarRes) throws OSBConnectorException, XmlDomExtendedException,
			SQLException, MalformedURLException, TransformerException {
		boolean fncGetAll = false;
		String requestDespuesDeValidacionSolAU = "";
		StringHolder wvarMensaje3 = new StringHolder();
		String wvarMensajeI = "";
		String wvarInputImpre = "";
		XmlDomExtended wobjXMLRequestExists = null;
		XmlDomExtended requestDespuesDeValidacionSolAUXML = null;
		Variant mvarCertiSec = new Variant();
		String mvarWDB_ESTADO = "";
		String mvarEmpresaCodigo = "";
		String mvarSucursalCodigo = "";
		String mvarCodError = "";

		wvarInputImpre = pvarRequest;

		GetValidacionSolAU gvsa = new GetValidacionSolAU();
		gvsa.setOsbConnector(getOsbConnector());
		StringHolder gvsaSH = new StringHolder();
		gvsa.IAction_Execute(pvarRequest, gvsaSH, null);
		requestDespuesDeValidacionSolAU = gvsaSH.getValue();

		// Si esta todo OK Llama a la grabacion de solicitud
		requestDespuesDeValidacionSolAUXML = new XmlDomExtended();
		requestDespuesDeValidacionSolAUXML.loadXML(requestDespuesDeValidacionSolAU);

		mvarCertiSec.set(0);

		// Si la validación vino con error y tiene otro encabezado, lo fuerza a ER
		if (requestDespuesDeValidacionSolAUXML.selectSingleNode("//Estado/@resultado") == (org.w3c.dom.Node) null) {
			mvarWDB_ESTADO = "ER";
		} else {
			mvarWDB_ESTADO = XmlDomExtended.getText(requestDespuesDeValidacionSolAUXML.selectSingleNode("//Estado/@resultado"));
		}
		if (!(mvarWDB_ESTADO.equalsIgnoreCase("TRUE"))) {
			wobjXMLRequestExists = new XmlDomExtended();
			wobjXMLRequestExists.loadXML(pvarRequest);
			// Si el Error es C010, entoces es una Solicitud que está Ok.
			// Busco el CertiSec y Guardo la Operacion unicamente
			// Sino, lo trato como error

			if (requestDespuesDeValidacionSolAUXML.selectSingleNode("//LBA_WS/@res_code") != null) {
				mvarCodError = XmlDomExtended.marshal(requestDespuesDeValidacionSolAUXML.selectSingleNode("//LBA_WS/@res_code"));
			} else {
				mvarCodError = "-";
			}
			if (mvarCodError.equalsIgnoreCase("C010")) {
				mvarWDB_ESTADO = callSelectOperEsp(pvarRes, wobjXMLRequestExists, mvarCertiSec);
			} else {
				if (Strings.left(mvarWDB_ESTADO, 1).equals("-")) {
					mvarWDB_ESTADO = "ER";
				}
				pvarRes.set(requestDespuesDeValidacionSolAU);
			}
			requestDespuesDeValidacionSolAUXML = new XmlDomExtended();
			requestDespuesDeValidacionSolAUXML.loadXML(pvarRequest);
		} else {
			// Grabo la solicitud
			mvarWDB_ESTADO = "OK";

			mvarCertiSec.set(XmlDomExtended.getText(requestDespuesDeValidacionSolAUXML.selectSingleNode("//CERTISEC")));

			//Ticket 1435
			// NUEVO PARA INSIS
			if ( XmlDomExtended.getText(requestDespuesDeValidacionSolAUXML.selectSingleNode("//NROSOLI")) == null ) {
				requestDespuesDeValidacionSolAU = requestDespuesDeValidacionSolAU.replace("</Request>", "<NROSOLI>" + mvarCertiSec + "</NROSOLI></Request>");
			}
			// Fin Ticket 1435
			
			if (!fncGrabaSolicitud(requestDespuesDeValidacionSolAU, wvarMensaje3, mvarCertiSec)) {
				pvarRes.set("<LBA_WS res_code=\"-215\" res_msg=\"Error al intentar obtener el numero de poliza\"></LBA_WS>");
				mvarWDB_ESTADO = "ER";
			} else {
				if (mvarCertiSec.toInt() >= 0) {

					mvarEmpresaCodigo = "0";
					if (!(requestDespuesDeValidacionSolAUXML.selectSingleNode(mcteParam_EmpresaCodigo) == (org.w3c.dom.Node) null)) {
						mvarEmpresaCodigo = XmlDomExtended.getText(requestDespuesDeValidacionSolAUXML.selectSingleNode(mcteParam_EmpresaCodigo));
					}
					mvarSucursalCodigo = "0";
					if (!(requestDespuesDeValidacionSolAUXML.selectSingleNode(mcteParam_SucursalCodigo) == (org.w3c.dom.Node) null)) {
						mvarSucursalCodigo = XmlDomExtended.getText(requestDespuesDeValidacionSolAUXML.selectSingleNode(mcteParam_SucursalCodigo));
					}

					// Solamente pido el impreso si no es HSBC ( 150 )
					if (!(Obj.toInt(mvarEmpresaCodigo) == 150)) {
						wvarInputImpre = Strings.replace(wvarInputImpre, "</Request>", "<CERTISEC>" + mvarCertiSec + "</CERTISEC></Request>");
						logger.log(Level.FINE, "Llamando localmente a GetImpreSolAU");
						GetImpreSolAU impreSolAu = new GetImpreSolAU();
						impreSolAu.setOsbConnector(getOsbConnector());
						StringHolder impreSolAuSH = new StringHolder();
						impreSolAu.IAction_Execute(wvarInputImpre, impreSolAuSH, "");
						wvarMensajeI = impreSolAuSH.getValue();
						 
						if (wvarMensajeI.indexOf("<Response>") != -1) {
							wvarMensajeI = wvarMensajeI.substring(wvarMensajeI.indexOf("<Response>") + 10, wvarMensajeI.indexOf("</Response>"));
						}
					} else {
						wvarMensajeI = "";
					}
					pvarRes.set(
							"<LBA_WS res_code=\"0\" res_msg=\"\">" 
							+ "<Response>" 
								+ "<REQUESTID>"+ XmlDomExtended.getText(requestDespuesDeValidacionSolAUXML.selectSingleNode("//REQUESTID")) + "</REQUESTID>" 
								+ "<CERTISEC>" + mvarCertiSec + "</CERTISEC>" 
								+ "<PDF64>" + wvarMensajeI + "</PDF64>" 
								+ "<Estpol>"+ XmlDomExtended.getText(requestDespuesDeValidacionSolAUXML.selectSingleNode("//EMISION/Estpol")) + "</Estpol>" 
								+ "<MotivoRet>"+ XmlDomExtended.getText(requestDespuesDeValidacionSolAUXML.selectSingleNode("//EMISION/MotivoRet")) + "</MotivoRet>" 
								+ "<PolizaNo>"+ XmlDomExtended.getText(requestDespuesDeValidacionSolAUXML.selectSingleNode("//EMISION/PolizaNo")) + "</PolizaNo>" 
								+ "<TipoCerti>"+ XmlDomExtended.getText(requestDespuesDeValidacionSolAUXML.selectSingleNode("//EMISION/TipoCerti")) + "</TipoCerti>" 
								+ "<TipoInspe>"+ XmlDomExtended.getText(requestDespuesDeValidacionSolAUXML.selectSingleNode("//EMISION/TipoInspe")) + "</TipoInspe>" 
								+ "<TipoRastreo>"+ XmlDomExtended.getText(requestDespuesDeValidacionSolAUXML.selectSingleNode("//EMISION/TipoRastreo")) + "</TipoRastreo>" 
							+ "</Response>" 
							+ "</LBA_WS>");
				} else {
					pvarRes.set("<LBA_WS res_code=\"-215\" res_msg=\"Error al intentar obtener el numero de poliza\"></LBA_WS>");
				}
			}
		}
		String loggedResponse = Utils.formatLogResponseChompPDF64(pvarRes);
		// Grabo la operacion
		grabarOperacionEnBase(pvarRequest, requestDespuesDeValidacionSolAUXML, mvarCertiSec, mvarWDB_ESTADO, mvarEmpresaCodigo, mvarSucursalCodigo, loggedResponse);

		fncGetAll = true;
		return fncGetAll;
	}

	protected String callSelectOperEsp(StringHolder pvarRes, XmlDomExtended wobjXMLRequestExists, Variant mvarCertiSec) throws SQLException, XmlDomExtendedException {
		String mvarWDB_ESTADO;
		java.sql.Connection jdbcConn = null;
		try {
			com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
			jdbcConn = connFactory.getJDBCConnection(General.mcteDB);
			String sp = "{call SPSNCV_BRO_SELECT_OPER_ESP(?,?,?,?)}";
			CallableStatement ps = jdbcConn.prepareCall(sp);
			//
			// wvarStep = 50;
			// wobjDBParm = new Parameter("@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(XmlDomExtended.getText(wobjXMLRequestExists.selectSingleNode("//CODINST"))));
			ps.setInt(1, Integer.parseInt(XmlDomExtended.getText(wobjXMLRequestExists.selectSingleNode("//CODINST"))));
			//
			// wvarStep = 51;
			// wobjDBParm = new Parameter("@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(XmlDomExtended.getText(wobjXMLRequestExists.selectSingleNode("//REQUESTID"))));
			ps.setInt(2, Integer.parseInt(XmlDomExtended.getText(wobjXMLRequestExists.selectSingleNode("//REQUESTID"))));
			//
			// wvarStep = 52;
			// wobjDBParm = new Parameter("@WDB_TIPO_OPERACION", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant("S"));
			ps.setString(3, "S");
			//
			// wvarStep = 53;
			// wobjDBParm = new Parameter("@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant("OK"));
			ps.setString(4, "OK");
			boolean result = ps.execute();

			if (result) {
				// El primero es un ResultSet
			} else {
				// Salteo el "updateCount"
				int uc = ps.getUpdateCount();
				if (uc == -1) {
					// System.out.println("No hay resultados");
				}
				result = ps.getMoreResults();
			}

			java.sql.ResultSet cursor = ps.getResultSet();
			cursor.next();
			pvarRes.set("<LBA_WS res_code=\"0\" res_msg=\"\">" + "<Response>" + "<REQUESTID>" + XmlDomExtended.getText(wobjXMLRequestExists.selectSingleNode("//REQUESTID")) + "</REQUESTID>" + "<CERTISEC>" + cursor.getString(13) + "</CERTISEC>" + "</Response>" + "</LBA_WS>");
			mvarCertiSec.set(cursor.getString(13));
			mvarWDB_ESTADO = "OK";
			wobjXMLRequestExists = null;
			cursor.close();
			jdbcConn.close();
		} finally {
			try {
				if (jdbcConn != null)
					jdbcConn.close();
			} catch (Exception e) {
				logger.log(Level.WARNING, "Exception al hacer un close", e);
			}
		}
		return mvarWDB_ESTADO;
	}

	protected void grabarOperacionEnBase(String pvarRequest, XmlDomExtended wobjXMLRequestSolis, Variant mvarCertiSec, String mvarWDB_ESTADO, String mvarEmpresaCodigo, String mvarSucursalCodigo, String loggedResponse) throws XmlDomExtendedException, SQLException {
		String mvarWDB_IDENTIFICACION_BROKER;
		String mvarWDB_NRO_OPERACION_BROKER;
		String mvarWDB_NRO_COTIZACION_LBA;
		String mvarWDB_TIPO_OPERACION;
		String mvarWDB_CERTIANN;
		String mvarWDB_RECEPCION_PEDIDOANO;
		String mvarWDB_RECEPCION_PEDIDOMES;
		String mvarWDB_RECEPCION_PEDIDODIA;
		String mvarWDB_RECEPCION_PEDIDOHORA;
		String mvarWDB_RECEPCION_PEDIDOMINUTO;
		String mvarWDB_RECEPCION_PEDIDOSEGUNDO;
		String mvarWDB_TIEMPO_PROCESO_AIS;
		String mvarWDB_OPERACION_XML;

		mvarWDB_IDENTIFICACION_BROKER = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_WDB_IDENTIFICACION_BROKER));
		mvarWDB_NRO_OPERACION_BROKER = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_WDB_NRO_OPERACION_BROKER));
		mvarWDB_NRO_COTIZACION_LBA = "0";

		if (!(wobjXMLRequestSolis.selectSingleNode(mcteParam_WDB_NRO_COTIZACION_LBA) == (org.w3c.dom.Node) null)) {
			if (new Variant((Object) wobjXMLRequestSolis.selectSingleNode(mcteParam_WDB_NRO_COTIZACION_LBA)).isNumeric()) {
				mvarWDB_NRO_COTIZACION_LBA = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_WDB_NRO_COTIZACION_LBA));
			}
		}

		mvarWDB_TIPO_OPERACION = "S";

		mvarWDB_RECEPCION_PEDIDOANO = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_MSGANO));
		mvarWDB_RECEPCION_PEDIDOMES = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_MSGMES));
		mvarWDB_RECEPCION_PEDIDODIA = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_MSGDIA));
		mvarWDB_RECEPCION_PEDIDOHORA = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_MSGHORA));
		mvarWDB_RECEPCION_PEDIDOMINUTO = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_MSGMINUTO));
		mvarWDB_RECEPCION_PEDIDOSEGUNDO = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_MSGSEGUNDO));
		mvarWDB_TIEMPO_PROCESO_AIS = "0";
		if (!(wobjXMLRequestSolis.selectSingleNode(mcteParm_TIEMPO) == (org.w3c.dom.Node) null)) {
			mvarWDB_TIEMPO_PROCESO_AIS = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParm_TIEMPO));
		}

		mvarWDB_OPERACION_XML = pvarRequest + loggedResponse;
		mvarWDB_OPERACION_XML = StringUtils.defaultString(StringUtils.left(mvarWDB_OPERACION_XML, 8000));
		logger.log(Level.FINEST, "Operación a loggear: " + mvarWDB_OPERACION_XML);
		
		com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();

		java.sql.Connection jdbcConn = null;
		try {
			jdbcConn = connFactory.getJDBCConnection(General.mcteDB);
			String sp = "{call " + mcteStoreProcAltaOperacion + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			CallableStatement ps = jdbcConn.prepareCall(sp);
			int posiSP = 1;

			// wvarStep = 110;
			// wobjDBParm = new Parameter("@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarWDB_IDENTIFICACION_BROKER));
			if (mvarWDB_IDENTIFICACION_BROKER.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarWDB_IDENTIFICACION_BROKER));
			}

			// wvarStep = 120;
			// wobjDBParm = new Parameter("@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarWDB_NRO_OPERACION_BROKER));
			double operaBroker = Double.parseDouble(mvarWDB_NRO_OPERACION_BROKER.replace(",", "."));
			ps.setDouble(posiSP++, operaBroker);

			// wvarStep = 130;
			// wobjDBParm = new Parameter("@WDB_NRO_COTIZACION_LBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarWDB_NRO_COTIZACION_LBA));
			if (mvarWDB_NRO_COTIZACION_LBA.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarWDB_NRO_COTIZACION_LBA));
			}

			// wvarStep = 140;
			// wobjDBParm = new Parameter("@WDB_TIPO_OPERACION", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarWDB_TIPO_OPERACION));
			ps.setString(posiSP++, String.valueOf(mvarWDB_TIPO_OPERACION));

			// wvarStep = 150;
			if ((Strings.len(mvarWDB_ESTADO) >= 3) || (Strings.left(mvarWDB_ESTADO, 1).equals("-"))) {
				mvarWDB_ESTADO = "ER";
			}

			// wobjDBParm = new Parameter("@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant(mvarWDB_ESTADO));
			ps.setString(posiSP++, String.valueOf(mvarWDB_ESTADO));

			// wvarStep = 160;
			// wobjDBParm = new Parameter("@WDB_RECEPCION_PEDIDO", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant(mvarWDB_RECEPCION_PEDIDOANO + "-" + mvarWDB_RECEPCION_PEDIDOMES + "-" + mvarWDB_RECEPCION_PEDIDODIA + " " + mvarWDB_RECEPCION_PEDIDOHORA + ":" + mvarWDB_RECEPCION_PEDIDOMINUTO + ":" + mvarWDB_RECEPCION_PEDIDOSEGUNDO));

			ps.setString(posiSP++, new String(mvarWDB_RECEPCION_PEDIDOANO + "-" + mvarWDB_RECEPCION_PEDIDOMES + "-" + mvarWDB_RECEPCION_PEDIDODIA + " " + mvarWDB_RECEPCION_PEDIDOHORA + ":" + mvarWDB_RECEPCION_PEDIDOMINUTO + ":" + mvarWDB_RECEPCION_PEDIDOSEGUNDO));

			// wvarStep = 170;
			// wobjDBParm = new Parameter("@WDB_ENVIO_RESPUESTA", AdoConst.adDate, AdoConst.adParamInput, 8, new Variant(DateTime.year(DateTime.now()) + "-" + DateTime.month(DateTime.now()) + "-" + DateTime.day(DateTime.now()) + " " + DateTime.hour(DateTime.now()) + ":" + DateTime.minute(DateTime.now()) + ":" + DateTime.second(DateTime.now())));

			ps.setString(posiSP++, new String(DateTime.year(DateTime.now()) + "-" + DateTime.month(DateTime.now()) + "-" + DateTime.day(DateTime.now()) + " " + DateTime.hour(DateTime.now()) + ":" + DateTime.minute(DateTime.now()) + ":" + DateTime.second(DateTime.now())));

			// wvarStep = 180;
			// wobjDBParm = new Parameter("@WDB_RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant("AUS1"));
			ps.setString(posiSP++, "AUS1");

			// wvarStep = 190;
			// wobjDBParm = new Parameter("@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 200;
			// wobjDBParm = new Parameter("@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(1));
			ps.setInt(posiSP++, 1);

			// wvarStep = 201;
			// Solo para el HSBC
			mvarWDB_CERTIANN = "0";
			if (mvarEmpresaCodigo.equals("150")) {
				mvarWDB_IDENTIFICACION_BROKER = "150";
				mvarWDB_CERTIANN = mvarSucursalCodigo;
			}

			// wvarStep = 210;
			// wobjDBParm = new Parameter("@WDB_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(Obj.toInt(mvarWDB_IDENTIFICACION_BROKER)));
			if (mvarWDB_IDENTIFICACION_BROKER.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarWDB_IDENTIFICACION_BROKER));
			}

			// wvarStep = 220;
			// wobjDBParm = new Parameter("@WDB_CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(Obj.toInt(mvarWDB_CERTIANN)));
			if (mvarWDB_CERTIANN.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarWDB_CERTIANN));
			}

			// wvarStep = 230;
			// wobjDBParm = new Parameter("@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, mvarCertiSec);
			if (mvarCertiSec.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarCertiSec.toString()));
			}

			// wvarStep = 240;
			// wobjDBParm = new Parameter("@WDB_TIEMPO_PROCESO_AIS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarWDB_TIEMPO_PROCESO_AIS));
			if (mvarWDB_TIEMPO_PROCESO_AIS.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarWDB_TIEMPO_PROCESO_AIS));
			}

			// wvarStep = 250;
			// wobjDBParm = new Parameter("@WDB_OPERACION_XML", AdoConst.adVarChar, AdoConst.adParamInput, 8000, (mvarWDB_OPERACION_XML.equals("") ? new Variant((Object) null) : new Variant(Strings.left(mvarWDB_OPERACION_XML, 8000))));
			ps.setString(posiSP++, mvarWDB_OPERACION_XML);

			boolean result = ps.execute();

			if (result) {
				// El primero es un ResultSet
			} else {
				// Salteo el "updateCount"
				int uc = ps.getUpdateCount();
				if (uc == -1) {
					// NOP, no hay resultados pero no es problema xq es un SP de
					// insert
				}
				result = ps.getMoreResults();
			}
		} finally {
			try {
				if (jdbcConn != null)
					jdbcConn.close();
			} catch (Exception e) {
				logger.log(Level.WARNING, "Exception al hacer un close", e);
			}
		}
	}

	private boolean fncGrabaSolicitud(String pvarXMLrequest, StringHolder pvarXMLresponse, Variant pvarCertiSec) throws XmlDomExtendedException, OSBConnectorException, MalformedURLException, SQLException, TransformerException {
		boolean fncGrabaSolicitud = false;
		XmlDomExtended wobjXMLRequest = null;
		XmlDomExtended wobjXMLResponse = null;
		org.w3c.dom.NodeList wobjXMLList = null;
		XmlDomExtended mobjXMLDoc = null;
		String mvarRequest = "";
		String mvarResponse = "";
		int wvarCount = 0;
		String mvarWDB_IDENTIFICACION_BROKER = "";
		String mvarPRODUCTOR = "";
		String mvarNUMEDOCU = "";
		String mvarTipoDocu = "";
		String mvarClienApe1 = "";
		String mvarClienApe2 = "";
		String mvarClienNom1 = "";
		String mvarNacimAnn = "";
		String mvarNacimMes = "";
		String mvarNacimDia = "";
		String mvarCLIENSEX = "";
		String mvarCLIENEST = "";
		String mvarNUMHIJOS = "";
		String mvarEMAIL = "";
		String mvarDOMICDOM = "";
		String mvarDomicDnu = "";
		String mvarDOMICPIS = "";
		String mvarDomicPta = "";
		String mvarDOMICPOB = "";
		String mvarDOMICCPO = "";
		String mvarPROVICOD = "";
		String mvarTELCOD = "";
		String mvarTELNRO = "";
		String mvarPRINCIPAL = "";
		String mvarDOMICDOM_CO = "";
		String mvarDOMICDNU_CO = "";
		String mvarDOMICPIS_CO = "";
		String mvarDOMICPTA_CO = "";
		String mvarDOMICPOB_CO = "";
		String mvarDOMICCPO_CO = "";
		String mvarPROVICOD_CO = "";
		String mvarTELCOD_CO = "";
		String mvarTELNRO_CO = "";
		String mvarPRINCIPAL_CO = "";
		String mvarCOBROTIP = "";
		String mvarCUENNUME = "";
		String mvarVENCIANN = "";
		String mvarVENCIMES = "";
		String mvarVENCIDIA = "";
		String mvarBANCOCOD = "";
		String mvarSUCURCOD = "";
		String mvarFRANQCOD = "";
		String mvarPLANCOD = "";
		String mvarZONA = "";
		String mvarCLIENIVA = "";
		String mvarSUMAASEG = "";
		String mvarCLUB_LBA = "";
		String mvarDESTRUCCION_80 = "";
		String mvarLUNETA = "";
		String mvarCLUBECO = "";
		String mvarROBOCONT = "";
		String mvarGRANIZO = "";
		String mvarIBB = "";
		String mvarNROIBB = "";
		String mvarINSTALADP = "";
		String mvarPOSEEDISP = "";
		String mvarCUITNUME = "";
		String mvarRAZONSOC = "";
		String mvarCLIENTIP = "";
		String mvarCPAANO = "";
		String mvarCTAKMS = "";
		String mvarESCERO = "";
		String mvarSIFMVEHI_DES = "";
		String mvarMOTORNUM = "";
		String mvarCHASINUM = "";
		String mvarPATENNUM = "";
		String mvarMODEAUTCOD = "";
		String mvarVEHCLRCOD = "";
		String mvarEFECTANN = "";
		String mvarGUGARAGE = "";
		String mvarGUDOMICI = "";
		String mvarAUCATCOD = "";
		String mvarAUTIPCOD = "";
		String mvarAUANTANN = "";
		String mvarAUNUMSIN = "";
		String mvarAUUSOGNC = "";
		String mvarCAMPACOD = "";
		String mvarCONDUAPE = "";
		String mvarCONDUNOM = "";
		String mvarCONDUFEC = "";
		String mvarCONDUSEX = "";
		String mvarCONDUEST = "";
		String mvarCONDUEXC = "";
		String mvarACCESORIOS = "";
		String mvarAUACCCOD = "";
		String mvarAUVEASUM = "";
		String mvarAUVEADES = "";
		String mvarAUVEADEP = "";
		String mvarINSPECOD = "";
		String mvarRastreo = "";
		String mvarALARMCOD = "";
		String mvarCOBROCOD = "";
		String mvarANOTACIO = "";
		String mvarVIGENANN = "";
		String mvarVIGENMES = "";
		String mvarVIGENDIA = "";
		String mvarSUMALBA = "";
		String mvarCOBERCOD = "";
		String mvarCOBERORD = "";
		String mvarCAPITASG = "";
		String mvarCAPITIMP = "";
		String mvarPRECIO_MENSUAL = "";
		String mvarEmpresaCodigo = "";
		String mvarSucursalCodigo = "";
		String mvarLegajoVend = "";
		String mvarLegajoCia = "";
		String mvarNROSOLI = "";
		String mvarNUMEDOCUACRE = "";
		String mvarTIPODOCUACRE = "";
		String mvarAPELLIDOACRE = "";
		String mvarNOMBRESACRE = "";
		String mvarDOMICDOMACRE = "";
		String mvarDOMICDNUACRE = "";
		String mvarDOMICPISACRE = "";
		String mvarDOMICPTAACRE = "";
		String mvarDOMICPOBACRE = "";
		String mvarDOMICCPOACRE = "";
		String mvarPROVICODACRE = "";
		String mvarPAISSCODACRE = "";
		String mvarWDB_CERTIANN = "";
		String mvarDOCUMASEG = "";
		String mvarNOMBREASEG = "";
		String mvarNROFACTU = "";
		String mvarCENSICOD = "";

		fncGrabaSolicitud = true;
		wobjXMLRequest = new XmlDomExtended();
		wobjXMLRequest.loadXML(pvarXMLrequest);

		wobjXMLResponse = new XmlDomExtended();
		wobjXMLResponse.loadXML(pvarXMLresponse.getValue());

		mvarWDB_IDENTIFICACION_BROKER = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_WDB_IDENTIFICACION_BROKER));
		mvarSUMALBA = "0";
		if (!(wobjXMLRequest.selectSingleNode("//SUMALBA") == (org.w3c.dom.Node) null)) {
			mvarSUMALBA = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//SUMALBA"));
		}
		mvarPRODUCTOR = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PRODUCTOR));

		// AM HSBC
		mvarEmpresaCodigo = "0";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_EmpresaCodigo) == (org.w3c.dom.Node) null)) {
			mvarEmpresaCodigo = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_EmpresaCodigo));
		}
		//
		mvarSucursalCodigo = "0";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_SucursalCodigo) == (org.w3c.dom.Node) null)) {
			mvarSucursalCodigo = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_SucursalCodigo));
		}
		//
		mvarLegajoVend = "0";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_LegajoVend) == (org.w3c.dom.Node) null)) {
			mvarLegajoVend = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_LegajoVend));
		}
		//
		mvarLegajoCia = "0";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_LegajoCia) == (org.w3c.dom.Node) null)) {
			mvarLegajoCia = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_LegajoCia));
		}
		//
		mvarNROSOLI = "0";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_NROSOLI) == (org.w3c.dom.Node) null)) {
			mvarNROSOLI = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_NROSOLI));
		}
		// Fin AM HSBC
		mvarNUMEDOCU = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_NumeDocu));
		mvarTipoDocu = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_TipoDocu));
		mvarClienApe1 = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ClienApe1)));
		mvarClienApe2 = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_ClienApe2) == (org.w3c.dom.Node) null)) {
			mvarClienApe2 = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ClienApe2)));
		}
		mvarClienNom1 = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ClienNom1)));
		mvarNacimAnn = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_NacimAnn));
		mvarNacimMes = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_NacimMes));
		mvarNacimDia = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_NacimDia));
		mvarCLIENSEX = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CLIENSEX)));
		mvarCLIENEST = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CLIENEST)));
		mvarNUMHIJOS = "0";
		if (!(wobjXMLRequest.selectNodes(mcteParam_CONDUCTORES) == (org.w3c.dom.NodeList) null)) {
			mvarNUMHIJOS = String.valueOf(wobjXMLRequest.selectNodes(mcteParam_CONDUCTORES).getLength());
		}
		mvarEMAIL = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_EMAIL) == (org.w3c.dom.Node) null)) {
			mvarEMAIL = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_EMAIL));
		}
		// Domicilio de riesgo
		mvarDOMICDOM = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICDOM)));
		mvarDomicDnu = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DomicDnu));
		mvarDOMICPIS = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPIS) == (org.w3c.dom.Node) null)) {
			mvarDOMICPIS = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPIS)));
		}
		mvarDomicPta = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_DomicPta) == (org.w3c.dom.Node) null)) {
			mvarDomicPta = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DomicPta)));
		}
		mvarDOMICPOB = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPOB)));
		mvarDOMICCPO = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICCPO));
		mvarPROVICOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PROVICOD));
		// jc 09/2010 se agrega area
		mvarTELCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_TELCOD));
		mvarTELNRO = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_TELNRO));
		mvarPRINCIPAL = "";
		// datos del domicilio de correspondencia
		mvarDOMICDOM_CO = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICDOM_CO)));
		mvarDOMICDNU_CO = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DomicDnu_CO));
		mvarDOMICPIS_CO = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPIS_CO) == (org.w3c.dom.Node) null)) {
			mvarDOMICPIS_CO = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPIS_CO)));
		}
		mvarDOMICPTA_CO = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_DomicPta_CO) == (org.w3c.dom.Node) null)) {
			mvarDOMICPTA_CO = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DomicPta_CO)));
		}
		mvarDOMICPOB_CO = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPOB_CO)));
		mvarDOMICCPO_CO = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICCPO_CO));
		mvarPROVICOD_CO = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_PROVICOD_CO) == (org.w3c.dom.Node) null)) {
			mvarPROVICOD_CO = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PROVICOD_CO));
		}
		// jc 09/2010 se agrega area
		mvarTELCOD_CO = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_TELCOD_CO) == (org.w3c.dom.Node) null)) {
			mvarTELCOD_CO = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_TELCOD_CO));
		}
		mvarTELNRO_CO = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_TELNRO_CO) == (org.w3c.dom.Node) null)) {
			mvarTELNRO_CO = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_TELNRO_CO));
		}
		mvarPRINCIPAL_CO = "";
		// datos de la cuenta
		mvarCOBROTIP = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_COBROTIP)));

		mvarCUENNUME = "0";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_CUENNUME) == (org.w3c.dom.Node) null)) {
			mvarCUENNUME = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CUENNUME));
		}

		mvarVENCIANN = "0";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_VENCIANN) == (org.w3c.dom.Node) null)) {
			mvarVENCIANN = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_VENCIANN));
		}
		mvarVENCIMES = "0";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_VENCIMES) == (org.w3c.dom.Node) null)) {
			mvarVENCIMES = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_VENCIMES));
		}
		mvarVENCIDIA = "0";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_VENCIDIA) == (org.w3c.dom.Node) null)) {
			mvarVENCIDIA = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_VENCIDIA));
		}

		// Si Pago con Tarjeta de Credito, no va ni el Banco ni la Sucursal
		mvarBANCOCOD = "0";
		if (Obj.toInt(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_COBROCOD))) == 4) {
			// jc 09/2010 en la sp forzaba los valores a 9000 se fuerza aqui
			// para ser mas claro
			mvarBANCOCOD = "0";
			mvarSUCURCOD = "0";
		} else {
			mvarBANCOCOD = "9000";
			mvarSUCURCOD = "8888";
		}

		if (!(wobjXMLRequest.selectSingleNode(mcteParam_BANCOCOD) == (org.w3c.dom.Node) null)) {
			mvarBANCOCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_BANCOCOD));
		}

		// datos de la operacion
		mvarFRANQCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_FRANQCOD));
		mvarPLANCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PLANCOD));

		mvarRequest = "<Request><RAMOPCOD>AUS1</RAMOPCOD><PROVICOD>" + mvarPROVICOD + "</PROVICOD><CPACODPO>" + mvarDOMICCPO + "</CPACODPO></Request>";
		logger.log(Level.FINE, "Llamando localmente a lbaw_GetZona");
		lbaw_GetZona getZona = new lbaw_GetZona();
		StringHolder getZonaSH = new StringHolder();
		getZona.IAction_Execute(mvarRequest, getZonaSH, "");
		mvarResponse = getZonaSH.getValue();

		mobjXMLDoc = new XmlDomExtended();

		mobjXMLDoc.loadXML(mvarResponse);
		if (XmlDomExtended.getText(mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado")).equals("true")) {
			mvarZONA = XmlDomExtended.getText(mobjXMLDoc.selectSingleNode("//CODIZONA"));
		} else {
			mvarZONA = "";
		}
		mobjXMLDoc = null;
		// fin jc
		mvarCLIENIVA = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CLIENIVA));
		mvarSUMAASEG = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_SUMAASEG));
		mvarCLUB_LBA = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CLUB_LBA)));
		// jc 09/2010 nuevas
		mvarDESTRUCCION_80 = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DESTRUCCION_80)));
		mvarLUNETA = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Luneta)));
		mvarCLUBECO = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ClubEco)));
		mvarROBOCONT = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Robocont)));
		mvarGRANIZO = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Granizo)));
		mvarIBB = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_IBB) == (org.w3c.dom.Node) null)) {
			mvarIBB = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_IBB)));
		}
		mvarNROIBB = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_NROIBB) == (org.w3c.dom.Node) null)) {
			mvarNROIBB = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_NROIBB)));
		}
		mvarINSTALADP = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_INSTALADP)));
		mvarPOSEEDISP = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_POSEEDISP)));
		// 11/2010 se agregan distintos iva
		mvarCUITNUME = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_CUITNUME) == (org.w3c.dom.Node) null)) {
			mvarCUITNUME = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CUITNUME)));
		}
		mvarRAZONSOC = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_RAZONSOC) == (org.w3c.dom.Node) null)) {
			mvarRAZONSOC = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_RAZONSOC)));
		}
		mvarCLIENTIP = "00";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_CLIENTIP) == (org.w3c.dom.Node) null)) {
			mvarCLIENTIP = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CLIENTIP)));
		}
		// jc fin
		mvarCPAANO = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CPAANO));
		mvarCTAKMS = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CTAKMS));
		mvarESCERO = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Escero)));
		mvarSIFMVEHI_DES = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_SIFMVEHI_DES)));
		mvarMOTORNUM = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_MOTORNUM));
		mvarCHASINUM = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CHASINUM));
		mvarPATENNUM = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PATENNUM));
		// AUMARCOD+AUMODCOD+AUSUBCOD+AUADICODAUMODORI
		mvarMODEAUTCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ModeAutCod));
		mvarVEHCLRCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_VEHCLRCOD));
		mvarEFECTANN = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_EfectAnn));
		mvarGUGARAGE = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_GUGARAGE)));
		mvarGUDOMICI = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_GUDOMICI)));
		mvarAUCATCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_AUCATCOD));
		mvarAUTIPCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_AUTIPCOD));
		mvarAUANTANN = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_AUANTANN));
		mvarAUNUMSIN = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_AUNUMSIN));
		mvarAUUSOGNC = Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_AUUSOGNC)));
		mvarCAMPACOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CampaCod));

		mvarACCESORIOS = "N";
		if (!(wobjXMLRequest.selectNodes(mcteParam_ACCESORIOS) == (org.w3c.dom.NodeList) null)) {
			mvarACCESORIOS = (wobjXMLRequest.selectNodes(mcteParam_ACCESORIOS).getLength() == 0 ? "N" : "S");
		}

		mvarINSPECOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_INSPECOD));
		mvarRastreo = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_RASTREO));
		mvarALARMCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ALARMCOD));
		mvarCOBROCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_COBROCOD));
		mvarANOTACIO = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_ANOTACIO) == (org.w3c.dom.Node) null)) {
			mvarANOTACIO = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ANOTACIO));
		}
		mvarVIGENANN = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_VIGENANN));
		mvarVIGENMES = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_VIGENMES));
		mvarVIGENDIA = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_VIGENDIA));

		mvarPRECIO_MENSUAL = "0";
		if (!(wobjXMLRequest.selectSingleNode("//PRECIOPLAN_SH") == (org.w3c.dom.Node) null)) {
			// Inicio parche
			String precioPlanSH = String.valueOf(Obj.toDouble(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PRECIOPLAN_SH"))));

			if (precioPlanSH.indexOf(".") != -1) {
				mvarPRECIO_MENSUAL = precioPlanSH.substring(0, precioPlanSH.indexOf("."));
			} else {
				mvarPRECIO_MENSUAL = precioPlanSH;
			}
			// Fin parche
		}
		if (!(wobjXMLRequest.selectSingleNode("//PRECIOPLAN_CH") == (org.w3c.dom.Node) null)) {
			// Inicio parche
			String precioPlanCH = String.valueOf(Obj.toDouble(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//PRECIOPLAN_CH"))));

			if (precioPlanCH.indexOf(".") != -1) {
				mvarPRECIO_MENSUAL = precioPlanCH.substring(0, precioPlanCH.indexOf("."));
			} else {
				mvarPRECIO_MENSUAL = precioPlanCH;
			}
			// Fin parche
		}

		mvarNROFACTU = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_NROFACTU) == (org.w3c.dom.Node) null)) {
			mvarNROFACTU = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_NROFACTU));
		}

		mvarCENSICOD = "";
		if (!(wobjXMLRequest.selectSingleNode(mcteParam_CENSICOD) == (org.w3c.dom.Node) null)) {
			mvarCENSICOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CENSICOD));
		}

		java.sql.Connection jdbcConn = null;

		try {

			com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
			jdbcConn = connFactory.getJDBCConnection(General.mcteDB);
			String sp = "{call "
					+ mcteStoreProc
					+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			CallableStatement ps = jdbcConn.prepareCall(sp);
			int posiSP = 1;
			// Se prepara como parámetro de input-output el CertiSEC
			ps.registerOutParameter(6, Types.INTEGER);

			// wvarStep = 116;
			// wobjDBParm = new Parameter("@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant("AUS1"));
			ps.setString("RAMOPCOD", "AUS1");
			posiSP++;
			
			// wvarStep = 117;
			// wobjDBParm = new Parameter("@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 130;
			// wobjDBParm = new Parameter("@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(1));
			ps.setInt(posiSP++, 1);

			// wvarStep = 131;
			// Solo para el HSBC
			mvarWDB_CERTIANN = "0";
			if (Obj.toInt(mvarEmpresaCodigo) == 150) {
				mvarWDB_IDENTIFICACION_BROKER = "150";
				mvarWDB_CERTIANN = mvarSucursalCodigo;
			}
			// wvarStep = 132;
			if (Obj.toInt(mvarEmpresaCodigo) > 0) {
				mvarBANCOCOD = mvarEmpresaCodigo;
			}

			// wvarStep = 140;
			// wobjDBParm = new Parameter("@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(Obj.toInt(mvarWDB_IDENTIFICACION_BROKER)));
			if (mvarWDB_IDENTIFICACION_BROKER.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarWDB_IDENTIFICACION_BROKER));
			}

			// wvarStep = 150;
			// wobjDBParm = new Parameter("@CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(Obj.toInt(mvarWDB_CERTIANN)));
			if (mvarWDB_CERTIANN.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarWDB_CERTIANN));
			}

			// wvarStep = 160;
			// wobjDBParm = new Parameter("@CERTISEC", AdoConst.adNumeric, AdoConst.adParamInputOutput, 0, pvarCertiSec);
			if (pvarCertiSec.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(pvarCertiSec.toString()));
			}

			// wvarStep = 170;
			// wobjDBParm = new Parameter("@SUPLENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 180;
			// wobjDBParm = new Parameter("@USUARCOD", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant(mvarWDB_IDENTIFICACION_BROKER));
			ps.setString(posiSP++, String.valueOf(mvarWDB_IDENTIFICACION_BROKER));

			// wvarStep = 190;
			// wobjDBParm = new Parameter("@NUMEDOCU", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant(mvarNUMEDOCU));
			ps.setString(posiSP++, String.valueOf(mvarNUMEDOCU));

			// wvarStep = 200;
			// wobjDBParm = new Parameter("@TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarTipoDocu));
			if (mvarTipoDocu.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarTipoDocu));
			}

			// wvarStep = 210;
			// wobjDBParm = new Parameter("@CLIENAP1", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant(mvarClienApe1));
			ps.setString(posiSP++, String.valueOf(mvarClienApe1));

			// wvarStep = 220;
			// wobjDBParm = new Parameter("@CLIENAP2", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant(mvarClienApe2));
			ps.setString(posiSP++, String.valueOf(mvarClienApe2));

			// wvarStep = 230;
			// wobjDBParm = new Parameter("@CLIENNOM", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant(mvarClienNom1));
			ps.setString(posiSP++, String.valueOf(mvarClienNom1));

			// wvarStep = 240;
			// wobjDBParm = new Parameter("@NACIMANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarNacimAnn));
			if (mvarNacimAnn.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarNacimAnn));
			}

			// wvarStep = 250;
			// wobjDBParm = new Parameter("@NACIMMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarNacimMes));
			if (mvarNacimMes.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarNacimMes));
			}

			// wvarStep = 260;
			// wobjDBParm = new Parameter("@NACIMDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarNacimDia));
			if (mvarNacimDia.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarNacimDia));
			}

			// wvarStep = 270;
			// wobjDBParm = new Parameter("@CLIENSEX", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarCLIENSEX));
			ps.setString(posiSP++, String.valueOf(mvarCLIENSEX));

			// wvarStep = 280;
			// wobjDBParm = new Parameter("@CLIENEST", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarCLIENEST));
			ps.setString(posiSP++, String.valueOf(mvarCLIENEST));

			// wvarStep = 290;
			// wobjDBParm = new Parameter("@PAISSCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant("00"));
			ps.setString(posiSP++, "00");

			// wvarStep = 300;
			// wobjDBParm = new Parameter("@IDIOMCOD", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant(" "));
			ps.setString(posiSP++, " ");

			// wvarStep = 310;
			// wobjDBParm = new Parameter("@NUMHIJOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarNUMHIJOS));
			if (mvarNUMHIJOS.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarNUMHIJOS));
			}

			// wvarStep = 320;
			// wobjDBParm = new Parameter("@EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(DateTime.year(DateTime.now())));
			ps.setInt(posiSP++, Integer.parseInt(new Variant(DateTime.year(DateTime.now())).toString()));

			// wvarStep = 330;
			// wobjDBParm = new Parameter("@EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(DateTime.month(DateTime.now())));
			ps.setInt(posiSP++, Integer.parseInt(new Variant(DateTime.month(DateTime.now())).toString()));

			// wvarStep = 340;
			// wobjDBParm = new Parameter("@EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(DateTime.day(DateTime.now())));
			ps.setInt(posiSP++, Integer.parseInt(new Variant(DateTime.day(DateTime.now())).toString()));

			// wvarStep = 350;
			// wobjDBParm = new Parameter("@CLIENTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant(mvarCLIENTIP));
			ps.setString(posiSP++, String.valueOf(mvarCLIENTIP));

			// wvarStep = 360;
			// wobjDBParm = new Parameter("@CLIENFUM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(" "));
			ps.setString(posiSP++, " ");

			// wvarStep = 370;
			// wobjDBParm = new Parameter("@CLIENDOZ", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(" "));
			ps.setString(posiSP++, " ");

			// wvarStep = 380;
			// wobjDBParm = new Parameter("@ABRIDTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant(" "));
			ps.setString(posiSP++, " ");

			// wvarStep = 390;
			// wobjDBParm = new Parameter("@ABRIDNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant(" "));
			ps.setString(posiSP++, " ");

			// wvarStep = 400;
			// wobjDBParm = new Parameter("@CLIENORG", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant(" "));
			ps.setString(posiSP++, " ");

			// wvarStep = 410;
			if (mvarCLIENTIP.equals("00")) {
				// wobjDBParm = new Parameter("@PERSOTIP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant("F"));
				ps.setString(posiSP++, "F");
			} else {
				// wobjDBParm = new Parameter("@PERSOTIP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant("J"));
				ps.setString(posiSP++, "J");
			}

			// wvarStep = 420;
			// wobjDBParm = new Parameter("@DOMICSEC", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant("0"));
			ps.setString(posiSP++, "0");

			// wvarStep = 430;
			// wobjDBParm = new Parameter("@CLIENCLA", AdoConst.adChar, AdoConst.adParamInput, 9, new Variant(" "));
			ps.setString(posiSP++, " ");

			// wvarStep = 440;
			// wobjDBParm = new Parameter("@FALLEANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 450;
			// wobjDBParm = new Parameter("@FALLEMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 460;
			// wobjDBParm = new Parameter("@FALLEDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 470;
			// wobjDBParm = new Parameter("@FBAJAANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 480;
			// wobjDBParm = new Parameter("@FBAJAMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 490;
			// wobjDBParm = new Parameter("@FBAJADIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 500;
			// wobjDBParm = new Parameter("@EMAIL", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant(mvarEMAIL));
			ps.setString(posiSP++, String.valueOf(mvarEMAIL));

			// wvarStep = 510;
			// wobjDBParm = new Parameter("@DOMICCAL", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant(" "));
			ps.setString(posiSP++, " ");

			// wvarStep = 520;
			// wobjDBParm = new Parameter("@DOMICDOM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant(mvarDOMICDOM));
			ps.setString(posiSP++, String.valueOf(mvarDOMICDOM));

			// wvarStep = 530;
			// wobjDBParm = new Parameter("@DOMICDNU", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant(mvarDomicDnu));
			ps.setString(posiSP++, String.valueOf(mvarDomicDnu));

			// wvarStep = 540;
			// wobjDBParm = new Parameter("@DOMICESC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(" "));
			ps.setString(posiSP++, " ");

			// wvarStep = 550;
			// wobjDBParm = new Parameter("@DOMICPIS", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant(mvarDOMICPIS));
			ps.setString(posiSP++, String.valueOf(mvarDOMICPIS));

			// wvarStep = 560;
			// wobjDBParm = new Parameter("@DOMICPTA", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant(mvarDomicPta));
			ps.setString(posiSP++, String.valueOf(mvarDomicPta));

			// wvarStep = 570;
			// wobjDBParm = new Parameter("@DOMICPOB", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant(mvarDOMICPOB));
			ps.setString(posiSP++, String.valueOf(mvarDOMICPOB));

			// wvarStep = 580;
			// wobjDBParm = new Parameter("@DOMICCPO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarDOMICCPO));
			if (mvarDOMICCPO.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarDOMICCPO));
			}

			// wvarStep = 590;
			// wobjDBParm = new Parameter("@PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarPROVICOD));
			if (mvarPROVICOD.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarPROVICOD));
			}

			// wvarStep = 600;
			// wobjDBParm = new Parameter("@TELCOD", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant(mvarTELCOD));
			ps.setString(posiSP++, String.valueOf(mvarTELCOD));

			// wvarStep = 610;
			// wobjDBParm = new Parameter("@TELNRO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant(mvarTELNRO));
			ps.setString(posiSP++, String.valueOf(mvarTELNRO));

			// wvarStep = 620;
			// wobjDBParm = new Parameter("@PRINCIPAL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarPRINCIPAL));
			ps.setString(posiSP++, String.valueOf(mvarPRINCIPAL));

			// wvarStep = 630;
			// wobjDBParm = new Parameter("@DOMICCAL_CO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 640;
			// wobjDBParm = new Parameter("@DOMICDOM_CO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant(mvarDOMICDOM_CO));
			ps.setString(posiSP++, String.valueOf(mvarDOMICDOM_CO));

			// wvarStep = 650;
			// wobjDBParm = new Parameter("@DOMICDNU_CO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant(mvarDOMICDNU_CO));
			ps.setString(posiSP++, String.valueOf(mvarDOMICDNU_CO));

			// wvarStep = 660;
			// wobjDBParm = new Parameter("@DOMICESC_CO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 670;
			// wobjDBParm = new Parameter("@DOMICPIS_CO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant(mvarDOMICPIS_CO));
			ps.setString(posiSP++, String.valueOf(mvarDOMICPIS_CO));

			// wvarStep = 680;
			// wobjDBParm = new Parameter("@DOMICPTA_CO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant(mvarDOMICPTA_CO));
			ps.setString(posiSP++, String.valueOf(mvarDOMICPTA_CO));

			// wvarStep = 690;
			// wobjDBParm = new Parameter("@DOMICPOB_CO", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant(mvarDOMICPOB_CO));
			ps.setString(posiSP++, String.valueOf(mvarDOMICPOB_CO));

			// wvarStep = 700;
			// wobjDBParm = new Parameter("@DOMICCPO_CO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarDOMICCPO_CO));
			if (mvarDOMICCPO_CO.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarDOMICCPO_CO));
			}

			// wvarStep = 710;
			// wobjDBParm = new Parameter("@PROVICOD_CO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarPROVICOD_CO));
			if (mvarPROVICOD_CO.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarPROVICOD_CO));
			}

			// wvarStep = 720;
			// wobjDBParm = new Parameter("@TELCOD_CO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant(mvarTELCOD_CO));
			ps.setString(posiSP++, String.valueOf(mvarTELCOD_CO));

			// wvarStep = 730;
			// wobjDBParm = new Parameter("@TELNRO_CO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant(mvarTELNRO_CO));
			ps.setString(posiSP++, String.valueOf(mvarTELNRO_CO));

			// wvarStep = 740;
			// wobjDBParm = new Parameter("@PRINCIPAL_CO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarPRINCIPAL_CO));
			ps.setString(posiSP++, String.valueOf(mvarPRINCIPAL_CO));

			// wvarStep = 750;
			// wobjDBParm = new Parameter("@TIPOCUEN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(" "));
			ps.setString(posiSP++, " ");

			// wvarStep = 760;
			// wobjDBParm = new Parameter("@COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant(mvarCOBROTIP));
			ps.setString(posiSP++, String.valueOf(mvarCOBROTIP));

			// wvarStep = 770;
			// wobjDBParm = new Parameter("@BANCOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarBANCOCOD));
			if (mvarBANCOCOD.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarBANCOCOD));
			}

			// wvarStep = 780;

			// wobjDBParm = new Parameter("@SUCURCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(Math.floor(Obj.toDouble(mvarSUCURCOD))));
			if (mvarSUCURCOD.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setDouble(posiSP++, Math.floor(Obj.toDouble(mvarSUCURCOD)));
			}

			// wvarStep = 790;
			// wobjDBParm = new Parameter("@CUENTDC", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 800;
			// wobjDBParm = new Parameter("@CUENNUME", AdoConst.adChar, AdoConst.adParamInput, 16, new Variant(mvarCUENNUME));
			ps.setString(posiSP++, String.valueOf(mvarCUENNUME));

			// wvarStep = 810;
			// wobjDBParm = new Parameter("@TARJECOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 820;
			// wobjDBParm = new Parameter("@VENCIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarVENCIANN.equals("") ? new Variant((Object) new Integer(0)) : new Variant(mvarVENCIANN)));
			if (mvarVENCIANN.isEmpty()) {
				ps.setInt(posiSP++, 0);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarVENCIANN));
			}

			// wvarStep = 830;
			// wobjDBParm = new Parameter("@VENCIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarVENCIMES.equals("") ? new Variant((Object) new Integer(0)) : new Variant(mvarVENCIMES)));
			if (mvarVENCIMES.isEmpty()) {
				ps.setInt(posiSP++, 0);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarVENCIMES));
			}

			// wvarStep = 840;
			// wobjDBParm = new Parameter("@VENCIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarVENCIDIA.equals("") ? new Variant((Object) new Integer(0)) : new Variant(mvarVENCIDIA)));
			if (mvarVENCIDIA.isEmpty()) {
				ps.setInt(posiSP++, 0);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarVENCIDIA));
			}

			// wvarStep = 850;
			// wobjDBParm = new Parameter("@FRANQCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant(mvarFRANQCOD));
			ps.setString(posiSP++, String.valueOf(mvarFRANQCOD));

			// wvarStep = 860;
			// wobjDBParm = new Parameter("@PQTDES", AdoConst.adChar, AdoConst.adParamInput, 120, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 870;
			// wobjDBParm = new Parameter("@PLANCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarPLANCOD));
			if (mvarPLANCOD.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarPLANCOD));
			}

			// wvarStep = 880;
			// wobjDBParm = new Parameter("@HIJOS1416", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 890;
			// wobjDBParm = new Parameter("@HIJOS1729", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarNUMHIJOS));
			if (mvarNUMHIJOS.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarNUMHIJOS));
			}

			// wvarStep = 900;
			// wobjDBParm = new Parameter("@ZONA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarZONA));
			if (mvarZONA.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarZONA));
			}

			// wvarStep = 910;
			// wobjDBParm = new Parameter("@CODPROV", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarPROVICOD));
			if (mvarPROVICOD.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarPROVICOD));
			}

			// wvarStep = 920;
			// wobjDBParm = new Parameter("@SUMALBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarSUMALBA));
			if (mvarSUMALBA.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarSUMALBA));
			}

			// wvarStep = 930;
			// wobjDBParm = new Parameter("@CLIENIVA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarCLIENIVA));
			ps.setString(posiSP++, String.valueOf(mvarCLIENIVA));

			// wvarStep = 940;
			// wobjDBParm = new Parameter("@SUMAASEG", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarSUMAASEG));
			String sumaAsegConver = null;

			if (mvarSUMAASEG.indexOf(".") != -1) {
				sumaAsegConver = mvarSUMAASEG.substring(0, mvarSUMAASEG.indexOf("."));
			} else {
				sumaAsegConver = mvarSUMAASEG;
			}
			if (sumaAsegConver.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(sumaAsegConver));
			}

			// wvarStep = 950;
			// wobjDBParm = new Parameter("@CLUB_LBA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarCLUB_LBA));
			ps.setString(posiSP++, String.valueOf(mvarCLUB_LBA));

			// wvarStep = 960;
			// wobjDBParm = new Parameter("@CPAANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarCPAANO));
			if (mvarCPAANO.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarCPAANO));
			}

			// wvarStep = 970;
			// wobjDBParm = new Parameter("@CTAKMS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarCTAKMS));
			if (mvarCTAKMS.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarCTAKMS));
			}

			// wvarStep = 980;
			// wobjDBParm = new Parameter("@ESCERO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarESCERO));
			ps.setString(posiSP++, String.valueOf(mvarESCERO));

			// wvarStep = 990;
			// wobjDBParm = new Parameter("@TIENEPLAN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant("N"));
			ps.setString(posiSP++, "N");

			// wvarStep = 1000;
			// wobjDBParm = new Parameter("@COND_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant("N"));
			ps.setString(posiSP++, "N");

			// wvarStep = 1010;
			// wobjDBParm = new Parameter("@ASEG_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant("N"));
			ps.setString(posiSP++, "N");

			// wvarStep = 1020;
			// wobjDBParm = new Parameter("@FH_NAC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(Obj.toInt(mvarNacimAnn + mvarNacimMes + mvarNacimDia)));
			if ((mvarNacimAnn + mvarNacimMes + mvarNacimDia).isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarNacimAnn + mvarNacimMes + mvarNacimDia));
			}

			// wvarStep = 1030;
			// wobjDBParm = new Parameter("@SEXO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarCLIENSEX));
			ps.setString(posiSP++, String.valueOf(mvarCLIENSEX));

			// wvarStep = 1040;
			// wobjDBParm = new Parameter("@ESTCIV", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarCLIENEST));
			ps.setString(posiSP++, String.valueOf(mvarCLIENEST));

			// wvarStep = 1050;
			// wobjDBParm = new Parameter("@SIFMVEHI_DES", AdoConst.adChar, AdoConst.adParamInput, 80, new Variant(mvarSIFMVEHI_DES));
			ps.setString(posiSP++, String.valueOf(mvarSIFMVEHI_DES));

			// wvarStep = 1060;
			// wobjDBParm = new Parameter("@PROFECOD", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant("000000"));
			ps.setString(posiSP++, "000000");

			// wvarStep = 1070;
			// wobjDBParm = new Parameter("@ACCESORIOS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarACCESORIOS));
			ps.setString(posiSP++, String.valueOf(mvarACCESORIOS));

			// wvarStep = 1080;
			// wobjDBParm = new Parameter("@REFERIDO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 1090;
			// wobjDBParm = new Parameter("@MOTORNUM", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant(mvarMOTORNUM));
			ps.setString(posiSP++, String.valueOf(mvarMOTORNUM));

			// wvarStep = 1100;
			// wobjDBParm = new Parameter("@CHASINUM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant(mvarCHASINUM));
			ps.setString(posiSP++, String.valueOf(mvarCHASINUM));

			// wvarStep = 1110;
			// wobjDBParm = new Parameter("@PATENNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant(mvarPATENNUM));
			ps.setString(posiSP++, String.valueOf(mvarPATENNUM));

			// wvarStep = 1120;
			// wobjDBParm = new Parameter("@AUMARCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(Strings.mid(mvarMODEAUTCOD, 1, 5)));
			if (Strings.mid(mvarMODEAUTCOD, 1, 5).isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(Strings.mid(mvarMODEAUTCOD, 1, 5)));
			}

			// wvarStep = 1130;
			// wobjDBParm = new Parameter("@AUMODCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(Strings.mid(mvarMODEAUTCOD, 6, 5)));
			if (Strings.mid(mvarMODEAUTCOD, 6, 5).isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(Strings.mid(mvarMODEAUTCOD, 6, 5)));
			}

			// wvarStep = 1140;
			// wobjDBParm = new Parameter("@AUSUBCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(Strings.mid(mvarMODEAUTCOD, 11, 5)));
			if (Strings.mid(mvarMODEAUTCOD, 11, 5).isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(Strings.mid(mvarMODEAUTCOD, 11, 5)));
			}

			// wvarStep = 1150;
			// wobjDBParm = new Parameter("@AUADICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(Strings.mid(mvarMODEAUTCOD, 16, 5)));
			if (Strings.mid(mvarMODEAUTCOD, 16, 5).isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(Strings.mid(mvarMODEAUTCOD, 16, 5)));
			}

			// wvarStep = 1160;
			// wobjDBParm = new Parameter("@AUMODORI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(Strings.mid(mvarMODEAUTCOD, 21, 1)));
			ps.setString(posiSP++, String.valueOf(Strings.mid(mvarMODEAUTCOD, 21, 1)));

			// wvarStep = 1170;
			// wobjDBParm = new Parameter("@AUUSOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(1));
			ps.setInt(posiSP++, 1);

			// wvarStep = 1180;
			// wobjDBParm = new Parameter("@AUVTVCOD", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant("N"));
			ps.setString(posiSP++, "N");

			// wvarStep = 1190;
			// wobjDBParm = new Parameter("@AUVTVDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 1200;
			// wobjDBParm = new Parameter("@AUVTVMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 1205;
			// wobjDBParm = new Parameter("@AUVTVANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 1210;
			// wobjDBParm = new Parameter("@VEHCLRCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarVEHCLRCOD));
			if (mvarVEHCLRCOD.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarVEHCLRCOD));
			}

			// wvarStep = 1215;
			// wobjDBParm = new Parameter("@AUKLMNUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarCTAKMS));
			if (mvarCTAKMS.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarCTAKMS));
			}

			// wvarStep = 1220;
			// wobjDBParm = new Parameter("@FABRICAN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarEFECTANN));
			if (mvarEFECTANN.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarEFECTANN));
			}

			// wvarStep = 1225;
			// wobjDBParm = new Parameter("@FABRICMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(1));
			ps.setInt(posiSP++, 1);

			// wvarStep = 1230;
			// wobjDBParm = new Parameter("@GUGARAGE", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarGUGARAGE));
			ps.setString(posiSP++, String.valueOf(mvarGUGARAGE));

			// wvarStep = 1235;
			// wobjDBParm = new Parameter("@GUDOMICI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarGUDOMICI));
			ps.setString(posiSP++, String.valueOf(mvarGUDOMICI));

			// wvarStep = 1240;
			// wobjDBParm = new Parameter("@AUCATCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarAUCATCOD));
			if (mvarAUCATCOD.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarAUCATCOD));
			}

			// wvarStep = 1245;
			// wobjDBParm = new Parameter("@AUTIPCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarAUTIPCOD));
			if (mvarAUTIPCOD.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarAUTIPCOD));
			}

			// wvarStep = 1250;
			// wobjDBParm = new Parameter("@AUCIASAN", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant(mvarAUTIPCOD));
			ps.setString(posiSP++, String.valueOf(mvarAUTIPCOD));

			// wvarStep = 1255;
			// wobjDBParm = new Parameter("@AUANTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarAUANTANN));
			if (mvarAUANTANN.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarAUANTANN));
			}

			// wvarStep = 1260;
			// wobjDBParm = new Parameter("@AUNUMSIN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarAUNUMSIN));
			if (mvarAUNUMSIN.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarAUNUMSIN));
			}

			// wvarStep = 1270;
			// wobjDBParm = new Parameter("@AUNUMKMT", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarCTAKMS));
			if (mvarCTAKMS.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarCTAKMS));
			}

			// wvarStep = 1280;
			// wobjDBParm = new Parameter("@AUUSOGNC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarAUUSOGNC));
			ps.setString(posiSP++, String.valueOf(mvarAUUSOGNC));

			// wvarStep = 1290;
			// wobjDBParm = new Parameter("@SITUCPOL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant("A"));
			// Ticket 899: Grabamos con "S", que significa que lo enviamos a
			// Insis
			// SITUCPOL
			ps.setString(posiSP++, "S");

			// wvarStep = 1300;
			// wobjDBParm = new Parameter("@CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 1310;
			// wobjDBParm = new Parameter("@CUENTSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 1320;
			// wobjDBParm = new Parameter("@COBROFOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 1330;
			// wobjDBParm = new Parameter("@EDADACTU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(DateTime.diff("YYYY", DateTime.dateSerial(Obj.toInt(mvarNacimAnn), Obj.toInt(mvarNacimMes), Obj.toInt(mvarNacimDia)), DateTime.now())));
			if (new Variant(DateTime.diff("YYYY", DateTime.dateSerial(Obj.toInt(mvarNacimAnn), Obj.toInt(mvarNacimMes), Obj.toInt(mvarNacimDia)), DateTime.now())).isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(new Variant(DateTime.diff("YYYY", DateTime.dateSerial(Obj.toInt(mvarNacimAnn), Obj.toInt(mvarNacimMes), Obj.toInt(mvarNacimDia)), DateTime.now())).toString()));
			}

			// wvarStep = 1340;
			for (wvarCount = 1; wvarCount <= 30; wvarCount++) {
				mvarCOBERCOD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//COBERCOD" + wvarCount));
				mvarCOBERORD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//COBERORD" + wvarCount));
				mvarCAPITASG = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CAPITASG" + wvarCount));
				mvarCAPITIMP = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//CAPITIMP" + wvarCount));

				// wobjDBParm = new Parameter("@COBERCOD" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarCOBERCOD));
				if (mvarCOBERCOD.isEmpty()) {
					ps.setNull(posiSP++, Types.NUMERIC);
				} else {
					ps.setInt(posiSP++, Integer.parseInt(mvarCOBERCOD));
				}

				// wobjDBParm = new Parameter("@COBERORD" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarCOBERORD));
				if (mvarCOBERORD.isEmpty()) {
					ps.setNull(posiSP++, Types.NUMERIC);
				} else {
					ps.setInt(posiSP++, Integer.parseInt(mvarCOBERORD));
				}

				// wobjDBParm = new Parameter("@CAPITASG" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarCAPITASG));
				if (mvarCAPITASG.isEmpty()) {
					ps.setNull(posiSP++, Types.NUMERIC);
				} else {
					ps.setDouble(posiSP++, Double.parseDouble(mvarCAPITASG.replace(",", ".")));
				}

				// wobjDBParm = new Parameter("@CAPITIMP" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarCAPITIMP));
				if (mvarCAPITIMP.isEmpty()) {
					ps.setNull(posiSP++, Types.NUMERIC);
				} else {
					ps.setDouble(posiSP++, Double.parseDouble(mvarCAPITIMP.replace(",", ".")));
				}
			}

			// Asegurados adicionales
			// wvarStep = 1350;
			wobjXMLList = wobjXMLRequest.selectNodes(mcteParam_ASEGURADOS);
			// wvarStep = 1351;
			for (wvarCount = 1; wvarCount <= 10; wvarCount++) {
				mvarDOCUMASEG = "0";
				mvarNOMBREASEG = "";

				if ((wvarCount <= wobjXMLList.getLength()) && !((wobjXMLRequest.selectSingleNode(mcteParam_ASEGADIC) == (org.w3c.dom.Node) null))) {
					if (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ASEGADIC)).equals("S")) {
						XmlDomExtended wvarXMLElem = new XmlDomExtended();
						wvarXMLElem.loadXML(XmlDomExtended.prettyPrint(wobjXMLList.item(wvarCount - 1)));
						if (!((wvarXMLElem.selectSingleNode(mcteParam_DOCUMASEG) == null)) && !((wvarXMLElem.selectSingleNode(mcteParam_NOMBREASEG) == null))) {
							// wvarStep = 1352;
							mvarDOCUMASEG = XmlDomExtended.getText(wvarXMLElem.selectSingleNode(mcteParam_DOCUMASEG));
							// wvarStep = 1353;
							mvarNOMBREASEG = XmlDomExtended.getText(wvarXMLElem.selectSingleNode(mcteParam_NOMBREASEG));
						}
					}
				}
				// wvarStep = 1354;
				// wobjDBParm = new Parameter("@DOCUMDAT" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarDOCUMASEG));
				if (mvarDOCUMASEG.isEmpty()) {
					ps.setNull(posiSP++, Types.NUMERIC);
				} else {
					ps.setInt(posiSP++, Integer.parseInt(mvarDOCUMASEG));
				}
				// wvarStep = 1355;
				// wobjDBParm = new Parameter("@NOMBREAS" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 49, new Variant(mvarNOMBREASEG));
				ps.setString(posiSP++, String.valueOf(mvarNOMBREASEG));
				// wobjDBParm = (Parameter) null;

			}

			// wvarStep = 1360;
			// wobjDBParm = new Parameter("@CAMP_CODIGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarCAMPACOD));
			if (mvarCAMPACOD.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarCAMPACOD));
			}

			// wvarStep = 1370;
			// wobjDBParm = new Parameter("@CAMP_DESC", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 1380;
			// wobjDBParm = new Parameter("@LEGAJO_GTE", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 1390;
			// wobjDBParm = new Parameter("@NRO_PROD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarPRODUCTOR));
			if (mvarPRODUCTOR.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarPRODUCTOR));
			}

			// wvarStep = 1395;
			// wobjDBParm = new Parameter("@EMPRESA_CODIGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarLegajoCia));
			if (mvarLegajoCia.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarLegajoCia));
			}

			// wvarStep = 1400;
			// wobjDBParm = new Parameter("@SUCURSAL_CODIGO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant(mvarSucursalCodigo));
			ps.setString(posiSP++, String.valueOf(mvarSucursalCodigo));

			// wvarStep = 1410;
			// wobjDBParm = new Parameter("@LEGAJO_VEND", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant(mvarLegajoVend));
			ps.setString(posiSP++, String.valueOf(mvarLegajoVend));

			// wvarStep = 1420;
			// wobjDBParm = new Parameter("@EMPL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 1420;
			wobjXMLList = wobjXMLRequest.selectNodes(mcteParam_CONDUCTORES);
			for (wvarCount = 1; wvarCount <= 10; wvarCount++) {

				if (wvarCount <= wobjXMLList.getLength()) {
					XmlDomExtended wvarXMLElem = new XmlDomExtended();
					wvarXMLElem.loadXML(XmlDomExtended.prettyPrint(wobjXMLList.item(wvarCount - 1)));
					mvarCONDUAPE = XmlDomExtended.getText(wvarXMLElem.selectSingleNode(mcteParam_CONDUAPE));
					mvarCONDUNOM = XmlDomExtended.getText(wvarXMLElem.selectSingleNode(mcteParam_CONDUNOM));
					mvarCONDUFEC = XmlDomExtended.getText(wvarXMLElem.selectSingleNode(mcteParam_CONDUFEC));
					mvarCONDUSEX = Strings.toUpperCase(XmlDomExtended.getText(wvarXMLElem.selectSingleNode(mcteParam_CONDUSEX)));
					mvarCONDUEST = Strings.toUpperCase(XmlDomExtended.getText(wvarXMLElem.selectSingleNode(mcteParam_CONDUEST)));
					mvarCONDUEXC = "1";
				} else {
					mvarCONDUAPE = "";
					mvarCONDUNOM = "";
					mvarCONDUFEC = "0";
					mvarCONDUSEX = "";
					mvarCONDUEST = "";
					mvarCONDUEXC = "0";
				}
				// wobjDBParm = new Parameter("@CONDUAPE" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 20, new Variant(mvarCONDUAPE));
				ps.setString(posiSP++, String.valueOf(mvarCONDUAPE));
				// wobjDBParm = (Parameter) null;

				// wobjDBParm = new Parameter("@CONDUNOM" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 30, new Variant(mvarCONDUNOM));
				ps.setString(posiSP++, String.valueOf(mvarCONDUNOM));
				// wobjDBParm = (Parameter) null;

				// wobjDBParm = new Parameter("@CONDUFEC" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarCONDUFEC));
				if (mvarCONDUFEC.isEmpty()) {
					ps.setNull(posiSP++, Types.NUMERIC);
				} else {
					ps.setInt(posiSP++, Integer.parseInt(mvarCONDUFEC));
				}

				// wobjDBParm = new Parameter("@CONDUSEX" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarCONDUSEX));
				ps.setString(posiSP++, String.valueOf(mvarCONDUSEX));
				// wobjDBParm = (Parameter) null;

				// wobjDBParm = new Parameter("@CONDUEST" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarCONDUEST));
				ps.setString(posiSP++, String.valueOf(mvarCONDUEST));
				// wobjDBParm = (Parameter) null;

				// wobjDBParm = new Parameter("@CONDUEXC" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarCONDUEXC));
				ps.setString(posiSP++, String.valueOf(mvarCONDUEXC));
				// wobjDBParm = (Parameter) null;
			}

			// wvarStep = 1430;
			wobjXMLList = (org.w3c.dom.NodeList) null;
			wobjXMLList = wobjXMLRequest.selectNodes(mcteParam_ACCESORIOS);
			for (wvarCount = 1; wvarCount <= 10; wvarCount++) {

				if (wvarCount <= wobjXMLList.getLength()) {
					XmlDomExtended wvarXMLElem = new XmlDomExtended();
					wvarXMLElem.loadXML(XmlDomExtended.prettyPrint(wobjXMLList.item(wvarCount - 1)));
					mvarAUACCCOD = XmlDomExtended.getText(wvarXMLElem.selectSingleNode(mcteParam_AUACCCOD));
					mvarAUVEASUM = XmlDomExtended.getText(wvarXMLElem.selectSingleNode(mcteParam_AUVEASUM));
					mvarAUVEADES = XmlDomExtended.getText(wvarXMLElem.selectSingleNode(mcteParam_AUVEADES));
					mvarAUVEADEP = "S";
				} else {
					mvarAUACCCOD = "0";
					mvarAUVEASUM = "0";
					mvarAUVEADES = "";
					mvarAUVEADEP = "N";
				}

				// wobjDBParm = new Parameter("@AUACCCOD" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarAUACCCOD));
				if (mvarAUACCCOD.isEmpty()) {
					ps.setNull(posiSP++, Types.NUMERIC);
				} else {
					ps.setInt(posiSP++, Integer.parseInt(mvarAUACCCOD));
				}

				// wobjDBParm = new Parameter("@AUVEASUM" + wvarCount, AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarAUVEASUM));
				if (mvarAUVEASUM.isEmpty()) {
					ps.setNull(posiSP++, Types.NUMERIC);
				} else {
					ps.setInt(posiSP++, Integer.parseInt(mvarAUVEASUM));
				}

				// wobjDBParm = new Parameter("@AUVEADES" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 30, new Variant(Strings.toUpperCase(mvarAUVEADES)));
				ps.setString(posiSP++, String.valueOf(Strings.toUpperCase(mvarAUVEADES)));
				// wobjDBParm = (Parameter) null;

				// wobjDBParm = new Parameter("@AUVEADEP" + wvarCount, AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarAUVEADEP));
				ps.setString(posiSP++, String.valueOf(mvarAUVEADEP));
				// wobjDBParm = (Parameter) null;
			}

			// wvarStep = 1435;
			// wobjDBParm = new Parameter("@ESTAINSP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarINSPECOD));
			if (mvarINSPECOD.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarINSPECOD));
			}

			// wobjDBParm = new Parameter("@INSPECOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wobjDBParm = new Parameter("@INSPEDES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant(""));
			ps.setString(posiSP++, "");

			// wobjDBParm = new Parameter("@INSPADOM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(""));
			ps.setString(posiSP++, "");

			// wobjDBParm = new Parameter("@INSPEOBS", AdoConst.adChar, AdoConst.adParamInput, 50, new Variant(""));
			ps.setString(posiSP++, "");

			// wobjDBParm = new Parameter("@INSPEKMS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wobjDBParm = new Parameter("@CENSICOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant(mvarCENSICOD));
			ps.setString(posiSP++, String.valueOf(mvarCENSICOD));

			// wobjDBParm = new Parameter("@CENSIDES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant(""));
			ps.setString(posiSP++, "");

			// wobjDBParm = new Parameter("@DISPOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarRastreo));
			if (mvarRastreo.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarRastreo));
			}

			// wobjDBParm = new Parameter("@PRESTCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant(mvarALARMCOD));
			ps.setString(posiSP++, String.valueOf(mvarALARMCOD));
			// Borrada porque no se envia a la base
			// mvarACREPREN = "N";
			mvarNUMEDOCUACRE = "";
			mvarTIPODOCUACRE = "0";
			mvarAPELLIDOACRE = "";
			mvarNOMBRESACRE = "";
			mvarDOMICDOMACRE = "";
			mvarDOMICDNUACRE = "";
			mvarDOMICPISACRE = "";
			mvarDOMICPTAACRE = "";
			mvarDOMICPOBACRE = "";
			mvarDOMICCPOACRE = "0";
			mvarPROVICODACRE = "0";
			mvarPAISSCODACRE = "";

			if (!(wobjXMLRequest.selectSingleNode(mcteParam_ACREPREN) == (org.w3c.dom.Node) null)) {
				// mvarACREPREN = XmlDomExtended.getText(
				// wobjXMLRequest.selectSingleNode( mcteParam_ACREPREN ) );
				if (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ACREPREN)).equals("S")) {
					if (!(wobjXMLRequest.selectSingleNode(mcteParam_NUMEDOCUACRE) == (org.w3c.dom.Node) null)) {
						mvarNUMEDOCUACRE = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_NUMEDOCUACRE));
					}
					if (!(wobjXMLRequest.selectSingleNode(mcteParam_TIPODOCUACRE) == (org.w3c.dom.Node) null)) {
						mvarTIPODOCUACRE = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_TIPODOCUACRE));
					}
					if (!(wobjXMLRequest.selectSingleNode(mcteParam_APELLIDOACRE) == (org.w3c.dom.Node) null)) {
						mvarAPELLIDOACRE = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_APELLIDOACRE));
					}
					if (!(wobjXMLRequest.selectSingleNode(mcteParam_NOMBRESACRE) == (org.w3c.dom.Node) null)) {
						mvarNOMBRESACRE = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_NOMBRESACRE));
					}
					if (!(wobjXMLRequest.selectSingleNode(mcteParam_DOMICDOMACRE) == (org.w3c.dom.Node) null)) {
						mvarDOMICDOMACRE = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICDOMACRE));
					}
					if (!(wobjXMLRequest.selectSingleNode(mcteParam_DOMICDNUACRE) == (org.w3c.dom.Node) null)) {
						mvarDOMICDNUACRE = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICDNUACRE));
					}
					if (!(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPISACRE) == (org.w3c.dom.Node) null)) {
						mvarDOMICPISACRE = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPISACRE));
					}
					if (!(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPTAACRE) == (org.w3c.dom.Node) null)) {
						mvarDOMICPTAACRE = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPTAACRE));
					}
					if (!(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPOBACRE) == (org.w3c.dom.Node) null)) {
						mvarDOMICPOBACRE = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICPOBACRE));
					}
					if (!(wobjXMLRequest.selectSingleNode(mcteParam_DOMICCPOACRE) == (org.w3c.dom.Node) null)) {
						mvarDOMICCPOACRE = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOMICCPOACRE));
					}
					if (!(wobjXMLRequest.selectSingleNode(mcteParam_PROVICODACRE) == (org.w3c.dom.Node) null)) {
						mvarPROVICODACRE = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PROVICODACRE));
					}
					if (!(wobjXMLRequest.selectSingleNode(mcteParam_PAISSCODACRE) == (org.w3c.dom.Node) null)) {
						mvarPAISSCODACRE = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PAISSCODACRE));
					}
				}
			}

			// wvarStep = 1550;
			// wobjDBParm = new Parameter("@NUMEDOCU_ACRE", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant(mvarNUMEDOCUACRE));
			ps.setString(posiSP++, String.valueOf(mvarNUMEDOCUACRE));

			// wvarStep = 1560;
			// wobjDBParm = new Parameter("@TIPODOCU_ACRE", AdoConst.adNumeric, AdoConst.adParamInput, 0, (Strings.trim(mvarTIPODOCUACRE).equals("") ? new Variant((Object) new Integer(0)) : new Variant(mvarTIPODOCUACRE)));
			if (mvarTIPODOCUACRE.isEmpty()) {
				ps.setInt(posiSP++, 0);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarTIPODOCUACRE));
			}

			// wvarStep = 1570;
			// wobjDBParm = new Parameter("@APELLIDO_ACRE", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant(mvarAPELLIDOACRE));
			ps.setString(posiSP++, String.valueOf(mvarAPELLIDOACRE));

			// wvarStep = 1580;
			// wobjDBParm = new Parameter("@NOMBRES_ACRE", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant(mvarNOMBRESACRE));
			ps.setString(posiSP++, String.valueOf(mvarNOMBRESACRE));

			// wvarStep = 1590;
			// wobjDBParm = new Parameter("@DOMICDOM_ACRE", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant(mvarDOMICDOMACRE));
			ps.setString(posiSP++, String.valueOf(mvarDOMICDOMACRE));

			// wvarStep = 1600;
			// wobjDBParm = new Parameter("@DOMICDNU_ACRE", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant(mvarDOMICDNUACRE));
			ps.setString(posiSP++, String.valueOf(mvarDOMICDNUACRE));

			// wvarStep = 1610;
			// wobjDBParm = new Parameter("@DOMICESC_ACRE", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 1620;
			// wobjDBParm = new Parameter("@DOMICPIS_ACRE", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant(mvarDOMICPISACRE));
			ps.setString(posiSP++, String.valueOf(mvarDOMICPISACRE));

			// wvarStep = 1630;
			// wobjDBParm = new Parameter("@DOMICPTA_ACRE", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant(mvarDOMICPTAACRE));
			ps.setString(posiSP++, String.valueOf(mvarDOMICPTAACRE));

			// wvarStep = 1640;
			// wobjDBParm = new Parameter("@DOMICPOB_ACRE", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant(mvarDOMICPOBACRE));
			ps.setString(posiSP++, String.valueOf(mvarDOMICPOBACRE));

			// wvarStep = 1650;
			// wobjDBParm = new Parameter("@DOMICCPO_ACRE", AdoConst.adNumeric, AdoConst.adParamInput, 0, (Strings.trim(mvarDOMICCPOACRE).equals("") ? new Variant((Object) new Integer(0)) : new Variant(mvarDOMICCPOACRE)));
			if (mvarDOMICCPOACRE.isEmpty()) {
				ps.setInt(posiSP++, 0);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarDOMICCPOACRE));
			}

			// wvarStep = 1660;
			// wobjDBParm = new Parameter("@PROVICOD_ACRE", AdoConst.adNumeric, AdoConst.adParamInput, 0, (Strings.trim(mvarPROVICODACRE).equals("") ? new Variant((Object) new Integer(0)) : new Variant(mvarPROVICODACRE)));
			if (mvarPROVICODACRE.isEmpty()) {
				ps.setInt(posiSP++, 0);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarPROVICODACRE));
			}

			// wvarStep = 1670;
			// wobjDBParm = new Parameter("@PAISSCOD_ACRE", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant(mvarPAISSCODACRE));
			ps.setString(posiSP++, String.valueOf(mvarPAISSCODACRE));

			// wvarStep = 1680;
			// wobjDBParm = new Parameter("@COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarCOBROCOD));
			if (mvarCOBROCOD.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarCOBROCOD));
			}

			// wvarStep = 1690;
			// wobjDBParm = new Parameter("@INSPEANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 1700;
			// wobjDBParm = new Parameter("@INSPEMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 1710;
			// wobjDBParm = new Parameter("@INSPEDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 1720;
			// wobjDBParm = new Parameter("@ENTDOMSC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 1730;
			// wobjDBParm = new Parameter("@ANOTACIO", AdoConst.adChar, AdoConst.adParamInput, 100, new Variant(mvarANOTACIO));
			ps.setString(posiSP++, String.valueOf(mvarANOTACIO));

			// wvarStep = 1740;
			String precioMensual = String.valueOf(Obj.toDouble(mvarPRECIO_MENSUAL) / 100);

			String pcioMensualConver;
			if (precioMensual.indexOf(".") != -1) {
				pcioMensualConver = precioMensual.substring(0, precioMensual.indexOf("."));
			} else {
				pcioMensualConver = precioMensual;
			}
			// wobjDBParm = new Parameter("@PRECIO_MENSUAL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(pcioMensualConver));
			if (pcioMensualConver.isEmpty()) {
				ps.setInt(posiSP++, 0);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(pcioMensualConver));
			}

			// wvarStep = 1750;
			// wobjDBParm = new Parameter("@EFECTANN_SOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarVIGENANN));
			if (mvarVIGENANN.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarVIGENANN));
			}

			// wvarStep = 1760;
			// wobjDBParm = new Parameter("@EFECTMES_SOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarVIGENMES));
			if (mvarVIGENMES.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarVIGENMES));
			}

			// wvarStep = 1770;
			// wobjDBParm = new Parameter("@EFECTDIA_SOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarVIGENDIA));
			if (mvarVIGENDIA.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarVIGENDIA));
			}

			// wvarStep = 17710;
			// wobjDBParm = new Parameter("@CUITNUME", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant(mvarCUITNUME));
			ps.setString(posiSP++, String.valueOf(mvarCUITNUME));

			// wvarStep = 17711;
			// wobjDBParm = new Parameter("@RAZONSOC", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant(mvarRAZONSOC));
			ps.setString(posiSP++, String.valueOf(mvarRAZONSOC));

			// wvarStep = 17712;
			// wobjDBParm = new Parameter("@CLIEIBTP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarIBB));
			ps.setString(posiSP++, String.valueOf(mvarIBB));

			// wvarStep = 17713;
			// wobjDBParm = new Parameter("@NROIIBB", AdoConst.adChar, AdoConst.adParamInput, 15, new Variant(mvarNROIBB));
			ps.setString(posiSP++, String.valueOf(mvarNROIBB));

			// wvarStep = 17714;
			// wobjDBParm = new Parameter("@LUNETA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarLUNETA));
			ps.setString(posiSP++, String.valueOf(mvarLUNETA));

			// wvarStep = 17715;
			// wobjDBParm = new Parameter("@HIJOS1729_EXC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 17716;
			// wobjDBParm = new Parameter("@NROFACTU", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant(mvarNROFACTU));
			ps.setString(posiSP++, String.valueOf(mvarNROFACTU));

			// wvarStep = 17717;
			// wobjDBParm = new Parameter("@INS_DOMICDOM", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17718;
			// wobjDBParm = new Parameter("@INS_CPACODPO", AdoConst.adChar, AdoConst.adParamInput, 8, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17719;
			// wobjDBParm = new Parameter("@INS_DOMICPOB", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17720;
			// wobjDBParm = new Parameter("@INS_PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 17721;
			// wobjDBParm = new Parameter("@INS_DOMICTLF", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17722;
			// wobjDBParm = new Parameter("@INS_RANGOHOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 17723;
			// wobjDBParm = new Parameter("@DDJJ", AdoConst.adChar, AdoConst.adParamInput, 140, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17724;
			// wobjDBParm = new Parameter("@DERIVACI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17725;
			// wobjDBParm = new Parameter("@ALEATORIO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17726;
			// wobjDBParm = new Parameter("@ESTAIDES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17727;
			// wobjDBParm = new Parameter("@DISPODES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17728;
			// wobjDBParm = new Parameter("@INSTALADP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarINSTALADP));
			ps.setString(posiSP++, String.valueOf(mvarINSTALADP));

			// wvarStep = 17729;
			// wobjDBParm = new Parameter("@POSEEDISP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarPOSEEDISP));
			ps.setString(posiSP++, String.valueOf(mvarPOSEEDISP));

			// wvarStep = 17730;
			// wobjDBParm = new Parameter("@AUTIPGAMA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17731;
			// wobjDBParm = new Parameter("@SWDT80", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarDESTRUCCION_80));
			ps.setString(posiSP++, String.valueOf(mvarDESTRUCCION_80));

			// wvarStep = 17732;
			// wobjDBParm = new Parameter("@TIPO_PROD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant("PR"));
			ps.setString(posiSP++, "PR");

			// wvarStep = 17733;
			// wobjDBParm = new Parameter("@SWAPODER", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant("N"));
			ps.setString(posiSP++, "N");

			// wvarStep = 17734;
			// wobjDBParm = new Parameter("@OCUPACODCLIE", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17735;
			// wobjDBParm = new Parameter("@OCUPACODAPOD", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17736;
			// wobjDBParm = new Parameter("@CLIENSECAPOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(0));
			ps.setInt(posiSP++, 0);

			// wvarStep = 17737;
			// wobjDBParm = new Parameter("@DOMISECAPOD", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17738;
			// wobjDBParm = new Parameter("@GREEN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarCLUBECO));
			ps.setString(posiSP++, String.valueOf(mvarCLUBECO));

			// wvarStep = 17739;
			// wobjDBParm = new Parameter("@GRANIZO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarGRANIZO));
			ps.setString(posiSP++, String.valueOf(mvarGRANIZO));

			// wvarStep = 17740;
			// wobjDBParm = new Parameter("@ROBOCONT", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant(mvarROBOCONT));
			ps.setString(posiSP++, String.valueOf(mvarROBOCONT));

			// wvarStep = 17741;
			// wobjDBParm = new Parameter("@SN_NBWS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant("N"));
			ps.setString(posiSP++, "N");

			// wvarStep = 17742;
			// wobjDBParm = new Parameter("@EMAIL_NBWS", AdoConst.adChar, AdoConst.adParamInput, 50, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17743;
			// wobjDBParm = new Parameter("@SN_EPOLIZA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant("N"));
			ps.setString(posiSP++, "N");

			// wvarStep = 17744;
			// wobjDBParm = new Parameter("@EMAIL_EPOLIZA", AdoConst.adChar, AdoConst.adParamInput, 50, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17745;
			// wobjDBParm = new Parameter("@SN_PRESTAMAIL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant("N"));
			ps.setString(posiSP++, "N");

			// wvarStep = 17746;
			// wobjDBParm = new Parameter("@PASSEPOL", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant(""));
			ps.setString(posiSP++, "");

			// wvarStep = 17747;
			// wobjDBParm = new Parameter("@CERTISECNEWSAS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant(mvarNROSOLI));
			if (mvarNROSOLI.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarNROSOLI));
			}

			// wvarStep = 1790;
			System.out.println(ps);
			boolean result = ps.execute();

			if (result) {
				// El primero es un ResultSet
			} else {
				// Salteo el "updateCount"
				int uc = ps.getUpdateCount();
				if (uc == -1) {
					// System.out.println("No hay resultados");
				}
				result = ps.getMoreResults();
			}
			int certiSecValRet = ps.getInt(6);
			pvarCertiSec.set(certiSecValRet);

			return fncGrabaSolicitud;
		} finally {
			try {
				if (jdbcConn != null)
					jdbcConn.close();
			} catch (Exception e) {
				logger.log(Level.WARNING, "Exception al hacer un close", e);
			}
		}
	}

	public void Activate() throws Exception {
	}

	public boolean CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		//
		ObjectControl_CanBePooled = true;
		//
		return ObjectControl_CanBePooled;
	}

	public void Deactivate() throws Exception {
	}
}
