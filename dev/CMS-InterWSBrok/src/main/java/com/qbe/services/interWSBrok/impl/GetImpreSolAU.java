package com.qbe.services.interWSBrok.impl;

import java.net.MalformedURLException;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mwGenerico.impl.lbaw_MQMW;
import com.qbe.services.ofVirtualLBA.impl.lbaw_GetTipIngBrut;
import com.qbe.services.ofVirtualLBA.impl.lbaw_GetTiposIVA;
import com.qbe.services.webmq.impl.lbaw_GetVehiculos;
import com.qbe.services.webmq.impl.lbaw_GetZona;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

import diamondedge.ado.Connection;
import diamondedge.ado.Parameter;
import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;

/**
 * Componente interno, no se publica como WebService a los brokers
 * 
 * TODO Especificar interface
 * 
 * @author ramiro
 *
 */
public class GetImpreSolAU extends BaseOSBClient implements VBObjectClass {

	protected static Logger logger = Logger.getLogger(GetImpreSolAU.class.getName());

	static final String mcteClassName = "LBA_InterWSBrok.GetImpreSolAU";
	static final String mcteStoreProc = "SPSNCV_BRO_SOLI_AUS1_IMPRE";
	/**
	 * Parametros XML de Entrada
	 */
	static final String mcteParam_WDB_IDENTIFICACION_BROKER = "//CODINST";
	static final String mcteParam_WDB_NRO_OPERACION_BROKER = "//REQUESTID";
	static final String mcteParam_WDB_NRO_COTIZACION_LBA = "//COT_NRO";
	static final String mcteParam_WDB_TIPO_OPERACION = "//TIPOOPERACION";
	static final String mcteParam_PRODUCTOR = "//AGECOD";
	static final String mcteParam_NumeDocu = "//NUMEDOCU";
	static final String mcteParam_TipoDocu = "//TIPODOCU";
	static final String mcteParam_ClienApe1 = "//CLIENAP1";
	static final String mcteParam_ClienApe2 = "//CLIENAP2";
	static final String mcteParam_ClienNom1 = "//CLIENNOM";
	static final String mcteParam_NacimAnn = "//NACIMANN";
	static final String mcteParam_NacimMes = "//NACIMMES";
	static final String mcteParam_NacimDia = "//NACIMDIA";
	static final String mcteParam_CLIENSEX = "//SEXO";
	static final String mcteParam_CLIENEST = "//ESTADO";
	static final String mcteParam_EMAIL = "//EMAIL";

	/**
	 * Domicilio de riesgo
	 */
	static final String mcteParam_DOMICDOM = "//DOMICDOM";
	static final String mcteParam_DomicDnu = "//DOMICDNU";
	static final String mcteParam_DOMICPIS = "//DOMICPIS";
	static final String mcteParam_DomicPta = "//DOMICPTA";
	static final String mcteParam_DOMICPOB = "//DOMICPOB";
	static final String mcteParam_DOMICCPO = "//LOCALIDADCOD";
	static final String mcteParam_PROVICOD = "//PROVI";
	static final String mcteParam_TELCOD = "//TELCOD";
	static final String mcteParam_TELNRO = "//TELNRO";
	/**
	 * datos del domicilio de correspondencia
	 */
	static final String mcteParam_DOMICDOM_CO = "//DOMICDOMCOR";
	static final String mcteParam_DomicDnu_CO = "//DOMICDNUCOR";
	static final String mcteParam_DOMICPIS_CO = "//DOMICPISCOR";
	static final String mcteParam_DomicPta_CO = "//DOMICPTACOR";
	static final String mcteParam_DOMICPOB_CO = "//DOMICPOBCOR";
	static final String mcteParam_DOMICCPO_CO = "//LOCALIDADCODCOR";
	static final String mcteParam_PROVICOD_CO = "//PROVICOR";
	static final String mcteParam_TELCOD_CO = "//TELCODCOR";
	static final String mcteParam_TELNRO_CO = "//TELNROCOR";
	/**
	 * datos de la cuenta
	 */
	static final String mcteParam_COBROTIP = "//COBROTIP";
	static final String mcteParam_CUENNUME = "//CUENNUME";
	static final String mcteParam_VENCIANN = "//VENCIANN";
	static final String mcteParam_VENCIMES = "//VENCIMES";
	static final String mcteParam_VENCIDIA = "//VENCIDIA";
	/**
	 * datos de la operacion
	 */
	static final String mcteParam_FRANQCOD = "//FRANQCOD";
	static final String mcteParam_PLANCOD = "//PLANNCOD";
	static final String mcteParam_ZONA = "//CODZONA";
	static final String mcteParam_CLIENIVA = "//IVA";
	static final String mcteParam_SUMAASEG = "//SUMAASEG";
	static final String mcteParam_CLUB_LBA = "//CLUBLBA";
	static final String mcteParam_CPAANO = "//CPAANO";
	static final String mcteParam_CTAKMS = "//KMSRNGCOD";
	static final String mcteParam_Escero = "//ESCERO";
	static final String mcteParam_SIFMVEHI_DES = "//VEHDES";
	static final String mcteParam_MOTORNUM = "//MOTORNUM";
	static final String mcteParam_CHASINUM = "//CHASINUM";
	static final String mcteParam_PATENNUM = "//PATENNUM";
	/**
	 * AUMARCOD+AUMODCOD+AUSUBCOD+AUADICODAUMODORI
	 */
	static final String mcteParam_ModeAutCod = "//MODEAUTCOD";
	static final String mcteParam_VEHCLRCOD = "//VEHCLRCOD";
	static final String mcteParam_EfectAnn = "//EFECTANN";
	static final String mcteParam_GUGARAGE = "//SIGARAGE";
	static final String mcteParam_GUDOMICI = "//GUDOMICI";
	static final String mcteParam_AUCATCOD = "//AUCATCOD";
	static final String mcteParam_AUTIPCOD = "//AUTIPCOD";
	static final String mcteParam_AUANTANN = "//AUANTANN";
	static final String mcteParam_AUNUMSIN = "//SINIESTROS";
	static final String mcteParam_AUUSOGNC = "//GAS";
	static final String mcteParam_CampaCod = "//CAMPACOD";
	static final String mcteParam_CONDUCTORES = "//HIJOS/HIJO";
	static final String mcteParam_CONDUAPE = "APELLIDOHIJO";
	static final String mcteParam_CONDUNOM = "NOMBREHIJO";
	static final String mcteParam_CONDUFEC = "NACIMHIJO";
	static final String mcteParam_CONDUSEX = "SEXOHIJO";
	static final String mcteParam_CONDUEST = "ESTADOHIJO";
	static final String mcteParam_CONDUEXC = "INCLUIDO";
	static final String mcteParam_ACCESORIOS = "//ACCESORIOS/ACCESORIO";
	static final String mcteParam_AUACCCOD = "CODIGOACC";
	static final String mcteParam_AUVEASUM = "PRECIOACC";
	static final String mcteParam_AUVEADES = "DESCRIPCIONACC";
	static final String mcteParam_INSPECOD = "//INSPECOD";
	static final String mcteParam_RASTREO = "//RASTREO";
	static final String mcteParam_ALARMCOD = "//ALARMCOD";
	static final String mcteParam_COBROCOD = "//COBROCOD";
	static final String mcteParam_ANOTACIO = "//ANOTACIONES";
	static final String mcteParam_VIGENANN = "//VIGENANN";
	static final String mcteParam_VIGENMES = "//VIGENMES";
	static final String mcteParam_VIGENDIA = "//VIGENDIA";
	/**
	 * hora en que empezo la transaccion
	 */
	static final String mcteParam_MSGANO = "//MSGANO";
	static final String mcteParam_MSGMES = "//MSGMES";
	static final String mcteParam_MSGDIA = "//MSGDIA";
	static final String mcteParam_MSGHORA = "//MSGHORA";
	static final String mcteParam_MSGMINUTO = "//MSGMINUTO";
	static final String mcteParam_MSGSEGUNDO = "//MSGSEGUNDO";
	/**
	 * Datos de la cotizacion
	 */
	static final String mcteParm_TIEMPO = "//TIEMPO";
	static final String mcteParam_DESTRUCCION_80 = "//DESTRUCCION_80";
	static final String mcteParam_Luneta = "//LUNETA";
	static final String mcteParam_ClubEco = "//CLUBECO";
	static final String mcteParam_Robocont = "//ROBOCONT";
	static final String mcteParam_Granizo = "//GRANIZO";
	static final String mcteParam_IBB = "//IBB";
	static final String mcteParam_NROIBB = "//NROIBB";
	static final String mcteParam_INSTALADP = "//INSTALADP";
	static final String mcteParam_POSEEDISP = "//POSEEDISP";
	static final String mcteParam_CERTISEC = "//CERTISEC";
	static final String mcteParam_PRECIO = "//PRECIO";
	/**
	 * 11/2010 se permiten distintos tipos de iva
	 */
	static final String mcteParam_CUITNUME = "//CUITNUME";
	static final String mcteParam_RAZONSOC = "//RAZONSOC";
	static final String mcteParam_CLIENTIP = "//CLIENTIP";

	public int IAction_Execute(String pvarRequest, StringHolder pvarResponse, String pvarContextInfo) {
		int IAction_Execute = 0;
		String wvarMensaje = "";
		StringHolder pvarRes = new StringHolder();
		try {
			pvarRes.set("");
			getImpresos(pvarRequest, wvarMensaje, pvarRes);
			pvarResponse.set("<Response_WS>" + pvarRes.getValue() + "</Response_WS>");
			IAction_Execute = 0;
			return IAction_Execute;
		} catch (Exception _e_) {
			logger.log(Level.SEVERE, "", _e_);
			IAction_Execute = 1;
			// Logueo el timestamp para que si el usuario reporta el error
			// podamos usarlo para buscar en los logs
			String resultResponse = "<LBA_WS res_code=\"-2000\" res_msg=\"" + General.mcteErrorInesperadoDescr + " \">" + "Error inesperado - TS-"+ System.currentTimeMillis() + " CI - " + pvarContextInfo + "</LBA_WS>";
			pvarResponse.set("<Response_WS>" + resultResponse + "</Response_WS>");
		}
		return IAction_Execute;
	}

	private boolean getImpresos(String pvarRequest, String wvarMensaje, StringHolder pvarRes) throws XmlDomExtendedException, SQLException,
			MalformedURLException, OSBConnectorException {
		boolean fncGetAll = false;
		String mvarDatosImpreso = "";
		String mvarNodoImpreso = "";
		
		XmlDomExtended wobjXMLRequestSolis = new XmlDomExtended();
		wobjXMLRequestSolis.loadXML(pvarRequest);
				
		mvarDatosImpreso = buildImpresosRequest(wobjXMLRequestSolis);
		
		// Se generan los pdf en base 64, son 4 pdf, corresponden a 3 nodos
		// a enviar
		// BRO_SolicitudAUS1 va siempre
		// BRO_CertificadoProvisorioAUS1.rptdesign va siempre
		// BRO_AutorizacionDebitoTarjAUS1.rptdesign solo cuando es tarjeta
		// BRO_AutorizacionDebitoAUS1.rptdesign solo cuando es cbu

		String mvarImpreso64 = "";
		
		mvarNodoImpreso = getImpresoSolicitud(mvarDatosImpreso);
		mvarImpreso64 = mvarImpreso64 + "<IMPSOLI>" + mvarNodoImpreso + "</IMPSOLI>";

		mvarNodoImpreso = getImpresoCertificadoProvisorio(mvarDatosImpreso);
		mvarImpreso64 = mvarImpreso64 + "<IMPCERTI>" + mvarNodoImpreso + "</IMPCERTI>";

		boolean debitoEnTarjeta = Obj.toInt(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_COBROCOD))) == 4;
		mvarNodoImpreso = getImpresoDebito(mvarDatosImpreso, debitoEnTarjeta);
		mvarImpreso64 = mvarImpreso64 + "<IMPAUTORI>" + mvarNodoImpreso + "</IMPAUTORI>";

		pvarRes.set(mvarImpreso64);

		fncGetAll = true;
		return fncGetAll;
	}

	protected String getImpresoDebito(String mvarDatosImpreso, boolean debitoEnTarjeta) throws OSBConnectorException, MalformedURLException,
			XmlDomExtendedException {
		XmlDomExtended mobjXMLDoc;
		String mvarNodoImpreso;
		String mvarRequest;
		String mvarResponse;
		// Autorización debito en cuenta o tarjeta
		if (debitoEnTarjeta) {
			mvarRequest = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>"
					+ mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_AutorizacionDebitoTarjAUS1</reportId>" + "</Request>";
		} else {
			mvarRequest = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>"
					+ mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_AutorizacionDebitoAUS1</reportId>" + "</Request>";
		}
		mvarResponse = getOsbConnector().executeRequest("lbaw_OVMWGen", mvarRequest);

		mobjXMLDoc = new XmlDomExtended();
		mobjXMLDoc.loadXML(mvarResponse);
		// Verifica que haya respuesta de MQ
		if (XmlDomExtended.getText(mobjXMLDoc.selectSingleNode("//Estado").getAttributes().getNamedItem("resultado")).equals("true")) {
			// Verifica que exista el PDF si no error Middle
			if (!(mobjXMLDoc.selectSingleNode("//generatePdfReportReturn") == (org.w3c.dom.Node) null)) {
				mvarNodoImpreso = XmlDomExtended.getText(XmlDomExtended.Node_selectNodes(mobjXMLDoc.getDocument().getDocumentElement(),
						"//generatePdfReportReturn"));
			} else {
				// error de mdw
				mvarNodoImpreso = "No se pudo obtener el impreso MDW";
			}
		} else {
			mvarNodoImpreso = "No se pudo obtener el impreso Autoriz. Debito (MQ)";
		}

		mobjXMLDoc = null;
		return mvarNodoImpreso;
	}

	protected String getImpresoCertificadoProvisorio(String mvarDatosImpreso) throws OSBConnectorException, MalformedURLException, XmlDomExtendedException {
		XmlDomExtended mobjXMLDoc;
		String mvarNodoImpreso;
		String mvarRequest;
		String mvarResponse;
		mvarRequest = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>"
				+ mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_CertificadoProvisorioAUS1</reportId>" + "</Request>";

		logger.log(Level.FINE, "Llamando localmente a lbaw_OVMWGen");
		lbaw_MQMW mqmw = new lbaw_MQMW();
		StringHolder mqmwSH = new StringHolder();
		mqmw.IAction_Execute(mvarRequest, mqmwSH, "");
		mvarResponse = mqmwSH.getValue();

		mobjXMLDoc = new XmlDomExtended();
		mobjXMLDoc.loadXML(mvarResponse);
		// Verifica que haya respuesta de MQ
		if (XmlDomExtended.getText(mobjXMLDoc.selectSingleNode("//Estado").getAttributes().getNamedItem("resultado")).equals("true")) {
			// Verifica que exista el PDF si no error Middle
			if (!(mobjXMLDoc.selectSingleNode("//generatePdfReportReturn") == (org.w3c.dom.Node) null)) {
				mvarNodoImpreso = XmlDomExtended.getText(XmlDomExtended.Node_selectNodes(mobjXMLDoc.getDocument().getDocumentElement(),
						"//generatePdfReportReturn"));
			} else {
				// error de mdw
				mvarNodoImpreso = "No se pudo obtener el impreso MDW";
			}
		} else {
			mvarNodoImpreso = "No se pudo obtener el impreso de Certificado (MQ)";
		}

		mobjXMLDoc = null;
		return mvarNodoImpreso;
	}

	protected String getImpresoSolicitud(String mvarDatosImpreso) throws OSBConnectorException, MalformedURLException, XmlDomExtendedException {
		XmlDomExtended mobjXMLDoc;
		String mvarNodoImpreso;
		String mvarRequest;
		String mvarResponse;
		mvarRequest = "<Request>" + "<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" + "<Raiz>share:generatePdfReport</Raiz>" + "<xmlDataSource>"
				+ mvarDatosImpreso + "</xmlDataSource>" + "<reportId>BRO_SolicitudAUS1</reportId>" + "</Request>";

		logger.log(Level.FINE, "Llamando localmente a lbaw_OVMWGen");
		lbaw_MQMW mqmw = new lbaw_MQMW();
		StringHolder mqmwSH = new StringHolder();
		mqmw.IAction_Execute(mvarRequest, mqmwSH, "");
		mvarResponse = mqmwSH.getValue();

		mobjXMLDoc = new XmlDomExtended();
		mobjXMLDoc.loadXML(mvarResponse);
		// Verifica que haya respuesta de MQ
		if (XmlDomExtended.getText(mobjXMLDoc.selectSingleNode("//Estado").getAttributes().getNamedItem("resultado")).equals("true")) {
			// Verifica que exista el PDF si no error Middle
			if (!(mobjXMLDoc.selectSingleNode("//generatePdfReportReturn") == (org.w3c.dom.Node) null)) {
				mvarNodoImpreso = XmlDomExtended.getText(XmlDomExtended.Node_selectNodes(mobjXMLDoc.getDocument().getDocumentElement(),
						"//generatePdfReportReturn"));
			} else {
				// error de mdw
				mvarNodoImpreso = "No se pudo obtener el impreso MDW";
			}
		} else {
			mvarNodoImpreso = "No se pudo obtener el impreso de Solicitud (MQ)";
		}

		mobjXMLDoc = null;
		return mvarNodoImpreso;
	}

	protected String buildImpresosRequest(XmlDomExtended wobjXMLRequestSolis) throws XmlDomExtendedException,
			SQLException, OSBConnectorException, MalformedURLException {
		XmlDomExtended mobjXMLDoc;
		String mvarAUX;
		String mvarAUMARCOD;
		String mvarNUMECOLO;
		String mvarESTADCOD;
		String mvarPLANNCOD;
		String mvarFRANQCOD;
		String mvarDOCUMTIP;
		String mvarCOBROTIP;
		String mvarAGENTCOD;
		String mvarCAMPACOD;
		String mvarMARCA;
		String mvarCOLOR;
		String mvarINSPEDES;
		String mvarPLANNDES;
		String mvarFRANQDES;
		String mvarDOCUDES;
		String mvarCOBRODES;
		String mvarAGENTDES;
		String mvarDatosImpreso;
		org.w3c.dom.Node oModelo;
		String mvarRequest;
		String mvarResponse;
		String mvarOrigen;
		String mvarGas;
		String mvarCero;
		String mvarRastreo;
		String mvarZONA;
		String mvarZonaDes;
		String mvarGarage;
		String mvarStros;
		String mvarSEXO;
		String mvarEC;
		String mvarNUMHIJOS;
		String mvarHEC;
		int wvariCounter;
		org.w3c.dom.NodeList wobjXMLList;
		String mvarOPT_IVA;
		String mvarCLIENTIP;
		String mvarOPT_IBB;
		String mvarCUENNUME;
		String mvarRAZONSOC;
		String mvarCUITNUME;
		String mvarCAMPADES = ""; 
		String mvarOPT_MODELOS = "";
		// ejecuta la sp para rescatar literales
		mvarAUX = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_ModeAutCod));
		mvarAUMARCOD = Strings.left(mvarAUX, 5);
		mvarNUMECOLO = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_VEHCLRCOD));
		mvarESTADCOD = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_INSPECOD));
		mvarPLANNCOD = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_PLANCOD));
		mvarFRANQCOD = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_FRANQCOD));
		mvarDOCUMTIP = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_TipoDocu));
		mvarCOBROTIP = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_COBROTIP));
		mvarAGENTCOD = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_PRODUCTOR));
		mvarCAMPACOD = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CampaCod));

		java.sql.Connection jdbcConn = null;

		try {
			com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
			jdbcConn = connFactory.getJDBCConnection(General.mcteDB);
			String sp = "{call " + mcteStoreProc + "(?,?,?,?,?,?,?,?,?)}";
			CallableStatement ps = jdbcConn.prepareCall(sp);
			int posiSP = 1;

			if (mvarAUMARCOD.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarAUMARCOD));
			}

			if (mvarNUMECOLO.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarNUMECOLO));
			}

			if (mvarESTADCOD.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarESTADCOD));
			}

			if (mvarPLANNCOD.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarPLANNCOD));
			}

			ps.setString(posiSP++, String.valueOf(mvarFRANQCOD));

			if (mvarDOCUMTIP.isEmpty()) {
				ps.setNull(posiSP++, Types.NUMERIC);
			} else {
				ps.setInt(posiSP++, Integer.parseInt(mvarDOCUMTIP));
			}

			ps.setString(posiSP++, String.valueOf(mvarCOBROTIP));
			ps.setString(posiSP++, String.valueOf(mvarAGENTCOD));
			ps.setString(posiSP++, String.valueOf(mvarCAMPACOD));

			boolean result = ps.execute();

			if (result) {
				// El primero es un ResultSet
			} else {
				// Salteo el "updateCount"
				int uc = ps.getUpdateCount();
				if (uc == -1) {
					// System.out.println("No hay resultados");
				}
				result = ps.getMoreResults();
			}

			java.sql.ResultSet cursor = ps.getResultSet();
			if (cursor.next()) {
				mvarMARCA = cursor.getString("MARCA");
				mvarCOLOR = cursor.getString("COLOR");
				mvarINSPEDES = cursor.getString("INSPEDES");
				mvarPLANNDES = cursor.getString("PLANNDES");
				mvarFRANQDES = cursor.getString("FRANQDES");
				mvarDOCUDES = cursor.getString("DOCUDES");
				mvarCOBRODES = cursor.getString("COBRODES");
			} else {
				mvarMARCA = "";
				mvarCOLOR = "";
				mvarINSPEDES = "";
				mvarPLANNDES = "";
				mvarFRANQDES = "";
				mvarDOCUDES = "";
				mvarCOBRODES = "";
				mvarCAMPADES = "";
				mvarAGENTDES = "";
			}
		} finally {

			try {
				if (jdbcConn != null)
					jdbcConn.close();
			} catch (Exception e) {
			}
		}

		// busca modelo
		mvarRequest = "<Request><AUMARCOD>" + mvarAUMARCOD + "</AUMARCOD><AUVEHDES></AUVEHDES></Request>";

		logger.log(Level.FINE, "Llamando localmente a lbaw_GetVehiculos");
		lbaw_GetVehiculos getVehiculos = new lbaw_GetVehiculos();
		StringHolder getVehiculosSH = new StringHolder();
		getVehiculos.IAction_Execute(mvarRequest, getVehiculosSH, "");
		mvarResponse = getVehiculosSH.getValue();

		
		mobjXMLDoc = new XmlDomExtended();
		mobjXMLDoc.loadXML(mvarResponse);

		if (mobjXMLDoc.selectNodes("//OPTION").getLength() > 0) {
			for (int noModelo = 0; noModelo < mobjXMLDoc.selectNodes("//OPTION").getLength(); noModelo++) {
				oModelo = mobjXMLDoc.selectNodes("//OPTION").item(noModelo);
				if (XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_ModeAutCod)).equals(
						XmlDomExtended.getText(oModelo.getAttributes().getNamedItem("value")))) {
					mvarOPT_MODELOS = XmlDomExtended.getText(oModelo);
				}
			}
		} else {
			mvarOPT_MODELOS = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_SIFMVEHI_DES));
		}

		mobjXMLDoc = null;

		mvarRequest = "<Request></Request>";
		lbaw_GetTiposIVA wobjGetTiposIVA = new lbaw_GetTiposIVA();
		StringHolder shMvarResponse = new StringHolder(mvarResponse);
		wobjGetTiposIVA.IAction_Execute(mvarRequest, shMvarResponse, "");
		mvarResponse = shMvarResponse.getValue();
		mobjXMLDoc = new XmlDomExtended();
		mobjXMLDoc.loadXML(mvarResponse);

		mvarOPT_IVA = "";
		if (mobjXMLDoc.selectNodes("//OPTION").getLength() > 0) {
			for (int noModelo = 0; noModelo < mobjXMLDoc.selectNodes("//OPTION").getLength(); noModelo++) {
				oModelo = mobjXMLDoc.selectNodes("//OPTION").item(noModelo);
				if (XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CLIENIVA)).equals(
						XmlDomExtended.getText(oModelo.getAttributes().getNamedItem("value")))) {
					mvarOPT_IVA = XmlDomExtended.getText(oModelo);
				}
			}
		}

		mobjXMLDoc = null;

		mvarCLIENTIP = "F";
		if (!(wobjXMLRequestSolis.selectNodes(mcteParam_CLIENTIP) == (org.w3c.dom.NodeList) null)) {
			if (wobjXMLRequestSolis.selectNodes(mcteParam_CLIENTIP).getLength() == 0) {
				mvarCLIENTIP = "F";
			} else {
				if (XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CLIENTIP)).equals("15")) {
					mvarCLIENTIP = "J";
				} else {
					mvarCLIENTIP = "F";
				}
			}
		}
		
		
		mvarOPT_IBB = "";
		if (!XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_IBB)).equals("0")) {
			mvarRequest = "<Request><NUMERO>0</NUMERO></Request>";

			lbaw_GetTipIngBrut wobjGetTipIngBrut = new lbaw_GetTipIngBrut();
			shMvarResponse = new StringHolder(mvarResponse);
			wobjGetTipIngBrut.IAction_Execute(mvarRequest, shMvarResponse, "");
			mvarResponse = shMvarResponse.getValue();

			mobjXMLDoc = new XmlDomExtended();
			mobjXMLDoc.loadXML(mvarResponse);

			if (mobjXMLDoc.selectNodes("//OPTION").getLength() > 0) {
				for (int noModelo = 0; noModelo < mobjXMLDoc.selectNodes("//OPTION").getLength(); noModelo++) {
					oModelo = mobjXMLDoc.selectNodes("//OPTION").item(noModelo);
					if (XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_IBB)).equals(
							XmlDomExtended.getText(oModelo.getAttributes().getNamedItem("value")))) {
						mvarOPT_IBB = XmlDomExtended.getText(oModelo);
					}
				}
			}
		}

		mobjXMLDoc = null;

		// Nombre productor
		mvarRequest = "<Request><DEFINICION>GetNombreProductor.xml</DEFINICION>";
		mvarRequest = mvarRequest + "<USUARCOD></USUARCOD><AGENTCLA>PR</AGENTCLA>";
		mvarRequest = mvarRequest + "<AGENTCOD>" + mvarAGENTCOD + "</AGENTCOD></Request>";

		logger.log(Level.FINE, "Llamando localmente a LBAW_MQMensaje");
		mvarResponse = Utils.getConsultaMQ(mvarRequest);

		mobjXMLDoc = new XmlDomExtended();
		mobjXMLDoc.loadXML(mvarResponse);

		if (!(mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") == (org.w3c.dom.Node) null)) {
			if (XmlDomExtended.getText(mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado")).equals("true")) {
				mvarAGENTDES = XmlDomExtended.getText(mobjXMLDoc.selectSingleNode("//PRODUCTOR/NOMBRE"));
			} else {
				mvarAGENTDES = "";
			}
		} else {
			mvarAGENTDES = "";
		}

		mobjXMLDoc = null;

		// nombre campania
		mvarRequest = "<Request><DEFINICION>CampanasHabilitadasProductor.xml</DEFINICION>";
		// no hace falta el valor del usuarcod, solo informar el campo
		mvarRequest = mvarRequest + "<USUARCOD>AUS1</USUARCOD>";
		mvarRequest = mvarRequest + "<RAMOPCOD>AUS1</RAMOPCOD><AGENTCLA>PR</AGENTCLA>";
		mvarRequest = mvarRequest + "<AGENTCOD>" + mvarAGENTCOD + "</AGENTCOD></Request>";

		mvarResponse = Utils.getConsultaMQ(mvarRequest);
		mobjXMLDoc = new XmlDomExtended();
		mobjXMLDoc.loadXML(mvarResponse);

		if (!(mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") == (org.w3c.dom.Node) null)) {
			if (XmlDomExtended.getText(mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado")).equals("true")) {

				wobjXMLList = mobjXMLDoc.selectNodes("//CAMPOS/T-CAMPANAS-SAL/CAMPANA");
				for (wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++) {
					if (XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), "CAMPACOD")).equals(mvarCAMPACOD)) {
						mvarCAMPADES = XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), "CAMPADES"));
					}
				}
			} else {
				mvarCAMPADES = "";
			}
		} else {
			mvarCAMPADES = "";
		}

		mobjXMLDoc = null;
		wobjXMLList = (org.w3c.dom.NodeList) null;
		// obtiene otros literales
		mvarAUX = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_ModeAutCod));
		mvarAUX = Strings.mid(mvarAUX, 21, 1);
		if (mvarAUX.equals("N")) {
			mvarOrigen = "NACIONAL";
		} else {
			mvarOrigen = "IMPORTADO";
		}

		if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_AUUSOGNC))).equals("S")) {
			mvarGas = "SI";
		} else {
			mvarGas = "NO";
		}

		if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_Escero))).equals("S")) {
			mvarCero = "SI";
		} else {
			mvarCero = "NO";
		}

		if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_INSTALADP))).equals("S")) {
			mvarRastreo = "SOLICITA";
		} else {
			if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_POSEEDISP))).equals("S")) {
				mvarRastreo = "YA POSEE";
			} else {
				mvarRastreo = "";
			}
		}
		mvarRequest = "<Request><RAMOPCOD>AUS1</RAMOPCOD><PROVICOD>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_PROVICOD))
				+ "</PROVICOD><CPACODPO>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_DOMICCPO)) + "</CPACODPO></Request>";
		// Hacemos una llamada local, no es necesario mandarlo por OSB porque es un componente migrado y así facilita el logging
		logger.log(Level.FINE, "Llamando localmente a lbaw_GetZona");
		lbaw_GetZona getZona = new lbaw_GetZona();
		StringHolder getZonaSH = new StringHolder();
		getZona.IAction_Execute(mvarRequest, getZonaSH, "");
		mvarResponse = getZonaSH.getValue();

		mobjXMLDoc = new XmlDomExtended();
		mobjXMLDoc.loadXML(mvarResponse);
		if (XmlDomExtended.getText(mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado")).equals("true")) {
			mvarZONA = XmlDomExtended.getText(mobjXMLDoc.selectSingleNode("//CODIZONA"));
			mvarZonaDes = XmlDomExtended.getText(mobjXMLDoc.selectSingleNode("//ZONASDES"));
		} else {
			mvarZONA = "";
			mvarZonaDes = "";
		}
		mobjXMLDoc = null;
		if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_GUGARAGE))).equals("S")) {
			mvarGarage = "SI";
		} else {
			mvarGarage = "NO";
		}

		if (XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_AUNUMSIN)).equals("0")) {
			mvarStros = "NINGUNO";
		} else {
			mvarStros = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_AUNUMSIN));
		}

		if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CLIENSEX))).equals("M")) {
			mvarSEXO = "MASCULINO";
		} else {
			mvarSEXO = "FEMENINO";
		}

		if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CLIENEST))).equals("S")) {
			mvarEC = "SOLTERO/A";
		} else {
			if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CLIENEST))).equals("C")) {
				mvarEC = "CASADO/A";
			} else {
				if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CLIENEST))).equals("E")) {
					mvarEC = "SEPARADO/A";
				} else {
					if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CLIENEST))).equals("V")) {
						mvarEC = "VIUDO/A";
					} else {
						if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CLIENEST))).equals("D")) {
							mvarEC = "DIVORCIADO/A";
						} else {
							mvarEC = "";
						}
					}
				}
			}
		}

		// conforma xml para la impresión
		mvarDatosImpreso = "<Request>";
		mvarDatosImpreso = mvarDatosImpreso + "<PROCEANN>" + DateTime.year(DateTime.now()) + "</PROCEANN>";
		mvarDatosImpreso = mvarDatosImpreso + "<PROCEMES>" + DateTime.month(DateTime.now()) + "</PROCEMES>";
		mvarDatosImpreso = mvarDatosImpreso + "<PROCEDIA>" + DateTime.day(DateTime.now()) + "</PROCEDIA>";
		mvarDatosImpreso = mvarDatosImpreso + "<POLIZANN>00</POLIZANN>";
		mvarDatosImpreso = mvarDatosImpreso + "<POLIZSEC>1</POLIZSEC>";
		mvarDatosImpreso = mvarDatosImpreso + "<CERTIPOL>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_WDB_IDENTIFICACION_BROKER))
				+ "</CERTIPOL>";
		mvarDatosImpreso = mvarDatosImpreso + "<CERTIANN>0000</CERTIANN>";
		mvarDatosImpreso = mvarDatosImpreso + "<CERTISEC>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CERTISEC)) + "</CERTISEC>";
		mvarDatosImpreso = mvarDatosImpreso + "<VIGENANN>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_VIGENANN)) + "</VIGENANN>";
		mvarDatosImpreso = mvarDatosImpreso + "<VIGENMES>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_VIGENMES)) + "</VIGENMES>";
		mvarDatosImpreso = mvarDatosImpreso + "<VIGENDIA>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_VIGENDIA)) + "</VIGENDIA>";
		mvarDatosImpreso = mvarDatosImpreso + "<CLIENNOM>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_ClienNom1)) + "</CLIENNOM>";
		mvarDatosImpreso = mvarDatosImpreso + "<CLIENAP1>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_ClienApe1)) + "</CLIENAP1>";
		mvarDatosImpreso = mvarDatosImpreso
				+ "<CLIENAP2>"
				+ ((wobjXMLRequestSolis.selectSingleNode(mcteParam_ClienApe2) == (org.w3c.dom.Node) null) ? "" : XmlDomExtended.getText(wobjXMLRequestSolis
						.selectSingleNode(mcteParam_ClienApe2))) + "</CLIENAP2>";
		mvarDatosImpreso = mvarDatosImpreso + "<DOMICDOMCOR>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_DOMICDOM_CO))
				+ "</DOMICDOMCOR>";
		mvarDatosImpreso = mvarDatosImpreso + "<DOMICDNUCOR>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_DomicDnu_CO))
				+ "</DOMICDNUCOR>";
		mvarDatosImpreso = mvarDatosImpreso
				+ "<DOMICPISCOR>"
				+ ((wobjXMLRequestSolis.selectSingleNode(mcteParam_DOMICPIS_CO) == (org.w3c.dom.Node) null) ? "" : XmlDomExtended.getText(wobjXMLRequestSolis
						.selectSingleNode(mcteParam_DOMICPIS_CO))) + "</DOMICPISCOR>";
		mvarDatosImpreso = mvarDatosImpreso
				+ "<DOMICPTACOR>"
				+ ((wobjXMLRequestSolis.selectSingleNode(mcteParam_DomicPta_CO) == (org.w3c.dom.Node) null) ? "" : XmlDomExtended.getText(wobjXMLRequestSolis
						.selectSingleNode(mcteParam_DomicPta_CO))) + "</DOMICPTACOR>";
		mvarDatosImpreso = mvarDatosImpreso + "<DOMICPOBCOR>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_DOMICPOB_CO))
				+ "</DOMICPOBCOR>";
		mvarDatosImpreso = mvarDatosImpreso + "<LOCALIDADCODCOR>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_DOMICCPO_CO))
				+ "</LOCALIDADCODCOR>";
		mvarDatosImpreso = mvarDatosImpreso
				+ "<TELCODCOR>"
				+ ((wobjXMLRequestSolis.selectSingleNode(mcteParam_TELCOD_CO) == (org.w3c.dom.Node) null) ? "" : XmlDomExtended.getText(wobjXMLRequestSolis
						.selectSingleNode(mcteParam_TELCOD_CO))) + "</TELCODCOR>";
		mvarDatosImpreso = mvarDatosImpreso
				+ "<TELNROCOR>"
				+ ((wobjXMLRequestSolis.selectSingleNode(mcteParam_TELNRO_CO) == (org.w3c.dom.Node) null) ? "" : XmlDomExtended.getText(wobjXMLRequestSolis
						.selectSingleNode(mcteParam_TELNRO_CO))) + "</TELNROCOR>";
		mvarDatosImpreso = mvarDatosImpreso
				+ "<EMAIL>"
				+ ((wobjXMLRequestSolis.selectSingleNode(mcteParam_EMAIL) == (org.w3c.dom.Node) null) ? "" : XmlDomExtended.getText(wobjXMLRequestSolis
						.selectSingleNode(mcteParam_EMAIL))) + "</EMAIL>";
		mvarDatosImpreso = mvarDatosImpreso + "<MARCA>" + mvarMARCA + "</MARCA>";
		mvarDatosImpreso = mvarDatosImpreso + "<VEHDES>" + mvarOPT_MODELOS + "</VEHDES>";
		mvarDatosImpreso = mvarDatosImpreso + "<PATENNUM>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_PATENNUM)) + "</PATENNUM>";
		mvarDatosImpreso = mvarDatosImpreso + "<MOTORNUM>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_MOTORNUM)) + "</MOTORNUM>";
		mvarDatosImpreso = mvarDatosImpreso + "<CHASINUM>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CHASINUM)) + "</CHASINUM>";
		mvarDatosImpreso = mvarDatosImpreso + "<EFECTANN>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_EfectAnn)) + "</EFECTANN>";
		mvarDatosImpreso = mvarDatosImpreso + "<SUMASEG>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_SUMAASEG)) + "</SUMASEG>";
		mvarDatosImpreso = mvarDatosImpreso + "<VEHCLRCOD>" + mvarCOLOR + "</VEHCLRCOD>";
		mvarDatosImpreso = mvarDatosImpreso + "<VEHORIGEN>" + mvarOrigen + "</VEHORIGEN>";
		mvarDatosImpreso = mvarDatosImpreso + "<GAS>" + mvarGas + "</GAS>";
		mvarDatosImpreso = mvarDatosImpreso + "<ESCERO>" + mvarCero + "</ESCERO>";
		mvarDatosImpreso = mvarDatosImpreso + "<INSPEDES>" + mvarINSPEDES + "</INSPEDES>";
		mvarDatosImpreso = mvarDatosImpreso + "<RASTREO>" + mvarRastreo + "</RASTREO>";
		mvarDatosImpreso = mvarDatosImpreso + "<PLANNCOD>" + mvarPLANNCOD + "</PLANNCOD>";
		mvarDatosImpreso = mvarDatosImpreso + "<FRANQCOD>" + mvarFRANQCOD + "</FRANQCOD>";
		mvarDatosImpreso = mvarDatosImpreso + "<PLANNDES>" + mvarPLANNDES + "</PLANNDES>";
		mvarDatosImpreso = mvarDatosImpreso + "<FRANQDES>" + mvarFRANQDES + "</FRANQDES>";
		mvarDatosImpreso = mvarDatosImpreso + "<ZONASDES>" + mvarZonaDes + "</ZONASDES>";
		mvarDatosImpreso = mvarDatosImpreso + "<ZONATARI>" + mvarZONA + "</ZONATARI>";
		mvarDatosImpreso = mvarDatosImpreso + "<KMSRNGCOD>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CTAKMS)) + "</KMSRNGCOD>";
		mvarDatosImpreso = mvarDatosImpreso + "<CPAANO>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CPAANO)) + "</CPAANO>";
		mvarDatosImpreso = mvarDatosImpreso + "<SIGARAGE>" + mvarGarage + "</SIGARAGE>";
		mvarDatosImpreso = mvarDatosImpreso + "<SINIESTROS>" + mvarStros + "</SINIESTROS>";
		mvarDatosImpreso = mvarDatosImpreso + "<NACIMANN>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_NacimAnn)) + "</NACIMANN>";
		mvarDatosImpreso = mvarDatosImpreso + "<NACIMMES>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_NacimMes)) + "</NACIMMES>";
		mvarDatosImpreso = mvarDatosImpreso + "<NACIMDIA>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_NacimDia)) + "</NACIMDIA>";
		mvarDatosImpreso = mvarDatosImpreso + "<TIPODOCU>" + mvarDOCUDES + "</TIPODOCU>";
		mvarDatosImpreso = mvarDatosImpreso + "<NUMEDOCU>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_NumeDocu)) + "</NUMEDOCU>";
		mvarDatosImpreso = mvarDatosImpreso
				+ "<EDAD>"
				+ DateTime.diff(
						"YYYY",
						DateTime.dateSerial(Obj.toInt(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_NacimAnn))),
								Obj.toInt(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_NacimMes))),
								Obj.toInt(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_NacimDia)))), DateTime.now()) + "</EDAD>";
		mvarDatosImpreso = mvarDatosImpreso + "<SEXO>" + mvarSEXO + "</SEXO>";
		mvarDatosImpreso = mvarDatosImpreso + "<ESTADO>" + mvarEC + "</ESTADO>";
		mvarDatosImpreso = mvarDatosImpreso + "<AUANTANN>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_AUANTANN)) + "</AUANTANN>";
		mvarDatosImpreso = mvarDatosImpreso + "<HIJOS>";

		// ciclo de hijos
		if (!(wobjXMLRequestSolis.selectNodes(mcteParam_CONDUCTORES) == (org.w3c.dom.NodeList) null)) {
			if (wobjXMLRequestSolis.selectNodes(mcteParam_CONDUCTORES).getLength() > 0) {
				mvarNUMHIJOS = String.valueOf(wobjXMLRequestSolis.selectNodes(mcteParam_CONDUCTORES).getLength());
				wobjXMLList = wobjXMLRequestSolis.selectNodes(mcteParam_CONDUCTORES);
				for (wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++) {
					mvarDatosImpreso = mvarDatosImpreso + "<HIJO>";
					mvarDatosImpreso = mvarDatosImpreso + "<CANTHIJOS>" + mvarNUMHIJOS + "</CANTHIJOS>";
					mvarDatosImpreso = mvarDatosImpreso
							+ "<NACIMHIJOAA>"
							+ Strings.mid(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CONDUFEC)), 5,
									4) + "</NACIMHIJOAA>";
					mvarDatosImpreso = mvarDatosImpreso
							+ "<NACIMHIJOMM>"
							+ Strings.mid(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CONDUFEC)), 3,
									2) + "</NACIMHIJOMM>";
					mvarDatosImpreso = mvarDatosImpreso
							+ "<NACIMHIJODD>"
							+ Strings.mid(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CONDUFEC)), 1,
									2) + "</NACIMHIJODD>";
					if (Strings.toUpperCase(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CONDUEST)))
							.equals("S")) {
						mvarHEC = "SOLTERO/A";
					} else {
						if (Strings.toUpperCase(
								XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CONDUEST))).equals("C")) {
							mvarHEC = "CASADO/A";
						} else {
							if (Strings.toUpperCase(
									XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CONDUEST))).equals(
									"E")) {
								mvarHEC = "SEPARADO/A";
							} else {
								if (Strings.toUpperCase(
										XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CONDUEST)))
										.equals("V")) {
									mvarHEC = "VIUDO/A";
								} else {
									if (Strings.toUpperCase(
											XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CONDUEST)))
											.equals("D")) {
										mvarHEC = "DIVORCIADO/A";
									} else {
										mvarHEC = "";
									}
								}
							}
						}
					}
					mvarDatosImpreso = mvarDatosImpreso + "<ESTADOHIJO>" + mvarHEC + "</ESTADOHIJO>";
					mvarDatosImpreso = mvarDatosImpreso + "<APELLIDOHIJO>"
							+ XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CONDUAPE))
							+ "</APELLIDOHIJO>";
					mvarDatosImpreso = mvarDatosImpreso + "<NOMBREHIJO>"
							+ XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CONDUNOM))
							+ "</NOMBREHIJO>";

					mvarDatosImpreso = mvarDatosImpreso + "</HIJO>";
				}
			} else {
				mvarDatosImpreso = mvarDatosImpreso + "<HIJO>";
				mvarDatosImpreso = mvarDatosImpreso + "<CANTHIJOS>0</CANTHIJOS>";
				mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJOAA></NACIMHIJOAA>";
				mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJOMM></NACIMHIJOMM>";
				mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJODD></NACIMHIJODD>";
				mvarDatosImpreso = mvarDatosImpreso + "<ESTADOHIJO></ESTADOHIJO>";
				mvarDatosImpreso = mvarDatosImpreso + "<APELLIDOHIJO></APELLIDOHIJO>";
				mvarDatosImpreso = mvarDatosImpreso + "<NOMBREHIJO></NOMBREHIJO>";
				mvarDatosImpreso = mvarDatosImpreso + "</HIJO>";
			}
		} else {
			mvarDatosImpreso = mvarDatosImpreso + "<HIJO>";
			mvarDatosImpreso = mvarDatosImpreso + "<CANTHIJOS>0</CANTHIJOS>";
			mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJOAA></NACIMHIJOAA>";
			mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJOMM></NACIMHIJOMM>";
			mvarDatosImpreso = mvarDatosImpreso + "<NACIMHIJODD></NACIMHIJODD>";
			mvarDatosImpreso = mvarDatosImpreso + "<ESTADOHIJO></ESTADOHIJO>";
			mvarDatosImpreso = mvarDatosImpreso + "<APELLIDOHIJO></APELLIDOHIJO>";
			mvarDatosImpreso = mvarDatosImpreso + "<NOMBREHIJO></NOMBREHIJO>";
			mvarDatosImpreso = mvarDatosImpreso + "</HIJO>";
		}
		mvarDatosImpreso = mvarDatosImpreso + "</HIJOS>";

		// ciclo de accesorios
		mvarDatosImpreso = mvarDatosImpreso + "<ACCESORIOS>";
		if (!(wobjXMLRequestSolis.selectNodes(mcteParam_ACCESORIOS) == (org.w3c.dom.NodeList) null)) {
			if (wobjXMLRequestSolis.selectNodes(mcteParam_ACCESORIOS).getLength() > 0) {
				wobjXMLList = wobjXMLRequestSolis.selectNodes(mcteParam_ACCESORIOS);
				for (wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++) {
					mvarDatosImpreso = mvarDatosImpreso + "<ACCESORIO>";
					mvarDatosImpreso = mvarDatosImpreso + "<PRECIOACC>"
							+ XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_AUVEASUM)) + "</PRECIOACC>";
					mvarDatosImpreso = mvarDatosImpreso + "<CODIGOACC>"
							+ XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_AUACCCOD)) + "</CODIGOACC>";
					mvarDatosImpreso = mvarDatosImpreso + "<DESCRIPCIONACC>"
							+ XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_AUVEADES))
							+ "</DESCRIPCIONACC>";
					mvarDatosImpreso = mvarDatosImpreso + "</ACCESORIO>";
				}
				mvarDatosImpreso = mvarDatosImpreso + "</ACCESORIOS>";
			} else {
				mvarDatosImpreso = mvarDatosImpreso + "<ACCESORIO>";
				mvarDatosImpreso = mvarDatosImpreso + "<PRECIOACC></PRECIOACC>";
				mvarDatosImpreso = mvarDatosImpreso + "<CODIGOACC></CODIGOACC>";
				mvarDatosImpreso = mvarDatosImpreso + "<DESCRIPCIONACC></DESCRIPCIONACC>";
				mvarDatosImpreso = mvarDatosImpreso + "</ACCESORIO>";
				mvarDatosImpreso = mvarDatosImpreso + "</ACCESORIOS>";
			}
		} else {
			mvarDatosImpreso = mvarDatosImpreso + "<ACCESORIO>";
			mvarDatosImpreso = mvarDatosImpreso + "<PRECIOACC></PRECIOACC>";
			mvarDatosImpreso = mvarDatosImpreso + "<CODIGOACC></CODIGOACC>";
			mvarDatosImpreso = mvarDatosImpreso + "<DESCRIPCIONACC></DESCRIPCIONACC>";
			mvarDatosImpreso = mvarDatosImpreso + "</ACCESORIO>";
			mvarDatosImpreso = mvarDatosImpreso + "</ACCESORIOS>";
		}

		// fin accesorios
		mvarDatosImpreso = mvarDatosImpreso
				+ "<PRECIO>"
				+ ((wobjXMLRequestSolis.selectSingleNode(mcteParam_PRECIO) == (org.w3c.dom.Node) null) ? "" : XmlDomExtended.getText(wobjXMLRequestSolis
						.selectSingleNode(mcteParam_PRECIO))) + "</PRECIO>";
		if (Obj.toInt(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_COBROCOD))) == 4) {
			mvarDatosImpreso = mvarDatosImpreso + "<COBROFOR>TARJETA " + mvarCOBRODES + "</COBROFOR>";
		} else {
			mvarDatosImpreso = mvarDatosImpreso + "<COBROFOR>" + mvarCOBRODES + "</COBROFOR>";
		}

		// mvarDatosImpreso = mvarDatosImpreso & "<CUENNUME>" &
		// wobjXMLRequestSolis.selectSingleNode(mcteParam_CUENNUME).Text &
		// "</CUENNUME>"
		// cbu
		if (Obj.toInt(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_COBROCOD))) == 5) {
			if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_COBROTIP))).equals("DB")) {
				mvarDatosImpreso = mvarDatosImpreso
						+ "<CUENNUME>"
						+ ((wobjXMLRequestSolis.selectSingleNode(mcteParam_CUENNUME) == (org.w3c.dom.Node) null) ? "" : XmlDomExtended
								.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CUENNUME)))
						+ ((wobjXMLRequestSolis.selectSingleNode(mcteParam_VENCIMES) == (org.w3c.dom.Node) null) ? "" : XmlDomExtended
								.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_VENCIMES)))
						+ ((wobjXMLRequestSolis.selectSingleNode(mcteParam_VENCIANN) == (org.w3c.dom.Node) null) ? "" : XmlDomExtended
								.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_VENCIANN))) + "</CUENNUME>";
			} else {
				mvarDatosImpreso = mvarDatosImpreso
						+ "<CUENNUME>"
						+ ((wobjXMLRequestSolis.selectSingleNode(mcteParam_CUENNUME) == (org.w3c.dom.Node) null) ? "" : XmlDomExtended
								.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CUENNUME))) + "</CUENNUME>";
			}
		} else {
			mvarCUENNUME = "";
			if (!(wobjXMLRequestSolis.selectSingleNode(mcteParam_CUENNUME) == (org.w3c.dom.Node) null)) {
				mvarCUENNUME = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CUENNUME));
			}
			mvarDatosImpreso = mvarDatosImpreso + "<CUENNUME>" + mvarCUENNUME + "</CUENNUME>";
		}
		if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CLUB_LBA))).equals("S")) {
			mvarDatosImpreso = mvarDatosImpreso + "<CLUBLBA>SI</CLUBLBA>";
		} else {
			mvarDatosImpreso = mvarDatosImpreso + "<CLUBLBA>NO</CLUBLBA>";
		}
		if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_DESTRUCCION_80))).equals("S")) {
			mvarDatosImpreso = mvarDatosImpreso + "<DESTRUCCION_80>SI</DESTRUCCION_80>";
		} else {
			mvarDatosImpreso = mvarDatosImpreso + "<DESTRUCCION_80>NO</DESTRUCCION_80>";
		}
		if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_Luneta))).equals("S")) {
			mvarDatosImpreso = mvarDatosImpreso + "<LUNETA>SI</LUNETA>";
		} else {
			mvarDatosImpreso = mvarDatosImpreso + "<LUNETA>NO</LUNETA>";
		}
		if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_ClubEco))).equals("S")) {
			mvarDatosImpreso = mvarDatosImpreso + "<CLUBECO>SI</CLUBECO>";
		} else {
			mvarDatosImpreso = mvarDatosImpreso + "<CLUBECO>NO</CLUBECO>";
		}
		if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_Robocont))).equals("S")) {
			mvarDatosImpreso = mvarDatosImpreso + "<ROBOCONT>SI</ROBOCONT>";
		} else {
			mvarDatosImpreso = mvarDatosImpreso + "<ROBOCONT>NO</ROBOCONT>";
		}
		if (Strings.toUpperCase(XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_Granizo))).equals("S")) {
			mvarDatosImpreso = mvarDatosImpreso + "<GRANIZO>SI</GRANIZO>";
		} else {
			mvarDatosImpreso = mvarDatosImpreso + "<GRANIZO>NO</GRANIZO>";
		}
		mvarDatosImpreso = mvarDatosImpreso + "<AGECOD>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_PRODUCTOR)) + "</AGECOD>";
		mvarDatosImpreso = mvarDatosImpreso + "<AGEDES>" + mvarAGENTDES + "</AGEDES>";
		mvarDatosImpreso = mvarDatosImpreso + "<CAMPACOD>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CampaCod)) + "</CAMPACOD>";
		mvarDatosImpreso = mvarDatosImpreso + "<CAMPADES>" + mvarCAMPADES + "</CAMPADES>";
		// 11/2010 se aceptan distintos IVA
		mvarDatosImpreso = mvarDatosImpreso + "<IVA>" + XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CLIENIVA)) + "</IVA>";
		mvarDatosImpreso = mvarDatosImpreso + "<DESCIVA>" + mvarOPT_IVA + "</DESCIVA>";
		mvarRAZONSOC = "";
		if (!(wobjXMLRequestSolis.selectSingleNode(mcteParam_RAZONSOC) == (org.w3c.dom.Node) null)) {
			mvarRAZONSOC = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_RAZONSOC));
		}
		mvarDatosImpreso = mvarDatosImpreso + "<RAZONSOC>" + mvarRAZONSOC + "</RAZONSOC>";
		mvarCUITNUME = "";
		if (!(wobjXMLRequestSolis.selectSingleNode(mcteParam_CUITNUME) == (org.w3c.dom.Node) null)) {
			mvarCUITNUME = XmlDomExtended.getText(wobjXMLRequestSolis.selectSingleNode(mcteParam_CUITNUME));
		}
		mvarDatosImpreso = mvarDatosImpreso + "<CUITNUME>" + mvarCUITNUME + "</CUITNUME>";
		mvarDatosImpreso = mvarDatosImpreso + "<DESCIBB>" + mvarOPT_IBB + "</DESCIBB>";
		mvarDatosImpreso = mvarDatosImpreso
				+ "<NROIBB>"
				+ ((wobjXMLRequestSolis.selectSingleNode(mcteParam_NROIBB) == (org.w3c.dom.Node) null) ? "" : XmlDomExtended.getText(wobjXMLRequestSolis
						.selectSingleNode(mcteParam_NROIBB))) + "</NROIBB>";

		mvarDatosImpreso = mvarDatosImpreso + "<CLIENTIP>" + mvarCLIENTIP + "</CLIENTIP>";
		// fin iva
		mvarDatosImpreso = mvarDatosImpreso + "</Request>";
		return mvarDatosImpreso;
	}

	public void Activate() throws Exception {
	}

	public boolean CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		//
		ObjectControl_CanBePooled = true;
		//
		return ObjectControl_CanBePooled;
	}

	public void Deactivate() throws Exception {
	}
}
