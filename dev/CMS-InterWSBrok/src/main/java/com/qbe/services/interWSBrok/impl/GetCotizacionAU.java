package com.qbe.services.interWSBrok.impl;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.webmq.impl.lbaw_GetZona;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

import diamondedge.util.DateTime;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;

/**
 * 
 * Componente público, usado por los brokers
 * 
 * TODO Definir interface
 * 
 * En el uso de los WS de cotización y solicitud , existe un orden que deben seguir las invocaciones 
 * de las cotizaciones y solicitudes:
 * 
 * Cada broker por cada requerimiento, puede llamar varias cotizaciones, una solicitud
 * solo luego de una cotización “OK” y solo puede llamar otra solicitud si la primera fue con error, 
 * una vez que se grabó una solicitud  OK no se puede enviar más ese nro de requerimiento. 
 * ( a esto se refiere la "última operación" u "operación anterior" a las invocaciones con igual requerimiento)
 * 
 * TODO Esta clase se podría emprolijar como hicimos con GetSolicitudAU
 * 
 * @author ramiro
 *
 */
public class GetCotizacionAU extends BaseOSBClient implements VBObjectClass
{
	
	  protected static Logger logger = Logger.getLogger( GetCotizacionAU.class.getName());

  static final String mcteClassName = "LBA_InterWSBrok.GetCotizacionAU";
  static final String mcteStoreProcAltaOperacion = "SPSNCV_BRO_ALTA_OPER";
  static final String mcteStoreProcInsertCot = "SPSNCV_BRO_COTI_AUS1_GRABA";
  static final String mcteArchivoAUSCOT_XML = "LBA_VALIDACION_COT_AU.xml";
  
  /**
   * jc 08/2010 se agrega xsl de formateo a la nueva salida del ws
   */
  static final String mcteFormatoAUSCOT_XSL = "LBA_FORMAT_COT_AU.xsl";
  static final String mcteParam_WDB_IDENTIFICACION_BROKER = "//CODINST";
  static final String mcteParam_WDB_NRO_OPERACION_BROKER = "//REQUESTID";

  static final String mcteParam_WDB_NRO_COTIZACION_LBA = "//COT_NRO";
  static final String mcteParam_WDB_TIPO_OPERACION = "//TIPOOPERACION";
  static final String mcteParam_MSGANO = "//MSGANO";
  static final String mcteParam_MSGMES = "//MSGMES";
  static final String mcteParam_MSGDIA = "//MSGDIA";
  static final String mcteParam_MSGHORA = "//MSGHORA";
  static final String mcteParam_MSGMINUTO = "//MSGMINUTO";
  static final String mcteParam_MSGSEGUNDO = "//MSGSEGUNDO";
  static final String mcteParam_CODINST = "//CODINST";
  static final String mcteParam_Cliensec = "CERTISEC";
  static final String mcteParam_NacimAnn = "NACIMANN";
  static final String mcteParam_NacimMes = "NACIMMES";
  static final String mcteParam_NacimDia = "NACIMDIA";
  static final String mcteParam_Sexo = "SEXO";
  static final String mcteParam_Estado = "ESTADO";

  static final String mcteParam_CLIENIVA = "IVA";
  static final String mcteParam_ModeAutCod = "MODEAUTCOD";
  static final String mcteParam_KMsrngCod = "KMSRNGCOD";
  static final String mcteParam_EfectAnn = "EFECTANN";
  static final String mcteParam_SiGarage = "SIGARAGE";
  static final String mcteParam_SumAseg = "SUMAASEG";
  static final String mcteParam_Siniestros = "SINIESTROS";
  static final String mcteParam_Gas = "GAS";
  static final String mcteParam_Provi = "PROVI";
  static final String mcteParam_LocalidadCod = "LOCALIDADCOD";

  static final String mcteParam_CampaCod = "CAMPACOD";

  static final String mcteParam_DatosPlan = "DATOSPLAN";
  static final String mcteParam_ClubLBA = "CLUBLBA";

  static final String mcteParam_DESTRUCCION_80 = "DESTRUCCION_80";
  static final String mcteParam_Luneta = "LUNETA";
  static final String mcteParam_ClubEco = "CLUBECO";
  static final String mcteParam_Robocont = "ROBOCONT";
  static final String mcteParam_Granizo = "GRANIZO";
  static final String mcteParam_IBB = "IBB";
  static final String mcteParam_Escero = "ESCERO";

  static final String mcteParam_CLIENTIP = "CLIENTIP";

  static final String mcteParam_Portal = "PORTAL";

  static final String mcteParam_PRODUCTOR = "AGECOD";
  static final String mcteParam_COBROCOD = "COBROCOD";
  static final String mcteParam_COBROTIP = "COBROTIP";

  static final String mcteParam_NROCOT = "//COT_NRO";
  static final String mcteParam_SumaMin = "//SUMA_MINIMA";
  static final String mcteParam_SumaMax_GBA = "//SUMA_MAXIMA_GBA";
  static final String mcteParam_SumaMax_INT = "//SUMA_MAXIMA_INT";
  static final String mcteParam_Control_GNC_GBA = "//CONTROL_GNC_GBA";
  static final String mcteParam_Control_GNC_INT = "//CONTROL_GNC_INT";
  static final String mcteParam_Permite_GNC = "//PERMITE_GNC";
  static final String mcteParam_FRANQCOD = "//FRANQCOD";
  static final String mcteParam_PLANCOD = "//PLANCOD";
  static final String mcteParam_PLANDES = "//PLANDES";
  static final String mcteParam_CODZONA = "//CODZONA";
  /**
   *  DATOS DE LOS HIJOS
   */
  static final String mcteNodos_Hijos = "//Request/HIJOS/HIJO";
  static final String mcteParam_NacimHijo = "NACIMHIJO";
  static final String mcteParam_SexoHijo = "SEXOHIJO";
  static final String mcteParam_EstadoHijo = "ESTADOHIJO";
  static final String mcteParam_EdadHijo = "EDADHIJO";
  /**
   *  DATOS DE LOS ACCESORIOS
   */
  static final String mcteNodos_Accesorios = "//Request/ACCESORIOS/ACCESORIO";
  static final String mcteParam_PrecioAcc = "PRECIOACC";
  static final String mcteParam_DescripcionAcc = "DESCRIPCIONACC";
  static final String mcteParam_CodigoAcc = "CODIGOACC";
  /**
   * Datos del mensaje
   */
  static final String mcteParam_REQUESTID = "//REQUESTID";
  static final String mcteParam_VEHDES = "//VEHDES";
  static final String mcteParm_TIEMPO = "//TIEMPO";
  
  private EventLog mobjEventLog = new EventLog();

  private final String wcteFnName = "fncGrabaCotizacion";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    int wvarStep = 0;
    String wvarMensaje = "";
    StringHolder pvarRes = new StringHolder();
    
    try 
    {
      wvarStep = 10;
      pvarRequest = Strings.mid( pvarRequest, 1, Strings.find( 1, pvarRequest, ">" ) ) + 
    		  "<MSGANO>" + DateTime.year( DateTime.now() ) + "</MSGANO>" + 
    		  "<MSGMES>" + DateTime.month( DateTime.now() ) + "</MSGMES>" + 
    		  "<MSGDIA>" + DateTime.day( DateTime.now() ) + "</MSGDIA>" + 
    		  "<MSGHORA>" + DateTime.hour( DateTime.now() ) + "</MSGHORA>" + 
    		  "<MSGMINUTO>" + DateTime.minute( DateTime.now() ) + "</MSGMINUTO>" + 
    		  "<MSGSEGUNDO>" + DateTime.second( DateTime.now() ) + "</MSGSEGUNDO>" + 
    		  Strings.mid( pvarRequest, (Strings.find( 1, pvarRequest, ">" ) + 1) );

      pvarRes.set( "" );

		boolean fncResult = fncGetAll(pvarRequest, wvarMensaje, pvarRes);
		pvarResponse.set("<Response_WS>" + pvarRes.getValue() + "</Response_WS>");
		String loggedResponse = Utils.formatLogResponseChompPDF64(pvarResponse);
		logger.log(Level.FINEST, this.getClass().getName() + " response es: " + loggedResponse);

		IAction_Execute = 0;
		return IAction_Execute;

	} catch (Exception _e_) {
		logger.log(Level.SEVERE, this.getClass().getName(), _e_);
		IAction_Execute = 1;
		// Logueo el timestamp para que si el usuario reporta el error
		// podamos usarlo para buscar en los logs
		String resultResponse = Utils.createLBA_WS_XML(-1000, "Error inesperado [" + _e_.getMessage() + "] - TS-" + System.currentTimeMillis() + " CI - " + pvarContextInfo);
		pvarResponse.set("<Response_WS>" + resultResponse + "</Response_WS>");
	}
    return IAction_Execute;
  }

  private boolean fncGetAll( String pvarRequest, String wvarMensaje, StringHolder pvarRes ) throws OSBConnectorException, XmlDomExtendedException, ParseException, IOException, SQLException
  {
    boolean fncGetAll = false;
    int wvarStep = 0;
    String wvarMensaje2 = "";
    XmlDomExtended wobjXMLRequestValid = null;
    XmlDomExtended wobjXMLRequestCotis = null;
    XmlDomExtended wobjXMLError = null;
    XmlDomExtended mobjLocXSL = null;
    String mvarWDB_IDENTIFICACION_BROKER = "";
    String mvarWDB_NRO_OPERACION_BROKER = "";
    String mvarWDB_NRO_COTIZACION_LBA = "";
    String mvarWDB_TIPO_OPERACION = "";
    String mvarWDB_ESTADO = "";
    String mvarWDB_CERTIPOL = "";
    Variant mvarWDB_CERTISEC = new Variant();
    String mvarWDB_RECEPCION_PEDIDOANO = "";
    String mvarWDB_RECEPCION_PEDIDOMES = "";
    String mvarWDB_RECEPCION_PEDIDODIA = "";
    String mvarWDB_RECEPCION_PEDIDOHORA = "";
    String mvarWDB_RECEPCION_PEDIDOMINUTO = "";
    String mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
    String mvarWDB_ENVIO_RESPUESTA = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_TIEMPO_PROCESO_AIS = "";
    String mvarWDB_OPERACION_XML = "";
    String mvarInicioAIS = "";
    String mvarFinAIS = "";
    String mvarTiempoAIS = "";

      //Llama a validacion
      wvarStep = 10;

      GetValidacionCotAU getValidacionCotAU = new GetValidacionCotAU();
      getValidacionCotAU.setOsbConnector(this.getOsbConnector());
      StringHolder shwvarMensaje2 = new StringHolder(wvarMensaje2);
      getValidacionCotAU.IAction_Execute(pvarRequest, shwvarMensaje2, "");
      wvarMensaje2 = shwvarMensaje2.getValue();

      //Analizo la respuesta del validador
      wvarStep = 20;
      wobjXMLRequestValid = new XmlDomExtended();
      wobjXMLRequestValid.loadXML( wvarMensaje2 );

      wobjXMLRequestCotis = new XmlDomExtended();

      mvarWDB_ESTADO = XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( "//Response/Estado/@resultado" )  );
      //Si viene con errores, no continuo con la cotizacion. Grabo el error y muestro
      //el resultado
      wvarStep = 30;
      if( ! (Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( "//Response/Estado/@resultado" )  ) ).equals( "TRUE" )) )
      {
        pvarRes.set(Utils.createLBA_WS_XML(XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( "//Response/Estado/@resultado" )  ), XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( "//Response/Estado/@mensaje" ))));
        wobjXMLRequestValid = new XmlDomExtended();
        wobjXMLRequestValid.loadXML( pvarRequest );
      }
      else
      {
        //Llama al cotizador
        wvarStep = 40;
        mvarInicioAIS = DateTime.format( DateTime.now() );

        String requestGetCotiAUS = wvarMensaje2.substring(wvarMensaje2.indexOf("<Request>"), wvarMensaje2.indexOf("</Request>") + 10);
        wvarMensaje = getOsbConnector().executeRequest("lbaw_OVGetCotiAUS", requestGetCotiAUS);
        mvarFinAIS = DateTime.format( DateTime.now() );

        wobjXMLRequestCotis.loadXML( wvarMensaje );

        mvarTiempoAIS = String.valueOf( DateTime.diff( "s", DateTime.toDate( mvarInicioAIS ), DateTime.toDate( mvarFinAIS ) ) );

        mvarWDB_ESTADO = XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( "//Response/Estado/@resultado" )  );
        //Controlo si no hubo ningun error cuando fue a buscar la cotizacion al MQ
        wvarStep = 50;

        if( XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
        {
          if ( Strings.left( XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( "//Response/Estado/@mensaje" )), 8 ).equals( "Consulta" ))
        	  pvarRes.set(Utils.createLBA_WS_XML(-301, XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( "//Response/Estado/@mensaje" ))));
        	  
        	  pvarRes.set(Utils.createLBA_WS_XML(-300, XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( "//Response/Estado/@mensaje" ))));
        }
        else
        {
          //La Cotizacion esta OK
          //Grabo la cotizacion en la base de datos de SQL
          //si falla la grabacion la respuesta se la envio igual al broker
          wvarStep = 70;
          if( wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_TIPO_OPERACION )  == null )
          {
            if( !fncGrabaCotizacion(wvarMensaje2, wvarMensaje, mvarWDB_CERTISEC) )
            {
              pvarRes.set(Utils.createLBA_WS_XML(-552, "No se grabo correctamente el registro"));
              mvarWDB_ESTADO = "ER";
            }
            else
            {
              XmlDomExtended.setText( wobjXMLRequestValid.selectSingleNode( "//" + mcteParam_Cliensec ) , mvarWDB_CERTISEC.toString() );
              wobjXMLRequestCotis.selectSingleNode( "//Response" ).appendChild( wobjXMLRequestCotis.getDocument().createElement( "REQUESTID" ) );
              XmlDomExtended.setText( wobjXMLRequestCotis.selectSingleNode( "//REQUESTID" ) , XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_NRO_OPERACION_BROKER )  ) );

              wobjXMLRequestCotis.selectSingleNode( "//Response" ).appendChild( wobjXMLRequestCotis.getDocument().createElement( "VEHDES" ) );
              XmlDomExtended.setText( wobjXMLRequestCotis.selectSingleNode( "//VEHDES" ) , XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( mcteParam_VEHDES )  ) );

              mobjLocXSL = new XmlDomExtended();
              mobjLocXSL.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteFormatoAUSCOT_XSL));
              wobjXMLRequestCotis.loadXML(wobjXMLRequestCotis.transformNode( mobjLocXSL ) );
              pvarRes.set(XmlDomExtended.prettyPrint(wobjXMLRequestCotis.getDocument().getDocumentElement()));
            }
          }
        }
      }
      //Graba todas las Operaciones incluyendo las que vienen con error
      wvarStep = 80;
      if( wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_TIPO_OPERACION )  == (org.w3c.dom.Node) null )
      {
        //Inicializa las variables por el caso de que venga con error
        wvarStep = 90;
        mvarWDB_IDENTIFICACION_BROKER = "0";
        if(wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_IDENTIFICACION_BROKER )  != null)
        {
          mvarWDB_IDENTIFICACION_BROKER = XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_IDENTIFICACION_BROKER )  );
        }
        wvarStep = 100;
        mvarWDB_NRO_OPERACION_BROKER = "0";
        if( wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_NRO_OPERACION_BROKER )  != null)
        {
          mvarWDB_NRO_OPERACION_BROKER = XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_NRO_OPERACION_BROKER )  );
        }
        wvarStep = 110;
        mvarWDB_NRO_COTIZACION_LBA = "0";

        if(wobjXMLRequestCotis.selectSingleNode( mcteParam_WDB_NRO_COTIZACION_LBA )  != null)
        {
          mvarWDB_NRO_COTIZACION_LBA = XmlDomExtended.getText( wobjXMLRequestCotis.selectSingleNode( mcteParam_WDB_NRO_COTIZACION_LBA )  );
        }
        wvarStep = 120;
        mvarWDB_TIPO_OPERACION = "C";
        if(wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_TIPO_OPERACION )  != null)
        {
          mvarWDB_TIPO_OPERACION = "S";
        }
        wvarStep = 130;
        mvarWDB_CERTIPOL = "0";
        if(wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_IDENTIFICACION_BROKER )  != null)
        {
          mvarWDB_CERTIPOL = XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( mcteParam_WDB_IDENTIFICACION_BROKER )  );
        }
        wvarStep = 140;
        mvarWDB_CERTISEC.set( 0 );
        if(wobjXMLRequestValid.selectSingleNode("//" + mcteParam_Cliensec) != null)
        {
          mvarWDB_CERTISEC.set( XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( "//" + mcteParam_Cliensec )  ) );
        }
        wvarStep = 150;
        mvarWDB_RECEPCION_PEDIDOANO = XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( mcteParam_MSGANO )  );
        mvarWDB_RECEPCION_PEDIDOMES = XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( mcteParam_MSGMES )  );
        mvarWDB_RECEPCION_PEDIDODIA = XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( mcteParam_MSGDIA )  );
        mvarWDB_RECEPCION_PEDIDOHORA = XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( mcteParam_MSGHORA )  );
        mvarWDB_RECEPCION_PEDIDOMINUTO = XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( mcteParam_MSGMINUTO )  );
        mvarWDB_RECEPCION_PEDIDOSEGUNDO = XmlDomExtended.getText( wobjXMLRequestValid.selectSingleNode( mcteParam_MSGSEGUNDO )  );
        mvarWDB_TIEMPO_PROCESO_AIS = "0";

        wvarStep = 160;

        if( mvarWDB_ESTADO.equals( "true" ) )
        {
          mvarWDB_ESTADO = "OK";
        }
        else
        {
          mvarWDB_ESTADO = "ER";
        }
        //
        mvarWDB_OPERACION_XML = pvarRequest;
        wvarStep = 170;
        //
        wobjXMLRequestCotis = null;
        
		java.sql.Connection jdbcConn = null;
		try {
			JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
			jdbcConn = connFactory.getJDBCConnection(General.mcteDB);

	        String sp = "{call SPSNCV_BRO_ALTA_OPER(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	        CallableStatement ps = jdbcConn.prepareCall(sp);
	        int posiSP = 1;
	        wvarStep = 180;
	        ps.setInt(posiSP++, Integer.parseInt(mvarWDB_IDENTIFICACION_BROKER));
	        wvarStep = 190;
	        ps.setBigDecimal(posiSP++, new BigDecimal(mvarWDB_NRO_OPERACION_BROKER));
	        wvarStep = 200;
	        ps.setInt(posiSP++, Integer.parseInt(mvarWDB_NRO_COTIZACION_LBA));
	        wvarStep = 210;
	        ps.setString(posiSP++, String.valueOf(mvarWDB_TIPO_OPERACION));
	        wvarStep = 220;
	        ps.setString(posiSP++, String.valueOf(mvarWDB_ESTADO));
	        wvarStep = 230;
	
	        mvarWDB_RECEPCION_PEDIDOANO = Strings.right( Strings.fill( 4, "0" ) + mvarWDB_RECEPCION_PEDIDOANO, 4 );
	        mvarWDB_RECEPCION_PEDIDOMES = Strings.right( Strings.fill( 2, "0" ) + mvarWDB_RECEPCION_PEDIDOMES, 2 );
	        mvarWDB_RECEPCION_PEDIDODIA = Strings.right( Strings.fill( 2, "0" ) + mvarWDB_RECEPCION_PEDIDODIA, 2 );
	        //
	        mvarWDB_RECEPCION_PEDIDOHORA = Strings.right( Strings.fill( 2, "0" ) + mvarWDB_RECEPCION_PEDIDOHORA, 2 );
	        mvarWDB_RECEPCION_PEDIDOMINUTO = Strings.right( Strings.fill( 2, "0" ) + mvarWDB_RECEPCION_PEDIDOMINUTO, 2 );
	        mvarWDB_RECEPCION_PEDIDOSEGUNDO = Strings.right( Strings.fill( 2, "0" ) + mvarWDB_RECEPCION_PEDIDOSEGUNDO, 2 );
	        ps.setString(posiSP++,  new String( mvarWDB_RECEPCION_PEDIDOANO + "-" + mvarWDB_RECEPCION_PEDIDOMES + "-" + mvarWDB_RECEPCION_PEDIDODIA + " " + mvarWDB_RECEPCION_PEDIDOHORA + ":" + mvarWDB_RECEPCION_PEDIDOMINUTO + ":" + mvarWDB_RECEPCION_PEDIDOSEGUNDO) );
	
	        wvarStep = 240;
	
	        ps.setString(posiSP++,  new String( DateTime.year( DateTime.now() ) + "-" + DateTime.month( DateTime.now() ) + "-" + DateTime.day( DateTime.now() ) + " " + DateTime.hour( DateTime.now() ) + ":" + DateTime.minute( DateTime.now() ) + ":" + DateTime.second( DateTime.now() ) ));
	        wvarStep = 250;
	        ps.setString(posiSP++, String.valueOf("AUS1"));
	        wvarStep = 260;
	        ps.setInt(posiSP++, 0);
	        wvarStep = 270;
	        ps.setInt(posiSP++, 1);
	        wvarStep = 280;
	        ps.setInt(posiSP++, Integer.parseInt(mvarWDB_CERTIPOL));
	        wvarStep = 290;
	        ps.setInt(posiSP++, 0);
	        wvarStep = 300;
	        ps.setInt(posiSP++, Integer.parseInt(mvarWDB_CERTISEC.toString()));
	        wvarStep = 310;
	        if( mvarTiempoAIS.equals( "" ) )
	        {
	          mvarTiempoAIS = "1";
	        }
	        ps.setInt(posiSP++, Integer.parseInt(mvarTiempoAIS));
	        wvarStep = 320;
	        ps.setString(posiSP++, String.valueOf(mvarWDB_OPERACION_XML.equals( "" ) ? new Variant((Object)null) : new Variant(Strings.left( mvarWDB_OPERACION_XML, 8000 ))));
	        wvarStep = 330;
	
	        boolean result = ps.execute();
	
	        if (result) {
	      	  //El primero es un ResultSet
	        } else {
	      	  //Salteo el "updateCount"
	      	  int uc = ps.getUpdateCount();
	      	  if (uc  == -1) {
	//      		  System.out.println("No hay resultados");
	      	  }
	      	  result = ps.getMoreResults();
	        }
		} finally {
			try {
				if (jdbcConn != null)
					jdbcConn.close();
			} catch (Exception e) {
				logger.log(Level.WARNING, "Exception al hacer un close", e);
			}
		}

      }
      fncGetAll = true;
      return fncGetAll;
  }

  private boolean fncGrabaCotizacion( String pvarXMLrequest, String pvarXMLresponse, Variant pvarCertiSec )
  {
    boolean fncGrabaCotizacion = false;
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    XmlDomExtended mobjXMLDoc = null;
    int wvarCount = 0;
    String mvarCODINT = "";
    String mvarCertiSec = "";
    String mvarSUPLENUM = "";
    String mvarNacimAnn = "";
    String mvarNacimMes = "";
    String mvarNacimDia = "";
    String mvarCLIENEST = "";
    String mvarNUMHIJOS = "";
    String mvarDOMICCPO = "";
    String mvarPROVICOD = "";
    String mvarTIPOCUEN = "";
    String mvarCOBROTIP = "";
    String mvarFRANQCOD = "";
    String mvarPQTDES = "";
    String mvarPLANCOD = "";
    String mvarHIJOS1729 = "";
    String mvarZONA = "";
    String mvarCLIENIVA = "";
    String mvarSUMAASEG = "";
    String mvarSUMALBA = "";
    String mvarCLUB_LBA = "";
    String mvarDESTRUCCION_80 = "";
    String mvarLUNETA = "";
    String mvarCLUBECO = "";
    String mvarROBOCONT = "";
    String mvarGRANIZO = "";
    String mvarIBB = "";
    String mvarESCERO = "";
    String wvarCLIENTIP = "";
    String wvarCLIENTIP2 = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarSEXO = "";
    String mvarESTCIV = "";
    String mvarSIFMVEHI_DES = "";
    String mvarACCESORIOS = "";
    String mvarMOTORNUM = "";
    String mvarCHASINUM = "";
    String mvarPATENNUM = "";
    String mvarAUMARCOD = "";
    String mvarAUMODCOD = "";
    String mvarAUSUBCOD = "";
    String mvarAUADICOD = "";
    String mvarAUMODORI = "";
    String mvarAUKLMNUM = "";
    String mvarFABRICAN = "";
    String mvarGUGARAGE = "";
    String mvarAUNUMSIN = "";
    String mvarAUNUMKMT = "";
    String mvarAUUSOGNC = "";
    String mvarCAMP_CODIGO = "";
    String mvarNRO_PROD = "";
    String mvarCONDUAPE = "";
    String mvarCONDUNOM = "";
    String mvarCONDUFEC = "";
    String mvarCONDUSEX = "";
    String mvarCONDUEST = "";
    String mvarCONDUEXC = "";
    String mvarAUACCCOD = "";
    String mvarAUVEASUM = "";
    String mvarAUVEADES = "";
    String mvarAUVEADEP = "";
    String mvarCOBROCOD = "";

	java.sql.Connection jdbcConn = null;
    try 
    {
      wvarStep = 10;
      fncGrabaCotizacion = true;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarXMLrequest );

      wvarStep = 20;
      wobjXMLResponse = new XmlDomExtended();
      wobjXMLResponse.loadXML( pvarXMLresponse );

      wvarStep = 30;
      mvarNRO_PROD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PRODUCTOR )  );
      mvarCODINT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CODINST )  );
      mvarCertiSec = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Cliensec )  );
      mvarNacimAnn = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimAnn )  );
      mvarNacimMes = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimMes )  );
      mvarNacimDia = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_NacimDia )  );
      mvarCLIENEST = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Estado )  ) );
      mvarNUMHIJOS = String.valueOf( wobjXMLRequest.selectNodes( mcteNodos_Hijos ) .getLength() );
      mvarDOMICCPO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCod )  );
      mvarPROVICOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi )  );
      mvarCOBROTIP = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP )  ) );
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO_OPERACION )  == (org.w3c.dom.Node) null) )
      {
        mvarFRANQCOD = XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( mcteParam_FRANQCOD )  );
        mvarPQTDES = XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( mcteParam_PLANDES )  );
        mvarPLANCOD = XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( mcteParam_PLANCOD )  );
      }
      else
      {
        mvarFRANQCOD = " ";
        mvarPQTDES = " ";
        mvarPLANCOD = "0";
      }
      mvarHIJOS1729 = String.valueOf( wobjXMLRequest.selectNodes( mcteNodos_Hijos ) .getLength() );
      mvarRequest = "<Request><RAMOPCOD>AUS1</RAMOPCOD><PROVICOD>" + mvarPROVICOD + "</PROVICOD><CPACODPO>" + mvarDOMICCPO + "</CPACODPO></Request>";

		logger.log(Level.FINE, "Llamando localmente a lbaw_GetZona");
		lbaw_GetZona getZona = new lbaw_GetZona();
		StringHolder getZonaSH = new StringHolder();
		getZona.IAction_Execute(mvarRequest, getZonaSH, "");
		mvarResponse = getZonaSH.getValue();

		
      mobjXMLDoc = new XmlDomExtended();
      mobjXMLDoc.loadXML( mvarResponse );
      if( XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        mvarZONA = XmlDomExtended.getText( mobjXMLDoc.selectSingleNode( "//CODIZONA" )  );
      }
      else
      {
        mvarZONA = "";
      }
      mobjXMLDoc = null;

      mvarCLIENIVA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENIVA )  );
      mvarSUMAASEG = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SumAseg )  );
      mvarSUMALBA = XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//SUMALBA" )  );
      mvarCLUB_LBA = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClubLBA )  ) );
      mvarSEXO = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Sexo )  ) );
      mvarSIFMVEHI_DES = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_VEHDES )  ) );
      mvarACCESORIOS = (wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) .getLength() > 0 ? "S" : "N");
      mvarAUMARCOD = Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ModeAutCod )  ), 1, 5 );
      mvarAUMODCOD = Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ModeAutCod )  ), 6, 5 );
      mvarAUSUBCOD = Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ModeAutCod )  ), 11, 5 );
      mvarAUADICOD = Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ModeAutCod )  ), 16, 5 );
      mvarAUMODORI = Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ModeAutCod )  ), 21, 1 );
      mvarAUKLMNUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_KMsrngCod )  );
      mvarFABRICAN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_EfectAnn )  );
      mvarGUGARAGE = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_SiGarage )  ) );
      mvarAUNUMSIN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Siniestros )  );
      mvarAUUSOGNC = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Gas )  ) );
      mvarCAMP_CODIGO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CampaCod )  );
      mvarCOBROCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD )  );

      mvarDESTRUCCION_80 = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_DESTRUCCION_80 )  ) );
      mvarLUNETA = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Luneta )  ) );
      mvarCLUBECO = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ClubEco )  ) );
      mvarROBOCONT = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Robocont )  ) );
      mvarGRANIZO = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Granizo )  ) );
      mvarIBB = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_IBB )  ) );
      mvarESCERO = Strings.toUpperCase( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Escero )  ) );

      if( wobjXMLRequest.selectNodes( ("//" + mcteParam_CLIENTIP) ) .getLength() == 0 )
      {
        //Para forzar la persona física si no informa el nodo
        wvarCLIENTIP = "00";
      }
      else
      {
        wvarCLIENTIP = Strings.right( "00" + Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENTIP )  ) ), 2 );
      }

      if( wvarCLIENTIP.equals( "00" ) )
      {
        wvarCLIENTIP2 = "F";
      }
      else
      {
        wvarCLIENTIP2 = "J";
      }

      wvarStep = 40;
      
			JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
			jdbcConn = connFactory.getJDBCConnection(General.mcteDB);
      String sp = "{? = call SPSNCV_BRO_COTI_AUS1_GRABA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

      CallableStatement ps = jdbcConn.prepareCall(sp);
      int posiSP = 1;
      ps.registerOutParameter(posiSP++, Types.INTEGER);
      //Se prepara como parámetro de input-output el CertiSEC
      ps.registerOutParameter(7, Types.INTEGER);
      

	  wvarStep = 50;
      ps.setString(posiSP++, "AUS1");

      wvarStep = 60;
      ps.setInt(posiSP++, 0);
      
      wvarStep = 70;
      ps.setInt(posiSP++, 1);
      
      wvarStep = 80;
      ps.setInt(posiSP++, Integer.parseInt(mvarCODINT));

      wvarStep = 90;
      ps.setInt(posiSP++, 0);

      wvarStep = 100;
      ps.setInt(posiSP++, Integer.parseInt(mvarCertiSec));

      wvarStep = 110;
      ps.setInt(posiSP++, 0);

      wvarStep = 120;
      ps.setString(posiSP++, String.valueOf(mvarCODINT));

      wvarStep = 130;
      ps.setString(posiSP++, String.valueOf("0"));

      wvarStep = 140;
      ps.setInt(posiSP++, 0);

      wvarStep = 150;
      ps.setString(posiSP++, String.valueOf("INNOMINADA"));
      
      wvarStep = 160;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));
      
      wvarStep = 170;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 180;
      ps.setInt(posiSP++, Integer.parseInt(mvarNacimAnn));


      wvarStep = 190;
      ps.setInt(posiSP++, Integer.parseInt(mvarNacimMes));

      wvarStep = 200;
      ps.setInt(posiSP++, Integer.parseInt(mvarNacimDia));

      wvarStep = 210;
      ps.setString(posiSP++, String.valueOf(mvarSEXO));

      wvarStep = 220;
      ps.setString(posiSP++, String.valueOf(mvarCLIENEST));

      wvarStep = 230;
      ps.setString(posiSP++, String.valueOf("00"));

      wvarStep = 240;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 250;
      ps.setInt(posiSP++, Integer.parseInt(mvarNUMHIJOS));


      wvarStep = 260;
      ps.setInt(posiSP++, DateTime.year( DateTime.now() ));

      wvarStep = 270;
      ps.setInt(posiSP++,  DateTime.month( DateTime.now() ));


      wvarStep = 280;
      
      ps.setInt(posiSP++, DateTime.day( DateTime.now() ) );

      wvarStep = 290;
      // jc 10/2010 no es mas F persona f�sica
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTIP", adChar, adParamInput, 2, "F")
      //11/2010 ch.r.
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTIP", adChar, adParamInput, 2, "00")
      ps.setString(posiSP++, String.valueOf(wvarCLIENTIP));

      wvarStep = 300;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 310;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 320;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 330;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 340;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 350;
      //11/2010 ch.r.
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@PERSOTIP", adChar, adParamInput, 1, "F")
      ps.setString(posiSP++, String.valueOf(wvarCLIENTIP2));

      wvarStep = 360;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 370;
      ps.setInt(posiSP++, 0);

      wvarStep = 380;
      ps.setInt(posiSP++, 0);

      wvarStep = 390;
      ps.setInt(posiSP++, 0);

      wvarStep = 400;
      ps.setInt(posiSP++, 0);

      wvarStep = 410;
      ps.setInt(posiSP++, 0);

      wvarStep = 420;
      ps.setInt(posiSP++, 0);

      wvarStep = 430;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));
      
      wvarStep = 440;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));
      
      wvarStep = 450;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 460;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 470;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 480;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 490;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 500;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 510;
      ps.setInt(posiSP++, Integer.parseInt(mvarDOMICCPO));

      wvarStep = 520;
      ps.setInt(posiSP++, Integer.parseInt(mvarPROVICOD));

      wvarStep = 530;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 540;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 550;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 560;
      ps.setString(posiSP++, String.valueOf(mvarTIPOCUEN));

      wvarStep = 570;
      ps.setString(posiSP++, String.valueOf(mvarCOBROTIP));

      wvarStep = 580;
      ps.setInt(posiSP++, 9000);

      wvarStep = 590;
      ps.setInt(posiSP++, 8888);

      wvarStep = 600;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 610;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 620;
      ps.setInt(posiSP++, 0);

      wvarStep = 630;
      ps.setInt(posiSP++, 0);

      wvarStep = 640;
      ps.setInt(posiSP++, 0);

      wvarStep = 650;
      ps.setInt(posiSP++, 0);

      wvarStep = 660;
      ps.setString(posiSP++, String.valueOf(mvarFRANQCOD));

      wvarStep = 670;
      ps.setString(posiSP++, String.valueOf(mvarPQTDES));

      wvarStep = 680;
      ps.setInt(posiSP++, Integer.parseInt(mvarPLANCOD));
      
      wvarStep = 690;
      ps.setInt(posiSP++, 0);

      wvarStep = 700;
      ps.setInt(posiSP++, Integer.parseInt(mvarHIJOS1729));

      wvarStep = 710;
      ps.setInt(posiSP++, Integer.parseInt(mvarZONA));

      wvarStep = 720;
      ps.setInt(posiSP++, Integer.parseInt(mvarPROVICOD));

      wvarStep = 730;
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@SUMALBA", adNumeric, adParamInput, , mvarSUMALBA)
      String mvarSUMALBAdec = String.valueOf( Obj.toDouble( mvarSUMALBA ) / 100 );
      if (mvarSUMALBAdec.indexOf(".") != -1) {
    	  mvarSUMALBA = mvarSUMALBAdec.substring(0, mvarSUMALBAdec.indexOf("."));
      } else {
    	  mvarSUMALBA = mvarSUMALBAdec;
      }
      
      ps.setInt(posiSP++, Integer.parseInt(mvarSUMALBA));

      wvarStep = 740;
      ps.setString(posiSP++, String.valueOf(mvarCLIENIVA));

      wvarStep = 750;
      
      double susmaAseg = Double.parseDouble(mvarSUMAASEG.replace(",", "."));
      ps.setDouble(posiSP++, susmaAseg);

      wvarStep = 760;
      ps.setString(posiSP++, String.valueOf(mvarCLUB_LBA));

      wvarStep = 770;
      ps.setInt(posiSP++, 0);


      wvarStep = 780;
      ps.setInt(posiSP++, 0);

      wvarStep = 790;
      //jc 08/2010 ahora se informa valor
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@ESCERO", adChar, adParamInput, 1, " ") 'Reemplazado
      
      ps.setString(posiSP++, String.valueOf(mvarESCERO));

      

      wvarStep = 800;
      ps.setString(posiSP++, String.valueOf("N"));

      wvarStep = 810;
      ps.setString(posiSP++, String.valueOf("N"));

      wvarStep = 820;
      ps.setString(posiSP++, String.valueOf("N"));
      
      wvarStep = 830;
      ps.setInt(posiSP++, Integer.parseInt( mvarNacimAnn + mvarNacimMes + mvarNacimDia ));

      wvarStep = 840;
      ps.setString(posiSP++, String.valueOf(mvarSEXO));

      wvarStep = 850;
      ps.setString(posiSP++, String.valueOf(mvarCLIENEST));

      wvarStep = 860;
      ps.setString(posiSP++, String.valueOf( Strings.left( mvarSIFMVEHI_DES, 35 ) ));

      wvarStep = 870;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf( " " ));

      wvarStep = 880;
      ps.setString(posiSP++, String.valueOf(mvarACCESORIOS));

      wvarStep = 890;
      ps.setInt(posiSP++, 0);

      wvarStep = 900;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 910;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 920;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 930;
      ps.setInt(posiSP++, Integer.parseInt(mvarAUMARCOD));


      wvarStep = 940;
      ps.setInt(posiSP++, Integer.parseInt(mvarAUMODCOD ));


      wvarStep = 950;
      ps.setInt(posiSP++, Integer.parseInt(mvarAUSUBCOD));


      wvarStep = 960;
      ps.setInt(posiSP++, Integer.parseInt(mvarAUADICOD));


      wvarStep = 970;
      ps.setString(posiSP++, String.valueOf(mvarAUMODORI));

      wvarStep = 980;
      ps.setInt(posiSP++, 1);


      wvarStep = 990;
      ps.setString(posiSP++, String.valueOf("N"));

      wvarStep = 1000;
      ps.setInt(posiSP++, 0);

      wvarStep = 1010;
      ps.setInt(posiSP++, 0);
      
      wvarStep = 1020;
      ps.setInt(posiSP++, 0);

      wvarStep = 1030;
      ps.setInt(posiSP++, 0);

      wvarStep = 1040;
      ps.setInt(posiSP++, Integer.parseInt(mvarAUKLMNUM));

      wvarStep = 1050;
      ps.setInt(posiSP++, Integer.parseInt(mvarFABRICAN));

      wvarStep = 1060;
      ps.setInt(posiSP++, 1);

      wvarStep = 1070;
      ps.setString(posiSP++, String.valueOf(mvarGUGARAGE));

      wvarStep = 1080;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 1090;
      ps.setInt(posiSP++, 0);

      wvarStep = 1100;
      ps.setInt(posiSP++, 0);

      wvarStep = 1110;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 1120;
      ps.setInt(posiSP++, 0);

      wvarStep = 1130;
      ps.setInt(posiSP++, Integer.parseInt(mvarAUNUMSIN));

      wvarStep = 1140;
      ps.setInt(posiSP++, Integer.parseInt(mvarAUKLMNUM));

      wvarStep = 1150;
      ps.setString(posiSP++, String.valueOf(mvarAUUSOGNC));

      wvarStep = 1160;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf("A"));

      wvarStep = 1170;
      ps.setInt(posiSP++, 1);

      wvarStep = 1180;
      ps.setInt(posiSP++, DateTime.diff( "YYYY", DateTime.dateSerial( Obj.toInt( mvarNacimAnn ), Obj.toInt( mvarNacimMes ), Obj.toInt( mvarNacimDia ) ), DateTime.now()));

      wvarStep = 1190;
      for( wvarCount = 1; wvarCount <= 20; wvarCount++ )
      {
        ps.setInt(posiSP++, 0);
        
        ps.setInt(posiSP++, 0);
        
        ps.setInt(posiSP++, 0);
        
        ps.setInt(posiSP++, 0);
      }

      wvarStep = 1200;
      for( wvarCount = 1; wvarCount <= 10; wvarCount++ )
      {
        
        ps.setInt(posiSP++, 0);

        ps.setString(posiSP++, String.valueOf(" "));
      }

      wvarStep = 1210;
      ps.setInt(posiSP++, Integer.parseInt(mvarCAMP_CODIGO));

      wvarStep = 1220;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 1230;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 1240;
      ps.setInt(posiSP++, Integer.parseInt(mvarNRO_PROD));

      wvarStep = 1250;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 1260;
      //Reemplazado
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 1270;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Hijos ) ;
      for( wvarCount = 1; wvarCount <= 10; wvarCount++ )
      {
        ps.setString(posiSP++, String.valueOf(" "));

        //Reemplazado
        ps.setString(posiSP++, String.valueOf(" "));

        if( wvarCount <= Obj.toInt( mvarNUMHIJOS ) )
        {
          wobjXMLNode = wobjXMLList.item( wvarCount - 1 );
          mvarCONDUFEC = XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "//" + mcteParam_NacimHijo ) );
          mvarCONDUSEX = Strings.toUpperCase( XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "//" + mcteParam_SexoHijo )  ) );
          mvarCONDUEST = Strings.toUpperCase( XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "//" + mcteParam_EstadoHijo )  ) );
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          mvarCONDUFEC = "0";
          mvarCONDUSEX = " ";
          mvarCONDUEST = " ";
        }

        ps.setInt(posiSP++, Integer.parseInt( mvarCONDUFEC ));

        ps.setString(posiSP++, String.valueOf(mvarCONDUSEX));
        
        ps.setString(posiSP++, String.valueOf(mvarCONDUEST));
        
        ps.setString(posiSP++, String.valueOf("S"));
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;

      wvarStep = 1280;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) ;
      for( wvarCount = 1; wvarCount <= 10; wvarCount++ )
      {
        if( wvarCount <= wobjXMLList.getLength() )
        {
          wobjXMLNode = wobjXMLList.item( wvarCount - 1 );
          mvarAUACCCOD = XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "//" + mcteParam_CodigoAcc )  );
          mvarAUVEASUM = XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "//" + mcteParam_PrecioAcc )  );
          mvarAUVEADES = XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "//" + mcteParam_DescripcionAcc )  );
          mvarAUVEADEP = "S";
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          mvarAUACCCOD = "0";
          mvarAUVEASUM = "0";
          mvarAUVEADES = " ";
          mvarAUVEADEP = " ";
        }
        
        ps.setInt(posiSP++, Integer.parseInt(mvarAUACCCOD));
        
        ps.setInt(posiSP++, Integer.parseInt(mvarAUVEASUM));
        
        ps.setString(posiSP++, String.valueOf(mvarAUVEADES));
        
        ps.setString(posiSP++, String.valueOf(mvarAUVEADEP));
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;

      wvarStep = 1290;
      ps.setInt(posiSP++, Integer.parseInt(mvarCOBROCOD));

      //jc 08/2010 se agregan nuevos campos
      wvarStep = 1291;
      ps.setString(posiSP++, String.valueOf(mvarDESTRUCCION_80));

      wvarStep = 1292;
      ps.setString(posiSP++, String.valueOf(mvarLUNETA));

      wvarStep = 1293;
      ps.setString(posiSP++, String.valueOf(mvarCLUBECO));

      wvarStep = 1294;
      ps.setString(posiSP++, String.valueOf(mvarROBOCONT));

      wvarStep = 1295;
      ps.setString(posiSP++, String.valueOf(mvarGRANIZO));

      wvarStep = 1296;
      ps.setString(posiSP++, String.valueOf(mvarIBB));


      wvarStep = 1300;
      boolean result = ps.execute();
      int certiSecValRetu = ps.getInt(7);
      pvarCertiSec.set(certiSecValRetu);
      if (result) {
    	  //El primero es un ResultSet
      } else {
    	  //Salteo el "updateCount"
    	  int uc = ps.getUpdateCount();
    	  if (uc  == -1) {
//    		  System.out.println("No hay resultados");
    	  }
    	  result = ps.getMoreResults();
      }

      return fncGrabaCotizacion;
	} catch (XmlDomExtendedException e) {
		logger.log(Level.WARNING, "Exception al grabar la cotización", e);
        fncGrabaCotizacion = false;
	} catch (SQLException e) {
		logger.log(Level.WARNING, "Exception al grabar la cotización", e);
        fncGrabaCotizacion = false;
	} finally {
		try {
			if (jdbcConn != null)
				jdbcConn.close();
		} catch (Exception e) {
			logger.log(Level.WARNING, "Exception al hacer un close", e);
		}
	}
    return fncGrabaCotizacion;
  }

  public void Activate() throws Exception
  {
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
