package com.qbe.services.interWSBrok.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.interWSBrok.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Implementacion de los objetos
 * Objetos del FrameWork
 */

public class GetVehiculo implements VBObjectClass
{
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "LBA_InterWSBrok.GetVehiculo";
  static final String mcteStoreProc = "SPSNCV_BRO_ALTA_OPER";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_Marca = "MARCA";
  static final String mcteParam_Modelo = "MODELO";
  static final String mcteParam_EfectAnn = "EFECTANN";
  /**
   * TDF
   */
  static final String mcteParam_Provicod = "PROVICOD";
  static final String mcteParam_EstadoProc = "ESTADOPROC";
  static final String mcteParam_TiempoProceso = "TIEMPOPROCESO";
  static final String mcteParam_PedidoAno = "MSGANO";
  static final String mcteParam_PedidoMes = "MSGMES";
  static final String mcteParam_PedidoDia = "MSGDIA";
  static final String mcteParam_PedidoHora = "MSGHORA";
  static final String mcteParam_PedidoMinuto = "MSGMINUTO";
  static final String mcteParam_PedidoSegundo = "MSGSEGUNDO";
  static final String mcteParam_USUARCOD = "USUARCOD";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncGetAll
   * static variable for method: fncInsertNode
   * static variable for method: fncPutSolHO
   * static variable for method: fncPutSolicitud
   */
  private final String wcteFnName = "fncPutSolicitud";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Variant wvarMensaje = new Variant();
    Variant wvarCodErr = new Variant();
    Variant pvarRes = new Variant();
    HSBCInterfaces.IAction wobjClass = null;
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      pvarRequest = Strings.mid( pvarRequest, 1, Strings.find( 1, pvarRequest, ">" ) ) + "<MSGANO>" + DateTime.year( DateTime.now() ) + "</MSGANO>" + "<MSGMES>" + DateTime.month( DateTime.now() ) + "</MSGMES>" + "<MSGDIA>" + DateTime.day( DateTime.now() ) + "</MSGDIA>" + "<MSGHORA>" + DateTime.hour( DateTime.now() ) + "</MSGHORA>" + "<MSGMINUTO>" + DateTime.minute( DateTime.now() ) + "</MSGMINUTO>" + "<MSGSEGUNDO>" + DateTime.second( DateTime.now() ) + "</MSGSEGUNDO>" + Strings.mid( pvarRequest, (Strings.find( 1, pvarRequest, ">" ) + 1) );
      wvarStep = 10;
      wvarMensaje.set( "" );
      wvarCodErr.set( "" );
      pvarRes.set( "" );
      //
      if( ! (invoke( "fncGetAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        pvarResponse.set( pvarRes );
        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*TBD mobjCOM_Context.SetAbort() ;*/
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*TBD mobjCOM_Context.SetComplete() ;*/
        }
        return IAction_Execute;
      }

      pvarResponse.set( pvarRes );
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetComplete() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncGetAll( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncGetAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLReturnVal = null;
    XmlDomExtended wobjXMLError = null;
    XmlDomExtended wobjXMLRequestExists = null;
    Object wobjClass = null;
    Variant wvarMensajeStoreProc = new Variant();
    Variant wvarMensajePutTran = new Variant();
    String mvar_Estado = "";
    String mvarCertiSec = "";
    HSBCInterfaces.IDBConnection wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    String pTiempoAIS = "";

    //
    //
    //
    try 
    {

      //Instancia la clase de validacion
      wvarMensaje.set( "" );
      wvarMensajeStoreProc.set( "" );
      //
      wvarStep = 20;
      wobjClass = new GetValidacionVehiculo();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(pvarRequest, wvarMensajeStoreProc, "")
      wobjClass = null;
      //
      //Analizo la respuesta del validador
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( wvarMensajeStoreProc.toString() );
      //
      //Si la respuesta viene con error
      wvarStep = 30;
      if( ! ( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ).equals( "0" )) )
      {
        wvarCodErr.set( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ) );
        wvarMensaje.set( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" )  ) );

        mvar_Estado = "ER";
        pvarRes.set( wvarMensajeStoreProc );
        wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
        // si no viene con error de la BD
      }
      else
      {

        //Guarda la Solicitud en el SP
        wvarStep = 60;

        if( invoke( "fncPutSolicitud", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes), new Variant(pTiempoAIS) } ) )
        {

          wobjXMLReturnVal = new XmlDomExtended();
          wobjXMLReturnVal.loadXML( pvarRes.toString() );
          //
          if( ! ( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ).equals( "0" )) )
          {
            //hubo un error
            wvarStep = 65;
            wvarCodErr.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ) );
            wvarMensaje.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" )  ) );
            wvarMensajeStoreProc.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\">" + pvarRequest + "</LBA_WS>" );
            mvar_Estado = "ER";

            //Este es el caso en que no haya contestado el SP o sea un error no identificado
          }
          else
          {
            wvarStep = 70;
            mvar_Estado = "OK";
            wvarMensajeStoreProc.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
          }
          //
          wobjXMLReturnVal = null;

        }


        //
      }

      invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(mvar_Estado), new Variant("ESTADOPROC") } );

      // Agrego el tiempo de impresion
      invoke( "fncInsertNode", new Variant[] { new Variant(wvarMensajeStoreProc), new Variant("//LBA_WS/Request"), new Variant(pTiempoAIS), new Variant("TIEMPOPROCESO") } );

      //Alta de la OPERACION
      wvarStep = 75;
      if( invoke( "fncPutSolHO", new Variant[] { new Variant(wvarMensajeStoreProc.toString()), new Variant(wvarMensaje.toString()), new Variant(wvarCodErr), new Variant(wvarMensajePutTran) } ) )
      {
        //
        wobjXMLReturnVal = new XmlDomExtended();
        wobjXMLReturnVal.loadXML( wvarMensajePutTran.toString() );
        //
        wvarStep = 80;
        if( ! ( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ).equals( "0" )) )
        {
          wvarCodErr.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_code" )  ) );
          wvarMensaje.set( XmlDomExtended .getText( wobjXMLReturnVal.selectSingleNode( "//LBA_WS/@res_msg" )  ) );
          pvarRes.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
          fncGetAll = false;
          return fncGetAll;
        }
        //
        fncGetAll = true;
        //
      }
      fin: 
      wobjClass = null;
      wobjXMLRequest = null;
      return fncGetAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncGetAll = false;
        /*TBD mobjCOM_Context.SetComplete() ;*/

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncGetAll;
  }

  private boolean fncInsertNode( Variant pvarRequest, String pvarHeader, String pvarNodeValue, String pvarNodeName )
  {
    boolean fncInsertNode = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;


    try 
    {

      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest.toString() );
      //
      wvarStep = 20;
      /*unsup wobjXMLRequest.selectSingleNode( pvarHeader ) */.appendChild( null /*unsup wobjXMLRequest.createNode( new Variant( org.w3c.dom.Node.ELEMENT_NODE ), pvarNodeName, "" ) */ );
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//" + pvarNodeName ) , pvarNodeValue );
      //
      pvarRequest.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
      wobjXMLRequest = null;
      fncInsertNode = true;
      return fncInsertNode;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncInsertNode = false;
        /*TBD mobjCOM_Context.SetComplete() ;*/

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncInsertNode;
  }

  private boolean fncPutSolHO( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutSolHO = false;
    int wvarStep = 0;
    HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    XmlDomExtended wobjXMLRequest = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_NROCOT = "";
    String mvarWDB_TIPOOPERAC = "";
    String mvarWDB_ESTADO = "";
    String mvarWDB_RECEPCION_PEDIDOANO = "";
    String mvarWDB_RECEPCION_PEDIDOMES = "";
    String mvarWDB_RECEPCION_PEDIDODIA = "";
    String mvarWDB_RECEPCION_PEDIDOHORA = "";
    String mvarWDB_RECEPCION_PEDIDOMINUTO = "";
    String mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_TIEMPOPROCESO = "";
    String mvarWDB_XML = "";

    //
    //
    //
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_CODINST = "0";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST )  );
      }
      //
      wvarStep = 30;
      mvarWDB_REQUESTID = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID )  );
      }
      //
      wvarStep = 60;
      mvarWDB_ESTADO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EstadoProc) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_ESTADO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_EstadoProc )  );
      }
      //
      wvarStep = 70;
      mvarWDB_RECEPCION_PEDIDOANO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoAno) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOANO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoAno )  );
      }
      //
      wvarStep = 80;
      mvarWDB_RECEPCION_PEDIDOMES = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMes) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMes )  );
      }
      //
      wvarStep = 90;
      mvarWDB_RECEPCION_PEDIDODIA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoDia) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDODIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoDia )  );
      }
      //
      wvarStep = 100;
      mvarWDB_RECEPCION_PEDIDOHORA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoHora) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOHORA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoHora )  );
      }
      //
      wvarStep = 110;
      mvarWDB_RECEPCION_PEDIDOMINUTO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoMinuto) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOMINUTO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoMinuto )  );
      }
      //
      wvarStep = 120;
      mvarWDB_RECEPCION_PEDIDOSEGUNDO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PedidoSegundo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RECEPCION_PEDIDOSEGUNDO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PedidoSegundo )  );
      }
      //
      wvarStep = 170;
      mvarWDB_TIEMPOPROCESO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TiempoProceso) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_TIEMPOPROCESO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TiempoProceso )  );
      }



      wvarStep = 180;
      mvarWDB_XML = "";
      mvarWDB_XML = pvarRequest;
      //
      wvarStep = 190;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.mcteDB);
      //
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 200;
      wobjDBParm = new Parameter( "@WDB_IDENTIFICACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      
      //
      wvarStep = 210;
      wobjDBParm = new Parameter( "@WDB_NRO_OPERACION_BROKER", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_REQUESTID.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_REQUESTID)) );
      
      //
      wvarStep = 220;
      wobjDBParm = new Parameter( "@WDB_NRO_COTIZACION_LBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_NROCOT.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_NROCOT)) );
      
      //
      wvarStep = 230;
      
      
      //
      wvarStep = 240;
      
      
      //
      wvarStep = 250;
      
      
      //
      wvarStep = 260;
      
      
      //
      wvarStep = 270;
      
      
      //
      wvarStep = 280;
      wobjDBParm = new Parameter( "@WDB_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZANN)) );
      
      //
      wvarStep = 290;
      wobjDBParm = new Parameter( "@WDB_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_POLIZSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_POLIZSEC)) );
      
      //
      wvarStep = 300;
      wobjDBParm = new Parameter( "@WDB_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      
      //
      wvarStep = 310;
      
      
      //
      wvarStep = 320;
      wobjDBParm = new Parameter( "@WDB_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CERTISEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CERTISEC)) );
      
      //
      wvarStep = 330;
      wobjDBParm = new Parameter( "@WDB_TIEMPO_PROCESO_AIS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIEMPOPROCESO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIEMPOPROCESO)) );
      
      //
      wvarStep = 340;
      
      
      //
      wvarStep = 350;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      wobjDBCnn.close();

      fncPutSolHO = true;
      pvarRes.set( "<LBA_WS res_code=\"" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_code" )  ) + "\" res_msg=\"" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//LBA_WS/@res_msg" )  ) + "\"></LBA_WS>" );
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = null;
      wobjClass = (HSBCInterfaces.IAction) null;
      return fncPutSolHO;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );

        fncPutSolHO = false;
        //mobjCOM_Context.SetComplete
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutSolHO;
  }

  private boolean fncPutSolicitud( String pvarRequest, Variant wvarMensaje, Variant wvarCodErr, Variant pvarRes, String pTiempoAIS )
  {
    boolean fncPutSolicitud = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Object wobjClass = null;
    HSBCInterfaces.IDBConnection wobjHSBC_DBCnn = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended mobjXMLDoc = null;
    org.w3c.dom.NodeList wobjXMLNodeList = null;
    int wvarcounter = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_REQUESTID = "";
    String mvarWDB_MARCA = "";
    String mvarWDB_MODELO = "";
    String mvarWDB_EFECTANN = "";
    String mvarWDB_USUARCOD = "";
    String mvarWDB_MENSAJE = "";
    String mvarWDB_PROVICOD = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarOk = "";
    String mvar_TiempoProceso = "";
    String mvarInicioAIS = "";
    String mvarFinAIS = "";
    String mvarTiempoAIS = "";

    //
    //TDF
    // variables para calcular tiempo proceso de la impresi�n
    try 
    {


      //Recupero datos del request
      wvarStep = 100;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 140;
      mvarWDB_REQUESTID = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_REQUESTID) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_REQUESTID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_REQUESTID )  );
      }
      //
      wvarStep = 150;
      mvarWDB_CODINST = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST )  );
      }
      //
      wvarStep = 160;
      mvarWDB_MARCA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Marca) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_MARCA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Marca )  );
      }
      //
      wvarStep = 170;
      mvarWDB_MODELO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Modelo) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_MODELO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Modelo )  );
      }
      //
      wvarStep = 180;
      mvarWDB_EFECTANN = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_EfectAnn) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_EFECTANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_EfectAnn )  );
      }
      //
      wvarStep = 420;
      mvarWDB_USUARCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_USUARCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_USUARCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_USUARCOD )  );
      }

      wvarStep = 500;
      mvarWDB_PROVICOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provicod) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provicod )  );
      }
      else
      {
        //Default por si no viene
        mvarWDB_PROVICOD = "01";
      }

      //conforma xml para el request
      wvarStep = 610;
      mvarRequest = "<Request>" + "<DEFINICION>1050_WSvehiculos.xml</DEFINICION>" + "<USUARCOD>PR21072060</USUARCOD>" + "<MARCA>" + mvarWDB_MARCA + "</MARCA>" + "<MODELO>" + Strings.toUpperCase( mvarWDB_MODELO ) + "</MODELO>" + "<ANIO>" + mvarWDB_EFECTANN + "</ANIO>" + "<FECANN>" + DateTime.year( DateTime.now() ) + "</FECANN>" + "<FECMES>" + DateTime.month( DateTime.now() ) + "</FECMES>" + "<FECDIA>" + DateTime.day( DateTime.now() ) + "</FECDIA>" + "<PROVICOD>" + mvarWDB_PROVICOD + "</PROVICOD>" + "</Request>";

      wvarStep = 620;


      wobjClass = new lbawA_OVMQGen.lbaw_MQMensaje();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(mvarRequest, mvarResponse, "")
      wobjClass = null;

      mobjXMLDoc = new XmlDomExtended();
      mobjXMLDoc.loadXML( mvarResponse );
      //Call mobjXMLDoc.Load(App.Path & "\pruebaVehiculo.xml")
      mvarOk = "ER";
      if( ! (mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended .getText( mobjXMLDoc.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          mvarOk = "OK";
        }
        else
        {
          mvarOk = "ER";
        }
      }
      else
      {
        mvarOk = "ER";
      }

      if( mvarOk.equals( "ER" ) )
      {
        wvarCodErr.set( -101 );
        wvarMensaje.set( "No se pudo recuperar el Vehiculo" );
        pvarRes.set( "<LBA_WS res_code=\"-54\" res_msg=\"Error\"><Response><Estado resultado=\"false\" mensaje=" + wvarMensaje + " /></Response></LBA_WS>" );
        return fncPutSolicitud;
      }
      else
      {
        fncPutSolicitud = true;

        // Armo el XML de salida
        wobjXMLNodeList = mobjXMLDoc.selectNodes( "//Response/CAMPOS/RESULTADOS/VEHICULOS" ) ;

        pvarRes.set( "<LBA_WS res_code=\"0\" res_msg=\"\"><Response><REQUESTID>" + mvarWDB_REQUESTID + "</REQUESTID><VEHICULOS>" );

        for( wvarcounter = 0; wvarcounter <= (wobjXMLNodeList.getLength() - 1); wvarcounter++ )
        {
          wvarStep = 931;
          pvarRes.set( pvarRes + "<VEHICULO>" );
          pvarRes.set( pvarRes + "<MARCA>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNodeList.item(wvarcounter), "AUMARCOD" )  ) + "</MARCA>" );
          pvarRes.set( pvarRes + "<MODELO>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNodeList.item(wvarcounter), "AUMODCOD" )  ) + "</MODELO>" );
          pvarRes.set( pvarRes + "<SUBMODELO>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNodeList.item(wvarcounter), "AUSUBCOD" )  ) + "</SUBMODELO>" );
          pvarRes.set( pvarRes + "<ADICIONAL>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNodeList.item(wvarcounter), "AUADICOD" )  ) + "</ADICIONAL>" );
          pvarRes.set( pvarRes + "<ORIGEN>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNodeList.item(wvarcounter), "AUMODORI" )  ) + "</ORIGEN>" );
          pvarRes.set( pvarRes + "<DESCRIPCION>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNodeList.item(wvarcounter), "AUVEHDES" )  ) + "</DESCRIPCION>" );
          pvarRes.set( pvarRes + "<INFOAUTO>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNodeList.item(wvarcounter), "AUPRONUM" )  ) + "</INFOAUTO>" );
          pvarRes.set( pvarRes + "<TIPOVEHI>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNodeList.item(wvarcounter), "AUTIPCOD" )  ) + "</TIPOVEHI>" );
          pvarRes.set( pvarRes + "<CATEGO>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNodeList.item(wvarcounter), "AUCATCOD" )  ) + "</CATEGO>" );
          pvarRes.set( pvarRes + "<COMBUSTIBLE>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNodeList.item(wvarcounter), "AUTIPCOM" )  ) + "</COMBUSTIBLE>" );
          pvarRes.set( pvarRes + "<SUMA>" + String.valueOf( new Variant( XmlDomExtended .getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNodeList.item(wvarcounter), "AUSUMASE" )  ) ).toDecimal().divide( new java.math.BigDecimal( 100 ), java.math.BigDecimal.ROUND_HALF_EVEN ).doubleValue() ) + "</SUMA>" );
          pvarRes.set( pvarRes + "</VEHICULO>" );

        }
        pvarRes.set( pvarRes + "</VEHICULOS></Response></LBA_WS>" );

      }

      fin: 
      //libero los objectos
      wobjXMLRequest = null;
      wobjClass = null;
      mobjXMLDoc = null;
      return fncPutSolicitud;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"-54\" res_msg=\"Error\"><Response><Estado resultado=\"false\" mensaje=" + wvarMensaje + " /></Response></LBA_WS>" );

        wvarCodErr.set( General.mcteErrorInesperadoCod );

        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncPutSolicitud = false;
        //mobjCOM_Context.SetComplete
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutSolicitud;
  }

  public String fechaVige( Variant pvarStrFecha ) throws Exception
  {
    String fechaVige = "";
    String wvarIntAnio = "";
    String wvarIntMes = "";
    String wvarIntDia = "";
    String[] warrSplit = null;


    pvarStrFecha.set( Strings.format( pvarStrFecha.toString(), "General Date" ) );

    wvarIntAnio = String.valueOf( DateTime.year( pvarStrFecha.toDate() ) );
    wvarIntMes = Strings.right( "0" + DateTime.month( pvarStrFecha.toDate() ), 2 );
    wvarIntDia = Strings.right( "0" + DateTime.day( pvarStrFecha.toDate() ), 2 );

    fechaVige = wvarIntDia + "/" + wvarIntMes + "/" + wvarIntAnio;

    return fechaVige;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
