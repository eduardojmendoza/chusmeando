package com.qbe.services.interWSBrok.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.interWSBrok.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class PutCotizacionHO implements VBObjectClass
{
  static final String mcteClassName = "LBA_InterWSBrok.PutCotizacionHO";
  static final String mcteStoreProc = "SPSNCV_BRO_COTI_HOM1_GRABA";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "REQUESTID";
  static final String mcteParam_CODINST = "CODINST";
  static final String mcteParam_POLIZANN = "POLIZANN";
  static final String mcteParam_POLIZSEC = "POLIZSEC";
  /**
   * EG 10-2010 se cambia la constante
   * Const mcteParam_COBROCOD             As String = "COBROCOD"
   */
  static final String mcteParam_COBROCOD = "FPAGO";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_TIPOHOGAR = "TIPOHOGAR";
  /**
   * EG 10-2010 se cambia la constante
   * Const mcteParam_PLANNCOD             As String = "PLANNCOD"
   */
  static final String mcteParam_PLANNCOD = "PLAN";
  /**
   * EG 10-2010 se cambia la constante
   * Const mcteParam_Provi                As String = "PROVI"
   */
  static final String mcteParam_Provi = "PROVCOD";
  /**
   * EG 10-2010 se cambia la constante
   * Const mcteParam_LocalidadCod         As String = "LOCALIDADCOD"
   */
  static final String mcteParam_LocalidadCod = "LOCALIDAD";
  static final String mcteParam_TipoOperac = "TIPOOPERAC";
  static final String mcteParam_RAMOPCOD = "RAMOPCOD";
  static final String mcteParam_CLIENIVA = "CLIENIVA";
  static final String mcteParam_NROCOT = "COT_NRO";
  /**
   * Const mcteParam_Estado               As String = "ESTADO"
   */
  static final String mcteParam_PedidoAno = "MSGANO";
  static final String mcteParam_PedidoMes = "MSGMES";
  static final String mcteParam_PedidoDia = "MSGDIA";
  static final String mcteParam_PedidoHora = "MSGHORA";
  static final String mcteParam_PedidoMinuto = "MSGMINUTO";
  static final String mcteParam_PedidoSegundo = "MSGSEGUNDO";
  static final String mcteParam_CERTISEC = "CERTISEC";
  static final String mcteParam_DOMICCPO = "DOMICCPO";
  static final String mcteParam_ZONA = "CODZONA";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS/COBERTURA";
  static final String mcteParam_COBERCOD = "COBERCOD";
  static final String mcteParam_CONTRMOD = "CONTRMOD";
  static final String mcteParam_CAPITASG = "CAPITASG";
  /**
   * DATOS DE LAS CAMPA�AS
   */
  static final String mcteNodos_Campa = "//Request/CAMPANIAS/CAMPANIA";
  static final String mcteParam_CampaCod = "CAMPACOD";
  /**
   * PRODUCTOR
   */
  static final String mctePORTAL = "LBA";
  /**
   * Horas de Ingreso y Salida
   */
  static final String mcteParam_Recepcion = "RECEPCION";
  static final String mcteParam_Envio = "ENVIO";
  /**
   * Objetos del FrameWork
   */
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   * static variable for method: fncPutAll
   */
  private final String wcteFnName = "fncPutAll";

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarMensaje = "";
    Variant wvarCodErr = new Variant();
    Variant pvarRes = new Variant();
    //
    //
    //declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wvarMensaje = "";
      wvarCodErr.set( "" );
      pvarRes.set( "" );
      if( ! (invoke( "fncPutAll", new Variant[] { new Variant(pvarRequest), new Variant(wvarMensaje), new Variant(wvarCodErr), new Variant(pvarRes) } )) )
      {
        if( !wvarCodErr.toString().equals( General.mcteErrorInesperadoCod ) )
        {
          pvarResponse.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );
        }
        else
        {
          pvarResponse.set( pvarRes );
        }

        if( General.mcteIfFunctionFailed_failClass )
        {
          wvarStep = 20;
          IAction_Execute = 1;
          /*TBD mobjCOM_Context.SetAbort() ;*/
        }
        else
        {
          wvarStep = 30;
          IAction_Execute = 0;
          /*TBD mobjCOM_Context.SetComplete() ;*/
        }
        return IAction_Execute;
      }
      //
      pvarResponse.set( pvarRes );
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarResponse.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetComplete() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncPutAll( String pvarRequest, String wvarMensaje, Variant wvarCodErr, Variant pvarRes )
  {
    boolean fncPutAll = false;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    XmlDomExtended wobjXMLRequest = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.Node wobjXMLNode = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    int wvarcounter = 0;
    String mvarWDB_CODINST = "";
    String mvarWDB_RAMOPCOD = "";
    String mvarWDB_POLIZANN = "";
    String mvarWDB_POLIZSEC = "";
    String mvarWDB_CERTISEC = "";
    String mvarWDB_USUARCOD = "";
    String mvarWDB_EFECTANN = "";
    String mvarWDB_EFECTMES = "";
    String mvarWDB_EFECTDIA = "";
    String mvarWDB_CLIENTIP = "";
    String mvarWDB_DOMICCPO = "";
    String mvarWDB_PROVICOD = "";
    String mvarWDB_COBROTIP = "";
    String mvarWDB_P_CIAASCOD = "";
    String mvarWDB_P_EMISIANN = "";
    String mvarWDB_P_EMISIMES = "";
    String mvarWDB_P_EMISIDIA = "";
    String mvarWDB_P_COBROCOD = "";
    String mvarWDB_P_COBROFOR = "";
    String mvarWDB_P_TIVIVCOD = "";
    String mvarWDB_P_PLANNCOD = "";
    String mvarWDB_P_ZONA = "";
    String mvarWDB_P_CLIENIVA = "";

    //
    //
    //
    try 
    {

      //Alta de la OPERACION
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarWDB_CODINST = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CODINST) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CODINST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CODINST )  );
      }
      //
      wvarStep = 30;
      mvarWDB_RAMOPCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_RAMOPCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_RAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_RAMOPCOD )  );
      }
      //
      wvarStep = 40;
      mvarWDB_POLIZANN = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZANN) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZANN )  );
      }
      //
      wvarStep = 50;
      mvarWDB_POLIZSEC = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_POLIZSEC) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_POLIZSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_POLIZSEC )  );
      }
      //
      wvarStep = 60;
      mvarWDB_CERTISEC = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CERTISEC) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_CERTISEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CERTISEC )  );
      }
      //
      wvarStep = 70;
      mvarWDB_USUARCOD = mvarWDB_CODINST;
      //
      wvarStep = 80;
      mvarWDB_DOMICCPO = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_LocalidadCod) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_DOMICCPO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_LocalidadCod )  );
      }
      //
      wvarStep = 90;
      mvarWDB_PROVICOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_Provi) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_PROVICOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_Provi )  );
      }
      //
      wvarStep = 100;
      mvarWDB_P_EMISIANN = Strings.right( Strings.fill( 4, "0" ) + DateTime.year( DateTime.now() ), 4 );
      mvarWDB_P_EMISIMES = Strings.right( Strings.fill( 2, "0" ) + DateTime.month( DateTime.now() ), 2 );
      mvarWDB_P_EMISIDIA = Strings.right( Strings.fill( 2, "0" ) + DateTime.day( DateTime.now() ), 2 );
      //
      mvarWDB_EFECTANN = Strings.right( Strings.fill( 4, "0" ) + DateTime.year( DateTime.now() ), 4 );
      mvarWDB_EFECTMES = Strings.right( Strings.fill( 2, "0" ) + DateTime.month( DateTime.now() ), 2 );
      mvarWDB_EFECTDIA = Strings.right( Strings.fill( 2, "0" ) + DateTime.day( DateTime.now() ), 2 );
      //
      wvarStep = 110;
      mvarWDB_COBROTIP = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROTIP) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_COBROTIP = Strings.toUpperCase( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROTIP )  ) );
      }
      //
      wvarStep = 120;
      mvarWDB_P_COBROCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_COBROCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_COBROCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_COBROCOD )  );
      }
      //
      wvarStep = 130;
      mvarWDB_P_TIVIVCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_TIPOHOGAR) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_TIVIVCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_TIPOHOGAR )  );
      }
      //
      wvarStep = 140;
      mvarWDB_P_PLANNCOD = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_PLANNCOD) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_PLANNCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_PLANNCOD )  );
      }
      //
      wvarStep = 150;
      mvarWDB_P_ZONA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_ZONA) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_ZONA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_ZONA )  );
      }
      //
      wvarStep = 160;
      mvarWDB_P_CLIENIVA = "";
      if( ! (wobjXMLRequest.selectSingleNode( ("//" + mcteParam_CLIENIVA) )  == (org.w3c.dom.Node) null) )
      {
        mvarWDB_P_CLIENIVA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + mcteParam_CLIENIVA )  );
      }
      //
      wvarStep = 170;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.mcteDB);
      //
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 180;
      //
      
      
      //
      wvarStep = 190;
      
      
      //
      wvarStep = 200;
      
      
      //
      wvarStep = 210;
      
      
      //
      wvarStep = 220;
      
      
      //
      wvarStep = 230;
      
      
      //
      wvarStep = 240;
      
      
      //
      wvarStep = 250;
      
      
      //
      wvarStep = 260;
      
      
      //
      wvarStep = 270;
      
      
      //
      wvarStep = 280;
      
      
      //
      wvarStep = 290;
      
      
      //
      wvarStep = 300;
      
      
      //
      wvarStep = 310;
      
      
      //
      wvarStep = 320;
      
      
      //
      wvarStep = 330;
      
      
      //
      wvarStep = 340;
      
      
      //
      wvarStep = 350;
      
      
      //
      wvarStep = 360;
      
      
      //
      wvarStep = 370;
      
      
      //
      wvarStep = 380;
      
      
      //
      wvarStep = 390;
      
      
      //
      wvarStep = 400;
      
      
      //
      wvarStep = 410;
      
      
      //
      wvarStep = 420;
      //10/2010 no es mas F persona f�sica
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTIP", adChar, adParamInput, 2, "F")
      
      
      //
      wvarStep = 430;
      
      
      //
      wvarStep = 440;
      
      
      //
      wvarStep = 450;
      
      
      //
      wvarStep = 460;
      
      
      //
      wvarStep = 470;
      
      
      //
      wvarStep = 480;
      
      
      //
      wvarStep = 490;
      
      
      //
      wvarStep = 500;
      
      
      //
      wvarStep = 510;
      
      
      //
      wvarStep = 520;
      
      
      //
      wvarStep = 530;
      
      
      //
      wvarStep = 540;
      
      
      //
      wvarStep = 550;
      
      
      //
      wvarStep = 560;
      
      
      //
      wvarStep = 570;
      
      
      //
      wvarStep = 580;
      
      
      //
      wvarStep = 590;
      
      
      //
      wvarStep = 600;
      
      
      //
      wvarStep = 610;
      
      
      //
      wvarStep = 620;
      
      
      //
      wvarStep = 630;
      
      
      //
      wvarStep = 640;
      
      
      //
      wvarStep = 650;
      
      
      //
      wvarStep = 660;
      
      
      //
      wvarStep = 670;
      
      
      //
      wvarStep = 680;
      
      
      //
      wvarStep = 690;
      
      
      //
      wvarStep = 700;
      
      
      //
      wvarStep = 710;
      
      
      //
      wvarStep = 720;
      
      
      //
      wvarStep = 730;
      
      
      //
      wvarStep = 740;
      
      
      //
      wvarStep = 750;
      
      
      //
      wvarStep = 760;
      
      
      //
      wvarStep = 770;
      
      
      //
      wvarStep = 780;
      
      
      //
      wvarStep = 790;
      
      
      //
      wvarStep = 800;
      
      
      //
      wvarStep = 810;
      
      
      //
      wvarStep = 820;
      
      
      //
      wvarStep = 830;
      
      
      //
      wvarStep = 840;
      
      
      //
      wvarStep = 850;
      
      
      //
      wvarStep = 860;
      
      
      //
      wvarStep = 870;
      
      
      //
      wvarStep = 880;
      
      
      //
      wvarStep = 890;
      
      
      //
      wvarStep = 900;
      
      
      //
      wvarStep = 910;
      
      
      //
      wvarStep = 920;
      
      
      //
      wvarStep = 930;
      
      
      //
      wvarStep = 940;
      
      
      //
      wvarStep = 950;
      
      
      //
      wvarStep = 960;
      
      
      //
      wvarStep = 970;
      
      
      //
      wvarStep = 980;
      
      
      //
      wvarStep = 990;
      
      
      //
      wvarStep = 1000;
      
      
      //
      wvarStep = 1010;
      
      
      //
      wvarStep = 1020;
      
      
      //
      wvarStep = 1030;
      
      
      //
      wvarStep = 1040;
      
      
      //
      wvarStep = 1050;
      
      
      //
      wvarStep = 1060;
      
      
      //
      wvarStep = 1070;
      
      
      //
      wvarStep = 1080;
      
      
      //
      wvarStep = 1090;
      
      
      //
      wvarStep = 1100;
      
      
      //
      wvarStep = 1110;
      
      
      //
      wvarStep = 1120;
      
      
      //
      wvarStep = 1130;
      
      
      //
      wvarStep = 1140;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Campa ) ;
      //
      for( wvarcounter = 1; wvarcounter <= 10; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
          wobjDBParm = new Parameter( "@P_CAMPACOD_" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_CampaCod )  ) ) );
          
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          wobjDBParm = new Parameter( "@P_CAMPACOD_" + String.valueOf( wvarcounter ), AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( "" ) );
          
        }
      }
      //
      wobjXMLList = (org.w3c.dom.NodeList) null;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Cober ) ;
      //
      wvarStep = 1150;
      // AM se cambio de 20 a 30
      for( wvarcounter = 1; wvarcounter <= 30; wvarcounter++ )
      {
        if( wobjXMLList.getLength() >= wvarcounter )
        {
          wobjXMLNode = wobjXMLList.item( wvarcounter - 1 );
          wobjDBParm = new Parameter( "@P_COBERCOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_COBERCOD )  ) ) );
          
          //
          
          
          //
          wobjDBParm = new Parameter( "@P_CAPITASG" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_CAPITASG )  ) ) );
          
          //
          
          
          //
          wobjDBParm = new Parameter( "@P_CONTRMOD" + String.valueOf( wvarcounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( XmlDomExtended .getText( wobjXMLNode.selectSingleNode( mcteParam_CONTRMOD )  ) ) );
          
          //
          wobjXMLNode = (org.w3c.dom.Node) null;
        }
        else
        {
          
          
          //
          
          
          //
          
          
          //
          
          
          //
          
          
        }
      }
      //
      wvarStep = 1160;
      //
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      wobjDBCnn.close();
      fncPutAll = true;
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( "//CERTISEC" ) , wobjDBCmd.getParameters().getParameter("@CERTISEC").getValue().toString() );
      pvarRes.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );

      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = null;
      wobjClass = (HSBCInterfaces.IAction) null;
      return fncPutAll;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        pvarRes.set( "<LBA_WS res_code=\"" + General.mcteErrorInesperadoCod + "\" res_msg=\"" + General.mcteErrorInesperadoDescr + "\"></LBA_WS>" );
        wvarCodErr.set( General.mcteErrorInesperadoCod );


        //error: function 'Log' was not found.
        //unsup: vbLogEventTypeError
        fncPutAll = false;
        /*TBD mobjCOM_Context.SetComplete() ;*/

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return fncPutAll;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
