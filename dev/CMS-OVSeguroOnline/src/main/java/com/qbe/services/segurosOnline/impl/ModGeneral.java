package com.qbe.services.segurosOnline.impl;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.mqgeneric.impl.IModGeneral;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import com.qbe.vbcompat.format.VBFixesUtil;

public class ModGeneral implements IModGeneral
{
	
	protected static Logger logger = Logger.getLogger(ModGeneral.class.getName());

  public static final String gcteDBLOG = "lbawA_OfVirtualLBA.udl";
//  Comentado porque no se usa
//  public static final String gcteDBCAM = "camA_OficinaVirtual.udl";
  public static final String gcteDBACTIONS = "lbaw_SCH2Fwk.udl";
  /**
   *  Parametros XML de Configuracion
   */
  public static final String gcteQueueManager = "//QUEUEMANAGER";
  public static final String gctePutQueue = "//PUTQUEUE";
  public static final String gcteGetQueue = "//GETQUEUE";
  public static final String gcteGMOWaitInterval = "//GMO_WAITINTERVAL";
  public static final String wcteProductosHabilitados = "XMLs/ProductosHabilitados.xml";
  public static final String wcteNBWS_TempFilesServer = "NBWS_TempFilesServer.xml";
  public static final String gcteClassMQConnection = "WD.Frame2MQ";

  public String MidAsString( String pvarStringCompleto, Variant pvarActualCounter, int pvarLongitud ) throws Exception
  {
    String MidAsString = "";
    MidAsString = Strings.mid( pvarStringCompleto, pvarActualCounter.toInt(), pvarLongitud );
    pvarActualCounter.set( pvarActualCounter.add( new Variant( pvarLongitud ) ) );
    return MidAsString;
  }

  public String CompleteZero( String pvarString, int pvarLongitud ) throws Exception
  {
    String CompleteZero = "";
    int wvarCounter = 0;
    String wvarstrTemp = "";
    for( wvarCounter = 1; wvarCounter <= pvarLongitud; wvarCounter++ )
    {
      wvarstrTemp = wvarstrTemp + "0";
    }
    CompleteZero = Strings.right( wvarstrTemp + pvarString, pvarLongitud );
    return CompleteZero;
  }

  public String GetErrorInformacionDato( Node pobjXMLContenedor, String pvarPathDato, String pvarTipoDato, Node pobjLongitud, Node pobjDecimales, Node pobjDefault, Node pobjObligatorio ) throws Exception
  {
    String GetErrorInformacionDato = "";
    Node wobjNodoValor = null;
    String wvarDatoValue = "";
    int wvarCounter = 0;
    double wvarCampoNumerico = 0.0;
    boolean wvarIsDatoObligatorio = false;

    if( ! (pobjObligatorio == (Node) null) )
    {
      if( XmlDomExtended.getText( pobjObligatorio ).equals( "SI" ) )
      {
        //Es un Dato Obligatorio
        wvarIsDatoObligatorio = true;
        wobjNodoValor = XmlDomExtended.Node_selectSingleNode(pobjXMLContenedor,"./" + Strings.mid( pvarPathDato, 3 ) ) ;
        if( wobjNodoValor == (Node) null )
        {
          GetErrorInformacionDato = "Nodo Obligatorio " + Strings.mid( pvarPathDato, 3 ) + " NO INFORMADO";
          return GetErrorInformacionDato;
        }
      }
    }

    wobjNodoValor = XmlDomExtended.Node_selectSingleNode(pobjXMLContenedor,"./" + Strings.mid( pvarPathDato, 3 ) ) ;
    if( wobjNodoValor == (Node) null )
    {
      if( ! (pobjDefault == (Node) null) )
      {
        wvarDatoValue = XmlDomExtended.getText( pobjDefault );
      }
    }
    else
    {
      wvarDatoValue = XmlDomExtended.getText( wobjNodoValor );
    }
    //
    
    if( pvarTipoDato.equals( "TEXTO" ) || pvarTipoDato.equals( "TEXTOIZQUIERDA" ) )
    {
      //Dato del Tipo String
      if( wvarIsDatoObligatorio && (Strings.trim( wvarDatoValue ).equals( "" )) )
      {
        GetErrorInformacionDato = "Campo Obligatorio " + Strings.mid( pvarPathDato, 3 ) + " SIN VALOR INGRESADO";
        return GetErrorInformacionDato;
      }
    }
    else if( pvarTipoDato.equals( "ENTERO" ) || pvarTipoDato.equals( "DECIMAL" ) )
    {
      //Dato del Tipo Numerico
      if( wvarDatoValue.equals( "" ) )
      {
        wvarDatoValue = "0";
      }
      if( ! (new Variant( wvarDatoValue ).isNumeric()) )
      {
        GetErrorInformacionDato = "Campo " + Strings.mid( pvarPathDato, 3 ) + " CON FORMATO INVALIDO (Valor Informado: " + wvarDatoValue + ". Requerido: Numerico)";
        return GetErrorInformacionDato;
      }
      else
      {
        if( wvarIsDatoObligatorio && wvarDatoValue.equals("0") )
        {
          GetErrorInformacionDato = "Campo Obligatorio " + Strings.mid( pvarPathDato, 3 ) + " SIN VALOR INGRESADO";
          return GetErrorInformacionDato;
        }
      }
    }
    else if( pvarTipoDato.equals( "FECHA" ) )
    {
      //Dato del Tipo "Fecha" Debe venir del tipo dd/mm/yyyy devuelve YYYYMMDD
      if( wvarIsDatoObligatorio && ! (wvarDatoValue.matches( "*/*/*" )) )
      {
        GetErrorInformacionDato = "Campo Obligatorio " + Strings.mid( pvarPathDato, 3 ) + " VALOR INGRESADO EN FORMATO INVALIDO (Valor Informado: " + wvarDatoValue + ". Requerido: dd/mm/yyyy)";
      }
    }
    //
    ClearObjects: 
    wobjNodoValor = (Node) null;
    return GetErrorInformacionDato;
  }

  public Node GetRequestRetornado( XmlDomExtended pobjXMLRequest, Node pobjXMLRequestDef, String pvarStrRetorno ) throws Exception
  {
    Node GetRequestRetornado = null;
    Node wobjNodoRequestDef = null;
    Node wobjNodoVectorDef = null;
    Node wobjNewNodo = null;
    int pvarStartCount = 0;
    String wvarLastValue = "";
    int wvarCount = 0;

    //Desestimo el Numero de mensaje
    pvarStartCount = 5;
    //
    for( int nwobjNodoRequestDef = 0; nwobjNodoRequestDef < pobjXMLRequestDef.getChildNodes().getLength(); nwobjNodoRequestDef++ )
    {
      wobjNodoRequestDef = pobjXMLRequestDef.getChildNodes().item( nwobjNodoRequestDef );
      //
      if( wobjNodoRequestDef.getAttributes().getNamedItem( "Cantidad" ) == (Node) null )
      {
        //No proceso los vectores del Request
        if( pobjXMLRequest.selectSingleNode( ("//" + XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Nombre" ) )) )  == (Node) null )
        {
          wobjNewNodo = pobjXMLRequest.getDocument().createElement( XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Nombre" ) ) );
          pobjXMLRequest.getDocument().getChildNodes().item( 0 ).appendChild( wobjNewNodo );
        }
        else
        {
          wobjNewNodo = pobjXMLRequest.selectSingleNode( "//" + XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Nombre" ) ) ) ;
        }
        //
        if( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) == (Node) null )
        {
          wvarLastValue = Strings.mid( pvarStrRetorno, pvarStartCount, Obj.toInt( XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Enteros" ) ) ) );
        }
        else
        {
          wvarLastValue = Strings.mid( pvarStrRetorno, pvarStartCount, Obj.toInt( XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Enteros" ) ) + XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) ) ) );
        }
        //
        
         if( XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "TipoDato" ) ).equals( "ENTERO" ) )
        {
        	String newValue = "";
        	try {
				//Esto lo hago, para que solo lo convierta a int, si viene informado, en caso contrario tendrá un 0 por default
				int parsedInt = Integer.parseInt(Strings.trim(wvarLastValue.toString()));
				newValue += parsedInt;
			} catch (NumberFormatException e) {
				newValue += "0";
			}
          XmlDomExtended.setText( wobjNewNodo, newValue);
        }
        else if( XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "TipoDato" ) ).equals( "DECIMAL" ) )
        {
          XmlDomExtended.setText( wobjNewNodo, String.valueOf( VBFixesUtil.val( wvarLastValue ) / Math.pow( 10, VBFixesUtil.val( XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) ) ) ) ) );
        }
        else if( XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "TipoDato" ) ).equals( "FECHA" ) )
        {
          XmlDomExtended.setText( wobjNewNodo, Strings.right( wvarLastValue, 2 ) + "/" + Strings.mid( wvarLastValue, 5, 2 ) + "/" + Strings.left( wvarLastValue, 4 ) );
        }
        else
        {
          XmlDomExtended.setText( wobjNewNodo, Strings.trim( wvarLastValue ) );
        }
        if( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) == (Node) null )
        {
          pvarStartCount = pvarStartCount + Obj.toInt( XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Enteros" ) ) );
        }
        else
        {
          pvarStartCount = pvarStartCount + Obj.toInt( XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Enteros" ) ) ) + Obj.toInt( XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) ) );
        }
      }
      else
      {
        //Salteo todos los registros del vector
        for( wvarCount = 1; wvarCount <= Obj.toInt( XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Cantidad" ) ) ); wvarCount++ )
        {
          for( int nwobjNodoVectorDef = 0; nwobjNodoVectorDef < wobjNodoRequestDef.getChildNodes().getLength(); nwobjNodoVectorDef++ )
          {
            wobjNodoVectorDef = wobjNodoRequestDef.getChildNodes().item( nwobjNodoVectorDef );
            if( wobjNodoVectorDef.getAttributes().getNamedItem( "Decimales" ) == (Node) null )
            {
              pvarStartCount = pvarStartCount + Obj.toInt( XmlDomExtended.getText( wobjNodoVectorDef.getAttributes().getNamedItem( "Enteros" ) ) );
            }
            else
            {
              pvarStartCount = pvarStartCount + Obj.toInt( XmlDomExtended.getText( wobjNodoVectorDef.getAttributes().getNamedItem( "Enteros" ) ) ) + Obj.toInt( XmlDomExtended.getText( wobjNodoVectorDef.getAttributes().getNamedItem( "Decimales" ) ) );
            }
          }
        }
      }
      //
    }
    //
    GetRequestRetornado = pobjXMLRequest.getDocument().getChildNodes().item( 0 );
    wobjNewNodo = (Node) null;
    wobjNodoRequestDef = (Node) null;
    return GetRequestRetornado;
  }

  /**
   * Implementación del Multi simple, que envía varios requests secuencialmente.
   * Además en la respuesta retorna solamente los response
   * 
   * La implementación original de VB era con requests en paralelo pero era una optimización
   * de performance que no sabemos si es necesaria actualmente para los pocos casos que la usan
   * ( sSiniestros )
   */
  public static String cmdp_ExecuteTrnMultiSimple( OSBConnector conn, String pvarActionCode, String pvarSchemaFile, String pvarRequest, StringHolder pvarResponse ) throws Exception
  {
    int wvarCount = 0;
    int i = 0;
    List<String> wvarRequestActionCodes = new ArrayList<String>();
    List<String> wvarRequestRequests = new ArrayList<String>();
    List<String> wvarResponseMultiple = new ArrayList<String>();
    List<String> wvarResultMultiple = new ArrayList<String>();
    List<String> wvarResultMultipleId = new ArrayList<String>();
    List<String> wvarResultMultipleCIAASCOD = new ArrayList<String>();
    int wvarCountOK = 0;
    int wvarCountBAD = 0;
    XmlDomExtended wobjXMLDocRequest = null;
    XmlDomExtended wobjXMLDocResponse = null;
    NodeList objNodeList = null;
    Node wobjNode = null;
    Node nodeActionCode = null;
    org.w3c.dom.Attr nodeAttribute = null;
    String wvarResponseText = "";
    String wvarActionCode = "";


    wobjXMLDocRequest = new XmlDomExtended();
    wobjXMLDocRequest.loadXML( pvarRequest );

    objNodeList = wobjXMLDocRequest.selectNodes( "//Request/Request" ) ;
    wvarCount = objNodeList.getLength();
    if( wvarCount == 0 )
    {
    	//TODO Nada para ejecutar? Devolvemos una respuesta vacía
    	throw new Exception("Branch no implementado: multi sin requests");
    }
    else
    {
      i = 0;
      while( i < wvarCount )
      {
        //
        wobjNode = objNodeList.item( i );
        //
        //Recupera el id para devolverlo en el response
        wvarResultMultipleId.add(XmlDomExtended.getText( wobjNode.getAttributes().getNamedItem( "id" ) ));
        //
        //Si se seteó el ciaascod en el request, lo recupera para devolverlo en el response
        if( ! (wobjNode.getAttributes().getNamedItem( "ciaascod" ) == (Node) null) )
        {
          wvarResultMultipleCIAASCOD.add( XmlDomExtended.getText( wobjNode.getAttributes().getNamedItem( "ciaascod" ) ) );
        }
        else
        {
          wvarResultMultipleCIAASCOD.add(null);
        }
        //
        nodeActionCode = wobjNode.getAttributes().getNamedItem( "actionCode" );
        wvarActionCode = XmlDomExtended.getText( nodeActionCode );
        wvarRequestActionCodes.add(wvarActionCode);
        wvarRequestRequests.add(XmlDomExtended.marshal(wobjNode));
        i = i + 1;
      }

      wvarCountOK = 0;
      wvarCountBAD = 0;
      for (int j = 0; j < wvarCount; j++) {
    	  String actionCode = wvarRequestActionCodes.get(j);
    	  String request = wvarRequestRequests.get(j);
    	  try {
			String wvarResponse = conn.executeRequest(actionCode, request);
			wvarResponseMultiple.add(wvarResponse);
			wvarResultMultiple.add("0");
	        wvarCountOK = wvarCountOK + 1;
		} catch (OSBConnectorException e) {
			wvarResponseMultiple.add(null);
			wvarResultMultiple.add("1");
	        wvarCountBAD = wvarCountBAD + 1;
		}
      }
      
      StringBuffer respBuffer = new StringBuffer();
      respBuffer.append("<Response>\n" );
      respBuffer.append("<count>" );
      respBuffer.append(wvarCount );
      respBuffer.append("</count>\n");
      respBuffer.append("<wvarCountOK>" );
      respBuffer.append(wvarCountOK );
      respBuffer.append("</wvarCountOK>\n");
      respBuffer.append("<wvarCountBAD>" );
      respBuffer.append(wvarCountBAD );
      respBuffer.append("</wvarCountBAD>\n");

      for (int j = 0; j < wvarCount; j++) {
    	  wvarResponseText = wvarResponseMultiple.get(j);
    	  if ( wvarResponseText == null || wvarResponseText.isEmpty()) {
    		  wvarResponseText = "<Response><Estado resultado='false' mensaje='Codigo de Error: " + wvarResultMultiple.get(j) + "'/></Response>";
    	  }

          if ( wvarResultMultipleId.get(j) != null )
          {
            //
            //Carga el XML de respuesta
            wobjXMLDocResponse = new XmlDomExtended();
            wobjXMLDocResponse.loadXML( wvarResponseText );
            //
            //Agrega el atributo id para identificar la ejecución del COM+
            wobjNode = wobjXMLDocResponse.selectSingleNode( "//Response" ) ;
            nodeAttribute = wobjXMLDocResponse.getDocument().createAttribute( "id" );
            nodeAttribute.setValue( wvarResultMultipleId.get(j));
            wobjNode.getAttributes().setNamedItem( nodeAttribute );
            //
            //Agrega el atributo ciaascod que vino en el request para poder identificar a la compañía (NYL / LBA)
            if ( wvarResultMultipleCIAASCOD.get(j) != null )
            {
              nodeAttribute = wobjXMLDocResponse.getDocument().createAttribute( "ciaascod" );
              nodeAttribute.setValue( wvarResultMultipleCIAASCOD.get(j) );
              wobjNode.getAttributes().setNamedItem( nodeAttribute );
            }
            //
            wvarResponseText = XmlDomExtended.marshal(wobjXMLDocResponse.getDocument().getDocumentElement());
          }
          //
          respBuffer.append(wvarResponseText );

      }   
      respBuffer.append("</Response>" );
      pvarResponse.set(respBuffer.toString());
    }
    return pvarResponse.getValue();
  }

  
  
  
  
  /**
   * 
   * 
   * ESTO ESTÁ DESHABILITADO. UNSUPPORTED.
   * 
   * ***************************************************************
   * *** Ejecuta COM+ Multithreading
   * ***************************************************************
   * Ejemplo de pvarRequest:
   * <Request>
   *    <Request id="1" actionCode="lbaw_OVVerifReimpresion" ciaascod="0001"/>
   *    <Request id="2" actionCode="lbaw_OVVerifReimpresion" ciaascod="0001"/>
   *    <Request id="3" actionCode="lbaw_OVVerifReimpresion" ciaascod="0020"/>
   *    <Request id="4" actionCode="lbaw_OVVerifReimpresion" ciaascod="0020"/>
   * </Request>
   * ***************************************************************
   */
  public static String cmdp_ExecuteTrnMulti( String pvarActionCode, String pvarSchemaFile, String pvarRequest, Variant pvarResponse ) throws Exception
  {
	 return "NIET";
	 /*
    String cmdp_ExecuteTrnMulti = "";
    Object wobjCmdProcessor = null;
    WD.CmdProcessorArray wobjCmdProcessorArray = new WD.CmdProcessorArray();
    String wvarRequest = "";
    String wvarExecReturn = "";
    int wvarCount = 0;
    int i = 0;
    Variant wvarRequestMultiple = new Variant();
    Variant wvarResponseMultiple = new Variant();
    Variant wvarResultMultiple = new Variant();
    Variant wvarResultMultipleId = new Variant();
    Variant wvarResultMultipleCIAASCOD = new Variant();
    int wvarCountOK = 0;
    int wvarCountBAD = 0;
    XmlDomExtended wobjXMLDocRequest = null;
    XmlDomExtended wobjXMLDocResponse = null;
    NodeList objNodeList = null;
    Node wobjNode = null;
    Node nodeActionCode = null;
    org.w3c.dom.Attr nodeAttribute = null;
    String wvarResponseText = "";
    String wvarActionCode = "";


    wobjXMLDocRequest = new XmlDomExtended();
    wobjXMLDocRequest.loadXML( pvarRequest );

    objNodeList = wobjXMLDocRequest.selectNodes( "//Request/Request" ) ;
    wvarCount = objNodeList.getLength();
    if( wvarCount == 0 )
    {
      wvarRequest = cmdp_FormatRequest( pvarActionCode, pvarSchemaFile, pvarRequest );
      //
      wobjCmdProcessor = new HSBC_ASP.CmdProcessor();
      //error: function 'Execute' was not found.
      //unsup: wvarExecReturn = wobjCmdProcessor.Execute(pvarRequest, pvarResponse)
      //
    }
    else
    {
      // On Error Resume Next (optionally ignored)
      wvarRequestMultiple.set( (Variant[]) VB.initArray( new Variant[wvarCount+1], Variant.class ) );
      wvarResponseMultiple.set( (Variant[]) VB.initArray( new Variant[wvarCount+1], Variant.class ) );
      wvarResultMultiple.set( (Variant[]) VB.initArray( new Variant[wvarCount+1], Variant.class ) );
      wvarResultMultipleId.set( (Variant[]) VB.initArray( new Variant[wvarCount+1], Variant.class ) );
      wvarResultMultipleCIAASCOD.set( (Variant[]) VB.initArray( new Variant[wvarCount+1], Variant.class ) );

      i = 0;
      while( i < wvarCount )
      {
        //
        wobjNode = objNodeList.item( i );
        Err.clear();
        //
        //Recupera el id para devolverlo en el response
        wvarResultMultipleId.setValueAt( i, new Variant( XmlDomExtended.getText( wobjNode.getAttributes().getNamedItem( "id" ) ) ) );
        //
        //Si se sete� el ciaascod en el request, lo recupera para devolverlo en el response
        if( ! (wobjNode.getAttributes().getNamedItem( "ciaascod" ) == (Node) null) )
        {
          wvarResultMultipleCIAASCOD.setValueAt( i, new Variant( XmlDomExtended.getText( wobjNode.getAttributes().getNamedItem( "ciaascod" ) ) ) );
        }
        else
        {
          wvarResultMultipleCIAASCOD.getValueAt( i ).setNull();
        }
        //
        if( Err.getError().getNumber() != 0 )
        {
          wvarResultMultipleId.getValueAt( i ).setNull();
          wvarResultMultipleCIAASCOD.getValueAt( i ).setNull();
        }
        //
        nodeActionCode = wobjNode.getAttributes().getNamedItem( "actionCode" );
        wvarActionCode = XmlDomExtended.getText( nodeActionCode );
        wvarRequestMultiple.setValueAt( i, new Variant( cmdp_FormatRequest( wvarActionCode, pvarSchemaFile, wobjNode.toString() ) ) );
        i = i + 1;
      }
      //
      wobjCmdProcessorArray = new WD.CmdProcessorArray();
      wvarExecReturn = String.valueOf( wobjCmdProcessorArray.Execute( wvarRequestMultiple, wvarResponseMultiple, wvarResultMultiple ) );
      //
      wvarCountOK = 0;
      wvarCountBAD = 0;
      i = 0;
      while( i < wvarCount )
      {
        if( wvarResultMultiple.getValueAt( i ).toInt() == 0 )
        {
          wvarCountOK = wvarCountOK + 1;
        }
        else
        {
          wvarCountBAD = wvarCountBAD + 1;
        }
        i = i + 1;
      }

      pvarResponse.set( "<Response>" + String.valueOf( (char)(13) ) + String.valueOf( (char)(10) ) );
      pvarResponse.set( pvarResponse + "<count>" );
      pvarResponse.set( String.valueOf( pvarResponse ) + wvarCount );
      pvarResponse.set( pvarResponse + "</count>" + String.valueOf( (char)(13) ) + String.valueOf( (char)(10) ) );
      pvarResponse.set( pvarResponse + "<wvarCountOK>" );
      pvarResponse.set( String.valueOf( pvarResponse ) + wvarCountOK );
      pvarResponse.set( pvarResponse + "</wvarCountOK>" + String.valueOf( (char)(13) ) + String.valueOf( (char)(10) ) );
      pvarResponse.set( pvarResponse + "<wvarCountBAD>" );
      pvarResponse.set( String.valueOf( pvarResponse ) + wvarCountBAD );
      pvarResponse.set( pvarResponse + "</wvarCountBAD>" + String.valueOf( (char)(13) ) + String.valueOf( (char)(10) ) );

      i = 0;
      while( i < wvarCount )
      {
        wvarResponseText = wvarResponseMultiple.getValueAt( i ).toString();
        //
        if( Strings.trim( String.valueOf( wvarResponseText ) ).equals( "" ) )
        {
          wvarResponseText = "<Response><Estado resultado='false' mensaje='Codigo de Error: " + wvarResultMultiple.getValueAt( i ) + "'/></Response>";
        }
        //
        if( ! (wvarResultMultipleId.getValueAt( i ).isNull()) )
        {
          //
          //Carga el XML de respuesta
          wobjXMLDocResponse = new XmlDomExtended();
          wobjXMLDocResponse.loadXML( wvarResponseText );
          //
          //Agrega el atributo id para identificar la ejecuci�n del COM+
          wobjNode = wobjXMLDocResponse.selectSingleNode( "//Response" ) ;
          nodeAttribute = wobjXMLDocResponse.getDocument().createAttribute( "id" );
          nodeAttribute.setValue( wvarResultMultipleId.getValueAt( i ).toString() );
          wobjNode.getAttributes().setNamedItem( nodeAttribute );
          //
          //Agrega el atributo ciaascod que vino en el request para poder identificar a la compa��a (NYL / LBA)
          if( ! (wvarResultMultipleCIAASCOD.getValueAt( i ).isNull()) )
          {
            nodeAttribute = wobjXMLDocResponse.getDocument().createAttribute( "ciaascod" );
            nodeAttribute.setValue( wvarResultMultipleCIAASCOD.getValueAt( i ).toString() );
            wobjNode.getAttributes().setNamedItem( nodeAttribute );
          }
          //
          wvarResponseText = XmlDomExtended.marshal(wobjXMLDocResponse.getDocument().getDocumentElement());
        }
        //
        pvarResponse.set( pvarResponse + wvarResponseText );
        i = i + 1;
      }
      pvarResponse.set( pvarResponse + "</Response>" );
      //
    }
    //
    cmdp_ExecuteTrnMulti = wvarExecReturn;
    //
    wobjXMLDocRequest = null;
    wobjXMLDocResponse = null;
    objNodeList = (NodeList) null;
    wobjNode = (Node) null;
    nodeActionCode = (Node) null;
    wobjCmdProcessor = null;
    wobjCmdProcessorArray = (WD.CmdProcessorArray) null;
    nodeAttribute = (org.w3c.dom.Attr) null;
    //
    return cmdp_ExecuteTrnMulti;
    */
  }

  /**
   * ***************************************************************
   * *** Formatea el documento para la transacción
   * ***************************************************************
   */
  public static String cmdp_FormatRequest( String pvarActionCode, String pvarSchemaFile, String pvarBody ) throws Exception
  {
    String cmdp_FormatRequest = "";
    String wvarRequest = "";
    String wvarSchema = "";
    boolean xXML_UseSchema = false;
    String xXML_PATH = "";
    String xAPP_CODE = "";

    xXML_UseSchema = false;
    //
    //wvarRequest = "<?xml version='1.0' encoding='UTF-8'?>"
    wvarRequest = "";
    wvarSchema = "";
    //
    if( xXML_UseSchema )
    {
      wvarSchema = " xmlns='x-schema:" + xXML_PATH + pvarSchemaFile + "'";
    }
    wvarRequest = wvarRequest + "<HSBC_MSG" + wvarSchema + ">" + "<HEADER>" + "<APPLICATION_CODE>" + xAPP_CODE + "</APPLICATION_CODE>" + "<ACTION_CODE>" + pvarActionCode + "</ACTION_CODE>" + "</HEADER>" + "<BODY>" + pvarBody + "</BODY>" + "</HSBC_MSG>";
    //
    cmdp_FormatRequest = wvarRequest;
    return cmdp_FormatRequest;
  }

  /**
   * *****************************************************************
   * *** Genera un string random, de longitud indicada por parametro.
   * *****************************************************************
   */
  public static String generar_RNDSTR( Variant strLength )
  {
    String generar_RNDSTR = "";
    String strCode = "";
    int nroAux = 0;
    // Esta función genera un string de longitud strLength aleatorio.
    // Si la longitud no se especifica, se asume igual a 4.
    try 
    {


      strCode = "";

      VB.randomize();

      if( strLength.toInt() == 0 )
      {
        strLength.set( 4 );
      }

      while( Strings.len( strCode ) < strLength.toInt() )
      {
        nroAux = (int)Math.rint( Math.floor( (92 * VB.rnd()) + 33 ) );
        if( ((nroAux >= 48) && (nroAux <= 57)) || ((nroAux >= 65) && (nroAux <= 90)) || ((nroAux >= 97) && (nroAux <= 122)) )
        {
          // 48:57 --> 0..9
          // 65:90 --> a..z
          // 97:122 -> A..Z
          strCode = strCode + String.valueOf( (char)(nroAux) );
        }
      }

      generar_RNDSTR = strCode;

      return generar_RNDSTR;

    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        generar_RNDSTR = new Variant().toString();

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return generar_RNDSTR;
  }

  static Variant fncCalculaPeriodos1( Variant pPeriodo1, Variant pPeriodo2, Variant pPeriodo3 ) throws Exception
  {
    Variant fncCalculaPeriodos1 = new Variant();
    //Arma los períodos de los resumenes a mostrar
    pPeriodo1.set( (DateTime.year( DateTime.now() ) - 1) + "1231" );
    pPeriodo2.set( (DateTime.year( DateTime.now() ) - 1) + "0630" );
    pPeriodo3.set( (DateTime.year( DateTime.now() ) - 2) + "1231" );
    //
    return fncCalculaPeriodos1;
  }

  static Variant fncCalculaPeriodos2( Variant pPeriodo1, Variant pPeriodo2, Variant pPeriodo3 ) throws Exception
  {
    Variant fncCalculaPeriodos2 = new Variant();
    //Arma los períodos de los resumenes a mostrar
    pPeriodo1.set( DateTime.year( DateTime.now() ) + "0630" );
    pPeriodo2.set( (DateTime.year( DateTime.now() ) - 1) + "1231" );
    pPeriodo3.set( (DateTime.year( DateTime.now() ) - 1) + "0630" );
    //
    return fncCalculaPeriodos2;
  }

  /**
   * Recupera la descripción del mensaje desde mensajes.xml en base a la clase y cte de mensaje
   */
  public static String getMensaje( String pvarClassName, String pvarMsgCte ) throws Exception
  {
    String getMensaje = "";
    XmlDomExtended wobjXMLMsg = null;

    wobjXMLMsg = new XmlDomExtended();
    wobjXMLMsg.load( Thread.currentThread().getContextClassLoader().getResourceAsStream("XMLs" + File.separator + "Mensajes.xml" ));

    if( ! (wobjXMLMsg.selectSingleNode( ("//CLASE[@nombre='" + pvarClassName + "']/MENSAJE[@ERROR='" + pvarMsgCte + "']") )  == (Node) null) )
    {
      getMensaje = XmlDomExtended.getText( wobjXMLMsg.selectSingleNode( "//CLASE[@nombre='" + pvarClassName + "']/MENSAJE[@ERROR='" + pvarMsgCte + "']" )  );
    }
    else
    {
      getMensaje = "Mensaje de error no definido";
    }
    return getMensaje;
  }
}
