package com.qbe.services.segurosOnline.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * OJO OJO WARNING WARNING
 * Hay dos copias de esta clase, si editamos una actualizar la otra!!!!!
 * 
./CMS-OVSeguroOnline/src/main/java/com/qbe/services/segurosOnline/impl/CapicomServiceClient.java
./CMS-SegurosOnline/src/main/java/com/qbe/services/segurosOnline/impl/CapicomServiceClient.java

 * 
 * @author ramiro
 *
 */
public class CapicomServiceClient {

	private static final String FORM_PARAM_TEXTO = "txtCadena";
	private static final String FORM_PARAM_OPT_ENCRYPT = "optEncrypt";

	private static final String FORM_URL = "capicom.host";
	private static String propsFilename = System.getProperty("cms.config.properties");

	//Para que devuelva un 401 ( falta uno dentro de QBE )
//	private static final String FORM_URL = "http://snoop.git.cloudforge.com/";
	
	//Para que devuelva un 500
//	private static final String FORM_URL = "http://10.1.10.98:8011/SegurosMobile";
	
	//Para que devuelva un 302
//	private static final String FORM_URL = "http://10.1.10.98:7200/em";

	public CapicomServiceClient() {
	}

	public String doOperation(String data, boolean encriptar) throws ClientProtocolException, IOException {
		//El form se llama frmMain
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(loadConfig(FORM_URL));
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair(FORM_PARAM_OPT_ENCRYPT, encriptar ? "S" : "N"));
		nvps.add(new BasicNameValuePair(FORM_PARAM_TEXTO, data));
		httpPost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));
		
		CloseableHttpResponse response = httpclient.execute(httpPost);

		try {
			// FIXME hacer un retry si falla con un 503 o algo que podría ser temporal
			// ver http://stackoverflow.com/questions/22827291/force-retry-on-specific-http-status-code
			
			// Cuando necesitemos autenticación ver https://hc.apache.org/httpcomponents-client-ga/httpclient/examples/org/apache/http/examples/client/ClientAuthentication.java
			if ( response.getStatusLine().getStatusCode() != 200 ) {
				throw new ComponentExecutionException("Fallo en el servicio capicom: " + response.getStatusLine());
			}
		    HttpEntity entity2 = response.getEntity();

		    InputStream is = entity2.getContent();
		    StringWriter writer = new StringWriter();
		    IOUtils.copy(is, writer);
		    String pageContent = writer.toString();
		    //tengo todo el contenido de la página capicom. El valor está entre
		    String startTag = "<textarea name=\"txtCadena\" id=\"txtCadena\" >";
		    // y
		    String endTag = "</textarea>";
		    String result = StringUtils.substringBetween(pageContent, startTag, endTag);
		    // Ensure it is fully consumed
		    EntityUtils.consume(entity2);
		    return result;
		} finally {
		    response.close();
		}
	}
	
	
	private static String loadConfig(String key) throws IOException {
		Properties fileProper = new Properties();
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(propsFilename);
		fileProper.load(in);
		in.close();
		return fileProper.getProperty(key);

	}
	
	
}
