package com.qbe.services.segurosOnline.impl;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mwGenerico.impl.lbaw_MQMW;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
public class NbwsA_Alta  extends BaseOSBClient  implements VBObjectClass
{

	protected static Logger logger = Logger.getLogger(NbwsA_Alta.class.getName());


  static final String mcteEMailTemplateConfig = "NBWS_EnvioMailConfig.xml";
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_Alta";
  static final String mcteParam_MAIL = "//MAIL";
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_DOCUMDAT = "//DOCUMDAT";
  static final String mcteParam_VIAINSCRIPCION = "//VIAINSCRIPCION";
  static final String mcteParam_RESPONSABLEALTA = "//RESPONSABLEALTA";
  static final String mcteParam_CONFORMIDAD = "//CONFORMIDAD";
  static final String mcteParam_DESBLOQUEO = "//DESBLOQUEO";//add
  /**
   * Constantes de XML de definiciones para SQL Generico
   */
  static final String mcteParam_XMLSQLGENREGI = "P_NBWS_Registracion.xml";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_ERROR = 1;
  
  private EventLog mobjEventLog = new EventLog();

  /**
   * Constantes de error (los números matchean los de la tabla SQL tipoAccesos)
   */
  private String[] mcteMsg_DESCRIPTION = new String[13];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();

    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXMLRespuesta1184 = null;
    XmlDomExtended wobjXMLRespuesta1185 = null;
    org.w3c.dom.NodeList wobjXMLProductosAIS_List = null;
    org.w3c.dom.Node wobjXMLProductosAIS_Node = null;
    String wobjRequestSQL = "";
    String wobjResponseSQL = "";
    String wobjRequestAIS = "";
    String wobjResponseAIS = "";
    String wvarMail = "";
    String wvarCAI = "";
    String wvarDocumtip = "";
    String wvarDocumdat = "";
    String wvarRESPONSABLEALTA = "";
    String wvarVIAINSCRIPCION = "";
    String wvarCONFORMIDAD = "";
    String wvarDESBLOQUEO = "";//add
    String wvarError = "";
    String wvarCAIEnc = "";
    String wvarRequest = "";
    String wvarResponse ;
    String wvarRetVal = "";
    String wvarRAMOPCOD = "";
    String wvarPOLIZANN = "";
    String wvarPOLIZSEC = "";
    String wvarCERTIPOL = "";
    String wvarCERTIANN = "";
    String wvarCERTISEC = "";
    String wvarSWSUSCRI = "";
    String wvarSWCONFIR = "";
    String wvarCLAVE = "";
    String wvarSWCLAVE = "";
    String wvarSWTIPOSUS = "";
    //
    //XML con el request
    //
    //
    //PARAMETROS LLAMADA 1185
    // Dim wvarMail            As String
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializacion de variables
      wvarStep = 10;
      wvarError = String.valueOf( mcteMsg_OK );
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_ERROR] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_ERROR" );
      //
      //Carga del REQUEST
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 25;
      wvarMail = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MAIL )  );
      wvarStep = 30;
      wvarDocumtip = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  );
      wvarStep = 35;
      wvarDocumdat = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMDAT )  );
      wvarStep = 40;
      wvarVIAINSCRIPCION = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_VIAINSCRIPCION )  );
      wvarStep = 45;
      wvarRESPONSABLEALTA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RESPONSABLEALTA )  );
      wvarStep = 50;
      wvarCONFORMIDAD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CONFORMIDAD )  );
      wvarStep = 55;//add
      wvarDESBLOQUEO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DESBLOQUEO )  );//add
      
      // ***********************************************************************
      // GED 30/09/2011 - ANEXO I: SE AGREGA ENVIO DE SUSCRIPCION AL AIS
      // ***********************************************************************
      wvarRequest = "<Request>" + "<DOCUMTIP>" + wvarDocumtip + "</DOCUMTIP>" + "<DOCUMDAT>" + wvarDocumdat + "</DOCUMDAT>" + 
      "<MAIL>" + wvarMail + "</MAIL>" + "</Request>";

      wvarStep = 60;

//      wobjClass = new nbwsA_Transacciones.nbwsA_sInicio();
      //error: function 'Execute' was not found.
      //Antes: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
      //Begin convert
      
//	  OSBConnector conn = new OSBConnector();
//	  conn.setServiceURL("http://10.1.10.202:8011/OV/Proxy/QBESvc?wsdl");
//	  this.setOsbConnector(conn);
//	  logger.info("osb Connector: " + getOsbConnector());
//      wvarResponse = getOsbConnector().executeRequest(nbwsA_sInicio.ACTION_CODE, wvarRequest);
      //End convert

      nbwsA_sInicio sinicio = new nbwsA_sInicio();
      StringHolder sh = new StringHolder();
      sinicio.IAction_Execute(wvarRequest, sh, null);
      wvarResponse = sh.getValue();
      
      
      //
      wvarStep = 70;
      //Carga las dos respuestas en un XML.
      wobjXMLRespuesta1184 = new XmlDomExtended();
      wobjXMLRespuesta1184.loadXML(wvarResponse);
      //
      wvarStep = 80;
      //Verifica que no haya pinchado el COM+
      if( wobjXMLRespuesta1184.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null )
      {
        //
        wvarStep = 90;
        //SI NO PUEDO OBTENER RESPUESTA DEL 1184 (FALLO AIS O MQ)
        wvarRetVal = "ERROR";
        wvarError = String.valueOf( mcteMsg_ERROR );

      }

      //Levanta en un listado cada producto del cliente
      wvarStep = 100;
      wobjXMLProductosAIS_List = wobjXMLRespuesta1184.selectNodes( "//Response/PRODUCTO[CIAASCOD='0001' and HABILITADO_EPOLIZA='S' and SWSUSCRI!='S' and MAS_DE_CINCO_CERT='N']" ) ;

      wvarStep = 110;
      // Fin si no tiene pólizas a mandar por 1185
      if( wobjXMLProductosAIS_List.getLength() > 0 )
      {
        wvarStep = 120;
        wvarRequest = "<Request id=\"1\"  actionCode=\"nbwsA_MQGenericoAIS\" >" + "<DEFINICION>LBA_1185_SusyDesus_epoliza.xml</DEFINICION>" + "<EPOLIZAS>";
        wvarStep = 130;

        //Recorre pólizas de clientes.
        for( int nwobjXMLProductosAIS_Node = 0; nwobjXMLProductosAIS_Node < wobjXMLProductosAIS_List.getLength(); nwobjXMLProductosAIS_Node++ )
        {
          wobjXMLProductosAIS_Node = wobjXMLProductosAIS_List.item( nwobjXMLProductosAIS_Node );
          //
          wvarRAMOPCOD = XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "RAMOPCOD") );
          wvarPOLIZANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "POLIZANN") );
          wvarPOLIZSEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "POLIZSEC") );
          wvarCERTIPOL = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "CERTIPOL") );
          wvarCERTIANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "CERTIANN") );
          wvarCERTISEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "CERTISEC" ) );
          // "S"
          wvarSWSUSCRI = "S";
          wvarCLAVE = "";
          wvarSWCLAVE = "";
          // "W" CUANDO ES SUSCRIPCION A SEGUROS ON LINE
          wvarSWTIPOSUS = "W";


          wvarRequest = wvarRequest + "<EPOLIZA><CIAASCOD>0001</CIAASCOD>" + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>" + 
          "<POLIZANN>" + wvarPOLIZANN + "</POLIZANN>" + "<POLIZSEC>" + wvarPOLIZSEC + "</POLIZSEC>" + 
        		  "<CERTIPOL>" + wvarCERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + wvarCERTIANN + "</CERTIANN>" + 
          "<CERTISEC>" + wvarCERTISEC + "</CERTISEC>" + "<SWSUSCRI>" + wvarSWSUSCRI + "</SWSUSCRI>" + 
        		  "<MAIL>" + wvarMail + "</MAIL>" + "<CLAVE>" + wvarCLAVE + "</CLAVE>" + "<SW-CLAVE>" + wvarSWCLAVE + "</SW-CLAVE>" + 
          "<SWTIPOSUS>" + wvarSWTIPOSUS + "</SWTIPOSUS>" + "</EPOLIZA>";

        }

        wvarRequest = wvarRequest + "</EPOLIZAS></Request>";

//  No multi
//        wvarRequest = "<Request>" + wvarRequest + "</Request>";

        wvarStep = 140;

//        ModGeneral.cmdp_ExecuteTrnMulti( "nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse );
       // wvarResponse = getOsbConnector().executeRequest("nbwsA_MQGenericoAIS", wvarRequest);
        

        NBWSA_MQGenericoAIS mqGenericoAIS = new NBWSA_MQGenericoAIS();
        sh = new StringHolder();
        mqGenericoAIS.IAction_Execute(wvarRequest, sh, null);
        wvarResponse = sh.getValue();

        wvarStep = 150;

        wobjXMLRespuesta1185 = new XmlDomExtended();
        wobjXMLRespuesta1185.loadXML( wvarResponse );

        wvarStep = 160;
        if( wobjXMLRespuesta1185.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null )
        {
          //
          wvarStep = 160;
          wvarRetVal = "ERROR";
          wvarError = String.valueOf( mcteMsg_ERROR );
        }
      }


      // ***********************************************************************
      // FIN ANEXO I
      // ***********************************************************************
      //SI DA ERROR LA CONSULTA 1184 O LA ACTUALIZACION 1185
      if( Obj.toInt( wvarError ) == mcteMsg_OK )
      {

        //
        wvarStep = 170;
        wvarCAI = Strings.toUpperCase( ModGeneral.generar_RNDSTR( new Variant( 8 )/*warning: ByRef value change will be lost.*/ ) );
        wvarStep = 180;
        wvarCAIEnc = ModEncryptDecrypt.CapicomEncrypt( wvarCAI );
        //
        wvarStep = 190;
        wvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENREGI + "</DEFINICION>" + "<MAIL>" + wvarMail + "</MAIL>" + 
        "<CAI><![CDATA[" + wvarCAIEnc + "]]></CAI>" + "<DOCUMTIP>" + wvarDocumtip + "</DOCUMTIP>" + "<DOCUMDAT>" + wvarDocumdat + "</DOCUMDAT>" + 
        		"<RESPONSABLEALTA>" + wvarRESPONSABLEALTA + "</RESPONSABLEALTA>" + "<VIAINSCRIPCION>" + wvarVIAINSCRIPCION + "</VIAINSCRIPCION>" + 
        "<CONFORMIDAD>" + wvarCONFORMIDAD + "</CONFORMIDAD>" + "<DESBLOQUEO>" + wvarDESBLOQUEO + "</DESBLOQUEO>" + "</Request>";
        //add desbloqueo
        wvarStep = 200;
        
        logger.log(Level.FINE, "nbwsA_Alta: por llamar a SP de registración: " + wvarRequest);
        nbwsA_SQLGenerico sql2 = new nbwsA_SQLGenerico();
        StringHolder mvarResponseSH = new StringHolder();
        sql2.IAction_Execute(wvarRequest, mvarResponseSH, "" );
        wvarResponse = mvarResponseSH.getValue();
        logger.log(Level.FINE, "nbwsA_Alta: resultado de SP de registración: " + wvarResponse);

        //
        wvarStep = 250;
        wobjXMLResponse = new XmlDomExtended();
        wobjXMLResponse.loadXML( wvarResponse );
        //
        //Analiza resultado del SP
        wvarStep = 260;
        if( ! (wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
        {
          if( XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
          {
            //
            wvarStep = 270;
            if( XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//RESULTADO" )  ).equals( "0" ) )
            {
              wvarRetVal = "OK";
//              fncEnviarMail(wvarMail, wvarCAI, wvarVIAINSCRIPCION);
              fncEnviarMailLocal(wvarMail, wvarCAI, wvarVIAINSCRIPCION, wvarDESBLOQUEO);//add wvarDESBLOQUEO

            }
            else
            {
              wvarRetVal = "ERROR";
              wvarError = String.valueOf( mcteMsg_ERROR );
            }
          }
        }
        //
        //
      }

      wvarStep = 290;
      pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>" + wvarRetVal + "</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "</Response>" );
      //
      //Finaliza y libera objetos
      wvarStep = 700;
      IAction_Execute = 0;
      //
      return IAction_Execute;
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " - " + wvarRequest, vbLogEventTypeError );
        Err.clear();
        throw new ComponentExecutionException(_e_);
    }
  }

  /**
   * GD: 17/11/2009 -  Envio de mail via SOAPMW
   */
 //add pvarDESBLOQUEO
  private boolean fncEnviarMailLocal( String pvarMail, String pvarCAI, String pvarVIAINSCRIPCION, String pvarDESBLOQUEO ) throws Exception
  {
    boolean fncEnviarMail = false;
    Object wobjClass = null;
    XmlDomExtended wvarXMLResponseMdw = null;
    XmlDomExtended wobjXMLConfig = null;
    String wvarResponseMdw = "";
    String mvarRequestMDW = "";
    String mvarTemplateCAI = "";
    String mvarMailPrueba = "";
    String mvarCopiaOculta = "";
    //
    //
    //Se obtienen los parámetros desde XML
    wobjXMLConfig = new XmlDomExtended();
    wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteEMailTemplateConfig));

    if( pvarDESBLOQUEO.equals( "S" ) )//add
    {
      mvarTemplateCAI = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/ENVIOCAP" )  );
    }
    else
    {
        //LR 17/08/2011 diferenciar los mails cuando son por altas desde la OV o desde otro canal
	    if( pvarVIAINSCRIPCION.equals( "OV" ) )
	    {
	      mvarTemplateCAI = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/ENVIOCAIOV" )  );
	    }
	    else
	    {
	      mvarTemplateCAI = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/ENVIOCAI" )  );
	    }
    }

    //GED: para que no envie al mail que viene si no al que se define para pruebas, asi se evita mandar al exterior
    // correos con info no deseada accidentalmente
    if( ! (wobjXMLConfig.selectSingleNode( "//MAILPRUEBA" )  == (org.w3c.dom.Node) null) )
    {
      mvarMailPrueba = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MAILPRUEBA" )  );
    }
    else
    {
      mvarMailPrueba = "";
    }

    if( ! (wobjXMLConfig.selectSingleNode( "//COPIAOCULTA" )  == (org.w3c.dom.Node) null) )
    {
      mvarCopiaOculta = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//COPIAOCULTA" )  );
    }
    else
    {
      mvarCopiaOculta = "";
    }

    MailSender sender = new MailSender();
    boolean result = sender.sendCAIMail(pvarMail, pvarVIAINSCRIPCION, pvarCAI);
    return result;
    
  }

  
  /**
   * GD: 17/11/2009 -  Envio de mail via SOAPMW
   */
  private boolean fncEnviarMail( String pvarMail, String pvarCAI, String pvarVIAINSCRIPCION ) throws Exception
  {
    boolean fncEnviarMail = false;
    Object wobjClass = null;
    XmlDomExtended wvarXMLResponseMdw = null;
    XmlDomExtended wobjXMLConfig = null;
    String wvarResponseMdw = "";
    String mvarRequestMDW = "";
    String mvarTemplateCAI = "";
    String mvarMailPrueba = "";
    String mvarCopiaOculta = "";
    //
    //
    //Se obtienen los parámetros desde XML
    wobjXMLConfig = new XmlDomExtended();
    wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteEMailTemplateConfig));

    //LR 17/08/2011 diferenciar los mails cuando son por altas desde la OV o desde otro canal
    if( pvarVIAINSCRIPCION.equals( "OV" ) )
    {
      mvarTemplateCAI = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/ENVIOCAIOV" )  );
    }
    else
    {
      mvarTemplateCAI = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/ENVIOCAI" )  );
    }

    //GED: para que no envie al mail que viene si no al que se define para pruebas, asi se evita mandar al exterior
    // correos con info no deseada accidentalmente
    if( ! (wobjXMLConfig.selectSingleNode( "//MAILPRUEBA" )  == (org.w3c.dom.Node) null) )
    {
      mvarMailPrueba = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MAILPRUEBA" )  );
    }
    else
    {
      mvarMailPrueba = "";
    }

    if( ! (wobjXMLConfig.selectSingleNode( "//COPIAOCULTA" )  == (org.w3c.dom.Node) null) )
    {
      mvarCopiaOculta = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//COPIAOCULTA" )  );
    }
    else
    {
      mvarCopiaOculta = "";
    }

    mvarRequestMDW = "<Request>" + "<DEFINICION>ismSendPdfReportLBAVO.xml</DEFINICION>" + "<Raiz>share:sendPdfReport</Raiz>" + "<files/>" + 
    "<applicationId>NBWS</applicationId>" + "<reportId/>" + "<recipientTO>" + pvarMail + "</recipientTO>" + "<recipientsCC/>" + 
    		"<recipientsBCC>" + mvarCopiaOculta + "</recipientsBCC>" + "<templateFileName>" + mvarTemplateCAI + "</templateFileName>" + 
    "<parametersTemplate>CLAVE=" + pvarCAI + "</parametersTemplate>" + "<from/>" + "<replyTO/>" + "<bodyText/>" + 
    		"<subject/>" + "<importance/>" + "<imagePathFile/>" + "<attachPassword/>" + "<attachFileName/>" + "</Request>";
    //
    
    logger.log(Level.FINE, "nbwsA_Alta: por enviar mail via MW: " + mvarRequestMDW);

    lbaw_MQMW mqobjClass = new lbaw_MQMW();
    StringHolder mvarResponseSH = new StringHolder();
    mqobjClass.IAction_Execute(mvarRequestMDW, mvarResponseSH, "" );
    wvarResponseMdw = mvarResponseSH.getValue();
    logger.log(Level.FINE, "nbwsA_Alta: respuesta al enviar mail via MW: " + wvarResponseMdw);

    wobjClass = null;
    //
    wvarXMLResponseMdw = new XmlDomExtended();
    wvarXMLResponseMdw.loadXML( wvarResponseMdw );
    //
    if( ! (wvarXMLResponseMdw.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended.getText( wvarXMLResponseMdw.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        if( wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" )  == (org.w3c.dom.Node) null )
        {
          fncEnviarMail = true;
        }
        else
        {
          fncEnviarMail = false;
        }
      }
      else
      {
        if( ! (wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" )  == (org.w3c.dom.Node) null) )
        {
          fncEnviarMail = false;
        }
        else
        {
          fncEnviarMail = false;
        }
        fncEnviarMail = false;
      }
    }
    else
    {
      fncEnviarMail = false;
    }
    //
    wobjClass = null;
    wvarXMLResponseMdw = null;
    wobjXMLConfig = null;

    return fncEnviarMail;
  }

  /**
   * GD 17-11-2009 Función anterior de envio de CAI
   */
  // COMENTADA porque no se usa
  
/*
  private boolean fncEnviarMail_bak( String pvarMail, String pvarCAI ) throws Exception
  {
    boolean fncEnviarMail_bak = false;
    String wvarRequest = "";
    String wvarResponse = "";
    Object wobjClass = null;
    XmlDomExtended wvarXMLRequest = null;
    XmlDomExtended wvarXMLResponse = null;
    XmlDomExtended wobjXMLParametro = null;
    XmlDomExtended wobjXMLCODOPParam = null;
    boolean wvarRellamar = false;
    String wvarResponseAIS = "";
    String wvarCODOPDesc = "";
    //
    //
    wobjXMLParametro = new XmlDomExtended();
    wobjXMLParametro.load( System.getProperty("user.dir") + "\\XMLs\\NBWSEnvioCAI.xml" );
    //
    agregarParametro(wobjXMLParametro, "%%CAI%%", pvarCAI);
    //
    XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//TO" ) , pvarMail );
    //
    wvarRequest = wobjXMLParametro.selectSingleNode( "//Request" ) .toString();
    //
    //NO_HACE_FALTA CONVERTIR, todo este método es dead code 
    //wobjClass = new cam_OficinaVirtual.camA_EnviarMail();
    //error: function 'Execute' was not found.
    //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    wobjClass = null;
    //
    wvarXMLResponse = new XmlDomExtended();
    wvarXMLResponse.loadXML( wvarResponse );
    //
    if( ! (wvarXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended.getText( wvarXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        fncEnviarMail_bak = true;
      }
      else
      {
        fncEnviarMail_bak = false;
      }
    }
    else
    {
      fncEnviarMail_bak = false;
    }
    //
    wvarXMLResponse = null;
    wobjClass = null;
    wobjXMLParametro = null;
    return fncEnviarMail_bak;
  }

  private void agregarParametro( XmlDomExtended pobjXMLParametro, String pvarParam, String pvarValor ) throws Exception
  {
    org.w3c.dom.Element wobjXMLElement = null;
    org.w3c.dom.Element wobjXMLElementAux = null;
    org.w3c.dom.CDATASection wobjXMLElementCda = null;
    XmlDomExtended wobjXMLParametro = null;

    wobjXMLElement = pobjXMLParametro.getDocument().createElement( "PARAMETRO" );
    //
    wobjXMLElementAux = pobjXMLParametro.getDocument().createElement( "PARAM_NOMBRE" );
    wobjXMLElementCda = pobjXMLParametro.getDocument().createCDATASection( pvarParam );
    wobjXMLElementAux.appendChild( wobjXMLElementCda );
    wobjXMLElement.appendChild( wobjXMLElementAux );
    wobjXMLElementAux = pobjXMLParametro.getDocument().createElement( "PARAM_VALOR" );
    wobjXMLElementCda = pobjXMLParametro.getDocument().createCDATASection( pvarValor );
    wobjXMLElementAux.appendChild( wobjXMLElementCda );
    //
    wobjXMLElement.appendChild( wobjXMLElementAux );
    //
    pobjXMLParametro.selectSingleNode( "//PARAMETROS" ).appendChild( wobjXMLElement );
    //
  }
*/
  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
