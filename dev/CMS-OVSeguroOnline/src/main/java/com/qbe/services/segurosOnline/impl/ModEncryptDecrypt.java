package com.qbe.services.segurosOnline.impl;
import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import diamondedge.util.Obj;
import diamondedge.util.Strings;

public class ModEncryptDecrypt
{

  /**
   *  *****************************************************************
   *  Function : CapicomEncrypt
   *  Abstract : Encripta un string.
   *  Synopsis : CapicomEncrypt(ByVal strText As String) As String
   *  *
 * @throws IOException 
 * ****************************************************************
   */
  public static String CapicomEncrypt( String strText ) throws  IOException
  {
	  return (new CapicomServiceClient()).doOperation(strText, true);
  }

  /**
   *  *****************************************************************
   *  Function : CapicomDecrypt
   *  Abstract : Desencripta un string
   *  Synopsis : CapicomDecrypt(ByVal strText As String) As String
   *  *
 * @throws IOException 
 * ****************************************************************
   */
  public static String CapicomDecrypt( String strText ) throws IOException
  {
	  return (new CapicomServiceClient()).doOperation(strText, false);
  }

}
