package com.qbe.services.segurosOnline.impl;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.mwGenerico.impl.lbaw_MQMW;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

import diamondedge.util.Obj;



public class nbwsA_getDenuItem implements VBObjectClass {
	
	protected static Logger logger = Logger.getLogger(nbwsA_getDenuItem.class.getName());

  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_getDenuItem";
  static final String mcteParam_NROSINIESTRO = "//NROSINIESTRO";

  @Override
	public int IAction_Execute(String Request, StringHolder Response,
			String ContextInfo) {
		int IAction_Execute = 0;
		XmlDomExtended wobjXMLRequest = null;
		String wvarNROSINIESTRO = "";

		try {
			// Carga XML de entrada
			wobjXMLRequest = new XmlDomExtended();
			wobjXMLRequest.loadXML(Request);
			wvarNROSINIESTRO = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_NROSINIESTRO));

			int wvarITEM = getItemNumber(wvarNROSINIESTRO);

			// Ahora sí se trae el PDF
			String wvarBASE64 = getPDF(wvarITEM, wvarNROSINIESTRO);
			Response.set("<Response><Estado resultado=\"true\" mensaje=\"\"></Estado>"+ wvarBASE64 + "</Response>");
			IAction_Execute = 0;
			return IAction_Execute;
		} catch (MWException _e_) {
			Response.set("<Response><Estado resultado=\"false\" mensaje=\"" + _e_.getMessage() + "\"></Estado></Response>");
		} catch (XmlDomExtendedException e) {
			throw new ComponentExecutionException(e);
		}
		return IAction_Execute;
	}

	private int getItemNumber(String wvarNROSINIESTRO)
			throws XmlDomExtendedException, MWException {
		XmlDomExtended wobjXMLResponse;
		int wvarUltimo;

		String wvarRequest = "<Request>"
				+ "    <DEFINICION>ismRetrieveReportList.xml</DEFINICION>"
				+ "    <coldViewPubNode>CVUN_RepositoryEstructure/CVUns_Pub_Root/LBAPOLIZAS/LBASiniestros</coldViewPubNode>"
				+ "    <coldViewMetadata>NroSiniestroDef=" + wvarNROSINIESTRO + "</coldViewMetadata>" + "</Request>";

		// Comentado porque esto le agrega el forma
		// wvarRequest = ModGeneral.cmdp_FormatRequest( "lbaw_OVMWGen",
		// "lbaw_OVMWGen.biz", wvarRequest );
		// El original que lo manda por un componente que no migramos
		// wobjCmdProcessor = new HSBC_ASP.CmdProcessor();
		// wvarExecReturn = String.valueOf( wobjCmdProcessor.Execute(
		// wvarRequest, wvarResponse ) );
		//
		// El actionCode lo implementa lbaw_MQMW
		// actionImplMap.put("lbaw_OVMWGen",
		// com.qbe.services.mwGenerico.impl.lbaw_MQMW.class);
		StringHolder retrieveReportListSH = new StringHolder();
		lbaw_MQMW proxyMW = new lbaw_MQMW();
		int resultCode = proxyMW.IAction_Execute(wvarRequest, retrieveReportListSH, "");

		wobjXMLResponse = new XmlDomExtended();
		wobjXMLResponse.loadXML(retrieveReportListSH.toString());

				   
		if (!(wobjXMLResponse.selectSingleNode("Response/Estado/@resultado") == (org.w3c.dom.Node) null)) {
			//FIXME No revisar resultado=true o resultado=false? Sí hacerlo!
			if (!(wobjXMLResponse
					.selectSingleNode("Response/retrieveReportListReturn//item") == (org.w3c.dom.Node) null)) {
				// Recupera el último ITEM (se asume que ese es el PDF a
				// mostrar)
				wvarUltimo = wobjXMLResponse.selectNodes(
						"Response/retrieveReportListReturn//item").getLength();
				return Obj.toInt(XmlDomExtended.getText(wobjXMLResponse
						.selectNodes("Response/retrieveReportListReturn//item")
						.item(wvarUltimo - 1)));
				//
			} else {
				logger.log(Level.SEVERE, "Respuesta de MQMW es: " + retrieveReportListSH);
				String mensaje = XmlDomExtended.getText(wobjXMLResponse.selectSingleNode("Response/Estado/@mensaje"));
				String detalle = XmlDomExtended.getText(wobjXMLResponse.selectSingleNode("Response/Estado"));
				throw new MWException("No se encontraron items en coldview. Motivo: " + mensaje + ". Detalle: " + detalle);
			}
		} else {
			logger.log(Level.SEVERE, "Respuesta de MQMW es: " + retrieveReportListSH);
			throw new MWException("Error al consultar a Coldview. TS: " + System.currentTimeMillis());
		}
	}

	private String getPDF(int wvarITEM, String wvarNROSINIESTRO)
			throws XmlDomExtendedException, MWException {
		int resultCode;
		String wvarRequest = "<Request>"
				+ "    <DEFINICION>ismRetrieveReport.xml</DEFINICION>"
				+ "    <coldViewPubNode>CVUN_RepositoryEstructure/CVUns_Pub_Root/LBAPOLIZAS/LBASiniestros</coldViewPubNode>"
				+ "    <coldViewMetadata>NroSiniestroDef=" + wvarNROSINIESTRO
				+ "</coldViewMetadata>" + "    <item>" + wvarITEM + "</item>"
				+ "</Request>";

		// wvarRequest = ModGeneral.cmdp_FormatRequest( "lbaw_OVMWGen",
		// "lbaw_OVMWGen.biz", wvarRequest );
		//
		StringHolder retrieveReportResponseSH = new StringHolder();
		lbaw_MQMW proxyMWReport = new lbaw_MQMW();
		resultCode = proxyMWReport.IAction_Execute(wvarRequest,
				retrieveReportResponseSH, "");

		XmlDomExtended wobjXMLResponseReport = new XmlDomExtended();
		wobjXMLResponseReport.loadXML(retrieveReportResponseSH.toString());

		if (!(wobjXMLResponseReport.selectSingleNode("Response/Estado/@resultado") == (org.w3c.dom.Node) null)) {
			//FIXME chequear que no sea resultado=false
			if (!(wobjXMLResponseReport.selectSingleNode("//report") == (org.w3c.dom.Node) null)) {
				// Recupera el base 64
				return "<PDFSTREAM>"
						+ XmlDomExtended.getText(wobjXMLResponseReport.selectSingleNode("//report")) + "</PDFSTREAM>";
				//
			} else {
				throw new MWException("No se encontró el PDF en coldview.");
			}
		} else {
			throw new MWException("Error al consultar a Coldview. TS: " + System.currentTimeMillis());
		}
	}


  public void Activate() throws Exception
  {
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
