package com.qbe.services.segurosOnline.impl;
import java.io.File;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Variant;


public class nbwsA_sImpresos extends BaseOSBClient implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_sImpresos";
  /**
   * 
   */
  static final String wcteXSL_sImpresos = "XSLs" + File.separator + "sImpresos.xsl";

  private EventLog mobjEventLog = new EventLog();

  private final String wcteFnName = "IAction_Execute";


  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLRespuestas = null;
    XmlDomExtended wobjXSLSalida = null;
    org.w3c.dom.NodeList wobjXML_PRODUCTOS_List = null;
    org.w3c.dom.Node wobjXML_PRODUCTOS_Node = null;
    org.w3c.dom.Element wobjElemento = null;
    int wvarCount = 0;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse;
    String wvarResponse_HTML = "";
    String wvarResponse_XML = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanta las pólizas a recorrer
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      //Solo pólizas están habilitadas para NBWS y si están habilitadas para ser navegadas en el sitio
      wobjXML_PRODUCTOS_List = wobjXMLRequest.selectNodes( "Request/PRODUCTOS/PRODUCTO[./HABILITADO_NBWS = 'S' and ./HABILITADO_NAVEGACION = 'S' and ./POLIZA_EXCLUIDA = 'N']" ) ;
      wvarRequest = "";
      wvarCount = 1;
      //Recorre cada póliza
      for( int nwobjXML_PRODUCTOS_Node = 0; nwobjXML_PRODUCTOS_Node < wobjXML_PRODUCTOS_List.getLength(); nwobjXML_PRODUCTOS_Node++ )
      {
        wobjXML_PRODUCTOS_Node = wobjXML_PRODUCTOS_List.item( nwobjXML_PRODUCTOS_Node );
        //
        wvarStep = 30;
        //Se fija de qué compañía es la póliza
        if( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_PRODUCTOS_Node, "CIAASCOD" )  ).equals( "0020" ) )
        {
        	throw new Exception("Pólizas de NYL no soportadas");
            // Comentado porque agregamos la exception
          	/*
          wvarStep = 40;
          //Póliza de NYL: Arma XML de entrada a la función Multithreading (muchos request)
          wvarRequest = wvarRequest + "<Request id=\"" + wvarCount + "\" actionCode=\"nbwsA_getResuList\" ciaascod=\"0020\">" + "   <RAMOPCOD>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "RAMOPCOD" )  ) + "</RAMOPCOD>" + "   <POLIZANN>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "POLIZANN" )  ) + "</POLIZANN>" + "   <POLIZSEC>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "POLIZSEC" )  ) + "</POLIZSEC>" + "   <CERTIPOL>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTIPOL" )  ) + "</CERTIPOL>" + "   <CERTIANN>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTIANN" )  ) + "</CERTIANN>" + "   <CERTISEC>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTISEC" )  ) + "</CERTISEC>" + "   <SUPLENUM>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "SUPLENUM" )  ) + "</SUPLENUM>" + "</Request>";
          	 */
        }
        else
        {
          //
          wvarStep = 50;
          //Póliza de LBA: Arma XML de entrada a la función Multithreading (muchos request)
          wvarRequest = wvarRequest + "<Request id=\"" + wvarCount + "\" actionCode=\"lbaw_OVVerifReimpresion\" ciaascod=\"0001\">" + "   <USUARIO></USUARIO>" + 
          "   <RAMOPCOD>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_PRODUCTOS_Node, "RAMOPCOD" )  ) + "</RAMOPCOD>" + 
          "   <POLIZANN>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_PRODUCTOS_Node, "POLIZANN" )  ) + "</POLIZANN>" + 
          "   <POLIZSEC>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_PRODUCTOS_Node, "POLIZSEC" )  ) + "</POLIZSEC>" + 
          "   <CERTIPOL>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_PRODUCTOS_Node, "CERTIPOL" )  ) + "</CERTIPOL>" + 
          "   <CERTIANN>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_PRODUCTOS_Node, "CERTIANN" )  ) + "</CERTIANN>" + 
          "   <CERTISEC>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_PRODUCTOS_Node, "CERTISEC" )  ) + "</CERTISEC>" + 
          "   <SUPLENUM>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_PRODUCTOS_Node, "SUPLENUM" )  ) + "</SUPLENUM>" + "</Request>";
        }
        wvarCount = wvarCount + 1;
        /*unsup wobjXML_PRODUCTOS_List.nextNode() */;
        //
      }
      //
      wvarStep = 60;
      //Ejecuta la función Multithreading
      wvarRequest = "<Request>" + wvarRequest + "</Request>";
      StringHolder wvarResponseSH = new StringHolder();
      ModGeneral.cmdp_ExecuteTrnMultiSimple(getOsbConnector(), "lbaw_OVVerifReimpresion", "lbaw_OVVerifReimpresion.biz", wvarRequest, wvarResponseSH );
      wvarResponse = wvarResponseSH.getValue();

      
      //
      wvarStep = 70;
      //Carga las respuestas
      wobjXMLRespuestas = new XmlDomExtended();
      wobjXMLRespuestas.loadXML( wvarResponse.toString() );
      //
      wvarStep = 80;
      //Verifica si pinchó el COM+
      if( wobjXMLRespuestas.selectSingleNode( "//Response" )  == (org.w3c.dom.Node) null )
      {
        //
        wvarStep = 90;
        //Pinchó el COM+
        String description = "Falló la ejecución de la función Multithreading.";
        Err.getError().setDescription( description);
        //FIXEDunsup GoTo ErrorHandler
        throw new Exception(description);
      }
      else
      {
        //
        //Analiza las respuestas en base al request
        /*unsup wobjXML_PRODUCTOS_List.reset() */;
        wvarCount = 1;
        wvarResponse_HTML = "";
        for( int nwobjXML_PRODUCTOS_Node = 0; nwobjXML_PRODUCTOS_Node < wobjXML_PRODUCTOS_List.getLength(); nwobjXML_PRODUCTOS_Node++ )
        {
          wobjXML_PRODUCTOS_Node = wobjXML_PRODUCTOS_List.item( nwobjXML_PRODUCTOS_Node );
          //
          wvarStep = 100;
          if( wobjXMLRespuestas.selectSingleNode( ("Response/Response[@id='" + wvarCount + "']") )  == (org.w3c.dom.Node) null )
          {
            //
            wvarStep = 110;
            //Pinchó el COM+ para ese PRODUCTO.
            //En este caso el XSL no va a tener el response, con lo cual no dibujará ninguna impresorita.
          }
          else
          {
              //TODO CONVERT Ese clone() tira Ex por agregar un nodo en un doc que no lo creó
              //Lo reemplacé por lo que sigue
//              wobjElemento = (org.w3c.dom.Element) wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='" + wvarCount + "']" ) .cloneNode( true );
//              wobjXML_PRODUCTOS_Node.appendChild( wobjElemento );
              wobjElemento = (org.w3c.dom.Element) wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='" + wvarCount + "']" );
              XmlDomExtended.nodeImportAsChildNode(wobjXML_PRODUCTOS_Node, wobjElemento);
          }
          //
          //Al nodo original le agrega la respuesta
          wvarStep = 130;
          wvarResponse_XML = wvarResponse_XML +XmlDomExtended.marshal(wobjXML_PRODUCTOS_Node);
          wvarCount = wvarCount + 1;
          /*unsup wobjXML_PRODUCTOS_List.nextNode() */;
        }
        //
      }
      //
      wvarStep = 140;
      //Levanta XSL para armar XML de salida
      wobjXSLSalida = new XmlDomExtended();
      wobjXSLSalida.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(wcteXSL_sImpresos));
      //
      wvarStep = 150;
      //Devuelve respuesta en formato HTML
      wobjXMLRespuestas.loadXML( "<PRODUCTOS>" + wvarResponse_XML + "</PRODUCTOS>" );
      wvarResponse_HTML = wobjXMLRespuestas.transformNode( wobjXSLSalida ).toString();
      //
      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"></Estado><Response_HTML><![CDATA[" + wvarResponse_HTML + "]]></Response_HTML></Response>" );
      //
      wvarStep = 190;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        throw new ComponentExecutionException(_e_);
    }
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
