package com.qbe.services.segurosOnline.impl;
import java.io.File;
import java.net.URL;
import java.sql.CallableStatement;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.fileutils.CMSFileUtils;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mwGenerico.impl.lbaw_MQMW;
import com.qbe.vbcompat.base64.Base64Encoding;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.framework.jaxb.Response;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.ado.Command;
import diamondedge.ado.Connection;
import diamondedge.ado.Parameter;
import diamondedge.ado.Recordset;
import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.FileSystem;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;

public class nbwsA_ExeScheduler  extends BaseOSBClient implements VBObjectClass
{

	public static Logger logger = Logger.getLogger(nbwsA_ExeScheduler.class.getName());

  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_ExeScheduler";
  /**
   * Stored Procedures
   */
  static final String mcteStoreProcInsLog = "P_NBWS_INSERT_ENVIOEPOLIZAS_LOG";
  static final String mcteEPolizaConfig = "NBWS_ePolizaConfig.xml";
  static final String mcteOperacion = "EXESCH";

  private EventLog mobjEventLog = new EventLog();
  private NODOSREQUEST mvarNodosRequest = new NODOSREQUEST();

  private final String wcteFnName = "IAction_Execute";

  @Override
  public int IAction_Execute(String pvarRequest, StringHolder pvarResponse,
  		String pvarContextInfo) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    TEMPLATEMAIL mvarTemplate = new TEMPLATEMAIL();
    XmlDomExtended wobjXMLRespuestas = null;
    XmlDomExtended wobjXMLResponseAIS = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLListadoProd = null;
    XmlDomExtended wobjXMLArchivos = null;
    org.w3c.dom.Node wobjXMLNode = null;
    org.w3c.dom.Node wobjXMLArchivo = null;
    XmlDomExtended wobjXMLConfig = null;
    String mvarResponseAIS = "";
    boolean mvarEnvioMDW = false;
    boolean mvarProcConErrores = false;
    boolean mvarErrorRecuperaPDF = false;
    boolean mvarProcesar = false;
    int mvarPendientes = 0;
    String mvarMSGError = "";
    String mvarPathPDF = "";
    String mvarCodError = "";
    String mvarEstadoEnvio = "";
    String mvarEstadoLog = "";
    String wvarPDFBase64 = "";
    String mvarRequestMDW = "";
    String wvarFalseAIS = "";
    String wvarPDFArchivos = "";
    String wvarArchivo = "";
    String wvarNombre = "";
    String mvarTemplateEmail = "";
    String mvarMailPrueba = "";
    String mvarCopiaOculta = "";
    boolean mvarBorrarArchivo = false;
    String mvarTemplateLBAALTAAUTO = "";
    String mvarTemplateLBAENDOSOAUTO = "";
    String mvarTemplateLBACARTERAAUTO = "";
    String mvarTemplateLBAALTANOAUTO = "";
    String mvarTemplateLBAENDOSONOAUTO = "";
    String mvarTemplateLBACARTERANOAUTO = "";
    String mvarTemplateLBAALTANORCP = "";
    String mvarTemplateLBAENDOSONORCP = "";
    //
    //
    //PG - 06-08-2012 RCP1
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicialización de variables
      mvarProcConErrores = false;
      mvarCodError = "";

      wvarStep = 10;
      //pvarRequest = Strings.replace(pvarRequest, "&", "")
      //DoEvents
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 20;

      //Se obtienen los parámetros desde XML
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteEPolizaConfig ));

      wvarStep = 30;
      mvarPathPDF = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//PATHPDF" )  );
      wvarStep = 31;
      mvarTemplate.AUSSYSPASS = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/AUSSYSPASS" )  );
      wvarStep = 32;
      mvarTemplate.HOMSYSPASS = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/HOMSYSPASS" )  );
      wvarStep = 33;
      mvarTemplate.NYLSYSPASS = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/NYLSYSPASS" )  );
      wvarStep = 34;
      mvarTemplate.AUSUSRPASS = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/AUSUSRPASS" )  );
      wvarStep = 35;
      mvarTemplate.HOMUSRPASS = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/HOMUSRPASS" )  );
      wvarStep = 36;
      mvarTemplate.NYLUSRPASS = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/NYLUSRPASS" )  );

      wvarStep = 40;
      // GED: ANEXO I - SE AGREGAN NUEVAS TEMPLATES
      mvarTemplateLBAALTAAUTO = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBAALTAAUTO" )  );
      mvarTemplateLBAENDOSOAUTO = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBAENDOSOAUTO" )  );
      mvarTemplateLBACARTERAAUTO = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBACARTERAAUTO" )  );

      wvarStep = 50;
      mvarTemplateLBAALTANOAUTO = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBAALTANOAUTO" )  );
      mvarTemplateLBAENDOSONOAUTO = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBAENDOSONOAUTO" )  );
      mvarTemplateLBACARTERANOAUTO = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBACARTERANOAUTO" )  );

      //PG - 06-08-2012 RCP1
      wvarStep = 55;
      mvarTemplateLBAALTANORCP = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBAALTARCP" )  );
      mvarTemplateLBAENDOSONORCP = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/LBAENDOSORCP" )  );

      //GED: para que no envie al mail que viene si no al que se define para pruebas, asi se evita mandar al exterior
      // correos con info no deseada accidentalmente
      wvarStep = 60;

      if( ! (wobjXMLConfig.selectSingleNode( "//MAILPRUEBA" )  == (org.w3c.dom.Node) null) )
      {
        mvarMailPrueba = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MAILPRUEBA" )  );
      }
      else
      {
        mvarMailPrueba = "";
      }

      wvarStep = 70;
      if( ! (wobjXMLConfig.selectSingleNode( "//COPIAOCULTA" )  == (org.w3c.dom.Node) null) )
      {
        mvarCopiaOculta = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//COPIAOCULTA" )  );
      }
      else
      {
        mvarCopiaOculta = "";
      }

      wvarStep = 80;

      if( ! (wobjXMLConfig.selectSingleNode( "//BORRARARCHIVO" )  == (org.w3c.dom.Node) null) )
      {
        if( Strings.toUpperCase( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//BORRARARCHIVO" )  ) ).equals( "S" ) )
        {
          mvarBorrarArchivo = true;
        }
        else
        {
          mvarBorrarArchivo = false;
        }
      }
      else
      {
        mvarBorrarArchivo = false;
      }

      //
      wvarStep = 90;
      mvarNodosRequest.CIAASCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CIAASCOD" )  );
      mvarNodosRequest.RAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//RAMOPCOD" )  );
      mvarNodosRequest.POLIZANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//POLIZANN" )  );
      mvarNodosRequest.POLIZSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//POLIZSEC" )  );
      mvarNodosRequest.CERTIPOL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CERTIPOL" )  );
      mvarNodosRequest.CERTIANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CERTIANN" )  );
      mvarNodosRequest.CERTISEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CERTISEC" )  );
      mvarNodosRequest.OPERAPOL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//OPERAPOL" )  );
      mvarNodosRequest.DOCUMTIP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DOCUMTIP" )  );
      mvarNodosRequest.DOCUMDAT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DOCUMDAT" )  );
      mvarNodosRequest.CLIENAP1 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CLIENAP1" )  );
      mvarNodosRequest.CLIENAP2 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CLIENAP2" )  );
      mvarNodosRequest.CLIENNOM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CLIENNOM" )  );
      mvarNodosRequest.NOMARCH = XmlDomExtended.marshal(wobjXMLRequest.selectSingleNode( "//ARCHIVOS" ));
      mvarNodosRequest.FORMUDES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//FORMUDES" )  );
      mvarNodosRequest.SWSUSCRI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//SWSUSCRI" )  );
      if( mvarMailPrueba.equals( "" ) )
      {
        //DA - 20/07/2010: se reemplaza el espacio en blanco por guión bajo ya
        //que hay fallos en las entregas de los emails por este motivo, porque
        //el MQ Genérico que viene del AIS reemplaza los guiones bajos por espacios.
        mvarNodosRequest.MAIL = Strings.replace( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//MAIL" )  ), " ", "_" );
      }
      else
      {
        mvarNodosRequest.MAIL = mvarMailPrueba;
      }

      mvarNodosRequest.CLAVE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CLAVE" )  );
      mvarNodosRequest.SWCLAVE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//SW-CLAVE" )  );
      mvarNodosRequest.SWENDOSO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//SWENDOSO" )  );
      mvarNodosRequest.OPERATIP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//OPERATIP" )  );
      mvarNodosRequest.ESTADO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//ESTADO" )  );
      mvarNodosRequest.DOCUMTIP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DOCUMTIP" )  );
      mvarNodosRequest.CODESTAD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//CODESTAD" )  );


      //GED Asigna template segun producto
      //If Left(mvarNodosRequest.RAMOPCOD, 3) = "AUS" And mvarNodosRequest.SWCLAVE = "S" Then mvarTemplateEmail = mvarTemplate.AUSSYSPASS
      //If Left(mvarNodosRequest.RAMOPCOD, 3) = "AUS" And mvarNodosRequest.SWCLAVE = "U" Then mvarTemplateEmail = mvarTemplate.AUSUSRPASS
      //If Left(mvarNodosRequest.RAMOPCOD, 3) = "HOM" And mvarNodosRequest.SWCLAVE = "S" Then mvarTemplateEmail = mvarTemplate.HOMSYSPASS
      //If Left(mvarNodosRequest.RAMOPCOD, 3) = "HOM" And mvarNodosRequest.SWCLAVE = "U" Then mvarTemplateEmail = mvarTemplate.HOMUSRPASS
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "AUS" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "8" )) )
      {
        mvarTemplateEmail = mvarTemplateLBAALTAAUTO;
      }
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "AUS" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "9" )) )
      {
        mvarTemplateEmail = mvarTemplateLBAENDOSOAUTO;
      }
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "AUS" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "13" )) )
      {
        mvarTemplateEmail = mvarTemplateLBACARTERAAUTO;
      }

      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "HOM" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "8" )) )
      {
        mvarTemplateEmail = mvarTemplateLBAALTANOAUTO;
      }
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "HOM" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "9" )) )
      {
        mvarTemplateEmail = mvarTemplateLBAENDOSONOAUTO;
      }
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "HOM" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "13" )) )
      {
        mvarTemplateEmail = mvarTemplateLBACARTERANOAUTO;
      }

      //PG - 06-08-2012 RCP1
      wvarStep = 95;
      //If Left(mvarNodosRequest.RAMOPCOD, 3) = "RCP" Then
      //    pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /></Response>"
      //    Exit Function
      //End If
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "RCP" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "8" )) )
      {
        mvarTemplateEmail = mvarTemplateLBAALTANORCP;
      }
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "RCP" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "9" )) )
      {
        mvarTemplateEmail = mvarTemplateLBAENDOSONORCP;
      }
      if( (Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "RCP" )) && (Strings.trim( mvarNodosRequest.OPERATIP ).equals( "13" )) )
      {
        mvarTemplateEmail = mvarTemplateLBAENDOSONOAUTO;
      }

      //
      //Registración en Log de inicio de ejecución
      wvarStep = 100;
      logger.fine(String.format("ExeScheduler: Operacion = [%s] I-OVEXESCH - Nueva ejecución - (CLIENTE: [%s] [%s] - IMPRESO: [%s] )", mcteOperacion, mvarNodosRequest.DOCUMDAT , mvarNodosRequest.DOCUMTIP,  mvarNodosRequest.FORMUDES ));
      //
      //
      //En caso de error se resume para evitar el corte del proceso (se realiza control de err.Number)
      //On Error Resume Next
      //
      //Recuperación de ARCHIVO del file server
      wvarPDFBase64 = "";
      mvarErrorRecuperaPDF = false;

      wobjXMLArchivos = new XmlDomExtended();
      wobjXMLArchivos.loadXML( mvarNodosRequest.NOMARCH );
      //
      // ************************************************************************************
      // Recupera del file server c/u de los .pdf
      // *****************************************************************************
      for( int nwobjXMLArchivo = 0; nwobjXMLArchivo < wobjXMLArchivos.selectNodes( "//ARCHIVOS/ARCHIVO" ) .getLength(); nwobjXMLArchivo++ )
      {
        wobjXMLArchivo = wobjXMLArchivos.selectNodes( "//ARCHIVOS/ARCHIVO" ) .item( nwobjXMLArchivo );


        wvarArchivo = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLArchivo, "//NOMARCH" )  );
        wvarNombre = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLArchivo, "//FORMUDES" )  );
        if( Strings.right( wvarNombre, 8 ).equals( "Patronal" ) )
        {
          wvarNombre = "Poliza de RC Patronal"; // Esto es para sacarle el acento a Póliza? - rgm
        }

        wvarStep = 110;


        if( !wvarArchivo.equals( "" ) )
        {
          wvarPDFBase64 = getFileToBase64(mvarPathPDF, wvarArchivo, wvarNombre + ".pdf", mvarBorrarArchivo);
        }
        else
        {
          break;
        }

        wvarStep = 120;

        if( wvarPDFBase64.equals( "" ) )
        {
          mvarErrorRecuperaPDF = true;
          logger.fine("mvarErrorRecuperaPDF = true;");
          break;
        }
        else
        {
          wvarPDFArchivos = wvarPDFArchivos + wvarPDFBase64;
        }
      }

      //Armar Request de envio a MDW
      wvarStep = 130;

      if( ! (mvarErrorRecuperaPDF) )
      {

        //Se cambia envio de mail por medio de MQ.
        if( Strings.left( mvarNodosRequest.RAMOPCOD, 3 ).equals( "RCP" ) )
        {
          mvarRequestMDW = "<Request>" + "<DEFINICION>ismSendPdfReportCAC.xml</DEFINICION>" + "<Raiz>share:sendPdfReport</Raiz>" + "<files><files>" + wvarPDFArchivos + "</files></files>" + "<applicationId>NBWS</applicationId>" + "<reportId/>" + "<recipientTO>" + mvarNodosRequest.MAIL + "</recipientTO>" + "<recipientsCC/>" + "<recipientsBCC>" + mvarCopiaOculta + "</recipientsBCC>" + "<templateFileName>" + mvarTemplateEmail + "</templateFileName>" + "<parametersTemplate/>" + "<from/>" + "<replyTO/>" + "<bodyText/>" + "<subject/>" + "<importance/>" + "<imagePathFile/>" + "<attachPassword/>" + "<attachFileName/>" + "</Request>";
        }
        else
        {
          mvarRequestMDW = "<Request>" + "<DEFINICION>ismSendPdfReportLBAVO.xml</DEFINICION>" + "<Raiz>share:sendPdfReport</Raiz>" + "<files/>" + "<applicationId>NBWS</applicationId>" + "<reportId/>" + "<recipientTO>" + mvarNodosRequest.MAIL + "</recipientTO>" + "<recipientsCC/>" + "<recipientsBCC>" + mvarCopiaOculta + "</recipientsBCC>" + "<templateFileName>" + mvarTemplateEmail + "</templateFileName>" + "<parametersTemplate/>" + "<from/>" + "<replyTO/>" + "<bodyText/>" + "<subject/>" + "<importance/>" + "<imagePathFile/>" + "<attachPassword/>" + "<attachFileName/>" + "</Request>";
        }

        wvarStep = 140;
        // ************************************************************************************
        // Envía mail via MDW
        // *****************************************************************************
        //Este if comentado, xq no manejamos errores de esta manera post mig, así que es dead code. rgm
//        if( Err.getError().getNumber() == 0 )
//        {
          StringHolder mvarMSGErrorHolder = new StringHolder();
          mvarEnvioMDW = enviarXMLaMDW(mvarRequestMDW, mvarMSGErrorHolder);
          mvarMSGError = mvarMSGErrorHolder.getValue();
//        }
//        else
//        {
//          mvarMSGError = Err.getError().getDescription();
//        }
      }
      // ***************************************************************************************
      //Procesa el envio del resultado del envio de mail al AIS mensaje 1188
      // ***************************************************************************************
      wvarStep = 150;

      //Si da error MDW envio estado "F"allo
      mvarEstadoLog = "E";

      if( mvarErrorRecuperaPDF || ! (mvarEnvioMDW) )
      {
        mvarEstadoLog = "P";
      }



      if( mvarErrorRecuperaPDF || ! (mvarEnvioMDW) )
      {
        mvarEstadoEnvio = "F";
      }


      // Si da OK MDW y no hubo ningun otro error "E"nviado
      if( mvarEnvioMDW && (Err.getError().getNumber() == 0) )
      {
        mvarEstadoEnvio = "E";
      }

      wvarStep = 160;
      insertLog(mcteOperacion, 
    		  mvarNodosRequest.RAMOPCOD + "-" + Strings.right( "00" + mvarNodosRequest.POLIZANN, 2) + "-" + Strings.right( "000000" + mvarNodosRequest.POLIZSEC, 6) + "/" + 
      Strings.right( "0000" + mvarNodosRequest.CERTIPOL, 4) + "-" + Strings.right("0000" + mvarNodosRequest.CERTIANN,4) + "-" + Strings.right( "000000" + mvarNodosRequest.CERTISEC, 6), 
    		  mvarNodosRequest.CLIENAP1 + " " + mvarNodosRequest.CLIENAP2 + ", " + mvarNodosRequest.CLIENNOM, 
    		  mvarNodosRequest.MAIL, 
    		  mvarEstadoLog, 
    		  mvarNodosRequest.DOCUMTIP, 
    		  mvarNodosRequest.DOCUMDAT);



      wvarRequest = "";


      wvarStep = 168;
      //Póliza de LBA: Arma XML de entrada a la función Multithreading (muchos request)
      wvarRequest = wvarRequest + "<Request id=\"1\" actionCode=\"nbwsA_MQGenericoAIS\" ciaascod=\"0001\">" + "<DEFINICION>LBA_1188_EnvioPoliza.xml</DEFINICION>" + "<LNK-CIAASCOD>0001</LNK-CIAASCOD>" + "<RAMOPCOD>" + mvarNodosRequest.RAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + mvarNodosRequest.POLIZANN + "</POLIZANN>" + "<POLIZSEC>" + mvarNodosRequest.POLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + mvarNodosRequest.CERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + mvarNodosRequest.CERTIANN + "</CERTIANN>" + "<CERTISEC>" + mvarNodosRequest.CERTISEC + "</CERTISEC>" + "<OPERAPOL>" + mvarNodosRequest.OPERAPOL + "</OPERAPOL>" + "<CODIESTA>" + mvarEstadoEnvio + "</CODIESTA>" + "</Request>";

      wvarStep = 170;
      //No multi
      //Ejecuta la función Multithreading
//      wvarRequest = "<Request>" + wvarRequest + "</Request>";
//      ModGeneral.cmdp_ExecuteTrnMulti( "nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, new Variant( wvarResponse )/*warning: ByRef value change will be lost.*/ );
      wvarResponse = getOsbConnector().executeRequest("nbwsA_MQGenericoAIS", wvarRequest);

      wobjXMLRespuestas = new XmlDomExtended();
      wobjXMLRespuestas.loadXML( wvarResponse );
      //Carga las dos respuestas en un XML.
      if( wobjXMLRespuestas.selectSingleNode( "Response/Estado" )  == (org.w3c.dom.Node) null )
      {
        //
        //Falló Response 1
        Err.getError().setDescription( "Response 1: falló COM+ de Request 1" );
        //FIXEDunsup GoTo ErrorHandler
        throw new Exception();
      }

      wvarStep = 180;


      //Arma un XML con los productos de las dos compañias
      wvarResponse = "";
      //
      wvarStep = 190;
      if( ! (wobjXMLRespuestas.selectSingleNode( "Response/CAMPOS/CANTRECI" )  == (org.w3c.dom.Node) null) )
      {
        wvarResponse = XmlDomExtended.getText(wobjXMLRespuestas.selectSingleNode( "Response/CAMPOS/CANTRECI")) ;
      }
      else
      {
        wvarResponse = "";
        wvarFalseAIS = XmlDomExtended.getText( wobjXMLRespuestas.selectSingleNode( "Response/Estado/@mensaje" )  );
      }
      logger.fine(String.format("ExeScheduler: Response/CAMPOS/CANTRECI wvarResponse = %s", wvarResponse)); 
      
      //
      if( wvarResponse.equals( "0" ) )
      {

        logger.fine(String.format("ExeScheduler: mcteOperacion = [%s] E-Fin OVEXESCH.  Estado: ERROR - No se impacto resultado de envío OK en el AIS", mcteOperacion)); 
      }

      wvarStep = 200;


      if( wvarResponse.equals( "" ) )
      {
        //GoTo FalseAIS
    	//else agregado, es lo que se salteaba el GOTO anterior
      } else {

          //
          //Reestablece el estado de On Error
          //
          //Registración en Log de fin de proceso
          wvarStep = 210;
          if( mvarProcConErrores )
          {
            //insertLog mcteOperacion, "E-Fin EXESCH.  Estado: ERROR - " & mvarMSGError
            pvarResponse.set(Response.marshaledResultadoFalse("Error", mvarMSGError));
          }
          else
          {
            //insertLog mcteOperacion, "0-Fin EXESCH.  Estado: OK"
            pvarResponse.set(Response.marshaledResultadoTrue("OK", ""));
          }
      }
      //FIN
      FalseAIS: 


      //
      wvarStep = 220;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      wvarStep = 230;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        //Inserta error en Log
        //insertLog mcteOperacion, mcteClassName & " - " & _
        //                      wcteFnName & " - " & _
        //error: syntax error: near " & ":
        //unsup: wvarStep & " - " & _
        //insertLog mcteOperacion, "99-Fin EXESCH.  Estado: ABORTADO POR ERROR"
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        pvarResponse.set(Response.marshalWithException("99-Fin EXESCH.  Estado: ABORTADO POR ERROR",_e_));
    }
    return IAction_Execute;
  }


  /**
   * pvarPath es la primer parte de un URL como http://ard807vwncfs o file://ard807vwncfs/PlanetPress
   * 
   * @param pvarPath
   * @param pvarNomArchivo
   * @param pvarFormuDes
   * @param pvarBorrarArchivo
   * @return
   * @throws Exception
   */
  private String getFileToBase64( String pvarPath, String pvarNomArchivo, String pvarFormuDes, boolean pvarBorrarArchivo ) throws Exception
  {
    String getFileToBase64 = "";
    XmlDomExtended wobjXMLImpreso = null;
    org.w3c.dom.Element wobjEle = null;
    byte[] wvarArrBytes = null;
    URL fileURL = new URL(pvarPath + File.separator + pvarNomArchivo);

    //Returns a string representing the name of a file, directory, or folder that matches a specified pattern or file attribute, or the volume label of a drive.
    // 0 = normal file
    //Optional. String expression that specifies a file name, directory or folder name, or drive volume label. A zero-length string ("") is returned if PathName is not found.
    if ( !CMSFileUtils.resourceExists(fileURL) )
    {
      getFileToBase64 = "";
    }
    else
    {
      wobjXMLImpreso = new XmlDomExtended();
      //wobjXMLPdf.loadXML "<PDF xmlns:dt=""urn:schemas-microsoft-com:datatypes"" dt:dt=""bin.base64"">" & pDatos & "</PDF>"
      wobjXMLImpreso.loadXML( "<FileParam></FileParam>" );

      //If this property is set to False, no external includes and imports will be resolved.
      //exensup wobjXMLImpreso.resolveExternals = true;

      wobjEle = wobjXMLImpreso.getDocument().createElement( "content" );

      wobjXMLImpreso.selectSingleNode("//FileParam").appendChild( wobjEle );

      wobjEle.setAttribute( "xmlns:dt", "urn:schemas-microsoft-com:datatypes" );

      //Analizamos la doc y parece q no causa problemas
      //unsup wobjEle.dataType = "bin.base64";

      //Leo el binario y lo guardo en wvarArrBytes
//      wvarFileGet = FileSystem.getFreeFile();
      //error: Opening files for 'Binary' access is not supported.
      //unsup: Open wvarFileName For Binary Access Read As wvarFileGet
//      wvarStep = 320;

//      wvarArrBytes = new byte[FileSystem.getFileLen( wvarFileName ) - 1+1];
      //error: syntax error: near "Get":
      //unsup: Get wvarFileGet, , wvarArrBytes
//      FileSystem.close( wvarFileGet );

      //Cargo el Array en el XML
      //unsup wobjEle.nodeTypedValue = wvarArrBytes;

      wvarArrBytes = CMSFileUtils.loadResource(fileURL);
      XmlDomExtended.setText(wobjEle,Base64Encoding.encode(wvarArrBytes));
      
      wobjEle = wobjXMLImpreso.getDocument().createElement( "name" );
      XmlDomExtended.setText( wobjEle, pvarFormuDes );

      wobjXMLImpreso.selectSingleNode( "//FileParam" ).appendChild( wobjEle );


      getFileToBase64 = XmlDomExtended.marshal(wobjXMLImpreso.getDocument().getDocumentElement());
      //
      //GED  borrar archivo
      //
      if( pvarBorrarArchivo )
      {
    	  //Deshabilitado, no puedo borrar aún
    	  logger.log(Level.WARNING, String.format("Función no implementada: borrado de archivo %s", fileURL));
          //FileSystem.kill( wvarFileName );
      }
    }
    return getFileToBase64;
  }

  private Variant insertLog( String pvarCODOP, String pvarDescripcion, String pvarCliente, String pvarEmail, String pvarEstado, String pvarDocumtip, String pvarDocumdat ) throws Exception
  {
    Variant insertLog = new Variant();
    XmlDomExtended wobjXMLConfig = null;
    boolean wvarReqRespInTxt = false;
    int mvarDebugCode = 0;
    //
    //
    wvarReqRespInTxt = false;
    //
    wobjXMLConfig = new XmlDomExtended();
    wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteEPolizaConfig ));
    //1=Debug to BD - 2=Debug to BD and File sin request/Responses - 3=Debug to BD andFile completo
    mvarDebugCode = Obj.toInt( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//DEBUG" )  ) );
    //
    if( (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<REQUEST>", true ) > 0) || (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<RESPONSE>", true ) > 0) )
    {
      wvarReqRespInTxt = true;
    }
    //
    if( (mvarDebugCode >= 1) && ! (wvarReqRespInTxt) )
    {
      debugToBD(pvarCODOP,pvarDescripcion,pvarCliente,pvarEmail,pvarEstado,pvarDocumtip,pvarDocumdat);
    }
    if( (mvarDebugCode == 3) || ((mvarDebugCode == 2) && ! (wvarReqRespInTxt)) )
    {
    	debugToFile(pvarCODOP,pvarDescripcion,pvarCliente,pvarEmail,pvarEstado,pvarDocumtip,pvarDocumdat);
    }
    //
    return insertLog;
  }

  private boolean debugToBD( String pvarCODOP, String pvarDescripcion, String pvarCliente, String pvarEmail, String pvarEstado, String pvarDocumtip, String pvarDocumdat ) throws Exception
  {
    boolean debugToBD = false;

    java.sql.Connection jdbcConn = null;
    try {
        JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
        jdbcConn = connFactory.getJDBCConnection(ModGeneral.gcteDBLOG);
    
	String sp = String.format("{? = call %s (?,?,?,?,?,?,?)}", mcteStoreProcInsLog);
	CallableStatement cs = jdbcConn.prepareCall(sp);
	
	cs.registerOutParameter(1, Types.INTEGER);
	int paramIndex = 2;
//    wobjDBParm = new Parameter( "@CODOP", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( Strings.left( pvarCODOP, 6 ) ) );
	cs.setString(paramIndex++, Strings.left( pvarCODOP, 6 ));

//    wobjDBParm = new Parameter( "@DESCRIPCION", AdoConst.adChar, AdoConst.adParamInput, 100, new Variant( Strings.left( pvarDescripcion, 100 ) ) );
	cs.setString(paramIndex++, Strings.left( pvarDescripcion, 100 ));

//	wobjDBParm = new Parameter( "@CLIENTE", AdoConst.adChar, AdoConst.adParamInput, 70, new Variant( Strings.left( pvarCliente, 70 ) ) );
	cs.setString(paramIndex++, Strings.left( pvarCliente, 70 ));

//	wobjDBParm = new Parameter( "@EMAIL", AdoConst.adChar, AdoConst.adParamInput, 50, new Variant( Strings.left( pvarEmail, 50 ) ) );
	cs.setString(paramIndex++, Strings.left( pvarEmail, 50 ));
	
//	wobjDBParm = new Parameter( "@ESTADO", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( Strings.left( pvarEstado, 3 ) ) );
	cs.setString(paramIndex++, Strings.left( pvarEstado, 3 ));

//    wobjDBParm = new Parameter( "@DOCUMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( pvarDocumtip ) );
	cs.setInt(paramIndex++, Integer.parseInt(pvarDocumtip));

//    wobjDBParm = new Parameter( "@DOCUMDAT", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( pvarDocumdat ) );
	cs.setInt(paramIndex++, Integer.parseInt(pvarDocumdat));

	cs.execute();

	int returnValue = cs.getInt(1);

    //
    //Controlamos la respuesta del SQL
    if( returnValue >= 0 )
    {
      debugToBD = true;
    }
    else
    {
      debugToBD = false;
    }
    return debugToBD;
    } finally {
        try {
            if (jdbcConn != null)
                jdbcConn.close();
        } catch (Exception e) {
            logger.log(Level.WARNING, "Exception al hacer un close", e);
        }
    }
  }

  private void debugToFile( String pvarCODOP, String pvarDescripcion, String pvarCliente, String pvarEmail, String pvarEstado, String pvarDocumtip, String pvarDocumdat ) throws Exception
  {
    String wvarText = "";
    String wvarFileName = "";
    int wvarNroArch = 0;
    //
    //
    wvarText = "(" + DateTime.now() + "-" + DateTime.now() + "):" + pvarDescripcion + "-" + pvarCliente + "-" + pvarEmail + "-" + pvarEstado + "-" + pvarDocumtip + "-" + pvarDocumdat;

    wvarFileName = "debug-" + pvarCODOP + "-" + DateTime.year( DateTime.now() ) + Strings.right( ("00" + DateTime.month( DateTime.now() )), 2 ) + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) + ".log";
    
    logger.log(Level.FINE, wvarText);
    wvarNroArch = FileSystem.getFreeFile();
    FileSystem.openAppend( "/tmp/" + wvarFileName, 1 );
    FileSystem.out(1).writeValue( wvarText );
    FileSystem.out(1).println();
    FileSystem.close( 1 );
    //
  }

  private boolean enviarXMLaMDW( String pvarRequestMdw, StringHolder pvarMsgErrorHolder ) throws Exception
  {
    boolean enviarXMLaMDW = false;
    VBObjectClass wobjClass = null;
    XmlDomExtended wvarXMLResponseMdw = null;
    String wvarResponseMdw = "";
    //
    wobjClass = new lbaw_MQMW();
    
    StringHolder wvarResponseMdwSH = new StringHolder(); 
//    pvarRequestMdw = pvarRequestMdw.replace("carla.vaccaro@hsbc.com.ar", "ramiro@snoopconsulting.com");
    wobjClass.IAction_Execute(pvarRequestMdw, wvarResponseMdwSH, "");
    wvarResponseMdw = wvarResponseMdwSH.getValue();

    wobjClass = null;
    //
    wvarXMLResponseMdw = new XmlDomExtended();
    wvarXMLResponseMdw.loadXML( wvarResponseMdw );
    //
    if(  (wvarXMLResponseMdw.selectSingleNode( "//Response/Estado/@resultado" )  != (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended.getText( wvarXMLResponseMdw.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        if( wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" )  == (org.w3c.dom.Node) null )
        {
          enviarXMLaMDW = true;
        }
        else
        {
          enviarXMLaMDW = false;
          pvarMsgErrorHolder.set( XmlDomExtended.getText( wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" )  ) );
        }
      }
      else
      {
        if( ! (wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" )  == (org.w3c.dom.Node) null) )
        {
        	pvarMsgErrorHolder.set( XmlDomExtended.getText( wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" )  ) );
        }
        else
        {
        	pvarMsgErrorHolder.set( "Error en la ejecución de LBAA_MWGenerico.lbaw_MQMW: " + XmlDomExtended.marshal(wvarXMLResponseMdw.selectSingleNode( "//Response/Estado" )));
        }
        enviarXMLaMDW = false;
      }
    }
    else
    {
    	pvarMsgErrorHolder.set( "Error en la ejecución de LBAA_MWGenerico.lbaw_MQMW" );
      enviarXMLaMDW = false;
    }

    return enviarXMLaMDW;
  }

  private void ObjectControl_Activate() throws Exception
  {
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }

}
