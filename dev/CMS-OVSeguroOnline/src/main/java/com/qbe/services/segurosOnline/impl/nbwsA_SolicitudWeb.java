package com.qbe.services.segurosOnline.impl;
import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class nbwsA_SolicitudWeb extends BaseOSBClient implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_SolicitudWeb";
  static final String mcteParam_MAIL = "//MAIL";
  static final String mcteParam_DOCUMDAT = "//DOCUMDAT";
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_CONFORMIDAD = "//CONFORMIDAD";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_EXISTEUSR = 1;
  static final int mcteMsg_EXISTEDOC = 2;
  static final int mcteMsg_EXISTEMAIL = 3;
  static final int mcteMsg_NOPRODNBWS = 4;
  static final int mcteMsg_NOPRODNAVEXVEND = 5;
  static final int mcteMsg_NOPRODNAVEXCERT = 6;
  static final int mcteMsg_NOEXISTEUSR = 7;
  static final int mcteMsg_NOEXISTEUSRSQL = 8;
  static final int mcteMsg_NOEXISTEUSRAIS = 9;
  static final int mcteMsg_ERRCONSULTA = 10;
  static final int mcteMsg_ERRCONSULTASQL = 11;
  static final int mcteMsg_ERRCONSULTAAIS = 12;
  static final int mcteMsg_NOPRODNBWS_HAB = 13;
  static final int mcteMsg_NOPRODNBWS_POSITIVEID = 14;
  static final int mcteMsg_POLIZAEXCLUIDA = 15;
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error
   */
  private String[] mcteMsg_DESCRIPTION = new String[16];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    NbwsA_Alta altaComp = new NbwsA_Alta();
    nbwsA_ccBuscaSuscr buscaSuscrComp = null;
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    org.w3c.dom.NodeList wobjNodeList = null;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarMail = "";
    String wvarDocumtip = "";
    String wvarDocumdat = "";
    String wvarCONFORMIDAD = "";
    int wvarError = 0;
    String wvarReturnMsg = "";
    //
    //XML con el request
    //XML con el response del SQL
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializacion de variables
      wvarStep = 10;
      wvarError = mcteMsg_OK;
      wvarReturnMsg = "";
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_EXISTEUSR] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_EXISTEUSR" );
      mcteMsg_DESCRIPTION[mcteMsg_EXISTEDOC] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_EXISTEDOC" );
      mcteMsg_DESCRIPTION[mcteMsg_EXISTEMAIL] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_EXISTEMAIL" );
      mcteMsg_DESCRIPTION[mcteMsg_NOPRODNBWS] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOPRODNBWS" );
      mcteMsg_DESCRIPTION[mcteMsg_NOPRODNAVEXVEND] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOPRODNAVEXVEND" );
      mcteMsg_DESCRIPTION[mcteMsg_NOPRODNAVEXCERT] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOPRODNAVEXCERT" );
      mcteMsg_DESCRIPTION[mcteMsg_NOEXISTEUSR] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOEXISTEUSR" );
      mcteMsg_DESCRIPTION[mcteMsg_NOEXISTEUSRSQL] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOEXISTEUSRSQL" );
      mcteMsg_DESCRIPTION[mcteMsg_NOEXISTEUSRAIS] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOEXISTEUSRAIS" );
      mcteMsg_DESCRIPTION[mcteMsg_ERRCONSULTA] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_ERRCONSULTA" );
      mcteMsg_DESCRIPTION[mcteMsg_ERRCONSULTASQL] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_ERRCONSULTASQL" );
      mcteMsg_DESCRIPTION[mcteMsg_ERRCONSULTAAIS] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_ERRCONSULTAAIS" );
      mcteMsg_DESCRIPTION[mcteMsg_NOPRODNBWS_HAB] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOPRODNBWS_HAB" );
      mcteMsg_DESCRIPTION[mcteMsg_NOPRODNBWS_POSITIVEID] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOPRODNBWS_POSITIVEID" );
      mcteMsg_DESCRIPTION[mcteMsg_POLIZAEXCLUIDA] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_POLIZAEXCLUIDA" );
      //
      //Carga del REQUEST
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //Obtiene parametros
      wvarStep = 30;
      wvarMail = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MAIL )  );
      wvarDocumtip = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  );
      wvarDocumdat = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMDAT )  );
      wvarCONFORMIDAD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CONFORMIDAD )  );
      //
      //Armado del Request para nbwsA_ccBuscaSuscr
      wvarStep = 40;
      wvarRequest = "<Request>" + "<DOCUMTIP>" + wvarDocumtip + "</DOCUMTIP>" + "<DOCUMDAT>" + wvarDocumdat + "</DOCUMDAT>" + "<MAIL>" + wvarMail + "</MAIL>" + "<ACCION>A</ACCION>" + "</Request>";
      //
      wvarStep = 50;
      buscaSuscrComp = new nbwsA_ccBuscaSuscr(); //OK envío local porque el ccBuscaSuscr se procesa siempre en CMS
      buscaSuscrComp.setOsbConnector(getOsbConnector());
      StringHolder shBusca = new StringHolder();
      buscaSuscrComp.IAction_Execute(wvarRequest, shBusca, null);
      wvarResponse = shBusca.getValue();
      
      //
      wvarStep = 60;
      wobjXMLResponse = new XmlDomExtended();
      /*solved wobjXMLResponse.setProperty( "SelectionLanguage", "XPath" ) */;
      wobjXMLResponse.loadXML( wvarResponse );
      //
      //Analiza resultado del componente
      wvarStep = 80;
      if( ! (wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          //
          wvarStep = 90;
          if( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//CODRESULTADO" )  ).equals( "OK" ) )
          {
            //
            //HABILITADO_NBWS = S (habilitado según tabla de prod hab)
            //HABILITADO_NAVEGACION = S (habilitado según vendedores)
            //POLIZA_EXCLUIDA = 'N' (la póliza no está excluída x esquema de encuadradas)
            //MAS_DE_CINCO_CERT = 'N' (la póliza tiene menos de 5 certificados)
            //Con que haya al menos UNA póliza que cumpla con dicha validación se puede dar de alta
            //DA - 12/11/2009: se agrega la validación MAS_DE_CINCO_CERT = 'N'
            wvarStep = 100;
            if( wobjXMLResponse.selectNodes( "//Response_XML/Response/PRODUCTO[HABILITADO_NBWS='S' and substring(HABILITADO_NAVEGACION,1,1)='S' and POLIZA_EXCLUIDA = 'N' and MAS_DE_CINCO_CERT = 'N']" ) .getLength() != 0 )
            {
              //
              //DA - 12/11/2009: se separa la validación del PositiveID (defect 22)
              if( wobjXMLResponse.selectNodes( "//Response_XML/Response/PRODUCTO[HABILITADO_NBWS='S' and substring(HABILITADO_NAVEGACION,1,1)='S' and POLIZA_EXCLUIDA = 'N' and MAS_DE_CINCO_CERT = 'N' and POSITIVEID != '']" ) .getLength() != 0 )
              {
                wvarError = mcteMsg_OK;
              }
              else
              {
                wvarError = mcteMsg_NOPRODNBWS_POSITIVEID;
              }
              //
            }
            else
            {
              wvarError = mcteMsg_NOPRODNBWS_HAB;
            }
            //
          }
          else
          {
            wvarStep = 130;
            wvarError = Obj.toInt( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//CODERROR" )  ) );
          }
        }
      }
      //
      //Si wvarError = mcteMsg_OK entonces procede a dar el alta
      wvarStep = 140;
      if( wvarError == mcteMsg_OK )
      {
        //
        wvarStep = 150;
        wvarRequest = "<Request>" + "<DOCUMTIP>" + wvarDocumtip + "</DOCUMTIP>" + "<DOCUMDAT>" + wvarDocumdat + "</DOCUMDAT>" + "<MAIL>" + wvarMail + "</MAIL>" + "<VIAINSCRIPCION>NBWS</VIAINSCRIPCION>" + "<RESPONSABLEALTA></RESPONSABLEALTA>" + "<CONFORMIDAD>" + wvarCONFORMIDAD + "</CONFORMIDAD>" + "</Request>";
        //
        wvarStep = 160;
        altaComp = new NbwsA_Alta();
        altaComp.setOsbConnector(getOsbConnector());
        StringHolder shAlta = new StringHolder();
        altaComp.IAction_Execute(wvarRequest, shAlta, null);  // Invocación local ( in process ) porque nbwsA_Alta llama a OSB cuando necesita
        wvarResponse = shAlta.getValue();

        //
        wobjXMLResponse = new XmlDomExtended();
        wobjXMLResponse.loadXML( wvarResponse );
        //
        //Analiza resultado del componente
        wvarStep = 170;
        if( ! (wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
        {
          if( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
          {
            //
            wvarStep = 180;
            if( !XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//CODRESULTADO" )  ).equals( "OK" ) )
            {
              wvarStep = 190;
              wvarError = Obj.toInt( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//CODERROR" )  ) );
            }
          }
        }
      }
      //
      wvarStep = 300;
      if( wvarError == mcteMsg_OK )
      {
        wvarStep = 310;
        pvarResponse.set( "<Response>" + "<Estado resultado=\"true\" estado=\"\" mensaje=\"\"/>" + "<CODRESULTADO>OK</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[wvarError] + "</MSGRESULTADO>" + "</Response>" );
      }
      else
      {
        wvarStep = 320;
        pvarResponse.set( "<Response>" + "<Estado resultado=\"true\" estado=\"\" mensaje=\"\"/>" + "<CODRESULTADO>ERROR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[wvarError] + "</MSGRESULTADO>" + "</Response>" );
      }
      //
      //Finaliza y libera objetos
      wvarStep = 500;
      //
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      //Inserta Error en EventViewer
      mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
      Err.clear();
      throw new ComponentExecutionException(_e_);
    }
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
