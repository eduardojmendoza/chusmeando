package com.qbe.services.segurosOnline.impl;
import java.util.logging.Logger;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.Variant;

public class nbwsA_setMisDatos  extends BaseOSBClient implements VBObjectClass
{
	protected static Logger logger = Logger.getLogger(nbwsA_setMisDatos.class.getName());

  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_setMisDatos";
  static final String mcteParam_XMLSQLGENVALUSR = "P_NBWS_ValidaUsuario.xml";
  static final String mcteParam_XMLSQLGENUPDDATPERS = "P_NBWS_UpdateDatosPersonales.xml";
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_DOCUMDAT = "//DOCUMDAT";
  static final String mcteParam_EMAIL_ORIGINAL = "//EMAIL_ORIGINAL";
  static final String mcteParam_EMAIL_NUEVO = "//EMAIL_NUEVO";
  static final String mcteParam_PREGUNTA = "//PREGUNTA";
  static final String mcteParam_RESPUESTA = "//RESPUESTA";
  static final String mcteParam_VALIDAR_USUARIO = "//VALIDAR_USUARIO";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_EXISTEUSR = 1;
 

  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error (los números matchean los de la tabla SQL tipoAccesos)
   */
  private String[] mcteMsg_DESCRIPTION = new String[2];

  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLDoc = null;
    XmlDomExtended wobjXMLRespuesta1184 = null;
    XmlDomExtended wobjXMLRespuesta1185 = null;
    org.w3c.dom.NodeList wobjXMLProductosAIS_List = null;
    org.w3c.dom.Node wobjXMLProductosAIS_Node = null;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = new String();
    String wvarEMAIL_ORIGINAL = "";
    String wvarEMAIL_NUEVO = "";
    String wvarPREGUNTA = "";
    String wvarRespuesta = "";
    String wvarVALIDAR_USUARIO = "";
    int wvarError = 0;
    String wvarExisteUsr = "";
    String wvarDOCUMTIP = "";
    String wvarDOCUMDAT = "";
    String wvarRAMOPCOD = "";
    String wvarPOLIZANN = "";
    String wvarPOLIZSEC = "";
    String wvarCERTIPOL = "";
    String wvarCERTIANN = "";
    String wvarCERTISEC = "";
    String wvarSWSUSCRI = "";
    String wvarSWCONFIR = "";
    String wvarCLAVE = "";
    String wvarSWCLAVE = "";
    String wvarSWTIPOSUS = "";
    //
    //
    //
    //PARAMETROS LLAMADA 1185
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Carga XML de entrada
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_EXISTEUSR] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_EXISTEUSR" );
      wvarError = mcteMsg_OK;
      //
      wvarStep = 20;
      wvarEMAIL_ORIGINAL = Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EMAIL_ORIGINAL )  ) );
      wvarEMAIL_NUEVO = Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EMAIL_NUEVO )  ) );
      wvarPREGUNTA = Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PREGUNTA )  ) );
      wvarRespuesta = Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RESPUESTA )  ) );
      wvarVALIDAR_USUARIO = Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_VALIDAR_USUARIO )  ) );



      wvarDOCUMTIP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  );
      wvarDOCUMDAT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMDAT )  );

      //
      wvarStep = 30;
      if( wvarVALIDAR_USUARIO.equals( "S" ) )
      {
        wvarExisteUsr = fncValidarUsuario(wvarEMAIL_NUEVO);
      }
      else
      {
        //Si la púgina web NO pide validar usuario, es porque el cliente no modificó la dirección de email.
        //Con lo cual simulamos que el cliente no existe para que no arroje error de CLIENTE EXISTENTE
        wvarExisteUsr = "N";
      }
      //
      wvarStep = 35;
      if( wvarExisteUsr.equals( "S" ) )
      {
        wvarError = mcteMsg_EXISTEUSR;
      }
      else
      {
        wvarError = mcteMsg_OK;
      }
      //
      wvarStep = 40;
      if( wvarError == mcteMsg_OK )
      {

        wvarStep = 50;

        // ***********************************************************************
        // GED 30/09/2011 - ANEXO I: SE AGREGA ENVIO DE SUSCRIPCION AL AIS
        // ***********************************************************************
        wvarRequest = "<Request>" + "<DOCUMTIP>" + wvarDOCUMTIP + "</DOCUMTIP>" + "<DOCUMDAT>" + wvarDOCUMDAT + "</DOCUMDAT>" + "<MAIL>" + wvarEMAIL_ORIGINAL + "</MAIL>" + "</Request>";

        wvarStep = 60;

        nbwsA_sInicio sini = new nbwsA_sInicio();
        sini.setOsbConnector(getOsbConnector());
        StringHolder wvarResponseSH = new StringHolder();
        sini.IAction_Execute(wvarRequest, wvarResponseSH, "");
        wvarResponse = wvarResponseSH.getValue();

        //
        wvarStep = 70;
        //Carga las dos respuestas en un XML.
        wobjXMLRespuesta1184 = new XmlDomExtended();
        wobjXMLRespuesta1184.loadXML( wvarResponse.toString() );
        //
        wvarStep = 80;
        //Verifica que no haya pinchado el COM+
        if( wobjXMLRespuesta1184.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null )
        {
          //
          wvarStep = 90;
          //Fallú Response 1
          String description = "Response : falló COM+ de Request 1184";
		Err.getError().setDescription( description );
          //FIXEDunsup GoTo ErrorHandler
          throw new Exception(description);
          //
        }


        //Levanta en un listado cada producto del cliente
        wvarStep = 100;
        wobjXMLProductosAIS_List = wobjXMLRespuesta1184.selectNodes( "//Response/PRODUCTO[CIAASCOD='0001' and HABILITADO_EPOLIZA='S' and SWSUSCRI!='S' and MAS_DE_CINCO_CERT='N']" ) ;
        wvarStep = 110;

        if( wobjXMLProductosAIS_List.getLength() > 0 )
        {

          wvarStep = 120;
          //Recorre pólizas de clientes.
          wvarRequest = "<Request id=\"1\"  actionCode=\"nbwsA_MQGenericoAIS\" >" + "<DEFINICION>LBA_1185_SusyDesus_epoliza.xml</DEFINICION>" + "<EPOLIZAS>";

          wvarStep = 130;

          for( int nwobjXMLProductosAIS_Node = 0; nwobjXMLProductosAIS_Node < wobjXMLProductosAIS_List.getLength(); nwobjXMLProductosAIS_Node++ )
          {
            wobjXMLProductosAIS_Node = wobjXMLProductosAIS_List.item( nwobjXMLProductosAIS_Node );
            //
            wvarRAMOPCOD = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "RAMOPCOD" )  );
            wvarPOLIZANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "POLIZANN" )  );
            wvarPOLIZSEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "POLIZSEC" )  );
            wvarCERTIPOL = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "CERTIPOL" )  );
            wvarCERTIANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "CERTIANN" )  );
            wvarCERTISEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "CERTISEC" )  );
            // "S" PARA LAS ALTAS
            wvarSWSUSCRI = "S";
            wvarCLAVE = "";
            wvarSWCLAVE = "U";
            // "W" CUANDO ES SUSCRIPCION A SEGUROS ON LINE
            wvarSWTIPOSUS = "W";


            wvarRequest = wvarRequest + "<EPOLIZA><CIAASCOD>0001</CIAASCOD>" + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + wvarPOLIZANN + "</POLIZANN>" + "<POLIZSEC>" + wvarPOLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + wvarCERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + wvarCERTIANN + "</CERTIANN>" + "<CERTISEC>" + wvarCERTISEC + "</CERTISEC>" + "<SWSUSCRI>" + wvarSWSUSCRI + "</SWSUSCRI>" + "<MAIL>" + wvarEMAIL_NUEVO + "</MAIL>" + "<CLAVE>" + wvarCLAVE + "</CLAVE>" + "<SW-CLAVE>" + wvarSWCLAVE + "</SW-CLAVE>" + "<SWTIPOSUS>" + wvarSWTIPOSUS + "</SWTIPOSUS>" + "</EPOLIZA>";

          }

          wvarRequest = wvarRequest + "</EPOLIZAS></Request>";
          //Modificado para no usar el cmdp_ExecuteTrnMulti; como manda solamente 1 request, uso el que tengo armado hasta ahora, sin encapsularlo en otro
//          wvarRequest = "<Request>" + wvarRequest + "</Request>";

          wvarStep = 140;
          //Ejecuto vía OSB
//          ModGeneral.cmdp_ExecuteTrnMulti( "nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse );
          wvarResponse = getOsbConnector().executeRequest("nbwsA_MQGenericoAIS", wvarRequest);

          //
          wvarStep = 150;

          wobjXMLRespuesta1185 = new XmlDomExtended();
          wobjXMLRespuesta1185.loadXML( wvarResponse.toString() );

          if( wobjXMLRespuesta1185.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null )
          {
            //
            final String description = "Response : falló COM+ de Request 1185";
			Err.getError().setDescription( description );
            //FIXEDunsup GoTo ErrorHandler
            throw new Exception(description);
            //
          }

        }

        // ***********************************************************************
        // FIN ANEXO I
        // ***********************************************************************
        //
        wvarStep = 160;
        wvarRequest = "<Request><DEFINICION>" + mcteParam_XMLSQLGENUPDDATPERS + "</DEFINICION><MAIL>" + wvarEMAIL_ORIGINAL + "</MAIL><MAILNUEVO>" + wvarEMAIL_NUEVO + "</MAILNUEVO><PREGUNTA>" + ModEncryptDecrypt.CapicomEncrypt( wvarPREGUNTA ) + "</PREGUNTA><RESPUESTA>" + ModEncryptDecrypt.CapicomEncrypt( wvarRespuesta ) + "</RESPUESTA></Request>";
        //
        wvarStep = 170;
        nbwsA_SQLGenerico sql2 = new nbwsA_SQLGenerico();
        sql2.IAction_Execute(wvarRequest, wvarResponseSH, "" );
        wvarResponse = wvarResponseSH.getValue();

        //
        wvarStep = 180;
        wobjXMLResponse = new XmlDomExtended();
        wobjXMLResponse.loadXML( wvarResponse.toString() );
        //
        if( ! (wobjXMLResponse.selectSingleNode( "//Response" )  == (org.w3c.dom.Node) null) )
        {
          wvarStep = 190;

          Response.set( "<Response><Estado resultado=\"true\" mensaje=\"" + mcteMsg_DESCRIPTION[wvarError] + "\"></Estado></Response>" );
        }
        else
        {
          //            Err.Description = "Pinchú el COM+ de SQL Generico: setMisDatos"
          //            GoTo ErrorHandler
          wvarStep = 200;
          Response.set( "<Response><Estado resultado=\"false\" mensaje=\"No es posible continuar con la operaci&oacute;n solicitada. Por favor intente m&aacute;s tarde. Muchas gracias.\"></Estado></Response>" );
        }
        //
      }
      else
      {
        wvarStep = 210;
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"" + mcteMsg_DESCRIPTION[wvarError] + "\"></Estado></Response>" );
      }
      //

      wvarStep = 220;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"" + mcteMsg_DESCRIPTION[wvarError] + "\"></Estado></Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " - " + Request, vbLogEventTypeError );
        Err.clear();
        throw new ComponentExecutionException(_e_);
    }
  }

  private String fncValidarUsuario( String pvarUsuario ) throws Exception
  {
    String fncValidarUsuario = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarRetVal = "";
    XmlDomExtended mobjXMLResponse = null;
    nbwsA_SQLGenerico mobjClass = null;
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENVALUSR + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_SQLGenerico();
    StringHolder mvarResponseSH = new StringHolder();
    mobjClass.IAction_Execute(mvarRequest, mvarResponseSH, "" );
    mvarResponse = mvarResponseSH.getValue();

    mobjClass = null;
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        //
        if( (XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//EXISTE" )  ).equals( "S" )) || (XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//EXISTE" )  ).equals( "N" )) )
        {
          //
          //DA - 09/10/2009: puede que el usuario exista pero estú dado de baja.
          if( !XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//ESTADO" )  ).equals( "A" ) )
          {
            mvarRetVal = "N";
          }
          else
          {
            mvarRetVal = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//EXISTE" )  );
          }
          //
        }
      }
    }
    //
    if( mvarRetVal.equals( "" ) )
    {
      mvarRetVal = "ERR";
    }
    //
    fncValidarUsuario = mvarRetVal;
    mobjClass = null;
    mobjXMLResponse = null;
    //
    return fncValidarUsuario;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
