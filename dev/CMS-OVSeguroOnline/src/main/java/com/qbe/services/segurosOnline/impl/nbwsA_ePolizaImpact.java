package com.qbe.services.segurosOnline.impl;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.Variant;

public class nbwsA_ePolizaImpact  extends BaseOSBClient  implements VBObjectClass
{
	protected static Logger logger = Logger.getLogger(nbwsA_ePolizaImpact.class.getName());

  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_ePolizaImpact";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_CIAASCOD = "//CIAASCOD";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_SWSUSCRI = "//SWSUSCRI";
  static final String mcteParam_MAIL = "//MAIL";
  static final String mcteParam_CLAVE = "//CLAVE";
  static final String mcteParam_SWCLAVE = "//SW-CLAVE";
  /**
   * Parametro XML de Salida
   */
  static final String mcteParam_SWCONFIR = "//SWCONFIR";

  private EventLog mobjEventLog = new EventLog();

  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    Object wobjClass = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLRespuestas = null;
    XmlDomExtended wobjXSLSalida = null;
    XmlDomExtended wobjXMLEpolizas = null;
    org.w3c.dom.NodeList wobjXML_POLIZAS_List = null;
    org.w3c.dom.Node wobjXML_POLIZAS_Node = null;
    org.w3c.dom.Element wobjElemento = null;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarResponse_XML = "";
    String wvarResponse_HTML = "";
    String wvarFalseAIS = "";
    String wvarDocumtip = "";
    String wvarDocumdat = "";
    String wvarCLIENNOM = "";
    String wvarCLIENAPE = "";
    String wvarCIAASCOD = "";
    String wvarRAMOPCOD = "";
    String wvarPOLIZANN = "";
    String wvarPOLIZSEC = "";
    String wvarCERTIPOL = "";
    String wvarCERTIANN = "";
    String wvarCERTISEC = "";
    String wvarSWSUSCRI = "";
    String wvarSWCONFIR = "";
    String wvarMail = "";
    String wvarCLAVE = "";
    String wvarSWCLAVE = "";
    String wvarCONFORME = "";
    String wvarSWTIPOSUS = "";
    int wvarContador = 0;

    //
    //Parámetros de entrada
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Carga XML de entrada
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;

      //
      //
      wvarStep = 30;
      //Arma XML de entrada al COM+ Multithreading
      //
      wobjXML_POLIZAS_List = wobjXMLRequest.selectNodes( "//Request/EPOLIZAS/EPOLIZA[CIAASCOD='0001']" ) ;


      //Recorre cada póliza
      wvarRequest = "";
      wvarContador = 1;

      if( wobjXML_POLIZAS_List.getLength() == 0 )
      {

        wvarContador = 0;

      }
      else
      {


        wvarRequest = wvarRequest + "<Request id=\"" + wvarContador + "\"  actionCode=\"nbwsA_MQGenericoAIS\" ciaascod=\"0001\">" + "<DEFINICION>LBA_1185_SusyDesus_epoliza.xml</DEFINICION>" + "<EPOLIZAS>";

        for( int nwobjXML_POLIZAS_Node = 0; nwobjXML_POLIZAS_Node < wobjXML_POLIZAS_List.getLength(); nwobjXML_POLIZAS_Node++ )
        {
          wobjXML_POLIZAS_Node = wobjXML_POLIZAS_List.item( nwobjXML_POLIZAS_Node );

          wvarRAMOPCOD = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "RAMOPCOD" )  );
          wvarPOLIZANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "POLIZANN" )  );
          wvarPOLIZSEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "POLIZSEC" )  );
          wvarCERTIPOL = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "CERTIPOL" )  );
          wvarCERTIANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "CERTIANN" )  );
          wvarCERTISEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "CERTISEC" )  );
          wvarSWSUSCRI = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "SWSUSCRI" )  );
          wvarMail = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "MAIL" )  );
          wvarCLAVE = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "CLAVE" )  );
          wvarSWCLAVE = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "SW-CLAVE" )  );

          //GED 20-10-2011 - ANEXO I
          wvarSWTIPOSUS = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "SWTIPOSUS" )  );

          wvarRequest = wvarRequest + "<EPOLIZA><CIAASCOD>0001</CIAASCOD>" + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + wvarPOLIZANN + "</POLIZANN>" + "<POLIZSEC>" + wvarPOLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + wvarCERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + wvarCERTIANN + "</CERTIANN>" + "<CERTISEC>" + wvarCERTISEC + "</CERTISEC>" + "<SWSUSCRI>" + wvarSWSUSCRI + "</SWSUSCRI>" + "<MAIL>" + wvarMail + "</MAIL>" + "<CLAVE>" + wvarCLAVE + "</CLAVE>" + "<SW-CLAVE>" + wvarSWCLAVE + "</SW-CLAVE>" + "<SWTIPOSUS>" + wvarSWTIPOSUS + "</SWTIPOSUS></EPOLIZA>";


        }


        wvarRequest = wvarRequest + "</EPOLIZAS></Request>";

      }


      wobjXML_POLIZAS_List = (org.w3c.dom.NodeList) null;

      wobjXML_POLIZAS_List = wobjXMLRequest.selectNodes( "//Request/EPOLIZAS/EPOLIZA[CIAASCOD='0020']" ) ;

      if( wobjXML_POLIZAS_List.getLength() != 0 )
      {
    	  throw new Exception("El componente no está preparado para manejar requests de NYL ( ciaascod=\"0020\" )");
    	  //Lo que sigue comentado xq agregamos la Ex anterior
    	  /*
        wvarContador = wvarContador + 1;


        wvarRequest = wvarRequest + "<Request id=\"" + wvarContador + "\" actionCode=\"nbwsA_MQGenericoAIS\" ciaascod=\"0020\">" + "<DEFINICION>NYL_1185_SusyDesus_epoliza.xml</DEFINICION>" + "<EPOLIZAS>";


        for( int nwobjXML_POLIZAS_Node = 0; nwobjXML_POLIZAS_Node < wobjXML_POLIZAS_List.getLength(); nwobjXML_POLIZAS_Node++ )
        {
          wobjXML_POLIZAS_Node = wobjXML_POLIZAS_List.item( nwobjXML_POLIZAS_Node );

          wvarRAMOPCOD = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "RAMOPCOD" )  );
          wvarPOLIZANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "POLIZANN" )  );
          wvarPOLIZSEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "POLIZSEC" )  );
          wvarCERTIPOL = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "CERTIPOL" )  );
          wvarCERTIANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "CERTIANN" )  );
          wvarCERTISEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "CERTISEC" )  );
          wvarSWSUSCRI = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "SWSUSCRI" )  );
          wvarMail = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "MAIL" )  );
          wvarCLAVE = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "CLAVE" )  );
          wvarSWCLAVE = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "SW-CLAVE" )  );

          wvarRequest = wvarRequest + "<EPOLIZA><CIAASCOD>0020</CIAASCOD>" + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + wvarPOLIZANN + "</POLIZANN>" + "<POLIZSEC>" + wvarPOLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + wvarCERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + wvarCERTIANN + "</CERTIANN>" + "<CERTISEC>" + wvarCERTISEC + "</CERTISEC>" + "<SWSUSCRI>" + wvarSWSUSCRI + "</SWSUSCRI>" + "<MAIL>" + wvarMail + "</MAIL>" + "<CLAVE>" + wvarCLAVE + "</CLAVE>" + "<SW-CLAVE>" + wvarSWCLAVE + "</SW-CLAVE></EPOLIZA>";

        }

        wvarRequest = wvarRequest + "</EPOLIZAS></Request>";
*/
      }

      //No multi
//      wvarRequest = "<Request>" + wvarRequest + "</Request>";

      wvarStep = 40;
      //Ejecuta la función Multithreading
//      ModGeneral.cmdp_ExecuteTrnMulti( "nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, new Variant( wvarResponse )/*warning: ByRef value change will be lost.*/ );

      logger.log(Level.FINE, "nbwsA_ePolizaImpact: Enviando a OSB" + wvarRequest);
      wvarResponse = getOsbConnector().executeRequest("nbwsA_MQGenericoAIS", wvarRequest);

      //
      wvarStep = 50;
      //Carga las dos respuestas en un XML.
      wobjXMLRespuestas = new XmlDomExtended();
      wobjXMLRespuestas.loadXML( wvarResponse );
      //
      wvarStep = 60;

      //Verifica que no haya pinchado el COM+
      // Era esto, lo cambié porque ya no tenemos multithreading
      //      if( wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/Estado" )  == (org.w3c.dom.Node) null )
      if( wobjXMLRespuestas.selectSingleNode( "Response/Estado" )  == (org.w3c.dom.Node) null )
      {
        //
        //Falló Response 1
        Err.getError().setDescription( "Response 1: falló COM+ de Request 1" );
        throw new Exception("Response 1: falló COM+ de Request 1");
        //
      }

      wvarStep = 70;
      if( wvarContador == 2 )
      { //Nunca puede entrar acá si hacemos un solo request
    	  throw new Exception("Can't happen!");
    	  /*
        if( wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='2']/Estado" )  == (org.w3c.dom.Node) null )
        {
          //
          //Falló Response 2
          Err.getError().setDescription( "Response 2: falló COM+ de Request 2" );
          //unsup GoTo ErrorHandler
          //
        }
        */
      }
      //Anduvieron los dos llamados al AIS.
      //Arma un XML con los productos de las dos compañas
      wvarResponse = "";
      //
      wvarStep = 80;
      if( ! (wobjXMLRespuestas.selectSingleNode( "Response/CAMPOS/EPOLIZAS" )  == (org.w3c.dom.Node) null) )
      {
        wvarResponse = wvarResponse + XmlDomExtended.marshal(wobjXMLRespuestas.selectSingleNode( "Response/CAMPOS/EPOLIZAS" ));
      }
      else
      {
        wvarResponse = wvarResponse + "";
        wvarFalseAIS = XmlDomExtended.getText( wobjXMLRespuestas.selectSingleNode( "Response/Estado/@mensaje" )  );
      }
      //
      wvarStep = 90;
      if( wvarContador == 2 )
      { //Nunca puede entrar acá si hacemos un solo request
    	  throw new Exception("Can't happen!");
    	  /*
        if( ! (wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='2']/CAMPOS/EPOLIZAS" )  == (org.w3c.dom.Node) null) )
        {
          wvarResponse = wvarResponse + wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='2']/CAMPOS/EPOLIZAS" ) .toString();
        }
        else
        {
          wvarResponse = wvarResponse + "";
          wvarFalseAIS = wvarFalseAIS + String.valueOf( (char)(13) ) + XmlDomExtended.getText( wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='2']/Estado/@mensaje" )  );
        }
        */
      }
      if( wvarResponse.equals( "" ) )
      {
        //FIXEDunsup GoTo FalseAIS
          Response.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarFalseAIS + "\"></Estado><Response_XML></Response_XML><Response_HTML>" + wvarFalseAIS + "</Response_HTML></Response>" );
          IAction_Execute = 0;
          return IAction_Execute;

      }

      wvarResponse = "<Response>" + Strings.replace( Strings.replace( wvarResponse, "<EPOLIZAS>", "" ), "</EPOLIZAS>", "" ) + "</Response>";

      wvarStep = 100;

      wobjXMLEpolizas = new XmlDomExtended();
      wobjXMLEpolizas.loadXML( wvarResponse );


      wobjXML_POLIZAS_List = wobjXMLEpolizas.selectNodes( "Response/EPOLIZA" ) ;

      wvarResponse_XML = "<EPOLIZAS>";
      wvarStep = 110;
      for( int nwobjXML_POLIZAS_Node = 0; nwobjXML_POLIZAS_Node < wobjXML_POLIZAS_List.getLength(); nwobjXML_POLIZAS_Node++ )
      {
        wobjXML_POLIZAS_Node = wobjXML_POLIZAS_List.item( nwobjXML_POLIZAS_Node );

        wvarCIAASCOD = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "CIAASCOD" )  );
        wvarRAMOPCOD = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "RAMOPCOD" )  );
        wvarPOLIZANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "POLIZANN" )  );
        wvarPOLIZSEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "POLIZSEC" )  );
        wvarCERTIPOL = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "CERTIPOL" )  );
        wvarCERTIANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "CERTIANN" )  );
        wvarCERTISEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "CERTISEC" )  );
        wvarSWCONFIR = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXML_POLIZAS_Node, "SWCONFIR" )  );

        wvarStep = 120;

        if( (!Strings.trimRight( Strings.trimLeft( wvarCIAASCOD ) ).equals( "" )) && (!Strings.trimRight( Strings.trimLeft( wvarRAMOPCOD ) ).equals( "" )) )
        {
          wvarResponse_XML = wvarResponse_XML + "<EPOLIZA>" + "<CIAASCOD>" + wvarCIAASCOD + "</CIAASCOD>" + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + wvarPOLIZANN + "</POLIZANN>" + "<POLIZSEC>" + wvarPOLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + wvarCERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + wvarCERTIANN + "</CERTIANN>" + "<CERTISEC>" + wvarCERTISEC + "</CERTISEC>" + "<SWCONFIR>" + wvarSWCONFIR + "</SWCONFIR>" + "</EPOLIZA>";

          //Si se confirma la recepción del AIS grabo en el SQL
          wvarCONFORME = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//EPOLIZA[RAMOPCOD='" + wvarRAMOPCOD + "' and POLIZANN='" + wvarPOLIZANN + "' and POLIZSEC='" + wvarPOLIZSEC + "' and CERTIPOL='" + wvarCERTIPOL + "' and CERTIANN='" + wvarCERTIANN + "' and CERTISEC='" + wvarCERTISEC + "']/CONFORME" )  );

          if( (wvarSWCONFIR.equals( "S" )) && (wvarCONFORME.equals( "S" )) )
          {
            wvarDocumtip = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//EPOLIZA[RAMOPCOD='" + wvarRAMOPCOD + "' and POLIZANN='" + wvarPOLIZANN + "' and POLIZSEC='" + wvarPOLIZSEC + "' and CERTIPOL='" + wvarCERTIPOL + "' and CERTIANN='" + wvarCERTIANN + "' and CERTISEC='" + wvarCERTISEC + "']/DOCUMTIP" )  );
            wvarDocumdat = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//EPOLIZA[RAMOPCOD='" + wvarRAMOPCOD + "' and POLIZANN='" + wvarPOLIZANN + "' and POLIZSEC='" + wvarPOLIZSEC + "' and CERTIPOL='" + wvarCERTIPOL + "' and CERTIANN='" + wvarCERTIANN + "' and CERTISEC='" + wvarCERTISEC + "']/DOCUMDAT" )  );
            wvarMail = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//EPOLIZA[RAMOPCOD='" + wvarRAMOPCOD + "' and POLIZANN='" + wvarPOLIZANN + "' and POLIZSEC='" + wvarPOLIZSEC + "' and CERTIPOL='" + wvarCERTIPOL + "' and CERTIANN='" + wvarCERTIANN + "' and CERTISEC='" + wvarCERTISEC + "']/MAIL" )  );
            wvarCLIENNOM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//EPOLIZA[RAMOPCOD='" + wvarRAMOPCOD + "' and POLIZANN='" + wvarPOLIZANN + "' and POLIZSEC='" + wvarPOLIZSEC + "' and CERTIPOL='" + wvarCERTIPOL + "' and CERTIANN='" + wvarCERTIANN + "' and CERTISEC='" + wvarCERTISEC + "']/CLIENNOM" )  );
            wvarCLIENAPE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//EPOLIZA[RAMOPCOD='" + wvarRAMOPCOD + "' and POLIZANN='" + wvarPOLIZANN + "' and POLIZSEC='" + wvarPOLIZSEC + "' and CERTIPOL='" + wvarCERTIPOL + "' and CERTIANN='" + wvarCERTIANN + "' and CERTISEC='" + wvarCERTISEC + "']/CLIENAPE" )  );

            wvarStep = 130;

            wvarRequest = "<Request>" + "<DEFINICION>P_NBWS_Suscripciones_Insert.xml</DEFINICION>" + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + wvarPOLIZANN + "</POLIZANN>" + "<POLIZSEC>" + wvarPOLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + wvarCERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + wvarCERTIANN + "</CERTIANN>" + "<CERTISEC>" + wvarCERTISEC + "</CERTISEC>" + "<SUPLENUM>0</SUPLENUM>" + "<TIPO>EPOL</TIPO>" + "<DOCUMTIP>" + wvarDocumtip + "</DOCUMTIP>" + "<DOCUMDAT>" + wvarDocumdat + "</DOCUMDAT>" + "<EMAIL>" + wvarMail + "</EMAIL>" + "<VIA_SUSCRIPC>NBWS</VIA_SUSCRIPC>" + "<NOMBRE>" + wvarCLIENNOM + "</NOMBRE>" + "<APELLIDO>" + wvarCLIENAPE + "</APELLIDO>" + "</Request>";

            wvarStep = 140;

            nbwsA_SQLGenerico mobjClass = new nbwsA_SQLGenerico();
            StringHolder wvarResponseSH = new StringHolder();
            mobjClass.IAction_Execute(wvarRequest, wvarResponseSH, "" );
            wvarResponse = wvarResponseSH.getValue();

          }
        }
      }
      wvarStep = 150;

      wvarResponse_XML = wvarResponse_XML + "</EPOLIZAS>";

      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"></Estado><Response_XML>" + wvarResponse_XML + "</Response_XML><Response_HTML><![CDATA[" + wvarResponse_HTML + "]]></Response_HTML></Response>" );


      FalseAIS: 
      if( wvarResponse.equals( "" ) )
      {
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarFalseAIS + "\"></Estado><Response_XML></Response_XML><Response_HTML>" + wvarFalseAIS + "</Response_HTML></Response>" );
      }

      // On Error Resume Next (optionally ignored)
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;

    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " - " + wvarRequest, vbLogEventTypeError );
        Err.clear();
        throw new ComponentExecutionException(_e_);
    }
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
