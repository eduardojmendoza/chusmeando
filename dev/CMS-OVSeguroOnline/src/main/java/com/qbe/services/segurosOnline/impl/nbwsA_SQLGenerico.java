package com.qbe.services.segurosOnline.impl;
import java.io.File;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.Element;

import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

import diamondedge.ado.AdoConst;
import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;

public class nbwsA_SQLGenerico implements VBObjectClass
{
	
	protected static Logger logger = Logger.getLogger(nbwsA_SQLGenerico.class.getName());
	
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_SQLGenerico";
  static final String mcteSubDirName = "DefinicionesSQL";
  static final String mcteLogPath = "LogSQL";

  private EventLog mobjEventLog = new EventLog();
  private String mvarConsultaRealizada = "";

  
  /**
   * XML con constantes de ADO para tipos de dato
   */
  private XmlDomExtended wobjCnstADO = null;

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLOldLog = null;
    XmlDomExtended wobjXMLLog = null;
    int wvarStep = 0;
    XmlDomExtended wobjXMLDefinition = null;			//XML de definición
    XmlDomExtended wobjXMLRequest = null;				    //XML con el request
    org.w3c.dom.NodeList wobjXMLRecordsets = null;		    //Nodos descriptores de recordsets
    org.w3c.dom.Node wobjNodoRec = null;			    //Nodo descriptor de un recordset
    String wvarDefinitionFile = "";			    //nombre del archivo XML de definición
    String wvarStoredProcedure = "";			//nombre del stored procedure
    String wvarTipoSP = "";						//tipo de store procedure. C (consulta) o ABM
    String wvarUDL = "";						//nombre del archivo UDL
    String wobjSalida = "";						//String de salida
    String wvarPathLog = "";					//Para LOGs
    java.util.Date wvarFechaConsulta = DateTime.EmptyDate;
    int wvarCantLlamadasLogueadas = 0;
    int wvarCantLlamadasALoguear = 0;
    int wvarCounter = 0;
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
    	//Agregado porque no tenemos el framework de activación, si no lo pongo acá nadie lo llama
    	this.Activate();
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarFechaConsulta = DateTime.now();
      //
      //carga del REQUEST
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );

      //Carga del XML de definición
      wvarStep = 20;
      wobjXMLDefinition = new XmlDomExtended();
      wvarDefinitionFile = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DEFINICION" )  );
      mvarConsultaRealizada = Strings.left( wvarDefinitionFile, Strings.len( wvarDefinitionFile ) - 4 );

      wobjXMLDefinition.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteSubDirName + File.separator + wvarDefinitionFile ));

      //Carga del nombre del SP
      wvarStep = 30;
      wvarStoredProcedure = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/NOMBRE" )  );

      //Carga del tipo de SP
      wvarStep = 40;
      wvarTipoSP = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/TIPO" )  );

      //Carga del UDL
      wvarStep = 50;
      wvarUDL = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/UDL" )  );

      //FIXME Acá chequear que haya levantado un valor del udl, y si no tirar una Exception xq no se va a poder coenctar.
      
      //selección del tipo de interface de conexión dependiendo de si el SP hace altas, modificaciones o bajas o no
      //TODO CONVERT En el original usa un componente que maneja "transacciones" para el primer caso: HSBC.DBConnectionTrx. No sé cual es la diferencia,
      // nosotros usamos este que soporta ambas.
      wvarStep = 60;
      
//      if( wvarTipoSP.equals( "ABM" ) )
      //FIXME Si el tipo es ABM o AB, no debería hacer un commit? rgm
      
      //conexión a la base de datos
      java.sql.Connection jdbcConn = null;
      try {
          JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
          jdbcConn = connFactory.getJDBCConnection(wvarUDL);


      //'''''''''''''''''''''''''''
      //armado de la llamada al SP'
      //'''''''''''''''''''''''''''
      wvarStep = 80;
      CallableStatement cs = generarCommand(jdbcConn, wvarStoredProcedure, wobjXMLDefinition.selectNodes( "//ENTRADA/PARAMETRO" ), 
    		  wobjXMLRequest.selectSingleNode( "Request" ) );


      boolean result = cs.execute();
      
      if (result) {
    	  //El primero es un ResultSet
      } else {
    	  //Salteo el "updateCount"
          int uc = cs.getUpdateCount();
          if (uc  == -1) {
//        	  System.out.println("No hay resultados");
          }
          result = cs.getMoreResults();
      }

      ResultSet cursor = cs.getResultSet();

      //''''''''''''''''''''
      //armado de la salida'
      //''''''''''''''''''''
      //Carga del nodos descriptores de recordsets
      wvarStep = 100;
      wobjXMLRecordsets = wobjXMLDefinition.selectNodes( "//SALIDA/RECORDSET" ) ;

      wvarStep = 110;
      wobjSalida = "";

      //por cada recordset definido en el XML de definición procesa un recordset devuelto por la consulta
      for( int nwobjNodoRec = 0; nwobjNodoRec < wobjXMLRecordsets.getLength(); nwobjNodoRec++ )
      {
        wobjNodoRec = wobjXMLRecordsets.item( nwobjNodoRec );
        //si hay menos recordsets que los indicados en la definición tira un error
        
        if( cursor == null )
        {
          throw new Exception( "2504 - ERROR EN XML DE DEFINICION - se definieron más recordsets que la cantidad devuelta por el stored procedure" );
        }
        //Lo casteo a element, porque lo trata como tal. Esperemos que no explote
        wobjSalida = wobjSalida + 
        		generarXMLSalida(cursor, (Element)wobjNodoRec);
        
        boolean moreResults = cs.getMoreResults();

        if ((moreResults == false) && (cs.getUpdateCount() == -1)) {
        	//No hay más
        }
        
        cursor = cs.getResultSet();
      }

      wvarStep = 120;
      if( ! (wobjXMLDefinition.selectSingleNode( "//RETORNARREQUEST" )  == (org.w3c.dom.Node) null) )
      {
        wobjSalida = wobjSalida + Request;
      }

      wvarStep = 130;
      Response.set( etiquetar("Response",wobjSalida) );

      
      // ****************************************************************
      //Generamos un Log en caso que se solicite
      // ****************************************************************
      //
      // FIXME No generamos el log por ahora
      if( 0==1 && (! ((wobjXMLRequest.selectSingleNode( "//GENERARLOG" )  == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//GENERARLOG" )  == (org.w3c.dom.Node) null)) ))
      {
        //
        wvarStep = 135;
        wvarCantLlamadasALoguear = 1;
        if( ! (wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "CantLlamadasALoguear" ) == (org.w3c.dom.Node) null) )
        {
          wvarCantLlamadasALoguear = Obj.toInt( XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "CantLlamadasALoguear" ) ) );
        }
        //
        wvarStep = 140;
        wvarPathLog = System.getProperty("user.dir") + "\\" + mcteLogPath + "\\" + wvarDefinitionFile; //FIXME
        //
        wvarStep = 150;
        if( ! (wobjXMLDefinition.selectSingleNode( "//GENERARLOG" )  == (org.w3c.dom.Node) null) )
        {
          if( ! (wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
          {//FIXME
            wvarPathLog = System.getProperty("user.dir") + "\\" + mcteLogPath + "\\" + XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) );
          }
        }
        else
        {
          if( ! (wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
          {
            wvarPathLog = System.getProperty("user.dir") + "\\" + mcteLogPath + "\\" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) );
          }
        }
        //
        wvarStep = 160;
        if( ! (wvarPathLog.matches( "*\\" )) )
        {

          //Genero el Log solo si en el nodo se especifico un nombre de archivo
          wobjXMLLog = new XmlDomExtended();
          //
          wvarStep = 170;
          wobjXMLLog.loadXML( "<LOGs><LOG StoredProcedure='" + wvarStoredProcedure + "' InicioConsulta='" + DateTime.format( wvarFechaConsulta, "dd/MM/yyyy HH:mm:ss" ) + "' TiempoIncurrido = '" + (int)Math.rint( (DateTime.diff( wvarFechaConsulta, DateTime.now() ) * 86400) ) + " Seg' >" + Request + Response + "</LOG></LOGs>" );
          //
          wvarStep = 180;
          //
          if( !"" /*unsup this.Dir( wvarPathLog, 0 ) */.equals( "" ) )
          {
            wvarStep = 190;
            //Ya existe el Log, entonces agrego la nueva consulta al LOG
            wobjXMLOldLog = new XmlDomExtended();
            wobjXMLOldLog.load( wvarPathLog );
            wvarStep = 200;
            if( wobjXMLOldLog.selectSingleNode( "//LOGs" )  == (org.w3c.dom.Node) null )
            {
              // *********LOG NO VALIDO***********
              wobjXMLLog.save( wvarPathLog );
            }
            else
            {
              //''''                    wvarStep = 210
              //''''                    wobjXMLOldLog.selectSingleNode("//LOGs").appendChild wobjXMLLog.selectSingleNode("//LOGs").childNodes(0)
              //''''                    wvarStep = 220
              //''''                    wobjXMLOldLog.Save wvarPathLog
              //
              //DA
              wvarStep = 210;
              wvarCantLlamadasLogueadas = wobjXMLOldLog.selectSingleNode( "//LOGs" ) .getChildNodes().getLength();
              for( wvarCounter = 1; wvarCounter <= ((wvarCantLlamadasLogueadas - wvarCantLlamadasALoguear) + 1); wvarCounter++ )
              {
                wobjXMLOldLog.selectSingleNode( "//LOGs" ).removeChild( wobjXMLOldLog.selectSingleNode( "//LOGs" ) .getChildNodes().item( 0 ) );
              }
              //
              //DA
              wvarStep = 220;
              wobjXMLOldLog.selectSingleNode( "//LOGs" ).appendChild( wobjXMLLog.selectSingleNode( "//LOGs" ) .getChildNodes().item( 0 ) );
              //DA
              wvarStep = 225;
              wobjXMLOldLog.save( wvarPathLog );
              //
            }
          }
          else
          {
            wvarStep = 230;
            wobjXMLLog.save( wvarPathLog );
          }
        }
      }

      // ****************************************************************
      //FIN generación de LOG
      // ****************************************************************
      } finally {
          try {
              if (jdbcConn != null)
                  jdbcConn.close();
          } catch (Exception e) {
              logger.log(Level.WARNING, "Exception al hacer un close", e);
          }
      }
      IAction_Execute = 0;
      return IAction_Execute;
    } catch( Exception e ) {
        logger.log(Level.SEVERE, this.getClass().getName(), e);
        throw new ComponentExecutionException(e);
    }
  }

  private CallableStatement generarCommand( java.sql.Connection jdbcConn, String pobjStoredProcedure, org.w3c.dom.NodeList pobjNodos, org.w3c.dom.Node pobjXMLRequest ) throws SQLException, XmlDomExtendedException, SQLGenericoException
  {
    int wvarStep = 0;
    org.w3c.dom.Node wobjNodeCampo = null;
    String wvarNombrePrm = "";
    int wvarTipoPrm = 0;
    String wvarValuePrm = "";

      wvarStep = 10;
      
      StringBuffer spSB = new StringBuffer();
      spSB.append("{call " + pobjStoredProcedure + "(");
      if ( pobjNodos.getLength() > 0 ) {
          for( int i = 0; i < pobjNodos.getLength() - 1; i++ ) {
        	  spSB.append("?, "); 
          }
          spSB.append("?");
      }
      spSB.append(")}");
      
      String sp = spSB.toString();
      CallableStatement ps = jdbcConn.prepareCall(sp);


      for( int nwobjNodeCampo = 0; nwobjNodeCampo < pobjNodos.getLength(); nwobjNodeCampo++ )
      {
        wobjNodeCampo = pobjNodos.item( nwobjNodeCampo );

        wvarStep = 20;
        wvarNombrePrm = XmlDomExtended.getText( wobjNodeCampo.getAttributes().getNamedItem( "Nombre" ) );

        wvarStep = 30;
        wvarTipoPrm = Obj.toInt( XmlDomExtended.getText( wobjCnstADO.selectSingleNode( "//DATATYPE[@name=\"" + XmlDomExtended.getText( wobjNodeCampo.getAttributes().getNamedItem( "Type" ) ) + "\"]" )  ) );

        wvarStep = 40;

        if( ! (wobjNodeCampo.getAttributes().getNamedItem( "Xml" ) == (org.w3c.dom.Node) null) )
        {
          wvarValuePrm = XmlDomExtended.marshal( XmlDomExtended.Node_selectSingleNode(pobjXMLRequest, wvarNombrePrm ) ); // era toString()
        }
        else
        {
          if( XmlDomExtended.Node_selectSingleNode(pobjXMLRequest, wvarNombrePrm )  == (org.w3c.dom.Node) null )
          {
            wvarValuePrm = wobjNodeCampo.getAttributes().getNamedItem( "Default" ).getNodeValue();
          }
          else
          {
            wvarValuePrm = XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(pobjXMLRequest, wvarNombrePrm));
          }
        }

        int jdbcParamIndex = nwobjNodeCampo + 1;
        
        switch (wvarTipoPrm) {
		case AdoConst.adNumeric:
		case AdoConst.adDecimal:
			ps.setDouble(jdbcParamIndex, Double.parseDouble(wvarValuePrm));
			break;

		case AdoConst.adLongVarChar:
		case AdoConst.adVarChar:
		case AdoConst.adChar:
			ps.setString(jdbcParamIndex, wvarValuePrm);
			break;
			
		case AdoConst.adSmallInt :
			ps.setInt(jdbcParamIndex, Integer.parseInt(wvarValuePrm));
			break;
			
		default:
			throw new SQLGenericoException("Tipo no soportado: " + wvarTipoPrm + " valor: " + wvarValuePrm);
		}
      }

      ps.setMaxRows(0);
      return ps;
  }

  private String generarXMLSalida( ResultSet cursor, Element pobjNodo )
  {
    String generarXMLSalida = "";
    int wvari = 0;
    int wvarStep = 0;
    org.w3c.dom.NodeList wobjNodosCampos = null;
    org.w3c.dom.Node wobjNodoCampo = null;
    String wobjNomRecordset = "";
    String wobjNomFila = "";
    String wobjNomCampo = "";
    String wobjAux = "";
    String wobjAuxRegs = "";
    int wvar = 0;

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wobjAux = "";

      wobjNomRecordset = XmlDomExtended.getText( pobjNodo.getAttributes().getNamedItem( "Nombre" ) );
      wobjNomFila = XmlDomExtended.getText( pobjNodo.getAttributes().getNamedItem( "NombreFila" ) );

      wobjNodosCampos = XmlDomExtended.Node_selectNodes(pobjNodo, "//RECORDSET[@Nombre=\"" + wobjNomRecordset + "\"]/CAMPO" ) ;

      wvarStep = 10;
      //procesa cada registro del recordset
      while( cursor.next() )
      {
        //si no coincide la cantidad de campos del recordset con la cantidad de campos definidas en el XML tira error
        if( wobjNodosCampos.getLength() != cursor.getMetaData().getColumnCount() )
        {
          throw new Exception("2504 - ERROR EN XML DE DEFINICION - No coincide la cantidad de campos devueltos por el stored procedure (" + cursor.getMetaData().getColumnCount() + " campos) con la cantidad de campos definidos en el XML (" + wobjNodosCampos.getLength() + " campos)" );
        }

        wvarStep = 20;
        wvari = 0;
        //procesa cada campo del registro
        for( int nwobjCampo = 0; nwobjCampo < cursor.getMetaData().getColumnCount(); nwobjCampo++ )
        {
        	Object field = cursor.getObject(nwobjCampo + 1);

          wobjNodoCampo = wobjNodosCampos.item( wvari );
          //si debe mostrarse el campo, muestra su valor con las etiquetas indicadas
          if( wobjNodoCampo.getAttributes().getNamedItem( "OCULTO" ) == (org.w3c.dom.Node) null )
          {
            if( wobjNodoCampo.getAttributes().getNamedItem( "CDATA" ) == (org.w3c.dom.Node) null )
            {
              wobjAux = wobjAux + etiquetar(XmlDomExtended.getText( wobjNodoCampo.getAttributes().getNamedItem( "Nombre" )), field.toString()) ;
            }
            else
            {
              wobjAux = wobjAux + etiquetar(XmlDomExtended.getText( wobjNodoCampo.getAttributes().getNamedItem( "Nombre" )), "<![CDATA[" + field.toString() + "]]>" );
            }
          }
          wvari = wvari + 1;
        }

        wvarStep = 30;
        wobjAuxRegs = wobjAuxRegs + etiquetar(wobjNomFila, wobjAux);
        wobjAux = "";
      }

      wvarStep = 40;

      generarXMLSalida = "<Estado resultado=\"true\"/>" + etiquetar(wobjNomRecordset, wobjAuxRegs);

      return generarXMLSalida;

      //~~~~~~~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      logger.log(Level.FINE,"generarXMLSalida",_e_);

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mvarConsultaRealizada, "generarXMLSalida", wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), null );

        generarXMLSalida = "<Response><Estado resultado='false'/>" + _e_.getMessage() + "</Response>";
        Err.clear();
    }
    return generarXMLSalida;
  }

  private String etiquetar( String pobjEtiqueta, String pvarContenido )
  {
    return "<" + pobjEtiqueta + ">" + pvarContenido + "</" + pobjEtiqueta + ">";
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    wobjCnstADO = new XmlDomExtended();
    wobjCnstADO.load( Thread.currentThread().getContextClassLoader().getResourceAsStream("constantesADO.xml"));
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
