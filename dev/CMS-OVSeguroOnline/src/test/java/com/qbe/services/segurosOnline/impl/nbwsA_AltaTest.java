package com.qbe.services.segurosOnline.impl;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.vbcompat.string.StringHolder;

public class nbwsA_AltaTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIAction_Execute() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		NbwsA_Alta comp = new NbwsA_Alta();
		OSBConnector conn = new OSBConnector();
		conn.setServiceURL(TestConstants.TEST_SERVICE);
		comp.setOsbConnector(conn);
		String pvarRequest = "	<Request><DOCUMTIP>01</DOCUMTIP><DOCUMDAT>31467581</DOCUMDAT><MAIL>ramiro@snoopconsulting.com</MAIL><VIAINSCRIPCION>CC</VIAINSCRIPCION><RESPONSABLEALTA/><CONFORMIDAD>S</CONFORMIDAD></Request>";
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
//		System.out.println(sh.getValue());
	}

}
