package com.qbe.services.segurosOnline.impl;

import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Node;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

public class nbwsA_setMisDatosTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIAction_Execute() throws IOException, XmlDomExtendedException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		nbwsA_setMisDatos comp = new nbwsA_setMisDatos();
		OSBConnector conn = new OSBConnector();
		conn.setServiceURL(TestConstants.ROUTER_OV);
		comp.setOsbConnector(conn);
		String log = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("Log_nbwsA_setMisDatos.xml"));
		XmlDomExtended logXml = new XmlDomExtended();
		logXml.loadXML(log);
		Node requestNode = logXml.selectSingleNode("//LOG/Request");

		String pvarRequest = XmlDomExtended.marshal(requestNode); 
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
//		System.out.println(sh.getValue());
	}

}
