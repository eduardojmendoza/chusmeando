package com.qbe.services.segurosOnline.impl;

import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;

public class ModEncryptDecryptTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws IOException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		String base = "juancarlos";
		String encrypted = "4D4659474353734741515142676A64594136424A4D4563474369734741515142676A6459417747674F54413341674D4341414543416D674241674941674151410D0A42424272416C4871346563344135726A587947692F474271424254645670634E512B71614C4277684F4F7033436C6A2F6F30364643673D3D0D0A";
		String newEncrypted = ModEncryptDecrypt.CapicomEncrypt(base);
		String newBase = ModEncryptDecrypt.CapicomDecrypt(newEncrypted);
		assertTrue(String.format("No son ni parecidos los encriptados %s y %s", encrypted, newEncrypted), StringUtils.getCommonPrefix(new String[] { encrypted, newEncrypted }).length() > 10 );
		assertTrue(String.format("No coinciden los base %s y %s", base, newBase), base.equalsIgnoreCase(newBase));
	}

}
