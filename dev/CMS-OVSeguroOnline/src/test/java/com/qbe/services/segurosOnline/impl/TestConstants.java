package com.qbe.services.segurosOnline.impl;

public interface TestConstants {

	/**
	 * Integrado en server de test
	 */
	@Deprecated
	public static final String TEST_SERVICE = "http://10.1.10.98:8011/OV/Proxy/QBESvc?wsdl";

	/**
	 * Integrado en server de UAT
	 */
	@Deprecated
	public static final String UAT_SERVICE = "http://10.1.10.202:8011/OV/Proxy/QBESvc?wsdl";

	/**
	 * Endpoint con ruteo, punto de entrada. Envía los que están en ambos sistemas por el integrado, tiene
	 * configuraciones expecífica, y los que no encuentra los manda a CMS.
	 */
	@Deprecated
	public static final String ROUTER_OV = TEST_SERVICE;


}
