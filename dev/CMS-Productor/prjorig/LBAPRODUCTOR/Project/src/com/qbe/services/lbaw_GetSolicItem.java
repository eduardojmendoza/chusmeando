package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetSolicItem implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorLBA.lbaw_GetSolicItem";
  static final String mcteStoreProc = "SPSNCV_PROD_SOLI_SELECT_ITEM";
  static final String mcteStoreProcHijos = "SPSNCV_PROD_SOLI_HIJOS_SELECT";
  static final String mcteStoreProcAcc = "SPSNCV_PROD_SOLI_ACC_SELECT";
  static final String mcteStoreProcAseg = "SPSNCV_PROD_SOLI_ASEGADIC_SELECT";
  static final String mcteStoreProcCta = "SPSNCV_PROD_SIFMCUEN_SELECT";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_SUPLENUM = "//SUPLENUM";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Recordset wrstDBResultHijos = null;
    Recordset wrstDBResultAcc = null;
    Recordset wrstDBResultAseg = null;
    Recordset wrstDBResultCuentas = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarRAMOPCOD = "";
    String wvarPOLIZANN = "";
    String wvarPOLIZSEC = "";
    String wvarCERTIPOL = "";
    String wvarCERTIANN = "";
    String wvarCERTISEC = "";
    String wvarSUPLENUM = "";
    String wvarCLIENSEC = "";
    String wvarCUENTSEC = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarRAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD ) */ );
      wvarPOLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN ) */ );
      wvarPOLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC ) */ );
      wvarCERTIPOL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL ) */ );
      wvarCERTIANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN ) */ );
      wvarCERTISEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC ) */ );
      wvarSUPLENUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUPLENUM ) */ );
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 20;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //
      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      //
      wvarStep = 40;
      wobjDBCmd = new Command();
      //
      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 60;
      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarRAMOPCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 70;
      wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPOLIZANN ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 80;
      wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPOLIZSEC ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 90;
      wobjDBParm = new Parameter( "@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCERTIPOL ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 100;
      wobjDBParm = new Parameter( "@CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCERTIANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 110;
      wobjDBParm = new Parameter( "@CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCERTISEC ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 120;
      wobjDBParm = new Parameter( "@SUPLENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarSUPLENUM ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 130;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );
      //
      wvarStep = 140;
      if( ! (wrstDBResult.isEOF()) )
      {
        //
        wvarStep = 150;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 160;
        /*unsup wrstDBResult.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
        //
        wvarStep = 170;
        //unsup wobjXSLResponse.async = false;
        wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
        //
        wvarStep = 180;
        wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
        //
        wvarStep = 190;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        wvarCLIENSEC = wrstDBResult.getField("CLIENSEC").getValue().toString();
        wvarCUENTSEC = wrstDBResult.getField("CUENTSEC").getValue().toString();
        //
        //A partir de aca Hijos
        //
        wvarStep = 200;
        wobjDBCmd = new Command();
        //
        wobjDBCmd.setActiveConnection( wobjDBCnn );
        wobjDBCmd.setCommandText( mcteStoreProcHijos );
        wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
        //
        wvarStep = 210;
        wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarRAMOPCOD ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 220;
        wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPOLIZANN ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 230;
        wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPOLIZSEC ) );
        wobjDBParm.setPrecision( (byte)( 6 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 240;
        wobjDBParm = new Parameter( "@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCERTIPOL ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 250;
        wobjDBParm = new Parameter( "@CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCERTIANN ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 260;
        wobjDBParm = new Parameter( "@CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCERTISEC ) );
        wobjDBParm.setPrecision( (byte)( 6 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 270;
        wobjDBParm = new Parameter( "@SUPLENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarSUPLENUM ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 280;
        wrstDBResultHijos = wobjDBCmd.execute();
        wrstDBResultHijos.setActiveConnection( (Connection) null );
        //
        wvarStep = 290;
        if( ! (wrstDBResultHijos.isEOF()) )
        {
          //
          wvarStep = 300;
          wobjXMLResponse = new diamondedge.util.XmlDom();
          wobjXSLResponse = new diamondedge.util.XmlDom();
          //
          wvarStep = 310;
          /*unsup wrstDBResultHijos.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
          //
          wvarStep = 320;
          //unsup wobjXSLResponse.async = false;
          wobjXSLResponse.loadXML( invoke( "p_GetXSLHijos", new Variant[] {} ) );
          //
          wvarStep = 330;
          wvarResult = wvarResult + "<HIJOS>" + Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" ) + "</HIJOS>";
          //
          wvarStep = 340;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          wobjXSLResponse = (diamondedge.util.XmlDom) null;
          //
        }
        else
        {
          wvarStep = 350;
          wvarResult = wvarResult + "<HIJOS></HIJOS>";
        }
        //
        //Hasta aca hijos
        //
        //Desde aca accesorios
        //
        wvarStep = 360;
        wobjDBCmd = new Command();
        //
        wobjDBCmd.setActiveConnection( wobjDBCnn );
        wobjDBCmd.setCommandText( mcteStoreProcAcc );
        wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
        //
        wvarStep = 370;
        wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarRAMOPCOD ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 380;
        wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPOLIZANN ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 390;
        wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPOLIZSEC ) );
        wobjDBParm.setPrecision( (byte)( 6 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 400;
        wobjDBParm = new Parameter( "@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCERTIPOL ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 410;
        wobjDBParm = new Parameter( "@CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCERTIANN ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 420;
        wobjDBParm = new Parameter( "@CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCERTISEC ) );
        wobjDBParm.setPrecision( (byte)( 6 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 430;
        wobjDBParm = new Parameter( "@SUPLENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarSUPLENUM ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 440;
        wrstDBResultAcc = wobjDBCmd.execute();
        wrstDBResultAcc.setActiveConnection( (Connection) null );
        //
        wvarStep = 450;
        if( ! (wrstDBResultAcc.isEOF()) )
        {
          //
          wvarStep = 460;
          wobjXMLResponse = new diamondedge.util.XmlDom();
          wobjXSLResponse = new diamondedge.util.XmlDom();
          //
          wvarStep = 470;
          /*unsup wrstDBResultAcc.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
          //
          wvarStep = 480;
          //unsup wobjXSLResponse.async = false;
          wobjXSLResponse.loadXML( invoke( "p_GetXSLAcc", new Variant[] {} ) );
          //
          wvarStep = 490;
          wvarResult = wvarResult + "<ACCESORIOS>" + Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" ) + "</ACCESORIOS>";
          //
          wvarStep = 500;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          wobjXSLResponse = (diamondedge.util.XmlDom) null;
          //
        }
        else
        {
          wvarStep = 510;
          wvarResult = wvarResult + "<ACCESORIOS></ACCESORIOS>";
        }
        //
        //Hasta aca accesorios
        //
        //Desde aca Asegurados Adicionales
        //
        wvarStep = 520;
        wobjDBCmd = new Command();
        //
        wobjDBCmd.setActiveConnection( wobjDBCnn );
        wobjDBCmd.setCommandText( mcteStoreProcAseg );
        wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
        //
        wvarStep = 530;
        wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarRAMOPCOD ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 540;
        wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPOLIZANN ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 550;
        wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPOLIZSEC ) );
        wobjDBParm.setPrecision( (byte)( 6 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 560;
        wobjDBParm = new Parameter( "@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCERTIPOL ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 570;
        wobjDBParm = new Parameter( "@CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCERTIANN ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 580;
        wobjDBParm = new Parameter( "@CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCERTISEC ) );
        wobjDBParm.setPrecision( (byte)( 6 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 590;
        wobjDBParm = new Parameter( "@SUPLENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarSUPLENUM ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 600;
        wrstDBResultAseg = wobjDBCmd.execute();
        wrstDBResultAseg.setActiveConnection( (Connection) null );
        //
        wvarStep = 610;
        if( ! (wrstDBResultAseg.isEOF()) )
        {
          //
          wvarStep = 620;
          wobjXMLResponse = new diamondedge.util.XmlDom();
          wobjXSLResponse = new diamondedge.util.XmlDom();
          //
          wvarStep = 630;
          /*unsup wrstDBResultAseg.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
          //
          wvarStep = 640;
          //unsup wobjXSLResponse.async = false;
          wobjXSLResponse.loadXML( invoke( "p_GetXSLAseg", new Variant[] {} ) );
          //
          wvarStep = 650;
          wvarResult = wvarResult + "<ASEGURADOS>" + Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" ) + "</ASEGURADOS>";
          //
          wvarStep = 660;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          wobjXSLResponse = (diamondedge.util.XmlDom) null;
          //
        }
        else
        {
          wvarStep = 670;
          wvarResult = wvarResult + "<ASEGURADOS></ASEGURADOS>";
        }
        //
        //Hasta aca Asegurados Adicionales
        //Desde aca CUENTAS
        //
        wvarStep = 680;
        wobjDBCmd = new Command();
        //
        wobjDBCmd.setActiveConnection( wobjDBCnn );
        wobjDBCmd.setCommandText( mcteStoreProcCta );
        wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
        //
        wvarStep = 690;
        wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCLIENSEC ) );
        wobjDBParm.setPrecision( (byte)( 9 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 700;
        wrstDBResultCuentas = wobjDBCmd.execute();
        wrstDBResultCuentas.setActiveConnection( (Connection) null );
        //
        wvarStep = 710;
        if( ! (wrstDBResultCuentas.isEOF()) )
        {
          //
          wvarStep = 720;
          wobjXMLResponse = new diamondedge.util.XmlDom();
          wobjXSLResponse = new diamondedge.util.XmlDom();
          //
          wvarStep = 730;
          /*unsup wrstDBResultCuentas.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
          //
          wvarStep = 740;
          //unsup wobjXSLResponse.async = false;
          wobjXSLResponse.loadXML( invoke( "p_GetXSLCuentas", new Variant[] { new Variant(wvarCUENTSEC) } ) );
          //
          wvarStep = 750;
          wvarResult = wvarResult + Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
          //
          wvarStep = 760;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          wobjXSLResponse = (diamondedge.util.XmlDom) null;
          //
        }
        else
        {
          wvarStep = 770;
          wvarResult = wvarResult + "<></>";
        }
        //
        //Hasta aca CUENTAS
        //
        wvarStep = 780;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        wvarStep = 790;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS." + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //
      wvarStep = 800;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 810;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 820;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 830;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 840;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 850;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 880;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        if( ! (wrstDBResult == (Recordset) null) )
        {
          if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='SOLICITUD'>";
    //
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='RAMOPCOD'><xsl:value-of select='@RAMOPCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='POLIZANN'><xsl:value-of select='@POLIZANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='POLIZSEC'><xsl:value-of select='@POLIZSEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CERTIPOL'><xsl:value-of select='@CERTIPOL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CERTIANN'><xsl:value-of select='@CERTIANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CERTISEC'><xsl:value-of select='@CERTISEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SUPLENUM'><xsl:value-of select='@SUPLENUM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FRANQCOD'><xsl:value-of select='@FRANQCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='HIJOS1416'><xsl:value-of select='@HIJOS1416' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='HIJOS1729'><xsl:value-of select='@HIJOS1729' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='HIJOS1729ND'><xsl:value-of select='@HIJOS1729_EXC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ZONA'><xsl:value-of select='@ZONA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CODPROV'><xsl:value-of select='@CODPROV' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SUMALBA'><xsl:value-of select='@SUMALBA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLUB_LBA'><xsl:value-of select='@CLUB_LBA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DESTRUCCION_80'><xsl:value-of select='@SWDT80' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CPAANO'><xsl:value-of select='@CPAANO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CTAKMS'><xsl:value-of select='@CTAKMS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ESCERO'><xsl:value-of select='@ESCERO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TIENEPLAN'><xsl:value-of select='@TIENEPLAN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='COND_ADIC'><xsl:value-of select='@COND_ADIC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ASEG_ADIC'><xsl:value-of select='@ASEG_ADIC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FH_NAC'><xsl:value-of select='@FH_NAC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SEXO'><xsl:value-of select='@SEXO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ESTCIV'><xsl:value-of select='@ESTCIV' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SIFMVEHI_DES'><xsl:value-of select='@SIFMVEHI_DES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PROFECOD'><xsl:value-of select='@PROFECOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SIACCESORIOS'><xsl:value-of select='@ACCESORIOS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='REFERIDO'><xsl:value-of select='@REFERIDO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='MOTORNUM'><xsl:value-of select='@MOTORNUM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CHASINUM'><xsl:value-of select='@CHASINUM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PATENNUM'><xsl:value-of select='@PATENNUM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUMARCOD'><xsl:value-of select='@AUMARCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUMODCOD'><xsl:value-of select='@AUMODCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUSUBCOD'><xsl:value-of select='@AUSUBCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUADICOD'><xsl:value-of select='@AUADICOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUMODORI'><xsl:value-of select='@AUMODORI' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUUSOCOD'><xsl:value-of select='@AUUSOCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUVTVCOD'><xsl:value-of select='@AUVTVCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUVTVDIA'><xsl:value-of select='@AUVTVDIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUVTVMES'><xsl:value-of select='@AUVTVMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUVTVANN'><xsl:value-of select='@AUVTVANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='VEHCLRCOD'><xsl:value-of select='@VEHCLRCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUKLMNUM'><xsl:value-of select='@AUKLMNUM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FABRICAN'><xsl:value-of select='@FABRICAN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FABRICMES'><xsl:value-of select='@FABRICMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='GUGARAGE'><xsl:value-of select='@GUGARAGE' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='GUDOMICI'><xsl:value-of select='@GUDOMICI' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUCATCOD'><xsl:value-of select='@AUCATCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUTIPCOD'><xsl:value-of select='@AUTIPCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUCIASAN'><xsl:value-of select='@AUCIASAN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUANTANN'><xsl:value-of select='@AUANTANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUNUMSIN'><xsl:value-of select='@AUNUMSIN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUNUMKMT'><xsl:value-of select='@AUNUMKMT' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUUSOGNC'><xsl:value-of select='@AUUSOGNC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SITUCPOL'><xsl:value-of select='@SITUCPOL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENSEC'><xsl:value-of select='@CLIENSEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NUMEDOCU'><xsl:value-of select='@NUMEDOCU' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TIPODOCU'><xsl:value-of select='@TIPODOCU' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICSEC'><xsl:value-of select='@DOMICSEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CUENTSEC'><xsl:value-of select='@CUENTSEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='COBROFOR'><xsl:value-of select='@COBROFOR' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EDADACTU'><xsl:value-of select='@EDADACTU' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CAMP_CODIGO'><xsl:value-of select='@CAMP_CODIGO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CAMP_DESC'><xsl:value-of select='@CAMP_DESC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='LEGAJO_GTE'><xsl:value-of select='@LEGAJO_GTE' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NRO_PROD'><xsl:value-of select='@NRO_PROD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SUCURSAL_CODIGO'><xsl:value-of select='@SUCURSAL_CODIGO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='LEGAJO_VEND'><xsl:value-of select='@LEGAJO_VEND' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SITUCEST'><xsl:value-of select='@SITUCEST' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='INSPECOD'><xsl:value-of select='@INSPECOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='INSPECTOR'><xsl:value-of select='@INSPECTOR' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='INSPEADOM'><xsl:value-of select='@INSPEADOM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='OBSERTXT'><xsl:value-of select='@OBSERTXT' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='INSPENUM'><xsl:value-of select='@INSPENUM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CENTRCOD'><xsl:value-of select='@CENTRCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CENTRDES'><xsl:value-of select='@CENTRDES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='INSPECC'><xsl:value-of select='@INSPECC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='RASTREO'><xsl:value-of select='@RASTREO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ALARMCOD'><xsl:value-of select='@ALARMCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NUMEDOCU_ACRE'><xsl:value-of select='@NUMEDOCU_ACRE' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TIPODOCU_ACRE'><xsl:value-of select='@TIPODOCU_ACRE' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='APELLIDO'><xsl:value-of select='@APELLIDO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NOMBRES'><xsl:value-of select='@NOMBRES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICDOM'><xsl:value-of select='@DOMICDOM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICDNU'><xsl:value-of select='@DOMICDNU' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICESC'><xsl:value-of select='@DOMICESC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICPIS'><xsl:value-of select='@DOMICPIS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICPTA'><xsl:value-of select='@DOMICPTA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICPOB'><xsl:value-of select='@DOMICPOB' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICCPO'><xsl:value-of select='@DOMICCPO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PROVICOD'><xsl:value-of select='@PROVICOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PAISSCOD'><xsl:value-of select='@PAISSCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='INSPEANN'><xsl:value-of select='@INSPEANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='INSPEMES'><xsl:value-of select='@INSPEMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='INSPEDIA'><xsl:value-of select='@INSPEDIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PQTDES'><xsl:value-of select='@PQTDES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PLANCOD'><xsl:value-of select='@PLANCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENNOM'><xsl:value-of select='@CLIENNOM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENAP1'><xsl:value-of select='@CLIENAP1' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENAP2'><xsl:value-of select='@CLIENAP2' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PROVIDES'><xsl:value-of select='@PROVIDES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICPOB_SOLI'><xsl:value-of select='@DOMICPOB_SOLI' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICCPO_SOLI'><xsl:value-of select='@DOMICCPO_SOLI' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PROFEDES'><xsl:value-of select='@PROFEDES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DESCCOLO'><xsl:value-of select='@DESCCOLO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='BANCOCOD'><xsl:value-of select='@BANCOCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='COBROCOD'><xsl:value-of select='@COBROCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EFECTDIA'><xsl:value-of select='@EFECTDIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EFECTMES'><xsl:value-of select='@EFECTMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EFECTANN'><xsl:value-of select='@EFECTANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='COBRODES'><xsl:value-of select='@COBRODES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ENTDOMSC'><xsl:value-of select='@ENTDOMSC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ANOTACIO'><xsl:value-of select='@ANOTACIO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SUMAASEG'><xsl:value-of select='@SUMAASEG' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PRECIO_MENSUAL'><xsl:value-of select='@PRECIO_MENSUAL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOCUMDAB'><xsl:value-of select='@DOCUMDAB' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOCUMDAB_ACRE'><xsl:value-of select='@DOCUMDAB_ACRE' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUMARDES'><xsl:value-of select='@AUMARDES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PROVIDES_ACRE'><xsl:value-of select='@PROVIDES_ACRE' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CUITNUME'><xsl:value-of select='@CUITNUME' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='RAZONSOC'><xsl:value-of select='@RAZONSOC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIEIBTP'><xsl:value-of select='@CLIEIBTP' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NROIIBB'><xsl:value-of select='@NROIIBB' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='LUNETA'><xsl:value-of select='@LUNETA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIIBBDES'><xsl:value-of select='@CIBBDESC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='IVADES'><xsl:value-of select='@CIVADESC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EMAIL'><xsl:value-of select='@EMAIL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENIVA'><xsl:value-of select='@CLIENIVA' /></xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='RAMOPCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='FRANQCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLUB_LBA'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='ESCERO'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='TIENEPLAN'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='COND_ADIC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='ASEG_ADIC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='SEXO'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='ESTCIV'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='SIFMVEHI_DES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='PROFECOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='ACCESORIOS'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='MOTORNUM'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CHASINUM'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='PATENNUM'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='AUMODORI'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='AUVTVCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='GUGARAGE'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='GUDOMICI'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='AUCIASAN'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='AUUSOGNC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='USUARCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='SITUCPOL'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='NUMEDOCU'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICSEC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CUENTSEC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='LEGAJO_GTE'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='LEGAJO_VEND'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='SITUCEST'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='INSPECTOR'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='INSPEADOM'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='OBSERTXT'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CENTRDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='NUMEDOCU_ACRE'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='APELLIDO'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='NOMBRES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICDOM'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICDNU'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICESC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICPIS'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICPTA'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICPOB'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='PAISSCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='PQTDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENNOM'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENAP1'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENAP2'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='PROVIDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICPOB_SOLI'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='PROFEDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DESCCOLO'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='BANCOCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='COBRODES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='ANOTACIO'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='AUMARDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CUITNUME'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='RAZONSOC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIEIBTP'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='NROIIBB'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='LUNETA'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIIBBDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='IVADES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='EMAIL'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private String p_GetXSLHijos() throws Exception
  {
    String p_GetXSLHijos = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='HIJO'>";
    //
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='APELLIDOHIJO'><xsl:value-of select='@CONDUAPE' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NOMBREHIJO'><xsl:value-of select='@CONDUNOM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NACIMHIJO'><xsl:value-of select=\"substring(concat('00000000',string(@CONDUFEC)),string-length(string(@CONDUFEC)) + 1)\"/></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SEXOHIJO'><xsl:value-of select='@CONDUSEX' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ESTADOHIJO'><xsl:value-of select='@CONDUEST' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='INCLUIDO'><xsl:value-of select='@CONDUEXC' /></xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='APELLIDOHIJO'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='NOMBREHIJO'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='SEXOHIJO'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='ESTADOHIJO'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='INCLUIDO'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLHijos = wvarStrXSL;
    return p_GetXSLHijos;
  }

  private String p_GetXSLAcc() throws Exception
  {
    String p_GetXSLAcc = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='ACCESORIO'>";
    //
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CODIGOACC'><xsl:value-of select='@AUACCCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PRECIOACC'><xsl:value-of select='@AUVEASUM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DESCRIPCIONACC'><xsl:value-of select='@AUVEADES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DEPRECIA'><xsl:value-of select='@AUVEADEP' /></xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DESCRIPCIONACC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DEPRECIA'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLAcc = wvarStrXSL;
    return p_GetXSLAcc;
  }

  private String p_GetXSLAseg() throws Exception
  {
    String p_GetXSLAseg = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='ASEGURADO'>";
    //
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOCUMDAT'><xsl:value-of select='@DOCUMDAT' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NOMBREAS'><xsl:value-of select='@NOMBREAS' /></xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='NOMBREAS'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLAseg = wvarStrXSL;
    return p_GetXSLAseg;
  }

  private String p_GetXSLCuentas( String pCUENTSEC ) throws Exception
  {
    String p_GetXSLCuentas = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row[@CUENTSEC = \"" + pCUENTSEC + "\"]'>";
    //
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CUENTSEC'><xsl:value-of select='@CUENTSEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TIPOCUEN'><xsl:value-of select='@TIPOCUEN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='COBROTIP'><xsl:value-of select='@COBROTIP' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FPBANCOCOD'><xsl:value-of select='@BANCOCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FPSUCURCOD'><xsl:value-of select='@SUCURCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CUENTDC'><xsl:value-of select='@CUENTDC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CUENNUME'><xsl:value-of select='@CUENNUME' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TARJECOD'><xsl:value-of select='@TARJECOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='VENCIANN'><xsl:value-of select='@VENCIANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='VENCIMES'><xsl:value-of select='@VENCIMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='VENCIDIA'><xsl:value-of select='@VENCIDIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TARJEDES'><xsl:value-of select='@COBRODES' /></xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='USUARCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='TIPOCUEN'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='COBROTIP'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CUENTDC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CUENNUME'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='TARJEDES'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLCuentas = wvarStrXSL;
    return p_GetXSLCuentas;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
