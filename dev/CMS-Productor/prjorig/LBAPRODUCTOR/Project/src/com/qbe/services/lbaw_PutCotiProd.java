package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_PutCotiProd implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorLBA.lbaw_PutCotiProd";
  static final String mcteStoreProc = "SPSNCV_PROD_COTI_INSERT";
  /**
   *  Parametros XML de Entrada
   *  DATOS GENERALES
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_SUPLENUM = "//SUPLENUM";
  static final String mcteParam_FRANQCOD = "//FRANQCOD";
  static final String mcteParam_PQTDES = "//PQTDES";
  static final String mcteParam_PLANCOD = "//PLANCOD";
  static final String mcteParam_HIJOS1416 = "//HIJOS1416";
  static final String mcteParam_HIJOS1729 = "//HIJOS1729";
  static final String mcteParam_ZONA = "//ZONA";
  static final String mcteParam_CODPROV = "//CODPROV";
  static final String mcteParam_SUMALBA = "//SUMALBA";
  static final String mcteParam_CLIENIVA = "//CLIENIVA";
  static final String mcteParam_SUMASEG = "//SUMASEG";
  static final String mcteParam_CLUB_LBA = "//CLUB_LBA";
  static final String mcteParam_DEST80 = "//DESTRUCCION_80";
  static final String mcteParam_CPAANO = "//CPAANO";
  static final String mcteParam_CTAKMS = "//CTAKMS";
  static final String mcteParam_ESCERO = "//ESCERO";
  static final String mcteParam_TIENEPLAN = "//TIENEPLAN";
  static final String mcteParam_COND_ADIC = "//COND_ADIC";
  static final String mcteParam_ASEG_ADIC = "//ASEG_ADIC";
  static final String mcteParam_FH_NAC = "//FH_NAC";
  static final String mcteParam_SEXO = "//SEXO";
  static final String mcteParam_ESTCIV = "//ESTCIV";
  static final String mcteParam_SIFMVEHI_DES = "//SIFMVEHI_DES";
  static final String mcteParam_PROFECOD = "//PROFECOD";
  static final String mcteParam_SIACCESORIOS = "//SIACCESORIOS";
  static final String mcteParam_REFERIDO = "//REFERIDO";
  static final String mcteParam_MOTORNUM = "//MOTORNUM";
  static final String mcteParam_CHASINUM = "//CHASINUM";
  static final String mcteParam_PATENNUM = "//PATENNUM";
  static final String mcteParam_AUMARCOD = "//AUMARCOD";
  static final String mcteParam_AUMODCOD = "//AUMODCOD";
  static final String mcteParam_AUSUBCOD = "//AUSUBCOD";
  static final String mcteParam_AUADICOD = "//AUADICOD";
  static final String mcteParam_AUMODORI = "//AUMODORI";
  static final String mcteParam_AUUSOCOD = "//AUUSOCOD";
  static final String mcteParam_AUVTVCOD = "//AUVTVCOD";
  static final String mcteParam_AUVTVDIA = "//AUVTVDIA";
  static final String mcteParam_AUVTVMES = "//AUVTVMES";
  static final String mcteParam_AUVTVANN = "//AUVTVANN";
  static final String mcteParam_VEHCLRCOD = "//VEHCLRCOD";
  static final String mcteParam_AUKLMNUM = "//AUKLMNUM";
  static final String mcteParam_FABRICAN = "//FABRICAN";
  static final String mcteParam_FABRICMES = "//FABRICMES";
  static final String mcteParam_GUGARAGE = "//GUGARAGE";
  static final String mcteParam_GUDOMICI = "//GUDOMICI";
  static final String mcteParam_AUCATCOD = "//AUCATCOD";
  static final String mcteParam_AUTIPCOD = "//AUTIPCOD";
  static final String mcteParam_AUCIASAN = "//AUCIASAN";
  static final String mcteParam_AUANTANN = "//AUANTANN";
  static final String mcteParam_AUNUMSIN = "//AUNUMSIN";
  static final String mcteParam_AUNUMKMT = "//AUNUMKMT";
  static final String mcteParam_AUUSOGNC = "//AUUSOGNC";
  static final String mcteParam_USUARCOD = "//USUARCOD";
  static final String mcteParam_SITUCPOL = "//SITUCPOL";
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_NUMEDOCU = "//NUMEDOCU";
  static final String mcteParam_TIPODOCU = "//TIPODOCU";
  static final String mcteParam_DOMICSEC = "//DOMICSEC";
  static final String mcteParam_CUENTSEC = "//CUENTSEC";
  static final String mcteParam_COBROFOR = "//COBROFOR";
  static final String mcteParam_EDADACTU = "//EDADACTU";
  static final String mcteParam_BANCOCOD = "//BANCOCOD";
  static final String mcteParam_COBROCOD = "//COBROCOD";
  static final String mcteParam_EFECTANN = "//EFECTANN";
  static final String mcteParam_EFECTMES = "//EFECTMES";
  static final String mcteParam_EFECTDIA = "//EFECTDIA";
  static final String mcteParam_COBROTIP = "//COBROTIP";
  static final String mcteParam_CAMP_CODIGO = "//CAMP_CODIGO";
  static final String mcteParam_CAMP_DESC = "//CAMP_DESC";
  static final String mcteParam_LEGAJO_GTE = "//LEGAJO_GTE";
  static final String mcteParam_TIPO_PROD = "//AGENTCLA";
  static final String mcteParam_NRO_PROD = "//NRO_PROD";
  static final String mcteParam_SUCURSAL_CODIGO = "//SUCURSAL_CODIGO";
  static final String mcteParam_LEGAJO_VEND = "//LEGAJO_VEND";
  static final String mcteParam_CUITNUME = "//CUITNUME";
  static final String mcteParam_RAZONSOC = "//RAZONSOC";
  static final String mcteParam_CLIEIBTP = "//CLIEIBTP";
  static final String mcteParam_NROIIBB = "//NROIIBB";
  static final String mcteParam_LUNETA = "//LUNETA";
  static final String mcteParam_INSTALADP = "//INSTALADP";
  static final String mcteParam_POSEEDISP = "//POSEEDISP";
  static final String mcteParam_AUTIPGAMA = "//AUTIPGAMA";
  static final String mcteParam_PRESTCOD = "//PRESTCOD";
  /**
   * Green Products
   */
  static final String mcteParam_CLUBECO = "//CLUBECO";
  static final String mcteParam_GRANIZO = "//GRANIZO";
  static final String mcteParam_ROBOCONT = "//ROBOCONT";
  /**
   *  Conductores
   */
  static final String mcteParam_CONDUCTOR = "//Request/HIJOS/HIJO";
  static final String mcteParam_CONDUFEC = "NACIMHIJO";
  static final String mcteParam_CONDUSEX = "SEXOHIJO";
  static final String mcteParam_CONDUEST = "ESTADOHIJO";
  /**
   *  Accesorios
   */
  static final String mcteParam_ACCESORIO = "//Request/ACCESORIOS/ACCESORIO";
  static final String mcteParam_AUACCCOD = "CODIGOACC";
  static final String mcteParam_AUVEASUM = "PRECIOACC";
  static final String mcteParam_AUVEADES = "DESCRIPCIONACC";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private com.qbe.services.HSBCInterfaces.IDBConnection mobjHSBC_DBCnn = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarResponse = "";
    String wvarRequest = "";
    String wvarMensaje = "";
    String wvarRAMOPCOD = "";
    String wvarPOLIZANN = "";
    String wvarPOLIZSEC = "";
    String wvarCERTIPOL = "";
    String wvarCERTIANN = "";
    String wvarSUPLENUM = "";
    String wvarFRANQCOD = "";
    String wvarPQTDES = "";
    String wvarPLANCOD = "";
    String wvarHIJOS1416 = "";
    String wvarHIJOS1729 = "";
    String wvarHIJOS1729ND = "";
    String wvarZONA = "";
    String wvarCODPROV = "";
    String wvarSUMALBA = "";
    String wvarCLIENIVA = "";
    String wvarSUMASEG = "";
    String wvarCLUB_LBA = "";
    String mvarDEST80 = "";
    String wvarCPAANO = "";
    String wvarCTAKMS = "";
    String wvarESCERO = "";
    String wvarTIENEPLAN = "";
    String wvarCOND_ADIC = "";
    String wvarASEG_ADIC = "";
    String wvarFH_NAC = "";
    String wvarSEXO = "";
    String wvarESTCIV = "";
    String wvarSIFMVEHI_DES = "";
    String wvarPROFECOD = "";
    String wvarSIACCESORIOS = "";
    String wvarREFERIDO = "";
    String wvarMOTORNUM = "";
    String wvarCHASINUM = "";
    String wvarPATENNUM = "";
    String wvarAUMARCOD = "";
    String wvarAUMODCOD = "";
    String wvarAUSUBCOD = "";
    String wvarAUADICOD = "";
    String wvarAUMODORI = "";
    String wvarAUUSOCOD = "";
    String wvarAUVTVCOD = "";
    String wvarAUVTVDIA = "";
    String wvarAUVTVMES = "";
    String wvarAUVTVANN = "";
    String wvarVEHCLRCOD = "";
    String wvarAUKLMNUM = "";
    String wvarFABRICAN = "";
    String wvarFABRICMES = "";
    String wvarGUGARAGE = "";
    String wvarGUDOMICI = "";
    String wvarAUCATCOD = "";
    String wvarAUTIPCOD = "";
    String wvarAUCIASAN = "";
    String wvarAUANTANN = "";
    String wvarAUNUMSIN = "";
    String wvarAUNUMKMT = "";
    String wvarAUUSOGNC = "";
    String wvarUSUARCOD = "";
    String wvarSITUCPOL = "";
    String wvarCLIENSEC = "";
    String wvarNUMEDOCU = "";
    String wvarTIPODOCU = "";
    String wvarDOMICSEC = "";
    String wvarCUENTSEC = "";
    String wvarCOBROFOR = "";
    String wvarEDADACTU = "";
    String wvarBANCOCOD = "";
    String wvarCOBROCOD = "";
    String wvarEFECTANN = "";
    String wvarEFECTMES = "";
    String wvarEFECTDIA = "";
    String wvarCOBROTIP = "";
    String wvarCAMP_CODIGO = "";
    String wvarCAMP_DESC = "";
    String wvarLEGAJO_GTE = "";
    String wvarNRO_PROD = "";
    String wvarSUCURSAL_CODIGO = "";
    String wvarLEGAJO_VEND = "";
    String wvarTIPOPROD = "";
    String wvarCONDUFEC = "";
    String wvarCONDUSEX = "";
    String wvarCONDUEST = "";
    String wvarAUACCCOD = "";
    String wvarAUVEASUM = "";
    String wvarAUVEADES = "";
    String wvarCUITNUME = "";
    String wvarRAZONSOC = "";
    String wvarCLIEIBTP = "";
    String wvarNROIIBB = "";
    String wvarLUNETA = "";
    String wvarINSTALADP = "";
    String wvarPOSEEDISP = "";
    String wvarAUTIPGAMA = "";
    String wvarPRESTCOD = "";
    String wvarCLUBECO = "";
    String wvarGRANIZO = "";
    String wvarROBOCONT = "";
    //
    //
    // DATOS GENERALES
    // CONDUCTORES
    // ACCESORIOS
    //Green Products
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      // CARGO EL XML DE ENTRADA
      wvarStep = 5;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      // DATOS GENERALES
      wvarStep = 20;
      wvarRAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD ) */ );
      wvarPOLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN ) */ );
      wvarPOLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC ) */ );
      wvarCERTIPOL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL ) */ );
      wvarCERTIANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN ) */ );

      wvarStep = 30;
      wvarSUPLENUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUPLENUM ) */ );
      wvarFRANQCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FRANQCOD ) */ );
      wvarPQTDES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PQTDES ) */ );
      wvarPLANCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PLANCOD ) */ );
      wvarHIJOS1416 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1416 ) */ );

      wvarStep = 40;
      wvarHIJOS1729 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1729 ) */ );
      wvarHIJOS1729ND = "0";
      wvarZONA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ZONA ) */ );
      wvarCODPROV = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CODPROV ) */ );
      wvarSUMALBA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUMALBA ) */ );
      wvarCLIENIVA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENIVA ) */ );

      wvarStep = 50;
      wvarSUMASEG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUMASEG ) */ );
      wvarCLUB_LBA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLUB_LBA ) */ );

      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_DEST80 ) */.getLength() > 0 )
      {
        //23/01/2006
        mvarDEST80 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DEST80 ) */ );
      }
      if( mvarDEST80.equals( "" ) )
      {
        mvarDEST80 = "N";
      }

      wvarCPAANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CPAANO ) */ );
      wvarCTAKMS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CTAKMS ) */ );
      wvarESCERO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ESCERO ) */ );

      wvarStep = 60;
      wvarTIENEPLAN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIENEPLAN ) */ );
      wvarCOND_ADIC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COND_ADIC ) */ );
      wvarASEG_ADIC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ASEG_ADIC ) */ );
      wvarFH_NAC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FH_NAC ) */ );
      wvarSEXO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SEXO ) */ );

      wvarStep = 70;
      wvarESTCIV = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ESTCIV ) */ );
      wvarSIFMVEHI_DES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SIFMVEHI_DES ) */ );
      wvarPROFECOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROFECOD ) */ );
      wvarSIACCESORIOS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SIACCESORIOS ) */ );
      wvarREFERIDO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_REFERIDO ) */ );

      wvarStep = 80;
      wvarMOTORNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MOTORNUM ) */ );
      wvarCHASINUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CHASINUM ) */ );
      wvarPATENNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PATENNUM ) */ );
      wvarAUMARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUMARCOD ) */ );
      wvarAUMODCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUMODCOD ) */ );

      wvarStep = 90;
      wvarAUSUBCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUSUBCOD ) */ );
      wvarAUADICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUADICOD ) */ );
      wvarAUMODORI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUMODORI ) */ );
      wvarAUUSOCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUUSOCOD ) */ );
      wvarAUVTVCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUVTVCOD ) */ );

      wvarStep = 100;
      wvarAUVTVDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUVTVDIA ) */ );
      wvarAUVTVMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUVTVMES ) */ );
      wvarAUVTVANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUVTVANN ) */ );
      wvarVEHCLRCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VEHCLRCOD ) */ );
      wvarAUKLMNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUKLMNUM ) */ );

      wvarStep = 120;
      wvarFABRICAN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FABRICAN ) */ );
      wvarFABRICMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FABRICMES ) */ );
      wvarGUGARAGE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GUGARAGE ) */ );
      wvarGUDOMICI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GUDOMICI ) */ );
      wvarAUCATCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUCATCOD ) */ );

      wvarStep = 130;
      wvarAUTIPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUTIPCOD ) */ );
      wvarAUCIASAN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUCIASAN ) */ );
      wvarAUANTANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUANTANN ) */ );
      wvarAUNUMSIN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUNUMSIN ) */ );
      wvarAUNUMKMT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUNUMKMT ) */ );

      wvarStep = 140;
      wvarAUUSOGNC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUUSOGNC ) */ );
      wvarUSUARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD ) */ );
      wvarSITUCPOL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SITUCPOL ) */ );
      wvarCLIENSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC ) */ );
      wvarNUMEDOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU ) */ );

      wvarStep = 150;
      wvarTIPODOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU ) */ );
      wvarDOMICSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICSEC ) */ );
      wvarCUENTSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CUENTSEC ) */ );
      wvarCOBROFOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COBROFOR ) */ );
      wvarEDADACTU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EDADACTU ) */ );

      wvarStep = 160;
      wvarCAMP_CODIGO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CAMP_CODIGO ) */ );
      wvarCAMP_DESC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CAMP_DESC ) */ );
      wvarLEGAJO_GTE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LEGAJO_GTE ) */ );
      wvarNRO_PROD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NRO_PROD ) */ );
      wvarSUCURSAL_CODIGO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUCURSAL_CODIGO ) */ );
      wvarLEGAJO_VEND = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LEGAJO_VEND ) */ );

      wvarStep = 165;
      wvarBANCOCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_BANCOCOD ) */ );
      wvarCOBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COBROCOD ) */ );
      wvarEFECTANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTANN ) */ );
      wvarEFECTMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTMES ) */ );
      wvarEFECTDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTDIA ) */ );
      wvarCOBROTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COBROTIP ) */ );

      wvarStep = 167;
      wvarCUITNUME = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_CUITNUME ) */.getLength() > 0 )
      {
        wvarCUITNUME = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CUITNUME ) */ );
      }
      wvarRAZONSOC = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_RAZONSOC ) */.getLength() > 0 )
      {
        wvarRAZONSOC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAZONSOC ) */ );
      }
      wvarCLIEIBTP = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_CLIEIBTP ) */.getLength() > 0 )
      {
        wvarCLIEIBTP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIEIBTP ) */ );
      }
      wvarNROIIBB = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_NROIIBB ) */.getLength() > 0 )
      {
        wvarNROIIBB = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NROIIBB ) */ );
      }
      wvarLUNETA = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_LUNETA ) */.getLength() > 0 )
      {
        wvarLUNETA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUNETA ) */ );
      }

      wvarStep = 168;
      wvarINSTALADP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSTALADP ) */ );
      wvarPOSEEDISP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POSEEDISP ) */ );
      wvarAUTIPGAMA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUTIPGAMA ) */ );
      wvarPRESTCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PRESTCOD ) */ );
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPO_PROD ) */ == (org.w3c.dom.Node) null) )
      {
        wvarTIPOPROD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPO_PROD ) */ );
      }

      //GREEN PRODUCTS
      wvarStep = 169;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLUBECO ) */ == (org.w3c.dom.Node) null) )
      {
        wvarCLUBECO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLUBECO ) */ );
      }

      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GRANIZO ) */ == (org.w3c.dom.Node) null) )
      {
        wvarGRANIZO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GRANIZO ) */ );
      }

      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROBOCONT ) */ == (org.w3c.dom.Node) null) )
      {
        wvarROBOCONT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROBOCONT ) */ );
      }


      wvarStep = 170;
      wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      wobjDBCmd = new Command();

      wvarStep = 180;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      // Campos Obligatorios sin loop
      wvarStep = 190;
      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //'''''''''''''''''''''''''''''''''''''''''''''
      wvarStep = 200;
      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (wvarRAMOPCOD.equals( "" ) ? " " : wvarRAMOPCOD) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 210;
      wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPOLIZANN ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 215;
      wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPOLIZSEC ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 220;
      wobjDBParm = new Parameter( "@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCERTIPOL ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 230;
      wobjDBParm = new Parameter( "@CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 4, new Variant( wvarCERTIANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 240;
      wobjDBParm = new Parameter( "@SUPLENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarSUPLENUM ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 250;
      wobjDBParm = new Parameter( "@FRANQCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (wvarFRANQCOD.equals( "" ) ? " " : wvarFRANQCOD) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 260;
      wobjDBParm = new Parameter( "@PQTDES", AdoConst.adChar, AdoConst.adParamInput, 120, new Variant( (wvarPQTDES.equals( "" ) ? " " : wvarPQTDES) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 270;
      wobjDBParm = new Parameter( "@PLANCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPLANCOD ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 280;
      wobjDBParm = new Parameter( "@HIJOS1416", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarHIJOS1416 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 290;
      wobjDBParm = new Parameter( "@HIJOS1729", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarHIJOS1729 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 300;
      wobjDBParm = new Parameter( "@ZONA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarZONA ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 310;
      wobjDBParm = new Parameter( "@CODPROV", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCODPROV ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 320;
      wobjDBParm = new Parameter( "@SUMALBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarSUMALBA ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 330;
      wobjDBParm = new Parameter( "@CLIENIVA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarCLIENIVA.equals( "" ) ? " " : wvarCLIENIVA) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 340;
      wobjDBParm = new Parameter( "@SUMAASEG", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarSUMASEG ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 350;
      wobjDBParm = new Parameter( "@CLUB_LBA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarCLUB_LBA.equals( "" ) ? " " : wvarCLUB_LBA) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 360;
      wobjDBParm = new Parameter( "@CPAANO ", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCPAANO ) );
      wobjDBParm.setPrecision( (byte)( 8 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 370;
      wobjDBParm = new Parameter( "@CTAKMS ", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCTAKMS ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 380;
      wobjDBParm = new Parameter( "@ESCERO ", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarESCERO.equals( "" ) ? " " : wvarESCERO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 390;
      wobjDBParm = new Parameter( "@TIENEPLAN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarTIENEPLAN.equals( "" ) ? " " : wvarTIENEPLAN) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 400;
      wobjDBParm = new Parameter( "@COND_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarCOND_ADIC.equals( "" ) ? " " : wvarCOND_ADIC) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 410;
      wobjDBParm = new Parameter( "@ASEG_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarASEG_ADIC.equals( "" ) ? " " : wvarASEG_ADIC) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 420;
      wobjDBParm = new Parameter( "@FH_NAC ", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarFH_NAC ) );
      wobjDBParm.setPrecision( (byte)( 8 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 430;
      wobjDBParm = new Parameter( "@SEXO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarSEXO.equals( "" ) ? " " : wvarSEXO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 440;
      wobjDBParm = new Parameter( "@ESTCIV", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarESTCIV.equals( "" ) ? " " : wvarESTCIV) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 450;
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@SIFMVEHI_DES", adChar, adParamInput, 50, IIf(wvarSIFMVEHI_DES = "", " ", wvarSIFMVEHI_DES)) ' MQ 0093
      // MQ 0051
      wobjDBParm = new Parameter( "@SIFMVEHI_DES", AdoConst.adChar, AdoConst.adParamInput, 80, new Variant( (wvarSIFMVEHI_DES.equals( "" ) ? " " : wvarSIFMVEHI_DES) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 460;
      wobjDBParm = new Parameter( "@PROFECOD", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( (wvarPROFECOD.equals( "" ) ? " " : wvarPROFECOD) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 470;
      wobjDBParm = new Parameter( "@ACCESORIOS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarSIACCESORIOS.equals( "" ) ? " " : wvarSIACCESORIOS) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 480;
      wobjDBParm = new Parameter( "@REFERIDO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarREFERIDO ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 490;
      wobjDBParm = new Parameter( "@MOTORNUM", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( (wvarMOTORNUM.equals( "" ) ? " " : wvarMOTORNUM) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 500;
      wobjDBParm = new Parameter( "@CHASINUM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( (wvarCHASINUM.equals( "" ) ? " " : wvarCHASINUM) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 510;
      wobjDBParm = new Parameter( "@PATENNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( (wvarPATENNUM.equals( "" ) ? " " : wvarPATENNUM) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 520;
      wobjDBParm = new Parameter( "@AUMARCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUMARCOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 530;
      wobjDBParm = new Parameter( "@AUMODCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUMODCOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 540;
      wobjDBParm = new Parameter( "@AUSUBCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUSUBCOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 550;
      wobjDBParm = new Parameter( "@AUADICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUADICOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 560;
      wobjDBParm = new Parameter( "@AUMODORI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarAUMODORI.equals( "" ) ? " " : wvarAUMODORI) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 570;
      wobjDBParm = new Parameter( "@AUUSOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUUSOCOD ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 580;
      wobjDBParm = new Parameter( "@AUVTVCOD", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarAUVTVCOD.equals( "" ) ? " " : wvarAUVTVCOD) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 590;
      wobjDBParm = new Parameter( "@AUVTVDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUVTVDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 600;
      wobjDBParm = new Parameter( "@AUVTVMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUVTVMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 610;
      wobjDBParm = new Parameter( "@AUVTVANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUVTVANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 620;
      wobjDBParm = new Parameter( "@VEHCLRCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarVEHCLRCOD ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 630;
      wobjDBParm = new Parameter( "@AUKLMNUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUKLMNUM ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 640;
      wobjDBParm = new Parameter( "@FABRICAN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarFABRICAN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 650;
      wobjDBParm = new Parameter( "@FABRICMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarFABRICMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 660;
      wobjDBParm = new Parameter( "@GUGARAGE", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarGUGARAGE.equals( "" ) ? " " : wvarGUGARAGE) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 670;
      wobjDBParm = new Parameter( "@GUDOMICI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarGUDOMICI.equals( "" ) ? " " : wvarGUDOMICI) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 680;
      wobjDBParm = new Parameter( "@AUCATCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUCATCOD ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 690;
      wobjDBParm = new Parameter( "@AUTIPCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUTIPCOD ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 700;
      wobjDBParm = new Parameter( "@AUCIASAN", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (wvarAUCIASAN.equals( "" ) ? " " : wvarAUCIASAN) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 710;
      wobjDBParm = new Parameter( "@AUANTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUANTANN ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 720;
      wobjDBParm = new Parameter( "@AUNUMSIN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUNUMSIN ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 730;
      wobjDBParm = new Parameter( "@AUNUMKMT", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUNUMKMT ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 740;
      wobjDBParm = new Parameter( "@AUUSOGNC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarAUUSOGNC.equals( "" ) ? " " : wvarAUUSOGNC) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 750;
      wobjDBParm = new Parameter( "@USUARCOD", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( (wvarUSUARCOD.equals( "" ) ? " " : wvarUSUARCOD) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 760;
      wobjDBParm = new Parameter( "@SITUCPOL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarSITUCPOL.equals( "" ) ? " " : wvarSITUCPOL) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 770;
      wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCLIENSEC ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 780;
      wobjDBParm = new Parameter( "@NUMEDOCU", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( (wvarNUMEDOCU.equals( "" ) ? " " : wvarNUMEDOCU) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 790;
      wobjDBParm = new Parameter( "@TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarTIPODOCU ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 800;
      wobjDBParm = new Parameter( "@DOMICSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (wvarDOMICSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(wvarDOMICSEC)) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 810;
      wobjDBParm = new Parameter( "@CUENTSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, (wvarCUENTSEC.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(wvarCUENTSEC)) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 820;
      wobjDBParm = new Parameter( "@COBROFOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCOBROFOR ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 830;
      wobjDBParm = new Parameter( "@EDADACTU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarEDADACTU ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      // desde aca Coberturas
      wvarStep = 10000;

      for( wvarCounter = 1; wvarCounter <= 20; wvarCounter++ )
      {

        wvarStep = 10100;
        wobjDBParm = new Parameter( "@COBERCOD" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 3 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 10200;
        wobjDBParm = new Parameter( "@COBERORD" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 10300;
        wobjDBParm = new Parameter( "@CAPITASG" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 10400;
        wobjDBParm = new Parameter( "@CAPITIMP" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }


      // hasta aca Coberturas
      //'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      // desde aca asegurados
      wvarStep = 20000;
      for( wvarCounter = 1; wvarCounter <= 10; wvarCounter++ )
      {

        wvarStep = 20300;
        wobjDBParm = new Parameter( "@DOCUMDAT" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 9 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 20400;
        wobjDBParm = new Parameter( "@NOMBREAS" + String.valueOf( wvarCounter ), AdoConst.adChar, AdoConst.adParamInput, 49, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      // hasta aca Asegurados
      //'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      wvarStep = 850;
      wobjDBParm = new Parameter( "@CAMP_CODIGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCAMP_CODIGO ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 855;
      wobjDBParm = new Parameter( "@CAMP_DESC", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( Strings.left( Strings.trim( wvarCAMP_DESC ), 30 ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 860;
      wobjDBParm = new Parameter( "@LEGAJO_GTE", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( (wvarLEGAJO_GTE.equals( "" ) ? " " : wvarLEGAJO_GTE) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 870;
      wobjDBParm = new Parameter( "@NRO_PROD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarNRO_PROD ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 880;
      wobjDBParm = new Parameter( "@SUCURSAL_CODIGO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarSUCURSAL_CODIGO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 890;
      wobjDBParm = new Parameter( "@LEGAJO_VEND", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( (wvarLEGAJO_VEND.equals( "" ) ? " " : wvarLEGAJO_VEND) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      //
      // Desde aca conductores
      wvarStep = 30000;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_CONDUCTOR ) */;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        wvarCONDUFEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUFEC ) */ );
        wvarCONDUSEX = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUSEX ) */ );
        wvarCONDUEST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUEST ) */ );

        wvarStep = 30100;
        wobjDBParm = new Parameter( "@CONDUAPE" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 30200;
        wobjDBParm = new Parameter( "@CONDUNOM" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 30300;
        wobjDBParm = new Parameter( "@CONDUFEC" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCONDUFEC ) );
        wobjDBParm.setPrecision( (byte)( 8 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 30400;
        wobjDBParm = new Parameter( "@CONDUSEX" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarCONDUSEX.equals( "" ) ? " " : wvarCONDUSEX) ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 30500;
        wobjDBParm = new Parameter( "@CONDUEST" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (wvarCONDUEST.equals( "" ) ? " " : wvarCONDUEST) ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 30600;
        wobjDBParm = new Parameter( "@CONDUEXC" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "S" ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wvarStep = 30700;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
      {

        wvarStep = 30800;
        wobjDBParm = new Parameter( "@CONDUAPE" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 30900;
        wobjDBParm = new Parameter( "@CONDUNOM" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 31000;
        wobjDBParm = new Parameter( "@CONDUFEC" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 8 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 31100;
        wobjDBParm = new Parameter( "@CONDUSEX" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 31200;
        wobjDBParm = new Parameter( "@CONDUEST" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 31300;
        wobjDBParm = new Parameter( "@CONDUEXC" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wobjXMLList = (org.w3c.dom.NodeList) null;
      // hasta aca conductores
      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      // Desde aca accesorios
      wvarStep = 40000;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_ACCESORIO ) */;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        wvarAUACCCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_AUACCCOD ) */ );
        wvarAUVEASUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_AUVEASUM ) */ );
        wvarAUVEADES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_AUVEADES ) */ );

        wvarStep = 40010;
        wobjDBParm = new Parameter( "@AUACCCOD" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUACCCOD ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 40020;
        wobjDBParm = new Parameter( "@AUVEASUM" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, (wvarAUVEASUM.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(wvarAUVEASUM)) );
        wobjDBParm.setPrecision( (byte)( 14 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 40030;
        wobjDBParm = new Parameter( "@AUVEADES" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( (wvarAUVEADES.equals( "" ) ? " " : wvarAUVEADES) ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 40040;
        wobjDBParm = new Parameter( "@AUVEADEP" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "S" ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wvarStep = 40050;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
      {

        wvarStep = 40060;
        wobjDBParm = new Parameter( "@AUACCCOD" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 40070;
        wobjDBParm = new Parameter( "@AUVEASUM" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 14 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 40080;
        wobjDBParm = new Parameter( "@AUVEADES" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 40090;
        wobjDBParm = new Parameter( "@AUVEADEP" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wobjXMLList = (org.w3c.dom.NodeList) null;
      // hasta aca accesorios
      wvarStep = 900;
      wobjDBParm = new Parameter( "@BANCOCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarBANCOCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 910;
      wobjDBParm = new Parameter( "@COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCOBROCOD ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 920;
      wobjDBParm = new Parameter( "@EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarEFECTANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 930;
      wobjDBParm = new Parameter( "@EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarEFECTMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 940;
      wobjDBParm = new Parameter( "@EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarEFECTDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 945;
      wobjDBParm = new Parameter( "@COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarCOBROTIP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //------------------------------------------
      wvarStep = 950;
      wobjDBParm = new Parameter( "@CUITNUME", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( wvarCUITNUME ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 955;
      wobjDBParm = new Parameter( "@RAZONSOC", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant( wvarRAZONSOC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 960;
      wobjDBParm = new Parameter( "@CLIEIBTP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarCLIEIBTP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 965;
      wobjDBParm = new Parameter( "@NROIIBB", AdoConst.adChar, AdoConst.adParamInput, 15, new Variant( wvarNROIIBB ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 970;
      wobjDBParm = new Parameter( "@LUNETA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarLUNETA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 975;
      wobjDBParm = new Parameter( "@HIJOS1729_EXC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarHIJOS1729ND ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 976;
      wobjDBParm = new Parameter( "@INSTALADP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarINSTALADP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 977;
      wobjDBParm = new Parameter( "@POSEEDISP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarPOSEEDISP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 978;
      wobjDBParm = new Parameter( "@AUTIPGAMA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarAUTIPGAMA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 979;
      wobjDBParm = new Parameter( "@PRESTCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( Strings.right( Strings.fill( 4, " " ) + wvarPRESTCOD, 4 ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      // 23/01/2006
      wvarStep = 980;
      wobjDBParm = new Parameter( "@SWDT80", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarDEST80 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      if( ! (wvarTIPOPROD.equals( "" )) )
      {
        //22/3/2006 Ale
        wvarStep = 981;
        wobjDBParm = new Parameter( "@TIPO_PROD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarTIPOPROD ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      //GREEN PRODUCTS
      // 20/05/2008
      wvarStep = 982;
      wobjDBParm = new Parameter( "@GREEN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarCLUBECO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      // 20/05/2008
      wvarStep = 983;
      wobjDBParm = new Parameter( "@GRANIZO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarGRANIZO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      // 20/05/2008
      wvarStep = 984;
      wobjDBParm = new Parameter( "@ROBOCONT", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarROBOCONT ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //------------------------------------------
      //'''''''''''''''''''''' Ver parametros '''''''''''''''
      //    Dim aa
      //    Dim i
      //    For i = 1 To wobjDBCmd.Parameters.Count - 1
      //        If wobjDBCmd.Parameters(i).Type = adChar Then
      //            aa = aa & "'" & wobjDBCmd.Parameters(i).Value & "',"
      //        Else
      //            aa = aa & wobjDBCmd.Parameters(i).Value & ","
      //        End If
      //    Next
      //    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
      // '                    mcteClassName, _
      //                     wcteFnName, _
      // '                     wvarStep, _
      //                     Err.Number, _
      // '                     aa, _
      //                     vbLogEventTypeError
      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      wvarStep = 989;
      // On Error Resume Next (optionally ignored)
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );

      if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().isEmpty() )
      {
        Err.raise( Err.getError().getNumber(), Err.getError().getSource(), Err.getError().getDescription() );
      }

      wvarStep = 990;
      if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><COTIID>" + wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue() + "</COTIID></Response>" );
      }
      else
      {
        
        if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -1 )
        {
          wvarMensaje = "Error en la tabla SIFSPOLI";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -2 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -3 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -4 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -5 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -6 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -7 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -8 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -9 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -10 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -11 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 10";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -12 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -13 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -14 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -15 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -16 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -17 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -18 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -19 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -20 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -21 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 10";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -22 )
        {
          wvarMensaje = "Error en la tabla SIFSMOVI";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -23 )
        {
          wvarMensaje = "Error en la tabla SIFSFIMA_TARI";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -24 )
        {
          wvarMensaje = "Error en la tabla SIFSFIMA_VEHI";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -25 )
        {
          wvarMensaje = "Error en la tabla SIFSPECO";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -26 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_CAMP";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -27 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_GTE";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -28 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_PROD";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -29 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_SUCU";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -30 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_VEND";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -31 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -32 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -33 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -34 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -35 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -36 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -37 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -38 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -39 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -40 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 10";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -41 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -42 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -43 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -44 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -45 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -46 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -47 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -48 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -49 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -50 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 10";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -51 )
        {
          wvarMensaje = "el cliente no pertenece al usuario";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -60 )
        {
          wvarMensaje = "error en la grabacion de SEFMDECA #1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -61 )
        {
          wvarMensaje = "error en la grabacion de SEFMDECA #2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -62 )
        {
          wvarMensaje = "error en la grabacion de SEFMDECA #3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -63 )
        {
          wvarMensaje = "error en la grabacion de SEFMDECA #4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -64 )
        {
          wvarMensaje = "error en la grabacion de SEFMDECA #5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -65 )
        {
          wvarMensaje = "error en la grabacion de SEFMDECA #6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -66 )
        {
          wvarMensaje = "error en la grabacion de SEFMDECA #7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -67 )
        {
          wvarMensaje = "error en la grabacion de SEFMDECA #8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -68 )
        {
          wvarMensaje = "error en la grabacion de SEFMDECA #9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -69 )
        {
          wvarMensaje = "error en la grabacion de SEFMDECA #10";

        }

        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + wvarMensaje + String.valueOf( (char)(34) ) + " /></Response>" );
      }

      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      //!!!!!!!!!!!!!!!!!
      wvarStep = 1000;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
