package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * 14/11/2006 - DA
 * AML (Lavado de Dinero): Se agrega el campo UIFCUIT
 * Objetos del FrameWork
 */

public class lbaw_UpdPersona implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorLBA.lbaw_UpdPersona";
  static final String mcteStoreProc = "SPSNCV_PROD_SIFMPERS_UPDATE";
  static final String mcteStoreProcDom = "SPSNCV_PROD_SIFMDOMI_UPDATE";
  static final String mcteStoreProcCta = "SPSNCV_PROD_SIFMCUEN_UPDATE";
  /**
   *  Parametros XML de Entrada
   *  DATOS GENERALES
   */
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_NUMEDOCU = "//NUMEDOCU";
  static final String mcteParam_TIPODOCU = "//TIPODOCU";
  static final String mcteParam_CLIENAP1 = "//CLIENAP1";
  static final String mcteParam_CLIENAP2 = "//CLIENAP2";
  static final String mcteParam_CLIENNOM = "//CLIENNOM";
  static final String mcteParam_NACIMANN = "//NACIMANN";
  static final String mcteParam_NACIMMES = "//NACIMMES";
  static final String mcteParam_NACIMDIA = "//NACIMDIA";
  static final String mcteParam_CLIENSEX = "//CLIENSEX";
  static final String mcteParam_CLIENEST = "//CLIENEST";
  static final String mcteParam_PAISSCOD = "//PAISSCOD";
  static final String mcteParam_IDIOMCOD = "//IDIOMCOD";
  static final String mcteParam_NUMHIJOS = "//NUMHIJOS";
  static final String mcteParam_EFECTANN = "//EFECTANN";
  static final String mcteParam_EFECTMES = "//EFECTMES";
  static final String mcteParam_EFECTDIA = "//EFECTDIA";
  static final String mcteParam_CLIENTIP = "//CLIENTIP";
  static final String mcteParam_CLIENFUM = "//CLIENFUM";
  static final String mcteParam_CLIENDOZ = "//CLIENDOZ";
  static final String mcteParam_ABRIDTIP = "//ABRIDTIP";
  static final String mcteParam_ABRIDNUM = "//ABRIDNUM";
  static final String mcteParam_CLIENORG = "//CLIENORG";
  static final String mcteParam_PERSOTIP = "//PERSOTIP";
  static final String mcteParam_DOMICSEC = "//DOMICSEC";
  static final String mcteParam_CLIENCLA = "//CLIENCLA";
  static final String mcteParam_FALLEANN = "//FALLEANN";
  static final String mcteParam_FALLEMES = "//FALLEMES";
  static final String mcteParam_FALLEDIA = "//FALLEDIA";
  static final String mcteParam_FBAJAANN = "//FBAJAANN";
  static final String mcteParam_FBAJAMES = "//FBAJAMES";
  static final String mcteParam_FBAJADIA = "//FBAJADIA";
  static final String mcteParam_EMAIL = "//EMAIL";
  static final String mcteParam_UIFCUIT = "//UIFCUIT";
  /**
   *  Domicilios
   */
  static final String mcteParam_DOMICILIOS = "//DOMICILIOS/DOMICILIO";
  static final String mcteParam_DCLIENSEC = "CLIENSEC";
  static final String mcteParam_DDOMICSEC = "DOMICSEC";
  static final String mcteParam_DOMICCAL = "DOMICCAL";
  static final String mcteParam_DOMICDOM = "DOMICDOM";
  static final String mcteParam_DOMICDNU = "DOMICDNU";
  static final String mcteParam_DOMICESC = "DOMICESC";
  static final String mcteParam_DOMICPIS = "DOMICPIS";
  static final String mcteParam_DOMICPTA = "DOMICPTA";
  static final String mcteParam_DOMICPOB = "DOMICPOB";
  static final String mcteParam_DOMICCPO = "DOMICCPO";
  static final String mcteParam_PROVICOD = "PROVICOD";
  static final String mcteParam_DPAISSCOD = "PAISSCOD";
  static final String mcteParam_TELCOD = "TELCOD";
  static final String mcteParam_TELNRO = "TELNRO";
  static final String mcteParam_PRINCIPAL = "PRINCIPAL";
  /**
   *  Cuentas
   */
  static final String mcteParam_CUENTAS = "//CUENTAS/CUENTA";
  static final String mcteParam_CCLIENSEC = "CLIENSEC";
  static final String mcteParam_CUENTSEC = "CUENTSEC";
  static final String mcteParam_TIPOCUEN = "TIPOCUEN";
  static final String mcteParam_COBROTIP = "COBROTIP";
  static final String mcteParam_BANCOCOD = "BANCOCOD";
  static final String mcteParam_SUCURCOD = "SUCURCOD";
  static final String mcteParam_CUENTDC = "CUENTDC";
  static final String mcteParam_CUENNUME = "CUENNUME";
  static final String mcteParam_TARJECOD = "TARJECOD";
  static final String mcteParam_VENCIANN = "VENCIANN";
  static final String mcteParam_VENCIMES = "VENCIMES";
  static final String mcteParam_VENCIDIA = "VENCIDIA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private com.qbe.services.HSBCInterfaces.IDBConnection mobjHSBC_DBCnn = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String wvarCLIENSEC = "";
    String wvarDCLIENSEC = "";
    String wvarCCLIENSEC = "";
    String wvarNUMEDOCU = "";
    String wvarTIPODOCU = "";
    String wvarCLIENAP1 = "";
    String wvarCLIENAP2 = "";
    String wvarCLIENNOM = "";
    String wvarNACIMANN = "";
    String wvarNACIMMES = "";
    String wvarNACIMDIA = "";
    String wvarCLIENSEX = "";
    String wvarCLIENEST = "";
    String wvarPAISSCOD = "";
    String wvarIDIOMCOD = "";
    String wvarNUMHIJOS = "";
    String wvarEFECTANN = "";
    String wvarEFECTMES = "";
    String wvarEFECTDIA = "";
    String wvarCLIENTIP = "";
    String wvarCLIENFUM = "";
    String wvarCLIENDOZ = "";
    String wvarABRIDTIP = "";
    String wvarABRIDNUM = "";
    String wvarCLIENORG = "";
    String wvarPERSOTIP = "";
    String wvarDOMICSEC = "";
    String wvarCLIENCLA = "";
    String wvarFALLEANN = "";
    String wvarFALLEMES = "";
    String wvarFALLEDIA = "";
    String wvarFBAJAANN = "";
    String wvarFBAJAMES = "";
    String wvarFBAJADIA = "";
    String wvarEMAIL = "";
    String wvarUIFCUIT = "";
    String wvarDDOMICSEC = "";
    String wvarDOMICCAL = "";
    String wvarDOMICDOM = "";
    String wvarDOMICDNU = "";
    String wvarDOMICESC = "";
    String wvarDOMICPIS = "";
    String wvarDOMICPTA = "";
    String wvarDOMICPOB = "";
    String wvarDOMICCPO = "";
    String wvarPROVICOD = "";
    String wvarDPAISSCOD = "";
    String wvarTELCOD = "";
    String wvarTELNRO = "";
    String wvarPRINCIPAL = "";
    String wvarCUENTSEC = "";
    String wvarTIPOCUEN = "";
    String wvarCOBROTIP = "";
    String wvarBANCOCOD = "";
    String wvarSUCURCOD = "";
    String wvarCUENTDC = "";
    String wvarCUENNUME = "";
    String wvarTARJECOD = "";
    String wvarVENCIANN = "";
    String wvarVENCIMES = "";
    String wvarVENCIDIA = "";
    //
    //
    // DATOS GENERALES
    // DATOS Domicilios
    // DATOS Cuentas
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      // DATOS GENERALES
      wvarCLIENSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC ) */ );
      wvarNUMEDOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU ) */ );
      wvarTIPODOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU ) */ );
      wvarCLIENAP1 = Strings.trim( Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENAP1 ) */ ) + Strings.space( 20 ), 20 ) );
      wvarCLIENAP2 = Strings.trim( Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENAP2 ) */ ) + Strings.space( 20 ), 20 ) );
      wvarCLIENNOM = Strings.trim( Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENNOM ) */ ) + Strings.space( 20 ), 20 ) );
      wvarNACIMANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NACIMANN ) */ );
      wvarNACIMMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NACIMMES ) */ );
      wvarNACIMDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NACIMDIA ) */ );
      wvarCLIENSEX = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEX ) */ );
      wvarCLIENEST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENEST ) */ );
      wvarPAISSCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAISSCOD ) */ );
      wvarIDIOMCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_IDIOMCOD ) */ );
      wvarNUMHIJOS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMHIJOS ) */ );
      wvarEFECTANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTANN ) */ );
      wvarEFECTMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTMES ) */ );
      wvarEFECTDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTDIA ) */ );
      wvarCLIENTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENTIP ) */ );
      wvarCLIENFUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENFUM ) */ );
      wvarCLIENDOZ = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENDOZ ) */ );
      wvarABRIDTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ABRIDTIP ) */ );
      wvarABRIDNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ABRIDNUM ) */ );
      wvarCLIENORG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENORG ) */ );
      wvarPERSOTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PERSOTIP ) */ );
      wvarDOMICSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICSEC ) */ );
      wvarCLIENCLA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENCLA ) */ );
      wvarFALLEANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FALLEANN ) */ );
      wvarFALLEMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FALLEMES ) */ );
      wvarFALLEDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FALLEDIA ) */ );
      wvarFBAJAANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FBAJAANN ) */ );
      wvarFBAJAMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FBAJAMES ) */ );
      wvarFBAJADIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FBAJADIA ) */ );
      wvarEMAIL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMAIL ) */ );

      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_UIFCUIT ) */ == (org.w3c.dom.Node) null) )
      {
        wvarUIFCUIT = Strings.trim( Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_UIFCUIT ) */ ) + Strings.space( 11 ), 11 ) );
      }
      else
      {
        wvarUIFCUIT = "";
      }


      wvarStep = 30;
      wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      //
      wobjDBCmd = new Command();
      //
      // Personas
      wvarStep = 40;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 50;
      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 60;
      wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCLIENSEC ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 70;
      wobjDBParm = new Parameter( "@NUMEDOCU", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( wvarNUMEDOCU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 80;
      wobjDBParm = new Parameter( "@TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarTIPODOCU ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 90;
      wobjDBParm = new Parameter( "@CLIENAP1", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( wvarCLIENAP1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 100;
      wobjDBParm = new Parameter( "@CLIENAP2", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( wvarCLIENAP2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 110;
      wobjDBParm = new Parameter( "@CLIENNOM", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( wvarCLIENNOM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 120;
      wobjDBParm = new Parameter( "@NACIMANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarNACIMANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 130;
      wobjDBParm = new Parameter( "@NACIMMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarNACIMMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 140;
      wobjDBParm = new Parameter( "@NACIMDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarNACIMDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 150;
      wobjDBParm = new Parameter( "@CLIENSEX", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarCLIENSEX ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 160;
      wobjDBParm = new Parameter( "@CLIENEST", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarCLIENEST ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 170;
      wobjDBParm = new Parameter( "@PAISSCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarPAISSCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 180;
      wobjDBParm = new Parameter( "@IDIOMCOD", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( wvarIDIOMCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 190;
      wobjDBParm = new Parameter( "@NUMHIJOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarNUMHIJOS ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 200;
      wobjDBParm = new Parameter( "@EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarEFECTANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 210;
      wobjDBParm = new Parameter( "@EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarEFECTMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 220;
      wobjDBParm = new Parameter( "@EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarEFECTDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 230;
      wobjDBParm = new Parameter( "@CLIENTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarCLIENTIP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 240;
      wobjDBParm = new Parameter( "@CLIENFUM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarCLIENFUM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 250;
      wobjDBParm = new Parameter( "@CLIENDOZ", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarCLIENDOZ ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 260;
      wobjDBParm = new Parameter( "@ABRIDTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarABRIDTIP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 270;
      wobjDBParm = new Parameter( "@ABRIDNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( wvarABRIDNUM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 280;
      wobjDBParm = new Parameter( "@CLIENORG", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( wvarCLIENORG ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 290;
      wobjDBParm = new Parameter( "@PERSOTIP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarPERSOTIP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 300;
      wobjDBParm = new Parameter( "@DOMICSEC", AdoConst.adChar, AdoConst.adParamInput, 3, new Variant( wvarDOMICSEC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 310;
      wobjDBParm = new Parameter( "@CLIENCLA", AdoConst.adChar, AdoConst.adParamInput, 9, new Variant( wvarCLIENCLA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 320;
      wobjDBParm = new Parameter( "@FALLEANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarFALLEANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 330;
      wobjDBParm = new Parameter( "@FALLEMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarFALLEMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 340;
      wobjDBParm = new Parameter( "@FALLEDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarFALLEDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 350;
      wobjDBParm = new Parameter( "@FBAJAANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarFBAJAANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 360;
      wobjDBParm = new Parameter( "@FBAJAMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarFBAJAMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 370;
      wobjDBParm = new Parameter( "@FBAJADIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarFBAJADIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 380;
      wobjDBParm = new Parameter( "@EMAIL", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant( wvarEMAIL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 381;
      wobjDBParm = new Parameter( "@UIFCUIT", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( wvarUIFCUIT ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      // Se ejecuta el stored de personas
      wvarStep = 390;
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
      //
      wvarStep = 400;
      if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == 0 )
      {
        // Ok
        // Domicilios (Pueden ser varios)
        wvarStep = 410;
        wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_DOMICILIOS ) */;
        //
        for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
        {
          // DATOS DOMICILIO
          wvarStep = 410;
          wvarDCLIENSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_DCLIENSEC ) */ );
          wvarDDOMICSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_DDOMICSEC ) */ );
          wvarDOMICCAL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_DOMICCAL ) */ );
          wvarDOMICDOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_DOMICDOM ) */ );
          wvarDOMICDNU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_DOMICDNU ) */ );
          wvarDOMICESC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_DOMICESC ) */ );
          wvarDOMICPIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_DOMICPIS ) */ );
          wvarDOMICPTA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_DOMICPTA ) */ );
          wvarDOMICPOB = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_DOMICPOB ) */ );
          wvarDOMICCPO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_DOMICCPO ) */ );
          wvarPROVICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_PROVICOD ) */ );
          wvarDPAISSCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_DPAISSCOD ) */ );
          wvarTELCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_TELCOD ) */ );
          wvarTELNRO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_TELNRO ) */ );
          wvarPRINCIPAL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_PRINCIPAL ) */ );

          wvarStep = 420;
          wobjDBCmd = (Command) null;
          wobjDBCmd = new Command();

          wvarStep = 430;
          wobjDBCmd.setActiveConnection( wobjDBCnn );
          wobjDBCmd.setCommandText( mcteStoreProcDom );
          wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
          //
          wvarStep = 440;
          wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wvarStep = 450;
          wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarDCLIENSEC ) );
          wobjDBParm.setPrecision( (byte)( 9 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 460;
          wobjDBParm = new Parameter( "@DOMICSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarDDOMICSEC ) );
          wobjDBParm.setPrecision( (byte)( 3 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 470;
          wobjDBParm = new Parameter( "@DOMICCAL", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( wvarDOMICCAL ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 480;
          wobjDBParm = new Parameter( "@DOMICDOM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( wvarDOMICDOM ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 490;
          wobjDBParm = new Parameter( "@DOMICDNU", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( wvarDOMICDNU ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 500;
          wobjDBParm = new Parameter( "@DOMICESC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarDOMICESC ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 510;
          wobjDBParm = new Parameter( "@DOMICPIS", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarDOMICPIS ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 520;
          wobjDBParm = new Parameter( "@DOMICPTA", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarDOMICPTA ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 530;
          wobjDBParm = new Parameter( "@DOMICPOB", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant( wvarDOMICPOB ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 540;
          wobjDBParm = new Parameter( "@DOMICCPO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarDOMICCPO ) );
          wobjDBParm.setPrecision( (byte)( 5 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 550;
          wobjDBParm = new Parameter( "@PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPROVICOD ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 560;
          wobjDBParm = new Parameter( "@PAISSCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarDPAISSCOD ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 570;
          wobjDBParm = new Parameter( "@TELCOD", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( wvarTELCOD ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 580;
          wobjDBParm = new Parameter( "@TELNRO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( wvarTELNRO ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 585;
          wobjDBParm = new Parameter( "@PRINCIPAL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarPRINCIPAL ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          // Se ejecuta el stored de domicilios
          wvarStep = 590;
          wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
          //
          wvarStep = 600;
          if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == 0 )
          {
            pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><DOMICILIO>" + wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue() + "</DOMICILIO></Response>" );
            // Mal el update de personas
          }
          else
          {
            wvarMensaje = "ERROR AL HACER UPDATE DE UN DOMICILIO";
            pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + wvarMensaje + " /></Response>" );
          }
          //
        }
        //
        wvarStep = 610;
        wobjXMLList = (org.w3c.dom.NodeList) null;
        //
        // Cuentas (Pueden ser varias)
        wvarStep = 620;
        wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_CUENTAS ) */;
        //
        for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
        {
          // DATOS CUENTA
          wvarCCLIENSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CCLIENSEC ) */ );
          wvarCUENTSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CUENTSEC ) */ );
          wvarTIPOCUEN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_TIPOCUEN ) */ );
          wvarCOBROTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_COBROTIP ) */ );
          wvarBANCOCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_BANCOCOD ) */ );
          wvarSUCURCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_SUCURCOD ) */ );
          wvarCUENTDC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CUENTDC ) */ );
          wvarCUENNUME = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CUENNUME ) */ );
          wvarTARJECOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_TARJECOD ) */ );
          wvarVENCIANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_VENCIANN ) */ );
          wvarVENCIMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_VENCIMES ) */ );
          wvarVENCIDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_VENCIDIA ) */ );

          wvarStep = 630;
          wobjDBCmd = (Command) null;
          wobjDBCmd = new Command();

          wvarStep = 640;
          wobjDBCmd.setActiveConnection( wobjDBCnn );
          wobjDBCmd.setCommandText( mcteStoreProcCta );
          wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
          //
          wvarStep = 650;
          wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          wvarStep = 660;
          wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCCLIENSEC ) );
          wobjDBParm.setPrecision( (byte)( 9 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 670;
          wobjDBParm = new Parameter( "@CUENTSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCUENTSEC ) );
          wobjDBParm.setPrecision( (byte)( 3 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 680;
          wobjDBParm = new Parameter( "@TIPOCUEN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarTIPOCUEN ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 690;
          wobjDBParm = new Parameter( "@COBROTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarCOBROTIP ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 700;
          wobjDBParm = new Parameter( "@BANCOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarBANCOCOD ) );
          wobjDBParm.setPrecision( (byte)( 4 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 710;
          wobjDBParm = new Parameter( "@SUCURCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarSUCURCOD ) );
          wobjDBParm.setPrecision( (byte)( 4 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 720;
          wobjDBParm = new Parameter( "@CUENTDC", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarCUENTDC ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 730;
          wobjDBParm = new Parameter( "@CUENNUME", AdoConst.adChar, AdoConst.adParamInput, 16, new Variant( wvarCUENNUME ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 740;
          wobjDBParm = new Parameter( "@TARJECOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarTARJECOD ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 750;
          wobjDBParm = new Parameter( "@VENCIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarVENCIANN ) );
          wobjDBParm.setPrecision( (byte)( 4 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 760;
          wobjDBParm = new Parameter( "@VENCIMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarVENCIMES ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 770;
          wobjDBParm = new Parameter( "@VENCIDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarVENCIDIA ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;
          //
          // Se ejecuta el stored de cuentas
          wvarStep = 780;
          wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
          //
          wvarStep = 790;
          if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == 0 )
          {
            pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><CUENTA>" + wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue() + "</CUENTA></Response>" );
            // Mal el update de cuentas
          }
          else
          {
            wvarMensaje = "ERROR AL HACER UPDATE DE UNA CUENTA";
            pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + wvarMensaje + " /></Response>" );
          }
          //
        }
        //
        wvarStep = 800;
        wobjXMLList = (org.w3c.dom.NodeList) null;

        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><PERSONAID>" + wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue() + "</PERSONAID></Response>" );
        // Mal el update de personas
      }
      else
      {
        wvarMensaje = "ERROR AL HACER UPDATE PERSONAS";
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + wvarMensaje + " /></Response>" );
      }

      wvarStep = 810;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      wvarStep = 820;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
