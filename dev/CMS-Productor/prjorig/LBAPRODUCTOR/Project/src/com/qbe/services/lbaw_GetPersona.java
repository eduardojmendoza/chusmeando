package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * 14/11/2006 - DA
 * AML (Lavado de Dinero): Se agrega el campo UIFCUIT
 * Objetos del FrameWork
 */

public class lbaw_GetPersona implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorLBA.lbaw_GetPersona";
  static final String mcteStoreProc = "SPSNCV_PROD_SIFMPERS_SELECT_ITEM";
  static final String mcteStoreProcDom = "SPSNCV_PROD_SIFMDOMI_SELECT";
  static final String mcteStoreProcCta = "SPSNCV_PROD_SIFMCUEN_SELECT";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Recordset wrstDBResultDom = null;
    Recordset wrstDBResultCta = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCLIENSEC = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarCLIENSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC ) */ );
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 20;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //
      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      //
      wvarStep = 40;
      wobjDBCmd = new Command();
      //
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCLIENSEC ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 50;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );
      //
      wvarStep = 60;
      if( ! (wrstDBResult.isEOF()) )
      {
        //
        wvarStep = 70;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 80;
        /*unsup wrstDBResult.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
        //
        wvarStep = 90;
        //unsup wobjXSLResponse.async = false;
        wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
        //
        wvarStep = 100;
        wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
        //
        wvarStep = 120;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        // A partir de aca domicilios
        wvarStep = 130;
        wobjDBCmd = new Command();
        //
        wobjDBCmd.setActiveConnection( wobjDBCnn );
        wobjDBCmd.setCommandText( mcteStoreProcDom );
        wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
        //
        wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCLIENSEC ) );
        wobjDBParm.setPrecision( (byte)( 9 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 140;
        wrstDBResultDom = wobjDBCmd.execute();
        wrstDBResultDom.setActiveConnection( (Connection) null );
        //
        wvarStep = 150;
        if( ! (wrstDBResultDom.isEOF()) )
        {
          //
          wvarStep = 160;
          wobjXMLResponse = new diamondedge.util.XmlDom();
          wobjXSLResponse = new diamondedge.util.XmlDom();
          //
          wvarStep = 180;
          /*unsup wrstDBResultDom.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
          //
          wvarStep = 190;
          //unsup wobjXSLResponse.async = false;
          wobjXSLResponse.loadXML( invoke( "p_GetXSLDom", new Variant[] {} ) );
          //
          wvarStep = 200;
          wvarResult = wvarResult + "<DOMICILIOS>" + Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" ) + "</DOMICILIOS>";
          //
          wvarStep = 210;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          wobjXSLResponse = (diamondedge.util.XmlDom) null;
          //
        }
        else
        {
          wvarStep = 230;
          wvarResult = wvarResult + "<DOMICILIOS></DOMICILIOS>";
        }
        //
        // fin domicilios
        // a partir de aca cuentas
        //
        wvarStep = 240;
        wobjDBCmd = new Command();
        //
        wobjDBCmd.setActiveConnection( wobjDBCnn );
        wobjDBCmd.setCommandText( mcteStoreProcCta );
        wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
        //
        wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCLIENSEC ) );
        wobjDBParm.setPrecision( (byte)( 9 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 250;
        wrstDBResultCta = wobjDBCmd.execute();
        wrstDBResultCta.setActiveConnection( (Connection) null );
        //
        wvarStep = 260;
        if( ! (wrstDBResultCta.isEOF()) )
        {
          //
          wvarStep = 270;
          wobjXMLResponse = new diamondedge.util.XmlDom();
          wobjXSLResponse = new diamondedge.util.XmlDom();
          //
          wvarStep = 280;
          /*unsup wrstDBResultCta.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
          //
          wvarStep = 290;
          //unsup wobjXSLResponse.async = false;
          wobjXSLResponse.loadXML( invoke( "p_GetXSLCta", new Variant[] {} ) );
          //
          wvarStep = 300;
          wvarResult = "<ROW>" + wvarResult + "<CUENTAS>" + Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" ) + "</CUENTAS>" + "</ROW>";
          //
          wvarStep = 310;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          wobjXSLResponse = (diamondedge.util.XmlDom) null;
          //
        }
        else
        {
          wvarStep = 320;
          wvarResult = "<ROW>" + wvarResult + "<CUENTAS></CUENTAS>" + "</ROW>";
        }
        // Fin Cuentas
        //
        wvarStep = 330;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        wvarStep = 340;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS." + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //
      wvarStep = 350;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 360;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 370;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 380;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 390;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 400;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 410;
      if( ! (wrstDBResultDom == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResultDom.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResultDom.close();
        }
      }
      //
      wvarStep = 420;
      wrstDBResultDom = (Recordset) null;
      //
      wvarStep = 430;
      if( ! (wrstDBResultCta == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResultCta.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResultCta.close();
        }
      }
      //
      wvarStep = 420;
      wrstDBResultCta = (Recordset) null;
      //
      wvarStep = 430;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        if( ! (wrstDBResult == (Recordset) null) )
        {
          if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='PERSONA'>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENSEC'><xsl:value-of select='@CLIENSEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PAISSDES'><xsl:value-of select='@PAISSDES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOCUMDAB'><xsl:value-of select='@DOCUMDAB' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENDES'><xsl:value-of select='@CLIENDES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NUMEDOCU'><xsl:value-of select='@NUMEDOCU' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TIPODOCU'><xsl:value-of select='@TIPODOCU' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENAP1'><xsl:value-of select='@CLIENAP1' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENAP2'><xsl:value-of select='@CLIENAP2' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENNOM'><xsl:value-of select='@CLIENNOM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NACIMANN'><xsl:value-of select='@NACIMANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NACIMMES'><xsl:value-of select='@NACIMMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NACIMDIA'><xsl:value-of select='@NACIMDIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENSEX'><xsl:value-of select='@CLIENSEX' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENEST'><xsl:value-of select='@CLIENEST' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PAISSCOD'><xsl:value-of select='@PAISSCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='IDIOMCOD'><xsl:value-of select='@IDIOMCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NUMHIJOS'><xsl:value-of select='@NUMHIJOS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EFECTANN'><xsl:value-of select='@EFECTANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EFECTMES'><xsl:value-of select='@EFECTMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EFECTDIA'><xsl:value-of select='@EFECTDIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENTIP'><xsl:value-of select='@CLIENTIP' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENFUM'><xsl:value-of select='@CLIENFUM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENDOZ'><xsl:value-of select='@CLIENDOZ' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ABRIDTIP'><xsl:value-of select='@ABRIDTIP' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ABRIDNUM'><xsl:value-of select='@ABRIDNUM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENORG'><xsl:value-of select='@CLIENORG' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PERSOTIP'><xsl:value-of select='@PERSOTIP' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICSEC'><xsl:value-of select='@DOMICSEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENCLA'><xsl:value-of select='@CLIENCLA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FALLEANN'><xsl:value-of select='@FALLEANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FALLEMES'><xsl:value-of select='@FALLEMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FALLEDIA'><xsl:value-of select='@FALLEDIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FBAJAANN'><xsl:value-of select='@FBAJAANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FBAJAMES'><xsl:value-of select='@FBAJAMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FBAJADIA'><xsl:value-of select='@FBAJADIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EMAIL'><xsl:value-of select='@EMAIL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='UIFCUIT'><xsl:value-of select='@UIFCUIT' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='USUARCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='PAISSDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOCUMDAB'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENAP1'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENAP2'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENNOM'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENSEX'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENEST'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENTIP'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENFUM'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENDOZ'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='ABRIDTIP'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='ABRIDNUM'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENORG'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='PERSOTIP'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='EMAIL'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private String p_GetXSLDom() throws Exception
  {
    String p_GetXSLDom = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='DOMICILIO'>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICSEC'><xsl:value-of select='@DOMICSEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICCAL'><xsl:value-of select='@DOMICCAL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICDOM'><xsl:value-of select='@DOMICDOM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICDNU'><xsl:value-of select='@DOMICDNU' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICESC'><xsl:value-of select='@DOMICESC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICPIS'><xsl:value-of select='@DOMICPIS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICPTA'><xsl:value-of select='@DOMICPTA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICPOB'><xsl:value-of select='@DOMICPOB' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICCPO'><xsl:value-of select='@DOMICCPO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PROVICOD'><xsl:value-of select='@PROVICOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PAISSCOD'><xsl:value-of select='@PAISSCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TELCOD'><xsl:value-of select='@TELCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TELNRO'><xsl:value-of select='@TELNRO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PRINCIPAL'><xsl:value-of select='@PRINCIPAL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PROVIDES'><xsl:value-of select='@PROVIDES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";

    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICSEC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICCAL'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICDOM'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICDNU'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICESC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICPIS'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICPTA'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOMICPOB'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='PAISSCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='TELCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='TELNRO'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLDom = wvarStrXSL;
    return p_GetXSLDom;
  }

  private String p_GetXSLCta() throws Exception
  {
    String p_GetXSLCta = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='CUENTA'>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CUENTSEC'><xsl:value-of select='@CUENTSEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TIPOCUEN'><xsl:value-of select='@TIPOCUEN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='COBROTIP'><xsl:value-of select='@COBROTIP' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='BANCOCOD'><xsl:value-of select='@BANCOCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SUCURCOD'><xsl:value-of select='@SUCURCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CUENTDC'><xsl:value-of select='@CUENTDC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CUENNUME'><xsl:value-of select='@CUENNUME' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TARJECOD'><xsl:value-of select='@TARJECOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='VENCIANN'><xsl:value-of select='@VENCIANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='VENCIMES'><xsl:value-of select='@VENCIMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='VENCIDIA'><xsl:value-of select='@VENCIDIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='COBRODES'><xsl:value-of select='@COBRODES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='USUARCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='TIPOCUEN'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='COBROTIP'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CUENTDC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CUENNUME'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='COBRODES'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLCta = wvarStrXSL;
    return p_GetXSLCta;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
