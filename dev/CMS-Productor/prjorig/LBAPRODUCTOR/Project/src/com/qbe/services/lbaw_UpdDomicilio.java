package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_UpdDomicilio implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorLBA.lbaw_UpdDomicilio";
  static final String mcteStoreProc = "SPSNCV_PROD_SIFMDOMI_UPDATE";
  /**
   *  Parametros XML de Entrada
   */
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_DDOMICSEC = "//DOMICSEC";
  static final String mcteParam_DOMICCAL = "//DOMICCAL";
  static final String mcteParam_DOMICDOM = "//DOMICDOM";
  static final String mcteParam_DOMICDNU = "//DOMICDNU";
  static final String mcteParam_DOMICESC = "//DOMICESC";
  static final String mcteParam_DOMICPIS = "//DOMICPIS";
  static final String mcteParam_DOMICPTA = "//DOMICPTA";
  static final String mcteParam_DOMICPOB = "//DOMICPOB";
  static final String mcteParam_DOMICCPO = "//DOMICCPO";
  static final String mcteParam_PROVICOD = "//PROVICOD";
  static final String mcteParam_PAISSCOD = "//PAISSCOD";
  static final String mcteParam_TELCOD = "//TELCOD";
  static final String mcteParam_TELNRO = "//TELNRO";
  static final String mcteParam_PRINCIPAL = "//PRINCIPAL";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private com.qbe.services.HSBCInterfaces.IDBConnection mobjHSBC_DBCnn = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String wvarCLIENSEC = "";
    String wvarDDOMICSEC = "";
    String wvarDOMICCAL = "";
    String wvarDOMICDOM = "";
    String wvarDOMICDNU = "";
    String wvarDOMICESC = "";
    String wvarDOMICPIS = "";
    String wvarDOMICPTA = "";
    String wvarDOMICPOB = "";
    String wvarDOMICCPO = "";
    String wvarPROVICOD = "";
    String wvarPAISSCOD = "";
    String wvarTELCOD = "";
    String wvarTELNRO = "";
    String wvarPRINCIPAL = "";
    //
    //
    // DATOS GENERALES
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      wvarCLIENSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC ) */ );
      wvarDDOMICSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DDOMICSEC ) */ );
      wvarDOMICCAL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICCAL ) */ );
      wvarDOMICDOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOM ) */ );
      wvarDOMICDNU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDNU ) */ );
      wvarDOMICESC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICESC ) */ );
      wvarDOMICPIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPIS ) */ );
      wvarDOMICPTA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPTA ) */ );
      wvarDOMICPOB = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOB ) */ );
      wvarDOMICCPO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICCPO ) */ );
      wvarPROVICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROVICOD ) */ );
      wvarPAISSCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAISSCOD ) */ );
      wvarTELCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TELCOD ) */ );
      wvarTELNRO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TELNRO ) */ );
      wvarPRINCIPAL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PRINCIPAL ) */ );

      wvarStep = 30;
      wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      wobjDBCmd = new Command();

      wvarStep = 40;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 50;
      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 60;
      wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCLIENSEC ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 65;
      wobjDBParm = new Parameter( "@DOMICSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarDDOMICSEC ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 70;
      wobjDBParm = new Parameter( "@DOMICCAL", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( wvarDOMICCAL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 80;
      wobjDBParm = new Parameter( "@DOMICDOM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( wvarDOMICDOM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 90;
      wobjDBParm = new Parameter( "@DOMICDNU", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( wvarDOMICDNU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 100;
      wobjDBParm = new Parameter( "@DOMICESC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarDOMICESC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 110;
      wobjDBParm = new Parameter( "@DOMICPIS", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarDOMICPIS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 120;
      wobjDBParm = new Parameter( "@DOMICPTA", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarDOMICPTA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 130;
      wobjDBParm = new Parameter( "@DOMICPOB", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant( wvarDOMICPOB ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 140;
      wobjDBParm = new Parameter( "@DOMICCPO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarDOMICCPO ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 150;
      wobjDBParm = new Parameter( "@PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPROVICOD ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 160;
      wobjDBParm = new Parameter( "@PAISSCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarPAISSCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 170;
      wobjDBParm = new Parameter( "@TELCOD", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( wvarTELCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 180;
      wobjDBParm = new Parameter( "@TELNRO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( wvarTELNRO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 190;
      wobjDBParm = new Parameter( "@PRINCIPAL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarPRINCIPAL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 200;
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );

      wvarStep = 210;
      if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      else
      {
        
        if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -1 )
        {
          wvarMensaje = "ERROR AL ACTUALIZAR";
        }

        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + wvarMensaje + " /></Response>" );
      }

      wvarStep = 220;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      wvarStep = 230;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
