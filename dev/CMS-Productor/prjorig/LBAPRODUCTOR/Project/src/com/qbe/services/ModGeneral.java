package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class ModGeneral
{
  public static final String gcteDB = "lbawA_OfVirtualLBA.udl";
  /**
   *  Parametros XML de la Instalacion
   */
  public static final String gcteParamFileName = "Productor.xml";
  public static final String gcteNodosAutoScoring = "//AUTOSCORING";
  public static final String gcteRAMOPCOD = "/RAMOPCOD";
  public static final String gcteINSTACOD = "/INSTACOD";
}
