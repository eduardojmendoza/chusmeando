package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_UpdSolicProd implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorLBA.lbaw_UpdSolicProd";
  static final String mcteStoreProc = "SPSNCV_PROD_SOLI_UPDATE";
  /**
   *  Parametros XML de Entrada
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_SUPLENUM = "//SUPLENUM";
  static final String mcteParam_FRANQCOD = "//FRANQCOD";
  static final String mcteParam_PQTDES = "//PQTDES";
  static final String mcteParam_PLANCOD = "//PLANCOD";
  static final String mcteParam_HIJOS1416 = "//HIJOS1416";
  static final String mcteParam_HIJOS1729 = "//HIJOS1729";
  static final String mcteParam_HIJOS1729ND = "//HIJOS1729ND";
  static final String mcteParam_ZONA = "//ZONA";
  static final String mcteParam_CODPROV = "//CODPROV";
  static final String mcteParam_SUMALBA = "//SUMALBA";
  static final String mcteParam_CLIENIVA = "//CLIENIVA";
  static final String mcteParam_SUMASEG = "//SUMASEG";
  static final String mcteParam_CLUB_LBA = "//CLUB_LBA";
  static final String mcteParam_CPAANO = "//CPAANO";
  static final String mcteParam_CTAKMS = "//CTAKMS";
  static final String mcteParam_ESCERO = "//ESCERO";
  static final String mcteParam_TIENEPLAN = "//TIENEPLAN";
  static final String mcteParam_COND_ADIC = "//COND_ADIC";
  static final String mcteParam_ASEG_ADIC = "//ASEG_ADIC";
  static final String mcteParam_FH_NAC = "//FH_NAC";
  static final String mcteParam_SEXO = "//SEXO";
  static final String mcteParam_ESTCIV = "//ESTCIV";
  static final String mcteParam_SIFMVEHI_DES = "//SIFMVEHI_DES";
  static final String mcteParam_PROFECOD = "//PROFECOD";
  static final String mcteParam_SIACCESORIOS = "//SIACCESORIOS";
  static final String mcteParam_REFERIDO = "//REFERIDO";
  static final String mcteParam_MOTORNUM = "//MOTORNUM";
  static final String mcteParam_CHASINUM = "//CHASINUM";
  static final String mcteParam_PATENNUM = "//PATENNUM";
  static final String mcteParam_AUMARCOD = "//AUMARCOD";
  static final String mcteParam_AUMODCOD = "//AUMODCOD";
  static final String mcteParam_AUSUBCOD = "//AUSUBCOD";
  static final String mcteParam_AUADICOD = "//AUADICOD";
  static final String mcteParam_AUMODORI = "//AUMODORI";
  static final String mcteParam_AUUSOCOD = "//AUUSOCOD";
  static final String mcteParam_AUVTVCOD = "//AUVTVCOD";
  static final String mcteParam_AUVTVDIA = "//AUVTVDIA";
  static final String mcteParam_AUVTVMES = "//AUVTVMES";
  static final String mcteParam_AUVTVANN = "//AUVTVANN";
  static final String mcteParam_VEHCLRCOD = "//VEHCLRCOD";
  static final String mcteParam_AUKLMNUM = "//AUKLMNUM";
  static final String mcteParam_FABRICAN = "//FABRICAN";
  static final String mcteParam_FABRICMES = "//FABRICMES";
  static final String mcteParam_GUGARAGE = "//GUGARAGE";
  static final String mcteParam_GUDOMICI = "//GUDOMICI";
  static final String mcteParam_AUCATCOD = "//AUCATCOD";
  static final String mcteParam_AUTIPCOD = "//AUTIPCOD";
  static final String mcteParam_AUCIASAN = "//AUCIASAN";
  static final String mcteParam_AUANTANN = "//AUANTANN";
  static final String mcteParam_AUNUMSIN = "//AUNUMSIN";
  static final String mcteParam_AUNUMKMT = "//AUNUMKMT";
  static final String mcteParam_AUUSOGNC = "//AUUSOGNC";
  static final String mcteParam_USUARCOD = "//USUARCOD";
  static final String mcteParam_SITUCPOL = "//SITUCPOL";
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_NUMEDOCU = "//NUMEDOCU";
  static final String mcteParam_TIPODOCU = "//TIPODOCU";
  static final String mcteParam_DOMICSEC = "//DOMICSEC";
  static final String mcteParam_CUENTSEC = "//CUENTSEC";
  static final String mcteParam_COBROFOR = "//COBROFOR";
  static final String mcteParam_EDADACTU = "//EDADACTU";
  /**
   *  Coberturas
   */
  static final String mcteParam_COT_NRO = "//COT_NRO";
  static final String mcteParam_COBERCOD = "//COBERCOD";
  static final String mcteParam_COBERORD = "//COBERORD";
  static final String mcteParam_CAPITASG = "//CAPITASG";
  static final String mcteParam_CAPITIMP = "//CAPITIMP";
  /**
   *  Coberturas
   */
  static final String mcteParam_ASEGURADOS = "//ASEGURADOS/ASEGURADO";
  static final String mcteParam_DOCUMDAT = "DOCUMDAT";
  static final String mcteParam_NOMBREAS = "NOMBREAS";
  static final String mcteParam_CAMP_CODIGO = "//CAMP_CODIGO";
  static final String mcteParam_CAMP_DESC = "//CAMP_DESC";
  static final String mcteParam_LEGAJO_GTE = "//LEGAJO_GTE";
  static final String mcteParam_NRO_PROD = "//NRO_PROD";
  static final String mcteParam_SUCURSAL_CODIGO = "//SUCURSAL_CODIGO";
  static final String mcteParam_LEGAJO_VEND = "//LEGAJO_VEND";
  /**
   *  Coberturas
   */
  static final String mcteParam_CONDUCTORES = "//HIJOS/HIJO";
  static final String mcteParam_CONDUAPE = "APELLIDOHIJO";
  static final String mcteParam_CONDUNOM = "NOMBREHIJO";
  static final String mcteParam_CONDUFEC = "NACIMHIJO";
  static final String mcteParam_CONDUSEX = "SEXOHIJO";
  static final String mcteParam_CONDUEST = "ESTADOHIJO";
  static final String mcteParam_CONDUEXC = "INCLUIDO";
  /**
   *  Accesorios
   */
  static final String mcteParam_ACCESORIOS = "//ACCESORIOS/ACCESORIO";
  static final String mcteParam_AUACCCOD = "CODIGOACC";
  static final String mcteParam_AUVEASUM = "PRECIOACC";
  static final String mcteParam_AUVEADES = "DESCRIPCIONACC";
  /**
   *  Inspecciones y acreedor Prendario
   */
  static final String mcteParam_SITUCEST = "//SITUCEST";
  static final String mcteParam_INSPECOD = "//INSPECOD";
  static final String mcteParam_INSPECTOR = "//INSPECTOR";
  static final String mcteParam_INSPEADOM = "//INSPEADOM";
  static final String mcteParam_OBSERTXT = "//OBSERTXT";
  static final String mcteParam_INSPENUM = "//INSPENUM";
  static final String mcteParam_CENTRCOD_INS = "//CENTRCOD_INS";
  static final String mcteParam_CENTRDES = "//CENTRDES";
  static final String mcteParam_INSPECC = "//INSPECC";
  static final String mcteParam_RASTREO = "//RASTREO";
  static final String mcteParam_ALARMCOD = "//ALARMCOD";
  static final String mcteParam_NUMEDOCU_ACRE = "//NUMEDOCU_ACRE";
  static final String mcteParam_TIPODOCU_ACRE = "//TIPODOCU_ACRE";
  static final String mcteParam_APELLIDO = "//APELLIDO";
  static final String mcteParam_NOMBRES = "//NOMBRES";
  static final String mcteParam_DOMICDOM = "//DOMICDOM";
  static final String mcteParam_DOMICDNU = "//DOMICDNU";
  static final String mcteParam_DOMICESC = "//DOMICESC";
  static final String mcteParam_DOMICPIS = "//DOMICPIS";
  static final String mcteParam_DOMICPTA = "//DOMICPTA";
  static final String mcteParam_DOMICPOB = "//DOMICPOB";
  static final String mcteParam_DOMICCPO = "//DOMICCPO";
  static final String mcteParam_PROVICOD = "//PROVICOD";
  static final String mcteParam_PAISSCOD = "//PAISSCOD";
  static final String mcteParam_BANCOCOD = "//BANCOCOD";
  static final String mcteParam_COBROCOD = "//COBROCOD";
  static final String mcteParam_EFECTANN = "//EFECTANN2";
  static final String mcteParam_EFECTMES = "//EFECTMES";
  static final String mcteParam_EFECTDIA = "//EFECTDIA";
  static final String mcteParam_INSPEANN = "//INSPEANN";
  static final String mcteParam_INSPEMES = "//INSPEMES";
  static final String mcteParam_INSPEDIA = "//INSPEDIA";
  static final String mcteParam_ENTDOMSC = "//ENTDOMSC";
  static final String mcteParam_ANOTACIO = "//ANOTACIO";
  static final String mcteParam_PRECIO_MENSUAL = "//PRECIO_MENSUAL";
  static final String mcteParam_TIPO_ACTU = "//TIPO_ACTU";
  static final String mcteParam_CUITNUME = "//CUITNUME";
  static final String mcteParam_RAZONSOC = "//RAZONSOC";
  static final String mcteParam_CLIEIBTP = "//CLIEIBTP";
  static final String mcteParam_NROIIBB = "//NROIIBB";
  static final String mcteParam_LUNETA = "//LUNETA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private com.qbe.services.HSBCInterfaces.IDBConnection mobjHSBC_DBCnn = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLCoberturas = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    Object wobjHSBC_DBCnn = null;
    Object wobjClass = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String wvarRequest = "";
    String wvarResponse = "";
    String mvarCERTIPOL = "";
    String mvarCERTIANN = "";
    String mvarCERTISEC = "";
    String mvarSUPLENUM = "";
    String mvarFRANQCOD = "";
    String mvarPQTDES = "";
    String mvarPLANCOD = "";
    String mvarHIJOS1416 = "";
    String mvarHIJOS1729 = "";
    String mvarHIJOS1729ND = "";
    String mvarZONA = "";
    String mvarCODPROV = "";
    String mvarSUMALBA = "";
    String mvarCLIENIVA = "";
    String mvarSUMASEG = "";
    String mvarCLUB_LBA = "";
    String mvarCPAANO = "";
    String mvarCTAKMS = "";
    String mvarESCERO = "";
    String mvarTIENEPLAN = "";
    String mvarCOND_ADIC = "";
    String mvarASEG_ADIC = "";
    String mvarFH_NAC = "";
    String mvarSEXO = "";
    String mvarESTCIV = "";
    String mvarSIFMVEHI_DES = "";
    String mvarPROFECOD = "";
    String mvarSIACCESORIOS = "";
    String mvarREFERIDO = "";
    String mvarMOTORNUM = "";
    String mvarCHASINUM = "";
    String mvarPATENNUM = "";
    String mvarAUMARCOD = "";
    String mvarAUMODCOD = "";
    String mvarAUSUBCOD = "";
    String mvarAUADICOD = "";
    String mvarAUMODORI = "";
    String mvarAUUSOCOD = "";
    String mvarAUVTVCOD = "";
    String mvarAUVTVDIA = "";
    String mvarAUVTVMES = "";
    String mvarAUVTVANN = "";
    String mvarVEHCLRCOD = "";
    String mvarAUKLMNUM = "";
    String mvarFABRICAN = "";
    String mvarFABRICMES = "";
    String mvarGUGARAGE = "";
    String mvarGUDOMICI = "";
    String mvarAUCATCOD = "";
    String mvarAUTIPCOD = "";
    String mvarAUCIASAN = "";
    String mvarAUANTANN = "";
    String mvarAUNUMSIN = "";
    String mvarAUNUMKMT = "";
    String mvarAUUSOGNC = "";
    String mvarUSUARCOD = "";
    String mvarSITUCPOL = "";
    String mvarCLIENSEC = "";
    String mvarNUMEDOCU = "";
    String mvarTIPODOCU = "";
    String mvarDOMICSEC = "";
    String mvarCUENTSEC = "";
    String mvarCOBROFOR = "";
    String mvarEDADACTU = "";
    String mvarCODPOST = "";
    String wvarCot_Nro = "";
    String mvarCOBERCOD = "";
    String mvarCOBERORD = "";
    String mvarCAPITASG = "";
    String mvarCAPITIMP = "";
    String mvarDOCUMDAT = "";
    String mvarNOMBREAS = "";
    String mvarCAMP_CODIGO = "";
    String mvarCAMP_DESC = "";
    String mvarLEGAJO_GTE = "";
    String mvarNRO_PROD = "";
    String mvarSUCURSAL_CODIGO = "";
    String mvarLEGAJO_VEND = "";
    String mvarCONDUAPE = "";
    String mvarCONDUNOM = "";
    String mvarCONDUFEC = "";
    String mvarCONDUSEX = "";
    String mvarCONDUEST = "";
    String mvarCONDUEXC = "";
    String mvarAUACCCOD = "";
    String mvarAUVEASUM = "";
    String mvarAUVEADES = "";
    String mvarAUVEADEP = "";
    String mvarSITUCEST = "";
    String mvarINSPECOD = "";
    String mvarINSPECTOR = "";
    String mvarINSPEADOM = "";
    String mvarOBSERTXT = "";
    String mvarINSPENUM = "";
    String mvarCENTRCOD_INS = "";
    String mvarCENTRDES = "";
    String mvarINSPECC = "";
    String mvarRASTREO = "";
    String mvarALARMCOD = "";
    String mvarNUMEDOCU_ACRE = "";
    String mvarTIPODOCU_ACRE = "";
    String mvarAPELLIDO = "";
    String mvarNOMBRES = "";
    String mvarDOMICDOM = "";
    String mvarDOMICDNU = "";
    String mvarDOMICESC = "";
    String mvarDOMICPIS = "";
    String mvarDOMICPTA = "";
    String mvarDOMICPOB = "";
    String mvarDOMICCPO = "";
    String mvarPROVICOD = "";
    String mvarPAISSCOD = "";
    String mvarBANCOCOD = "";
    String mvarCOBROCOD = "";
    String mvarEFECTANN = "";
    String mvarEFECTMES = "";
    String mvarEFECTDIA = "";
    String mvarINSPEANN = "";
    String mvarINSPEMES = "";
    String mvarINSPEDIA = "";
    String mvarENTDOMSC = "";
    String mvarANOTACIO = "";
    String mvarPRECIO_MENSUAL = "";
    String mvarTIPO_ACTU = "";
    String mvarCUITNUME = "";
    String mvarRAZONSOC = "";
    String mvarCLIEIBTP = "";
    String mvarNROIIBB = "";
    String mvarLUNETA = "";
    String mvarRAMOPCOD = "";
    String mvarPlanNCod = "";
    String mvarPOLIZSEC = "";
    String mvarPOLIZANN = "";
    String mvarSucurCod = "";
    String mvarAgentCla = "";
    String aa = "";
    int i = 0;
    //
    //
    // DATOS GENERALES
    // Ojo se usa para cotizar
    // Coberturas
    // Asegurados
    // Conductores
    // Accesorios
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // CARGO EL XML DE ENTRADA
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      // VUELVO A COTIZAR PARA EL PLAN SELECCIONADO SOLO EN EL CASO QUE TIPO_ACTU <> I
      mvarTIPO_ACTU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPO_ACTU ) */ );

      if( !mvarTIPO_ACTU.equals( "I" ) )
      {
        wvarStep = 20;
        //Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVLBAMQ.lbaw_GetCotizacion")
        wobjClass = new lbawA_OVMQCotizar.lbaw_OVGetCotiAUS();
        //error: function 'Execute' was not found.
        //unsup: Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
        wobjClass = null;
        //
        // GUARDO LAS COBERTURAS, LAS SUMAS ASEGURADAS, CAMPANIA, PRODUCTOR Y TELEFONO
        wvarStep = 30;
        wobjXMLCoberturas = new diamondedge.util.XmlDom();
        //unsup wobjXMLCoberturas.async = false;
        wobjXMLCoberturas.loadXML( wvarResponse );
        //controlo como devolvió la cotización
        if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "true" ) )
        {
          wvarCot_Nro = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_COT_NRO ) */ );
          diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUMASEG ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_SUMASEG ) */ ) );
          diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUMALBA ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_SUMALBA ) */ ) );
        }
        else
        {
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='No se pudo realizar la cotización correspondiente' /></Response>" );
          //unsup GoTo ErrorHandler
        }
      }
      //
      //
      wvarStep = 40;
      //Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
      //
      wvarStep = 50;
      // DATOS GENERALES
      mvarRAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD ) */ );
      mvarPOLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN ) */ );
      mvarPOLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC ) */ );
      mvarCERTIPOL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL ) */ );
      mvarCERTIANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN ) */ );
      mvarCERTISEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC ) */ );

      wvarStep = 60;
      mvarSUPLENUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUPLENUM ) */ );
      mvarFRANQCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FRANQCOD ) */ );
      mvarPQTDES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PQTDES ) */ );
      mvarPLANCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PLANCOD ) */ );
      mvarHIJOS1416 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1416 ) */ );

      wvarStep = 70;
      mvarHIJOS1729 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1729 ) */ );
      mvarHIJOS1729ND = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1729ND ) */ );
      mvarZONA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ZONA ) */ );
      mvarCODPROV = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CODPROV ) */ );
      if( !mvarTIPO_ACTU.equals( "I" ) )
      {
        mvarSUMALBA = String.valueOf( Obj.toDouble( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUMALBA ) */ ) ) / 100 );
      }
      else
      {
        mvarSUMALBA = String.valueOf( Obj.toDouble( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUMALBA ) */ ) ) / 100 );
      }
      mvarCLIENIVA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENIVA ) */ );

      wvarStep = 80;
      if( !mvarTIPO_ACTU.equals( "I" ) )
      {
        mvarSUMASEG = Strings.replace( String.valueOf( Obj.toDouble( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUMASEG ) */ ) ) / 100 ), ",", "." );
      }
      else
      {
        mvarSUMASEG = Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUMASEG ) */ ), ",", "." );
      }
      mvarCLUB_LBA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLUB_LBA ) */ );
      mvarCPAANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CPAANO ) */ );
      mvarCTAKMS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CTAKMS ) */ );
      mvarESCERO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ESCERO ) */ );

      wvarStep = 90;
      mvarTIENEPLAN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIENEPLAN ) */ );
      mvarCOND_ADIC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COND_ADIC ) */ );
      mvarASEG_ADIC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ASEG_ADIC ) */ );
      mvarFH_NAC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FH_NAC ) */ );
      mvarSEXO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SEXO ) */ );

      wvarStep = 100;
      mvarESTCIV = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ESTCIV ) */ );
      mvarSIFMVEHI_DES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SIFMVEHI_DES ) */ );
      mvarPROFECOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROFECOD ) */ );
      mvarSIACCESORIOS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SIACCESORIOS ) */ );
      mvarREFERIDO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_REFERIDO ) */ );

      wvarStep = 110;
      mvarMOTORNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MOTORNUM ) */ );
      mvarCHASINUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CHASINUM ) */ );
      mvarPATENNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PATENNUM ) */ );
      mvarAUMARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUMARCOD ) */ );
      mvarAUMODCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUMODCOD ) */ );

      wvarStep = 120;
      mvarAUSUBCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUSUBCOD ) */ );
      mvarAUADICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUADICOD ) */ );
      mvarAUMODORI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUMODORI ) */ );
      mvarAUUSOCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUUSOCOD ) */ );
      mvarAUVTVCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUVTVCOD ) */ );

      wvarStep = 130;
      mvarAUVTVDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUVTVDIA ) */ );
      mvarAUVTVMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUVTVMES ) */ );
      mvarAUVTVANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUVTVANN ) */ );
      mvarVEHCLRCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VEHCLRCOD ) */ );
      mvarAUKLMNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUKLMNUM ) */ );

      wvarStep = 140;
      mvarFABRICAN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FABRICAN ) */ );
      mvarFABRICMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FABRICMES ) */ );
      mvarGUGARAGE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GUGARAGE ) */ );
      mvarGUDOMICI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GUDOMICI ) */ );
      mvarAUCATCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUCATCOD ) */ );

      wvarStep = 150;
      mvarAUTIPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUTIPCOD ) */ );
      mvarAUCIASAN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUCIASAN ) */ );
      mvarAUANTANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUANTANN ) */ );
      mvarAUNUMSIN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUNUMSIN ) */ );
      mvarAUNUMKMT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUNUMKMT ) */ );

      wvarStep = 160;
      mvarAUUSOGNC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AUUSOGNC ) */ );
      mvarUSUARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD ) */ );
      mvarSITUCPOL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SITUCPOL ) */ );
      mvarCLIENSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC ) */ );
      mvarNUMEDOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU ) */ );

      wvarStep = 170;
      mvarTIPODOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU ) */ );
      mvarDOMICSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICSEC ) */ );
      mvarCUENTSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CUENTSEC ) */ );
      mvarCOBROFOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COBROFOR ) */ );
      mvarEDADACTU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EDADACTU ) */ );

      wvarStep = 180;
      mvarCAMP_CODIGO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CAMP_CODIGO ) */ );
      mvarCAMP_DESC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CAMP_DESC ) */ );
      mvarLEGAJO_GTE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LEGAJO_GTE ) */ );
      mvarNRO_PROD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NRO_PROD ) */ );
      mvarSUCURSAL_CODIGO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUCURSAL_CODIGO ) */ );
      mvarLEGAJO_VEND = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LEGAJO_VEND ) */ );

      wvarStep = 190;
      mvarSITUCEST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SITUCEST ) */ );
      mvarINSPECOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPECOD ) */ );
      mvarINSPECTOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPECTOR ) */ );
      mvarINSPEADOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEADOM ) */ );

      wvarStep = 200;
      mvarOBSERTXT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_OBSERTXT ) */ );
      mvarINSPENUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPENUM ) */ );
      mvarCENTRCOD_INS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CENTRCOD_INS ) */ );
      mvarCENTRDES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CENTRDES ) */ );

      wvarStep = 205;
      mvarINSPECC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPECC ) */ );
      mvarRASTREO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RASTREO ) */ );
      mvarALARMCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ALARMCOD ) */ );
      mvarNUMEDOCU_ACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU_ACRE ) */ );

      wvarStep = 210;
      mvarTIPODOCU_ACRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU_ACRE ) */ );
      mvarAPELLIDO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_APELLIDO ) */ );
      mvarNOMBRES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NOMBRES ) */ );
      mvarDOMICDOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOM ) */ );
      mvarDOMICDNU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDNU ) */ );
      mvarDOMICESC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICESC ) */ );

      wvarStep = 220;
      mvarDOMICPIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPIS ) */ );
      mvarDOMICPTA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPTA ) */ );
      mvarDOMICPOB = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOB ) */ );
      mvarDOMICCPO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICCPO ) */ );
      mvarPROVICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROVICOD ) */ );
      mvarPAISSCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAISSCOD ) */ );

      wvarStep = 230;
      mvarBANCOCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_BANCOCOD ) */ );
      mvarCOBROCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COBROCOD ) */ );
      mvarEFECTANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTANN ) */ );
      mvarEFECTMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTMES ) */ );
      mvarEFECTDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTDIA ) */ );
      mvarINSPEANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEANN ) */ );
      mvarINSPEMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEMES ) */ );
      mvarINSPEDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INSPEDIA ) */ );
      mvarENTDOMSC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ENTDOMSC ) */ );
      mvarANOTACIO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ANOTACIO ) */ );
      mvarPRECIO_MENSUAL = Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PRECIO_MENSUAL ) */ ), ",", "." );
      mvarTIPO_ACTU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPO_ACTU ) */ );

      wvarStep = 235;
      mvarCUITNUME = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_CUITNUME ) */.getLength() > 0 )
      {
        mvarCUITNUME = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CUITNUME ) */ );
      }
      mvarRAZONSOC = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_RAZONSOC ) */.getLength() > 0 )
      {
        mvarRAZONSOC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAZONSOC ) */ );
      }
      mvarCLIEIBTP = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_CLIEIBTP ) */.getLength() > 0 )
      {
        mvarCLIEIBTP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIEIBTP ) */ );
      }
      mvarNROIIBB = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_NROIIBB ) */.getLength() > 0 )
      {
        mvarNROIIBB = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NROIIBB ) */ );
      }
      mvarLUNETA = "";
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_LUNETA ) */.getLength() > 0 )
      {
        mvarLUNETA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUNETA ) */ );
      }

      //
      wvarStep = 240;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 250;
      wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      wobjDBCmd = new Command();

      wvarStep = 260;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      // Obligatorios
      wvarStep = 270;
      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( "0" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      // Parametros
      wvarStep = 280;
      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarRAMOPCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 290;
      wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPOLIZANN ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 300;
      wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPOLIZSEC ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 310;
      wobjDBParm = new Parameter( "@CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCERTIPOL ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 320;
      wobjDBParm = new Parameter( "@CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCERTIANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 325;
      wobjDBParm = new Parameter( "@CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCERTISEC ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 330;
      wobjDBParm = new Parameter( "@SUPLENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarSUPLENUM ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 340;
      wobjDBParm = new Parameter( "@FRANQCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarFRANQCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 350;
      wobjDBParm = new Parameter( "@PQTDES", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant( mvarPQTDES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 360;
      wobjDBParm = new Parameter( "@PLANCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPLANCOD ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 370;
      wobjDBParm = new Parameter( "@HIJOS1416", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarHIJOS1416 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 380;
      wobjDBParm = new Parameter( "@HIJOS1729", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarHIJOS1729 ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 390;
      wobjDBParm = new Parameter( "@ZONA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarZONA ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 400;
      wobjDBParm = new Parameter( "@CODPROV", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCODPROV ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 410;
      wobjDBParm = new Parameter( "@SUMALBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarSUMALBA ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 420;
      wobjDBParm = new Parameter( "@CLIENIVA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLIENIVA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 430;
      wobjDBParm = new Parameter( "@SUMAASEG", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarSUMASEG ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 440;
      wobjDBParm = new Parameter( "@CLUB_LBA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLUB_LBA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 450;
      wobjDBParm = new Parameter( "@CPAANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCPAANO ) );
      wobjDBParm.setPrecision( (byte)( 8 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 460;
      wobjDBParm = new Parameter( "@CTAKMS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCTAKMS ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 470;
      wobjDBParm = new Parameter( "@ESCERO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarESCERO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 480;
      wobjDBParm = new Parameter( "@TIENEPLAN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarTIENEPLAN ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 490;
      wobjDBParm = new Parameter( "@COND_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCOND_ADIC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 500;
      wobjDBParm = new Parameter( "@ASEG_ADIC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarASEG_ADIC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 510;
      wobjDBParm = new Parameter( "@FH_NAC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarFH_NAC ) );
      wobjDBParm.setPrecision( (byte)( 8 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 520;
      wobjDBParm = new Parameter( "@SEXO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarSEXO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 530;
      wobjDBParm = new Parameter( "@ESTCIV", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarESTCIV ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 540;
      wobjDBParm = new Parameter( "@SIFMVEHI_DES", AdoConst.adChar, AdoConst.adParamInput, 50, new Variant( mvarSIFMVEHI_DES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 550;
      wobjDBParm = new Parameter( "@PROFECOD", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( mvarPROFECOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 560;
      wobjDBParm = new Parameter( "@ACCESORIOS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarSIACCESORIOS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 570;
      wobjDBParm = new Parameter( "@REFERIDO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarREFERIDO ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 580;
      wobjDBParm = new Parameter( "@MOTORNUM", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarMOTORNUM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 590;
      wobjDBParm = new Parameter( "@CHASINUM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarCHASINUM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 600;
      wobjDBParm = new Parameter( "@PATENNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarPATENNUM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 610;
      wobjDBParm = new Parameter( "@AUMARCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUMARCOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 620;
      wobjDBParm = new Parameter( "@AUMODCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUMODCOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 630;
      wobjDBParm = new Parameter( "@AUSUBCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUSUBCOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 640;
      wobjDBParm = new Parameter( "@AUADICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUADICOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 650;
      wobjDBParm = new Parameter( "@AUMODORI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarAUMODORI ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 660;
      wobjDBParm = new Parameter( "@AUUSOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUUSOCOD ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 670;
      wobjDBParm = new Parameter( "@AUVTVCOD", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarAUVTVCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 680;
      wobjDBParm = new Parameter( "@AUVTVDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUVTVDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 690;
      wobjDBParm = new Parameter( "@AUVTVMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUVTVMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 700;
      wobjDBParm = new Parameter( "@AUVTVANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUVTVANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 710;
      wobjDBParm = new Parameter( "@VEHCLRCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarVEHCLRCOD ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 720;
      wobjDBParm = new Parameter( "@AUKLMNUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUKLMNUM ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 730;
      wobjDBParm = new Parameter( "@FABRICAN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarFABRICAN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 740;
      wobjDBParm = new Parameter( "@FABRICMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarFABRICMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 750;
      wobjDBParm = new Parameter( "@GUGARAGE", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarGUGARAGE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 760;
      wobjDBParm = new Parameter( "@GUDOMICI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarGUDOMICI ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 770;
      wobjDBParm = new Parameter( "@AUCATCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUCATCOD ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 780;
      wobjDBParm = new Parameter( "@AUTIPCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUTIPCOD ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 790;
      wobjDBParm = new Parameter( "@AUCIASAN", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarAUCIASAN ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 800;
      wobjDBParm = new Parameter( "@AUANTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUANTANN ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 810;
      wobjDBParm = new Parameter( "@AUNUMSIN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUNUMSIN ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 820;
      wobjDBParm = new Parameter( "@AUNUMKMT", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUNUMKMT ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 830;
      wobjDBParm = new Parameter( "@AUUSOGNC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarAUUSOGNC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 840;
      wobjDBParm = new Parameter( "@USUARCOD", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarUSUARCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 850;
      wobjDBParm = new Parameter( "@SITUCPOL", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarSITUCPOL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 860;
      wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCLIENSEC ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 870;
      wobjDBParm = new Parameter( "@NUMEDOCU", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( mvarNUMEDOCU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 880;
      wobjDBParm = new Parameter( "@TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarTIPODOCU ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 890;
      wobjDBParm = new Parameter( "@DOMICSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarDOMICSEC ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 900;
      wobjDBParm = new Parameter( "@CUENTSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCUENTSEC ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 910;
      wobjDBParm = new Parameter( "@COBROFOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCOBROFOR ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 920;
      wobjDBParm = new Parameter( "@EDADACTU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarEDADACTU ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 925;
      wobjDBParm = new Parameter( "@SWCLIENT", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //
      // Coberturas
      wvarStep = 930;
      if( !mvarTIPO_ACTU.equals( "I" ) )
      {
        for( wvarCounter = 1; wvarCounter <= 20; wvarCounter++ )
        {
          mvarCOBERCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_COBERCOD + wvarCounter ) */ );
          mvarCOBERORD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_COBERORD + wvarCounter ) */ );
          mvarCAPITASG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_CAPITASG + wvarCounter ) */ );
          mvarCAPITIMP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_CAPITIMP + wvarCounter ) */ );

          wvarStep = 940;
          wobjDBParm = new Parameter( "@COBERCOD" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCOBERCOD ) );
          wobjDBParm.setPrecision( (byte)( 3 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 950;
          wobjDBParm = new Parameter( "@COBERORD" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCOBERORD ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 960;
          wobjDBParm = new Parameter( "@CAPITASG" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCAPITASG ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 970;
          wobjDBParm = new Parameter( "@CAPITIMP" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCAPITIMP ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

        }
      }
      else
      {
        for( wvarCounter = 1; wvarCounter <= 20; wvarCounter++ )
        {

          wvarStep = 972;
          wobjDBParm = new Parameter( "@COBERCOD" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setPrecision( (byte)( 3 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 974;
          wobjDBParm = new Parameter( "@COBERORD" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setPrecision( (byte)( 2 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 976;
          wobjDBParm = new Parameter( "@CAPITASG" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

          wvarStep = 978;
          wobjDBParm = new Parameter( "@CAPITIMP" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
          wobjDBParm.setPrecision( (byte)( 15 ) );
          wobjDBParm.setScale( (byte)( 0 ) );
          wobjDBCmd.getParameters().append( wobjDBParm );
          wobjDBParm = (Parameter) null;

        }

      }
      // Fin Coberturas
      // Asegurados
      wvarStep = 980;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_ASEGURADOS ) */;

      wvarStep = 990;
      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        mvarDOCUMDAT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_DOCUMDAT ) */ );
        mvarNOMBREAS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_NOMBREAS ) */ );

        wvarStep = 1000;
        wobjDBParm = new Parameter( "@DOCUMDAT" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarDOCUMDAT ) );
        wobjDBParm.setPrecision( (byte)( 9 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1010;
        wobjDBParm = new Parameter( "@NOMBREAS" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 49, new Variant( mvarNOMBREAS ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wvarStep = 1020;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
      {

        wvarStep = 1030;
        wobjDBParm = new Parameter( "@DOCUMDAT" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 9 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1040;
        wobjDBParm = new Parameter( "@NOMBREAS" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 49, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wobjXMLList = (org.w3c.dom.NodeList) null;
      // Fin Asegurados
      wvarStep = 1050;
      wobjDBParm = new Parameter( "@CAMP_CODIGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCAMP_CODIGO ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1055;
      wobjDBParm = new Parameter( "@CAMP_DESC", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( Strings.left( Strings.trim( mvarCAMP_DESC ), 30 ) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1060;
      wobjDBParm = new Parameter( "@LEGAJO_GTE", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarLEGAJO_GTE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1070;
      wobjDBParm = new Parameter( "@NRO_PROD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarNRO_PROD ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1080;
      wobjDBParm = new Parameter( "@SUCURSAL_CODIGO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarSUCURSAL_CODIGO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1090;
      wobjDBParm = new Parameter( "@LEGAJO_VEND", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarLEGAJO_VEND ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      // Conductores
      wvarStep = 1100;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_CONDUCTORES ) */;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        mvarCONDUAPE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUAPE ) */ );
        mvarCONDUNOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUNOM ) */ );

        wvarStep = 1110;
        mvarCONDUFEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUFEC ) */ );
        mvarCONDUSEX = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUSEX ) */ );
        mvarCONDUEST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUEST ) */ );
        mvarCONDUEXC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CONDUEXC ) */ );

        wvarStep = 1120;
        wobjDBParm = new Parameter( "@CONDUAPE" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( mvarCONDUAPE ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1130;
        wobjDBParm = new Parameter( "@CONDUNOM" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarCONDUNOM ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1140;
        wobjDBParm = new Parameter( "@CONDUFEC" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCONDUFEC ) );
        wobjDBParm.setPrecision( (byte)( 8 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1150;
        wobjDBParm = new Parameter( "@CONDUSEX" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCONDUSEX ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1160;
        wobjDBParm = new Parameter( "@CONDUEST" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCONDUEST ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1170;
        wobjDBParm = new Parameter( "@CONDUEXC" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCONDUEXC ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wvarStep = 1180;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
      {

        wvarStep = 1190;
        wobjDBParm = new Parameter( "@CONDUAPE" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1200;
        wobjDBParm = new Parameter( "@CONDUNOM" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1210;
        wobjDBParm = new Parameter( "@CONDUFEC" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 8 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1220;
        wobjDBParm = new Parameter( "@CONDUSEX" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1230;
        wobjDBParm = new Parameter( "@CONDUEST" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1240;
        wobjDBParm = new Parameter( "@CONDUEXC" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wvarStep = 1250;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      // Conductores
      // Accesorios
      wvarStep = 1260;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_ACCESORIOS ) */;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        wvarStep = 1270;
        mvarAUACCCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_AUACCCOD ) */ );
        mvarAUVEASUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_AUVEASUM ) */ );
        mvarAUVEADES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_AUVEADES ) */ );
        mvarAUVEADEP = "S";

        wvarStep = 1280;
        wobjDBParm = new Parameter( "@AUACCCOD" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarAUACCCOD ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1290;
        wobjDBParm = new Parameter( "@AUVEASUM" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarAUVEASUM.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarAUVEASUM)) );
        wobjDBParm.setPrecision( (byte)( 14 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1300;
        wobjDBParm = new Parameter( "@AUVEADES" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarAUVEADES ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1310;
        wobjDBParm = new Parameter( "@AUVEADEP" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarAUVEADEP ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wvarStep = 1320;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
      {

        wvarStep = 1330;
        wobjDBParm = new Parameter( "@AUACCCOD" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1340;
        wobjDBParm = new Parameter( "@AUVEASUM" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 14 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1350;
        wobjDBParm = new Parameter( "@AUVEADES" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1360;
        wobjDBParm = new Parameter( "@AUVEADEP" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wvarStep = 1370;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      // Fin Accesorios
      wvarStep = 1380;
      wobjDBParm = new Parameter( "@SITUCEST", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarSITUCEST ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1390;
      wobjDBParm = new Parameter( "@INSPECOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPECOD ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1400;
      wobjDBParm = new Parameter( "@INSPECTOR", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarINSPECTOR ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1410;
      wobjDBParm = new Parameter( "@INSPEADOM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarINSPEADOM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1420;
      wobjDBParm = new Parameter( "@OBSERTXT", AdoConst.adChar, AdoConst.adParamInput, 75, new Variant( mvarOBSERTXT ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1430;
      wobjDBParm = new Parameter( "@INSPENUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPENUM ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1440;
      wobjDBParm = new Parameter( "@CENTRCOD_INS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCENTRCOD_INS ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1450;
      wobjDBParm = new Parameter( "@CENTRDES", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarCENTRDES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1452;
      wobjDBParm = new Parameter( "@INSPECC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPECC ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1454;
      wobjDBParm = new Parameter( "@RASTREO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarRASTREO ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1456;
      wobjDBParm = new Parameter( "@ALARMCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarALARMCOD ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1460;
      wobjDBParm = new Parameter( "@NUMEDOCU_ACRE", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( mvarNUMEDOCU_ACRE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1470;
      wobjDBParm = new Parameter( "@TIPODOCU_ACRE", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarTIPODOCU_ACRE ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1480;
      wobjDBParm = new Parameter( "@APELLIDO", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( mvarAPELLIDO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1490;
      wobjDBParm = new Parameter( "@NOMBRES", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( mvarNOMBRES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1500;
      wobjDBParm = new Parameter( "@DOMICDOM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( mvarDOMICDOM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1510;
      wobjDBParm = new Parameter( "@DOMICDNU", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( mvarDOMICDNU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1520;
      wobjDBParm = new Parameter( "@DOMICESC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarDOMICESC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1530;
      wobjDBParm = new Parameter( "@DOMICPIS", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarDOMICPIS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1540;
      wobjDBParm = new Parameter( "@DOMICPTA", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarDOMICPTA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1550;
      wobjDBParm = new Parameter( "@DOMICPOB", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( mvarDOMICPOB ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1560;
      wobjDBParm = new Parameter( "@DOMICCPO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarDOMICCPO ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1570;
      wobjDBParm = new Parameter( "@PROVICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPROVICOD ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1580;
      wobjDBParm = new Parameter( "@PAISSCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarPAISSCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1590;
      wobjDBParm = new Parameter( "@BANCOCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarBANCOCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1600;
      wobjDBParm = new Parameter( "@COBROCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarCOBROCOD ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1610;
      wobjDBParm = new Parameter( "@EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarEFECTANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1620;
      wobjDBParm = new Parameter( "@EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarEFECTMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1630;
      wobjDBParm = new Parameter( "@EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarEFECTDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1640;
      wobjDBParm = new Parameter( "@INSPEANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPEANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1650;
      wobjDBParm = new Parameter( "@INSPEMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPEMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1660;
      wobjDBParm = new Parameter( "@INSPEDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarINSPEDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1670;
      wobjDBParm = new Parameter( "@ENTDOMSC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarENTDOMSC ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1680;
      wobjDBParm = new Parameter( "@ANOTACIO", AdoConst.adChar, AdoConst.adParamInput, 100, new Variant( mvarANOTACIO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1690;
      wobjDBParm = new Parameter( "@PRECIO_MENSUAL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPRECIO_MENSUAL ) );
      wobjDBParm.setPrecision( (byte)( 13 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1695;
      wobjDBParm = new Parameter( "@TIPO_ACTUA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarTIPO_ACTU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 1700;
      wobjDBParm = new Parameter( "@CUITNUME", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( mvarCUITNUME ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1710;
      wobjDBParm = new Parameter( "@RAZONSOC", AdoConst.adChar, AdoConst.adParamInput, 60, new Variant( mvarRAZONSOC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1720;
      wobjDBParm = new Parameter( "@CLIEIBTP", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarCLIEIBTP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1730;
      wobjDBParm = new Parameter( "@NROIIBB", AdoConst.adChar, AdoConst.adParamInput, 15, new Variant( mvarNROIIBB ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1740;
      wobjDBParm = new Parameter( "@LUNETA", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( mvarLUNETA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1745;
      wobjDBParm = new Parameter( "@HIJOS1729_EXC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarHIJOS1729ND ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //'''''''''''''''''''''' Ver parametros '''''''''''''''
      wvarStep = 1750;
      for( i = 1; i <= (wobjDBCmd.getParameters().getCount() - 1); i++ )
      {
        if( wobjDBCmd.getParameters().getParameter(i).getType() == AdoConst.adChar )
        {
          aa = aa + "'" + wobjDBCmd.getParameters().getParameter(i).getValue() + "',";
        }
        else
        {
          aa = aa + wobjDBCmd.getParameters().getParameter(i).getValue() + ",";
        }
      }
      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      wvarStep = 1760;
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );

      wvarStep = 1770;
      if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><CADENA>" + wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue() + "</CADENA></Response>" );
      }
      else
      {
        
        if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -1 )
        {
          wvarMensaje = "Error en la tabla SIFSPOLI";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -2 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -3 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -4 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -5 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -6 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -7 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -8 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -9 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -10 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -11 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 10";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -12 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -13 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -14 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -15 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -16 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -17 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -18 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -19 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -20 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -21 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 10";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -22 )
        {
          wvarMensaje = "Error en la tabla SIFSMOVI                ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -23 )
        {
          wvarMensaje = "Error en la tabla SIFSFIMA_TARI               ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -24 )
        {
          wvarMensaje = "Error en la tabla SIFSFIMA_VEHI               ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -25 )
        {
          wvarMensaje = "Error en la tabla SIFSPECO                ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -26 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_CAMP               ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -27 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_GTE                ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -28 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_PROD               ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -29 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_SUCU               ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -30 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_VEND               ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -31 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 1         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -32 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 2         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -33 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 3         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -34 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 4         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -35 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 5         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -36 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 6         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -37 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 7         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -38 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 8         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -39 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 9         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -40 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 10        ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -41 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 1         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -42 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 2         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -43 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 3         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -44 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 4         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -45 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 5         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -46 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 6         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -47 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 7         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -48 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 8         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -49 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 9         ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -50 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 10        ";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -51 )
        {
          wvarMensaje = "Error en la tabla SIFWINSV                ";
        }
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='" + wvarMensaje + "' /></Response>" );
      }

      wvarStep = 1780;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      wvarStep = 1790;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
