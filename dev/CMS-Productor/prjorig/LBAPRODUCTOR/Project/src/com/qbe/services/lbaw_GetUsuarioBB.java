package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetUsuarioBB implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorLBA.lbaw_GetUsuarioBB";
  static final String mcteStoreProc = "P_BB_SELECT_USUARIO";
  /**
   *  Parametros XML de Entrada
   */
  static final String mcteParam_USUARCOD = "//USUARCOD";
  static final String mcteParam_USUARIOBB = "//USUARIOBB";
  static final String mcteParam_MODO = "//MODO";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private com.qbe.services.HSBCInterfaces.IDBConnection mobjHSBC_DBCnn = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    String wvarMensaje = "";
    String wvarUSUARCOD = "";
    String wvarUSUARIOBB = "";
    String wvarMODO = "";
    Recordset wrstDBResult = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    String wvarResult = "";
    //
    //
    //DATOS GENERALES
    //
    //RESPUESTA
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      wvarUSUARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD ) */ );
      wvarUSUARIOBB = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_USUARIOBB ) */ );
      wvarMODO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MODO ) */ );
      //
      wvarStep = 30;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      wobjDBCmd = new Command();

      wvarStep = 40;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 50;
      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 60;
      wobjDBParm = new Parameter( "@USUARCOD", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( wvarUSUARCOD ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 70;
      wobjDBParm = new Parameter( "@USUARIOBB", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( wvarUSUARIOBB ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 80;
      wobjDBParm = new Parameter( "MODO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarMODO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 90;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );

      if( ! (wrstDBResult.isEOF()) )
      {
        //
        wvarStep = 100;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 110;
        /*unsup wrstDBResult.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
        //
        wvarStep = 120;
        //unsup wobjXSLResponse.async = false;
        wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
        //
        wvarStep = 130;
        wobjXMLResponse.loadXML( "<Response>" + Strings.replace( /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" ) + "</Response>" );
        //
        wvarStep = 140;
        //Desencripto la clave
        diamondedge.util.XmlDom.setText( null /*unsup wobjXMLResponse.selectSingleNode( "//PWDBB" ) */, ModEncryptDecrypt.CapicomDecrypt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//PWDBB" ) */ ) ) );
        wvarResult = Strings.replace( Strings.replace( null /*unsup wobjXMLResponse.selectSingleNode( "//Response" ) */.toString(), "<Response>", "" ), "</Response>", "" );
        //
        wvarStep = 141;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        wvarStep = 150;
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        wvarStep = 160;
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS." + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //
      wvarStep = 170;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 180;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResult.close();
        }
      }
      //
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 190;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      wvarStep = 200;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    //
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='USUARCOD'>";
    wvarStrXSL = wvarStrXSL + "     <xsl:value-of select='@USUARCOD' />";
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";

    wvarStrXSL = wvarStrXSL + "  <xsl:element name='USUARIOBB'>";
    wvarStrXSL = wvarStrXSL + "     <xsl:value-of select='@USUARIOBB' />";
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";

    wvarStrXSL = wvarStrXSL + "  <xsl:element name='PWDBB'>";
    wvarStrXSL = wvarStrXSL + "     <xsl:value-of select='@PWDBB' />";
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";

    wvarStrXSL = wvarStrXSL + "  <xsl:element name='FECHA'>";
    wvarStrXSL = wvarStrXSL + "     <xsl:value-of select='@FECHA' />";
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";

    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";

    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
