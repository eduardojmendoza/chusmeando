package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetCotizProd implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorLBA.lbaw_GetCotizProd";
  static final String mcteStoreProc = "SPSNCV_PROD_COTI_X_USUARIO";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARCOD = "//USUARCOD";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_CLIENNOM = "//CLIENNOM";
  static final String mcteParam_CLIENAP = "//CLIENAP";
  static final String mcteParam_NUMEDOCU = "//NUMEDOCU";
  static final String mcteParam_TIPODOCU = "//TIPODOCU";
  static final String mcteParam_LSTAGENTCODXML = "//LST_AGENTCOD_XML";
  /**
   *  FJO - Agregado para Filtrar por Fecha de ser necesario - 18-07-2006
   */
  static final String mcteParam_FECDES_DIA = "//FECDES_DIA";
  static final String mcteParam_FECDES_MES = "//FECDES_MES";
  static final String mcteParam_FECDES_ANN = "//FECDES_ANN";
  static final String mcteParam_FECHAS_DIA = "//FECHAS_DIA";
  static final String mcteParam_FECHAS_MES = "//FECHAS_MES";
  static final String mcteParam_FECHAS_ANN = "//FECHAS_ANN";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarUSUARCOD = "";
    String wvarRAMOPCOD = "";
    String wvarCLIENNOM = "";
    String wvarCLIENAP = "";
    String wvarNUMEDOCU = "";
    String wvarTIPODOCU = "";
    String wvarLSTAGENTCODXML = "";
    String wvarFECDES_DIA = "";
    String wvarFECDES_MES = "";
    String wvarFECDES_ANN = "";
    String wvarFECHAS_DIA = "";
    String wvarFECHAS_MES = "";
    String wvarFECHAS_ANN = "";
    //
    //
    //
    //FJO - Agregado para control por rango de fechas -- 18-07-2006
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarUSUARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD ) */ );
      wvarRAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD ) */ );
      wvarCLIENNOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENNOM ) */ );
      wvarCLIENAP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENAP ) */ );
      wvarNUMEDOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU ) */ );
      wvarTIPODOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU ) */ );
      //Ale 27/3
      wvarLSTAGENTCODXML = null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LSTAGENTCODXML ) */.getChildNodes().item( 0 ).toString();
      //FJO - Agregado para control por rango de fechas -- 18-07-2006
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECDES_DIA ) */ == (org.w3c.dom.Node) null) )
      {
        wvarFECDES_DIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECDES_DIA ) */ );
      }

      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECDES_MES ) */ == (org.w3c.dom.Node) null) )
      {
        wvarFECDES_MES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECDES_MES ) */ );
      }

      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECDES_ANN ) */ == (org.w3c.dom.Node) null) )
      {
        wvarFECDES_ANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECDES_ANN ) */ );
      }

      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECHAS_DIA ) */ == (org.w3c.dom.Node) null) )
      {
        wvarFECHAS_DIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECHAS_DIA ) */ );
      }

      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECHAS_MES ) */ == (org.w3c.dom.Node) null) )
      {
        wvarFECHAS_MES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECHAS_MES ) */ );
      }

      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECHAS_ANN ) */ == (org.w3c.dom.Node) null) )
      {
        wvarFECHAS_ANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECHAS_ANN ) */ );
      }
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 20;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //
      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      //
      wvarStep = 40;
      wobjDBCmd = new Command();
      //
      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 60;
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARCOD", adChar, adParamInput, 10, wvarUSUARCOD)
      //wobjDBCmd.Parameters.Append wobjDBParm
      //Set wobjDBParm = Nothing
      wvarStep = 70;
      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adVarChar, AdoConst.adParamInput, 4, new Variant( wvarRAMOPCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 80;
      wobjDBParm = new Parameter( "@CLIENNOM", AdoConst.adVarChar, AdoConst.adParamInput, 20, new Variant( wvarCLIENNOM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 90;
      wobjDBParm = new Parameter( "@CLIENAP", AdoConst.adVarChar, AdoConst.adParamInput, 20, new Variant( wvarCLIENAP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 100;
      wobjDBParm = new Parameter( "@NUMEDOCU", AdoConst.adVarChar, AdoConst.adParamInput, 11, new Variant( wvarNUMEDOCU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 110;
      wobjDBParm = new Parameter( "@TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 2, new Variant( wvarTIPODOCU ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 115;
      //DA: 25/09/2006: este par�metro ten�a 1000 char y pas� a soportar 8000
      wobjDBParm = new Parameter( "@LST_AGENTCOD_XML", AdoConst.adVarChar, AdoConst.adParamInput, 8000, new Variant( wvarLSTAGENTCODXML ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 116;
      if( (!wvarFECDES_ANN.equals( "" )) && (!wvarFECHAS_ANN.equals( "" )) )
      {
        wvarStep = 117;
        wobjDBParm = new Parameter( "@FECDES_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 2, new Variant( wvarFECDES_DIA ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 118;
        wobjDBParm = new Parameter( "@FECDES_MES", AdoConst.adNumeric, AdoConst.adParamInput, 2, new Variant( wvarFECDES_MES ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 119;
        wobjDBParm = new Parameter( "@FECDES_ANN", AdoConst.adNumeric, AdoConst.adParamInput, 4, new Variant( wvarFECDES_ANN ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 120;
        wobjDBParm = new Parameter( "@FECHAS_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 2, new Variant( wvarFECHAS_DIA ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 121;
        wobjDBParm = new Parameter( "@FECHAS_MES", AdoConst.adNumeric, AdoConst.adParamInput, 2, new Variant( wvarFECHAS_MES ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
        wvarStep = 122;
        wobjDBParm = new Parameter( "@FECHAS_ANN", AdoConst.adNumeric, AdoConst.adParamInput, 4, new Variant( wvarFECHAS_ANN ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
        //
      }
      //
      wvarStep = 125;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );
      //
      wvarStep = 130;
      if( ! (wrstDBResult.isEOF()) )
      {
        //
        wvarStep = 140;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 150;
        /*unsup wrstDBResult.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
        //
        wvarStep = 160;
        //unsup wobjXSLResponse.async = false;
        wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
        //
        wvarStep = 170;
        wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
        //
        wvarStep = 180;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        wvarStep = 190;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        wvarStep = 200;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS." + String.valueOf( (char)(34) ) + " /></Response>" );
      }

      wvarStep = 210;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 220;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 230;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 240;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 250;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 260;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 270;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        if( ! (wrstDBResult == (Recordset) null) )
        {
          if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='ROW'>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='RAMOPCOD'><xsl:value-of select='@RAMOPCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='POLIZANN'><xsl:value-of select='@POLIZANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='POLIZSEC'><xsl:value-of select='@POLIZSEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CERTIPOL'><xsl:value-of select='@CERTIPOL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CERTIANN'><xsl:value-of select='@CERTIANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CERTISEC'><xsl:value-of select='@CERTISEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SUPLENUM'><xsl:value-of select='@SUPLENUM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='RAMOPDES'><xsl:value-of select='@RAMOPDES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENNOM'><xsl:value-of select='@CLIENNOM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENAP1'><xsl:value-of select='@CLIENAP1' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENAP2'><xsl:value-of select='@CLIENAP2' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TIPODOCU'><xsl:value-of select='@TIPODOCU' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOCUMDAB'><xsl:value-of select='@DOCUMDAB' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NUMEDOCU'><xsl:value-of select='@NUMEDOCU' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EMISIDIA'><xsl:value-of select='@EMISIDIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EMISIMES'><xsl:value-of select='@EMISIMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EMISIANN'><xsl:value-of select='@EMISIANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENSEC'><xsl:value-of select='@CLIENSEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SITUCPOL'><xsl:value-of select='@SITUCPOL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CODIGDES'><xsl:value-of select='@CODIGDES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AGENTCLA'><xsl:value-of select='@AGENTCLA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AGENTCOD'><xsl:value-of select='@AGENTCOD' /></xsl:element>";

    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='RAMOPCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='RAMOPDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='USUARCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENNOM'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENAP1'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENAP2'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='DOCUMDAB'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='NUMEDOCU'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='SITUCPOL'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CODIGDES'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
