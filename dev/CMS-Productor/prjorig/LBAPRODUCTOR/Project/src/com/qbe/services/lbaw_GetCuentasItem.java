package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetCuentasItem implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorLBA.lbaw_GetCuentasItem";
  static final String mcteStoreProcCta = "SPSNCV_PROD_SIFMCUEN_SELECT";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Recordset wrstDBResultDom = null;
    Recordset wrstDBResultCta = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCLIENSEC = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarCLIENSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC ) */ );
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 20;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //
      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      //
      wvarStep = 40;
      wobjDBCmd = new Command();
      //
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProcCta );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wobjDBParm = new Parameter( "@CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCLIENSEC ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 50;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );
      //
      wvarStep = 60;
      if( ! (wrstDBResult.isEOF()) )
      {
        //
        wvarStep = 70;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 80;
        /*unsup wrstDBResult.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
        //
        wvarStep = 90;
        //unsup wobjXSLResponse.async = false;
        wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
        //
        wvarStep = 100;
        wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
        //
        wvarStep = 120;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;

        wvarStep = 330;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><CUENTAS>" + wvarResult + "</CUENTAS></Response>" );
      }
      else
      {
        wvarStep = 340;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS." + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //
      wvarStep = 350;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 360;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 370;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 380;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 390;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 400;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 410;
      if( ! (wrstDBResultDom == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResultDom.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResultDom.close();
        }
      }
      //
      wvarStep = 420;
      wrstDBResultDom = (Recordset) null;
      //
      wvarStep = 430;
      if( ! (wrstDBResultCta == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResultCta.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResultCta.close();
        }
      }
      //
      wvarStep = 420;
      wrstDBResultCta = (Recordset) null;
      //
      wvarStep = 430;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        if( ! (wrstDBResult == (Recordset) null) )
        {
          if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='CUENTA'>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CUENTSEC'><xsl:value-of select='@CUENTSEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TIPOCUEN'><xsl:value-of select='@TIPOCUEN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='COBROTIP'><xsl:value-of select='@COBROTIP' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='BANCOCOD'><xsl:value-of select='@BANCOCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='SUCURCOD'><xsl:value-of select='@SUCURCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CUENTDC'><xsl:value-of select='@CUENTDC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CUENNUME'><xsl:value-of select='@CUENNUME' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TARJECOD'><xsl:value-of select='@TARJECOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='VENCIANN'><xsl:value-of select='@VENCIANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='VENCIMES'><xsl:value-of select='@VENCIMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='VENCIDIA'><xsl:value-of select='@VENCIDIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='COBRODES'><xsl:value-of select='@COBRODES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='USUARCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='TIPOCUEN'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='COBROTIP'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CUENTDC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CUENNUME'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='COBRODES'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
