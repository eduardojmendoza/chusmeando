package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetPersonas implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorLBA.lbaw_GetPersonas";
  static final String mcteStoreProc = "SPSNCV_PROD_SIFMPERS_SELECT";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARCOD = "//USUARCOD";
  static final String mcteParam_CLIENNOM = "//CLIENNOM";
  static final String mcteParam_CLIENAP = "//CLIENAP";
  static final String mcteParam_NUMEDOCU = "//NUMEDOCU";
  static final String mcteParam_TIPODOCU = "//TIPODOCU";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarUSUARCOD = "";
    String wvarCLIENNOM = "";
    String wvarCLIENAP = "";
    String wvarNUMEDOCU = "";
    String wvarTIPODOCU = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarUSUARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD ) */ );
      wvarCLIENNOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENNOM ) */ );
      wvarCLIENAP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENAP ) */ );
      wvarNUMEDOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU ) */ );
      wvarTIPODOCU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU ) */ );
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 20;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //
      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      //
      wvarStep = 40;
      wobjDBCmd = new Command();
      //
      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 60;
      wobjDBParm = new Parameter( "@USUARCOD", AdoConst.adVarChar, AdoConst.adParamInput, 10, new Variant( wvarUSUARCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 70;
      wobjDBParm = new Parameter( "@CLIENNOM", AdoConst.adVarChar, AdoConst.adParamInput, 20, new Variant( wvarCLIENNOM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 80;
      wobjDBParm = new Parameter( "@CLIENAP", AdoConst.adVarChar, AdoConst.adParamInput, 20, new Variant( wvarCLIENAP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 90;
      wobjDBParm = new Parameter( "@NUMEDOCU", AdoConst.adVarChar, AdoConst.adParamInput, 11, new Variant( wvarNUMEDOCU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 100;
      wobjDBParm = new Parameter( "@TIPODOCU", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarTIPODOCU ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 110;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );
      //
      wvarStep = 160;
      if( ! (wrstDBResult.isEOF()) )
      {
        //
        wvarStep = 170;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 180;
        /*unsup wrstDBResult.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
        //
        wvarStep = 190;
        //unsup wobjXSLResponse.async = false;
        wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
        //
        wvarStep = 200;
        wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
        //
        wvarStep = 220;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        wvarStep = 230;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        wvarStep = 240;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS." + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //
      wvarStep = 250;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 260;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 270;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 280;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 290;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 300;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 310;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        if( ! (wrstDBResult == (Recordset) null) )
        {
          if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='ROW'>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENSEC'><xsl:value-of select='@CLIENSEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NUMEDOCU'><xsl:value-of select='@NUMEDOCU' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TIPODOCU'><xsl:value-of select='@TIPODOCU' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENAP1'><xsl:value-of select='@CLIENAP1' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENAP2'><xsl:value-of select='@CLIENAP2' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENNOM'><xsl:value-of select='@CLIENNOM' /></xsl:element>";
    // Agregados para Hogar
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NACIMANN'><xsl:value-of select='@NACIMANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NACIMMES'><xsl:value-of select='@NACIMMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NACIMDIA'><xsl:value-of select='@NACIMDIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENSEX'><xsl:value-of select='@CLIENSEX' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENEST'><xsl:value-of select='@CLIENEST' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PAISSCOD'><xsl:value-of select='@PAISSCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PERSOTIP'><xsl:value-of select='@PERSOTIP' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='DOMICSEC'><xsl:value-of select='@DOMICSEC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EMAIL'><xsl:value-of select='@EMAIL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENAP1'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENAP2'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENNOM'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENSEX'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENEST'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='PERSOTIP'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='EMAIL'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
