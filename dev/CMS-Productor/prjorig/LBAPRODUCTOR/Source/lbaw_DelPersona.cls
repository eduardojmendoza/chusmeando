VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_DelPersona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog
Private mobjHSBC_DBCnn          As HSBCInterfaces.IDBConnection

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_ProductorLBA.lbaw_DelPersona"
Const mcteStoreProc             As String = "SPSNCV_PROD_SIFMPERS_DELETE"

' Parametros XML de Entrada
' DATOS GENERALES
Const mcteParam_CLIENSEC    As String = "//CLIENSEC"


Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    '
    ' DATOS GENERALES
    Dim wvarCLIENSEC    As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
        '
        wvarStep = 20
        ' DATOS GENERALES
        wvarCLIENSEC = .selectSingleNode(mcteParam_CLIENSEC).Text
    End With
    
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 60
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc

    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
        
    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEC", adNumeric, adParamInput, , wvarCLIENSEC)
    wobjDBParm.Precision = 9
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    
    wvarStep = 400
    wobjDBCmd.Execute adExecuteNoRecords

    wvarStep = 410
    
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value = 0 Then
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><PERSONAID>" & wobjDBCmd.Parameters("@RETURN_VALUE").Value & "</PERSONAID></Response>"
    Else   ' Mal el update de cotizaciones
        Select Case wobjDBCmd.Parameters("@RETURN_VALUE").Value
            Case -1
                wvarMensaje = "Error actualizacion en SIFSPOLI"
            Case -2
                wvarMensaje = "El cliente seleccionado fue borrado con anterioridad"
            Case -3
                wvarMensaje = "El cliente seleccionado tiene cotizaciones realizadas"
            Case -4
                wvarMensaje = "El cliente seleccionado tiene solicitudes"
        End Select
        
        wvarStep = 1390
        pvarResponse = "<Response><Estado resultado=" & Chr(39) & "false" & Chr(39) & " mensaje=" & Chr(39) & wvarMensaje & Chr(39) & " /></Response>"
    End If
            
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 420
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub











