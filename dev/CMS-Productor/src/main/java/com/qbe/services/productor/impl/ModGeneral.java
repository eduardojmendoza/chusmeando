package com.qbe.services.productor.impl;

public class ModGeneral
{
  public static final String gcteDB = "lbawA_OfVirtualLBA.udl";
  /**
   *  Parametros XML de la Instalacion
   */
  public static final String gcteParamFileName = "Productor.xml";
  public static final String gcteNodosAutoScoring = "//AUTOSCORING";
  public static final String gcteRAMOPCOD = "/RAMOPCOD";
  public static final String gcteINSTACOD = "/INSTACOD";
}
