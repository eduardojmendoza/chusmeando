package com.qbe.services.productor.impl;
import java.sql.CallableStatement;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_PutSolicProd extends BaseOSBClient implements VBObjectClass
{
	private static Logger logger = Logger.getLogger(lbaw_PutSolicProd.class.getName());

	/**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorLBA.lbaw_PutSolicProd";
  static final String mcteStoreProc = "SPSNCV_PROD_SOLI_INSERT";
  /**
   *  Parametros XML de Entrada
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_SUPLENUM = "//SUPLENUM";
  static final String mcteParam_FRANQCOD = "//FRANQCOD";
  static final String mcteParam_PQTDES = "//PQTDES";
  static final String mcteParam_PLANCOD = "//PLANCOD";
  static final String mcteParam_HIJOS1416 = "//HIJOS1416";
  static final String mcteParam_HIJOS1729 = "//HIJOS1729";
  static final String mcteParam_HIJOS1729ND = "//HIJOS1729ND";
  static final String mcteParam_ZONA = "//ZONA";
  static final String mcteParam_CODPROV = "//CODPROV";
  static final String mcteParam_SUMALBA = "//SUMALBA";
  static final String mcteParam_CLIENIVA = "//CLIENIVA";
  static final String mcteParam_SUMASEG = "//SUMASEG";
  static final String mcteParam_CLUB_LBA = "//CLUB_LBA";
  static final String mcteParam_CPAANO = "//CPAANO";
  static final String mcteParam_CTAKMS = "//CTAKMS";
  static final String mcteParam_ESCERO = "//ESCERO";
  static final String mcteParam_TIENEPLAN = "//TIENEPLAN";
  static final String mcteParam_COND_ADIC = "//COND_ADIC";
  static final String mcteParam_ASEG_ADIC = "//ASEG_ADIC";
  static final String mcteParam_FH_NAC = "//FH_NAC";
  static final String mcteParam_SEXO = "//SEXO";
  static final String mcteParam_ESTCIV = "//ESTCIV";
  static final String mcteParam_SIFMVEHI_DES = "//SIFMVEHI_DES";
  static final String mcteParam_PROFECOD = "//PROFECOD";
  static final String mcteParam_SIACCESORIOS = "//SIACCESORIOS";
  static final String mcteParam_REFERIDO = "//REFERIDO";
  static final String mcteParam_MOTORNUM = "//MOTORNUM";
  static final String mcteParam_CHASINUM = "//CHASINUM";
  static final String mcteParam_PATENNUM = "//PATENNUM";
  static final String mcteParam_AUMARCOD = "//AUMARCOD";
  static final String mcteParam_AUMODCOD = "//AUMODCOD";
  static final String mcteParam_AUSUBCOD = "//AUSUBCOD";
  static final String mcteParam_AUADICOD = "//AUADICOD";
  static final String mcteParam_AUMODORI = "//AUMODORI";
  static final String mcteParam_AUUSOCOD = "//AUUSOCOD";
  static final String mcteParam_AUVTVCOD = "//AUVTVCOD";
  static final String mcteParam_AUVTVDIA = "//AUVTVDIA";
  static final String mcteParam_AUVTVMES = "//AUVTVMES";
  static final String mcteParam_AUVTVANN = "//AUVTVANN";
  static final String mcteParam_VEHCLRCOD = "//VEHCLRCOD";
  static final String mcteParam_AUKLMNUM = "//AUKLMNUM";
  static final String mcteParam_FABRICAN = "//FABRICAN";
  static final String mcteParam_FABRICMES = "//FABRICMES";
  static final String mcteParam_GUGARAGE = "//GUGARAGE";
  static final String mcteParam_GUDOMICI = "//GUDOMICI";
  static final String mcteParam_AUCATCOD = "//AUCATCOD";
  static final String mcteParam_AUTIPCOD = "//AUTIPCOD";
  static final String mcteParam_AUCIASAN = "//AUCIASAN";
  static final String mcteParam_AUANTANN = "//AUANTANN";
  static final String mcteParam_AUNUMSIN = "//AUNUMSIN";
  static final String mcteParam_AUNUMKMT = "//AUNUMKMT";
  static final String mcteParam_AUUSOGNC = "//AUUSOGNC";
  static final String mcteParam_USUARCOD = "//USUARCOD";
  static final String mcteParam_SITUCPOL = "//SITUCPOL";
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_NUMEDOCU = "//NUMEDOCU";
  static final String mcteParam_TIPODOCU = "//TIPODOCU";
  static final String mcteParam_DOMICSEC = "//DOMICSEC";
  static final String mcteParam_CUENTSEC = "//CUENTSEC";
  static final String mcteParam_COBROFOR = "//COBROFOR";
  static final String mcteParam_EDADACTU = "//EDADACTU";
  /**
   *  Coberturas
   */
  static final String mcteParam_COT_NRO = "//COT_NRO";
  static final String mcteParam_COBERCOD = "//COBERCOD";
  static final String mcteParam_COBERORD = "//COBERORD";
  static final String mcteParam_CAPITASG = "//CAPITASG";
  static final String mcteParam_CAPITIMP = "//CAPITIMP";
  /**
   *  Coberturas
   */
  static final String mcteParam_ASEGURADOS = "//ASEGURADOS/ASEGURADO";
  static final String mcteParam_DOCUMDAT = "DOCUMDAT";
  static final String mcteParam_NOMBREAS = "NOMBREAS";
  static final String mcteParam_CAMP_CODIGO = "//CAMP_CODIGO";
  static final String mcteParam_CAMP_DESC = "//CAMP_DESC";
  static final String mcteParam_LEGAJO_GTE = "//LEGAJO_GTE";
  static final String mcteParam_NRO_PROD = "//NRO_PROD";
  static final String mcteParam_SUCURSAL_CODIGO = "//SUCURSAL_CODIGO";
  static final String mcteParam_LEGAJO_VEND = "//LEGAJO_VEND";
  /**
   *  Coberturas
   */
  static final String mcteParam_CONDUCTORES = "//HIJOS/HIJO";
  static final String mcteParam_CONDUAPE = "APELLIDOHIJO";
  static final String mcteParam_CONDUNOM = "NOMBREHIJO";
  static final String mcteParam_CONDUFEC = "NACIMHIJO";
  static final String mcteParam_CONDUSEX = "SEXOHIJO";
  static final String mcteParam_CONDUEST = "ESTADOHIJO";
  static final String mcteParam_CONDUEXC = "INCLUIDO";
  /**
   *  Accesorios
   */
  static final String mcteParam_ACCESORIOS = "//ACCESORIOS/ACCESORIO";
  static final String mcteParam_AUACCCOD = "CODIGOACC";
  static final String mcteParam_AUVEASUM = "PRECIOACC";
  static final String mcteParam_AUVEADES = "DESCRIPCIONACC";
  /**
   *  Inspecciones y acreedor Prendario
   */
  static final String mcteParam_SITUCEST = "//SITUCEST";
  static final String mcteParam_INSPECOD = "//INSPECOD";
  static final String mcteParam_INSPECTOR = "//INSPECTOR";
  static final String mcteParam_INSPEADOM = "//INSPEADOM";
  static final String mcteParam_OBSERTXT = "//OBSERTXT";
  static final String mcteParam_INSPENUM = "//INSPENUM";
  static final String mcteParam_CENTRCOD_INS = "//CENTRCOD_INS";
  static final String mcteParam_CENTRDES = "//CENTRDES";
  static final String mcteParam_INSPECC = "//INSPECC";
  static final String mcteParam_RASTREO = "//RASTREO";
  static final String mcteParam_ALARMCOD = "//ALARMCOD";
  static final String mcteParam_NUMEDOCU_ACRE = "//NUMEDOCU_ACRE";
  static final String mcteParam_TIPODOCU_ACRE = "//TIPODOCU_ACRE";
  static final String mcteParam_APELLIDO = "//APELLIDO";
  static final String mcteParam_NOMBRES = "//NOMBRES";
  static final String mcteParam_DOMICDOM = "//DOMICDOM";
  static final String mcteParam_DOMICDNU = "//DOMICDNU";
  static final String mcteParam_DOMICESC = "//DOMICESC";
  static final String mcteParam_DOMICPIS = "//DOMICPIS";
  static final String mcteParam_DOMICPTA = "//DOMICPTA";
  static final String mcteParam_DOMICPOB = "//DOMICPOB";
  static final String mcteParam_DOMICCPO = "//DOMICCPO";
  static final String mcteParam_PROVICOD = "//PROVICOD";
  static final String mcteParam_PAISSCOD = "//PAISSCOD";
  static final String mcteParam_BANCOCOD = "//BANCOCOD";
  static final String mcteParam_COBROCOD = "//COBROCOD";
  static final String mcteParam_EFECTANN = "//EFECTANN2";
  static final String mcteParam_EFECTMES = "//EFECTMES";
  static final String mcteParam_EFECTDIA = "//EFECTDIA";
  static final String mcteParam_INSPEANN = "//INSPEANN";
  static final String mcteParam_INSPEMES = "//INSPEMES";
  static final String mcteParam_INSPEDIA = "//INSPEDIA";
  static final String mcteParam_ENTDOMSC = "//ENTDOMSC";
  static final String mcteParam_ANOTACIO = "//ANOTACIO";
  static final String mcteParam_PRECIO_MENSUAL = "//PRECIO_MENSUAL";
  static final String mcteParam_CUITNUME = "//CUITNUME";
  static final String mcteParam_RAZONSOC = "//RAZONSOC";
  static final String mcteParam_CLIEIBTP = "//CLIEIBTP";
  static final String mcteParam_NROIIBB = "//NROIIBB";
  static final String mcteParam_LUNETA = "//LUNETA";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLCoberturas = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    Object wobjClass = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String wvarRequest = "";
    String wvarResponse = "";
    String mvarCERTIPOL = "";
    String mvarCERTIANN = "";
    String mvarSUPLENUM = "";
    String mvarFRANQCOD = "";
    String mvarPQTDES = "";
    String mvarPLANCOD = "";
    String mvarHIJOS1416 = "";
    String mvarHIJOS1729 = "";
    String mvarHIJOS1729ND = "";
    String mvarZONA = "";
    String mvarCODPROV = "";
    String mvarSUMALBA = "";
    String mvarCLIENIVA = "";
    String mvarSUMASEG = "";
    String mvarCLUB_LBA = "";
    String mvarCPAANO = "";
    String mvarCTAKMS = "";
    String mvarESCERO = "";
    String mvarTIENEPLAN = "";
    String mvarCOND_ADIC = "";
    String mvarASEG_ADIC = "";
    String mvarFH_NAC = "";
    String mvarSEXO = "";
    String mvarESTCIV = "";
    String mvarSIFMVEHI_DES = "";
    String mvarPROFECOD = "";
    String mvarSIACCESORIOS = "";
    String mvarREFERIDO = "";
    String mvarMOTORNUM = "";
    String mvarCHASINUM = "";
    String mvarPATENNUM = "";
    String mvarAUMARCOD = "";
    String mvarAUMODCOD = "";
    String mvarAUSUBCOD = "";
    String mvarAUADICOD = "";
    String mvarAUMODORI = "";
    String mvarAUUSOCOD = "";
    String mvarAUVTVCOD = "";
    String mvarAUVTVDIA = "";
    String mvarAUVTVMES = "";
    String mvarAUVTVANN = "";
    String mvarVEHCLRCOD = "";
    String mvarAUKLMNUM = "";
    String mvarFABRICAN = "";
    String mvarFABRICMES = "";
    String mvarGUGARAGE = "";
    String mvarGUDOMICI = "";
    String mvarAUCATCOD = "";
    String mvarAUTIPCOD = "";
    String mvarAUCIASAN = "";
    String mvarAUANTANN = "";
    String mvarAUNUMSIN = "";
    String mvarAUNUMKMT = "";
    String mvarAUUSOGNC = "";
    String mvarUSUARCOD = "";
    String mvarSITUCPOL = "";
    String mvarCLIENSEC = "";
    String mvarNUMEDOCU = "";
    String mvarTIPODOCU = "";
    String mvarDOMICSEC = "";
    String mvarCUENTSEC = "";
    String mvarCOBROFOR = "";
    String mvarEDADACTU = "";
    String mvarCODPOST = "";
    String wvarCot_Nro = "";
    String mvarCOBERCOD = "";
    String mvarCOBERORD = "";
    String mvarCAPITASG = "";
    String mvarCAPITIMP = "";
    String mvarDOCUMDAT = "";
    String mvarNOMBREAS = "";
    String mvarCAMP_CODIGO = "";
    String mvarCAMP_DESC = "";
    String mvarLEGAJO_GTE = "";
    String mvarNRO_PROD = "";
    String mvarSUCURSAL_CODIGO = "";
    String mvarLEGAJO_VEND = "";
    String mvarCONDUAPE = "";
    String mvarCONDUNOM = "";
    String mvarCONDUFEC = "";
    String mvarCONDUSEX = "";
    String mvarCONDUEST = "";
    String mvarCONDUEXC = "";
    String mvarAUACCCOD = "";
    String mvarAUVEASUM = "";
    String mvarAUVEADES = "";
    String mvarAUVEADEP = "";
    String mvarSITUCEST = "";
    String mvarINSPECOD = "";
    String mvarINSPECTOR = "";
    String mvarINSPEADOM = "";
    String mvarOBSERTXT = "";
    String mvarINSPENUM = "";
    String mvarCENTRCOD_INS = "";
    String mvarCENTRDES = "";
    String mvarINSPECC = "";
    String mvarRASTREO = "";
    String mvarALARMCOD = "";
    String mvarNUMEDOCU_ACRE = "";
    String mvarTIPODOCU_ACRE = "";
    String mvarAPELLIDO = "";
    String mvarNOMBRES = "";
    String mvarDOMICDOM = "";
    String mvarDOMICDNU = "";
    String mvarDOMICESC = "";
    String mvarDOMICPIS = "";
    String mvarDOMICPTA = "";
    String mvarDOMICPOB = "";
    String mvarDOMICCPO = "";
    String mvarPROVICOD = "";
    String mvarPAISSCOD = "";
    String mvarBANCOCOD = "";
    String mvarCOBROCOD = "";
    String mvarEFECTANN = "";
    String mvarEFECTMES = "";
    String mvarEFECTDIA = "";
    String mvarINSPEANN = "";
    String mvarINSPEMES = "";
    String mvarINSPEDIA = "";
    String mvarENTDOMSC = "";
    String mvarANOTACIO = "";
    String mvarPRECIO_MENSUAL = "";
    String mvarCUITNUME = "";
    String mvarRAZONSOC = "";
    String mvarCLIEIBTP = "";
    String mvarNROIIBB = "";
    String mvarLUNETA = "";
    String mvarRAMOPCOD = "";
    String mvarPlanNCod = "";
    String mvarPOLIZSEC = "";
    String mvarPOLIZANN = "";
    String mvarSucurCod = "";
    String mvarAgentCla = "";
    String aa = "";
    int i = 0;
    //
    //
    // DATOS GENERALES
    // Ojo se usa para cotizar
    // Coberturas
    // Asegurados
    // Conductores
    // Accesorios
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // CARGO EL XML DE ENTRADA
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      // VUELVO A COTIZAR PARA EL PLAN SELECCIONADO
      wvarStep = 20;
      //Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVLBAMQ.lbaw_GetCotizacion")
      
//      wobjClass = new lbawA_OVMQCotizar.lbaw_OVGetCotiAUS();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
      //Begin convert
      wvarResponse = getOsbConnector().executeRequest("lbaw_OVGetCotiAUS", XmlDomExtended.marshal(wobjXMLRequest.getDocument().getDocumentElement()));
      //End convert
      wobjClass = null;
      //
      // GUARDO LAS COBERTURAS, LAS SUMAS ASEGURADAS, CAMPANIA, PRODUCTOR Y TELEFONO
      wvarStep = 30;
      wobjXMLCoberturas = new XmlDomExtended();
      wobjXMLCoberturas.loadXML( wvarResponse );
      //controlo como devolvió la cotización
      if( XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='No se pudo realizar la cotización correspondiente' /></Response>" );
        //unsup GoTo ErrorHandler
      }
      wvarCot_Nro = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_COT_NRO )  );
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_SUMASEG ) , XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_SUMASEG )  ) );
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_SUMALBA ) , XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_SUMALBA )  ) );
      //
      wvarStep = 40;
      //
      wvarStep = 50;
      // DATOS GENERALES
      mvarRAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  );
      mvarPOLIZANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN )  );
      mvarPOLIZSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC )  );
      mvarCERTIPOL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL )  );
      mvarCERTIANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN )  );

      wvarStep = 60;
      mvarSUPLENUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SUPLENUM )  );
      mvarFRANQCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FRANQCOD )  );
      mvarPQTDES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PQTDES )  );
      mvarPLANCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PLANCOD )  );
      mvarHIJOS1416 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1416 )  );

      wvarStep = 70;
      mvarHIJOS1729 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1729 )  );
      mvarHIJOS1729ND = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1729ND )  );
      mvarZONA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ZONA )  );
      mvarCODPROV = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CODPROV )  );
      String mvarSUMALBAdec = String.valueOf( Obj.toDouble( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SUMALBA )  ) ) / 100 );
      if (mvarSUMALBAdec.indexOf(".") != -1) {
    	  mvarSUMALBA = mvarSUMALBAdec.substring(0, mvarSUMALBAdec.indexOf("."));
      } else {
    	  mvarSUMALBA = mvarSUMALBAdec;
      }
      mvarSUMALBA = Strings.replace( mvarSUMALBA, ",", "." );
      mvarCLIENIVA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENIVA )  );

      wvarStep = 80;
      String mvarSUMASEGdec = String.valueOf( Obj.toDouble( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SUMASEG )  ) ) / 100 );
      if (mvarSUMASEGdec.indexOf(".") != -1) {
    	  mvarSUMASEG = mvarSUMASEGdec.substring(0, mvarSUMASEGdec.indexOf("."));
      } else {
    	  mvarSUMASEG = mvarSUMASEGdec;
      }
      mvarSUMASEG = Strings.replace( mvarSUMASEG, ",", "." );
      mvarCLUB_LBA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLUB_LBA )  );
      mvarCPAANO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CPAANO )  );
      mvarCTAKMS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CTAKMS )  );
      mvarESCERO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ESCERO )  );

      wvarStep = 90;
      mvarTIENEPLAN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIENEPLAN )  );
      mvarCOND_ADIC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_COND_ADIC )  );
      mvarASEG_ADIC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ASEG_ADIC )  );
      mvarFH_NAC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FH_NAC )  );
      mvarSEXO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SEXO )  );

      wvarStep = 100;
      mvarESTCIV = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ESTCIV )  );
      mvarSIFMVEHI_DES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SIFMVEHI_DES )  );
      mvarPROFECOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PROFECOD )  );
      mvarSIACCESORIOS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SIACCESORIOS )  );
      mvarREFERIDO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_REFERIDO )  );

      wvarStep = 110;
      mvarMOTORNUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MOTORNUM )  );
      mvarCHASINUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CHASINUM )  );
      mvarPATENNUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PATENNUM )  );
      mvarAUMARCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUMARCOD )  );
      mvarAUMODCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUMODCOD )  );

      wvarStep = 120;
      mvarAUSUBCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUSUBCOD )  );
      mvarAUADICOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUADICOD )  );
      mvarAUMODORI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUMODORI )  );
      mvarAUUSOCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUUSOCOD )  );
      mvarAUVTVCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUVTVCOD )  );

      wvarStep = 130;
      mvarAUVTVDIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUVTVDIA )  );
      mvarAUVTVMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUVTVMES )  );
      mvarAUVTVANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUVTVANN )  );
      mvarVEHCLRCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_VEHCLRCOD )  );
      mvarAUKLMNUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUKLMNUM )  );

      wvarStep = 140;
      mvarFABRICAN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FABRICAN )  );
      mvarFABRICMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FABRICMES )  );
      mvarGUGARAGE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_GUGARAGE )  );
      mvarGUDOMICI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_GUDOMICI )  );
      mvarAUCATCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUCATCOD )  );

      wvarStep = 150;
      mvarAUTIPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUTIPCOD )  );
      mvarAUCIASAN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUCIASAN )  );
      mvarAUANTANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUANTANN )  );
      mvarAUNUMSIN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUNUMSIN )  );
      mvarAUNUMKMT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUNUMKMT )  );

      wvarStep = 160;
      mvarAUUSOGNC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AUUSOGNC )  );
      mvarUSUARCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD )  );
      mvarSITUCPOL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SITUCPOL )  );
      mvarCLIENSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC )  );
      mvarNUMEDOCU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU )  );

      wvarStep = 170;
      mvarTIPODOCU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU )  );
      mvarDOMICSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICSEC )  );
      mvarCUENTSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CUENTSEC )  );
      mvarCOBROFOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_COBROFOR )  );
      mvarEDADACTU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EDADACTU )  );

      wvarStep = 180;
      mvarCAMP_CODIGO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CAMP_CODIGO )  );
      mvarCAMP_DESC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CAMP_DESC )  );
      mvarLEGAJO_GTE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_LEGAJO_GTE )  );
      mvarNRO_PROD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NRO_PROD )  );
      mvarSUCURSAL_CODIGO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SUCURSAL_CODIGO )  );
      mvarLEGAJO_VEND = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_LEGAJO_VEND )  );

      wvarStep = 190;
      mvarSITUCEST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SITUCEST )  );
      mvarINSPECOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPECOD )  );
      mvarINSPECTOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPECTOR )  );
      mvarINSPEADOM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPEADOM )  );

      wvarStep = 200;
      mvarOBSERTXT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_OBSERTXT )  );
      mvarINSPENUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPENUM )  );
      mvarCENTRCOD_INS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CENTRCOD_INS )  );
      mvarCENTRDES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CENTRDES )  );
      //
      wvarStep = 205;
      mvarINSPECC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPECC )  );
      mvarRASTREO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RASTREO )  );
      mvarALARMCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ALARMCOD )  );
      mvarNUMEDOCU_ACRE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NUMEDOCU_ACRE )  );

      wvarStep = 210;
      mvarTIPODOCU_ACRE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIPODOCU_ACRE )  );
      mvarAPELLIDO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_APELLIDO )  );
      mvarNOMBRES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NOMBRES )  );
      mvarDOMICDOM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOM )  );
      mvarDOMICDNU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICDNU )  );
      mvarDOMICESC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICESC )  );

      wvarStep = 220;
      mvarDOMICPIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICPIS )  );
      mvarDOMICPTA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICPTA )  );
      mvarDOMICPOB = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOB )  );
      mvarDOMICCPO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMICCPO )  );
      mvarPROVICOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PROVICOD )  );
      mvarPAISSCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PAISSCOD )  );

      wvarStep = 230;
      mvarBANCOCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_BANCOCOD )  );
      mvarCOBROCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_COBROCOD )  );
      mvarEFECTANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EFECTANN )  );
      mvarEFECTMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EFECTMES )  );
      mvarEFECTDIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EFECTDIA )  );
      mvarINSPEANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPEANN )  );
      mvarINSPEMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPEMES )  );
      mvarINSPEDIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSPEDIA )  );
      mvarENTDOMSC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ENTDOMSC )  );
      mvarANOTACIO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ANOTACIO )  );
      mvarPRECIO_MENSUAL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PRECIO_MENSUAL )  );

      wvarStep = 167;
      mvarCUITNUME = "";
      if( wobjXMLRequest.selectNodes( mcteParam_CUITNUME ) .getLength() > 0 )
      {
        mvarCUITNUME = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CUITNUME )  );
      }
      mvarRAZONSOC = "";
      if( wobjXMLRequest.selectNodes( mcteParam_RAZONSOC ) .getLength() > 0 )
      {
        mvarRAZONSOC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAZONSOC )  );
      }
      mvarCLIEIBTP = "";
      if( wobjXMLRequest.selectNodes( mcteParam_CLIEIBTP ) .getLength() > 0 )
      {
        mvarCLIEIBTP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIEIBTP )  );
      }
      mvarNROIIBB = "";
      if( wobjXMLRequest.selectNodes( mcteParam_NROIIBB ) .getLength() > 0 )
      {
        mvarNROIIBB = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NROIIBB )  );
      }
      mvarLUNETA = "";
      if( wobjXMLRequest.selectNodes( mcteParam_LUNETA ) .getLength() > 0 )
      {
        mvarLUNETA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_LUNETA )  );
      }


      //
      wvarStep = 240;
      wobjXMLParametros = null;
      //
      wvarStep = 250;
      java.sql.Connection jdbcConn = null;
      try {
          JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
          jdbcConn = connFactory.getJDBCConnection(ModGeneral.gcteDB);
      String sp = "{? = call SPSNCV_PROD_SOLI_INSERT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
      CallableStatement ps = jdbcConn.prepareCall(sp);
      int posiSP = 1;
      ps.registerOutParameter(posiSP++, Types.INTEGER);
      
      // Obligatorios

		
		 wvarStep = 270;
	      //
	      // Parametros
	      wvarStep = 280;
	      ps.setString(posiSP++, String.valueOf(mvarRAMOPCOD));
	      
	      wvarStep = 290;
	      
	      if (mvarPOLIZANN.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarPOLIZANN)); 
		 }

	      wvarStep = 300;
	      
	      if (mvarPOLIZSEC.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarPOLIZSEC)); 
		 }

	      wvarStep = 310;
	      
	      if (mvarCERTIPOL.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarCERTIPOL)); 
		 }

	      wvarStep = 320;
	      
	      if (mvarCERTIANN.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarCERTIANN)); 
		 }

	      wvarStep = 330;
	      
	      if (mvarSUPLENUM.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarSUPLENUM)); 
		 }

	      wvarStep = 340;
	      ps.setString(posiSP++, String.valueOf(mvarFRANQCOD));

	      wvarStep = 350;
	      ps.setString(posiSP++, String.valueOf(mvarPQTDES));

	      wvarStep = 360;
	      
	      if (mvarPLANCOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarPLANCOD)); 
		 }

	      wvarStep = 370;
	      
	      if (mvarHIJOS1416.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarHIJOS1416)); 
		 }

	      wvarStep = 380;
	      
	      if (mvarHIJOS1729.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarHIJOS1729)); 
		 }

	      wvarStep = 390;
	      
	      if (mvarZONA.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarZONA)); 
		 }

	      wvarStep = 400;
	      
	      if (mvarCODPROV.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarCODPROV)); 
		 }

	      wvarStep = 410;
	      
	      if (mvarSUMALBA.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarSUMALBA)); 
		 }

	      wvarStep = 420;
	      ps.setString(posiSP++, String.valueOf(mvarCLIENIVA));

	      wvarStep = 430;
	      
	      if (mvarSUMASEG.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarSUMASEG)); 
		 }

	      wvarStep = 440;
	      ps.setString(posiSP++, String.valueOf(mvarCLUB_LBA));

	      wvarStep = 450;
	      
	      if (mvarCPAANO.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarCPAANO)); 
		 }

	      wvarStep = 460;
	      
	      if (mvarCTAKMS.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarCTAKMS)); 
		 }

	      wvarStep = 470;
	      ps.setString(posiSP++, String.valueOf(mvarESCERO));

	      wvarStep = 480;
	      ps.setString(posiSP++, String.valueOf(mvarTIENEPLAN));

	      wvarStep = 490;
	      ps.setString(posiSP++, String.valueOf(mvarCOND_ADIC));

	      wvarStep = 500;
	      ps.setString(posiSP++, String.valueOf(mvarASEG_ADIC));

	      wvarStep = 510;
	      
	      if (mvarFH_NAC.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarFH_NAC)); 
		 }

	      wvarStep = 520;
	      ps.setString(posiSP++, String.valueOf(mvarSEXO));

	      wvarStep = 530;
	      ps.setString(posiSP++, String.valueOf(mvarESTCIV));

	      wvarStep = 540;
	      ps.setString(posiSP++, String.valueOf(mvarSIFMVEHI_DES));

	      wvarStep = 550;
	      ps.setString(posiSP++, String.valueOf(mvarPROFECOD));

	      wvarStep = 560;
	      ps.setString(posiSP++, String.valueOf(mvarSIACCESORIOS));

	      wvarStep = 570;
	      
	      if (mvarREFERIDO.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarREFERIDO)); 
		 }

	      wvarStep = 580;
	      ps.setString(posiSP++, String.valueOf(mvarMOTORNUM));

	      wvarStep = 590;
	      ps.setString(posiSP++, String.valueOf(mvarCHASINUM));

	      wvarStep = 600;
	      ps.setString(posiSP++, String.valueOf(mvarPATENNUM));

	      wvarStep = 610;
	      
	      if (mvarAUMARCOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUMARCOD)); 
		 }

	      wvarStep = 620;
	      
	      if (mvarAUMODCOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUMODCOD)); 
		 }

	      wvarStep = 630;
	      
	      if (mvarAUSUBCOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUSUBCOD)); 
		 }

	      wvarStep = 640;
	      
	      if (mvarAUADICOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUADICOD)); 
		 }

	      wvarStep = 650;
	      ps.setString(posiSP++, String.valueOf(mvarAUMODORI));

	      wvarStep = 660;
	      
	      if (mvarAUUSOCOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUUSOCOD)); 
		 }

	      wvarStep = 670;
	      ps.setString(posiSP++, String.valueOf(mvarAUVTVCOD));

	      wvarStep = 680;
	      
	      if (mvarAUVTVDIA.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUVTVDIA)); 
		 }

	      wvarStep = 690;
	      
	      if (mvarAUVTVMES.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUVTVMES)); 
		 }

	      wvarStep = 700;
	      
	      if (mvarAUVTVANN.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUVTVANN)); 
		 }

	      wvarStep = 710;
	      
	      if (mvarVEHCLRCOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarVEHCLRCOD)); 
		 }

	      wvarStep = 720;
	      
	      if (mvarAUKLMNUM.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUKLMNUM)); 
		 }

	      wvarStep = 730;
	      
	      if (mvarFABRICAN.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarFABRICAN)); 
		 }

	      wvarStep = 740;
	      
	      if (mvarFABRICMES.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarFABRICMES)); 
		 }

	      wvarStep = 750;
	      ps.setString(posiSP++, String.valueOf(mvarGUGARAGE));

	      wvarStep = 760;
	      ps.setString(posiSP++, String.valueOf(mvarGUDOMICI));

	      wvarStep = 770;
	      
	      if (mvarAUCATCOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUCATCOD)); 
		 }

	      wvarStep = 780;
	      
	      if (mvarAUTIPCOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUTIPCOD)); 
		 }

	      wvarStep = 790;
	      ps.setString(posiSP++, String.valueOf(mvarAUCIASAN));

	      wvarStep = 800;
	      
	      if (mvarAUANTANN.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUANTANN)); 
		 }

	      wvarStep = 810;
	      
	      if (mvarAUNUMSIN.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUNUMSIN)); 
		 }

	      wvarStep = 820;
	      
	      if (mvarAUNUMKMT.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUNUMKMT)); 
		 }

	      wvarStep = 830;
	      ps.setString(posiSP++, String.valueOf(mvarAUUSOGNC));

	      wvarStep = 840;
	      ps.setString(posiSP++, String.valueOf(mvarUSUARCOD));

	      wvarStep = 850;
	      ps.setString(posiSP++, String.valueOf(mvarSITUCPOL));

	      wvarStep = 860;
	      
	      if (mvarCLIENSEC.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarCLIENSEC)); 
		 }

	      wvarStep = 870;
	      ps.setString(posiSP++, String.valueOf(mvarNUMEDOCU));

	      wvarStep = 880;
	      
	      if (mvarTIPODOCU.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarTIPODOCU)); 
		 }

	      wvarStep = 890;
	      
	      if (mvarDOMICSEC.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarDOMICSEC)); 
		 }

	      wvarStep = 900;
	      
	      if (mvarCUENTSEC.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarCUENTSEC)); 
		 }

	      wvarStep = 910;
	      
	      if (mvarCOBROFOR.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarCOBROFOR)); 
		 }

	      wvarStep = 920;
	      
	      if (mvarEDADACTU.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarEDADACTU)); 
		 }

	      //
	      // Coberturas
	      wvarStep = 930;
	      for( wvarCounter = 1; wvarCounter <= 20; wvarCounter++ )
	      {
	        mvarCOBERCOD = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_COBERCOD + wvarCounter )  );
	        mvarCOBERORD = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_COBERORD + wvarCounter )  );
	        mvarCAPITASG = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_CAPITASG + wvarCounter )  );
	        mvarCAPITIMP = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_CAPITIMP + wvarCounter )  );

	        wvarStep = 940;
	        if (mvarCOBERCOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarCOBERCOD)); 
		 }

	        wvarStep = 950;
	        if (mvarCOBERORD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarCOBERORD)); 
		 }

	        wvarStep = 960;
	        double capitaSg = Double.parseDouble(mvarCAPITASG.replace(",", "."));
	        ps.setDouble(posiSP++, capitaSg);

	        wvarStep = 970;
	        double capitimp = Double.parseDouble(mvarCAPITIMP.replace(",", "."));
	        ps.setDouble(posiSP++, capitimp);

	      }
	      // Fin Coberturas
	      // Asegurados
	      wvarStep = 980;
	      wobjXMLList = wobjXMLRequest.selectNodes( mcteParam_ASEGURADOS ) ;

	      wvarStep = 990;
	      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
	      {
	        mvarDOCUMDAT = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_DOCUMDAT )  );
	        mvarNOMBREAS = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_NOMBREAS )  );

	        wvarStep = 1000;
	        if (mvarDOCUMDAT.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarDOCUMDAT)); 
		 }

	        wvarStep = 1010;
	        ps.setString(posiSP++, String.valueOf(mvarNOMBREAS));

	      }

	      wvarStep = 1020;
	      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
	      {

	        wvarStep = 1030;
	        ps.setInt(posiSP++, 0);

	        wvarStep = 1040;
	        ps.setString(posiSP++, String.valueOf(" "));

	      }

	      wobjXMLList = (org.w3c.dom.NodeList) null;
	      // Fin Asegurados
	      wvarStep = 1050;
	      
	      if (mvarCAMP_CODIGO.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarCAMP_CODIGO)); 
		 }

	      wvarStep = 1055;
	      ps.setString(posiSP++,  Strings.left( Strings.trim( mvarCAMP_DESC ), 30 ) );

	      wvarStep = 1060;
	      ps.setString(posiSP++, String.valueOf(mvarLEGAJO_GTE));

	      wvarStep = 1070;
	      
	      if (mvarNRO_PROD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarNRO_PROD)); 
		 }

	      wvarStep = 1080;
	      ps.setString(posiSP++, String.valueOf(mvarSUCURSAL_CODIGO));

	      wvarStep = 1090;
	      ps.setString(posiSP++, String.valueOf(mvarLEGAJO_VEND));

	      // Hijos
	      wvarStep = 1100;
	      wobjXMLList = wobjXMLRequest.selectNodes( mcteParam_CONDUCTORES ) ;

	      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
	      {
	        mvarCONDUAPE = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_CONDUAPE )  );
	        mvarCONDUNOM = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_CONDUNOM )  );

	        wvarStep = 1110;
	        mvarCONDUFEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_CONDUFEC )  );
	        mvarCONDUSEX = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_CONDUSEX )  );
	        mvarCONDUEST = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_CONDUEST )  );
	        mvarCONDUEXC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_CONDUEXC )  );

	        wvarStep = 1120;
	        ps.setString(posiSP++, String.valueOf(mvarCONDUAPE));

	        wvarStep = 1130;
	        ps.setString(posiSP++, String.valueOf(mvarCONDUNOM));

	        wvarStep = 1140;
	        if (mvarCONDUFEC.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarCONDUFEC)); 
		 }

	        wvarStep = 1150;
	        ps.setString(posiSP++, String.valueOf(mvarCONDUSEX));

	        wvarStep = 1160;
	        ps.setString(posiSP++, String.valueOf(mvarCONDUEST));

	        wvarStep = 1170;
	        ps.setString(posiSP++, String.valueOf(mvarCONDUEXC));

	      }

	      wvarStep = 1180;
	      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
	      {

	        wvarStep = 1190;
	        ps.setString(posiSP++, String.valueOf(" "));

	        wvarStep = 1200;
	        ps.setString(posiSP++, String.valueOf(" "));

	        wvarStep = 1210;
	        ps.setInt(posiSP++, 0);

	        wvarStep = 1220;
	        ps.setString(posiSP++, String.valueOf(" "));

	        wvarStep = 1230;
	        ps.setString(posiSP++, String.valueOf(" "));

	        wvarStep = 1240;
	        ps.setString(posiSP++, String.valueOf(" "));

	      }

	      wvarStep = 1250;
	      wobjXMLList = (org.w3c.dom.NodeList) null;
	      // Conductores
	      // Accesorios
	      wvarStep = 1260;
	      wobjXMLList = wobjXMLRequest.selectNodes( mcteParam_ACCESORIOS ) ;

	      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
	      {
	        wvarStep = 1270;
	        mvarAUACCCOD = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_AUACCCOD )  );
	        mvarAUVEASUM = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_AUVEASUM )  );
	        mvarAUVEADES = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_AUVEADES )  );
	        mvarAUVEADEP = "S";

	        wvarStep = 1280;
	        if (mvarAUACCCOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarAUACCCOD)); 
		 }

	        wvarStep = 1290;
	        ps.setInt(posiSP++, mvarAUVEASUM.equals( "" ) ? 0 : Integer.parseInt(mvarAUVEASUM));

	        wvarStep = 1300;
	        ps.setString(posiSP++, String.valueOf(mvarAUVEADES));

	        wvarStep = 1310;
	        ps.setString(posiSP++, String.valueOf(mvarAUVEADEP));

	      }

	      wvarStep = 1320;
	      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
	      {

	        wvarStep = 1330;
	        ps.setInt(posiSP++, 0);

	        wvarStep = 1340;
	        ps.setInt(posiSP++, 0);

	        wvarStep = 1350;
	        ps.setString(posiSP++, String.valueOf(" "));

	        wvarStep = 1360;
	        ps.setString(posiSP++, String.valueOf(" "));

	      }

	      wvarStep = 1370;
	      wobjXMLList = (org.w3c.dom.NodeList) null;
	      // Fin Accesorios
	      wvarStep = 1380;
	      ps.setString(posiSP++, String.valueOf(mvarSITUCEST));

	      wvarStep = 1390;
	      
	      if (mvarINSPECOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarINSPECOD)); 
		 }

	      wvarStep = 1400;
	      ps.setString(posiSP++, String.valueOf(mvarINSPECTOR));

	      wvarStep = 1410;
	      ps.setString(posiSP++, String.valueOf(mvarINSPEADOM));

	      wvarStep = 1420;
	      ps.setString(posiSP++, String.valueOf(mvarOBSERTXT));

	      wvarStep = 1430;
	      
	      if (mvarINSPENUM.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarINSPENUM)); 
		 }

	      wvarStep = 1440;
	      
	      if (mvarCENTRCOD_INS.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarCENTRCOD_INS)); 
		 }

	      wvarStep = 1450;
	      ps.setString(posiSP++, String.valueOf(mvarCENTRDES));

	      wvarStep = 1452;
	      
	      if (mvarINSPECC.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarINSPECC)); 
		 }

	      wvarStep = 1454;
	      
	      if (mvarRASTREO.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarRASTREO)); 
		 }

	      wvarStep = 1456;
	      
	      if (mvarALARMCOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarALARMCOD)); 
		 }

	      wvarStep = 1460;
	      ps.setString(posiSP++, String.valueOf(mvarNUMEDOCU_ACRE));

	      wvarStep = 1470;
	      
	      if (mvarTIPODOCU_ACRE.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarTIPODOCU_ACRE)); 
		 }

	      wvarStep = 1480;
	      ps.setString(posiSP++, String.valueOf(mvarAPELLIDO));

	      wvarStep = 1490;
	      ps.setString(posiSP++, String.valueOf(mvarNOMBRES));

	      wvarStep = 1500;
	      ps.setString(posiSP++, String.valueOf(mvarDOMICDOM));

	      wvarStep = 1510;
	      ps.setString(posiSP++, String.valueOf(mvarDOMICDNU));

	      wvarStep = 1520;
	      ps.setString(posiSP++, String.valueOf(mvarDOMICESC));

	      wvarStep = 1530;
	      ps.setString(posiSP++, String.valueOf(mvarDOMICPIS));

	      wvarStep = 1540;
	      ps.setString(posiSP++, String.valueOf(mvarDOMICPTA));

	      wvarStep = 1550;
	      ps.setString(posiSP++, String.valueOf(mvarDOMICPOB));

	      wvarStep = 1560;
	      
	      if (mvarDOMICCPO.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarDOMICCPO)); 
		 }

	      wvarStep = 1570;
	      
	      if (mvarPROVICOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarPROVICOD)); 
		 }

	      wvarStep = 1580;
	      ps.setString(posiSP++, String.valueOf(mvarPAISSCOD));

	      wvarStep = 1590;
	      ps.setString(posiSP++, String.valueOf(mvarBANCOCOD));

	      wvarStep = 1600;
	      
	      if (mvarCOBROCOD.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarCOBROCOD)); 
		 }

	      wvarStep = 1610;
	      
	      if (mvarEFECTANN.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarEFECTANN)); 
		 }

	      wvarStep = 1620;
	      
	      if (mvarEFECTMES.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarEFECTMES)); 
		 }

	      wvarStep = 1630;
	      
	      if (mvarEFECTDIA.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarEFECTDIA)); 
		 }

	      wvarStep = 1640;
	      
	      if (mvarINSPEANN.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarINSPEANN)); 
		 }

	      wvarStep = 1650;
	      
	      if (mvarINSPEMES.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarINSPEMES)); 
		 }

	      wvarStep = 1660;
	      
	      if (mvarINSPEDIA.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarINSPEDIA)); 
		 }

	      wvarStep = 1670;
	      
	      if (mvarENTDOMSC.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarENTDOMSC)); 
		 }

	      wvarStep = 1680;
	      ps.setString(posiSP++, String.valueOf(mvarANOTACIO));

	      wvarStep = 1690;
	      
	      double precioMensual = Double.parseDouble(mvarPRECIO_MENSUAL.replace(",", "."));
	      ps.setDouble(posiSP++, precioMensual);

	      wvarStep = 1700;
	      ps.setString(posiSP++, String.valueOf(mvarCUITNUME));

	      wvarStep = 1710;
	      ps.setString(posiSP++, String.valueOf(mvarRAZONSOC));

	      wvarStep = 1720;
	      ps.setString(posiSP++, String.valueOf(mvarCLIEIBTP));

	      wvarStep = 1730;
	      ps.setString(posiSP++, String.valueOf(mvarNROIIBB));

	      wvarStep = 1740;
	      ps.setString(posiSP++, String.valueOf(mvarLUNETA));

	      wvarStep = 1745;
	      
	      if (mvarHIJOS1729ND.isEmpty()) {
			 ps.setNull(posiSP++, Types.NUMERIC);
		 } else { 
			 ps.setInt(posiSP++, Integer.parseInt(mvarHIJOS1729ND)); 
		 }
	
      //Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_ERRORMSG", adChar, adParamInput, 70, " ")
      //wobjDBCmd.Parameters.Append wobjDBParm
      //Set wobjDBParm = Nothing
      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      wvarStep = 1760;
//      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdStoredProc );
      boolean result = ps.execute();

      if (result) {
    	  //El primero es un ResultSet
      } else {
    	  //Salteo el "updateCount"
    	  int uc = ps.getUpdateCount();
    	  if (uc  == -1) {
//    		  System.out.println("No hay resultados");
    	  }
    	  result = ps.getMoreResults();
      }

      wvarStep = 1770;
      int returnValue = ps.getInt(1);
      if( returnValue >= 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><CADENA>" + returnValue + "</CADENA></Response>" );
      }
      else
      {
        
        if( returnValue == -1 )
        {
          wvarMensaje = "Error en la tabla SIFSPOLI";
        }
        else if( returnValue == -2 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 1";
        }
        else if( returnValue == -3 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 2";
        }
        else if( returnValue == -4 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 3";
        }
        else if( returnValue == -5 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 4";
        }
        else if( returnValue == -6 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 5";
        }
        else if( returnValue == -7 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 6";
        }
        else if( returnValue == -8 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 7";
        }
        else if( returnValue == -9 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 8";
        }
        else if( returnValue == -10 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 9";
        }
        else if( returnValue == -11 )
        {
          wvarMensaje = "Error en la tabla SEFMDECA en el registro # 10";
        }
        else if( returnValue == -12 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 1";
        }
        else if( returnValue == -13 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 2";
        }
        else if( returnValue == -14 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 3";
        }
        else if( returnValue == -15 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 4";
        }
        else if( returnValue == -16 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 5";
        }
        else if( returnValue == -17 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 6";
        }
        else if( returnValue == -18 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 7";
        }
        else if( returnValue == -19 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 8";
        }
        else if( returnValue == -20 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 9";
        }
        else if( returnValue == -21 )
        {
          wvarMensaje = "Error en la tabla SEFMDERI_ASEGADIC en el registro # 10";
        }
        else if( returnValue == -22 )
        {
          wvarMensaje = "Error en la tabla SIFSMOVI                ";
        }
        else if( returnValue == -23 )
        {
          wvarMensaje = "Error en la tabla SIFSFIMA_TARI               ";
        }
        else if( returnValue == -24 )
        {
          wvarMensaje = "Error en la tabla SIFSFIMA_VEHI               ";
        }
        else if( returnValue == -25 )
        {
          wvarMensaje = "Error en la tabla SIFSPECO                ";
        }
        else if( returnValue == -26 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_CAMP               ";
        }
        else if( returnValue == -27 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_GTE                ";
        }
        else if( returnValue == -28 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_PROD               ";
        }
        else if( returnValue == -29 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_SUCU               ";
        }
        else if( returnValue == -30 )
        {
          wvarMensaje = "Error en la tabla SIFSDERI_VEND               ";
        }
        else if( returnValue == -31 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 1         ";
        }
        else if( returnValue == -32 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 2         ";
        }
        else if( returnValue == -33 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 3         ";
        }
        else if( returnValue == -34 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 4         ";
        }
        else if( returnValue == -35 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 5         ";
        }
        else if( returnValue == -36 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 6         ";
        }
        else if( returnValue == -37 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 7         ";
        }
        else if( returnValue == -38 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 8         ";
        }
        else if( returnValue == -39 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 9         ";
        }
        else if( returnValue == -40 )
        {
          wvarMensaje = "Error en la tabla SIFMVECO en el registro # 10        ";
        }
        else if( returnValue == -41 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 1         ";
        }
        else if( returnValue == -42 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 2         ";
        }
        else if( returnValue == -43 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 3         ";
        }
        else if( returnValue == -44 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 4         ";
        }
        else if( returnValue == -45 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 5         ";
        }
        else if( returnValue == -46 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 6         ";
        }
        else if( returnValue == -47 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 7         ";
        }
        else if( returnValue == -48 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 8         ";
        }
        else if( returnValue == -49 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 9         ";
        }
        else if( returnValue == -50 )
        {
          wvarMensaje = "Error en la tabla SIFMVEAC en el registro # 10        ";
        }
        else if( returnValue == -51 )
        {
          wvarMensaje = "Error en la tabla SIFWINSV                ";
        }
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='" + wvarMensaje + "' /></Response>" );
      }

      wvarStep = 1780;
      } finally {
          try {
              if (jdbcConn != null)
                  jdbcConn.close();
          } catch (Exception e) {
              logger.log(Level.WARNING, "Exception al hacer un close", e);
          }
      }
      IAction_Execute = 0;
      return IAction_Execute;
    } catch( Exception e ) {
        logger.log(Level.SEVERE, this.getClass().getName(), e);
        throw new ComponentExecutionException(e);
    }
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
