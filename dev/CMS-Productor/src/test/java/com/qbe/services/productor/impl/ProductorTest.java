package com.qbe.services.productor.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.testutils.TestUtils;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;

public class ProductorTest  {
		
	public static final String TEST_SERVICE = "http://10.1.10.98:8011/OV/Proxy/QBESvc?wsdl";


		public ProductorTest() {
			super();
		}
		
		/**
		 * Testea la ejecución del mensaje, y que no vuelva un código de error. Valida contra la respuesta esperada
		 * 
		 * @param msgCode
		 * @throws Exception 
		 */
		protected void testMessageFullRequest (String msgCode, VBObjectClass comp) throws Exception {
			String responseText = this.testMessageJustExecution(msgCode, comp);
		
			Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
			XMLUnit.setIgnoreWhitespace(true);
			String expectedResponse = TestUtils.loadResponse(msgCode);
			if ( expectedResponse == null || expectedResponse.length()==0) throw new Exception("No se pudo cargar el request");
			DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(expectedResponse, responseText));
			List allDifferences = myDiff.getAllDifferences();
			assertEquals(myDiff.toString(), 0, allDifferences.size());
			//Eliminar los &lt; y &gt;
			XMLAssert.assertXMLEqual("No volvió la respuesta esperada", expectedResponse, responseText);
		
		}
		
		/**
		 * Testea la ejecución del mensaje, y que no vuelva un código de error. No valida contra la respuesta esperada, la devuelve solamente
		 * 
		 * @param msgCode
		 * @throws Exception 
		 */
		protected String testMessageJustExecution (String msgCode, VBObjectClass comp) throws Exception {
			String request = TestUtils.loadRequest(msgCode);
			if ( request == null || request.length()==0 || request.trim().equals("")) throw new Exception("No se pudo cargar el request");
			StringHolder responseHolder = new StringHolder();
			int result = comp.IAction_Execute(request, responseHolder, null);
//			System.out.println("Response: " + responseHolder);
			if ( result == 1) Assert.fail ("Abortó la ejecución, ver el log");
			Assert.assertTrue("Result no es 1 ni 0, es un error",result == 0);
			try {
				TestUtils.parseResultado(responseHolder.getValue());
				TestUtils.parseMensaje(responseHolder.getValue());
			} catch (Exception e){
				return responseHolder.getValue();
			}
			String responseText = responseHolder.getValue();
			if (( responseText == null) || ( responseText.length()==0)) {
				fail("responseText vacío");
			}
			return responseText;
		}
	
		//------------------------------ T E S T ------------------------------------------	
		@Ignore
		@Test
		public void testLbaw_PutSolicProd() throws Exception {
			//Descomentar las líneas de lbaw_PutSolicProd, para las pruebas.
			lbaw_PutSolicProd comp = new lbaw_PutSolicProd();
			OSBConnector conn = new OSBConnector();
			conn.setServiceURL(TEST_SERVICE);
			comp.setOsbConnector(conn);
			//No hace full request porque ahora vuelve un No se pudo realizar la cotización correspondiente
			testMessageJustExecution("_lbawA_ProductorLBA.lbaw_PutSolicProd", comp);
		}
		
		@Ignore
		@Test
		public void testLbaw_UpdSolicProd() throws Exception {
			//Descomentar las líneas de lbaw_UpdSolicProd, para las pruebas.
			lbaw_UpdSolicProd comp = new lbaw_UpdSolicProd();
			OSBConnector conn = new OSBConnector();
			conn.setServiceURL(TEST_SERVICE);
			comp.setOsbConnector(conn);
			testMessageFullRequest("_lbawA_ProductorLBA.lbaw_UpdSolicProd", comp);
		}
}