package com.qbe.scheduler;

public interface SchedulerConstants {

	public static final String USUARIO = "SCHEDULER_INT";
	public static final String LOCK_USER = "SCHEDULER_ENGINE";
	public static final Short NO_TERMINADO = 0;
	public static final Short TERMINADO = 1;
	public static final Short LOCKED = 1;
	public static final Short UNLOCKED = 0;
	public static final String ESTADO_FINAL_OK = "OK";
	public static final String ESTADO_FINAL_ERROR = "ERR";
	public static final String NBWS_E_POLIZA_FILL_APP = "NBWS_ePoliza_Fill";
	public static final String NBWS_E_POLIZA_EXE_APP = "NBWS_ePoliza_Exe";
	public static final String NBWS_A_FILL_SCHEDULER_ACTION_CODE = "nbwsA_FillScheduler";
	public static final String NBWS_A_EXE_SCHEDULER_ACTION_CODE = "nbwsA_ExeScheduler";
	static final String TESTER_USER = "Tester";
	public static final String TESTER_APLIC = "TestScheduler";
	public static final String SINI_PENDIENTES_APP = "SINI_PENDIENTES";

}
