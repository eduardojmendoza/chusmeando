package com.qbe.scheduler;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 
 */
@Entity
@Table ( name = "SERVOSB_PARAMETROS")
public class SchedulerAppParameters implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(length=20)
	private String aplic;

	@Column(length=20,nullable=false)
	private String paso;

	@Column(length=39)
	private String iaction;

	@Column(length=20)
	private String nextok;

	@Column(length=20)
	private String nexterr;

	private Short intentos;
	
	private Short periodicidad;

	@Column(length=20)
	private String dias;

	@Column(length=20)
	private String desde;

	@Column(length=20)
	private String hasta;

	private Integer retryinterval;

	public SchedulerAppParameters() {
	}

	public SchedulerAppParameters(String aplic, String paso) {
		this.aplic = aplic;
		this.paso = paso;
	}

	public SchedulerAppParameters(String aplic, String paso, String iaction,
			String nextok, String nexterr, Short intentos, Short periodicidad,
			String dias, String desde, String hasta, Integer retryinterval) {
		this.aplic = aplic;
		this.paso = paso;
		this.iaction = iaction;
		this.nextok = nextok;
		this.nexterr = nexterr;
		this.intentos = intentos;
		this.periodicidad = periodicidad;
		this.dias = dias;
		this.desde = desde;
		this.hasta = hasta;
		this.retryinterval = retryinterval;
	}

	public String getAplic() {
		return this.aplic;
	}

	public void setAplic(String aplic) {
		this.aplic = aplic;
	}

	public String getPaso() {
		return this.paso;
	}

	public void setPaso(String paso) {
		this.paso = paso;
	}

	public String getIaction() {
		return this.iaction;
	}

	public void setIaction(String iaction) {
		this.iaction = iaction;
	}

	public String getNextok() {
		return this.nextok;
	}

	public void setNextok(String nextok) {
		this.nextok = nextok;
	}

	public String getNexterr() {
		return this.nexterr;
	}

	public void setNexterr(String nexterr) {
		this.nexterr = nexterr;
	}

	public Short getIntentos() {
		return this.intentos;
	}

	public void setIntentos(Short intentos) {
		this.intentos = intentos;
	}

	public Short getPeriodicidad() {
		return this.periodicidad;
	}

	public void setPeriodicidad(Short periodicidad) {
		this.periodicidad = periodicidad;
	}

	public String getDias() {
		return this.dias;
	}

	public void setDias(String dias) {
		this.dias = dias;
	}

	public String getDesde() {
		return this.desde;
	}

	public void setDesde(String desde) {
		this.desde = desde;
	}

	public String getHasta() {
		return this.hasta;
	}

	public void setHasta(String hasta) {
		this.hasta = hasta;
	}

	public Integer getRetryinterval() {
		return this.retryinterval;
	}

	public void setRetryinterval(Integer retryinterval) {
		this.retryinterval = retryinterval;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SchedulerAppParameters))
			return false;
		SchedulerAppParameters castOther = (SchedulerAppParameters) other;

		return ((this.getAplic() == castOther.getAplic()) || (this.getAplic() != null
				&& castOther.getAplic() != null && this.getAplic().equals(
				castOther.getAplic())))
				&& ((this.getPaso() == castOther.getPaso()) || (this.getPaso() != null
						&& castOther.getPaso() != null && this.getPaso()
						.equals(castOther.getPaso())))
				&& ((this.getIaction() == castOther.getIaction()) || (this
						.getIaction() != null && castOther.getIaction() != null && this
						.getIaction().equals(castOther.getIaction())))
				&& ((this.getNextok() == castOther.getNextok()) || (this
						.getNextok() != null && castOther.getNextok() != null && this
						.getNextok().equals(castOther.getNextok())))
				&& ((this.getNexterr() == castOther.getNexterr()) || (this
						.getNexterr() != null && castOther.getNexterr() != null && this
						.getNexterr().equals(castOther.getNexterr())))
				&& ((this.getIntentos() == castOther.getIntentos()) || (this
						.getIntentos() != null
						&& castOther.getIntentos() != null && this
						.getIntentos().equals(castOther.getIntentos())))
				&& ((this.getPeriodicidad() == castOther.getPeriodicidad()) || (this
						.getPeriodicidad() != null
						&& castOther.getPeriodicidad() != null && this
						.getPeriodicidad().equals(castOther.getPeriodicidad())))
				&& ((this.getDias() == castOther.getDias()) || (this.getDias() != null
						&& castOther.getDias() != null && this.getDias()
						.equals(castOther.getDias())))
				&& ((this.getDesde() == castOther.getDesde()) || (this
						.getDesde() != null && castOther.getDesde() != null && this
						.getDesde().equals(castOther.getDesde())))
				&& ((this.getHasta() == castOther.getHasta()) || (this
						.getHasta() != null && castOther.getHasta() != null && this
						.getHasta().equals(castOther.getHasta())))
				&& ((this.getRetryinterval() == castOther.getRetryinterval()) || (this
						.getRetryinterval() != null
						&& castOther.getRetryinterval() != null && this
						.getRetryinterval()
						.equals(castOther.getRetryinterval())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getAplic() == null ? 0 : this.getAplic().hashCode());
		result = 37 * result
				+ (getPaso() == null ? 0 : this.getPaso().hashCode());
		result = 37 * result
				+ (getIaction() == null ? 0 : this.getIaction().hashCode());
		result = 37 * result
				+ (getNextok() == null ? 0 : this.getNextok().hashCode());
		result = 37 * result
				+ (getNexterr() == null ? 0 : this.getNexterr().hashCode());
		result = 37 * result
				+ (getIntentos() == null ? 0 : this.getIntentos().hashCode());
		result = 37
				* result
				+ (getPeriodicidad() == null ? 0 : this.getPeriodicidad()
						.hashCode());
		result = 37 * result
				+ (getDias() == null ? 0 : this.getDias().hashCode());
		result = 37 * result
				+ (getDesde() == null ? 0 : this.getDesde().hashCode());
		result = 37 * result
				+ (getHasta() == null ? 0 : this.getHasta().hashCode());
		result = 37
				* result
				+ (getRetryinterval() == null ? 0 : this.getRetryinterval()
						.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
