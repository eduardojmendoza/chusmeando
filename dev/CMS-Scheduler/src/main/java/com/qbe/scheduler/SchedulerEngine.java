package com.qbe.scheduler;

import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.cms.osbconnector.OSBConnectorException;

/**
 * Ver https://sites.google.com/a/snoopconsulting.com/migracioncomplus/diseno/scheduler
 * 
 * @author ramiro
 *
 */
@Service
public class SchedulerEngine {
	
	private static Logger logger = Logger.getLogger(SchedulerEngine.class.getName());
	
	private static final String ENVIA_ERROR = "1";

	private static final String ENVIA_SIEMPRE = "2";

	/**
	 * Map de los dias de la semana como se especifican en las tablas del scheduler a los que usa Java
	 */
	private static Map<Integer,String> DOWMap;

	@Autowired ( required=true )
	private SchedulerDAO dao;

	@Autowired ( required=true )
	private OSBConnector osbCon;
	
    @Autowired
    private MailSender mailSender;

    @Autowired
    private SimpleMailMessage mailMessage;

	public SchedulerEngine() {
	}

	/**
	 * Lista los items de bandeja del usuario
	 * 
	 * @return Un listado de los items 
	 * @param usuario
	 * @param terminados flag que indica si incluye o no los terminados en el listado
	 * @throws SchedulerConfigurationException Si encontramos algo mal configurado en el esquema del scheduler, 
	 * como por ejemplo una aplicación cargada dos veces 
	 */
	public String listUserInbox(String usuario, Boolean terminados)  {
		Short terminado = terminados ? SchedulerConstants.TERMINADO : SchedulerConstants.NO_TERMINADO;
		List<InboxItem> items = dao.findItemsByTerminadoUsu(terminado, usuario);
		StringBuffer result = new StringBuffer();
		for (InboxItem inboxItem : items) {
			result.append(inboxItem.toString());
		}
		return result.toString();
	}
			

	/**
	 * 
	 * 
	 * @return Un reporte de la ejecución 
	 * @param name
	 * @throws SchedulerConfigurationException Si encontramos algo mal configurado en el esquema del scheduler, 
	 * como por ejemplo una aplicación cargada dos veces 
	 */
	public String processUserInbox(String usuario)  {
		logger.fine(String.format("Inicio de proceso de scheduler para usuario %s", usuario));
		List<InboxItem> items = dao.findPendingItemsForUser(usuario);
		String result = processInboxItems(items, usuario);
		logger.fine(String.format("Fin de proceso de scheduler para usuario %s. Reporte: [%s]", usuario, result));
		return result;
	}

	private String processInboxItems(List<InboxItem> items, String usuario) {
		SchedulerItemResults results = new SchedulerItemResults();
		if ( items.size() == 0) {
			SchedulerItemResult itemRes = new SchedulerItemResult();
			itemRes.setStatus("SIN ITEMS");
			itemRes.setMessage(String.format("No se encontraron items en la bandeja para el usuario %s", usuario));
			results.add(itemRes);
		} else {
			logger.fine(String.format("Scheduler de usuario %s. items size = [%d]", usuario, items.size()));

			for (InboxItem itemBandeja : items) {
				SchedulerItemResult itemRes = processItem(itemBandeja);
				if ( itemRes != null) {
					results.add(itemRes);
				}
			}
		}
		return results.toReport();
	}

	protected SchedulerItemResult processItem(InboxItem itemBandeja) {
		
		logger.fine(String.format("Scheduler. Procesando item: [%s]", itemBandeja));
		SchedulerItemResult itemResult = new SchedulerItemResult();
		itemResult.setItem(itemBandeja);
		
		try {
			itemBandeja.setLock(SchedulerConstants.LOCKED);
			itemBandeja.setLockOwner(SchedulerConstants.LOCK_USER);
			//Grabo así toma este lock
			dao.persist(itemBandeja);
			
			SchedulerAppParameters appParams = dao.findParametroAplic(itemBandeja.getAplic());
			if ( appParams == null) {
				itemResult.setError(String.format("No pude encontrar los parámetros de la app con nombre: %s", itemBandeja.getAplic()));
				itemBandeja.setLock(SchedulerConstants.UNLOCKED);
				itemBandeja.setLockOwner(null);
				//Grabo así toma este (un)lock
				logger.fine(String.format("SchedulerEngine: por persistir itemBandeja: [%s] para liberar el lock", itemBandeja));
				dao.persist(itemBandeja);
			} else {
				if ( programacionActiva(appParams)) {
					String actionCode = appParams.getIaction();
					String dataRequestXml = itemBandeja.getDatain();
					logger.finest(String.format("Item habilitado para procesarlo. actionCode=[%s], request=[%s]", actionCode, dataRequestXml));

					try {
						String response = this.sendRequest(actionCode,dataRequestXml);
						logger.finest(String.format("SchedulerEngine.processItem: response=[%s]", response));
						
						itemBandeja.setTerminado(SchedulerConstants.TERMINADO);
						itemBandeja.setEstFinal(SchedulerConstants.ESTADO_FINAL_OK);
						itemBandeja.setRcod(0);
						itemBandeja.setRmsg(response);
						itemResult.setCompletado(response);
						//Grabo
						logger.fine(String.format("SchedulerEngine: por persistir itemBandeja: [%s]", itemBandeja));
						dao.persist(itemBandeja);
						
						logger.fine(String.format("SchedulerEngine: por reschedulear itemBandeja: [%s]", itemBandeja));
						reschedule(itemBandeja);
					} catch (OSBConnectorException e) {
						logger.log(Level.SEVERE, String.format("SchedulerEngine: al ejecutar itemBandeja [%s]", itemBandeja), e);
						String errorMsg = String.format("Exception %s [ %s ] al ejecutar item bandeja [%s]. StackTrace: %s", e.getClass().getName(), e.getMessage(), itemBandeja, ExceptionUtils.getStackTrace(e));
						itemResult.setError(errorMsg);
					} finally {
						itemBandeja.setLock(SchedulerConstants.UNLOCKED);
						itemBandeja.setLockOwner(null);
						//Grabo así toma este (un)lock
						logger.fine(String.format("SchedulerEngine: por persistir itemBandeja: [%s] para liberar el lock", itemBandeja));
						dao.persist(itemBandeja);
					}
				} else {
					logger.fine(String.format("SchedulerEngine: retornando null porque en este momento no está activa la programación de %s", appParams));
					itemBandeja.setLock(SchedulerConstants.UNLOCKED);
					itemBandeja.setLockOwner(null);
					//Grabo así toma este (un)lock
					logger.fine(String.format("SchedulerEngine: por persistir itemBandeja: [%s] para liberar el lock", itemBandeja));
					try {
						dao.persist(itemBandeja);
					} catch (DataAccessException e) {
						logger.log(Level.SEVERE, String.format("SchedulerEngine: al intentar grabar deslockeo de item %s",  itemBandeja), e);
					}
					return null;
				}
				
			}

		} catch (DataAccessException e) {
			String msg = String.format("SchedulerEngine: DataAccessException ( %s ) mientras procesaba item %s", e.getMessage(), itemBandeja);
			logger.log(Level.WARNING, msg, e);
			itemResult.setError(msg);
		} catch (SchedulerConfigurationException e) {
			String msg = String.format("SchedulerEngine: SchedulerConfigurationException ( %s ) mientras procesaba item %s", e.getMessage(), itemBandeja);
			logger.log(Level.WARNING, msg, e);
			itemResult.setError(msg);
		}
		sendItemReport(itemResult);
		return itemResult;

	}

	private void sendItemReport(SchedulerItemResult itemResult) {
		String enviaConfig = null;
		//Condición por default
		enviaConfig = ENVIA_SIEMPRE;
		try {
			SchedulerApplication app;
			app = dao.findAplicacionByAplic(itemResult.getItem().getAplic());
			if ( app == null ) {
				String errorMsg = String.format("SchedulerEngine: No se encontró app [%s]. No puedo levantar config de envio de mail, tomo default", itemResult.getItem().getAplic());
				logger.warning(errorMsg);
				itemResult.setError(errorMsg);
				enviaConfig = ENVIA_SIEMPRE;
			} else {
				enviaConfig = app.getEnverror();
				this.mailMessage.setTo(app.getMail());
			}
		} catch (SchedulerConfigurationException e) {
			itemResult.setError(String.format("Exception %s [ %s ] al enviar mail con resultado de item: %s. StackTrace: %s", e.getClass().getName(), e.getMessage(), itemResult.getItemData(),ExceptionUtils.getStackTrace(e)));
		}
		if ( ENVIA_SIEMPRE.equals(enviaConfig) || ( ENVIA_ERROR.equals(enviaConfig) && itemResult.isError() ) ) {
			//Mando mail con el resultado
			if ( this.mailMessage == null || this.mailSender == null) {
				logger.severe("No está configurado el mailSender ni message en Scheduler Engine. Revisar la configuración de Spring");
			}
			this.mailMessage.setSubject("Resultado de ejecución de item de bandeja por el Scheduler");
			this.mailMessage.setText(itemResult.toReport());
			this.mailSender.send(this.mailMessage);
			logger.fine("SchedulerEngine: Mail enviado");
		}
		
	}

	/**
	 * Re-schedulea el item si la aplicación está así configurada
	 * @param itemBandeja
	 * @throws SchedulerConfigurationException 
	 */
	protected InboxItem reschedule(InboxItem itemBandeja) throws SchedulerConfigurationException {
		SchedulerApplication app = dao.findAplicacionByAplic(itemBandeja.getAplic());
		if ( app == null) {
			//Si no tiene configuración, no se reschedulea
			logger.warning(String.format("SchedulerEngine: No se encontró app para itemBandeja: [%s]. No puedo reschedulear", itemBandeja));
			return null;
		}
		InboxItem nuevo = createRescheduledItem(itemBandeja, app);

		dao.persist(nuevo);
		return nuevo;
	}

	protected InboxItem createRescheduledItem(InboxItem itemBandeja, SchedulerApplication app) {
		InboxItem nuevo = new InboxItem();
		nuevo.setAplic(itemBandeja.getAplic());
		nuevo.setDatain(itemBandeja.getDatain());
		nuevo.setTerminado(SchedulerConstants.NO_TERMINADO);
		nuevo.setCodUsu(itemBandeja.getCodUsu());
		nuevo.setLock(SchedulerConstants.UNLOCKED);
		nuevo.setLockOwner(null);

		Calendar nuevaFecha = Calendar.getInstance();
		nuevaFecha.setTime(itemBandeja.getFechaIni()); 
		nuevaFecha.add(Calendar.DAY_OF_MONTH, app.getPeriodicidad());
		
		nuevo.setFechaIni(nuevaFecha.getTime());
		logger.fine(String.format("SchedulerEngine: nuevo itemBandeja a reschedulear: [%s]", nuevo));
		return nuevo;
	}

	/**
	 * Chequea si en este momento corresponde ejecutar los items que corresponden a appParams de acuerdo a la programación definida ( dias y horarios )
	 *  
	 * @param appParams
	 * @return
	 * @throws SchedulerConfigurationException Si el formato del desde o el hasta es incorrecto. Debe ser HH:MM
	 */
	protected boolean programacionActiva(SchedulerAppParameters appParams) throws SchedulerConfigurationException {
		String dias = appParams.getDias();
		String paramDesde = appParams.getDesde();
		String paramHasta = appParams.getHasta();
		Calendar ahora = Calendar.getInstance();

		String appDOW = getDOWMap().get(ahora.get(Calendar.DAY_OF_WEEK));
		logger.fine(String.format("Scheduler: dias = [%s], hoy es DOW: [%s]", dias, appDOW));
		if ( !dias.contains(appDOW)) return false;
		Calendar desde = getCalendarWithNewHour(paramDesde, ahora);
		Calendar hasta = getCalendarWithNewHour(paramHasta, ahora);
		if ( desde == null || hasta == null) {
			throw new SchedulerConfigurationException(String.format("Formato de horas inválido en los parámetros de la aplicación ",appParams.getAplic()));
		}
		logger.fine(String.format("Scheduler: desde = [%s], ahora = [%s], hasta = [%s]", desde, ahora, hasta));
		return ( desde.before(ahora) && ahora.before(hasta));
	}
	
	protected Calendar getCalendarWithNewHour(String paramHasta, Calendar ahora) {
		if ( paramHasta.length() != 5 ) return null;
		int hora = Integer.parseInt(paramHasta.substring(0, 2));
		int min = Integer.parseInt(paramHasta.substring(3, 5));
		
		Calendar nuevo = Calendar.getInstance();
		nuevo.setTimeInMillis(ahora.getTimeInMillis());
		nuevo.set(Calendar.HOUR_OF_DAY, hora);
		nuevo.set(Calendar.MINUTE, min);
		return nuevo;
	}

	protected static Map<Integer,String> getDOWMap() {
		if ( DOWMap == null) {
			DOWMap = new HashMap<Integer, String>();
			DOWMap.put(Calendar.SUNDAY, "D");
			DOWMap.put(Calendar.MONDAY, "L");
			DOWMap.put(Calendar.TUESDAY, "M");
			DOWMap.put(Calendar.WEDNESDAY, "X");
			DOWMap.put(Calendar.THURSDAY, "J");
			DOWMap.put(Calendar.FRIDAY, "V");
			DOWMap.put(Calendar.SATURDAY, "S");
		}
		return DOWMap;
	}

	/**
	 * Ejecuta el action code
	 * @param name
	 * @param actionCode
	 * @param dataRequestXml
	 * @throws MalformedURLException 
	 * @throws OSBConnectorException 
	 */
	private String sendRequest(String actionCode,
			String dataRequestXml) throws OSBConnectorException {
		String requestXml = "<Request>" + dataRequestXml + "</Request>";
			String response = osbCon.executeQueueRequest(actionCode, requestXml);
			return response;
		
	}

	public SchedulerDAO getDao() {
		return dao;
	}

	public void setDao(SchedulerDAO dao) {
		this.dao = dao;
	}

	public OSBConnector getOsbCon() {
		return osbCon;
	}

	public void setOsbCon(OSBConnector osbCon) {
		this.osbCon = osbCon;
	}
	
}
