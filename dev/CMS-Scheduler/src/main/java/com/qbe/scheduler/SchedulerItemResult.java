package com.qbe.scheduler;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlCDATA;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SchedulerItemResult")
public class SchedulerItemResult {

	private static final String EJECUTADO = "EJECUTADO";
	
	private static final String ERROR = "ERROR";

    @XmlAttribute
	private String status = "N/A";
	
    @XmlElement
    @XmlCDATA
    private String message = "N/A";
    
    //FIXME cambiarlo por un DTO del itemBandeja así tiene todos los datos
    @XmlElement
    private String itemData = "N/A";
	
    private InboxItem item;
    
	public SchedulerItemResult() {
	}
	
	public boolean isError() {
		return this.status.equals(ERROR);
	}
	
	public void setError(String data) {
		this.status = ERROR;
		this.message = data;
	}

	public void setCompletado(String data) {
		this.status = EJECUTADO;
		this.message = data;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setItem(InboxItem itemBandeja) {
		this.item = itemBandeja;
		this.itemData = String.format("Ident: %d, Aplic: %s, Fecha_ini: %s, Datain: %s", itemBandeja.getIdent(), itemBandeja.getAplic(), itemBandeja.getFechaIni(), itemBandeja.getDatain());
	}

	public InboxItem getItem() {
		return item;
	}

	public String getItemData() {
		return itemData;
	}

	public void setItemData(String itemData) {
		this.itemData = itemData;
	}

	public String toReport() {
		StringBuffer sb = new StringBuffer();
		sb.append("SchedulerItemResult: \n\n");
		this.addReportTo(sb);
		return sb.toString();
	}

	public void addReportTo(StringBuffer sb) {
		sb.append("Estado: ");
		sb.append(status);
		sb.append("\n\n");
		
		sb.append("Mensaje: ");
		sb.append(message);
		sb.append("\n\n");
		
		sb.append("Item: ");
		if ( item != null ) {
			this.item.addReportTo(sb);
		} else {
			sb.append("N/A");
		}
		sb.append("\n\n");
	}

	
}
