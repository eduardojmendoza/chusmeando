package com.qbe.scheduler;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;

/**
 * 
 */
@Entity
@Table ( name = "SERVOSB_BANDEJA")
public class InboxItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7720277301951100993L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int ident;

	@Column(length=20)
	private String aplic;
	
	private Integer solicitud;
	
	@Column(length=20)
	private String paso;
	
	private Short intento;
	
	@Column(length=5000)
	private String datain;
	
	@Column(length=1000)
	private String dataex;
	
	private Short terminado;
	
	@Column (name = "EST_FINAL")
	private String estFinal;
	
	@Column (name = "FECHA_INI")
	private Date fechaIni;
	
	@Column (name = "FECHA_PASO")
	private Date fechaPaso;

	@Column (name = "COD_USU", length = 20)
	private String codUsu;
	
	private Integer rcod;
	
	@Column(length=200)
	private String rmsg;
	
	private Short lock;
	
	@Column (name = "LOCK_OWNER", length = 64)
	private String lockOwner;

	public InboxItem() {
	}

	public InboxItem(int ident) {
		this.ident = ident;
	}

	public InboxItem(int ident, String aplic, Integer solicitud,
			String paso, Short intento, String datain, String dataex,
			Short terminado, String estFinal, Date fechaIni, Date fechaPaso,
			String codUsu, Integer rcod, String rmsg, Short lock,
			String lockOwner) {
		this.ident = ident;
		this.aplic = aplic;
		this.solicitud = solicitud;
		this.paso = paso;
		this.intento = intento;
		this.datain = datain;
		this.dataex = dataex;
		this.terminado = terminado;
		this.estFinal = estFinal;
		this.fechaIni = fechaIni;
		this.fechaPaso = fechaPaso;
		this.codUsu = codUsu;
		this.rcod = rcod;
		this.rmsg = rmsg;
		this.lock = lock;
		this.lockOwner = lockOwner;
	}

	public int getIdent() {
		return this.ident;
	}

	public void setIdent(int ident) {
		this.ident = ident;
	}

	public String getAplic() {
		return this.aplic;
	}

	public void setAplic(String aplic) {
		this.aplic = aplic;
	}

	public Integer getSolicitud() {
		return this.solicitud;
	}

	public void setSolicitud(Integer solicitud) {
		this.solicitud = solicitud;
	}

	public String getPaso() {
		return this.paso;
	}

	public void setPaso(String paso) {
		this.paso = paso;
	}

	public Short getIntento() {
		return this.intento;
	}

	public void setIntento(Short intento) {
		this.intento = intento;
	}

	public String getDatain() {
		return this.datain;
	}

	public void setDatain(String datain) {
		this.datain = datain;
	}

	public String getDataex() {
		return this.dataex;
	}

	public void setDataex(String dataex) {
		this.dataex = dataex;
	}

	public Short getTerminado() {
		return this.terminado;
	}

	public void setTerminado(Short terminado) {
		this.terminado = terminado;
	}

	public String getEstFinal() {
		return this.estFinal;
	}

	public void setEstFinal(String estFinal) {
		this.estFinal = estFinal;
	}

	public Date getFechaIni() {
		return this.fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public Date getFechaPaso() {
		return this.fechaPaso;
	}

	public void setFechaPaso(Date fechaPaso) {
		this.fechaPaso = fechaPaso;
	}

	public String getCodUsu() {
		return this.codUsu;
	}

	public void setCodUsu(String codUsu) {
		this.codUsu = codUsu;
	}

	public Integer getRcod() {
		return this.rcod;
	}

	public void setRcod(Integer rcod) {
		this.rcod = rcod;
	}

	public String getRmsg() {
		return this.rmsg;
	}

	public void setRmsg(String rmsg) {
		this.rmsg = StringUtils.mid( rmsg, 0, 200);
	}

	public Short getLock() {
		return this.lock;
	}

	public void setLock(Short lock) {
		this.lock = lock;
	}

	public String getLockOwner() {
		return this.lockOwner;
	}

	public void setLockOwner(String lockOwner) {
		this.lockOwner = lockOwner;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof InboxItem))
			return false;
		InboxItem castOther = (InboxItem) other;

		return (this.getIdent() == castOther.getIdent())
				&& ((this.getAplic() == castOther.getAplic()) || (this
						.getAplic() != null && castOther.getAplic() != null && this
						.getAplic().equals(castOther.getAplic())))
				&& ((this.getSolicitud() == castOther.getSolicitud()) || (this
						.getSolicitud() != null
						&& castOther.getSolicitud() != null && this
						.getSolicitud().equals(castOther.getSolicitud())))
				&& ((this.getPaso() == castOther.getPaso()) || (this.getPaso() != null
						&& castOther.getPaso() != null && this.getPaso()
						.equals(castOther.getPaso())))
				&& ((this.getIntento() == castOther.getIntento()) || (this
						.getIntento() != null && castOther.getIntento() != null && this
						.getIntento().equals(castOther.getIntento())))
				&& ((this.getDatain() == castOther.getDatain()) || (this
						.getDatain() != null && castOther.getDatain() != null && this
						.getDatain().equals(castOther.getDatain())))
				&& ((this.getDataex() == castOther.getDataex()) || (this
						.getDataex() != null && castOther.getDataex() != null && this
						.getDataex().equals(castOther.getDataex())))
				&& ((this.getTerminado() == castOther.getTerminado()) || (this
						.getTerminado() != null
						&& castOther.getTerminado() != null && this
						.getTerminado().equals(castOther.getTerminado())))
				&& ((this.getEstFinal() == castOther.getEstFinal()) || (this
						.getEstFinal() != null
						&& castOther.getEstFinal() != null && this
						.getEstFinal().equals(castOther.getEstFinal())))
				&& ((this.getFechaIni() == castOther.getFechaIni()) || (this
						.getFechaIni() != null
						&& castOther.getFechaIni() != null && this
						.getFechaIni().equals(castOther.getFechaIni())))
				&& ((this.getFechaPaso() == castOther.getFechaPaso()) || (this
						.getFechaPaso() != null
						&& castOther.getFechaPaso() != null && this
						.getFechaPaso().equals(castOther.getFechaPaso())))
				&& ((this.getCodUsu() == castOther.getCodUsu()) || (this
						.getCodUsu() != null && castOther.getCodUsu() != null && this
						.getCodUsu().equals(castOther.getCodUsu())))
				&& ((this.getRcod() == castOther.getRcod()) || (this.getRcod() != null
						&& castOther.getRcod() != null && this.getRcod()
						.equals(castOther.getRcod())))
				&& ((this.getRmsg() == castOther.getRmsg()) || (this.getRmsg() != null
						&& castOther.getRmsg() != null && this.getRmsg()
						.equals(castOther.getRmsg())))
				&& ((this.getLock() == castOther.getLock()) || (this.getLock() != null
						&& castOther.getLock() != null && this.getLock()
						.equals(castOther.getLock())))
				&& ((this.getLockOwner() == castOther.getLockOwner()) || (this
						.getLockOwner() != null
						&& castOther.getLockOwner() != null && this
						.getLockOwner().equals(castOther.getLockOwner())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getIdent();
		result = 37 * result
				+ (getAplic() == null ? 0 : this.getAplic().hashCode());
		result = 37 * result
				+ (getSolicitud() == null ? 0 : this.getSolicitud().hashCode());
		result = 37 * result
				+ (getPaso() == null ? 0 : this.getPaso().hashCode());
		result = 37 * result
				+ (getIntento() == null ? 0 : this.getIntento().hashCode());
		result = 37 * result
				+ (getDatain() == null ? 0 : this.getDatain().hashCode());
		result = 37 * result
				+ (getDataex() == null ? 0 : this.getDataex().hashCode());
		result = 37 * result
				+ (getTerminado() == null ? 0 : this.getTerminado().hashCode());
		result = 37 * result
				+ (getEstFinal() == null ? 0 : this.getEstFinal().hashCode());
		result = 37 * result
				+ (getFechaIni() == null ? 0 : this.getFechaIni().hashCode());
		result = 37 * result
				+ (getFechaPaso() == null ? 0 : this.getFechaPaso().hashCode());
		result = 37 * result
				+ (getCodUsu() == null ? 0 : this.getCodUsu().hashCode());
		result = 37 * result
				+ (getRcod() == null ? 0 : this.getRcod().hashCode());
		result = 37 * result
				+ (getRmsg() == null ? 0 : this.getRmsg().hashCode());
		result = 37 * result
				+ (getLock() == null ? 0 : this.getLock().hashCode());
		result = 37 * result
				+ (getLockOwner() == null ? 0 : this.getLockOwner().hashCode());
		return result;
	}

	@Override
	public String toString() {
		return String.format("InboxItem {" +
				"ident = {%s}, " +
				"aplic = {%s}, " +
				"solicitud = {%s}, " +
				"paso = {%s}, " +
				"intento = {%s}, " +
				"datain = {%s}, " +
				"dataex = {%s}, " +
				"terminado = {%s}, " +
				"estFinal = {%s}, " +
				"fechaIni = {%s}, " +
				"fechaPaso = {%s}, " +
				"codUsu = {%s}, " +
				"rcod = {%s}, " +
				"rmsg = {%s}, " +
				"lock = {%s}, " +
				"lockOwner = {%s} " +
				"}", ident, aplic, solicitud, paso, intento, datain, dataex, terminado, estFinal, fechaIni, fechaPaso, codUsu, rcod, rmsg, lock, lockOwner);
	}

	public void addReportTo(StringBuffer sb) {
		sb.append(String.format(
				"ident = %s \n" +
				"aplic = %s \n" +
				"solicitud = %s \n" +
				"paso = %s \n" +
				"intento = %s \n" +
				"datain = %s \n" +
				"dataex = %s \n" +
				"terminado = %s \n" +
				"estFinal = %s \n" +
				"fechaIni = %s \n" +
				"fechaPaso = %s \n" +
				"codUsu = %s \n" +
				"rcod = %s \n" +
				"rmsg = %s \n" +
				"lock = %s \n" +
				"lockOwner = %s \n",
				ident, aplic, solicitud, paso, intento, datain, dataex, terminado, estFinal, fechaIni, fechaPaso, codUsu, rcod, rmsg, lock, lockOwner));
	}
}
