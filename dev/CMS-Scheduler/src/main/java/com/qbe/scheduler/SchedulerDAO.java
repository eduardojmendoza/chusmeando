package com.qbe.scheduler;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.NonUniqueResultException;

import org.hibernate.SessionFactory;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 */
@Component
public class SchedulerDAO extends HibernateDaoSupport {

	protected static Logger logger = Logger.getLogger(SchedulerDAO.class.getName());

	/**
	 * Timeout para las queries, en segundos
	 */
	private static final int TIMEOUT = 30;
	
	public SchedulerDAO() {
	}

	@Autowired
	public SchedulerDAO(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Transactional
	public List<InboxItem> findItemsByAplic(String aplic) {
		@SuppressWarnings("unchecked")
		List<InboxItem> items = this.getSessionFactory().getCurrentSession()
				.createQuery("from InboxItem where aplic= :aplic ")
				.setString("aplic", aplic)
				.setTimeout(TIMEOUT)
				.list();
		return items;
	}

	@Transactional
	public List<InboxItem> findItemsByAplicTerminadoUsu(String aplic, Short terminado, String codUsu) {
		@SuppressWarnings("unchecked")
		List<InboxItem> items = this.getSessionFactory().getCurrentSession()
				.createQuery("from InboxItem where aplic= :aplic and terminado = :terminado and codUsu = :codUsu")
				.setString("aplic", aplic)
				.setShort("terminado", terminado)
				.setString("codUsu", codUsu)
				.setTimeout(TIMEOUT)
				.list();
		return items;
	}

	@Transactional
	public List<InboxItem> findItemsByTerminadoUsu(Short terminado, String codUsu) {
		@SuppressWarnings("unchecked")
		List<InboxItem> items = this.getSessionFactory().getCurrentSession()
				.createQuery("from InboxItem where terminado = :terminado and codUsu = :codUsu")
				.setShort("terminado", terminado)
				.setString("codUsu", codUsu)
				.setTimeout(TIMEOUT)
				.list();
		return items;
	}

	@Transactional
	public List<InboxItem> findUnlockedItemsByAplicTerminadoUsu(String name,
			Short terminado, String usuario) {
		logger.log(Level.FINEST, String.format("findUnlockedItemsByAplicTerminadoUsu %s %d %s", name, terminado, usuario));
		@SuppressWarnings("unchecked")
		List<InboxItem> items = this.getSessionFactory().getCurrentSession()
				.createQuery("from InboxItem where aplic= :aplic and terminado = :terminado and codUsu = :codUsu and lock = :lock")
				.setString("aplic", name)
				.setShort("terminado", terminado)
				.setString("codUsu", usuario)
				.setShort("lock", SchedulerConstants.UNLOCKED)
				.setTimeout(TIMEOUT)
				.list();
		return items;
	}

	@Transactional
	public List<InboxItem> findPendingItemsForUser(String usuario) {
		@SuppressWarnings("unchecked")
		List<InboxItem> items = this.getSessionFactory().getCurrentSession()
				.createQuery("from InboxItem where terminado = :terminado and codUsu = :codUsu and lock = :lock and fechaIni <= :ahora")
				.setShort("terminado", SchedulerConstants.NO_TERMINADO)
				.setString("codUsu", usuario)
				.setShort("lock", SchedulerConstants.UNLOCKED)
				.setDate("ahora", new java.sql.Date(Calendar.getInstance().getTimeInMillis()))
				.setTimeout(TIMEOUT)
				.list();
		return items; 
	}


	@Transactional
	public SchedulerApplication findAplicacionByAplic(String aplic) throws SchedulerConfigurationException {
		try {
			SchedulerApplication found = (SchedulerApplication)this.getSessionFactory().getCurrentSession()
				.createQuery("from SchedulerApplication where aplic= :aplic ")
				.setString("aplic", aplic)
				.setTimeout(TIMEOUT)
				.uniqueResult();
			return found;
		} catch (NonUniqueResultException e) {
			throw new SchedulerConfigurationException(String.format("Hay más de una aplicación con nombre %s",aplic));
		}
	}

	@Transactional
	public SchedulerAppParameters findParametroAplic(String aplic) throws SchedulerConfigurationException {
		try {
		SchedulerAppParameters item = (SchedulerAppParameters) this.getSessionFactory().getCurrentSession()
				.createQuery("from SchedulerAppParameters where aplic= :aplic ")
				.setString("aplic", aplic)
				.setTimeout(TIMEOUT)
				.uniqueResult();
		return item;
		} catch (NonUniqueResultException e) {
			throw new SchedulerConfigurationException(String.format("Hay más de una aplicación con nombre %s",aplic));
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.springframework.orm.hibernate3.support.HibernateDaoSupport#
	 * createHibernateTemplate(org.hibernate.SessionFactory)
	 */
	@Override
	protected HibernateTemplate createHibernateTemplate(
			SessionFactory sessionFactory) {
		HibernateTemplate hibernateTemplate = super
				.createHibernateTemplate(sessionFactory);
		hibernateTemplate.setFlushMode(10);  // Esto de 10 de dónde salió?
		return hibernateTemplate;
	}

	@Transactional(readOnly = false)
	public void delete(Collection entitys) {
		getHibernateTemplate().deleteAll(entitys);
	}

	@Transactional(readOnly = false)
	public void delete(Object entity) {
		getHibernateTemplate().delete(entity);
	}

	// @Transactional(readOnly = true)
	public <T> List<T> find(Class<T> entityClass) {
		final List<T> entities = getHibernateTemplate().loadAll(entityClass);
		return entities;
	}

	// @Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public <T> List<T> find(String hql) {
		final List<T> entities = getHibernateTemplate().find(hql);
		return entities;
	}

	//@Transactional(readOnly = true)
	public <T> T load(Class<T> entityClass, Serializable id) {
		final T entity = getHibernateTemplate().load(entityClass, id);
		return entity;
	}

	@Transactional(readOnly = false)
	public void persist(Object entity) {
		getHibernateTemplate().saveOrUpdate(entity);
	}

	@Transactional
	public void persist(Object[] entities) {
		for (Object entitie : entities) {
			persist(entitie);
		}
	}

	@Transactional(readOnly = true)
	public void refresh(Object entity) {
		getHibernateTemplate().refresh(entity);
	}

	@Transactional
	public void doWork(Work work) {
		this.getSessionFactory().getCurrentSession().doWork(work);
	}

}
