package com.qbe.scheduler;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

// Generated Aug 9, 2013 4:50:01 PM by Hibernate Tools 3.4.0.CR1

/**
 * 
 */
@Entity
@Table ( name = "SERVOSB_SCHEDULER")
public class SchedulerApplication implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4790594955116314760L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int ident;

	@Column(length=20)
	private String aplic;

	@Column(length=20)
	private String pasoinic;

	private Integer periodicidad;
	
	@Column(length=40)
	private String mail;

	@Column(length=1)
	private String enverror;

	public SchedulerApplication() {
	}

	public SchedulerApplication(int ident) {
		this.ident = ident;
	}

	public SchedulerApplication(int ident, String aplic, String pasoinic,
			Integer periodicidad, String mail, String enverror) {
		this.ident = ident;
		this.aplic = aplic;
		this.pasoinic = pasoinic;
		this.periodicidad = periodicidad;
		this.mail = mail;
		this.enverror = enverror;
	}

	public int getIdent() {
		return this.ident;
	}

	public void setIdent(int ident) {
		this.ident = ident;
	}

	public String getAplic() {
		return this.aplic;
	}

	public void setAplic(String aplic) {
		this.aplic = aplic;
	}

	public String getPasoinic() {
		return this.pasoinic;
	}

	public void setPasoinic(String pasoinic) {
		this.pasoinic = pasoinic;
	}

	public Integer getPeriodicidad() {
		return this.periodicidad;
	}

	public void setPeriodicidad(Integer periodicidad) {
		this.periodicidad = periodicidad;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getEnverror() {
		return this.enverror;
	}

	public void setEnverror(String enverror) {
		this.enverror = enverror;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SchedulerApplication))
			return false;
		SchedulerApplication castOther = (SchedulerApplication) other;

		return (this.getIdent() == castOther.getIdent())
				&& ((this.getAplic() == castOther.getAplic()) || (this
						.getAplic() != null && castOther.getAplic() != null && this
						.getAplic().equals(castOther.getAplic())))
				&& ((this.getPasoinic() == castOther.getPasoinic()) || (this
						.getPasoinic() != null
						&& castOther.getPasoinic() != null && this
						.getPasoinic().equals(castOther.getPasoinic())))
				&& ((this.getPeriodicidad() == castOther.getPeriodicidad()) || (this
						.getPeriodicidad() != null
						&& castOther.getPeriodicidad() != null && this
						.getPeriodicidad().equals(castOther.getPeriodicidad())))
				&& ((this.getMail() == castOther.getMail()) || (this.getMail() != null
						&& castOther.getMail() != null && this.getMail()
						.equals(castOther.getMail())))
				&& ((this.getEnverror() == castOther.getEnverror()) || (this
						.getEnverror() != null
						&& castOther.getEnverror() != null && this
						.getEnverror().equals(castOther.getEnverror())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getIdent();
		result = 37 * result
				+ (getAplic() == null ? 0 : this.getAplic().hashCode());
		result = 37 * result
				+ (getPasoinic() == null ? 0 : this.getPasoinic().hashCode());
		result = 37
				* result
				+ (getPeriodicidad() == null ? 0 : this.getPeriodicidad()
						.hashCode());
		result = 37 * result
				+ (getMail() == null ? 0 : this.getMail().hashCode());
		result = 37 * result
				+ (getEnverror() == null ? 0 : this.getEnverror().hashCode());
		return result;
	}

}
