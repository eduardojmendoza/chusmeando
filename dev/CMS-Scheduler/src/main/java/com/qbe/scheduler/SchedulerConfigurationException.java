package com.qbe.scheduler;

public class SchedulerConfigurationException extends Exception {

	public SchedulerConfigurationException() {
	}

	public SchedulerConfigurationException(String arg0) {
		super(arg0);
	}

	public SchedulerConfigurationException(Throwable arg0) {
		super(arg0);
	}

	public SchedulerConfigurationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
