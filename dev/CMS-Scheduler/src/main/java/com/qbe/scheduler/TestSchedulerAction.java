package com.qbe.scheduler;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Properties;

import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.framework.jaxb.Response;
import com.qbe.vbcompat.string.StringHolder;

public class TestSchedulerAction implements VBObjectClass {

	public TestSchedulerAction()  {
	}

	@Override
	public int IAction_Execute(String wvarRequest, StringHolder wvarResponse,
			String extra) {
		Properties props = System.getProperties();
		StringWriter sw = new StringWriter();
		try {
			props.store(sw, "Current");
		} catch (IOException e) {
			sw.append("No pude escribir las System.getProperties() por IOException (?) ");
		}
		wvarResponse.set(Response.marshaledResultadoTrue("OK", sw.toString()));
		return 0;
	}

}
