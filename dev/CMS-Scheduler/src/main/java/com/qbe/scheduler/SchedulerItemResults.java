package com.qbe.scheduler;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SchedulerItemResults")
public class SchedulerItemResults {

	@XmlElement
	private List<SchedulerItemResult> results = new ArrayList<SchedulerItemResult>();
	
	public SchedulerItemResults() {
	}
	
	public List<SchedulerItemResult> getResults() {
		return results;
	}

	public void add(SchedulerItemResult itemRes) {
		this.results.add(itemRes);
	}
	
	public String toReport() {
		StringBuffer sb = new StringBuffer();
		sb.append("Resultados: ");
		for (SchedulerItemResult result : results) {
			result.addReportTo(sb);
			sb.append("\n");
		}
		return sb.toString();
	}
}
