package com.qbe.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class InstallSchedulerDB {

	@Autowired
	private SchedulerDAO dao;
	
	public InstallSchedulerDB() {
	}
	
	public SchedulerDAO getDao() {
		return dao;
	}

	public void setDao(SchedulerDAO dao) {
		this.dao = dao;
	}

	@Transactional
	public void loadSchema() {
		this.loadData();
		this.createObjects();
	}

	@Transactional
	public void loadData() {
		SchedulerApplication fillApp = new SchedulerApplication();
		fillApp.setAplic(SchedulerConstants.NBWS_E_POLIZA_FILL_APP);
		fillApp.setEnverror("2");
		fillApp.setMail("ramiro@snoopconsulting.com");
//		app.setIdent(2);
		fillApp.setPasoinic("100");
		fillApp.setPeriodicidad(1);
		dao.persist(fillApp);
		
		SchedulerAppParameters fillParams = new SchedulerAppParameters();
		fillParams.setAplic(SchedulerConstants.NBWS_E_POLIZA_FILL_APP);
		fillParams.setPaso("100");
		fillParams.setIaction(SchedulerConstants.NBWS_A_FILL_SCHEDULER_ACTION_CODE);
//		fillParams.setNextok("999");
//		fillParams.setNexterr("999");
//		fillParams.setIntentos((short)1);
//		fillParams.setPeriodicidad((short)0);
		fillParams.setDias("LMXJVSD");
		fillParams.setDesde("00:00");
		fillParams.setHasta("23:00");
//		fillParams.setRetryinterval(null);
		dao.persist(fillParams);
		
		SchedulerAppParameters exeParams = new SchedulerAppParameters();
		exeParams.setAplic(SchedulerConstants.NBWS_E_POLIZA_EXE_APP);
		exeParams.setPaso("100");
		exeParams.setIaction(SchedulerConstants.NBWS_A_EXE_SCHEDULER_ACTION_CODE);
//		exeParams.setNextok("999");
//		exeParams.setNexterr("999");
//		exeParams.setIntentos((short)1);
//		exeParams.setPeriodicidad((short)0);
		exeParams.setDias("LMXJVSD");
		exeParams.setDesde("00:00");
		exeParams.setHasta("23:45");
//		exeParams.setRetryinterval(null);
		dao.persist(exeParams);
		
		InboxItem exeItem = new InboxItem();
//		exeItem.setIdent(1);
		exeItem.setAplic(SchedulerConstants.NBWS_E_POLIZA_EXE_APP);
		exeItem.setSolicitud(0);
		exeItem.setPaso("100");
		exeItem.setIntento((short)0);
		exeItem.setDatain("<CIAASCOD>0001</CIAASCOD><RAMOPCOD>AUS1</RAMOPCOD><POLIZANN>0</POLIZANN><POLIZSEC>1</POLIZSEC><CERTIPOL>0</CERTIPOL><CERTIANN>1</CERTIANN><CERTISEC>276864</CERTISEC><OPERAPOL>13</OPERAPOL><DOCUMTIP>01</DOCUMTIP><DOCUMDAT>12351984</DOCUMDAT>");
		exeItem.setDataex(null);
		exeItem.setTerminado(SchedulerConstants.NO_TERMINADO);
		exeItem.setEstFinal(null);
		exeItem.setFechaIni(Calendar.getInstance().getTime());
		exeItem.setFechaPaso(Calendar.getInstance().getTime());
		exeItem.setCodUsu(SchedulerConstants.USUARIO);
		exeItem.setRcod(1);
		exeItem.setRmsg(null);
		exeItem.setLock(SchedulerConstants.UNLOCKED);
		exeItem.setLockOwner(null);
		dao.persist(exeItem);
		
		InboxItem fillItem = new InboxItem();
//		fillItem.setIdent(2);
		fillItem.setAplic(SchedulerConstants.NBWS_E_POLIZA_FILL_APP);
		fillItem.setSolicitud(0);
		fillItem.setPaso("100");
		fillItem.setIntento((short)0);
		fillItem.setDatain("<INSCTRL>S</INSCTRL>");
		fillItem.setDataex(null);
		fillItem.setTerminado(SchedulerConstants.NO_TERMINADO);
		fillItem.setEstFinal(null);
		fillItem.setFechaIni(Calendar.getInstance().getTime());
		fillItem.setFechaPaso(Calendar.getInstance().getTime());
		fillItem.setCodUsu(SchedulerConstants.USUARIO);
		fillItem.setRcod(1);
		fillItem.setRmsg(null);
		fillItem.setLock(SchedulerConstants.UNLOCKED);
		fillItem.setLockOwner(null);
		dao.persist(fillItem);
		
		loadTestSchedulerData();
	}

	private void loadTestSchedulerData() {
		
		SchedulerAppParameters appParams = new SchedulerAppParameters();
		appParams.setAplic(SchedulerConstants.TESTER_APLIC);
		appParams.setPaso("100");
		appParams.setIaction(SchedulerConstants.TESTER_APLIC);
		appParams.setDias("LMXJVSD");
		appParams.setDesde("00:00");
		appParams.setHasta("23:59");
		dao.persist(appParams);

		SchedulerApplication schedulerParams = new SchedulerApplication();
		schedulerParams.setAplic(SchedulerConstants.TESTER_APLIC);
		schedulerParams.setEnverror("2");
		schedulerParams.setMail("ramiro@snoopconsulting.com");
		schedulerParams.setPasoinic("100");
		schedulerParams.setPeriodicidad(1);
		dao.persist(schedulerParams);
		
		InboxItem testSchedulerItem = new InboxItem();
		testSchedulerItem.setAplic(SchedulerConstants.TESTER_APLIC);
		testSchedulerItem.setDatain("<DATA>Zilch</DATA>");
		testSchedulerItem.setTerminado(SchedulerConstants.NO_TERMINADO);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyy");
        Date date = Calendar.getInstance().getTime();
		try {
			date = formatter.parse("04-06-2013");
		} catch (ParseException e) {
		}
		testSchedulerItem.setFechaIni(date);
		testSchedulerItem.setCodUsu(SchedulerConstants.TESTER_USER);
		testSchedulerItem.setLock(SchedulerConstants.UNLOCKED);
		dao.persist(testSchedulerItem);
	}

	@Transactional
	public void cleanTables() {
		dao.doWork(
			    new Work() {
			        public void execute(Connection con) throws SQLException 
			        { 
			    		String delete = 
			    				"delete FROM SERVOSB_BANDEJA";
			    		PreparedStatement spps = con.prepareStatement(delete);
			    		spps.executeUpdate();
			    		spps.close();

			    		delete = 
			    				"delete FROM SERVOSB_PARAMETROS";
			    		spps = con.prepareStatement(delete);
			    		spps.executeUpdate();
			    		spps.close();

			    		delete = 
			    				"delete FROM SERVOSB_SCHEDULER";
			    		spps = con.prepareStatement(delete);
			    		spps.executeUpdate();
			    		spps.close();
}
			    }
			);
		
	}
	
	@Transactional
	public void createObjects() {
		
		dao.doWork(
			    new Work() {
			        public void execute(Connection con) throws SQLException 
			        { 
			    		String createSP = 
			    				"CREATE PROCEDURE P_OSB_SA_BANDEJA_INSERT (\n" +
			    				"OUT RETURN_VALUE INT, \n" +
			    				"APLIC   VARCHAR(20),     	-- Identificador de aplicación  \n" +
			    				"SOLICITUD  INT,       	-- Numero de Solicitud  \n" +
			    				"PASO   VARCHAR(20),    	-- Paso que identifica la Tarea a ejecutar  \n" +
			    				"FECHAEXEC DATETIME,		-- Fecha de ejecución del job\n" +
			    				"DATAIN   VARCHAR(5000),    	-- Data para el xml de entrada al IAction  \n" +
			    				"COD_USU  VARCHAR(20)     	-- Usuario que inserto la tarea  \n" +
			    				")\n" +
			    				"   MODIFIES SQL DATA DYNAMIC RESULT SETS 1\n" +
			    				"BEGIN ATOMIC \n" +
			    				"INSERT  INTO SERVOSB_BANDEJA \n" +
			    				"(APLIC, SOLICITUD, PASO, INTENTO, DATAIN, DATAEX, TERMINADO, EST_FINAL, FECHA_INI, FECHA_PASO, COD_USU, LOCK) \n" +
			    				"VALUES \n" +
			    				"(APLIC, SOLICITUD, PASO, 0, DATAIN, '', 0, '', FECHAEXEC, FECHAEXEC, COD_USU,  0);  \n" +
			    				"SET RETURN_VALUE = IDENTITY();" +
			    				"   END";

			    		PreparedStatement spps = con.prepareStatement(createSP);
			    		spps.executeUpdate();
			    		spps.close();
			    		
			    		createSP = 
			    				"CREATE PROCEDURE P_NBWS_INSERT_ENVIOEPOLIZAS_LOG (\n" +
			    				"OUT RETURN_VALUE INT, \n" +
			    				"CODOP       VARCHAR(6),\n" +
			    				"DESCRIPCION VARCHAR(100),\n" +
			    				"CLIENTE     VARCHAR(70),\n" +
			    				"EMAIL       VARCHAR(50),\n" +
			    				"ESTADO      VARCHAR(3),\n" +
			    				"DOCUMTIP    NUMERIC(2),\n" +
			    				"DOCUMDAT    NUMERIC(11)\n" +
			    				")\n" +
			    				"   MODIFIES SQL DATA DYNAMIC RESULT SETS 1\n" +
			    				"BEGIN ATOMIC \n" +
			    				"SET RETURN_VALUE = 0;" +
			    				"   END";
			    				
			    		spps = con.prepareStatement(createSP);
			    		spps.executeUpdate();
			    		spps.close();

			    		String create = "CREATE USER SIS_SNCV1 PASSWORD RHVC7673";
			    		spps = con.prepareStatement(create);
			    		spps.executeUpdate();
			    		spps.close();

			    		String grant = 
			    				"GRANT EXECUTE ON PROCEDURE P_NBWS_INSERT_ENVIOEPOLIZAS_LOG TO PUBLIC";
			    		spps = con.prepareStatement(grant);
			    		spps.executeUpdate();
			    		spps.close();
			    		
			        }
			    }
			);
		dao.doWork(
			    new Work() {
			        public void execute(Connection con) throws SQLException 
			        { 
			    		String grant = 
			    				"GRANT EXECUTE ON PROCEDURE P_OSB_SA_BANDEJA_INSERT TO PUBLIC";
			    		PreparedStatement spps = con.prepareStatement(grant);
			    		spps.executeUpdate();
			    		spps.close();

	    				String create = "CREATE USER SIS_OVLBA PASSWORD OV45772";
			    		spps = con.prepareStatement(create);
			    		spps.executeUpdate();
			    		spps.close();

			        }
			    }
			);
		
	}
	
	public void shutdownData() {
		dao.doWork(
			    new Work() {
			        public void execute(Connection con) throws SQLException 
			        { 
//			    		String wipeAll = 
//			    				"DROP SCHEMA PUBLIC CASCADE";
			        	String wipeAll = "SHUTDOWN";
			    		PreparedStatement spps = con.prepareStatement(wipeAll);
			    		spps.executeUpdate();
			    		spps.close();
			        }
			    }
			);
	}


}
