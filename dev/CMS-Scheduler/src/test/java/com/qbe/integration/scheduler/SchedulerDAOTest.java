package com.qbe.integration.scheduler;

import static org.junit.Assert.*;

import java.awt.ItemSelectable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.hibernate.jdbc.Work;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.qbe.scheduler.InstallSchedulerDB;
import com.qbe.scheduler.SchedulerApplication;
import com.qbe.scheduler.SchedulerConstants;
import com.qbe.scheduler.SchedulerDAO;
import com.qbe.scheduler.InboxItem;
import com.qbe.scheduler.SchedulerAppParameters;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;

@Ignore
public class SchedulerDAOTest {

	private static final String SCHEDULER_DAO = "schedulerDAO";
	private ApplicationContext context;
	private SchedulerDAO dao;
	
	@BeforeClass
	public static void load() throws Exception {
		if ( CurrentProfile.getProfileName().equals("dev")) {
//			System.out.println("Profile es: " + CurrentProfile.getProfileName());
			SchedulerDAO dao = (SchedulerDAO) new ClassPathXmlApplicationContext(
					"classpath*:scheduler-beans.xml").getBean(SCHEDULER_DAO);
			InstallSchedulerDB installer = new InstallSchedulerDB();
			installer.setDao(dao);
			installer.loadSchema();
		}
	}

	public SchedulerDAOTest() {
		context = new ClassPathXmlApplicationContext(
				"classpath*:scheduler-beans.xml");
		dao = (SchedulerDAO) context.getBean(SCHEDULER_DAO);
	}

	@AfterClass
	public static void tearDown() throws Exception {
		if ( CurrentProfile.getProfileName().equals("dev")) {
			SchedulerDAO dao = (SchedulerDAO) new ClassPathXmlApplicationContext(
					"classpath*:scheduler-beans.xml").getBean(SCHEDULER_DAO);
			InstallSchedulerDB installer = new InstallSchedulerDB();
			installer.setDao(dao);
			installer.shutdownData();
		}
	}

	@Test
	public void testLoadApp() throws Exception {
		SchedulerApplication fillApp = dao.findAplicacionByAplic(SchedulerConstants.NBWS_E_POLIZA_FILL_APP);
		assertTrue(fillApp.getAplic().equals(SchedulerConstants.NBWS_E_POLIZA_FILL_APP));
	}

	@Test
	public void testLoadAppParams() throws Exception {
		SchedulerAppParameters fillAppParams = dao.findParametroAplic(SchedulerConstants.NBWS_E_POLIZA_FILL_APP);
		assertTrue(fillAppParams.getAplic().equals(SchedulerConstants.NBWS_E_POLIZA_FILL_APP));
		assertTrue(fillAppParams.getIaction().equals(SchedulerConstants.NBWS_A_FILL_SCHEDULER_ACTION_CODE));
	}

	@Test
	public void testLoadAppItems() throws Exception {
		List<InboxItem> items = dao.findItemsByAplicTerminadoUsu(SchedulerConstants.NBWS_E_POLIZA_EXE_APP, (short) 0, SchedulerConstants.USUARIO);
		assertNotNull(items);
		//Siempre hay
		assertTrue(items.size() > 0);
		InboxItem first = items.get(0);
		assertTrue(first.getAplic().equals(SchedulerConstants.NBWS_E_POLIZA_EXE_APP));
	}

	@Test
	public void testFindUnlockedItemsByAplicTerminadoUsu() throws Exception {
		//Usuarios posibles: "SCHEDULER", ITDEV. Ver SERVAUT_BANDEJA_FILL
		List<InboxItem> items = dao.findUnlockedItemsByAplicTerminadoUsu(SchedulerConstants.NBWS_E_POLIZA_FILL_APP, SchedulerConstants.NO_TERMINADO, SchedulerConstants.USUARIO);
		
		assertNotNull(items);
		//Siempre hay
		assertTrue(items.size() > 0);
		InboxItem first = items.get(0);
		assertTrue(first.getAplic().equals(SchedulerConstants.NBWS_E_POLIZA_FILL_APP));
	}
	/**
	 * >> 3) SQL SERVER 2000 DATA CONVERSION LIMITATIONS
>>
>> When using SQL Server 2000 with the JDBC driver, the following data
>> conversion limitations apply:
>>
>> - String data cannot be converted to an underlying money or
>> smallmoney column.
>> - String data longer than 4000 characters cannot be converted
>> to char or varchar underlying columns.
>> - String data more than 4000 characters cannot be converted to nchar or
>> nvarchar underlying columns if the sendStringParametersAsUnicode
>> connection string property is set to false, or the setAsciiStream
>> method is called.
>> - String data cannot be converted to text columns if the
>> sendStringParametersAsUnicode connection string property is set
>> to true. If you need to support string to text columns conversions,
>> set the sendStringParametersAsUnicode property to false.
>> 
	 * @throws Exception
	 */
	@Test
	public void testSP_BANDEJA_INSERT() throws Exception {
		dao.doWork(
			    new Work() {
			        public void execute(Connection con) throws SQLException
			        
			        { 
			        	try {
							String datain = StringUtils.repeat("X", 4999);
							String sp = "{ ? = call P_OSB_SA_BANDEJA_INSERT(?,?,?,?,?,?)}";
							if ( CurrentProfile.getProfileName().equals("dev")) {
								//HSQLDB no soporta return parameters
								sp = "{ call P_OSB_SA_BANDEJA_INSERT(?,?,?,?,?,?,?)}";
							}
							CallableStatement cs = con.prepareCall(sp);
							
							int paramIndex = 1;
							
			        	cs.registerOutParameter(paramIndex++, Types.INTEGER);
							
//	    				"(APLIC   VARCHAR(20),     	-- Identificador de aplicación  \n" +
							cs.setString(paramIndex++, "TEST_APP");
							
//	    				"SOLICITUD  INT,       	-- Numero de Solicitud  \n" +
							cs.setInt(paramIndex++, 0);
							
//	    				"PASO   VARCHAR(20),    	-- Paso que identifica la Tarea a ejecutar  \n" +
							cs.setString(paramIndex++, "PASO");
							
//	    				"FECHAEXEC DATETIME,		-- Fecha de ejecución del job\n" +
							cs.setDate(paramIndex++, null);
							
//	    				"DATAIN   VARCHAR(5000),    	-- Data para el xml de entrada al IAction  \n" +
							//Limitado porque el driver no soporta este largo, ver comentario del método
							cs.setString(paramIndex++, StringUtils.abbreviate(datain,4000));
							
//	    				"COD_USU  VARCHAR(20)     	-- Usuario que inserto la tarea  \n" +
							cs.setString(paramIndex++, "USUTESTXXX");
							
							boolean result = cs.execute();
						} catch (SQLException e) {
							e.printStackTrace();
							throw e;
						} catch (CurrentProfileException e) {
							e.printStackTrace();
							fail();
						}
			        }
			    }
			);

	}
	

}
