package com.qbe.integration.scheduler;

import static org.junit.Assert.*;

import java.awt.ItemSelectable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.hibernate.jdbc.Work;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.qbe.scheduler.InstallSchedulerDB;
import com.qbe.scheduler.SchedulerApplication;
import com.qbe.scheduler.SchedulerConstants;
import com.qbe.scheduler.SchedulerDAO;
import com.qbe.scheduler.InboxItem;
import com.qbe.scheduler.SchedulerAppParameters;
import com.qbe.services.common.CurrentProfile;

@Ignore //Habilitar para instalar los datos base
public class DBInstaller {

	private static final String SCHEDULER_DAO = "schedulerDAO";
	private ApplicationContext context;
	private SchedulerDAO dao;
	
	@BeforeClass
	public static void load() throws Exception {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"classpath*:scheduler-beans.xml");
		SchedulerDAO dao = (SchedulerDAO) context.getBean(SCHEDULER_DAO);
		InstallSchedulerDB installer = new InstallSchedulerDB();
		installer.setDao(dao);
		installer.cleanTables();
		installer.loadData();
	}

	public DBInstaller() {
		context = new ClassPathXmlApplicationContext(
				"classpath*:scheduler-beans.xml");
		dao = (SchedulerDAO) context.getBean(SCHEDULER_DAO);
	}

	@Test
	public void testLoadApp() throws Exception {
		SchedulerApplication fillApp = dao.findAplicacionByAplic(SchedulerConstants.NBWS_E_POLIZA_FILL_APP);
		assertTrue(fillApp.getAplic().equals(SchedulerConstants.NBWS_E_POLIZA_FILL_APP));
	}

	@Test
	public void testLoadAppParams() throws Exception {
		SchedulerAppParameters fillAppParams = dao.findParametroAplic(SchedulerConstants.NBWS_E_POLIZA_FILL_APP);
		assertTrue(fillAppParams.getAplic().equals(SchedulerConstants.NBWS_E_POLIZA_FILL_APP));
		assertTrue(fillAppParams.getIaction().equals(SchedulerConstants.NBWS_A_FILL_SCHEDULER_ACTION_CODE));
	}

	@Test
	public void testLoadAppItems() throws Exception {
		List<InboxItem> items = dao.findItemsByAplicTerminadoUsu(SchedulerConstants.NBWS_E_POLIZA_EXE_APP, (short) 0, SchedulerConstants.USUARIO);
		assertNotNull(items);
		//Siempre hay
		assertTrue(items.size() > 0);
		InboxItem first = items.get(0);
		assertTrue(first.getAplic().equals(SchedulerConstants.NBWS_E_POLIZA_EXE_APP));
		assertTrue(first.getPaso().equals("100"));
	}

	@Test
	public void testFindUnlockedItemsByAplicTerminadoUsu() throws Exception {
		//Usuarios posibles: "SCHEDULER", ITDEV. Ver SERVAUT_BANDEJA_FILL
		List<InboxItem> items = dao.findUnlockedItemsByAplicTerminadoUsu(SchedulerConstants.NBWS_E_POLIZA_FILL_APP, SchedulerConstants.NO_TERMINADO, SchedulerConstants.USUARIO);
		
		assertNotNull(items);
		//Siempre hay
		assertTrue(items.size() > 0);
		InboxItem first = items.get(0);
		assertTrue(first.getAplic().equals(SchedulerConstants.NBWS_E_POLIZA_FILL_APP));
		assertTrue(first.getPaso().equals("100"));
	}
	
}
