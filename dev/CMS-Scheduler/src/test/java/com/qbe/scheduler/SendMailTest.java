package com.qbe.scheduler;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;

public class SendMailTest {

    @Autowired
    private MailSender mailSender;

    @Autowired
    private SimpleMailMessage mailMessage;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Ignore
	public void testSendMail() throws Exception {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost("10.1.12.150");
		mailSender.setDefaultEncoding("UTF-8");
		
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setFrom("martin.cabrera@zurich.com.ar");
		mailMessage.setTo("martin.cabrera@zurich.com.ar");
		mailMessage.setCc("ramiro@snoopconsulting.com");
		mailMessage.setText("Prueba!");
		mailMessage.setSubject("Prueba desde el Scheduler");
		mailSender.send(mailMessage);
	}

}
