package com.qbe.scheduler;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.junit.Test;

public class SchedulerEngineLocalTest {

	@Test
	public void testReschedule() throws SchedulerConfigurationException {
		Calendar c = Calendar.getInstance();
		c.set(2014,5, 4, 11, 00, 00);
		InboxItem ii = new InboxItem(0, null, null, null, null, null, null, null, null, c.getTime(), null, null, null, null, null, null);
		SchedulerEngine se = new SchedulerEngine();
		SchedulerApplication app = new SchedulerApplication(0, "Aplic", null, 1, null, null);
		InboxItem ni = se.createRescheduledItem(ii, app);
		String nueva = DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE).format(ni.getFechaIni());
		assertEquals("05/06/14", nueva);
	}

}
