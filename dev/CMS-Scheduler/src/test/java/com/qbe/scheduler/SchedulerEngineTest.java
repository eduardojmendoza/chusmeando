package com.qbe.scheduler;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;

/**
 * Tester del scheduler engine.
 * 
 * El Engine envia mensajes via OSB, para correr los tests necesita un Weblogic/OSB que pueda atenderlos
 * 
 * @author ramiro
 *
 */
@Ignore
public class SchedulerEngineTest {
	
	private static Logger logger = Logger.getLogger(SchedulerEngineTest.class.getName());

	private static ApplicationContext context;
	
	@BeforeClass
	public static void load() throws Exception {
		if ( CurrentProfile.getProfileName().equals("dev")) {
//			System.out.println("Profile es: " + CurrentProfile.getProfileName());
			InstallSchedulerDB installer = getContext().getBean(InstallSchedulerDB.class);
			installer.loadSchema();
		}
	}

	public SchedulerEngineTest() {
	}

	@AfterClass
	public static void tearDown() throws Exception {
		if ( CurrentProfile.getProfileName().equals("dev")) {
			InstallSchedulerDB installer = getContext().getBean(InstallSchedulerDB.class);
			installer.shutdownData();
		}
	}

	@Test
	@Ignore
	public void testRunAplic() throws SchedulerConfigurationException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		SchedulerEngine eng = getSchedulerEngine();
		String res = eng.processUserInbox(SchedulerConstants.USUARIO);
//		System.out.println(res);
	}

	@Test
	public void testGetCalendarWithNewHour() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		SchedulerEngine eng = getSchedulerEngine();
		Calendar ahora = Calendar.getInstance();
		Calendar nuevo = eng.getCalendarWithNewHour("09:45", ahora);
		assertTrue(nuevo.get(Calendar.HOUR) == 9);
		assertTrue(nuevo.get(Calendar.MINUTE) == 45);
	}
	
	@Test
	@Ignore
	public void testReschedule() throws SchedulerConfigurationException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		List<InboxItem> items = getSchedulerDAO().findUnlockedItemsByAplicTerminadoUsu(SchedulerConstants.NBWS_E_POLIZA_FILL_APP, SchedulerConstants.NO_TERMINADO, SchedulerConstants.USUARIO);
		int origSize = items.size(); 
		assertTrue("Encontró cant de items " + origSize + " <> 1, espero uno de " + SchedulerConstants.NBWS_E_POLIZA_FILL_APP, origSize == 1);
		SchedulerEngine eng = getSchedulerEngine();
		InboxItem nuevo = eng.reschedule(items.get(0));
		items = getSchedulerDAO().findUnlockedItemsByAplicTerminadoUsu(SchedulerConstants.NBWS_E_POLIZA_FILL_APP, SchedulerConstants.NO_TERMINADO, SchedulerConstants.USUARIO);
		int newSize = items.size();
		assertTrue(newSize == origSize + 1);
		getSchedulerDAO().delete(nuevo);
		
	}

	@Test
	public void testRescheduleSINI_PENDIENTES() throws SchedulerConfigurationException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		List<InboxItem> items = getSchedulerDAO().findUnlockedItemsByAplicTerminadoUsu(SchedulerConstants.SINI_PENDIENTES_APP, SchedulerConstants.NO_TERMINADO, SchedulerConstants.USUARIO);
		int origSize = items.size();
		if ( origSize == 0 ) {
			return;
		}
		logger.log(Level.FINE, "Actual: " + items.get(0).toString());
		SchedulerEngine eng = getSchedulerEngine();
		InboxItem nuevo = eng.reschedule(items.get(0));
		logger.log(Level.FINE, "nuevo: " + items.get(0).toString());
		items = getSchedulerDAO().findUnlockedItemsByAplicTerminadoUsu(SchedulerConstants.SINI_PENDIENTES_APP, SchedulerConstants.NO_TERMINADO, SchedulerConstants.USUARIO);
		int newSize = items.size();
		assertTrue(newSize == origSize + 1);
		getSchedulerDAO().delete(nuevo);
	}


	//Hack: vuela los registros viejos ( > 2 días ) de SINI_PENDIENTES
	@Test
	public void testCleanOldSINI_PENDIENTES() throws SchedulerConfigurationException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		List<InboxItem> items = getSchedulerDAO().findUnlockedItemsByAplicTerminadoUsu(SchedulerConstants.SINI_PENDIENTES_APP, SchedulerConstants.NO_TERMINADO, SchedulerConstants.USUARIO);
		int origSize = items.size(); 
		if ( origSize == 0 ) {
			return;
		}
		logger.log(Level.FINE, "# registros encontrados: " + origSize);
		//Hace 2 días
		Calendar hace2Dias = Calendar.getInstance();
		hace2Dias.add(Calendar.DAY_OF_MONTH, -2);

		for (InboxItem inboxItem : items) {
			if ( inboxItem.getFechaIni().before(hace2Dias.getTime())) {
				getSchedulerDAO().delete(inboxItem);
			}
		}
		
	}

	
	
	@Test
	@Ignore
	public void testTestSchedulerAction() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		SchedulerEngine eng = getSchedulerEngine();
		String result = eng.processUserInbox(SchedulerConstants.TESTER_USER);
		assertTrue("No encontré una property", StringUtils.contains(result, "os.name"));
		//Tiene que haber dejado otro programado para mañana
		List<InboxItem> items = getSchedulerDAO().findPendingItemsForUser(SchedulerConstants.TESTER_USER);
		assertTrue("Encontró un item pendiente cuando debería haber procesado todos",items.size() == 0);
		
		List<InboxItem> futureItems = getSchedulerDAO().findUnlockedItemsByAplicTerminadoUsu(SchedulerConstants.TESTER_APLIC, SchedulerConstants.NO_TERMINADO, SchedulerConstants.TESTER_USER);
		assertTrue("No encontró un item programado",futureItems.size() == 1);
	}

	private SchedulerEngine getSchedulerEngine() {
		SchedulerEngine eng = (SchedulerEngine)getContext().getBean(SchedulerEngine.class);
		return eng;
	}

	private SchedulerDAO getSchedulerDAO() {
		return (SchedulerDAO)getContext().getBean(SchedulerDAO.class);
	}
	
	private static ApplicationContext getContext() {
		if ( context == null) {
			context = new ClassPathXmlApplicationContext(
					"classpath*:scheduler-beans.xml");
		}
		return context;
	}
	
	
}
