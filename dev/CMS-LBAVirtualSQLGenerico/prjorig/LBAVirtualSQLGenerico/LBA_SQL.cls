VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "OVLBASQLGen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'******************************************************************************
'COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
'LIMITED 2012. ALL RIGHTS RESERVED

'This software is only to be used for the purpose for which it has been provided.
'No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
'system or translated in any human or computer language in any way or for any other
'purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
'Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
'offence, which can result in heavy fines and payment of substantial damages
'******************************************************************************
'Nombre del Modulo: OVLBASQLGen
'Fecha de Creaci�n: 04/01/2012
'PPcR: 2011-00390
'Desarrollador: Leonardo Ruiz
'Descripci�n:  Componente Generico de SQL, version 2
'******************************************************************************

'LR 14/01/2009 Se agrega un campo Opcional "CANT_REG_RESP" q devuelve una X cantidad de
'registros para mostrar
Implements ObjectControl
Implements HSBCInterfaces.IAction

Const mcteClassName             As String = "LBAA_SQLGenerico.OVLBASQLGen"
Const mcteSubDirName            As String = "DEFINICIONES"
Const mcteLogPath               As String = "LogSQL"
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog
Private mvarConsultaRealizada   As String
Private mvarStrCmdExec          As String

'XML con constantes de ADO para tipos de dato
Dim wobjCnstADO As MSXML2.DOMDocument


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName                As String = "IAction_Execute"
    Dim wvarStep                    As Integer
        
    Dim wobjXMLDefinition           As MSXML2.DOMDocument 'XML de definici�n
    Dim wobjXMLRequest              As MSXML2.DOMDocument 'XML con el request
    Dim wobjXMLRecordsets           As MSXML2.IXMLDOMNodeList 'Nodos descriptores de recordsets
    Dim wobjNodoRec                 As MSXML2.IXMLDOMNode 'Nodo descriptor de un recordset
    
    Dim wvarDefinitionFile          As String 'nombre del archivo XML de definici�n
    Dim wvarStoredProcedure         As String 'nombre del stored procedure
    Dim wvarTipoSP                  As String 'tipo de store procedure. C (consulta) o ABM
    Dim wvarUDL                     As String 'nombre del archivo UDL
    Dim wvarCANT_REG_RESP           As Integer 'LR 14/01/2009 campo Opcional q devuelve una X cantidad de registros para mostrar
    Dim wobjSalida                  As String 'String de salida
        
    Dim wobjHSBC_DBCnn              As HSBCInterfaces.IDBConnection 'interface de conexi�n a la base de datos
    Dim wobjDBCnn                   As ADODB.Connection 'conexi�n a la base de datos
    Dim wobjDBCmd                   As ADODB.Command
    Dim wobjRecordset               As ADODB.Recordset 'objeto recordset
    
    Dim wvarPathLog                 As String
    Dim wvarFechaConsulta           As Date
    
    'Para LOGs
    Dim wvarCantLlamadasLogueadas   As Long
    Dim wvarCantLlamadasALoguear    As Long
    Dim wvarCounter                 As Long
        
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarFechaConsulta = Now
    '
    'carga del REQUEST
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    wobjXMLRequest.loadXML Request
    
    'Carga del XML de definici�n
    wvarStep = 20
    Set wobjXMLDefinition = CreateObject("MSXML2.DOMDocument")
    wvarDefinitionFile = wobjXMLRequest.selectSingleNode("//DEFINICION").Text
    mvarConsultaRealizada = Left(wvarDefinitionFile, Len(wvarDefinitionFile) - 4)
    wobjXMLDefinition.async = False
    wobjXMLDefinition.Load App.Path & "\" & mcteSubDirName & "\" & wvarDefinitionFile
    
    'Carga del nombre del SP
    wvarStep = 30
    wvarStoredProcedure = wobjXMLDefinition.selectSingleNode("//DEFINICION/NOMBRE").Text
    
    'Carga del tipo de SP
    wvarStep = 40
    wvarTipoSP = wobjXMLDefinition.selectSingleNode("//DEFINICION/TIPO").Text
    
    'Carga del UDL
    wvarStep = 50
    wvarUDL = wobjXMLDefinition.selectSingleNode("//DEFINICION/UDL").Text
    
    'LR 14/01/2009 Se agrega campo Opcional: CANT_REG_RESP
    wvarStep = 55
    If Not wobjXMLRequest.selectSingleNode("//Request/CANT_REG_RESP") Is Nothing Then
        'Compruebo q venga un nro o sino le muevo un cero
        If IsNumeric(wobjXMLRequest.selectSingleNode("//CANT_REG_RESP").Text) Then
            wvarCANT_REG_RESP = wobjXMLRequest.selectSingleNode("//CANT_REG_RESP").Text
        Else
            wvarCANT_REG_RESP = 0 'Cero significa q no va tomar en cuenta esta logica de cant de Reg devueltos
        End If
    Else
        wvarCANT_REG_RESP = 0 'Cero significa q no va tomar en cuenta esta logica de cant de Reg devueltos
    End If
    
    'selecci�n del tipo de interface de conexi�n dependiendo de si el SP hace altas, modificaciones o bajas o no
    wvarStep = 60
    If wvarTipoSP = "ABM" Then
        Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Else
        Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    End If
    
    'conexi�n a la base de datos
    wvarStep = 70
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(wvarUDL)
    
    
    ''''''''''''''''''''''''''''
    'armado de la llamada al SP'
    ''''''''''''''''''''''''''''

    wvarStep = 80
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd = generarCommand(wobjDBCnn, wvarStoredProcedure, wobjXMLDefinition.selectNodes("//ENTRADA/PARAMETRO"), wobjXMLRequest.selectSingleNode("Request"))
    
    
    ''''''''''''''''''
    'ejecuci�n del SP'
    ''''''''''''''''''
    wvarStep = 90
    Set wobjRecordset = wobjDBCmd.Execute
   
    
    
    '''''''''''''''''''''
    'armado de la salida'
    '''''''''''''''''''''
    
    'Carga del nodos descriptores de recordsets
    wvarStep = 100
    Set wobjXMLRecordsets = wobjXMLDefinition.selectNodes("//SALIDA/RECORDSET")

    wvarStep = 110
    wobjSalida = ""
    
    'LR 14/01/2009 Se agrega contador para la cant de Reg devueltos
    mvarCount = 0
    
    'por cada recordset definido en el XML de definici�n procesa un recordset devuelto por la consulta
    For Each wobjNodoRec In wobjXMLRecordsets
        'si hay menos recordsets que los indicados en la definici�n tira un error
        If wobjRecordset Is Nothing Then
            Err.Raise 2504, , "ERROR EN XML DE DEFINICION - se definieron m�s recordsets que la cantidad devuelta por el stored procedure"
        End If
        
        'LR 14/01/2009 le mando el param con wvarCANT_REG_RESP
        wobjSalida = wobjSalida & generarXMLSalida(wobjRecordset, wobjNodoRec, wvarCANT_REG_RESP)
        'wobjSalida = wobjSalida & generarXMLSalida(wobjRecordset, wobjNodoRec)
        Set wobjRecordset = wobjRecordset.NextRecordset
    Next
    
    wvarStep = 120
    If Not wobjXMLDefinition.selectSingleNode("//RETORNARREQUEST") Is Nothing Then
        wobjSalida = wobjSalida & Request
    End If
    
    wvarStep = 130
    Response = etiquetar("Response", wobjSalida)
    
    '****************************************************************
    'Generamos un Log en caso que se solicite
    '****************************************************************
    '
    If (Not wobjXMLRequest.selectSingleNode("//GENERARLOG") Is Nothing) Or (Not wobjXMLDefinition.selectSingleNode("//GENERARLOG") Is Nothing) Then
        '
        wvarStep = 135
        wvarCantLlamadasALoguear = 1
        If Not wobjXMLDefinition.selectSingleNode("//GENERARLOG").Attributes.getNamedItem("CantLlamadasALoguear") Is Nothing Then wvarCantLlamadasALoguear = wobjXMLDefinition.selectSingleNode("//GENERARLOG").Attributes.getNamedItem("CantLlamadasALoguear").Text
        '
        wvarStep = 140
        wvarPathLog = App.Path & "\" & mcteLogPath & "\" & wvarDefinitionFile
        '
        wvarStep = 150
        If (Not wobjXMLDefinition.selectSingleNode("//GENERARLOG") Is Nothing) Then
            If Not wobjXMLDefinition.selectSingleNode("//GENERARLOG").Attributes.getNamedItem("LogFile") Is Nothing Then wvarPathLog = App.Path & "\" & mcteLogPath & "\" & wobjXMLDefinition.selectSingleNode("//GENERARLOG").Attributes.getNamedItem("LogFile").Text
        Else
            If Not wobjXMLRequest.selectSingleNode("//GENERARLOG").Attributes.getNamedItem("LogFile") Is Nothing Then wvarPathLog = App.Path & "\" & mcteLogPath & "\" & wobjXMLRequest.selectSingleNode("//GENERARLOG").Attributes.getNamedItem("LogFile").Text
        End If
        '
        wvarStep = 160
        If Not wvarPathLog Like "*\" Then
        
            'Genero el Log solo si en el nodo se especifico un nombre de archivo
            Set wobjXMLLog = CreateObject("MSXML2.DOMDocument")
            wobjXMLLog.async = False
            '
            wvarStep = 170
            wobjXMLLog.loadXML "<LOGs><LOG StoredProcedure='" & wvarStoredProcedure & "' InicioConsulta='" & Format(wvarFechaConsulta, "dd/mm/yyyy hh:mm:ss") & "' TiempoIncurrido = '" & CLng((Now - wvarFechaConsulta) * 86400) & " Seg' >" & Request & Response & "</LOG></LOGs>"
            '
            wvarStep = 180
            '
            If Dir(wvarPathLog) <> "" Then
                wvarStep = 190
                'Ya existe el Log, entonces agrego la nueva consulta al LOG
                Set wobjXMLOldLog = CreateObject("MSXML2.DOMDocument")
                wobjXMLOldLog.async = False
                wobjXMLOldLog.Load wvarPathLog
                wvarStep = 200
                If wobjXMLOldLog.selectSingleNode("//LOGs") Is Nothing Then
                    '*********LOG NO VALIDO***********
                    wobjXMLLog.Save wvarPathLog
                Else
'''''                    wvarStep = 210
'''''                    wobjXMLOldLog.selectSingleNode("//LOGs").appendChild wobjXMLLog.selectSingleNode("//LOGs").childNodes(0)
'''''                    wvarStep = 220
'''''                    wobjXMLOldLog.Save wvarPathLog
                    '
                    wvarStep = 210 'DA
                    wvarCantLlamadasLogueadas = wobjXMLOldLog.selectSingleNode("//LOGs").childNodes.length
                    For wvarCounter = 1 To wvarCantLlamadasLogueadas - wvarCantLlamadasALoguear + 1
                        wobjXMLOldLog.selectSingleNode("//LOGs").removeChild wobjXMLOldLog.selectSingleNode("//LOGs").childNodes(0)
                    Next wvarCounter
                    '
                    wvarStep = 220 'DA
                    wobjXMLOldLog.selectSingleNode("//LOGs").appendChild wobjXMLLog.selectSingleNode("//LOGs").childNodes(0)
                    wvarStep = 225 'DA
                    wobjXMLOldLog.Save wvarPathLog
                    '
                End If
            Else
                wvarStep = 230
                wobjXMLLog.Save wvarPathLog
            End If
        End If
    End If

    '****************************************************************
    'FIN generaci�n de LOG
    '****************************************************************
    
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName & "--" & mvarConsultaRealizada, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjRecordset = Nothing
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function
Private Function generarCommand(pobjDBCnn As ADODB.Connection, pobjStoredProcedure As String, pobjNodos As IXMLDOMNodeList, pobjXMLRequest As IXMLDOMNode) As ADODB.Command
    Const wcteFnName                As String = "generarCommand"
    Dim wvarStep                    As Integer
    
    Dim wobjCommand                 As ADODB.Command 'Command a crear
    Dim wobjParameter               As ADODB.Parameter 'parametro
    Dim wobjNodeCampo               As IXMLDOMNode ' Nodo con campo de entrada
    Dim wvarNombrePrm               As String 'Nombre de un par�metro
    Dim wvarTipoPrm                 As Integer 'Tipo de un par�metro
    Dim wvarValuePrm                As String
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandlergenerarCommand
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    wvarStep = 10
    
    Set wobjCommand = CreateObject("ADODB.Command")
    Set wobjCommand.ActiveConnection = pobjDBCnn

    wobjCommand.CommandText = pobjStoredProcedure
    wobjCommand.CommandType = adCmdStoredProc
    
    'Se almacena la cadena de ejecucion
    mvarStrCmdExec = pobjStoredProcedure & Space(1)
    
    'por cada campo de entrada se crea un par�metro y se agrega al Command
    For Each wobjNodeCampo In pobjNodos
        
        wvarStep = 20
        wvarNombrePrm = wobjNodeCampo.Attributes.getNamedItem("Nombre").Text
        
        wvarStep = 30
        wvarTipoPrm = CInt(wobjCnstADO.selectSingleNode("//DATATYPE[@name=""" & wobjNodeCampo.Attributes.getNamedItem("Type").Text & """]").Text)
        Set wobjParameter = wobjCommand.CreateParameter
        wobjParameter.Name = "@" & wvarNombrePrm
        wobjParameter.Type = wvarTipoPrm
        wobjParameter.Direction = adParamInput
        
        wvarStep = 40
        
        If Not (wobjNodeCampo.Attributes.getNamedItem("Xml") Is Nothing) Then
            wvarValuePrm = pobjXMLRequest.selectSingleNode(wvarNombrePrm).xml
        Else
            If pobjXMLRequest.selectSingleNode(wvarNombrePrm) Is Nothing Then
                wvarValuePrm = wobjNodeCampo.Attributes.getNamedItem("Default").Text
            Else
                wvarValuePrm = pobjXMLRequest.selectSingleNode(wvarNombrePrm).Text
            End If
        End If
            
        'Actual manera de agregar par�metros
        Select Case wvarTipoPrm
            Case 129, 200, 201
                'adodb.adVarChar adodb.adChar adLongVarChar(text)
                mvarStrCmdExec = mvarStrCmdExec & "'" & wvarValuePrm & "',"
            Case Else
                mvarStrCmdExec = mvarStrCmdExec & wvarValuePrm & ","
        End Select
         
        'los tipos de dato num�rico y decimal (131 y 14) requieren determinar
        'Precision y NumericScale. Los demas requieres determinar Size
        If wvarTipoPrm = ADODB.adNumeric Or wvarTipoPrm = ADODB.adDecimal Then
            wvarStep = 50
            wobjParameter.Precision = wobjNodeCampo.Attributes.getNamedItem("Precision").Text
            
            wvarStep = 60
            wobjParameter.NumericScale = wobjNodeCampo.Attributes.getNamedItem("Scale").Text
            
        Else
            
            If wvarTipoPrm = ADODB.adLongVarChar Then 'Tipo text
                wvarStep = 70
                wobjParameter.Size = Len(wvarValuePrm)
            Else
                wvarStep = 75
                wobjParameter.Size = wobjNodeCampo.Attributes.getNamedItem("Size").Text
            End If
            
        End If
        
        If wvarTipoPrm = ADODB.adLongVarChar Then 'Tipo text
            wvarStep = 80
            wobjParameter.AppendChunk (wvarValuePrm)
        Else
            wvarStep = 85
            wobjParameter.Value = wvarValuePrm
        End If
        
        wvarStep = 90
        wobjCommand.Parameters.Append wobjParameter
    Next
    
    
    'Se elimina el ultimo caracter(coma) en la cadena de ejecucion
    wvarStep = 100
    mvarStrCmdExec = Left(mvarStrCmdExec, Len(mvarStrCmdExec) - 1)
    
    Set generarCommand = wobjCommand
    
    Exit Function
    
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ErrorHandlergenerarCommand:
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName & "--" & mvarConsultaRealizada, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Parametro: " & wvarNombrePrm, _
                     vbLogEventTypeError
    
    Set wobjCommand = Nothing
    
    Set generarCommand = Nothing
    mobjCOM_Context.SetAbort
End Function
Private Function generarXMLSalida(pobjRecordset As ADODB.Recordset, pobjNodo As MSXML2.IXMLDOMNode, wvarCANT_REG_RESP As Integer) As String
    Const wcteFnName                As String = "generarXMLSalida"
    Dim wvarStep                    As Integer
    
    Dim wobjNodosCampos             As MSXML2.IXMLDOMNodeList
    Dim wobjNodoCampo               As MSXML2.IXMLDOMNode
    
    Dim wobjNomRecordset            As String 'Etiqueta de salida para un recordset
    Dim wobjNomFila                 As String 'Etiqueta de salida para una fila
    Dim wobjNomCampo                As String 'Etiqueta de salida para un campo
    
    Dim wobjCampo                   As Field 'campo de un recordset
    Dim wobjAux                     As String 'almacena temporalmente la salida
    Dim wobjAuxRegs                 As MSXML2.DOMDocument 'almacena temporalmente la salida // REGISTROS
    Dim wvari                       As Integer 'contador
    Dim mvarCount                   As Integer 'contador
    Dim wobjNodo                    As MSXML2.DOMDocument
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandlerXMLSalida
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    wobjAux = ""
    'LR 14/01/2009 Se agrega contador para la cant de Reg devueltos
    mvarCount = 0
       
    wobjNomRecordset = pobjNodo.Attributes.getNamedItem("Nombre").Text
    wobjNomFila = pobjNodo.Attributes.getNamedItem("NombreFila").Text
        
    Set wobjNodosCampos = pobjNodo.selectNodes("//RECORDSET[@Nombre=""" & wobjNomRecordset & """]/CAMPO")
    
    wvarStep = 10
    
    '02/11/2011 LR Crear obj para acumular
    Set wobjAuxRegs = CreateObject("MSXML2.DOMDocument")
        wobjAuxRegs.async = False
    wobjAuxRegs.loadXML "<" & wobjNomRecordset & "/>"
    
    Set wobjNodo = CreateObject("MSXML2.DOMDocument")
        wobjNodo.async = False
    
    'procesa cada registro del recordset
    Do While Not pobjRecordset.EOF
        'si no coincide la cantidad de campos del recordset con la cantidad de campos definidas en el XML tira error
        If wobjNodosCampos.length <> pobjRecordset.Fields.Count Then
           Err.Raise 2504, , "ERROR EN XML DE DEFINICION - No coincide la cantidad de campos devueltos por el stored procedure (" & pobjRecordset.Fields.Count & " campos) con la cantidad de campos definidos en el XML (" & wobjNodosCampos.length & " campos)"
        End If
        
        wvarStep = 20
        wvari = 0
        'procesa cada campo del registro
        For Each wobjCampo In pobjRecordset.Fields
            Set wobjNodoCampo = wobjNodosCampos.Item(wvari)
            'si debe mostrarse el campo, muestra su valor con las etiquetas indicadas
            If wobjNodoCampo.Attributes.getNamedItem("OCULTO") Is Nothing Then
                If wobjNodoCampo.Attributes.getNamedItem("CDATA") Is Nothing Then
                    wobjAux = wobjAux & etiquetar(wobjNodoCampo.Attributes.getNamedItem("Nombre").Text, wobjCampo.Value)
                Else
                    wobjAux = wobjAux & etiquetar(wobjNodoCampo.Attributes.getNamedItem("Nombre").Text, "<![CDATA[" & wobjCampo.Value & "]]>")
                End If
            End If
            wvari = wvari + 1
        Next
        
        wvarStep = 30
        'wobjAuxRegs = wobjAuxRegs + etiquetar(wobjNomFila, wobjAux)
        wobjNodo.loadXML "<" & wobjNomFila & ">" & wobjAux & "</" & wobjNomFila & ">"
        
        wvarStep = 35
        Call wobjAuxRegs.selectSingleNode("//" & wobjNomRecordset).appendChild(wobjNodo.selectSingleNode("//" & wobjNomFila))
        
        wvarStep = 36
        wobjAux = ""
        
        'LR 14/01/2009 Se agrega logica para salir de For si se ingresa un campo de cant de limitacion en la resp de Reg
        If wvarCANT_REG_RESP <> 0 Then
            mvarCount = mvarCount + 1
            If wvarCANT_REG_RESP <= mvarCount Then
                Exit Do   ' Salir del loop hasta esta cant de reg.
            End If
        End If
        
        pobjRecordset.MoveNext
    Loop
    
    wvarStep = 40
    
    generarXMLSalida = "<Estado resultado=""true""/>" + wobjAuxRegs.xml
    
    Exit Function
    
'~~~~~~~~~~~~~~~~~~~~~
ErrorHandlerXMLSalida:
'~~~~~~~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName & "--" & mvarConsultaRealizada, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjAuxRegs = Nothing
    Set wobjNodo = Nothing
    Set pobjRecordset = Nothing
    Set wobjCampo = Nothing
    generarXMLSalida = "<Response><Estado resultado='false'></Response>"
    mobjCOM_Context.SetAbort
End Function

Private Function etiquetar(pobjEtiqueta As String, pvarContenido As String) As String
    Const wcteFnName                As String = "etiquetar"
    Dim wvarStep                    As Integer
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandlerEtiquetar
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    wvarStep = 10
    etiquetar = "<" & pobjEtiqueta & ">" & pvarContenido & "</" & pobjEtiqueta & ">"
    
    Exit Function
    
'~~~~~~~~~~~~~~~~~~~~~
ErrorHandlerEtiquetar:
'~~~~~~~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName & "--" & mvarConsultaRealizada, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
   
    etiquetar = 1
    mobjCOM_Context.SetAbort
End Function
    
Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    Set wobjCnstADO = CreateObject("MSXML2.DOMDocument")
    wobjCnstADO.async = False
    wobjCnstADO.Load App.Path & "\constantesADO.xml"
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub
