package com.qbe.services.lbahogarmq.impl;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.connector.mq.MQProxy;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Strings;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetPolCob implements VBObjectClass
{
	
	  protected static Logger logger = Logger.getLogger(lbaw_GetPolCob.class.getName());
	
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorHgMQ.lbaw_GetPolCob";
  static final String mcteOpID = "0023";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_MSGCOD = "//MSGCOD";
  static final String mcteParam_CIAASCOD = "//CIAASCOD";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_PLANNCOD = "//PLANNCOD";
  static final String mcteParam_EFECTANN = "//EFECTANN";
  static final String mcteParam_EFECTMES = "//EFECTMES";
  static final String mcteParam_EFECTDIA = "//EFECTDIA";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    String wvarMensaje = "";
    String wvarStep = "";
    String wvarResult = "";
    String wvarMSGCOD = "";
    String wvarCIAASCOD = "";
    String wvarRAMOPCOD = "";
    String wvarPOLIZANN = "";
    String wvarPOLIZSEC = "";
    String wvarPlanNCod = "";
    String wvarEFECTANN = "";
    String wvarEFECTMES = "";
    String wvarEFECTDIA = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = "10";
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarMSGCOD = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MSGCOD )  ), 4 );
      wvarCIAASCOD = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CIAASCOD )  ), 4 );
      wvarRAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  );
      wvarPOLIZANN = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN )  ), 2 );
      wvarPOLIZSEC = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC )  ), 6 );
      wvarPlanNCod = Strings.right( "000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PLANNCOD )  ), 3 );
      wvarEFECTANN = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EFECTANN )  ), 4 );
      wvarEFECTMES = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EFECTMES )  ), 2 );
      wvarEFECTDIA = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EFECTDIA )  ), 2 );
      //
      wobjXMLRequest = null;

      wvarStep = "20";
      wobjXMLParametros = null;
      //
      wvarStep = "30";
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      wvarMensaje = wvarMSGCOD;
      wvarMensaje = wvarMensaje + wvarCIAASCOD;
      wvarMensaje = wvarMensaje + wvarRAMOPCOD;
      wvarMensaje = wvarMensaje + wvarPOLIZANN;
      wvarMensaje = wvarMensaje + wvarPOLIZSEC;
      wvarMensaje = wvarMensaje + wvarPlanNCod;
      wvarMensaje = wvarMensaje + wvarEFECTANN;
      wvarMensaje = wvarMensaje + wvarEFECTMES;
      wvarMensaje = wvarMensaje + wvarEFECTDIA;

      wvarArea = wvarMensaje;
      MQProxy wobjFrame2MQ = null;
      wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);
      StringHolder sHstrParseString = new StringHolder(strParseString);
      try {
		wvarMQError = wobjFrame2MQ.executePrim( wvarArea, sHstrParseString);
	} catch ( MQProxyTimeoutException te) {
		logger.log(Level.SEVERE, "Timeout al enviar mensaje MQ via conector MQProxy.AISPROXY ( error " + wvarMQError + " )",te);
		throw new ComponentExecutionException(3, "Ocurrió un timeout al comunicarse con el cotizador AIS ( error " + wvarMQError + " )", te);
	} catch (MQProxyException e) {
		String msg = "Exception [ Msg:"+ e.getMessage() + ", causa:" + e.getCause() + "] al enviar mensaje: [" + wvarArea + "]";
		logger.log(Level.SEVERE, msg,e);
		throw new ComponentExecutionException(1, "Ocurrió un error al comunicarse con el cotizador AIS ( error " + wvarMQError + " )", e);
	}

	strParseString = sHstrParseString.getValue();

      //
      wvarStep = "150";
      wvarResult = "";
      wvarPos = 1;
      //
      strParseString = Strings.mid( strParseString, 28, Strings.len( strParseString ) - 31 );
      //
      if( (Strings.mid( strParseString, wvarPos, 2 ).equals( "ER" )) || (Strings.trim( Strings.mid( strParseString, wvarPos, 2 ) ).equals( "" )) )
      {
        // NO SE ENCONTRARON DATOS
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) );
        Response.set( Response + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" );
        Response.set( Response + "<COBERTURAS><COBERTURA>" );
        Response.set( Response + "<COBERCOD>000</COBERCOD>" );
        Response.set( Response + "<COBERDES></COBERDES>" );
        Response.set( Response + "<COBERDAB></COBERDAB>" );
        Response.set( Response + "<COBERORD></COBERORD>" );
        Response.set( Response + "<CAPITASG></CAPITASG>" );
        Response.set( Response + "<SWOBLIGA></SWOBLIGA>" );
        Response.set( Response + "<COBEBCOD></COBEBCOD>" );
        Response.set( Response + "<COBETCOD></COBETCOD>" );
        Response.set( Response + "<CAMAXMOD></CAMAXMOD>" );
        Response.set( Response + "<CAMINMOD></CAMINMOD>" );
        Response.set( Response + "</COBERTURA></COBERTURAS></Response>" );
      }
      else
      {
        // ARMA LA RESPUESTA
        //
        strParseString = Strings.trim( Strings.mid( strParseString, 3, Strings.len( strParseString ) - 2 ) );
        //
        wvarResult = wvarResult + "<COBERTURAS>";
        while( (!Strings.trim( Strings.mid( strParseString, wvarPos, 3 ) ).equals( "000" )) && (!Strings.trim( Strings.mid( strParseString, (wvarPos + 4), 60 ) ).equals( "" )) )
        {
          wvarResult = wvarResult + "<COBERTURA>";
          wvarResult = wvarResult + "<COBERCOD>" + Strings.mid( strParseString, wvarPos, 3 ) + "</COBERCOD>";
          wvarResult = wvarResult + "<COBERDES>" + Strings.mid( strParseString, (wvarPos + 3), 60 ) + "</COBERDES>";
          wvarResult = wvarResult + "<COBERDAB>" + Strings.mid( strParseString, (wvarPos + 63), 20 ) + "</COBERDAB>";
          wvarResult = wvarResult + "<COBERORD>" + Strings.mid( strParseString, (wvarPos + 83), 2 ) + "</COBERORD>";
          wvarResult = wvarResult + "<CAPITASG>" + Strings.mid( strParseString, (wvarPos + 85), 15 ) + "</CAPITASG>";
          wvarResult = wvarResult + "<SWOBLIGA>" + Strings.mid( strParseString, (wvarPos + 100), 1 ) + "</SWOBLIGA>";
          wvarResult = wvarResult + "<COBEBCOD>" + Strings.mid( strParseString, (wvarPos + 101), 3 ) + "</COBEBCOD>";
          wvarResult = wvarResult + "<COBETCOD>" + Strings.mid( strParseString, (wvarPos + 104), 3 ) + "</COBETCOD>";
          wvarResult = wvarResult + "<CAMAXMOD>" + Strings.mid( strParseString, (wvarPos + 107), 2 ) + "</CAMAXMOD>";
          wvarResult = wvarResult + "<CAMINMOD>" + Strings.mid( strParseString, (wvarPos + 109), 2 ) + "</CAMINMOD>";
          wvarResult = wvarResult + "</COBERTURA>";
          //
          strParseString = Strings.mid( strParseString, 112, Strings.len( strParseString ) - 111 );
          wvarPos = 1;
        }
        wvarResult = wvarResult + "</COBERTURAS>";
        //
        wobjXMLResponse = new XmlDomExtended();
        wobjXSLResponse = new XmlDomExtended();
        wobjXMLResponse.loadXML( wvarResult );
        wobjXSLResponse.loadXML( p_GetXSL());
        wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );

        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );

      }
      //
      wvarStep = "160";
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
    }
    catch (ComponentExecutionException e) {
    	//La propagamos, ya contiene el detalle del error
		throw e;
	}
	catch( Exception _e_ )
	{
		logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
        throw new ComponentExecutionException(_e_);
      }
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //wvarStrXSL = "             <?xml version='1.0' encoding='UTF-8'?>"
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:fo='http://www.w3.org/1999/XSL/Format'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "<xsl:key name='cobertura' match='COBERTURA' use='COBEBCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:template match='/'>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='COBERTURAS'>";
    wvarStrXSL = wvarStrXSL + "      <xsl:for-each select=\"//COBERTURAS/COBERTURA[number(COBEBCOD) = 0]\">";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='COBERTURA'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COBERCOD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select=\"COBERCOD\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COBERDAB'>";
    wvarStrXSL = wvarStrXSL + "                  <xsl:value-of select=\"COBERDAB\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COBERORD'>";
    wvarStrXSL = wvarStrXSL + "                  <xsl:value-of select=\"COBERORD\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CAMINMOD'>";
    wvarStrXSL = wvarStrXSL + "                  <xsl:value-of select=\"CAMINMOD\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CAMAXMOD'>";
    wvarStrXSL = wvarStrXSL + "                  <xsl:value-of select=\"CAMAXMOD\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CAPITASG'>";
    wvarStrXSL = wvarStrXSL + "                  <xsl:value-of select=\"CAPITASG\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SWOBLIGA'>";
    wvarStrXSL = wvarStrXSL + "                  <xsl:value-of select=\"SWOBLIGA\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COBETCOD'>";
    wvarStrXSL = wvarStrXSL + "                  <xsl:value-of select=\"COBETCOD\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SECUNDARIAS'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:for-each select=\"key('cobertura', COBERCOD)\">";
    wvarStrXSL = wvarStrXSL + "                <xsl:element name='SECUNDARIA'>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_COBERCOD'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"COBERCOD\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_COBEBCOD'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"COBEBCOD\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_COBERDAB'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"COBERDAB\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_COBERORD'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"COBERORD\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_CAPITASG'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"CAPITASG\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_SWOBLIGA'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"SWOBLIGA\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_CAMINMOD'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"CAMINMOD\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_CAMAXMOD'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"CAMAXMOD\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_COBETCOD'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"COBETCOD\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             </xsl:for-each>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "      </xsl:for-each>";
    wvarStrXSL = wvarStrXSL + "      </xsl:element>";
    wvarStrXSL = wvarStrXSL + "</xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";

    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
