package com.qbe.services.lbahogarmq.impl;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.connector.mq.MQProxy;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Strings;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetPolPlanes implements VBObjectClass
{
	
	  protected static Logger logger = Logger.getLogger(lbaw_GetPolPlanes.class.getName());
	
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorHgMQ.lbaw_GetPolPlanes";
  static final String mcteOpID = "0021";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_MSGCOD = "//MSGCOD";
  static final String mcteParam_CIAASCOD = "//CIAASCOD";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_EFECTANN = "//EFECTANN";
  static final String mcteParam_EFECTMES = "//EFECTMES";
  static final String mcteParam_EFECTDIA = "//EFECTDIA";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    String wvarMensaje = "";
    String wvarStep = "";
    String wvarResult = "";
    String wvarMSGCOD = "";
    String wvarCIAASCOD = "";
    String wvarRAMOPCOD = "";
    String wvarPOLIZANN = "";
    String wvarPOLIZSEC = "";
    String wvarEFECTANN = "";
    String wvarEFECTMES = "";
    String wvarEFECTDIA = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = "10";
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarMSGCOD = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MSGCOD )  ), 4 );
      wvarCIAASCOD = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CIAASCOD )  ), 4 );
      wvarRAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  );
      wvarPOLIZANN = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN )  ), 2 );
      wvarPOLIZSEC = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC )  ), 6 );
      wvarEFECTANN = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EFECTANN )  ), 4 );
      wvarEFECTMES = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EFECTMES )  ), 2 );
      wvarEFECTDIA = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EFECTDIA )  ), 2 );
      //
      wobjXMLRequest = null;

      wvarStep = "30";
      wobjXMLParametros = null;
      //
      wvarStep = "40";
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      // wvarMensaje = mcteOpID
      wvarMensaje = wvarMSGCOD;
      wvarMensaje = wvarMensaje + wvarCIAASCOD;
      wvarMensaje = wvarMensaje + wvarRAMOPCOD;
      wvarMensaje = wvarMensaje + wvarPOLIZANN;
      wvarMensaje = wvarMensaje + wvarPOLIZSEC;
      wvarMensaje = wvarMensaje + wvarEFECTANN;
      wvarMensaje = wvarMensaje + wvarEFECTMES;
      wvarMensaje = wvarMensaje + wvarEFECTDIA;


      wvarArea = wvarMensaje;
      MQProxy wobjFrame2MQ = null;
      wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);
      StringHolder sHstrParseString = new StringHolder(strParseString);
      try {
		wvarMQError = wobjFrame2MQ.executePrim( wvarArea, sHstrParseString);
	} catch ( MQProxyTimeoutException te) {
		logger.log(Level.SEVERE, "Timeout al enviar mensaje MQ via conector MQProxy.AISPROXY ( error " + wvarMQError + " )",te);
		throw new ComponentExecutionException(3, "Ocurrió un timeout al comunicarse con el cotizador AIS ( error " + wvarMQError + " )", te);
	} catch (MQProxyException e) {
		String msg = "Exception [ Msg:"+ e.getMessage() + ", causa:" + e.getCause() + "] al enviar mensaje: [" + wvarArea + "]";
		logger.log(Level.SEVERE, msg,e);
		throw new ComponentExecutionException(1, "Ocurrió un error al comunicarse con el cotizador AIS ( error " + wvarMQError + " )", e);
	}

	strParseString = sHstrParseString.getValue();

	wvarStep = "160";
      wvarResult = "";
      wvarPos = 1;
      strParseString = Strings.mid( strParseString, 25, Strings.len( strParseString ) - 23 );


      if( (Strings.mid( strParseString, wvarPos, 2 ).equals( "ER" )) || (Strings.trim( Strings.mid( strParseString, wvarPos, 2 ) ).equals( "" )) )
      {
        // NO SE ENCONTRARON DATOS
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><PLANNCOD>000</PLANNCOD><PLANTIPO></PLANTIPO><PLANDES></PLANDES><PLANDAB></PLANDAB></Response>" );
      }
      else
      {
        // ARMA LA RESPUESTA
        strParseString = Strings.trim( Strings.mid( strParseString, 3, Strings.len( strParseString ) - 2 ) );

        while( (!Strings.trim( Strings.mid( strParseString, wvarPos, 5 ) ).equals( "00000" )) && (!Strings.trim( Strings.mid( strParseString, (wvarPos + 6), 60 ) ).equals( "" )) )
        {
          wvarResult = wvarResult + "<OPTION value='" + Strings.mid( strParseString, wvarPos, 3 ) + "'";
          wvarResult = wvarResult + " PLANTIPO='" + Strings.mid( strParseString, (wvarPos + 3), 2 ) + "'";
          wvarResult = wvarResult + " PLANDAB='" + Strings.mid( strParseString, (wvarPos + 65), 20 ) + "'>";
          //wvarResult = wvarResult & Mid(strParseString, wvarPos + 5, 60) & "</OPTION>"
          wvarResult = wvarResult + Strings.mid( strParseString, wvarPos + 65, 20 ) + "</OPTION>";

          strParseString = Strings.mid( strParseString, 86, Strings.len( strParseString ) - 85 );
          wvarPos = 1;

        }
        //
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      //
      wvarStep = "170";
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
    }
    catch (ComponentExecutionException e) {
    	//La propagamos, ya contiene el detalle del error
		throw e;
	}
	catch( Exception _e_ )
	{
		logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
        throw new ComponentExecutionException(_e_);
      }
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
