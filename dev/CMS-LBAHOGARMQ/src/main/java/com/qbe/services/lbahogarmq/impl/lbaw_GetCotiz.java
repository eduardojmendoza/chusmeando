package com.qbe.services.lbahogarmq.impl;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.connector.mq.MQProxy;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.ofvirtuallba.impl.lbaw_GetNroCot;
import com.qbe.services.ofvirtuallba.impl.lbaw_GetPortalComerc;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetCotiz implements VBObjectClass
{

	  protected static Logger logger = Logger.getLogger(lbaw_GetCotiz.class.getName());
	
	
	/**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorHgMQ.lbaw_GetCotiz";
  static final String mcteOpID = "0033";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteCIAASCOD = "0001";
  static final String mcteRAMOPCOD = "HOM1";
  static final String mctePLAN = "//PLAN";
  static final String mctePOLIZSEC = "//POLIZSEC";
  static final String mctePOLIZANN = "//POLIZANN";
  static final String mcteParam_TIPOHOGAR = "//TIPOHOGAR";
  static final String mcteParam_PROVCOD = "//PROVCOD";
  static final String mcteParam_ProviCod = "//PROVICOD";
  static final String mcteParam_LOCALIDAD = "//LOCALIDAD";
  static final String mcteParam_FPAGO = "//FPAGO";
  static final String mcteParam_COBROTIP = "//COBROTIP";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS/COBERTURA";
  static final String mcteParam_CoberCod = "COBERCOD";
  static final String mcteParam_ContrMod = "CONTRMOD";
  static final String mcteParam_CapitAsg = "CAPITASG";
  /**
   * DATOS DE LAS CAMPA�AS
   */
  static final String mcteNodos_Campa = "//CAMPANIAS/CAMPANIA";
  static final String mcteParam_CampaCod = "CAMPACOD";
  /**
   * PRODUCTOR
   */
  static final String mcteParam_AgeCod = "//AGECOD";
  /**
   * MMC 2012-12-03
   */
  static final String mcteParam_AgeCla = "//AGECLA";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   */
  public String FormatearNumero( String pvarNumero ) throws Exception
  {
    String FormatearNumero = "";
    XmlDomExtended wobjXML = null;
    XmlDomExtended wobjXSL = null;

    wobjXML = new XmlDomExtended();
    wobjXML.loadXML( "<PARAM>" + pvarNumero + "</PARAM>" );

    wobjXSL = new XmlDomExtended();
    wobjXSL.loadXML( p_GetXSLNumero());

    FormatearNumero = wobjXML.transformNode( wobjXSL ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
    return FormatearNumero;
  }

  private String p_GetXSLNumero() throws Exception
  {
    String p_GetXSLNumero = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='/'>";

    wvarStrXSL = wvarStrXSL + "<xsl:value-of select=\"format-number(number(PARAM) div 100, '####,00', 'european')\"/>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLNumero = wvarStrXSL;
    return p_GetXSLNumero;
  }

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    lbaw_GetPortalComerc wobjClass = new lbaw_GetPortalComerc();
    String wvarRequest = "";
    String wvarResponse = "";
    int wvarMQError = 0;
    String wvarArea = "";
    String wvarRAMOPCOD = "";
    String wvarCIAASCOD = "";
    String wvarAgentCod = "";
    String wvarAgentCla = "";
    String wvarPlanNCod = "";
    String wvarPOLIZSEC = "";
    String wvarPOLIZANN = "";
    String wvarCotiID = "";
    String wvarFechaDia = "";
    String wvarFechaVenc = "";
    String wvarFpago = "";
    String wvarCobroTip = "";
    String wvarTipoHogar = "";
    String wvarProviCod = "";
    String wvarLocalidad = "";
    String wvarCoberturas = "";
    String wvarCampanias = "";
    String wvarPrecio = "";
    int wvarStep = 0;
    String wvarResult = "";
    String wvarMensaje = "";
    String strParseString = "";
    int wvariCounter = 0;
    //
    //
    //
    //
    //MMC 2012-12-03
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      // Obtengo el numero de cotizacion
      wvarStep = 10;
      
      wvarRequest = "<Request></Request>";
      lbaw_GetNroCot wobjClassGetNroCot = new lbaw_GetNroCot();
      StringHolder strHoldGetNroCot = new StringHolder(wvarResponse);
      wobjClassGetNroCot.IAction_Execute(wvarRequest, strHoldGetNroCot, "");
      wvarResponse = strHoldGetNroCot.getValue(); 
      //
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( wvarResponse );
      wvarCotiID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//NROCOT" )  );
      wvarFechaDia = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//FECHA_DIA" )  );
      wvarFechaVenc = "99991231";
      //
      wobjXMLRequest = null;
      //
      wvarStep = 30;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      if( wobjXMLRequest.selectSingleNode( mctePOLIZANN )  == (org.w3c.dom.Node) null )
      {
        // **********************************
        wobjXMLParametros = new XmlDomExtended();
        wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName2));
        wvarRAMOPCOD = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gcteRAMOPCOD )  );
        wvarPlanNCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePLANNCOD )  );
        wvarPOLIZSEC = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePOLIZSEC )  );
        wvarPOLIZANN = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePOLIZANN )  );
        //
        wvarStep = 80;
        wobjXMLParametros = null;
        // **********************************
        wvarFpago = "0004";
        wvarCobroTip = "  ";
        wvarProviCod = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ProviCod )  ), 2 );

      }
      else
      {

        wvarRAMOPCOD = mcteRAMOPCOD;
        wvarPOLIZANN = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mctePOLIZANN )  ), 2 );
        wvarPOLIZSEC = Strings.right( "000000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mctePOLIZSEC )  ), 6 );
        wvarPlanNCod = Strings.right( "000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mctePLAN )  ), 3 );
        wvarFpago = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FPAGO )  ), 4 );
        wvarCobroTip = Strings.right( "  " + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_COBROTIP )  ), 2 );
        wvarProviCod = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PROVCOD )  ), 2 );
      }
      wvarCIAASCOD = mcteCIAASCOD;
      wvarTipoHogar = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIPOHOGAR )  ), 2 );
      wvarLocalidad = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_LOCALIDAD )  ) + Strings.fill( 8, " " ), 8 );
      //
      wvarCoberturas = "";
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Cober ) ;
      wvarStep = 40;
      //
      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarCoberturas = wvarCoberturas + Strings.right( "000" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CoberCod )  ), 3 );
        wvarCoberturas = wvarCoberturas + Strings.right( "00" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_ContrMod )  ), 2 );
        wvarCoberturas = wvarCoberturas + "00";
        wvarCoberturas = wvarCoberturas + Strings.right( Strings.fill( 15, "0" ) + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CapitAsg )  ), 15 );
        wvarCoberturas = wvarCoberturas + "S";
      }
      //
      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 49; wvariCounter++ )
      {
        wvarCoberturas = wvarCoberturas + Strings.fill( 22, "0" ) + " ";
      }
      //
      wvarStep = 50;
      wvarCampanias = "";
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Campa ) ;

      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarCampanias = wvarCampanias + Strings.right( "0000" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CampaCod )  ), 4 );
      }
      //
      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 9; wvariCounter++ )
      {
        wvarCampanias = wvarCampanias + Strings.fill( 4, "0" );
      }

      //
      wobjXMLRequest = null;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      //
      wvarStep = 60;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));

      //
      // LEVANTO LOS PARAMETROS PARA COTIZAR HOGAR (ParametrosCotizador.xml)
      wvarStep = 80;

      wvarStep = 90;
      // BUSCO LOS DATOS DE TELEFONO
      wvarStep = 100;
      wvarRequest = "<Request><PORTAL>LBA</PORTAL><RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD></Request>";
      lbaw_GetPortalComerc wobjClassGetPortalComerc = new lbaw_GetPortalComerc();
      StringHolder strHoldGetPortalComerc = new StringHolder(wvarResponse);
      wobjClassGetPortalComerc.IAction_Execute( wvarRequest, strHoldGetPortalComerc, "");
      wvarResponse = strHoldGetPortalComerc.getValue();          
      //
      wvarStep = 110;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.loadXML( wvarResponse );
      //MMC 2012-12-03
      wvarAgentCod = Strings.trim( XmlDomExtended .getText( wobjXMLParametros.selectSingleNode( mcteParam_AgeCod )  ) );
      //MMC 2012-12-03
      wvarAgentCla = Strings.trim( XmlDomExtended .getText( wobjXMLParametros.selectSingleNode( mcteParam_AgeCla )  ) );
      //'
      wvarAgentCod = "";
      wvarAgentCla = "";
      //'
      wobjXMLParametros = null;
      //
      //
      wvarMensaje = mcteOpID + Strings.right( Strings.fill( 9, "0" ) + wvarCotiID, 9 );
      // precio que va a calcular
      wvarMensaje = wvarMensaje + Strings.fill( 18, "0" );
      // zona
      wvarMensaje = wvarMensaje + Strings.fill( 4, "0" );
      //tipo de iva
      wvarMensaje = wvarMensaje + "3";
      wvarMensaje = wvarMensaje + Strings.right( "0000" + wvarAgentCod, 4 );
      wvarMensaje = wvarMensaje + wvarCIAASCOD;
      //RAMO = HOM1
      wvarMensaje = wvarMensaje + wvarRAMOPCOD;
      wvarMensaje = wvarMensaje + wvarPOLIZANN;
      wvarMensaje = wvarMensaje + wvarPOLIZSEC;
      wvarMensaje = wvarMensaje + Strings.fill( 4, "0" ) + Strings.fill( 4, "0" ) + Strings.fill( 6, "0" ) + Strings.fill( 4, "0" );
      wvarMensaje = wvarMensaje + wvarFechaDia + wvarFechaVenc + wvarFechaVenc + wvarFechaDia + wvarFechaVenc;
      wvarMensaje = wvarMensaje + "1";
      wvarMensaje = wvarMensaje + wvarFpago;
      wvarMensaje = wvarMensaje + wvarCobroTip;
      wvarMensaje = wvarMensaje + Strings.fill( 15, "0" ) + Strings.fill( 15, "0" ) + Strings.fill( 15, "0" );
      wvarMensaje = wvarMensaje + wvarTipoHogar + "00" + "00" + " " + " " + "00" + "00" + " " + " " + " " + " " + " " + "00";
      wvarMensaje = wvarMensaje + wvarPlanNCod + Strings.fill( 3, "0" ) + "  ";
      wvarMensaje = wvarMensaje + Strings.fill( 5, " " ) + Strings.fill( 40, " " ) + Strings.fill( 5, " " ) + Strings.fill( 4, " " ) + Strings.fill( 4, " " ) + " ";
      wvarMensaje = wvarMensaje + Strings.fill( 40, " " ) + wvarLocalidad + wvarProviCod + "00" + Strings.fill( 8, " " ) + Strings.fill( 14, " " );
      wvarMensaje = wvarMensaje + wvarCoberturas;
      wvarMensaje = wvarMensaje + wvarCampanias;
      // MMC 2012-12-03
      wvarMensaje = wvarMensaje + wvarAgentCla;
      //
      wvarArea = wvarMensaje;

      MQProxy wobjFrame2MQ = null;
      wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);
      StringHolder sHstrParseString = new StringHolder(strParseString);
      try {
		wvarMQError = wobjFrame2MQ.executePrim( wvarArea, sHstrParseString);
	} catch ( MQProxyTimeoutException te) {
		logger.log(Level.SEVERE, "Timeout al enviar mensaje MQ via conector MQProxy.AISPROXY ( error " + wvarMQError + " )",te);
		throw new ComponentExecutionException(3, "Ocurrió un timeout al comunicarse con el cotizador AIS ( error " + wvarMQError + " )", te);
	} catch (MQProxyException e) {
		String msg = "Exception [ Msg:"+ e.getMessage() + ", causa:" + e.getCause() + "] al enviar mensaje: [" + wvarRequest + "]";
		logger.log(Level.SEVERE, msg,e);
		throw new ComponentExecutionException(1, "Ocurrió un error al comunicarse con el cotizador AIS ( error " + wvarMQError + " )", e);
	}

	strParseString = sHstrParseString.getValue();

      //
      wvarResult = "";

      if( Obj.toDouble( Strings.mid( strParseString, 10, 18 ) ) > 0 )
      {
    	  //SE EXTRAE SOLO LO QUE CONTIENE EL PRECIO y EN EL XSL VA LA DIVISION POR 100 PARA LOS DECIMALES
        wvarPrecio = FormatearNumero(Strings.mid( strParseString, 10, 18 ));
      }
      else
      {
        wvarPrecio = "0,00";
      }

      wvarResult = "<COT_NRO>" + wvarCotiID + "</COT_NRO><COTIZACION>" + wvarPrecio + "</COTIZACION>";
      //
      wvarStep = 190;
      Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      //
      wvarStep = 200;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
    }
    catch (ComponentExecutionException e) {
    	//La propagamos, ya contiene el detalle del error
		throw e;
	}
	catch( Exception _e_ )
	{
		logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
        throw new ComponentExecutionException(_e_);
      }
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
