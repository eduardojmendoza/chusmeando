import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetCotiz implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorHgMQ.lbaw_GetCotiz";
  static final String mcteOpID = "0033";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteCIAASCOD = "0001";
  static final String mcteRAMOPCOD = "HOM1";
  static final String mctePLAN = "//PLAN";
  static final String mctePOLIZSEC = "//POLIZSEC";
  static final String mctePOLIZANN = "//POLIZANN";
  static final String mcteParam_TIPOHOGAR = "//TIPOHOGAR";
  static final String mcteParam_PROVCOD = "//PROVCOD";
  static final String mcteParam_ProviCod = "//PROVICOD";
  static final String mcteParam_LOCALIDAD = "//LOCALIDAD";
  static final String mcteParam_FPAGO = "//FPAGO";
  static final String mcteParam_COBROTIP = "//COBROTIP";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS/COBERTURA";
  static final String mcteParam_CoberCod = "COBERCOD";
  static final String mcteParam_ContrMod = "CONTRMOD";
  static final String mcteParam_CapitAsg = "CAPITASG";
  /**
   * DATOS DE LAS CAMPAÑAS
   */
  static final String mcteNodos_Campa = "//CAMPANIAS/CAMPANIA";
  static final String mcteParam_CampaCod = "CAMPACOD";
  /**
   * PRODUCTOR
   */
  static final String mcteParam_AgeCod = "//AGECOD";
  /**
   * MMC 2012-12-03
   */
  static final String mcteParam_AgeCla = "//AGECLA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   */
  public String FormatearNumero( String pvarNumero ) throws Exception
  {
    String FormatearNumero = "";
    diamondedge.util.XmlDom wobjXML = null;
    diamondedge.util.XmlDom wobjXSL = null;

    wobjXML = new diamondedge.util.XmlDom();
    //unsup wobjXML.async = false;
    wobjXML.loadXML( "<PARAM>" + pvarNumero + "</PARAM>" );

    wobjXSL = new diamondedge.util.XmlDom();
    //unsup wobjXSL.async = false;
    wobjXSL.loadXML( invoke( "p_GetXSLNumero", new Variant[] {} ) );

    FormatearNumero = Strings.replace( new Variant() /*unsup wobjXML.transformNode( wobjXSL ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
    return FormatearNumero;
  }

  private String p_GetXSLNumero() throws Exception
  {
    String p_GetXSLNumero = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='/'>";

    wvarStrXSL = wvarStrXSL + "<xsl:value-of select=\"format-number(number(PARAM), '###.###,00', 'european')\"/>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLNumero = wvarStrXSL;
    return p_GetXSLNumero;
  }

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    lbawA_OfVirtualLBA.lbaw_GetPortalComerc wobjClass = new lbawA_OfVirtualLBA.lbaw_GetPortalComerc();
    String wvarRequest = "";
    String wvarResponse = "";
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    String wvarRAMOPCOD = "";
    String wvarCIAASCOD = "";
    String wvarAgentCod = "";
    String wvarAgentCla = "";
    String wvarPlanNCod = "";
    String wvarPOLIZSEC = "";
    String wvarPOLIZANN = "";
    String wvarCotiID = "";
    String wvarFechaDia = "";
    String wvarFechaVenc = "";
    String wvarFpago = "";
    String wvarCobroTip = "";
    String wvarTipoHogar = "";
    String wvarProviCod = "";
    String wvarLocalidad = "";
    String wvarCoberturas = "";
    String wvarCampanias = "";
    String wvarPrecio = "";
    int wvarStep = 0;
    String wvarResult = "";
    String wvarMensaje = "";
    String strParseString = "";
    int wvariCounter = 0;
    //
    //
    //
    //
    //MMC 2012-12-03
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      // Obtengo el numero de cotizacion
      wvarStep = 10;
      wvarRequest = "<Request></Request>";
      wobjClass = new lbawA_OfVirtualLBA.lbaw_GetNroCot();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
      wobjClass = (lbawA_OfVirtualLBA.lbaw_GetPortalComerc) null;
      //
      wvarStep = 20;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( wvarResponse );
      wvarCotiID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//NROCOT" ) */ );
      wvarFechaDia = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//FECHA_DIA" ) */ );
      wvarFechaVenc = "99991231";
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //
      if( null /*unsup wobjXMLRequest.selectSingleNode( mctePOLIZANN ) */ == (org.w3c.dom.Node) null )
      {
        // **********************************
        wobjXMLParametros = new diamondedge.util.XmlDom();
        //unsup wobjXMLParametros.async = false;
        wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName2 );
        wvarRAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gcteRAMOPCOD ) */ );
        wvarPlanNCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePLANNCOD ) */ );
        wvarPOLIZSEC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePOLIZSEC ) */ );
        wvarPOLIZANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePOLIZANN ) */ );
        //
        wvarStep = 80;
        wobjXMLParametros = (diamondedge.util.XmlDom) null;
        // **********************************
        wvarFpago = "0004";
        wvarCobroTip = "  ";
        wvarProviCod = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ProviCod ) */ ), 2 );

      }
      else
      {

        wvarRAMOPCOD = mcteRAMOPCOD;
        wvarPOLIZANN = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mctePOLIZANN ) */ ), 2 );
        wvarPOLIZSEC = Strings.right( "000000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mctePOLIZSEC ) */ ), 6 );
        wvarPlanNCod = Strings.right( "000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mctePLAN ) */ ), 3 );
        wvarFpago = Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FPAGO ) */ ), 4 );
        wvarCobroTip = Strings.right( "  " + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COBROTIP ) */ ), 2 );
        wvarProviCod = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROVCOD ) */ ), 2 );
      }
      wvarCIAASCOD = mcteCIAASCOD;
      wvarTipoHogar = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPOHOGAR ) */ ), 2 );
      wvarLocalidad = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LOCALIDAD ) */ ) + Strings.fill( 8, " " ), 8 );
      //
      wvarCoberturas = "";
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) */;
      wvarStep = 40;
      //
      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarCoberturas = wvarCoberturas + Strings.right( "000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CoberCod ) */ ), 3 );
        wvarCoberturas = wvarCoberturas + Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_ContrMod ) */ ), 2 );
        wvarCoberturas = wvarCoberturas + "00";
        wvarCoberturas = wvarCoberturas + Strings.right( Strings.fill( 15, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CapitAsg ) */ ), 15 );
        wvarCoberturas = wvarCoberturas + "S";
      }
      //
      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 49; wvariCounter++ )
      {
        wvarCoberturas = wvarCoberturas + Strings.fill( 22, "0" ) + " ";
      }
      //
      wvarStep = 50;
      wvarCampanias = "";
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) */;

      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarCampanias = wvarCampanias + Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CampaCod ) */ ), 4 );
      }
      //
      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 9; wvariCounter++ )
      {
        wvarCampanias = wvarCampanias + Strings.fill( 4, "0" );
      }

      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      //
      wvarStep = 60;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );

      //
      // LEVANTO LOS PARAMETROS PARA COTIZAR HOGAR (ParametrosCotizador.xml)
      wvarStep = 80;

      wvarStep = 90;
      // BUSCO LOS DATOS DE TELEFONO
      wvarStep = 100;
      wvarRequest = "<Request><PORTAL>LBA</PORTAL><RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD></Request>";
      wobjClass = new lbawA_OfVirtualLBA.lbaw_GetPortalComerc();
      wobjClass.Execute( wvarRequest, wvarResponse, "" );
      //
      wvarStep = 110;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.loadXML( wvarResponse );
      //MMC 2012-12-03
      wvarAgentCod = Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( mcteParam_AgeCod ) */ ) );
      //MMC 2012-12-03
      wvarAgentCla = Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( mcteParam_AgeCla ) */ ) );
      //'
      wvarAgentCod = "";
      wvarAgentCla = "";
      //'
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      wobjClass = (lbawA_OfVirtualLBA.lbaw_GetPortalComerc) null;
      //
      //
      wvarMensaje = mcteOpID + Strings.right( Strings.fill( 9, "0" ) + wvarCotiID, 9 );
      // precio que va a calcular
      wvarMensaje = wvarMensaje + Strings.fill( 18, "0" );
      // zona
      wvarMensaje = wvarMensaje + Strings.fill( 4, "0" );
      //tipo de iva
      wvarMensaje = wvarMensaje + "3";
      wvarMensaje = wvarMensaje + Strings.right( "0000" + wvarAgentCod, 4 );
      wvarMensaje = wvarMensaje + wvarCIAASCOD;
      //RAMO = HOM1
      wvarMensaje = wvarMensaje + wvarRAMOPCOD;
      wvarMensaje = wvarMensaje + wvarPOLIZANN;
      wvarMensaje = wvarMensaje + wvarPOLIZSEC;
      wvarMensaje = wvarMensaje + Strings.fill( 4, "0" ) + Strings.fill( 4, "0" ) + Strings.fill( 6, "0" ) + Strings.fill( 4, "0" );
      wvarMensaje = wvarMensaje + wvarFechaDia + wvarFechaVenc + wvarFechaVenc + wvarFechaDia + wvarFechaVenc;
      wvarMensaje = wvarMensaje + "1";
      wvarMensaje = wvarMensaje + wvarFpago;
      wvarMensaje = wvarMensaje + wvarCobroTip;
      wvarMensaje = wvarMensaje + Strings.fill( 15, "0" ) + Strings.fill( 15, "0" ) + Strings.fill( 15, "0" );
      wvarMensaje = wvarMensaje + wvarTipoHogar + "00" + "00" + " " + " " + "00" + "00" + " " + " " + " " + " " + " " + "00";
      wvarMensaje = wvarMensaje + wvarPlanNCod + Strings.fill( 3, "0" ) + "  ";
      wvarMensaje = wvarMensaje + Strings.fill( 5, " " ) + Strings.fill( 40, " " ) + Strings.fill( 5, " " ) + Strings.fill( 4, " " ) + Strings.fill( 4, " " ) + " ";
      wvarMensaje = wvarMensaje + Strings.fill( 40, " " ) + wvarLocalidad + wvarProviCod + "00" + Strings.fill( 8, " " ) + Strings.fill( 14, " " );
      wvarMensaje = wvarMensaje + wvarCoberturas;
      wvarMensaje = wvarMensaje + wvarCampanias;
      // MMC 2012-12-03
      wvarMensaje = wvarMensaje + wvarAgentCla;
      //
      wvarArea = wvarMensaje;

      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, String.valueOf( wvarStep ), Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      wvarResult = "";

      if( Obj.toDouble( Strings.mid( strParseString, 10, 18 ) ) > 0 )
      {
        wvarPrecio = invoke( "FormatearNumero", new Variant[] { new Variant(Strings.replace( new Variant(String.valueOf( new Variant(Obj.toDouble( new Variant(Strings.mid( new Variant(strParseString), new Variant(10), new Variant(18) )) ) / 100) )), new Variant(","), new Variant(".") )) } );
      }
      else
      {
        wvarPrecio = "0,00";
      }

      wvarResult = "<COT_NRO>" + wvarCotiID + "</COT_NRO><COTIZACION>" + wvarPrecio + "</COTIZACION>";
      //
      wvarStep = 190;
      Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      //
      wvarStep = 200;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, String.valueOf( wvarStep ), Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarMensaje + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
