import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetPolCob implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_ProductorHgMQ.lbaw_GetPolCob";
  static final String mcteOpID = "0023";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_MSGCOD = "//MSGCOD";
  static final String mcteParam_CIAASCOD = "//CIAASCOD";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_PLANNCOD = "//PLANNCOD";
  static final String mcteParam_EFECTANN = "//EFECTANN";
  static final String mcteParam_EFECTMES = "//EFECTMES";
  static final String mcteParam_EFECTDIA = "//EFECTDIA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    String wvarMensaje = "";
    String wvarStep = "";
    String wvarResult = "";
    String wvarMSGCOD = "";
    String wvarCIAASCOD = "";
    String wvarRAMOPCOD = "";
    String wvarPOLIZANN = "";
    String wvarPOLIZSEC = "";
    String wvarPlanNCod = "";
    String wvarEFECTANN = "";
    String wvarEFECTMES = "";
    String wvarEFECTDIA = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = "10";
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarMSGCOD = Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MSGCOD ) */ ), 4 );
      wvarCIAASCOD = Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CIAASCOD ) */ ), 4 );
      wvarRAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD ) */ );
      wvarPOLIZANN = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN ) */ ), 2 );
      wvarPOLIZSEC = Strings.right( "000000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC ) */ ), 6 );
      wvarPlanNCod = Strings.right( "000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PLANNCOD ) */ ), 3 );
      wvarEFECTANN = Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTANN ) */ ), 4 );
      wvarEFECTMES = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTMES ) */ ), 2 );
      wvarEFECTDIA = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTDIA ) */ ), 2 );
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;

      wvarStep = "20";
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = "30";
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      wvarMensaje = wvarMSGCOD;
      wvarMensaje = wvarMensaje + wvarCIAASCOD;
      wvarMensaje = wvarMensaje + wvarRAMOPCOD;
      wvarMensaje = wvarMensaje + wvarPOLIZANN;
      wvarMensaje = wvarMensaje + wvarPOLIZSEC;
      wvarMensaje = wvarMensaje + wvarPlanNCod;
      wvarMensaje = wvarMensaje + wvarEFECTANN;
      wvarMensaje = wvarMensaje + wvarEFECTMES;
      wvarMensaje = wvarMensaje + wvarEFECTDIA;

      wvarArea = wvarMensaje;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = "150";
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      wvarStep = "150";
      wvarResult = "";
      wvarPos = 1;
      //
      strParseString = Strings.mid( strParseString, 28, Strings.len( strParseString ) - 31 );
      //
      if( (Strings.mid( strParseString, wvarPos, 2 ).equals( "ER" )) || (Strings.trim( Strings.mid( strParseString, wvarPos, 2 ) ).equals( "" )) )
      {
        // NO SE ENCONTRARON DATOS
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) );
        Response.set( Response + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" );
        Response.set( Response + "<COBERTURAS><COBERTURA>" );
        Response.set( Response + "<COBERCOD>000</COBERCOD>" );
        Response.set( Response + "<COBERDES></COBERDES>" );
        Response.set( Response + "<COBERDAB></COBERDAB>" );
        Response.set( Response + "<COBERORD></COBERORD>" );
        Response.set( Response + "<CAPITASG></CAPITASG>" );
        Response.set( Response + "<SWOBLIGA></SWOBLIGA>" );
        Response.set( Response + "<COBEBCOD></COBEBCOD>" );
        Response.set( Response + "<COBETCOD></COBETCOD>" );
        Response.set( Response + "<CAMAXMOD></CAMAXMOD>" );
        Response.set( Response + "<CAMINMOD></CAMINMOD>" );
        Response.set( Response + "</COBERTURA></COBERTURAS></Response>" );
      }
      else
      {
        // ARMA LA RESPUESTA
        //
        strParseString = Strings.trim( Strings.mid( strParseString, 3, Strings.len( strParseString ) - 2 ) );
        //
        wvarResult = wvarResult + "<COBERTURAS>";
        while( (!Strings.trim( Strings.mid( strParseString, wvarPos, 3 ) ).equals( "000" )) && (!Strings.trim( Strings.mid( strParseString, (wvarPos + 4), 60 ) ).equals( "" )) )
        {
          wvarResult = wvarResult + "<COBERTURA>";
          wvarResult = wvarResult + "<COBERCOD>" + Strings.mid( strParseString, wvarPos, 3 ) + "</COBERCOD>";
          wvarResult = wvarResult + "<COBERDES>" + Strings.mid( strParseString, (wvarPos + 3), 60 ) + "</COBERDES>";
          wvarResult = wvarResult + "<COBERDAB>" + Strings.mid( strParseString, (wvarPos + 63), 20 ) + "</COBERDAB>";
          wvarResult = wvarResult + "<COBERORD>" + Strings.mid( strParseString, (wvarPos + 83), 2 ) + "</COBERORD>";
          wvarResult = wvarResult + "<CAPITASG>" + Strings.mid( strParseString, (wvarPos + 85), 15 ) + "</CAPITASG>";
          wvarResult = wvarResult + "<SWOBLIGA>" + Strings.mid( strParseString, (wvarPos + 100), 1 ) + "</SWOBLIGA>";
          wvarResult = wvarResult + "<COBEBCOD>" + Strings.mid( strParseString, (wvarPos + 101), 3 ) + "</COBEBCOD>";
          wvarResult = wvarResult + "<COBETCOD>" + Strings.mid( strParseString, (wvarPos + 104), 3 ) + "</COBETCOD>";
          wvarResult = wvarResult + "<CAMAXMOD>" + Strings.mid( strParseString, (wvarPos + 107), 2 ) + "</CAMAXMOD>";
          wvarResult = wvarResult + "<CAMINMOD>" + Strings.mid( strParseString, (wvarPos + 109), 2 ) + "</CAMINMOD>";
          wvarResult = wvarResult + "</COBERTURA>";
          //
          strParseString = Strings.mid( strParseString, 112, Strings.len( strParseString ) - 111 );
          wvarPos = 1;
        }
        wvarResult = wvarResult + "</COBERTURAS>";
        //
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        wobjXMLResponse.loadXML( wvarResult );
        //unsup wobjXSLResponse.async = false;
        wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
        wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );

        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );

      }
      //
      wvarStep = "160";
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarMensaje + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //wvarStrXSL = "             <?xml version='1.0' encoding='UTF-8'?>"
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:fo='http://www.w3.org/1999/XSL/Format'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "<xsl:key name='cobertura' match='COBERTURA' use='COBEBCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:template match='/'>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='COBERTURAS'>";
    wvarStrXSL = wvarStrXSL + "      <xsl:for-each select=\"//COBERTURAS/COBERTURA[number(COBEBCOD) = 0]\">";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='COBERTURA'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COBERCOD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select=\"COBERCOD\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COBERDAB'>";
    wvarStrXSL = wvarStrXSL + "                  <xsl:value-of select=\"COBERDAB\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COBERORD'>";
    wvarStrXSL = wvarStrXSL + "                  <xsl:value-of select=\"COBERORD\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CAMINMOD'>";
    wvarStrXSL = wvarStrXSL + "                  <xsl:value-of select=\"CAMINMOD\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CAMAXMOD'>";
    wvarStrXSL = wvarStrXSL + "                  <xsl:value-of select=\"CAMAXMOD\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CAPITASG'>";
    wvarStrXSL = wvarStrXSL + "                  <xsl:value-of select=\"CAPITASG\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SWOBLIGA'>";
    wvarStrXSL = wvarStrXSL + "                  <xsl:value-of select=\"SWOBLIGA\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COBETCOD'>";
    wvarStrXSL = wvarStrXSL + "                  <xsl:value-of select=\"COBETCOD\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SECUNDARIAS'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:for-each select=\"key('cobertura', COBERCOD)\">";
    wvarStrXSL = wvarStrXSL + "                <xsl:element name='SECUNDARIA'>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_COBERCOD'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"COBERCOD\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_COBEBCOD'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"COBEBCOD\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_COBERDAB'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"COBERDAB\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_COBERORD'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"COBERORD\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_CAPITASG'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"CAPITASG\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_SWOBLIGA'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"SWOBLIGA\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_CAMINMOD'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"CAMINMOD\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_CAMAXMOD'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"CAMAXMOD\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                    <xsl:element name='S_COBETCOD'>";
    wvarStrXSL = wvarStrXSL + "                        <xsl:value-of select=\"COBETCOD\"/>";
    wvarStrXSL = wvarStrXSL + "                    </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             </xsl:for-each>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "      </xsl:for-each>";
    wvarStrXSL = wvarStrXSL + "      </xsl:element>";
    wvarStrXSL = wvarStrXSL + "</xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";

    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
