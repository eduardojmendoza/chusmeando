import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class ModGeneral
{
  public static final String gcteDB = "lbawA_OfVirtualLBA.udl";
  public static final String gcteClassMQConnection = "WD.Frame2MQ";
  /**
   *  Parametros XML de la Instalacion
   */
  public static final String gcteParamFileName = "Productor.xml";
  /**
   *  Parametros XML de Configuracion
   */
  public static final String gcteConfFileName = "MQConfig.xml";
  public static final String gcteQueueManager = "//QUEUEMANAGER";
  public static final String gctePutQueue = "//PUTQUEUE";
  public static final String gcteGetQueue = "//GETQUEUE";
  public static final String gcteGMOWaitInterval = "//GMO_WAITINTERVAL";
  /**
   *  Parametros XML del Cotizador
   */
  public static final String gcteParamFileName2 = "ParametrosCotizador.xml";
  public static final String gcteNodosHogar = "//HOGAR";
  public static final String gcteNodosAutoScoring = "//AUTOSCORING";
  public static final String gcteNodosComisiones = "//COMISIONES";
  public static final String gcteRAMOPCOD = "/RAMOPCOD";
  public static final String gctePLANNCOD = "/PLANNCOD";
  public static final String gctePOLIZSEC = "/POLIZSEC";
  public static final String gctePOLIZANN = "/POLIZANN";
  public static final String gcteBANCOCOD = "/BANCOCOD";
  public static final String gcteSUCURCOD = "/SUCURCOD";
  public static final String gcteCIAASCOD = "/CIAASCOD";
}
