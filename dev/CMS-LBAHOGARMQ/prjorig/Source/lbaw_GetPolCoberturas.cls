VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetPolCob"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_ProductorHgMQ.lbaw_GetPolCob"
Const mcteOpID              As String = "0023"

'Parametros XML de Entrada
Const mcteParam_MSGCOD      As String = "//MSGCOD"
Const mcteParam_CIAASCOD    As String = "//CIAASCOD"
Const mcteParam_RAMOPCOD    As String = "//RAMOPCOD"
Const mcteParam_POLIZANN    As String = "//POLIZANN"
Const mcteParam_POLIZSEC    As String = "//POLIZSEC"
Const mcteParam_PLANNCOD    As String = "//PLANNCOD"
Const mcteParam_EFECTANN    As String = "//EFECTANN"
Const mcteParam_EFECTMES    As String = "//EFECTMES"
Const mcteParam_EFECTDIA    As String = "//EFECTDIA"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarMensaje         As String
    Dim wvarStep            As String
    Dim wvarResult          As String
    Dim wvarMSGCOD          As String
    Dim wvarCIAASCOD        As String
    Dim wvarRAMOPCOD        As String
    Dim wvarPOLIZANN        As String
    Dim wvarPOLIZSEC        As String
    Dim wvarPlanNCod        As String
    Dim wvarEFECTANN        As String
    Dim wvarEFECTMES        As String
    Dim wvarEFECTDIA        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarMSGCOD = Right("0000" & .selectSingleNode(mcteParam_MSGCOD).Text, 4)
        wvarCIAASCOD = Right("0000" & .selectSingleNode(mcteParam_CIAASCOD).Text, 4)
        wvarRAMOPCOD = .selectSingleNode(mcteParam_RAMOPCOD).Text
        wvarPOLIZANN = Right("00" & .selectSingleNode(mcteParam_POLIZANN).Text, 2)
        wvarPOLIZSEC = Right("000000" & .selectSingleNode(mcteParam_POLIZSEC).Text, 6)
        wvarPlanNCod = Right("000" & .selectSingleNode(mcteParam_PLANNCOD).Text, 3)
        wvarEFECTANN = Right("0000" & .selectSingleNode(mcteParam_EFECTANN).Text, 4)
        wvarEFECTMES = Right("00" & .selectSingleNode(mcteParam_EFECTMES).Text, 2)
        wvarEFECTDIA = Right("00" & .selectSingleNode(mcteParam_EFECTDIA).Text, 2)
    End With
    '
    Set wobjXMLRequest = Nothing
    
    wvarStep = 20
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
      
    wvarMensaje = wvarMSGCOD
    wvarMensaje = wvarMensaje & wvarCIAASCOD
    wvarMensaje = wvarMensaje & wvarRAMOPCOD
    wvarMensaje = wvarMensaje & wvarPOLIZANN
    wvarMensaje = wvarMensaje & wvarPOLIZSEC
    wvarMensaje = wvarMensaje & wvarPlanNCod
    wvarMensaje = wvarMensaje & wvarEFECTANN
    wvarMensaje = wvarMensaje & wvarEFECTMES
    wvarMensaje = wvarMensaje & wvarEFECTDIA
    
    wvarArea = wvarMensaje
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
        wvarStep = 150
        wvarResult = ""
        wvarPos = 1
        '
        strParseString = Mid(strParseString, 28, Len(strParseString) - 31)
        '
        If Mid(strParseString, wvarPos, 2) = "ER" Or Trim(Mid(strParseString, wvarPos, 2)) = "" Then
            ' NO SE ENCONTRARON DATOS
            Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34)
            Response = Response & " mensaje=" & Chr(34) & Chr(34) & " />"
            Response = Response & "<COBERTURAS><COBERTURA>"
            Response = Response & "<COBERCOD>000</COBERCOD>"
            Response = Response & "<COBERDES></COBERDES>"
            Response = Response & "<COBERDAB></COBERDAB>"
            Response = Response & "<COBERORD></COBERORD>"
            Response = Response & "<CAPITASG></CAPITASG>"
            Response = Response & "<SWOBLIGA></SWOBLIGA>"
            Response = Response & "<COBEBCOD></COBEBCOD>"
            Response = Response & "<COBETCOD></COBETCOD>"
            Response = Response & "<CAMAXMOD></CAMAXMOD>"
            Response = Response & "<CAMINMOD></CAMINMOD>"
            Response = Response & "</COBERTURA></COBERTURAS></Response>"
        Else
           ' ARMA LA RESPUESTA
           '
           strParseString = Trim(Mid(strParseString, 3, Len(strParseString) - 2))
           '
           wvarResult = wvarResult & "<COBERTURAS>"
           Do While Trim(Mid(strParseString, wvarPos, 3)) <> "000" And Trim(Mid(strParseString, wvarPos + 4, 60)) <> ""
            wvarResult = wvarResult & "<COBERTURA>"
            wvarResult = wvarResult & "<COBERCOD>" & Mid(strParseString, wvarPos, 3) & "</COBERCOD>"
            wvarResult = wvarResult & "<COBERDES>" & Mid(strParseString, wvarPos + 3, 60) & "</COBERDES>"
            wvarResult = wvarResult & "<COBERDAB>" & Mid(strParseString, wvarPos + 63, 20) & "</COBERDAB>"
            wvarResult = wvarResult & "<COBERORD>" & Mid(strParseString, wvarPos + 83, 2) & "</COBERORD>"
            wvarResult = wvarResult & "<CAPITASG>" & Mid(strParseString, wvarPos + 85, 15) & "</CAPITASG>"
            wvarResult = wvarResult & "<SWOBLIGA>" & Mid(strParseString, wvarPos + 100, 1) & "</SWOBLIGA>"
            wvarResult = wvarResult & "<COBEBCOD>" & Mid(strParseString, wvarPos + 101, 3) & "</COBEBCOD>"
            wvarResult = wvarResult & "<COBETCOD>" & Mid(strParseString, wvarPos + 104, 3) & "</COBETCOD>"
            wvarResult = wvarResult & "<CAMAXMOD>" & Mid(strParseString, wvarPos + 107, 2) & "</CAMAXMOD>"
            wvarResult = wvarResult & "<CAMINMOD>" & Mid(strParseString, wvarPos + 109, 2) & "</CAMINMOD>"
            wvarResult = wvarResult & "</COBERTURA>"
            '
            strParseString = Mid(strParseString, 112, Len(strParseString) - 111)
            wvarPos = 1
           Loop
           wvarResult = wvarResult & "</COBERTURAS>"
           '
           
            Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
            Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
            wobjXMLResponse.loadXML wvarResult
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSL())
            wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
           
            Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
           
        End If
    '
    wvarStep = 160
    mobjCOM_Context.SetComplete
    IAction_Execute = 0

ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & wvarMensaje & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    'wvarStrXSL = "             <?xml version='1.0' encoding='UTF-8'?>"
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:fo='http://www.w3.org/1999/XSL/Format'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "<xsl:key name='cobertura' match='COBERTURA' use='COBEBCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='COBERTURAS'>"
    wvarStrXSL = wvarStrXSL & "      <xsl:for-each select=""//COBERTURAS/COBERTURA[number(COBEBCOD) = 0]"">"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='COBERTURA'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='COBERCOD'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select=""COBERCOD""/>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='COBERDAB'>"
    wvarStrXSL = wvarStrXSL & "                  <xsl:value-of select=""COBERDAB""/>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='COBERORD'>"
    wvarStrXSL = wvarStrXSL & "                  <xsl:value-of select=""COBERORD""/>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CAMINMOD'>"
    wvarStrXSL = wvarStrXSL & "                  <xsl:value-of select=""CAMINMOD""/>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CAMAXMOD'>"
    wvarStrXSL = wvarStrXSL & "                  <xsl:value-of select=""CAMAXMOD""/>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CAPITASG'>"
    wvarStrXSL = wvarStrXSL & "                  <xsl:value-of select=""CAPITASG""/>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='SWOBLIGA'>"
    wvarStrXSL = wvarStrXSL & "                  <xsl:value-of select=""SWOBLIGA""/>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='COBETCOD'>"
    wvarStrXSL = wvarStrXSL & "                  <xsl:value-of select=""COBETCOD""/>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
                    
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='SECUNDARIAS'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:for-each select=""key('cobertura', COBERCOD)"">"
    wvarStrXSL = wvarStrXSL & "                <xsl:element name='SECUNDARIA'>"
    wvarStrXSL = wvarStrXSL & "                    <xsl:element name='S_COBERCOD'>"
    wvarStrXSL = wvarStrXSL & "                        <xsl:value-of select=""COBERCOD""/>"
    wvarStrXSL = wvarStrXSL & "                    </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                    <xsl:element name='S_COBEBCOD'>"
    wvarStrXSL = wvarStrXSL & "                        <xsl:value-of select=""COBEBCOD""/>"
    wvarStrXSL = wvarStrXSL & "                    </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                    <xsl:element name='S_COBERDAB'>"
    wvarStrXSL = wvarStrXSL & "                        <xsl:value-of select=""COBERDAB""/>"
    wvarStrXSL = wvarStrXSL & "                    </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                    <xsl:element name='S_COBERORD'>"
    wvarStrXSL = wvarStrXSL & "                        <xsl:value-of select=""COBERORD""/>"
    wvarStrXSL = wvarStrXSL & "                    </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                    <xsl:element name='S_CAPITASG'>"
    wvarStrXSL = wvarStrXSL & "                        <xsl:value-of select=""CAPITASG""/>"
    wvarStrXSL = wvarStrXSL & "                    </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                    <xsl:element name='S_SWOBLIGA'>"
    wvarStrXSL = wvarStrXSL & "                        <xsl:value-of select=""SWOBLIGA""/>"
    wvarStrXSL = wvarStrXSL & "                    </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                    <xsl:element name='S_CAMINMOD'>"
    wvarStrXSL = wvarStrXSL & "                        <xsl:value-of select=""CAMINMOD""/>"
    wvarStrXSL = wvarStrXSL & "                    </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                    <xsl:element name='S_CAMAXMOD'>"
    wvarStrXSL = wvarStrXSL & "                        <xsl:value-of select=""CAMAXMOD""/>"
    wvarStrXSL = wvarStrXSL & "                    </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                    <xsl:element name='S_COBETCOD'>"
    wvarStrXSL = wvarStrXSL & "                        <xsl:value-of select=""COBETCOD""/>"
    wvarStrXSL = wvarStrXSL & "                    </xsl:element>"
    
    wvarStrXSL = wvarStrXSL & "                </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             </xsl:for-each>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "      </xsl:for-each>"
    wvarStrXSL = wvarStrXSL & "      </xsl:element>"
    wvarStrXSL = wvarStrXSL & "</xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"

    p_GetXSL = wvarStrXSL
End Function


'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
