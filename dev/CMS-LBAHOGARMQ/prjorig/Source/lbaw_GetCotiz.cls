VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetCotiz"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_ProductorHgMQ.lbaw_GetCotiz"
Const mcteOpID                  As String = "0033"

'Parametros XML de Entrada
Const mcteCIAASCOD              As String = "0001"
Const mcteRAMOPCOD              As String = "HOM1"
Const mctePLAN                  As String = "//PLAN"
Const mctePOLIZSEC              As String = "//POLIZSEC"
Const mctePOLIZANN              As String = "//POLIZANN"
Const mcteParam_TIPOHOGAR       As String = "//TIPOHOGAR"
Const mcteParam_PROVCOD         As String = "//PROVCOD"
Const mcteParam_ProviCod        As String = "//PROVICOD"
Const mcteParam_LOCALIDAD       As String = "//LOCALIDAD"
Const mcteParam_FPAGO           As String = "//FPAGO"
Const mcteParam_COBROTIP        As String = "//COBROTIP"
'DATOS DE LAS COBERTURAS
Const mcteNodos_Cober           As String = "//Request/COBERTURAS/COBERTURA"
Const mcteParam_CoberCod        As String = "COBERCOD"
Const mcteParam_ContrMod        As String = "CONTRMOD"
Const mcteParam_CapitAsg        As String = "CAPITASG"
'DATOS DE LAS CAMPAÑAS
Const mcteNodos_Campa           As String = "//CAMPANIAS/CAMPANIA"
Const mcteParam_CampaCod        As String = "CAMPACOD"
'PRODUCTOR
Const mcteParam_AgeCod          As String = "//AGECOD"
Const mcteParam_AgeCla          As String = "//AGECLA" 'MMC 2012-12-03
'

Function FormatearNumero(ByVal pvarNumero) As String
    Dim wobjXML As MSXML2.DOMDocument
    Dim wobjXSL As MSXML2.DOMDocument
    
    Set wobjXML = New MSXML2.DOMDocument
    wobjXML.async = False
    wobjXML.loadXML "<PARAM>" & pvarNumero & "</PARAM>"
    
    Set wobjXSL = New MSXML2.DOMDocument
    wobjXSL.async = False
    wobjXSL.loadXML p_GetXSLNumero
    
    FormatearNumero = Replace(wobjXML.transformNode(wobjXSL), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
End Function
Private Function p_GetXSLNumero() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select=""format-number(number(PARAM), '###.###,00', 'european')""/>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLNumero = wvarStrXSL
End Function


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjClass           As HSBCInterfaces.IAction
    '
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarRAMOPCOD        As String
    Dim wvarCIAASCOD        As String
    Dim wvarAgentCod        As String
    Dim wvarAgentCla        As String 'MMC 2012-12-03
    Dim wvarPlanNCod        As String
    Dim wvarPOLIZSEC        As String
    Dim wvarPOLIZANN        As String
    Dim wvarCotiID          As String
    Dim wvarFechaDia        As String
    Dim wvarFechaVenc       As String
    Dim wvarFpago           As String
    Dim wvarCobroTip        As String
    Dim wvarTipoHogar       As String
    Dim wvarProviCod        As String
    Dim wvarLocalidad       As String
    Dim wvarCoberturas      As String
    Dim wvarCampanias       As String
    Dim wvarPrecio          As String
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    '
    Dim wvarMensaje         As String
    '
    Dim strParseString      As String
    Dim wvariCounter        As Integer
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    ' Obtengo el numero de cotizacion
    wvarStep = 10
    wvarRequest = "<Request></Request>"
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OfVirtualLBA.lbaw_GetNroCot")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    '
    wvarStep = 20
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(wvarResponse)
        wvarCotiID = .selectSingleNode("//NROCOT").Text
        wvarFechaDia = .selectSingleNode("//FECHA_DIA").Text
        wvarFechaVenc = "99991231"
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        '
        
        If .selectSingleNode(mctePOLIZANN) Is Nothing Then
            '**********************************
            Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
            With wobjXMLParametros
                .async = False
                Call .Load(App.Path & "\" & gcteParamFileName2)
                wvarRAMOPCOD = .selectSingleNode(gcteNodosHogar & gcteRAMOPCOD).Text
                wvarPlanNCod = .selectSingleNode(gcteNodosHogar & gctePLANNCOD).Text
                wvarPOLIZSEC = .selectSingleNode(gcteNodosHogar & gctePOLIZSEC).Text
                wvarPOLIZANN = .selectSingleNode(gcteNodosHogar & gctePOLIZANN).Text
            End With
            '
            wvarStep = 80
            Set wobjXMLParametros = Nothing
            '**********************************
            wvarFpago = "0004"
            wvarCobroTip = "  "
            wvarProviCod = Right("00" & .selectSingleNode(mcteParam_ProviCod).Text, 2)
            
        Else
        
            wvarRAMOPCOD = mcteRAMOPCOD
            wvarPOLIZANN = Right("00" & .selectSingleNode(mctePOLIZANN).Text, 2)
            wvarPOLIZSEC = Right("000000" & .selectSingleNode(mctePOLIZSEC).Text, 6)
            wvarPlanNCod = Right("000" & .selectSingleNode(mctePLAN).Text, 3)
            wvarFpago = Right("0000" & .selectSingleNode(mcteParam_FPAGO).Text, 4)
            wvarCobroTip = Right("  " & .selectSingleNode(mcteParam_COBROTIP).Text, 2)
            wvarProviCod = Right("00" & .selectSingleNode(mcteParam_PROVCOD).Text, 2)
        End If
        wvarCIAASCOD = mcteCIAASCOD
        wvarTipoHogar = Right("00" & .selectSingleNode(mcteParam_TIPOHOGAR).Text, 2)
        wvarLocalidad = Left(.selectSingleNode(mcteParam_LOCALIDAD).Text & String(8, " "), 8)
        '
        wvarCoberturas = ""
        Set wobjXMLList = .selectNodes(mcteNodos_Cober)
        wvarStep = 40
        '
        For wvariCounter = 0 To wobjXMLList.length - 1
            wvarCoberturas = wvarCoberturas & Right("000" & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_CoberCod).Text, 3)
            wvarCoberturas = wvarCoberturas & Right("00" & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_ContrMod).Text, 2)
            wvarCoberturas = wvarCoberturas & "00"
            wvarCoberturas = wvarCoberturas & Right(String(15, "0") & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_CapitAsg).Text, 15)
            wvarCoberturas = wvarCoberturas & "S"
        Next
        '
        For wvariCounter = wobjXMLList.length To 49
            wvarCoberturas = wvarCoberturas & String(22, "0") & " "
        Next
        '
        wvarStep = 50
        wvarCampanias = ""
        Set wobjXMLList = .selectNodes(mcteNodos_Campa)
        
        For wvariCounter = 0 To wobjXMLList.length - 1
            wvarCampanias = wvarCampanias & Right("0000" & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_CampaCod).Text, 4)
        Next
        '
        For wvariCounter = wobjXMLList.length To 9
            wvarCampanias = wvarCampanias & String(4, "0")
        Next
        
    End With
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLList = Nothing
    '
    wvarStep = 60
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName

    '
    ' LEVANTO LOS PARAMETROS PARA COTIZAR HOGAR (ParametrosCotizador.xml)
    wvarStep = 80
    
    wvarStep = 90
    ' BUSCO LOS DATOS DE TELEFONO
    wvarStep = 100
    wvarRequest = "<Request><PORTAL>LBA</PORTAL><RAMOPCOD>" & wvarRAMOPCOD & "</RAMOPCOD></Request>"
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OfVirtualLBA.lbaw_GetPortalComerc")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    '
    wvarStep = 110
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.loadXML wvarResponse
    wvarAgentCod = Trim(wobjXMLParametros.selectSingleNode(mcteParam_AgeCod).Text) 'MMC 2012-12-03
    wvarAgentCla = Trim(wobjXMLParametros.selectSingleNode(mcteParam_AgeCla).Text) 'MMC 2012-12-03
    ''
    wvarAgentCod = ""
    wvarAgentCla = ""
    ''
    Set wobjXMLParametros = Nothing
    Set wobjClass = Nothing
    '

    '
    wvarMensaje = mcteOpID & Right(String(9, "0") & wvarCotiID, 9)
    wvarMensaje = wvarMensaje & String(18, "0") ' precio que va a calcular
    wvarMensaje = wvarMensaje & String(4, "0") ' zona
    wvarMensaje = wvarMensaje & "3" 'tipo de iva
    wvarMensaje = wvarMensaje & Right("0000" & wvarAgentCod, 4)
    wvarMensaje = wvarMensaje & wvarCIAASCOD
    wvarMensaje = wvarMensaje & wvarRAMOPCOD 'RAMO = HOM1
    wvarMensaje = wvarMensaje & wvarPOLIZANN
    wvarMensaje = wvarMensaje & wvarPOLIZSEC
    wvarMensaje = wvarMensaje & String(4, "0") & String(4, "0") & String(6, "0") & String(4, "0")
    wvarMensaje = wvarMensaje & wvarFechaDia & wvarFechaVenc & wvarFechaVenc & wvarFechaDia & wvarFechaVenc
    wvarMensaje = wvarMensaje & "1"
    wvarMensaje = wvarMensaje & wvarFpago
    wvarMensaje = wvarMensaje & wvarCobroTip
    wvarMensaje = wvarMensaje & String(15, "0") & String(15, "0") & String(15, "0")
    wvarMensaje = wvarMensaje & wvarTipoHogar & "00" & "00" & " " & " " & "00" & "00" & " " & " " & " " & " " & " " & "00"
    wvarMensaje = wvarMensaje & wvarPlanNCod & String(3, "0") & "  "
    wvarMensaje = wvarMensaje & String(5, " ") & String(40, " ") & String(5, " ") & String(4, " ") & String(4, " ") & " "
    wvarMensaje = wvarMensaje & String(40, " ") & wvarLocalidad & wvarProviCod & "00" & String(8, " ") & String(14, " ")
    wvarMensaje = wvarMensaje & wvarCoberturas
    wvarMensaje = wvarMensaje & wvarCampanias
    wvarMensaje = wvarMensaje & wvarAgentCla ' MMC 2012-12-03
    '
    wvarArea = wvarMensaje

    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '

       
        wvarResult = ""
        
        If CDbl(Mid(strParseString, 10, 18)) > 0 Then
           wvarPrecio = FormatearNumero(Replace(CStr(CDbl(Mid(strParseString, 10, 18)) / 100), ",", "."))
        Else
           wvarPrecio = "0,00"
        End If
           
        wvarResult = "<COT_NRO>" & wvarCotiID & "</COT_NRO><COTIZACION>" & wvarPrecio & "</COTIZACION>"
    '
    wvarStep = 190
    Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    '
    wvarStep = 200
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & wvarMensaje & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
