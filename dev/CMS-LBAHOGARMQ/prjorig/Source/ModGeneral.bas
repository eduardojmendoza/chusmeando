Attribute VB_Name = "ModGeneral"
Option Explicit

Public Const gcteDB As String = "lbawA_OfVirtualLBA.udl"
Public Const gcteClassMQConnection  As String = "WD.Frame2MQ"

' Parametros XML de la Instalacion
Public Const gcteParamFileName      As String = "Productor.xml"

' Parametros XML de Configuracion
Public Const gcteConfFileName       As String = "MQConfig.xml"
Public Const gcteQueueManager       As String = "//QUEUEMANAGER"
Public Const gctePutQueue           As String = "//PUTQUEUE"
Public Const gcteGetQueue           As String = "//GETQUEUE"
Public Const gcteGMOWaitInterval    As String = "//GMO_WAITINTERVAL"

' Parametros XML del Cotizador
Public Const gcteParamFileName2      As String = "ParametrosCotizador.xml"
Public Const gcteNodosHogar         As String = "//HOGAR"
Public Const gcteNodosAutoScoring   As String = "//AUTOSCORING"
Public Const gcteNodosComisiones    As String = "//COMISIONES"
Public Const gcteRAMOPCOD           As String = "/RAMOPCOD"
Public Const gctePLANNCOD           As String = "/PLANNCOD"
Public Const gctePOLIZSEC           As String = "/POLIZSEC"
Public Const gctePOLIZANN           As String = "/POLIZANN"
Public Const gcteBANCOCOD           As String = "/BANCOCOD"
Public Const gcteSUCURCOD           As String = "/SUCURCOD"
Public Const gcteCIAASCOD           As String = "/CIAASCOD"
