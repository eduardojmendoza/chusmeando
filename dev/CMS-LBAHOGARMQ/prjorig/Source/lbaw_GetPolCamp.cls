VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetPolCamp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_ProductorHgMQ.lbaw_GetPolCamp"
Const mcteOpID              As String = "0024"

'Parametros XML de Entrada
Const mcteParam_MSGCOD      As String = "//MSGCOD"
Const mcteParam_CIAASCOD    As String = "//CIAASCOD"
Const mcteParam_RAMOPCOD    As String = "//RAMOPCOD"
Const mcteParam_POLIZANN    As String = "//POLIZANN"
Const mcteParam_POLIZSEC    As String = "//POLIZSEC"
Const mcteParam_EFECTANN    As String = "//EFECTANN"
Const mcteParam_EFECTMES    As String = "//EFECTMES"
Const mcteParam_EFECTDIA    As String = "//EFECTDIA"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarMensaje         As String
    Dim wvarStep            As String
    Dim wvarResult          As String
    Dim wvarMSGCOD          As String
    Dim wvarCIAASCOD        As String
    Dim wvarRAMOPCOD        As String
    Dim wvarPOLIZANN        As String
    Dim wvarPOLIZSEC        As String
    Dim wvarEFECTANN        As String
    Dim wvarEFECTMES        As String
    Dim wvarEFECTDIA        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarMSGCOD = Right("0000" & .selectSingleNode(mcteParam_MSGCOD).Text, 4)
        wvarCIAASCOD = Right("0000" & .selectSingleNode(mcteParam_CIAASCOD).Text, 4)
        wvarRAMOPCOD = .selectSingleNode(mcteParam_RAMOPCOD).Text
        wvarPOLIZANN = Right("00" & .selectSingleNode(mcteParam_POLIZANN).Text, 2)
        wvarPOLIZSEC = Right("000000" & .selectSingleNode(mcteParam_POLIZSEC).Text, 6)
        wvarEFECTANN = Right("0000" & .selectSingleNode(mcteParam_EFECTANN).Text, 4)
        wvarEFECTMES = Right("00" & .selectSingleNode(mcteParam_EFECTMES).Text, 2)
        wvarEFECTDIA = Right("00" & .selectSingleNode(mcteParam_EFECTDIA).Text, 2)
    End With
    '
    Set wobjXMLRequest = Nothing
    
    wvarStep = 20
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    wvarMensaje = wvarMSGCOD
    wvarMensaje = wvarMensaje & wvarCIAASCOD
    wvarMensaje = wvarMensaje & wvarRAMOPCOD
    wvarMensaje = wvarMensaje & wvarPOLIZANN
    wvarMensaje = wvarMensaje & wvarPOLIZSEC
    wvarMensaje = wvarMensaje & wvarEFECTANN
    wvarMensaje = wvarMensaje & wvarEFECTMES
    wvarMensaje = wvarMensaje & wvarEFECTDIA

    wvarArea = wvarMensaje
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    wvarStep = 150
    wvarResult = ""
    wvarPos = 1
    '
    strParseString = Mid(strParseString, 25, Len(strParseString) - 24)
    '
    '
    '<CAMPANAS><CAMPANA><TAG1></TAG1><TAG2></TAG2></CAMPANA><CAMPANAS>
    '
    If Mid(strParseString, wvarPos, 2) = "ER" Or Trim(Mid(strParseString, wvarPos, 2)) = "" Then
        ' NO SE ENCONTRARON DATOS
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34)
        Response = Response & " mensaje=" & Chr(34) & Chr(34) & " />"
        Response = Response & "<CAMPANAS><CAMPANA>"
        Response = Response & "<CAMPACOD>0000</CAMPACOD>"
        Response = Response & "<CAMPADES></CAMPADES>"
        Response = Response & "</CAMPANA></CAMPANAS></Response>"
    Else
       ' ARMA LA RESPUESTA
       '
       strParseString = Mid(strParseString, 3, Len(strParseString) - 2)
       '
       wvarResult = wvarResult & "<CAMPANAS>"
       Do While Trim(Mid(strParseString, wvarPos, 4)) <> "0000" And Trim(Mid(strParseString, wvarPos + 4, 60)) <> ""
        wvarResult = wvarResult & "<CAMPANA>"
        wvarResult = wvarResult & "<CAMPACOD>" & Mid(strParseString, wvarPos, 4) & "</CAMPACOD>"
        wvarResult = wvarResult & "<CAMPADES>" & Mid(strParseString, wvarPos + 4, 60) & "</CAMPADES>"
        wvarResult = wvarResult & "</CAMPANA>"
        '
        strParseString = Mid(strParseString, 65, Len(strParseString) - 63)
        wvarPos = 1
       Loop
       wvarResult = wvarResult & "</CAMPANAS>"
       '
       Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    End If
    '
    wvarStep = 160
    mobjCOM_Context.SetComplete
    IAction_Execute = 0

ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & wvarMensaje & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub


