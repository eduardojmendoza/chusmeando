package com.qbe.services.paginate;

import static org.junit.Assert.*;

import javax.xml.bind.JAXBException;

import org.junit.Test;

public class Request1107x9Test {

	@Test
	public void testMarshall() throws JAXBException {
		Request1107x9 r = new Request1107x9();
		r.setUsuario("EX009005L");
		r.setNivelAs("GO");
		r.setClienSecAs("101556884");
		String m = r.marshall();
		assertTrue("No encontró USUARIO", m.contains("<USUARIO>EX009005L</USUARIO>"));
		assertTrue("No encontró NIVELAS", m.contains("<NIVELAS>GO</NIVELAS>"));
		assertTrue("No encontró CLIENSECAS", m.contains("<CLIENSECAS>101556884</CLIENSECAS>"));
		}

}
