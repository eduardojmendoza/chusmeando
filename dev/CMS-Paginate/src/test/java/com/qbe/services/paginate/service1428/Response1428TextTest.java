package com.qbe.services.paginate.service1428;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import com.qbe.vbcompat.framework.jaxb.Estado;

public class Response1428TextTest {

	@Test
	public void testMarshal() throws JAXBException {
		Response1428Text r = new Response1428Text();
		r.setEstado(Estado.newWithResultadoTrue());
		r.getMixedContent().add(
				"\n" + 
				"Apellido / Razón Social,Nro. Póliza,Endoso,Estado de Póliza,Nro. deRecibo,Moneda,Importe,Moneda,Importe,Fecha de Cobro, Canal,Judicial\n" +
				"BARBERON  ; ESTEBAN FABIAN,EEQ1-00-081284,1,ANULADA,173149784,$,-12252.40,,,15/07/2013,DEBITO AUTOMATICO BA,N\n");
		System.out.println(r.marshal());
	}

}
