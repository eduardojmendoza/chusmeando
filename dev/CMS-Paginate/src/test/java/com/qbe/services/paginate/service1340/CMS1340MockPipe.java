package com.qbe.services.paginate.service1340;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.springframework.util.StreamUtils;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.CMSPipe;
import com.qbe.services.paginate.CommRequest;
import com.qbe.services.paginate.MockPipePointer;
import com.qbe.services.paginate.PipeException;
import com.qbe.services.paginate.PipePointer;
import com.qbe.services.paginate.TestPipe;
import com.qbe.services.paginate.mqgestion.GetDeudaCobradaCanalesDetalle;
import com.qbe.services.paginate.siniMQgenerico.siniestroDenuncia_getListadoClientes;

/**
 * Un pipe que trae datos del actionCode 1340
 * 
 * @author gavilan
 *
 */
public class CMS1340MockPipe extends CMSPipe implements Serializable, TestPipe {

	private static Logger logger = Logger.getLogger(CMS1340MockPipe.class.getName());
	
	private String[] regsFileNames = new String[] { "1340/cms/resp1_1.xml","",""};

	protected int nextBlockIndex = 0;
	protected int executeRequestCount = 0;
	
	public CMS1340MockPipe() {
		this.setRequest(new Request1340());
		this.setResponseUnmarshaller(new Response1340Unmarshaller());
		this.setActionCode(siniestroDenuncia_getListadoClientes.ACTION_CODE_1340);
	}
	
	
	


	protected String executeRequest(CommRequest nextRequest)
			throws OSBConnectorException, MalformedURLException, JAXBException {
		String response;
		try {
			logger.log(Level.FINE,"Recupero block: " + nextBlockIndex);
			executeRequestCount++;
			logger.log(Level.FINE,"executeRequestCount: " + executeRequestCount);
			
			response = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(regsFileNames[nextBlockIndex++]), Charset.forName("UTF-8"));
			response = BASIC_REQUEST;
		} catch (IOException e) {
			e.printStackTrace();
			throw new OSBConnectorException("Al levantar uno de los archivos", e);
		}
		return response;
	}

	@Override
	public void reset() {
		super.reset();
		nextBlockIndex = 0;
	}

	@Override
	public PipePointer currentPointer() {
		MockPipePointer p = new MockPipePointer();
		p.setBlock(nextBlockIndex);
		p.setExecuteRequestCount(executeRequestCount);
		p.setPipePointer(super.currentPointer());
		return p;
	}

	@Override
	public void seek(PipePointer pp) throws PipeException {
		MockPipePointer cpp = (MockPipePointer)pp;
		nextBlockIndex = cpp.getBlock() - 1;
		logger.log(Level.FINE,"Seek block: " + nextBlockIndex);
		super.seek(cpp.getPipePointer());
		executeRequestCount = cpp.getExecuteRequestCount();
	}
	
	@Override
	public int getSourceRequests() {
		return executeRequestCount;
	}

	private static final String BASIC_REQUEST =  "<Response> <Estado mensaje=\"\" resultado=\"true\"/>"+
			"<CAMPOS>" +
			"<CANT>4</CANT>" +
			"<ITEMS>" +
			"<ITEM>" +
				"<RAMOPCOD>APL1</RAMOPCOD>" +
				"<POLIZANN>00</POLIZANN>" +
				"<POLIZSEC>001908</POLIZSEC>" +
				"<CERTIPOL>0000</CERTIPOL>" +
				"<CERTIANN>0000</CERTIANN>" +
				"<CERTISEC>004689</CERTISEC>" +
				"<SUPLENUM>0</SUPLENUM>" +
				"<CLIENDES>DONNAMARIA , NORMA ALICIA</CLIENDES>" +
				"<TOMARIES>RENAULT 9 AA05672</TOMARIES>" +
				"<PATENNUM>ASE040</PATENNUM>" +
				"<SITUCPOL>VENCIDA</SITUCPOL>" +
				"<SWDENHAB>S</SWDENHAB>" +
				"<SWSINIES>N</SWSINIES>" +
				"<SWEXIGIB>0</SWEXIGIB>" +
				"<RAMO>1</RAMO>" +
				"<VIGDESDE>19960425</VIGDESDE>" +
				"<VIGHASTA>19990510</VIGHASTA>" +
				"<FULTSTRO>19990316</FULTSTRO>" +
			"</ITEM>" +
			"<ITEM>" +
				"<RAMOPCOD>APL1</RAMOPCOD>" +
				"<POLIZANN>00</POLIZANN>" +
				"<POLIZSEC>002986</POLIZSEC>" +
				"<CERTIPOL>0000</CERTIPOL>" +
				"<CERTIANN>0000</CERTIANN>" +
				"<CERTISEC>007262</CERTISEC>" +
				"<SUPLENUM>4</SUPLENUM>" +
				"<CLIENDES>DONNANGELO, FABIANA  ,</CLIENDES>" +
				"<TOMARIES>FIAT SIENA 6284160</TOMARIES>" +
				"<PATENNUM>EWU880</PATENNUM>" +
				"<SITUCPOL>VENCIDA</SITUCPOL>" +
				"<SWDENHAB>S</SWDENHAB>" +
				"<SWSINIES>N</SWSINIES>" +
				"<SWEXIGIB>0</SWEXIGIB>" +
				"<RAMO>1</RAMO>" +
				"<VIGDESDE>20050323</VIGDESDE>" +
				"<VIGHASTA>20081201</VIGHASTA>" +
				"<FULTSTRO>00000000</FULTSTRO>" +
			"</ITEM>" +
			"<ITEM>" +
				"<RAMOPCOD>APL1</RAMOPCOD>" +
				"<POLIZANN>00</POLIZANN>" +
				"<POLIZSEC>002986</POLIZSEC>" +
				"<CERTIPOL>0000</CERTIPOL>" +
				"<CERTIANN>0000</CERTIANN>" +
				"<CERTISEC>015419</CERTISEC>" +
				"<SUPLENUM>0</SUPLENUM>" +
				"<CLIENDES>DONNALOIA ,  CLAUDIO SERGIO</CLIENDES>" +
				"<TOMARIES>FIAT PALIO 8357532</TOMARIES>" +
				"<PATENNUM>HMX432</PATENNUM>" +
				"<SITUCPOL>VENCIDA</SITUCPOL>" +
				"<SWDENHAB>S</SWDENHAB>" +
				"<SWSINIES>N</SWSINIES>" +
				"<SWEXIGIB>0</SWEXIGIB>" +
				"<RAMO>1</RAMO>" +
				"<VIGDESDE>20080910</VIGDESDE>" +
				"<VIGHASTA>20100301</VIGHASTA>" +
				"<FULTSTRO>20100201</FULTSTRO>" +
			"</ITEM>" +
			"<ITEM>"+
		        "<RAMOPCOD>AUS1</RAMOPCOD>"+
		        "<POLIZANN>00</POLIZANN>"+
		        "<POLIZSEC>000001</POLIZSEC>" +
		        "<CERTIPOL>0000</CERTIPOL>" +
		        "<CERTIANN>0001</CERTIANN>" +
		        "<CERTISEC>542690</CERTISEC>" +
		        "<SUPLENUM>0</SUPLENUM>" +
		        "<CLIENDES>DONNARI , SUSANA GRACIELA</CLIENDES>" +
		        "<TOMARIES>VOLKSWAGEN GOL UNF470038</TOMARIES>" +
		        "<PATENNUM>HFF987</PATENNUM>" +
		        "<SITUCPOL>PROX.RENOV</SITUCPOL>" +
		        "<SWDENHAB>S</SWDENHAB>" +
		        "<SWSINIES>N</SWSINIES>" +
		        "<SWEXIGIB>0</SWEXIGIB>" +
		        "<RAMO>1</RAMO>" +
		        "<VIGDESDE>20130605</VIGDESDE>" +
		        "<VIGHASTA>20140601</VIGHASTA>" +
		        "<FULTSTRO>00000000</FULTSTRO>" +
		   "</ITEM>" +
        "</ITEMS>" +
        "<Request>" +
	        "<NIVELAS>PR</NIVELAS>" +
	        "<CLIENSECAS>100072343</CLIENSECAS>" +
	        "<NIVEL1/>" +
	        "<CLIENSEC1>0</CLIENSEC1>" +
	        "<NIVEL2/>" +
	        "<CLIENSEC2>0</CLIENSEC2>" +
	        "<NIVEL3/>" +
	        "<CLIENSEC3>0</CLIENSEC3>" +
	        "<RowCount>10</RowCount>" +
	        "<StartRow>0</StartRow>" +
	        "<DEFINICION>siniestroDenuncia_getListadoClientes.xml</DEFINICION>" +
	        "<NIVELCLA1/>" +
	        "<NIVELCLA2/>" +
	        "<NIVELCLA3/>" +
	        "<RAMO/>" +
	        "<POLIZA>0</POLIZA>" +
	        "<CERTIF>0</CERTIF>" +
	        "<PATENTE/>" +
	        "<APELLIDO>DONNA</APELLIDO>" +
	        "<DOCUMTIP>0</DOCUMTIP>" +
	        "<DOCUMDAT/>" +
	        "<FECHASINIE>20130810</FECHASINIE>" +
	        "<TIPOSINIE>AU</TIPOSINIE>" +
	        "<SWBUSCA>N</SWBUSCA>" +
	        "<ORDEN>A</ORDEN>" +
	        "<COLUMNA>1</COLUMNA>" +
	        "<RETORNARREQUEST/>" +
	        "<NROQRY>3322026</NROQRY>" +
	        "<RETOMA>00000000</RETOMA>" +
	        "<CIAASCOD>0001</CIAASCOD>" +
	        "<USUARCOD/>" +
	        "<ESTADO>OK</ESTADO>" +
	        "<ERROR/>" +
	        "<NIVELCLAS/>" +
        "</Request>" +
     "</CAMPOS>" +
   "</Response>";	
}
