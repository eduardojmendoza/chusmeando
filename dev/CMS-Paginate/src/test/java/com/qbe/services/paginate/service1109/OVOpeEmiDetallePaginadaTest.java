package com.qbe.services.paginate.service1109;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.util.StreamUtils;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.services.paginate.CMSPipe;
import com.qbe.services.paginate.Constants;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.InsisEmptyPipe;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MessagePipe;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.Request1107x9;
import com.qbe.services.paginate.Response;
import com.qbe.services.paginate.service1109.mocks.CMS1109MockPipe;
import com.qbe.vbcompat.framework.jaxb.Estado;
import com.qbe.vbcompat.string.StringHolder;

public class OVOpeEmiDetallePaginadaTest {

	private static final String BASIC_REQUEST = "		<Request>" +
"			<USUARIO>EX009005L</USUARIO>" +
"			<NIVELAS>OR</NIVELAS>" +
"			<CLIENSECAS>100029438</CLIENSECAS>" +
"			<NIVEL1 />" +
"			<CLIENSEC1 />" +
"			<NIVEL2 />" +
"			<CLIENSEC2 />" +
"			<NIVEL3>PR</NIVEL3>" +
"			<CLIENSEC3>100029438</CLIENSEC3>" +
"			<FECDES>20140224</FECDES>" +
"			<FECHAS>20140225</FECHAS>" +
"			<FECCONT />" +
"			<PRODUCCONT />" +
"			<POLIZACONT />" +
"			<OPECONT />" +
"		</Request>";
	
	private static final String REQUEST_TEMPLATE = "		<Request>" +
"			<USUARIO>EX009005L</USUARIO>" +
"			<NIVELAS>OR</NIVELAS>" +
"			<CLIENSECAS>100029438</CLIENSECAS>" +
"			<NIVEL1 />" +
"			<CLIENSEC1 />" +
"			<NIVEL2 />" +
"			<CLIENSEC2 />" +
"			<NIVEL3>PR</NIVEL3>" +
"			<CLIENSEC3>100029438</CLIENSEC3>" +
"			<FECDES>20130225</FECDES>" +
"			<FECHAS>20130226</FECHAS>" +
"			<FECCONT />" +
"			<PRODUCCONT />" +
"			<POLIZACONT />" +
"			<OPECONT>%s</OPECONT>" +
"		</Request>";

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFetchTwoPagesAtATime() throws JAXBException, IOException {
		PaginatedActionCode action = new OVOpeEmiDetallePaginada();
		//Tomo de a 2 páginas, más fácil para determinar qué registro cae
		action.setPageSize(360);
		action.setTestPipes(this.createTestPipes());
		String request = BASIC_REQUEST;
		StringHolder sh = new StringHolder();
		int result = action.IAction_Execute(request, sh, null);
		Response1109 resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertTrue(((Reg1109)resp1109.getRecords().get(0)).getPol().equals("00797106"));
		assertTrue(resp1109.getRecords().size() == action.getPageSize()); 

		//TODO Validar que el request que armo sea igual a "1109trs/req2.txt"

		String secondRequest = String.format(REQUEST_TEMPLATE,resp1109.getOpecont());
				
		result = action.IAction_Execute(secondRequest, sh, null);
		resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertEquals("390250",((Reg1109)resp1109.getRecords().get(0)).getCersec());
		assertEquals("11",((Reg1109)resp1109.getRecords().get(0)).getOperapol());

		assertTrue(resp1109.getRecords().size() == action.getPageSize() ); 
	}

	@Test
	public void testEmptyPipeReturnsEstadoFalse() throws Exception {
		//TODO Implement me
	}
	
	/**
	 * Recorre una query completa, paginando claro
	 * @throws Exception
	 */
	@Test
	public void testFullNavigationLocal() throws Exception {
		int pageSize = 17;
		// 2 pipes de 9*180 + 20 = 3280 registros
		double numRegs = 3280d;
		
		String requestTemplate = REQUEST_TEMPLATE;
		String request = String.format(requestTemplate, "");
		PaginatedActionCode action = new OVOpeEmiDetallePaginada();
		action.setTestPipes(this.createTestPipes());
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		int result = action.IAction_Execute(request, sh, null);
		Response1109 resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertFalse("Vino false", resp1109.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
		
		ContinuationStatus paginateFlag = resp1109.getContinuationStatus();
		int numPages = 1;
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR)) {
			numPages++;

			String nextReq = String.format(requestTemplate,resp1109.getOpecont());
			result = action.IAction_Execute(nextReq, sh, null);
			resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
			assertFalse("Vino false", resp1109.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
			paginateFlag = resp1109.getContinuationStatus();
		}
		
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
	}
	
	
	
	/**
	 * Recorre una query completa, paginando claro.
	 * El pipe de insis está vacío
	 * @throws Exception
	 */
	@Test
	public void testFullNavigationLocalEmptyInsis() throws Exception {
		int pageSize = 100;
		// 1 pipe de 9*180 + 20
		double numRegs = 9*180 + 20d;
		
		String requestTemplate = REQUEST_TEMPLATE;
		String request = String.format(requestTemplate, "");
		PaginatedActionCode action = new OVOpeEmiDetallePaginada();
		List<Pipe> pipes = this.createOneCMSTestPipe();
		pipes.addAll(this.createOneEmptyInsisPipe());
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		int result = action.IAction_Execute(request, sh, null);
		Response1109 resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertFalse("Vino false", resp1109.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
		
		ContinuationStatus paginateFlag = resp1109.getContinuationStatus();
		int numPages = 1;
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR)) {
			numPages++;

			String nextReq = String.format(requestTemplate,resp1109.getOpecont());
			result = action.IAction_Execute(nextReq, sh, null);
			resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
			assertFalse("Vino false", resp1109.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
			paginateFlag = resp1109.getContinuationStatus();
		}
		
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
	}
	
// De acá en adelante, falta
	
	
	@Test
	@Ignore
	public void testFullNavigationInsis() throws Exception {
		if ( !CurrentProfile.getProfileName().equals("test")) {
			return;
		}
		String requestTemplate = "		<Request>" +
"			<USUARIO>EX009005L</USUARIO>" +
"			<NIVELAS>OR</NIVELAS>" +
"			<CLIENSECAS>100029438</CLIENSECAS>" +
"			<NIVEL1 />" +
"			<CLIENSEC1 />" +
"			<NIVEL2 />" +
"			<CLIENSEC2 />" +
"			<NIVEL3>PR</NIVEL3>" +
"			<CLIENSEC3>100029438</CLIENSEC3>" +
"			<FECDES>20130402</FECDES>" +
"			<FECHAS>20130405</FECHAS>" +
"			<FECCONT />" +
"			<PRODUCCONT />" +
"			<POLIZACONT />" +
"			<OPECONT>%s</OPECONT>" +
"		</Request>";
		String request = String.format(requestTemplate, "");
		PaginatedActionCode action = new OVOpeEmiDetallePaginada();
		action.setPageSize(50);
		action.setTestPipes(this.createOneInsisPipe(request));
		
		StringHolder sh = new StringHolder();
		int result = action.IAction_Execute(request, sh, null);
		Response1109 resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertFalse("Vino false", resp1109.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
		
		ContinuationStatus paginateFlag = resp1109.getContinuationStatus();
		int numPages = 0;
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR)) {
			numPages++;

			String nextReq = String.format(requestTemplate,resp1109.getOpecont());
			result = action.IAction_Execute(nextReq, sh, null);
			resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
			assertFalse("Vino false", resp1109.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
			paginateFlag = resp1109.getContinuationStatus();
		}
		assertTrue("No recorrió las páginas que debería", numPages > 0);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
	}

	@Test
	public void testTwoCMS() throws Exception {
		if ( CurrentProfile.getProfileName().equals("test")) {

		String request = "		<Request>" +
"			<USUARIO>EX009005L</USUARIO>" +
"			<NIVELAS>OR</NIVELAS>" +
"			<CLIENSECAS>100029438</CLIENSECAS>" +
"			<NIVEL1 />" +
"			<CLIENSEC1 />" +
"			<NIVEL2 />" +
"			<CLIENSEC2 />" +
"			<NIVEL3>PR</NIVEL3>" +
"			<CLIENSEC3>100029438</CLIENSEC3>" +
"			<FECDES>20130224</FECDES>" +
"			<FECHAS>20130324</FECHAS>" +
"			<FECCONT />" +
"			<PRODUCCONT />" +
"			<POLIZACONT />" +
"			<OPECONT />" +
"		</Request>";

		PaginatedActionCode action = new OVOpeEmiDetallePaginada();
		action.setPageSize(5);
		
		action.setTestPipes(this.createTwoCMSPipes(request));

		StringHolder sh = new StringHolder();
		int result = action.IAction_Execute(request, sh, null);
//		System.out.println("Response pagina 1 = " + sh.getValue());
		
		Response1109 resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertTrue(((Reg1109)resp1109.getRecords().get(0)).getPol().equals("00797763"));
		assertTrue(resp1109.getRecords().size() == action.getPageSize() ); 
		

		String secondRequest = String.format("		<Request>" +
"			<USUARIO>EX009005L</USUARIO>" +
"			<NIVELAS>OR</NIVELAS>" +
"			<CLIENSECAS>100029438</CLIENSECAS>" +
"			<NIVEL1 />" +
"			<CLIENSEC1 />" +
"			<NIVEL2 />" +
"			<CLIENSEC2 />" +
"			<NIVEL3>PR</NIVEL3>" +
"			<CLIENSEC3>100029438</CLIENSEC3>" +
"			<FECDES>20130224</FECDES>" +
"			<FECHAS>20130324</FECHAS>" +
"			<FECCONT />" +
"			<PRODUCCONT />" +
"			<POLIZACONT />" +
"			<OPECONT>%s</OPECONT>" +
"		</Request>",resp1109.getOpecont());
	
		result = action.IAction_Execute(secondRequest, sh, null);
//		System.out.println("Response pagina 2 = " + sh.getValue());
		
		resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertEquals("00797894",((Reg1109)resp1109.getRecords().get(0)).getPol());
		assertEquals("2",((Reg1109)resp1109.getRecords().get(0)).getOperapol());
		
		
		assertEquals("00798108",((Reg1109)resp1109.getRecords().get(4)).getPol());
		assertEquals("2",((Reg1109)resp1109.getRecords().get(4)).getOperapol());
		
		
		assertTrue(resp1109.getRecords().size() == action.getPageSize() ); 
		}
	}

	@Test
	@Ignore
	public void testCMSContinuated() throws Exception {
		if ( CurrentProfile.getProfileName().equals("test")) {

		String request = "		<Request>" +
"			<USUARIO>EX009005L</USUARIO>" +
"			<NIVELAS>OR</NIVELAS>" +
"			<CLIENSECAS>100029438</CLIENSECAS>" +
"			<NIVEL1 />" +
"			<CLIENSEC1 />" +
"			<NIVEL2 />" +
"			<CLIENSEC2 />" +
"			<NIVEL3>PR</NIVEL3>" +
"			<CLIENSEC3>100029438</CLIENSEC3>" +
"			<FECDES>20130224</FECDES>" +
"			<FECHAS>20130324</FECHAS>" +
"			<FECCONT />" +
"			<PRODUCCONT />" +
"			<POLIZACONT />" +
"			<OPECONT />" +
"		</Request>";

		
		PaginatedActionCode action = new OVOpeEmiDetallePaginada();
		// CMS devuelve 180 registros, con 200 fuerzo que busque una página más
		action.setPageSize(200);
		action.setTestPipes(this.createOneCMSPipe(request));
		
		StringHolder sh = new StringHolder();
		int result = action.IAction_Execute(request, sh, null);
		System.out.println("testIAction_ExecuteCMSContinuated: Response pagina 1 = " + sh.getValue());
		
		Response1109 resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertTrue(((Reg1109)resp1109.getRecords().get(0)).getPol().equals("00797763"));
		assertTrue(resp1109.getRecords().size() == action.getPageSize() ); 
		
		//Si me devuelve en el 181 lo mismo que en el 0, es que no pasó a la página siguiente
		assertFalse(((Reg1109)resp1109.getRecords().get(180)).getPol().equals("00797763"));
		

		String secondRequest = String.format("		<Request>" +
"			<USUARIO>EX009005L</USUARIO>" +
"			<NIVELAS>OR</NIVELAS>" +
"			<CLIENSECAS>100029438</CLIENSECAS>" +
"			<NIVEL1 />" +
"			<CLIENSEC1 />" +
"			<NIVEL2 />" +
"			<CLIENSEC2 />" +
"			<NIVEL3>PR</NIVEL3>" +
"			<CLIENSEC3>100029438</CLIENSEC3>" +
"			<FECDES>20130224</FECDES>" +
"			<FECHAS>20130324</FECHAS>" +
"			<FECCONT />" +
"			<PRODUCCONT />" +
"			<POLIZACONT />" +
"			<OPECONT>%s</OPECONT>" +
"		</Request>",resp1109.getOpecont());
	
		result = action.IAction_Execute(secondRequest, sh, null);
//		System.out.println("Response pagina 2 = " + sh.getValue());
		
		resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertTrue(resp1109.getRecords().size() == action.getPageSize() ); 
		assertEquals("00000001",((Reg1109)resp1109.getRecords().get(0)).getPol());
		assertEquals("385057",((Reg1109)resp1109.getRecords().get(0)).getCersec());
		assertEquals("10",((Reg1109)resp1109.getRecords().get(0)).getOperapol());
		
		}
	}
	
	
	@Test
	@Ignore
	public void testPaginatedInsis() throws Exception {
		if ( CurrentProfile.getProfileName().equals("test")) {

		String request = "		<Request>" +
"			<USUARIO>EX009005L</USUARIO>" +
"			<NIVELAS>OR</NIVELAS>" +
"			<CLIENSECAS>100029438</CLIENSECAS>" +
"			<NIVEL1 />" +
"			<CLIENSEC1 />" +
"			<NIVEL2 />" +
"			<CLIENSEC2 />" +
"			<NIVEL3>PR</NIVEL3>" +
"			<CLIENSEC3>100029438</CLIENSEC3>" +
"			<FECDES>20130224</FECDES>" +
"			<FECHAS>20130324</FECHAS>" +
"			<FECCONT />" +
"			<PRODUCCONT />" +
"			<POLIZACONT />" +
"			<OPECONT />" +
"		</Request>";

		PaginatedActionCode action = new OVOpeEmiDetallePaginada();
		action.setPageSize(5);
		action.setTestPipes(this.createOneInsisPipe(request));

		StringHolder sh = new StringHolder();
		int result = action.IAction_Execute(request, sh, null);
//		System.out.println("Response pagina 1 = " + sh.getValue());
		
		Response1109 resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertTrue(resp1109.getRecords().size() == action.getPageSize() ); 
		assertTrue(((Reg1109)resp1109.getRecords().get(0)).getPol().equals("00000001"));
		assertTrue(((Reg1109)resp1109.getRecords().get(0)).getCersec().equals("800015"));
		assertTrue(((Reg1109)resp1109.getRecords().get(0)).getOperapol().equals("3000002353"));
		
		assertTrue(((Reg1109)resp1109.getRecords().get(4)).getPol().equals("00000001"));
		assertTrue(((Reg1109)resp1109.getRecords().get(4)).getCersec().equals("800025"));
		assertTrue(((Reg1109)resp1109.getRecords().get(4)).getOperapol().equals("0"));

		String secondRequest = String.format("		<Request>" +
"			<USUARIO>EX009005L</USUARIO>" +
"			<NIVELAS>OR</NIVELAS>" +
"			<CLIENSECAS>100029438</CLIENSECAS>" +
"			<NIVEL1 />" +
"			<CLIENSEC1 />" +
"			<NIVEL2 />" +
"			<CLIENSEC2 />" +
"			<NIVEL3>PR</NIVEL3>" +
"			<CLIENSEC3>100029438</CLIENSEC3>" +
"			<FECDES>20130224</FECDES>" +
"			<FECHAS>20130324</FECHAS>" +
"			<FECCONT />" +
"			<PRODUCCONT />" +
"			<POLIZACONT />" +
"			<OPECONT>%s</OPECONT>" +
"		</Request>",resp1109.getOpecont());
	
		result = action.IAction_Execute(secondRequest, sh, null);
//		System.out.println("Response pagina 2 = " + sh.getValue());
		
		resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertTrue(resp1109.getRecords().size() == action.getPageSize() ); 

		assertTrue(((Reg1109)resp1109.getRecords().get(0)).getPol().equals("00000001"));
		assertTrue(((Reg1109)resp1109.getRecords().get(0)).getCersec().equals("800025"));
		assertTrue(((Reg1109)resp1109.getRecords().get(0)).getOperapol().equals("3000002326"));
		
		assertTrue(((Reg1109)resp1109.getRecords().get(4)).getPol().equals("00000001"));
		assertTrue(((Reg1109)resp1109.getRecords().get(4)).getCersec().equals("800026"));
		assertTrue(((Reg1109)resp1109.getRecords().get(4)).getOperapol().equals("3000002333"));
		}
	}

	
	@Test
	public void testCombined() throws Exception {
		if ( CurrentProfile.getProfileName().equals("test")) {

		String request = "		<Request>" +
"			<USUARIO>EX009005L</USUARIO>" +
"			<NIVELAS>OR</NIVELAS>" +
"			<CLIENSECAS>100029438</CLIENSECAS>" +
"			<NIVEL1 />" +
"			<CLIENSEC1 />" +
"			<NIVEL2 />" +
"			<CLIENSEC2 />" +
"			<NIVEL3>PR</NIVEL3>" +
"			<CLIENSEC3>100029438</CLIENSEC3>" +
"			<FECDES>20130224</FECDES>" +
"			<FECHAS>20130324</FECHAS>" +
"			<FECCONT />" +
"			<PRODUCCONT />" +
"			<POLIZACONT />" +
"			<OPECONT />" +
"		</Request>";

		PaginatedActionCode action = new OVOpeEmiDetallePaginada();
		action.setPageSize(5);

		StringHolder sh = new StringHolder();
		int result = action.IAction_Execute(request, sh, null);
//		System.out.println("Response pagina 1 = " + sh.getValue());
		
		Response1109 resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertTrue(resp1109.getRecords().size() == action.getPageSize() ); 

		
		assertEquals("AUA1",((Reg1109)resp1109.getRecords().get(0)).getProd());
		assertEquals("00797763",((Reg1109)resp1109.getRecords().get(0)).getPol());
		assertEquals("000000",((Reg1109)resp1109.getRecords().get(0)).getCersec());
		assertEquals("4",((Reg1109)resp1109.getRecords().get(0)).getOperapol());
		
		assertEquals("AUA1",((Reg1109)resp1109.getRecords().get(4)).getProd());
		assertEquals("00798108",((Reg1109)resp1109.getRecords().get(4)).getPol());
		assertEquals("000000",((Reg1109)resp1109.getRecords().get(4)).getCersec());
		assertEquals("2",((Reg1109)resp1109.getRecords().get(4)).getOperapol());

		String secondRequest = String.format("		<Request>" +
"			<USUARIO>EX009005L</USUARIO>" +
"			<NIVELAS>OR</NIVELAS>" +
"			<CLIENSECAS>100029438</CLIENSECAS>" +
"			<NIVEL1 />" +
"			<CLIENSEC1 />" +
"			<NIVEL2 />" +
"			<CLIENSEC2 />" +
"			<NIVEL3>PR</NIVEL3>" +
"			<CLIENSEC3>100029438</CLIENSEC3>" +
"			<FECDES>20130224</FECDES>" +
"			<FECHAS>20130324</FECHAS>" +
"			<FECCONT />" +
"			<PRODUCCONT />" +
"			<POLIZACONT />" +
"			<OPECONT>%s</OPECONT>" +
"		</Request>",resp1109.getOpecont());
	
		result = action.IAction_Execute(secondRequest, sh, null);
//		System.out.println("Response pagina 2 = " + sh.getValue());
		
		resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertTrue(resp1109.getRecords().size() == action.getPageSize() ); 

		assertEquals("AUS1",((Reg1109)resp1109.getRecords().get(0)).getProd());
		assertEquals("00000001",((Reg1109)resp1109.getRecords().get(0)).getPol());
		assertEquals("311846",((Reg1109)resp1109.getRecords().get(0)).getCersec());
		assertEquals("16",((Reg1109)resp1109.getRecords().get(0)).getOperapol());
		
		assertEquals("AUS1",((Reg1109)resp1109.getRecords().get(4)).getProd());
		assertEquals("00000001",((Reg1109)resp1109.getRecords().get(4)).getPol());
		assertEquals("348020",((Reg1109)resp1109.getRecords().get(4)).getCersec());
		assertEquals("12",((Reg1109)resp1109.getRecords().get(4)).getOperapol());
		}
	}

	
	@Test
	public void testFullNavigationCMSTest() throws Exception {
		String requestTemplate = REQUEST_TEMPLATE;
		String request = String.format(requestTemplate, "");
		PaginatedActionCode action = new OVOpeEmiDetallePaginada();
		action.setPageSize(180);
		
		action.setTestPipes(this.createOneCMSTestPipe());
		
		StringHolder sh = new StringHolder();
		int result = action.IAction_Execute(request, sh, null);
		Response1109 resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertFalse("Vino false", resp1109.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
		
		ContinuationStatus paginateFlag = resp1109.getContinuationStatus();
		int numPages = 0;
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR)) {
			numPages++;

			String nextReq = String.format(requestTemplate,resp1109.getOpecont());
			result = action.IAction_Execute(nextReq, sh, null);
			resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
			assertFalse("Vino false", resp1109.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
			paginateFlag = resp1109.getContinuationStatus();
		}
		assertTrue("No recorrió las páginas que debería", numPages == 9);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
	}
		
	private List<Pipe> createOneCMSPipe(String request) throws Exception {

		Request1107x9 request1109 = (Request1107x9) (new Request1107x9()).unmarshall(request);

		List<Pipe> pipes = new ArrayList<Pipe>();
		MessagePipe cms1 = new CMSPipe();
		cms1.setRequest(request1109);
		cms1.setResponseUnmarshaller(new Response1109Unmarshaller());
		cms1.setActionCode(OVOpeEmiDetallePaginada.ACTION_CODE_1109);
		pipes.add(cms1);
		return pipes;
	}

	private List<Pipe> createOneEmptyInsisPipe() {
		List<Pipe> pipes = new ArrayList<Pipe>();
		MessagePipe pipe = new InsisEmptyPipe();
		pipe.setResponseUnmarshaller(new Response1109Unmarshaller());
		pipe.setActionCode(OVOpeEmiDetallePaginada.ACTION_CODE_1109);
		pipes.add(pipe);

		return pipes;
	}


	private List<Pipe> createOneInsisPipe(String request) throws Exception {

		Request1107x9 request1109 = (Request1107x9) (new Request1107x9()).unmarshall(request);

		List<Pipe> pipes = new ArrayList<Pipe>();
		MessagePipe pipe = new Insis1109Pipe();
		pipe.setRequest(request1109);
		pipe.setResponseUnmarshaller(new Response1109Unmarshaller());
		pipe.setActionCode(OVOpeEmiDetallePaginada.ACTION_CODE_1109);
		pipes.add(pipe);

		return pipes;
	}

	private List<Pipe> createTwoCMSPipes(String request) throws Exception {

		Request1107x9 request1109 = (Request1107x9) (new Request1107x9()).unmarshall(request);

		List<Pipe> pipes = new ArrayList<Pipe>();
		MessagePipe cms1 = new CMSPipe();
		cms1.setRequest(request1109);
		cms1.setResponseUnmarshaller(new Response1109Unmarshaller());
		cms1.setActionCode(OVOpeEmiDetallePaginada.ACTION_CODE_1109);
		pipes.add(cms1);

		MessagePipe cms2 = new CMSPipe();
		cms2.setRequest(request1109);
		cms2.setResponseUnmarshaller(new Response1109Unmarshaller());
		cms2.setActionCode(OVOpeEmiDetallePaginada.ACTION_CODE_1109);
		pipes.add(cms2);

		return pipes;
	}

	@Test
	public void testMarshalResponse1109() throws JAXBException {
		ContinuationResponse resp = new Response1109();
		resp.setEstado(Estado.newWithResultadoFalse());
		Reg1109 reg1 = new Reg1109();
		reg1.setCerpol("23");
		resp.addRecord(reg1);
		String s = resp.marshal();
		assertTrue(s.contains("<Response>"));
		assertTrue(s.contains("<REGS>"));
		assertTrue(s.contains("<REG>"));
	}

	private List<Pipe> createTestPipes() {
		List<Pipe> pipes = new ArrayList<Pipe>();
		CMS1109MockPipe cms1 = new CMS1109MockPipe();
		pipes.add(cms1);

		CMS1109MockPipe cms2 = new CMS1109MockPipe();
		pipes.add(cms2);
		
		return pipes;
	}

	private List<Pipe> createOneCMSTestPipe() {
		List<Pipe> pipes = new ArrayList<Pipe>();
		CMS1109MockPipe cms1 = new CMS1109MockPipe();
		pipes.add(cms1);

		return pipes;
	}
	
}
