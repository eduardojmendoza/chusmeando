package com.qbe.services.paginate;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.CMSPipePointer;
import com.qbe.services.paginate.PipePointer;

public class MockPipePointer extends PipePointer {

	private int block;
	
	private PipePointer pipePointer;
	
	private int executeRequestCount;
	
	public int getExecuteRequestCount() {
		return executeRequestCount;
	}

	public void setExecuteRequestCount(int executeRequestCount) {
		this.executeRequestCount = executeRequestCount;
	}

	public MockPipePointer() {
	}

	public int getBlock() {
		return block;
	}

	public void setBlock(int block) {
		this.block = block;
	}

	public PipePointer getPipePointer() {
		return pipePointer;
	}

	public void setPipePointer(PipePointer pipePointer) {
		this.pipePointer = pipePointer;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	
}
