package com.qbe.services.paginate.service1301;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.InsisEmptyPipe;
import com.qbe.services.paginate.InsisMockPipe;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MessagePipe;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;
import com.qbe.vbcompat.framework.jaxb.Estado;
import com.qbe.vbcompat.string.StringHolder;

public class OVSiniListadoDetallesTest {

	private static Logger logger = Logger.getLogger(OVSiniListadoDetallesTest.class.getName());

	
	private static final String REQUEST_BASICO = "<Request>" + 
 "    <USUARIO>EX009005L</USUARIO>" + 
 "    <NIVELAS>GO</NIVELAS>" + 
 "    <CLIENSECAS>100029438</CLIENSECAS>" + 
 "    <NIVEL1>GO</NIVEL1>" + 
 "    <CLIENSEC1>100029438</CLIENSEC1>" + 
 "    <NIVEL2/>" + 
 "    <CLIENSEC2/>" + 
 "    <NIVEL3/>" + 
 "    <CLIENSEC3/>" + 
 "    <FECDES>20140104</FECDES>" + 
 "    <FECHAS>20140404</FECHAS>" + 
 "    <MSGEST/>" + 
 "    <CONTINUAR/>" + 
 "</Request>";

	/**
	 * Testea que el request a CMS salga bien formado
	 * Bug 1672
	 * @throws JAXBException 
	 */
	@Test
	public void testCMSRequestFormat() throws JAXBException {
		
		Request1301 r1301 = (Request1301) (new Request1301()).unmarshall(REQUEST_BASICO);

		String actualRequestValue = r1301.marshall();
		assertFalse("Manda USUARCOD", actualRequestValue.contains("<USUARCOD>"));
		assertTrue("No manda <USUARIO>EX009005L</USUARIO>", actualRequestValue.contains("<USUARIO>EX009005L</USUARIO>"));
		assertTrue("No manda MSGEST", actualRequestValue.contains("<MSGEST>"));
	}
	
	
	/**
	 * Pagina contra CMS(AIS), revisando que se esté mandando el CONTINUAR que necesita AIS
	 * El pipe de insis está vacío
	 * @throws Exception
	 */
	@Test
	public void testAISContinuar() throws Exception {
		int pageSize = 390;
		double numRegs = CMS1301MockPipe.RESPONSE_1301_1_REGS_SIZE;
		
		String request = REQUEST_BASICO;
		PaginatedActionCode action = new OVSiniListadoDetalles();
		List<Pipe> pipes = new ArrayList<Pipe>();
		CMS1301MockPipe cms1 = new CMS1301MockPipe();
		pipes.add(cms1);
		
		MessagePipe pipe = new InsisEmptyPipe();
		pipe.setResponseUnmarshaller(new Response1301Unmarshaller());
		pipe.setActionCode(OVSiniListadoDetalles.ACTION_CODE_1301);
		pipes.add(pipe);
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		//Lamada 1ra página
		action.IAction_Execute(request, sh, null);
		Response1301 resp1301 = (Response1301) (new Response1301()).unmarshal(sh.getValue());
		validateResponse1301(resp1301);
		ContinuationStatus paginateFlag = resp1301.getContinuationStatus();
		int numPages = 1;
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR)) {
			numPages++;
			assertEquals("Una página intermedia no trajo todos los registros pedidos", pageSize, resp1301.getRecords().size());

			String nextReq = request;
			action.IAction_Execute(nextReq, sh, null);
			resp1301 = (Response1301) (new Response1301()).unmarshal(sh.getValue());
			validateResponse1301(resp1301);
			
			//Valido que haya enviado el continuar de AIS
			Request1301 actualRequest = cms1.getLastExecutedRequest();
			String actualRequestValue = actualRequest.marshall();
			assertTrue("NO manda el CONTINUAR de AIS, el request fue:" + actualRequestValue, actualRequestValue.contains("<CONTINUAR>GO100029438  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  0000000000120140112AUS110305721</CONTINUAR>"));
			
			paginateFlag = resp1301.getContinuationStatus();
		}
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));

		//De acá en adelante, tenemos los datos de la última página solamente		
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1301.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		
	}
		
	
	
	
	/**
	 * Recorre una query completa, paginando claro.
	 * Usa un pipe local de CMS, y uno local de Insis
	 * 
	 * @throws Exception
	 */
	@Test
	@Ignore //No tenemos datos de Insis aún
	public void testFullNavigationCMSLocalInsisLocal() throws Exception {
		int pageSize = 30;	
		double numRegs = CMS1301MockPipe.RESPONSE_1301_1_REGS_SIZE + 52;
		
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		
		String request = REQUEST_BASICO;
		
		PaginatedActionCode action = new OVSiniListadoDetalles();
		List<Pipe> pipes = this.createOneCMSTestPipe();
		pipes.addAll(this.createOneInsisTestPipe());
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		action.IAction_Execute(request, sh, null);
		Response1301 resp1301 = (Response1301) (new Response1301()).unmarshal(sh.getValue());
		validateResponse1301(resp1301);
		ContinuationStatus paginateFlag = resp1301.getContinuationStatus();
		int numPages = 1;
		logger.info("Pagina: "+ numPages + " trae " + resp1301.getRecords().size());
		logger.info("Continuar: " + resp1301.getContinuar() + "  Continuacion " +  paginateFlag);
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR )) {
				numPages++;
				String nextReq = request;
				
				action.IAction_Execute(nextReq, sh, null);
				resp1301 = (Response1301) (new Response1301()).unmarshal(sh.getValue());
				
				logger.info("CONTINUAR: " + resp1301.getContinuar() + " ESTATUS: " + resp1301.getContinuationStatus());
				logger.info("Pagina: "+ numPages + " trae " + resp1301.getRecords().size());
				validateResponse1301(resp1301);
				paginateFlag = resp1301.getContinuationStatus();
		}
		
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1301.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
	}
	
	/**
	 * Recorre una query completa, paginando claro.
	 * Usa 11 pipes locales de CMS, y un pipe vacío de Insis
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFullNavigationMultiCMSLocal() throws Exception {
		int pageSize = 30;	
		int pipeCount = 11;
		double numRegs = pipeCount * CMS1301MockPipe.RESPONSE_1301_1_REGS_SIZE;
		
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		
		String request = REQUEST_BASICO;
		
		PaginatedActionCode action = new OVSiniListadoDetalles();
		List<Pipe> pipes = this.createCMSTestPipe(pipeCount);
		pipes.addAll(this.createOneEmptyInsisPipe());
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		action.IAction_Execute(request, sh, null);
		Response1301 resp1301 = (Response1301) (new Response1301()).unmarshal(sh.getValue());
		validateResponse1301(resp1301);
		ContinuationStatus paginateFlag = resp1301.getContinuationStatus();
		int numPages = 1;
		logger.info("Pagina: "+ numPages + " trae " + resp1301.getRecords().size());
		logger.info("Continuar: " + resp1301.getContinuar() + "  Continuacion " +  paginateFlag);
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR )) {
				numPages++;
				String nextReq = request;
				
				CMS1301MockPipe cp = (CMS1301MockPipe)action.getTestPipes().get(0);
				logger.info("CMS MOCK PIPE: tiene mas " + cp.hasNext());
				
				
				action.IAction_Execute(nextReq, sh, null);
				resp1301 = (Response1301) (new Response1301()).unmarshal(sh.getValue());
				
				cp = (CMS1301MockPipe)action.getTestPipes().get(0);
				logger.info("CMS MOCK PIPE: tiene mas " + cp.hasNext());			
				logger.info("CONTINUAR: " + resp1301.getContinuar() + " ESTATUS: " + resp1301.getContinuationStatus());
				logger.info("Pagina: "+ numPages + " trae " + resp1301.getRecords().size());
				validateResponse1301(resp1301);
				paginateFlag = resp1301.getContinuationStatus();
		}
		
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1301.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
	}
	
	
	/**
	 * Recorre una query completa, paginando claro.
	 * El pipe de insis está vacío
	 * @throws Exception
	 */
	@Test
	public void testFullNavigationCMSLocalEmptyInsis() throws Exception {
		int pageSize = 2;
		double numRegs = CMS1301MockPipe.RESPONSE_1301_1_REGS_SIZE;
		
		String request = REQUEST_BASICO;
		PaginatedActionCode action = new OVSiniListadoDetalles();
		List<Pipe> pipes = this.createOneCMSTestPipe();
		pipes.addAll(this.createOneEmptyInsisPipe());
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		//Lamada 1ra página
		action.IAction_Execute(request, sh, null);
		Response1301 resp1301 = (Response1301) (new Response1301()).unmarshal(sh.getValue());
		validateResponse1301(resp1301);
		ContinuationStatus paginateFlag = resp1301.getContinuationStatus();
		int numPages = 1;
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR)) {
			numPages++;
			assertEquals("Una página intermedia no trajo todos los registros pedidos", pageSize, resp1301.getRecords().size());

			String nextReq = request;
			action.IAction_Execute(nextReq, sh, null);
			resp1301 = (Response1301) (new Response1301()).unmarshal(sh.getValue());
			validateResponse1301(resp1301);
			paginateFlag = resp1301.getContinuationStatus();
		}
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));

		//De acá en adelante, tenemos los datos de la última página solamente		
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1301.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		
	}
	
	
	private void validateResponse1301(Response1301 resp1301) {
		assertFalse("Vino false", resp1301.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
		assertTrue("El response vino sin Request de continuación", resp1301.getContinuar() != null);		
	}



	private List<Pipe> createOneEmptyInsisPipe() {
		List<Pipe> pipes = new ArrayList<Pipe>();
		MessagePipe pipe = new InsisEmptyPipe();
		pipe.setResponseUnmarshaller(new Response1301Unmarshaller());
		pipe.setActionCode(OVSiniListadoDetalles.ACTION_CODE_1301);
		pipes.add(pipe);

		return pipes;
	}
	

	private List<Pipe> createOneCMSTestPipe() {
		List<Pipe> pipes = new ArrayList<Pipe>();
		CMS1301MockPipe cms1 = new CMS1301MockPipe();
		pipes.add(cms1);

		return pipes;
	}

	private List<Pipe> createOneInsisTestPipe() throws IOException, JAXBException {
		List<Pipe> pipes = new ArrayList<Pipe>();

		String _52_REG_FILE_NAME = "1301/1301_insis_52_resp_full.xml";

		InsisPipe pipe = new InsisMockPipe(_52_REG_FILE_NAME, Request1301.class, Response1301Unmarshaller.class, Response1301.class, OVSiniListadoDetalles.ACTION_CODE_1301);
		pipes.add(pipe);

		return pipes;
	}

	private List<Pipe> createCMSTestPipe(int count) {
		List<Pipe> pipes = new ArrayList<Pipe>();
		for (int i = 0; i < count; i++) {
			pipes.add(new CMS1301MockPipe());
		}
		return pipes;
	}
	
}
