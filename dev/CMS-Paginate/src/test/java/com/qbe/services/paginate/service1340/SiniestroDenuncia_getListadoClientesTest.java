package com.qbe.services.paginate.service1340;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.junit.Assume;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.InsisMockPipe;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;
import com.qbe.services.paginate.service1428.Response1428;
import com.qbe.services.paginate.siniMQgenerico.siniestroDenuncia_getListadoClientes;
import com.qbe.vbcompat.string.StringHolder;

public class SiniestroDenuncia_getListadoClientesTest {

	private static Logger logger = Logger.getLogger(SiniestroDenuncia_getListadoClientesTest.class.getName());

	
	private static final String BASIC_REQUEST = "		<Request>" +
			"			<DEFINICION>siniestroDenuncia_getListadoClientes.xml</DEFINICION>" +
			"				<USUARCOD>EX009005L</USUARCOD>" +
			"			    <NIVELCLAS>PR</NIVELCLAS>" +
			"		 		<CLIENSECAS>100072343</CLIENSECAS>" +
			"				<NIVELCLA1/>" +
			"				<CLIENSEC1/>" +
			"				<NIVELCLA2/>" +
			"				<CLIENSEC2/>" +
			"				<NIVELCLA3/>" +
			"				<CLIENSEC3/>" +
			"				<RAMO><![CDATA[]]></RAMO>" +
			"				<POLIZA><![CDATA[]]></POLIZA>" +
			"				<CERTIF><![CDATA[]]></CERTIF>" +
			"				<PATENTE><![CDATA[]]></PATENTE>" +
			"				<APELLIDO><![CDATA[DONNA]]></APELLIDO>" +
			"           	<DOCUMTIP/>" +
			"           	<DOCUMDAT><![CDATA[]]></DOCUMDAT>" +
			"           	<FECHASINIE>20130810</FECHASINIE>" +
			"           	<TIPOSINIE>AU</TIPOSINIE>" +
			"           	<SWBUSCA>N</SWBUSCA>" +
			"           	<DOCUMTIP/>" +
			"           	<ORDEN>A</ORDEN>" +
			"           	<COLUMNA>1</COLUMNA>" +
			"           	<NROQRY>0</NROQRY>" +
			"           	<RETOMA/>" +
			"           	<RETORNARREQUEST/>" +
			"		</Request>";

	
	
	/**
	 * Recorre una query completa, paginando claro.
	 * Usa un pipe local de CMS, y uno local de Insis
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFullNavigationCMSLocalInsisLocal() throws Exception {
		int pageSize = 200;	
		double numRegs = 1 + 4;
		
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		
		String request = BASIC_REQUEST;
		
		PaginatedActionCode action = new siniestroDenuncia_getListadoClientes();
		List<Pipe> pipes = this.createOneCMSTestPipe();
		pipes.addAll(this.createOneInsisTestPipe());
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		action.IAction_Execute(request, sh, null);
		
		Response1340 resp1340 = (Response1340) (new Response1340()).unmarshal(sh.getValue());
	//	validateResponse1340(resp1340);
		ContinuationStatus paginateFlag = resp1340.getContinuationStatus();
		int numPages = 1;
		logger.info("Pagina: "+ numPages + " trae " + resp1340.getRecords().size());
		logger.info("Continua: " + resp1340.getRequest().getEstado().toString() + "  Continuacion " +  paginateFlag);
	//	assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR )) {
				numPages++;
				String nextReq = resp1340.getCampos().getRequest().marshall();
				
				action.IAction_Execute(nextReq, sh, null);
				resp1340 = (Response1340) (new Response1340()).unmarshal(sh.getValue());
				
				logger.info("CONTINUA: " + resp1340.getRequest().getEstado() + " ESTATUS: " + resp1340.getContinuationStatus());
				logger.info("Pagina: "+ numPages + " trae " + resp1340.getRecords().size());
//				validateResponse1340(resp1340);
				paginateFlag = resp1340.getContinuationStatus();
				logger.info("Continua: " + resp1340.getRequest().getEstado());
		}
		
		//Ignoro porque no sé la cantidad
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1340.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
	}
	
	private List<Pipe> createOneCMSTestPipe() {
		List<Pipe> pipes = new ArrayList<Pipe>();
		CMS1340MockPipe cms1 = new CMS1340MockPipe();
		pipes.add(cms1);

		return pipes;
	}
	
	private List<Pipe> createOneInsisTestPipe() throws IOException, JAXBException {
		List<Pipe> pipes = new ArrayList<Pipe>();
		
		String REG_FILE_NAME = "1340/insis/1340_insis_resp.xml";
		
		InsisPipe pipe = new InsisMockPipe(REG_FILE_NAME, Request1340.class, Response1340Unmarshaller.class, Response1340.class, siniestroDenuncia_getListadoClientes.ACTION_CODE_1340);
		pipes.add(pipe);

		return pipes;
	}
	
	/**
	 * Verificar que este armando correctamente el xml de respuesta
	 * @throws Exception
	 */
	@Test
	public void testUnmarshal (){
		try {
			Response1340 resp1340 = (Response1340) (new Response1340()).unmarshal(RESPGENERAL);
		  logger.info("Resultado : " + resp1340.getEstado().getResultado());
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private static final String RESPGENERAL = "		<Response>" +
			"<Estado 	resultado=\"true\" mensaje=\"\"/>" +
					"<CAMPOS>" +
					"<CANT>1</CANT>" +
					"<ResultSetID/>" +
					"<ITEMS>" +
					"<ITEM>" +
					"<RAMOPCOD>AUS1</RAMOPCOD>" +
					"<POLIZANN>00</POLIZANN>" +
					"<POLIZSEC>000001</POLIZSEC>" +
					"<CERTIPOL>0000</CERTIPOL>" +
					"<CERTIANN>0001</CERTIANN>" +
					"<CERTISEC>800373</CERTISEC>" +
					"<CLIENDES>DONNARUMMA , ANA CRISTINA </CLIENDES>" +
					"<TOMARIES>AUDI A3 *51269</TOMARIES>" +
					"<PATENNUM>NMN512</PATENNUM>" +
					"<SITUCPOL>VIGENTE</SITUCPOL>" +
					"<SWDENHAB>S</SWDENHAB>" +
					"<SWSINIES>S</SWSINIES>" +
					"<SWEXIGIB>0</SWEXIGIB>" +
					"<RAMO>1</RAMO>" +
					"<VIGDESDE>20130801</VIGDESDE>" +
					"<VIGHASTA>20140731</VIGHASTA>" +
					"<FULTSTRO>20140728</FULTSTRO>" +
					"</ITEM>" +
					"</ITEMS>" +
					"</CAMPOS>" +
					"</Response>";
}
