package com.qbe.services.paginate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import com.qbe.services.paginate.service1109.OVOpeEmiDetallePaginada;
import com.qbe.services.paginate.service1109.Response1109;
import com.qbe.services.paginate.service1109.Response1109Unmarshaller;
import com.qbe.services.paginate.service1109.mocks.CMS1109MockPipe;

public class Pipes1107x09Factory {

	public static List<Pipe> create2InsisTestPipes(int pageSize) throws IOException, JAXBException {
		return createInsisPipes(2,pageSize,Pipes1107x09Factory._200_REG_FILE_NAME);
	}

	private static List<Pipe> createInsisPipes(int cant, int pageSize, String regsFileName)
			throws IOException, JAXBException {
		List<Pipe> pipes = new ArrayList<Pipe>();
		for (int i = 0; i < cant; i++) {
			InsisMockPipe p1 = new InsisMockPipe(regsFileName, Request1107x9.class, Response1109Unmarshaller.class, Response1109.class, OVOpeEmiDetallePaginada.ACTION_CODE_1109);
			p1.setRowCount(pageSize);
			pipes.add(p1);
		}
		return pipes;
	}

	public static List<Pipe> create3InsisTestPipes(int pageSize) throws IOException, JAXBException {
		return createInsisPipes(3,pageSize,Pipes1107x09Factory._200_REG_FILE_NAME);
	}

	public static List<Pipe> createInsisTestPipe(int pageSize) throws IOException, JAXBException {
		return createInsisPipes(1,pageSize,Pipes1107x09Factory._200_REG_FILE_NAME);
	}

	public static List<Pipe> create1RegInsisTestPipe(int pageSize) throws IOException, JAXBException {
		return createInsisPipes(1,pageSize,Pipes1107x09Factory._1_REG_FILE_NAME);
	}

	public static List<Pipe> create3x1RegInsisTestPipe(int pageSize) throws IOException, JAXBException {
		return createInsisPipes(3,pageSize,Pipes1107x09Factory._1_REG_FILE_NAME);
	}


	public static List<Pipe> createCMSTestPipes() {
		List<Pipe> pipes = new ArrayList<Pipe>();
		Pipe p1 = new CMS1109MockPipe();
		pipes.add(p1);
	
		Pipe p2 = new CMS1109MockPipe();
		pipes.add(p2);
		
		return pipes;
	}

	public static final String REQUEST_TEMPLATE = "		<Request>" +
	"			<USUARIO>EX009005L</USUARIO>" +
	"			<NIVELAS>OR</NIVELAS>" +
	"			<CLIENSECAS>100029438</CLIENSECAS>" +
	"			<NIVEL1 />" +
	"			<CLIENSEC1 />" +
	"			<NIVEL2 />" +
	"			<CLIENSEC2 />" +
	"			<NIVEL3>PR</NIVEL3>" +
	"			<CLIENSEC3>100029438</CLIENSEC3>" +
	"			<FECDES>20130225</FECDES>" +
	"			<FECHAS>20130226</FECHAS>" +
	"			<FECCONT />" +
	"			<PRODUCCONT />" +
	"			<POLIZACONT />" +
	"			<OPECONT>%s</OPECONT>" +
	"		</Request>";
	public static final String _200_REG_FILE_NAME = "seq/insisResp1_reg.txt";
	public static final String _1_REG_FILE_NAME = "seq/insisResp2_reg.txt";

}
