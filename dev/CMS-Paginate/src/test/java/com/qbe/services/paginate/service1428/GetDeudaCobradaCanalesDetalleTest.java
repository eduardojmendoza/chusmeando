package com.qbe.services.paginate.service1428;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.util.StreamUtils;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.InsisEmptyPipe;
import com.qbe.services.paginate.InsisMockPipe;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MessagePipe;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;
import com.qbe.services.paginate.mqgestion.GetDeudaCobradaCanalesDetalle;
import com.qbe.vbcompat.framework.jaxb.Estado;
import com.qbe.vbcompat.string.StringHolder;

public class GetDeudaCobradaCanalesDetalleTest {

	private static Logger logger = Logger.getLogger(GetDeudaCobradaCanalesDetalleTest.class.getName());

	
	private static final String REQUEST_BASICO = "		<Request>" +
	        "<DEFINICION>GetDeudaCobradaCanalesDetalle.xml</DEFINICION>"+
	        "<USUARCOD>EX009005L</USUARCOD>"+
	        "<NIVELAS>PR</NIVELAS>"+
	        "<CLIENSECAS>100029438</CLIENSECAS>"+
	        "<NIVELCLA1></NIVELCLA1>"+
	        "<CLIENSEC1></CLIENSEC1>"+
	        "<NIVELCLA2></NIVELCLA2>"+
	        "<CLIENSEC2></CLIENSEC2>"+
	        "<NIVELCLA3></NIVELCLA3>"+
	        "<CLIENSEC3></CLIENSEC3>"+
	        "<FECHADESDE>14/8/2014</FECHADESDE>"+
	        "<FECHAHASTA>14/9/2014</FECHAHASTA>"+
	        "<COBROCOD-E>4</COBROCOD-E>"+
	        "<REORDEN>N</REORDEN>"+
	        "<COLUMNAORDEN>1</COLUMNAORDEN>"+
	        "<ASCDESC>A</ASCDESC>"+
	        "<CLAVEBACK></CLAVEBACK>"+
	        "<CLAVEBACK2></CLAVEBACK2>"+
	        "<RETORNARREQUEST></RETORNARREQUEST>"+
"		</Request>";
	
	private static final String REQUEST_XSL = "		<Request>" +
	        "<DEFINICION>GetDeudaCobradaCanalesDetalle.xml</DEFINICION>"+
			"<AplicarXSL>GetDeudaCobradaCanalesDetalleXLS.xsl</AplicarXSL>"+
	        "<USUARCOD>EX009005L</USUARCOD>"+
	        "<NIVELAS>GO</NIVELAS>"+
	        "<CLIENSECAS>100029438</CLIENSECAS>"+
	        "<NIVELCLA1>GO</NIVELCLA1>"+
	        "<CLIENSEC1>100029438</CLIENSEC1>"+
	        "<NIVELCLA2></NIVELCLA2>"+
	        "<CLIENSEC2></CLIENSEC2>"+
	        "<NIVELCLA3></NIVELCLA3>"+
	        "<CLIENSEC3></CLIENSEC3>"+
	        "<FECHADESDE>15/7/2013</FECHADESDE>"+
	        "<FECHAHASTA>15/8/2013</FECHAHASTA>"+
	        "<PAGINASRETORNADAS>0</PAGINASRETORNADAS>"+
	        "<COBROCOD-E>5</COBROCOD-E>"+
	        "<ESEXCEL>S</ESEXCEL>"+
"		</Request>";

	private static final String REQUEST_TXT = "		<Request>" +
	        "<DEFINICION>GetDeudaCobradaCanalesDetalle.xml</DEFINICION>"+
			"<AplicarXSL>GetDeudaCobradaCanalesDetalleTXT.xsl</AplicarXSL>"+
	        "<USUARCOD>EX009005L</USUARCOD>"+
	        "<NIVELAS>GO</NIVELAS>"+
	        "<CLIENSECAS>100029438</CLIENSECAS>"+
	        "<NIVELCLA1>GO</NIVELCLA1>"+
	        "<CLIENSEC1>100029438</CLIENSEC1>"+
	        "<NIVELCLA2></NIVELCLA2>"+
	        "<CLIENSEC2></CLIENSEC2>"+
	        "<NIVELCLA3></NIVELCLA3>"+
	        "<CLIENSEC3></CLIENSEC3>"+
	        "<FECHADESDE>15/7/2013</FECHADESDE>"+
	        "<FECHAHASTA>15/8/2013</FECHAHASTA>"+
	        "<PAGINASRETORNADAS>0</PAGINASRETORNADAS>"+
	        "<COBROCOD-E>5</COBROCOD-E>"+
	        "<ESEXCEL>S</ESEXCEL>"+
"		</Request>";

	private static final String REQUEST_INSIS = "<Request>"+
        "<DEFINICION>GetDeudaCobradaCanalesDetalle.xml</DEFINICION>"+
        "<USUARCOD>EX009008L</USUARCOD>"+
        "<NIVELAS>GO</NIVELAS>"+
        "<CLIENSECAS>100029438</CLIENSECAS>"+
        "<NIVELCLA1></NIVELCLA1>"+
        "<CLIENSEC1></CLIENSEC1>"+
        "<NIVELCLA2></NIVELCLA2>"+
        "<CLIENSEC2></CLIENSEC2>"+
        "<NIVELCLA3></NIVELCLA3>"+
        "<CLIENSEC3></CLIENSEC3>"+
        "<FECHADESDE>14/08/2014</FECHADESDE>"+
        "<FECHAHASTA>20/9/2014</FECHAHASTA>"+
        "<COBROCOD-E>4</COBROCOD-E>"+
        "<REORDEN>N</REORDEN>"+
        "<COLUMNAORDEN>1</COLUMNAORDEN>"+
        "<ASCDESC>A</ASCDESC>"+
        "<CLAVEBACK></CLAVEBACK>"+
        "<CLAVEBACK2></CLAVEBACK2>"+
        "<RETORNARREQUEST></RETORNARREQUEST>"+
    "</Request>";
	
	
	
	/**
	 * Bug 1667: Recorre un resultset que tiene resultados de Insis y CMS, y los resultados de Insis no 
	 * entran en la primer página xq el resultset está ordenado alfabéticamente y el resultset de Insis arranca en MORELI
	 * 
	 * @throws Exception
	 */
	@Test
	public void testBug1667UAT() throws Exception {
		
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("uat"));
		
		String request = this.getRequest1667();
		
		PaginatedActionCode action = new GetDeudaCobradaCanalesDetalle();
		
		boolean moreliFound = false;
		int moreliFoundCount = 0;
		
		StringHolder sh = new StringHolder();
		action.IAction_Execute(request, sh, null);
		
		if ( sh.getValue().contains("<CLIENDES>MORELI , MARIANITA </CLIENDES>") ) {
			moreliFound = true;
			moreliFoundCount++;
		}
		Response1428 resp1428 = (Response1428) (new Response1428()).unmarshal(sh.getValue());
		validateResponse1428(resp1428);
		ContinuationStatus paginateFlag = resp1428.getContinuationStatus();
		int numPages = 1;
		logger.info("Pagina: "+ numPages + " trae " + resp1428.getRecords().size());
		logger.info("Continua: " + resp1428.getRequest().getEstado().toString() + "  Continuacion " +  paginateFlag);
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		
		while ( paginateFlag.equals(ContinuationStatus.TR )) {
				numPages++;
				String nextReq = resp1428.getCampos().getRequest().marshall();
				
				action = new GetDeudaCobradaCanalesDetalle();

				action.IAction_Execute(nextReq, sh, null);
				if ( sh.getValue().contains("<CLIENDES>MORELI , MARIANITA </CLIENDES>") ) {
					moreliFound = true;
					moreliFoundCount++;
				}

				resp1428 = (Response1428) (new Response1428()).unmarshal(sh.getValue());
				if ( resp1428.getRequest() != null ) {
					logger.info("CONTINUA: " + resp1428.getRequest().getEstado() + " ESTATUS: " + resp1428.getContinuationStatus());
				}
				logger.info("Pagina: "+ numPages + " trae " + resp1428.getRecords().size());
//				validateResponse1428(resp1428);
				paginateFlag = resp1428.getContinuationStatus();
				logger.info("Continua: " + paginateFlag);
		}
		
		assertTrue("No está MORELI MARIANITA", moreliFound);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
		assertEquals("MORELI MARIANITA aparece más de una vez", 1, moreliFoundCount);
	}
		
	

	@Test
	public void testCMS1428MockPipe() throws MalformedURLException, OSBConnectorException, JAXBException {
		CMS1428MockPipe mp = new CMS1428MockPipe();
		for (int i = 0; i < 3; i++) {
			mp.executeRequest(null);
		}
	}

	/**
	 * Recorre una query completa, paginando claro.
	 * Usa un pipe local de CMS, y uno local de Insis
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFullNavigationSoloInsisLocal() throws Exception {
		int pageSize = 30;	
		double numRegs = 52;
		
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		
		String request = REQUEST_INSIS;
		
		PaginatedActionCode action = new GetDeudaCobradaCanalesDetalle();
		List<Pipe> pipes = this.createOneInsisTestPipe();
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		action.IAction_Execute(request, sh, null);
		Response1428 resp1428 = (Response1428) (new Response1428()).unmarshal(sh.getValue());
		validateResponse1428(resp1428);
		ContinuationStatus paginateFlag = resp1428.getContinuationStatus();
		int numPages = 1;
		logger.info("Pagina: "+ numPages + " trae " + resp1428.getRecords().size());
		logger.info("Continua: " + resp1428.getRequest().getEstado().toString() + "  Continuacion " +  paginateFlag);
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR )) {
				numPages++;
				String nextReq = resp1428.getCampos().getRequest().marshall();
				
				action.IAction_Execute(nextReq, sh, null);
				resp1428 = (Response1428) (new Response1428()).unmarshal(sh.getValue());
				
				logger.info("CONTINUA: " + resp1428.getRequest().getEstado() + " ESTATUS: " + resp1428.getContinuationStatus());
				logger.info("Pagina: "+ numPages + " trae " + resp1428.getRecords().size());
				validateResponse1428(resp1428);
				paginateFlag = resp1428.getContinuationStatus();
				logger.info("Continua: " + resp1428.getRequest().getEstado());
		}
		
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1428.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
	}
	
	/**
	 * Recorre una query completa, paginando claro.
	 * Usa un pipe local de CMS, y uno local de Insis
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFullNavigationCMSLocalInsisLocal() throws Exception {
		int pageSize = 30;	
		double numRegs = 3 + 52;
		
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		
		String request = REQUEST_INSIS;
		
		PaginatedActionCode action = new GetDeudaCobradaCanalesDetalle();
		List<Pipe> pipes = this.createOneCMSTestPipe();
		pipes.addAll(this.createOneInsisTestPipe());
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		action.IAction_Execute(request, sh, null);
		Response1428 resp1428 = (Response1428) (new Response1428()).unmarshal(sh.getValue());
		validateResponse1428(resp1428);
		ContinuationStatus paginateFlag = resp1428.getContinuationStatus();
		int numPages = 1;
		logger.info("Pagina: "+ numPages + " trae " + resp1428.getRecords().size());
		logger.info("Continua: " + resp1428.getRequest().getEstado().toString() + "  Continuacion " +  paginateFlag);
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR )) {
				numPages++;
				String nextReq = resp1428.getCampos().getRequest().marshall();
				
				action.IAction_Execute(nextReq, sh, null);
				resp1428 = (Response1428) (new Response1428()).unmarshal(sh.getValue());
				
				logger.info("CONTINUA: " + resp1428.getRequest().getEstado() + " ESTATUS: " + resp1428.getContinuationStatus());
				logger.info("Pagina: "+ numPages + " trae " + resp1428.getRecords().size());
				validateResponse1428(resp1428);
				paginateFlag = resp1428.getContinuationStatus();
				logger.info("Continua: " + resp1428.getRequest().getEstado());
		}
		
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1428.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
	}
	
	/**
	 * Recorre una query completa, paginando claro.
	 * Usa 11 pipes locales de CMS, y un pipe vacío de Insis
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFullNavigationMultiCMSLocal() throws Exception {
		int pageSize = 30;	
		int pipeCount = 11;
		double numRegs = pipeCount * 3;
		
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		
		String request = REQUEST_INSIS;
		
		PaginatedActionCode action = new GetDeudaCobradaCanalesDetalle();
		List<Pipe> pipes = this.createCMSTestPipe(pipeCount);
		pipes.addAll(this.createOneEmptyInsisPipe());
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		action.IAction_Execute(request, sh, null);
		Response1428 resp1428 = (Response1428) (new Response1428()).unmarshal(sh.getValue());
		validateResponse1428(resp1428);
		ContinuationStatus paginateFlag = resp1428.getContinuationStatus();
		int numPages = 1;
		logger.info("Pagina: "+ numPages + " trae " + resp1428.getRecords().size());
		logger.info("Continua: " + resp1428.getRequest().getEstado().toString() + "  Continuacion " +  paginateFlag);
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR )) {
				numPages++;
				String nextReq = resp1428.getCampos().getRequest().marshall();
				
				CMS1428MockPipe cp = (CMS1428MockPipe)action.getTestPipes().get(0);
				logger.info("CMS MOCK PIPE: tiene mas " + cp.hasNext());
				
				
				action.IAction_Execute(nextReq, sh, null);
				resp1428 = (Response1428) (new Response1428()).unmarshal(sh.getValue());
				
				cp = (CMS1428MockPipe)action.getTestPipes().get(0);
				logger.info("CMS MOCK PIPE: tiene mas " + cp.hasNext());			
				logger.info("CONTINUA: " + resp1428.getRequest().getEstado() + " ESTATUS: " + resp1428.getContinuationStatus());
				logger.info("Pagina: "+ numPages + " trae " + resp1428.getRecords().size());
				validateResponse1428(resp1428);
				paginateFlag = resp1428.getContinuationStatus();
				logger.info("Continua: " + resp1428.getRequest().getEstado());
		}
		
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1428.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
	}
	
	
	/**
	 * Recorre una query completa, paginando claro.
	 * El pipe de insis está vacío
	 * @throws Exception
	 */
	@Test
	public void testFullNavigationLocalEmptyInsis() throws Exception {
		int pageSize = 2;
		//cada response tiene un solo 'CANAL'
		double numRegs = 3;
		
		String request = REQUEST_BASICO;
		PaginatedActionCode action = new GetDeudaCobradaCanalesDetalle();
		List<Pipe> pipes = this.createOneCMSTestPipe();
		pipes.addAll(this.createOneEmptyInsisPipe());
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		//Lamada 1ra página
		action.IAction_Execute(request, sh, null);
		Response1428 resp1428 = (Response1428) (new Response1428()).unmarshal(sh.getValue());
		validateResponse1428(resp1428);
		ContinuationStatus paginateFlag = resp1428.getContinuationStatus();
		int numPages = 1;
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR)) {
			numPages++;
			assertEquals("Una página intermedia no trajo todos los registros pedidos", pageSize, resp1428.getRecords().size());

			String nextReq = resp1428.getCampos().getRequest().marshall();
			action.IAction_Execute(nextReq, sh, null);
			resp1428 = (Response1428) (new Response1428()).unmarshal(sh.getValue());
			validateResponse1428(resp1428);
			paginateFlag = resp1428.getContinuationStatus();
		}
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));

		//De acá en adelante, tenemos los datos de la última página solamente		
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1428.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		
	}
	
	
	/**
	 * Prueba de formateo XSL sin paginado.
	 * Pide una página gigante para que levante todos los datos de un pipe CMS local
	 * El pipe de insis está vacío
	 * 
	 * @throws Exception
	 */
	@Test
	public void testXSLWithLocalPipes() throws Exception {

		String request = REQUEST_XSL;
		PaginatedActionCode action = new GetDeudaCobradaCanalesDetalle();
		List<Pipe> pipes = this.createOneCMSTestPipe();
		pipes.addAll(this.createOneEmptyInsisPipe());
		action.setTestPipes(pipes);
		action.setPageSize(Integer.MAX_VALUE);
		
		StringHolder sh = new StringHolder();
		action.IAction_Execute(request, sh, null);
		System.out.println(sh.getValue());
		Response1428Text resp1428 = (Response1428Text) Response1428Text.unmarshal(sh.getValue());
		
		//FIXME Acá comparar con el XSL esperado
		
	}
	
	/**
	 * Prueba de formateo TXT sin paginado.
	 * Pide una página gigante para que levante todos los datos de un pipe CMS local
	 * El pipe de insis está vacío
	 * 
	 * @throws Exception
	 */
	@Test
	public void testTXTWithLocalPipes() throws Exception {

		String request = REQUEST_TXT;
		PaginatedActionCode action = new GetDeudaCobradaCanalesDetalle();
		List<Pipe> pipes = this.createOneCMSTestPipe();
		pipes.addAll(this.createOneEmptyInsisPipe());
		action.setTestPipes(pipes);
		action.setPageSize(Integer.MAX_VALUE);
		
		StringHolder sh = new StringHolder();
		action.IAction_Execute(request, sh, null);
		System.out.println(sh.getValue());
		Response1428Text resp1428 = (Response1428Text) Response1428Text.unmarshal(sh.getValue());
		
		//FIXME Acá comparar con el TXT esperado
		
	}
	
	private void validateResponse1428(Response1428 resp1428) {
		assertFalse("Vino false", resp1428.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
		assertTrue("El response vino sin Request de continuación", resp1428.getRequest() != null);		
	}



	private List<Pipe> createOneEmptyInsisPipe() {
		List<Pipe> pipes = new ArrayList<Pipe>();
		MessagePipe pipe = new InsisEmptyPipe();
		pipe.setResponseUnmarshaller(new Response1428Unmarshaller());
		pipe.setActionCode(GetDeudaCobradaCanalesDetalle.ACTION_CODE_1428);
		pipes.add(pipe);

		return pipes;
	}
	

	private List<Pipe> createOneCMSTestPipe() {
		List<Pipe> pipes = new ArrayList<Pipe>();
		CMS1428MockPipe cms1 = new CMS1428MockPipe();
		pipes.add(cms1);

		return pipes;
	}

	
	
	private List<Pipe> createOneInsisTestPipe() throws IOException, JAXBException {
		List<Pipe> pipes = new ArrayList<Pipe>();

		String _52_REG_FILE_NAME = "1428/1428_insis_52_resp_full.xml";

		InsisPipe pipe = new InsisMockPipe(_52_REG_FILE_NAME, Request1428.class, Response1428Unmarshaller.class, Response1428.class, GetDeudaCobradaCanalesDetalle.ACTION_CODE_1428);
		pipes.add(pipe);

		return pipes;
	}

	private List<Pipe> createCMSTestPipe(int count) {
		List<Pipe> pipes = new ArrayList<Pipe>();
		for (int i = 0; i < count; i++) {
			pipes.add(new CMS1428MockPipe());
		}
		return pipes;
	}
	
	
	private String getRequest1667() throws IOException {
		String source = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("1667/requestBug1667.xml"), Charset.forName("UTF-8"));
		return source;
	}
	
	private String getRequest1246() throws IOException {
		String source = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("1428/Bug1246/requestBug1246.xml"), Charset.forName("UTF-8"));
		return source;
	}
	
	
	/**
	 * No está trabajando bien la bajada a excel o txt ya que en estos casos no tiene
	   que paginar, debe devolver todos los registros. ahora devuelve solo la primer
	   página, se maneja con el parámetro       
      <PAGINASRETORNADAS>0</PAGINASRETORNADAS>
	 * @throws Exception
	 */	
	@Test
	public void testBug1246UAT() throws Exception {

		Assume.assumeTrue(CurrentProfile.getProfileName().equals("uat"));
		
		String request = this.getRequest1246();
		
		PaginatedActionCode action = new GetDeudaCobradaCanalesDetalle();
	
		int sizePageSize=action.getPageSize();
		
		StringHolder sh = new StringHolder();
		action.IAction_Execute(request, sh, null);
		
		System.out.println(sh.getValue());
		Response1428Text resp1428 = (Response1428Text) Response1428Text.unmarshal(sh.getValue());
		
		// chequeo resultado
		String regs=resp1428.getMixedContent().get(1).toString();
		int j=-1;
		for(int i=2;i<regs.length();i++)
			if(regs.charAt(i)=='\n')
			j++;
		
		logger.info("Cantidad de registros: "+j);
		logger.info("Tamaño de pagina: "+sizePageSize);
		assertTrue("La cantidad de reg retornados, tiene que ser mayor al tamaño de pagina :",j>sizePageSize);
		
 	}
	


	
}
