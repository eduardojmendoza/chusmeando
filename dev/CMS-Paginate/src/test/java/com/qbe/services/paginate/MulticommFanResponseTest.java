package com.qbe.services.paginate;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Test;
import org.springframework.util.StreamUtils;

public class MulticommFanResponseTest {

	@Test
	public void testUnmarshall() throws IOException, JAXBException {
		String response = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("multicommfanresponse.txt"), Charset.forName("UTF-8"));

		MulticommRequestsListResponse resp = (new MulticommRequestsListResponse()).unmarshall(response);
		List<? extends PaginateRequest> requests = resp.getRequests();
		assertTrue("Deberían haber venido 3 requests", requests.size() == 3);
	}

}
