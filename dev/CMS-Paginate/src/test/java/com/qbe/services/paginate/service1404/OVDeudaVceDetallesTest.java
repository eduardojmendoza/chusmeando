package com.qbe.services.paginate.service1404;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Assume;
import org.junit.Test;
import org.springframework.util.StreamUtils;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.services.paginate.InsisMockPipe;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.Pipe;
import com.qbe.services.paginate.mqgestion.GetDeudaCobradaCanalesDetalle;
import com.qbe.services.paginate.service1109.mocks.CMS1109MockPipe;
import com.qbe.services.paginate.service1428.Request1428;
import com.qbe.services.paginate.service1428.Response1428;
import com.qbe.services.paginate.service1428.Response1428Unmarshaller;
import com.qbe.vbcompat.string.StringHolder;

public class OVDeudaVceDetallesTest {

	@Test
	public void test() throws IOException, CurrentProfileException, JAXBException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));

		OVDeudaVceDetalles ac = new OVDeudaVceDetalles();
		String req1404Xml = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("req_1404_para_tots.xml"), Charset.forName("UTF-8"));
		StringHolder responseSH = new StringHolder();
		ac.IAction_Execute(req1404Xml, responseSH, null);
		
		Response1404 resp1404 = (Response1404) (new Response1404()).unmarshal(responseSH.getValue());
		resp1404.recalculateTots();
		assertTrue("Faltan tots", resp1404.getTots().size() == 2);
		
	}

	@Test
	public void testBug1525() throws IOException, CurrentProfileException, JAXBException  {
		/**DESCRIPCION:
		 * Exception java.lang.NumberFormatException [ For
			input string: &quot;&quot; ] al ejecutar el actionCode
			lbaw_OVDeudaVceDetalles_Paginado."
			resultado="false">java.lang.NumberFormatException: For input string: ""
		 	cuando el request tiene campo NROORDEN no tiene contenido (<NROORDEN/>)
		 */
		                                   
		
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));

		OVDeudaVceDetalles ac = new OVDeudaVceDetalles();
		
		List<Pipe> pipes = this.createOneInsisTestPipe();
		pipes.addAll(this.createOneCMSTestPipe());
		
		ac.setTestPipes(pipes);
		ac.setPageSize(Integer.MAX_VALUE);
		
		String req1404Xml = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("req_1404_bug1525.xml"), Charset.forName("UTF-8"));
		StringHolder responseSH = new StringHolder();
		ac.IAction_Execute(req1404Xml, responseSH, null);
		
		Response1404 resp1404 = (Response1404) (new Response1404()).unmarshal(responseSH.getValue());
		resp1404.recalculateTots();
		assertTrue("Faltan tots", resp1404.getTots().size() == 2);
		
		
		
	}
	private List<Pipe> createOneInsisTestPipe() throws IOException, JAXBException {
		List<Pipe> pipes = new ArrayList<Pipe>();

		String _52_REG_FILE_NAME = "resp1404_1.xml";

		InsisPipe pipe = new InsisMockPipe(_52_REG_FILE_NAME, Request1404.class, Response1404Unmarshaller.class, Response1404.class, OVDeudaVceDetalles.ACTION_CODE_1404);
		pipes.add(pipe);

		return pipes;
	}
	
	private List<Pipe> createOneCMSTestPipe() {
		List<Pipe> pipes = new ArrayList<Pipe>();
		CMS1404MockPipe cms1 = new CMS1404MockPipe();
		pipes.add(cms1);

		return pipes;
	}
	
	
}
