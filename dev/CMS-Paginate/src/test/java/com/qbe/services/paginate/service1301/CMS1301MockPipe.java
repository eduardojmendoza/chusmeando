package com.qbe.services.paginate.service1301;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.springframework.util.StreamUtils;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.CMSPipe;
import com.qbe.services.paginate.CommRequest;
import com.qbe.services.paginate.MockPipePointer;
import com.qbe.services.paginate.PipeException;
import com.qbe.services.paginate.PipePointer;
import com.qbe.services.paginate.TestPipe;

/**
 * Un pipe que trae datos del actionCode 1301
 * 
 * @author ramiro
 *
 */
public class CMS1301MockPipe extends CMSPipe implements Serializable, TestPipe {

	private static Logger logger = Logger.getLogger(CMS1301MockPipe.class.getName());
	
	private String[] regsFileNames = new String[] { "1301/resp1301_1_1.xml", "1301/resp1301_1_2.xml"};

	protected int nextBlockIndex = 0;
	protected int executeRequestCount = 0;
	
	protected Request1301 lastExecutedRequest = null;

	/**
	 * Cantidad de registros en el CMS Pipe
	 */
	static final int RESPONSE_1301_1_REGS_SIZE = 33 + 390;
	
	public CMS1301MockPipe() {
		this.setRequest(new Request1301());
		this.setResponseUnmarshaller(new Response1301Unmarshaller());
		this.setActionCode(OVSiniListadoDetalles.ACTION_CODE_1301);
	}

	protected String executeRequest(CommRequest nextRequest)
			throws OSBConnectorException, MalformedURLException, JAXBException {
		lastExecutedRequest = (Request1301)nextRequest;
		String response;
		try {
			logger.log(Level.FINE,"Recupero block: " + nextBlockIndex);
			executeRequestCount++;
			logger.log(Level.FINE,"executeRequestCount: " + executeRequestCount);
			
			response = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(regsFileNames[nextBlockIndex++]), Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
			throw new OSBConnectorException("Al levantar uno de los archivos", e);
		}
		return response;
	}

	@Override
	public void reset() {
		super.reset();
		nextBlockIndex = 0;
	}

	@Override
	public PipePointer currentPointer() {
		MockPipePointer p = new MockPipePointer();
		p.setBlock(nextBlockIndex);
		p.setExecuteRequestCount(executeRequestCount);
		p.setPipePointer(super.currentPointer());
		return p;
	}

	@Override
	public void seek(PipePointer pp) throws PipeException {
		MockPipePointer cpp = (MockPipePointer)pp;
		nextBlockIndex = cpp.getBlock() - 1;
		logger.log(Level.FINE,"Seek block: " + nextBlockIndex);
		super.seek(cpp.getPipePointer());
		executeRequestCount = cpp.getExecuteRequestCount();
	}
	
	@Override
	public int getSourceRequests() {
		return executeRequestCount;
	}

	public Request1301 getLastExecutedRequest() {
		return lastExecutedRequest;
	}

	public void setLastExecutedRequest(Request1301 lastExecutedRequest) {
		this.lastExecutedRequest = lastExecutedRequest;
	}

	
}
