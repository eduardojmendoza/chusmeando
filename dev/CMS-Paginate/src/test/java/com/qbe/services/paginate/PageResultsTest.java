package com.qbe.services.paginate;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.qbe.services.paginate.service1109.OVOpeEmiDetallePaginada;
import com.qbe.services.paginate.service1109.Reg1109;
import com.qbe.services.paginate.service1109.Response1109;
import com.qbe.vbcompat.framework.jaxb.Estado;
import com.qbe.vbcompat.string.StringHolder;

/**
 * Testea que los registros devueltos sean los esperados
 * 
 * @author ramiro
 *
 */
@RunWith(Parameterized.class)
public class PageResultsTest {

	private static final Map<Integer, String> oneInsisPageSize10;
	private static final Map<Integer, String> threeInsisPageSize1;
	private static final Map<Integer, String> oneMinInsisPageSize1;
    static {
    	{
        Map<Integer, String> aMap = new HashMap<Integer, String>();
        aMap.put(1, "REG1");
        aMap.put(2, "REG11");
        aMap.put(20, "REG191");
        oneInsisPageSize10 = Collections.unmodifiableMap(aMap);
    	}
    	
    	{
        Map<Integer, String> aMap = new HashMap<Integer, String>();
        aMap.put(1, "REG1");
        aMap.put(2, "REG1");
        aMap.put(3, "REG1");
        aMap.put(600, "REG200");
        threeInsisPageSize1 = Collections.unmodifiableMap(aMap);
    	}
    
    	{
            Map<Integer, String> aMap = new HashMap<Integer, String>();
            aMap.put(1, "REG1");
            oneMinInsisPageSize1 = Collections.unmodifiableMap(aMap);
        }
    }

	protected List<Pipe> pipes;
	protected int pageSize;
	protected Map<Integer, String> pageFirstClides;
	
	
	public PageResultsTest(List<Pipe> pipes, int pageSize, Map<Integer, String> pageFirstClides) {
		this.pipes = pipes;
		this.pageSize = pageSize;
		this.pageFirstClides = pageFirstClides;
	}
	
	/**
	 * Test data generator. This method is called the the JUnit parameterized
	 * test runner and returns a Collection of Arrays. For each Array in the
	 * Collection, each array element corresponds to a parameter in the
	 * constructor.
	 * @throws JAXBException 
	 * @throws IOException 
	 */
	@Parameters
	public static Collection<Object[]> generateData() throws IOException, JAXBException {
		List<Object []> params = new ArrayList<Object[]>();
		// TODO ¿en CMS tengo que manterner el tamaño de página?
		params.add(new Object[] {Pipes1107x09Factory.createCMSTestPipes(),  179, null});
		params.add(new Object[] {Pipes1107x09Factory.create2InsisTestPipes(100),  100, null});
		params.add(new Object[] {Pipes1107x09Factory.create3InsisTestPipes(1),  1, threeInsisPageSize1});
		params.add(new Object[] {Pipes1107x09Factory.createInsisTestPipe(10), 10, oneInsisPageSize10});
		params.add(new Object[] {Pipes1107x09Factory.create1RegInsisTestPipe(1), 1, oneMinInsisPageSize1});
		params.add(new Object[] {Pipes1107x09Factory.create3x1RegInsisTestPipe(1), 1, threeInsisPageSize1});

		return params;
		
	}
	
	public List<Pipe> getPipes() {
		return pipes;
	}

	public void setPipes(List<Pipe> pipes) {
		this.pipes = pipes;
	}

	/**
	 * Recorre una query completa
	 * 
	 * @throws Exception
	 */
	@Test
	public void testPageResults() throws Exception {
		
		String requestTemplate = Pipes1107x09Factory.REQUEST_TEMPLATE;
		String request = String.format(requestTemplate, "");
		PaginatedActionCode action = new OVOpeEmiDetallePaginada();
		action.setTestPipes(this.pipes);
		action.setPageSize(pageSize);
		
		int pageNum = 1;
		ContinuationStatus paginateFlag;		
		do {
			StringHolder sh = new StringHolder();
			int result = action.IAction_Execute(request, sh, null);
			Response1109 resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
			if ( resp1109.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE) ) {
				System.out.println(sh.getValue());
				fail("Vino resultado false");
			}
			
			if ( pageFirstClides != null ) {
				String expectedClides = pageFirstClides.get(pageNum);
				if ( expectedClides != null) {
					assertEquals(expectedClides, ((Reg1109)resp1109.getRecords().get(0)).getClides());
				}
			}
			
			paginateFlag = resp1109.getContinuationStatus();
			pageNum++;
			request = String.format(requestTemplate,resp1109.getOpecont());
			
		} while (paginateFlag.equals(Constants.PAGINATOR_TR_TAG) );
		
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
		
	}
	
}
