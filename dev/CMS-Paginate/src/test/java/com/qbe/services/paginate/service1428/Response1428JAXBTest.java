package com.qbe.services.paginate.service1428;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.junit.Test;

import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.vbcompat.framework.jaxb.Estado;
 
public class Response1428JAXBTest {
 
	@Test
	public void testMarshal() throws JAXBException {
		Response1428 resp = new Response1428();
		resp.setEstado(Estado.newWithResultadoTrue());
		
		ResponseCampos1428 campos = new ResponseCampos1428();
		resp.setCampos(campos);
		
		Canal1428 canal1 = new Canal1428();
		canal1.setImporte("1000");
		resp.addRecord(canal1);
		
		resp.setContinuationstatus(ContinuationStatus.TR);
		
		resp.getRequest().getNrocons().add("1");
		resp.getRequest().getNrocons().add("1");
		resp.getRequest().getNrocons().add("1");
		System.out.println(resp.marshal());
		
	}
}

