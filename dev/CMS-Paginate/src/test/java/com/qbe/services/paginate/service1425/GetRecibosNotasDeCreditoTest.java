package com.qbe.services.paginate.service1425;

import static org.junit.Assert.assertTrue;

import javax.xml.bind.JAXBException;

import org.junit.Assume;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.services.paginate.mqgestion.GetConsultaMQGestion;
import com.qbe.vbcompat.string.StringHolder;

/**
 * Test para probar el nuevo paginado.
 * 
 * @author gavilan
 *
 */
public class GetRecibosNotasDeCreditoTest {

	
	private static final String REQUEST_1425_TEMPLATE = "<Request>" +
														"<DEFINICION>getRecibosNotasDeCredito.xml</DEFINICION>"+
														    "<USUARCOD>EX009005L</USUARCOD>"+
														    "<NIVELCLAS>GO</NIVELCLAS>"+
														    "<CLIENSECAS>100029438</CLIENSECAS>"+
														    "<CANTLINREC>2</CANTLINREC>"+
														    "<RECIBOS>"+
														    "<RECIBO>"+
														    "    <RECNUM>021500381</RECNUM>"+
														    "  </RECIBO>"+
														    "  <RECIBO>"+
														    "    <RECNUM>021500824</RECNUM>"+
														    "  </RECIBO>"+
														    " </RECIBOS>"+
													"</Request>";
	
	
	@Test
	public void testBug859() throws JAXBException, CurrentProfileException {
		
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("uat"));
    	
		String request = String.format(REQUEST_1425_TEMPLATE, "");
		
		GetConsultaMQGestion action = new GetConsultaMQGestion();
		
		StringHolder sh = new StringHolder();
		int result = action.IAction_Execute(request, sh, null);
		Response1425 resp1425 = (Response1425) (new Response1425()).unmarshal(sh.getValue());
		
		assertTrue(resp1425.getRecords().size()!=0);
	}
	
	@Test
	public void testRequestFormat() throws JAXBException {
		
		String request = String.format(REQUEST_1425_TEMPLATE, "");
		
		Request1425 req1425 = (Request1425) (new Request1425()).unmarshall(request);
		
		assertTrue("tiene DEFINICION ", req1425.getDefinicion()!="");
		
	}
	
	
	
}
