package com.qbe.services.paginate;

import static org.junit.Assert.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class SequentialPaginatorImplTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	/**
	 * Primer página, 4 elementos
	 * @throws PipeException 
	 */
	@Test
	public void testFetchFirstPage() throws PipeException {
		List<Pipe> pipes = getPipes();
		Paginator paginator = new SequentialPaginator();
		paginator.setPipes(pipes);
		
		Page nextPage = paginator.fetchPage(0, 4);
		List<Object> elems = nextPage.getElements();
		assertTrue(elems.size() == 4);
		assertTrue(elems.get(0).equals("a"));
		assertTrue(elems.get(1).equals("b"));
		assertTrue(elems.get(2).equals("c"));
		assertTrue(elems.get(3).equals("d"));
	}
	
	/**
	 * Segunda página, 4 elementos
	 * @throws PipeException 
	 */
	@Test
	public void testFetchSecondPage() throws PipeException {
		List<Pipe> pipes = getPipes();
		Paginator paginator = new SequentialPaginator();
		paginator.setPipes(pipes);
		Page nextPage2 = paginator.fetchPage(1, 4);

		List<Object> elems2 = nextPage2.getElements();
		assertSecondPage(elems2);
	}

	public void assertSecondPage(List<Object> elems2) {
		assertTrue(elems2.size() == 4);
		assertTrue(elems2.get(0).equals("e"));
		assertTrue(elems2.get(1).equals("f"));
		assertTrue(elems2.get(2).equals("g"));
		assertTrue(elems2.get(3).equals("h"));
	}
	
	/**
	 * Tercera página, de 2 elementos
	 * @throws PipeException 
	 */
	@Test
	public void testFetchThirdPage() throws PipeException {
		List<Pipe> pipes = getPipes();
		Paginator paginator = new SequentialPaginator();
		paginator.setPipes(pipes);
		Page nextPage3 = paginator.fetchPage(2, 2);

		List<Object> elems3 = nextPage3.getElements();
		assertTrue(elems3.size() == 2);
		assertTrue(elems3.get(0).equals("e"));
		assertTrue(elems3.get(1).equals("f"));
	}

	@Test
	public void testEmptyPage() throws PipeException {
		List<Pipe> pipes = getPipes();
		Paginator paginator = new SequentialPaginator();
		paginator.setPipes(pipes);
		Page nextPage3 = paginator.fetchPage(3, 4);

		List<Object> elems3 = nextPage3.getElements();
		assertTrue(elems3.size() == 0);
	}

	/**
	 * 2 páginas de pipes con los mismos datos
	 * @throws PipeException 
	 */
	@Test
	public void testFetchDupPipes() throws PipeException {
		List<Pipe> pipes = new ArrayList<Pipe>();
		pipes.add(getPipe1());
		pipes.add(getPipe1());
		Paginator paginator = new SequentialPaginator();
		paginator.setPipes(pipes);

		int pageSize = 5;
		Page nextPage = paginator.fetchPage(0, pageSize);
		List<Object> elems = nextPage.getElements();
		assertTrue(elems.size() == pageSize);
		assertTrue(elems.get(0).equals("a"));
		assertTrue(elems.get(1).equals("a"));
		assertTrue(elems.get(2).equals("c"));
		assertTrue(elems.get(3).equals("c"));
		assertTrue(elems.get(4).equals("d"));

		nextPage = paginator.fetchPage(1, pageSize);
		elems = nextPage.getElements();
		assertTrue(elems.size() == pageSize);
		assertTrue(elems.get(0).equals("d"));
		assertTrue(elems.get(1).equals("e"));
		assertTrue(elems.get(2).equals("e"));
		assertTrue(elems.get(3).equals("f"));
		assertTrue(elems.get(4).equals("f"));
	}
	
	public List<Pipe> getPipes() {
		StringPipe pipe1 = getPipe1();
		StringPipe pipe2 = getPipe2();

		List<Pipe> pipes = new ArrayList<Pipe>();
		pipes.add(pipe1);
		pipes.add(pipe2);
		return pipes;
	}

	public StringPipe getPipe2() {
		StringPipe pipe2 = new StringPipe();
		List<String> pipe2Elems = Arrays.asList("b", "g", "h");
		pipe2.getSource().addAll(pipe2Elems);
		assertTrue(pipe2.getElements().size() == 0);
		pipe2.setSourceBulkSize(2);
		return pipe2;
	}

	public StringPipe getPipe1() {
		StringPipe pipe1 = new StringPipe();
		List<String> pipe1Elems = Arrays.asList("a","c","d","e","f");
		pipe1.getSource().addAll(pipe1Elems);
		assertTrue(pipe1.getElements().size() == 0);
		pipe1.setSourceBulkSize(3);
		return pipe1;
	}

}
