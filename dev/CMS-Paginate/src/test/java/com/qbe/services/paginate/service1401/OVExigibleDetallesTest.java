package com.qbe.services.paginate.service1401;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.util.StreamUtils;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.InsisMockPipe;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;
import com.qbe.services.paginate.ResponseUnmarshaller;
import com.qbe.services.paginate.mqgestion.GetDeudaCobradaCanalesDetalle;
import com.qbe.services.paginate.service1428.Response1428;
import com.qbe.vbcompat.string.StringHolder;

public class OVExigibleDetallesTest {
	private static Logger logger = Logger.getLogger(OVExigibleDetallesTest.class.getName());

	String REQUEST =   "<Request> " +
			"<USUARIO>EX009005L</USUARIO>" +
			"<NIVELAS>GO</NIVELAS>" +
			"<CLIENSECAS>100029438</CLIENSECAS>" +
			"<NIVEL1>GO</NIVEL1>" +
			"<CLIENSEC1>100029438</CLIENSEC1>" +
			"<NIVEL2/>" +
			"<CLIENSEC2/>" +
			"<NIVEL3/>" +
			"<CLIENSEC3/>" +
			"<PRODUCTO/>" +
			"<POLIZA/>" +
	        "<NROCONS/>" +
	        "<NROORDEN/>" +
	        "<TFILTRO/>" +
	        "<VFILTRO/>" +
	      "</Request> ";

	@Ignore
	@Test
	public void testBug1556() throws Exception   {
		int pageSize = 200;
			
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));

		
		OVExigibleDetalles ac = new OVExigibleDetalles();
		
		try {
			List<Pipe> pipes = this.createOneCMSTestPipe();
			pipes.addAll(this.createOneInsisTestPipe());
			ac.setTestPipes(pipes);  
			ac.setPageSize(pageSize);
			
			StringHolder responseSH = new StringHolder();
			
			ac.IAction_Execute(REQUEST, responseSH, null);
			
			Response1401 resp1401 = (Response1401) (new Response1401()).unmarshal(responseSH.getValue());
		    System.out.println(resp1401.toString());	
	
		    
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * Recorre una query completa, paginando claro.
	 * Usa un pipe local de CMS, y uno local de Insis
	 * 
	 * @throws Exception
	 */
	@Ignore
	@Test
	public void testWithInsisPipe() throws Exception {
		int pageSize = 200;	
		double numRegs = 52;
		
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("uat"));
		
		String request = REQUEST;
		PaginateRequest request1401 = new Request1401(); 
		ResponseUnmarshaller rum = new Response1401Unmarshaller();
		PaginatedActionCode action = new OVExigibleDetalles();
		action.setTestPipes(action.createInsisPipes(request1401,OVExigibleDetalles.ACTION_CODE_1401,rum, pageSize)) ;		
		
		StringHolder sh = new StringHolder();
		action.IAction_Execute(request, sh, null);
		Response1401 resp1401 = (Response1401) (new Response1401()).unmarshal(sh.getValue());
		
		ContinuationStatus paginateFlag = resp1401.getContinuationStatus();
		int numPages = 1;
		logger.info("Pagina: "+ numPages + " trae " + resp1401.getRecords().size());
		System.out.println(resp1401);
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		/*
		 * while ( paginateFlag.equals(ContinuationStatus.TR )) {
				numPages++;
				String nextReq = resp1401.;
				
				action.IAction_Execute(nextReq, sh, null);
				resp1401 = (Response1401) (new Response1401()).unmarshal(sh.getValue());
				
				
				logger.info("Pagina: "+ numPages + " trae " + resp1401.getRecords().size());

				paginateFlag = resp1401.getContinuationStatus();
				
		}
		*/
		
		//assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1401.getRecords().size());
		//assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		//assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
	}

	
	private List<Pipe> createOneCMSTestPipe() {
		List<Pipe> pipes = new ArrayList<Pipe>();
		CMS1401MockPipe cms1 = new CMS1401MockPipe();
		pipes.add(cms1);

		return pipes;
	}
	
	private List<Pipe> createOneInsisTestPipe() throws IOException, JAXBException {
		List<Pipe> pipes = new ArrayList<Pipe>();
		
		String REG_FILE_NAME = "1401/insis/1401_insis_resp.xml";
		
		InsisPipe pipe = new InsisMockPipe(REG_FILE_NAME, Request1401.class, Response1401Unmarshaller.class, Response1401.class, OVExigibleDetalles.ACTION_CODE_1401);
		pipes.add(pipe);

		return pipes;
	}

}
