package com.qbe.services.paginate;

import static org.junit.Assert.*;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.qbe.services.paginate.service1109.Comparator1109;
import com.qbe.services.paginate.service1109.Reg1109;

public class SimpleTests {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testComparator() {
		int compare = Integer.signum("".compareTo("AUS1"));
		assertTrue(compare < 0);
	}
	
	@Test
	public void testComparatorWithNullKeys() throws JAXBException {

		String nullRegSer = "<REG><PROD/><POL/><CERPOL/><CERANN/><CERSEC/><SUPLENUM>0000</SUPLENUM><OPERAPOL>0</OPERAPOL><CLIDES>SYNCH1 , TESTUSER USER</CLIDES><FECVIGDES>04/04/2013</FECVIGDES><FECVIGHAS>04/05/2013</FECVIGHAS><MOTIV>RENOVACION DE CERTIFICADO</MOTIV><FECEMI>04/04/2013</FECEMI><PRIMA>712,13</PRIMA><PRECIO>920,07</PRECIO><COD>PR-3233</COD><RAMO>2</RAMO></REG>";
		String nonNullRegSer = "<REG><PROD>AUS1</PROD><POL>00000001</POL><CERPOL>0000</CERPOL><CERANN>0001</CERANN><CERSEC>123632</CERSEC><SUPLENUM>0000</SUPLENUM><OPERAPOL>3000002401</OPERAPOL><CLIDES>SYNCH1 , TESTUSER USER</CLIDES><FECVIGDES>04/04/2013</FECVIGDES><FECVIGHAS>04/05/2013</FECVIGHAS><MOTIV>RENOVACION DE CERTIFICADO</MOTIV><FECEMI>04/04/2013</FECEMI><PRIMA>712,13</PRIMA><PRECIO>920,07</PRECIO><COD>PR-3233</COD><RAMO>2</RAMO></REG>";
		Reg1109 nullReg = (Reg1109) (new Reg1109()).unmarshal(nullRegSer);
		Reg1109 nonNullReg = (Reg1109) (new Reg1109()).unmarshal(nonNullRegSer);
		Comparator1109 compa = new Comparator1109();
		int result = compa.compare(nullReg, nonNullReg);
		assertTrue(result < 0);
	}
	
	

}
