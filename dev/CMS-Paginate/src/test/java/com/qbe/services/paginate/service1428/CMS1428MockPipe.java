package com.qbe.services.paginate.service1428;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.springframework.util.StreamUtils;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.CMSPipe;
import com.qbe.services.paginate.CommRequest;
import com.qbe.services.paginate.MockPipePointer;
import com.qbe.services.paginate.PipeException;
import com.qbe.services.paginate.PipePointer;
import com.qbe.services.paginate.TestPipe;
import com.qbe.services.paginate.mqgestion.GetDeudaCobradaCanalesDetalle;

/**
 * Un pipe que trae datos del actionCode 1428
 * 
 * @author ramiro
 *
 */
public class CMS1428MockPipe extends CMSPipe implements Serializable, TestPipe {

	private static Logger logger = Logger.getLogger(CMS1428MockPipe.class.getName());
	
	private String[] regsFileNames = new String[] { "1428/resp1_1.xml", "1428/resp2_2.xml", "1428/resp3_3.xml"};

	protected int nextBlockIndex = 0;
	protected int executeRequestCount = 0;
	
	public CMS1428MockPipe() {
		this.setRequest(new Request1428());
		this.setResponseUnmarshaller(new Response1428Unmarshaller());
		this.setActionCode(GetDeudaCobradaCanalesDetalle.ACTION_CODE_1428);
	}

	protected String executeRequest(CommRequest nextRequest)
			throws OSBConnectorException, MalformedURLException, JAXBException {
		String response;
		try {
			logger.log(Level.FINE,"Recupero block: " + nextBlockIndex);
			executeRequestCount++;
			logger.log(Level.FINE,"executeRequestCount: " + executeRequestCount);
			
			response = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(regsFileNames[nextBlockIndex++]), Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
			throw new OSBConnectorException("Al levantar uno de los archivos", e);
		}
		return response;
	}

	@Override
	public void reset() {
		super.reset();
		nextBlockIndex = 0;
	}

	@Override
	public PipePointer currentPointer() {
		MockPipePointer p = new MockPipePointer();
		p.setBlock(nextBlockIndex);
		p.setExecuteRequestCount(executeRequestCount);
		p.setPipePointer(super.currentPointer());
		return p;
	}

	@Override
	public void seek(PipePointer pp) throws PipeException {
		MockPipePointer cpp = (MockPipePointer)pp;
		nextBlockIndex = cpp.getBlock() - 1;
		logger.log(Level.FINE,"Seek block: " + nextBlockIndex);
		super.seek(cpp.getPipePointer());
		executeRequestCount = cpp.getExecuteRequestCount();
	}
	
	@Override
	public int getSourceRequests() {
		return executeRequestCount;
	}

	
}
