package com.qbe.services.paginate.service1302;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.util.StreamUtils;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.InsisEmptyPipe;
import com.qbe.services.paginate.InsisMockPipe;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MessagePipe;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;
import com.qbe.vbcompat.framework.jaxb.Estado;
import com.qbe.vbcompat.string.StringHolder;

public class OVSiniConsultaTest {

	private static Logger logger = Logger.getLogger(OVSiniConsultaTest.class.getName());

	
	private static final String REQUEST_BASICO = "<Request>" + 
 "    <USUARIO>EX009005L</USUARIO>" + 
 "    <NIVELAS>GO</NIVELAS>" + 
 "    <CLIENSECAS>100029438</CLIENSECAS>" + 
 "    <DOCUMTIP/>" + 
 "    <DOCUMNRO/>" + 
 "    <CLIENDES>RAMIREZ</CLIENDES>" + 
 "    <PRODUCTO/>" + 
 "    <POLIZA/>" + 
 "    <CERTI/>" + 
 "    <PATENTE/>" + 
 "    <SINIAN/>" + 
 "    <SININUM/>" + 
 "    <MSGEST/>" + 
 "    <CONTINUAR/>" + 
 "</Request>";

	/**
	 * Testea que el request a CMS salga bien formado
	 * Bug 1672
	 * @throws JAXBException 
	 */
	@Test
	public void testCMSRequestFormat() throws JAXBException {
		
		int pageSize = 2;
		double numRegs = CMS1302MockPipe.RESPONSE_1302_1_REGS_SIZE;
		
		String request = REQUEST_BASICO;
		PaginatedActionCode action = new OVSiniConsulta();
		List<Pipe> pipes = new ArrayList<Pipe>();
		CMS1302MockPipe cms1 = new CMS1302MockPipe();
		pipes.add(cms1);
		
		MessagePipe pipe = new InsisEmptyPipe();
		pipe.setResponseUnmarshaller(new Response1302Unmarshaller());
		pipe.setActionCode(OVSiniConsulta.ACTION_CODE_1302);
		pipes.add(pipe);
		
		
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		//Lamada 1ra página
		action.IAction_Execute(request, sh, null);
		Response1302 resp1302 = (Response1302) (new Response1302()).unmarshal(sh.getValue());
		validateResponse1302(resp1302);
		
		Request1302 actualRequest = cms1.getLastExecutedRequest();
		String actualRequestValue = actualRequest.marshall();
		assertFalse("Manda USUARCOD", actualRequestValue.contains("<USUARCOD>"));
		assertTrue("No manda USUARIO", actualRequestValue.contains("<USUARIO>"));
		assertTrue("No manda MSGEST", actualRequestValue.contains("<MSGEST>"));
	}
	
	
	/**
	 * Pagina contra CMS(AIS), revisando que se esté mandando el CONTINUAR que necesita AIS
	 * El pipe de insis está vacío
	 * @throws Exception
	 */
	@Test
	public void testAISContinuar() throws Exception {
		int pageSize = 390;
		double numRegs = CMS1302MockPipe.RESPONSE_1302_1_REGS_SIZE;
		
		String request = REQUEST_BASICO;
		PaginatedActionCode action = new OVSiniConsulta();
		List<Pipe> pipes = new ArrayList<Pipe>();
		CMS1302MockPipe cms1 = new CMS1302MockPipe();
		pipes.add(cms1);
		
		MessagePipe pipe = new InsisEmptyPipe();
		pipe.setResponseUnmarshaller(new Response1302Unmarshaller());
		pipe.setActionCode(OVSiniConsulta.ACTION_CODE_1302);
		pipes.add(pipe);
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		//Lamada 1ra página
		action.IAction_Execute(request, sh, null);
		Response1302 resp1302 = (Response1302) (new Response1302()).unmarshal(sh.getValue());
		validateResponse1302(resp1302);
		ContinuationStatus paginateFlag = resp1302.getContinuationStatus();
		int numPages = 1;
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR)) {
			numPages++;
			assertEquals("Una página intermedia no trajo todos los registros pedidos", pageSize, resp1302.getRecords().size());

			String nextReq = request;
			action.IAction_Execute(nextReq, sh, null);
			resp1302 = (Response1302) (new Response1302()).unmarshal(sh.getValue());
			validateResponse1302(resp1302);
			
			//Valido que haya enviado el continuar de AIS
			Request1302 actualRequest = cms1.getLastExecutedRequest();
			String actualRequestValue = actualRequest.marshall();
			assertTrue("NO manda el CONTINUAR de AIS, el request fue:" + actualRequestValue, actualRequestValue.contains("<CONTINUAR>GO100029438  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  000000000  0000000000120140112AUS110305721</CONTINUAR>"));
			
			paginateFlag = resp1302.getContinuationStatus();
		}
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));

		//De acá en adelante, tenemos los datos de la última página solamente		
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1302.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		
	}
		
	
	
	
	/**
	 * Recorre una query completa, paginando claro.
	 * Usa un pipe local de CMS, y uno local de Insis
	 * 
	 * @throws Exception
	 */
	@Test
	@Ignore //No tenemos datos de Insis aún
	public void testFullNavigationCMSLocalInsisLocal() throws Exception {
		int pageSize = 30;	
		double numRegs = CMS1302MockPipe.RESPONSE_1302_1_REGS_SIZE + 52;
		
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		
		String request = REQUEST_BASICO;
		
		PaginatedActionCode action = new OVSiniConsulta();
		List<Pipe> pipes = this.createOneCMSTestPipe();
		pipes.addAll(this.createOneInsisTestPipe());
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		action.IAction_Execute(request, sh, null);
		Response1302 resp1302 = (Response1302) (new Response1302()).unmarshal(sh.getValue());
		validateResponse1302(resp1302);
		ContinuationStatus paginateFlag = resp1302.getContinuationStatus();
		int numPages = 1;
		logger.info("Pagina: "+ numPages + " trae " + resp1302.getRecords().size());
		logger.info("Continuar: " + resp1302.getContinuar() + "  Continuacion " +  paginateFlag);
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR )) {
				numPages++;
				String nextReq = request;
				
				action.IAction_Execute(nextReq, sh, null);
				resp1302 = (Response1302) (new Response1302()).unmarshal(sh.getValue());
				
				logger.info("CONTINUAR: " + resp1302.getContinuar() + " ESTATUS: " + resp1302.getContinuationStatus());
				logger.info("Pagina: "+ numPages + " trae " + resp1302.getRecords().size());
				validateResponse1302(resp1302);
				paginateFlag = resp1302.getContinuationStatus();
		}
		
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1302.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
	}
	
	/**
	 * Recorre una query completa, paginando claro.
	 * Usa 11 pipes locales de CMS, y un pipe vacío de Insis
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFullNavigationMultiCMSLocal() throws Exception {
		int pageSize = 30;	
		int pipeCount = 11;
		double numRegs = pipeCount * CMS1302MockPipe.RESPONSE_1302_1_REGS_SIZE;
		
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		
		String request = REQUEST_BASICO;
		
		PaginatedActionCode action = new OVSiniConsulta();
		List<Pipe> pipes = this.createCMSTestPipe(pipeCount);
		pipes.addAll(this.createOneEmptyInsisPipe());
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		action.IAction_Execute(request, sh, null);
		Response1302 resp1302 = (Response1302) (new Response1302()).unmarshal(sh.getValue());
		validateResponse1302(resp1302);
		ContinuationStatus paginateFlag = resp1302.getContinuationStatus();
		int numPages = 1;
		logger.info("Pagina: "+ numPages + " trae " + resp1302.getRecords().size());
		logger.info("Continuar: " + resp1302.getContinuar() + "  Continuacion " +  paginateFlag);
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR )) {
				numPages++;
				String nextReq = request;
				
				CMS1302MockPipe cp = (CMS1302MockPipe)action.getTestPipes().get(0);
				logger.info("CMS MOCK PIPE: tiene mas " + cp.hasNext());
				
				
				action.IAction_Execute(nextReq, sh, null);
				resp1302 = (Response1302) (new Response1302()).unmarshal(sh.getValue());
				
				cp = (CMS1302MockPipe)action.getTestPipes().get(0);
				logger.info("CMS MOCK PIPE: tiene mas " + cp.hasNext());			
				logger.info("CONTINUAR: " + resp1302.getContinuar() + " ESTATUS: " + resp1302.getContinuationStatus());
				logger.info("Pagina: "+ numPages + " trae " + resp1302.getRecords().size());
				validateResponse1302(resp1302);
				paginateFlag = resp1302.getContinuationStatus();
		}
		
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1302.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
	}
	
	
	/**
	 * Recorre una query completa, paginando claro.
	 * El pipe de insis está vacío
	 * @throws Exception
	 */
	@Test
	public void testFullNavigationCMSLocalEmptyInsis() throws Exception {
		int pageSize = 2;
		double numRegs = CMS1302MockPipe.RESPONSE_1302_1_REGS_SIZE;
		
		String request = REQUEST_BASICO;
		PaginatedActionCode action = new OVSiniConsulta();
		List<Pipe> pipes = this.createOneCMSTestPipe();
		pipes.addAll(this.createOneEmptyInsisPipe());
		action.setTestPipes(pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		//Lamada 1ra página
		action.IAction_Execute(request, sh, null);
		Response1302 resp1302 = (Response1302) (new Response1302()).unmarshal(sh.getValue());
		validateResponse1302(resp1302);
		ContinuationStatus paginateFlag = resp1302.getContinuationStatus();
		int numPages = 1;
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR)) {
			numPages++;
			assertEquals("Una página intermedia no trajo todos los registros pedidos", pageSize, resp1302.getRecords().size());

			String nextReq = request;
			action.IAction_Execute(nextReq, sh, null);
			resp1302 = (Response1302) (new Response1302()).unmarshal(sh.getValue());
			validateResponse1302(resp1302);
			paginateFlag = resp1302.getContinuationStatus();
		}
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));

		//De acá en adelante, tenemos los datos de la última página solamente		
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1302.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		
	}
	
	
	private void validateResponse1302(Response1302 resp1302) {
		assertFalse("Vino false", resp1302.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
		assertTrue("El response vino sin Request de continuación", resp1302.getContinuar() != null);		
	}



	private List<Pipe> createOneEmptyInsisPipe() {
		List<Pipe> pipes = new ArrayList<Pipe>();
		MessagePipe pipe = new InsisEmptyPipe();
		pipe.setResponseUnmarshaller(new Response1302Unmarshaller());
		pipe.setActionCode(OVSiniConsulta.ACTION_CODE_1302);
		pipes.add(pipe);

		return pipes;
	}
	

	private List<Pipe> createOneCMSTestPipe() {
		List<Pipe> pipes = new ArrayList<Pipe>();
		CMS1302MockPipe cms1 = new CMS1302MockPipe();
		pipes.add(cms1);

		return pipes;
	}

	private List<Pipe> createOneInsisTestPipe() throws IOException, JAXBException {
		List<Pipe> pipes = new ArrayList<Pipe>();

		String _52_REG_FILE_NAME = "1302/1302_insis_52_resp_full.xml";

		InsisPipe pipe = new InsisMockPipe(_52_REG_FILE_NAME, Request1302.class, Response1302Unmarshaller.class, Response1302.class, OVSiniConsulta.ACTION_CODE_1302);
		pipes.add(pipe);

		return pipes;
	}

	private List<Pipe> createCMSTestPipe(int count) {
		List<Pipe> pipes = new ArrayList<Pipe>();
		for (int i = 0; i < count; i++) {
			pipes.add(new CMS1302MockPipe());
		}
		return pipes;
	}
	
	
	/**
	 * Testea contra UAT 
	 * 
	 * @throws IOException
	 * @throws CurrentProfileException
	 * @throws JAXBException
	 */
	@Test
	public void testBug1611() throws IOException, CurrentProfileException, JAXBException {
	/**
	 * DESC: ambos sistemas devuelven datos pero en alguno de los pasos de la paginación después da error.
	 */
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("uat"));
		
		OVSiniConsulta ac = new OVSiniConsulta();
		
		String req1302 = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("1302/req_1302_Bug1611.xml"), Charset.forName("UTF-8"));
		StringHolder responseSH = new StringHolder();
		ac.IAction_Execute(req1302, responseSH, null);
		
		Response1302 resp1302 = (Response1302) (new Response1302()).unmarshal(responseSH.getValue());
		
		logger.info("Corrio todo el test : "+resp1302.getRecords().size());
		
		
	}

	
}
