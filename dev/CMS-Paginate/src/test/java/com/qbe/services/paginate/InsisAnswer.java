package com.qbe.services.paginate;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.util.StreamUtils;

import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.jaxb.Estado;

/**
 * Un Answer que devuelve el subconjunto de registros especificado de una lista de registros que se cargan de archivos.
 * 
 * A diferencia de CMS que trabaja con los request marshaleados, este trabaja con cada registros. 
 * Para procesar los registros necesita parsearlos, para lo que usa requestUnmarshaller, responseUnmarshaller y continuationResponseClass 
 *  
 * @see CMSAnswer
 * 
 * @author ramiro
 *
 */
public class InsisAnswer implements Answer<String> {

	private static Logger logger = Logger.getLogger(InsisAnswer.class.getName());

	protected List<String> fileNames = new ArrayList<String>();
	
	protected List<ResponseRecord> regs = new ArrayList<ResponseRecord>();
	
	protected ResponseUnmarshaller responseUnmarshaller = null;
	
	protected PaginateRequest requestUnmarshaller = null;

	protected Class<? extends ContinuationResponse> continuationResponseClass;

	public InsisAnswer() {
	}

	@Override
	public String answer(InvocationOnMock invocation) throws Throwable {
		loadIfNeeded();

		Object[] args = invocation.getArguments();
		String actionCode = (String)args[0];
		assertNotNull(actionCode);
		String requestXML = (String)args[1];
		assertNotNull(requestXML);

		logger.log(Level.INFO, "actionCode: " + actionCode);

		PaginateRequest pr = (PaginateRequest) getRequestUnmarshaller().unmarshall(requestXML);
		
		ContinuationResponse r;
		try {
			r = continuationResponseClass.newInstance();
		} catch (InstantiationException e) {
			throw new ComponentExecutionException("Al instanciar la respuesta", e);
		} catch (IllegalAccessException e) {
			throw new ComponentExecutionException("Al instanciar la respuesta", e);
		}
		r.setEstado(Estado.newWithResultadoTrue());
		r.setContinuationstatus(Constants.PAGINATOR_TR_TAG);
		//Insis es 1 based
		int j = 0;
		for (int i = pr.getStartRow() - 1 ; i < pr.getStartRow() - 1 + pr.getRowCount() && i < regs.size(); i++, j++) {
			r.addRecord(regs.get(i));
		}
		logger.log(Level.FINE,"Agregados: " + j + " elements");
		
		String response = r.marshal();
		return response;
	}

	private void loadIfNeeded() throws Exception {
		if ( regs.size() == 0 && fileNames.size() > 0 ) {
			refresh();
		}
	}

	public void refresh() throws Exception {
		try {
			for (String filename : fileNames) {
				String source = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
						.getResourceAsStream(filename), Charset.forName("UTF-8"));
				ContinuationResponse responseWithRegs = getResponseUnmarshaller().unmarshalResponse(source);
				this.regs.addAll(responseWithRegs.getRecords());
			}
		} catch (IOException e) {
			logger.log(Level.SEVERE, "",e);
			throw new Exception(e);
		} catch (JAXBException e) {
			logger.log(Level.SEVERE, "",e);
			throw new Exception(e);
		}
		
	}
	public List<String> getFileNames() {
		return fileNames;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	public ResponseUnmarshaller getResponseUnmarshaller() {
		return responseUnmarshaller;
	}

	public void setResponseUnmarshaller(ResponseUnmarshaller responseUnmarshaller) {
		this.responseUnmarshaller = responseUnmarshaller;
	}

	public PaginateRequest getRequestUnmarshaller() {
		return requestUnmarshaller;
	}

	public void setRequestUnmarshaller(PaginateRequest requestUnmarshaller) {
		this.requestUnmarshaller = requestUnmarshaller;
	}

	public Class<? extends ContinuationResponse> getContinuationResponseClass() {
		return continuationResponseClass;
	}

	public void setContinuationResponseClass(Class<? extends ContinuationResponse> continuationResponseClass) {
		this.continuationResponseClass = continuationResponseClass;
	}

	public void addFileName(String filename) {
		getFileNames().add(filename);
	}

}
