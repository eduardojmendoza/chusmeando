package com.qbe.services.paginate.service1416;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import com.qbe.services.paginate.InsisMockPipe;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.Pipe;
import com.qbe.vbcompat.string.StringHolder;

public class OVLOGetRecibosTest {
	private static Logger logger = Logger.getLogger(OVLOGetRecibosTest.class.getName());

	String REQUEST =   "<Request>"+				
							"<USUARIO>EX009005L</USUARIO>"+		
							"<NIVELAS>PR</NIVELAS>"+		
							"<CLIENSECAS>100072343</CLIENSECAS>"+		
							"<FLAGBUSQ>T</FLAGBUSQ>"+		
							"<POLIZAS>"+		
								"<POLIZA>"+
									"<PROD>ICO1</PROD>"+
									"<POL>00174870</POL>"+
									"<CERTI>00000000000000</CERTI>"+
								"</POLIZA>"+	
							"</POLIZAS>"+		
						"</Request>";			

	@Test
	public void testBug859() throws Exception   {
		int pageSize = 200;
			
		//Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));

		
		OVLOGetRecibos ac = new OVLOGetRecibos();
		
		try {
			List<Pipe> pipes = this.createOneCMSTestPipe();
			pipes.addAll(this.createOneInsisTestPipe());
			ac.setTestPipes(pipes);  
			ac.setPageSize(pageSize);
			
			StringHolder responseSH = new StringHolder();
			
			ac.IAction_Execute(REQUEST, responseSH, null);
			
			Response1416 resp1416 = (Response1416) (new Response1416()).unmarshal(responseSH.getValue());
		    System.out.println(resp1416.marshal());	
	
		    
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * Recorre una query completa, paginando claro.
	 * Usa un pipe local de CMS, y uno local de Insis
	 * 
	 * @throws Exception
	 */
//	@Ignore
//	@Test
//	public void testWithInsisPipe() throws Exception {
//		int pageSize = 200;	
//		double numRegs = 52;
//		
//		Assume.assumeTrue(CurrentProfile.getProfileName().equals("uat"));
//		
//		String request = REQUEST;
//		PaginateRequest request1401 = new Request1401(); 
//		ResponseUnmarshaller rum = new Response1401Unmarshaller();
//		PaginatedActionCode action = new OVExigibleDetalles();
//		action.setTestPipes(action.createInsisPipes(request1401,OVExigibleDetalles.ACTION_CODE_1401,rum, pageSize)) ;		
//		
//		StringHolder sh = new StringHolder();
//		action.IAction_Execute(request, sh, null);
//		Response1401 resp1401 = (Response1401) (new Response1401()).unmarshal(sh.getValue());
//		
//		ContinuationStatus paginateFlag = resp1401.getContinuationStatus();
//		int numPages = 1;
//		logger.info("Pagina: "+ numPages + " trae " + resp1401.getRecords().size());
//		System.out.println(resp1401);
//		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
//		/*
//		 * while ( paginateFlag.equals(ContinuationStatus.TR )) {
//				numPages++;
//				String nextReq = resp1401.;
//				
//				action.IAction_Execute(nextReq, sh, null);
//				resp1401 = (Response1401) (new Response1401()).unmarshal(sh.getValue());
//				
//				
//				logger.info("Pagina: "+ numPages + " trae " + resp1401.getRecords().size());
//
//				paginateFlag = resp1401.getContinuationStatus();
//				
//		}
//		*/
//		
//		//assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1401.getRecords().size());
//		//assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
//		//assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
//	}

	
	private List<Pipe> createOneCMSTestPipe() {
		List<Pipe> pipes = new ArrayList<Pipe>();
		CMS1416MockPipe cms1 = new CMS1416MockPipe();
		pipes.add(cms1);

		return pipes;
	}
	
	private List<Pipe> createOneInsisTestPipe() throws IOException, JAXBException {
		List<Pipe> pipes = new ArrayList<Pipe>();
		
		String REG_FILE_NAME = "1416/insis/1416_insis_resp.xml";
		
		InsisPipe pipe = new InsisMockPipe(REG_FILE_NAME, Request1416.class, Response1416Unmarshaller.class, Response1416.class, OVLOGetRecibos.ACTION_CODE_1416);
		pipes.add(pipe);

		return pipes;
	}

}
