package com.qbe.services.paginate;

import java.net.MalformedURLException;

import javax.xml.bind.JAXBException;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.cms.osbconnector.OSBConnectorException;

public class InsisEmptyPipe extends InsisPipe {

	public InsisEmptyPipe() {
	}

	@Override
	public OSBConnector getConnector() {
		return null;
	}

	@Override
	protected CommRequest getNextSourceBlockRequest() {
		return new Request1107x9();
	}
	

	@Override
	protected String executeRequest(CommRequest nextRequest)
			throws OSBConnectorException, MalformedURLException, JAXBException {
		return "<Response><Estado mensaje=\"No se han encontrado resultados para la consulta solicitada\" resultado=\"false\" xmlns:cms=\"http://cms.services.qbe.com/\"/><MSGEST xmlns:cms=\"http://cms.services.qbe.com/\">ER</MSGEST></Response>" ;
	}
}
