package com.qbe.services.paginate.service1428;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.verification.AtLeast;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.util.StreamUtils;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.paginate.CMSAnswer;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.InsisAnswer;
import com.qbe.services.paginate.OSBConnectorLocator;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.mqgestion.GetDeudaCobradaCanalesDetalle;
import com.qbe.vbcompat.framework.jaxb.Estado;
import com.qbe.vbcompat.string.StringHolder;

public class GetDeudaCobradaCanalesDetalle1428MockitoTest {

	private static Logger logger = Logger.getLogger(GetDeudaCobradaCanalesDetalle1428MockitoTest.class.getName());

	@Before
     public void setUp() throws OSBConnectorException, IOException {
		
		 OSBConnectorLocator locator = mock(OSBConnectorLocator.class);
		 CMSAnswer cmsAnswer = new CMSAnswer();
		 cmsAnswer.addReqRespFiles("1667/requestCMS_pagina_1.xml", "1667/responseCMS_pagina_1.xml");
		 cmsAnswer.addReqRespFiles("1667/requestCMS_pagina_2.xml", "1667/responseCMS_pagina_2.xml");
		 cmsAnswer.addReqRespFiles("1667/requestCMS_pagina_3.xml", "1667/responseCMS_pagina_3.xml");
		 
		 OSBConnector mockedCMS = mock(OSBConnector.class);
		 when(mockedCMS.executeRequest(anyString(), anyString())).thenAnswer(cmsAnswer);
         when(locator.getCMSConnector()).thenReturn(mockedCMS);

         InsisAnswer insis1428 = new InsisAnswer();
         insis1428.setContinuationResponseClass(Response1428.class);
         insis1428.setRequestUnmarshaller(new Request1428());
         insis1428.setResponseUnmarshaller(new Response1428Unmarshaller());
         insis1428.addFileName("1667/responseInsis_pagina_1.xml");
         
		 OSBConnector mockedInsis1428 = mock(OSBConnector.class);
		 when(mockedInsis1428.executeRequest(anyString(), anyString())).thenAnswer(insis1428);
         
		 when(locator.getInsis1428Connector()).thenReturn(mockedInsis1428);
         
         OSBConnectorLocator.instance = locator;
	}


	@After
    public void tearDown() throws OSBConnectorException, IOException {
		OSBConnectorLocator.instance = null;
	}
	
	/**
	 * Bug 1667: Recorre un resultset que tiene resultados de Insis y CMS, y los resultados de Insis no 
	 * entran en la primer página xq el resultset está ordenado alfabéticamente y el resultset de Insis arranca en MORELI
	 * 
	 * @throws Exception
	 */
	@Test
	public void testBug1667() throws Exception {
		int pageSize = GetDeudaCobradaCanalesDetalle.MESSAGE_1428_PAGE_SIZE;	
		double numRegs = 22 /* insis */ + 150 * 3 /* ais */;
		
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		
		String request = this.getRequest1667();
		
		PaginatedActionCode action = new GetDeudaCobradaCanalesDetalle();
		
		boolean moreliFound = false;
		int moreliFoundCount = 0;
		
		StringHolder sh = new StringHolder();
		action.IAction_Execute(request, sh, null);
		
		Response1428 resp1428 = (Response1428) (new Response1428()).unmarshal(sh.getValue());
		validateResponse1428(resp1428);

		List<Canal1428> canales = resp1428.getCampos().getRecords();
		for (Canal1428 canal1428 : canales) {
			if ( canal1428.getCliendes().trim().equals("MORELI , MARIANITA ".trim()) ) {
				moreliFound = true;
				moreliFoundCount++;
			}
		}

		ContinuationStatus paginateFlag = resp1428.getContinuationStatus();
		int numPages = 1;
		logger.info("Pagina: "+ numPages + " trae " + resp1428.getRecords().size());
		logger.info("Continua: " + resp1428.getRequest().getEstado().toString() + "  Continuacion " +  paginateFlag);
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		
		while ( paginateFlag.equals(ContinuationStatus.TR )) {
				numPages++;
				String nextReq = resp1428.getCampos().getRequest().marshall();
				
				action = new GetDeudaCobradaCanalesDetalle();

				action.IAction_Execute(nextReq, sh, null);
				resp1428 = (Response1428) (new Response1428()).unmarshal(sh.getValue());

				for (Canal1428 canal1428 : resp1428.getCampos().getRecords()) {
					if ( canal1428.getCliendes().trim().equals("MORELI , MARIANITA ".trim()) ) {
						moreliFound = true;
						moreliFoundCount++;
					}
				}
				validateResponse1428(resp1428);
				paginateFlag = resp1428.getContinuationStatus();
		}
		
		assertTrue("No está MORELI MARIANITA", moreliFound);
		assertEquals("No aparecen todos los registros de MORELI MARIANITA", 6, moreliFoundCount);
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1428.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
		
		// 2 veces por página para las primeras xq devolvemos 150 y de AIS vienen 150, más 1 para la última
		verify(OSBConnectorLocator.instance, atLeast(8)).getCMSConnector();
		//
		verify(OSBConnectorLocator.instance, atLeast(4)).getInsis1428Connector();
		verify(OSBConnectorLocator.instance, never()).getInsis1401Connector();
		
	}

	private String getRequest1667() throws IOException {
		String source = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("1667/requestBug1667.xml"), Charset.forName("UTF-8"));
		return source;
	}

	private void validateResponse1428(Response1428 resp1428) {
		assertFalse("Vino false", resp1428.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
		assertTrue("El response vino sin Request de continuación", resp1428.getRequest() != null);		
	}

	
}
