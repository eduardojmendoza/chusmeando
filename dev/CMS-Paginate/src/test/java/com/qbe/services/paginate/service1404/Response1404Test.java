package com.qbe.services.paginate.service1404;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;

import javax.xml.bind.JAXBException;

import org.junit.Test;
import org.springframework.util.StreamUtils;

import com.qbe.services.paginate.ResponseRecord;

public class Response1404Test {

	@Test
	public void testRecalculateTots() throws IOException, JAXBException, ParseException {
		String resp1404Xml = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("resp1404_1.xml"), Charset.forName("UTF-8"));

		Response1404 resp1404 = (Response1404) (new Response1404()).unmarshal(resp1404Xml);
		resp1404.recalculateTots();
		assertTrue("Faltan tots", resp1404.getTots().size() == 2);
		for (ResponseRecord tot : resp1404.getTots()) {
			Tot1404 tot1404 = (Tot1404)tot;
			if ( tot1404.getTmon().equals("$")) {
//				System.out.println(tot.getT30());
				assertTrue("Suma en pesos erronea", tot1404.getT30().equals("1.000,50"));
			} else if ( tot1404.getTmon().equals("U$S")) {
//				System.out.println(tot.getT30());
				assertTrue("Suma en dolares erronea", tot1404.getT30().equals("1.000.500,50"));
			}
		}
		
		System.out.println(resp1404.marshal());
	}
}
