package com.qbe.services.paginate.service1109.mocks;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.springframework.util.StreamUtils;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.CMSPipe;
import com.qbe.services.paginate.CMSPipePointer;
import com.qbe.services.paginate.CommRequest;
import com.qbe.services.paginate.Constants;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.MessagePipe;
import com.qbe.services.paginate.MockPipePointer;
import com.qbe.services.paginate.PipePointer;
import com.qbe.services.paginate.PipeException;
import com.qbe.services.paginate.Request1107x9;
import com.qbe.services.paginate.TestPipe;
import com.qbe.services.paginate.service1109.OVOpeEmiDetallePaginada;
import com.qbe.services.paginate.service1109.Response1109Unmarshaller;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Un pipe que trae datos del actionCode 1109
 * 
 * @author ramiro
 *
 */
public class CMS1109MockPipe extends CMSPipe implements Serializable, TestPipe {

	private static Logger logger = Logger.getLogger(CMS1109MockPipe.class.getName());
	
	private String[] regsFileNames = new String[] { "1109trs/resp1.txt",  
			"1109trs/resp2.txt",  "1109trs/resp3.txt",  "1109trs/resp4.txt",  
			"1109trs/resp5.txt",  "1109trs/resp6.txt",  "1109trs/resp7.txt",  
			"1109trs/resp8.txt",  "1109trs/resp9.txt",  "1109trs/resp10.txt"};

	protected int nextBlockIndex = 0;
	protected int executeRequestCount = 0;
	
	public CMS1109MockPipe() {
		this.setRequest(new Request1107x9());
		this.setResponseUnmarshaller(new Response1109Unmarshaller());
		this.setActionCode(OVOpeEmiDetallePaginada.ACTION_CODE_1109);
	}

	protected String executeRequest(CommRequest nextRequest)
			throws OSBConnectorException, MalformedURLException, JAXBException {
		String response;
		try {
			logger.log(Level.FINE,"Recupero block: " + nextBlockIndex);
			executeRequestCount++;
			logger.log(Level.FINE,"executeRequestCount: " + executeRequestCount);
			
			response = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(regsFileNames[nextBlockIndex++]), Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
			throw new OSBConnectorException("Al levantar uno de los archivos", e);
		}
		return response;
	}

	@Override
	public void reset() {
		super.reset();
		nextBlockIndex = 0;
	}

	@Override
	public PipePointer currentPointer() {
		MockPipePointer p = new MockPipePointer();
		p.setBlock(nextBlockIndex);
		p.setExecuteRequestCount(executeRequestCount);
		p.setPipePointer(super.currentPointer());
		return p;
	}

	@Override
	public void seek(PipePointer pp) throws PipeException {
		MockPipePointer cpp = (MockPipePointer)pp;
		nextBlockIndex = cpp.getBlock() - 1;
		logger.log(Level.FINE,"Seek block: " + nextBlockIndex);
		super.seek(cpp.getPipePointer());
		executeRequestCount = cpp.getExecuteRequestCount();
	}
	
	@Override
	public int getSourceRequests() {
		return executeRequestCount;
	}

	
}
