package com.qbe.services.paginate;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.qbe.services.paginate.service1109.OVOpeEmiDetallePaginada;
import com.qbe.services.paginate.service1109.Reg1109;
import com.qbe.services.paginate.service1109.Response1109;
import com.qbe.vbcompat.framework.jaxb.Estado;
import com.qbe.vbcompat.string.StringHolder;

/**
 * Testea que la cantidad de llamadas a los sources sea la mínima
 * 
 * @author ramiro
 *
 */
@RunWith(Parameterized.class)
public class SourceCallsTest {

	protected List<Pipe> pipes;
	protected Integer[] sourceRequests;
	protected int pageSize;
	protected String[] pageFirstClides;
	
	
	public SourceCallsTest(List<Pipe> pipes, Integer[] sourceRequests, int pageSize, String[] pageFirstClides) {
		this.pipes = pipes;
		this.sourceRequests = sourceRequests;
		this.pageSize = pageSize;
		this.pageFirstClides = pageFirstClides;
	}
	
	/**
	 * Test data generator. This method is called the the JUnit parameterized
	 * test runner and returns a Collection of Arrays. For each Array in the
	 * Collection, each array element corresponds to a parameter in the
	 * constructor.
	 * @throws JAXBException 
	 * @throws IOException 
	 */
	@Parameters
	public static Collection<Object[]> generateData() throws IOException, JAXBException {
		List<Object []> params = new ArrayList<Object[]>();
		// sourceRequests es 10 porque lo tienen cableado
		// TODO ¿en CMS tengo que manterner el tamaño de página?
		params.add(new Object[] {Pipes1107x09Factory.createCMSTestPipes(), new Integer[] {10,10}, 179, null});
		// 4 queries en c/u, porque al ser 2 para un pageSize de 100, toma 50 de cada pipe
		// y cada vez que pide una página, genera un request al source ( no serializa los resultados que no consume de la página anterior )
		params.add(new Object[] {Pipes1107x09Factory.create2InsisTestPipes(100), new Integer[] {4,4}, 100, null});
		params.add(new Object[] {Pipes1107x09Factory.createInsisTestPipe(10), new Integer[] {20}, 100, new String[] { "REG1", "REG101"}});
		return params;
		
	}
	
	public List<Pipe> getPipes() {
		return pipes;
	}

	public void setPipes(List<Pipe> pipes) {
		this.pipes = pipes;
	}

	/**
	 * Recorre una query completa
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFullNavigationLocal() throws Exception {
		
		String requestTemplate = Pipes1107x09Factory.REQUEST_TEMPLATE;
		String request = String.format(requestTemplate, "");
		PaginatedActionCode action = new OVOpeEmiDetallePaginada();
		action.setTestPipes(this.pipes);
		action.setPageSize(pageSize);
		
		StringHolder sh = new StringHolder();
		int result = action.IAction_Execute(request, sh, null);
		Response1109 resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
		assertFalse("Vino false", resp1109.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
		
		int pageNum = 0;
		if ( pageFirstClides != null ) {
			String expectedClides = pageFirstClides[pageNum];
			assertEquals(expectedClides, ((Reg1109)resp1109.getRecords().get(0)).getClides());
		}
		
		ContinuationStatus paginateFlag = resp1109.getContinuationStatus();
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR)) {
			pageNum++;
			String nextReq = String.format(requestTemplate,resp1109.getOpecont());
			result = action.IAction_Execute(nextReq, sh, null);
			resp1109 = (Response1109) (new Response1109()).unmarshal(sh.getValue());
			assertFalse("Vino false", resp1109.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
			if ( pageFirstClides != null ) {
				String expectedClides = pageFirstClides[pageNum];
				assertEquals(expectedClides, ((Reg1109)resp1109.getRecords().get(0)).getClides());
			}
			
			paginateFlag = resp1109.getContinuationStatus();
		}
		
//		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
		
		for (int i = 0; i < action.lastPaginator.getPipes().size(); i++) {
			TestPipe tp = (TestPipe)action.lastPaginator.getPipes().get(i);
			assertEquals("Pipe " + i,this.sourceRequests[i].intValue() , tp.getSourceRequests());
		}
	}
	
}
