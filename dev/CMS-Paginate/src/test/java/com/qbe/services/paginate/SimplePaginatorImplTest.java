package com.qbe.services.paginate;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.qbe.vbcompat.base64.Base64Encoding;

public class SimplePaginatorImplTest {

	private static final int PAGE_SIZE = 4;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	/**
	 * Iteración, manteniendo el paginator
	 * @throws PipeException 
	 */
	@Test
	public void testIteration() throws PipeException {
		List<Pipe> pipes = getPipes();
		Paginator paginator = getPaginator();
		paginator.setPipes(pipes);
		
		Page nextPage = paginator.fetchPage(0, PAGE_SIZE);
		List<Object> elems = nextPage.getElements();
		assertFirstPage(elems);
		
		Page nextPage2 = paginator.fetchPage(1, PAGE_SIZE);
		List<Object> elems2 = nextPage2.getElements();
		assertSecondPage(elems2);

		Page nextPage3 = paginator.fetchPage(2, PAGE_SIZE);
		List<Object> elems3 = nextPage3.getElements();
		assertTrue(elems3.size() == 0);
	}

	/**
	 * Iteración, serializando el paginator entre páginas
	 * 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws PipeException 
	 */
	@Test
	public void testIterationStateless() throws IOException, ClassNotFoundException, PipeException {
	    String serializado = getPaginator().serialize();
//	    System.out.println("serializado: " + serializado);

	    Paginator paginatorPagina0 = Paginator.deserialize(serializado);
		Page nextPage = paginatorPagina0.fetchPage(0, PAGE_SIZE);
		List<Object> elems = nextPage.getElements();
		assertFirstPage(elems);
		
	    Paginator paginatorPagina1 = Paginator.deserialize(paginatorPagina0.serialize());
	      
		Page nextPage2 = paginatorPagina1.fetchPage(1, PAGE_SIZE);
		List<Object> elems2 = nextPage2.getElements();
		assertSecondPage(elems2);

	    Paginator paginatorPagina2 = Paginator.deserialize(paginatorPagina1.serialize());
		Page nextPage3 = paginatorPagina2.fetchPage(2, PAGE_SIZE);
		List<Object> elems3 = nextPage3.getElements();
		assertTrue(elems3.size() == 0);
	}

	public void assertFirstPage(List<Object> elems) {
		assertTrue(elems.size() == PAGE_SIZE);
		assertTrue(elems.get(0).equals("a"));
		assertTrue(elems.get(1).equals("b"));
		assertTrue(elems.get(2).equals("c"));
		assertTrue(elems.get(3).equals("d"));
	}

	public Paginator getPaginator() {
		Paginator paginator = new SequentialPaginator();
		List<Pipe> pipes = getPipes();
		paginator.setPipes(pipes);
		return paginator;
	}
	
	public void assertSecondPage(List<Object> elems2) {
		assertTrue(elems2.size() == PAGE_SIZE);
		assertTrue(elems2.get(0).equals("e"));
		assertTrue(elems2.get(1).equals("f"));
		assertTrue(elems2.get(2).equals("g"));
		assertTrue(elems2.get(3).equals("h"));
	}
	

	
	public List<Pipe> getPipes() {
		StringPipe pipe1 = getPipe1();
		StringPipe pipe2 = getPipe2();

		List<Pipe> pipes = new ArrayList<Pipe>();
		pipes.add(pipe1);
		pipes.add(pipe2);
		return pipes;
	}

	public StringPipe getPipe2() {
		StringPipe pipe2 = new StringPipe();
		pipe2.getSource().addAll(Arrays.asList("b", "g", "h"));
		assertTrue(pipe2.getElements().size() == 0);
		pipe2.setSourceBulkSize(2);
		return pipe2;
	}

	public StringPipe getPipe1() {
		StringPipe pipe1 = new StringPipe();
		pipe1.getSource().addAll(Arrays.asList("a","c","d","e","f"));
		assertTrue(pipe1.getElements().size() == 0);
		pipe1.setSourceBulkSize(3);
		return pipe1;
	}

}
