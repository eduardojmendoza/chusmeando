package com.qbe.services.paginate.service1401;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.springframework.util.StreamUtils;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.CMSPipe;
import com.qbe.services.paginate.CommRequest;
import com.qbe.services.paginate.MockPipePointer;
import com.qbe.services.paginate.PipeException;
import com.qbe.services.paginate.PipePointer;
import com.qbe.services.paginate.TestPipe;

/**
 * Un pipe que trae datos del actionCode 1401
 * 
 * @author cbonilla
 *
 */
public class CMS1401MockPipe extends CMSPipe implements Serializable, TestPipe {

	private static Logger logger = Logger.getLogger(CMS1401MockPipe.class.getName());
	
	private String[] regsFileNames = new String[] { "1401/cms/resp1_1.xml","1401/cms/resp1_2.xml","1401/cms/resp1_3.xml"};

	protected int nextBlockIndex = 0;
	protected int executeRequestCount = 0;
	
	public CMS1401MockPipe() {
		this.setRequest(new Request1401());
		this.setResponseUnmarshaller(new Response1401Unmarshaller());
		this.setActionCode(OVExigibleDetalles.ACTION_CODE_1401);
	}


	protected String executeRequest(CommRequest nextRequest)
			throws OSBConnectorException, MalformedURLException, JAXBException {
		String response;
		try {
			logger.log(Level.FINE,"Recupero block: " + nextBlockIndex);
			executeRequestCount++;
			logger.log(Level.FINE,"executeRequestCount: " + executeRequestCount);
			
			response = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(regsFileNames[nextBlockIndex++]), Charset.forName("UTF-8"));
			
		} catch (IOException e) {
			e.printStackTrace();
			throw new OSBConnectorException("Al levantar uno de los archivos", e);
		}
		return response;
	}
	
	
	@Override
	public void reset() {
		super.reset();
		nextBlockIndex = 0;
	}

	@Override
	public PipePointer currentPointer() {
		MockPipePointer p = new MockPipePointer();
		p.setBlock(nextBlockIndex);
		p.setExecuteRequestCount(executeRequestCount);
		p.setPipePointer(super.currentPointer());
		return p;
	}

	@Override
	public void seek(PipePointer pp) throws PipeException {
		MockPipePointer cpp = (MockPipePointer)pp;
		nextBlockIndex = cpp.getBlock() - 1;
		logger.log(Level.FINE,"Seek block: " + nextBlockIndex);
		super.seek(cpp.getPipePointer());
		executeRequestCount = cpp.getExecuteRequestCount();
	}
	
	@Override
	public int getSourceRequests() {
		return executeRequestCount;
	}

	private static final String BASIC_REQUEST =  	"<Response>" +
	"<Estado 	resultado=\"true\" mensaje=\"\"/>" +
	"<MSGEST>TR</MSGEST>" +
     "<PRODUCTO/>" +
     "<POLIZA/>" +
     "<NROCONS/>" +
     "<NROORDEN/>" +
     "<DIRORDEN/>" +
     "<TFILTRO/>" +
     "<VFILTRO/>" +
     "<PAGINADO/>" +
	"<REGS>" +
		"<REG>" +
         " <AGE>PR-3233</AGE>" +
         " <SINI>0</SINI>" +
         " <RAMO>1</RAMO>" +
         " <CLIDES>COSTABEL, GUSTAVO FERNANDO</CLIDES>" +
         " <PROD>AUS1</PROD>" +
         " <POL>00000001</POL>" +
         " <CERPOL>0000</CERPOL>" +
         " <CERANN>0001</CERANN>" +
         " <CERSEC>558362</CERSEC>" +
         " <MON>$</MON>" +
         " <IMPTO>419,44</IMPTO>" +
         " <I_30>0,00</I_30>" +
         " <I_60>0,00</I_60>" +
          "<I_90>0,00</I_90>" +
          "<I_M90>419,44</I_M90>" +
          "<EST>VIG</EST>" +
          "<COB>DEB</COB>" +
        "</REG>" +
	"</REGS>"    +
"</Response>";	
}
