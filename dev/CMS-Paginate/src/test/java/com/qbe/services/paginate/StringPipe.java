package com.qbe.services.paginate;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import java.util.ArrayList;

/**
 * Pipe que retorna Strings, tomado de un source fijo con el que se inicializa
 * 
 * @author ramiro
 *
 */
public class StringPipe extends Pipe implements Serializable {
	
	private static final int DEFAULT_SOURCE_BULK_SIZE = 3;

	private static final int FIRST_SOURCE_INDEX = 0;

	private List<String> source = new ArrayList<String>();
	
	private int nextSource = FIRST_SOURCE_INDEX;
	
	private int sourceBulkSize = DEFAULT_SOURCE_BULK_SIZE;
	
	/**
	 * Carga la página siguiente y actualiza la pageTable
	 * 
	 */
	@Override
	protected void fetchNextSourceBlock() {
		primFetchSourceBlock();
	}

	public void primFetchSourceBlock() {
		for (int i = 0; i < getSourceBulkSize() && nextSource < source.size(); i++) {
			this.getElements().add(source.get(nextSource++));
		}
	}

	public boolean isSourceExhausted() {
		return false;
	}


	public List<String> getSource() {
		return source;
	}

	public void setSource(List<String> source) {
		this.source = source;
	}

	public int getNextSource() {
		return nextSource;
	}

	public void setNextSource(int nextSource) {
		this.nextSource = nextSource;
	}

	public int getSourceBulkSize() {
		return sourceBulkSize;
	}

	public void setSourceBulkSize(int sourceBulkSize) {
		this.sourceBulkSize = sourceBulkSize;
	}

	@Override
	public void reset() {
		super.reset();
		nextSource = FIRST_SOURCE_INDEX;
	}

	@Override
	public void seek(PipePointer pp) {
	}

	@Override
	public PipePointer currentPointer() {
		return null;
	}

}
