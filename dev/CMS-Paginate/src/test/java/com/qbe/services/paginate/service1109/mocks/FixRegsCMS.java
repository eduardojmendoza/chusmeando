package com.qbe.services.paginate.service1109.mocks;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.util.StreamUtils;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.CMSPipe;
import com.qbe.services.paginate.CMSPipePointer;
import com.qbe.services.paginate.CommRequest;
import com.qbe.services.paginate.Constants;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MessagePipe;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.PipePointer;
import com.qbe.services.paginate.PipeException;
import com.qbe.services.paginate.Request1107x9;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.services.paginate.TestPipe;
import com.qbe.services.paginate.service1109.Reg1109;
import com.qbe.services.paginate.service1109.Response1109;
import com.qbe.services.paginate.service1109.Response1109Unmarshaller;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.jaxb.Estado;

/**
 * Acomoda los CLIDES de los archivos 1190trs para que sea más fácil seguirlos en los tests
 * 
 * @author ramiro
 *
 */
public class FixRegsCMS {

	private String[] regsFileNames = new String[] { "1109trs/resp1.txt",  
			"1109trs/resp2.txt",  "1109trs/resp3.txt",  "1109trs/resp4.txt",  
			"1109trs/resp5.txt",  "1109trs/resp6.txt",  "1109trs/resp7.txt",  
			"1109trs/resp8.txt",  "1109trs/resp9.txt",  "1109trs/resp10.txt"};

	protected Response1109 responseWithRegs;
	protected int numReg = 1;
	
	public FixRegsCMS() {
	}

	public static void main(String[] args) throws IOException, JAXBException {
		FixRegsCMS fr = new FixRegsCMS();
		fr.go();
	}
	
	public void go() throws IOException, JAXBException {
		for (int i = 0; i < regsFileNames.length; i++) {
			String regFileName = regsFileNames[i];
			loadRegs(regFileName);
			changeRegs(regFileName);
			saveRegs(regFileName);
		}
	}

	public void saveRegs(String regFileName) throws JAXBException, IOException {
		String m = responseWithRegs.marshal();
		FileUtils.writeStringToFile(new File(regFileName), m, Charset.forName("UTF-8"));
	}
	
	public void changeRegs(String regFileName) {
		for (ResponseRecord record : responseWithRegs.getRecords()) {
			Reg1109 r = (Reg1109)record;
			r.setClides("REG" + numReg);
			numReg++;
		}
	}
	
	private void loadRegs(String regFileName) throws IOException, JAXBException {
		String source = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(regFileName), Charset.forName("UTF-8"));
		Response1109Unmarshaller unm = new Response1109Unmarshaller();
		responseWithRegs = (Response1109) unm.unmarshalResponse(source);
	}
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	
}
