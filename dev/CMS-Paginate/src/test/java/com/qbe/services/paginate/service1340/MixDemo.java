package com.qbe.services.paginate.service1340;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
 
public class MixDemo {
 
    public static void main(String[] args) throws Exception {
        JAXBContext jc = JAXBContext.newInstance(Customer.class, Gato.class, Perro.class);
 
        Customer customer = new Customer();
        customer.getData().add(new Gato());
        customer.getData().add(new Perro());
 
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(customer, System.out);
    }
}


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class Customer {
 
	@XmlElementWrapper(name="dataes")
	@XmlElement(name="data1111")
    private List<Object> data;
     
    public Customer() {
    	data = new ArrayList<Object>();
    }
 
    public List<Object> getData() {
        return data;
    }
 
    public void setData(List<Object> data) {
        this.data = data;
    }
 
}

@XmlType(name="Perro")
@XmlAccessorType(XmlAccessType.FIELD)
class Perro {

	@XmlElement
	public String name = "pichicho";
    
	public Perro() {
    }

}

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class Gato {

	@XmlElement
	public String name = "de Antonia";
    
	public Gato() {
    }

}