package com.qbe.services.paginate.service1302;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.springframework.util.StreamUtils;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.CMSPipe;
import com.qbe.services.paginate.CommRequest;
import com.qbe.services.paginate.MockPipePointer;
import com.qbe.services.paginate.PipeException;
import com.qbe.services.paginate.PipePointer;
import com.qbe.services.paginate.TestPipe;

/**
 * Un pipe que trae datos del actionCode 1302
 * 
 * @author ramiro
 *
 */
public class CMS1302MockPipe extends CMSPipe implements Serializable, TestPipe {

	private static Logger logger = Logger.getLogger(CMS1302MockPipe.class.getName());
	
	private String[] regsFileNames = new String[] { "1302/resp1302_1_1.xml", "1302/resp1302_1_2.xml"};

	protected int nextBlockIndex = 0;
	protected int executeRequestCount = 0;
	
	protected Request1302 lastExecutedRequest = null;

	/**
	 * Cantidad de registros en el CMS Pipe
	 */
	static final int RESPONSE_1302_1_REGS_SIZE = 20 + 7;
	
	public CMS1302MockPipe() {
		this.setRequest(new Request1302());
		this.setResponseUnmarshaller(new Response1302Unmarshaller());
		this.setActionCode(OVSiniConsulta.ACTION_CODE_1302);
	}

	protected String executeRequest(CommRequest nextRequest)
			throws OSBConnectorException, MalformedURLException, JAXBException {
		lastExecutedRequest = (Request1302)nextRequest;
		String response;
		try {
			logger.log(Level.FINE,"Recupero block: " + nextBlockIndex);
			executeRequestCount++;
			logger.log(Level.FINE,"executeRequestCount: " + executeRequestCount);
			
			response = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(regsFileNames[nextBlockIndex++]), Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
			throw new OSBConnectorException("Al levantar uno de los archivos", e);
		}
		return response;
	}

	@Override
	public void reset() {
		super.reset();
		nextBlockIndex = 0;
	}

	@Override
	public PipePointer currentPointer() {
		MockPipePointer p = new MockPipePointer();
		p.setBlock(nextBlockIndex);
		p.setExecuteRequestCount(executeRequestCount);
		p.setPipePointer(super.currentPointer());
		return p;
	}

	@Override
	public void seek(PipePointer pp) throws PipeException {
		MockPipePointer cpp = (MockPipePointer)pp;
		nextBlockIndex = cpp.getBlock() - 1;
		logger.log(Level.FINE,"Seek block: " + nextBlockIndex);
		super.seek(cpp.getPipePointer());
		executeRequestCount = cpp.getExecuteRequestCount();
	}
	
	@Override
	public int getSourceRequests() {
		return executeRequestCount;
	}

	public Request1302 getLastExecutedRequest() {
		return lastExecutedRequest;
	}

	public void setLastExecutedRequest(Request1302 lastExecutedRequest) {
		this.lastExecutedRequest = lastExecutedRequest;
	}

	
}
