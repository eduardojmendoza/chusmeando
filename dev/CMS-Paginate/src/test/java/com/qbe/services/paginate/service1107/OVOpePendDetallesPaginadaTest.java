package com.qbe.services.paginate.service1107;

import static org.junit.Assert.*;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.vbcompat.framework.jaxb.Estado;
import com.qbe.vbcompat.string.StringHolder;

public class OVOpePendDetallesPaginadaTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Ignore
	public void testBug452() throws JAXBException, CurrentProfileException {
		if ( !CurrentProfile.getProfileName().equals("test")) {
			return;
		}
		String requestTemplate = "		<Request>" +
"			<USUARIO>EX009005L</USUARIO>" +
"			<NIVELAS>PR</NIVELAS>" +
"			<CLIENSECAS>101555163</CLIENSECAS>" +
"			<NIVEL1 />" +
"			<CLIENSEC1 />" +
"			<NIVEL2 />" +
"			<CLIENSEC2 />" +
"			<NIVEL3></NIVEL3>" +
"			<CLIENSEC3></CLIENSEC3>" +
"			<FECDES>20130801</FECDES>" +
"			<FECHAS>20130901</FECHAS>" +
"			<FECCONT />" +
"			<PRODUCCONT />" +
"			<POLIZACONT />" +
"			<OPECONT>%s</OPECONT>" +
"		</Request>";
		String request = String.format(requestTemplate, "");
		PaginatedActionCode action = new OVOpePendDetallesPaginada();
		
		StringHolder sh = new StringHolder();
		int result = action.IAction_Execute(request, sh, null);
		Response1107 resp1107 = (Response1107) (new Response1107()).unmarshal(sh.getValue());
		assertFalse("Vino false" + sh.getValue(), resp1107.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
		
		ContinuationStatus paginateFlag = resp1107.getContinuationStatus();
		int numPages = 0;
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		while ( paginateFlag.equals(ContinuationStatus.TR)) {
			numPages++;

			String nextReq = String.format(requestTemplate,resp1107.getOpecont());
			result = action.IAction_Execute(nextReq, sh, null);
			resp1107 = (Response1107) (new Response1107()).unmarshal(sh.getValue());
			assertFalse("Vino false: " + sh.getValue(), resp1107.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
			paginateFlag = resp1107.getContinuationStatus();
		}
		assertTrue("No recorrió las páginas que debería", numPages > 0);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
	}
}
