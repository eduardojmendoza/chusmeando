package com.qbe.services.paginate;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.util.StreamUtils;

/**
 * Un Answer que devuelve los registros que matchean con el request recibido, como hace AIS
 * El matching lo hace por igualdad ( String.equals() ) o usando similaridad según Diff de XmlUnit, comparando
 * XMLs e ignorando whitespace, comentarios, orden de atributos/elementos. 
 * 
 * @see org.custommonkey.xmlunit.Diff.similar()
 * @see InsisAnswer
 * 
 * @author ramiro
 *
 */
public class CMSAnswer implements Answer<String> {

	private static Logger logger = Logger.getLogger(CMSAnswer.class.getName());

	protected  Map<String, String> reqRespfilenamePairs = new HashMap<String, String>();

	protected  Map<String, String> reqRespPairs = new HashMap<String, String>();
	
	protected ResponseUnmarshaller responseUnmarshaller = null;
	
	protected PaginateRequest requestUnmarshaller = null;

	protected Class<? extends ContinuationResponse> continuationResponseClass;

	public CMSAnswer() {
	}

	@Override
	public String answer(InvocationOnMock invocation) throws Throwable {
		loadIfNeeded();

		Object[] args = invocation.getArguments();
		String actionCode = (String)args[0];
		assertNotNull(actionCode);
		String requestXML = (String)args[1];
		assertNotNull(requestXML);

		logger.log(Level.INFO, "actionCode: " + actionCode);
		String response = reqRespPairs.get(requestXML);
		if ( response == null ) {
			//Si no encontré un match exacto pruebo buscar similares usando XmlUnit
			
			XMLUnit.setCompareUnmatched(true);
			XMLUnit.setIgnoreAttributeOrder(true);
			XMLUnit.setIgnoreComments(true);
			XMLUnit.setIgnoreDiffBetweenTextAndCDATA(true);
			XMLUnit.setIgnoreWhitespace(true);
			for (String eachRequest : reqRespPairs.keySet()) {
				Diff diff = XMLUnit.compareXML(eachRequest, requestXML);
				if ( diff.similar() ) {
					return reqRespPairs.get(eachRequest);
				}
			}
			throw new Exception("No se encontró un match para el request: " + requestXML);
		}
		return response;
	}

	private void loadIfNeeded() throws Exception {
		if ( reqRespPairs.size() == 0 && reqRespfilenamePairs.size() > 0 ) {
			refresh();
		}
	}

	public void refresh() throws Exception {
		try {
			for (String key : reqRespfilenamePairs.keySet()) {
				 String request = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
							.getResourceAsStream(key), Charset.forName("UTF-8"));
				 String response = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
							.getResourceAsStream(reqRespfilenamePairs.get(key)), Charset.forName("UTF-8"));
				 reqRespPairs.put(request, response);
			}
		} catch (IOException e) {
			logger.log(Level.SEVERE, "",e);
			throw new Exception(e);
		}
		
	}
	
	public ResponseUnmarshaller getResponseUnmarshaller() {
		return responseUnmarshaller;
	}

	public void setResponseUnmarshaller(ResponseUnmarshaller responseUnmarshaller) {
		this.responseUnmarshaller = responseUnmarshaller;
	}

	public PaginateRequest getRequestUnmarshaller() {
		return requestUnmarshaller;
	}

	public void setRequestUnmarshaller(PaginateRequest requestUnmarshaller) {
		this.requestUnmarshaller = requestUnmarshaller;
	}

	public Class<? extends ContinuationResponse> getContinuationResponseClass() {
		return continuationResponseClass;
	}

	public void setContinuationResponseClass(Class<? extends ContinuationResponse> continuationResponseClass) {
		this.continuationResponseClass = continuationResponseClass;
	}

	public void addReqRespFiles(String reqFilename, String respFilename) {
		reqRespfilenamePairs.put(reqFilename, respFilename);
	}

}
