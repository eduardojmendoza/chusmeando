package com.qbe.services.paginate;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.util.StreamUtils;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.CommRequest;
import com.qbe.services.paginate.Constants;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MockPipePointer;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.PipeException;
import com.qbe.services.paginate.PipePointer;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.services.paginate.ResponseUnmarshaller;
import com.qbe.services.paginate.TestPipe;
import com.qbe.services.paginate.mqgestion.GetDeudaCobradaCanalesDetalle;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.jaxb.Estado;

/**
 * Un pipe de Insis mockeado.
 * Se carga con un archivo que contiene el result set completo, y este pipe va devolviendo los subsets ( páginas ) 
 * como hace Insis
 * 
 * FIXME:
 * No maneja el ResultSetID, debería tener el mismo comportamiento q Insis
 * 
 * @author ramiro
 *
 */
public class InsisMockPipe extends InsisPipe implements Serializable, TestPipe {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(InsisMockPipe.class.getName());
	
	public String regsFileName;

	protected List<ResponseRecord> regs = new ArrayList<ResponseRecord>();
	
	protected transient int executeRequestCount = 0;
	
	protected Class<? extends ContinuationResponse> continuationResponseClass;
	
	public InsisMockPipe(String regsFileName, Class<? extends PaginateRequest> requestClass, Class<? extends ResponseUnmarshaller> unmarshallerClass, Class<? extends ContinuationResponse> continuationResponseClass, String actionCode) throws IOException, JAXBException {
		PaginateRequest pr;
		try {
			pr = requestClass.newInstance();
		} catch (InstantiationException e) {
			throw new ComponentExecutionException("Al instanciar la respuesta", e);
		} catch (IllegalAccessException e) {
			throw new ComponentExecutionException("Al instanciar la respuesta", e);
		}
		
		this.setRequest(pr);

		ResponseUnmarshaller ru;
		try {
			ru = unmarshallerClass.newInstance();
		} catch (InstantiationException e) {
			throw new ComponentExecutionException("Al instanciar el unmarshaller", e);
		} catch (IllegalAccessException e) {
			throw new ComponentExecutionException("Al instanciar el unmarshaller", e);
		}
		this.setResponseUnmarshaller(ru);

		this.setActionCode(actionCode);
		this.regsFileName = regsFileName;
		this.continuationResponseClass = continuationResponseClass;
		loadRegs();
	}

	private void loadRegs() throws IOException, JAXBException {
		String source = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(regsFileName), Charset.forName("UTF-8"));
		ContinuationResponse responseWithRegs = getResponseUnmarshaller().unmarshalResponse(source);
		this.regs.addAll(responseWithRegs.getRecords());
		logger.log(Level.FINE,"Cargados: " + responseWithRegs.getRecords().size() + " elements");

	}

	protected String executeRequest(CommRequest nextRequest)
			throws OSBConnectorException, MalformedURLException, JAXBException {
		
		//FIXME Testear que el request sea el esperado?
		logger.log(Level.FINE,"nextRequest es: " + nextRequest);
		executeRequestCount++;
		logger.log(Level.FINE,"executeRequestCount es: " + executeRequestCount);

		
		PaginateRequest pr = (PaginateRequest)nextRequest;
		
		ContinuationResponse r;
		try {
			r = continuationResponseClass.newInstance();
		} catch (InstantiationException e) {
			throw new ComponentExecutionException("Al instanciar la respuesta", e);
		} catch (IllegalAccessException e) {
			throw new ComponentExecutionException("Al instanciar la respuesta", e);
		}
		r.setEstado(Estado.newWithResultadoTrue());
		r.setContinuationstatus(Constants.PAGINATOR_TR_TAG);
		//Insis es 1 based
		int j = 0;
		for (int i = pr.getStartRow() - 1 ; i < pr.getStartRow() - 1 + pr.getRowCount() && i < regs.size(); i++, j++) {
			r.addRecord(regs.get(i));
		}
		logger.log(Level.FINE,"Agregados: " + j + " elements");
		
		String response = r.marshal();
		return response;
	}

	@Override
	public PipePointer currentPointer() {
		MockPipePointer p = new MockPipePointer();
		p.setExecuteRequestCount(executeRequestCount);
		logger.log(Level.FINE,"executeRequestCount es: " + executeRequestCount);
		p.setPipePointer(super.currentPointer());
		return p;
	}

	@Override
	public void seek(PipePointer pp) throws PipeException {
		MockPipePointer p = (MockPipePointer)pp;
		super.seek(p.getPipePointer());
		executeRequestCount = p.getExecuteRequestCount();
		logger.log(Level.FINE,"executeRequestCount es: " + executeRequestCount);
	}

	@Override
	public OSBConnector getConnector() {
		return null;
	}

	public int getExecuteRequestCounts() {
		return executeRequestCount;
	}

	public void setExecuteRequestCounts(int executeRequestCounts) {
		this.executeRequestCount = executeRequestCounts;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public int getSourceRequests() {
		return executeRequestCount;
	}

	
}
