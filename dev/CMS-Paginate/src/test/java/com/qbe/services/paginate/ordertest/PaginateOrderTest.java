package com.qbe.services.paginate.ordertest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.StreamUtils;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.paginate.CMSAnswer;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.InsisAnswer;
import com.qbe.services.paginate.OSBConnectorLocator;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.services.paginate.mqgestion.GetDeudaCobradaCanalesDetalle;
import com.qbe.services.paginate.service1428.Canal1428;
import com.qbe.services.paginate.service1428.Request1428;
import com.qbe.services.paginate.service1428.Response1428;
import com.qbe.services.paginate.service1428.Response1428Unmarshaller;
import com.qbe.vbcompat.framework.jaxb.Estado;
import com.qbe.vbcompat.string.StringHolder;

/**
 * Testea que devuelva los registros ordenados en las páginas que corresponde.
 * Usar la estructura del 1428
 * 
 * @author ramiro
 *
 */
public class PaginateOrderTest {

	private static Logger logger = Logger.getLogger(PaginateOrderTest.class.getName());

	private OSBConnectorLocator locator;
	
	@Before
     public void setUp() throws OSBConnectorException, IOException {
		
		 locator = mock(OSBConnectorLocator.class);
		 CMSAnswer cmsAnswer = new CMSAnswer();
		 cmsAnswer.addReqRespFiles("ordertest/requestCMS_pagina_1.xml", "ordertest/responseCMS_pagina_1.xml");
		 cmsAnswer.addReqRespFiles("ordertest/requestCMS_pagina_2.xml", "ordertest/responseCMS_pagina_2.xml");
		 cmsAnswer.addReqRespFiles("ordertest/requestCMS_pagina_3.xml", "ordertest/responseCMS_pagina_3.xml");
		 
		 OSBConnector mockedCMS = mock(OSBConnector.class);
		 when(mockedCMS.executeRequest(anyString(), anyString())).thenAnswer(cmsAnswer);
         when(locator.getCMSConnector()).thenReturn(mockedCMS);

         InsisAnswer insis1428 = new InsisAnswer();
         insis1428.setContinuationResponseClass(Response1428.class);
         insis1428.setRequestUnmarshaller(new Request1428());
         insis1428.setResponseUnmarshaller(new Response1428Unmarshaller());
         insis1428.addFileName("ordertest/responseInsis_pagina_1.xml");
         
		 OSBConnector mockedInsis1428 = mock(OSBConnector.class);
		 when(mockedInsis1428.executeRequest(anyString(), anyString())).thenAnswer(insis1428);
         
		 when(locator.getInsis1428Connector()).thenReturn(mockedInsis1428);
         
         OSBConnectorLocator.instance = locator;
	}

	@After
    public void tearDown() throws OSBConnectorException, IOException {
		OSBConnectorLocator.instance = null;
	}
	

	
	/**
	 * Recorre un resultset que tiene resultados de Insis y CMS, y los resultados de Insis no 
	 * entran en la primer página xq el resultset está ordenado por CLIENDES ( en el request el COLUMANORDER = 1 ) 
	 * y el resultset de Insis arranca en 2000
	 * 
	 * @throws Exception
	 */
	@Test
	public void testOrder() throws Exception {
		
		GetDeudaCobradaCanalesDetalle.MESSAGE_1428_PAGE_SIZE = 5;
		String[][] cliendesOrder =  {
				{"1000", "1010", "1020", "1030", "1040"},
				{"1050", "1060", "1070", "2001", "2011"},
				{"2021", "2031", "2041", "2050", "2051"},
				{"2060", "2061", "2070", "2071", "2080"},
				{"2090", "2100", "2110", "2120", "3000"},
				{"3010", "3020", "3030", "3040"}
		};
		
		int pageSize = GetDeudaCobradaCanalesDetalle.MESSAGE_1428_PAGE_SIZE;	
		double numRegs = 8 /* insis */ + 8 * 2 /* ais */ + 5;
		
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		
		String request = this.getOrderTestRequest();
		
		PaginatedActionCode action = new GetDeudaCobradaCanalesDetalle();
		
		StringHolder sh = new StringHolder();
		int pageNum = 0;
		logger.log(Level.INFO, "Start pageNum:" + pageNum);
		action.IAction_Execute(request, sh, null);
		
		Response1428 resp1428 = (Response1428) (new Response1428()).unmarshal(sh.getValue());
		validateResponse1428(resp1428, cliendesOrder[pageNum]);
		ContinuationStatus paginateFlag = resp1428.getContinuationStatus();
		int numPages = 1;
		assertFalse("No vino el tag de continuación!", paginateFlag.equals(ContinuationStatus.EMPTY));
		
		while ( paginateFlag.equals(ContinuationStatus.TR )) {
				numPages++;
				pageNum++;
				logger.log(Level.INFO, "Start pageNum:" + pageNum);
				String nextReq = resp1428.getCampos().getRequest().marshall();
				
				action = new GetDeudaCobradaCanalesDetalle();

				action.IAction_Execute(nextReq, sh, null);

				resp1428 = (Response1428) (new Response1428()).unmarshal(sh.getValue());
				validateResponse1428(resp1428, cliendesOrder[pageNum]);
				paginateFlag = resp1428.getContinuationStatus();
		}
		
		assertEquals("La última página no tiene la cantidad de registros esperada", (int)numRegs - ( pageSize * (numPages - 1 ) ), (int)resp1428.getRecords().size());
		assertEquals("No recorrió las páginas que debería", (int)Math.ceil(numRegs / pageSize), numPages);
		assertTrue("No cerró la continuación con un OK", paginateFlag.equals(ContinuationStatus.OK));
		
		// CMS devuelve 8 regs, el paginate devuelve 5, Insis devuelve 8
		// 1ra pagina: CMS++ INSIS++. Entran solamente de CMS, sobran 3
		// 2da pagina CMS ++, INSIS++, CMS++ 3 de CMS, 1 de insis,  de cms
		// 3ra pagina CMS++, INSIS++ ( refresh )
		// 4ta pagina CMS++, INSIS++ ( refresh ) se terminaron los de insis queda exhausted
		// 5ta pagina CMS++, CMS++
		// 6ta pagina CMS++
		verify(locator, times(8)).getCMSConnector();
		verify(locator, times(4)).getInsis1428Connector();
	}

	private String getOrderTestRequest() throws IOException {
		String source = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("ordertest/requestOrderTest.xml"), Charset.forName("UTF-8"));
		return source;
	}

	private void validateResponse1428(Response1428 resp1428, String[] cliendesRegs) {
		assertFalse("Vino false", resp1428.getEstado().getResultado().equalsIgnoreCase(Estado.FALSE));
		assertTrue("El response vino sin Request de continuación", resp1428.getRequest() != null);
		assertEquals(cliendesRegs.length, resp1428.getRecords().size());
		for (int j = 0; j < cliendesRegs.length; j++) {
			ResponseRecord rr = resp1428.getRecords().get(j);
			Canal1428 c = (Canal1428)rr;
			logger.log(Level.INFO, c.getCliendes());
			assertEquals("Vino un CLIENDES inesperado", c.getCliendes(), cliendesRegs[j]);
		}
	}
	
}
