package com.qbe.services.paginate.service1107;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.OSBConnectorLocator;

/**
 * FIXME Parametrizar. Poner el OSBConnector y pasárselo al instanciarlo, y saco esta clase y la 
 * análoga del 1107
 * 
 * @author ramiro
 *
 */
public class Insis1107Pipe extends InsisPipe {

	@Override
	public OSBConnector getConnector() {
		return OSBConnectorLocator.getInstance().getInsis1107Connector();
	}

	
}
