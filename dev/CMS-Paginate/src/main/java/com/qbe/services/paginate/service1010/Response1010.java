package com.qbe.services.paginate.service1010;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ResponseRecord;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 *
 * 
<Response>
	<Estado mensaje="" resultado="true"/>
	<MSGEST>TR</MSGEST>
	<QRYCONT/>
	<CLICONT/>
	<PRODUCONT>##PAGER##e778f279-0564-46b0-b51f-8da5d0c4df57#202#201##PAGER##</PRODUCONT>
	<POLIZACONT/>
	<SUPLENUMS/>
	<CLIENSECS/>
	<AGENTCLAS/>
	<AGENTCODS/>
	<NROITEMS/>
	<ESTPOL>TODAS</ESTPOL>
	<CLIENDES/>
	<REGS>
		<REG>
			<PROD>0007</PROD>
			<POL>2350</POL>
			<CERPOL/>
			<CERANN/>
			<CERSEC/>
			<CLIDES>PAPASIDERO , JULIA MARIA </CLIDES>
			<TOMA>CHEVROLET VECTRA R70000514</TOMA>
			<EST>VIGENTE</EST>
			<SINI>0</SINI>
			<ALERTEXI>0</ALERTEXI>
			<AGE>PR-3233</AGE>
			<RAMO>2</RAMO>
			<MARCAREIMPRESION>N</MARCAREIMPRESION>
			<MARCAENDOSABLE>S</MARCAENDOSABLE>
		</REG>
usw. 
 * 
 * @author ramiro
 *
 */


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "Response")
public class Response1010 extends ContinuationResponse1010 {

	
	@XmlElement(required = true, name = "ESTPOL")
	private String estpol = "";
	@XmlElement(required = true, name = "CLIENDES")
	private String cliendes = "";
	
	@XmlElementWrapper(name = "REGS")
	@XmlElement(required = true, name = "REG")
	private List<Reg1010> records = new ArrayList<Reg1010>();

	public Response1010() {
	}

	
	@Override
	protected JAXBContext createMarshalContext() throws JAXBException {
		return JAXBContext.newInstance(this.getClass(), Reg1010.class);
	}

	@Override
	public void setSerializedData(String serialize) {
		this.setProducont(serialize);
	}

	@Override
	public String getSerializedData() {
		return getProducont();
	}

	@Override
	public List<? extends ResponseRecord> getRecords() {
		return records;
	}

	public void setRecords(List<Reg1010> records) {
		this.records = records;
	}

	@Override
	public void addRecord(ResponseRecord r) {
		this.records.add((Reg1010)r);
	}

	public String getEstpol() {
		return estpol;
	}

	public void setEstpol(String estpol) {
		this.estpol = estpol;
	}

	public String getCliendes() {
		return cliendes;
	}

	public void setCliendes(String cliendes) {
		this.cliendes = cliendes;
	}

	
}
