package com.qbe.services.paginate.service1406;

import javax.xml.bind.JAXBException;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.ResponseUnmarshaller;

public class Response1406Unmarshaller extends ResponseUnmarshaller {

	@Override
	public ContinuationResponse unmarshalResponse(String response) throws JAXBException {
		ContinuationResponse cr = (new Response1406()).unmarshal(response);
		return cr;
	}

}
