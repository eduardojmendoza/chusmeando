package com.qbe.services.paginate.service1340;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ContinuationStatus;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class ResponseCampos1340 {

	@XmlElement(required=true, name="CANT")
	private int cant;
	
	@XmlElement(required = true, name = "ITEMS")
	 private Items1340 items;

	/**
	 * Otro caso especial del 1340. Si no hay más resultados no devuelve el Request, y por lo tanto no hay flag de continuación
	 */
	@XmlElement(required=true, name="Request")
	private Request1340 request;


	public ResponseCampos1340() {
		this.items = new Items1340();
	}

	public List<Item1340> getRecords() {
		return this.items.getRecords();
	}

	public void setRecords(List<Item1340> records) {
		this.items.setRecords(records);
	}

	public Request1340 getRequest() {
		return request;
	}

	public void setRequest(Request1340 request) {
		this.request = request;
	}

	/**
	 * Setea el nroqry en el Request.
	 * Crea un Request por default si está en null
	 * 
	 * @param serialize
	 */
	public void setNroqry(String serialize) {
		getRequestLazyly().setNroqry(serialize);		
	}

	private Request1340 getRequestLazyly() {
		if ( this.request == null ) {
			this.request = new Request1340();
		}
		return this.request;
	}

	public String getNroqry() {
		if ( this.request == null ) {
			return null;
		} else {
			return getRequest().getNroqry();
		}
		
	}

	/**
	 * Setea el Estado en el Request.
	 * Crea un Request por default si está en null
	 * 
	 * @param msgest
	 */
	public void setEstado(ContinuationStatus msgest) {
		getRequestLazyly().setEstado(msgest);
	}

	public ContinuationStatus getEstado() {
		if ( this.request == null ) {
			return null;
		} else {
			return getRequest().getEstado();
		}
	}

	public Items1340 getItems() {
		return this.items;
	}

	public void setItems(Items1340 items) {
		this.items = items;
	}

	public int getCant() {
		return cant;
	}

	public void setCant(int cant) {
		this.cant = cant;
	}

	

}
