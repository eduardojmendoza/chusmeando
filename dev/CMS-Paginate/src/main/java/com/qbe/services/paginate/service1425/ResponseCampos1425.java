package com.qbe.services.paginate.service1425;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ResponseRecord;


/**
 * Estructura de CAMPOS del Response del 1425
 * 
 * 
<CAMPOS>
			<COTIDOLAR>5.0761</COTIDOLAR>
			<CANTLIN>1</CANTLIN>
			<REGS>
				<REG>
					<RAMO>1</RAMO>
					<CLIDES>MARIELA RUIS</CLIDES>
					<CLIENSEC>600024001</CLIENSEC>
					<PROD>AUS1</PROD>
					<POL>00000001</POL>
					<CERPOL>0000</CERPOL>
					<CERANN>0001</CERANN>
					<CERSEC>558738</CERSEC>
					<OPERAPOL>3</OPERAPOL>
					<RECNUM>021500824</RECNUM>
					<ESTADO>S</ESTADO>
					<MON>$</MON>
					<SIG/>
					<IMP>1805.79</IMP>
					<IMPCALC>1805.79</IMPCALC>
					<RENDIDO>0</RENDIDO>
					<FECVTO>13/12/2013</FECVTO>
					<AGE>PR-3233</AGE>
				</REG>
				
				<REG>
					<RAMO/>
					<CLIDES/>
					<CLIENSEC/>
					<PROD/>
					<POL/>
					<CERPOL/>
					<CERANN/>
					<CERSEC/>
					<OPERAPOL/>
					<RECNUM>021500381</RECNUM>
					<ESTADO>N</ESTADO>
					<MON/>
					<SIG/>
					<IMP/>
					<IMPCALC/>
					<RENDIDO/>
					<FECVTO/>
					<AGE/>
				</REG>
			</REGS>
</CAMPOS>
 * 
 *
 * @author martin
 *
 */


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="CAMPOS")
public class ResponseCampos1425 {



	
	@XmlElement(required = true, name = "COTIDOLAR")
	private String cotidolar = "";
	
	
	@XmlElement(required = true, name = "CANTLIN")
	private String cantlin = "";

	
	@XmlElementWrapper(name = "REGS")
	@XmlElement(required = true, name = "REG")
	private List<Reg1425> records = new ArrayList<Reg1425>();


	public String getCotidolar() {
		return cotidolar;
	}


	public void setCotidolar(String cotidolar) {
		this.cotidolar = cotidolar;
	}


	public String getCantlin() {
		return cantlin;
	}


	public void setCantlin(String cantlin) {
		this.cantlin = cantlin;
	}


	public List<Reg1425> getRecords() {
		return records;
	}


	public void setRecords(List<Reg1425> records) {
		this.records = records;
	}

	
	public void addRecord(ResponseRecord r) {
		this.records.add((Reg1425)r);
	}
	
	
}
