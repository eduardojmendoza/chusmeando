package com.qbe.services.paginate.service1109;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import com.qbe.services.paginate.ActionCode1107x9;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.Pipe;

public class OVOpeEmiDetallePaginada extends ActionCode1107x9  {

	/**
	 * Es el page size especificado del 1109. 
	 */
	public static final int MESSAGE_1109_PAGE_SIZE = 180;
	
	public static final String ACTION_CODE_1109 = "lbaw_OVOpeEmiDetalles";
	
	private static Logger logger = Logger.getLogger(OVOpeEmiDetallePaginada.class.getName());

	public OVOpeEmiDetallePaginada() {
		setPageSize(MESSAGE_1109_PAGE_SIZE);		
	}

	protected ContinuationResponse createResponse() {
		return new Response1109();
	}

	protected Comparator<Object> createComparator(PaginateRequest request) {
		return new Comparator1109();
	}

	public List<Pipe> createPipes(PaginateRequest request) {
		
		List<Pipe> pipes = new ArrayList<Pipe>();
		pipes.add(createCMSPipe(request, ACTION_CODE_1109, new Response1109Unmarshaller()));
		pipes.addAll(createInsisPipes(request, ACTION_CODE_1109, new Response1109Unmarshaller(), MESSAGE_1109_PAGE_SIZE));
		return pipes;
	}
	
	public InsisPipe createInsisPipe() {
		InsisPipe insis = new Insis1109Pipe();
		return insis;
	}
	
}
