package com.qbe.services.paginate.service1416;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.ResponseRecord;

/**
 * Response específico del 1416
 * 
 * 
<Response>			
	<Estado resultado="true" mensaje=""/>		
	<MSGEST>OK</MSGEST>		
	<COTIDOLAR>5.0920000 </COTIDOLAR>		
	<REGS>		
		<REG>	
			<RAMO>2</RAMO>
			<CLIDES><![CDATA[AARON ARIEL RUIZ HERMIDA]]></CLIDES>
			<CLIENSEC>4907540</CLIENSEC>
			<PROD>ICO1</PROD>
			<POL>00174870</POL>
			<CERPOL>0000</CERPOL>
			<CERANN>0000</CERANN>
			<CERSEC>000000</CERSEC>
			<OPERAPOL>0</OPERAPOL>
			<RECNUM>651327485</RECNUM>
			<MON><![CDATA[$]]></MON>
			<SIG/>
			<IMP>263,00</IMP>
			<IMPCALC>263.00</IMPCALC>
			<RENDIDO>0</RENDIDO>
			<FECVTO><![CDATA[25/03/2013]]></FECVTO>
			<AGE><![CDATA[PR-7836]]></AGE>
			<ESTADO>X</ESTADO>
		</REG>	
[...]
 * 
 *
 * @author gavilan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "Response")
public class Response1416 extends ContinuationResponse {

	
	@XmlElement(required = true, name = "MSGEST")
	private String msgest = "";
	@XmlElement(required = true, name = "COTIDOLAR")
	private String cotidolar = "";

	@XmlElementWrapper(name = "REGS")
	@XmlElement(required = true, name = "REG")
	private List<Reg1416> records = new ArrayList<Reg1416>();


	public Response1416() {
		super();
	}

	@Override
	public void setSerializedData(String serialize) {
	}

	@Override
	public String getSerializedData() {
		return "";
	}

	@Override
	public List<? extends ResponseRecord> getRecords() {
		return records;
	}

	public void setRecords(List<Reg1416> records) {
		this.records = records;
	}

	@Override
	public void addRecord(ResponseRecord r) {
		this.records.add((Reg1416)r);
	}

	
	@Override
	public void beforeMarshal() {
		this.recalculateMsgest();
	}
	
	/**
	 * Recálculo específico del 1416
	 * 
	 * De la spec:
	 * 	If the query obtains <= 100 coupons , then it returns "LIST" is TAG <MSGEST >and the list of copupons
	 *  If the query obtains > 100 coupons , then it returns "ING" is TAG <MSGEST> and doesn´t return coupons
	 *  If there are not coupons ,then it returns "VAC" is TAG <MSGEST> and doesn´t return coupons
	 *  	
	 * 
	 * 
	 * @throws ParseException 
	 */
	public void recalculateMsgest() {
		int recCount = getRecords().size();
		if ( recCount == 0) {
			setMsgest("VAC");
		} else if ( recCount <= 100) {
			setMsgest("LIST");
		} else  /* if ( recCount > 100 ) */ {
			setMsgest("ING");
		}
	}

	public String getMsgest() {
		return msgest;
	}

	public void setMsgest(String msgest) {
		this.msgest = msgest;
	}

	public String getCotidolar() {
		return cotidolar;
	}

	public void setCotidolar(String cotidolar) {
		this.cotidolar = cotidolar;
	}

	

	/**
	 * Este mensaje no implementa paginado, así que retornamos siempre el valor de fin de paginado
	 */
	@Override
	public ContinuationStatus getContinuationStatus() {
		return ContinuationStatus.OK;
	}

	/**
	 * Este mensaje no implementa paginado, así que es una NO OP
	 */
	@Override
	public void setContinuationstatus(ContinuationStatus msgest) {
	}


}