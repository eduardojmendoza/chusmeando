package com.qbe.services.paginate;

import java.io.StringReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.cms.osbconnector.OSBConnectorException;

/**
 * Lista de requests que se obtienen de un request Multicomm a partir del servicio MulticommFan
 * Subclases concretas devuelven requests concretos
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="Response")
public class MulticommRequestsListResponse<T extends PaginateRequest> extends Response {

	private static Logger logger = Logger.getLogger(MulticommRequestsListResponse.class.getName());
	
	@XmlElementWrapper(name = "Requests")
	@XmlElement(required = true, name = "Request")
	private List<T> records = new ArrayList<T>();

	public MulticommRequestsListResponse() {
	}

	public List<T> getRequests() {
		return records;
	}
	
	public List<? extends PaginateRequest> getMulticommRequests(PaginateRequest request) throws MalformedURLException, OSBConnectorException, JAXBException {
		OSBConnector multicommFan = OSBConnectorLocator.getInstance().getMulticommFan();
		String response = multicommFan.executeRequest("", request.marshall());
		logger.log(Level.FINE,"Respuesta del multicommFan: " + response);
		MulticommRequestsListResponse<T> multiResponse = this.unmarshall(response);
		return multiResponse.getRequests();
	}
	
	public MulticommRequestsListResponse<T> unmarshall(String response) throws JAXBException {
		JAXBContext ovContext = JAXBContext.newInstance(this.getClass());
		Unmarshaller um = ovContext.createUnmarshaller();
		StringReader sr = new StringReader(response);
		Object o = um.unmarshal(sr);
		if (!(o instanceof MulticommRequestsListResponse)) {
			throw new IllegalArgumentException("No pude unmarshalear a un MulticommRequestsListResponse, el resultado es de otra clase");
		}
		MulticommRequestsListResponse<T> mrp = (MulticommRequestsListResponse<T>) o;
		return mrp;
	}
	
	
}
