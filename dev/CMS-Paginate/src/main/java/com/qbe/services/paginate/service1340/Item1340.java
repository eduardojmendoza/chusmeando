package com.qbe.services.paginate.service1340;

import java.io.Serializable;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Registro de respuesta del 1340 . 
 * 
   						<RAMOPCOD>AUA1</RAMOPCOD>
                        <POLIZANN>00</POLIZANN>
                        <POLIZSEC>696481</POLIZSEC>
                        <CERTIPOL>6481</CERTIPOL>
                        <CERTIANN>0008</CERTIANN>
                        <CERTISEC>000001</CERTISEC>
                        <SUPLENUM>0</SUPLENUM>
                        <CLIENDES>DONNADEL S.A.</CLIENDES>
                        <TOMARIES>ASTIVIA AS 3 NL</TOMARIES>
                        <PATENNUM>HKS453</PATENNUM>
                        <SITUCPOL>ANULADA</SITUCPOL>
                        <SWDENHAB>S</SWDENHAB>
                        <SWSINIES>N</SWSINIES>
                        <SWEXIGIB>0</SWEXIGIB>
                        <RAMO>2</RAMO>
                        <VIGDESDE>20080723</VIGDESDE>
                        <VIGHASTA>20090123</VIGHASTA>
                        <FULTSTRO>00000000</FULTSTRO>

 * 
 * @author gavilan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="ITEM")
public class Item1340 extends ResponseRecord implements Serializable{
	
	@XmlElement(required=true, name="RAMOPCOD")
	private String ramocod = "";

	@XmlElement(required=true, name="POLIZANN")
	private String polizann = "";

	@XmlElement(required=true, name="POLIZSEC")
	private String polizsec = "";

	@XmlElement(required=true, name="CERTIPOL")
	private String certipol = "";

	@XmlElement(required=true, name="CERTIANN")
	private String certiann = "";
   
	@XmlElement(required=true, name="CERTISEC")
	private String certisec = "";
	
	@XmlElement(required=true, name="SUPLENUM")
	private String suplenum = "";
   
	@XmlElement(required=true, name="CLIENDES")
	private String cliendes = "";
	
	@XmlElement(required=true, name="TOMARIES")
	private String tomaries = "";
 
	@XmlElement(required=true, name="PATENNUM")
	private String patennum = "";

	@XmlElement(required=true, name="SITUCPOL")
	private String situcpol = "";

	@XmlElement(required=true, name="SWDENHAB")
	private String swdenhab = "";
	
	@XmlElement(required=true, name="SWSINIES")
	private String swsinies = "";
	
	@XmlElement(required=true, name="SWEXIGIB")
	private String swexigib = "";

	@XmlElement(required=true, name="RAMO")
	private String ramo = "";
	
	@XmlElement(required=true, name="VIGDESDE")
	private String vigdesde = "";
	
	@XmlElement(required=true, name="VIGHASTA")
	private String vighasta = "";
	
	@XmlElement(required=true, name="FULTSTRO")
	private String fultstro = "";
	
	/**
	 * Usado para logging en el comparator
	 * @return
	 */
	public String getComparePath() {
		// ver esto :Number of column on which thr result will be sort( 1- Policy Numer; 2: Customer Name; 3:TOMARIES; 4:PATENNUM; 5: Policy State)
		return "#" + getCliendes();
	}
	
	public String getPoliza() {
		return getPolizann() + getPolizsec() + getCertipol() + getCertiann() + getCertisec();
	}
	
	public String getRamocod() {
		return ramocod;
	}

	public void setRamocod(String ramocod) {
		this.ramocod = ramocod;
	}

	public String getPolizann() {
		return polizann;
	}

	public void setPolizann(String polizann) {
		this.polizann = polizann;
	}

	public String getPolizsec() {
		return polizsec;
	}

	public void setPolizsec(String polizsec) {
		this.polizsec = polizsec;
	}

	public String getCertipol() {
		return certipol;
	}

	public void setCertipol(String certipol) {
		this.certipol = certipol;
	}

	public String getCertiann() {
		return certiann;
	}

	public void setCertiann(String certiann) {
		this.certiann = certiann;
	}

	public String getCertisec() {
		return certisec;
	}

	public void setCertisec(String certisec) {
		this.certisec = certisec;
	}

	public String getSuplenum() {
		return suplenum;
	}

	public void setSuplenum(String suplenum) {
		this.suplenum = suplenum;
	}

	public String getCliendes() {
		return cliendes;
	}

	public void setCliendes(String cliendes) {
		this.cliendes = cliendes;
	}

	public String getTomaries() {
		return tomaries;
	}

	public void setTomaries(String tomaries) {
		this.tomaries = tomaries;
	}

	public String getPatennum() {
		return patennum;
	}

	public void setPatennum(String patennum) {
		this.patennum = patennum;
	}

	public String getSitucpol() {
		return situcpol;
	}

	public void setSitucpol(String situcpol) {
		this.situcpol = situcpol;
	}

	public String getSwdenhab() {
		return swdenhab;
	}

	public void setSwdenhab(String swdenhab) {
		this.swdenhab = swdenhab;
	}

	public String getSwsinies() {
		return swsinies;
	}

	public void setSwsinies(String swsinies) {
		this.swsinies = swsinies;
	}

	public String getSwexigib() {
		return swexigib;
	}

	public void setSwexigib(String swexigib) {
		this.swexigib = swexigib;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getVigdesde() {
		return vigdesde;
	}

	public void setVigdesde(String vigdesde) {
		this.vigdesde = vigdesde;
	}

	public String getVighasta() {
		return vighasta;
	}

	public void setVighasta(String vighasta) {
		this.vighasta = vighasta;
	}

	public String getFultstro() {
		return fultstro;
	}

	public void setFultstro(String fultstro) {
		this.fultstro = fultstro;
	}

}
