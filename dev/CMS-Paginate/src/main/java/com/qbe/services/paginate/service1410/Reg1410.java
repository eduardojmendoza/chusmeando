package com.qbe.services.paginate.service1410;

import java.io.Serializable;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Registro de respuesta del 1410
 * 
<RAMO>1</RAMO>
<PROD>AUS1</PROD>
<POL>00000001</POL>
<CERPOL>0000</CERPOL>
<CERANN>0001</CERANN>
<CERSEC>350837</CERSEC>
<FECANU>21/05/2013</FECANU>
<MON><![CDATA[$]]></MON>
<SIG><![CDATA[-]]></SIG>
<IMPANU>620,09</IMPANU>
<CLIDES>GALVAN OSCAR ALFREDO</CLIDES>
<COD>PR-3233</COD>
<SINI>N</SINI>
 * 
 * @author gavilan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="REG")
public class Reg1410 extends ResponseRecord implements Serializable{

	@XmlElement(required=true, name="RAMO")
	private String ramo = "";
	
	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	
	
	@XmlElement(required=true, name="PROD")
	private String prod = "";
	
	public String getProd() {
		return prod;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}
	
	@XmlElement(required=true, name="POL")
	private String pol = "";

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}
		
	@XmlElement(required=true, name="CERPOL")
	private String cerpol = "";
	
	public String getCerpol() {
		return cerpol;
	}

	public void setCerpol(String cerpol) {
		this.cerpol = cerpol;
	}
	
	@XmlElement(required=true, name="CERANN")
	private String cerann = "";

	public String getCerann() {
		return cerann;
	}

	public void setCerann(String cerann) {
		this.cerann = cerann;
	}	

	@XmlElement(required=true, name="CERSEC")
	private String cersec = "";

	public String getCersec() {
		return cersec;
	}

	public void setCersec(String cersec) {
		this.cersec = cersec;
	}
	@XmlElement(required=true, name="FECANU")
	private String fecanu = "";

	public String getFecanu() {
		return fecanu;
	}
	public void setFecanu(String fecanu) {
		this.fecanu = fecanu;
	}	
	
	@XmlElement(required=true, name="MON")
	private String mon = "";
	
	public String getMon() {
		return mon;
	}
	public void setMon(String mon) {
		this.mon = mon;
	}
	
	
	@XmlElement(required=true, name="SIG")
	private String sig = "";

	public String getSig() {
		return sig;
	}
	public void setSig(String sig) {
		this.sig = sig;
	}
	
	@XmlElement(required=true, name="IMPANU")
	private String impanu = "";
	
	public String getImpanu() {
		return impanu;
	}
	public void setImpanu(String impanu) {
		this.impanu = impanu;
	}
	
	@XmlElement(required=true, name="CLIDES")
	private String clides = "";
	public String getClides() {
		return clides;
	}
	public void setClides(String clides) {
		this.clides = clides;
	}
	
	@XmlElement(required=true, name="COD")
	private String cod = "";

	public String getCod() {
		return cod;
	}
	public void setCod (String cod) {
		this.cod = cod;
	}
		
	
	@XmlElement(required=true, name="SINI")
	private String sini = "";
	
	public String getSini() {
		return sini;
	}
	public void setSini (String sini) {
		this.sini = sini;
	}
		

	/**
	 * Usado para logging en el comparator
	 * @return
	 */
	public String getComparePath() {
		return "#" +getClides();
	}
	
}
