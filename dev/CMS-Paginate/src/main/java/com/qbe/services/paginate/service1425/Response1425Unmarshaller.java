package com.qbe.services.paginate.service1425;

import javax.xml.bind.JAXBException;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.ResponseUnmarshaller;

public class Response1425Unmarshaller extends ResponseUnmarshaller {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public ContinuationResponse unmarshalResponse(String response) throws JAXBException {
		ContinuationResponse cr = (new Response1425()).unmarshal(response);
		return cr;
	}

}
