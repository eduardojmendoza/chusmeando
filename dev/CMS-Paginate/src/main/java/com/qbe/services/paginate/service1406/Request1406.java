package com.qbe.services.paginate.service1406;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.UsuarioNivelasRequest;

/**
 * Request del mensaje 1406
 * 
 * 
 * 
<USUARIO>EX009005L</USUARIO>
<NIVELAS>OR</NIVELAS>
<CLIENSECAS>100029438</CLIENSECAS>
<NIVEL1/>
<CLIENSEC1/>
<NIVEL2/>
<CLIENSEC2/>
<NIVEL3>PR</NIVEL3>
<CLIENSEC3>100029438</CLIENSEC3>
<FECENVCONT/>
<PRODUCTO/>
<POLIZA/>
 * 
 * 
 * Campos específicos
 * <FECENVCONT/>
 * <PRODUCTO/>
 * <POLIZA/>

 * Utilizo el campo <POLIZA/> para el serializado del paginado.
 * 
 * @author gavilan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="Request")
public class Request1406 extends UsuarioNivelasRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(required=true, name="FECENVCONT")
	private String fecenvcont = "";


	@XmlElement(required=true, name="PRODUCTO")
	private String producto = "";

	@XmlElement(required=true, name="POLIZA")
	private String poliza = "";

	public Request1406() {
	}

	@Override
	public String getSerializedData() {
		return getPoliza();
	}

	@Override
	public void setSerializedData(String value) {
		this.setPoliza(value);
	}

	@Override
	public PaginateRequest createRequestForNextBlockFromResponse(ContinuationResponse resp) {
		Request1406 newRequest = (Request1406) SerializationUtils.clone(this);
		Response1406 cr = (Response1406)resp;
		if ( newRequest != null ) {
			newRequest.setPoliza(cr.getPoliza());
			newRequest.setFecenvcont(cr.getFecenvcont());
			newRequest.setProducto(cr.getProducto());
			
		}
		return newRequest;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getFecenvcont() {
		return fecenvcont;
	}

	public void setFecenvcont(String fecenvcont) {
		this.fecenvcont = fecenvcont;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	

	
}
