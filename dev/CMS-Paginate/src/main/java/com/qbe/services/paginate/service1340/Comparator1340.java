package com.qbe.services.paginate.service1340;

import java.io.Serializable;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.paginate.PaginateRequest;

/**
 * Compara dos Reg1404 teniendo en cuenta los criterios de orden definidos en la 
 * especificación del servicio
 * 
 * @author ramiro
 *
 */
public class Comparator1340 implements Comparator<Object>, Serializable {

	private static Logger logger = Logger.getLogger(Comparator1340.class.getName());

	protected Request1340 request;
	
	public Comparator1340(PaginateRequest request) {
		this.request = (Request1340)request;
	}

	/**
	 * 
	 * 
	 * @param o1
	 * @param o2
	 * @return
	 */
	@Override
    public int compare(Object o1, Object o2) {
    	if ( o1 == null && o2 == null ) {
            	throw new NullPointerException("Ambos elementos son null");              
            }else if(o2 == null) {
                return -1;
            } else if (o1 == null) {
                return 1;
            } else {
            	Item1340 r1 = (Item1340)o1;
            	Item1340 r2 = (Item1340)o2;
            	
            	int orden = -1;
            	
            	if ( request.getColumna() != null && request.getColumna() != "" ) {
            		orden = Integer.parseInt(request.getColumna());
            	}

            	logger.log(Level.FINEST, String.format("Comparando [ orden = %d]:\n%s\n%s", orden, r1.getComparePath(), r2.getComparePath()));
            		
        		switch (orden) {
				case 1:
					return Integer.signum(r1.getPoliza().compareTo(r2.getPoliza()));
				case 2:
					return Integer.signum(r1.getCliendes().compareTo(r2.getCliendes()));
				case 3:
					return Integer.signum(r1.getTomaries().compareTo(r2.getTomaries()));
				case 4:
					return Integer.signum(r1.getPatennum().compareTo(r2.getPatennum()));
				case 5:
					return Integer.signum(r1.getSitucpol().compareTo(r2.getSitucpol())); 
				default:
					return 0;
        		}
            }
        }

	}
