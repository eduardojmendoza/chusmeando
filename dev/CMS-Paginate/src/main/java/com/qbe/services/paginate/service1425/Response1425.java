package com.qbe.services.paginate.service1425;

import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.GenericMarshaller;
import com.qbe.services.paginate.MSGESTContinuationResponse;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.services.paginate.service1340.ResponseCampos1340;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Response específico del 1425
 * 
 * 
<Response>
		<Estado  	resultado="true" mensaje=""/>
		<MSGEST>OK</MSGEST>
		<CAMPOS>
			<COTIDOLAR>5.0761</COTIDOLAR>
			<CANTLIN>1</CANTLIN>
			<REGS>
				<REG>
					<RAMO>1</RAMO>
					<CLIDES>MARIELA RUIS</CLIDES>
					<CLIENSEC>600024001</CLIENSEC>
					<PROD>AUS1</PROD>
					<POL>00000001</POL>
					<CERPOL>0000</CERPOL>
					<CERANN>0001</CERANN>
					<CERSEC>558738</CERSEC>
					<OPERAPOL>3</OPERAPOL>
					<RECNUM>021500824</RECNUM>
					<ESTADO>S</ESTADO>
					<MON>$</MON>
					<SIG/>
					<IMP>1805.79</IMP>
					<IMPCALC>1805.79</IMPCALC>
					<RENDIDO>0</RENDIDO>
					<FECVTO>13/12/2013</FECVTO>
					<AGE>PR-3233</AGE>
				</REG>
				
				<REG>
					<RAMO/>
					<CLIDES/>
					<CLIENSEC/>
					<PROD/>
					<POL/>
					<CERPOL/>
					<CERANN/>
					<CERSEC/>
					<OPERAPOL/>
					<RECNUM>021500381</RECNUM>
					<ESTADO>N</ESTADO>
					<MON/>
					<SIG/>
					<IMP/>
					<IMPCALC/>
					<RENDIDO/>
					<FECVTO/>
					<AGE/>
				</REG>
			</REGS>
		</CAMPOS>
</Response>
 * 
 *
 * @author martin
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "Response")
public class Response1425 extends ContinuationResponse {


	@XmlElement(required = true, name = "MSGEST")
	private String msgest = "";
	
	@XmlElement(name = "CAMPOS")
	ResponseCampos1425 campos = new ResponseCampos1425();
	
	
	
	

	public Response1425() {
		super();
	}

	@Override
	public void setSerializedData(String serialize) {
	}

	@Override
	public String getSerializedData() {
		return "";
	}

	
	public List<Reg1425> getRecords() {
		return campos.getRecords();
	}

	public void setRecords(List<Reg1425> campos) {
		this.campos.setRecords(campos);
	}

	
	public void addRecord(ResponseRecord r) {
		this.campos.addRecord((Reg1425)r);
	}

	
	@Override
	public void beforeMarshal() {
		this.recalculateMsgest();
	}
	
	/**
	 * Recálculo específico del 1425
	 * 
	 * De la spec:
	 * 	If the query obtains <= 100 coupons , then it returns "LIST" is TAG <MSGEST >and the list of copupons
	 *  If the query obtains > 100 coupons , then it returns "ING" is TAG <MSGEST> and doesn´t return coupons
	 *  If there are not coupons ,then it returns "VAC" is TAG <MSGEST> and doesn´t return coupons
	 *  	
	 * 
	 * 
	 * @throws ParseException 
	 */
	public void recalculateMsgest() {
		int recCount = getRecords().size();
		if ( recCount == 0) {
			setMsgest("VAC");
		} else if ( recCount <= 100) {
			setMsgest("LIST");
		} else  /* if ( recCount > 100 ) */ {
			setMsgest("ING");
		}
	}

	public String getMsgest() {
		return msgest;
	}

	public void setMsgest(String msgest) {
		this.msgest = msgest;
	}

	

	/**
	 * Este mensaje no implementa paginado, así que retornamos siempre el valor de fin de paginado
	 */
	@Override
	public ContinuationStatus getContinuationStatus() {
		return ContinuationStatus.OK;
	}

	/**
	 * Este mensaje no implementa paginado, así que es una NO OP
	 */
	@Override
	public void setContinuationstatus(ContinuationStatus msgest) {
	}


}