package com.qbe.services.paginate.service1416;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.services.paginate.UsuarioNivelasRequest;

/**
 * Request del mensaje 1416
 * 
 * 
<Request>				
		<USUARIO>EX009005L</USUARIO>		
		<NIVELAS>PR</NIVELAS>		
		
		<CLIENSECAS>100072343</CLIENSECAS>		
		<FLAGBUSQ>T</FLAGBUSQ>		
		<POLIZAS>		
			<POLIZA>	
				<PROD>ICO1</PROD>
				<POL>00174870</POL>
				<CERTI>00000000000000</CERTI>
			</POLIZA>	
		</POLIZAS>		
	</Request>
 * 
 *
 * @author gavilan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="Request")
public class Request1416 extends UsuarioNivelasRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(required=true, name="CLIENSECAS")
	private String cliensecas = "";

	@XmlElement(required=true, name="FLAGBUSQ")
	private String flagbusq = "";
	
	@XmlElementWrapper(name = "POLIZAS")
	@XmlElement(required = true, name = "POLIZA")
	private List<Pol1416> records = new ArrayList<Pol1416>();

	public Request1416() {
	}

	@Override
	public String getSerializedData() {
		return "";
	}

	@Override
	public void setSerializedData(String value) {
	}

	@Override
	public PaginateRequest createRequestForNextBlockFromResponse(ContinuationResponse resp) {
		return (Request1416) SerializationUtils.clone(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getCliensecas() {
		return cliensecas;
	}

	public void setCliensecas(String cliensecas) {
		this.cliensecas = cliensecas;
	}

	public String getFlagbusq() {
		return flagbusq;
	}

	public void setFlagbusq(String flagbusq) {
		this.flagbusq = flagbusq;
	}

}

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="POLIZA")
 class Pol1416 extends ResponseRecord implements Serializable{
   
	@XmlElement(required=true, name="PROD")
	private String prod = "";
	
	@XmlElement(required=true, name="POL")
	private String pol = "";
	
	@XmlElement(required=true, name="CERTI")
	private String certi = "";

	public String getProd() {
		return prod;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getCerti() {
		return certi;
	}

	public void setCerti(String certi) {
		this.certi = certi;
	}

	

}