package com.qbe.services.paginate.service1109;

import java.io.Serializable;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Compara dos Reg1109 teniendo en cuenta el policyNumber y el operapol
 * 
 * @author ramiro
 *
 */
public class Comparator1109 implements Comparator<Object>, Serializable {

	private static Logger logger = Logger.getLogger(Comparator1109.class.getName());

	/**
	 * 
	 * 
	 * @param o1
	 * @param o2
	 * @return
	 */
	@Override
    public int compare(Object o1, Object o2) {
    	if ( o1 == null && o2 == null ) {
            	throw new NullPointerException("Ambos elementos son null");              
            }else if(o2 == null) {
                return -1;
            } else if (o1 == null) {
                return 1;
            } else {
            	Reg1109 r1 = (Reg1109)o1;
            	Reg1109 r2 = (Reg1109)o2;
            	
            	logger.log(Level.FINEST, String.format("Comparando:\n%s\n%s", r1.getPolicyNumber(), r2.getPolicyNumber()));

            	int compare = Integer.signum(r1.getProd().compareTo(r2.getProd())); 
            	if ( compare != 0 ) return compare; 
            	
            	compare = Integer.signum(r1.getPol().compareTo(r2.getPol()));
            	if ( compare != 0 ) return compare;
            	
            	compare = Integer.signum(r1.getCerpol().compareTo(r2.getCerpol()));
            	if ( compare != 0 ) return compare;

            	compare = Integer.signum(r1.getCerann().compareTo(r2.getCerann()));
            	if ( compare != 0 ) return compare;

            	compare = Integer.signum(r1.getCersec().compareTo(r2.getCersec()));
            	if ( compare != 0 ) return compare;

            	compare = Integer.signum(r1.getOperapol().compareTo(r2.getOperapol()));
            	if ( compare != 0 ) return compare;
            	
                return 0;
            }
        }

	}
