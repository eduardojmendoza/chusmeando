package com.qbe.services.paginate.service1107;

import javax.xml.bind.JAXBException;

import com.qbe.services.paginate.ResponseUnmarshaller;
import com.qbe.services.paginate.ContinuationResponse;

/**
 * FIXME Refactorizar y parametrizar con Response1109Marshaller
 * @author ramiro
 *
 */
public class Response1107Unmarshaller extends ResponseUnmarshaller {

	@Override
	public ContinuationResponse unmarshalResponse(String response) throws JAXBException {
		ContinuationResponse response1107 = (new Response1107()).unmarshal(response);
		return response1107;
	}

}
