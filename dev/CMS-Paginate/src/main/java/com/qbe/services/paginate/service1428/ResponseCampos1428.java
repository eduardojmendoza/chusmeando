package com.qbe.services.paginate.service1428;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ContinuationStatus;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class ResponseCampos1428 {

	@XmlElement(required = true, name = "CANAL")
	private List<Canal1428> records = new ArrayList<Canal1428>();


	/**
	 * Otro caso especial del 1428. Si no hay más resultados no devuelve el Request, y por lo tanto no hay flag de continuación
	 */
	@XmlElement(required=true, name="Request")
	private Request1428 request;

	public ResponseCampos1428() {
	}

	public List<Canal1428> getRecords() {
		return records;
	}

	public void setRecords(List<Canal1428> records) {
		this.records = records;
	}

	public Request1428 getRequest() {
		return request;
	}

	public void setRequest(Request1428 request) {
		this.request = request;
	}

	/**
	 * Setea el nroqry_s en el Request.
	 * Crea un Request por default si está en null
	 * 
	 * @param serialize
	 */
	public void setNroqry_s(String serialize) {
		getRequestLazyly().setNroqry_s(serialize);		
	}

	private Request1428 getRequestLazyly() {
		if ( this.request == null ) {
			this.request = new Request1428();
		}
		return this.request;
	}

	public String getNroqry_s() {
		if ( this.request == null ) {
			return null;
		} else {
			return getRequest().getNroqry_s();
		}
		
	}

	/**
	 * Setea el Estado en el Request.
	 * Crea un Request por default si está en null
	 * 
	 * @param msgest
	 */
	public void setEstado(ContinuationStatus msgest) {
		getRequestLazyly().setEstado(msgest);
	}

	public ContinuationStatus getEstado() {
		if ( this.request == null ) {
			return null;
		} else {
			return getRequest().getEstado();
		}
	}

}
