package com.qbe.services.paginate.service1401;

import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.GenericMarshaller;
import com.qbe.services.paginate.MSGESTContinuationResponse;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Response específico del 1401
 * 
 * 
 *  <Response>
          <Estado resultado="true" mensaje=""/>
          <MSGEST>TR</MSGEST>
          <PRODUCTO/>
          <POLIZA/>
          <NROCONS/>
          <NROORDEN/>
          <DIRORDEN/>
          <TFILTRO/>
          <VFILTRO/>
          <PAGINADO/> Este campo proviene de la migracion del componente de Com+ y vale 1 cuando NROCONS es diferente 0
          <REGS>
            <REG>
 * 
 * TODO Necesita refactoring con ContinuationResponse1010
 *
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "Response")
public class Response1401 extends MSGESTContinuationResponse {

	
	private static DecimalFormat montoDecimalFormat = null;
	static {
		// Create a DecimalFormat that fits your requirements
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator('.');
		symbols.setDecimalSeparator(',');
		String pattern = "#,##0.00#";
		montoDecimalFormat = new DecimalFormat(pattern, symbols);
		montoDecimalFormat.setParseBigDecimal(true);	
	}

	
	@XmlElement(required = false, name = "PRODUCTO")
	private String producto = "";
	@XmlElement(required = false, name = "POLIZA")
	private String poliza = "";
	@XmlElement(required = false, name = "NROCONS")
	private String nrocons = "";
	@XmlElement(required = false, name = "NROORDEN")
	private String nroorden = "";
	@XmlElement(required = false, name = "DIRORDEN")
	private String dirorden = "";
	@XmlElement(required = false, name = "TFILTRO")
	private String tfiltro = "";
	@XmlElement(required = false, name = "VFILTRO")
	private String vfiltro = "";
	@XmlElement(required = true, name = "PAGINADO")
	private String paginado;


	public Response1401() {
		super();
	}

	@XmlElementWrapper(name = "REGS")
	@XmlElement(required = true, name = "REG")
	private List<Reg1401> records = new ArrayList<Reg1401>();

	@XmlElementWrapper(name = "TOTS")
	@XmlElement(required = true, name = "TOT")
	private List<Tot1401> tots = new ArrayList<Tot1401>();

	@Override
	public void setSerializedData(String serialize) {
		this.setNrocons(serialize);
	}

	@Override
	public String getSerializedData() {
		return getNrocons();
	}

	@Override
	public List<? extends ResponseRecord> getRecords() {
		return records;
	}

	public void setRecords(List<Reg1401> records) {
		this.records = records;
	}

	@Override
	public void addRecord(ResponseRecord r) {
		this.records.add((Reg1401)r);
	}

	@Override
	public void beforeMarshal() {
		this.recalculateTots();
	}
	
	/**
	 * Recálculo específico del 1404
	 * @throws ParseException 
	 */
	public void recalculateTots() {
		try {
			Tot1401 totPesos = new Tot1401();
			totPesos.setTmon("$");
			totPesos.setTimptos("");
			totPesos.setT30s("");
			totPesos.setT60s("");
			totPesos.setT90s("");
			totPesos.setTm90s("");
			
			Tot1401 totDolares = new Tot1401();
			totDolares.setTmon("U$S");
			totDolares.setTimptos("");
			totDolares.setT30s("");
			totDolares.setT60s("");
			totDolares.setT90s("");
			totDolares.setTm90s("");

			BigDecimal imptoPesos = new BigDecimal(0);
			BigDecimal t30Pesos = new BigDecimal(0);
			BigDecimal t60Pesos = new BigDecimal(0);
			BigDecimal t90Pesos = new BigDecimal(0);
			BigDecimal tm90Pesos = new BigDecimal(0);
			
			BigDecimal imptoDolares = new BigDecimal(0);
			BigDecimal t30Dolares = new BigDecimal(0);
			BigDecimal t60Dolares = new BigDecimal(0);
			BigDecimal t90Dolares = new BigDecimal(0);
			BigDecimal tm90Dolares = new BigDecimal(0);
			
			for (Reg1401 reg : records) {
				if ( "$".equals(reg.getMon()) ) {
					imptoPesos = imptoPesos.add((BigDecimal)montoDecimalFormat.parse(reg.getImpto()));
					t30Pesos = t30Pesos.add((BigDecimal)montoDecimalFormat.parse(reg.getI30()));
					t60Pesos = t60Pesos.add((BigDecimal)montoDecimalFormat.parse(reg.getI60()));
					t90Pesos = t90Pesos.add((BigDecimal)montoDecimalFormat.parse(reg.getI90()));
					tm90Pesos = tm90Pesos.add((BigDecimal)montoDecimalFormat.parse(reg.getIm90()));
				} else if ( "U$S".equals(reg.getMon())) {
					imptoDolares = imptoDolares.add((BigDecimal)montoDecimalFormat.parse(reg.getImpto()));
					t30Dolares = t30Dolares.add((BigDecimal)montoDecimalFormat.parse(reg.getI30()));
					t60Dolares = t60Dolares.add((BigDecimal)montoDecimalFormat.parse(reg.getI60()));
					t90Dolares = t90Dolares.add((BigDecimal)montoDecimalFormat.parse(reg.getI90()));
					tm90Dolares = tm90Dolares.add((BigDecimal)montoDecimalFormat.parse(reg.getIm90()));
				} else {
					throw new ComponentExecutionException("Signo de moneda no reconocido: " + reg.getMon());
				}
			}
			
			totPesos.setTimpto(montoDecimalFormat.format(imptoPesos));
			totPesos.setT30(montoDecimalFormat.format(t30Pesos));
			totPesos.setT60(montoDecimalFormat.format(t60Pesos));
			totPesos.setT90(montoDecimalFormat.format(t90Pesos));
			totPesos.setTm90(montoDecimalFormat.format(tm90Pesos));
			
			
			totDolares.setTimpto(montoDecimalFormat.format(imptoDolares));
			totDolares.setT30(montoDecimalFormat.format(t30Dolares));
			totDolares.setT60(montoDecimalFormat.format(t60Dolares));
			totDolares.setT90(montoDecimalFormat.format(t90Dolares));
			totDolares.setTm90(montoDecimalFormat.format(tm90Dolares));
			
			this.tots.clear();
			this.tots.add(totPesos);
			this.tots.add(totDolares);
		} catch (ParseException e) {
			throw new ComponentExecutionException("Exception al convertir un monto", e);
		}
	}


	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public String getNrocons() {
		return nrocons;
	}

	public void setNrocons(String nrocons) {
		this.nrocons = nrocons;
	}

	public String getNroorden() {
		return nroorden;
	}

	public void setNroorden(String nroorden) {
		this.nroorden = nroorden;
	}

	public String getDirorden() {
		return dirorden;
	}

	public void setDirorden(String dirorden) {
		this.dirorden = dirorden;
	}

	public String getTfiltro() {
		return tfiltro;
	}

	public void setTfiltro(String tfiltro) {
		this.tfiltro = tfiltro;
	}

	public String getVfiltro() {
		return vfiltro;
	}

	public void setVfiltro(String vfiltro) {
		this.vfiltro = vfiltro;
	}

	public String getPaginado() {
		return paginado;
	}

	public void setPaginado(String paginado) {
		this.paginado = paginado;
	}

	public List<? extends ResponseRecord> getTots() {
		return tots;
	}

	public void setTots(List<Tot1401> tots) {
		this.tots = tots;
	}

}