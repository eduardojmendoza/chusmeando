package com.qbe.services.paginate.service1425;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.UsuarioNivelasRequest;

/**
 * Request del mensaje 1425
 * 
 * 
<Request xmlns="">
        <DEFINICION>getRecibosNotasDeCredito.xml</DEFINICION>
        <USUARCOD>EX009005L</USUARCOD>
        <NIVELCLAS>GO</NIVELCLAS>
        <CLIENSECAS>100029438</CLIENSECAS>
        <CANTLINREC>2</CANTLINREC>
        <RECIBOS>
          <RECIBO>
            <RECNUM>021500381</RECNUM>
          </RECIBO>
          <RECIBO>
            <RECNUM>021500824</RECNUM>
          </RECIBO>
        </RECIBOS>
</Request>
 * 
 * 
 * Campos específicos
 * <CLIENSECAS/>
 * <RECNUM/>
 * 
 * FIXME Como el request no tiene paginado, agrego CONTFILLER para simplificar la lógica del resto
 * 
 * @author martin
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="Request")
public class Request1425 extends UsuarioNivelasRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(required=true, name="CLIENSECAS")
	private String cliensecas = "";
	
	@XmlElement(required=true, name="DEFINICION")
	private String definicion= "";
	
	
	@XmlElementWrapper(name = "RECIBOS")
	@XmlElement(required = true, name = "RECIBO")
	private List<Rec1425> records = new ArrayList<Rec1425>();

	

	public Request1425() {
	}

	@Override
	public String getSerializedData() {
		return "";
	}

	@Override
	public void setSerializedData(String value) {
	}

	@Override
	public PaginateRequest createRequestForNextBlockFromResponse(ContinuationResponse resp) {
		return (Request1425) SerializationUtils.clone(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getCliensecas() {
		return cliensecas;
	}

	public void setCliensecas(String cliensecas) {
		this.cliensecas = cliensecas;
	}

	public List<Rec1425> getRecords() {
		return records;
	}

	public void setRecords(List<Rec1425> records) {
		this.records = records;
	}

	public String getDefinicion() {
		return definicion;
	}

	public void setDefinicion(String definicion) {
		this.definicion = definicion;
	}
	
	

}
