package com.qbe.services.paginate.service1428;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.Constants;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.GenericMarshaller;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Response específico del 1428
 * 
 * El formato de XML que tenemos que representar es un delirio, tiene una estructura 
 * clásica con una colección de CANALes wrappeados en un tag CAMPOS, pero después del
 * último CANAL manda un Request.
 * 
 * No puedo usar la anotación de XmlElementWrapper así que tengo que separar el Campos
 *  en su clase ( ResponseCampos1428 )
 * 
 * <Response>		
	<Estado resultado="true" mensaje=""/>	
	<CAMPOS>	
		<CANAL>
		<Request>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "Response")
public class Response1428 extends ContinuationResponse {

	@XmlElement(name = "CAMPOS")
	ResponseCampos1428 campos = new ResponseCampos1428();
	
	public Response1428() {
		super();
	}

	/**
	 * Tomar el originatingRequest como base para el que devuelve dentro de CAMPOS
	 */
	@Override
	public void originatingRequest(PaginateRequest request) {
		Request1428 r1428 = (Request1428)request;
		getCampos().setRequest(r1428);
	}

	@Override
	public void setSerializedData(String serialize) {
		getCampos().setNroqry_s(serialize);
	}
	
	@Override
	public String getSerializedData() {
		return getCampos().getNroqry_s();
	}
	

	@Override
	public List<? extends ResponseRecord> getRecords() {
		return getCampos().getRecords();
	}

	public void setRecords(List<Canal1428> records) {
		getCampos().setRecords(records);
	}

	@Override
	public void addRecord(ResponseRecord r) {
		getCampos().getRecords().add((Canal1428)r);
	}

	@Override
	public ContinuationStatus getContinuationStatus() {
		return getCampos().getEstado() == null ? Constants.PAGINATOR_OK_TAG : getCampos().getEstado();
	}

	@Override
	public void setContinuationstatus(ContinuationStatus msgest) {
		getCampos().setEstado(msgest);
	}

	public Request1428 getRequest() {
		return getCampos().getRequest();
	}

	public String getNrocons() {
		return getCampos().getNroqry_s();
	}


	public ResponseCampos1428 getCampos() {
		return campos;
	}


	public void setCampos(ResponseCampos1428 campos) {
		this.campos = campos;
	}
}