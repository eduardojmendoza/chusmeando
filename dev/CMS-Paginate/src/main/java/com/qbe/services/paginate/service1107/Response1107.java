package com.qbe.services.paginate.service1107;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ContinuationResponse1107x09;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 *
 * 
		<Response>
<Estado mensaje="" resultado="true" />
    <MSGEST>OK</MSGEST>
    <FECCONT />
    <POLIZACONT />
    <OPECONT />
    <PRIMERAPAGINA />
    <REGS>
      <REG>
        <PROD>AUS1</PROD>
        <POL>00000001</POL>
        <CERPOL>9000</CERPOL>
        <CERANN>2016</CERANN>
        <CERSEC>647061</CERSEC>
        <CLIDES>ZUBIETA , HECTOR RAUL </CLIDES>
        <TOMARIES>FIAT  PALIO WE ADVENTURE 1.6 LOCKER</TOMARIES>
        <EST>RETENIDA</EST>
        <TIPOPE>NUEVO CERTIFICADO</TIPOPE>
        <FECING>01/01/2014</FECING>
        <COD>PR-3233</COD>
        <RAMO>1</RAMO>
        <LUPA>S</LUPA>
        <RENOVACION>N</RENOVACION>
        <MARCAOV />
        <SQLCERTIPOL />
        <SQLCERTIANN />
        <SQLCERTISEC />
      </REG> 
 * 
 * @author ramiro
 *
 */


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "Response")
public class Response1107 extends ContinuationResponse1107x09{

	@XmlElementWrapper(name = "REGS")
	@XmlElement(required = true, name = "REG")
	private List<Reg1107> records = new ArrayList<Reg1107>();

	@XmlElement(required=true, name="PRIMERAPAGINA")
	private String primeraPagina = "";

	public Response1107() {
	}

	@Override
	protected JAXBContext createMarshalContext() throws JAXBException {
		return JAXBContext.newInstance(this.getClass(), Reg1107.class);
	}

	@Override
	public void setSerializedData(String serialize) {
		this.setOpecont(serialize);
	}

	@Override
	public String getSerializedData() {
		return getOpecont();
	}

	@Override
	public List<? extends ResponseRecord> getRecords() {
		return records;
	}

	public void setRecords(List<Reg1107> records) {
		this.records = records;
	}

	@Override
	public void addRecord(ResponseRecord r) {
		this.records.add((Reg1107)r);
	}

	
}
