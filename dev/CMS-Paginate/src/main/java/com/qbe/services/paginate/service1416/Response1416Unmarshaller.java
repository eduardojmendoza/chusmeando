package com.qbe.services.paginate.service1416;

import javax.xml.bind.JAXBException;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.ResponseUnmarshaller;

public class Response1416Unmarshaller extends ResponseUnmarshaller {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public ContinuationResponse unmarshalResponse(String response) throws JAXBException {
		ContinuationResponse cr = (new Response1416()).unmarshal(response);
		return cr;
	}

}
