package com.qbe.services.paginate;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.Queue;

import com.qbe.vbcompat.base64.Base64Encoding;

/**
 * Pipe de lectura, recupera elementos de un source, de a bloques, a
 * medida que se va quedando vacío.
 * 
 * Presenta una interface de Queue ( peek(), pop(), hasNext(), reset() ) para 
 * recuperar los elementos de a uno
 * 
 * Expone un PipePointer que es un Memento que permite externalizar el estado de un pipe en un momento y volver a posicionarlo en el mismo lugar
 * 
 * @author ramiro
 *
 */
public abstract class Pipe implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Buffer de elementos
	 */
	private transient LinkedList<Object> elements = new LinkedList<Object>();

	public Pipe() {
	}
	
	/**
	 * Retorna en elemento en la cabeza de la queue, o null si no hay ninguno.
	 * No remueve el elemento
	 * 
	 * @return
	 * @throws PipeException
	 */
	public Object peek() throws PipeException {
		refetchIfNeeded();
		return getElements().peek();
	}
	
	/**
	 * Remueve y retorna en elemento en la cabeza de la queue, o null si no hay ninguno.
	 * 
	 * @return
	 * @throws PipeException
	 */
	public Object pop() throws PipeException {
		refetchIfNeeded();
		return getElements().poll();
	}
	
	/**
	 * Resetea el pipe posicionándolo antes del primer elemento.
	 * El próximo peek o get va a generar un fetch al source
	 */
	public void reset() {
		this.resetElements();
	}

	/**
	 * Retorna true si tiene al menos un elemento más.
	 * Implementación por default usa un peek(), puede forzar un refetch
	 *  
	 * 
	 * @return
	 * @throws PipeException
	 */
	public boolean hasNext() throws PipeException {
		return this.peek() != null;
	}

	/**
	 * Retorna un PipePointer que representa el estado del Pipe. Es un Memento.
	 * El PipePointer retornado se debe poder pasar en el seek(PipePointer) y el Pipe debe quedar posicionado en la misma posición.
	 * @return
	 */
	public abstract PipePointer currentPointer();

	/**
	 * Posiciona el receptor en la ubicación indicada por el PipePointer.
	 * 
	 * @param pp
	 * @throws PipeException
	 */
	public abstract void seek(PipePointer pp) throws PipeException;

	/**
	 * Carga el bloque siguiente desde el origen
	 * @throws PipeException 
	 * 
	 */
	protected abstract void fetchNextSourceBlock() throws PipeException;
	
	/**
	 * Indica si el origen tiene o no más bloques
	 * 
	 * @return
	 */
	protected abstract boolean isSourceExhausted();
	
	/**
	 * Deserializa a partir de un String Base64
	 * 
	 * @param serializado
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Pipe deserialize(String serializado) throws IOException, ClassNotFoundException {
		Pipe pipe;
		ByteArrayInputStream fis = new ByteArrayInputStream(Base64Encoding.decode(serializado));
		ObjectInputStream in = null;

		in = new ObjectInputStream(fis);
		pipe = (Pipe) in.readObject();
		in.close();
		
		return pipe;
	}

	/**
	 * Serializa a un Stream Base64
	 * 
	 * @return
	 * @throws IOException
	 */
	public String serialize() throws IOException {
		
		ObjectOutputStream out = null;
		ByteArrayOutputStream fos = new ByteArrayOutputStream(20000);
		out = new ObjectOutputStream(fos);
		out.writeObject(this);
		out.close();
		String serializado = Base64Encoding.encode(fos.toByteArray());
		return serializado;
	}

	private void writeObject(java.io.ObjectOutputStream stream)
		     throws IOException {
		stream.defaultWriteObject();
	}
	
	private void readObject(java.io.ObjectInputStream in)
		     throws IOException, ClassNotFoundException {
 		in.defaultReadObject();		
	}
	
	
	
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	/**
	 * Trae el bloque siguiente del source si se vació el buffer
	 * 
	 * @throws PipeException
	 */
	private void refetchIfNeeded() throws PipeException {
		if ( getElements().isEmpty() && !isSourceExhausted()) {
			this.fetchNextSourceBlock();
		}
	}

	private void resetElements() {
		this.elements = new LinkedList<Object>();
	}

	protected Queue<Object> getElements() {
		if ( elements == null ) {
			elements = new LinkedList<Object>();
		}
		return elements;
	}

	protected void setElements(LinkedList<Object> elements) {
		this.elements = elements;
	}


}
