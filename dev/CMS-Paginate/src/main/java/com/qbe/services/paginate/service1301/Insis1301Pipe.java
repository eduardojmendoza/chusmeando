package com.qbe.services.paginate.service1301;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.OSBConnectorLocator;

public class Insis1301Pipe extends InsisPipe {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public OSBConnector getConnector() {
		return OSBConnectorLocator.getInstance().getInsis1301Connector();
	}

	
}
