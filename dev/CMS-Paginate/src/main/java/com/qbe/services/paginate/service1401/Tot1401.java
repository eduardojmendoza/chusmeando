package com.qbe.services.paginate.service1401;

import java.io.Serializable;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Registro de totales de la respuesta del 1401
 * 
	<TOT>	
		<TMON>U$S</TMON>
		<T_IMPTOS/> <!-- esto que puede tener?? -->
		<T_IMPTO>23.150,23</T_IMPTO>
		<T_30S/>
		<T_30>18.902,23</T_30>
		<T_60S/>
		<T_60>1.492,00</T_60>
		<T_90S/>
		<T_90>1.378,00</T_90>
		<T_M90S/>
		<T_M90>1.378,00</T_M90>
	</TOT>	
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="TOT")
public class Tot1401 extends ResponseRecord implements Serializable{
	
	@XmlElement(required=true, name="TMON")
	private String tmon = "";

	@XmlElement(required=true, name="T_IMPTOS")
	private String timptos = "";

	@XmlElement(required=true, name="T_IMPTO")
	private String timpto = "";

	@XmlElement(required=true, name="T_30S")
	private String t30s = "";

	@XmlElement(required=true, name="T_30")
	private String t30 = "";

	@XmlElement(required=true, name="T_60S")
	private String t60s = "";

	@XmlElement(required=true, name="T_60")
	private String t60 = "";

	@XmlElement(required=true, name="T_90S")
	private String t90s = "";

	@XmlElement(required=true, name="T_90")
	private String t90 = "";

	@XmlElement(required=true, name="T_M90S")
	private String tm90s = "";

	@XmlElement(required=true, name="T_M90")
	private String tm90 = "";


	public String getTmon() {
		return tmon;
	}


	public void setTmon(String tmon) {
		this.tmon = tmon;
	}


	public String getTimptos() {
		return timptos;
	}


	public void setTimptos(String timptos) {
		this.timptos = timptos;
	}


	public String getTimpto() {
		return timpto;
	}


	public void setTimpto(String timpto) {
		this.timpto = timpto;
	}


	public String getT30s() {
		return t30s;
	}


	public void setT30s(String t30s) {
		this.t30s = t30s;
	}


	public String getT30() {
		return t30;
	}


	public void setT30(String t30) {
		this.t30 = t30;
	}


	public String getT60s() {
		return t60s;
	}


	public void setT60s(String t60s) {
		this.t60s = t60s;
	}


	public String getT60() {
		return t60;
	}


	public void setT60(String t60) {
		this.t60 = t60;
	}


	public String getT90s() {
		return t90s;
	}


	public void setT90s(String t90s) {
		this.t90s = t90s;
	}


	public String getT90() {
		return t90;
	}


	public void setT90(String t90) {
		this.t90 = t90;
	}


	public String getTm90s() {
		return tm90s;
	}


	public void setTm90s(String tm90s) {
		this.tm90s = tm90s;
	}


	public String getTm90() {
		return tm90;
	}


	public void setTm90(String tm90) {
		this.tm90 = tm90;
	}

	
}
