package com.qbe.services.paginate;

import java.io.Serializable;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.context.ApplicationContext;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.service1107.Response1107;

/**
 * Request de los mensajes 1107 y 1109
 * 
 * Ejemplo
 * 
 * <Request>
 * 	<USUARIO>EX009005L</USUARIO>
 * 	<NIVELAS>GO</NIVELAS>
 * 	<CLIENSECAS>100029438</CLIENSECAS>
 * 	<NIVEL1>GO</NIVEL1>
 * 	<CLIENSEC1>100029438</CLIENSEC1>
 * 	<NIVEL2></NIVEL2>
 * 	<CLIENSEC2></CLIENSEC2>
 * 	<NIVEL3></NIVEL3>
 * 	<CLIENSEC3></CLIENSEC3>
 * 	<FECDES>20120601</FECDES>
 * 	<FECHAS>20120601</FECHAS>
 * 	<FECCONT></FECCONT>
 * 	<PRODUCCONT></PRODUCCONT>
 * 	<POLIZACONT></POLIZACONT>
 * 	<OPECONT></OPECONT>
 * </Request>
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="Request")
public class Request1107x9 extends UsuarioNivelasRequest {

	@XmlElement(required=true, name="FECDES")
	private String fecDes = "";

	@XmlElement(required=true, name="FECHAS")
	private String fecHas = "";

	@XmlElement(required=true, name="FECCONT")
	private String fecCont = "";

	@XmlElement(required=true, name="PRODUCCONT")
	private String producCont = "";

	@XmlElement(required=true, name="POLIZACONT")
	private String polizaCont = "";

	@XmlElement(required=true, name="OPECONT")
	private String opeCont = "";

	public Request1107x9() {
	}

	public String getSerializedData() {
		return getOpeCont();
	}

	public void setSerializedData(String value) {
		this.setOpeCont(value);
	}

	public String getFecDes() {
		return fecDes;
	}


	public void setFecDes(String fecDes) {
		this.fecDes = fecDes;
	}


	public String getFecHas() {
		return fecHas;
	}


	public void setFecHas(String fecHas) {
		this.fecHas = fecHas;
	}


	public String getFecCont() {
		return fecCont;
	}


	public void setFecCont(String fecCont) {
		this.fecCont = fecCont;
	}


	public String getProducCont() {
		return producCont;
	}


	public void setProducCont(String producCont) {
		this.producCont = producCont;
	}


	public String getPolizaCont() {
		return polizaCont;
	}


	public void setPolizaCont(String polizaCont) {
		this.polizaCont = polizaCont;
	}


	public String getOpeCont() {
		return opeCont;
	}


	public void setOpeCont(String opeCont) {
		this.opeCont = opeCont;
	}

	@Override
	public PaginateRequest createRequestForNextBlockFromResponse(ContinuationResponse resp) {
		Request1107x9 newRequest = (Request1107x9)SerializationUtils.clone(this);
		ContinuationResponse1107x09 cr = (ContinuationResponse1107x09)resp;
		if ( newRequest != null ) {
			newRequest.setFecCont(cr.getFeccont());
			newRequest.setProducCont(cr.getProduccont());
			newRequest.setPolizaCont(cr.getPolizacont());
			newRequest.setOpeCont(cr.getOpecont());
		}
		return newRequest;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
