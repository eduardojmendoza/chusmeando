package com.qbe.services.paginate;

public class PipeUnexpectedErrorException extends PipeException {

	public PipeUnexpectedErrorException() {
	}

	public PipeUnexpectedErrorException(String message) {
		super(message);
	}

	public PipeUnexpectedErrorException(Throwable cause) {
		super(cause);
	}

	public PipeUnexpectedErrorException(String message, Throwable cause) {
		super(message, cause);
	}

}
