package com.qbe.services.paginate.service1340;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class Items1340 {
	
	public Items1340() {
		// TODO Auto-generated constructor stub
	}
		 
	@XmlElement(required = false, name = "ITEM")
	private List<Item1340> records = new ArrayList<Item1340>();

	public List<Item1340> getRecords() {
			return records;
	}

	public void setRecords(List<Item1340> records) {
			this.records = records;
	}
	
}
