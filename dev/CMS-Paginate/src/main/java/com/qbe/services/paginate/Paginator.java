package com.qbe.services.paginate;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.vbcompat.base64.Base64Encoding;

public abstract class Paginator implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(Paginator.class.getName());

	protected static ExecutorService executor = Executors.newFixedThreadPool(100);
	
	private Comparator<Object> comparator = new HashcodeComparator();
	
	protected List<Pipe> pipes = null;

	public static Paginator deserialize(String serializado) throws IOException, ClassNotFoundException {
		Paginator paginator;
		ByteArrayInputStream fis = new ByteArrayInputStream(Base64Encoding.decode(serializado));
		ObjectInputStream in = null;

		in = new ObjectInputStream(fis);
		paginator = (Paginator) in.readObject();
		in.close();
		return paginator;
	}

	public String serialize() throws IOException {
		ObjectOutputStream out = null;
		ByteArrayOutputStream fos = new ByteArrayOutputStream(20000);
		out = new ObjectOutputStream(fos);
		out.writeObject(this);
		out.close();
		String serializado = Base64Encoding.encode(fos.toByteArray());
		return serializado;
	}

	public Page fetchPage(int pageNumber, int pageSize) throws PipeException {

		logger.log(Level.FINE,"fetchPage: " + pageNumber + " pageSize = " + pageSize);

		preHook(pageNumber, pageSize);
		
		int firstReg = pageNumber * pageSize;
		
		Page page = new Page();
		page.setPageSize(pageSize);

		logger.log(Level.FINE, "Start of pageNumber: " + pageNumber);

		setUpPipes(pageNumber, pageSize);

		peekAll();
		
		skipUpTo(firstReg);
		
		buildPage(page);
		logger.log(Level.FINE, "End of pageNumber: " + pageNumber);

		postHook(pageNumber);
	
		
		return page;	
	}

	protected void preHook(int pageNumber, int pageSize) throws PipeException {
	}
	
	protected void postHook(int pageNumber) {
	}
	
	protected void skipUpTo(int firstReg) throws PipeException {
	}
	
	protected void setUpPipes(int pageNumber, int pageSize) throws PipeException {
	}

	protected void peekAll() {
		//Mando peek()s en paralelo, para que los pipes traigan sus registros en forma concurrente
		List<FutureTask<Object>> futures = new ArrayList<FutureTask<Object>>();
		for (Pipe pipe : pipes) {
			futures.add(new FutureTask<Object>(new PipePeeker(pipe)));
		}
		
		for (FutureTask<Object> futureTask : futures) {
			logger.log(Level.FINEST,"Lanzando future " + futureTask);
			executor.execute(futureTask);
		}
		//todos ejecutando en paralelo ahora...
		for (FutureTask<Object> futureTask : futures) {
			try {
				//Ignoro el resultado porque hice un peek() simplemente para que busque los datos
				futureTask.get();
				logger.log(Level.FINEST,"Después de get() del future " + futureTask);
			} catch (InterruptedException e) {
				// Ignorado, en el peor de los casos cuando recupere los elementos en sí va a tener que esperar
				logger.log(Level.WARNING,"InterruptedException haciendo get a un Future de pipe", e);
			} catch (ExecutionException e) {
				// Hubo una exception al hacer el get. Logueo y sigo, en todo caso pinchará más adelante si no puede recuperar los elementos
				logger.log(Level.WARNING,"ExecutionException en peekAll() haciendo get a un Future de pipe", e);
			}
		}
	}

	protected void buildPage(Page page) throws PipeException {
		while ( !page.isFull()) {
			Object nextElem = this.popFirst(pipes);
			if ( nextElem != null ) {
				page.add(nextElem);
				logger.log(Level.FINEST, "Adding to page: " + nextElem);
			} else {
				break;
			}
		}
	}

	/**
	 * Retorna el primer elemento de todos los pipes según el orden definido por el Comparator
	 * 
	 * @param pipes
	 * @return
	 * @throws PipeException
	 */
	protected Object popFirst(List<Pipe> pipes) throws PipeException {
		if ( pipes.size() == 0) {
			throw new IllegalArgumentException("Debe recibir al menos 1 pipe");
		}
		if ( pipes.size() == 1) {
			return pipes.get(0).pop();
		} else {
			Object minor = pipes.get(0).peek();
			Pipe minorPipe = pipes.get(0);
			for (Pipe pipe : pipes) {
				if ( pipe == minorPipe ) {
					continue;
				}
				Object current = pipe.peek();
				if ( current == null && minor == null) {
					continue;
				}
				if (minor(current,minor)) {
					minor = current;
					minorPipe = pipe;
				}
			}
			return minorPipe.pop();
		}
	}

	/**
	 * Retorna el menor de acuerdo al Comparator
	 * 
	 * @param current
	 * @param minor
	 * @return
	 */
	protected boolean minor(Object current, Object minor) {
		return getComparator().compare(current, minor) == -1;
	}

	/**
	 * Clásico hasNext, devuelve true si hay algún elemento más para devolver
	 */
	public boolean hasNext() throws PipeException {
		boolean hasNext = false;
		//FIXME Tirarlos en paralelo por si forzan un peek
		//FIXME no es shortcircuit!
		for (Pipe pipe : pipes) {
			hasNext = hasNext || pipe.hasNext();
		}
		return hasNext;
	}

	
	public Comparator<Object> getComparator() {
		return comparator;
	}

	public void setComparator(Comparator<Object> comparator) {
		this.comparator = comparator;
	}

	public List<Pipe> getPipes() {
		return pipes;
	}

	public void setPipes(List<Pipe> pipes) {
		this.pipes = pipes;
	}

}
