package com.qbe.services.paginate.service1425;

import java.io.Serializable;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Compara dos Reg1425 teniendo en cuenta los criterios de orden definidos en la 
 * especificación del servicio
 * 
 * Como no hay orden especificado, devuelvo ordenado por clides / cliensec
 * 
 * @author ramiro
 *
 */
public class Comparator1425 implements Comparator<Object>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(Comparator1425.class.getName());

	/**
	 * 
	 * 
	 * @param o1
	 * @param o2
	 * @return
	 */
	@Override
    public int compare(Object o1, Object o2) {
    	if ( o1 == null && o2 == null ) {
            	throw new NullPointerException("Ambos elementos son null");              
            }else if(o2 == null) {
                return -1;
            } else if (o1 == null) {
                return 1;
            } else {
            	Reg1425 r1 = (Reg1425)o1;
            	Reg1425 r2 = (Reg1425)o2;
            	
            	logger.log(Level.FINEST, String.format("Comparando:\n%s\n%s", r1.getClides() + r1.getCliensec(), r2.getClides() + r2.getCliensec()));

            	int compare = 0;
            	compare = Integer.signum(r1.getClides().compareTo(r2.getClides())); 
            	if ( compare != 0 ) return compare; 
            	
            	compare = Integer.signum(r1.getCliensec().compareTo(r2.getCliensec()));
            	if ( compare != 0 ) return compare;
            	
                return 0;
            }
        }

	}
