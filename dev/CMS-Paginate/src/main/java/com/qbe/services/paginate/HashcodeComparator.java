package com.qbe.services.paginate;

import java.io.Serializable;
import java.util.Comparator;

public class HashcodeComparator implements Comparator<Object>, Serializable {

	/**
	 * 
	 * Compara o1 y o2 de acuerdo a sus hashcode()s
	 * @param o1
	 * @param o2
	 * @return
	 */
    public int compare(Object o1, Object o2) {
    	if ( o1 == null && o2 == null ) {
            	throw new NullPointerException("Ambos elementos son null");              
            }else if(o2 == null) {
                return -1;
            } else if (o1 == null) {
                return 1;
            } else {   
            	Integer hc1 = o1.hashCode();
            	Integer hc2 = o2.hashCode();
                return hc1.compareTo(hc2);
            }
        }
	}
