package com.qbe.services.paginate.service1428;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.UsuarcodNivelasRequest;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Request del mensaje 1428
 * 
 * 
<NROCONS>1</NROCONS>
<DEFINICION>GetDeudaCobradaCanalesDetalle.xml</DEFINICION>
<USUARCOD>EX009005L</USUARCOD>
<NIVELAS>GO</NIVELAS>
<CLIENSECAS>100029438</CLIENSECAS>
<NIVELCLA1>GO</NIVELCLA1>
<CLIENSEC1>100029438</CLIENSEC1>
<NIVELCLA2/>
<CLIENSEC2>0</CLIENSEC2>
<NIVELCLA3/>
<CLIENSEC3>0</CLIENSEC3>
<FECHADESDE>11/08/2012</FECHADESDE>
<FECHAHASTA>11/09/2012</FECHAHASTA>
<COBROCOD-E>4</COBROCOD-E>
<REORDEN>N</REORDEN>
<COLUMNAORDEN>1</COLUMNAORDEN>
<ASCDESC>A</ASCDESC>
<CLAVEBACK>CARUSO  , ROBERTO JOSE</CLAVEBACK>
<CLAVEBACK2>0</CLAVEBACK2>
<RETORNARREQUEST/>
<CIAASCOD>0001</CIAASCOD>
<ESTADO>TR</ESTADO>
<ERROR/>
<NIVELCLAS/>
<CLASE-S/>
<NROQRY-S>36607</NROQRY-S>
<COBROCOD-S>0</COBROCOD-S>
<FECSITUE-S>0</FECSITUE-S>
<ORDENNUM-S>78039924</ORDENNUM-S>
 * 
 * 
 * Usamos NROQRY-S para guardar los datos serializados de continuación
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="Request")
public class Request1428 extends UsuarcodNivelasRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(required = false, name = "NROCONS")
	private List<String> nrocons = new ArrayList<String>();

	@XmlElement(required=true, name="DEFINICION")
	private String definicion;

    @XmlElement(required=false, name="AplicarXSL")
	private String aplicarXSL;

	@XmlElement(required=false, name="NIVELCLA1")
	private String nivelcla1;
	
	@XmlElement(required=false, name="NIVELCLA2")
	private String nivelcla2;
	
	@XmlElement(required=false, name="NIVELCLA3")
	private String nivelcla3;

	@XmlElement(required=false, name="FECHADESDE")
	private String fechaDesde;

	@XmlElement(required=false, name="FECHAHASTA")
	private String fechaHasta;
	
	@XmlElement(required=false, name="COBROCOD-E")
	private String cobroCodE;
	
	@XmlElement(required=false, name="REORDEN")
	private String reorden;

	@XmlElement(required=false, name="COLUMNAORDEN")
	private String colunmanorden;
	
	@XmlElement(required=false, name="ASCDESC")
	private String ascdesc;
	
	@XmlElement(required=false, name="CLAVEBACK")
	private String claveback;

	@XmlElement(required=false, name="CLAVEBACK2")
	private String claveback2;

	/**
	 * RETORNARREQUEST Se inicializa a "" para que siempre se genere en el request que va a CMS, porque si no no vuelve el request que se usa para la continuación aunque haya más registros.
	 * 
	 * Debería ir en la definición, pero no la modificamos por si hay llamadas de OV que no lo mandan adrede porque no lo esperan en la respuesta
	 */
	@XmlElement(required=false, name="RETORNARREQUEST")
	private String retornarrequest = "";

	//De acá para abajo, los campos que agrega para manejar la continuación
	@XmlElement(required=false, name="CIAASCOD")
	private String ciaascod;

	@XmlElement(required=false, name="ESTADO")
	private ContinuationStatus estado;

	@XmlElement(required=false, name="ERROR")
	private String error;

	@XmlElement(required=false, name="NIVELCLAS")
	private String nivelclas;

	@XmlElement(required=false, name="CLASE-S")
	private String clase_s;

	@XmlElement(required=false, name="NROQRY-S")
	private String nroqry_s;

	@XmlElement(required=false, name="COBROCOD-S")
	private String cobrocod_s;

	@XmlElement(required=false, name="FECSITUE-S")
	private String fecsitue_s;

	@XmlElement(required=false, name="ORDENNUM-S")
	private String ordenum_s;


	@XmlElement(required=false, name="PAGINASRETORNADAS")
	private String paginasRetornadas;
	
	//No figura en la definición. Se usará?
	@XmlElement(required=false, name="ESEXCEL")
	private String esExcel;


	public Request1428() {
	}

	/**
	 * Retorna una deep-copy del receptor
	 * @return
	 */
	public Request1428 copy()  {
		try {
			return (Request1428) unmarshall(this.marshall());
		} catch (JAXBException e) {
			throw new ComponentExecutionException("Exception al copiar un Request", e);
		}
	}
	
	
	/**
	 * El Request1428 es un caso especial de paginado, porque AIS devuelve en la respuesta
	 * el request completo que debemos enviar para obtener la página siguiente
	 */
	@Override
	public PaginateRequest createRequestForNextBlockFromResponse(ContinuationResponse resp) {
		Response1428 resp1428 = (Response1428) resp;
		return resp1428.getCampos().getRequest();
	}

	@Override
	public String getSerializedData() {
		return getNroqry_s();
	}

	@Override
	public void setSerializedData(String value) {
		this.setNroqry_s(value);
	}
	
	public String getDefinicion() {
		return definicion;
	}

	public void setDefinicion(String definicion) {
		this.definicion = definicion;
	}

	public List<String> getNrocons() {
		return nrocons;
	}

	public void setNrocons(List<String> nroCons) {
		this.nrocons = nroCons;
	}
	public String getNivelcla1() {
		return nivelcla1;
	}

	public void setNivelcla1(String nivelcla1) {
		this.nivelcla1 = nivelcla1;
	}

	public String getNivelcla2() {
		return nivelcla2;
	}

	public void setNivelcla2(String nivelcla2) {
		this.nivelcla2 = nivelcla2;
	}

	public String getNivelcla3() {
		return nivelcla3;
	}

	public void setNivelcla3(String nivelcla3) {
		this.nivelcla3 = nivelcla3;
	}

	public String getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public String getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public String getPaginasRetornadas() {
		return paginasRetornadas;
	}

	public void setPaginasRetornadas(String paginasRetornadas) {
		this.paginasRetornadas = paginasRetornadas;
	}

	public String getCobroCodE() {
		return cobroCodE;
	}

	public void setCobroCodE(String cobroCodE) {
		this.cobroCodE = cobroCodE;
	}
	
	public String getReorden() {
		return reorden;
	}

	public void setReorden(String reorden) {
		this.reorden = reorden;
	}

	public String getColumnaOrden() {
		return colunmanorden;
	}

	public void setColumnaOrden(String colunmanorden) {
		this.colunmanorden = colunmanorden;
	}

	public String getAscdesc() {
		return ascdesc;
	}

	public void setAscdesc(String ascdesc) {
		this.ascdesc = ascdesc;
	}


	public String getEsExcel() {
		return esExcel;
	}

	public void setEsExcel(String esExcel) {
		this.esExcel = esExcel;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getAplicarXSL() {
		return aplicarXSL;
	}

	public void setAplicarXSL(String aplicarXSL) {
		this.aplicarXSL = aplicarXSL;
	}

	public String getClaveback() {
		return claveback;
	}

	public void setClaveback(String claveback) {
		this.claveback = claveback;
	}

	public String getClaveback2() {
		return claveback2;
	}

	public void setClaveback2(String claveback2) {
		this.claveback2 = claveback2;
	}

	public String getRetornarrequest() {
		return retornarrequest;
	}

	public void setRetornarrequest(String retornarrequest) {
		this.retornarrequest = retornarrequest;
	}

	public String getCiaascod() {
		return ciaascod;
	}

	public void setCiaascod(String ciaascod) {
		this.ciaascod = ciaascod;
	}

	public ContinuationStatus getEstado() {
		return estado;
	}

	public void setEstado(ContinuationStatus estado) {
		this.estado = estado;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getNivelclas() {
		return nivelclas;
	}

	public void setNivelclas(String nivelclas) {
		this.nivelclas = nivelclas;
	}

	public String getClase_s() {
		return clase_s;
	}

	public void setClase_s(String clase_s) {
		this.clase_s = clase_s;
	}

	public String getNroqry_s() {
		return nroqry_s;
	}

	public void setNroqry_s(String nroqry_s) {
		this.nroqry_s = nroqry_s;
	}

	public String getCobrocod_s() {
		return cobrocod_s;
	}

	public void setCobrocod_s(String cobrocod_s) {
		this.cobrocod_s = cobrocod_s;
	}

	public String getFecsitue_s() {
		return fecsitue_s;
	}

	public void setFecsitue_s(String fecsitue_s) {
		this.fecsitue_s = fecsitue_s;
	}

	public String getOrdenum_s() {
		return ordenum_s;
	}

	public void setOrdenum_s(String ordenum_s) {
		this.ordenum_s = ordenum_s;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}
