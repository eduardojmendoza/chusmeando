package com.qbe.services.paginate.service1406;

import java.io.Serializable;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Compara dos Reg1406 teniendo en cuenta los criterios de orden definidos en la 
 * especificación del servicio
 * 
 * @author gavilan
 *
 */
public class Comparator1406 implements Comparator<Object>, Serializable {

	private static Logger logger = Logger.getLogger(Comparator1406.class.getName());

	/**
	 * 
	 * 
	 * @param o1
	 * @param o2
	 * @return
	 */
	@Override
    public int compare(Object o1, Object o2) {
    	if ( o1 == null && o2 == null ) {
            	throw new NullPointerException("Ambos elementos son null");              
            }else if(o2 == null) {
                return -1;
            } else if (o1 == null) {
                return 1;
            } else {
            	Reg1406 r1 = (Reg1406)o1;
            	Reg1406 r2 = (Reg1406)o2;
            	
            	logger.log(Level.FINEST, String.format("Comparando:\n%s\n%s", r1.getComparePath(), r2.getComparePath()));

            	int compare = 0;
            	compare = Integer.signum(r1.getProd().compareTo(r2.getProd())); 
            	if ( compare != 0 ) return compare; 
            	
            	compare = Integer.signum(r1.getPol().compareTo(r2.getPol()));
            	if ( compare != 0 ) return compare;
            	
            	compare = Integer.signum(r1.getCerpol().compareTo(r2.getCerpol()));
            	if ( compare != 0 ) return compare;

            	compare = Integer.signum(r1.getCerann().compareTo(r2.getCerann()));
            	if ( compare != 0 ) return compare;

            	compare = Integer.signum(r1.getCersec().compareTo(r2.getCersec()));
            	if ( compare != 0 ) return compare;

            	compare = Integer.signum(r1.getMon().compareTo(r2.getMon())); 
            	if ( compare != 0 ) return compare; 
            	
            

                return 0;
            }
        }

	}
