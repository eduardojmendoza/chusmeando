package com.qbe.services.paginate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;



import com.qbe.vbcompat.framework.ComponentExecutionException;


/**
 * Un SkippingPaginator saltea las páginas n-1 cuando le piden la página n
 * No puede acceder directamente a una página arbitraria, tiene que haber recorrido las páginas anteriores
 * FIXME Podríamos hacer que si le piden que saltee una página, haga el recorrido como SequentialPaginator
 * 
 * @author ramiro
 *
 */
public class SkippingPaginator extends Paginator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -0L;

	private static Logger logger = Logger.getLogger(SkippingPaginator.class.getName());

	private static final int NO_PAGE_SIZE = -1;
	
	protected int pageSize = NO_PAGE_SIZE;
	

	/**
	 * Guarda un PipePointer de cada Pipe para cada página
	 */
	protected Map<Integer, Map<Pipe, PipePointer>> pageMap = new HashMap<Integer, Map<Pipe,PipePointer>>();
	
	public SkippingPaginator() {
	}

	@Override
	protected void postHook(int pageNumber) {
		//Grabo pointers
		Map<Pipe,PipePointer> newEntry = new HashMap<Pipe,PipePointer>();
		pageMap.put(pageNumber, newEntry);
		for (Pipe pipe : pipes) {
			newEntry.put(pipe, pipe.currentPointer());
		}
	}

	@Override
	protected void setUpPipes(int pageNumber, int pageSize) throws PipeException {
		if ( pageNumber > 0 ) {
			// Se saltea los registros iniciales, que ya devolvió en páginas anteriores
			Map<Pipe, PipePointer> lastPagePointers = pageMap.get(pageNumber - 1);
			if ( lastPagePointers == null ) {
				throw new ComponentExecutionException("No hay pointer de la página anterior a " + pageNumber);
			}
			
			for (Pipe pipe : pipes) {
				PipePointer pp = lastPagePointers.get(pipe);
				if ( pp == null ) {
					throw new ComponentExecutionException("No hay pointer para el pipe " + pipe);
				}
				pipe.seek(pp);
			}
		}
	}

	@Override
	protected void preHook(int pageNumber, int pageSize) throws PipeException {
		if ( this.pageSize != NO_PAGE_SIZE && this.pageSize != pageSize) {
			throw new ComponentExecutionException("No puedo retornar páginas de distinto tamaño. " +
					"Ya se pidieron páginas de tamaño " + this.pageSize + " y ahora se pidió tamaño " + pageSize);
		}
	}

}
