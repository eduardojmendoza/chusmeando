package com.qbe.services.paginate.service1107;

import java.io.Serializable;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * 
 * 
      <REG>
        <PROD>AUS1</PROD>
        <POL>00000001</POL>
        <CERPOL>9000</CERPOL>
        <CERANN>2016</CERANN>
        <CERSEC>647061</CERSEC>
        <CLIDES>ZUBIETA , HECTOR RAUL </CLIDES>
        <TOMARIES>FIAT  PALIO WE ADVENTURE 1.6 LOCKER</TOMARIES>
        <EST>RETENIDA</EST>
        <TIPOPE>NUEVO CERTIFICADO</TIPOPE>
        <FECING>01/01/2014</FECING>
        <COD>PR-3233</COD>
        <RAMO>1</RAMO>
        <LUPA>S</LUPA>
        <RENOVACION>N</RENOVACION>
        <MARCAOV />
        <SQLCERTIPOL />
        <SQLCERTIANN />
        <SQLCERTISEC />
      </REG> 
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="REG")
public class Reg1107 extends ResponseRecord implements Serializable{
	
	@XmlElement(required=true, name="PROD")
	private String prod = "";

	@XmlElement(required=true, name="POL")
	private String pol = "";

	@XmlElement(required=true, name="CERPOL")
	private String cerpol = "";

	@XmlElement(required=true, name="CERANN")
	private String cerann = "";

	@XmlElement(required=true, name="CERSEC")
	private String cersec = "";

//	@XmlElement(required=true, name="SUPLENUM")
//	private String suplenum = "";
//
//	@XmlElement(required=true, name="OPERAPOL")
//	private String operapol = "";

	@XmlElement(required=true, name="CLIDES")
	private String clides = "";

	@XmlElement(required=true, name="TOMARIES")
	private String tomaries = "";
	
	@XmlElement(required=true, name="EST")
	private String est = "";

	@XmlElement(required=true, name="TIPOPE")
	private String tipope = "";

	@XmlElement(required=true, name="FECING")
	private String fecing = "";

	@XmlElement(required=true, name="COD")
	private String cod = "";

	@XmlElement(required=true, name="RAMO")
	private String ramo = "";

	@XmlElement(required=true, name="LUPA")
	private String lupa = "";

	@XmlElement(required=true, name="RENOVACION")
	private String renovacion = "";

	@XmlElement(required=true, name="MARCAOV")
	private String marcaov = "";

	@XmlElement(required=true, name="SQLCERTIPOL")
	private String sqlcertipol = "";

	@XmlElement(required=true, name="SQLCERTIANN")
	private String sqlcertiann = "";

	@XmlElement(required=true, name="SQLCERTISEC")
	private String sqlcertisec = "";

	public String getPolicyNumber() {
		return "#" + getProd() + "-" + getPol() + "-" + getCerpol() + "-" + getCerann() + "-" + getCersec(); // + "-" + getOperapol();
	}
	
	public String getProd() {
		return prod;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getCerpol() {
		return cerpol;
	}

	public void setCerpol(String cerpol) {
		this.cerpol = cerpol;
	}

	public String getCerann() {
		return cerann;
	}

	public void setCerann(String cerann) {
		this.cerann = cerann;
	}

	public String getCersec() {
		return cersec;
	}

	public void setCersec(String cersec) {
		this.cersec = cersec;
	}

	public String getClides() {
		return clides;
	}

	public void setClides(String clides) {
		this.clides = clides;
	}

	public String getTomaries() {
		return tomaries;
	}

	public void setTomaries(String tomaries) {
		this.tomaries = tomaries;
	}

	public String getEst() {
		return est;
	}

	public void setEst(String est) {
		this.est = est;
	}

	public String getTipope() {
		return tipope;
	}

	public void setTipope(String tipope) {
		this.tipope = tipope;
	}

	public String getFecing() {
		return fecing;
	}

	public void setFecing(String fecing) {
		this.fecing = fecing;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getLupa() {
		return lupa;
	}

	public void setLupa(String lupa) {
		this.lupa = lupa;
	}

	public String getRenovacion() {
		return renovacion;
	}

	public void setRenovacion(String renovacion) {
		this.renovacion = renovacion;
	}

	public String getMarcaov() {
		return marcaov;
	}

	public void setMarcaov(String marcaov) {
		this.marcaov = marcaov;
	}

	public String getSqlcertipol() {
		return sqlcertipol;
	}

	public void setSqlcertipol(String sqlcertipol) {
		this.sqlcertipol = sqlcertipol;
	}

	public String getSqlcertiann() {
		return sqlcertiann;
	}

	public void setSqlcertiann(String sqlcertiann) {
		this.sqlcertiann = sqlcertiann;
	}

	public String getSqlcertisec() {
		return sqlcertisec;
	}

	public void setSqlcertisec(String sqlcertisec) {
		this.sqlcertisec = sqlcertisec;
	}
	
}
