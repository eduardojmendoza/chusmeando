package com.qbe.services.paginate;

import java.util.concurrent.Callable;

public class PipePeeker implements Callable<Object> {

	private Pipe pipe;
	
	public PipePeeker(Pipe pipe) {
		this.pipe = pipe;
	}

	@Override
	public Object call() throws Exception {
		return pipe.peek();
	}

}
