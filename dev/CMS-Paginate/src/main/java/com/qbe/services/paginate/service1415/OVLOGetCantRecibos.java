package com.qbe.services.paginate.service1415;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.JAXBException;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MulticommRequestsListResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;

public class OVLOGetCantRecibos extends PaginatedActionCode {

	protected static final String ACTION_CODE_1415 = "lbaw_OVLOGetCantRecibos";

	public OVLOGetCantRecibos() {
		setPageSize(Integer.MAX_VALUE);
	}

	@Override
	protected ContinuationResponse createResponse() {
		return new Response1415();
	}

	@Override
	protected Comparator<Object> createComparator(PaginateRequest request) {
		return new Comparator1415();
	}

	@Override
	protected PaginateRequest parseRequestXML(String requestXML) throws JAXBException {
		return (new Request1415()).unmarshall(requestXML);
	}

	@Override
	public List<Pipe> createPipes(PaginateRequest request) {

		List<Pipe> pipes = new ArrayList<Pipe>();
		pipes.add(createCMSPipe(request, ACTION_CODE_1415, new Response1415Unmarshaller()));
		pipes.addAll(createInsisPipes(request, ACTION_CODE_1415, new Response1415Unmarshaller(), Integer.MAX_VALUE));
		return pipes;
	}

	@Override
	public List<? extends PaginateRequest> getMulticommRequests(PaginateRequest request) throws MalformedURLException,
			OSBConnectorException, JAXBException {
		List<? extends PaginateRequest> requests = (new MulticommRequestsListResponse<Request1415>())
				.getMulticommRequests(request);
		return requests;
	}

	@Override
	public InsisPipe createInsisPipe() {
		return new Insis1415Pipe();
	}

}
