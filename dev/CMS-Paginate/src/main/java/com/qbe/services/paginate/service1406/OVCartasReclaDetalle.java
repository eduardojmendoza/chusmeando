package com.qbe.services.paginate.service1406;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MulticommRequestsListResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;

public class OVCartasReclaDetalle extends PaginatedActionCode {

	public static final int MESSAGE_1406_PAGE_SIZE = 200;
	
	protected static final String ACTION_CODE_1406 = "lbaw_OVCartasReclaDetalle";
	
	private static Logger logger = Logger.getLogger(OVCartasReclaDetalle.class.getName());

	public OVCartasReclaDetalle() {
		setPageSize(MESSAGE_1406_PAGE_SIZE);		
	}

	public List<? extends PaginateRequest> getMulticommRequests(PaginateRequest request)
			throws MalformedURLException, OSBConnectorException, JAXBException {
				List<? extends PaginateRequest> requests = (new MulticommRequestsListResponse<Request1406>()).getMulticommRequests(request);
				return requests;
			}

	protected PaginateRequest parseRequestXML(String requestXML) throws JAXBException {
		return (new Request1406()).unmarshall(requestXML);
	}

	protected ContinuationResponse createResponse() {
		return new Response1406();
	}

	protected Comparator<Object> createComparator(PaginateRequest request) {
		return new Comparator1406();
	}

	public List<Pipe> createPipes(PaginateRequest request) {

		List<Pipe> pipes = new ArrayList<Pipe>();
		pipes.add(createCMSPipe(request, ACTION_CODE_1406, new Response1406Unmarshaller()));
		pipes.addAll(createInsisPipes(request, ACTION_CODE_1406, new Response1406Unmarshaller(), MESSAGE_1406_PAGE_SIZE));
		return pipes;
	}
	
	public InsisPipe createInsisPipe() {
		InsisPipe insis = new Insis1406Pipe();
		return insis;
	}
}