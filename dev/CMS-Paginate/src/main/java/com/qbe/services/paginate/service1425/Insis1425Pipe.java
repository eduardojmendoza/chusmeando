package com.qbe.services.paginate.service1425;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.OSBConnectorLocator;

public class Insis1425Pipe extends InsisPipe {

	@Override
	public OSBConnector getConnector() {
		return OSBConnectorLocator.getInstance().getInsis1425Connector();
	}

	
}