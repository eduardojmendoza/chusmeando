package com.qbe.services.paginate.service1404;

import java.io.Serializable;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.service1428.Request1428;

/**
 * Compara dos Reg1404 teniendo en cuenta los criterios de orden definidos en la 
 * especificación del servicio
 * 
 * @author ramiro
 *
 */
public class Comparator1404 implements Comparator<Object>, Serializable {

	private static Logger logger = Logger.getLogger(Comparator1404.class.getName());

	protected Request1404 request;
	
	public Comparator1404() {
	}
	
	public Comparator1404(PaginateRequest request) {
		this.request = (Request1404)request;
	}


	/**
	 * 
	 * 
	 * @param o1
	 * @param o2
	 * @return
	 */
	@Override
    public int compare(Object o1, Object o2) {
    	if ( o1 == null && o2 == null ) {
            	throw new NullPointerException("Ambos elementos son null");              
            }else if(o2 == null) {
                return -1;
            } else if (o1 == null) {
                return 1;
            } else {
            	Reg1404 r1 = (Reg1404)o1;
            	Reg1404 r2 = (Reg1404)o2;
            	
            	int orden = -1;

            	if ( request.getNroorden() != null && !request.getNroorden().equals("") ) {
            		orden = Integer.parseInt(request.getNroorden());
            	}

            	logger.log(Level.FINEST, String.format("Comparando [ orden = %d]:\n%s\n%s", orden, r1.getComparePath(), r2.getComparePath()));
            		
        		switch (orden) {
				case 1:
					return Integer.signum(r1.getClides().compareTo(r2.getClides()));
				case 2:
					return Integer.signum(r1.getPoliza().compareTo(r2.getPoliza()));
				case 3:
					return Integer.signum(r1.getMon().compareTo(r2.getMon()));
				case 4:
					return Integer.signum(r1.getImpto().compareTo(r2.getImpto()));
				case 5:
					return Integer.signum(r1.getI30().compareTo(r2.getI30()));
				case 6:
					return Integer.signum(r1.getI60().compareTo(r2.getI60())); 
				case 7:
					return Integer.signum(r1.getI90().compareTo(r2.getI90()));							
				case 8:
					return Integer.signum(r1.getIm90().compareTo(r2.getIm90()));							
				case 9:
					return Integer.signum(r1.getEst().compareTo(r2.getEst()));							
				case 10:
					return Integer.signum(r1.getSini().compareTo(r2.getSini()));							
				case 11:
					return Integer.signum(r1.getAge().compareTo(r2.getAge()));							
				case 12:
					return Integer.signum(r1.getFecvto().compareTo(r2.getFecvto()));							
				default:
					return 0;
				}
            }
        }

	}
