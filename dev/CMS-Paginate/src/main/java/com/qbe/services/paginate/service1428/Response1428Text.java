package com.qbe.services.paginate.service1428;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.services.paginate.Response;
import com.qbe.vbcompat.framework.ComponentExecutionException;


/**
 * Response del 1428 en el caso de que retorna un TXT o XSL.
 * Solamente tiene el tag Estado y después el texto
 * 
 * @author ramiro
 *
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Response1428Text extends Response {

	/**
	 * Guarda el texto va después del Estado.
	 * En realidad soporta varios chunks de texto mezclados, si llegamos a agregar algún otro tag
	 * Ver http://stackoverflow.com/questions/10940267/jaxb-xmlmixed-usage-for-reading-xmlvalue-and-xmlelement/11099303#11099303
	 * 
	 */
	@XmlElementRef(name="Response", type=Response1428Text.class)
	@XmlMixed
	private List<Object> mixedContent = new ArrayList<Object>();
	
	public List<Object> getMixedContent() {
        return mixedContent;
    }

    public void setMixedContent(List<Object> mixedContent) {
        this.mixedContent = mixedContent;
    }
    
    public Response1428Text() {
	}

	public static Response1428Text unmarshal(String xml) throws JAXBException {
		JAXBContext ovContext = JAXBContext.newInstance(Response1428Text.class);
		Unmarshaller um = ovContext.createUnmarshaller();
		StringReader sr = new StringReader(xml);
		Object o = um.unmarshal(sr);
		if (!(o instanceof Response1428Text)) {
			throw new ComponentExecutionException("No pude unmarshalear a un " + Response1428Text.class.getName() + ", el resultado es de otra clase");
		}
		Response1428Text request = (Response1428Text) o;
		return request;
	}


}
