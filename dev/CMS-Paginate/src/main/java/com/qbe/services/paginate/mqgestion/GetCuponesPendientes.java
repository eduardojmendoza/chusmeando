package com.qbe.services.paginate.mqgestion;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MulticommRequestsListResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;
import com.qbe.services.paginate.service1605.Comparator1605;
import com.qbe.services.paginate.service1605.Insis1605Pipe;
import com.qbe.services.paginate.service1605.Request1605;
import com.qbe.services.paginate.service1605.Response1605;
import com.qbe.services.paginate.service1605.Response1605Unmarshaller;

public class GetCuponesPendientes extends PaginatedActionCode {

	/**
	 * El pagesize especificado en la planilla del servicio es 201, pero
	 * AIS devuelve 200 registros.
	 * Dejamos 200 para que no haga 2 requests en el caso de que los primeros 200 sean de AIS
	 */
	public static final int MESSAGE_1605_PAGE_SIZE = 200;
	
	protected static final String ACTION_CODE_1605 = "lbaw_GetConsultaMQGestion";
	
	private static Logger logger = Logger.getLogger(GetCuponesPendientes.class.getName());

	public GetCuponesPendientes() {
		setPageSize(MESSAGE_1605_PAGE_SIZE);		
	}

	public List<? extends PaginateRequest> getMulticommRequests(PaginateRequest request)
			throws MalformedURLException, OSBConnectorException, JAXBException {
				List<? extends PaginateRequest> requests = (new MulticommRequestsListResponse<Request1605>()).getMulticommRequests(request);
				return requests;
			}

	protected PaginateRequest parseRequestXML(String requestXML) throws JAXBException {
		return (new Request1605()).unmarshall(requestXML);
	}

	protected ContinuationResponse createResponse() {
		return new Response1605();
	}

	protected Comparator<Object> createComparator(PaginateRequest request) {
		return new Comparator1605();
	}

	public List<Pipe> createPipes(PaginateRequest request) {

		List<Pipe> pipes = new ArrayList<Pipe>();
		pipes.add(createCMSPipe(request, ACTION_CODE_1605, new Response1605Unmarshaller()));
		pipes.addAll(createInsisPipes(request, ACTION_CODE_1605, new Response1605Unmarshaller(), MESSAGE_1605_PAGE_SIZE));
		return pipes;
	}
	
	public InsisPipe createInsisPipe() {
		InsisPipe insis = new Insis1605Pipe();
		return insis;
	}
}