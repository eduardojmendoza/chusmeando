package com.qbe.services.paginate;

import java.io.Serializable;

import javax.xml.bind.JAXBException;


/**
 * FIXME Se debería poder hacer una sola clase y parametrizar las instancias que debe crear
 * @author ramiro
 *
 */
public abstract class ResponseUnmarshaller implements Serializable {

	public abstract ContinuationResponse unmarshalResponse(String response) throws JAXBException;

}