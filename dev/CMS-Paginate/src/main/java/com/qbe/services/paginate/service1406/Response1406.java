package com.qbe.services.paginate.service1406;

import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.GenericMarshaller;
import com.qbe.services.paginate.MSGESTContinuationResponse;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Response específico del 1406
 * 
 * 
 * <Response>		
	<Estado resultado="true" mensaje=""/>	
	<MSGEST>OK</MSGEST>	si es TR 
	<FECENVCONT/>
    <PRODUCTO/>
    <POLIZA/>
	<REGS>	
		<REG>
 * 
 * TODO Necesita refactoring con ContinuationResponse1010
 *
 * @author gavilan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "Response")
public class Response1406 extends MSGESTContinuationResponse {

	
	@XmlElement(required=true, name="FECENVCONT")
	private String fecenvcont = "";


	@XmlElement(required=true, name="PRODUCTO")
	private String producto = "";

	@XmlElement(required=true, name="POLIZA")
	private String poliza = "";


	public Response1406() {
		super();
	}

	@XmlElementWrapper(name = "REGS")
	@XmlElement(required = true, name = "REG")
	private List<Reg1406> records = new ArrayList<Reg1406>();

	@Override
	public void setSerializedData(String serialize) {
		this.setPoliza(serialize);
	}

	@Override
	public String getSerializedData() {
		return getPoliza();
	}

	@Override
	public List<? extends ResponseRecord> getRecords() {
		return records;
	}

	public void setRecords(List<Reg1406> records) {
		this.records = records;
	}

	@Override
	public void addRecord(ResponseRecord r) {
		this.records.add((Reg1406)r);
	}

	public String getFecenvcont() {
		return fecenvcont;
	}

	public void setFecenvcont(String fecenvcont) {
		this.fecenvcont = fecenvcont;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	

}