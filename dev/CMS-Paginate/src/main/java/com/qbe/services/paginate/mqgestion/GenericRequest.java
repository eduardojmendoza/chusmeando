package com.qbe.services.paginate.mqgestion;

import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.GenericMarshaller;


/**
 * Un GenericRequest define las propiedades comunes a los requests de mensajes genéricos MQGestion o MQConsultaGestion

 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Request")
public class GenericRequest implements Serializable {

	@XmlElement(required = false, name = "DEFINICION")
	private String definicion = "";

	public static GenericRequest unmarshall(String Request) throws JAXBException {
		JAXBContext ovContext = JAXBContext.newInstance(GenericRequest.class);
		Unmarshaller um = ovContext.createUnmarshaller();
		StringReader sr = new StringReader(Request);
		Object o = um.unmarshal(sr);
		if (!(o instanceof GenericRequest)) {
			throw new IllegalArgumentException("No pude unmarshalear a un " + GenericRequest.class.getName() + ", el resultado es de otra clase");
		}
		GenericRequest request = (GenericRequest) o;
		return request;
	}
	
	public GenericRequest() {
		super();
	}

	public String marshall() throws JAXBException {
		return GenericMarshaller.marshal(this);
	}


	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getDefinicion() {
		return definicion;
	}

	public void setDefinicion(String definicion) {
		this.definicion = definicion;
	}
	
}