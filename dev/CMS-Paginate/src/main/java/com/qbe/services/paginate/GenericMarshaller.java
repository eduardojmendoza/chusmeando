package com.qbe.services.paginate;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class GenericMarshaller {

	public GenericMarshaller() {
		// TODO Auto-generated constructor stub
	}

	public static String marshal(Object o) throws JAXBException {
		JAXBContext ovContext = JAXBContext.newInstance(o.getClass());
		Marshaller m = ovContext.createMarshaller();
		m.setProperty(Marshaller.JAXB_FRAGMENT, true);
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		StringWriter writer = new StringWriter();
		m.marshal(o, writer);
		String xml = writer.getBuffer().toString().trim();
		return xml;
	}
}
