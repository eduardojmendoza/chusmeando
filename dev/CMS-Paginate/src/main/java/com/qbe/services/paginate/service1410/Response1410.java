package com.qbe.services.paginate.service1410;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.MSGESTContinuationResponse;
import com.qbe.services.paginate.ResponseRecord;

/**
 * Response específico del 1410
 * 
 * 
 * <Response>		
	<Estado resultado="true" mensaje=""/>	
	<MSGEST>OK</MSGEST>	si es TR 
	<CONTINUAR></CONTINUAR>
	<REGS>	
		<REG>
 * 
 * TODO Necesita refactoring con ContinuationResponse1010
 *
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "Response")
public class Response1410 extends MSGESTContinuationResponse {

	
	
	@XmlElement(required = true, name = "CONTINUAR")
	private String continuar = "";


	public Response1410() {
		super();
	}

	@XmlElementWrapper(name = "REGS")
	@XmlElement(required = true, name = "REG")
	private List<Reg1410> records = new ArrayList<Reg1410>();

	@Override
	public void setSerializedData(String serialize) {
		this.setContinuar(serialize);
	}

	@Override
	public String getSerializedData() {
		return getContinuar();
	}

	@Override
	public List<? extends ResponseRecord> getRecords() {
		return records;
	}

	public void setRecords(List<Reg1410> records) {
		this.records = records;
	}

	@Override
	public void addRecord(ResponseRecord r) {
		this.records.add((Reg1410)r);
	}

	public String getContinuar() {
		return continuar;
	}

	public void setContinuar(String continuar) {
		this.continuar = continuar;
	}


}