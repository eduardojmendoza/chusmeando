package com.qbe.services.paginate.service1428;

import javax.xml.bind.JAXBException;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.ResponseUnmarshaller;

public class Response1428Unmarshaller extends ResponseUnmarshaller {

	@Override
	public ContinuationResponse unmarshalResponse(String response) throws JAXBException {
		ContinuationResponse cr = (new Response1428()).unmarshal(response);
		return cr;
	}

}
