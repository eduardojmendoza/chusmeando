package com.qbe.services.paginate;

import javax.xml.bind.annotation.XmlElement;

public abstract class UsuarcodNivelasRequest extends PaginateRequest {

	@XmlElement(required = true, name="USUARCOD")
	private String usuarcod = "";

    @XmlElement(required = true, name="NIVELAS")	
	private String nivelAs = "";

	public String getUsuario() {
		return getUsuarcod();
	}

	public String getNivelAs() {
		return nivelAs;
	}

	public String getUsuarcod() {
		return usuarcod;
	}

	public void setUsuarcod(String usuarcod) {
		this.usuarcod = usuarcod;
	}

	public void setNivelAs(String nivelAs) {
		this.nivelAs = nivelAs;
	}


}
