package com.qbe.services.paginate.service1425;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ResponseRecord;

/**
 * Registro dentro de RECIBOS en el Request del 1425
 * 
<RECIBO>
            <RECNUM>021500381</RECNUM>
</RECIBO>
 * 
 * @author martin
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="RECIBO")
public class Rec1425 extends ResponseRecord implements Serializable{

	@XmlElement(required=true, name="RECNUM")
	private String recnum = "";
	

	public String getRecnum() {
		return recnum;
	}

	public void setRecnum(String recnum) {
		this.recnum = recnum;
	}

	
	

}
