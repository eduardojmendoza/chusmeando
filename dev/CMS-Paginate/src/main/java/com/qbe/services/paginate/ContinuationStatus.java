package com.qbe.services.paginate;


import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlEnum
public enum ContinuationStatus {

	@XmlEnumValue("")
	EMPTY(""),
	@XmlEnumValue("OK")
	OK("OK"),
	@XmlEnumValue("TR")
	TR("TR"),
	@XmlEnumValue("ER")
	ER("ER");

	private final String value;

	private ContinuationStatus(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	
}
