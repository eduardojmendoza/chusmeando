package com.qbe.services.paginate.service1415;

import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.GenericMarshaller;
import com.qbe.services.paginate.MSGESTContinuationResponse;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Response específico del 1415
 * 
 * 
<Response>			
	<Estado resultado="true" mensaje=""/>		
	<MSGEST>LIS</MSGEST>		
	<COTIDOLAR>5.4810000 </COTIDOLAR>		
	<PRODS>		
		<PROD><![CDATA[ALA1]]></PROD>	
		<PROD><![CDATA[ALM1]]></PROD>	
		<PROD><![CDATA[API1]]></PROD>	
		<PROD><![CDATA[APL1]]></PROD>	
		<PROD><![CDATA[APM1]]></PROD>	
		<PROD><![CDATA[ART1]]></PROD>	
		<PROD><![CDATA[ATD1]]></PROD>	
		<PROD><![CDATA[AUD1]]></PROD>	
		<PROD><![CDATA[AUI1]]></PROD>	
		<PROD><![CDATA[AUP1]]></PROD>	
		<PROD><![CDATA[AUS1]]></PROD>	
		<PROD><![CDATA[EGP1]]></PROD>	
		<PROD><![CDATA[HOB1]]></PROD>	
		<PROD><![CDATA[HOM1]]></PROD>	
		<PROD><![CDATA[ICI1]]></PROD>	
		<PROD><![CDATA[INH1]]></PROD>	
		<PROD><![CDATA[RCM1]]></PROD>	
		<PROD><![CDATA[RCP1]]></PROD>	
		<PROD><![CDATA[VRN1]]></PROD>	
	</PRODS>		
	<REGS>		
		<REG>	
			<CLIDES><![CDATA[CONS. PROP. PERGAMIN,]]></CLIDES>
			<CLIENSEC>2343090</CLIENSEC>
			<PROD>CON1</PROD>
			<POL>00168905</POL>
			<CERPOL>0000</CERPOL>
			<CERANN>0000</CERANN>
			<CERSEC>000000</CERSEC>
			<OPERAPOL>0</OPERAPOL>
			<RECNUM>641714312</RECNUM>
			<MON><![CDATA[$]]></MON>
			<SIG/>
			<IMP>103,00</IMP>
			<IMPCALC>103.00</IMPCALC>
			<RENDIDO>0</RENDIDO>
			<RAMO>2</RAMO>
			<FECVTO><![CDATA[01/08/2013]]></FECVTO>
			<AGE><![CDATA[PR-5641]]></AGE>
			<ESTADO>X</ESTADO>
		</REG>	
[...]
 * 
 *
 * @author martin
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "Response")
public class Response1415 extends ContinuationResponse {

	
	@XmlElement(required = true, name = "MSGEST")
	private String msgest = "";
	@XmlElement(required = true, name = "COTIDOLAR")
	private String cotidolar = "";

	@XmlElementWrapper(name = "PRODS")
	@XmlElement(required = true, name = "PROD")
	private List<String> prods = new ArrayList<String>();

	@XmlElementWrapper(name = "REGS")
	@XmlElement(required = true, name = "REG")
	private List<Reg1415> records = new ArrayList<Reg1415>();


	public Response1415() {
		super();
	}

	@Override
	public void setSerializedData(String serialize) {
	}

	@Override
	public String getSerializedData() {
		return "";
	}

	@Override
	public List<? extends ResponseRecord> getRecords() {
		return records;
	}

	public void setRecords(List<Reg1415> records) {
		this.records = records;
	}

	@Override
	public void addRecord(ResponseRecord r) {
		this.records.add((Reg1415)r);
	}

	
	@Override
	public void beforeMarshal() {
		this.recalculateMsgest();
	}
	
	/**
	 * Recálculo específico del 1415
	 * 
	 * De la spec:
	 * 	If the query obtains <= 100 coupons , then it returns "LIST" is TAG <MSGEST >and the list of copupons
	 *  If the query obtains > 100 coupons , then it returns "ING" is TAG <MSGEST> and doesn´t return coupons
	 *  If there are not coupons ,then it returns "VAC" is TAG <MSGEST> and doesn´t return coupons
	 *  	
	 * 
	 * 
	 * @throws ParseException 
	 */
	public void recalculateMsgest() {
		int recCount = getRecords().size();
		if ( recCount == 0) {
			setMsgest("VAC");
		} else if ( recCount <= 100) {
			setMsgest("LIST");
		} else  /* if ( recCount > 100 ) */ {
			setMsgest("ING");
		}
	}

	public String getMsgest() {
		return msgest;
	}

	public void setMsgest(String msgest) {
		this.msgest = msgest;
	}

	public String getCotidolar() {
		return cotidolar;
	}

	public void setCotidolar(String cotidolar) {
		this.cotidolar = cotidolar;
	}

	public List<String> getProds() {
		return prods;
	}

	public void setProds(List<String> prods) {
		this.prods = prods;
	}

	/**
	 * Este mensaje no implementa paginado, así que retornamos siempre el valor de fin de paginado
	 */
	@Override
	public ContinuationStatus getContinuationStatus() {
		return ContinuationStatus.OK;
	}

	/**
	 * Este mensaje no implementa paginado, así que es una NO OP
	 */
	@Override
	public void setContinuationstatus(ContinuationStatus msgest) {
	}


}