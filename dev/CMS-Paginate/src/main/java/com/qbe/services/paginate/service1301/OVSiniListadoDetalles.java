package com.qbe.services.paginate.service1301;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MulticommRequestsListResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;

public class OVSiniListadoDetalles extends PaginatedActionCode {

	/**
	 * El pagesize especificado en la planilla del servicio es 201, pero
	 * AIS devuelve 200 registros.
	 * Dejamos 200 para que no haga 2 requests en el caso de que los primeros 200 sean de AIS
	 */
	public static final int MESSAGE_1301_PAGE_SIZE = 200;
	
	protected static final String ACTION_CODE_1301 = "lbaw_OVSiniListadoDetalles";
	
	private static Logger logger = Logger.getLogger(OVSiniListadoDetalles.class.getName());

	public OVSiniListadoDetalles() {
		setPageSize(MESSAGE_1301_PAGE_SIZE);		
	}

	public List<? extends PaginateRequest> getMulticommRequests(PaginateRequest request)
			throws MalformedURLException, OSBConnectorException, JAXBException {
				List<? extends PaginateRequest> requests = (new MulticommRequestsListResponse<Request1301>()).getMulticommRequests(request);
				return requests;
			}

	protected PaginateRequest parseRequestXML(String requestXML) throws JAXBException {
		return (new Request1301()).unmarshall(requestXML);
	}

	protected ContinuationResponse createResponse() {
		return new Response1301();
	}

	protected Comparator<Object> createComparator(PaginateRequest request) {
		return new Comparator1301();
	}

	public List<Pipe> createPipes(PaginateRequest request) {

		List<Pipe> pipes = new ArrayList<Pipe>();
		pipes.add(createCMSPipe(request, ACTION_CODE_1301, new Response1301Unmarshaller()));
		pipes.addAll(createInsisPipes(request, ACTION_CODE_1301, new Response1301Unmarshaller(), MESSAGE_1301_PAGE_SIZE));
		return pipes;
	}
	
	public InsisPipe createInsisPipe() {
		InsisPipe insis = new Insis1301Pipe();
		return insis;
	}
}