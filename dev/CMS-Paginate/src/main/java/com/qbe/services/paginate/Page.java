package com.qbe.services.paginate;

import java.util.ArrayList;
import java.util.List;

public class Page {
	
	private int pageSize;
	
	private List<Object> elements = new ArrayList<Object>();

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	public boolean isFull() {
		return this.elements.size() == getPageSize();
	}
	
	public void add(Object o) {
		this.elements.add(o);
	}
	
	public List<Object> getElements() {
		return elements;
	}
	
	public int size() {
		return getElements().size();
	}

}
