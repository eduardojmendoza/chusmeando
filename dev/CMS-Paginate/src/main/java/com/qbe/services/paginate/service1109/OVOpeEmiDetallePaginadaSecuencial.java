package com.qbe.services.paginate.service1109;

import com.qbe.services.paginate.Paginator;
import com.qbe.services.paginate.SequentialPaginator;

public class OVOpeEmiDetallePaginadaSecuencial extends OVOpeEmiDetallePaginada {

	public OVOpeEmiDetallePaginadaSecuencial() {
	}

	@Override
	protected Paginator createPaginator() {
		return new SequentialPaginator();
	}

	
}
