package com.qbe.services.paginate;

import java.io.StringReader;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * ContinuationResponse tiene una lista de ResponseRecords, un estado y un flag que indica 
 * "la continuación", es decir si hay más registros disponibles en una página siguiente
 * 
 * 
 * Define métodos abstractos y algunas variables comunes a las subclases.
 * No lo pudimos definir así con la variable 'records' porque la
 * serialización con JAXB no se lleva bien con la herencia. Por ejemplo, agrega xsi:type a
 * cada registro de las subclases. En el caso de la Response los pudimos sacar con 
 * @XmlTransient, pero en los records los generaba todos con xsi:type
 * @see http://blog.bdoughan.com/2011/06/ignoring-inheritance-with-xmltransient.html
 * 
 * @author ramiro
 *
 */
@XmlTransient //Puesto para que no genere los xsi:type en las subclases
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class ContinuationResponse extends Response {

	public abstract void addRecord(ResponseRecord r);

	public abstract List<? extends ResponseRecord> getRecords();

	public abstract void setSerializedData(String serialize);

	public abstract String getSerializedData();

	/**
	 * Las subclases lo implementan si deben hacer algo, como hace el 1428 que copia el valor de la DEFINICION
	 */
	public void originatingRequest(PaginateRequest request) {
	}

	public abstract ContinuationStatus getContinuationStatus();
	
	public abstract void setContinuationstatus(ContinuationStatus msgest);

	public ContinuationResponse unmarshal(String response) throws JAXBException {
		JAXBContext ovContext = JAXBContext.newInstance(this.getClass());
		Unmarshaller um = ovContext.createUnmarshaller();
		StringReader sr = new StringReader(response);
		Object o = um.unmarshal(sr);
		if (!(o instanceof ContinuationResponse)) {
			throw new ComponentExecutionException("No pude unmarshalear a un " + this.getClass().getName() + ", el resultado es de otra clase");
		}
		ContinuationResponse request = (ContinuationResponse) o;
		return request;
	}


}