package com.qbe.services.paginate;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.qbe.vbcompat.framework.jaxb.Estado;

/**
 * Response básico, contiene un tag Estado, sin contenido específico
 * 
 * @author ramiro
 *
 */
@XmlTransient //Puesto para que no genere los xsi:type en las subclases
@XmlAccessorType(XmlAccessType.FIELD)
public class Response {

	@XmlElement(name = "Estado")
	protected Estado estado;

	public Response() {
		super();
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	/**
	 * Las subclases lo implementan si deben hacer algo, como calcular totales, antes
	 * de que sea marshaleado
	 */
	protected void beforeMarshal() {
	}

	public String marshal() throws JAXBException {
		beforeMarshal();
		JAXBContext ovContext = this.createMarshalContext();
		Marshaller m = ovContext.createMarshaller();
		m.setProperty(Marshaller.JAXB_FRAGMENT, true);
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		StringWriter writer = new StringWriter();
		m.marshal(this, writer);
		String xml = writer.getBuffer().toString().trim();
		return xml;
	}

	/**
	 * Crea un JAXBContext inicializado con las clases a marshalear.
	 * Por default agrega this.getClass()
	 * 
	 * Las subclases pueden redefinirlo para agregar las clases concretas a tener en cuenta,
	 * usualmente las que no se pueden alcanzar estáticamente - ejemplo las clases que van dentro de containers genéricos
	 * 
	 * @return
	 * @throws JAXBException 
	 */
	protected JAXBContext createMarshalContext() throws JAXBException {
		return JAXBContext.newInstance(this.getClass());
	}

}