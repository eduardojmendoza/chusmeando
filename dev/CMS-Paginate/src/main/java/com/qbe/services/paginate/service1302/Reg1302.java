package com.qbe.services.paginate.service1302;

import java.io.Serializable;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Registro de respuesta del 1302
 * 
	<PROD>AUS1</PROD>
	<SINIAN>10</SINIAN>
	<SININUM>108222</SININUM>
	<CLIDES><![CDATA[GIUSTI ALEJANDRO]]></CLIDES>
	<RAMO>1</RAMO>
	<POL>00000001</POL>
	<CERPOL>0000</CERPOL>
	<CERANN>0000</CERANN>
	<CERSEC>547397</CERSEC>
	<EST>C</EST>
	<FECSINI><![CDATA[13/01/2008]]></FECSINI>
	<TOMA><![CDATA[FIAT SIENA 178D70555865296]]></TOMA>
	<AGE>PR-9771</AGE>
 * 
 * @author mmoge
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="REG")
public class Reg1302 extends ResponseRecord implements Serializable{
	

	@XmlElement(required=true, name="PROD")
	private String prod = "";

	@XmlElement(required=true, name="SINIAN")
	private String sinian = "";

	@XmlElement(required=true, name="SININUM")
	private String sinium = "";
	
	@XmlElement(required=true, name="CLIDES")
	private String clides = "";
	
	@XmlElement(required=true, name="RAMO")
	private String ramo = "";
	
	@XmlElement(required=true, name="POL")
	private String pol = "";
	
	@XmlElement(required=true, name="CERPOL")
	private String cerpol = "";
	
	@XmlElement(required=true, name="CERANN")
	private String cerann = "";
	
	@XmlElement(required=true, name="CERSEC")
	private String cersec = "";
	
	@XmlElement(required=true, name="EST")
	private String est = "";
	
	@XmlElement(required=true, name="FECSINI")
	private String fecsini = "";
	
	@XmlElement(required=true, name="TOMA")
	private String toma = "";
	
	@XmlElement(required=true, name="AGE")
	private String age = "";
	
	

	/**
	 * Usado para logging en el comparator
	 * @return
	 */
	public String getComparePath() {
		return "#" +getClides() + "-" + getPoliza();
	}
	
	public String getPoliza() {
		// POLIZA 	= 	PRODUCTO+POLIZA+CERTI
		// CERTI 	= 	<CERPOL> + <CERANN> + <CERSEC>
		String poliza = getProd()+getPol()+getCerpol()+getCerann()+getCersec();
		
		return poliza;
	}
	
	public String getProd() {
		return prod;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}

	public String getSinian() {
		return sinian;
	}

	public void setSinian(String sinian) {
		this.sinian = sinian;
	}

	public String getSinium() {
		return sinium;
	}

	public void setSinium(String sinium) {
		this.sinium = sinium;
	}

	public String getClides() {
		return clides;
	}

	public void setClides(String clides) {
		this.clides = clides;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getCerpol() {
		return cerpol;
	}

	public void setCerpol(String cerpol) {
		this.cerpol = cerpol;
	}

	public String getCerann() {
		return cerann;
	}

	public void setCerann(String cerann) {
		this.cerann = cerann;
	}

	public String getCersec() {
		return cersec;
	}

	public void setCersec(String cersec) {
		this.cersec = cersec;
	}

	public String getEst() {
		return est;
	}

	public void setEst(String est) {
		this.est = est;
	}

	public String getFecsini() {
		return fecsini;
	}

	public void setFecsini(String fecsini) {
		this.fecsini = fecsini;
	}

	public String getToma() {
		return toma;
	}

	public void setToma(String toma) {
		this.toma = toma;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}


	

	
}
