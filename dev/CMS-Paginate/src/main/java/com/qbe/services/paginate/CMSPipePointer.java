package com.qbe.services.paginate;

import org.apache.commons.lang.builder.ToStringBuilder;

public class CMSPipePointer extends PipePointer {

	private int consumedElements;
	
	private PaginateRequest nextBlockRequest;

	private CommRequest lastBlockRequest;
	
	private PaginateRequest request;

	public CMSPipePointer() {
	}

	public int getConsumedElements() {
		return consumedElements;
	}

	public void setConsumedElements(int consumedElements) {
		this.consumedElements = consumedElements;
	}

	public PaginateRequest getNextBlockRequest() {
		return nextBlockRequest;
	}

	public void setNextBlockRequest(PaginateRequest nextBlockRequest) {
		this.nextBlockRequest = nextBlockRequest;
	}

	public PaginateRequest getRequest() {
		return request;
	}

	public void setRequest(PaginateRequest request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public CommRequest getLastBlockRequest() {
		return lastBlockRequest;
	}

	public void setLastBlockRequest(CommRequest lastBlockRequest) {
		this.lastBlockRequest = lastBlockRequest;
	}

	
}
