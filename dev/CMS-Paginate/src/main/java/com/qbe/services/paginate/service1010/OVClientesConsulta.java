package com.qbe.services.paginate.service1010;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import com.qbe.services.paginate.ActionCode1107x9;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.Pipe;
import com.qbe.services.paginate.service1109.Insis1109Pipe;

public class OVClientesConsulta extends ActionCode1010  {

	/**
	 * El pagesize especificado en la planilla del servicio es 201, pero
	 * AIS devuelve 200 registros.
	 * Dejamos 200 para que no haga 2 requests en el caso de que los primeros 200 sean de AIS
	 */
	public static final int MESSAGE_1010_PAGE_SIZE = 199;
	
	protected static final String ACTION_CODE_1010 = "lbaw_OVClientesConsulta";
	
	private static Logger logger = Logger.getLogger(OVClientesConsulta.class.getName());

	public OVClientesConsulta() {
		setPageSize(MESSAGE_1010_PAGE_SIZE);		
	}

	protected ContinuationResponse createResponse() {
		return new Response1010();
	}

	protected Comparator<Object> createComparator(PaginateRequest request) {
		return new Comparator1010();
	}

	public List<Pipe> createPipes(PaginateRequest request) {

		List<Pipe> pipes = new ArrayList<Pipe>();
		pipes.add(createCMSPipe(request, ACTION_CODE_1010, new Response1010Unmarshaller()));
		pipes.addAll(createInsisPipes(request, ACTION_CODE_1010, new Response1010Unmarshaller(), MESSAGE_1010_PAGE_SIZE));
		return pipes;
	}
	
	public InsisPipe createInsisPipe() {
		InsisPipe insis = new Insis1010Pipe();
		return insis;
	}
	
	
}
