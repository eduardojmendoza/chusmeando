package com.qbe.services.paginate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Un SequentialPaginator comienza a recorrer las páginas desde el principio por
 * cada una que le piden
 * 
 * @author ramiro
 *
 */
public class SequentialPaginator extends Paginator implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -0L;

	private static Logger logger = Logger.getLogger(SequentialPaginator.class.getName());
	

	@Override
	protected void skipUpTo(int firstReg) throws PipeException {
		// Se saltea los registros iniciales, que ya devolvió en páginas anteriores
		for (int i = 0; i < firstReg; i++) {
			Object skipped = this.popFirst(pipes);
			logger.log(Level.FINEST, "Skipping: " + skipped);
		}
	}

	@Override
	protected void setUpPipes(int pageNumber, int pageSize) throws PipeException {
		for (Pipe pipe : pipes) {
			pipe.reset();
		}
	}

}
