package com.qbe.services.paginate;

public class PipeTimeoutException extends PipeException {

	public PipeTimeoutException() {
	}

	public PipeTimeoutException(String message) {
		super(message);
	}

	public PipeTimeoutException(Throwable cause) {
		super(cause);
	}

	public PipeTimeoutException(String message, Throwable cause) {
		super(message, cause);
	}

}
