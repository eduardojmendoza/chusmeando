package com.qbe.services.paginate.service1010;

import java.io.Serializable;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * 
 * 
		<REG>
			<PROD>0007</PROD>
			<POL>2350</POL>
			<CERPOL/>
			<CERANN/>
			<CERSEC/>
			<CLIDES>PAPASIDERO , JULIA MARIA </CLIDES>
			<TOMA>CHEVROLET VECTRA R70000514</TOMA>
			<EST>VIGENTE</EST>
			<SINI>0</SINI>
			<ALERTEXI>0</ALERTEXI>
			<AGE>PR-3233</AGE>
			<RAMO>2</RAMO>
			<MARCAREIMPRESION>N</MARCAREIMPRESION>
			<MARCAENDOSABLE>S</MARCAENDOSABLE>
		</REG>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="REG")
public class Reg1010 extends ResponseRecord implements Serializable{
	
	@XmlElement(required=true, name="PROD")
	private String prod = "";

	@XmlElement(required=true, name="POL")
	private String pol = "";

	@XmlElement(required=true, name="CERPOL")
	private String cerpol = "";

	@XmlElement(required=true, name="CERANN")
	private String cerann = "";

	@XmlElement(required=true, name="CERSEC")
	private String cersec = "";

	@XmlElement(required=true, name="CLIDES")
	private String clides = "";

	@XmlElement(required=true, name="TOMA")
	private String toma = "";

	@XmlElement(required=true, name="EST")
	private String est = "";

	@XmlElement(required=true, name="SINI")
	private String sini = "";

	@XmlElement(required=true, name="ALERTEXI")
	private String alertexi = "";

	@XmlElement(required=true, name="AGE")
	private String age = "";

	@XmlElement(required=true, name="RAMO")
	private String ramo = "";

	@XmlElement(required=true, name="MARCAREIMPRESION")
	private String marcareimpresion = "";

	@XmlElement(required=true, name="MARCAENDOSABLE")
	private String marcaendosable = "";


	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	/**
	 * Usado para logging en el comparator
	 * @return
	 */
	public String getComparePath() {
		return "#" + getClides() + "-" + getProd() + "-" + getPol() + "-" + getCerpol() + "-" + getCerann() + "-" + getCersec();
	}
	
	public String getProd() {
		return prod;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getCerpol() {
		return cerpol;
	}

	public void setCerpol(String cerpol) {
		this.cerpol = cerpol;
	}

	public String getCerann() {
		return cerann;
	}

	public void setCerann(String cerann) {
		this.cerann = cerann;
	}

	public String getCersec() {
		return cersec;
	}

	public void setCersec(String cersec) {
		this.cersec = cersec;
	}

	public String getClides() {
		return clides;
	}

	public void setClides(String clides) {
		this.clides = clides;
	}

	public String getToma() {
		return toma;
	}

	public void setToma(String toma) {
		this.toma = toma;
	}

	public String getEst() {
		return est;
	}

	public void setEst(String est) {
		this.est = est;
	}

	public String getSini() {
		return sini;
	}

	public void setSini(String sini) {
		this.sini = sini;
	}

	public String getAlertexi() {
		return alertexi;
	}

	public void setAlertexi(String alertexi) {
		this.alertexi = alertexi;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getMarcareimpresion() {
		return marcareimpresion;
	}

	public void setMarcareimpresion(String marcareimpresion) {
		this.marcareimpresion = marcareimpresion;
	}

	public String getMarcaendosable() {
		return marcaendosable;
	}

	public void setMarcaendosable(String marcaendosable) {
		this.marcaendosable = marcaendosable;
	}

	public String getRamo() {
		return ramo;
	}

}
