package com.qbe.services.paginate;

import java.io.Serializable;

public class PipePointer implements Serializable{

	private boolean sourceExhausted = false;

	public PipePointer() {
	}

	public boolean isSourceExhausted() {
		return sourceExhausted;
	}

	public void setSourceExhausted(boolean sourceExhausted) {
		this.sourceExhausted = sourceExhausted;
	}
}
