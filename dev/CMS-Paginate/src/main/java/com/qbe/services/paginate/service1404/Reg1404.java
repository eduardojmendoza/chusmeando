package com.qbe.services.paginate.service1404;

import java.io.Serializable;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Registro de respuesta del 1404
 * 
<AGE>PR-3233</AGE>
<SINI>0</SINI>
<RAMO>1</RAMO>
<CLIDES><![CDATA[ABADIE, PABLO EUGENIO]]></CLIDES>
<PROD>AUS1</PROD>
<POL>00000001</POL>
<CERPOL>0000</CERPOL>
<CERANN>0001</CERANN>
<CERSEC>489019</CERSEC>
<MON><![CDATA[$]]></MON>
<IMPTO>3641,66</IMPTO>
<I_30>1050,95</I_30>
<I_60>2590,71</I_60>
<I_90>0,00</I_90>
<I_M90>0,00</I_M90>
<EST><![CDATA[VIG]]></EST>
<FECVTO><![CDATA[19/02/2013]]></FECVTO>
<ALERTEXI>0</ALERTEXI>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="REG")
public class Reg1404 extends ResponseRecord implements Serializable{
	
	@XmlElement(required=true, name="AGE")
	private String age = "";

	@XmlElement(required=true, name="SINI")
	private String sini = "";

	@XmlElement(required=true, name="RAMO")
	private String ramo = "";

	@XmlElement(required=true, name="CLIDES")
	private String clides = "";

	@XmlElement(required=true, name="PROD")
	private String prod = "";

	@XmlElement(required=true, name="POL")
	private String pol = "";

	@XmlElement(required=true, name="CERPOL")
	private String cerpol = "";

	@XmlElement(required=true, name="CERANN")
	private String cerann = "";

	@XmlElement(required=true, name="CERSEC")
	private String cersec = "";

	@XmlElement(required=true, name="MON")
	private String mon = "";

	@XmlElement(required=true, name="IMPTO")
	private String impto = "";

	@XmlElement(required=true, name="I_30")
	private String i30 = "";

	@XmlElement(required=true, name="I_60")
	private String i60 = "";

	@XmlElement(required=true, name="I_90")
	private String i90 = "";

	@XmlElement(required=true, name="I_M90")
	private String im90 = "";

	@XmlElement(required=true, name="EST")
	private String est = "";

	@XmlElement(required=true, name="FECVTO")
	private String fecvto = "";

	@XmlElement(required=true, name="ALERTEXI")
	private String alertexi = "";


	/**
	 * Usado para logging en el comparator
	 * @return
	 */
	public String getComparePath() {
		return "#" + getAge() + "-" + getSini() + "-" + getRamo() + "-" + getClides() + "-" + getProd() + "-" + getPol() + "-" + getCerpol() + "-" + getCerann() + "-" + getCersec();
	}
	
	public String getPoliza() {
		return getProd() + getPol() + getCerpol() + getCerann() + getCersec();
	}
	

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getSini() {
		return sini;
	}

	public void setSini(String sini) {
		this.sini = sini;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getClides() {
		return clides;
	}

	public void setClides(String clides) {
		this.clides = clides;
	}

	public String getProd() {
		return prod;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getCerpol() {
		return cerpol;
	}

	public void setCerpol(String cerpol) {
		this.cerpol = cerpol;
	}

	public String getCerann() {
		return cerann;
	}

	public void setCerann(String cerann) {
		this.cerann = cerann;
	}

	public String getCersec() {
		return cersec;
	}

	public void setCersec(String cersec) {
		this.cersec = cersec;
	}

	public String getMon() {
		return mon;
	}

	public void setMon(String mon) {
		this.mon = mon;
	}

	public String getImpto() {
		return impto;
	}

	public void setImpto(String impto) {
		this.impto = impto;
	}

	public String getI30() {
		return i30;
	}

	public void setI30(String i30) {
		this.i30 = i30;
	}

	public String getI60() {
		return i60;
	}

	public void setI60(String i60) {
		this.i60 = i60;
	}

	public String getI90() {
		return i90;
	}

	public void setI90(String i90) {
		this.i90 = i90;
	}

	public String getIm90() {
		return im90;
	}

	public void setIm90(String im90) {
		this.im90 = im90;
	}

	public String getEst() {
		return est;
	}

	public void setEst(String est) {
		this.est = est;
	}

	public String getFecvto() {
		return fecvto;
	}

	public void setFecvto(String fecvto) {
		this.fecvto = fecvto;
	}

	public String getAlertexi() {
		return alertexi;
	}

	public void setAlertexi(String alertexi) {
		this.alertexi = alertexi;
	}
	
}
