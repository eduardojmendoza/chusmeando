package com.qbe.services.paginate;

public class PipeTooManyRowsException extends PipeException {

	public PipeTooManyRowsException() {
	}

	public PipeTooManyRowsException(String message) {
		super(message);
	}

	public PipeTooManyRowsException(Throwable cause) {
		super(cause);
	}

	public PipeTooManyRowsException(String message, Throwable cause) {
		super(message, cause);
	}

}
