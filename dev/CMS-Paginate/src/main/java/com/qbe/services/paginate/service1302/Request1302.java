package com.qbe.services.paginate.service1302;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.UsuarioNivelasRequest;

/**
 * Request del mensaje 1302
 * 
 * 
 <USUARIO>EX009005L</USUARIO>
<NIVELAS>GO</NIVELAS>
<CLIENSECAS>100029438</CLIENSECAS>
<DOCUMTIP/>
<DOCUMNRO/>
<CLIENDES><![CDATA[GIUSTI]]></CLIENDES>
<PRODUCTO/>
<POLIZA/>
<CERTI/>
<PATENTE/>
<SINIAN/>
<SININUM/>
<MSGEST/>
<CONTINUAR/>
 * Campos específicos
 * 
<DOCUMTIP/>
<DOCUMNRO/>
<CLIENDES><![CDATA[GIUSTI]]></CLIENDES>
<PRODUCTO/>
<POLIZA/>
<CERTI/>
<PATENTE/>
<SINIAN/>
<SININUM/>
<MSGEST/>
<CONTINUAR/>

 *
 * Utilizo el campo <CONTINUAR/> para el serializado del paginado.
 * 
 * @author mmoge
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="Request")
public class Request1302 extends UsuarioNivelasRequest {

	/**
	 * 
	 **/
	private static final long serialVersionUID = 1L;

    @XmlElement(required=true, name="DOCUMTIP")
	private String documtip = "";

    @XmlElement(required=true, name="DOCUMNRO")
	private String documnro = "";

	@XmlElement(required=true, name="CLIENDES")
	private String cliendes = "";

	@XmlElement(required=true, name="PRODUCTO")
	private String producto = "";

	@XmlElement(required=true, name="POLIZA")
	private String poliza = "";
	
	@XmlElement(required=true, name="CERTI")
	private String certi = "";

	@XmlElement(required=true, name="PATENTE")
	private String patente = "";
	
	@XmlElement(required=true, name="SINIAN")
	private String sinian = "";
	
	@XmlElement(required=true, name="SININUM")
	private String sinium = "";
	
    @XmlElement(required=true, name="MSGEST")
	private String msgest = "";
	
	@XmlElement(required=true, name="CONTINUAR")
	private String continuar = "";
	
	public Request1302() {
	}

	@Override
	public String getSerializedData() {
		return getContinuar();
	}

	@Override
	public void setSerializedData(String value) {
		this.setContinuar(value);
	}

	@Override
	public PaginateRequest createRequestForNextBlockFromResponse(ContinuationResponse resp) {
		Request1302 newRequest = (Request1302) SerializationUtils.clone(this);
		Response1302 cr = (Response1302)resp;
		if ( newRequest != null ) {
						
			newRequest.setContinuar(cr.getContinuar());
			
						
		}
		return newRequest;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public String getDocumnro() {
		return documnro;
	}

	public void setDocumnro(String documnro) {
		this.documnro = documnro;
	}

	public String getCliendes() {
		return cliendes;
	}

	public void setCliendes(String cliendes) {
		this.cliendes = cliendes;
	}

	public String getCerti() {
		return certi;
	}

	public void setCerti(String certi) {
		this.certi = certi;
	}

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public String getSinian() {
		return sinian;
	}

	public void setSinian(String sinian) {
		this.sinian = sinian;
	}

	public String getSinium() {
		return sinium;
	}

	public void setSinium(String sinium) {
		this.sinium = sinium;
	}

	public String getContinuar() {
		return continuar;
	}

	public void setContinuar(String continuar) {
		this.continuar = continuar;
	}

	public String getDocumtip() {
		return documtip;
	}

	public void setDocumtip(String documtip) {
		this.documtip = documtip;
	}

	public String getMsgest() {
		return msgest;
	}

	public void setMsgest(String msgest) {
		this.msgest = msgest;
	}

	
}
