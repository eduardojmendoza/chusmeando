package com.qbe.services.paginate.service1410;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MulticommRequestsListResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;

public class OVAnulxPagoDetalles extends PaginatedActionCode {

	public static final int MESSAGE_1410_PAGE_SIZE = 200;
	
	protected static final String ACTION_CODE_1410 = "lbaw_OVAnulxPagoDetalles";
	
	private static Logger logger = Logger.getLogger(OVAnulxPagoDetalles.class.getName());

	public OVAnulxPagoDetalles() {
		setPageSize(MESSAGE_1410_PAGE_SIZE);		
	}

	public List<? extends PaginateRequest> getMulticommRequests(PaginateRequest request)
			throws MalformedURLException, OSBConnectorException, JAXBException {
				List<? extends PaginateRequest> requests = (new MulticommRequestsListResponse<Request1410>()).getMulticommRequests(request);
				return requests;
			}

	protected PaginateRequest parseRequestXML(String requestXML) throws JAXBException {
		return (new Request1410()).unmarshall(requestXML);
	}

	protected ContinuationResponse createResponse() {
		return new Response1410();
	}

	protected Comparator<Object> createComparator(PaginateRequest request) {
		return new Comparator1410();
	}

	public List<Pipe> createPipes(PaginateRequest request) {

		List<Pipe> pipes = new ArrayList<Pipe>();
		pipes.add(createCMSPipe(request, ACTION_CODE_1410, new Response1410Unmarshaller()));
		pipes.addAll(createInsisPipes(request, ACTION_CODE_1410, new Response1410Unmarshaller(), MESSAGE_1410_PAGE_SIZE));
		return pipes;
	}
	
	public InsisPipe createInsisPipe() {
		return new Insis1410Pipe();
	}
}