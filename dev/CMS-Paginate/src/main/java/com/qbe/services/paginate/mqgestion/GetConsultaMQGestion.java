package com.qbe.services.paginate.mqgestion;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;

import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;

public class GetConsultaMQGestion implements VBObjectClass {

	public GetConsultaMQGestion() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int IAction_Execute(String requestXML, StringHolder responseSH, String ContextInfo) {

		GenericRequest request;
		try {
			request = parseRequestXML(requestXML);
			String definicion = request.getDefinicion();
			String clazzName = StringUtils.substringBefore(definicion, ".xml");
			String packagePrefix = "com.qbe.services.paginate.mqgestion.";
			try {
				Class<?> loadedClass = Thread.currentThread().getContextClassLoader().loadClass(packagePrefix + clazzName);
				Class<? extends VBObjectClass> mqgestionClass = loadedClass.asSubclass(VBObjectClass.class);
				VBObjectClass vbObject = mqgestionClass.newInstance();
				return vbObject.IAction_Execute(requestXML, responseSH, ContextInfo);
				
			} catch (ClassNotFoundException e) {
				throw new ComponentExecutionException("No existe clase correspondiente al actionCode MQGestion y definición " + clazzName + ".",e);
			} catch (ClassCastException e) {
				// Ahora en este caso es un error, porque levantó una clase que no respeta la interface
				throw new ComponentExecutionException("La clase correspondiente al actionCode MQGestion y definición " + clazzName + " no es una VBObjectClass.",e);
			} catch (InstantiationException e) {
				throw new ComponentExecutionException("En MQGestion y definición " + clazzName + ".",e);
			} catch (IllegalAccessException e) {
				throw new ComponentExecutionException("En MQGestion y definición " + clazzName + ".",e);
			}
		} catch (JAXBException e) {
			throw new ComponentExecutionException("No pude parsear el request " + requestXML + ".",e);
		}
	}

	protected GenericRequest parseRequestXML(String requestXML) throws JAXBException {
		return GenericRequest.unmarshall(requestXML);
	}
	
}
