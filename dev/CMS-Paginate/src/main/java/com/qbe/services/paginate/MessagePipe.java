package com.qbe.services.paginate;

import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Recupera los elementos a partir de un request que envía a OSB
 * 
 * Maneja la lógica de continuación, donde cada respuesta puede indicar si
 * hay una página siguiente y como pedirla
 * 
 * 
 * @author ramiro
 *
 */
public abstract class MessagePipe extends Pipe {

	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(MessagePipe.class.getName());

	/**
	 * Request inicial
	 */
	private PaginateRequest request = null;

	/**
	 * actionCode a enviar a OSB para obtener las respuestas
	 */
	private String actionCode;
	
	/**
	 * Factory de Responses a partir del String obtenido de OSB
	 */
	private ResponseUnmarshaller responseUnmarshaller;

	/**
	 * Indica si no hay más elementos en el source
	 */
	private boolean sourceExhausted = false;
			
	/**
	 * Cantidad de elementos ya devueltos del bloque actual
	 */
	protected int consumedElements = 0;
	
	public MessagePipe() {
		super();
	}
	
	/**
	 * Conector específico para traer resultados desde OSB.
	 * No se guarda como variable porque no se puede serializar, las subclases concretas consiguen el OSBConnector cada
	 * vez que se les pide
	 * 
	 * @return
	 */
	protected abstract OSBConnector getConnector();

	/**
	 * Devuelve el Request a ejecutar para obtener el bloque siguiente al actual
	 * 
	 * @return
	 */
	protected abstract CommRequest getNextSourceBlockRequest();


	@Override
	public Object pop() throws PipeException {
		this.consumedElements++;
		return super.pop();
	}

	@Override
	public void reset() {
		super.reset();
		this.consumedElements = 0;
		sourceExhausted = false;
	}
	
	@Override
	public void seek(PipePointer pp) throws PipeException {
		this.consumedElements = 0;
		sourceExhausted = pp.isSourceExhausted();
	}

	@Override
	protected void fetchNextSourceBlock() throws PipeException {
		CommRequest nextPageRequest = getNextSourceBlockRequest();
		if ( nextPageRequest == null) {
			//Source no tiene más elementos
			return;
		}
		ContinuationResponse resp = doFetch(nextPageRequest);
		updateFromResponse(resp);
	}

	protected void updateFromResponse(ContinuationResponse resp) {
		if ( resp != null && resp.getEstado() != null && resp.getEstado().isTrue() && 
				( Constants.PAGINATOR_OK_TAG.equals(resp.getContinuationStatus()) || resp.getRecords().size() == 0 )) {
			setSourceExhausted(true);
		}
	}

	protected ContinuationResponse doFetch(CommRequest nextRequest) throws PipeException {
		try {
			if ( getElements().size() != 0 ) {
				throw new ComponentExecutionException("en doFetch getElements es != 0, es = " + getElements().size());
			}
			String response = executeRequest(nextRequest);
			logger.log(Level.FINEST, String.format("response = %s", response));
			if ( getResponseUnmarshaller() == null ) {
				throw new ComponentExecutionException("MessagePipe sin unmarshaller configurado" + this);
			}
			ContinuationResponse responseWithRegs = getResponseUnmarshaller().unmarshalResponse(response);
			
			if ( responseWithRegs.getEstado().isTrue()) {
				this.getElements().addAll(responseWithRegs.getRecords());
				this.consumedElements = 0;
			// Si viene false no necesariamente es un error	
			//Cuando AIS no encuentra resultados manda un ER con uno de estos mensajes. Primero chequeo esto y después por ER general 
			} else {
				if ( Constants.NO_SE_ENCONTRARON_DATOS.equalsIgnoreCase(responseWithRegs.getEstado().getMensaje()) || 
						Constants.NO_SE_HAN_ENCONTRADO_RESULTADOS.equalsIgnoreCase(responseWithRegs.getEstado().getMensaje()) ||
						("".equals(responseWithRegs.getEstado().getMensaje()) && responseWithRegs.getEstado().isFalse())) {
					logger.log(Level.FINE, "Source exhausted");
					sourceExhausted = true;
					return null;
				} else if ( Constants.TIMEOUT.equalsIgnoreCase(responseWithRegs.getEstado().getMensaje())) {
					throw new PipeTimeoutException();
				} else if ( Constants.ERROR_INESPERADO.equalsIgnoreCase(responseWithRegs.getEstado().getMensaje())) {
					throw new PipeUnexpectedErrorException();
				} else if ( StringUtils.contains(responseWithRegs.getEstado().getMensaje(), Constants.ERROR_CON_CODIGO) ) {
					throw new PipeException(responseWithRegs.getEstado().getMensaje());
				} else if ( StringUtils.contains(responseWithRegs.getEstado().getMensaje(), Constants.MUY_EXTENSA_KEY) ) {
					throw new PipeTooManyRowsException();
				} else if ( responseWithRegs.getContinuationStatus() != null && responseWithRegs.getContinuationStatus().equals(Constants.PAGINATOR_ER_TAG)) {
					logger.log(Level.WARNING, String.format("Respuesta con CONTINUACION=ER al buscar registros con request [%s] y response [%s]", nextRequest, response));
					throw new PipeException(responseWithRegs.getContinuationStatus() + " : " + responseWithRegs.getEstado().getMensaje());
				} else {
					logger.log(Level.WARNING, String.format("Respuesta en false al buscar registros con request [%s] y response [%s]", nextRequest, response));
					throw new PipeException("Error : " + responseWithRegs.getEstado().getMensaje());
				}
			}
			return responseWithRegs;
		} catch (MalformedURLException e) {
			logger.log(Level.SEVERE,"Ex. " + e.getMessage() + ". Propagando como ComponentExecutionException", e);
			e.printStackTrace();
			throw new ComponentExecutionException(e);
		} catch (OSBConnectorException e) {
			logger.log(Level.SEVERE,"Ex. " + e.getMessage() + ". Propagando como ComponentExecutionException", e);
			e.printStackTrace();
			throw new ComponentExecutionException(e);
		} catch (JAXBException e) {
			logger.log(Level.SEVERE,"Ex. " + e.getMessage() + ". Propagando como ComponentExecutionException", e);
			e.printStackTrace();
			throw new ComponentExecutionException(e);
		}
	}

	/**
	 * Ejecuta el request por el conector OSB configurado
	 * 
	 * @param nextRequest
	 * @return
	 * @throws OSBConnectorException
	 * @throws MalformedURLException
	 * @throws JAXBException
	 */
	protected String executeRequest(CommRequest nextRequest)
			throws OSBConnectorException, MalformedURLException, JAXBException {
		String response = getConnector().executeRequest(getActionCode(), nextRequest.marshall());
		return response;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public ResponseUnmarshaller getResponseUnmarshaller() {
		return responseUnmarshaller;
	}

	public void setResponseUnmarshaller(ResponseUnmarshaller responseUnmarshaller) {
		this.responseUnmarshaller = responseUnmarshaller;
	}

	public boolean isSourceExhausted() {
		return sourceExhausted;
	}

	public void setSourceExhausted(boolean sourceExhausted) {
		this.sourceExhausted = sourceExhausted;
	}
	
	protected PaginateRequest getRequest() {
		return request;
	}

	public void setRequest(PaginateRequest request) {
		this.request = request;
	}

	
}