package com.qbe.services.paginate;

public interface Constants {

	/**
	 *  
	 */
	public static final String PAGINATOR_PREFIX = "PAGINATOR:";
	public static final ContinuationStatus PAGINATOR_OK_TAG = ContinuationStatus.OK;
	public static final ContinuationStatus PAGINATOR_TR_TAG = ContinuationStatus.TR;
	public static final ContinuationStatus PAGINATOR_ER_TAG = ContinuationStatus.ER;
	public static final String NO_SE_ENCONTRARON_DATOS = "No se encontraron datos";
	public static final String TIMEOUT = "El servicio de consulta no se encuentra disponible";
	public static final String CODIGO_ERROR_3 = "Codigo Error:3";
	public static final String NO_SE_HAN_ENCONTRADO_RESULTADOS = "No se han encontrado resultados para la consulta solicitada";
	public static final String ERROR_INESPERADO = "SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA";
	public static final String ERROR_CON_CODIGO = "Codigo de Error:";
	public static final String MUY_EXTENSA_KEY = "realizada es muy extensa, por favor ingrese un dato";
	public static final String ERROR_RESPONSE_TEMPLATE = "<Response><Estado resultado=\"false\" mensaje=\"%s\"/></Response>";
	public static final Object MENSAJE_TOO_MANY_ROWS = "La búsqueda realizada es muy extensa, por favor ingrese un dato más específico y realícela nuevamente.";							
}