package com.qbe.services.paginate.service1109;

import javax.xml.bind.JAXBException;

import com.qbe.services.paginate.ResponseUnmarshaller;
import com.qbe.services.paginate.ContinuationResponse;

public class Response1109Unmarshaller extends ResponseUnmarshaller {

	@Override
	public ContinuationResponse unmarshalResponse(String response) throws JAXBException {
		ContinuationResponse response1109 = (new Response1109()).unmarshal(response);
		return response1109;
	}

}
