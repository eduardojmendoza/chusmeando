package com.qbe.services.paginate.service1428;

import java.io.Serializable;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Registro de respuesta del 1428
 * 
            <RAMOPCOD>AUS1</RAMOPCOD>
            <POLIZANN>00</POLIZANN>
            <POLIZSEC>000001</POLIZSEC>
            <CERTIPOL>0000</CERTIPOL>
            <CERTIANN>0001</CERTIANN>
            <CERTISEC>558576</CERTISEC>
            <SUPLENUM>0004</SUPLENUM>
            <CLIENSEC>600023014</CLIENSEC>
            <CLIENDES>ARIDO , ANA </CLIENDES>
            <SITUCPOL>VIGENTE</SITUCPOL>
            <RAMO>M</RAMO>
            <SINIESTRO>0</SINIESTRO>
            <EXIGIBLE>0</EXIGIBLE>
            <RECIBANN>02</RECIBANN>
            <RECIBTIP>1</RECIBTIP>
            <RECIBSEC>505013</RECIBSEC>
            <SECUENUM>3</SECUENUM>
            <REMESANN/>
            <REMESSEC/>
            <FECSITUE>29/08/2014</FECSITUE>
            <COBROTIPC>DB</COBROTIPC>
            <COBRODESC>NUMERO DE CBU</COBRODESC>
            <COBROCODC>5</COBROCODC>
            <COBRODABC>DEBITO AUTOMATICO BANCO</COBRODABC>
            <MONENCOD>1</MONENCOD>
            <MONENDES>$</MONENDES>
            <IMPORTE>525.57</IMPORTE>
            <ORDENJUDICIAL>N</ORDENJUDICIAL>

 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="CANAL")
public class Canal1428 extends ResponseRecord implements Serializable{
	
	@XmlElement(required=true, name="RAMOPCOD")
	private String ramocod = "";

	@XmlElement(required=true, name="POLIZANN")
	private String polizann = "";

	@XmlElement(required=true, name="POLIZSEC")
	private String polizsec = "";

	@XmlElement(required=true, name="CERTIPOL")
	private String certipol = "";
	
	@XmlElement(required=true, name="CERTIANN")
	private String certiann = "";
	
	@XmlElement(required=true, name="CERTISEC")
	private String certisec = "";
	
	@XmlElement(required=true, name="SUPLENUM")
	private String suplenum = "";
	
	@XmlElement(required=true, name="CLIENSEC")
	private String cliensec = "";
	
	@XmlElement(required=true, name="CLIENDES")
	private String cliendes = "";
	
	@XmlElement(required=true, name="SITUCPOL")
	private String situcpol = "";
	
	@XmlElement(required=true, name="RAMO")
	private String ramo = "";
	
	@XmlElement(required=true, name="SINIESTRO")
	private String siniestro = "";
	
	@XmlElement(required=true, name="EXIGIBLE")
	private String exigible = "";
	
	@XmlElement(required=true, name="RECIBANN")
	private String recibann = "";
	
	@XmlElement(required=true, name="RECIBTIP")
	private String recibtip = "";
	
	@XmlElement(required=true, name="RECIBSEC")
	private String recibsec = "";
	
	@XmlElement(required=true, name="SECUENUM")
	private String secuenum = "";
	
	@XmlElement(required=true, name="REMESANN")
	private String remesann = "";
	
	@XmlElement(required=true, name="REMESSEC")
	private String remessec = "";
	
	@XmlElement(required=true, name="FECSITUE")
	private String fecsitue = "";
	
	@XmlElement(required=true, name="COBROTIPC")
	private String cobrotipc = "";
	
	@XmlElement(required=true, name="COBRODESC")
	private String cobrodesc = "";
	
	@XmlElement(required=true, name="COBROCODC")
	private String cobrocodc = "";
	
	@XmlElement(required=true, name="COBRODABC")
	private String cobrodabc = "";
	
	@XmlElement(required=true, name="MONENCOD")
	private String monencod = "";
	
	@XmlElement(required=true, name="MONENDES")
	private String monendes = "";
	
	@XmlElement(required=true, name="IMPORTE")
	private String importe = "";
	
	@XmlElement(required=true, name="ORDENJUDICIAL")
	private String ordenjudicial = "";
	
	
	/**
	 * Usado para logging en el comparator
	 * @return
	 */
	public String getComparePath() {
		return "#" + getCliendes() + "-" + getPoliza() + "-" + getSuplenum() + "-" + getSitucpol() + "-" + getReceiptID() + "-" + getMonendes() + "-" + getImporte() + "-" + getFecsitue() + "-" + getCobrodabc();
	}
	
	public String getRamocod() {
		return ramocod;
	}

	public void setRamocod(String ramocod) {
		this.ramocod = ramocod;
	}

	public String getPolizann() {
		return polizann;
	}

	public void setPolizann(String polizann) {
		this.polizann = polizann;
	}

	public String getPolizsec() {
		return polizsec;
	}

	public void setPolizsec(String polizsec) {
		this.polizsec = polizsec;
	}

	public String getCertipol() {
		return certipol;
	}

	public void setCertipol(String certipol) {
		this.certipol = certipol;
	}

	public String getCertiann() {
		return certiann;
	}

	public void setCertiann(String certiann) {
		this.certiann = certiann;
	}

	public String getCertisec() {
		return certisec;
	}

	public void setCertisec(String certisec) {
		this.certisec = certisec;
	}

	public String getSuplenum() {
		return suplenum;
	}

	public void setSuplenum(String suplenum) {
		this.suplenum = suplenum;
	}

	public String getCliensec() {
		return cliensec;
	}

	public void setCliensec(String cliensec) {
		this.cliensec = cliensec;
	}

	public String getCliendes() {
		return cliendes;
	}

	public void setCliendes(String cliendes) {
		this.cliendes = cliendes;
	}

	public String getSitucpol() {
		return situcpol;
	}

	public void setSitucpol(String situcpol) {
		this.situcpol = situcpol;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getSiniestro() {
		return siniestro;
	}

	public void setSiniestro(String siniestro) {
		this.siniestro = siniestro;
	}

	public String getExigible() {
		return exigible;
	}

	public void setExigible(String exigible) {
		this.exigible = exigible;
	}

	public String getRecibann() {
		return recibann;
	}

	public void setRecibann(String recibann) {
		this.recibann = recibann;
	}

	public String getRecibtip() {
		return recibtip;
	}

	public void setRecibtip(String recibtip) {
		this.recibtip = recibtip;
	}

	public String getRecibsec() {
		return recibsec;
	}

	public void setRecibsec(String recibsec) {
		this.recibsec = recibsec;
	}

	public String getSecuenum() {
		return secuenum;
	}

	public void setSecuenum(String secuenum) {
		this.secuenum = secuenum;
	}

	public String getRemesann() {
		return remesann;
	}

	public void setRemesann(String remesann) {
		this.remesann = remesann;
	}

	public String getRemessec() {
		return remessec;
	}

	public void setRemessec(String remessec) {
		this.remessec = remessec;
	}

	public String getFecsitue() {
		return fecsitue;
	}

	public void setFecsitue(String fecsitue) {
		this.fecsitue = fecsitue;
	}

	public String getCobrotipc() {
		return cobrotipc;
	}

	public void setCobrotipc(String cobrotipc) {
		this.cobrotipc = cobrotipc;
	}

	public String getCobrodesc() {
		return cobrodesc;
	}

	public void setCobrodesc(String cobrodesc) {
		this.cobrodesc = cobrodesc;
	}

	public String getCobrocodc() {
		return cobrocodc;
	}

	public void setCobrocodc(String cobrocodc) {
		this.cobrocodc = cobrocodc;
	}

	public String getCobrodabc() {
		return cobrodabc;
	}

	public void setCobrodabc(String cobrodabc) {
		this.cobrodabc = cobrodabc;
	}

	public String getMonencod() {
		return monencod;
	}

	public void setMonencod(String monencod) {
		this.monencod = monencod;
	}

	public String getMonendes() {
		return monendes;
	}

	public void setMonendes(String monendes) {
		this.monendes = monendes;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getOrdenjudicial() {
		return ordenjudicial;
	}

	public void setOrdenjudicial(String ordenjudicial) {
		this.ordenjudicial = ordenjudicial;
	}
	
	public String getPoliza(){
		String poliza = getRamocod()+getPolizann()+getCertipol()+getCertiann()+getCertisec();
		return poliza;
	}
	
	public String getReceiptID(){
		String receiptID = getRecibann()+getRecibtip()+getRecibsec();
		return receiptID;
	}


}
