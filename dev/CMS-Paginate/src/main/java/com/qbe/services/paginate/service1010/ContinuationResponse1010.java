package com.qbe.services.paginate.service1010;

import java.io.StringReader;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.MSGESTContinuationResponse;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * ContinuationResponse1010 tiene una lista de ResponseRecords, un estado y un flag que indica 
 * "la continuación", es decir si hay más registros disponibles en una página siguiente
 * 
 * TODO Refactorizar con ContinuationResponse, porque hay aspectos comunes ( esta está hecha a partir 
 * de CR )
 *
 * 	<QRYCONT/>
	<CLICONT/>
	<PRODUCONT>##PAGER##e778f279-0564-46b0-b51f-8da5d0c4df57#202#201##PAGER##</PRODUCONT>
	<POLIZACONT/>

 * @author ramiro
 *
 */
@XmlTransient //Puesto para que no genere los xsi:type en las subclases
public abstract class ContinuationResponse1010 extends MSGESTContinuationResponse {

	@XmlElement(required = true, name = "QRYCONT")
	private String qrycont = "";
	@XmlElement(required = true, name = "CLICONT")
	private String clicont = "";
	@XmlElement(required = true, name = "PRODUCONT")
	private String producont = "";
	@XmlElement(required = true, name = "POLIZACONT")
	private String polizacont = "";
	@XmlElement(required = true, name = "SUPLENUMS")
	private String suplenums = "";
	@XmlElement(required = true, name = "CLIENSECS")
	private String cliensecs = "";
	@XmlElement(required = true, name = "AGENTCLAS")
	private String agentclas = "";
	@XmlElement(required = true, name = "AGENTCODS")
	private String agentcods = "";
	@XmlElement(required = true, name = "NROITEMS")
	private String nroitems = "";


	public ContinuationResponse1010() {
		super();
	}

	public abstract void addRecord(ResponseRecord r);

	public abstract List<? extends ResponseRecord> getRecords();

	public abstract void setSerializedData(String serialize);
	
	public String getPolizacont() {
		return polizacont;
	}

	public void setPolizacont(String polizacont) {
		this.polizacont = polizacont;
	}

	public String getQrycont() {
		return qrycont;
	}

	public void setQrycont(String qrycont) {
		this.qrycont = qrycont;
	}

	public String getProducont() {
		return producont;
	}

	public void setProducont(String producont) {
		this.producont = producont;
	}

	public String getClicont() {
		return clicont;
	}

	public void setClicont(String clicont) {
		this.clicont = clicont;
	}

	public String getSuplenums() {
		return suplenums;
	}

	public void setSuplenums(String suplenums) {
		this.suplenums = suplenums;
	}

	public String getCliensecs() {
		return cliensecs;
	}

	public void setCliensecs(String cliensecs) {
		this.cliensecs = cliensecs;
	}

	public String getAgentclas() {
		return agentclas;
	}

	public void setAgentclas(String agentclas) {
		this.agentclas = agentclas;
	}

	public String getAgentcods() {
		return agentcods;
	}

	public void setAgentcods(String agentcods) {
		this.agentcods = agentcods;
	}

	public String getNroitems() {
		return nroitems;
	}

	public void setNroitems(String nroitems) {
		this.nroitems = nroitems;
	}



}