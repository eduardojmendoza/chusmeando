package com.qbe.services.paginate.service1340;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.UsuarcodNivelclasRequest;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Request del mensaje 1340
 * 
 * 
<DEFINICION>siniestroDenuncia_getListadoClientes.xml</DEFINICION>
<USUARCOD>EX009005L</USUARCOD>
<NIVELCLAS>PR</NIVELCLAS>
<CLIENSECAS>100072343</CLIENSECAS>
<NIVELCLA1/>
<CLIENSEC1/>
<NIVELCLA2/>
<CLIENSEC2/>
<NIVELCLA3/>
<CLIENSEC3/>
<RAMO><![CDATA[]]></RAMO>
<POLIZA><![CDATA[]]></POLIZA>
<CERTIF><![CDATA[]]></CERTIF>
<PATENTE><![CDATA[]]></PATENTE>
<APELLIDO><![CDATA[ARMATI]]></APELLIDO>
<DOCUMTIP/>
<DOCUMDAT><![CDATA[]]></DOCUMDAT>
<FECHASINIE>20130401</FECHASINIE>
<TIPOSINIE>AU</TIPOSINIE>
<SWBUSCA>S</SWBUSCA>
<ORDEN>A</ORDEN>
<COLUMNA>1</COLUMNA>
<NROQRY>0</NROQRY>
<RETOMA/>
<RETORNARREQUEST/>
 * Usamos NROQRY para guardar los datos serializados de continuación
 * 
 * @author gavilan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="Request")
public class Request1340 extends UsuarcodNivelclasRequest {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(required=true, name="DEFINICION")
	private String definicion;

	@XmlElement(required=false, name="CLIENSECAS")
	private String cliensecas;
	
	@XmlElement(required=false, name="NIVELCLA1")
	private String nivelcla1;
	
	@XmlElement(required=false, name="CLIENSECA1")
	private String clienseca1;
	
	@XmlElement(required=false, name="NIVELCLA2")
	private String nivelcla2;
	
	@XmlElement(required=false, name="CLIENSECA2")
	private String clienseca2;
	
	@XmlElement(required=false, name="NIVELCLA3")
	private String nivelcla3;
	
	@XmlElement(required=false, name="CLIENSECA3")
	private String clienseca3;
	
	@XmlElement(required=false, name="RAMO")
	private String ramo;
	
	@XmlElement(required=false, name="POLIZA")
	private String poliza;
	
	@XmlElement(required=false, name="CERTIF")
	private String certif;
	
	@XmlElement(required=false, name="PATENTE")
	private String patente;
	
	@XmlElement(required=false, name="APELLIDO")
	private String apellido;
	
	@XmlElement(required=false, name="DOCUMTIP")
	private String documtip;
	
	@XmlElement(required=false, name="DOCUMDAT")
	private String documdat;
	
	@XmlElement(required=false, name="FECHASINIE")
	private String fechasinie;
	
	@XmlElement(required=false, name="TIPOSINIE")
	private String tiposinie;
	
	@XmlElement(required=false, name="SWBUSCA")
	private String swbusca;
		
	@XmlElement(required=false, name="ORDEN")
	private String orden;

	@XmlElement(required=false, name="COLUMNA")
	private String columna;

	
	//De acá para abajo, los campos que agrega para manejar la continuación
	@XmlElement(required=false, name="RETORNARREQUEST")
	private String retornarrequest = "";

	/**
	 * RETORNARREQUEST Se inicializa a "" para que siempre se genere en el request que va a CMS, 
	 * porque si no ,no vuelve el request que se usa para la continuación aunque haya más registros.
	 * 
	 * Debería ir en la definición, pero no la modificamos por si hay llamadas de OV que no lo 
	 * mandan adrede porque no lo esperan en la respuesta
	 */

	@XmlElement(required=false, name="NROQRY")
	private String nroqry;

	@XmlElement(required=false, name="RETOMA")
	private ContinuationStatus retoma;

	@XmlElement(required=false, name="CIAASCOD")
	private String ciaascod;

	@XmlElement(required=false, name="ESTADO")
	private ContinuationStatus estado;

	@XmlElement(required=false, name="ERROR")
	private String error;
	
	public Request1340() {
	}

	/**
	 * Retorna una deep-copy del receptor
	 * @return
	 */
	public Request1340 copy()  {
		try {
			return (Request1340) unmarshall(this.marshall());
		} catch (JAXBException e) {
			throw new ComponentExecutionException("Exception al copiar un Request", e);
		}
	}
	
	
	/**
	 * El Request1340 es un caso especial de paginado, porque AIS devuelve en la respuesta
	 * el request completo que debemos enviar para obtener la página siguiente
	 */
	@Override
	public PaginateRequest createRequestForNextBlockFromResponse(ContinuationResponse resp) {
		Response1340 resp1340 = (Response1340) resp;
		return resp1340.getCampos().getRequest();
	}

	@Override
	public String getSerializedData() {
		return getNroqry();
	}

	@Override
	public void setSerializedData(String value) {
		this.setNroqry(value);
	}
	
	public String getDefinicion() {
		return definicion;
	}

	public void setDefinicion(String definicion) {
		this.definicion = definicion;
	}

	public String getNivelcla1() {
		return nivelcla1;
	}

	public void setNivelcla1(String nivelcla1) {
		this.nivelcla1 = nivelcla1;
	}

	public String getNivelcla2() {
		return nivelcla2;
	}

	public void setNivelcla2(String nivelcla2) {
		this.nivelcla2 = nivelcla2;
	}

	public String getNivelcla3() {
		return nivelcla3;
	}

	public void setNivelcla3(String nivelcla3) {
		this.nivelcla3 = nivelcla3;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getRetornarrequest() {
		return retornarrequest;
	}

	public void setRetornarrequest(String retornarrequest) {
		this.retornarrequest = retornarrequest;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCliensecas() {
		return cliensecas;
	}

	public void setCliensecas(String cliensecas) {
		this.cliensecas = cliensecas;
	}

	public String getClienseca1() {
		return clienseca1;
	}

	public void setClienseca1(String clienseca1) {
		this.clienseca1 = clienseca1;
	}

	public String getClienseca2() {
		return clienseca2;
	}

	public void setClienseca2(String clienseca2) {
		this.clienseca2 = clienseca2;
	}

	public String getClienseca3() {
		return clienseca3;
	}

	public void setClienseca3(String clienseca3) {
		this.clienseca3 = clienseca3;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public String getCertif() {
		return certif;
	}

	public void setCertif(String certif) {
		this.certif = certif;
	}

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDocumtip() {
		return documtip;
	}

	public void setDocumtip(String documtip) {
		this.documtip = documtip;
	}

	public String getDocumdat() {
		return documdat;
	}

	public void setDocumdat(String documdat) {
		this.documdat = documdat;
	}

	public String getFechasinie() {
		return fechasinie;
	}

	public void setFechasinie(String fechasinie) {
		this.fechasinie = fechasinie;
	}

	public String getTiposinie() {
		return tiposinie;
	}

	public void setTiposinie(String tiposinie) {
		this.tiposinie = tiposinie;
	}

	public String getSwbusca() {
		return swbusca;
	}

	public void setSwbusca(String swbusca) {
		this.swbusca = swbusca;
	}

	public String getOrden() {
		return orden;
	}

	public void setOrden(String orden) {
		this.orden = orden;
	}

	public String getColumna() {
		return columna;
	}

	public void setColumna(String columna) {
		this.columna = columna;
	}

	public String getNroqry() {
		return nroqry;
	}

	public void setNroqry(String nroqry) {
		this.nroqry = nroqry;
	}

	public ContinuationStatus getRetoma() {
		return retoma;
	}

	public void setRetoma(ContinuationStatus retoma) {
		this.retoma = retoma;
	}

	public String getCiaascod() {
		return ciaascod;
	}

	public void setCiaascod(String ciaascod) {
		this.ciaascod = ciaascod;
	}

	public ContinuationStatus getEstado() {
		return estado;
	}

	public void setEstado(ContinuationStatus estado) {
		this.estado = estado;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	
	
}
