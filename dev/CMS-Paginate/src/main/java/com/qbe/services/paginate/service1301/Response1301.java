package com.qbe.services.paginate.service1301;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.MSGESTContinuationResponse;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Response específico del 1301
 * 
 * 
 * <Response>		
	<Estado resultado="true" mensaje=""/>		
	<MSGEST>TR</MSGEST>	
	<CONTINUAR>9530</CONTINUAR>	
	<REGS>	
		<REG>
 * 
 * TODO Necesita refactoring con ContinuationResponse1010
 *
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "Response")
public class Response1301 extends MSGESTContinuationResponse {

	
	@XmlElement(required = false, name = "FECDES")
	private String fecdes;
	@XmlElement(required = false, name = "FECHAS")
	private String fechas;
	@XmlElement(required = true, name = "CONTINUAR")
	private String continuar = "";

	public Response1301() {
		super();
	}

	@XmlElementWrapper(name = "REGS")
	@XmlElement(required = true, name = "REG")
	private List<Reg1301> records = new ArrayList<Reg1301>();

	@Override
	public void setSerializedData(String serialize) {
		this.setContinuar(serialize);
	}

	@Override
	public String getSerializedData() {
		return getContinuar();
	}

	@Override
	public List<? extends ResponseRecord> getRecords() {
		return records;
	}

	public void setRecords(List<Reg1301> records) {
		this.records = records;
	}

	@Override
	public void addRecord(ResponseRecord r) {
		this.records.add((Reg1301)r);
	}

	public String getFecdes() {
		return fecdes;
	}

	public void setFecdes(String fecdes) {
		this.fecdes = fecdes;
	}

	public String getFechas() {
		return fechas;
	}

	public void setFechas(String fechas) {
		this.fechas = fechas;
	}

	public String getContinuar() {
		return continuar;
	}

	public void setContinuar(String continuar) {
		this.continuar = continuar;
	}



}