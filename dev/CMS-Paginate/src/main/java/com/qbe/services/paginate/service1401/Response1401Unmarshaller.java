package com.qbe.services.paginate.service1401;

import javax.xml.bind.JAXBException;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.ResponseUnmarshaller;

public class Response1401Unmarshaller extends ResponseUnmarshaller {

	@Override
	public ContinuationResponse unmarshalResponse(String response) throws JAXBException {
		ContinuationResponse cr = (new Response1401()).unmarshal(response);
		return cr;
	}

}
