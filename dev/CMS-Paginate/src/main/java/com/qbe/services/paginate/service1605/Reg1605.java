package com.qbe.services.paginate.service1605;

import java.io.Serializable;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Registro de respuesta del 1401
 * 
            <REG>
				<RAMOPCOD>AUS1</RAMOPCOD>
				<POL>00000001</POL>
				<CERTIPOL>0000</CERTIPOL>
				<CERTIANN>0001</CERTIANN>
				<CERTISEC>571321</CERTISEC>
				<SUPLENUM>3</SUPLENUM>
				<CLIENSEC>1087332</CLIENSEC>
				<CLIENDES>GARCIA , MARIA EUGENIA </CLIENDES>
				<TOMARIES>PEUGEOT 308 1.6 HDI ALLURE</TOMARIES>
				<SITUCPOL>VIGENTE</SITUCPOL>
				<AGENTCLA>PR</AGENTCLA>
				<AGENTCOD>3233</AGENTCOD>
				<COBRODES>EFE</COBRODES>
				<RAMO>1</RAMO>
				<IMPRIME>S</IMPRIME>
				<EXIGIBLE>1</EXIGIBLE>
				<SINIESTR>N</SINIESTR>
            </REG>
 * 
 * @author Cesar
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="REG")
public class Reg1605 extends ResponseRecord implements Serializable{
	
	@XmlElement(required=true, name="RAMOPCOD")
	private String ramocod = "";

	@XmlElement(required=true, name="POL")
	private String pol = "";

	@XmlElement(required=true, name="CERTIPOL")
	private String certipol = "";

	@XmlElement(required=true, name="CERTIANN")
	private String certiann = "";

	@XmlElement(required=true, name="CERTISEC")
	private String certisec = "";

	@XmlElement(required=true, name="SUPLENUM")
	private String suplenum = "";

	@XmlElement(required=true, name="CLIENSEC")
	private String cliensec = "";

	@XmlElement(required=true, name="CLIENDES")
	private String cliendes = "";

	@XmlElement(required=true, name="TOMARIES")
	private String tomaries = "";

	@XmlElement(required=true, name="SITUCPOL")
	private String situcpol = "";

	@XmlElement(required=true, name="AGENTCLA")
	private String agentcla = "";

	@XmlElement(required=true, name="AGENTCOD")
	private String agentcod = "";

	@XmlElement(required=true, name="COBRODES")
	private String cobrodes = "";

	@XmlElement(required=true, name="RAMO")
	private String ramo = "";

	@XmlElement(required=true, name="IMPRIME")
	private String imprime = "";

	@XmlElement(required=true, name="EXIGIBLE")
	private String exigible = "";

	@XmlElement(required=true, name="SINIESTR")
	private String siniestr = "";


	/**
	 * Usado para logging en el comparator
	 * @return
	 */
	public String getComparePath() {
		return "#" +getCliendes() + "-" + getPoliza();
	}
	
	public String getRamocod() {
		return ramocod;
	}

	public void setRamocod(String ramocod) {
		this.ramocod = ramocod;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getCertipol() {
		return certipol;
	}

	public void setCertipol(String certipol) {
		this.certipol = certipol;
	}

	public String getCertiann() {
		return certiann;
	}

	public void setCertiann(String certiann) {
		this.certiann = certiann;
	}

	public String getCertisec() {
		return certisec;
	}

	public void setCertisec(String certisec) {
		this.certisec = certisec;
	}

	public String getSuplenum() {
		return suplenum;
	}

	public void setSuplenum(String suplenum) {
		this.suplenum = suplenum;
	}

	public String getCliensec() {
		return cliensec;
	}

	public void setCliensec(String cliensec) {
		this.cliensec = cliensec;
	}

	public String getCliendes() {
		return cliendes;
	}

	public void setCliendes(String cliendes) {
		this.cliendes = cliendes;
	}

	public String getTomaries() {
		return tomaries;
	}

	public void setTomaries(String tomaries) {
		this.tomaries = tomaries;
	}

	public String getSitucpol() {
		return situcpol;
	}

	public void setSitucpol(String situcpol) {
		this.situcpol = situcpol;
	}

	public String getAgentcla() {
		return agentcla;
	}

	public void setAgentcla(String agentcla) {
		this.agentcla = agentcla;
	}

	public String getAgentcod() {
		return agentcod;
	}

	public void setAgentcod(String agentcod) {
		this.agentcod = agentcod;
	}

	public String getCobrodes() {
		return cobrodes;
	}

	public void setCobrodes(String cobrodes) {
		this.cobrodes = cobrodes;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getImprime() {
		return imprime;
	}

	public void setImprime(String imprime) {
		this.imprime = imprime;
	}

	public String getExigible() {
		return exigible;
	}

	public void setExigible(String exigible) {
		this.exigible = exigible;
	}

	public String getSiniestr() {
		return siniestr;
	}

	public void setSiniestr(String siniestr) {
		this.siniestr = siniestr;
	}

	public String getPoliza(){
		String poliza = getRamocod()+getPol()+getCertipol()+getCertiann()+getCertisec();
		return poliza;
	}
	
}
