package com.qbe.services.paginate;

import java.io.Serializable;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Un Request que soporta opcionalmente los tags de PaginatedQuery que se envían a Insis
 * 
 * Las subclases deben implementar los métodos para almacenar la serializedData, 
 * usualmente eligiendo un campo de continuación para guardar esto,
 * y un método que arma el request para la siguiente página
 * 
 * @author ramiro
 *
 */
@XmlTransient
public abstract class PaginateRequest extends CommRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Tamaño de la página
	 */
	@XmlElement(required = false, name = "RowCount")
	private Integer rowCount = 10;
	
	/**
	 * Primer registro
	 */
	@XmlElement(required = false, name = "StartRow")
	private Integer startRow = 0;

	public PaginateRequest() {
	}

	/**
	 * Retorna los datos de serialización usados para continuación.
	 * @return
	 */
	public abstract String getSerializedData();

	/**
	 * Graba los datos de serialización usados para continuación.
	 * @return
	 */
	public abstract void setSerializedData(String value);

	public PaginateRequest unmarshall(String Request) throws JAXBException {
		JAXBContext ovContext = JAXBContext.newInstance(this.getClass());
		Unmarshaller um = ovContext.createUnmarshaller();
		StringReader sr = new StringReader(Request);
		Object o = um.unmarshal(sr);
		if (!(o instanceof PaginateRequest)) {
			throw new IllegalArgumentException("No pude unmarshalear a un " + PaginateRequest.class.getName() + ", el resultado es de otra clase");
		}
		PaginateRequest request = (PaginateRequest) o;
		return request;
	}

	/**
	 * Retorna un request para pedir el bloque siguiente al obtenido por este request
	 * 
	 * @param resp Es el resultado de ejecutar este Request, y que toma como base para armar el próximo
	 * @return Un PaginateRequest, o null si no hay más bloques para pedir
	 */
	public PaginateRequest createRequestForNextBlock(ContinuationResponse resp) {
		if ( Constants.PAGINATOR_OK_TAG.equals(resp.getContinuationStatus())) {
			return null;
		}
		return createRequestForNextBlockFromResponse(resp);
	}
	
	/**
	 * Definido por las subclases para completar los datos específicos del nuevo request
	 * 
	 * @param newRequest
	 * @return
	 */
	protected abstract PaginateRequest createRequestForNextBlockFromResponse(ContinuationResponse resp);
	
	public Integer getRowCount() {
		return rowCount;
	}

	public void setRowCount(Integer rowCount) {
		this.rowCount = rowCount;
	}

	public Integer getStartRow() {
		return startRow;
	}

	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}

}