package com.qbe.services.paginate;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.lang.SerializationException;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.vbcompat.base64.Base64Encoding;

public class PaginationStatus implements Serializable{

	private Paginator paginator;
	private int currentPage;

	public static PaginationStatus deserialize(String serializado) throws IOException,
			ClassNotFoundException {
		PaginationStatus status;
				if ( !serializado.startsWith(Constants.PAGINATOR_PREFIX))  {
					throw new SerializationException("Formato inválido, no comienza con " + Constants.PAGINATOR_PREFIX);
				}
				String data = serializado.substring(Constants.PAGINATOR_PREFIX.length());
				ByteArrayInputStream fis = new ByteArrayInputStream(Base64Encoding.decode(data));
				GZIPInputStream gis = new GZIPInputStream(fis);
				ObjectInputStream in = new ObjectInputStream(gis);
				status = (PaginationStatus) in.readObject();
				in.close();
				return status;
			}

	public Paginator getPaginator() {
		return paginator;
	}

	public void setPaginator(Paginator paginator) {
		this.paginator = paginator;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public PaginationStatus() {
		super();
	}

	public String serialize() throws IOException {
		ObjectOutputStream out = null;
		ByteArrayOutputStream fos = new ByteArrayOutputStream(20000);
		GZIPOutputStream gzip = new GZIPOutputStream(fos);
		out = new ObjectOutputStream(gzip);
		out.writeObject(this);
		out.close();
		String serializado = Constants.PAGINATOR_PREFIX + Base64Encoding.encode(fos.toByteArray());
		return serializado;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}