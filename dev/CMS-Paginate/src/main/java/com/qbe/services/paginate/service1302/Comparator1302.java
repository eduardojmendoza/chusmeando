package com.qbe.services.paginate.service1302;

import java.io.Serializable;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Compara dos Reg1302 teniendo en cuenta los criterios de orden definidos en la 
 * especificación del servicio
 * 
 * @author mmoge
 *
 */
public class Comparator1302 implements Comparator<Object>, Serializable {

	private static Logger logger = Logger.getLogger(Comparator1302.class.getName());

	/**
	 * 
	 * 
	 * @param o1
	 * @param o2
	 * @return
	 */
	@Override
    public int compare(Object o1, Object o2) {
    	if ( o1 == null && o2 == null ) {
            	throw new NullPointerException("Ambos elementos son null");              
            }else if(o2 == null) {
                return -1;
            } else if (o1 == null) {
                return 1;
            } else {
            	Reg1302 r1 = (Reg1302)o1;
            	Reg1302 r2 = (Reg1302)o2;
            	
            	logger.log(Level.FINEST, String.format("Comparando:\n%s\n%s", r1.getComparePath(), r2.getComparePath()));

            	int compare = 0;
            	compare = Integer.signum(r1.getClides().compareTo(r2.getClides())); 
            	if ( compare != 0 ) return compare; 
            	
            	compare = Integer.signum(r1.getPoliza().compareTo(r2.getPoliza()));
            	if ( compare != 0 ) return compare;
            	
                return 0;
            }
        }

	}
