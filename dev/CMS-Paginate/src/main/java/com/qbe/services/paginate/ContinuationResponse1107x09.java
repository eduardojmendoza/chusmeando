package com.qbe.services.paginate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public abstract class ContinuationResponse1107x09 extends MSGESTContinuationResponse {

	@XmlElement(required = true, name = "FECCONT")
	private String feccont = "";
	@XmlElement(required = true, name = "PRODUCCONT")
	private String produccont = "";
	@XmlElement(required = true, name = "POLIZACONT")
	private String polizacont = "";
	@XmlElement(required = true, name = "OPECONT")
	private String opecont = "";

	public String getFeccont() {
		return feccont;
	}

	public void setFeccont(String feccont) {
		this.feccont = feccont;
	}

	public String getProduccont() {
		return produccont;
	}

	public void setProduccont(String produccont) {
		this.produccont = produccont;
	}

	public String getPolizacont() {
		return polizacont;
	}

	public void setPolizacont(String polizacont) {
		this.polizacont = polizacont;
	}

	public String getOpecont() {
		return opecont;
	}

	public void setOpecont(String opecont) {
		this.opecont = opecont;
	}


}
