package com.qbe.services.paginate;

import javax.xml.bind.annotation.XmlElement;

public abstract class UsuarcodNivelclasRequest extends PaginateRequest {

	@XmlElement(required = true, name="USUARCOD")
	private String usuarcod = "";

    @XmlElement(required = true, name="NIVELCLAS")	
	private String nivelclas = "";

	public String getUsuario() {
		return getUsuarcod();
	}

	public String getNivelAs() {
		return getNivelclas();
	}

	public String getUsuarcod() {
		return usuarcod;
	}

	public void setUsuarcod(String usuarcod) {
		this.usuarcod = usuarcod;
	}

	public String getNivelclas() {
		return nivelclas;
	}

	public void setNivelclas(String nivelclas) {
		this.nivelclas = nivelclas;
	}

    
}
