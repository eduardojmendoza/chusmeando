package com.qbe.services.paginate;

import java.net.MalformedURLException;
import java.util.List;

import javax.xml.bind.JAXBException;

import com.qbe.services.cms.osbconnector.OSBConnectorException;

public abstract class ActionCode1107x9 extends PaginatedActionCode {

	public ActionCode1107x9() {
		super();
	}

	public List<? extends PaginateRequest> getMulticommRequests(PaginateRequest request)
			throws MalformedURLException, OSBConnectorException, JAXBException {
				List<? extends PaginateRequest> requests = (new MulticommRequestsListResponse<Request1107x9>()).getMulticommRequests(request);
				return requests;
			}

	protected PaginateRequest parseRequestXML(String requestXML) throws JAXBException {
		return (new Request1107x9()).unmarshall(requestXML);
	}

}