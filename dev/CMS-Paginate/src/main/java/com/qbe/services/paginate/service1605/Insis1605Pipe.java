package com.qbe.services.paginate.service1605;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.OSBConnectorLocator;

public class Insis1605Pipe extends InsisPipe {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public OSBConnector getConnector() {
		return OSBConnectorLocator.getInstance().getInsis1605Connector();
	}

	
}
