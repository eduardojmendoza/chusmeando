package com.qbe.services.paginate.service1301;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.UsuarioNivelasRequest;

/**
 * Request del mensaje 1301
 * 
 * 
<USUARIO>EX009005L</USUARIO>
<NIVELAS>PR</NIVELAS>
<CLIENSECAS>100072343</CLIENSECAS>
<NIVEL1/>
<CLIENSEC1/>
<NIVEL2/>
<CLIENSEC2/>
<NIVEL3/>
<CLIENSEC3/>
<FECDES>20130324</FECDES>
<FECHAS>20130424</FECHAS>
<MSGEST/>
<CONTINUAR/>
 * Campos específicos

<FECDES>20130324</FECDES>
<FECHAS>20130424</FECHAS>
<MSGEST/>
<CONTINUAR/>
 * 
 * 
 * Usamos CONTINUAR para guardar los datos serializados de continuación
 * 
 * @author César
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="Request")
public class Request1301 extends UsuarioNivelasRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
    @XmlElement(required=true, name="CONTINUAR")
	private String continuar = "";

	@XmlElement(required=true, name="FECDES")
	private String fecdes = "";

	@XmlElement(required=true, name="FECHAS")
	private String fechas = "";

    @XmlElement(required=true, name="MSGEST")
	private String msgest = "";
	
	public Request1301() {
	}

	@Override
	public String getSerializedData() {
		return getContinuar();
	}

	@Override
	public void setSerializedData(String value) {
		this.setContinuar(value);
	}

	@Override
	public PaginateRequest createRequestForNextBlockFromResponse(ContinuationResponse resp) {
		Request1301 newRequest = (Request1301) SerializationUtils.clone(this);
		Response1301 cr = (Response1301)resp;
		if ( newRequest != null ) {
			newRequest.setContinuar(cr.getContinuar());
		}
		return newRequest;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getContinuar() {
		return continuar;
	}

	public void setContinuar(String continuar) {
		this.continuar = continuar;
	}


	public String getFecdes() {
		return fecdes;
	}


	public void setFecdes(String fecdes) {
		this.fecdes = fecdes;
	}


	public String getFechas() {
		return fechas;
	}


	public void setFechas(String fechas) {
		this.fechas = fechas;
	}


	public String getMsgest() {
		return msgest;
	}


	public void setMsgest(String msgest) {
		this.msgest = msgest;
	}

	
	
}
