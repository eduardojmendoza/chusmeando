package com.qbe.services.paginate.service1406;

import java.io.Serializable;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Registro de respuesta del 1406
 * 
	<RAMO>1</RAMO>
	<PROD>AUS1</PROD>
	<POL>00000001</POL>
	<CERPOL>0000</CERPOL>
	<CERANN>0001</CERANN>
	<CERSEC>375042</CERSEC>
	<FECENV><![CDATA[31/05/2013]]></FECENV>
	<MON><![CDATA[$]]></MON>
	<IMP>684,39</IMP>
	<CLIDES>SIBILIA DIEGO OMAR</CLIDES>
	<COD>PR-9888</COD>
	<SINI>N</SINI>
 * 
 * @author gavilan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="REG")
public class Reg1406 extends ResponseRecord implements Serializable{
	

	@XmlElement(required=true, name="RAMO")
	private String ramo = "";
	
	@XmlElement(required=true, name="PROD")
	private String prod = "";
		
	@XmlElement(required=true, name="POL")
	private String pol = "";
		
	@XmlElement(required=true, name="CERPOL")
	private String cerpol = "";
		
	@XmlElement(required=true, name="CERANN")
	private String cerann = "";

	@XmlElement(required=true, name="CERSEC")
	private String cersec = "";
	
	@XmlElement(required=true, name="FECENV")
	private String fecenv = "";

	@XmlElement(required=true, name="MON")
	private String mon = "";
	
	@XmlElement(required=true, name="IMP")
	private String imp = "";
	
	@XmlElement(required=true, name="CLIDES")
	private String clides = "";
	
	@XmlElement(required=true, name="COD")
	private String cod = "";
	
	@XmlElement(required=true, name="SINI")
	private String sini = "";

	/**
	 * Usado para logging en el comparator
	 * @return
	 */
	public String getComparePath() {
		return "#" +getClides();
	}
	
	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getProd() {
		return prod;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getCerpol() {
		return cerpol;
	}

	public void setCerpol(String cerpol) {
		this.cerpol = cerpol;
	}

	public String getCerann() {
		return cerann;
	}

	public void setCerann(String cerann) {
		this.cerann = cerann;
	}

	public String getCersec() {
		return cersec;
	}

	public void setCersec(String cersec) {
		this.cersec = cersec;
	}

	public String getFecenv() {
		return fecenv;
	}

	public void setFecenv(String fecenv) {
		this.fecenv = fecenv;
	}

	public String getMon() {
		return mon;
	}

	public void setMon(String mon) {
		this.mon = mon;
	}

	public String getImp() {
		return imp;
	}

	public void setImp(String imp) {
		this.imp = imp;
	}

	public String getClides() {
		return clides;
	}

	public void setClides(String clides) {
		this.clides = clides;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getSini() {
		return sini;
	}

	public void setSini(String sini) {
		this.sini = sini;
	}

	
}
