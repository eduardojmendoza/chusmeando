package com.qbe.services.paginate.service1415;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.UsuarioNivelasRequest;

/**
 * Request del mensaje 1415
 * 
 * 
<Request>	
	<USUARIO>EX009005L</USUARIO>
	<NIVELAS>PR</NIVELAS>
	<CLIENSECAS>101862843</CLIENSECAS>
	<FLAGBUSQ>T</FLAGBUSQ>
</Request>
 * 
 * 
 * Campos específicos
 * <CLIENSECAS/>
 * <FLAGBUSQ/>
 * 
 * FIXME Como el request no tiene paginado, agrego CONTFILLER para simplificar la lógica del resto
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="Request")
public class Request1415 extends UsuarioNivelasRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(required=true, name="CLIENSECAS")
	private String cliensecas = "";

	@XmlElement(required=true, name="FLAGBUSQ")
	private String flagbusq = "";

	public Request1415() {
	}

	@Override
	public String getSerializedData() {
		return "";
	}

	@Override
	public void setSerializedData(String value) {
	}

	@Override
	public PaginateRequest createRequestForNextBlockFromResponse(ContinuationResponse resp) {
		return (Request1415) SerializationUtils.clone(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getCliensecas() {
		return cliensecas;
	}

	public void setCliensecas(String cliensecas) {
		this.cliensecas = cliensecas;
	}

	public String getFlagbusq() {
		return flagbusq;
	}

	public void setFlagbusq(String flagbusq) {
		this.flagbusq = flagbusq;
	}

}
