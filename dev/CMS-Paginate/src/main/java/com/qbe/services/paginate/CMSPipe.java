package com.qbe.services.paginate;

import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.cms.osbconnector.OSBConnectorException;

public class CMSPipe extends MessagePipe {

	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(CMSPipe.class.getName());
	
	/**
	 * El request usado para traer el bloque actual
	 */
	protected CommRequest lastBlockRequest;

	/**
	 * El request a usar para traer el próximo bloque
	 */
	protected PaginateRequest nextBlockRequest;
	
	public CMSPipe() {
		super();
	}


	@Override
	public void reset() {
		super.reset();
		lastBlockRequest = null;
		nextBlockRequest = getRequest();
	}

	@Override
	protected CommRequest getNextSourceBlockRequest() {
		return getNextBlockRequest();
	}

	/**
	 * Arma el request para el próximo bloque a partir de la respuesta
	 */
	@Override
	protected void updateFromResponse(ContinuationResponse resp) {
		super.updateFromResponse(resp);
		lastBlockRequest = nextBlockRequest;
		if ( resp == null ) {
			setNextBlockRequest(null);
		} else {
			PaginateRequest nextRequest = getNextBlockRequest().createRequestForNextBlock(resp);
			setNextBlockRequest(nextRequest);
		}
	}

	@Override
	public OSBConnector getConnector() {
		return OSBConnectorLocator.getInstance().getCMSConnector();
	}


	@Override
	public PipePointer currentPointer() {
		CMSPipePointer p = new CMSPipePointer();
		p.setRequest(getRequest());
		p.setLastBlockRequest(lastBlockRequest);
		p.setNextBlockRequest(nextBlockRequest);
		p.setConsumedElements(consumedElements);
		//FIXME Esto subirlo
		p.setSourceExhausted(this.isSourceExhausted());
		return p;
	}

	@Override
	public void seek(PipePointer pp) throws PipeException {
		super.seek(pp);
		CMSPipePointer cpp = (CMSPipePointer)pp;
		setRequest(cpp.getRequest());
		setNextBlockRequest(cpp.getNextBlockRequest());
		lastBlockRequest = cpp.getLastBlockRequest();
		doFetch(lastBlockRequest);
		//Descartar los elementos que ya devolví antes
		logger.log(Level.FINE,"Skipping over: " + cpp.getConsumedElements());
		
		for (int i = 0; i < cpp.getConsumedElements(); i++) {
			this.pop();
		}
	}

	public PaginateRequest getNextBlockRequest() {
		return nextBlockRequest;
	}


	public void setNextBlockRequest(PaginateRequest nextBlockRequest) {
		this.nextBlockRequest = nextBlockRequest;
	}

	@Override
	public void setRequest(PaginateRequest request) {
		super.setRequest(request);
		lastBlockRequest = null;
		nextBlockRequest = request;
		
	}


	/**
	 * Un CMSPipe ejecuta el request instanciando directamente, evita ir hasta OSB para volver a CMS
	 * 
	 */
	@Override
	protected String executeRequest(CommRequest nextRequest) throws OSBConnectorException, MalformedURLException,
			JAXBException {
		// TODO Implementar
		return super.executeRequest(nextRequest);
	}


}