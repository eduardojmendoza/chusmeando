package com.qbe.services.paginate.service1416;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.JAXBException;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MulticommRequestsListResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;

public class OVLOGetRecibos extends PaginatedActionCode {

	protected static final String ACTION_CODE_1416 = "lbaw_OVLOGetRecibos";

	public OVLOGetRecibos() {
		setPageSize(Integer.MAX_VALUE);
	}

	@Override
	protected ContinuationResponse createResponse() {
		return new Response1416();
	}

	@Override
	protected Comparator<Object> createComparator(PaginateRequest request) {
		return new Comparator1416();
	}

	@Override
	protected PaginateRequest parseRequestXML(String requestXML) throws JAXBException {
		return (new Request1416()).unmarshall(requestXML);
	}

	@Override
	public List<Pipe> createPipes(PaginateRequest request) {

		List<Pipe> pipes = new ArrayList<Pipe>();
		pipes.add(createCMSPipe(request, ACTION_CODE_1416, new Response1416Unmarshaller()));
		pipes.addAll(createInsisPipes(request, ACTION_CODE_1416, new Response1416Unmarshaller(), Integer.MAX_VALUE));
		return pipes;
	}

	@Override
	public List<? extends PaginateRequest> getMulticommRequests(PaginateRequest request) throws MalformedURLException,
			OSBConnectorException, JAXBException {
		List<? extends PaginateRequest> requests = (new MulticommRequestsListResponse<Request1416>())
				.getMulticommRequests(request);
		return requests;
	}

	@Override
	public InsisPipe createInsisPipe() {
		return new Insis1416Pipe();
	}

}
