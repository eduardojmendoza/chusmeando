package com.qbe.services.paginate;

public class InsisPipePointer extends PipePointer {

	private int nextRecordIndex;
	
	private PaginateRequest request;

	public InsisPipePointer() {
	}

	public int getNextRecordIndex() {
		return nextRecordIndex;
	}

	public void setNextRecordIndex(int nextRecordIndex) {
		this.nextRecordIndex = nextRecordIndex;
	}

	public PaginateRequest getRequest() {
		return request;
	}

	public void setRequest(PaginateRequest request) {
		this.request = request;
	}

}
