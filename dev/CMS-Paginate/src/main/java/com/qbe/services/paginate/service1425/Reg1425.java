package com.qbe.services.paginate.service1425;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ResponseRecord;

/**
 * Registro de respuesta del 1425
 * 
		<REG>
					<RAMO>1</RAMO>
					<CLIDES>MARIELA RUIS</CLIDES>
					<CLIENSEC>600024001</CLIENSEC>
					<PROD>AUS1</PROD>
					<POL>00000001</POL>
					<CERPOL>0000</CERPOL>
					<CERANN>0001</CERANN>
					<CERSEC>558738</CERSEC>
					<OPERAPOL>3</OPERAPOL>
					<RECNUM>021500824</RECNUM>
					<ESTADO>S</ESTADO>
					<MON>$</MON>
					<SIG/>
					<IMP>1805.79</IMP>
					<IMPCALC>1805.79</IMPCALC>
					<RENDIDO>0</RENDIDO>
					<FECVTO>13/12/2013</FECVTO>
					<AGE>PR-3233</AGE>
		</REG>
 * 
 * @author martin
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="REG")
public class Reg1425 extends ResponseRecord implements Serializable{

	@XmlElement(required=true, name="RAMO")
	private String ramo = "";
	
	@XmlElement(required=true, name="CLIDES")
	private String clides = "";
		
	@XmlElement(required=true, name="CLIENSEC")
	private String cliensec = "";
	
	@XmlElement(required=true, name="PROD")
	private String prod = "";
	
	@XmlElement(required=true, name="POL")
	private String pol = "";
	
	@XmlElement(required=true, name="CERPOL")
	private String cerpol = "";
	
	@XmlElement(required=true, name="CERANN")
	private String cerann = "";
	
	@XmlElement(required=true, name="CERSEC")
	private String cersec = "";
	
	@XmlElement(required=true, name="OPERAPOL")
	private String operapol = "";
	
	@XmlElement(required=true, name="RECNUM")
	private String recnum = "";
	
	@XmlElement(required=true, name="ESTADO")
	private String estado = "";
	
	@XmlElement(required=true, name="MON")
	private String mon = "";
	
	@XmlElement(required=true, name="SIG")
	private String sig = "";
	
	@XmlElement(required=true, name="IMP")
	private String imp = "";
	
	@XmlElement(required=true, name="IMPCALC")
	private String impcalc = "";
	
	@XmlElement(required=true, name="RENDIDO")
	private String rendido = "";
		
	@XmlElement(required=true, name="FECVTO")
	private String fecvto = "";
	
	@XmlElement(required=true, name="AGE")
	private String age = "";
	
	

	public String getClides() {
		return clides;
	}

	public void setClides(String clides) {
		this.clides = clides;
	}

	public String getCliensec() {
		return cliensec;
	}

	public void setCliensec(String cliensec) {
		this.cliensec = cliensec;
	}

	public String getProd() {
		return prod;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getCerpol() {
		return cerpol;
	}

	public void setCerpol(String cerpol) {
		this.cerpol = cerpol;
	}

	public String getCerann() {
		return cerann;
	}

	public void setCerann(String cerann) {
		this.cerann = cerann;
	}

	public String getCersec() {
		return cersec;
	}

	public void setCersec(String cersec) {
		this.cersec = cersec;
	}

	public String getOperapol() {
		return operapol;
	}

	public void setOperapol(String operapol) {
		this.operapol = operapol;
	}

	public String getRecnum() {
		return recnum;
	}

	public void setRecnum(String recnum) {
		this.recnum = recnum;
	}

	public String getMon() {
		return mon;
	}

	public void setMon(String mon) {
		this.mon = mon;
	}

	public String getSig() {
		return sig;
	}

	public void setSig(String sig) {
		this.sig = sig;
	}

	public String getImp() {
		return imp;
	}

	public void setImp(String imp) {
		this.imp = imp;
	}

	public String getImpcalc() {
		return impcalc;
	}

	public void setImpcalc(String impcalc) {
		this.impcalc = impcalc;
	}

	public String getRendido() {
		return rendido;
	}

	public void setRendido(String rendido) {
		this.rendido = rendido;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getFecvto() {
		return fecvto;
	}

	public void setFecvto(String fecvto) {
		this.fecvto = fecvto;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	

}
