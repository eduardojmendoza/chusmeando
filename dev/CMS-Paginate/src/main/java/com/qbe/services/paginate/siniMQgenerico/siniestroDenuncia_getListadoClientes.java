package com.qbe.services.paginate.siniMQgenerico;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MulticommRequestsListResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;
import com.qbe.services.paginate.Response;
import com.qbe.services.paginate.service1340.Comparator1340;
import com.qbe.services.paginate.service1340.Insis1340Pipe;
import com.qbe.services.paginate.service1340.Request1340;
import com.qbe.services.paginate.service1340.Response1340;
import com.qbe.services.paginate.service1340.Response1340Unmarshaller;

public class siniestroDenuncia_getListadoClientes extends PaginatedActionCode {

	/**
	 * El pagesize especificado en la planilla del servicio es 201, pero
	 * AIS devuelve 200 registros.
	 * Dejamos 200 para que no haga 2 requests en el caso de que los primeros 200 sean de AIS
	 */
	public static final int MESSAGE_1340_PAGE_SIZE = 200;
	
	public static final String ACTION_CODE_1340 = "lbaw_siniMQGenerico";
	
	private static Logger logger = Logger.getLogger(siniestroDenuncia_getListadoClientes.class.getName());

	public siniestroDenuncia_getListadoClientes() {
		setPageSize(MESSAGE_1340_PAGE_SIZE);		
	}

	public List<? extends PaginateRequest> getMulticommRequests(PaginateRequest request)
			throws MalformedURLException, OSBConnectorException, JAXBException {
				List<? extends PaginateRequest> requests = (new MulticommRequestsListResponse<Request1340>()).getMulticommRequests(request);
				return requests;
			}

	protected PaginateRequest parseRequestXML(String requestXML) throws JAXBException {
		return (new Request1340()).unmarshall(requestXML);
	}

	protected ContinuationResponse createResponse() {
		return new Response1340();
	}

	protected Comparator<Object> createComparator(PaginateRequest request) {
		return new Comparator1340(request);
	}

	public List<Pipe> createPipes(PaginateRequest request) {

		List<Pipe> pipes = new ArrayList<Pipe>();
		pipes.add(createCMSPipe(request, ACTION_CODE_1340, new Response1340Unmarshaller()));
		pipes.addAll(createInsisPipes(request, ACTION_CODE_1340, new Response1340Unmarshaller(), MESSAGE_1340_PAGE_SIZE));
		return pipes;
	}
	
	
	public InsisPipe createInsisPipe() {
		InsisPipe insis = new Insis1340Pipe();
		return insis;
	}

	
	/**
	 * Acciones custom después de haber armado la respuesta
	 * 
	 * <ul>
	 * 		<li> Elimina StartRow y RowCount ya que son internos del paginado
	 * </ul>
	 * 
	 * @return response en formato correcto (campos especificos del paginado) 
	 */
	@Override
	protected Response afterResponseBuilt(PaginateRequest request, Response response) {
		Request1340 r1340 = (Request1340)request;
		Response1340 resp1340 = (Response1340)response;
		
		if ( resp1340.getRequest() != null ) {
			resp1340.getRequest().setStartRow(null);
			resp1340.getRequest().setRowCount(null);
			
		}
		
		return resp1340;
	
	}
	
}