package com.qbe.services.paginate.service1010;

import javax.xml.bind.JAXBException;

import com.qbe.services.paginate.ResponseUnmarshaller;
import com.qbe.services.paginate.ContinuationResponse;

public class Response1010Unmarshaller extends ResponseUnmarshaller {

	@Override
	public ContinuationResponse unmarshalResponse(String response) throws JAXBException {
		ContinuationResponse cr = new Response1010().unmarshal(response);
		return cr;
	}

}
