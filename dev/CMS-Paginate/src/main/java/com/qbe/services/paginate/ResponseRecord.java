package com.qbe.services.paginate;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.vbcompat.framework.ComponentExecutionException;

@XmlTransient //Puesto para que no genere los xsi:type en las subclases
public abstract class ResponseRecord {

	public ResponseRecord() {
		super();
	}

	public ResponseRecord unmarshal(String record) throws JAXBException {
		JAXBContext ovContext = JAXBContext.newInstance(this.getClass());
		Unmarshaller um = ovContext.createUnmarshaller();
		StringReader sr = new StringReader(record);
		Object o = um.unmarshal(sr);
		if (!(o instanceof ResponseRecord)) {
			throw new ComponentExecutionException("No pude unmarshalear a un " + this.getClass().getName() + ", el resultado es de otra clase");
		}
		ResponseRecord rr = (ResponseRecord) o;
		return rr;
	}

	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	
}