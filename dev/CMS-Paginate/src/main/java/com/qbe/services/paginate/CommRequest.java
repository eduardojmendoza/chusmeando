package com.qbe.services.paginate;

import java.io.Serializable;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Un CommRequest define las propiedades comunes a los requests de mensajes que soportan "estructura comercial de O", 
 * esto es mensajes donde se especifican los distintos niveles/cliensecs
 * 
 * Los campos que pueden venir de dos formas ( USUARIO/USUARCOD y NIVELCLAS/NIVELAS ) no se pueden maper ni con un XmlElements
 * porque funciona el unmarshal pero al hacer el marshal no está determinado cual va a elegir, con lo cual cuando este Request se marshalea
 * para enviarlo a CMS, lo puede generar cambiando USUARIO por USUARCOD por ej.
 * 
 * Usar las subclases específicas
 * 
 * Ver
 * 	http://blog.bdoughan.com/2010/10/jaxb-and-xsd-choice-xmlelements.html
 *  y https://bugs.eclipse.org/328646
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
//Está puesto principalmente para poder hacer tests sobre esta clase. Si pongo Request, se pisa con las subclases
@XmlRootElement(name="CommRequest")
public abstract class CommRequest implements Serializable {

	@XmlElement(required = true, name = "CLIENSECAS")
	private String clienSecAs = "";

	@XmlElement(required = false, name = "NIVEL1")
	private String nivel1 = "";

	@XmlElement(required = false, name = "CLIENSEC1")
	private String clienSec1 = "";

	@XmlElement(required = false, name = "NIVEL2")
	private String nivel2 = "";

	@XmlElement(required = false, name = "CLIENSEC2")
	private String clienSec2 = "";

	@XmlElement(required = false, name = "NIVEL3")
	private String nivel3 = "";

	@XmlElement(required = false, name = "CLIENSEC3")
	private String clienSec3 = "";

	public CommRequest() {
		super();
	}

	public String marshall() throws JAXBException {
		return GenericMarshaller.marshal(this);
	}

	/**
	 * Retorna si el request tiene especificado filtros de estructura comercial
	 * @return
	 */
	public boolean isMulticomm() {

		return (!StringUtils.isEmpty(getUsuario()) || !StringUtils.isEmpty(getUsuario()) ) &&
				StringUtils.isEmpty(getNivelAs()) &&
				StringUtils.isEmpty(getNivel1()) &&
				StringUtils.isEmpty(getNivel2()) &&
				StringUtils.isEmpty(getNivel3()) &&
				StringUtils.isEmpty(getClienSecAs()) &&
				StringUtils.isEmpty(getClienSec1()) &&
				StringUtils.isEmpty(getClienSec2()) &&
				StringUtils.isEmpty(getClienSec3())
				;
	}

	public abstract String getUsuario();

	public abstract String getNivelAs();
	
	public String getClienSecAs() {
		return clienSecAs;
	}

	public void setClienSecAs(String clienSecAs) {
		this.clienSecAs = clienSecAs;
	}

	public String getNivel1() {
		return nivel1;
	}

	public void setNivel1(String nivel1) {
		this.nivel1 = nivel1;
	}

	public String getClienSec1() {
		return clienSec1;
	}

	public void setClienSec1(String clienSec1) {
		this.clienSec1 = clienSec1;
	}

	public String getNivel2() {
		return nivel2;
	}

	public void setNivel2(String nivel2) {
		this.nivel2 = nivel2;
	}

	public String getClienSec2() {
		return clienSec2;
	}

	public void setClienSec2(String clienSec2) {
		this.clienSec2 = clienSec2;
	}

	public String getNivel3() {
		return nivel3;
	}

	public void setNivel3(String nivel3) {
		this.nivel3 = nivel3;
	}

	public String getClienSec3() {
		return clienSec3;
	}

	public void setClienSec3(String clienSec3) {
		this.clienSec3 = clienSec3;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
}