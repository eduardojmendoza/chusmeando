package com.qbe.services.paginate.service1109;

import java.io.Serializable;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * 
 * 
				<REG>
					<PROD>AUA1</PROD>
					<POL>00797763</POL>
					<CERPOL>0000</CERPOL>
					<CERANN>0000</CERANN>
					<CERSEC>000000</CERSEC>
					<SUPLENUM>0000</SUPLENUM>
					<OPERAPOL>4</OPERAPOL>
					<CLIDES><![CDATA[NAKAMA S.A.]]></CLIDES>
					<FECVIGDES><![CDATA[01/08/2012]]></FECVIGDES>
					<FECVIGHAS><![CDATA[01/05/2013]]></FECVIGHAS>
					<MOTIV><![CDATA[MODIFICACION PATENTE]]></MOTIV>
					<FECEMI><![CDATA[11/03/2013]]></FECEMI>
					<PRIMA>0,00</PRIMA>
					<PRECIO>0,00</PRECIO>
					<COD>PR-3233</COD>
					<RAMO>2</RAMO>
				</REG>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="REG")
public class Reg1109 extends ResponseRecord implements Serializable{
	
	@XmlElement(required=true, name="PROD")
	private String prod = "";

	@XmlElement(required=true, name="POL")
	private String pol = "";

	@XmlElement(required=true, name="CERPOL")
	private String cerpol = "";

	@XmlElement(required=true, name="CERANN")
	private String cerann = "";

	@XmlElement(required=true, name="CERSEC")
	private String cersec = "";

	@XmlElement(required=true, name="SUPLENUM")
	private String suplenum = "";

	@XmlElement(required=true, name="OPERAPOL")
	private String operapol = "";

	@XmlElement(required=true, name="CLIDES")
	private String clides = "";

	@XmlElement(required=true, name="FECVIGDES")
	private String fecvigdes = "";

	@XmlElement(required=true, name="FECVIGHAS")
	private String fecvighas = "";

	@XmlElement(required=true, name="MOTIV")
	private String motiv = "";

	@XmlElement(required=true, name="FECEMI")
	private String fecemi = "";

	@XmlElement(required=true, name="PRIMA")
	private String prima = "";

	@XmlElement(required=true, name="PRECIO")
	private String precio = "";

	@XmlElement(required=true, name="COD")
	private String cod = "";

	@XmlElement(required=true, name="RAMO")
	private String ramo = "";

	public String getProd() {
		return prod;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getCerpol() {
		return cerpol;
	}

	public void setCerpol(String cerpol) {
		this.cerpol = cerpol;
	}

	public String getCerann() {
		return cerann;
	}

	public void setCerann(String cerann) {
		this.cerann = cerann;
	}

	public String getCersec() {
		return cersec;
	}

	public void setCersec(String cersec) {
		this.cersec = cersec;
	}

	public String getSuplenum() {
		return suplenum;
	}

	public void setSuplenum(String suplenum) {
		this.suplenum = suplenum;
	}

	public String getOperapol() {
		return operapol;
	}

	public void setOperapol(String operapol) {
		this.operapol = operapol;
	}

	public String getClides() {
		return clides;
	}

	public void setClides(String clides) {
		this.clides = clides;
	}

	public String getFecvigdes() {
		return fecvigdes;
	}

	public void setFecvigdes(String fecvigdes) {
		this.fecvigdes = fecvigdes;
	}

	public String getFecvighas() {
		return fecvighas;
	}

	public void setFecvighas(String fecvighas) {
		this.fecvighas = fecvighas;
	}

	public String getMotiv() {
		return motiv;
	}

	public void setMotiv(String motiv) {
		this.motiv = motiv;
	}

	public String getFecemi() {
		return fecemi;
	}

	public void setFecemi(String fecemi) {
		this.fecemi = fecemi;
	}

	public String getPrima() {
		return prima;
	}

	public void setPrima(String prima) {
		this.prima = prima;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getPolicyNumber() {
		return "#" + getProd() + "-" + getPol() + "-" + getCerpol() + "-" + getCerann() + "-" + getCersec() + "-" + getOperapol();
	}
	
}
