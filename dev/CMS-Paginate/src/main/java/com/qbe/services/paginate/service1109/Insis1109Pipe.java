package com.qbe.services.paginate.service1109;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.OSBConnectorLocator;

public class Insis1109Pipe extends InsisPipe {

	@Override
	public OSBConnector getConnector() {
		return OSBConnectorLocator.getInstance().getInsis1109Connector();
	}

	
}
