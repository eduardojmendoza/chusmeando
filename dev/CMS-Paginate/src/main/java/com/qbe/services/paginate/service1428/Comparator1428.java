package com.qbe.services.paginate.service1428;

import java.io.Serializable;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.paginate.PaginateRequest;

/**
 * Compara dos Reg1404 teniendo en cuenta los criterios de orden definidos en la 
 * especificación del servicio
 * 
 * @author ramiro
 *
 */
public class Comparator1428 implements Comparator<Object>, Serializable {

	private static Logger logger = Logger.getLogger(Comparator1428.class.getName());

	protected Request1428 request;
	
	public Comparator1428() {
	}
	
	public Comparator1428(PaginateRequest request) {
		this.request = (Request1428)request;
	}

	/**
	 * 
	 * 
	 * @param o1
	 * @param o2
	 * @return
	 */
	@Override
    public int compare(Object o1, Object o2) {
    	if ( o1 == null && o2 == null ) {
            	throw new NullPointerException("Ambos elementos son null");              
            }else if(o2 == null) {
                return -1;
            } else if (o1 == null) {
                return 1;
            } else {
            	Canal1428 r1 = (Canal1428)o1;
            	Canal1428 r2 = (Canal1428)o2;

            	int orden = -1;

            	if ( request.getColumnaOrden() != null && request.getColumnaOrden() != "" ) {
            		orden = Integer.parseInt(request.getColumnaOrden());
            	}

            	logger.log(Level.FINEST, String.format("Comparando [ orden = %d]:\n%s\n%s", orden, r1.getComparePath(), r2.getComparePath()));
            		
        		switch (orden) {
				case 1:
					return Integer.signum(r1.getCliendes().compareTo(r2.getCliendes()));
				case 2:
					return Integer.signum(r1.getPoliza().compareTo(r2.getPoliza()));
				case 3:
					return Integer.signum(r1.getSuplenum().compareTo(r2.getSuplenum()));
				case 4:
					return Integer.signum(r1.getSitucpol().compareTo(r2.getSitucpol()));
				case 5:
				case 6:
					return Integer.signum(r1.getReceiptID().compareTo(r2.getReceiptID())); 
				case 7:
					return Integer.signum(r1.getMonendes().compareTo(r2.getMonendes()));							
				case 8:
					return Integer.signum(r1.getImporte().compareTo(r2.getImporte()));							
				case 9:
					return Integer.signum(r1.getFecsitue().compareTo(r2.getFecsitue()));							
				case 10:
					return Integer.signum(r1.getCobrodabc().compareTo(r2.getCobrodabc()));							
				default:
					return 0;
				}
            }
        }

	}
