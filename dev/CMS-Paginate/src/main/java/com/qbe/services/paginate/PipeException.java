package com.qbe.services.paginate;

public class PipeException extends Exception {

	public PipeException() {
	}

	public PipeException(String message) {
		super(message);
	}

	public PipeException(Throwable cause) {
		super(cause);
	}

	public PipeException(String message, Throwable cause) {
		super(message, cause);
	}

}
