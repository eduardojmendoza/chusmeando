package com.qbe.services.paginate.service1605;

import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.GenericMarshaller;
import com.qbe.services.paginate.MSGESTContinuationResponse;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Response específico del 1605
 * 
 * 
 *  <Response>
          <Estado resultado="true" mensaje=""/>
          <MSGEST>TR</MSGEST>
          <NROCONS/>
          <REGS>
            <REG>
          <Request> Es opcional
 * 
 * TODO Necesita refactoring con ContinuationResponse1010
 *
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "Response")
public class Response1605 extends MSGESTContinuationResponse {

	@XmlElement(required=true, name="NROQRYS")
	private String nroqrys = "";
	
	@XmlElementWrapper(name = "REGS")
	@XmlElement(required = true, name = "REG")
	private List<Reg1605> records = new ArrayList<Reg1605>();

	@XmlElement(required=false, name="Request")
	private Request1605 request;
	
	public Response1605() {
		super();
	}

	@Override
	public void setSerializedData(String serialize) {
		this.setNroqrys(serialize);
	}

	@Override
	public String getSerializedData() {
		return getNroqrys();
	}

	@Override
	public List<? extends ResponseRecord> getRecords() {
		return records;
	}

	public void setRecords(List<Reg1605> records) {
		this.records = records;
	}

	@Override
	public void addRecord(ResponseRecord r) {
		this.records.add((Reg1605)r);
	}
	
	public String getNroqrys() {
		return nroqrys;
	}

	public void setNroqrys(String nroqrys) {
		this.nroqrys = nroqrys;
	}


	public Request1605 getRequest() {
		return request;
	}

	public void setRequest(Request1605 request) {
		this.request = request;
	}

}