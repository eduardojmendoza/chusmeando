package com.qbe.services.paginate;

import javax.xml.bind.annotation.XmlElement;

/**
 * Implementa el flag de continuación con el campo MSGEST
 * 
 * @author ramiro
 *
 */
public abstract class MSGESTContinuationResponse extends ContinuationResponse {

	@XmlElement(required = true, name = "MSGEST")
	private ContinuationStatus msgest;


	public ContinuationStatus getContinuationStatus() {
		return msgest;
	}

	public void setContinuationstatus(ContinuationStatus msgest) {
		this.msgest = msgest;
	}


}
