/**
 * 
 */
package com.qbe.services.paginate;

import org.apache.commons.lang.SerializationUtils;

import com.qbe.services.cms.osbconnector.OSBConnector;

/**
 * Un pipe que trae datos de Insis.
 * Va pidiendo los bloques usando la lógica de paginado de Insis
 * 
 * @author ramiro
 *
 */
public abstract class InsisPipe extends MessagePipe {

	private static final long serialVersionUID = 1L;

	/**
	 * Tamaño de página default a pedir a insis
	 * 
	 */
	public static final int DEFAULT_ROW_COUNT = 50;

	/**
	 * Tamaño de página efectivo
	 */
	protected int rowCount = DEFAULT_ROW_COUNT;
	
	/**
	 * Índice del primer registro según está definida en el paginado de Insis
	 * La implementación actual es 1-based
	 */
	private static final int INSIS_FIRST_ROW = 1;

	private static final int FIRST_RECORD = 0;

	/**
	 * Índice del próximo registro a devolver por el pipe
	 */
	private int nextRecordIndex = FIRST_RECORD;
	
	/**
	 * 
	 */
	public InsisPipe() {
	}

	/**
	 * En el caso de las paginated queries de Insis usamos el mismo request con los parámetros de paginado ajustados
	 */
	@Override
	protected CommRequest getNextSourceBlockRequest() {
		//FIXME no uso el resultSetId que devuelve Insis y que aceleraría las búsquedas
		PaginateRequest nextPageRequest = (PaginateRequest) SerializationUtils.clone(getRequest());
		// Pido uno más así el hasNext() me devuelve true sin hacer refetch en el 
		// caso de que haya más registros y haya consumido todos los de este bloque
		nextPageRequest.setRowCount(rowCount + 1);
		nextPageRequest.setStartRow(INSIS_FIRST_ROW + nextRecordIndex);
		return nextPageRequest;
	}

	@Override
	protected void updateFromResponse(ContinuationResponse resp) {
		super.updateFromResponse(resp);
		if ( resp != null && resp.getEstado() != null && resp.getEstado().isTrue() && 
				( resp.getRecords().size() < this.rowCount )) {
			setSourceExhausted(true);
		}
	}

	@Override
	public Object pop() throws PipeException {
		this.nextRecordIndex++;
		return super.pop();
	}
	
	/** 
	 * @see com.qbe.services.paginate.Pipe#reset()
	 */
	@Override
	public void reset() {
		super.reset();
		this.nextRecordIndex = FIRST_RECORD;
	}

	@Override
	public void seek(PipePointer pp) throws PipeException{
		this.reset();
		super.seek(pp);
		InsisPipePointer ipp = (InsisPipePointer)pp;
		this.nextRecordIndex = ipp.getNextRecordIndex();
		setRequest(ipp.getRequest());
	}

	@Override
	public PipePointer currentPointer() {
		InsisPipePointer p = new InsisPipePointer();
		p.setRequest(getRequest());
		p.setNextRecordIndex(nextRecordIndex);
		//FIXME Esto subirlo

		//Si quedaron elementos colgados, fuerzo que traiga de nuevo
		if ( this.getElements().size() > 0) {
			p.setSourceExhausted(false);
		} else {
			p.setSourceExhausted(this.isSourceExhausted());
		}
		return p;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

}
