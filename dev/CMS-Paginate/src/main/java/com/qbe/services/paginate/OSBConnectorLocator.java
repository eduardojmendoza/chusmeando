package com.qbe.services.paginate;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;

import com.qbe.services.cms.osbconnector.OSBConnector;

/**
 * Un ServiceLocator que maneja los conectores a OSB
 * Cada conector apunta a un Proxy publicado como WebService en OSB
 * 
 * @author ramiro
 *
 */
public class OSBConnectorLocator implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(OSBConnectorLocator.class.getName());

	public static final String OSB_INTEGRADO_BEAN = "osbIntegrado";
	
	public static final String OSB_DERECHO_CMS_BEAN = "osbDerechoCMS";
	
	public static final String OSB_INSIS_1109_BEAN = "osbDerechoInsis1109";
	
	public static final String OSB_INSIS_1107_BEAN = "osbDerechoInsis1107";

	public static final String OSB_INSIS_1010_BEAN = "osbDerechoInsis1010";

	public static final String OSB_INSIS_1301_BEAN = "osbDerechoInsis1301";
	
	public static final String OSB_INSIS_1404_BEAN = "osbDerechoInsis1404";
	
	public static final String OSB_INSIS_1428_BEAN = "osbDerechoInsis1428";

	public static final String OSB_INSIS_1401_BEAN = "osbDerechoInsis1401";
	
	public static final String OSB_INSIS_1410_BEAN = "osbDerechoInsis1410";
	
	public static final String OSB_INSIS_1415_BEAN = "osbDerechoInsis1415";
	
	public static final String OSB_INSIS_1416_BEAN = "osbDerechoInsis1416";
	
	public static final String OSB_INSIS_1406_BEAN = "osbDerechoInsis1406";
	
	public static final String OSB_INSIS_1302_BEAN = "osbDerechoInsis1302";
	
	public static final String OSB_INSIS_1605_BEAN = "osbDerechoInsis1605";

	public static final String OSB_INSIS_1340_BEAN = "osbDerechoInsis1340";
	
	public static final String OSB_INSIS_1425_BEAN = "osbDerechoInsis1425";

	public static final String MULTICOMM_FAN = "multicommFan";

	protected static ApplicationContext context; 

	protected synchronized static ApplicationContext getContext() {

		if ( context == null) {
			context = new ClassPathXmlApplicationContext("classpath*:spring-beans-pipe.xml");
		}
		return context;
	}

	/**
	 * Hecho público para que se pueda setear desde los tests
	 */
	public static OSBConnectorLocator instance; 

	public static OSBConnectorLocator getInstance() {
		if ( instance == null ) {
			instance = new OSBConnectorLocator();
		}
		return instance;
	}
	
	private OSBConnectorLocator() {
	}
	
	public OSBConnector getMulticommFan() {
		return (OSBConnector)getContext().getBean(MULTICOMM_FAN);
	}
		
	public OSBConnector getCMSConnector() {
		logger.log(Level.INFO, "getCMSConnector");
		return (OSBConnector)getContext().getBean(OSB_DERECHO_CMS_BEAN);
	}

	public OSBConnector getInsis1109Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1109_BEAN);
	}

	public OSBConnector getInsis1107Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1107_BEAN);
	}

	public OSBConnector getInsis1010Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1010_BEAN);
	}
	
	public OSBConnector getInsis1301Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1301_BEAN);
	}
	
	public OSBConnector getInsis1404Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1404_BEAN);
	}
	
	public OSBConnector getInsis1428Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1428_BEAN);
	}
	
	public OSBConnector getInsis1401Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1401_BEAN);
	}
	
	public OSBConnector getInsis1410Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1410_BEAN);
	}
	
	public OSBConnector getInsis1415Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1415_BEAN);
	}
	
	public OSBConnector getInsis1416Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1416_BEAN);
	}
	
	public OSBConnector getInsis1406Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1406_BEAN);
	}
	
	public OSBConnector getInsis1302Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1302_BEAN);
	}
	
	public OSBConnector getInsis1605Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1605_BEAN);
	}
	
	public OSBConnector getInsis1340Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1340_BEAN);
	}
	
	public OSBConnector getInsis1425Connector() {
		return (OSBConnector)getContext().getBean(OSB_INSIS_1425_BEAN);
	}
}
