package com.qbe.services.paginate.service1340;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.Constants;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.ContinuationStatus;
import com.qbe.services.paginate.GenericMarshaller;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Response específico del 1340
 * 
 * El formato de XML que tenemos que representar es un delirio, tiene una estructura 
 * clásica con una colección de ITEMS wrappeados en un tag CAMPOS, pero después del
 * último ITEM manda un Request.
 * 
 * No puedo usar la anotación de XmlElementWrapper así que tengo que separar el Campos
 *  en su clase ( ResponseCampos1340 )
 * 
 * <Response>		
	<Estado resultado="true" mensaje=""/>	
	<CAMPOS>	
		<CANT> <-- tengo que agregar este tag con la cantidad de registros
		<ITEMS>
			<ITEM></ITEM>
		<Request> <-- request para el proximo llamado.
	<CAMPOS>
 * 
 * @author gavilan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "Response")
public class Response1340 extends ContinuationResponse {

	@XmlElement(name = "CAMPOS")
	ResponseCampos1340 campos = new ResponseCampos1340();
	
	public Response1340() {
		super();
	}

	/**
	 * Tomar el originatingRequest como base para el que devuelve dentro de CAMPOS
	 */
	@Override
	public void originatingRequest(PaginateRequest request) {
		Request1340 r1340 = (Request1340)request;
		getCampos().setRequest(r1340);
	}

	@Override
	public void setSerializedData(String serialize) {
		getCampos().setNroqry(serialize);
	}
	
	@Override
	public String getSerializedData() {
		return getCampos().getNroqry();
	}
	

	@Override
	public List<? extends ResponseRecord> getRecords() {
		return getCampos().getRecords();
	}

	public void setRecords(List<Item1340> records) {
		getCampos().setRecords(records);
	}

	@Override
	public void addRecord(ResponseRecord r) {
		getCampos().getItems().getRecords().add((Item1340)r);
		getCampos().setCant(getCampos().getCant()+1); // llevo la cuenta de cada registro agregado
	}

	@Override
	public ContinuationStatus getContinuationStatus() {
		return getCampos().getEstado() == null ? Constants.PAGINATOR_OK_TAG : getCampos().getEstado();
	}

	@Override
	public void setContinuationstatus(ContinuationStatus msgest) {
		getCampos().setEstado(msgest);
	}

	public Request1340 getRequest() {
		return getCampos().getRequest();
	}

	public String getNroqry() {
		return getCampos().getNroqry();
	}


	public ResponseCampos1340 getCampos() {
		return campos;
	}


	public void setCampos(ResponseCampos1340 campos) {
		this.campos = campos;
	}
}