package com.qbe.services.paginate.mqgestion;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MulticommRequestsListResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;
import com.qbe.services.paginate.service1425.Comparator1425;
import com.qbe.services.paginate.service1425.Insis1425Pipe;
import com.qbe.services.paginate.service1425.Request1425;
import com.qbe.services.paginate.service1425.Response1425;
import com.qbe.services.paginate.service1425.Response1425Unmarshaller;

public class getRecibosNotasDeCredito extends PaginatedActionCode {

	
	public static final int MESSAGE_1425_PAGE_SIZE = 200;
	
	protected static final String ACTION_CODE_1425 = "lbaw_GetConsultaMQGestion";
	
	private static Logger logger = Logger.getLogger(getRecibosNotasDeCredito.class.getName());

	public getRecibosNotasDeCredito() {
		setPageSize(MESSAGE_1425_PAGE_SIZE);		
	}

	public List<? extends PaginateRequest> getMulticommRequests(PaginateRequest request)
			throws MalformedURLException, OSBConnectorException, JAXBException {
				List<? extends PaginateRequest> requests = (new MulticommRequestsListResponse<Request1425>()).getMulticommRequests(request);
				return requests;
			}

	protected PaginateRequest parseRequestXML(String requestXML) throws JAXBException {
		return (new Request1425()).unmarshall(requestXML);
	}

	protected ContinuationResponse createResponse() {
		return new Response1425();
	}

	protected Comparator<Object> createComparator(PaginateRequest request) {
		return new Comparator1425();
	}

	public List<Pipe> createPipes(PaginateRequest request) {

		List<Pipe> pipes = new ArrayList<Pipe>();
		pipes.add(createCMSPipe(request, ACTION_CODE_1425, new Response1425Unmarshaller()));
		pipes.addAll(createInsisPipes(request, ACTION_CODE_1425, new Response1425Unmarshaller(), MESSAGE_1425_PAGE_SIZE));
		return pipes;
	}
	
	public InsisPipe createInsisPipe() {
		InsisPipe insis = new Insis1425Pipe();
		return insis;
	}
}