package com.qbe.services.paginate.service1109;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ContinuationResponse1107x09;
import com.qbe.services.paginate.ResponseRecord;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 *
 * 
		<Response>
			<Estado resultado='true' mensaje='' />
			<FECCONT>00431454</FECCONT>
			<PRODUCCONT>AUS1</PRODUCCONT>
			<POLIZACONT>0000000100000001383028</POLIZACONT>
			<OPECONT>000000000139226182</OPECONT>
			<MSGEST>TR</MSGEST>
			<REGS>
				<REG>
					<PROD>AUA1</PROD>
					<POL>00797763</POL>
					<CERPOL>0000</CERPOL>
					<CERANN>0000</CERANN>
					<CERSEC>000000</CERSEC>
					<SUPLENUM>0000</SUPLENUM>
					<OPERAPOL>4</OPERAPOL>
					<CLIDES><![CDATA[NAKAMA S.A.]]></CLIDES>
					<FECVIGDES><![CDATA[01/08/2012]]></FECVIGDES>
					<FECVIGHAS><![CDATA[01/05/2013]]></FECVIGHAS>
					<MOTIV><![CDATA[MODIFICACION PATENTE]]></MOTIV>
					<FECEMI><![CDATA[11/03/2013]]></FECEMI>
					<PRIMA>0,00</PRIMA>
					<PRECIO>0,00</PRECIO>
					<COD>PR-3233</COD>
					<RAMO>2</RAMO>
				</REG>
 
 * 
 * @author ramiro
 *
 */


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "Response")
public class Response1109 extends ContinuationResponse1107x09{

	@XmlElementWrapper(name = "REGS")
	@XmlElement(required = true, name = "REG")
	private List<Reg1109> records = new ArrayList<Reg1109>();

	public Response1109() {
	}

	
	@Override
	protected JAXBContext createMarshalContext() throws JAXBException {
		return JAXBContext.newInstance(this.getClass(), Reg1109.class);
	}

	@Override
	public void setSerializedData(String serialize) {
		this.setOpecont(serialize);
	}

	@Override
	public String getSerializedData() {
		return getOpecont();
	}

	@Override
	public List<? extends ResponseRecord> getRecords() {
		return records;
	}

	public void setRecords(List<Reg1109> records) {
		this.records = records;
	}

	@Override
	public void addRecord(ResponseRecord r) {
		this.records.add((Reg1109)r);
	}

	
}
