package com.qbe.services.paginate;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.framework.jaxb.Estado;
import com.qbe.vbcompat.string.StringHolder;

public abstract class PaginatedActionCode implements VBObjectClass {

	private static final int DEFAULT_PAGE_SIZE = 100;

	private static Logger logger = Logger.getLogger(PaginatedActionCode.class.getName());

	//FIXME Arreglar cuando refactorice las subclases, así puedo meter pipes mockeados más elegantemente
	//El transient no tiene efecto, está como marcador de que no va acá realmente
	private transient List<Pipe> testPipes;

	//FIXME Está para poder acceder al último paginator desde los tests
	//El transient no tiene efecto, está como marcador de que no va acá realmente
	public transient Paginator lastPaginator;
	
	
	/**
	 * 
	 */
	private int pageSize = DEFAULT_PAGE_SIZE;
	
	public PaginatedActionCode() {
		super();
	}

	@Override
	public int IAction_Execute(String requestXML, StringHolder responseSH, String ContextInfo) {
	
		try {
			PaginateRequest request = parseRequestXML(requestXML);
	
			logger.log(Level.FINEST, String.format("recibido [%s]", request));
			Paginator paginator;
			int currentPage = -1;
			if ( request.getSerializedData() == null || !request.getSerializedData().startsWith(Constants.PAGINATOR_PREFIX)) {
				currentPage = 0;
				List<Pipe> pipes = testPipes != null ? testPipes : createPipes(request);
				paginator = createPaginator();
				paginator.setPipes(pipes);
				Comparator<Object> comparator = createComparator(request);
				paginator.setComparator(comparator);
			} else {
				PaginationStatus status = PaginationStatus.deserialize(request.getSerializedData());
				currentPage = status.getCurrentPage() + 1;
				paginator = status.getPaginator();
				if ( paginator == null ) {
					throw new ComponentExecutionException("No pude recuperar el paginator del request, es null");
				}
			}
			lastPaginator = paginator;
			Page nextPage = paginator.fetchPage(currentPage, getPageSize());
	
			ContinuationResponse response = createResponse();
			response.originatingRequest(request);
			
			if ( nextPage.size() == 0) {
				if ( currentPage != 0 ) {
					//Habrá mandado un mensaje con continuación después de que le mandamos uno con OK para avisarle que no había más datos?
					logger.log(Level.INFO,"check currentPage "+currentPage );
				}
				logger.log(Level.INFO,"check nextPage.size() "+nextPage.size() );
				response.setEstado(Estado.newWithResultadoFalse());
				response.getEstado().setMensaje(Constants.NO_SE_ENCONTRARON_DATOS);
				response.setContinuationstatus(Constants.PAGINATOR_ER_TAG);
			} else {
				response.setEstado(Estado.newWithResultadoTrue());
				for (Object o : nextPage.getElements()) {
					response.addRecord((ResponseRecord)o);
				}
				if ( paginator.hasNext()) {
					response.setContinuationstatus(Constants.PAGINATOR_TR_TAG); 
				} else {
					response.setContinuationstatus(Constants.PAGINATOR_OK_TAG);
				}
			}
			

			PaginationStatus newStatus = new PaginationStatus();
			newStatus.setPaginator(paginator);
			newStatus.setCurrentPage(currentPage);
			response.setSerializedData(newStatus.serialize());

			//Por si las subclases necesitan modificar la response antes de mandarla
			Response finalResponse = this.afterResponseBuilt(request, response);
			responseSH.set(finalResponse.marshal());
			return 0;
		//Convertimos las exceptions específicas en un mensaje legible para el usuario, porque
		// la OV muestra el valor del tag "mensaje" al usuario
		} catch (JAXBException e) {
			logger.log(Level.SEVERE, "Exception, convirtiendo en mensaje para el usuario", e);
			responseSH.set(String.format(Constants.ERROR_RESPONSE_TEMPLATE,Constants.ERROR_INESPERADO));
			return 1;
		} catch (PipeTimeoutException e) {
			logger.log(Level.SEVERE, "Exception, convirtiendo en mensaje para el usuario", e);
			responseSH.set(String.format(Constants.ERROR_RESPONSE_TEMPLATE,Constants.TIMEOUT));
			return 1;
		} catch (PipeUnexpectedErrorException e) {
			logger.log(Level.SEVERE, "Exception, convirtiendo en mensaje para el usuario", e);
			responseSH.set(String.format(Constants.ERROR_RESPONSE_TEMPLATE,Constants.ERROR_INESPERADO));
			return 1;
		} catch (PipeTooManyRowsException e) {
			logger.log(Level.SEVERE, "Exception, convirtiendo en mensaje para el usuario", e);
			responseSH.set(String.format(Constants.ERROR_RESPONSE_TEMPLATE,Constants.MENSAJE_TOO_MANY_ROWS));
			return 1;
		} catch (PipeException e) {
			//Paso el mensaje, y no uno genérico, porque puede venir de un pipe y tener datos relevantes
			logger.log(Level.SEVERE, "Exception, convirtiendo en mensaje para el usuario", e);
			responseSH.set(String.format(Constants.ERROR_RESPONSE_TEMPLATE,e.getMessage()));
			return 1;
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Exception, convirtiendo en mensaje para el usuario", e);
			responseSH.set(String.format(Constants.ERROR_RESPONSE_TEMPLATE,Constants.ERROR_INESPERADO));
			return 1;
		} catch (ClassNotFoundException e) {
			logger.log(Level.SEVERE, "Exception, convirtiendo en mensaje para el usuario", e);
			responseSH.set(String.format(Constants.ERROR_RESPONSE_TEMPLATE,Constants.ERROR_INESPERADO));
			return 1;
		} catch ( ComponentExecutionException e) {
			logger.log(Level.SEVERE, "Exception, convirtiendo en mensaje para el usuario", e);
			responseSH.set(String.format(Constants.ERROR_RESPONSE_TEMPLATE,Constants.ERROR_INESPERADO));
			return 1;
		}
	}

	/**
	 * Las subclases lo pueden redefinir si necesitan modificar la response antes de mandarla
	 * 
	 * @param request El request original 
	 * @param response La respuesta armada después del procesamiento.
	 * @return 
	 */
	protected Response afterResponseBuilt(PaginateRequest request, Response response) {
		return response;
	}

	protected Paginator createPaginator() {
		return new SkippingPaginator();
	}

	protected abstract ContinuationResponse createResponse();

	/**
	 * Crea el comparator usado para ordenar los resultados; le pasamos el request porque algunos ordenan
	 * de acuerdo a lo que indica el usuario
	 * 
	 * @param request
	 * @return
	 */
	protected abstract Comparator<Object> createComparator(PaginateRequest request);

	protected abstract PaginateRequest parseRequestXML(String requestXML) throws JAXBException;

	/**
	 * Retorna una lista de Pipes específicos para el actionCode. Usualmente contiene un pipe a CMS y, de acuerdo a si el request
	 * es Multicomm o no, varios o un InsisPipe
	 * 
	 * @param request
	 */
	public abstract List<Pipe> createPipes(PaginateRequest request);

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List<Pipe> getTestPipes() {
		return testPipes;
	}

	public void setTestPipes(List<Pipe> testPipes) {
		this.testPipes = testPipes;
	}

	public MessagePipe createCMSPipe(PaginateRequest request, String actionCode, ResponseUnmarshaller um) {
		MessagePipe cms = new CMSPipe();
		cms.setRequest(request);
		cms.setActionCode(actionCode);
		cms.setResponseUnmarshaller(um);
		return cms;
	}

	/**
	 * Crea una lista de InsisPipes, teniendo en cuenta si el request es multicomm ( en cuyo caso crea
	 * un pipe por cada Request multicomm a hacer ), o no ( en este caso crea un solo Pipe )
	 * 
	 * Invoca 
	 * @param request
	 * @param actionCode
	 * @param um
	 * @param pageSize
	 * @return
	 */
	public List<Pipe> createInsisPipes(PaginateRequest request, String actionCode, ResponseUnmarshaller um,
			int pageSize) {
				List<Pipe> pipes = new ArrayList<Pipe>();
				
				if ( request.isMulticomm()) {
					logger.log(Level.FINE,"Request es multicomm");
					try {
						List<? extends PaginateRequest> requests = getMulticommRequests(request);
						logger.log(Level.FINE,"Son " + requests.size() + " requests");
						for (PaginateRequest pRequest : requests) {
							logger.log(Level.FINE,"Agrego request " + pRequest);
							pipes.add(createInsisPipe(pRequest, actionCode, um, pageSize));
						}
					} catch (MalformedURLException e) {
						throw new ComponentExecutionException("Al buscar los multicommRequests", e);
					} catch (OSBConnectorException e) {
						throw new ComponentExecutionException("Al buscar los multicommRequests", e);
					} catch (JAXBException e) {
						throw new ComponentExecutionException("Al buscar los multicommRequests", e);
					}
				} else {
					pipes.add(createInsisPipe(request, actionCode, um, pageSize));
				}
				return pipes;
			}

	/**
	 * Retorna la lista de Requests obtenidos a partir de un request Multicomm, es decir una lista donde
	 * cada Request corresponde a una rama de la estructura comercial asociada al usuario del request Multicomm
	 * 
	 * @param request
	 * @return
	 * @throws MalformedURLException
	 * @throws OSBConnectorException
	 * @throws JAXBException
	 */
	public abstract List<? extends PaginateRequest> getMulticommRequests(
			PaginateRequest request) throws MalformedURLException,
			OSBConnectorException, JAXBException;

	/**
	 * Retorna un InsisPipe específico ( las subclases definen la clase ) configurado con los parámetros que recibe
	 * 
	 * @param request
	 * @param actionCode
	 * @param um
	 * @param pageSize
	 * @return
	 */
	public InsisPipe createInsisPipe(PaginateRequest request, String actionCode, ResponseUnmarshaller um,
			int pageSize) {
				InsisPipe insis = createInsisPipe();
				insis.setRequest(request);
				insis.setActionCode(actionCode);
				insis.setResponseUnmarshaller(um);
				insis.setRowCount(pageSize);
				return insis;
			}

	/**
	 * Crea un InsisPipe específico para el actionCode
	 * 
	 * @return
	 */
	public abstract InsisPipe createInsisPipe();

	
}