package com.qbe.services.paginate.service1605;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.UsuarcodNivelclasRequest;

/**
 * Request del mensaje 1605
 * 
 * 
 * 
 * 
<DEFINICION>GetCuponesPendientes.xml</DEFINICION>
<USUARCOD>PR23626965</USUARCOD>
<NIVELCLAS>GO</NIVELCLAS>
<CLIENSECAS>100029438</CLIENSECAS>
<TIPODOCU/>
<NUMEDOCU/>
<APELLIDO><![CDATA[MARTINEZ]]></APELLIDO>
<RAMOPCOD/>
<POLIZA/>
<CERTI/>
<ESTAPOL>TODAS</ESTAPOL>
<NROQRYS/>
<CLIENDESS/>
<RETORNARREQUEST/>
 * 
 * 
 * 
 * 
 * Campos específicos
        <TIPODOCU/>
        <NUMEDOCU/>
        <APELLIDO/>
        <RAMOPCOD>AUS1</RAMOPCOD>
        <POLIZA>00000001</POLIZA>
        <CERTI>00000001571321</CERTI>
        <ESTAPOL>TODAS</ESTAPOL>
        <NROQRYS/>
        <CLIENDESS/>
        <RETORNARREQUEST/> Si es 1 devolver el Request luego de los REGS
        
 * 
 * OJO: en la hoja de request dice:
 * PRODUCTO+POLIZA+NROCONS: Parameters of continuation
 * pero en la de response especifica todos los que hay que copiar:
 * PRODUCTO+POLIZA+NROCONS+NROORDEN+DIRORDEN+TFILTRO+VFILTRO
 * 
 * Usamos NROCONS para guardar los datos serializados de continuación
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="Request")
public class Request1605 extends UsuarcodNivelclasRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(required=true, name="DEFINICION")
	private String definicion = "";

	@XmlElement(required=true, name="TIPODOCU")
	private String tipodocu = "";

	@XmlElement(required=true, name="NUMEDOCU")
	private String numedocu = "";	

	@XmlElement(required=true, name="APELLIDO")
	private String apellido = "";

	@XmlElement(required=true, name="RAMOPCOD")
	private String ramocod = "";

	@XmlElement(required=true, name="POLIZA")
	private String poliza = "";

	@XmlElement(required=true, name="CERTI")
	private String certi = "";
	
	@XmlElement(required=true, name="ESTAPOL")
	private String estapol = "";
	
	@XmlElement(required=true, name="NROQRYS")
	private String nroqrys = "";
	
	@XmlElement(required=true, name="CLIENDESS")
	private String cliendess = "";
	
	@XmlElement(required=true, name="RETORNARREQUEST")
	private String retornarrequest = "";
	
	public Request1605() {
	}

	@Override
	public String getSerializedData() {
		return getNroqrys();
	}

	@Override
	public void setSerializedData(String value) {
		this.setNroqrys(value);
	}

	@Override
	public PaginateRequest createRequestForNextBlockFromResponse(ContinuationResponse resp) {
		Request1605 newRequest = (Request1605) SerializationUtils.clone(this);
		Response1605 cr = (Response1605)resp;
		if ( newRequest != null ) {
			newRequest.setNroqrys(cr.getNroqrys());
		}
		return newRequest;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getDefinicion() {
		return definicion;
	}

	public void setDefinicion(String definicion) {
		this.definicion = definicion;
	}

	public String getTipodocu() {
		return tipodocu;
	}

	public void setTipodocu(String tipodocu) {
		this.tipodocu = tipodocu;
	}

	public String getNumedocu() {
		return numedocu;
	}

	public void setNumedocu(String numedocu) {
		this.numedocu = numedocu;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getRamocod() {
		return ramocod;
	}

	public void setRamocod(String ramocod) {
		this.ramocod = ramocod;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public String getCerti() {
		return certi;
	}

	public void setCerti(String certi) {
		this.certi = certi;
	}

	public String getEstapol() {
		return estapol;
	}

	public void setEstapol(String estapol) {
		this.estapol = estapol;
	}

	public String getNroqrys() {
		return nroqrys;
	}

	public void setNroqrys(String nroqrys) {
		this.nroqrys = nroqrys;
	}

	public String getCliendess() {
		return cliendess;
	}

	public void setCliendess(String cliendess) {
		this.cliendess = cliendess;
	}

	public String getRetornarrequest() {
		return retornarrequest;
	}

	public void setRetornarrequest(String retornarrequest) {
		this.retornarrequest = retornarrequest;
	}
	
	

}
