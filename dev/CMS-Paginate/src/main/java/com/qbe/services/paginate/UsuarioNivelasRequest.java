package com.qbe.services.paginate;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
public abstract class UsuarioNivelasRequest extends PaginateRequest {

	@XmlElement(required = true, name="USUARIO")
	private String usuario = "";

    @XmlElement(required = true, name="NIVELAS")	
	private String nivelAs = "";

    @Override
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

    @Override
	public String getNivelAs() {
		return nivelAs;
	}

	public void setNivelAs(String nivelAs) {
		this.nivelAs = nivelAs;
	}

}
