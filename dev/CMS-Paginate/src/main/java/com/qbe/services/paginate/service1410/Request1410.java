package com.qbe.services.paginate.service1410;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.UsuarioNivelasRequest;

/**
 * Request del mensaje 1410
 * 
 * 
<USUARIO>EX009005L</USUARIO>
<NIVELAS>OR</NIVELAS>
<CLIENSECAS>100029438</CLIENSECAS>
<NIVEL1/>
<CLIENSEC1/>
<NIVEL2/>
<CLIENSEC2/>
<NIVEL3>PR</NIVEL3>
<CLIENSEC3>100029438</CLIENSEC3>
<CONTINUAR/>
 * 
 * 
 * Campos específicos
 * <CONTINUAR/>
 * <NROCONS/> 

	
 * Usamos CONTINUAR para guardar los datos serializados de continuación
 * 
 * @author gavilan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="Request")
public class Request1410 extends UsuarioNivelasRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(required=true, name="CONTINUAR")
	private String continuar = "";

	public Request1410() {
	}

	@Override
	public String getSerializedData() {
		return getContinuar();
	}

	@Override
	public void setSerializedData(String value) {
		this.setContinuar(value);
	}

	@Override
	public PaginateRequest createRequestForNextBlockFromResponse(ContinuationResponse resp) {
		Request1410 newRequest = (Request1410) SerializationUtils.clone(this);
		Response1410 cr = (Response1410)resp;
		if ( newRequest != null ) {
			newRequest.setContinuar(cr.getContinuar());
		}
		return newRequest;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getContinuar() {
		return continuar;
	}

	public void setContinuar(String continuar) {
		this.continuar = continuar;
	}

	
}
