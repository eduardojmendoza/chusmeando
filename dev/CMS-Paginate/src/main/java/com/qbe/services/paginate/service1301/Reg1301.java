package com.qbe.services.paginate.service1301;

import java.io.Serializable;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.qbe.services.paginate.ResponseRecord;
import com.qbe.vbcompat.framework.ComponentExecutionException;

/**
 * Registro de respuesta del 1301
 * 
<REG>
	<PROD>AUS1</PROD>
	<SINIAN>10</SINIAN>
	<SININUM>287553</SININUM>
	<CLIDES>MARTINEZZ , MARIA GRACIELA </CLIDES>
	<RAMO>1</RAMO>
	<POL>00000001</POL>
	<CERPOL>0000</CERPOL>
	<CERANN>0001</CERANN>
	<CERSEC>570270</CERSEC>
	<EST>A</EST>
	<FECSINI>27/12/2013</FECSINI>
	<AGE>PR-5509</AGE>
</REG> 
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="REG")
public class Reg1301 extends ResponseRecord implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(required=true, name="PROD")
	private String prod = "";

	@XmlElement(required=true, name="SINIAN")
	private String sinian = "";

	@XmlElement(required=true, name="SININUM")
	private String sininum = "";

	@XmlElement(required=true, name="CLIDES")
	private String clides = "";

	@XmlElement(required=true, name="RAMO")
	private String ramo = "";

	@XmlElement(required=true, name="POL")
	private String pol = "";

	@XmlElement(required=true, name="CERPOL")
	private String cerpol = "";

	@XmlElement(required=true, name="CERANN")
	private String cerann = "";

	@XmlElement(required=true, name="CERSEC")
	private String cersec = "";

	@XmlElement(required=true, name="EST")
	private String est = "";

	@XmlElement(required=true, name="FECSINI")
	private String fecsini = "";

	@XmlElement(required=true, name="AGE")
	private String age = "";


	/**
	 * Usado para logging en el comparator
	 * @return
	 */
	public String getComparePath() {
		return "#" + getClides() + getCersec(); 
	}
	
	public String getProd() {
		return prod;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}

	public String getSinian() {
		return sinian;
	}

	public void setSinian(String sinian) {
		this.sinian = sinian;
	}

	public String getSininum() {
		return sininum;
	}

	public void setSininum(String sininum) {
		this.sininum = sininum;
	}

	public String getClides() {
		return clides;
	}

	public void setClides(String clides) {
		this.clides = clides;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getCerpol() {
		return cerpol;
	}

	public void setCerpol(String cerpol) {
		this.cerpol = cerpol;
	}

	public String getCerann() {
		return cerann;
	}

	public void setCerann(String cerann) {
		this.cerann = cerann;
	}

	public String getCersec() {
		return cersec;
	}

	public void setCersec(String cersec) {
		this.cersec = cersec;
	}

	public String getEst() {
		return est;
	}

	public void setEst(String est) {
		this.est = est;
	}

	public String getFecsini() {
		return fecsini;
	}

	public void setFecsini(String fecsini) {
		this.fecsini = fecsini;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	
}
