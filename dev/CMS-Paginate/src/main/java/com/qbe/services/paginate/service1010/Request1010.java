package com.qbe.services.paginate.service1010;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.Request1107x9;
import com.qbe.services.paginate.UsuarioNivelasRequest;

/**
 * Request del mensaje 1010
 * 
 * Ejemplo
 * 
<Request>	
	<USUARIO>EX009005L</USUARIO>
	<NIVELAS>PR</NIVELAS>
	<CLIENSECAS>100072343</CLIENSECAS>
	<DOCUMTIP/>
	<DOCUMNRO/>
	<CLIENDES><![CDATA[ARMATI]]></CLIENDES>
	<PRODUCTO/>
	<POLIZA/>
	<CERTI/>
	<PATENTE/>
	<MOTOR/>
	<ESTPOL>TODAS</ESTPOL>
	<QRYCONT/>
	<CLICONT/>
	<PRODUCONT/>
	<POLIZACONT/>
	<SUPLENUMS/>
	<CLIENSECS/>
	<AGENTCLAS/>
	<AGENTCODS/>
	<NROITEMS/>
</Request>	

 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="Request")
public class Request1010 extends UsuarioNivelasRequest {

	@XmlElement(required=true, name="DOCUMTIP")
	private String documtip = "";

	@XmlElement(required=true, name="DOCUMNRO")
	private String documnro = "";

	@XmlElement(required=true, name="CLIENDES")
	private String cliendes = "";

	@XmlElement(required=true, name="PRODUCTO")
	private String producto = "";

	@XmlElement(required=true, name="POLIZA")
	private String poliza = "";

	@XmlElement(required=true, name="CERTI")
	private String certi = "";

	@XmlElement(required=true, name="PATENTE")
	private String patente = "";

	@XmlElement(required=true, name="MOTOR")
	private String motor = "";

	@XmlElement(required=true, name="ESTPOL")
	private String estpol = "";

	@XmlElement(required=true, name="QRYCONT")
	private String qrycont = "";

	@XmlElement(required=true, name="CLICONT")
	private String clicont = "";

	@XmlElement(required=true, name="PRODUCONT")
	private String producont = "";

	@XmlElement(required=true, name="POLIZACONT")
	private String polizacont = "";

	@XmlElement(required=true, name="SUPLENUMS")
	private String suplenums = "";

	@XmlElement(required=true, name="CLIENSECS")
	private String cliensecs = "";

	@XmlElement(required=true, name="AGENTCLAS")
	private String agentclas = "";

	@XmlElement(required=true, name="AGENTCODS")
	private String agentcods = "";

	@XmlElement(required=true, name="NROITEMS")
	private String nroitems = "";

	public Request1010() {
	}

	public String getSerializedData() {
		return getProducont();
	}

	public void setSerializedData(String value) {
		this.setProducont(value);
	}

	/**
	 * 
	 * 
QRYCONT	Parameters for recall
CLICONT	Parameters for recall
PRODUCONT	Parameters for recall
POLIZACONT	Parameters for recall
SUPLENUMS	Parameters for recall
CLIENSECS	Parameters for recall
AGENTCLAS	Parameters for recall
AGENTCODS	Parameters for recall
NROITEMS	Parameters for recall
	 */
	public PaginateRequest createRequestForNextBlockFromResponse(ContinuationResponse resp) {
		Request1010 newRequest = (Request1010) SerializationUtils.clone(this);
		ContinuationResponse1010 cr = (ContinuationResponse1010)resp;
		if ( newRequest != null ) {
			newRequest.setQrycont(cr.getQrycont());
			newRequest.setClicont(cr.getClicont());
			newRequest.setProducont(cr.getProducont());
			newRequest.setPolizacont(cr.getPolizacont());
			newRequest.setSuplenums(cr.getSuplenums());
			newRequest.setCliensecs(cr.getCliensecs());
			newRequest.setAgentclas(cr.getAgentclas());
			newRequest.setAgentcods(cr.getAgentcods());
			newRequest.setNroitems(cr.getNroitems());
		}
		return newRequest;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getDocumtip() {
		return documtip;
	}

	public void setDocumtip(String documtip) {
		this.documtip = documtip;
	}

	public String getDocumnro() {
		return documnro;
	}

	public void setDocumnro(String documnro) {
		this.documnro = documnro;
	}

	public String getCliendes() {
		return cliendes;
	}

	public void setCliendes(String cliendes) {
		this.cliendes = cliendes;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public String getCerti() {
		return certi;
	}

	public void setCerti(String certi) {
		this.certi = certi;
	}

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public String getMotor() {
		return motor;
	}

	public void setMotor(String motor) {
		this.motor = motor;
	}

	public String getEstpol() {
		return estpol;
	}

	public void setEstpol(String estpol) {
		this.estpol = estpol;
	}

	public String getQrycont() {
		return qrycont;
	}

	public void setQrycont(String qrycont) {
		this.qrycont = qrycont;
	}

	public String getClicont() {
		return clicont;
	}

	public void setClicont(String clicont) {
		this.clicont = clicont;
	}

	public String getProducont() {
		return producont;
	}

	public void setProducont(String producont) {
		this.producont = producont;
	}

	public String getPolizacont() {
		return polizacont;
	}

	public void setPolizacont(String polizacont) {
		this.polizacont = polizacont;
	}

	public String getSuplenums() {
		return suplenums;
	}

	public void setSuplenums(String suplenums) {
		this.suplenums = suplenums;
	}

	public String getCliensecs() {
		return cliensecs;
	}

	public void setCliensecs(String cliensecs) {
		this.cliensecs = cliensecs;
	}

	public String getAgentclas() {
		return agentclas;
	}

	public void setAgentclas(String agentclas) {
		this.agentclas = agentclas;
	}

	public String getAgentcods() {
		return agentcods;
	}

	public void setAgentcods(String agentcods) {
		this.agentcods = agentcods;
	}

	public String getNroitems() {
		return nroitems;
	}

	public void setNroitems(String nroitems) {
		this.nroitems = nroitems;
	}
	
	
}
