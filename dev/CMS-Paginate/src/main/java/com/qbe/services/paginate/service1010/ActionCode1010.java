package com.qbe.services.paginate.service1010;

import java.net.MalformedURLException;
import java.util.List;

import javax.xml.bind.JAXBException;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.paginate.MulticommRequestsListResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.PaginatedActionCode;

public abstract class ActionCode1010 extends PaginatedActionCode {

	public ActionCode1010() {
		super();
	}

	public List<? extends PaginateRequest> getMulticommRequests(PaginateRequest request)
			throws MalformedURLException, OSBConnectorException, JAXBException {
				List<? extends PaginateRequest> requests = (new MulticommRequestsListResponse<Request1010>()).getMulticommRequests(request);
				return requests;
			}

	protected PaginateRequest parseRequestXML(String requestXML) throws JAXBException {
		return (new Request1010()).unmarshall(requestXML);
	}

}