package com.qbe.services.paginate.service1404;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.UsuarioNivelasRequest;

/**
 * Request del mensaje 1404
 * 
 * 
<USUARIO>EX009005L</USUARIO>
<NIVELAS>OR</NIVELAS>
<CLIENSECAS>100029438</CLIENSECAS>
<NIVEL1/>
<CLIENSEC1/>
<NIVEL2/>
<CLIENSEC2/>
<NIVEL3>PR</NIVEL3>
<CLIENSEC3>100029438</CLIENSEC3>
<PRODUCTO/>
<POLIZA/>
<NROCONS/>
<NROORDEN/>
<DIRORDEN/>
<TFILTRO/>
<VFILTRO/>
 * 
 * 
 * Campos específicos
 * <PRODUCTO/>
 * <POLIZA/>
 * <NROCONS/>
 * <NROORDEN/>
 * <TFILTRO/>
 * <VFILTRO/>
 * 
 * OJO: en la hoja de request dice:
 * PRODUCTO+POLIZA+NROCONS: Parameters of continuation
 * pero en la de response especifica todos los que hay que copiar:
 * PRODUCTO+POLIZA+NROCONS+NROORDEN+DIRORDEN+TFILTRO+VFILTRO
 * 
 * Usamos NROCONS para guardar los datos serializados de continuación
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name="Request")
public class Request1404 extends UsuarioNivelasRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(required=true, name="PRODUCTO")
	private String producto = "";

	@XmlElement(required=true, name="POLIZA")
	private String poliza = "";

	@XmlElement(required=true, name="NROCONS")
	private String nrocons = "";

	@XmlElement(required=true, name="NROORDEN")
	private String nroorden = "";

	@XmlElement(required=true, name="DIRORDEN")
	private String dirorden = "";

	@XmlElement(required=true, name="TFILTRO")
	private String tfiltro = "";

	@XmlElement(required=true, name="VFILTRO")
	private String vfiltro = "";


	public Request1404() {
	}

	@Override
	public String getSerializedData() {
		return getNrocons();
	}

	@Override
	public void setSerializedData(String value) {
		this.setNrocons(value);
	}

	@Override
	public PaginateRequest createRequestForNextBlockFromResponse(ContinuationResponse resp) {
		Request1404 newRequest = (Request1404) SerializationUtils.clone(this);
		Response1404 cr = (Response1404)resp;
		if ( newRequest != null ) {
			newRequest.setProducto(cr.getProducto());
			newRequest.setPoliza(cr.getPoliza());
			newRequest.setNrocons(cr.getNrocons());
			newRequest.setNroorden(cr.getNroorden());
			newRequest.setDirorden(cr.getDirorden());
			newRequest.setTfiltro(cr.getTfiltro());
			newRequest.setVfiltro(cr.getVfiltro());
		}
		return newRequest;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public String getNrocons() {
		return nrocons;
	}

	public void setNrocons(String nrocons) {
		this.nrocons = nrocons;
	}

	public String getNroorden() {
		return nroorden;
	}

	public void setNroorden(String nroorden) {
		this.nroorden = nroorden;
	}

	public String getTfiltro() {
		return tfiltro;
	}

	public void setTfiltro(String tfiltro) {
		this.tfiltro = tfiltro;
	}

	public String getVfiltro() {
		return vfiltro;
	}

	public void setVfiltro(String vfiltro) {
		this.vfiltro = vfiltro;
	}

	public String getDirorden() {
		return dirorden;
	}

	public void setDirorden(String dirorden) {
		this.dirorden = dirorden;
	}

	
}
