package com.qbe.services.paginate.mqgestion;

import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.mqgeneric.impl.Constants;
import com.qbe.services.mqgeneric.impl.LBAW_MQMensajeGestion;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.MulticommRequestsListResponse;
import com.qbe.services.paginate.OSBConnectorLocator;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.PaginatedActionCode;
import com.qbe.services.paginate.Pipe;
import com.qbe.services.paginate.Response;
import com.qbe.services.paginate.service1428.Comparator1428;
import com.qbe.services.paginate.service1428.Insis1428Pipe;
import com.qbe.services.paginate.service1428.Request1428;
import com.qbe.services.paginate.service1428.Response1428;
import com.qbe.services.paginate.service1428.Response1428Text;
import com.qbe.services.paginate.service1428.Response1428Unmarshaller;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.jaxb.Estado;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

public class GetDeudaCobradaCanalesDetalle extends PaginatedActionCode {

	public static int MESSAGE_1428_PAGE_SIZE = 150;
	
	public static final String ACTION_CODE_1428 = "lbaw_GetConsultaMQGestion";
	
	private static Logger logger = Logger.getLogger(GetDeudaCobradaCanalesDetalle.class.getName());

	public GetDeudaCobradaCanalesDetalle() {
		setPageSize(MESSAGE_1428_PAGE_SIZE);		
	}

	public List<? extends PaginateRequest> getMulticommRequests(PaginateRequest request)
			throws MalformedURLException, OSBConnectorException, JAXBException {
				List<? extends PaginateRequest> requests = (new MulticommRequestsListResponse<Request1428>()).getMulticommRequests(request);
				return requests;
			}

	protected PaginateRequest parseRequestXML(String requestXML) throws JAXBException {
		return (new Request1428()).unmarshall(requestXML);
	}

	protected ContinuationResponse createResponse() {
		return new Response1428();
	}

	protected Comparator<Object> createComparator(PaginateRequest request) {
		return new Comparator1428(request);
	}

	public List<Pipe> createPipes(PaginateRequest request) {

		List<Pipe> pipes = new ArrayList<Pipe>();
		Request1428 aux= (Request1428)request;
	    
		//BUGFIX:1246
		//Cuando PaginasRetornadas es cero , quiere decir que es una bajada a excel o txt.
		//entonces debemos devolver todos los regitros.Sin paginar. 
		if (aux.getPaginasRetornadas().equals("0")){ 
			MESSAGE_1428_PAGE_SIZE=Integer.MAX_VALUE;
	        this.setPageSize(MESSAGE_1428_PAGE_SIZE);
	    }
		pipes.add(createCMSPipe(patchedCMSRequest(request), ACTION_CODE_1428, new Response1428Unmarshaller()));
		pipes.addAll(createInsisPipes(request, ACTION_CODE_1428, new Response1428Unmarshaller(), MESSAGE_1428_PAGE_SIZE));
		return pipes;
	}
	
	/**
	 * Devuelve una copia del request que tiene PAGINASRETORNADAS = 1 y saca el AplicarXSL ( para q me devuelva XMLs )
	 * Si mandamos PAGINASRETORNADAS = 0 a CMS, este va a usar la lógica de continuación y va a devolver ***TODOS*** los
	 * registros del resultset, y nosotros justamente queremos ir paginando 
	 * 
	 */
	private PaginateRequest patchedCMSRequest(PaginateRequest request) {
		Request1428 r = (Request1428)request;
		Request1428 c = r.copy();
		c.setPaginasRetornadas("1");
		c.setAplicarXSL(null);
		return c;
	}

	public InsisPipe createInsisPipe() {
		InsisPipe insis = new Insis1428Pipe();
		return insis;
	}


	/**
	 * Acciones custom después de haber armado la respuesta
	 * 
	 * <ul>
	 * <li> Parchar el PAGINASRETORNADAS si vuelve en el Request, para que sea igual a la cantidad que recibimos. Si vuelve de CMS va a venir un 1
	 * <li> Elimina StartRow y RowCount que son internos
	 * <li> Aplicar un XSL al response si viene especificado en el request. Esto tomado de AbstractMQMensaje
	 * <li> Volver a poner el AplicaXSL si vino en el request
	 * </ul>
	 * 
	 * @return Si no hay que aplicar nada, el response como viene. Si hay que aplicar, el resultado de aplicar el XSL 
	 * @see GetDeudaCobradaCanalesDetalle#patchedCMSRequest(PaginateRequest)
	 */
	@Override
	protected Response afterResponseBuilt(PaginateRequest request, Response response) {
		Request1428 r1428 = (Request1428)request;
		Response1428 resp1428 = (Response1428)response;
		
		if ( resp1428.getRequest() != null ) {
			resp1428.getRequest().setPaginasRetornadas(r1428.getPaginasRetornadas());
			resp1428.getRequest().setStartRow(null);
			resp1428.getRequest().setRowCount(null);
			resp1428.getRequest().setAplicarXSL(r1428.getAplicarXSL());
		}
		
		
		if ( r1428.getAplicarXSL() != null && r1428.getAplicarXSL().length() > 0 ) {
			String xslFilename = r1428.getAplicarXSL();
			logger.log(Level.FINE, "Por aplicar XSL:" + xslFilename);

			//Los XSLs procesan los //CANAL
			// Y devuelve:
			/*
			 <Response>
    			<Estado resultado="true" mensaje=""/>

				Apellido / Razón Social,Nro. Póliza,Endoso,Estado de Póliza,Nro. de Recibo,Moneda,Import
				etc
			 */			
			try {
				String sourceXML = resp1428.marshal();
				//Estos archivos están en el prj de mqgestion
				InputStream xslStream = LBAW_MQMensajeGestion.class.getClassLoader().getResourceAsStream(Constants.ROOT_DEF_DIR + File.separator + Constants.DEF_MENSAJES_GESTION + File.separator +  xslFilename); 
				if ( xslStream != null)	{ 
					String resultXml = applyXSL(sourceXML, xslStream);
					resultXml = resultXml.replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" ); 
					logger.log(Level.FINE, resultXml);
					
					Response1428Text finalResponse = new Response1428Text();
					finalResponse.setEstado(Estado.newWithResultadoTrue());
					finalResponse.getMixedContent().add(resultXml);
					return finalResponse;
				} else {
					throw new ComponentExecutionException("No pude levantar el XSL: " + xslFilename);
				}
			} catch (JAXBException e) {
				throw new ComponentExecutionException(e);
			}
		} else {
			return response;
		}
	}
	
	
	/**
 	 * Aplica el XSL contenido en el xslDom sobre el org.w3c.dom.Document del receptor
	 * 
	 * @param xslDom
	 * @return
	 * @throws XmlDomExtendedException
	 */
	public String applyXSL(String xml, InputStream xsl) {
	    TransformerFactory tFactory = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl",null);
	    try {
			Transformer transformer = tFactory.newTransformer(new StreamSource(xsl));
			StringWriter xmlResultWriter = new StringWriter();
			transformer.transform(new StreamSource(new StringReader(xml)),
					new StreamResult((xmlResultWriter)));
			return xmlResultWriter.toString();
		} catch (Exception e) {
			String msg = e.getClass().getName() + " en " + this.getClass().getName();
			logger.log(Level.SEVERE, msg,e);
			throw new ComponentExecutionException(msg, e);
		}
	}
	
	
	
}