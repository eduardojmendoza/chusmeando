package com.qbe.services.paginate.service1107;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import com.qbe.services.paginate.ActionCode1107x9;
import com.qbe.services.paginate.ContinuationResponse;
import com.qbe.services.paginate.InsisPipe;
import com.qbe.services.paginate.PaginateRequest;
import com.qbe.services.paginate.Pipe;
import com.qbe.services.paginate.service1109.Insis1109Pipe;

public class OVOpePendDetallesPaginada extends ActionCode1107x9   {

	/**
	 * Es el page size especificado del 1107. 
	 */
	public static final int MESSAGE_1107_PAGE_SIZE = 160;
	
	protected static final String ACTION_CODE_1107 = "lbaw_OVOpePendDetalles";
	
	private static Logger logger = Logger.getLogger(OVOpePendDetallesPaginada.class.getName());

	public OVOpePendDetallesPaginada() {
		setPageSize(MESSAGE_1107_PAGE_SIZE);		
	}

	protected ContinuationResponse createResponse() {
		return new Response1107();
	}

	protected Comparator<Object> createComparator(PaginateRequest request) {
		return new Comparator1107();
	}

	public List<Pipe> createPipes(PaginateRequest request) {

		List<Pipe> pipes = new ArrayList<Pipe>();
		pipes.add(createCMSPipe(request, ACTION_CODE_1107, new Response1107Unmarshaller()));
		pipes.addAll(createInsisPipes(request, ACTION_CODE_1107, new Response1107Unmarshaller(), MESSAGE_1107_PAGE_SIZE));
		return pipes;
	}
	
	public InsisPipe createInsisPipe() {
		InsisPipe insis = new Insis1107Pipe();
		return insis;
	}
	
	
}
