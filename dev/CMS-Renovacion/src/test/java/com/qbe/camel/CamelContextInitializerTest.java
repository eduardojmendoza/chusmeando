package com.qbe.camel;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;

public class CamelContextInitializerTest  {

	private static Logger logger = Logger.getLogger(CamelContextInitializer.class.getName());

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		logger.log(Level.INFO, "Inicializando camel routes");
		System.out.println("Inicializando camel routes");
        //super.init(config);
     
        final CamelContext camelContext = new DefaultCamelContext();
        try {
			camelContext.addRoutes(new CamelContextInitializer().createRouteBuilder());
			camelContext.start();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception al inicializar camel", e);
		}
		

	}
    

	
}
