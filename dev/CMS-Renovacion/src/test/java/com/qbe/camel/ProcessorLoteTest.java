package com.qbe.camel;

import java.io.File;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Test;

public class ProcessorLoteTest {
	
	private ProcessorLote processorLote;
	
	private String tPATH ="src/test/resources/";
	private String tFILE = "L1502091625.LOTE";
	
	@Test
	public void testProcessor() throws Exception {
		
		CamelContext ctx = new DefaultCamelContext(); // armo en Exchange
	    Exchange ex = new DefaultExchange(ctx);
	    
	    processorLote=new ProcessorLote();
	    
	    File fileLote=new File(tPATH,tFILE);
	    //cargo valores dummys
	    ex.getIn().setBody(fileLote);
	    ex.getIn().setHeader("CamelFileParent", "/tmp/renovacion/lote");
	    
	    processorLote.process(ex);
		
	    File file = ex.getIn().getBody(File.class);
			
		// falta agregar assert de esta estructura xml
	    //System.out.println("Test file: " + file.toString());
		
	 	//fail("Not yet implemented");
	}
	

}
