package com.qbe.camel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.logging.Logger;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.qbe.utils.batchrenewals.BatchFilesMerger;


/**
 * Procesa el archivo .LOTE.levanta el crudo de cada file y arma el mensaje para ser enviado al ws RenovacionPrepareWs
 * @author gavilan
 *
 */
public class ProcessorLoadFiles implements Processor {

	private static Logger logger = Logger.getLogger(ProcessorLoadFiles.class.getName());

	/*
	 *  templete del mensaje a enviar a RenovacionPrepareWs
	 */
	private static String TEMPLATE_ENV = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
										"<soap:Envelope  xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"+
											"<soap:Header></soap:Header>"+
											"<soap:Body>{0}</soap:Body>" + // va el xml <LOTE>
										"</soap:Envelope>";
	/*
	 *  encoding que utilizamos para levantar los archivos 
	 */
	private static String ENCODING="UTF-8";
	
	
   /**
    * Entrada principal .Supongo que viene el contenido del file LOTE , son tres archivos separados por espacios . 
    * por ejemplo: R1502200940.TXT N1502200940.TXT F1502200940.TXT
    */
	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub

		File file = exchange.getIn().getBody(File.class);
		
		logger.info("Contenido file: " + file);
		
		String line = readContentFile(file);
		
		//separo los nombres de los archivos por espacio
		String[] splited = line.split(" "); 
		
		String folder = exchange.getIn().getHeader("CamelFileParent").toString()+File.separator;
		
		String renovacionFile= splited[0];
		String notasInternasFile= splited[1];
		String textosFile= splited[2].trim();
		
		logger.info("Processing in folder "+folder);
		
		logger.info("Renovacion: " + renovacionFile+" Notas: "+notasInternasFile+" Textos : "+textosFile);
		
		String xmlCrudo = BatchFilesMerger.mergeBatchFiles(folder+renovacionFile,folder+notasInternasFile,folder+textosFile,ENCODING);
		
		String envelope=buildEnv(xmlCrudo);
		
		exchange.getIn().setBody(envelope);	
		
	}
	
	private String buildEnv (String body) {
		
		return MessageFormat.format(TEMPLATE_ENV, body);
	}
	
	/**
	 *  lee el contenido del archivo pasado por parametro.
	 *  
	 * @param file
	 * @return contenido del archivo .En este caso devuelve el contenido de LOTE
	 */
	private String readContentFile(File file){
		
		String lineOut="";
		FileInputStream fis;
		
	    try {
	    	
			fis = new FileInputStream(file);
			StringBuilder builder = new StringBuilder();
			int ch;
				
		    while((ch = fis.read()) != -1){
				    builder.append((char)ch);
		    }
		      
		    lineOut = builder.toString();
		    
		    fis.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	    
	    
		logger.info("Content : " + lineOut);
		
	    return lineOut;
		
	}


}
