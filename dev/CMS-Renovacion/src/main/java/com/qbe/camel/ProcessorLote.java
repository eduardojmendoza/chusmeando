package com.qbe.camel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.logging.Logger;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ProcessorLote implements Processor {
	
	private static Logger logger = Logger.getLogger(ProcessorLote.class.getName());

	
	private static String TEMPLATE_ENV = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><soap:Envelope  xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"+
										"<soap:Header></soap:Header>"+
										"<soap:Body>"+
											"<Lote>"+
											"<Path>"+"{0}"+"/</Path>"+
											"<RenovacionCrudo>" +"{1}"+"</RenovacionCrudo>" +
											"<NotasInternasCrudo>"+"{2}"+"</NotasInternasCrudo>" +
											"<TextosFrenteCrudo>"+"{3}" +"</TextosFrenteCrudo>" +
									      "</Lote>"+
										"</soap:Body></soap:Envelope>";
	
	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub

		File file = exchange.getIn().getBody(File.class);
		
		logger.info("Processing file: " + file);
		
		String line = readContentFile(file);
	// supongo que viene el contenido de lote viene separado por espacios
		String[] splited = line.split(" "); 
		
		String text = buildBody(exchange.getIn().getHeader("CamelFileParent").toString(),splited[0],splited[1],splited[2].trim());;
		
		logger.info("Message : " + text);
		
		exchange.getIn().setBody(text);	
		
	}
	
	private String buildBody (String path,String renovacion,String notas,String textos) {
		
		return MessageFormat.format(TEMPLATE_ENV, path, renovacion, notas,textos);
	}
	
	private String readContentFile(File file){
		
		String lineOut="";
		FileInputStream fis;
		
	    try {
	    	
			fis = new FileInputStream(file);
			StringBuilder builder = new StringBuilder();
			int ch;
				
		    while((ch = fis.read()) != -1){
				    builder.append((char)ch);
		    }
		      
		    lineOut = builder.toString();
		    
		    fis.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	    
	    
		logger.info("Content : " + lineOut);
		
	    return lineOut;
		
	}

}
