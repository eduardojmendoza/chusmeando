package com.qbe.utils.batchrenewals;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.xmlbeans.XmlObject;

public class FilesRenewals {
	
	
	/**
	 * Genera archivo xml con el contenido del content en la carpeta folder con el nombre de archivo name.
	 * 
	 * @param content  contenido del archivo.
	 * @param folder  carpeta donde va a crear el archivo
	 * @param name nombre archivo con direccion asoluta
	 * 
	 * 
	 */

	public static void writeFile (String content, String folder,String name) {
		
		try {
		
			File file = new File(folder+"/"+name);
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
 
			//System.out.println("Done xml");
 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/** 
	 * Genera un archivo .xml con los datos que se encuentra en el xml
	 * 
	 * @param xmlStatusExample : xml ArchivoRenovacionStatus 
	 */
	
	public static void writeFileStatus(XmlObject xmlStatusExample) {
	
		
		
	}
	
	/**
	 * Mueve el archivo file , desde la carpeta originFolder , a la carpeta destFolder
	 * @param file . archivo a mover
	 * @param originFolder . carpeta origen 
	 * @param destFolder - carpeta destino
	 */
	
	public static void moveFileStatus(String file,String originFolder,String destFolder) {
	
		InputStream inStream = null;
		OutputStream outStream = null;
	 
	    	try{
	 
	    	    File afile =new File(originFolder+"/"+file);
	    	    File bfile =new File(destFolder+"/"+file);
	 
	    	    inStream = new FileInputStream(afile);
	    	    outStream = new FileOutputStream(bfile);
	 
	    	    byte[] buffer = new byte[1024];
	 
	    	    int length;
	    	    //copy the file content in bytes 
	    	    while ((length = inStream.read(buffer)) > 0){
	 
	    	    	outStream.write(buffer, 0, length);
	 
	    	    }
	    	    inStream.close();
	    	    outStream.close();
	 
	    	    //borro archivo original
	    	    afile.delete();
	 
	    	  //  System.out.println("File is copied successful!");
	 
	    	}catch(IOException e){
	    	    e.printStackTrace();
	    	}
	} 
	
	

}
