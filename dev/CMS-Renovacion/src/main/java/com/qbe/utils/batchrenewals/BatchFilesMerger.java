package com.qbe.utils.batchrenewals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.text.MessageFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Lee tres archivos y los retorna en una estructura XML de "Lote de renovación de pólizas".
 * Prestar atención al Charset para que lea bien los caracteres > ASCII ( acentos etc )
 * Usualmente desde AIS viene ISO-8859-1
 * 
 * La implementación de la lectura está copiada de org.apache.commons.io.FileUtils
 *
 * Tiene un fix para sacar el terminador de los archivos que vienen de AIS ( los archivos terminan con un Hexa 1A dec 26 SUB ( CTRL-Z EOF )
 *
 * @author ramiro
 *
 */
public class BatchFilesMerger {


	public static final String NS = "http://xml.qbe.com/OSB-callouts/BatchFilesMerger";

	/**
	 * Usamos nuestro propio buffer, este es el tamaño
	 */
    private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

    /**
     * No more data
     */
	private static final int EOF = -1;

	/**
	 * Template del XML que retorna.
	 */
	private static String TEMPLATE = "<Lote>" +
	"<RenovacionCrudo>" +
	"{0}" + // un tag <poliza> por cada linea
	"</RenovacionCrudo>" +
	"<NotasInternasCrudo>" +
	"{1}" + //todo el contenido
	"</NotasInternasCrudo>" +
	"<TextoFrenteCrudo>" +
	"{2}" + //todo el contenido
	"</TextoFrenteCrudo>" +
	"</Lote>";


	/**
	 * Trabaja con Strings, usado principalmente para testear porque OSB prefiere trabajar con XMLObjects
	 *
	 * @param renewalFilePath
	 * @param internalNotesPath
	 * @param frontNotesPath
	 * @param charset
	 * @return
	 * @throws IOException
	 */
	public static String mergeBatchFiles ( String renewalFilePath, String internalNotesPath, String frontNotesPath, String charset ) throws IOException {

		String renewal = readFileToString(new File(renewalFilePath), Charset.forName(charset));
		String internal = readFileToString(new File(internalNotesPath), Charset.forName(charset));
		String front = readFileToString(new File(frontNotesPath), Charset.forName(charset));

		return MessageFormat.format(TEMPLATE, renewal, internal, front);
	}


	/**
	 *
	 * @param renewalFilePath
	 * @param internalNotesPath
	 * @param frontNotesPath
	 * @param charset
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws XmlException
	 */
	public static XmlObject mergeBatchFilesXML(String renewalFilePath, String internalNotesPath, String frontNotesPath, String charset ) throws IOException, ParserConfigurationException, XmlException {
		String renewal = readFileToString(new File(renewalFilePath), Charset.forName(charset));
		String internal = readFileToString(new File(internalNotesPath), Charset.forName(charset));
		String front = readFileToString(new File(frontNotesPath), Charset.forName(charset));

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document doc = db.newDocument();

		Element loteEl = doc.createElementNS(NS, "Lote");
		doc.appendChild(loteEl);

		Element renewalEl = doc.createElementNS(NS, "RenovacionCrudo");
		renewalEl.appendChild(doc.createTextNode(renewal));
		loteEl.appendChild(renewalEl);

		Element notesEl = doc.createElementNS(NS, "NotasInternasCrudo");
		notesEl.appendChild(doc.createTextNode(internal));
		loteEl.appendChild(notesEl);

		Element frontEl = doc.createElementNS(NS, "TextoFrenteCrudo");
		frontEl.appendChild(doc.createTextNode(front));
		loteEl.appendChild(frontEl);

		return XmlObject.Factory.parse(doc);
	}




    public static String readFileToString(File file, Charset encoding) throws IOException {
        InputStream in = null;
        try {
            in = openInputStream(file);
            String withSUBsMaybe = streamToString(in, encoding);

            return withSUBsMaybe.replace(new String(new char[]{0x1a}),"" );
        } finally {
            in.close();
        }
    }

    public static FileInputStream openInputStream(File file) throws IOException {
        if (file.exists()) {
            if (file.isDirectory()) {
                throw new IOException("File '" + file + "' exists but is a directory");
            }
            if (file.canRead() == false) {
                throw new IOException("File '" + file + "' cannot be read");
            }
        } else {
            throw new FileNotFoundException("File '" + file + "' does not exist");
        }
        return new FileInputStream(file);
    }

    public static String streamToString(InputStream input, Charset encoding) throws IOException {
        StringWriter sw = new StringWriter();
        copy(input, sw, encoding);
        return sw.toString();
    }

    public static void copy(InputStream input, StringWriter output, Charset encoding) throws IOException {
        InputStreamReader in = new InputStreamReader(input, encoding);
        copy(in, output);
    }

    public static int copy(Reader input, Writer output) throws IOException {
        long count = copyLarge(input, output);
        if (count > Integer.MAX_VALUE) {
            return -1;
        }
        return (int) count;
    }

    public static long copyLarge(Reader input, Writer output) throws IOException {
        return copyLarge(input, output, new char[DEFAULT_BUFFER_SIZE]);
    }

    public static long copyLarge(Reader input, Writer output, char [] buffer) throws IOException {
        long count = 0;
        int n = 0;
        while (EOF != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }

}
