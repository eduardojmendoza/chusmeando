package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVRiesHogMasivo implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * 
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQRiesgos.lbaw_OVRiesHogMasivo";
  static final String mcteOpID = "1206";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_Producto = "//PRODUCTO";
  static final String mcteParam_Poliza = "//POLIZA";
  static final String mcteParam_Certi = "//CERTI";
  static final String mcteParam_Suplenum = "//SUPLENUM";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarProducto = "";
    String wvarPoliza = "";
    String wvarCerti = "";
    String wvarSuplenum = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarProducto = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Producto ) */ ) + Strings.space( 4 ), 4 );
      wvarPoliza = Strings.right( Strings.fill( 8, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Poliza ) */ ), 8 );
      wvarCerti = Strings.right( Strings.fill( 14, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Certi ) */ ), 14 );
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_Suplenum ) */.getLength() != 0 )
      {
        wvarSuplenum = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Suplenum ) */ ), 4 );
      }
      else
      {
        wvarSuplenum = Strings.fill( 4, "0" );
      }
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los parámetros generales (ParametrosMQ.xml)
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 70;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + "000000000    000000000  000000000  000000000" + wvarProducto + wvarPoliza + wvarCerti + wvarSuplenum;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      wvarStep = 190;
      wvarResult = "";
      //cantidad de caracteres ocupados por parámetros de entrada
      wvarPos = 97;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 200;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 210;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 240;
        wvarResult = "";
        wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseString), new Variant(wvarstrLen) } );
        //
        if( !Strings.trim( wvarResult ).equals( "" ) )
        {
          wvarStep = 300;
          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
          //
        }
        else
        {
          //
          wvarStep = 270;
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          //
        }
      }
      //
      wvarStep = 320;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";

    //Es un solo dato.
    wvarResult = wvarResult + "<DATOS>";
    wvarResult = wvarResult + "<TIPVIV><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 2), 40 ) ) + "]]></TIPVIV>";
    wvarResult = wvarResult + "<ALT><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 42), 2 ) ) + "]]></ALT>";
    wvarResult = wvarResult + "<USO><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 46), 40 ) ) + "]]></USO>";
    wvarResult = wvarResult + "<CALDERA><![CDATA[" + ((Strings.mid( strParseString, (wvarPos + 86), 1 ).equals( "N" )) ? "NO" : "SI") + "]]></CALDERA>";
    wvarResult = wvarResult + "<ASCEN><![CDATA[" + ((Strings.mid( strParseString, (wvarPos + 87), 1 ).equals( "N" )) ? "NO" : "SI") + "]]></ASCEN>";
    wvarResult = wvarResult + "<ALARMA><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 90), 40 ) ) + "]]></ALARMA>";
    wvarResult = wvarResult + "<GUARDIA><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 132), 40 ) ) + "]]></GUARDIA>";
    wvarResult = wvarResult + "<REJAS><![CDATA[" + ((Strings.mid( strParseString, (wvarPos + 172), 1 ).equals( "N" )) ? "NO" : "SI") + "]]></REJAS>";
    wvarResult = wvarResult + "<PURBLIN><![CDATA[" + ((Strings.mid( strParseString, (wvarPos + 173), 1 ).equals( "N" )) ? "NO" : "SI") + "]]></PURBLIN>";
    wvarResult = wvarResult + "<DISY><![CDATA[" + ((Strings.mid( strParseString, (wvarPos + 174), 1 ).equals( "N" )) ? "NO" : "SI") + "]]></DISY>";
    wvarResult = wvarResult + "<PROP><![CDATA[" + ((Strings.mid( strParseString, (wvarPos + 196), 1 ).equals( "N" )) ? "NO" : "SI") + "]]></PROP>";
    wvarResult = wvarResult + "<ZONA><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 243), 20 ) ) + "]]></ZONA>";
    wvarResult = wvarResult + "<DOMI><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 263), 59 ) ) + "]]></DOMI>";
    wvarResult = wvarResult + "<PROVI><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 324), 25 ) ) + "]]></PROVI>";
    wvarResult = wvarResult + "<LOCALI><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 349), 38 ) ) + "]]></LOCALI>";
    wvarResult = wvarResult + "<CODPOS><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 387), 8 ) ) + "]]></CODPOS>";
    wvarResult = wvarResult + "</DATOS>";

    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
