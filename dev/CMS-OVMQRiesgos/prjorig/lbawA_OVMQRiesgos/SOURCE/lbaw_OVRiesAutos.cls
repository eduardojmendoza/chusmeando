VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVRiesAutos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQRiesgos.lbaw_OVRiesAutos"
Const mcteOpID              As String = "1202"

'Parametros XML de Entrada
Const mcteParam_Usuario      As String = "//USUARIO"
Const mcteParam_Producto     As String = "//PRODUCTO"
Const mcteParam_Poliza       As String = "//POLIZA"
Const mcteParam_Certi        As String = "//CERTI"
Const mcteParam_Suplenum     As String = "//SUPLENUM"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarProducto        As String
    Dim wvarPoliza          As String
    Dim wvarCerti           As String
    Dim wvarSuplenum        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarProducto = Left(.selectSingleNode(mcteParam_Producto).Text & Space(4), 4)
        wvarPoliza = Right(String(8, "0") & .selectSingleNode(mcteParam_Poliza).Text, 8)
        wvarCerti = Right(String(14, "0") & .selectSingleNode(mcteParam_Certi).Text, 14)
        If .selectNodes(mcteParam_Suplenum).length <> 0 Then
            wvarSuplenum = Right(String(4, "0") & .selectSingleNode(mcteParam_Suplenum).Text, 4)
        Else
            wvarSuplenum = String(4, "0")
        End If
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & Space(4) & "000000000    000000000  000000000  000000000" & wvarProducto & wvarPoliza & wvarCerti & wvarSuplenum
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    wvarStep = 190
    wvarResult = ""
    wvarPos = 97  'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 200
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 210
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 240
        wvarResult = ""
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        '
        If Trim(wvarResult) <> "" Then
            wvarStep = 300
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
            '
        Else
            '
            wvarStep = 270
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>"
            '
        End If
    End If
    '
    wvarStep = 320
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarstrLen As Long) As String
Dim wvarResult As String
        
    'Es un solo dato.
    wvarResult = wvarResult & "<DATOS>"
    '
    wvarResult = wvarResult & "<COMUNES>"
    wvarResult = wvarResult & "<MOTOR><![CDATA[" & Trim(Mid(strParseString, wvarPos, 25)) & "]]></MOTOR>"
    wvarResult = wvarResult & "<PATENTE><![CDATA[" & Trim(Mid(strParseString, wvarPos + 25, 10)) & "]]></PATENTE>"
    wvarResult = wvarResult & "<CHASIS><![CDATA[" & Trim(Mid(strParseString, wvarPos + 35, 30)) & "]]></CHASIS>"
    wvarResult = wvarResult & "<NROVEH><![CDATA[" & CCur(Trim(Mid(strParseString, wvarPos + 65, 9))) & "]]></NROVEH>"
    wvarResult = wvarResult & "<MARCA><![CDATA[" & Trim(Mid(strParseString, wvarPos + 79, 20)) & "]]></MARCA>"
    wvarResult = wvarResult & "<MODELO><![CDATA[" & Trim(Mid(strParseString, wvarPos + 104, 20)) & "]]></MODELO>"
    wvarResult = wvarResult & "<SUBMODEL><![CDATA[" & Trim(Mid(strParseString, wvarPos + 129, 20)) & "]]></SUBMODEL>"
    wvarResult = wvarResult & "<ADIC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 154, 20)) & "]]></ADIC>"
    wvarResult = wvarResult & "<ORIGEN><![CDATA[" & Trim(Mid(strParseString, wvarPos + 175, 20)) & "]]></ORIGEN>"
    wvarResult = wvarResult & "<USO><![CDATA[" & Trim(Mid(strParseString, wvarPos + 198, 20)) & "]]></USO>"
    wvarResult = wvarResult & "<VTV><![CDATA[" & IIf(Mid(strParseString, wvarPos + 218, 1) = "S", "SI", "NO") & "]]></VTV>"
    wvarResult = wvarResult & "<VTVFEC><![CDATA[" & IIf(Trim(Mid(strParseString, wvarPos + 219, 10)) = "00/00/0000", "", Trim(Mid(strParseString, wvarPos + 219, 10))) & "]]></VTVFEC>"
    wvarResult = wvarResult & "<COLOR><![CDATA[" & Trim(Mid(strParseString, wvarPos + 232, 20)) & "]]></COLOR>"
    wvarResult = wvarResult & "<KM><![CDATA[" & CCur(Mid(strParseString, wvarPos + 252, 7)) & "]]></KM>"
    wvarResult = wvarResult & "<CATEG><![CDATA[" & Trim(Mid(strParseString, wvarPos + 261, 20)) & "]]></CATEG>"
    wvarResult = wvarResult & "<TIPVEH><![CDATA[" & Trim(Mid(strParseString, wvarPos + 285, 20)) & "]]></TIPVEH>"
    wvarResult = wvarResult & "<ANIOFAB><![CDATA[" & Trim(Mid(strParseString, wvarPos + 305, 7)) & "]]></ANIOFAB>"
    wvarResult = wvarResult & "<GUARGARA><![CDATA[" & IIf(Mid(strParseString, wvarPos + 312, 1) = "S", "SI", "NO") & "]]></GUARGARA>"
    wvarResult = wvarResult & "<GUARDOMI><![CDATA[" & IIf(Mid(strParseString, wvarPos + 313, 1) = "S", "SI", "NO") & "]]></GUARDOMI>"
    wvarResult = wvarResult & "<RADI><![CDATA[" & Trim(Mid(strParseString, wvarPos + 316, 20)) & "]]></RADI>"
    wvarResult = wvarResult & "<PROV><![CDATA[" & Trim(Mid(strParseString, wvarPos + 338, 20)) & "]]></PROV>"
    wvarResult = wvarResult & "<ZONA><![CDATA[" & Trim(Mid(strParseString, wvarPos + 362, 20)) & "]]></ZONA>"
    wvarResult = wvarResult & "<CODPOS><![CDATA[" & Trim(Mid(strParseString, wvarPos + 382, 8)) & "]]></CODPOS>"
    wvarResult = wvarResult & "</COMUNES>"
    '
    wvarResult = wvarResult & "<LECTINI>"
    wvarResult = wvarResult & "<CIAANT><![CDATA[" & Trim(Mid(strParseString, wvarPos + 394, 20)) & "]]></CIAANT>"
    wvarResult = wvarResult & "<ANTIG><![CDATA[" & Trim(Mid(strParseString, wvarPos + 414, 2)) & "]]></ANTIG>"
    wvarResult = wvarResult & "<CANTSINI><![CDATA[" & CCur(Trim(Mid(strParseString, wvarPos + 416, 2))) & "]]></CANTSINI>"
    wvarResult = wvarResult & "<KMINI><![CDATA[" & CCur(Trim(Mid(strParseString, wvarPos + 418, 6))) & "]]></KMINI>"
    wvarResult = wvarResult & "<GNC><![CDATA[" & IIf(Mid(strParseString, wvarPos + 424, 1) = "S", "SI", "NO") & "]]></GNC>"
    wvarResult = wvarResult & "</LECTINI>"
    '
    wvarResult = wvarResult & "<CONDUCTORES>"
    While Trim(Mid(strParseString, wvarPos + 425, 20)) <> ""
        wvarResult = wvarResult & "<CONDUCTOR>"
        wvarResult = wvarResult & "<APENOM><![CDATA[" & Trim(Mid(strParseString, wvarPos + 425, 20)) & ", " & Trim(Mid(strParseString, wvarPos + 445, 30)) & "]]></APENOM>"
        wvarResult = wvarResult & "<TIPDOC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 477, 20)) & "]]></TIPDOC>"
        wvarResult = wvarResult & "<NRODOC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 497, 11)) & "]]></NRODOC>"
        wvarResult = wvarResult & "<ESTCIV><![CDATA[" & Trim(Mid(strParseString, wvarPos + 508, 10)) & "]]></ESTCIV>"
        wvarResult = wvarResult & "<SEXO><![CDATA[" & Trim(Mid(strParseString, wvarPos + 518, 10)) & "]]></SEXO>"
        wvarResult = wvarResult & "<FECNAC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 528, 10)) & "]]></FECNAC>"
        wvarResult = wvarResult & "<TIPO><![CDATA[" & Trim(Mid(strParseString, wvarPos + 538, 1)) & "]]></TIPO>"
        wvarResult = wvarResult & "<VINC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 539, 2)) & "]]></VINC>"
        wvarResult = wvarResult & "</CONDUCTOR>"
        '
        wvarPos = wvarPos + 116
    Wend
    wvarResult = wvarResult & "</CONDUCTORES>"
    '
    wvarPos = 97
    wvarResult = wvarResult & "<ACCESOS>"
    While Trim(Mid(strParseString, wvarPos + 1585, 4)) <> "0000"
        wvarResult = wvarResult & "<ACCESO>"
        wvarResult = wvarResult & "<COD><![CDATA[" & Trim(Mid(strParseString, wvarPos + 1589, 30)) & "]]></COD>"
        wvarResult = wvarResult & "<SUMAAS><![CDATA[" & CCur(Trim(Mid(strParseString, wvarPos + 1619, 14))) & "]]></SUMAAS>"
        wvarResult = wvarResult & "<DESC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 1633, 30)) & "]]></DESC>"
        wvarResult = wvarResult & "</ACCESO>"
        wvarPos = wvarPos + 78
    Wend
    wvarResult = wvarResult & "</ACCESOS>"
    '
    wvarPos = 97
    wvarResult = wvarResult & "<ADICIONALES>"
    While Trim(Mid(strParseString, wvarPos + 2365, 11)) <> ""
        wvarResult = wvarResult & "<ASEGADIC>"
        wvarResult = wvarResult & "<DOCUADIC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 2365, 11)) & "]]></DOCUADIC>"
        wvarResult = wvarResult & "<APEADIC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 2376, 49)) & "]]></APEADIC>"
        wvarResult = wvarResult & "</ASEGADIC>"
        wvarPos = wvarPos + 60
    Wend
    wvarResult = wvarResult & "</ADICIONALES>"
    '
    wvarResult = wvarResult & "</DATOS>"
    '
    ParseoMensaje = wvarResult
'
End Function











