package com.qbe.services.ovmqriesgos.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ovmqriesgos.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
import com.qbe.vbcompat.format.VBFixesUtil;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVRiesHogMasivo implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * 
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQRiesgos.lbaw_OVRiesHogMasivo";
  static final String mcteOpID = "1206";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_Producto = "//PRODUCTO";
  static final String mcteParam_Poliza = "//POLIZA";
  static final String mcteParam_Certi = "//CERTI";
  static final String mcteParam_Suplenum = "//SUPLENUM";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarProducto = "";
    String wvarPoliza = "";
    String wvarCerti = "";
    String wvarSuplenum = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUsuario = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarProducto = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Producto )  ) + Strings.space( 4 ), 4 );
      wvarPoliza = Strings.right( Strings.fill( 8, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Poliza )  ), 8 );
      wvarCerti = Strings.right( Strings.fill( 14, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Certi )  ), 14 );
      if( wobjXMLRequest.selectNodes( mcteParam_Suplenum ) .getLength() != 0 )
      {
        wvarSuplenum = Strings.right( Strings.fill( 4, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Suplenum )  ), 4 );
      }
      else
      {
        wvarSuplenum = Strings.fill( 4, "0" );
      }
      //
      wvarStep = 20;
      wobjXMLRequest = null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 60;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      //
      wvarStep = 70;
      wobjXMLParametros = null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + "000000000    000000000  000000000  000000000" + wvarProducto + wvarPoliza + wvarCerti + wvarSuplenum;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();

      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 190;
      wvarResult = "";
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos = 97;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 200;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 210;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 240;
        wvarResult = "";
        wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        //
        if( !Strings.trim( wvarResult ).equals( "" ) )
        {
          wvarStep = 300;
          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
          //
        }
        else
        {
          //
          wvarStep = 270;
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          //
        }
      }
      //
      wvarStep = 320;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch (MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
	try 
	{
		//~~~~~~~~~~~~~~~
			mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";

    //Es un solo dato.
    wvarResult = wvarResult + "<DATOS>";
    wvarResult = wvarResult + "<TIPVIV><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 2), 40 ) ) + "]]></TIPVIV>";
    wvarResult = wvarResult + "<ALT><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 42), 2 ) ) + "]]></ALT>";
    wvarResult = wvarResult + "<USO><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 46), 40 ) ) + "]]></USO>";
    wvarResult = wvarResult + "<CALDERA><![CDATA[" + ((Strings.mid( strParseString, (wvarPos + 86), 1 ).equals( "N" )) ? "NO" : "SI") + "]]></CALDERA>";
    wvarResult = wvarResult + "<ASCEN><![CDATA[" + ((Strings.mid( strParseString, (wvarPos + 87), 1 ).equals( "N" )) ? "NO" : "SI") + "]]></ASCEN>";
    wvarResult = wvarResult + "<ALARMA><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 90), 40 ) ) + "]]></ALARMA>";
    wvarResult = wvarResult + "<GUARDIA><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 132), 40 ) ) + "]]></GUARDIA>";
    wvarResult = wvarResult + "<REJAS><![CDATA[" + ((Strings.mid( strParseString, (wvarPos + 172), 1 ).equals( "N" )) ? "NO" : "SI") + "]]></REJAS>";
    wvarResult = wvarResult + "<PURBLIN><![CDATA[" + ((Strings.mid( strParseString, (wvarPos + 173), 1 ).equals( "N" )) ? "NO" : "SI") + "]]></PURBLIN>";
    wvarResult = wvarResult + "<DISY><![CDATA[" + ((Strings.mid( strParseString, (wvarPos + 174), 1 ).equals( "N" )) ? "NO" : "SI") + "]]></DISY>";
    wvarResult = wvarResult + "<PROP><![CDATA[" + ((Strings.mid( strParseString, (wvarPos + 196), 1 ).equals( "N" )) ? "NO" : "SI") + "]]></PROP>";
    wvarResult = wvarResult + "<ZONA><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 243), 20 ) ) + "]]></ZONA>";
    wvarResult = wvarResult + "<DOMI><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 263), 59 ) ) + "]]></DOMI>";
    wvarResult = wvarResult + "<PROVI><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 324), 25 ) ) + "]]></PROVI>";
    wvarResult = wvarResult + "<LOCALI><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 349), 38 ) ) + "]]></LOCALI>";
    wvarResult = wvarResult + "<CODPOS><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 387), 8 ) ) + "]]></CODPOS>";
    wvarResult = wvarResult + "</DATOS>";

    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
