package com.qbe.services.ovmqriesgos.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ovmqriesgos.impl.ModGeneral;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Variant;
/**
 *  Parametros XML de Configuracion
 */

public class ModGeneral
{
  public static final String gcteConfFileName = "LBAVirtualMQConfig.xml";
  public static final String gcteQueueManager = "//QUEUEMANAGER";
  public static final String gctePutQueue = "//PUTQUEUE";
  public static final String gcteGetQueue = "//GETQUEUE";
  public static final String gcteGMOWaitInterval = "//GMO_WAITINTERVAL";
  /**
   *  Parametros XML del Cotizador
   */
  public static final String gcteParamFileName = "ParametrosMQ.xml";
  public static final String gcteNodosHogar = "//HOGAR";
  public static final String gcteNodosAutoScoring = "//AUTOSCORING";
  public static final String gcteNodosGenerales = "//GENERALES";
  public static final String gcteRAMOPCOD = "/RAMOPCOD";
  public static final String gctePLANNCOD = "/PLANNCOD";
  public static final String gctePOLIZSEC = "/POLIZSEC";
  public static final String gctePOLIZANN = "/POLIZANN";
  public static final String gcteBANCOCOD = "/BANCOCOD";
  public static final String gcteSUCURCOD = "/SUCURCOD";
  public static final String gcteCIAASCOD = "/CIAASCOD";
  public static final String gcteClassMQConnection = "WD.Frame2MQ";

  static String FormatearNumero( Variant pvarNumero ) throws Exception
  {
    String FormatearNumero = "";
    XmlDomExtended wobjXML = null;
    XmlDomExtended wobjXSL = null;

    wobjXML = new XmlDomExtended();
    wobjXML.loadXML( "<PARAM>" + pvarNumero + "</PARAM>" );

    wobjXSL = new XmlDomExtended();
    wobjXSL.loadXML( p_GetXSLNumero() );

    FormatearNumero = wobjXML.transformNode( wobjXSL ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
    return FormatearNumero;
  }

  private static String p_GetXSLNumero() throws Exception
  {
    String p_GetXSLNumero = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='/'>";

    wvarStrXSL = wvarStrXSL + "<xsl:value-of select=\"format-number(number(PARAM), '###.###,00', 'european')\"/>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLNumero = wvarStrXSL;
    return p_GetXSLNumero;
  }
}
