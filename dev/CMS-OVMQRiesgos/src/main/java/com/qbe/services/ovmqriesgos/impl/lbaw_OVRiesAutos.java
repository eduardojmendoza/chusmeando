package com.qbe.services.ovmqriesgos.impl;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import java.io.File;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ovmqriesgos.impl.ModGeneral;
import diamondedge.util.*;
import com.qbe.vbcompat.format.VBFixesUtil;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVRiesAutos implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * 
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQRiesgos.lbaw_OVRiesAutos";
  static final String mcteOpID = "1202";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_Producto = "//PRODUCTO";
  static final String mcteParam_Poliza = "//POLIZA";
  static final String mcteParam_Certi = "//CERTI";
  static final String mcteParam_Suplenum = "//SUPLENUM";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarProducto = "";
    String wvarPoliza = "";
    String wvarCerti = "";
    String wvarSuplenum = "";
    Variant wvarPos = new Variant();
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarProducto = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Producto )  ) + Strings.space( 4 ), 4 );
      wvarPoliza = Strings.right( Strings.fill( 8, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Poliza )  ), 8 );
      wvarCerti = Strings.right( Strings.fill( 14, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Certi )  ), 14 );
      if( wobjXMLRequest.selectNodes( mcteParam_Suplenum ) .getLength() != 0 )
      {
        wvarSuplenum = Strings.right( Strings.fill( 4, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Suplenum )  ), 4 );
      }
      else
      {
        wvarSuplenum = Strings.fill( 4, "0" );
      }
      //
      wvarStep = 20;
      wobjXMLRequest = null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los par�metros generales (ParametrosMQ.xml)
      wvarStep = 60;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      //
      wvarStep = 70;
      wobjXMLParametros = null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + "000000000    000000000  000000000  000000000" + wvarProducto + wvarPoliza + wvarCerti + wvarSuplenum;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();

      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 190;
      wvarResult = "";
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos.set( 97 );
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 200;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 210;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 240;
        wvarResult = "";
        wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        //
        if( !Strings.trim( wvarResult ).equals( "" ) )
        {
          wvarStep = 300;
          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
          //
        }
        else
        {
          //
          wvarStep = 270;
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          //
        }
      }
      //
      wvarStep = 320;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
	try 
	{
		//~~~~~~~~~~~~~~~
			mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";

    //Es un solo dato.
    wvarResult = wvarResult + "<DATOS>";
    //
    wvarResult = wvarResult + "<COMUNES>";
    wvarResult = wvarResult + "<MOTOR><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 25 ) ) + "]]></MOTOR>";
    wvarResult = wvarResult + "<PATENTE><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 25 ) ).toInt(), 10 ) ) + "]]></PATENTE>";
    wvarResult = wvarResult + "<CHASIS><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 35 ) ).toInt(), 30 ) ) + "]]></CHASIS>";
    wvarResult = wvarResult + "<NROVEH><![CDATA[" + Obj.toDecimal( Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 65 ) ).toInt(), 9 ) ) ) + "]]></NROVEH>";
    wvarResult = wvarResult + "<MARCA><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 79 ) ).toInt(), 20 ) ) + "]]></MARCA>";
    wvarResult = wvarResult + "<MODELO><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 104 ) ).toInt(), 20 ) ) + "]]></MODELO>";
    wvarResult = wvarResult + "<SUBMODEL><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 129 ) ).toInt(), 20 ) ) + "]]></SUBMODEL>";
    wvarResult = wvarResult + "<ADIC><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 154 ) ).toInt(), 20 ) ) + "]]></ADIC>";
    wvarResult = wvarResult + "<ORIGEN><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 175 ) ).toInt(), 20 ) ) + "]]></ORIGEN>";
    wvarResult = wvarResult + "<USO><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 198 ) ).toInt(), 20 ) ) + "]]></USO>";
    wvarResult = wvarResult + "<VTV><![CDATA[" + ((Strings.mid( strParseString, wvarPos.add( new Variant( 218 ) ).toInt(), 1 ).equals( "S" )) ? "SI" : "NO") + "]]></VTV>";
    wvarResult = wvarResult + "<VTVFEC><![CDATA[" + ((Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 219 ) ).toInt(), 10 ) ).equals( "00/00/0000" )) ? "" : Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 219 ) ).toInt(), 10 ) )) + "]]></VTVFEC>";
    wvarResult = wvarResult + "<COLOR><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 232 ) ).toInt(), 20 ) ) + "]]></COLOR>";
    wvarResult = wvarResult + "<KM><![CDATA[" + Obj.toDecimal( Strings.mid( strParseString, wvarPos.add( new Variant( 252 ) ).toInt(), 7 ) ) + "]]></KM>";
    wvarResult = wvarResult + "<CATEG><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 261 ) ).toInt(), 20 ) ) + "]]></CATEG>";
    wvarResult = wvarResult + "<TIPVEH><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 285 ) ).toInt(), 20 ) ) + "]]></TIPVEH>";
    wvarResult = wvarResult + "<ANIOFAB><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 305 ) ).toInt(), 7 ) ) + "]]></ANIOFAB>";
    wvarResult = wvarResult + "<GUARGARA><![CDATA[" + ((Strings.mid( strParseString, wvarPos.add( new Variant( 312 ) ).toInt(), 1 ).equals( "S" )) ? "SI" : "NO") + "]]></GUARGARA>";
    wvarResult = wvarResult + "<GUARDOMI><![CDATA[" + ((Strings.mid( strParseString, wvarPos.add( new Variant( 313 ) ).toInt(), 1 ).equals( "S" )) ? "SI" : "NO") + "]]></GUARDOMI>";
    wvarResult = wvarResult + "<RADI><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 316 ) ).toInt(), 20 ) ) + "]]></RADI>";
    wvarResult = wvarResult + "<PROV><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 338 ) ).toInt(), 20 ) ) + "]]></PROV>";
    wvarResult = wvarResult + "<ZONA><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 362 ) ).toInt(), 20 ) ) + "]]></ZONA>";
    wvarResult = wvarResult + "<CODPOS><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 382 ) ).toInt(), 8 ) ) + "]]></CODPOS>";
    wvarResult = wvarResult + "</COMUNES>";
    //
    wvarResult = wvarResult + "<LECTINI>";
    wvarResult = wvarResult + "<CIAANT><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 394 ) ).toInt(), 20 ) ) + "]]></CIAANT>";
    wvarResult = wvarResult + "<ANTIG><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 414 ) ).toInt(), 2 ) ) + "]]></ANTIG>";
    wvarResult = wvarResult + "<CANTSINI><![CDATA[" + Obj.toDecimal( Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 416 ) ).toInt(), 2 ) ) ) + "]]></CANTSINI>";
    wvarResult = wvarResult + "<KMINI><![CDATA[" + Obj.toDecimal( Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 418 ) ).toInt(), 6 ) ) ) + "]]></KMINI>";
    wvarResult = wvarResult + "<GNC><![CDATA[" + ((Strings.mid( strParseString, wvarPos.add( new Variant( 424 ) ).toInt(), 1 ).equals( "S" )) ? "SI" : "NO") + "]]></GNC>";
    wvarResult = wvarResult + "</LECTINI>";
    //
    wvarResult = wvarResult + "<CONDUCTORES>";
    while( !Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 425 ) ).toInt(), 20 ) ).equals( "" ) )
    {
      wvarResult = wvarResult + "<CONDUCTOR>";
      wvarResult = wvarResult + "<APENOM><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 425 ) ).toInt(), 20 ) ) + ", " + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 445 ) ).toInt(), 30 ) ) + "]]></APENOM>";
      wvarResult = wvarResult + "<TIPDOC><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 477 ) ).toInt(), 20 ) ) + "]]></TIPDOC>";
      wvarResult = wvarResult + "<NRODOC><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 497 ) ).toInt(), 11 ) ) + "]]></NRODOC>";
      wvarResult = wvarResult + "<ESTCIV><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 508 ) ).toInt(), 10 ) ) + "]]></ESTCIV>";
      wvarResult = wvarResult + "<SEXO><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 518 ) ).toInt(), 10 ) ) + "]]></SEXO>";
      wvarResult = wvarResult + "<FECNAC><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 528 ) ).toInt(), 10 ) ) + "]]></FECNAC>";
      wvarResult = wvarResult + "<TIPO><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 538 ) ).toInt(), 1 ) ) + "]]></TIPO>";
      wvarResult = wvarResult + "<VINC><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 539 ) ).toInt(), 2 ) ) + "]]></VINC>";
      wvarResult = wvarResult + "</CONDUCTOR>";
      //
      wvarPos.set( wvarPos.add( new Variant( 116 ) ) );
    }
    wvarResult = wvarResult + "</CONDUCTORES>";
    //
    wvarPos.set( 97 );
    wvarResult = wvarResult + "<ACCESOS>";
    while( !Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 1585 ) ).toInt(), 4 ) ).equals( "0000" ) )
    {
      wvarResult = wvarResult + "<ACCESO>";
      wvarResult = wvarResult + "<COD><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 1589 ) ).toInt(), 30 ) ) + "]]></COD>";
      wvarResult = wvarResult + "<SUMAAS><![CDATA[" + Obj.toDecimal( Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 1619 ) ).toInt(), 14 ) ) ) + "]]></SUMAAS>";
      wvarResult = wvarResult + "<DESC><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 1633 ) ).toInt(), 30 ) ) + "]]></DESC>";
      wvarResult = wvarResult + "</ACCESO>";
      wvarPos.set( wvarPos.add( new Variant( 78 ) ) );
    }
    wvarResult = wvarResult + "</ACCESOS>";
    //
    wvarPos.set( 97 );
    wvarResult = wvarResult + "<ADICIONALES>";
    while( !Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 2365 ) ).toInt(), 11 ) ).equals( "" ) )
    {
      wvarResult = wvarResult + "<ASEGADIC>";
      wvarResult = wvarResult + "<DOCUADIC><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 2365 ) ).toInt(), 11 ) ) + "]]></DOCUADIC>";
      wvarResult = wvarResult + "<APEADIC><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 2376 ) ).toInt(), 49 ) ) + "]]></APEADIC>";
      wvarResult = wvarResult + "</ASEGADIC>";
      wvarPos.set( wvarPos.add( new Variant( 60 ) ) );
    }
    wvarResult = wvarResult + "</ADICIONALES>";
    //
    wvarResult = wvarResult + "</DATOS>";
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
