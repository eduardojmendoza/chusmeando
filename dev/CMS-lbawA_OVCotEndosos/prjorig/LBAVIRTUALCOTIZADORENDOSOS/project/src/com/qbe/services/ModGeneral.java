package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 *  Parametros XML de Configuracion
 */

public class ModGeneral
{
  public static final String gcteQueueManager = "//QUEUEMANAGER";
  public static final String gctePutQueue = "//PUTQUEUE";
  public static final String gcteGetQueue = "//GETQUEUE";
  public static final String gcteGMOWaitInterval = "//GMO_WAITINTERVAL";
  public static final String gcteClassMQConnection = "WD.Frame2MQ";

  public static String MidAsString( String pvarStringCompleto, Variant pvarActualCounter, int pvarLongitud ) throws Exception
  {
    String MidAsString = "";
    MidAsString = Strings.mid( pvarStringCompleto, pvarActualCounter.toInt(), pvarLongitud );
    pvarActualCounter.set( pvarActualCounter.add( new Variant( pvarLongitud ) ) );
    return MidAsString;
  }

  public static String CompleteZero( String pvarString, int pvarLongitud ) throws Exception
  {
    String CompleteZero = "";
    int wvarCounter = 0;
    String wvarstrTemp = "";
    for( wvarCounter = 1; wvarCounter <= pvarLongitud; wvarCounter++ )
    {
      wvarstrTemp = wvarstrTemp + "0";
    }
    CompleteZero = Strings.right( wvarstrTemp + pvarString, pvarLongitud );
    return CompleteZero;
  }

  public static String GetErrorInformacionDato( org.w3c.dom.Node pobjXMLContenedor, String pvarPathDato, String pvarTipoDato, org.w3c.dom.Node pobjLongitud, org.w3c.dom.Node pobjDecimales, org.w3c.dom.Node pobjDefault, org.w3c.dom.Node pobjObligatorio ) throws Exception
  {
    String GetErrorInformacionDato = "";
    org.w3c.dom.Node wobjNodoValor = null;
    String wvarDatoValue = "";
    int wvarCounter = 0;
    double wvarCampoNumerico = 0.0;
    boolean wvarIsDatoObligatorio = false;

    if( ! (pobjObligatorio == (org.w3c.dom.Node) null) )
    {
      if( diamondedge.util.XmlDom.getText( pobjObligatorio ).equals( "SI" ) )
      {
        //Es un Dato Obligatorio
        wvarIsDatoObligatorio = true;
        wobjNodoValor = null /*unsup pobjXMLContenedor.selectSingleNode( "./" + Strings.mid( pvarPathDato, 3 ) ) */;
        if( wobjNodoValor == (org.w3c.dom.Node) null )
        {
          GetErrorInformacionDato = "Nodo Obligatorio " + Strings.mid( pvarPathDato, 3 ) + " NO INFORMADO";
          //unsup GoTo ClearObjects
        }
      }
    }

    wobjNodoValor = null /*unsup pobjXMLContenedor.selectSingleNode( "./" + Strings.mid( pvarPathDato, 3 ) ) */;
    if( wobjNodoValor == (org.w3c.dom.Node) null )
    {
      if( ! (pobjDefault == (org.w3c.dom.Node) null) )
      {
        wvarDatoValue = diamondedge.util.XmlDom.getText( pobjDefault );
      }
    }
    else
    {
      wvarDatoValue = diamondedge.util.XmlDom.getText( wobjNodoValor );
    }
    //
    
    if( pvarTipoDato.equals( "TEXTO" ) || pvarTipoDato.equals( "TEXTOIZQUIERDA" ) )
    {
      //Dato del Tipo String
      if( wvarIsDatoObligatorio && (Strings.trim( wvarDatoValue ).equals( "" )) )
      {
        GetErrorInformacionDato = "Campo Obligatorio " + Strings.mid( pvarPathDato, 3 ) + " SIN VALOR INGRESADO";
        //unsup GoTo ClearObjects
      }
    }
    else if( pvarTipoDato.equals( "ENTERO" ) || pvarTipoDato.equals( "DECIMAL" ) )
    {
      //Dato del Tipo Numerico
      if( wvarDatoValue.equals( "" ) )
      {
        wvarDatoValue = "0";
      }
      if( ! (new Variant( wvarDatoValue ).isNumeric()) )
      {
        GetErrorInformacionDato = "Campo " + Strings.mid( pvarPathDato, 3 ) + " CON FORMATO INVALIDO (Valor Informado: " + wvarDatoValue + ". Requerido: Numerico)";
        //unsup GoTo ClearObjects
      }
      else
      {
        if( wvarIsDatoObligatorio && (Obj.toDouble( wvarDatoValue ) == 0) )
        {
          GetErrorInformacionDato = "Campo Obligatorio " + Strings.mid( pvarPathDato, 3 ) + " SIN VALOR INGRESADO";
          //unsup GoTo ClearObjects
        }
      }
    }
    else if( pvarTipoDato.equals( "FECHA" ) )
    {
      //Dato del Tipo "Fecha" Debe venir del tipo dd/mm/yyyy devuelve YYYYMMDD
      if( wvarIsDatoObligatorio && ! (wvarDatoValue.matches( "*/*/*" )) )
      {
        GetErrorInformacionDato = "Campo Obligatorio " + Strings.mid( pvarPathDato, 3 ) + " VALOR INGRESADO EN FORMATO INVALIDO (Valor Informado: " + wvarDatoValue + ". Requerido: dd/mm/yyyy)";
      }
    }
    //
    ClearObjects: 
    wobjNodoValor = (org.w3c.dom.Node) null;
    return GetErrorInformacionDato;
  }

  public static org.w3c.dom.Node GetRequestRetornado( diamondedge.util.XmlDom pobjXMLRequest, org.w3c.dom.Node pobjXMLRequestDef, String pvarStrRetorno ) throws Exception
  {
    org.w3c.dom.Node GetRequestRetornado = null;
    org.w3c.dom.Node wobjNodoRequestDef = null;
    org.w3c.dom.Node wobjNodoVectorDef = null;
    org.w3c.dom.Node wobjNewNodo = null;
    org.w3c.dom.Element wobjNewNodoPadre = null;
    int pvarStartCount = 0;
    String wvarLastValue = "";
    int wvarCount = 0;
    Variant pvarNameArray = new Variant();

    //Desestimo el Numero de mensaje
    pvarStartCount = 5;
    //
    for( int nwobjNodoRequestDef = 0; nwobjNodoRequestDef < pobjXMLRequestDef.getChildNodes().getLength(); nwobjNodoRequestDef++ )
    {
      wobjNodoRequestDef = pobjXMLRequestDef.getChildNodes().item( nwobjNodoRequestDef );
      //
      if( wobjNodoRequestDef.getAttributes().getNamedItem( "Cantidad" ) == (org.w3c.dom.Node) null )
      {
        //No proceso los vectores del Request
        if( null /*unsup pobjXMLRequest.selectSingleNode( ("//" + diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Nombre" ) )) ) */ == (org.w3c.dom.Node) null )
        {
          wobjNewNodo = pobjXMLRequest.getDocument().createElement( diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Nombre" ) ) );
          pobjXMLRequest.getDocument().getChildNodes().item( 0 ).appendChild( wobjNewNodo );
        }
        else
        {
          wobjNewNodo = null /*unsup pobjXMLRequest.selectSingleNode( "//" + diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Nombre" ) ) ) */;
        }
        //
        if( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) == (org.w3c.dom.Node) null )
        {
          wvarLastValue = Strings.mid( pvarStrRetorno, pvarStartCount, Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Enteros" ) ) ) );
        }
        else
        {
          wvarLastValue = Strings.mid( pvarStrRetorno, pvarStartCount, Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Enteros" ) ) + diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) ) ) );
        }
        //
        
        if( diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "TipoDato" ) ).equals( "ENTERO" ) )
        {
          diamondedge.util.XmlDom.setText( wobjNewNodo, String.valueOf( VB.val( wvarLastValue ) ) );
        }
        else if( diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "TipoDato" ) ).equals( "DECIMAL" ) )
        {
          diamondedge.util.XmlDom.setText( wobjNewNodo, String.valueOf( VB.val( wvarLastValue ) / Math.pow( 10, VB.val( diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) ) ) ) ) );
        }
        else if( diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "TipoDato" ) ).equals( "FECHA" ) )
        {
          diamondedge.util.XmlDom.setText( wobjNewNodo, Strings.right( wvarLastValue, 2 ) + "/" + Strings.mid( wvarLastValue, 5, 2 ) + "/" + Strings.left( wvarLastValue, 4 ) );
        }
        else
        {
          diamondedge.util.XmlDom.setText( wobjNewNodo, Strings.trim( wvarLastValue ) );
        }
        if( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) == (org.w3c.dom.Node) null )
        {
          pvarStartCount = pvarStartCount + Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Enteros" ) ) );
        }
        else
        {
          pvarStartCount = pvarStartCount + Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Enteros" ) ) ) + Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) ) );
        }
      }
      else
      {
        // spliteo el nombre del array y me fijo si existe
        pvarNameArray.set( (com.qbe.services.Variant[]) VB.initArray( new com.qbe.services.Variant[2], com.qbe.services.Variant.class ) );
        pvarNameArray.set( Strings.split( diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Nombre" ) ), "/", -1 ) );
        //Me fijo si existen los array , sino los creo
        //Salteo todos los registros del vector
        for( wvarCount = 1; wvarCount <= Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Cantidad" ) ) ); wvarCount++ )
        {

          //creo el nodo
          wobjNewNodoPadre = pobjXMLRequest.getDocument().createElement( pvarNameArray.getValueAt( 1 ).toString() );
          for( int nwobjNodoVectorDef = 0; nwobjNodoVectorDef < wobjNodoRequestDef.getChildNodes().getLength(); nwobjNodoVectorDef++ )
          {
            wobjNodoVectorDef = wobjNodoRequestDef.getChildNodes().item( nwobjNodoVectorDef );
            wobjNewNodo = pobjXMLRequest.getDocument().createElement( diamondedge.util.XmlDom.getText( wobjNodoVectorDef.getAttributes().getNamedItem( "Nombre" ) ) );
            wobjNewNodoPadre.appendChild( wobjNewNodo );
            if( wobjNodoVectorDef.getAttributes().getNamedItem( "Decimales" ) == (org.w3c.dom.Node) null )
            {
              wvarLastValue = Strings.mid( pvarStrRetorno, pvarStartCount, Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodoVectorDef.getAttributes().getNamedItem( "Enteros" ) ) ) );
            }
            else
            {
              wvarLastValue = Strings.mid( pvarStrRetorno, pvarStartCount, Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodoVectorDef.getAttributes().getNamedItem( "Enteros" ) ) + diamondedge.util.XmlDom.getText( wobjNodoVectorDef.getAttributes().getNamedItem( "Decimales" ) ) ) );
            }
            
            if( diamondedge.util.XmlDom.getText( wobjNodoVectorDef.getAttributes().getNamedItem( "TipoDato" ) ).equals( "ENTERO" ) )
            {
              diamondedge.util.XmlDom.setText( wobjNewNodo, String.valueOf( VB.val( wvarLastValue ) ) );
            }
            else if( diamondedge.util.XmlDom.getText( wobjNodoVectorDef.getAttributes().getNamedItem( "TipoDato" ) ).equals( "DECIMAL" ) )
            {
              diamondedge.util.XmlDom.setText( wobjNewNodo, String.valueOf( VB.val( wvarLastValue ) / Math.pow( 10, VB.val( diamondedge.util.XmlDom.getText( wobjNodoVectorDef.getAttributes().getNamedItem( "Decimales" ) ) ) ) ) );
            }
            else if( diamondedge.util.XmlDom.getText( wobjNodoVectorDef.getAttributes().getNamedItem( "TipoDato" ) ).equals( "FECHA" ) )
            {
              diamondedge.util.XmlDom.setText( wobjNewNodo, Strings.right( wvarLastValue, 2 ) + "/" + Strings.mid( wvarLastValue, 5, 2 ) + "/" + Strings.left( wvarLastValue, 4 ) );
            }
            else
            {
              diamondedge.util.XmlDom.setText( wobjNewNodo, Strings.trim( wvarLastValue ) );
            }
            if( wobjNodoVectorDef.getAttributes().getNamedItem( "Decimales" ) == (org.w3c.dom.Node) null )
            {
              pvarStartCount = pvarStartCount + Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodoVectorDef.getAttributes().getNamedItem( "Enteros" ) ) );
            }
            else
            {
              pvarStartCount = pvarStartCount + Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodoVectorDef.getAttributes().getNamedItem( "Enteros" ) ) ) + Obj.toInt( diamondedge.util.XmlDom.getText( wobjNodoVectorDef.getAttributes().getNamedItem( "Decimales" ) ) );
            }
          }
          // asigno a los hijos
          /*unsup pobjXMLRequest.selectSingleNode( "//" + pvarNameArray.getValueAt( 0 ) ) */.appendChild( wobjNewNodoPadre );
          wobjNewNodoPadre = (org.w3c.dom.Element) null;
        }

      }
      //
    }
    //
    GetRequestRetornado = pobjXMLRequest.getDocument().getChildNodes().item( 0 );
    wobjNewNodo = (org.w3c.dom.Node) null;
    wobjNodoRequestDef = (org.w3c.dom.Node) null;
    return GetRequestRetornado;
  }
}
