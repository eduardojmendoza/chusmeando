package com.qbe.services.cotizadorEndosos.impl;

import java.util.Iterator;

import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import net.sf.saxon.expr.flwor.ForClause;

/**
 * 
 * Objetos del FrameWork
 * 
 */

public class Lbaw_OVDatosRiesgoHogar implements VBObjectClass {

	/**
	 * 
	 * Implementacion de los objetos
	 * 
	 * Datos de la accion
	 * 
	 */

	static final String mcteClassName = "lbawA_OVMQ.Lbaw_OVRetencionesComisiones";

	static final String mcteOpID = "0044";

	/**
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 **/
	static final String CIAASCOD = "//CIAASCOD";
	static final String RAMOPCOD = "//RAMOPCOD";
	static final String POLIZANN = "//POLIZANN";
	static final String POLIZSEC = "//POLIZSEC";
	static final String CERTIPOL = "//CERTIPOL";
	static final String CERTIANN = "//CERTIANN";
	static final String CERTISEC = "//CERTISEC";
	static final String SUPLENUM = "//SUPLENUM";
	static final String PLANNCOD = "//PLANNCOD";

	// <ENTRADA>
	// (\S{4150}) 4424

	// <CAMPO Nombre="CIAASCOD" TipoDato="TEXTO" Enteros="4" Default="0001"/>
	static final String mcteParam_Ciaascod = "//CIAASCOD";
	// <CAMPO Nombre="USUARCOD" TipoDato="TEXTO" Enteros="10"/>
	static final String mcteParam_Usuarcod = "//USUARCOD";
	// <CAMPO Nombre="ESTADO" TipoDato="TEXTO" Enteros="2"/>
	static final String mcteParam_Estado = "//ESTADO";
	// <CAMPO Nombre="ERROR" TipoDato="TEXTO" Enteros="2"/>
	static final String mcteParam_Error = "//ERROR";
	// <CAMPO Nombre="CLIENSECAS" TipoDato="ENTERO" Enteros="9"/>
	static final String mcteParam_Cliensecas = "//CLIENSECAS";
	// <CAMPO Nombre="NIVELCLAAS" TipoDato="TEXTO" Enteros="2"/>
	static final String mcteParam_Nivelclaas = "//NIVELCLAAS";
	// <CAMPO Nombre="NIVELCLA1" TipoDato="TEXTO" Enteros="2"/>
	static final String mcteParam_Nivelcla1 = "//NIVELCLA1";
	// <CAMPO Nombre="CLIENSEC1" TipoDato="ENTERO" Enteros="9"/>
	static final String mcteParam_Cliensec1 = "//CLIENSEC1";
	// <CAMPO Nombre="NIVELCLA2" TipoDato="TEXTO" Enteros="2"/>
	static final String mcteParam_Nivelcla2 = "//NIVELCLA2";
	// <CAMPO Nombre="CLIENSEC2" TipoDato="ENTERO" Enteros="9"/>
	static final String mcteParam_Cliensec2 = "//CLIENSEC2";
	// <CAMPO Nombre="NIVELCLA3" TipoDato="TEXTO" Enteros="2"/>
	static final String mcteParam_Nivelcla3 = "//NIVELCLA3";
	// <CAMPO Nombre="CLIENSEC3" TipoDato="ENTERO" Enteros="9"/>
	static final String mcteParam_Cliensec3 = "//CLIENSEC3";
	// <CAMPO Nombre="CLIENSEC" TipoDato="ENTERO" Enteros="9"/>
	static final String mcteParam_Cliensec = "//CLIENSEC";
	// <CAMPO Nombre="LIQUISEC" TipoDato="ENTERO" Enteros="6"/>
	static final String mcteParam_Liquisec = "//LIQUISEC";
	// <CAMPO Nombre="TIPOCTAC" TipoDato="TEXTO" Enteros="1"/>
	static final String mcteParam_Tipoctac = "//TIPOCTAC";
	// <CAMPO Nombre="RECIBANN" TipoDato="ENTERO" Enteros="2"/>
	static final String mcteParam_Recibann = "//RECIBANN";
	// <CAMPO Nombre="RECIBTIP" TipoDato="ENTERO" Enteros="1"/>
	static final String mcteParam_Recibtip = "//RECIBTIP";
	// <CAMPO Nombre="RECIBSEC" TipoDato="ENTERO" Enteros="6"/>
	static final String mcteParam_Recibsec = "//RECIBSEC";
	// <CAMPO Nombre="RAMOPCOD" TipoDato="TEXTO" Enteros="4"/>
	static final String mcteParam_Ramopcod = "//RAMOPCOD";
	// <CAMPO Nombre="POLIZANN" TipoDato="ENTERO" Enteros="2" Default="0"/>
	static final String mcteParam_Polizann = "//POLIZANN";
	// <CAMPO Nombre="POLIZSEC" TipoDato="ENTERO" Enteros="6" Default="0"/>
	static final String mcteParam_Polizsec = "//POLIZSEC";
	// <CAMPO Nombre="CERTIPOL" TipoDato="ENTERO" Enteros="4" Default="0"/>
	static final String mcteParam_Certipol = "//CERTIPOL";
	// <CAMPO Nombre="CERTIANN" TipoDato="ENTERO" Enteros="4" Default="0"/>
	static final String mcteParam_Certiann = "//CERTIANN";
	// <CAMPO Nombre="CERTISEC" TipoDato="ENTERO" Enteros="6" Default="0"/>
	static final String mcteParam_certisec = "//CERTISEC";
	// <CAMPO Nombre="SUPLENUM" TipoDato="ENTERO" Enteros="4"/>
	static final String mcteParam_Suplenum = "//SUPLENUM";
	// <CAMPO Nombre="OPERAPOL" TipoDato="ENTERO" Enteros="18" Default="0"/>
	static final String mcteParam_Operapol = "//OPERAPOL";
	// </ENTRADA>
	//

	private Object mobjCOM_Context = null;

	private EventLog mobjEventLog = new EventLog();

	/**
	 * 
	 * static variable for method: IAction_Execute
	 * 
	 */

	private final String wcteFnName = "IAction_Execute";

	@Override
	public int IAction_Execute(String Request, StringHolder Response, String ContextInfo) {

		int IAction_Execute = 0;

		Variant vbLogEventTypeError = new Variant();

		XmlDomExtended wobjXMLRequest = null;

		XmlDomExtended wobjXMLConfig = null;

		XmlDomExtended wobjXMLParametros = null;

		int wvarMQError = 0;

		StringBuffer wvarArea = new StringBuffer();

		MQProxy wobjFrame2MQ = null;

		int wvarStep = 0;

		String wvarResult = "";

		String ciaascod = "";
		String ramopcod = "";
		String polizann = "";
		String polizsec = "";
		String certipol = "";
		String certiann = "";
		String certisec = "";
		String suplenum = "";
		String planncod = "";

		String wvarCiaAsCod = "";

		String wvarUsuario = "";

		



		String wvarProducto = "";

		String wvarPoliza = "";

		String wvarCerti = "";

		String wvarSuplenum = "";

		int wvarPos = 0;

		int wvarMaxLen = 0;

		String strParseString = "";

		int wvarstrLen = 0;

		String wvarEstado = "";

		try

		{

			wvarStep = 10;

			// Levanto los par�metros que llegan desde la p�gina

			wobjXMLRequest = new XmlDomExtended();

			wobjXMLRequest.loadXML(Request);

			// Deber� venir desde la p�gina

			/**
			 * 
			 * <CAMPO Nombre="CIAASCOD" TipoDato="TEXTO" Enteros="4" Default=
			 * "0001"/> <CAMPO Nombre="RAMOPCOD" TipoDato="TEXTO" Enteros="4"/>
			 * <CAMPO Nombre="POLIZANN" TipoDato="ENTERO" Enteros="2"/> <CAMPO
			 * Nombre="POLIZSEC" TipoDato="ENTERO" Enteros="6"/> <CAMPO Nombre=
			 * "CERTIPOL" TipoDato="ENTERO" Enteros="4"/> <CAMPO Nombre=
			 * "CERTIANN" TipoDato="ENTERO" Enteros="4"/> <CAMPO Nombre=
			 * "CERTISEC" TipoDato="ENTERO" Enteros="6"/> <CAMPO Nombre=
			 * "SUPLENUM" TipoDato="ENTERO" Enteros="4"/> <CAMPO Nombre=
			 * "PLANNCOD" TipoDato="ENTERO" Enteros="3"/>
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 
			 **/

			ciaascod = Strings
					.left(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(CIAASCOD)) + Strings.space(4), 4);

			ramopcod = Strings
					.left(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(RAMOPCOD)) + Strings.space(4), 4);

			polizann = Strings
					.right(Strings.fill(2, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(POLIZANN)), 2);
			polizsec = Strings
					.right(Strings.fill(6, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(POLIZSEC)), 6);
			certipol = Strings
					.right(Strings.fill(4, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(CERTIPOL)), 4);
			certiann = Strings
					.right(Strings.fill(4, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(CERTIANN)), 4);
			certisec = Strings
					.right(Strings.fill(6, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(CERTISEC)), 6);
			suplenum = Strings
					.right(Strings.fill(4, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(SUPLENUM)), 4);
			planncod = Strings
					.right(Strings.fill(3, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(PLANNCOD)), 3);

			if (wobjXMLRequest.selectNodes(mcteParam_Suplenum).getLength() != 0)

			{

				wvarSuplenum = Strings.right(Strings.fill(4, "0")
						+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Suplenum)), 4);

			}

			else

			{

				wvarSuplenum = Strings.fill(4, "0");

			}
			
			if (ciaascod.equals("    ")){
				ciaascod ="0001";
			}

			//

			//

			wvarStep = 20;

			wobjXMLRequest = null;

			//

			wvarStep = 30;

			// Levanto los datos de la cola de MQ del archivo de configuraci�n

			wobjXMLConfig = new XmlDomExtended();

			wobjXMLConfig.load(
					Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));

			//

			// Levanto los Parametros de la cola de MQ del archivo de
			// configuraci�n

			wvarStep = 60;

			wobjXMLParametros = new XmlDomExtended();

			// wobjXMLParametros.load(
			// Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));

			// wvarCiaAsCod = XmlDomExtended.getText(
			// wobjXMLParametros.selectSingleNode(ModGeneral.gcteNodosGenerales
			// + ModGeneral.gcteCIAASCOD));

			wobjXMLParametros = null;

			//

			wvarStep = 80;

			wvarArea.append(mcteOpID);
			wvarArea.append(ciaascod);
			wvarArea.append(ramopcod);
			wvarArea.append(polizann);
			wvarArea.append(polizsec);
			wvarArea.append(certipol);
			wvarArea.append(certiann);
			wvarArea.append(certisec);
			wvarArea.append(suplenum);
			wvarArea.append(planncod);

			XmlDomExtended
					.setText(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL"),
							String.valueOf(VB
									.val(XmlDomExtended
											.getText(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL")))
									* 36));
			/*
			 * wobjFrame2MQ = MQProxy.getInstance(wvarConfFileName);
			 * StringHolder strParseStringHolder = new StringHolder(
			 * strParseString); wvarMQError = wobjFrame2MQ.execute(wvarOpID +
			 * wvarArea, strParseStringHolder); strParseString =
			 * strParseStringHolder.getValue();
			 * 
			 */

			wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
			StringHolder strParseStringHolder = new StringHolder(strParseString);
			wvarMQError = wobjFrame2MQ.execute(wvarArea.toString(), strParseStringHolder);
			strParseString = strParseStringHolder.getValue();

			//

			if (wvarMQError != 0)

			{

				Response.set("<Response><Estado resultado=" + String.valueOf((char) (34)) + "false"
						+ String.valueOf((char) (34)) + " mensaje=" + String.valueOf((char) (34))
						+ "El servicio de consulta no se encuentra disponible" + String.valueOf((char) (34)) + " />"
						+ "Codigo Error:" + wvarMQError + "</Response>");

				mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"),
						mcteClassName + "--" + mcteClassName,
						wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - "
								+ strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(),
						vbLogEventTypeError);

				IAction_Execute = 1;

				/* TBD mobjCOM_Context.SetAbort() ; */

				return IAction_Execute;

			}

			wvarStep = 180;

			wvarResult = "";

			// cantidad de caracteres ocupados por par�metros de entrada

			wvarPos = Strings.len(wvarArea.toString()) + 1 - 4; // menos 4 porque el
															// mensaje no esta
			wvarMaxLen = 4424; // 4424
			//

			wvarstrLen = Strings.len(strParseString);
			strParseString = Strings.mid(strParseString, 1, (wvarMaxLen + wvarPos));
			
			

			//

			wvarStep = 190;

			// Corto el estado

			wvarEstado = Strings.mid(strParseString, 38, 2);

			if (wvarEstado.equals("ER")) {
				wvarStep = 200;

				Response.set(
						"<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>");
			} else {
				//

				wvarStep = 230;

				wvarResult = wvarResult + "<CAMPOS>";

				wvarResult = wvarResult + parseoMensaje(wvarPos, strParseString, wvarstrLen);

				//

				wvarStep = 240;

				wvarResult = wvarResult + "</CAMPOS>";

				//

				wvarStep = 250;

				Response.set("<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>");

			}

			//

			wvarStep = 260;

			/* TBD mobjCOM_Context.SetComplete() ; */

			IAction_Execute = 0;

			//

			// ~~~~~~~~~~~~~~~

			// ~~~~~~~~~~~~~~~

			// LIBERO LOS OBJETOS

			wobjXMLConfig = null;

			return IAction_Execute;

			//

			// ~~~~~~~~~~~~~~~

		}

		catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
			Response.set(
					"<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>");
			return 3;
		} catch (Exception _e_) {
			Err.set(_e_);
			java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());

			logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);

			try

			{

				// ~~~~~~~~~~~~~~~

				//

				mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName,
						wcteFnName, wvarStep, Err.getError().getNumber(),
						"Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:"
								+ mcteOpID + wvarCiaAsCod + wvarUsuario + wvarProducto + wvarPoliza + wvarCerti
								+ wvarSuplenum + " Hora:" + DateTime.now(),
						vbLogEventTypeError);

				IAction_Execute = 1;

				/* TBD mobjCOM_Context.SetAbort() ; */

				Err.clear();

				return IAction_Execute;

			}

			catch (Exception _e2_)

			{

			}

		}

		return IAction_Execute;

	}

	private String parseoMensaje(int wvarPos, String strParseString, int wvarstrLen) throws Exception

	{
		/**
		 * 
		 * 
		<AREA Nombre="CAMPOS" Pattern="(\S{2})(\S{60})(\S{60})(\S{2})(\S{2})(\S{2})(\S{1})(\S{1})(\S{2})(\S{2})(\S{1})(\S{1})(\S{1})(\S{1})(\S{1})(\S{2})(\S{5})(\S{40})(\S{5})(\S{4})(\S{4})(\S{1})(\S{40})(\S{8})(\S{2})(\S{2})(\S{8})(\S{14})(\S{4150})">
			<AREA Nombre="ESTADO" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="POLIZDES" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="PLANNDES" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="TIVIVCOD" Visible="SI" TipoDato="ENTERO"/>
			<AREA Nombre="ALTURNUM" Visible="SI" TipoDato="ENTERO"/>
			<AREA Nombre="USOTIPOS" Visible="SI" TipoDato="ENTERO"/>
			<AREA Nombre="SWCALDER" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="SWASCENS" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="ALARMTIP" Visible="SI" TipoDato="ENTERO"/>
			<AREA Nombre="GUARDTIP" Visible="SI" TipoDato="ENTERO"/>
			<AREA Nombre="SWCREJAS" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="SWPBLIMP" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="SWDISYUN" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="ZORIECOD" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="SWPROPIE" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="MASCOCOD" Visible="SI" TipoDato="ENTERO"/>
			<AREA Nombre="DOMICCAL" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="DOMICDOM" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="DOMICDNU" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="DOMICPIS" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="DOMICPTA" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="DOMICESC" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="DOMICPOB" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="CPACODPO" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="PROVICOD" Visible="SI" TipoDato="ENTERO"/>
			<AREA Nombre="PAISSCOD" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="BLOCKCOD" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="DOCOUNTR" Visible="SI" TipoDato="TEXTO"/>
			<AREA Nombre="REGS" NombrePorOcurrencia="REG" Pattern="(\S{3})(\S{2})(\S{15})(\S{60})(\S{3})">
				<AREA Nombre="COBERCOD" Visible="SI" CData="SI" TipoDato="ENTERO"/>
				<AREA Nombre="CONTRMOD" Visible="SI" CData="SI" TipoDato="ENTERO"/>
				<AREA Nombre="CAPITASG" Visible="SI" CData="SI" TipoDato="ENTERO"/>
				<AREA Nombre="COBERDES" Visible="SI" CData="SI" TipoDato="TEXTO"/>
				<AREA Nombre="COBEBCOD" Visible="SI" CData="SI" TipoDato="ENTERO"/>
			</AREA>
		</AREA>
				**/

		String wvarResult = "";
		//0001HOM100221284000000000287890000000OKPERSONAL DEL HSBC GROUP II                                 |                                                            010001NN0101SNS S00     CATRIEL                                 1328          BELLA VISTA ,ESTAFETA N  1              1661    0200        000000000     10021000000000000000                                                            00010121000000000000000                                                            00012321000000000000000                                                            00012421000000000000000                                                            00012621000000000000000                                                            00012721000000000000000                                                            00012821000000000000000                                                            00012921000000000000000                                                            00013421000000000000000                                                            00020208000000000000000                                                            00020821000000000000000                                                            00025615000000000000000                                                            00025715000000000000000                                                            00027101000000000000000                                                            00027401000000000000000                                                            00028201000000000000000                                                            00028301000000000000000                                                            00030521000000000000000                                                            00070001000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            00000000000000000000000                                                            000
		
		
		wvarResult = wvarResult + "<ESTADO>" + Strings.mid(strParseString, (wvarPos), 2).trim() + "</ESTADO>";

		wvarResult = wvarResult + "<POLIZDES>" + Strings.mid(strParseString, wvarPos + 2, 60).trim() + "</POLIZDES>";

		wvarResult = wvarResult + "<PLANNDES>" + Strings.mid(strParseString, wvarPos + 62, 60).trim() + "</PLANNDES>";

		wvarResult = wvarResult + "<TIVIVCOD>" + Strings.mid(strParseString, (wvarPos + 122), 2).trim() + "</TIVIVCOD>";

		wvarResult = wvarResult + "<ALTURNUM>" + Strings.mid(strParseString, (wvarPos + 124), 2).trim() + "</ALTURNUM>";

		wvarResult = wvarResult + "<USOTIPOS>" + Strings.mid(strParseString, (wvarPos + 126), 2).trim() + "</USOTIPOS>";

		wvarResult = wvarResult + "<SWCALDER>" + Strings.mid(strParseString, (wvarPos + 128), 1).trim() + "</SWCALDER>";

		wvarResult = wvarResult + "<SWASCENS>" + Strings.mid(strParseString, wvarPos + 129, 1).trim() + "</SWASCENS>";

		wvarResult = wvarResult + "<ALARMTIP>" + Strings.mid(strParseString, wvarPos + 130, 2).trim() + "</ALARMTIP>";

		wvarResult = wvarResult + "<GUARDTIP>" + Strings.mid(strParseString, (wvarPos + 132), 2).trim() + "</GUARDTIP>";

		wvarResult = wvarResult + "<SWCREJAS>" + Strings.mid(strParseString, (wvarPos + 134), 1).trim() + "</SWCREJAS>";

		wvarResult = wvarResult + "<SWPBLIMP>" + Strings.mid(strParseString, (wvarPos + 135), 1).trim() + "</SWPBLIMP>";

		wvarResult = wvarResult + "<SWDISYUN>" + Strings.mid(strParseString, (wvarPos + 136), 1).trim() + "</SWDISYUN>";

		wvarResult = wvarResult + "<ZORIECOD>" + Strings.mid(strParseString, wvarPos + 137, 1).trim() + "</ZORIECOD>";

		wvarResult = wvarResult + "<SWPROPIE>" + Strings.mid(strParseString, (wvarPos + 138), 1).trim() + "</SWPROPIE>";

		wvarResult = wvarResult + "<MASCOCOD>" + Strings.mid(strParseString, (wvarPos + 139), 2).trim() + "</MASCOCOD>";

		wvarResult = wvarResult + "<DOMICCAL>" + Strings.mid(strParseString, (wvarPos + 141), 5).trim() + "</DOMICCAL>";

		wvarResult = wvarResult + "<DOMICDOM>" + Strings.mid(strParseString, (wvarPos + 146), 40).trim() + "</DOMICDOM>";
		wvarResult = wvarResult + "<DOMICDNU>" + Strings.mid(strParseString, wvarPos + 186, 5).trim() + "</DOMICDNU>";		
		wvarResult = wvarResult + "<DOMICPIS>" + Strings.mid(strParseString, (wvarPos + 191), 4).trim() + "</DOMICPIS>";

		wvarResult = wvarResult + "<DOMICPTA>" + Strings.mid(strParseString, (wvarPos + 195), 4).trim() + "</DOMICPTA>";

		wvarResult = wvarResult + "<DOMICESC>" + Strings.mid(strParseString, (wvarPos + 199), 1).trim() + "</DOMICESC>";

		wvarResult = wvarResult + "<DOMICPOB>" + Strings.mid(strParseString, (wvarPos + 200), 40).trim() + "</DOMICPOB>";

		wvarResult = wvarResult + "<CPACODPO>" + Strings.mid(strParseString, (wvarPos + 240), 8).trim() + "</CPACODPO>";

		wvarResult = wvarResult + "<PROVICOD>" + Strings.mid(strParseString, (wvarPos + 248), 2).trim() + "</PROVICOD>";
		wvarResult = wvarResult + "<PAISSCOD>" + Strings.mid(strParseString, wvarPos + 250, 2).trim() + "</PAISSCOD>";

		wvarResult = wvarResult + "<BLOCKCOD>" + Strings.mid(strParseString, (wvarPos + 252), 8).trim() + "</BLOCKCOD>";

		wvarResult = wvarResult + "<DOCOUNTR>" + Strings.mid(strParseString, (wvarPos + 260), 14).trim() + "</DOCOUNTR>";
		
		
		// Es un solo dato.
//		int wvarCantLin = Integer.valueOf(Strings.mid(strParseString, wvarPos + 274, 6));
		//wvarResult = wvarResult + "<CANDEVUE>" + wvarCantLin + "</CANDEVUE>";
		wvarResult = wvarResult + "<REGS>";
		wvarResult = parseoReg(Strings.mid(strParseString, wvarPos + 274, 4424), wvarResult);
		wvarResult = wvarResult + "</REGS>";


		return wvarResult;

	}
	
	private String parseoReg(String regString, String wvarResult){
		VbScript_RegExp wobjRegExp = new VbScript_RegExp();
		
		wobjRegExp.setGlobal( true ); 
		wobjRegExp.setPattern("(\\S{3})(\\S{2})(\\S{15})(\\S{60})(\\S{3})");
		//MatchCollection wobjColMatch = wobjRegExp.Execute("10030000000000000000____________________________________________________________00010130000000000000000____________________________________________________________00012330000000000000000____________________________________________________________00012430000000000000000____________________________________________________________00012630000000000000000____________________________________________________________00012730000000000000000____________________________________________________________00012830000000000000000____________________________________________________________00012930000000000000000____________________________________________________________00013430000000000000000____________________________________________________________00020830000000000000000____________________________________________________________00025609000000000000000____________________________________________________________00025709000000000000000____________________________________________________________00027101000000000000000____________________________________________________________00027401000000000000000____________________________________________________________00028201000000000000000____________________________________________________________00028301000000000000000____________________________________________________________00030530000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________00000000000000000000000____________________________________________________________000");
		MatchCollection wobjColMatch = wobjRegExp.Execute(Strings.replace(regString, " ", "_" ));
		for (int i = 0; i < wobjColMatch.getCount(); i++) {
						
			Match match = wobjColMatch.next();
			if (match.SubMatches(0).equals("000") && match.SubMatches(1).equals("00") && Integer.valueOf(match.SubMatches(2))==0 && match.SubMatches(4).equals("000")){
				break;
			}
			
			wvarResult = wvarResult + "<REG>";	
			
			
			wvarResult = wvarResult + "<COBERCOD><![CDATA[" + match.SubMatches(0) + "]]></COBERCOD>";
		
			wvarResult = wvarResult + "<CONTRMOD><![CDATA[" + match.SubMatches(1) + "]]></CONTRMOD>";
		
			wvarResult = wvarResult + "<CAPITASG><![CDATA[" + Integer.valueOf(match.SubMatches(2)) + "]]></CAPITASG>";
		
			wvarResult = wvarResult + "<COBERDES><![CDATA[" + match.SubMatches(3).replace("_", "").trim() + "]]></COBERDES>";
		
			wvarResult = wvarResult + "<COBEBCOD><![CDATA[" + Integer.valueOf(match.SubMatches(4)) + "]]></COBEBCOD>";
			
			wvarResult = wvarResult + "</REG>";
		}
		
		
		return wvarResult;
	}
	
	

	public void Activate() throws Exception

	{

		//

		/* TBD mobjCOM_Context = this.GetObjectContext() ; */

		//

	}

	public boolean CanBePooled() throws Exception

	{

		boolean ObjectControl_CanBePooled = false;

		//

		ObjectControl_CanBePooled = true;

		//

		return ObjectControl_CanBePooled;

	}

	public void Deactivate() throws Exception

	{

		//

		mobjCOM_Context = (Object) null;

		mobjEventLog = null;

		//

	}

}
