package com.qbe.services.cotizadorEndosos.impl;

import com.qbe.services.mqgeneric.impl.IModGeneral;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import com.qbe.vbcompat.format.VBFixesUtil;

/**
 * Comparado con el de CMS-mqgen, este hace un procesamiento distinto de los
 * enteros y procesa nodos hijos
 * 
 * @author ramiro
 * 
 */
public class ModGeneral implements IModGeneral {

	public static final String gcteConfFileName = "LBAVirtualMQConfig.xml";
	public static final String gcteParamFileName = "ParametrosMQ.xml";

	public String MidAsString(String pvarStringCompleto, Variant pvarActualCounter, int pvarLongitud) throws Exception {
		String MidAsString = "";
		MidAsString = Strings.mid(pvarStringCompleto, pvarActualCounter.toInt(), pvarLongitud);
		pvarActualCounter.set(pvarActualCounter.add(new Variant(pvarLongitud)));
		return MidAsString;
	}

	public String CompleteZero(String pvarString, int pvarLongitud) throws Exception {
		String CompleteZero = "";
		int wvarCounter = 0;
		String wvarstrTemp = "";
		for (wvarCounter = 1; wvarCounter <= pvarLongitud; wvarCounter++) {
			wvarstrTemp = wvarstrTemp + "0";
		}
		CompleteZero = Strings.right(wvarstrTemp + pvarString, pvarLongitud);
		return CompleteZero;
	}

	public String GetErrorInformacionDato(org.w3c.dom.Node pobjXMLContenedor, String pvarPathDato, String pvarTipoDato,
			org.w3c.dom.Node pobjLongitud, org.w3c.dom.Node pobjDecimales, org.w3c.dom.Node pobjDefault,
			org.w3c.dom.Node pobjObligatorio) throws Exception {
		String GetErrorInformacionDato = "";
		org.w3c.dom.Node wobjNodoValor = null;
		String wvarDatoValue = "";
		boolean wvarIsDatoObligatorio = false;

		if (!(pobjObligatorio == (org.w3c.dom.Node) null)) {
			if (XmlDomExtended.getText(pobjObligatorio).equals("SI")) {
				// Es un Dato Obligatorio
				wvarIsDatoObligatorio = true;
				wobjNodoValor = XmlDomExtended.Node_selectSingleNode(pobjXMLContenedor,
						"./" + Strings.mid(pvarPathDato, 3));
				if (wobjNodoValor == (org.w3c.dom.Node) null) {
					GetErrorInformacionDato = "Nodo Obligatorio " + Strings.mid(pvarPathDato, 3) + " NO INFORMADO";
					return GetErrorInformacionDato;
				}
			}
		}

		wobjNodoValor = XmlDomExtended.Node_selectSingleNode(pobjXMLContenedor, "./" + Strings.mid(pvarPathDato, 3));
		if (wobjNodoValor == (org.w3c.dom.Node) null) {
			if (!(pobjDefault == (org.w3c.dom.Node) null)) {
				wvarDatoValue = XmlDomExtended.getText(pobjDefault);
			}
		} else {
			wvarDatoValue = XmlDomExtended.getText(wobjNodoValor);
		}
		//

		if (pvarTipoDato.equals("TEXTO") || pvarTipoDato.equals("TEXTOIZQUIERDA")) {
			// Dato del Tipo String
			if (wvarIsDatoObligatorio && (Strings.trim(wvarDatoValue).equals(""))) {
				GetErrorInformacionDato = "Campo Obligatorio " + Strings.mid(pvarPathDato, 3) + " SIN VALOR INGRESADO";
				return GetErrorInformacionDato;

			}
		} else if (pvarTipoDato.equals("ENTERO") || pvarTipoDato.equals("DECIMAL")) {
			// Dato del Tipo Numerico
			if (wvarDatoValue.equals("")) {
				wvarDatoValue = "0";
			}
			if (!(new Variant(wvarDatoValue).isNumeric())) {
				GetErrorInformacionDato = "Campo " + Strings.mid(pvarPathDato, 3)
						+ " CON FORMATO INVALIDO (Valor Informado: " + wvarDatoValue + ". Requerido: Numerico)";
				return GetErrorInformacionDato;
			} else {
				// DIFF con ModGeneral de CMS-mqgen
				// Antes: if( wvarIsDatoObligatorio && (Obj.toDouble(
				// wvarDatoValue ) == 0) )
				if (wvarIsDatoObligatorio && (wvarDatoValue.equals("0"))) {
					GetErrorInformacionDato = "Campo Obligatorio " + Strings.mid(pvarPathDato, 3)
							+ " SIN VALOR INGRESADO";
					return GetErrorInformacionDato;
				}
			}
		} else if (pvarTipoDato.equals("FECHA")) {
			// Dato del Tipo "Fecha" Debe venir del tipo dd/mm/yyyy devuelve
			// YYYYMMDD
			if (wvarIsDatoObligatorio && !(wvarDatoValue.matches("*/*/*"))) {
				GetErrorInformacionDato = "Campo Obligatorio " + Strings.mid(pvarPathDato, 3)
						+ " VALOR INGRESADO EN FORMATO INVALIDO (Valor Informado: " + wvarDatoValue
						+ ". Requerido: dd/mm/yyyy)";
			}
		}
		//
		// ClearObjects:
		wobjNodoValor = (org.w3c.dom.Node) null;
		return GetErrorInformacionDato;
	}

	public org.w3c.dom.Node GetRequestRetornado(XmlDomExtended pobjXMLRequest, org.w3c.dom.Node pobjXMLRequestDef,
			String pvarStrRetorno) throws Exception {
		org.w3c.dom.Node GetRequestRetornado = null;
		org.w3c.dom.Node wobjNodoRequestDef = null;
		org.w3c.dom.Node wobjNodoVectorDef = null;
		org.w3c.dom.Node wobjNewNodo = null;
		org.w3c.dom.Element wobjNewNodoPadre = null;
		int pvarStartCount = 0;
		String wvarLastValue = "";
		int wvarCount = 0;
		Variant pvarNameArray = new Variant();

		// No desestimo el Numero de mensaje porque el 0059 **NO LO MANDA**
		pvarStartCount = 1;
		//
		for (int nwobjNodoRequestDef = 0; nwobjNodoRequestDef < pobjXMLRequestDef.getChildNodes()
				.getLength(); nwobjNodoRequestDef++) {
			wobjNodoRequestDef = pobjXMLRequestDef.getChildNodes().item(nwobjNodoRequestDef);
			//
			if (wobjNodoRequestDef.getAttributes().getNamedItem("Cantidad") == (org.w3c.dom.Node) null) {
				// No proceso los vectores del Request

				String nombreNodo = XmlDomExtended.getText(wobjNodoRequestDef.getAttributes().getNamedItem("Nombre"));
				if (pobjXMLRequest.selectSingleNode(("//" + XmlDomExtended.getText(
						wobjNodoRequestDef.getAttributes().getNamedItem("Nombre")))) == (org.w3c.dom.Node) null) {
					wobjNewNodo = pobjXMLRequest.getDocument().createElement(
							XmlDomExtended.getText(wobjNodoRequestDef.getAttributes().getNamedItem("Nombre")));
					pobjXMLRequest.getDocument().getChildNodes().item(0).appendChild(wobjNewNodo);
				} else {
					wobjNewNodo = pobjXMLRequest.selectSingleNode(
							"//" + XmlDomExtended.getText(wobjNodoRequestDef.getAttributes().getNamedItem("Nombre")));
				}
				//
				if (wobjNodoRequestDef.getAttributes().getNamedItem("Decimales") == (org.w3c.dom.Node) null) {
					wvarLastValue = Strings.mid(pvarStrRetorno, pvarStartCount, Obj
							.toInt(XmlDomExtended.getText(wobjNodoRequestDef.getAttributes().getNamedItem("Enteros"))));
				} else {
					wvarLastValue = Strings.mid(pvarStrRetorno, pvarStartCount,
							Obj.toInt(XmlDomExtended.getText(wobjNodoRequestDef.getAttributes().getNamedItem("Enteros"))
									+ XmlDomExtended
											.getText(wobjNodoRequestDef.getAttributes().getNamedItem("Decimales"))));
				}
				//

				// DIFF con ModGeneral de CMS-mqgen
				// if( XmlDomExtended.getText(
				// wobjNodoRequestDef.getAttributes().getNamedItem( "TipoDato" )
				// ).equals( "ENTERO" ) )
				// {
				// XmlDomExtended.setText( wobjNewNodo, String.valueOf( VBFixesUtil.val(
				// wvarLastValue ) ) );
				// }
				if (XmlDomExtended.getText(wobjNodoRequestDef.getAttributes().getNamedItem("TipoDato"))
						.equals("ENTERO")) {
					String newValue = "";
					try {
						// Esto lo hago, para que solo lo convierta a int, si
						// viene informado, en caso contrario tendrá un 0 por
						// default
						int parsedInt = Integer.parseInt(Strings.trim(wvarLastValue.toString()));
						newValue += parsedInt;
					} catch (NumberFormatException e) {
						newValue += "0";
					}
					XmlDomExtended.setText(wobjNewNodo, newValue);
				} else if (XmlDomExtended.getText(wobjNodoRequestDef.getAttributes().getNamedItem("TipoDato"))
						.equals("DECIMAL")) {
					XmlDomExtended.setText(wobjNewNodo, String.valueOf(VBFixesUtil.val(wvarLastValue) / Math.pow(10, VBFixesUtil.val(
							XmlDomExtended.getText(wobjNodoRequestDef.getAttributes().getNamedItem("Decimales"))))));
				} else if (XmlDomExtended.getText(wobjNodoRequestDef.getAttributes().getNamedItem("TipoDato"))
						.equals("FECHA")) {
					XmlDomExtended.setText(wobjNewNodo, Strings.right(wvarLastValue, 2) + "/"
							+ Strings.mid(wvarLastValue, 5, 2) + "/" + Strings.left(wvarLastValue, 4));
				} else {
					XmlDomExtended.setText(wobjNewNodo, Strings.trim(wvarLastValue));
				}
				if (wobjNodoRequestDef.getAttributes().getNamedItem("Decimales") == (org.w3c.dom.Node) null) {
					pvarStartCount = pvarStartCount + Obj
							.toInt(XmlDomExtended.getText(wobjNodoRequestDef.getAttributes().getNamedItem("Enteros")));
				} else {
					pvarStartCount = pvarStartCount
							+ Obj.toInt(
									XmlDomExtended.getText(wobjNodoRequestDef.getAttributes().getNamedItem("Enteros")))
							+ Obj.toInt(XmlDomExtended
									.getText(wobjNodoRequestDef.getAttributes().getNamedItem("Decimales")));
				}
			} else {
				// DIFF con ModGeneral de CMS-mqgen

				// spliteo el nombre del array y me fijo si existe
				pvarNameArray.set((Variant[]) VB.initArray(new Variant[2], Variant.class));
				pvarNameArray.set(Strings.split(
						XmlDomExtended.getText(wobjNodoRequestDef.getAttributes().getNamedItem("Nombre")), "/", -1));
				// Me fijo si existen los array , sino los creo
				// Salteo todos los registros del vector
				for (wvarCount = 1; wvarCount <= Obj.toInt(XmlDomExtended
						.getText(wobjNodoRequestDef.getAttributes().getNamedItem("Cantidad"))); wvarCount++) {

					// creo el nodo
					wobjNewNodoPadre = pobjXMLRequest.getDocument()
							.createElement(pvarNameArray.getValueAt(1).toString());
					for (int nwobjNodoVectorDef = 0; nwobjNodoVectorDef < wobjNodoRequestDef.getChildNodes()
							.getLength(); nwobjNodoVectorDef++) {
						wobjNodoVectorDef = wobjNodoRequestDef.getChildNodes().item(nwobjNodoVectorDef);
						wobjNewNodo = pobjXMLRequest.getDocument().createElement(
								XmlDomExtended.getText(wobjNodoVectorDef.getAttributes().getNamedItem("Nombre")));
						wobjNewNodoPadre.appendChild(wobjNewNodo);
						if (wobjNodoVectorDef.getAttributes().getNamedItem("Decimales") == (org.w3c.dom.Node) null) {
							wvarLastValue = Strings.mid(pvarStrRetorno, pvarStartCount, Obj.toInt(
									XmlDomExtended.getText(wobjNodoVectorDef.getAttributes().getNamedItem("Enteros"))));
						} else {
							wvarLastValue = Strings.mid(pvarStrRetorno, pvarStartCount,
									Obj.toInt(XmlDomExtended
											.getText(wobjNodoVectorDef.getAttributes().getNamedItem("Enteros"))
											+ XmlDomExtended.getText(
													wobjNodoVectorDef.getAttributes().getNamedItem("Decimales"))));
						}

						// if( XmlDomExtended.getText(
						// wobjNodoVectorDef.getAttributes().getNamedItem(
						// "TipoDato" ) ).equals( "ENTERO" ) )
						// {
						// XmlDomExtended.setText( wobjNewNodo, String.valueOf(
						// VBFixesUtil.val( wvarLastValue ) ) );
						// }
						if (XmlDomExtended.getText(wobjNodoVectorDef.getAttributes().getNamedItem("TipoDato"))
								.equals("ENTERO")) {
							String newValue = "";
							try {
								// Esto lo hago, para que solo lo convierta a
								// int, si viene informado, en caso contrario
								// tendrá un 0 por default
								int parsedInt = Integer.parseInt(Strings.trim(wvarLastValue.toString()));
								newValue += parsedInt;
							} catch (NumberFormatException e) {
								newValue += "0";
							}
							XmlDomExtended.setText(wobjNewNodo, newValue);
						} else if (XmlDomExtended.getText(wobjNodoVectorDef.getAttributes().getNamedItem("TipoDato"))
								.equals("DECIMAL")) {
							String newValue = String.valueOf(VBFixesUtil.val(wvarLastValue) / Math.pow(10, VBFixesUtil.val(XmlDomExtended
									.getText(wobjNodoVectorDef.getAttributes().getNamedItem("Decimales")))));
							if (newValue.endsWith(".0"))
								newValue = newValue.substring(0, newValue.length() - 2);
							XmlDomExtended.setText(wobjNewNodo, newValue);
						}

						else if (XmlDomExtended.getText(wobjNodoVectorDef.getAttributes().getNamedItem("TipoDato"))
								.equals("FECHA")) {
							XmlDomExtended.setText(wobjNewNodo, Strings.right(wvarLastValue, 2) + "/"
									+ Strings.mid(wvarLastValue, 5, 2) + "/" + Strings.left(wvarLastValue, 4));
						} else {
							XmlDomExtended.setText(wobjNewNodo, Strings.trim(wvarLastValue));
						}
						if (wobjNodoVectorDef.getAttributes().getNamedItem("Decimales") == (org.w3c.dom.Node) null) {
							pvarStartCount = pvarStartCount + Obj.toInt(
									XmlDomExtended.getText(wobjNodoVectorDef.getAttributes().getNamedItem("Enteros")));
						} else {
							pvarStartCount = pvarStartCount
									+ Obj.toInt(XmlDomExtended
											.getText(wobjNodoVectorDef.getAttributes().getNamedItem("Enteros")))
									+ Obj.toInt(XmlDomExtended
											.getText(wobjNodoVectorDef.getAttributes().getNamedItem("Decimales")));
						}
					}
					// asigno a los hijos
					pobjXMLRequest.selectSingleNode("//" + pvarNameArray.getValueAt(0)).appendChild(wobjNewNodoPadre);
					wobjNewNodoPadre = (org.w3c.dom.Element) null;
				}

			}
			//
		}
		//
		GetRequestRetornado = pobjXMLRequest.getDocument().getChildNodes().item(0);
		wobjNewNodo = (org.w3c.dom.Node) null;
		wobjNodoRequestDef = (org.w3c.dom.Node) null;
		return GetRequestRetornado;
	}
}
