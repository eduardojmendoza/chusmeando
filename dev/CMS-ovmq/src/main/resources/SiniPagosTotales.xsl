<?xml version="1.0" encoding="UTF-8"?>
<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2009. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: SiniPagosTotales.xsl

Fecha de Creacion: Desconocida, se agrega el 15/10/2009 por modif. ppcr para QA

PPcR: 2008-00848

Desarrollador: Jose Casais

Descripcion: Resultado msj. 1310 Consulta de pagos de siniestros
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:key name="contacts-by-CL1CL2" match="REG" use="concat(CL1,'_',CL2)"/>
	<xsl:key name="contacts-by-CL1" match="//REGS/REG" use="CL1"/>
	<xsl:template match="/">
		<EST>
			<xsl:for-each select="//REGS/REG[count(. | key('contacts-by-CL1', CL1)[1]) = 1]">
				<xsl:if test="NI1 !=''">
					<GO>
						<xsl:attribute name="CLI"><xsl:value-of select="CL1"/></xsl:attribute>
						<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
						<xsl:attribute name="NIV"><xsl:value-of select="NI1"/></xsl:attribute>
						<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
						<xsl:attribute name="ALTA"><xsl:value-of select="ALTA"/></xsl:attribute>
						<xsl:attribute name="TESO"><xsl:value-of select="TESO"/></xsl:attribute>
						<xsl:attribute name="CAJA"><xsl:value-of select="CAJA"/></xsl:attribute>
						<xsl:attribute name="RETI"><xsl:value-of select="RETI"/></xsl:attribute>
						<!-- JC 10/09 Se agregan 2 campos -->
						<xsl:attribute name="ANUL"><xsl:value-of select="ANUL"/></xsl:attribute>
						<xsl:attribute name="DEVU"><xsl:value-of select="DEVU"/></xsl:attribute>
						
						<xsl:for-each select="key('contacts-by-CL1',CL1)">
							<xsl:for-each select="(current())[count(. | key('contacts-by-CL1CL2', concat(CL1,'_',CL2))[1]) = 1]">
								<xsl:if test="NI2 !=''">
									<OR>
										<xsl:attribute name="CLI"><xsl:value-of select="CL2"/></xsl:attribute>
										<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
										<xsl:attribute name="NIV"><xsl:value-of select="NI2"/></xsl:attribute>
										<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
										<xsl:attribute name="ALTA"><xsl:value-of select="ALTA"/></xsl:attribute>
										<xsl:attribute name="TESO"><xsl:value-of select="TESO"/></xsl:attribute>
										<xsl:attribute name="CAJA"><xsl:value-of select="CAJA"/></xsl:attribute>
										<xsl:attribute name="RETI"><xsl:value-of select="RETI"/></xsl:attribute>
										<!-- JC 10/09 Se agregan 2 campos -->
										<xsl:attribute name="ANUL"><xsl:value-of select="ANUL"/></xsl:attribute>
										<xsl:attribute name="DEVU"><xsl:value-of select="DEVU"/></xsl:attribute>
						
										<xsl:for-each select="key('contacts-by-CL1CL2',concat(CL1,'_',CL2))">
											<xsl:if test="NI3 !=''">
												<PR>
													<xsl:attribute name="CLI"><xsl:value-of select="CL3"/></xsl:attribute>
													<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
													<xsl:attribute name="NIV"><xsl:value-of select="NI3"/></xsl:attribute>
													<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
													<xsl:attribute name="ALTA"><xsl:value-of select="ALTA"/></xsl:attribute>
													<xsl:attribute name="TESO"><xsl:value-of select="TESO"/></xsl:attribute>
													<xsl:attribute name="CAJA"><xsl:value-of select="CAJA"/></xsl:attribute>
													<xsl:attribute name="RETI"><xsl:value-of select="RETI"/></xsl:attribute>
													<!-- JC 10/09 Se agregan 2 campos -->
													<xsl:attribute name="ANUL"><xsl:value-of select="ANUL"/></xsl:attribute>
													<xsl:attribute name="DEVU"><xsl:value-of select="DEVU"/></xsl:attribute>
						
												</PR>
											</xsl:if>
										</xsl:for-each>
									</OR>
								</xsl:if>
								<xsl:if test="NI2 =''">
									<xsl:for-each select="key('contacts-by-CL1CL2',concat(CL1,'_',CL2))">
										<xsl:if test="NI3 !=''">
											<PR>
												<xsl:attribute name="CLI"><xsl:value-of select="CL3"/></xsl:attribute>
												<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
												<xsl:attribute name="NIV"><xsl:value-of select="NI3"/></xsl:attribute>
												<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
												<xsl:attribute name="ALTA"><xsl:value-of select="ALTA"/></xsl:attribute>
												<xsl:attribute name="TESO"><xsl:value-of select="TESO"/></xsl:attribute>
												<xsl:attribute name="CAJA"><xsl:value-of select="CAJA"/></xsl:attribute>
												<xsl:attribute name="RETI"><xsl:value-of select="RETI"/></xsl:attribute>
												<!-- JC 10/09 Se agregan 2 campos -->
												<xsl:attribute name="ANUL"><xsl:value-of select="ANUL"/></xsl:attribute>
												<xsl:attribute name="DEVU"><xsl:value-of select="DEVU"/></xsl:attribute>
						
											</PR>
										</xsl:if>
									</xsl:for-each>
								</xsl:if>
							</xsl:for-each>
						</xsl:for-each>
					</GO>
				</xsl:if>
				<xsl:if test="(NI1 ='')">
					<xsl:for-each select="key('contacts-by-CL1',CL1)">
						<xsl:for-each select="(current())[count(. | key('contacts-by-CL1CL2', concat(CL1,'_',CL2))[1]) = 1]">
							<xsl:if test="NI2 !=''">
								<OR>
									<xsl:attribute name="CLI"><xsl:value-of select="CL2"/></xsl:attribute>
									<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
									<xsl:attribute name="NIV"><xsl:value-of select="NI2"/></xsl:attribute>
									<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
									<xsl:attribute name="ALTA"><xsl:value-of select="ALTA"/></xsl:attribute>
									<xsl:attribute name="TESO"><xsl:value-of select="TESO"/></xsl:attribute>
									<xsl:attribute name="CAJA"><xsl:value-of select="CAJA"/></xsl:attribute>
									<xsl:attribute name="RETI"><xsl:value-of select="RETI"/></xsl:attribute>
									<!-- JC 10/09 Se agregan 2 campos -->
									<xsl:attribute name="ANUL"><xsl:value-of select="ANUL"/></xsl:attribute>
									<xsl:attribute name="DEVU"><xsl:value-of select="DEVU"/></xsl:attribute>
						
									<xsl:for-each select="key('contacts-by-CL1CL2',concat(CL1,'_',CL2))">
										<xsl:if test="NI3 !=''">
											<PR>
												<xsl:attribute name="CLI"><xsl:value-of select="CL3"/></xsl:attribute>
												<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
												<xsl:attribute name="NIV"><xsl:value-of select="NI3"/></xsl:attribute>
												<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
												<xsl:attribute name="ALTA"><xsl:value-of select="ALTA"/></xsl:attribute>
												<xsl:attribute name="TESO"><xsl:value-of select="TESO"/></xsl:attribute>
												<xsl:attribute name="CAJA"><xsl:value-of select="CAJA"/></xsl:attribute>
												<xsl:attribute name="RETI"><xsl:value-of select="RETI"/></xsl:attribute>
												<!-- JC 10/09 Se agregan 2 campos -->
												<xsl:attribute name="ANUL"><xsl:value-of select="ANUL"/></xsl:attribute>
												<xsl:attribute name="DEVU"><xsl:value-of select="DEVU"/></xsl:attribute>
						
											</PR>
										</xsl:if>
									</xsl:for-each>
								</OR>
							</xsl:if>
							<xsl:if test="NI2 =''">
								<xsl:for-each select="key('contacts-by-CL1CL2',concat(CL1,'_',CL2))">
									<xsl:if test="NI3 !=''">
										<PR>
											<xsl:attribute name="CLI"><xsl:value-of select="CL3"/></xsl:attribute>
											<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
											<xsl:attribute name="NIV"><xsl:value-of select="NI3"/></xsl:attribute>
											<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
											<xsl:attribute name="ALTA"><xsl:value-of select="ALTA"/></xsl:attribute>
											<xsl:attribute name="TESO"><xsl:value-of select="TESO"/></xsl:attribute>
											<xsl:attribute name="CAJA"><xsl:value-of select="CAJA"/></xsl:attribute>
											<xsl:attribute name="RETI"><xsl:value-of select="RETI"/></xsl:attribute>
											<!-- JC 10/09 Se agregan 2 campos -->
											<xsl:attribute name="ANUL"><xsl:value-of select="ANUL"/></xsl:attribute>
											<xsl:attribute name="DEVU"><xsl:value-of select="DEVU"/></xsl:attribute>
						
										</PR>
									</xsl:if>
								</xsl:for-each>
							</xsl:if>
						</xsl:for-each>
					</xsl:for-each>
				</xsl:if>
			</xsl:for-each>
		</EST>
	</xsl:template>
</xsl:stylesheet>
