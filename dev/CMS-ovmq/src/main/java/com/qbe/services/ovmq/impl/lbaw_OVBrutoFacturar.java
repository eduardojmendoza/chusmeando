package com.qbe.services.ovmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import com.qbe.services.ovmq.impl.ModGeneral;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.format.FormatMapper;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
import com.qbe.vbcompat.format.VBFixesUtil;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVBrutoFacturar implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVBrutoFacturar";
  static final String mcteOpID = "1803";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_ClienSec = "//CLIENSEC";
  static final String mcteParam_EfectAnn = "//EFECTANN";
  static final String mcteParam_EfectMes = "//EFECTMES";
  static final String mcteParam_EfectDia = "//EFECTDIA";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarClienSec = "";
    String wvarEfectAnn = "";
    String wvarEfectMes = "";
    String wvarEfectDia = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarClienSec = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec )  ), 9 );
      wvarEfectAnn = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn )  );
      wvarEfectMes = Strings.right( Strings.fill( 2, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EfectMes )  ), 2 );
      wvarEfectDia = Strings.right( Strings.fill( 2, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EfectDia )  ), 2 );
      //
      wvarStep = 20;
      wobjXMLRequest = null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los Parametros de la cola de MQ del archivo de configuraci�n
      wvarStep = 60;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      wobjXMLParametros = null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSec + "    000000000  000000000  000000000" + wvarEfectAnn + wvarEfectMes + wvarEfectDia;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 400 ) );
      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();

      //
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        return IAction_Execute;
      }

      wvarStep = 190;
      wvarResult = "";
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos = 75;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 200;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 210;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        if( Strings.trim( Strings.mid( strParseString, (wvarPos + 47), 100 ) ).equals( "" ) )
        {
          //
          wvarStep = 220;
          if( (!Strings.trim( Strings.mid( strParseString, (wvarPos + 28), 13 ) ).equals( "" )) && (!Strings.trim( Strings.mid( strParseString, (wvarPos + 28), 13 ) ).equals( "0000000000000" )) )
          {
        	  //Antes: wvarResult = wvarResult + "<IMPORTEPESOS>" + String.valueOf( Obj.toDouble( Strings.mid( strParseString, wvarPos, 13 ) ) ) + "</IMPORTEPESOS>";
        	  wvarResult = wvarResult + "<IMPORTEPESOS>" + convertsDecimal(convertsDouble(Strings.mid( strParseString, wvarPos, 13 )) ) + "</IMPORTEPESOS>";
            wvarResult = wvarResult + "<SIGNOIMPPESOS>" + Strings.mid( strParseString, (wvarPos + 13), 1 ) + "</SIGNOIMPPESOS>";
            wvarResult = wvarResult + "<IMPORTEDOLAR>" + convertsDecimal(convertsDouble(Strings.mid( strParseString, (wvarPos + 14), 13 ) )) + "</IMPORTEDOLAR>";
            wvarResult = wvarResult + "<SIGNOIMPDOLAR>" + Strings.mid( strParseString, (wvarPos + 27), 1 ) + "</SIGNOIMPDOLAR>";
            wvarResult = wvarResult + "<IMPORTETOTAL>" + convertsDecimal(convertsDouble( Strings.mid( strParseString, (wvarPos + 28), 13 ) )) + "</IMPORTETOTAL>";
            wvarResult = wvarResult + "<SIGNOIMPTOTAL>" + Strings.mid( strParseString, (wvarPos + 41), 1 ) + "</SIGNOIMPTOTAL>";
            wvarResult = wvarResult + "<COTIZACION>" + convertsDecimal( convertsDouble(Strings.mid( strParseString, (wvarPos + 42), 5 ) )) + "</COTIZACION>";
            //
            wvarStep = 230;
            Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
          }
          else
          {
            Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='No se encontraron datos.' /></Response>" );
          }
        }
        else
        {
          Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='" + Strings.trim( Strings.mid( strParseString, (wvarPos + 44), 100 ) ) + "' /></Response>" );
        }
        //
      }
      //
      wvarStep = 240;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());
      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
      try 
       {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarClienSec + wvarEfectAnn + wvarEfectMes + wvarEfectDia + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    //
    wvarResult = wvarResult + "<RS>";
    //
    while( (wvarPos.toInt() < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ).equals( "" )) )
    {
      wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 138 ) + "]]></R>";
      wvarPos.set( wvarPos.add( new Variant( 138 ) ) );
    }
    //
    wvarResult = wvarResult + "</RS>";
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REGS'>";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REG'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='PROD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,1,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,5,8)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERPOL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,13,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERANN'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,17,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERSEC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,21,6)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CLIDES'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,40,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='TOMA'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,70,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='EST'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,100,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SINI'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,130,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='ALERTEXI'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,131,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='AGE'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,132,2)' />-<xsl:value-of select='substring(.,134,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RAMO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,138,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='TOMA'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='EST'/>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    //
    return p_GetXSL;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
  
	/**
	 * Convierte los doubles con .0 en Strings sin los 2 últimos dígitos
	 * @param floatNumber
	 * @return
	 */
	private String convertsDouble(String floatNumber) {
		String parteEntera = floatNumber;
		String parteEnteraConvertida = String
				.valueOf(Obj.toDouble(parteEntera));
		if (parteEnteraConvertida.endsWith(".0"))
			parteEnteraConvertida = parteEnteraConvertida.substring(0,
					parteEnteraConvertida.length() - 2);
		return parteEnteraConvertida;
	}
	
	/**
	 * @param number
	 * @return
	 */
	private String convertsDecimal(String number){		
		
        double d = Double.valueOf(number);
        
       String output = BigDecimal.valueOf(d).toPlainString();
       if (output.endsWith(".0")){
    	   output = output.substring(0, output.length() - 2);
       }
		return output;
	}
}
