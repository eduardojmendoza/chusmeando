package com.qbe.services.ovmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import java.io.File;
import java.math.BigDecimal;

import com.qbe.services.ovmq.impl.ModGeneral;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
import com.qbe.vbcompat.format.VBFixesUtil;

/**

 * Objetos del FrameWork

 */



public class lbaw_OVCtaCteDetalle implements VBObjectClass

{

  /**

   * Implementacion de los objetos

   * Datos de la accion

   */

  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVCtaCteDetalle";

  static final String mcteOpID = "1800";

  /**

   * Parametros XML de Entrada

   */

  static final String mcteParam_Usuario = "//USUARIO";

  static final String mcteParam_NivelAs = "//NIVELAS";

  static final String mcteParam_ClienSecAs = "//CLIENSECAS";

  static final String mcteParam_ClienSec1 = "//CLIENSEC1";

  static final String mcteParam_EfectAnn = "//EFECTANN";

  static final String mcteParam_EfectMes = "//EFECTMES";

  private Object mobjCOM_Context = null;

  private EventLog mobjEventLog = new EventLog();

  /**

   * static variable for method: IAction_Execute

   */

  private final String wcteFnName = "IAction_Execute";



  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {

    int IAction_Execute = 0;

    Variant vbLogEventTypeError = new Variant();

    XmlDomExtended wobjXMLRequest = null;

    XmlDomExtended wobjXMLConfig = null;

    XmlDomExtended wobjXMLResponse = null;

    XmlDomExtended wobjXSLResponse = null;

    XmlDomExtended wobjXMLParametros = null;

    int wvarMQError = 0;

    String wvarArea = "";

    MQProxy wobjFrame2MQ = null;

    int wvarStep = 0;

    String wvarResult = "";

    String wvarCiaAsCod = "";

    String wvarNivelAs = "";

    String wvarClienSecAs = "";

    String wvarClienSec1 = "";

    String wvarUsuario = "";

    String wvarEfectAnn = "";

    String wvarEfectMes = "";

    String wvarControl = "";

    String wvarCombo = "";

    String wvarLiquidacionMensual = "";

    int wvarPos = 0;

    int wvarVecPos = 0;

    String strParseString = "";

    int wvarstrLen = 0;

    String wvarEstado = "";

    //

    //

    //

    //

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~

    try 

    {

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //

      wvarStep = 10;

      wobjXMLRequest = new XmlDomExtended();

      wobjXMLRequest.loadXML( Request );

      wvarNivelAs = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NivelAs )  ) + Strings.space( 2 ), 2 );

      wvarClienSecAs = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSecAs )  ), 9 );

      wvarClienSec1 = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec1 )  ), 9 );

      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );

      wvarEfectAnn = Strings.right( Strings.fill( 4, "0" ) + String.valueOf( DateTime.year( DateTime.toDate( "01/01/" + Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn )  ) ) ) ) ), 4 );

      wvarEfectMes = Strings.right( Strings.fill( 2, "0" ) + Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EfectMes )  ) ), 2 );

      //

      //

      wvarStep = 20;

      wobjXMLRequest = null;

      //

      wvarStep = 30;

      if( wvarEfectAnn.equals( "" ) )

      {

        wvarEfectAnn = String.valueOf( DateTime.year( DateTime.now() ) );

        wvarEfectMes = Strings.format( DateTime.month( DateTime.now() ), "00" );

      }

      //

      wvarStep = 40;

      //Levanto los datos de la cola de MQ del archivo de configuraci�n

      wobjXMLConfig = new XmlDomExtended();

      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));

      //

      //Levanto los Parametros de la cola de MQ del archivo de configuraci�n

      wvarStep = 60;

      wobjXMLParametros = new XmlDomExtended();

      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));

      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );

      wobjXMLParametros = null;

      //

      wvarStep = 80;



      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + "  " + wvarClienSec1 + "  000000000  000000000" + wvarEfectAnn + wvarEfectMes;

      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 10 ) );

      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();


      //

      if( wvarMQError != 0 )

      {

        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        return IAction_Execute;

      }



      wvarStep = 160;

      wvarResult = "";

      //

      wvarstrLen = Strings.len( strParseString );

      //

      wvarStep = 170;

      //Corto el estado

      wvarEstado = Strings.mid( strParseString, 19, 2 );

      //

      wvarPos = 73;

      if( wvarEstado.equals( "OK" ) )

      {

        //

        wvarLiquidacionMensual = "<LIQUIDAMENSUAL>" + Strings.mid( strParseString, wvarPos + 10, 1 ) + "</LIQUIDAMENSUAL>";

        wvarControl = Strings.mid( strParseString, wvarPos + 9, 1 );

        //

        if( wvarControl.equals( "N" ) )

        {

          //

          wvarStep = 180;

          wvarCombo = wvarCombo + "<SELECT name=\"cliensec\">";

          wvarCombo = wvarCombo + "<OPTION value= \"";

          wvarCombo = wvarCombo + Strings.mid( strParseString, wvarPos, 9 ) + "\">" + Strings.mid( strParseString, wvarPos, 9 );

          wvarCombo = wvarCombo + "</OPTION>";

          wvarCombo = wvarCombo + "</SELECT>";

          //

          //Se cambio la longitud del vector de Cliensec de 20*9 a 200*9 29/12/2005

          //wvarPos = wvarPos + 190

          wvarPos = wvarPos + 1 + 1810;

        }

        else

        {

          //

          wvarStep = 190;

          wvarPos = wvarPos + 10 + 1;

          wvarVecPos = 0;

          wvarCombo = "";

          wvarCombo = wvarCombo + "<SELECT name=\"cliensec\">";

          wvarStep = 200;

          while( !Strings.mid( strParseString, (wvarPos + wvarVecPos), 9 ).equals( "000000000" ) )

          {

            wvarCombo = wvarCombo + "<OPTION value=\"" + Strings.mid( strParseString, (wvarPos + wvarVecPos), 9 ) + "\">" + Strings.mid( strParseString, (wvarPos + wvarVecPos), 9 ) + "</OPTION>";

            wvarVecPos = wvarVecPos + 9;

          }

          wvarCombo = wvarCombo + "</SELECT>";

          //Se cambio la longitud del vector de Cliensec de 20*9 a 200*9 29/12/2005

          //wvarPos = wvarPos + 180

          wvarPos = wvarPos + 1800;

          //

        }

        //

        wvarStep = 210;

        wvarResult = wvarResult + "<CTACTES>";

        //

        wvarStep = 220;

        while( (wvarPos < wvarstrLen) && (!Strings.mid( strParseString, (wvarPos + 8), 6 ).equals( "000000" )) )

        {

          wvarResult = wvarResult + "<CTACTE>";

          wvarResult = wvarResult + "<FECHA>" + Strings.mid( strParseString, (wvarPos + 6), 2 ) + "/" + Strings.mid( strParseString, (wvarPos + 4), 2 ) + "/" + Strings.mid( strParseString, wvarPos, 4 ) + "</FECHA>";

          wvarResult = wvarResult + "<LIQUISEC>" + Strings.mid( strParseString, (wvarPos + 8), 6 ) + "</LIQUISEC>";

          wvarResult = wvarResult + "<MOALFCOD>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 14), 3 ) ) + "</MOALFCOD>";

          wvarResult = wvarResult + "<CONCEPTO>" + Strings.mid( strParseString, (wvarPos + 17), 1 ) + "</CONCEPTO>";

          if( Strings.mid( strParseString, (wvarPos + 17), 1 ).equals( "D" ) )

          {

        	 //Se agrega el FIX, todavía no se probó.
        	  //Antes:  wvarResult = wvarResult + "<IMPORTE>" + Strings.replace( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, (wvarPos + 18), 15 ) ) / 10000000) ), ".", "," ) + "</IMPORTE>";
        	  String parteEntera = Strings.mid( strParseString, (wvarPos + 18), 15 );       	  
        	  String parteEnteraConvertida = String.valueOf(Obj.toDouble(parteEntera)/ 10000000);
        	  if (parteEnteraConvertida.endsWith(".0"))  parteEnteraConvertida = parteEnteraConvertida.substring(0, parteEnteraConvertida.length() - 2);
        	  
//        	  wvarResult = wvarResult + "<IMPORTE>" + Strings.replace(parteEnteraConvertida, ".", "," ) + "</IMPORTE>";
        	  
        	  wvarResult = wvarResult + "<IMPORTE>" + convertsDecimal(parteEnteraConvertida)+ "</IMPORTE>";
        	  //Fin FIX

          }

          else

          {
        	//Se agrega el FIX, todavía no se probó.
        	  //Antes: wvarResult = wvarResult + "<IMPORTE>" + Strings.replace( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, (wvarPos + 18), 15 ) ) / 100) ), ".", "," ) + "</IMPORTE>";

        	  String parteEntera = Strings.mid( strParseString, (wvarPos + 18), 15 );
        	  String parteEnteraConvertida = String.valueOf(Obj.toDouble(parteEntera) / 100);
        	  if (parteEnteraConvertida.endsWith(".0"))  parteEnteraConvertida = parteEnteraConvertida.substring(0, parteEnteraConvertida.length() - 2);
//        	  wvarResult = wvarResult + "<IMPORTE>" + Strings.replace(parteEnteraConvertida, ".", "," ) + "</IMPORTE>";
        	  wvarResult = wvarResult + "<IMPORTE>" + convertsDecimal(parteEnteraConvertida)+ "</IMPORTE>";
          }

          wvarResult = wvarResult + "<SIGNO>" + ((Strings.mid( strParseString, (wvarPos + 33), 1 ).equals( "+" )) ? "" : Strings.mid( strParseString, (wvarPos + 33), 1 )) + "</SIGNO>";

          wvarResult = wvarResult + "</CTACTE>";

          wvarPos = wvarPos + 34;

        }

        //

        wvarStep = 230;

        wvarResult = wvarResult + "</CTACTES>";

        //

        wvarStep = 240;

        wobjXMLResponse = new XmlDomExtended();

        wobjXSLResponse = new XmlDomExtended();

        //

        wvarStep = 250;

        wobjXMLResponse.loadXML( wvarResult );

        //

        wvarStep = 260;

        wobjXSLResponse.loadXML( p_GetXSL());

        //

        wvarStep = 270;

        wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );

        //

        wvarStep = 280;

        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarCombo + wvarLiquidacionMensual + wvarResult + "</Response>" );

      }

      else

      {

        wvarStep = 290;

        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS " + String.valueOf( (char)(34) ) + " /></Response>" );

      }

      //

      wvarStep = 300;

      /*TBD mobjCOM_Context.SetComplete() ;*/

      IAction_Execute = 0;

      //

      ClearObjects: 

      // LIBERO LOS OBJETOS

      wobjXMLConfig = null;



      return IAction_Execute;

      //

      //~~~~~~~~~~~~~~~

    }

    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());

      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);

      try 

       {

        //~~~~~~~~~~~~~~~

        wobjXMLResponse = null;

        wobjXSLResponse = null;

        //

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarClienSecAs + wvarEfectAnn + wvarEfectMes + "00" + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        Err.clear();

        return IAction_Execute;

      }

      catch( Exception _e2_ )

      {

      }

    }

    return IAction_Execute;

  }



  private void ObjectControl_Activate() throws Exception

  {

  }



  private boolean ObjectControl_CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    return ObjectControl_CanBePooled;

  }



  private void ObjectControl_Deactivate() throws Exception

  {

  }



  private String p_GetXSL() throws Exception

  {

    String p_GetXSL = "";

    String wvarStrXSL = "";

    //

    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";

    wvarStrXSL = wvarStrXSL + "<xsl:key name=\"cuentas\" match=\"CTACTE\" use=\"FECHA\" />";



    wvarStrXSL = wvarStrXSL + " <xsl:template match='/'>";

    wvarStrXSL = wvarStrXSL + "     <xsl:element name='CTACTES'>";

    wvarStrXSL = wvarStrXSL + "         <xsl:for-each select=\"//CTACTE[count(. | key('cuentas', FECHA)[1]) = 1]\">";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CTACTE'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:element name='FECHA'>";

    wvarStrXSL = wvarStrXSL + "                      <xsl:value-of select=\"FECHA\"/>";

    wvarStrXSL = wvarStrXSL + "                 </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:element name='LIQUISEC'>";

    wvarStrXSL = wvarStrXSL + "                      <xsl:value-of select=\"LIQUISEC\"/>";

    wvarStrXSL = wvarStrXSL + "                 </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"not(//CTACTE[CONCEPTO='F' and MOALFCOD='$' and FECHA=current()/FECHA])\">";

    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='BRUTO_PESOS'>0</xsl:element>";

    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='BRUTO_PESOS_SIGNO'></xsl:element>";

    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"not(//CTACTE[CONCEPTO='F' and MOALFCOD='U$S' and FECHA=current()/FECHA])\">";

    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='BRUTO_DOLAR'>0</xsl:element>";

    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='BRUTO_DOLAR_SIGNO'></xsl:element>";

    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";



    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"not(//CTACTE[CONCEPTO='C' and MOALFCOD='$' and FECHA=current()/FECHA])\">";

    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='NETO_PESOS'>0</xsl:element>";

    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='NETO_PESOS_SIGNO'></xsl:element>";

    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"not(//CTACTE[CONCEPTO='C' and MOALFCOD='U$S' and FECHA=current()/FECHA])\">";

    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='NETO_DOLAR'>0</xsl:element>";

    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='NETO_DOLAR_SIGNO'></xsl:element>";

    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";



    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"not(//CTACTE[CONCEPTO='P' and MOALFCOD='$' and FECHA=current()/FECHA])\">";

    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='PEND_PESOS'>0</xsl:element>";

    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='PEND_PESOS_SIGNO'></xsl:element>";

    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"not(//CTACTE[CONCEPTO='P' and MOALFCOD='U$S' and FECHA=current()/FECHA])\">";

    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='PEND_DOLAR'>0</xsl:element>";

    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='PEND_DOLAR_SIGNO'></xsl:element>";

    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";



    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"not(//CTACTE[CONCEPTO='D' and MOALFCOD='U$S' and FECHA=current()/FECHA])\">";

    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='COTIZ_DOLAR'>0</xsl:element>";

    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";



    wvarStrXSL = wvarStrXSL + "                 <xsl:for-each select=\"key('cuentas', FECHA)\">";

    wvarStrXSL = wvarStrXSL + "                     <xsl:choose>";

    wvarStrXSL = wvarStrXSL + "                         <xsl:when test=\"CONCEPTO='F'\">";

    wvarStrXSL = wvarStrXSL + "                             <xsl:choose>";

    wvarStrXSL = wvarStrXSL + "                                 <xsl:when test=\"MOALFCOD='$'\">";

    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='BRUTO_PESOS'>";

    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"IMPORTE\"/>";

    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='BRUTO_PESOS_SIGNO'>";

    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"SIGNO\"/>";

    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                                 </xsl:when>";

    wvarStrXSL = wvarStrXSL + "                                 <xsl:when test=\"MOALFCOD='U$S'\">";

    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='BRUTO_DOLAR'>";

    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"IMPORTE\"/>";

    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='BRUTO_DOLAR_SIGNO'>";

    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"SIGNO\"/>";

    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                                 </xsl:when>";

    wvarStrXSL = wvarStrXSL + "                             </xsl:choose>                                                                ";

    wvarStrXSL = wvarStrXSL + "                         </xsl:when>";

    wvarStrXSL = wvarStrXSL + "                     </xsl:choose>";



    wvarStrXSL = wvarStrXSL + "                     <xsl:choose>";

    wvarStrXSL = wvarStrXSL + "                         <xsl:when test=\"CONCEPTO='C'\">";

    wvarStrXSL = wvarStrXSL + "                             <xsl:choose>";

    wvarStrXSL = wvarStrXSL + "                                 <xsl:when test=\"MOALFCOD='$'\">";

    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='NETO_PESOS'>";

    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"IMPORTE\"/>";

    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='NETO_PESOS_SIGNO'>";

    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"SIGNO\"/>";

    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                                 </xsl:when>";

    wvarStrXSL = wvarStrXSL + "                                 <xsl:when test=\"MOALFCOD='U$S'\">";

    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='NETO_DOLAR'>";

    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"IMPORTE\"/>";

    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='NETO_DOLAR_SIGNO'>";

    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"SIGNO\"/>";

    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                                 </xsl:when>";

    wvarStrXSL = wvarStrXSL + "                             </xsl:choose>                                                                ";

    wvarStrXSL = wvarStrXSL + "                         </xsl:when>";

    wvarStrXSL = wvarStrXSL + "                     </xsl:choose>";



    wvarStrXSL = wvarStrXSL + "                     <xsl:choose>";

    wvarStrXSL = wvarStrXSL + "                         <xsl:when test=\"CONCEPTO='P'\">";

    wvarStrXSL = wvarStrXSL + "                             <xsl:choose>";

    wvarStrXSL = wvarStrXSL + "                                 <xsl:when test=\"MOALFCOD='$'\">";

    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='PEND_PESOS'>";

    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"IMPORTE\"/>";

    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='PEND_PESOS_SIGNO'>";

    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"SIGNO\"/>";

    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                                 </xsl:when>";

    wvarStrXSL = wvarStrXSL + "                                 <xsl:when test=\"MOALFCOD='U$S'\">";

    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='PEND_DOLAR'>";

    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"IMPORTE\"/>";

    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='PEND_DOLAR_SIGNO'>";

    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"SIGNO\"/>";

    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                                 </xsl:when>";

    wvarStrXSL = wvarStrXSL + "                             </xsl:choose>                                                                ";

    wvarStrXSL = wvarStrXSL + "                         </xsl:when>";

    wvarStrXSL = wvarStrXSL + "                     </xsl:choose>";



    wvarStrXSL = wvarStrXSL + "                     <xsl:choose>";

    wvarStrXSL = wvarStrXSL + "                         <xsl:when test=\"CONCEPTO='D'\">";

    wvarStrXSL = wvarStrXSL + "                             <xsl:choose>";

    wvarStrXSL = wvarStrXSL + "                                 <xsl:when test=\"MOALFCOD='U$S'\">";

    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='COTIZ_DOLAR'>";

    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"IMPORTE\"/>";

    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                                 </xsl:when>";

    wvarStrXSL = wvarStrXSL + "                             </xsl:choose>                                                                ";

    wvarStrXSL = wvarStrXSL + "                         </xsl:when>";

    wvarStrXSL = wvarStrXSL + "                     </xsl:choose>";



    wvarStrXSL = wvarStrXSL + "                 </xsl:for-each>";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "      </xsl:for-each>";

    wvarStrXSL = wvarStrXSL + "   </xsl:element>";

    wvarStrXSL = wvarStrXSL + " </xsl:template>";

    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='PRODUCTOR'/>";

    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";

    //

    p_GetXSL = wvarStrXSL;

    return p_GetXSL;

  }



  public void Activate() throws Exception

  {

    //

    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/

    //

  }



  public boolean CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    //

    ObjectControl_CanBePooled = true;

    //

    return ObjectControl_CanBePooled;

  }



  public void Deactivate() throws Exception

  {

    //

    mobjCOM_Context = (Object) null;

    mobjEventLog = null;

    //

  }
  
  
	/**
	 * @param number
	 * @return
	 */
	private String convertsDecimal(String number){		
		
      double d = Double.valueOf(number);
      
     String output = BigDecimal.valueOf(d).toPlainString();
     if (output.endsWith(".0")){
    	 output = output.substring(0, output.length() - 2);
     }
		return output;
	}
}

