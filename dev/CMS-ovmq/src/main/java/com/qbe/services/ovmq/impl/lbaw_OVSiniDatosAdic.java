package com.qbe.services.ovmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import java.io.File;
import com.qbe.services.ovmq.impl.ModGeneral;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
import com.qbe.vbcompat.format.VBFixesUtil;

/**

 * Objetos del FrameWork

 */



public class lbaw_OVSiniDatosAdic implements VBObjectClass

{

  /**

   * Implementacion de los objetos

   * Datos de la accion

   */

  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVSiniDatosAdic";

  static final String mcteOpID = "1305";

  /**

   * Parametros XML de Entrada

   */

  static final String mcteParam_Usuario = "//USUARIO";

  static final String mcteParam_Producto = "//PRODUCTO";

  static final String mcteParam_NivelAs = "//NIVELAS";

  static final String mcteParam_ClienSecAs = "//CLIENSECAS";

  static final String mcteParam_Nivel1 = "//NIVEL1";

  static final String mcteParam_ClienSec1 = "//CLIENSEC1";

  static final String mcteParam_Nivel2 = "//NIVEL2";

  static final String mcteParam_ClienSec2 = "//CLIENSEC2";

  static final String mcteParam_Nivel3 = "//NIVEL3";

  static final String mcteParam_ClienSec3 = "//CLIENSEC3";

  static final String mcteParam_Estado = "//MSGEST";

  static final String mcteParam_SiniAn = "//SINIAN";

  static final String mcteParam_SiniNum = "//SININUM";

  private Object mobjCOM_Context = null;

  private EventLog mobjEventLog = new EventLog();

  /**

   * static variable for method: IAction_Execute

   */

  private final String wcteFnName = "IAction_Execute";



  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {

    int IAction_Execute = 0;

    Variant vbLogEventTypeError = new Variant();

    XmlDomExtended wobjXMLRequest = null;

    XmlDomExtended wobjXMLConfig = null;

    XmlDomExtended wobjXMLParametros = null;

    int wvarMQError = 0;

    String wvarArea = "";

    MQProxy wobjFrame2MQ = null;

    int wvarStep = 0;

    String wvarResult = "";

    String wvarCiaAsCod = "";

    String wvarUsuario = "";

    String wvarNivelAs = "";

    String wvarClienSecAs = "";

    String wvarNivel1 = "";

    String wvarClienSec1 = "";

    String wvarNivel2 = "";

    String wvarClienSec2 = "";

    String wvarNivel3 = "";

    String wvarClienSec3 = "";

    String wvarProducto = "";

    String wvarPoliza = "";

    String wvarSiniAn = "";

    String wvarSiniNum = "";

    int wvarPos = 0;

    String strParseString = "";

    int wvarstrLen = 0;

    String wvarEstado = "";

    //

    //

    //

    //

    //

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~

    try 

    {

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //

      wvarStep = 10;

      //Levanto los par�metros que llegan desde la p�gina

      wobjXMLRequest = new XmlDomExtended();

      wobjXMLRequest.loadXML( Request );

      //Deber� venir desde la p�gina

      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );

      wvarNivelAs = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NivelAs )  ) + Strings.space( 2 ), 2 );

      wvarClienSecAs = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSecAs )  ), 9 );

      wvarNivel1 = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Nivel1 )  ) + Strings.space( 2 ), 2 );

      wvarClienSec1 = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec1 )  ), 9 );

      wvarNivel2 = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Nivel2 )  ) + Strings.space( 2 ), 2 );

      wvarClienSec2 = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec2 )  ), 9 );

      wvarNivel3 = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Nivel3 )  ) + Strings.space( 2 ), 2 );

      wvarClienSec3 = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec3 )  ), 9 );



      wvarProducto = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Producto )  ) + Strings.space( 4 ), 4 );

      wvarSiniAn = Strings.right( Strings.fill( 2, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SiniAn )  ), 2 );

      wvarSiniNum = Strings.right( Strings.fill( 6, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SiniNum )  ), 6 );



      //

      //

      wvarStep = 20;

      wobjXMLRequest = null;

      //

      wvarStep = 30;

      //Levanto los datos de la cola de MQ del archivo de configuraci�n

      wobjXMLConfig = new XmlDomExtended();

      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));

      //

      //Levanto los Parametros de la cola de MQ del archivo de configuraci�n

      wvarStep = 60;

      wobjXMLParametros = new XmlDomExtended();

      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));

      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );

      wobjXMLParametros = null;

      //

      wvarStep = 80;

      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + wvarNivel1 + wvarClienSec1 + wvarNivel2 + wvarClienSec2 + wvarNivel3 + wvarClienSec3 + wvarProducto + wvarSiniAn + wvarSiniNum;

      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 36 ) );

      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();


      //

      if( wvarMQError != 0 )

      {

        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        return IAction_Execute;

      }

      //

      wvarStep = 180;

      wvarResult = "";

      //cantidad de caracteres ocupados por par�metros de entrada

      wvarPos = 79;

      //

      wvarstrLen = Strings.len( strParseString );

      //

      wvarStep = 190;

      //Corto el estado

      wvarEstado = Strings.mid( strParseString, 19, 2 );

      if( wvarEstado.equals( "OK" ) )

      {

        //

        wvarStep = 200;

        wvarResult = wvarResult + "<DATOS>";

        wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);

        //

        wvarStep = 210;

        wvarResult = wvarResult + "</DATOS>";

        //

        wvarStep = 220;

        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );

      }

      else

      {

        wvarStep = 230;

        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );

      }

      //

      wvarStep = 240;

      /*TBD mobjCOM_Context.SetComplete() ;*/

      IAction_Execute = 0;

      //

      //~~~~~~~~~~~~~~~

      ClearObjects: 

      //~~~~~~~~~~~~~~~

      // LIBERO LOS OBJETOS

      wobjXMLConfig = null;



      return IAction_Execute;

      //

      //~~~~~~~~~~~~~~~

    }

    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());

      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);

      try 

       {

        //~~~~~~~~~~~~~~~

        //

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarProducto + wvarSiniAn + wvarSiniNum + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        Err.clear();

        return IAction_Execute;

      }

      catch( Exception _e2_ )

      {

      }

    }

    return IAction_Execute;

  }



  private void ObjectControl_Activate() throws Exception

  {

  }



  private boolean ObjectControl_CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    return ObjectControl_CanBePooled;

  }



  private void ObjectControl_Deactivate() throws Exception

  {

  }



  private String ParseoMensaje( int wvarPos, String strParseString, int wvarstrLen ) throws Exception

  {

    String ParseoMensaje = "";

    String wvarResult = "";



    //Es un solo dato.

    wvarResult = wvarResult + "<DA_PROPIO><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos, 30 ) ) + "]]></DA_PROPIO>";

    wvarResult = wvarResult + "<DA_TERCE><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 30), 30 ) ) + "]]></DA_TERCE>";

    wvarResult = wvarResult + "<LE_TERCE><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 60), 30 ) ) + "]]></LE_TERCE>";

    wvarResult = wvarResult + "<CONDUTIP><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 90), 30 ) ) + "]]></CONDUTIP>";

    wvarResult = wvarResult + "<CONDUDOCU>" + Strings.mid( strParseString, (wvarPos + 120), 11 ) + "</CONDUDOCU>";

    wvarResult = wvarResult + "<CONDUAPE><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 131), 40 ) ) + "]]></CONDUAPE>";

    wvarResult = wvarResult + "<CONDUNOM><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 171), 30 ) ) + "]]></CONDUNOM>";

    //

    ParseoMensaje = wvarResult;



    return ParseoMensaje;

  }



  public void Activate() throws Exception

  {

    //

    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/

    //

  }



  public boolean CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    //

    ObjectControl_CanBePooled = true;

    //

    return ObjectControl_CanBePooled;

  }



  public void Deactivate() throws Exception

  {

    //

    mobjCOM_Context = (Object) null;

    mobjEventLog = null;

    //

  }

}

