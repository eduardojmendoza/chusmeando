package com.qbe.services.ovmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import java.io.File;
import com.qbe.services.ovmq.impl.ModGeneral;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
import com.qbe.vbcompat.format.VBFixesUtil;
/**

 * Objetos del FrameWork

 */



public class lbaw_OVDatosGralCliente implements VBObjectClass

{

  /**

   * Implementacion de los objetos

   * Datos de la accion

   */

  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVDatosGralCliente";

  static final String mcteOpID = "1102";

  /**

   * Parametros XML de Entrada

   */

  static final String mcteParam_Usuario = "//USUARIO";

  static final String mcteParam_Producto = "//PRODUCTO";

  static final String mcteParam_Poliza = "//POLIZA";

  static final String mcteParam_Certi = "//CERTI";

  static final String mcteParam_Suplenum = "//SUPLENUM";

  private Object mobjCOM_Context = null;

  private EventLog mobjEventLog = new EventLog();

  /**

   * static variable for method: IAction_Execute

   */

  private final String wcteFnName = "IAction_Execute";



  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {

    int IAction_Execute = 0;

    Variant vbLogEventTypeError = new Variant();

    XmlDomExtended wobjXMLRequest = null;

    XmlDomExtended wobjXMLConfig = null;

    XmlDomExtended wobjXMLParametros = null;

    int wvarMQError = 0;

    String wvarArea = "";

    MQProxy wobjFrame2MQ = null;

    int wvarStep = 0;

    String wvarResult = "";

    String wvarCiaAsCod = "";

    String wvarUsuario = "";

    String wvarProducto = "";

    String wvarPoliza = "";

    String wvarCerti = "";

    String wvarSuplenum = "";

    Variant wvarPos = new Variant();

    String strParseString = "";

    int wvarstrLen = 0;

    String wvarEstado = "";

    //

    //

    //

    //

    //

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~

    try 

    {

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //

      wvarStep = 10;

      //Levanto los par�metros que llegan desde la p�gina

      wobjXMLRequest = new XmlDomExtended();

      wobjXMLRequest.loadXML( Request );

      //Deber� venir desde la p�gina

      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );

      wvarProducto = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Producto )  ) + Strings.space( 4 ), 4 );

      wvarPoliza = Strings.right( Strings.fill( 8, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Poliza )  ), 8 );

      wvarCerti = Strings.right( Strings.fill( 14, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Certi )  ), 14 );

      if( wobjXMLRequest.selectNodes( mcteParam_Suplenum ) .getLength() != 0 )

      {

        wvarSuplenum = Strings.right( Strings.fill( 4, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Suplenum )  ), 4 );

      }

      else

      {

        wvarSuplenum = Strings.fill( 4, "0" );

      }

      //

      //

      wvarStep = 20;

      wobjXMLRequest = null;

      //

      wvarStep = 30;

      //Levanto los datos de la cola de MQ del archivo de configuraci�n

      wobjXMLConfig = new XmlDomExtended();

      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));

      //

      //Levanto los Parametros de la cola de MQ del archivo de configuraci�n

      wvarStep = 60;

      wobjXMLParametros = new XmlDomExtended();

      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));

      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );

      wobjXMLParametros = null;

      //

      wvarStep = 80;

      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarProducto + wvarPoliza + wvarCerti + wvarSuplenum;

      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 36 ) );

      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();


      //

      if( wvarMQError != 0 )

      {

        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        return IAction_Execute;

      }

      wvarStep = 180;

      wvarResult = "";

      //cantidad de caracteres ocupados por par�metros de entrada

      wvarPos.set( 97 );

      //

      wvarstrLen = Strings.len( strParseString );

      //

      wvarStep = 190;

      //Corto el estado

      wvarEstado = Strings.mid( strParseString, 19, 2 );

      if( wvarEstado.equals( "OK" ) )

      {

        //

        wvarStep = 200;

        wvarResult = wvarResult + "<DATOS>";

        wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);

        //

        wvarStep = 210;

        wvarResult = wvarResult + "</DATOS>";

        //

        wvarStep = 220;

        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );

      }

      else

      {

        wvarStep = 230;

        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );

      }

      //

      wvarStep = 240;

      /*TBD mobjCOM_Context.SetComplete() ;*/

      IAction_Execute = 0;

      //

      //~~~~~~~~~~~~~~~

      ClearObjects: 

      //~~~~~~~~~~~~~~~

      // LIBERO LOS OBJETOS

      wobjXMLConfig = null;



      return IAction_Execute;

      //

      //~~~~~~~~~~~~~~~

    }

    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());

      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);

      try 

       {

        //~~~~~~~~~~~~~~~

        //

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarProducto + wvarPoliza + wvarCerti + wvarSuplenum + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        Err.clear();

        return IAction_Execute;

      }

      catch( Exception _e2_ )

      {

      }

    }

    return IAction_Execute;

  }



  private void ObjectControl_Activate() throws Exception

  {

  }



  private boolean ObjectControl_CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    return ObjectControl_CanBePooled;

  }



  private void ObjectControl_Deactivate() throws Exception

  {

  }



  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception

  {

    String ParseoMensaje = "";

    String wvarResult = "";



    //Es un solo dato.

    wvarResult = wvarResult + "<CLIENTE>";

    wvarResult = wvarResult + "<CLIENSEC>" + Strings.mid( strParseString, wvarPos.toInt(), 9 ) + "</CLIENSEC>";

    wvarResult = wvarResult + "<CLIENDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 9 ) ).toInt(), 60 ) ) + "]]></CLIENDES>";

    wvarResult = wvarResult + "<TIPODOCU><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 75 ) ).toInt(), 8 ) ) + "]]></TIPODOCU>";

    wvarResult = wvarResult + "<NUMEDOCU>" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 83 ) ).toInt(), 11 ) ) + "</NUMEDOCU>";

    wvarResult = wvarResult + "<NACIONALI><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 94 ) ).toInt(), 40 ) ) + "]]></NACIONALI>";

    wvarResult = wvarResult + "<SEXO><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 134 ) ).toInt(), 9 ) ) + "]]></SEXO>";

    wvarResult = wvarResult + "<FECNAC><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 143 ) ).toInt(), 10 ) ) + "]]></FECNAC>";

    wvarResult = wvarResult + "<ESTCIVIL><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 153 ) ).toInt(), 10 ) ) + "]]></ESTCIVIL>";

    wvarResult = wvarResult + "<FUMADOR><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 163 ) ).toInt(), 2 ) ) + "]]></FUMADOR>";

    wvarResult = wvarResult + "<CALLEPART><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 182 ) ).toInt(), 60 ) ) + "]]></CALLEPART>";

    wvarResult = wvarResult + "<CPOPART><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 242 ) ).toInt(), 8 ) ) + "]]></CPOPART>";

    wvarResult = wvarResult + "<LOCALPART><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 250 ) ).toInt(), 40 ) ) + "]]></LOCALPART>";

    wvarResult = wvarResult + "<PROVPART><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 290 ) ).toInt(), 20 ) ) + "]]></PROVPART>";

    wvarResult = wvarResult + "</CLIENTE>";



    wvarPos.set( wvarPos.add( new Variant( 310 ) ) );

    wvarResult = wvarResult + "<CONTACTOS>";

    while( (wvarPos.toInt() < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 3 ) ).toInt(), 30 ) ).equals( "" )) )

    {

      //

      wvarResult = wvarResult + "<CONTACTO>";

      wvarResult = wvarResult + "<DESCRIPCION><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 3 ) ).toInt(), 30 ) ) + "]]></DESCRIPCION>";

      wvarResult = wvarResult + "<DATOCONT><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 33 ) ).toInt(), 50 ) ) + "]]></DATOCONT>";

      wvarResult = wvarResult + "</CONTACTO>";

      //

      wvarPos.set( wvarPos.add( new Variant( 83 ) ) );

    }

    wvarResult = wvarResult + "</CONTACTOS>";

    ParseoMensaje = wvarResult;



    return ParseoMensaje;

  }



  public void Activate() throws Exception

  {

    //

    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/

    //

  }



  public boolean CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    //

    ObjectControl_CanBePooled = true;

    //

    return ObjectControl_CanBePooled;

  }



  public void Deactivate() throws Exception

  {

    //

    mobjCOM_Context = (Object) null;

    mobjEventLog = null;

    //

  }

}

