package com.qbe.services.ovmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import com.qbe.services.ovmq.impl.ModGeneral;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
import com.qbe.vbcompat.format.VBFixesUtil;

/**

 * Objetos del FrameWork

 */



public class lbaw_OVLOGetCantRecibos implements VBObjectClass

{

  /**

   * Implementacion de los objetos

   * Datos de la accion

   */

  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVLOGetCantRecibos";

  static final String mcteOpID = "1415";

  /**

   * Parametros XML de Entrada

   */

  static final String mcteParam_Usuario = "//USUARIO";

  static final String mcteParam_NivelAs = "//NIVELAS";

  static final String mcteParam_ClienSecAs = "//CLIENSECAS";

  static final String mcteParam_FlagBusq = "//FLAGBUSQ";

  private Object mobjCOM_Context = null;

  private EventLog mobjEventLog = new EventLog();

  /**

   * static variable for method: IAction_Execute

   */

  private final String wcteFnName = "IAction_Execute";



  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {

    int IAction_Execute = 0;

    Variant vbLogEventTypeError = new Variant();

    XmlDomExtended wobjXMLRequest = null;

    XmlDomExtended wobjXMLConfig = null;

    XmlDomExtended wobjXMLParametros = null;

    XmlDomExtended wobjXMLResponse = null;

    XmlDomExtended wobjXSLResponse = null;

    int wvarMQError = 0;

    String wvarArea = "";

    MQProxy wobjFrame2MQ = null;

    int wvarStep = 0;

    String wvarResult = "";

    String wvarCiaAsCod = "";

    String wvarUsuario = "";

    String wvarNivelAs = "";

    String wvarClienSecAs = "";

    String wvarFlagBusq = "";

    int wvarPos = 0;

    String strParseString = "";

    int wvarstrLen = 0;

    String wvarEstado = "";

    int wvarCantLin = 0;

    //

    //

    //

    //

    //

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~

    try 

    {

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //

      wvarStep = 10;

      //Levanto los par�metros que llegan desde la p�gina

      wobjXMLRequest = new XmlDomExtended();

      wobjXMLRequest.loadXML( Request );

      //Deber� venir desde la p�gina

      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );

      wvarNivelAs = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NivelAs )  ) + Strings.space( 2 ), 2 );

      wvarClienSecAs = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSecAs )  ), 9 );

      if( wobjXMLRequest.selectNodes( mcteParam_FlagBusq ) .getLength() != 0 )

      {

        wvarFlagBusq = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FlagBusq )  ) + "T", 1 );

      }

      else

      {

        wvarFlagBusq = "T";

      }

      //

      //

      wvarStep = 20;

      wobjXMLRequest = null;

      //

      wvarStep = 30;

      //Levanto los datos de la cola de MQ del archivo de configuraci�n

      wobjXMLConfig = new XmlDomExtended();

      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));

      //

      //Levanto los Parametros de la cola de MQ del archivo de configuraci�n

      wvarStep = 60;

      wobjXMLParametros = new XmlDomExtended();

      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));

      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );

      wobjXMLParametros = null;

      //

      wvarStep = 80;

      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + "  000000000  000000000  000000000" + wvarFlagBusq;

      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 36 ) );

      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();


      //

      if( wvarMQError != 0 )

      {

        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        return IAction_Execute;

      }

      //

      wvarStep = 200;

      wvarResult = "";

      //cantidad de caracteres ocupados por par�metros de entrada

      wvarPos = 68;

      //

      wvarstrLen = Strings.len( strParseString );

      //

      wvarStep = 210;

      //Corto el estado

      wvarEstado = Strings.mid( strParseString, 19, 2 );

      //

      if( wvarEstado.equals( "ER" ) )

      {

        //

        wvarStep = 220;

        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );

        //

      }

      else

      {

        //

        wvarStep = 230;

        wvarCantLin = Obj.toInt( Strings.mid( strParseString, wvarPos + 15, 4 ) );

        wvarResult = "";

        //

        if( wvarCantLin > 100 )

        {

          //

          wvarStep = 240;

          //pantalla de ingreso de datos

          wvarEstado = "<MSGEST>ING</MSGEST>";

          //Antes:  wvarEstado = wvarEstado + "<COTIDOLAR>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, wvarPos, 15 ) ) / 10000000) ), "0.0000000" ) + " </COTIDOLAR>";
          wvarResult = wvarResult + "<OPTION value='" + Strings.mid( strParseString, wvarPos, 4 ) + "' ";
          String parteEntera = Strings.mid( strParseString, wvarPos, 15 );
          String parteEnteraConvertida = String.valueOf( Obj.toDouble( parteEntera )  / 10000000 );
          	if ( parteEnteraConvertida.endsWith(".0")) parteEnteraConvertida = parteEnteraConvertida.substring(0, parteEnteraConvertida.length()-2);
          DecimalFormat formatter = new DecimalFormat("0.0000000",  new DecimalFormatSymbols(Locale.ENGLISH));
          	double toBeFormatted = Double.valueOf(parteEnteraConvertida);
          String formateado = formatter.format(toBeFormatted);
          //Fin FIX
          wvarEstado = wvarEstado + "<COTIDOLAR>" + formateado + " </COTIDOLAR>";

          //wvarEstado = wvarEstado & CargarVectorMasivos(strParseString, wvarstrLen, wvarPos + 23219)

          wvarEstado = wvarEstado + CargarVectorMasivos(strParseString, wvarstrLen, new Variant( wvarPos + 11619 ));

          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarEstado + "</Response>" );

          //

        }

        else if( wvarCantLin == 0 )

        {

          //

          wvarStep = 250;

          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /><MSGEST>VAC</MSGEST></Response>" );

          //

        }

        else

        {

          //

          wvarStep = 260;

          //listar polizas

          wvarEstado = "<MSGEST>LIS</MSGEST>";

          //Antes: wvarEstado = wvarEstado + "<COTIDOLAR>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, wvarPos, 15 ) ) / 10000000) ), "0.0000000" ) + " </COTIDOLAR>";

          String parteEntera = Strings.mid( strParseString, wvarPos, 15 );
          String parteEnteraConvertida = String.valueOf( Obj.toDouble( parteEntera )  / 10000000 );
          	if ( parteEnteraConvertida.endsWith(".0")) parteEnteraConvertida = parteEnteraConvertida.substring(0, parteEnteraConvertida.length()-2);
          DecimalFormat formatter = new DecimalFormat("0.0000000",  new DecimalFormatSymbols(Locale.ENGLISH));
          	double toBeFormatted = Double.valueOf(parteEnteraConvertida);
          String formateado = formatter.format(toBeFormatted);
          //Fin FIX
          wvarEstado = wvarEstado + "<COTIDOLAR>" + formateado + " </COTIDOLAR>";
          
          
          wvarResult = wvarResult + ParseoMensaje(new Variant( wvarPos + 19 ), strParseString, wvarCantLin);

          //wvarEstado = wvarEstado & CargarVectorMasivos(strParseString, wvarstrLen, wvarPos + 23219)

          wvarEstado = wvarEstado + CargarVectorMasivos(strParseString, wvarstrLen, new Variant( wvarPos + 11619 ));

          //

          wvarStep = 270;

          wobjXMLResponse = new XmlDomExtended();

          wobjXSLResponse = new XmlDomExtended();

          //

          wvarStep = 280;

          wobjXMLResponse.loadXML( wvarResult );

          //

          wvarStep = 290;

          if( wobjXMLResponse.selectNodes( "//R" ) .getLength() != 0 )

          {

            wobjXSLResponse.loadXML( p_GetXSL());

            //

            wvarStep = 300;

            wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );

            //

            wvarStep = 310;

            wobjXMLResponse = null;

            wobjXSLResponse = null;

            //

            wvarStep = 320;

            Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarEstado + wvarResult + "</Response>" );

            //

          }

        }

      }

      //

      wvarStep = 330;

      /*TBD mobjCOM_Context.SetComplete() ;*/

      IAction_Execute = 0;

      //

      //~~~~~~~~~~~~~~~

      ClearObjects: 

      //~~~~~~~~~~~~~~~

      // LIBERO LOS OBJETOS

      wobjXMLConfig = null;



      return IAction_Execute;

      //

      //~~~~~~~~~~~~~~~

    }

    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());

      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);

      try 

       {

        //~~~~~~~~~~~~~~~

        //

        wobjXMLResponse = null;

        wobjXSLResponse = null;

        //

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarNivelAs + wvarClienSecAs + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        Err.clear();

        return IAction_Execute;

      }

      catch( Exception _e2_ )

      {

      }

    }

    return IAction_Execute;

  }



  private void ObjectControl_Activate() throws Exception

  {

  }



  private boolean ObjectControl_CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    return ObjectControl_CanBePooled;

  }



  private void ObjectControl_Deactivate() throws Exception

  {

  }



  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarCantLin ) throws Exception

  {

    String ParseoMensaje = "";

    String wvarResult = "";

    int wvarCounter = 0;

    //

    wvarResult = wvarResult + "<RS>";

    //

    for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )

    {

      wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 116 ) + "]]></R>";

      wvarPos.set( wvarPos.add( new Variant( 116 ) ) );

    }

    //

    wvarResult = wvarResult + "</RS>";

    //

    ParseoMensaje = wvarResult;

    //

    return ParseoMensaje;

  }



  private String CargarVectorMasivos( String strParseString, int wvarstrLen, Variant wvarPos ) throws Exception

  {

    String CargarVectorMasivos = "";

    String wvarResult = "";

    int wvarCantLin = 0;

    int wvarCounter = 0;

    //

    wvarCantLin = Obj.toInt( Strings.mid( strParseString, wvarPos.toInt(), 4 ) );

    wvarResult = wvarResult + "<PRODS>";

    //

    for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )

    {

      if( Strings.mid( strParseString, wvarPos.add( new Variant( 8 ) ).toInt(), 1 ).equals( "1" ) )

      {

        wvarResult = wvarResult + "<PROD>";

        wvarResult = wvarResult + "<![CDATA[" + Strings.mid( strParseString, wvarPos.add( new Variant( 4 ) ).toInt(), 4 ) + "]]>";

        wvarResult = wvarResult + "</PROD>";

      }

      wvarPos.set( wvarPos.add( new Variant( 5 ) ) );

    }

    //

    wvarResult = wvarResult + "</PRODS>";

    //

    CargarVectorMasivos = wvarResult;

    //

    return CargarVectorMasivos;

  }



  private String p_GetXSL() throws Exception

  {

    String p_GetXSL = "";

    String wvarStrXSL = "";

    //

    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";

    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";

    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"eeuu\" decimal-separator=\".\"/>";

    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";

    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REGS'>";

    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";

    wvarStrXSL = wvarStrXSL + "         </xsl:element>";

    wvarStrXSL = wvarStrXSL + "     </xsl:template>";

    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";

    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REG'>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CLIDES'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,1,30))' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CLIENSEC'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number(number(substring(.,31,9)), \"0\")' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='PROD'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,40,4)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POL'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,44,8)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERPOL'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,52,4)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERANN'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,56,4)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERSEC'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,60,6)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='OPERAPOL'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number(number(substring(.,66,6)), \"0\")' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RECNUM'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,72,9)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='MON'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,81,3))' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SIG'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,84,1) ='-'\">";

    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='normalize-space(substring(.,84,1))' />";

    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMP'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,85,14)) div 100), \"###.###.##0,00\", \"european\")' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMPCALC'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,85,14)) div 100), \"########0.00\", \"eeuu\")' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RENDIDO'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,99,1)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RAMO'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,100,1)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='FECVTO'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,101,10)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='AGE'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,111,2)' />-<xsl:value-of select='substring(.,113,4)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='ESTADO'>X</xsl:element>";

    //

    wvarStrXSL = wvarStrXSL + "         </xsl:element>";

    wvarStrXSL = wvarStrXSL + "     </xsl:template>";

    //

    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIDES'/>";

    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='MON'/>";

    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='FECVTO'/>";

    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='AGE'/>";

    //

    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";

    //

    p_GetXSL = wvarStrXSL;

    //

    return p_GetXSL;

  }



  /**

   * -------------------------------------------------------------------------------------------------------------------

   * // Metodos del Framework

   * -------------------------------------------------------------------------------------------------------------------

   */

  public void Activate() throws Exception

  {

    //

    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/

    //

  }



  public boolean CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    //

    ObjectControl_CanBePooled = true;

    //

    return ObjectControl_CanBePooled;

  }



  public void Deactivate() throws Exception

  {

    //

    mobjCOM_Context = (Object) null;

    mobjEventLog = null;

    //

  }

}

