package com.qbe.services.ovmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import java.io.File;
import com.qbe.services.ovmq.impl.ModGeneral;
import com.qbe.services.decimal.DecimalFormater;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
import com.qbe.vbcompat.format.VBFixesUtil;

/**

 * Objetos del FrameWork

 */



public class lbaw_OVLOGetRecibos implements VBObjectClass

{

  /**

   * Implementacion de los objetos

   * Datos de la accion

   */

  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVLOGetRecibos";

  static final String mcteOpID = "1416";

  /**

   * Parametros XML de Entrada

   */

  static final String mcteParam_Usuario = "//USUARIO";

  static final String mcteParam_NivelAs = "//NIVELAS";

  static final String mcteParam_ClienSecAs = "//CLIENSECAS";

  static final String mcteNodos_Polizas = "//Request/POLIZAS/POLIZA";

  static final String mcteParam_PolProd = "PROD";

  static final String mcteParam_PolPol = "POL";

  static final String mcteParam_PolCerti = "CERTI";

  static final String mcteParam_FlagBusq = "//FLAGBUSQ";

  private Object mobjCOM_Context = null;

  private EventLog mobjEventLog = new EventLog();

  /**

   * static variable for method: IAction_Execute

   */

  private final String wcteFnName = "IAction_Execute";



  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {

    int IAction_Execute = 0;

    Variant vbLogEventTypeError = new Variant();

    XmlDomExtended wobjXMLRequest = null;

    XmlDomExtended wobjXMLConfig = null;

    XmlDomExtended wobjXMLParametros = null;

    XmlDomExtended wobjXMLResponse = null;

    XmlDomExtended wobjXSLResponse = null;

    org.w3c.dom.NodeList wobjXMLList = null;

    int wvarMQError = 0;

    String wvarArea = "";

    MQProxy wobjFrame2MQ = null;

    int wvarStep = 0;

    String wvarResult = "";

    String wvarCiaAsCod = "";

    String wvarUsuario = "";

    String wvarNivelAs = "";

    String wvarClienSecAs = "";

    String wvarPolizas = "";

    String wvarFlagBusq = "";

    int wvarPos = 0;

    String strParseString = "";

    int wvarstrLen = 0;

    String wvarEstado = "";

    String wvarCotiz = "";

    int wvariCounter = 0;

    String wvarCont = "";

    //

    //

    //

    //

    //

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~

    try 

    {

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //

      wvarStep = 10;

      //Levanto los par�metros que llegan desde la p�gina

      wobjXMLRequest = new XmlDomExtended();

      wobjXMLRequest.loadXML( Request );

      //Deber� venir desde la p�gina

      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );

      wvarNivelAs = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NivelAs )  ) + Strings.space( 2 ), 2 );

      wvarClienSecAs = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSecAs )  ), 9 );

      //

      // INICIO POLIZAS

      wvarPolizas = "";

      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Polizas ) ;



      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )

      {

        wvarPolizas = wvarPolizas + Strings.left( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_PolProd )  ) + Strings.space( 4 ), 4 );

        wvarPolizas = wvarPolizas + Strings.right( Strings.fill( 8, "0" ) + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_PolPol )  ), 8 );

        wvarPolizas = wvarPolizas + Strings.right( Strings.fill( 14, "0" ) + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_PolCerti )  ), 14 );

        wvarPolizas = wvarPolizas + "  ";

      }



      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 39; wvariCounter++ )

      {

        wvarPolizas = wvarPolizas + Strings.fill( 4, " " ) + Strings.fill( 22, "0" ) + Strings.fill( 2, " " );

      }

      //para saber cuantos son

      wvarCont = Strings.right( Strings.fill( 3, "0" ) + wobjXMLList.getLength(), 3 );

      // FIN POLIZAS

      if( wobjXMLRequest.selectNodes( mcteParam_FlagBusq ) .getLength() != 0 )

      {

        wvarFlagBusq = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FlagBusq )  ) + "T", 1 );

      }

      else

      {

        wvarFlagBusq = "T";

      }

      //

      wvarStep = 20;

      wobjXMLRequest = null;

      //

      wvarStep = 30;

      //Levanto los datos de la cola de MQ del archivo de configuraci�n

      wobjXMLConfig = new XmlDomExtended();

      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));

      //

      //Levanto los Parametros de la cola de MQ del archivo de configuraci�n

      wvarStep = 60;

      wobjXMLParametros = new XmlDomExtended();

      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));

      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );

      wobjXMLParametros = null;

      //

      wvarStep = 80;

      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + "  000000000  000000000  000000000" + wvarFlagBusq + wvarCont + wvarPolizas;

      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 80 ) );

      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();


      //

      if( wvarMQError != 0 )

      {

        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        return IAction_Execute;

      }

      //

      wvarStep = 200;

      wvarResult = "";

      //cantidad de caracteres ocupados por par�metros de entrada

      wvarPos = 1191;

      //

      wvarstrLen = Strings.len( strParseString );

      //

      wvarStep = 210;

      //Corto el estado

      wvarEstado = Strings.mid( strParseString, 19, 2 );

      //

      if( wvarEstado.equals( "ER" ) )

      {

        //

        wvarStep = 220;

        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );

        //

      }

      else

      {

        //

        wvarStep = 230;

        wvarResult = "";

        //

        //Antes: wvarCotiz = "<COTIDOLAR>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, wvarPos, 15 ) ) / 10000000) ), "0.0000000" ) + " </COTIDOLAR>";
        wvarCotiz = "<COTIDOLAR>" + DecimalFormater.executePuntoCeroFormatDividido(Strings.mid( strParseString, wvarPos, 15 ), "0.0000000", 10000000) + " </COTIDOLAR>";

        wvarResult = wvarResult + ParseoMensaje(new Variant( wvarPos + 15 ), strParseString);

        //

        wvarStep = 270;

        wobjXMLResponse = new XmlDomExtended();

        wobjXSLResponse = new XmlDomExtended();

        //

        wvarStep = 280;

        wobjXMLResponse.loadXML( wvarResult );

        //

        wvarStep = 290;

        if( wobjXMLResponse.selectNodes( "//R" ) .getLength() != 0 )

        {

          wobjXSLResponse.loadXML( p_GetXSL());

          //

          wvarStep = 300;

          wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );

          //

          wvarStep = 310;

          wobjXMLResponse = null;

          wobjXSLResponse = null;

          //

          wvarStep = 320;

          Response.set( "<Response><Estado resultado='true' mensaje='' />" + "<MSGEST>" + wvarEstado + "</MSGEST>" + wvarCotiz + wvarResult + "</Response>" );

          //

        }

        else

        {

          //

          wvarStep = 350;

          wobjXMLResponse = null;

          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );

          //

        }

      }

      //

      wvarStep = 330;

      /*TBD mobjCOM_Context.SetComplete() ;*/

      IAction_Execute = 0;

      //

      //~~~~~~~~~~~~~~~

      ClearObjects: 

      //~~~~~~~~~~~~~~~

      // LIBERO LOS OBJETOS

      wobjXMLConfig = null;



      return IAction_Execute;

      //

      //~~~~~~~~~~~~~~~

    }

    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());

      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);

      try 

       {

        //~~~~~~~~~~~~~~~

        //

        wobjXMLResponse = null;

        wobjXSLResponse = null;

        //

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarClienSecAs + wvarNivelAs + wvariCounter + wvarPolizas + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        Err.clear();

        return IAction_Execute;

      }

      catch( Exception _e2_ )

      {

      }

    }

    return IAction_Execute;

  }



  private void ObjectControl_Activate() throws Exception

  {

  }



  private boolean ObjectControl_CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    return ObjectControl_CanBePooled;

  }



  private void ObjectControl_Deactivate() throws Exception

  {

  }



  private String ParseoMensaje( Variant wvarPos, String strParseString ) throws Exception

  {

    String ParseoMensaje = "";

    String wvarResult = "";

    int wvarCantLin = 0;

    int wvarCounter = 0;

    //

    wvarResult = wvarResult + "<RS>";

    //

    if( !Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 3 ) ).equals( "" ) )

    {

      wvarCantLin = Obj.toInt( Strings.mid( strParseString, wvarPos.toInt(), 3 ) );

      wvarPos.set( wvarPos.add( new Variant( 3 ) ) );

      //

      for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )

      {

        wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 116 ) + "]]></R>";

        wvarPos.set( wvarPos.add( new Variant( 116 ) ) );

      }

      //

    }

    wvarResult = wvarResult + "</RS>";

    //

    ParseoMensaje = wvarResult;

    //

    return ParseoMensaje;

  }



  private String p_GetXSL() throws Exception

  {

    String p_GetXSL = "";

    String wvarStrXSL = "";

    //

    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";

    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";

    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"eeuu\" decimal-separator=\".\"/>";

    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";

    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REGS'>";

    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";

    wvarStrXSL = wvarStrXSL + "         </xsl:element>";

    wvarStrXSL = wvarStrXSL + "     </xsl:template>";

    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";

    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REG'>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RAMO'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,1,1)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CLIDES'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,2,30))' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CLIENSEC'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number(number(substring(.,32,9)), \"0\")' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='PROD'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,41,4)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POL'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,45,8)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERPOL'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,53,4)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERANN'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,57,4)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERSEC'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,61,6)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='OPERAPOL'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number(number(substring(.,67,6)), \"0\")' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RECNUM'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,73,9)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='MON'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,82,3))' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SIG'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,85,1) ='-'\">";

    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='normalize-space(substring(.,85,1))' />";

    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMP'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,86,14)) div 100), \"###.###.##0,00\", \"european\")' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMPCALC'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,86,14)) div 100), \"########0.00\", \"eeuu\")' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RENDIDO'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,100,1)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='FECVTO'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,101,10)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='AGE'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,111,2)' />-<xsl:value-of select='substring(.,113,4)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='ESTADO'>X</xsl:element>";

    //

    wvarStrXSL = wvarStrXSL + "         </xsl:element>";

    wvarStrXSL = wvarStrXSL + "     </xsl:template>";

    //

    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIDES'/>";

    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='MON'/>";

    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='FECVTO'/>";

    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='AGE'/>";

    //

    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";

    //

    p_GetXSL = wvarStrXSL;

    //

    return p_GetXSL;

  }



  /**

   * -------------------------------------------------------------------------------------------------------------------

   * // Metodos del Framework

   * -------------------------------------------------------------------------------------------------------------------

   */

  public void Activate() throws Exception

  {

    //

    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/

    //

  }



  public boolean CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    //

    ObjectControl_CanBePooled = true;

    //

    return ObjectControl_CanBePooled;

  }



  public void Deactivate() throws Exception

  {

    //

    mobjCOM_Context = (Object) null;

    mobjEventLog = null;

    //

  }

}

