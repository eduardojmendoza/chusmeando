package com.qbe.services.ovmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import java.io.File;
import com.qbe.services.ovmq.impl.ModGeneral;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVCtaCteaPC implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVCtaCteaPC";
  static final String mcteOpID = "1801";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_ClienSec = "//CLIENSEC";
  static final String mcteParam_LiquiSec = "//LIQUISEC";
  static final String mcteParam_TIPO_DISPLAY = "//TIPO_DISPLAY";
  /**
   *  TIPOS DE DISPLAY
   */
  static final String mcteDownload_TXT = "1";
  static final String mcteDownload_CSV = "2";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * VARIOS
   */
  private String strParseString = "";
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarResultParcial = "";
    String wvarStringTitulo = "";
    String wvarClienSec = "";
    String wvarLiquiSec = "";
    String wvarUsuario = "";
    String wvarCiaAsCod = "";
    String wvarTipoDisplay = "";
    String wvarContinuar = "";
    Variant wvarPos = new Variant();
    int wvarstrLen = 0;
    String wvarEstado = "";
    String wvarParametros = "";
    boolean wvarStatus = false;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarClienSec = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec )  ), 9 );
      wvarLiquiSec = Strings.right( Strings.fill( 6, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_LiquiSec )  ), 6 );
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarTipoDisplay = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIPO_DISPLAY )  );
      //
      wvarStep = 20;
      wobjXMLRequest = null;
      //
      wvarStep = 30;
      //
      //Levanto los Parametros de la cola de MQ del archivo de configuración
      wvarStep = 60;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      wobjXMLParametros = null;
      //
      wvarStep = 80;
      //
      wvarParametros = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + "000000000    " + wvarClienSec + "  000000000  000000000" + wvarLiquiSec;
      //
      wvarStep = 140;
      wvarContinuar = Strings.space( 53 );
      wvarStatus = this.CorrerMensaje(wvarParametros,wvarContinuar);
      //
      if( wvarStatus == false )
      {
        wvarStep = 150;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + strParseString + "</Response>" );
        IAction_Execute = 0;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [-1] - EL SERVICIO DE CONSULTA NO SE ENCUENTRA DISPONIBLE - Timeout Expired", vbLogEventTypeError );
        //
        //unsup GoTo ClearObjects
        //
      }

      //
      wvarStep = 160;
      wvarResult = "";
      //cantidad de caracteres ocupados por parámetros de entrada
      wvarPos.set( 126 );
      //
      wvarStep = 170;
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 180;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 190;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 200;
        wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        //
        while( wvarEstado.equals( "TR" ) )
        {
          //
          wvarStep = 210;
          wvarContinuar = Strings.mid( strParseString, 73, 53 );
          //
          wvarStep = 220;
          wvarStatus = this.CorrerMensaje(wvarParametros,wvarContinuar);
          //
          if( wvarStatus == false )
          {
            //
            wvarStep = 230;
            Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
            IAction_Execute = 0;
            //
            mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [-1] - EL SERVICIO DE CONSULTA NO SE ENCUENTRA DISPONIBLE - Timeout Expired", vbLogEventTypeError );
            //
            return IAction_Execute;
          }
          //
          //
          wvarStep = 240;
          //cantidad de caracteres ocupados por parámetros de entrada
          wvarPos.set( 126 );
          //
          wvarstrLen = Strings.len( strParseString );
          //
          wvarStep = 250;
          //Corto el estado
          wvarEstado = Strings.mid( strParseString, 19, 2 );
          //
          if( wvarEstado.equals( "ER" ) )
          {
            //
            wvarStep = 260;
            Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
            //
          }
          //
          wvarStep = 270;
          wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);
          //
        }
        //
        wvarStep = 280;
        wobjXMLResponse = new XmlDomExtended();
        wobjXSLResponse = new XmlDomExtended();
        //
        wvarStep = 290;
        wobjXMLResponse.loadXML( "<RS>" + wvarResult + "</RS>" );
        //
        wvarStep = 300;
        if( wobjXMLResponse.selectNodes( "//R" ) .getLength() != 0 )
        {
          //
          wvarStep = 310;
          if( wvarTipoDisplay.equals( mcteDownload_CSV ) )
          {
            //
            wvarStep = 320;
            wobjXSLResponse.loadXML( p_GetXSL_CSV());
            wvarStringTitulo = "CODIGO, POLIZA, CERTIF, ENDOSO, RECIBO, MONEDA, COMISION, BASE, PORC., PREMIO, FEC.MOV., FEC.EMI., LIQ.TIP., LIQ.DESC., MOV.TIP., MOV.DESC., ASEGURADO, REMESA, CAMBIO, PROV.COD., PROV DESC., DIFERIDO" + System.getProperty("line.separator");
            //
          }
          else if( wvarTipoDisplay.equals( mcteDownload_TXT ) )
          {
            //
            wvarStep = 330;
            wobjXSLResponse.loadXML( p_GetXSL_TXT());
            //wvarStringTitulo = "CODIGO " & vbTab & "POLIZA      " & vbTab & "CERTIFICADO   " & vbTab & "ENDO" & vbTab & "RECIBO   " & vbTab & "MON" & vbTab & "COMISION " & vbTab & vbTab & "BASE IMPONIBLE" & vbTab & "PORC." & vbTab & vbTab & "PREMIO        " & vbTab & "FEC.MOVIM." & vbTab & "LIQ " & vbTab & "LIQ.DESC." & Space(26) & vbTab & "MOVI" & vbTab & "MOV.DESC." & Space(26) & vbTab & "ASEGURADO" & Space(21) & vbTab & "REMESA" & Space(7) & vbTab & "TIP.CAMBIO" & vbTab & "  " & vbTab & "PROVINCIA" & Space(21) & vbCrLf
            //
          }
          //
          wvarStep = 340;

          wvarResultParcial = wvarResultParcial + wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
          wvarResultParcial = wvarStringTitulo + wvarResultParcial;
          //
          wvarStep = 350;
          wobjXMLResponse = null;
          wobjXSLResponse = null;
          //
        }
        else
        {
          //
          wvarStep = 360;
          wobjXMLResponse = null;
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          //
        }

        wvarStep = 370;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResultParcial + "</Response>" );
        //
      }
      //
      wvarStep = 380;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());
      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
      try 
       {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarClienSec + wvarLiquiSec + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCantLin = 0;
    int wvarCounter = 0;
    //
    if( !Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ).equals( "" ) )
    {
      wvarCantLin = Obj.toInt( Strings.mid( strParseString, wvarPos.toInt(), 4 ) );
      wvarPos.set( wvarPos.add( new Variant( 4 ) ) );
      //
      for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )
      {
        wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 265 ) + "]]></R>";
        wvarPos.set( wvarPos.add( new Variant( 265 ) ) );
      }
      //
    }
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  private String p_GetXSL_CSV() throws Exception
  {
    String p_GetXSL_CSV = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";

    wvarStrXSL = wvarStrXSL + "<xsl:template match='/RS/R'>";
    //AGENTE
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,1,6)' />, ";
    //PRODUCTO
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,7,4)' />";
    //POLIZA
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,11,8)' />, ";
    //CERTIFICADO
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,19,14)' />, ";
    //SUPLENUM
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,33,4)' />, ";
    //RECIBO
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,37,9)' />, ";
    //MONEDA
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='normalize-space(substring(.,46,3))' />, ";
    //SIGNO
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,49,1)' />";
    //COMISION
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='format-number((number(substring(.,50,14)) div 100), \"########0.00\", \"european\")' />, ";
    //BASE IMPONIBLE
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='format-number((number(substring(.,64,14)) div 100), \"########0.00\", \"european\")' />, ";
    //PORCENTAJE
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='format-number((number(substring(.,78,5)) div 100), \"########0.00\", \"european\")' />, ";
    //PREMIO
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='format-number((number(substring(.,83,14)) div 100), \"########0.00\", \"european\")' />, ";
    //FECHA DE MOVIMIENTO
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,97,10)' />, ";
    //FECHA DE EMISION
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,107,10)' />, ";
    //GRUPO MOVIMIENTO
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,117,4)' />, ";
    //DESC GRUPO
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='normalize-space(substring(.,121,35))' />, ";
    //TIPO MOVIMIENTO
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='normalize-space(substring(.,156,4))' />, ";
    //DESC MOV
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='normalize-space(substring(.,160,35))' />, ";
    //ASEGURADO
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='normalize-space(substring(.,195,25))' />, ";
    //REMESA
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='normalize-space(substring(.,220,13))' />, ";
    //TIPO CAMBIO
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='format-number((number(substring(.,233,10)) div 10000000), \"########0.00\", \"european\")' />, ";
    //COD PROV
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='normalize-space(substring(.,243,2))' />, ";
    //DESC PROV
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='normalize-space(substring(.,245,20))' />, ";
    //DIFERIDO
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='normalize-space(substring(.,265,1))' /> ";
    wvarStrXSL = wvarStrXSL + "<xsl:text><![CDATA[" + System.getProperty("line.separator") + "]]></xsl:text>";
    wvarStrXSL = wvarStrXSL + "</xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL_CSV = wvarStrXSL;
    //
    return p_GetXSL_CSV;
  }

  public boolean CorrerMensaje( String pvarParametros, String pvarContinuar ) throws Exception
  {
    boolean CorrerMensaje = false;
    int wvarMQError = 0;
    MQProxy wobjFrame2MQ = null;

    wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(pvarParametros + pvarContinuar, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();

    //
    if( wvarMQError == 0 )
    {
      CorrerMensaje = true;
    }
    else
    {
      /*TBD mobjCOM_Context.SetComplete() ;*/
      CorrerMensaje = false;
    }
    //
    return CorrerMensaje;
  }

  private String p_GetXSL_TXT() throws Exception
  {
    String p_GetXSL_TXT = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";

    wvarStrXSL = wvarStrXSL + "<xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,1,6)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,7,4)' />";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,11,8)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,19,14)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,33,4)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,37,9)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,46,3)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,49,1)' />";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='format-number((number(substring(.,50,14)) div 100), \"########0.00\", \"european\")' />&#009;";
    wvarStrXSL = wvarStrXSL + "     <xsl:if test='string-length(format-number((number(substring(.,50,14)) div 100), \"########0.00\", \"european\")) &lt; 7'>";
    wvarStrXSL = wvarStrXSL + "&#009;";
    wvarStrXSL = wvarStrXSL + "     </xsl:if>";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='format-number((number(substring(.,64,14)) div 100), \"########0.00\", \"european\")' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='format-number((number(substring(.,78,5)) div 100), \"########0.00\", \"european\")' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='format-number((number(substring(.,83,14)) div 100), \"########0.00\", \"european\")' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,97,10)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,107,10)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,117,4)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,121,35)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,156,4)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,160,35)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,195,25)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,220,13)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='format-number((number(substring(.,233,10)) div 10000000), \"########0.00\", \"european\")' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,243,2)' />&#009;";
	wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,245,20)' />&#009;";
	wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,265,1)' />";
	wvarStrXSL = wvarStrXSL + "<xsl:text><![CDATA[" + System.getProperty("line.separator") + "]]></xsl:text>";
	wvarStrXSL = wvarStrXSL + "</xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL_TXT = wvarStrXSL;
    //
    return p_GetXSL_TXT;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
