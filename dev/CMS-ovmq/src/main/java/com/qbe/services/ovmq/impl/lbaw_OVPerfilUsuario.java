package com.qbe.services.ovmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import java.io.File;
import com.qbe.services.ovmq.impl.ModGeneral;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
import com.qbe.vbcompat.format.VBFixesUtil;
/**

 * Objetos del FrameWork

 */



public class lbaw_OVPerfilUsuario implements VBObjectClass

{

  /**

   * Implementacion de los objetos

   * Datos de la accion

   */

  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVPerfilUsuario";

  static final String mcteOpID = "1000";

  /**

   * Parametros XML de Entrada

   */

  static final String mcteParam_Usuario = "//USUARIO";

  private Object mobjCOM_Context = null;

  private EventLog mobjEventLog = new EventLog();

  /**

   * static variable for method: IAction_Execute

   */

  private final String wcteFnName = "IAction_Execute";



  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {

    int IAction_Execute = 0;

    Variant vbLogEventTypeError = new Variant();

    XmlDomExtended wobjXMLRequest = null;

    XmlDomExtended wobjXMLConfig = null;

    XmlDomExtended wobjXMLParametros = null;

    int wvarMQError = 0;

    String wvarArea = "";

    MQProxy wobjFrame2MQ = null;

    int wvarStep = 0;

    String wvarResult = "";

    String wvarResultUnic = "";

    String wvarCiaAsCod = "";

    String wvarUsuario = "";

    int wvarPos = 0;

    String strParseString = "";

    int wvarstrLen = 0;

    String wvarEstado = "";

    //

    //

    //

    //

    //

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~

    try 

    {

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //

      wvarStep = 10;

      //Levanto los par�metros que llegan desde la p�gina

      wobjXMLRequest = new XmlDomExtended();

      wobjXMLRequest.loadXML( Request );

      //Deber� venir desde la p�gina

      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );

      //

      //

      wvarStep = 20;

      wobjXMLRequest = null;

      //

      wvarStep = 30;

      //Levanto los datos de la cola de MQ del archivo de configuraci�n

      wobjXMLConfig = new XmlDomExtended();

      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));

      //

      //Levanto los Parametros de la cola de MQ del archivo de configuraci�n

      wvarStep = 60;

      wobjXMLParametros = new XmlDomExtended();

      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));

      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );

      wobjXMLParametros = null;

      //

      wvarStep = 80;

      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarUsuario;

      //

      wvarStep = 100;

      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );

      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();


      //

      wvarStep = 150;

      if( wvarMQError != 0 )

      {

        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        return IAction_Execute;

      }

      //

      wvarStep = 180;

      wvarResult = "";

      //cantidad de caracteres ocupados por par�metros de entrada

      wvarPos = 77;

      //

      wvarstrLen = Strings.len( strParseString );

      //

      wvarStep = 190;

      //Corto el estado

      wvarEstado = Strings.mid( strParseString, 19, 2 );

      //

      if( wvarEstado.equals( "ER" ) )

      {

        //

        wvarStep = 200;

        Response.set( "<Response><Estado resultado='false' mensaje='El usuario no se encuentra habilitado en el sistema.' /></Response>" );

        //

      }

      else

      {

        //

        wvarStep = 230;

        wvarResultUnic = "<USER_NIVEL>" + Strings.trim( Strings.mid( strParseString, wvarPos, 1 ) ) + "</USER_NIVEL>";

        wvarResultUnic = wvarResultUnic + "<USER_NOM><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 1), 60 ) ) + "]]></USER_NOM>";

        //

        wvarResult = "";

        wvarResult = wvarResult + ParseoMensaje(new Variant( wvarPos + 61 ), strParseString, wvarstrLen);

        //

        if( !Strings.trim( wvarResult ).equals( "" ) )

        {

          wvarStep = 290;

          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResultUnic + wvarResult + "</Response>" );

        }

        else

        {

          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );

        }

        //

      }

      //

      wvarStep = 300;

      /*TBD mobjCOM_Context.SetComplete() ;*/

      IAction_Execute = 0;

      //

      //~~~~~~~~~~~~~~~

      ClearObjects: 

      //~~~~~~~~~~~~~~~

      // LIBERO LOS OBJETOS

      wobjXMLConfig = null;

      return IAction_Execute;

      //

      //~~~~~~~~~~~~~~~

    }

    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());

      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);

      try 

       {

        //~~~~~~~~~~~~~~~

        //

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        Err.clear();

        return IAction_Execute;

      }

      catch( Exception _e2_ )

      {

      }

    }

    return IAction_Execute;

  }



  private void ObjectControl_Activate() throws Exception

  {

  }



  private boolean ObjectControl_CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    return ObjectControl_CanBePooled;

  }



  private void ObjectControl_Deactivate() throws Exception

  {

  }



  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception

  {

    String ParseoMensaje = "";

    String wvarResult = "";

    int wvarCount = 0;

    int wvarCantEmisiones = 0;

    //

    wvarResult = wvarResult + "<ACCESO>";

    wvarCount = 1;

    //

    while( (wvarPos.toInt() < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 6 ) ).equals( "" )) && (wvarCount <= 200) )

    {

      wvarResult = wvarResult + "<MMENU><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 6 ) + "]]></MMENU>";

      wvarPos.set( wvarPos.add( new Variant( 66 ) ) );

      wvarCount = wvarCount + 1;

    }

    //

    wvarResult = wvarResult + "</ACCESO>";



    //Cargo los Ramos que puede emitir Online

    wvarPos.set( 13338 );

    if( Strings.len( strParseString ) > wvarPos.add( new Variant( 3 ) ).toInt() )

    {

      //El area incluye valores para emisiones Onlines permitidas

      if( new Variant( Strings.mid( strParseString, wvarPos.toInt(), 3 ) ).isNumeric() )

      {

        wvarResult = wvarResult + "<EMISIONESONLINE>";

        wvarCantEmisiones = Obj.toInt( Strings.mid( strParseString, wvarPos.toInt(), 3 ) );

        wvarPos.set( wvarPos.add( new Variant( 3 ) ) );

        for( wvarCount = 1; wvarCount <= wvarCantEmisiones; wvarCount++ )

        {

          if( wvarPos.toInt() > wvarstrLen )

          {

            break;

          }

          if( Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ).equals( "" ) )

          {

            break;

          }

          wvarResult = wvarResult + "<EMISION>" + Strings.mid( strParseString, wvarPos.toInt(), 4 ) + "</EMISION>";

          wvarPos.set( wvarPos.add( new Variant( 4 ) ) );

        }

        wvarResult = wvarResult + "</EMISIONESONLINE>";

      }

    }

    //

    ParseoMensaje = wvarResult;

    //

    return ParseoMensaje;

  }



  public void Activate() throws Exception

  {

    //

    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/

    //

  }



  public boolean CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    //

    ObjectControl_CanBePooled = true;

    //

    return ObjectControl_CanBePooled;

  }



  public void Deactivate() throws Exception

  {

    //

    mobjCOM_Context = (Object) null;

    mobjEventLog = null;

    //

  }

}

