package com.qbe.services.ovmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import java.io.File;
import com.qbe.services.ovmq.impl.ModGeneral;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
import com.qbe.vbcompat.format.VBFixesUtil;

/**

 * Objetos del FrameWork

 */



public class lbaw_OVExigibleDetPoliza implements VBObjectClass

{

  /**

   * Implementacion de los objetos

   * Datos de la accion

   */

  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVExigibleDetPoliza";

  static final String mcteOpID = "1402";

  /**

   * Parametros XML de Entrada

   */

  static final String mcteParam_Usuario = "//USUARIO";

  static final String mcteParam_Producto = "//PRODUCTO";

  static final String mcteParam_Poliza = "//POLIZA";

  static final String mcteParam_Certi = "//CERTI";

  private Object mobjCOM_Context = null;

  private EventLog mobjEventLog = new EventLog();

  /**

   * static variable for method: IAction_Execute

   */

  private final String wcteFnName = "IAction_Execute";



  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {

    int IAction_Execute = 0;

    Variant vbLogEventTypeError = new Variant();

    XmlDomExtended wobjXMLRequest = null;

    XmlDomExtended wobjXMLConfig = null;

    XmlDomExtended wobjXMLParametros = null;

    XmlDomExtended wobjXMLResponse = null;

    XmlDomExtended wobjXSLResponse = null;

    int wvarMQError = 0;

    String wvarArea = "";

    MQProxy wobjFrame2MQ = null;

    int wvarStep = 0;

    String wvarResult = "";

    String wvarCiaAsCod = "";

    String wvarUsuario = "";

    String wvarProducto = "";

    String wvarPoliza = "";

    String wvarCerti = "";

    Variant wvarPos = new Variant();

    String strParseString = "";

    int wvarstrLen = 0;

    String wvarEstado = "";

    //

    //

    //

    //

    //

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~

    try 

    {

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //

      wvarStep = 10;

      //Levanto los par�metros que llegan desde la p�gina

      wobjXMLRequest = new XmlDomExtended();

      wobjXMLRequest.loadXML( Request );

      //Deber� venir desde la p�gina

      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );

      wvarProducto = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Producto )  ) + Strings.space( 4 ), 4 );

      wvarPoliza = Strings.right( Strings.fill( 8, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Poliza )  ), 8 );

      wvarCerti = Strings.right( Strings.fill( 14, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Certi )  ), 14 );

      //

      //

      wobjXMLRequest = null;

      //

      wvarStep = 30;

      //Levanto los datos de la cola de MQ del archivo de configuraci�n

      wobjXMLConfig = new XmlDomExtended();

      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));

      //

      //Levanto los Parametros de la cola de MQ del archivo de configuraci�n

      wvarStep = 60;

      wobjXMLParametros = new XmlDomExtended();

      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));

      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );

      wobjXMLParametros = null;

      //

      wvarStep = 120;

      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarProducto + wvarPoliza + wvarCerti;

      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 36 ) );

      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();


      //

      if( wvarMQError != 0 )

      {

        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        return IAction_Execute;

      }

      //

      wvarStep = 240;

      wvarResult = "";

      //cantidad de caracteres ocupados por par�metros de entrada

      wvarPos.set( 93 );

      //

      wvarstrLen = Strings.len( strParseString );

      //

      wvarStep = 250;

      //Corto el estado

      wvarEstado = Strings.mid( strParseString, 19, 2 );

      //

      if( wvarEstado.equals( "ER" ) )

      {

        //

        wvarStep = 260;

        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );

        //

      }

      else

      {

        //

        wvarStep = 280;

        wvarResult = "";

        wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);

        //

        wvarStep = 290;

        wobjXMLResponse = new XmlDomExtended();

        wobjXSLResponse = new XmlDomExtended();

        //

        wvarStep = 300;

        wobjXMLResponse.loadXML( wvarResult );

        //

        wvarStep = 310;

        if( wobjXMLResponse.selectNodes( "//R" ) .getLength() != 0 )

        {

          wobjXSLResponse.loadXML( p_GetXSL());

          //

          wvarStep = 320;

          wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );

          //

          wvarStep = 330;

          wobjXMLResponse = null;

          wobjXSLResponse = null;

          //

          wvarStep = 340;

          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );

          //

        }

        else

        {

          //

          wvarStep = 350;

          wobjXMLResponse = null;

          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );

          //

        }

      }

      //

      wvarStep = 360;

      /*TBD mobjCOM_Context.SetComplete() ;*/

      IAction_Execute = 0;

      //

      //~~~~~~~~~~~~~~~

      ClearObjects: 

      //~~~~~~~~~~~~~~~

      // LIBERO LOS OBJETOS

      wobjXMLConfig = null;



      return IAction_Execute;

      //

      //~~~~~~~~~~~~~~~

    }

    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());

      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);

      try 

       {

        //~~~~~~~~~~~~~~~

        //

        wobjXMLResponse = null;

        wobjXSLResponse = null;

        //

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarProducto + wvarPoliza + wvarCerti + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        Err.clear();

        return IAction_Execute;

      }

      catch( Exception _e2_ )

      {

      }

    }

    return IAction_Execute;

  }



  private void ObjectControl_Activate() throws Exception

  {

  }



  private boolean ObjectControl_CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    return ObjectControl_CanBePooled;

  }



  private void ObjectControl_Deactivate() throws Exception

  {

  }



  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception

  {

    String ParseoMensaje = "";

    String wvarResult = "";

    int wvarCantLin = 0;

    int wvarCounter = 0;

    //

    wvarResult = wvarResult + "<RS>";

    //

    if( !Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ).equals( "" ) )

    {

      wvarCantLin = Obj.toInt( Strings.mid( strParseString, wvarPos.toInt(), 4 ) );

      wvarPos.set( wvarPos.add( new Variant( 4 ) ) );

      //

      for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )

      {

        wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 72 ) + "]]></R>";

        wvarPos.set( wvarPos.add( new Variant( 72 ) ) );

      }

      //

    }

    wvarResult = wvarResult + "</RS>";

    //

    ParseoMensaje = wvarResult;

    //

    return ParseoMensaje;

  }



  private String p_GetXSL() throws Exception

  {

    String p_GetXSL = "";

    String wvarStrXSL = "";

    //

    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";

    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";

    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";

    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REGS'>";

    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";

    wvarStrXSL = wvarStrXSL + "         </xsl:element>";

    wvarStrXSL = wvarStrXSL + "     </xsl:template>";

    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";

    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REG'>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='ENDOSO'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,1,6)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RECNUM'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,7,9)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SIGIMP'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,16,1) ='-'\">";

    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,16,1)' />";

    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMP'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,17,14)) div 100), \"###.###.##0,00\", \"european\")' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COB'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,31,4))' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='FECVTO'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,35,10)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SIGDIA'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,45,1) ='-'\">";

    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,45,1)' />";

    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='DIAS'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number(number(substring(.,46,4)), \"0\")' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RECHA'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,50,20))' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='MON'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,70,3))' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    //

    wvarStrXSL = wvarStrXSL + "         </xsl:element>";

    wvarStrXSL = wvarStrXSL + "     </xsl:template>";

    //

    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='COB'/>";

    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='FECVTO'/>";

    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='RECHA'/>";

    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='MON'/>";

    //

    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";

    //

    p_GetXSL = wvarStrXSL;

    //

    return p_GetXSL;

  }



  /**

   * -------------------------------------------------------------------------------------------------------------------

   * // Metodos del Framework

   * -------------------------------------------------------------------------------------------------------------------

   */

  public void Activate() throws Exception

  {

    //

    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/

    //

  }



  public boolean CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    //

    ObjectControl_CanBePooled = true;

    //

    return ObjectControl_CanBePooled;

  }



  public void Deactivate() throws Exception

  {

    //

    mobjCOM_Context = (Object) null;

    mobjEventLog = null;

    //

  }

}

