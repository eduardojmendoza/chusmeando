package com.qbe.services.ovmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import java.io.File;
import com.qbe.services.ovmq.impl.ModGeneral;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
import com.qbe.vbcompat.format.VBFixesUtil;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVExigibleDetalles implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * 
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVExigibleDetalles";
  static final String mcteOpID = "1401";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_NivelAs = "//NIVELAS";
  static final String mcteParam_ClienSecAs = "//CLIENSECAS";
  static final String mcteParam_Nivel1 = "//NIVEL1";
  static final String mcteParam_ClienSec1 = "//CLIENSEC1";
  static final String mcteParam_Nivel2 = "//NIVEL2";
  static final String mcteParam_ClienSec2 = "//CLIENSEC2";
  static final String mcteParam_Nivel3 = "//NIVEL3";
  static final String mcteParam_ClienSec3 = "//CLIENSEC3";
  static final String mcteParam_Producto = "//PRODUCTO";
  static final String mcteParam_Poliza = "//POLIZA";
  static final String mcteParam_NroConsu = "//NROCONS";
  static final String mcteParam_NroOrden = "//NROORDEN";
  static final String mcteParam_DirOrden = "//DIRORDEN";
  static final String mcteParam_TFiltro = "//TFILTRO";
  static final String mcteParam_VFiltro = "//VFILTRO";
  
  private Object mobjCOM_Context = null;
  
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarResultTR = "";
    String wvarNivelAs = "";
    String wvarClienSecAs = "";
    String wvarNivel1 = "";
    String wvarClienSec1 = "";
    String wvarNivel2 = "";
    String wvarClienSec2 = "";
    String wvarNivel3 = "";
    String wvarClienSec3 = "";
    String wvarCiaAsCod = "";
    String wvarProducto = "";
    String wvarPoliza = "";
    String wvarUsuario = "";
    String wvarNroConsu = "";
    String wvarNroOrden = "";
    String wvarDirOrden = "";
    String wvarTFiltro = "";
    String wvarVFiltro = "";
    Variant wvarPos = new Variant();
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarNivelAs = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NivelAs )  ) + Strings.space( 2 ), 2 );
      wvarClienSecAs = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSecAs )  ), 9 );
      wvarNivel1 = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Nivel1 )  ) + Strings.space( 2 ), 2 );
      wvarClienSec1 = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec1 )  ), 9 );
      wvarNivel2 = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Nivel2 )  ) + Strings.space( 2 ), 2 );
      wvarClienSec2 = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec2 )  ), 9 );
      wvarNivel3 = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Nivel3 )  ) + Strings.space( 2 ), 2 );
      wvarClienSec3 = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec3 )  ), 9 );
      wvarProducto = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Producto )  ) + Strings.space( 4 ), 4 );
      wvarPoliza = Strings.right( Strings.fill( 22, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Poliza )  ), 22 );
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarNroConsu = Strings.right( Strings.fill( 4, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NroConsu )  ), 4 );
      wvarNroOrden = Strings.right( Strings.fill( 2, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NroOrden )  ), 2 );
      if( wobjXMLRequest.selectNodes( mcteParam_DirOrden ) .getLength() != 0 )
      {
        wvarDirOrden = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DirOrden )  ) + "A", 1 );
      }
      else
      {
        wvarDirOrden = "A";
      }
      wvarTFiltro = Strings.right( "0" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TFiltro )  ), 1 );
      wvarVFiltro = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_VFiltro )  ) + Strings.space( 4 ), 4 );
      //
      wvarStep = 20;
      wobjXMLRequest = null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //Levanto los Parametros de la cola de MQ del archivo de configuración
      wvarStep = 60;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      wobjXMLParametros = null;
      //
      wvarStep = 80;
      //
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + wvarNivel1 + wvarClienSec1 + wvarNivel2 + wvarClienSec2 + wvarNivel3 + wvarClienSec3 + "1" + wvarNroConsu + wvarNroOrden + wvarDirOrden + wvarProducto + wvarPoliza + wvarTFiltro + wvarVFiltro;
      wvarStep = 100;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 50 ) );
      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();

      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }

      wvarStep = 190;
      wvarResult = "";
      wvarResultTR = "";
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos.set( 106 );
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 200;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 210;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 230;
        wvarResultTR = wvarResultTR + "<MSGEST>" + wvarEstado + "</MSGEST>";
        wvarResultTR = wvarResultTR + "<PRODUCTO>" + Strings.mid( strParseString, 75, 4 ) + "</PRODUCTO>";
        wvarResultTR = wvarResultTR + "<POLIZA>" + Strings.mid( strParseString, 79, 22 ) + "</POLIZA>";
        wvarResultTR = wvarResultTR + "<NROCONS>" + Strings.mid( strParseString, 68, 4 ) + "</NROCONS>";
        wvarResultTR = wvarResultTR + "<NROORDEN>" + Strings.mid( strParseString, 72, 2 ) + "</NROORDEN>";
        wvarResultTR = wvarResultTR + "<DIRORDEN>" + Strings.mid( strParseString, 74, 1 ) + "</DIRORDEN>";
        wvarResultTR = wvarResultTR + "<TFILTRO>" + Strings.mid( strParseString, 101, 1 ) + "</TFILTRO>";
        wvarResultTR = wvarResultTR + "<VFILTRO>" + Strings.mid( strParseString, 102, 4 ) + "</VFILTRO>";
        //
        if( Obj.toInt( wvarNroConsu ) != 0 )
        {
          wvarResultTR = wvarResultTR + "<PAGINADO>1</PAGINADO>";
        }
        else
        {
          wvarResultTR = wvarResultTR + "<PAGINADO>0</PAGINADO>";
        }
        //
        wvarStep = 240;
        wvarResult = "";
        wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        //
        wvarStep = 250;
        wobjXMLResponse = new XmlDomExtended();
        wobjXSLResponse = new XmlDomExtended();
        //
        wvarStep = 260;
        wobjXMLResponse.loadXML( wvarResult );
        //
        if( wobjXMLResponse.selectNodes( "//R" ) .getLength() != 0 )
        {
          wvarStep = 270;
          wobjXSLResponse.loadXML( p_GetXSL());
          //
          wvarStep = 280;
          wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
          //
          wvarStep = 290;
          wobjXMLResponse = null;
          wobjXSLResponse = null;
          //
          wvarStep = 300;
          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResultTR + wvarResult + "</Response>" );
          //
        }
        else
        {
          //
          wvarStep = 310;
          wobjXMLResponse = null;
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          //
        }
      }
      //
      wvarStep = 320;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());
      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
      try 
       {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + wvarNivel1 + wvarClienSec1 + wvarNivel2 + wvarClienSec2 + wvarNivel3 + wvarClienSec3 + "1" + wvarNroConsu + wvarNroOrden + wvarProducto + wvarPoliza + wvarTFiltro + wvarVFiltro + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCantLin = 0;
    int wvarCounter = 0;
    //
    wvarResult = wvarResult + "<RS>";
    //
    if( !Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ).equals( "" ) )
    {
      wvarCantLin = Obj.toInt( Strings.mid( strParseString, wvarPos.toInt(), 4 ) );
      wvarPos.set( wvarPos.add( new Variant( 4 ) ) );
      //
      for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )
      {
        wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 149 ) + "]]></R>";
        wvarPos.set( wvarPos.add( new Variant( 149 ) ) );
      }
      //
    }
    wvarResult = wvarResult + "</RS>";
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REGS'>";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R[substring(.,1,6) != \"      \"]'/>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='TOTS'>";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R[substring(.,1,6) = \"      \"]'/>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    // DETALLE
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R[substring(.,1,6) != \"      \"]'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REG'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='AGE'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,1,2)' />-<xsl:value-of select='substring(.,3,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SINI'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,7,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RAMO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,8,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CLIDES'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,9,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='PROD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,39,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,43,8)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERPOL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,51,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERANN'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,55,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERSEC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,59,6)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='MON'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,65,3))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //    wvarStrXSL = wvarStrXSL & "             <xsl:element name='IMPTOS'>"
    //    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,68,1) ='-'"">"
    //    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,68,1)' />"
    //    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    //    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMPTO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,68,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,68,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,69,14)) div 100), \"########0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_30S'>"
    //    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,83,1) ='-'"">"
    //    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,83,1)' />"
    //    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    //    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='I_30'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,83,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,83,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,84,14)) div 100), \"########0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_60S'>"
    //    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,98,1) ='-'"">"
    //    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,98,1)' />"
    //    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    //    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='I_60'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,98,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,98,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,99,14)) div 100), \"########0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_90S'>"
    //    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,113,1) ='-'"">"
    //    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,113,1)' />"
    //    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    //    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='I_90'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,113,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,113,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,114,14)) div 100), \"########0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_M90S'>"
    //    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,128,1) ='-'"">"
    //    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,128,1)' />"
    //    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    //    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='I_M90'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,128,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,128,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,129,14)) div 100), \"########0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='EST'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,143,3))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COB'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,146,3))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    // TOTALES
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R[substring(.,1,6) = \"      \"]'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='TOT'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='TMON'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,65,3)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_IMPTOS'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,68,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,68,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_IMPTO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,69,14)) div 100), \"###.###.##0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_30S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,83,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,83,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_30'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,84,14)) div 100), \"###.###.##0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_60S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,98,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,98,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_60'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,99,14)) div 100), \"###.###.##0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_90S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,113,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,113,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_90'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,114,14)) div 100), \"###.###.##0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_M90S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,128,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,128,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_M90'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,129,14)) div 100), \"###.###.##0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='MON'/>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    //
    return p_GetXSL;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
