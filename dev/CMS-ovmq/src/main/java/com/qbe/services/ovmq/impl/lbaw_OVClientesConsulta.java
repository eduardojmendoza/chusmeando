package com.qbe.services.ovmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import java.io.File;
import com.qbe.services.ovmq.impl.ModGeneral;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVClientesConsulta implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVClientesConsulta";
  static final String mcteOpID = "1010";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_NivelAs = "//NIVELAS";
  static final String mcteParam_ClienSecAs = "//CLIENSECAS";
  static final String mcteParam_DocumTip = "//DOCUMTIP";
  static final String mcteParam_DocumNro = "//DOCUMNRO";
  static final String mcteParam_Cliendes = "//CLIENDES";
  static final String mcteParam_Producto = "//PRODUCTO";
  static final String mcteParam_Poliza = "//POLIZA";
  static final String mcteParam_Certi = "//CERTI";
  static final String mcteParam_Patente = "//PATENTE";
  static final String mcteParam_Motor = "//MOTOR";
  static final String mcteParam_EstPol = "//ESTPOL";
  /**
   * Parametros para la paginación
   */
  static final String mcteParam_QryCont = "//QRYCONT";
  static final String mcteParam_CliCont = "//CLICONT";
  static final String mcteParam_ProduCont = "//PRODUCONT";
  static final String mcteParam_PolizaCont = "//POLIZACONT";
  /**
   * jc 09/2009
   */
  static final String mcteParam_Suplenums = "//SUPLENUMS";
  static final String mcteParam_Cliensecs = "//CLIENSECS";
  static final String mcteParam_Agentclas = "//AGENTCLAS";
  static final String mcteParam_Agentcods = "//AGENTCODS";
  static final String mcteParam_Nroitems = "//NROITEMS";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * fin jc
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarResultTR = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarNivelAs = "";
    String wvarClienSecAs = "";
    String wvarProducto = "";
    String wvarPoliza = "";
    String wvarCerti = "";
    String wvarDocumTip = "";
    String wvarDocumNro = "";
    String wvarCliendes = "";
    String wvarPatente = "";
    String wvarMotor = "";
    String wvarEstPol = "";
    Variant wvarPos = new Variant();
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    String wvarMensaje = "";
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    String wvarQryCont = "";
    String wvarCliCont = "";
    String wvarProduCont = "";
    String wvarPolizaCont = "";
    String wvarSuplenums = "";
    String wvarCliensecs = "";
    String wvarAgentclas = "";
    String wvarAgentcods = "";
    String wvarNroitems = "";
    //
    //
    //
    //
    //Para la paginación
    //JC 09/2009
    //Fin JC
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarNivelAs = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NivelAs )  ) + Strings.space( 2 ), 2 );
      wvarClienSecAs = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSecAs )  ), 9 );
      wvarProducto = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Producto )  ) + Strings.space( 4 ), 4 );
      wvarPoliza = Strings.right( Strings.fill( 8, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Poliza )  ), 8 );
      wvarCerti = Strings.right( Strings.fill( 14, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Certi )  ), 14 );
      wvarDocumTip = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DocumTip )  ), 2 );
      wvarDocumNro = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DocumNro )  ) + Strings.space( 11 ), 11 );
      wvarCliendes = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Cliendes )  ) + Strings.space( 20 ), 20 );
      wvarPatente = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Patente )  ) + Strings.space( 10 ), 10 );
      wvarMotor = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Motor )  ) + Strings.space( 20 ), 20 );
      wvarEstPol = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EstPol )  ) + Strings.space( 30 ), 30 );

      wvarQryCont = Strings.right( Strings.fill( 8, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_QryCont )  ), 8 );
      wvarCliCont = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CliCont )  ) + Strings.space( 30 ), 30 );
      wvarProduCont = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ProduCont )  ) + Strings.space( 4 ), 4 );
      wvarPolizaCont = Strings.right( Strings.fill( 22, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PolizaCont )  ), 22 );
      //jc 09/2009
      wvarSuplenums = Strings.right( Strings.fill( 4, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Suplenums )  ), 4 );
      wvarCliensecs = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Cliensecs )  ), 9 );
      wvarAgentclas = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Agentclas )  ) + Strings.space( 2 ), 2 );
      wvarAgentcods = Strings.right( Strings.fill( 4, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Agentcods )  ), 4 );
      wvarNroitems = Strings.right( Strings.fill( 4, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Nroitems )  ), 4 );
      //jc fin
      //
      wvarStep = 20;
      wobjXMLRequest = null;
      //
      //Levanto los Parametros de la cola de MQ del archivo de configuración
      wvarStep = 60;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      wobjXMLParametros = null;
      //
      wvarStep = 80;
      //FJO - Se modifica porque no se informaba el nivel asumido correctamente - 2008-04-18
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + "  000000000" + "  000000000  000000000" + wvarDocumTip + wvarDocumNro + wvarCliendes + wvarProducto + wvarPoliza + wvarCerti + wvarPatente + wvarMotor + wvarEstPol;
      //jc 09/2009
      //wvarArea = wvarArea & wvarQryCont & wvarCliCont & wvarProduCont & wvarPolizaCont
      wvarArea = wvarArea + wvarQryCont + wvarCliCont + wvarProduCont + wvarPolizaCont + wvarSuplenums + wvarCliensecs + wvarAgentclas + wvarAgentcods + wvarNroitems;
      //fin jc
      wvarStep = 190;
      wvarResult = "";

      //jc 09/2009
      //wvarPos = 250 'cantidad de caracteres ocupados por parámetros de entrada
      //cantidad de caracteres ocupados por parámetros de entrada
      wvarPos.set( 273 );
      //fin jc
      wvarStep = 100;
      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();

      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"El servicio de consulta no se encuentra disponible\" />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        return IAction_Execute;
      }

      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 200;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        wvarStep = 210;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
      }
      else
      {
        //
        if( wvarEstado.equals( "TR" ) )
        {
          //
          wvarStep = 230;
          //Cargo las variables para llamar a un segundo mensaje de ser necesario
          wvarResultTR = wvarResultTR + "<QRYCONT>" + Strings.mid( strParseString, 186, 8 ) + "</QRYCONT>";
          wvarResultTR = wvarResultTR + "<CLICONT>" + Strings.mid( strParseString, 194, 30 ) + "</CLICONT>";
          wvarResultTR = wvarResultTR + "<PRODUCONT>" + Strings.mid( strParseString, 224, 4 ) + "</PRODUCONT>";
          wvarResultTR = wvarResultTR + "<POLIZACONT>" + Strings.mid( strParseString, 228, 22 ) + "</POLIZACONT>";
          //jc 09/2009 se deben agregar mas variables
          wvarResultTR = wvarResultTR + "<CLIENDES>" + Strings.mid( strParseString, 80, 20 ) + "</CLIENDES>";
          wvarResultTR = wvarResultTR + "<ESTPOL>" + Strings.mid( strParseString, 156, 30 ) + "</ESTPOL>";
          //jc 09/2009
          wvarResultTR = wvarResultTR + "<SUPLENUMS>" + Strings.mid( strParseString, 250, 4 ) + "</SUPLENUMS>";
          wvarResultTR = wvarResultTR + "<CLIENSECS>" + Strings.mid( strParseString, 254, 9 ) + "</CLIENSECS>";
          wvarResultTR = wvarResultTR + "<AGENTCLAS>" + Strings.mid( strParseString, 263, 2 ) + "</AGENTCLAS>";
          wvarResultTR = wvarResultTR + "<AGENTCODS>" + Strings.mid( strParseString, 265, 4 ) + "</AGENTCODS>";
          wvarResultTR = wvarResultTR + "<NROITEMS>" + Strings.mid( strParseString, 269, 4 ) + "</NROITEMS>";
          //fin jc
        }
        //
        wvarStep = 240;
        wvarResult = "";
        wvarResultTR = wvarResultTR + "<MSGEST>" + wvarEstado + "</MSGEST>";
        wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        //
        wvarStep = 250;
        wobjXMLResponse = new XmlDomExtended();
        wobjXSLResponse = new XmlDomExtended();
        //
        wvarStep = 260;
        wobjXMLResponse.loadXML( wvarResult );
        //
        if( wobjXMLResponse.selectNodes( "//R" ) .getLength() != 0 )
        {
          wvarStep = 270;
          wobjXSLResponse.loadXML( p_GetXSL());
          //
          wvarStep = 280;
          wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
          //
          wvarStep = 290;
          wobjXMLResponse = null;
          wobjXSLResponse = null;
          //
          wvarStep = 300;

          //If wvarEstado = "TR" And Mid(strParseString, 21, 2) = "52" Then
          //    wvarMensaje = "<MSGEST><![CDATA[La b�squeda realizada puede estar incompleta. Si usted no encontr� el cliente deseado, ingrese un dato mas espec�fico y realice la b�squeda nuevamente.]]></MSGEST>"
          //    Response = "<Response><Estado resultado='true' mensaje='' />" & wvarMensaje & wvarResult & "</Response>"
          //Else
          //    wvarMensaje = "<MSGEST></MSGEST>"
          //    Response = "<Response><Estado resultado='true' mensaje='' />" & wvarMensaje & wvarResult & "</Response>"
          //End If
          //
          Response.set( "<Response><Estado  mensaje='' resultado='true' />" + wvarResultTR + wvarResult + "</Response>" );
        }
        else
        {
          //
          if( (wvarEstado.equals( "TR" )) && (Strings.mid( strParseString, 21, 2 ).equals( "52" )) )
          {
            Response.set( "<Response><Estado resultado='false' mensaje='La búsqueda realizada es muy extensa, por favor ingrese un dato más específico y realícela nuevamente.' /></Response>" );
          }
          else
          {
            wvarStep = 310;
            Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          }
          //
          wobjXMLResponse = null;
        }
      }
      //
      wvarStep = 320;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());
      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
      try 
       {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarDocumTip + wvarDocumNro + wvarCliendes + wvarProducto + wvarPoliza + wvarCerti + wvarPatente + wvarMotor + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    //
    //& Chr(13)
    wvarResult = wvarResult + "<RS>";
    //
    while( (wvarPos.toInt() < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ).equals( "" )) )
    {
//      System.out.println( String.valueOf( wvarPos.toDouble() ) + "|" + Strings.mid( strParseString, wvarPos.toInt(), 4 ) + "|" + Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ) + "|" + Strings.asc( Strings.mid( strParseString, wvarPos.toInt(), 1 ) ) + "-" + Strings.asc( Strings.mid( strParseString, wvarPos.add( new Variant( 1 ) ).toInt(), 1 ) ) + "-" + Strings.asc( Strings.mid( strParseString, wvarPos.add( new Variant( 2 ) ).toInt(), 1 ) ) + "-" + Strings.asc( Strings.mid( strParseString, wvarPos.add( new Variant( 3 ) ).toInt(), 1 ) ) );
      //'MHC: Soluci�n t�ctica - Fase II - Agregado de marca de Endosable
      //wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 139) & "]]></R>" '& Chr(13)
      //& Chr(13)
      wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 140 ) + "]]></R>";
      //wvarPos = wvarPos + 138
      //wvarPos = wvarPos + 139
      //MHC: Soluci�n t�ctica - Fase II - Agregado de marca de Endosable
      wvarPos.set( wvarPos.add( new Variant( 140 ) ) );
    }
    //
    wvarResult = wvarResult + "</RS>";
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REGS'>";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REG'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='PROD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,1,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,5,8)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERPOL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,13,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERANN'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,17,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERSEC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,21,6)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CLIDES'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,40,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='TOMA'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,70,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='EST'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,100,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SINI'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,130,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='ALERTEXI'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,131,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='AGE'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,132,2)' />-<xsl:value-of select='substring(.,134,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RAMO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,138,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='MARCAREIMPRESION'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,139,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //
    //MHC: Soluci�n t�ctica - Fase II - Agregado de marca de Endosable
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='MARCAENDOSABLE'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,140,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='TOMA'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='EST'/>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    //
    return p_GetXSL;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
