package com.qbe.services.ovmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import java.io.File;
import com.qbe.services.ovmq.impl.ModGeneral;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
import com.qbe.vbcompat.format.VBFixesUtil;


/**

 * Objetos del FrameWork

 */



public class lbaw_OVRolesAdministrativa implements VBObjectClass

{

  /**

   * Implementacion de los objetos

   * 

   * Datos de la accion

   */

  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVRolesAdministrativa";

  static final String mcteOpID = "1413";

  /**

   * Parametros XML de Entrada

   */

  static final String mcteParam_Usuario = "//USUARIO";

  static final String mcteParam_Tipo = "//TIPO";

  private Object mobjCOM_Context = null;

  private EventLog mobjEventLog = new EventLog();

  /**

   * static variable for method: IAction_Execute

   */

  private final String wcteFnName = "IAction_Execute";



  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {

    int IAction_Execute = 0;

    Variant vbLogEventTypeError = new Variant();

    XmlDomExtended wobjXMLRequest = null;

    XmlDomExtended wobjXMLConfig = null;

    XmlDomExtended wobjXMLParametros = null;

    XmlDomExtended wobjXMLResponse = null;

    XmlDomExtended wobjXSLResponse = null;

    int wvarMQError = 0;

    String wvarArea = "";

    MQProxy wobjFrame2MQ = null;

    int wvarStep = 0;

    String wvarResult = "";

    String wvarCiaAsCod = "";

    String wvarUsuario = "";

    String wvarTipo = "";

    Variant wvarPos = new Variant();

    String strParseString = "";

    int wvarstrLen = 0;

    String wvarEstado = "";

    //

    //

    //

    //

    //

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~

    try 

    {

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //

      wvarStep = 10;

      //Levanto los par�metros que llegan desde la p�gina

      wobjXMLRequest = new XmlDomExtended();

      wobjXMLRequest.loadXML( Request );

      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );

      if( wobjXMLRequest.selectNodes( mcteParam_Tipo ) .getLength() != 0 )

      {

        //es para que solo devuelva el nivel PR

        wvarTipo = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Tipo )  );

      }

      else

      {

        //todos los niveles

        wvarTipo = "N";

      }

      //

      //

      wvarStep = 20;

      wobjXMLRequest = null;

      //

      wvarStep = 30;

      //Levanto los datos de la cola de MQ del archivo de configuraci�n

      wobjXMLConfig = new XmlDomExtended();

      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));

      //

      //Levanto los Parametros de la cola de MQ del archivo de configuraci�n

      wvarStep = 60;

      wobjXMLParametros = new XmlDomExtended();

      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));

      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );

      wobjXMLParametros = null;

      //

      wvarStep = 80;

      //Genera y conecta el manager MQ

      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + "000000000    000000000  000000000  000000000" + wvarTipo;



      wvarStep = 100;

      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 8 ) );

      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();


      //

      wvarStep = 150;

      if( wvarMQError != 0 )

      {

        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        return IAction_Execute;

      }



      wvarStep = 190;

      wvarResult = "";

      //cantidad de caracteres ocupados por par�metros de entrada

      wvarPos.set( 68 );

      //

      wvarstrLen = Strings.len( strParseString );

      //

      wvarStep = 200;

      //Corto el estado

      wvarEstado = Strings.mid( strParseString, 19, 2 );

      //

      if( wvarEstado.equals( "ER" ) )

      {

        //

        wvarStep = 210;

        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );

        //

      }

      else

      {

        //

        wvarStep = 240;

        wvarResult = "";

        wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);

        //

        wvarStep = 250;

        wobjXMLResponse = new XmlDomExtended();

        wobjXSLResponse = new XmlDomExtended();

        //

        wvarStep = 260;

        wobjXMLResponse.loadXML( wvarResult );

        //

        if( wobjXMLResponse.selectNodes( "//R" ) .getLength() != 0 )

        {

          wvarStep = 270;

          wobjXSLResponse.loadXML( p_GetXSL());

          //

          wvarStep = 280;

          wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );

          //

          wvarStep = 290;

          wobjXMLResponse = null;

          wobjXSLResponse = null;

          //

          wvarStep = 300;

          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );

          //

        }

        else

        {

          //

          wvarStep = 270;

          wobjXMLResponse = null;

          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );

          //

        }

      }

      //

      wvarStep = 320;

      /*TBD mobjCOM_Context.SetComplete() ;*/

      IAction_Execute = 0;

      //

      //~~~~~~~~~~~~~~~

      ClearObjects: 

      //~~~~~~~~~~~~~~~

      // LIBERO LOS OBJETOS

      wobjXMLConfig = null;

      return IAction_Execute;

      //

      //~~~~~~~~~~~~~~~

    }

    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());

      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);

      try 

       {

        //~~~~~~~~~~~~~~~

        //

        wobjXMLResponse = null;

        wobjXSLResponse = null;

        //

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + " Hora:" + DateTime.now(), vbLogEventTypeError );

        IAction_Execute = 1;

        /*TBD mobjCOM_Context.SetAbort() ;*/

        Err.clear();

        return IAction_Execute;

      }

      catch( Exception _e2_ )

      {

      }

    }

    return IAction_Execute;

  }



  private void ObjectControl_Activate() throws Exception

  {

  }



  private boolean ObjectControl_CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    return ObjectControl_CanBePooled;

  }



  private void ObjectControl_Deactivate() throws Exception

  {

  }



  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception

  {

    String ParseoMensaje = "";

    String wvarResult = "";

    int wvarCantLin = 0;

    int wvarCounter = 0;

    //

    wvarResult = wvarResult + "<RS>";

    //

    if( !Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ).equals( "" ) )

    {

      wvarCantLin = Obj.toInt( Strings.mid( strParseString, wvarPos.toInt(), 4 ) );

      wvarPos.set( wvarPos.add( new Variant( 4 ) ) );

      //

      for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )

      {

        wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 41 ) + "]]></R>";

        wvarPos.set( wvarPos.add( new Variant( 41 ) ) );

      }

      //

    }

    wvarResult = wvarResult + "</RS>";

    //

    ParseoMensaje = wvarResult;

    //

    return ParseoMensaje;

  }



  private String p_GetXSL() throws Exception

  {

    String p_GetXSL = "";

    String wvarStrXSL = "";

    //

    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";

    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";

    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";

    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REGS'>";

    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";

    wvarStrXSL = wvarStrXSL + "         </xsl:element>";

    wvarStrXSL = wvarStrXSL + "     </xsl:template>";

    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";

    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REG'>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='NOMBRE'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,1,30))' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='NIVEL'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,31,2)' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CLIENSEC'>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number(number(substring(.,33,9)), \"0\")' />";

    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    //

    wvarStrXSL = wvarStrXSL + "         </xsl:element>";

    wvarStrXSL = wvarStrXSL + "     </xsl:template>";

    //

    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='NOMBRE'/>";

    //

    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";

    //

    p_GetXSL = wvarStrXSL;

    //

    return p_GetXSL;

  }



  public void Activate() throws Exception

  {

    //

    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/

    //

  }



  public boolean CanBePooled() throws Exception

  {

    boolean ObjectControl_CanBePooled = false;

    //

    ObjectControl_CanBePooled = true;

    //

    return ObjectControl_CanBePooled;

  }



  public void Deactivate() throws Exception

  {

    //

    mobjCOM_Context = (Object) null;

    mobjEventLog = null;

    //

  }

}

