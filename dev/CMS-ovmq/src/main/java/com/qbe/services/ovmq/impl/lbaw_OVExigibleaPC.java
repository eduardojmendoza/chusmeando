package com.qbe.services.ovmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.db.AdoUtils;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import java.io.File;
import com.qbe.services.ovmq.impl.ModGeneral;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.framework.VBObjectClass;
import diamondedge.util.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVExigibleaPC implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVExigibleaPC";
  static final String mcteOpID = "1419";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_NivelAs = "//NIVELAS";
  static final String mcteParam_ClienSecAs = "//CLIENSECAS";
  static final String mcteParam_Nivel1 = "//NIVEL1";
  static final String mcteParam_ClienSec1 = "//CLIENSEC1";
  static final String mcteParam_Nivel2 = "//NIVEL2";
  static final String mcteParam_ClienSec2 = "//CLIENSEC2";
  static final String mcteParam_Nivel3 = "//NIVEL3";
  static final String mcteParam_ClienSec3 = "//CLIENSEC3";
  static final String mcteParam_TIPO_DISPLAY = "//TIPO_DISPLAY";
  static final String mcteParam_Origen = "//ORIGEN";
  /**
   *  TIPOS DE DISPLAY
   */
  static final String mcteDownload_TXT = "1";
  static final String mcteDownload_CSV = "2";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  private String strParseString = "";
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarResultParcial = "";
    String wvarStringTitulo = "";
    String wvarUsuario = "";
    String wvarNivelAs = "";
    String wvarClienSecAs = "";
    String wvarNivel1 = "";
    String wvarClienSec1 = "";
    String wvarNivel2 = "";
    String wvarClienSec2 = "";
    String wvarNivel3 = "";
    String wvarClienSec3 = "";
    String wvarCiaAsCod = "";
    String wvarTipoDisplay = "";
    String wvarContinuar = "";
    String wvarOrigen = "";
    Variant wvarPos = new Variant();
    int wvarstrLen = 0;
    String wvarEstado = "";
    String wvarParametros = "";
    boolean wvarStatus = false;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarNivelAs = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NivelAs )  ) + Strings.space( 2 ), 2 );
      wvarClienSecAs = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSecAs )  ), 9 );
      wvarNivel1 = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Nivel1 )  ) + Strings.space( 2 ), 2 );
      wvarClienSec1 = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec1 )  ), 9 );
      wvarNivel2 = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Nivel2 )  ) + Strings.space( 2 ), 2 );
      wvarClienSec2 = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec2 )  ), 9 );
      wvarNivel3 = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Nivel3 )  ) + Strings.space( 2 ), 2 );
      wvarClienSec3 = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClienSec3 )  ), 9 );
      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarTipoDisplay = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TIPO_DISPLAY )  );
      wvarOrigen = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Origen )  );
      //
      wvarStep = 20;
      wobjXMLRequest = null;
      //
      wvarStep = 30;
      //
      //Levanto los Parametros de la cola de MQ del archivo de configuración
      wvarStep = 60;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );
      wobjXMLParametros = null;
      //
      wvarStep = 80;
      wvarParametros = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + wvarNivel1 + wvarClienSec1 + wvarNivel2 + wvarClienSec2 + wvarNivel3 + wvarClienSec3 + wvarOrigen;
      //
      wvarStep = 140;
      wvarContinuar = Strings.space( 57 );
      //CORREGIDO: el conversor se olvidó una instanciación. El original es:
      //		    wvarStatus = CorrerMensaje(wvarParametros, wvarContinuar)
      wvarStatus = this.CorrerMensaje(wvarParametros,wvarContinuar);
      //
      if( wvarStatus == false ) 
      {
        wvarStep = 150;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
        IAction_Execute = 0;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [-1] - EL SERVICIO DE CONSULTA NO SE ENCUENTRA DISPONIBLE - Timeout Expired", vbLogEventTypeError );
        //
        //unsup GoTo ClearObjects
        //
      }

      //
      wvarStep = 160;
      wvarResult = "";
      //cantidad de caracteres ocupados por parámetros de entrada
      wvarPos.set( 125 );
      //
      wvarStep = 170;
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 180;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 190;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 200;
        wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);
        //
        while( wvarEstado.equals( "TR" ) )
        {
          //
          wvarStep = 210;
          wvarContinuar = Strings.mid( strParseString, 68, 57 );
          //
          wvarStep = 220;
          //CORREGIDO: el conversor se olvidó una instanciación. El original es:
          //		    wvarStatus = CorrerMensaje(wvarParametros, wvarContinuar)
          wvarStatus = this.CorrerMensaje(wvarParametros,wvarContinuar);
          //
          if( wvarStatus == false )
          {
            //
            wvarStep = 230;
            Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
            IAction_Execute = 0;
            //
            mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [-1] - EL SERVICIO DE CONSULTA NO SE ENCUENTRA DISPONIBLE - Timeout Expired", vbLogEventTypeError );
            //
            return IAction_Execute;
          }
          //
          //
          wvarStep = 240;
          //cantidad de caracteres ocupados por parámetros de entrada
          wvarPos.set( 125 );
          //
          wvarstrLen = Strings.len( strParseString );
          //
          wvarStep = 250;
          //Corto el estado
          wvarEstado = Strings.mid( strParseString, 19, 2 );
          //
          if( wvarEstado.equals( "ER" ) )
          {
            //
            wvarStep = 260;
            Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
            //
          }
          //
          wvarStep = 270;
          wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);
          //
        }
        //
        wvarStep = 280;
        wobjXMLResponse = new XmlDomExtended();
        wobjXSLResponse = new XmlDomExtended();
        //
        wvarStep = 290;
        wobjXMLResponse.loadXML( "<RS>" + wvarResult + "</RS>" );
        //
        wvarStep = 300;
        if( wobjXMLResponse.selectNodes( "//R" ) .getLength() != 0 )
        {
          //
          wvarStep = 310;
          if( wvarTipoDisplay.equals( mcteDownload_CSV ) )
          {
            //
            wvarStep = 320;
            wobjXSLResponse.loadXML( p_GetXSL_CSV());
            wvarStringTitulo = "CODIGO,NOMBRE,PROD,POLIZA,CERTIF,ESTADO,ENDOSO,RECIBO,MONEDA,IMPORTE,CANAL,FEC.VTO.,DIAS,MOTIVO" + System.getProperty("line.separator");
            //
          }
          else if( wvarTipoDisplay.equals( mcteDownload_TXT ) )
          {
            //
            wvarStep = 330;
            wobjXSLResponse.loadXML( p_GetXSL_TXT());
            wvarStringTitulo = "";
            //
          }
          //
          wvarStep = 340;

          wvarResultParcial = wvarResultParcial + Strings.replace( wobjXMLResponse.transformNode( wobjXSLResponse ), "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "" );
          wvarResultParcial = wvarStringTitulo + wvarResultParcial;
          //
          wvarStep = 350;
          wobjXMLResponse = null;
          wobjXSLResponse = null;
          //
        }
        else
        {
          //
          wvarStep = 360;
          wobjXMLResponse = null;
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          //
        }

        wvarStep = 370;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResultParcial + "</Response>" );
        //
      }
      //
      wvarStep = 380;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
	}
	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());
      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
      try 
       {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarParametros + wvarContinuar + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCantLin = 0;
    int wvarCounter = 0;
    //
    if( !Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ).equals( "" ) )
    {
      wvarCantLin = Obj.toInt( Strings.mid( strParseString, wvarPos.toInt(), 4 ) );
      wvarPos.set( wvarPos.add( new Variant( 4 ) ) );
      //
      for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )
      {
        wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 138 ) + "]]></R>";
        wvarPos.set( wvarPos.add( new Variant( 138 ) ) );
      }
      //
    }
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  private String p_GetXSL_CSV() throws Exception
  {
    String p_GetXSL_CSV = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";

    wvarStrXSL = wvarStrXSL + "<xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,1,6)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,7,30)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,37,4)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,41,8)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,49,14)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,63,3)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,67,6)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,73,9)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,82,3)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,85,1)' /> ";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='format-number((number(substring(.,86,14)) div 100), \"########0.00\", \"european\")' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,100,4)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,104,10)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,114,1)' />";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='number(substring(.,115,4))' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='normalize-space(substring(.,119,20))' />";
    
    wvarStrXSL = wvarStrXSL + "<xsl:text><![CDATA[" + System.getProperty("line.separator") + "]]></xsl:text>";
    
    wvarStrXSL = wvarStrXSL + "</xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL_CSV = wvarStrXSL;
    //
    return p_GetXSL_CSV;
  }

  public boolean CorrerMensaje( String pvarParametros, String pvarContinuar ) throws Exception
  {
    boolean CorrerMensaje = false;
    int wvarMQError = 0;
    MQProxy wobjFrame2MQ = null;

    wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(pvarParametros + pvarContinuar, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();

    //
    if( wvarMQError == 0 )
    {
      CorrerMensaje = true;
    }
    else
    {
      /*TBD mobjCOM_Context.SetComplete() ;*/
      CorrerMensaje = false;
    }
    //
    return CorrerMensaje;
  }

  private String p_GetXSL_TXT() throws Exception
  {
    String p_GetXSL_TXT = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";

    wvarStrXSL = wvarStrXSL + "<xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,1,6)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,7,30)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,37,4)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,41,8)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,49,14)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,63,3)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,67,6)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,73,9)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,82,3)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,85,1)' />";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='format-number((number(substring(.,86,14)) div 100), \"########0.00\", \"european\")' />&#009;";
    wvarStrXSL = wvarStrXSL + "     <xsl:if test='string-length(format-number((number(substring(.,86,14)) div 100), \"########0.00\", \"european\")) &lt; 7'>";
    wvarStrXSL = wvarStrXSL + "&#009;";
    wvarStrXSL = wvarStrXSL + "     </xsl:if>";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,100,4)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,104,10)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,114,1)' />";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='number(substring(.,115,4))' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,119,20)' />";
    
    wvarStrXSL = wvarStrXSL + "<xsl:text><![CDATA[" + System.getProperty("line.separator") + "]]></xsl:text>";
    wvarStrXSL = wvarStrXSL + "</xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL_TXT = wvarStrXSL;
    //
    return p_GetXSL_TXT;
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
