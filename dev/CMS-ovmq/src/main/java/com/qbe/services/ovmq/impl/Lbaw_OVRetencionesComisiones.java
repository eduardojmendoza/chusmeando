package com.qbe.services.ovmq.impl;

import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;

/**
 * 
 * Objetos del FrameWork
 * 
 */

public class Lbaw_OVRetencionesComisiones implements VBObjectClass

{

	/**
	 * 
	 * Implementacion de los objetos
	 * 
	 * Datos de la accion
	 * 
	 */

	static final String mcteClassName = "lbawA_OVMQ.Lbaw_OVRetencionesComisiones";

	static final String mcteOpID = "1804";

	/**
	 * 
	 * Parametros XML de Entrada
	 * 
	 */

	static final String mcteParam_Usuario = "//USUARIO";

	static final String mcteParam_Producto = "//PRODUCTO";

	static final String mcteParam_Poliza = "//POLIZA";

	static final String mcteParam_Certi = "//CERTI";

	// <ENTRADA>
	//
	// <CAMPO Nombre="CIAASCOD" TipoDato="TEXTO" Enteros="4" Default="0001"/>
	static final String mcteParam_Ciaascod = "//CIAASCOD";
	// <CAMPO Nombre="USUARCOD" TipoDato="TEXTO" Enteros="10"/>
	static final String mcteParam_Usuarcod = "//USUARCOD";
	// <CAMPO Nombre="ESTADO" TipoDato="TEXTO" Enteros="2"/>
	static final String mcteParam_Estado = "//ESTADO";
	// <CAMPO Nombre="ERROR" TipoDato="TEXTO" Enteros="2"/>
	static final String mcteParam_Error = "//ERROR";
	// <CAMPO Nombre="CLIENSECAS" TipoDato="ENTERO" Enteros="9"/>
	static final String mcteParam_Cliensecas = "//CLIENSECAS";
	// <CAMPO Nombre="NIVELCLAAS" TipoDato="TEXTO" Enteros="2"/>
	static final String mcteParam_Nivelclaas = "//NIVELCLAAS";
	// <CAMPO Nombre="NIVELCLA1" TipoDato="TEXTO" Enteros="2"/>
	static final String mcteParam_Nivelcla1 = "//NIVELCLA1";
	// <CAMPO Nombre="CLIENSEC1" TipoDato="ENTERO" Enteros="9"/>
	static final String mcteParam_Cliensec1 = "//CLIENSEC1";
	// <CAMPO Nombre="NIVELCLA2" TipoDato="TEXTO" Enteros="2"/>
	static final String mcteParam_Nivelcla2 = "//NIVELCLA2";
	// <CAMPO Nombre="CLIENSEC2" TipoDato="ENTERO" Enteros="9"/>
	static final String mcteParam_Cliensec2 = "//CLIENSEC2";
	// <CAMPO Nombre="NIVELCLA3" TipoDato="TEXTO" Enteros="2"/>
	static final String mcteParam_Nivelcla3 = "//NIVELCLA3";
	// <CAMPO Nombre="CLIENSEC3" TipoDato="ENTERO" Enteros="9"/>
	static final String mcteParam_Cliensec3 = "//CLIENSEC3";
	// <CAMPO Nombre="CLIENSEC" TipoDato="ENTERO" Enteros="9"/>
	static final String mcteParam_Cliensec = "//CLIENSEC";
	// <CAMPO Nombre="LIQUISEC" TipoDato="ENTERO" Enteros="6"/>
	static final String mcteParam_Liquisec = "//LIQUISEC";
	// <CAMPO Nombre="TIPOCTAC" TipoDato="TEXTO" Enteros="1"/>
	static final String mcteParam_Tipoctac = "//TIPOCTAC";
	// <CAMPO Nombre="RECIBANN" TipoDato="ENTERO" Enteros="2"/>
	static final String mcteParam_Recibann = "//RECIBANN";
	// <CAMPO Nombre="RECIBTIP" TipoDato="ENTERO" Enteros="1"/>
	static final String mcteParam_Recibtip = "//RECIBTIP";
	// <CAMPO Nombre="RECIBSEC" TipoDato="ENTERO" Enteros="6"/>
	static final String mcteParam_Recibsec = "//RECIBSEC";
	// <CAMPO Nombre="RAMOPCOD" TipoDato="TEXTO" Enteros="4"/>
	static final String mcteParam_Ramopcod = "//RAMOPCOD";
	// <CAMPO Nombre="POLIZANN" TipoDato="ENTERO" Enteros="2" Default="0"/>
	static final String mcteParam_Polizann = "//POLIZANN";
	// <CAMPO Nombre="POLIZSEC" TipoDato="ENTERO" Enteros="6" Default="0"/>
	static final String mcteParam_Polizsec = "//POLIZSEC";
	// <CAMPO Nombre="CERTIPOL" TipoDato="ENTERO" Enteros="4" Default="0"/>
	static final String mcteParam_Certipol = "//CERTIPOL";
	// <CAMPO Nombre="CERTIANN" TipoDato="ENTERO" Enteros="4" Default="0"/>
	static final String mcteParam_Certiann = "//CERTIANN";
	// <CAMPO Nombre="CERTISEC" TipoDato="ENTERO" Enteros="6" Default="0"/>
	static final String mcteParam_certisec = "//CERTISEC";
	// <CAMPO Nombre="SUPLENUM" TipoDato="ENTERO" Enteros="4"/>
	static final String mcteParam_Suplenum = "//SUPLENUM";
	// <CAMPO Nombre="OPERAPOL" TipoDato="ENTERO" Enteros="18" Default="0"/>
	static final String mcteParam_Operapol = "//OPERAPOL";
	// </ENTRADA>
	//

	private Object mobjCOM_Context = null;

	private EventLog mobjEventLog = new EventLog();

	/**
	 * 
	 * static variable for method: IAction_Execute
	 * 
	 */

	private final String wcteFnName = "IAction_Execute";

	@Override
	public int IAction_Execute(String Request, StringHolder Response, String ContextInfo) {

		int IAction_Execute = 0;

		Variant vbLogEventTypeError = new Variant();

		XmlDomExtended wobjXMLRequest = null;

		XmlDomExtended wobjXMLConfig = null;

		XmlDomExtended wobjXMLParametros = null;

		int wvarMQError = 0;

		StringBuffer wvarArea = new StringBuffer();

		MQProxy wobjFrame2MQ = null;

		int wvarStep = 0;

		String wvarResult = "";

		String wvarCiaAsCod = "";

		String wvarUsuario = "";

		String ciaascod = "";
		String usuarcod = "";
		String estado = "";
		String error = "";
		String cliensecas = "";
		String nivelclaas = "";
		String nivelcla1 = "";
		String cliensec1 = "";
		String nivelcla2 = "";
		String cliensec2 = "";
		String nivelcla3 = "";
		String cliensec3 = "";
		String cliensec = "";
		String liquisec = "";
		String tipoctac = "";
		String recibann = "";
		String recibtip = "";
		String recibsec = "";
		String ramopcod = "";
		String polizann = "";
		String polizsec = "";
		String certipol = "";
		String certiann = "";
		String certisec = "";
		String suplenum = "";
		String operapol = "";

		String wvarProducto = "";

		String wvarPoliza = "";

		String wvarCerti = "";

		String wvarSuplenum = "";

		int wvarPos = 0;

		int wvarMaxLen = 0;

		String strParseString = "";

		int wvarstrLen = 0;

		String wvarEstado = "";

		//

		//

		//

		//

		//

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~

		try

		{

			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~

			//

			wvarStep = 10;

			// Levanto los par�metros que llegan desde la p�gina

			wobjXMLRequest = new XmlDomExtended();

			wobjXMLRequest.loadXML(Request);

			// Deber� venir desde la p�gina

			ciaascod = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Ciaascod)) + Strings.space(4), 4);
			usuarcod = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Usuarcod)) + Strings.space(10),
					10);
			estado = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Estado)) + Strings.space(2), 2);
			error = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Error)) + Strings.space(2), 2);
			cliensecas = Strings.right(Strings.fill(9, "0")
					+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Cliensecas)), 9);
			nivelclaas = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Nivelclaas)) + Strings.space(2),
					2);
			nivelcla1 = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Nivelcla1)) + Strings.space(2), 2);
			cliensec1 = Strings.right(
					Strings.fill(9, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Cliensec1)),
					9);
			nivelcla2 = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Nivelcla2)) + Strings.space(2), 2);
			cliensec2 = Strings.right(
					Strings.fill(9, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Cliensec2)),
					9);
			nivelcla3 = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Nivelcla3)) + Strings.space(2), 2);
			cliensec3 = Strings.right(
					Strings.fill(9, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Nivelcla3)),
					9);
			cliensec = Strings.right(
					Strings.fill(9, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Cliensec)),
					9);
			liquisec = Strings.right(
					Strings.fill(6, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Liquisec)),
					6);
			tipoctac = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Tipoctac)) + Strings.space(1), 1);
			recibann = Strings.right(
					Strings.fill(2, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Recibann)),
					2);
			recibtip = Strings.right(
					Strings.fill(1, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Recibtip)),
					1);
			recibsec = Strings.right(
					Strings.fill(6, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Recibsec)),
					6);
			ramopcod = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Ramopcod)) + Strings.space(4), 4);
			polizann = Strings.right(
					Strings.fill(2, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Polizann)),
					2);
			polizsec = Strings.right(
					Strings.fill(6, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Polizsec)),
					6);
			certipol = Strings.right(
					Strings.fill(4, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Certipol)),
					4);
			certiann = Strings.right(
					Strings.fill(4, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Certiann)),
					4);
			certisec = Strings.right(
					Strings.fill(6, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_certisec)),
					6);
			suplenum = Strings.right(
					Strings.fill(4, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Suplenum)),
					4);
			operapol = Strings.right(
					Strings.fill(18, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Operapol)),
					18);

			/**
			 * 
			 * wvarUsuario = Strings.left( XmlDomExtended.getText(
			 * wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) ) +
			 * Strings.space( 10 ), 10 );
			 * 
			 * wvarProducto = Strings.left( XmlDomExtended.getText(
			 * wobjXMLRequest.selectSingleNode( mcteParam_Producto ) ) +
			 * Strings.space( 4 ), 4 );
			 * 
			 * wvarPoliza = Strings.right( Strings.fill( 8, "0" ) +
			 * XmlDomExtended.getText( wobjXMLRequest.selectSingleNode(
			 * mcteParam_Poliza ) ), 8 );
			 * 
			 * wvarCerti = Strings.right( Strings.fill( 14, "0" ) +
			 * XmlDomExtended.getText( wobjXMLRequest.selectSingleNode(
			 * mcteParam_Certi ) ), 14 );
			 * 
			 **/

			if (wobjXMLRequest.selectNodes(mcteParam_Suplenum).getLength() != 0)

			{

				wvarSuplenum = Strings.right(Strings.fill(4, "0")
						+ XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Suplenum)), 4);

			}

			else

			{

				wvarSuplenum = Strings.fill(4, "0");

			}

			//

			//

			wvarStep = 20;

			wobjXMLRequest = null;

			//

			wvarStep = 30;

			// Levanto los datos de la cola de MQ del archivo de configuraci�n

			wobjXMLConfig = new XmlDomExtended();

			wobjXMLConfig.load(
					Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));

			//

			// Levanto los Parametros de la cola de MQ del archivo de
			// configuraci�n

			wvarStep = 60;

			wobjXMLParametros = new XmlDomExtended();

			wobjXMLParametros.load(
					Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));

			wvarCiaAsCod = XmlDomExtended.getText(
					wobjXMLParametros.selectSingleNode(ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD));

			wobjXMLParametros = null;

			//

			wvarStep = 80;

			wvarArea.append(mcteOpID);
			wvarArea.append(ciaascod);
			wvarArea.append(usuarcod);
			wvarArea.append(estado);
			wvarArea.append(error);
			wvarArea.append(cliensecas);
			wvarArea.append(nivelclaas);
			wvarArea.append(nivelcla1);
			wvarArea.append(cliensec1);
			wvarArea.append(nivelcla2);
			wvarArea.append(cliensec2);
			wvarArea.append(nivelcla3);
			wvarArea.append(cliensec3);
			wvarArea.append(cliensec);
			wvarArea.append(liquisec);
			wvarArea.append(tipoctac);
			wvarArea.append(recibann);
			wvarArea.append(recibtip);
			wvarArea.append(recibsec);
			wvarArea.append(ramopcod);
			wvarArea.append(polizann);
			wvarArea.append(polizsec);
			wvarArea.append(certipol);
			wvarArea.append(certiann);
			wvarArea.append(certisec);
			wvarArea.append(suplenum);
			wvarArea.append(operapol);

			XmlDomExtended
					.setText(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL"),
							String.valueOf(VB
									.val(XmlDomExtended
											.getText(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL")))
									* 36));
			/*
			 * wobjFrame2MQ = MQProxy.getInstance(wvarConfFileName);
			 * StringHolder strParseStringHolder = new StringHolder(
			 * strParseString); wvarMQError = wobjFrame2MQ.execute(wvarOpID +
			 * wvarArea, strParseStringHolder); strParseString =
			 * strParseStringHolder.getValue();
			 * 
			 */

			wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
			StringHolder strParseStringHolder = new StringHolder(strParseString);
			wvarMQError = wobjFrame2MQ.execute(wvarArea.toString(), strParseStringHolder);
			strParseString = strParseStringHolder.getValue();

			//

			if (wvarMQError != 0)

			{

				Response.set("<Response><Estado resultado=" + String.valueOf((char) (34)) + "false"
						+ String.valueOf((char) (34)) + " mensaje=" + String.valueOf((char) (34))
						+ "El servicio de consulta no se encuentra disponible" + String.valueOf((char) (34)) + " />"
						+ "Codigo Error:" + wvarMQError + "</Response>");

				mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"),
						mcteClassName + "--" + mcteClassName,
						wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - "
								+ strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(),
						vbLogEventTypeError);

				IAction_Execute = 1;

				/* TBD mobjCOM_Context.SetAbort() ; */

				return IAction_Execute;

			}

			wvarStep = 180;

			wvarResult = "";

			// cantidad de caracteres ocupados por par�metros de entrada

			wvarPos = Strings.len(wvarArea.toString()) + 1;
			wvarMaxLen = 1506;
			//

			wvarstrLen = Strings.len(strParseString);
			strParseString = Strings.mid(strParseString, 1, (wvarMaxLen + wvarPos) - 1);

			//

			wvarStep = 190;

			// Corto el estado

			wvarEstado = Strings.mid(strParseString, 19, 2);

			if (wvarEstado.equals("ER")){
				wvarStep = 200;

				Response.set(
						"<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>");
			}else{
				//

				wvarStep = 230;

				wvarResult = wvarResult + "<CAMPOS>";

				wvarResult = wvarResult + parseoMensaje(wvarPos, strParseString, wvarstrLen);

				//

				wvarStep = 240;

				wvarResult = wvarResult + "</CAMPOS>";

				//

				wvarStep = 250;

				Response.set("<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>");


			}

			//

			wvarStep = 260;

			/* TBD mobjCOM_Context.SetComplete() ; */

			IAction_Execute = 0;

			//

			// ~~~~~~~~~~~~~~~

			
			// ~~~~~~~~~~~~~~~

			// LIBERO LOS OBJETOS

			wobjXMLConfig = null;

			return IAction_Execute;

			//

			// ~~~~~~~~~~~~~~~

		}

		catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
			Response.set(
					"<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>");
			return 3;
		} catch (Exception _e_) {
			Err.set(_e_);
			java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());

			logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);

			try

			{

				// ~~~~~~~~~~~~~~~

				//

				mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName,
						wcteFnName, wvarStep, Err.getError().getNumber(),
						"Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:"
								+ mcteOpID + wvarCiaAsCod + wvarUsuario + wvarProducto + wvarPoliza + wvarCerti
								+ wvarSuplenum + " Hora:" + DateTime.now(),
						vbLogEventTypeError);

				IAction_Execute = 1;

				/* TBD mobjCOM_Context.SetAbort() ; */

				Err.clear();

				return IAction_Execute;

			}

			catch (Exception _e2_)

			{

			}

		}

		return IAction_Execute;

	}

	private String parseoMensaje(int wvarPos, String strParseString, int wvarstrLen) throws Exception

	{

		String wvarResult = "";

		// Es un solo dato.
		int wvarCantLin = Integer.valueOf(Strings.mid(strParseString, wvarPos, 6));
		wvarResult = wvarResult + "<CANDEVUE>" + wvarCantLin + "</CANDEVUE>";
		wvarPos += 6;
		
		wvarResult = wvarResult + "<RESULTADOS>";
		for (int wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++) {
			wvarResult = wvarResult + "<RESULTADO>";
			wvarResult = wvarResult + "<FORMUDES><![CDATA["+ Strings.trim(Strings.mid(strParseString, wvarPos, 60)) + "]]></FORMUDES>";
			wvarResult = wvarResult + "<NOMARCH><![CDATA["+ Strings.trim(Strings.mid(strParseString, wvarPos + 60, 90)) + "]]></NOMARCH>";
			wvarResult = wvarResult + "</RESULTADO>";

			wvarPos += 150;
		}

		wvarResult = wvarResult + "</RESULTADOS>";

		return wvarResult;

	}

	public void Activate() throws Exception

	{

		//

		/* TBD mobjCOM_Context = this.GetObjectContext() ; */

		//

	}

	public boolean CanBePooled() throws Exception

	{

		boolean ObjectControl_CanBePooled = false;

		//

		ObjectControl_CanBePooled = true;

		//

		return ObjectControl_CanBePooled;

	}

	public void Deactivate() throws Exception

	{

		//

		mobjCOM_Context = (Object) null;

		mobjEventLog = null;

		//

	}

}
