package com.qbe.services.ovmq.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.testutils.TestUtils;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;

/**
 * Corre los tests del OVMQ levantando los archivos de log
 * 
 * @author ramiro
 *
 */
public class OVMQMessageTest {
	
	public OVMQMessageTest() {
		super();
	}
	
	/**
	 * Testea la ejecución del mensaje, y que no vuelva un código de error. Valida contra la respuesta esperada
	 * 
	 * @param msgCode
	 * @throws Exception 
	 */
	protected void testMessageFullRequest (String msgCode, VBObjectClass comp) throws Exception {
		String responseText = this.testMessageJustExecution(msgCode, comp);
	
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		XMLUnit.setIgnoreWhitespace(true);
		String expectedResponse = TestUtils.loadResponse(msgCode);
		if ( expectedResponse == null || expectedResponse.length()==0) throw new Exception("No se pudo cargar el request");
		DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(expectedResponse, responseText));
		List allDifferences = myDiff.getAllDifferences();
		assertEquals(myDiff.toString(), 0, allDifferences.size());
		//Eliminar los &lt; y &gt;
		XMLAssert.assertXMLEqual("No volvió la respuesta esperada", expectedResponse, responseText);
	
	}
	
	/**
	 * Testea la ejecución del mensaje, y que no vuelva un código de error. No valida contra la respuesta esperada, la devuelve solamente
	 * 
	 * @param msgCode
	 * @throws Exception 
	 */
	protected String testMessageJustExecution (String msgCode, VBObjectClass comp) throws Exception {
		String request = TestUtils.loadRequest(msgCode);
		if ( request == null || request.length()==0 || request.trim().equals("")) throw new Exception("No se pudo cargar el request");
		StringHolder responseHolder = new StringHolder();
		int result = comp.IAction_Execute(request, responseHolder, null);
//		System.out.println("Response: " + responseHolder);
		if ( result == 1) Assert.fail ("Abortó la ejecución, ver el log");
		Assert.assertTrue("Result no es 1 ni 0, es un error",result == 0);
		try {
			TestUtils.parseResultado(responseHolder.getValue());
			TestUtils.parseMensaje(responseHolder.getValue());
		} catch (Exception e){
			assertTrue("No es posible parsear la respuesta", false);
		}
		String responseText = responseHolder.getValue();
		if (( responseText == null) || ( responseText.length()==0)) {
			fail("responseText vacío");
		}
		return responseText;
	}
	
	//------------------------------ T E S T ------------------------------------------	

	@Test
	public void testMSG_1010() throws Exception {
		String msgCode = "1010";
		testMessageFullRequest(msgCode,new lbaw_OVClientesConsulta());
	}
	@Ignore
	@Test
	public void testMSG_1102() throws Exception {
		String msgCode = "1102";
		testMessageFullRequest(msgCode,new lbaw_OVDatosGralCliente());
	}
	@Test
	public void testMSG_1103() throws Exception {
		String msgCode = "1103";
		testMessageFullRequest(msgCode, new lbaw_OVEndososDetalles());
	}
	@Test
	public void testMSG_1104() throws Exception {
		String msgCode = "1104";
		testMessageFullRequest(msgCode,new lbaw_OVEndososPrima());
	}
	@Test
	public void testMSG_1106() throws Exception {
		String msgCode = "1106";
		testMessageFullRequest(msgCode, new lbaw_OVOpePendTotales());
	}
	@Test
	public void testMSG_1107() throws Exception {
		String msgCode = "1107";
		testMessageFullRequest(msgCode, new lbaw_OVOpePendDetalles());
	}
	@Test
	public void testMSG_1108() throws Exception {
		String msgCode = "1108";
		testMessageFullRequest(msgCode, new lbaw_OVOpeEmiTotales());
	}
	@Test
	public void testMSG_1109() throws Exception {
		String msgCode = "1109";
		testMessageFullRequest(msgCode, new lbaw_OVOpeEmiDetalles());
	}
	@Test
	public void testMSG_1110() throws Exception {
		String msgCode = "1110";
		testMessageFullRequest(msgCode, new lbaw_OVCoberturasDetalle());
	}
	@Test
	public void testMSG_1113() throws Exception {
		String msgCode = "1113";
		testMessageFullRequest(msgCode, new lbaw_OVAvisoVtoPolTotales());
	}
	@Test
	public void testMSG_1301() throws Exception {
		String msgCode = "1301";
		testMessageFullRequest(msgCode, new lbaw_OVSiniListadoDetalles());
	}
	@Test
	public void testMSG_1302() throws Exception {
		String msgCode = "1302";
		testMessageJustExecution(msgCode, new lbaw_OVSiniConsulta());
	}
	@Test
	public void testMSG_1304() throws Exception {
		String msgCode = "1304";
		testMessageFullRequest(msgCode, new lbaw_OVSiniDetalle());
	}
	@Test
	public void testMSG_1305() throws Exception {
		String msgCode = "1305";
		testMessageFullRequest(msgCode, new lbaw_OVSiniDatosAdic());
	}
	@Test
	public void testMSG_1306() throws Exception {
		String msgCode = "1306";
		testMessageFullRequest(msgCode, new lbaw_OVSiniGestiones());
	}
	@Test
	public void testMSG_1307() throws Exception {
		String msgCode = "1307";
		testMessageFullRequest(msgCode, new lbaw_OVSiniSubcober());
	}
	@Test
	public void testMSG_1308() throws Exception {
		String msgCode = "1308";
		testMessageFullRequest(msgCode, new lbaw_OVSiniConsPagos());
	}
	@Test
	public void testMSG_1309() throws Exception {
		String msgCode = "1309";
		testMessageFullRequest(msgCode, new lbaw_OVSiniListadoTotales());
	}
	@Test
	@Ignore
	//Falla con <Response><Estado resultado="false" mensaje='No se encontraron datos.' /></Response>
	public void testMSG_1409() throws Exception {
		String msgCode = "1409";
		testMessageFullRequest(msgCode, new lbaw_OVAnulxPagoTotales());
	}
	@Test
	@Ignore //Sin respuesta cargada para el mensaje
	public void testMSG_1400() throws Exception {
		String msgCode = "1400";
		testMessageFullRequest(msgCode, new lbaw_OVExigibleTotales());
	}
	@Test
	public void testMSG_1401() throws Exception {
		String msgCode = "1401";
		testMessageFullRequest(msgCode, new lbaw_OVExigibleDetalles());
	}
	
	@Test
	public void testMSG_1402() throws Exception {
		String msgCode = "1402";
		testMessageFullRequest(msgCode, new lbaw_OVExigibleDetPoliza());
	}
	
	@Test
	@Ignore // Está dando timeout en test
	public void testMSG_1403() throws Exception {
		String msgCode = "1403";
		testMessageFullRequest(msgCode, new lbaw_OVDeudaVceTotales());
	}
	@Test
	public void testMSG_1404() throws Exception {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		String msgCode = "1404";
		testMessageJustExecution(msgCode, new lbaw_OVDeudaVceDetalles());
	}
	@Test
	public void testMSG_1405() throws Exception {
		String msgCode = "1405";
		testMessageFullRequest(msgCode, new lbaw_OVCartasReclaTotales());
	}
	@Test
	public void testMSG_1406() throws Exception {
		String msgCode = "1406";
		testMessageFullRequest(msgCode, new lbaw_OVCartasReclaDetalle());
	}
	@Test
	public void testMSG_1407() throws Exception {
		String msgCode = "1407";
		testMessageFullRequest(msgCode, new lbaw_OVCartasDetRecibos());
	}
	
	@Test
	public void testMSG_1408() throws Exception {
		String msgCode = "1408";
		testMessageFullRequest(msgCode, new lbaw_OVDetalleRecibo());
	}
	
	@Test
	public void testMSG_1411() throws Exception {
		String msgCode = "1411";
		testMessageFullRequest(msgCode, new lbaw_OVSitCobranzas());
	}
	
	@Test
	public void testMSG_1412() throws Exception {
		String msgCode = "1412";
		testMessageFullRequest(msgCode, new lbaw_OVDeudaVceDetPoliza());
	}
	
	@Test
	public void testMSG_1415() throws Exception {
		String msgCode = "1415";
		testMessageFullRequest(msgCode, new lbaw_OVLOGetCantRecibos());
	}
	
	@Test
	public void testMSG_1416() throws Exception {
		String msgCode = "1416";
		testMessageFullRequest(msgCode, new lbaw_OVLOGetRecibos());
	}
	
	@Test
	public void testMSG_1417() throws Exception {
		String msgCode = "1417";
		testMessageFullRequest(msgCode, new lbaw_OVLOGetNroLiqui());
	}
	
	@Test
	public void testMSG_1418() throws Exception {
		String msgCode = "1418";
		testMessageFullRequest(msgCode, new lbaw_OVCliensecxUsu());
	}
	
	@Test
	public void testMSG_1419() throws Exception {
		String msgCode = "1419";
		testMessageJustExecution(msgCode, new lbaw_OVExigibleaPC());
	}

	@Test
	public void testMSG_1420() throws Exception {
		String msgCode = "1420";
		testMessageFullRequest(msgCode, new lbaw_OVLOLiquixProd());
	}
	
	@Test
	public void testMSG_1421() throws Exception {
		String msgCode = "1421";
		testMessageFullRequest(msgCode, new lbaw_OVLOReimpLiqui());
	}
	@Test
	public void testMSG_1801() throws Exception {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		String msgCode = "1801";
		testMessageJustExecution(msgCode, new lbaw_OVCtaCteaPC());
	}
	@Test
	public void testMSG_1803() throws Exception {
		//Lo corremos en dev xq en test falla por timeout
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		String msgCode = "1803";
		testMessageFullRequest(msgCode, new lbaw_OVBrutoFacturar());
	}
	
	@Test
	public void testDecimalFormat() {
//  	XmlDomExtended.setText( wobjNewNodo, Strings.format( XmlDomExtended.getText( wobjNewNodo ), XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "FormatOuput" ) ) ) );
		DecimalFormat formatter = new DecimalFormat("0.0000000",  new DecimalFormatSymbols(Locale.ENGLISH));
      String toBeFormatted = "5.394";
      double d = Double.valueOf(toBeFormatted);
      String output = formatter.format(d);
	  Assert.assertEquals("5.3940000", output);
	  
}
}