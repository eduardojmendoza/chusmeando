package com.qbe.services.ovmq.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.qbe.vbcompat.string.StringHolder;

public class lbaw_OVCtaCteaPCTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIAction_Execute() {
		lbaw_OVCtaCteaPC aPC = new lbaw_OVCtaCteaPC();
		String request = "<Request><USUARIO>EX009005L</USUARIO><CLIENSEC>100029438</CLIENSEC><LIQUISEC>000551</LIQUISEC><TIPO_DISPLAY>2</TIPO_DISPLAY></Request>";
		StringHolder responseSH = new StringHolder();
		aPC.IAction_Execute(request, responseSH, null);
		assertTrue(responseSH.getValue().contains("resultado='true'"));
//		System.out.println(responseSH.getValue());
		
	}

}
