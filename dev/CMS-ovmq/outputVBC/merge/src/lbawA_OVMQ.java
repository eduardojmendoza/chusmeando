import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
import java.applet.*;

public class lbawA_OVMQ extends JApplet
{
  static {
    try {
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (Exception e) { System.out.println(e); }
  }
  public static String Title = "lbawA_OVMQ";
  public static String ProductName = "";
  public static int MajorVersion = 1;
  public static int MinorVersion = 0;
  public static int Revision = 128;
  public static String HelpFile = "";
  public static String Comments = "";
  public static String FileDescription = "";
  public static String CompanyName = "Hsbc Argentina";
  public static String LegalCopyright = "";
  public static String LegalTrademarks = "";

  public lbawA_OVMQ()
  {
  }

  // called only when running as an applet
  public void init()
  {
    getContentPane().setLayout( new java.awt.BorderLayout() );
    Application app = new Application( "lbawA_OVMQ" );
  }

  public String getAppletInfo()
  {
    return "lbawA_OVMQ" + " " + LegalCopyright;
  }

  // called only when running as a stand-alone application
  public static void main( String args[] )
  {
    final Application app = new Application( "lbawA_OVMQ" );
    app.setApplication( new lbawA_OVMQ(), args );
    try
    {
    }
    catch(Exception e) { Err.set(e); }
    app.endApplication();
    javax.swing.SwingUtilities.invokeLater( new Runnable() {
      public void run() {
        Application.setApplication( app );
      }
    });
  }
}
