import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVLOGetNroLiqui implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVLOGetNroLiqui";
  static final String mcteOpID = "1417";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_NivelAs = "//NIVELAS";
  static final String mcteParam_ClienSecAs = "//CLIENSECAS";
  /**
   * Const mcteParam_SWCheDif   As String = "//CHEQUEDIF"
   */
  static final String mcteNodos_Pagos = "//Request/PAGOS/PAGO";
  static final String mcteParam_PagForma = "FORMA";
  static final String mcteParam_PagImp = "IMPO";
  static final String mcteParam_PagCant = "CANT";
  static final String mcteNodos_Recibos = "//Request/RECIBOS/RECIBO";
  static final String mcteParam_RecRecib = "RECIB";
  static final String mcteParam_RecImp = "IMP";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    org.w3c.dom.NodeList wobjXMLListPag = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarClienSecAs = "";
    String wvarNivelAs = "";
    String wvarPagos = "";
    String wvarPagForma = "";
    String wvarPagImp = "";
    String wvarPagCant = "";
    String wvarRecibos = "";
    String wvarRecRecib = "";
    String wvarRecImp = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    int wvariCounter = 0;
    String wvarCont = "";
    //
    //
    //
    //Dim wvarSWCheDif        As String
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //Deber� venir desde la p�gina
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarNivelAs = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NivelAs ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSecAs = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSecAs ) */ ), 9 );
      //wvarSWCheDif = .selectSingleNode(mcteParam_SWCheDif).Text
      //
      // INICIO PAGOS
      wvarPagos = "";
      wobjXMLListPag = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Pagos ) */;

      for( wvariCounter = 0; wvariCounter <= (wobjXMLListPag.getLength() - 1); wvariCounter++ )
      {
        wvarPagos = wvarPagos + Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLListPag.item( wvariCounter ).selectSingleNode( mcteParam_PagForma ) */ ), 2 );
        wvarPagos = wvarPagos + Strings.right( Strings.fill( 14, "0" ) + Strings.replace( Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLListPag.item( wvariCounter ).selectSingleNode( mcteParam_PagImp ) */ ), ".", "" ), ",", "" ), 14 );
        wvarPagos = wvarPagos + Strings.right( Strings.fill( 3, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLListPag.item( wvariCounter ).selectSingleNode( mcteParam_PagCant ) */ ), 3 );
      }

      for( wvariCounter = wobjXMLListPag.getLength(); wvariCounter <= 5; wvariCounter++ )
      {
        wvarPagos = wvarPagos + Strings.fill( 19, "0" );
      }
      // FIN PAGOS
      // INICIO CERTIFICADOS
      wvarRecibos = "";
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Recibos ) */;

      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarRecibos = wvarRecibos + Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_RecRecib ) */ ), 9 );
        wvarRecibos = wvarRecibos + Strings.right( Strings.fill( 14, "0" ) + Strings.replace( Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_RecImp ) */ ), ".", "" ), ",", "" ), 14 );
      }

      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 199; wvariCounter++ )
      {
        wvarRecibos = wvarRecibos + Strings.fill( 23, "0" );
      }
      //para saber cuantos son
      wvarCont = Strings.right( Strings.fill( 3, "0" ) + wobjXMLList.getLength(), 3 );
      // FIN CERTIFICADOS
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los Parametros de la cola de MQ del archivo de configuraci�n
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + "  000000000  000000000  000000000" + wvarPagos + wvarCont + wvarRecibos;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 36 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup GoTo ClearObjects
      }
      //
      //DA - 29/09/2008: para debbugear
      //    Dim Fnum As Long
      //        Fnum = FreeFile
      //    Open App.Path & "\log.txt" For Append As #Fnum
      //    Print #Fnum, "<AreaIN>" & wvarArea & "</AreaIN><AreaOUT>" & strParseString & "</AreaOUT>"
      //    Close #Fnum
      //
      wvarStep = 200;
      wvarResult = "";
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos = 4784;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 210;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( !wvarEstado.equals( "OK" ) )
      {
        //
        if( Strings.mid( strParseString, 21, 2 ).equals( "01" ) )
        {
          wvarStep = 220;
          Response.set( "<Response><Estado resultado='false' mensaje='ESTA LIQUIDACION YA HA SIDO RENDIDA CON ANTERIORIDAD' /></Response>" );
        }
        else if( (Strings.mid( strParseString, 21, 2 ).equals( "02" )) || (Strings.mid( strParseString, 21, 2 ).equals( "03" )) )
        {
          wvarStep = 220;
          Response.set( "<Response><Estado resultado='false' mensaje='Su liquidaci�n no se ha podido generar, comun�quese con nuestra Mesa de Ayuda al 4323 - 4614' /></Response>" );
        }
        else
        {
          wvarStep = 220;
          Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        }
        //
      }
      else
      {
        //
        wvarStep = 230;
        wvarResult = "";
        //
        wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseString) } );
        //
        wvarStep = 320;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
        //
      }
      //
      wvarStep = 330;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarNivelAs + wvarClienSecAs + wvariCounter + wvarRecibos + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int wvarPos, String strParseString ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCantLin = 0;
    int wvarCounter = 0;
    //
    wvarResult = wvarResult + "<DATOS>";
    //
    wvarResult = wvarResult + "<NROLIQ>" + Strings.mid( strParseString, wvarPos, 9 ) + "</NROLIQ>";
    //DA - 26/09/2008: se agregan 4 codigos de barras mas de 32 char y se expande el antiguo de 29 a 32
    //wvarResult = wvarResult & "<BARCODE>" & Mid(strParseString, wvarPos + 9, 29) & "</BARCODE>"
    wvarResult = wvarResult + "<BARCODE>" + Strings.mid( strParseString, (wvarPos + 9), 32 ) + "</BARCODE>";
    wvarResult = wvarResult + "<BARCODE_EFEC>" + Strings.mid( strParseString, (wvarPos + 41), 32 ) + "</BARCODE_EFEC>";
    wvarResult = wvarResult + "<BARCODE_CHEQ>" + Strings.mid( strParseString, (wvarPos + 73), 32 ) + "</BARCODE_CHEQ>";
    wvarResult = wvarResult + "<BARCODE_CHEQ48>" + Strings.mid( strParseString, (wvarPos + 105), 32 ) + "</BARCODE_CHEQ48>";
    wvarResult = wvarResult + "<BARCODE_CHEQ48DIF>" + Strings.mid( strParseString, (wvarPos + 137), 32 ) + "</BARCODE_CHEQ48DIF>";

    //DA - 26/09/2008: se ajusta esto ya que hay que contemplar los nuevos codigos de barra
    //wvarResult = wvarResult & "<FECLIQ>" & Mid(strParseString, wvarPos + 38, 10) & "</FECLIQ>"
    //wvarResult = wvarResult & "<FECVTO>" & Mid(strParseString, wvarPos + 48, 10) & "</FECVTO>"
    //wvarResult = wvarResult & "<HORASVENC>" & Val(Mid(strParseString, wvarPos + 58, 3)) & "</HORASVENC>"
    wvarResult = wvarResult + "<FECLIQ>" + Strings.mid( strParseString, (wvarPos + 169), 10 ) + "</FECLIQ>";
    wvarResult = wvarResult + "<FECVTO>" + Strings.mid( strParseString, (wvarPos + 179), 10 ) + "</FECVTO>";
    wvarResult = wvarResult + "<HORASVENC>" + VB.val( Strings.mid( strParseString, (wvarPos + 189), 3 ) ) + "</HORASVENC>";
    wvarResult = wvarResult + "</DATOS>";
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
