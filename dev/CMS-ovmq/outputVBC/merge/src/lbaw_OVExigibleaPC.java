import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVExigibleaPC implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVExigibleaPC";
  static final String mcteOpID = "1419";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_NivelAs = "//NIVELAS";
  static final String mcteParam_ClienSecAs = "//CLIENSECAS";
  static final String mcteParam_Nivel1 = "//NIVEL1";
  static final String mcteParam_ClienSec1 = "//CLIENSEC1";
  static final String mcteParam_Nivel2 = "//NIVEL2";
  static final String mcteParam_ClienSec2 = "//CLIENSEC2";
  static final String mcteParam_Nivel3 = "//NIVEL3";
  static final String mcteParam_ClienSec3 = "//CLIENSEC3";
  static final String mcteParam_TIPO_DISPLAY = "//TIPO_DISPLAY";
  static final String mcteParam_Origen = "//ORIGEN";
  /**
   *  TIPOS DE DISPLAY
   */
  static final String mcteDownload_TXT = "1";
  static final String mcteDownload_CSV = "2";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private String strParseString = "";
  private diamondedge.util.XmlDom mobjXMLConfig = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarResultParcial = "";
    String wvarStringTitulo = "";
    String wvarUsuario = "";
    String wvarNivelAs = "";
    String wvarClienSecAs = "";
    String wvarNivel1 = "";
    String wvarClienSec1 = "";
    String wvarNivel2 = "";
    String wvarClienSec2 = "";
    String wvarNivel3 = "";
    String wvarClienSec3 = "";
    String wvarCiaAsCod = "";
    String wvarTipoDisplay = "";
    String wvarContinuar = "";
    String wvarOrigen = "";
    Variant wvarPos = new Variant();
    int wvarstrLen = 0;
    String wvarEstado = "";
    String wvarParametros = "";
    boolean wvarStatus = false;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarNivelAs = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NivelAs ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSecAs = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSecAs ) */ ), 9 );
      wvarNivel1 = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Nivel1 ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSec1 = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSec1 ) */ ), 9 );
      wvarNivel2 = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Nivel2 ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSec2 = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSec2 ) */ ), 9 );
      wvarNivel3 = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Nivel3 ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSec3 = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSec3 ) */ ), 9 );
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarTipoDisplay = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPO_DISPLAY ) */ );
      wvarOrigen = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Origen ) */ );
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      mobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup mobjXMLConfig.async = false;
      mobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los Parametros de la cola de MQ del archivo de configuración
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 80;
      wvarParametros = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + wvarNivel1 + wvarClienSec1 + wvarNivel2 + wvarClienSec2 + wvarNivel3 + wvarClienSec3 + wvarOrigen;
      //
      wvarStep = 140;
      wvarContinuar = Strings.space( 57 );
      wvarStatus = invoke( "CorrerMensaje", new Variant[] { new Variant(wvarParametros), new Variant(wvarContinuar) } );
      //
      if( wvarStatus == false )
      {
        wvarStep = 150;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
        IAction_Execute = 0;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [-1] - EL SERVICIO DE CONSULTA NO SE ENCUENTRA DISPONIBLE - Timeout Expired", vbLogEventTypeError );
        //
        //unsup GoTo ClearObjects
        //
      }

      //
      wvarStep = 160;
      wvarResult = "";
      //cantidad de caracteres ocupados por parámetros de entrada
      wvarPos.set( 125 );
      //
      wvarStep = 170;
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 180;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 190;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 200;
        wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseString), new Variant(wvarstrLen) } );
        //
        while( wvarEstado.equals( "TR" ) )
        {
          //
          wvarStep = 210;
          wvarContinuar = Strings.mid( strParseString, 68, 57 );
          //
          wvarStep = 220;
          wvarStatus = invoke( "CorrerMensaje", new Variant[] { new Variant(wvarParametros), new Variant(wvarContinuar) } );
          //
          if( wvarStatus == false )
          {
            //
            wvarStep = 230;
            Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
            IAction_Execute = 0;
            //
            mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [-1] - EL SERVICIO DE CONSULTA NO SE ENCUENTRA DISPONIBLE - Timeout Expired", vbLogEventTypeError );
            //
            //unsup GoTo ClearObjects
          }
          //
          //
          wvarStep = 240;
          //cantidad de caracteres ocupados por parámetros de entrada
          wvarPos.set( 125 );
          //
          wvarstrLen = Strings.len( strParseString );
          //
          wvarStep = 250;
          //Corto el estado
          wvarEstado = Strings.mid( strParseString, 19, 2 );
          //
          if( wvarEstado.equals( "ER" ) )
          {
            //
            wvarStep = 260;
            Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
            //
          }
          //
          wvarStep = 270;
          wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseString), new Variant(wvarstrLen) } );
          //
        }
        //
        wvarStep = 280;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 290;
        //unsup wobjXMLResponse.async = false;
        wobjXMLResponse.loadXML( "<RS>" + wvarResult + "</RS>" );
        //
        wvarStep = 300;
        if( null /*unsup wobjXMLResponse.selectNodes( "//R" ) */.getLength() != 0 )
        {
          //
          wvarStep = 310;
          //unsup wobjXSLResponse.async = false;
          if( wvarTipoDisplay.equals( mcteDownload_CSV ) )
          {
            //
            wvarStep = 320;
            wobjXSLResponse.loadXML( invoke( "p_GetXSL_CSV", new Variant[] {} ) );
            wvarStringTitulo = "CODIGO,NOMBRE,PROD,POLIZA,CERTIF,ESTADO,ENDOSO,RECIBO,MONEDA,IMPORTE,CANAL,FEC.VTO.,DIAS,MOTIVO" + System.getProperty("line.separator");
            //
          }
          else if( wvarTipoDisplay.equals( mcteDownload_TXT ) )
          {
            //
            wvarStep = 330;
            wobjXSLResponse.loadXML( invoke( "p_GetXSL_TXT", new Variant[] {} ) );
            wvarStringTitulo = "";
            //
          }
          //
          wvarStep = 340;

          wvarResultParcial = wvarResultParcial + Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
          wvarResultParcial = wvarStringTitulo + wvarResultParcial;
          //
          wvarStep = 350;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          wobjXSLResponse = (diamondedge.util.XmlDom) null;
          //
        }
        else
        {
          //
          wvarStep = 360;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          //
        }

        wvarStep = 370;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResultParcial + "</Response>" );
        //
      }
      //
      wvarStep = 380;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      mobjXMLConfig = (diamondedge.util.XmlDom) null;

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarParametros + wvarContinuar + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCantLin = 0;
    int wvarCounter = 0;
    //
    if( !Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ).equals( "" ) )
    {
      wvarCantLin = Obj.toInt( Strings.mid( strParseString, wvarPos.toInt(), 4 ) );
      wvarPos.set( wvarPos.add( new Variant( 4 ) ) );
      //
      for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )
      {
        wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 138 ) + "]]></R>";
        wvarPos.set( wvarPos.add( new Variant( 138 ) ) );
      }
      //
    }
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  private String p_GetXSL_CSV() throws Exception
  {
    String p_GetXSL_CSV = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";

    wvarStrXSL = wvarStrXSL + "<xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,1,6)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,7,30)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,37,4)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,41,8)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,49,14)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,63,3)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,67,6)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,73,9)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,82,3)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,85,1)' /> ";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='format-number((number(substring(.,86,14)) div 100), \"########0.00\", \"european\")' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,100,4)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,104,10)' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,114,1)' />";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='number(substring(.,115,4))' />,";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='normalize-space(substring(.,119,20))' />&#13;&#10;";
    wvarStrXSL = wvarStrXSL + "</xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL_CSV = wvarStrXSL;
    //
    return p_GetXSL_CSV;
  }

  public boolean CorrerMensaje( String pvarParametros, String pvarContinuar ) throws Exception
  {
    boolean CorrerMensaje = false;
    int wvarMQError = 0;
    Object wobjFrame2MQ = null;

    wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
    //error: function 'Execute' was not found.
    //unsup: wvarMQError = wobjFrame2MQ.Execute(pvarParametros & pvarContinuar, strParseString, mobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    wobjFrame2MQ = null;
    //
    if( wvarMQError == 0 )
    {
      CorrerMensaje = true;
    }
    else
    {
      /*unsup mobjCOM_Context.SetComplete() */;
      CorrerMensaje = false;
    }
    //
    return CorrerMensaje;
  }

  private String p_GetXSL_TXT() throws Exception
  {
    String p_GetXSL_TXT = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";

    wvarStrXSL = wvarStrXSL + "<xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,1,6)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,7,30)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,37,4)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,41,8)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,49,14)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,63,3)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,67,6)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,73,9)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,82,3)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,85,1)' />";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='format-number((number(substring(.,86,14)) div 100), \"########0.00\", \"european\")' />&#009;";
    wvarStrXSL = wvarStrXSL + "     <xsl:if test='string-length(format-number((number(substring(.,86,14)) div 100), \"########0.00\", \"european\")) &lt; 7'>";
    wvarStrXSL = wvarStrXSL + "&#009;";
    wvarStrXSL = wvarStrXSL + "     </xsl:if>";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,100,4)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,104,10)' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,114,1)' />";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='number(substring(.,115,4))' />&#009;";
    wvarStrXSL = wvarStrXSL + "<xsl:value-of select='substring(.,119,20)' />&#13;&#10;";
    wvarStrXSL = wvarStrXSL + "</xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL_TXT = wvarStrXSL;
    //
    return p_GetXSL_TXT;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
