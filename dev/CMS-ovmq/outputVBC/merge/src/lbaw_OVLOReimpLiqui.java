import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVLOReimpLiqui implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVLOReimpLiqui";
  static final String mcteOpID = "1421";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_NroLiq = "//NROLIQ";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarResultInt = "";
    String wvarResultPagos = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarNroLiq = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //Deberá venir desde la página
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarNroLiq = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NroLiq ) */ ), 9 );
      //
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los Parametros de la cola de MQ del archivo de configuración
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 120;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarNroLiq;
      wvarStep = 240;
      wvarResult = "";
      //cantidad de caracteres ocupados por parámetros de entrada
      wvarPos = 76;
      //
      wvarStep = 125;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 10 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 260;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 280;
        wvarResult = "";
        wvarResult = wvarResult + "<DATOS>";
        wvarResult = wvarResult + "<NROLIQ>" + Strings.mid( strParseString, (wvarPos - 9), 9 ) + "</NROLIQ>";
        wvarResult = wvarResult + "<CLIENSEC>" + Strings.mid( strParseString, wvarPos, 9 ) + "</CLIENSEC>";
        wvarResult = wvarResult + "<FECVTO>" + Strings.mid( strParseString, (wvarPos + 9), 10 ) + "</FECVTO>";
        //DA - 26/09/2008: se agregan 4 codigos de barras mas de 32 char y se expande el antiguo de 29 a 32
        //wvarResult = wvarResult & "<BARCODE>" & Mid(strParseString, wvarPos + 19, 29) & "</BARCODE>"
        wvarResult = wvarResult + "<BARCODE>" + Strings.mid( strParseString, (wvarPos + 19), 32 ) + "</BARCODE>";
        wvarResult = wvarResult + "<BARCODE_EFEC>" + Strings.mid( strParseString, (wvarPos + 51), 32 ) + "</BARCODE_EFEC>";
        wvarResult = wvarResult + "<BARCODE_CHEQ>" + Strings.mid( strParseString, (wvarPos + 83), 32 ) + "</BARCODE_CHEQ>";
        wvarResult = wvarResult + "<BARCODE_CHEQ48>" + Strings.mid( strParseString, (wvarPos + 115), 32 ) + "</BARCODE_CHEQ48>";
        wvarResult = wvarResult + "<BARCODE_CHEQ48DIF>" + Strings.mid( strParseString, (wvarPos + 147), 32 ) + "</BARCODE_CHEQ48DIF>";
        //DA - 26/09/2008: se ajusta esto ya que hay que contemplar los nuevos codigos de barra
        //wvarResult = wvarResult & "<COTIDOLAR>" & Format(CStr(CDbl(Mid(strParseString, wvarPos + 162, 15)) / 10000000), "0.0000000") & " </COTIDOLAR>"
        wvarResult = wvarResult + "<COTIDOLAR>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, (wvarPos + 293), 15 ) ) / 10000000) ), "0.0000000" ) + " </COTIDOLAR>";
        wvarResult = wvarResult + "</DATOS>";
        //
        //DA - 26/09/2008: se ajusta esto ya que hay que contemplar los nuevos codigos de barra
        //wvarResultPagos = ParseoMensajePagos(wvarPos + 48, strParseString, wvarstrLen)
        wvarResultPagos = invoke( "ParseoMensajePagos", new Variant[] { new Variant(new Variant( wvarPos + 179 ))/*warning: ByRef value change will be lost.*/, new Variant(strParseString), new Variant(wvarstrLen) } );
        //
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 280;
        //unsup wobjXMLResponse.async = false;
        wobjXMLResponse.loadXML( wvarResultPagos );
        //
        wvarStep = 290;
        if( null /*unsup wobjXMLResponse.selectNodes( "//RP" ) */.getLength() != 0 )
        {
          //unsup wobjXSLResponse.async = false;
          wobjXSLResponse.loadXML( invoke( "p_GetXSLPagos", new Variant[] {} ) );
          //
          wvarStep = 300;
          wvarResultPagos = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
          //
          wvarStep = 310;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          wobjXSLResponse = (diamondedge.util.XmlDom) null;
        }
        //
        //DA - 26/09/2008: se ajusta esto ya que hay que contemplar los nuevos codigos de barra
        //wvarResultInt = ParseoMensaje(wvarPos + 177, strParseString)
        wvarResultInt = invoke( "ParseoMensaje", new Variant[] { new Variant(new Variant( wvarPos + 308 ))/*warning: ByRef value change will be lost.*/, new Variant(strParseString) } );
        //
        wvarStep = 270;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 280;
        //unsup wobjXMLResponse.async = false;
        wobjXMLResponse.loadXML( wvarResultInt );
        //
        wvarStep = 290;
        if( null /*unsup wobjXMLResponse.selectNodes( "//R" ) */.getLength() != 0 )
        {
          //unsup wobjXSLResponse.async = false;
          wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
          //
          wvarStep = 300;
          wvarResultInt = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
          //
          wvarStep = 310;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          wobjXSLResponse = (diamondedge.util.XmlDom) null;
          //
        }
        //
        wvarStep = 320;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + wvarResultPagos + wvarResultInt + "</Response>" );
        //
      }
      //
      wvarStep = 360;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCantLin = 0;
    int wvarCounter = 0;
    //
    if( !Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 3 ) ).equals( "" ) )
    {
      wvarCantLin = Obj.toInt( Strings.mid( strParseString, wvarPos.toInt(), 3 ) );
      wvarPos.set( wvarPos.add( new Variant( 3 ) ) );
      //
      wvarResult = wvarResult + "<RS>";
      for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )
      {
        wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 106 ) + "]]></R>";
        wvarPos.set( wvarPos.add( new Variant( 106 ) ) );
      }
      //
      wvarResult = wvarResult + "</RS>";
      //
    }
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"eeuu\" decimal-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REGS'>";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REG'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CLIDES'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,1,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='PROD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,31,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,35,8)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERPOL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,43,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERANN'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,47,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERSEC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,51,6)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='OPERAPOL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='number(substring(.,57,6))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RECNUM'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,63,9)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='MON'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,72,3))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SIG'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,75,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='normalize-space(substring(.,75,1))' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMP'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,76,14)) div 100), \"###.###.##0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMPCALC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,76,14)) div 100), \"########0.00\", \"eeuu\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RAMO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,90,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='FECVTO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,101,10)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='AGE'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,111,2)' />-<xsl:value-of select='substring(.,113,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='MON'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='FECVTO'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='AGE'/>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    //
    return p_GetXSL;
  }

  private String ParseoMensajePagos( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensajePagos = "";
    String wvarResult = "";
    //
    wvarResult = wvarResult + "<RPS>";
    //
    while( (wvarPos.toInt() < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 2 ) ).equals( "00" )) )
    {
      wvarResult = wvarResult + "<RP><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 19 ) + "]]></RP>";
      wvarPos.set( wvarPos.add( new Variant( 19 ) ) );
    }
    //
    wvarResult = wvarResult + "</RPS>";
    //
    ParseoMensajePagos = wvarResult;
    //
    return ParseoMensajePagos;
  }

  private String p_GetXSLPagos() throws Exception
  {
    String p_GetXSLPagos = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"eeuu\" decimal-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='PAGOS'>";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RPS/RP'/>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RPS/RP'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='PAGO'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='FORMA'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='number(substring(.,1,2))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMPO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,3,14)) div 100), \"###.###.##0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CANT'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='number(substring(.,17,3))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLPagos = wvarStrXSL;
    //
    return p_GetXSLPagos;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
