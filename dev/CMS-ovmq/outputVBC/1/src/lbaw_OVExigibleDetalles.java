import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVExigibleDetalles implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * 
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVExigibleDetalles";
  static final String mcteOpID = "1401";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_NivelAs = "//NIVELAS";
  static final String mcteParam_ClienSecAs = "//CLIENSECAS";
  static final String mcteParam_Nivel1 = "//NIVEL1";
  static final String mcteParam_ClienSec1 = "//CLIENSEC1";
  static final String mcteParam_Nivel2 = "//NIVEL2";
  static final String mcteParam_ClienSec2 = "//CLIENSEC2";
  static final String mcteParam_Nivel3 = "//NIVEL3";
  static final String mcteParam_ClienSec3 = "//CLIENSEC3";
  static final String mcteParam_Producto = "//PRODUCTO";
  static final String mcteParam_Poliza = "//POLIZA";
  static final String mcteParam_NroConsu = "//NROCONS";
  static final String mcteParam_NroOrden = "//NROORDEN";
  static final String mcteParam_DirOrden = "//DIRORDEN";
  static final String mcteParam_TFiltro = "//TFILTRO";
  static final String mcteParam_VFiltro = "//VFILTRO";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarResultTR = "";
    String wvarNivelAs = "";
    String wvarClienSecAs = "";
    String wvarNivel1 = "";
    String wvarClienSec1 = "";
    String wvarNivel2 = "";
    String wvarClienSec2 = "";
    String wvarNivel3 = "";
    String wvarClienSec3 = "";
    String wvarCiaAsCod = "";
    String wvarProducto = "";
    String wvarPoliza = "";
    String wvarUsuario = "";
    String wvarNroConsu = "";
    String wvarNroOrden = "";
    String wvarDirOrden = "";
    String wvarTFiltro = "";
    String wvarVFiltro = "";
    Variant wvarPos = new Variant();
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarNivelAs = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NivelAs ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSecAs = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSecAs ) */ ), 9 );
      wvarNivel1 = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Nivel1 ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSec1 = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSec1 ) */ ), 9 );
      wvarNivel2 = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Nivel2 ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSec2 = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSec2 ) */ ), 9 );
      wvarNivel3 = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Nivel3 ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSec3 = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSec3 ) */ ), 9 );
      wvarProducto = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Producto ) */ ) + Strings.space( 4 ), 4 );
      wvarPoliza = Strings.right( Strings.fill( 22, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Poliza ) */ ), 22 );
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarNroConsu = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NroConsu ) */ ), 4 );
      wvarNroOrden = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NroOrden ) */ ), 2 );
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_DirOrden ) */.getLength() != 0 )
      {
        wvarDirOrden = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DirOrden ) */ ) + "A", 1 );
      }
      else
      {
        wvarDirOrden = "A";
      }
      wvarTFiltro = Strings.right( "0" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TFiltro ) */ ), 1 );
      wvarVFiltro = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VFiltro ) */ ) + Strings.space( 4 ), 4 );
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los Parametros de la cola de MQ del archivo de configuración
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 80;
      //
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + wvarNivel1 + wvarClienSec1 + wvarNivel2 + wvarClienSec2 + wvarNivel3 + wvarClienSec3 + "1" + wvarNroConsu + wvarNroOrden + wvarDirOrden + wvarProducto + wvarPoliza + wvarTFiltro + wvarVFiltro;
      wvarStep = 100;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 50 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }

      wvarStep = 190;
      wvarResult = "";
      wvarResultTR = "";
      //cantidad de caracteres ocupados por parámetros de entrada
      wvarPos.set( 106 );
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 200;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 210;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 230;
        wvarResultTR = wvarResultTR + "<MSGEST>" + wvarEstado + "</MSGEST>";
        wvarResultTR = wvarResultTR + "<PRODUCTO>" + Strings.mid( strParseString, 75, 4 ) + "</PRODUCTO>";
        wvarResultTR = wvarResultTR + "<POLIZA>" + Strings.mid( strParseString, 79, 22 ) + "</POLIZA>";
        wvarResultTR = wvarResultTR + "<NROCONS>" + Strings.mid( strParseString, 68, 4 ) + "</NROCONS>";
        wvarResultTR = wvarResultTR + "<NROORDEN>" + Strings.mid( strParseString, 72, 2 ) + "</NROORDEN>";
        wvarResultTR = wvarResultTR + "<DIRORDEN>" + Strings.mid( strParseString, 74, 1 ) + "</DIRORDEN>";
        wvarResultTR = wvarResultTR + "<TFILTRO>" + Strings.mid( strParseString, 101, 1 ) + "</TFILTRO>";
        wvarResultTR = wvarResultTR + "<VFILTRO>" + Strings.mid( strParseString, 102, 4 ) + "</VFILTRO>";
        //
        if( Obj.toInt( wvarNroConsu ) != 0 )
        {
          wvarResultTR = wvarResultTR + "<PAGINADO>1</PAGINADO>";
        }
        else
        {
          wvarResultTR = wvarResultTR + "<PAGINADO>0</PAGINADO>";
        }
        //
        wvarStep = 240;
        wvarResult = "";
        wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseString), new Variant(wvarstrLen) } );
        //
        wvarStep = 250;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 260;
        //unsup wobjXMLResponse.async = false;
        wobjXMLResponse.loadXML( wvarResult );
        //
        if( null /*unsup wobjXMLResponse.selectNodes( "//R" ) */.getLength() != 0 )
        {
          wvarStep = 270;
          //unsup wobjXSLResponse.async = false;
          wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
          //
          wvarStep = 280;
          wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
          //
          wvarStep = 290;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          wobjXSLResponse = (diamondedge.util.XmlDom) null;
          //
          wvarStep = 300;
          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResultTR + wvarResult + "</Response>" );
          //
        }
        else
        {
          //
          wvarStep = 310;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          //
        }
      }
      //
      wvarStep = 320;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + wvarNivel1 + wvarClienSec1 + wvarNivel2 + wvarClienSec2 + wvarNivel3 + wvarClienSec3 + "1" + wvarNroConsu + wvarNroOrden + wvarProducto + wvarPoliza + wvarTFiltro + wvarVFiltro + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCantLin = 0;
    int wvarCounter = 0;
    //
    wvarResult = wvarResult + "<RS>";
    //
    if( !Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ).equals( "" ) )
    {
      wvarCantLin = Obj.toInt( Strings.mid( strParseString, wvarPos.toInt(), 4 ) );
      wvarPos.set( wvarPos.add( new Variant( 4 ) ) );
      //
      for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )
      {
        wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 149 ) + "]]></R>";
        wvarPos.set( wvarPos.add( new Variant( 149 ) ) );
      }
      //
    }
    wvarResult = wvarResult + "</RS>";
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REGS'>";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R[substring(.,1,6) != \"      \"]'/>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='TOTS'>";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R[substring(.,1,6) = \"      \"]'/>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    // DETALLE
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R[substring(.,1,6) != \"      \"]'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REG'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='AGE'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,1,2)' />-<xsl:value-of select='substring(.,3,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SINI'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,7,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RAMO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,8,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CLIDES'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,9,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='PROD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,39,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,43,8)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERPOL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,51,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERANN'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,55,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERSEC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,59,6)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='MON'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,65,3))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //    wvarStrXSL = wvarStrXSL & "             <xsl:element name='IMPTOS'>"
    //    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,68,1) ='-'"">"
    //    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,68,1)' />"
    //    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    //    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMPTO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,68,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,68,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,69,14)) div 100), \"########0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_30S'>"
    //    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,83,1) ='-'"">"
    //    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,83,1)' />"
    //    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    //    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='I_30'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,83,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,83,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,84,14)) div 100), \"########0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_60S'>"
    //    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,98,1) ='-'"">"
    //    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,98,1)' />"
    //    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    //    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='I_60'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,98,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,98,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,99,14)) div 100), \"########0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_90S'>"
    //    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,113,1) ='-'"">"
    //    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,113,1)' />"
    //    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    //    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='I_90'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,113,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,113,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,114,14)) div 100), \"########0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //    wvarStrXSL = wvarStrXSL & "             <xsl:element name='I_M90S'>"
    //    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,128,1) ='-'"">"
    //    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,128,1)' />"
    //    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    //    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='I_M90'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,128,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,128,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,129,14)) div 100), \"########0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='EST'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,143,3))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COB'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,146,3))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    // TOTALES
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R[substring(.,1,6) = \"      \"]'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='TOT'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='TMON'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,65,3)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_IMPTOS'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,68,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,68,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_IMPTO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,69,14)) div 100), \"###.###.##0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_30S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,83,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,83,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_30'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,84,14)) div 100), \"###.###.##0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_60S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,98,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,98,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_60'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,99,14)) div 100), \"###.###.##0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_90S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,113,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,113,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_90'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,114,14)) div 100), \"###.###.##0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_M90S'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,128,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='substring(.,128,1)' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='T_M90'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,129,14)) div 100), \"###.###.##0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='MON'/>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    //
    return p_GetXSL;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
