import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVLOGetRecibos implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVLOGetRecibos";
  static final String mcteOpID = "1416";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_NivelAs = "//NIVELAS";
  static final String mcteParam_ClienSecAs = "//CLIENSECAS";
  static final String mcteNodos_Polizas = "//Request/POLIZAS/POLIZA";
  static final String mcteParam_PolProd = "PROD";
  static final String mcteParam_PolPol = "POL";
  static final String mcteParam_PolCerti = "CERTI";
  static final String mcteParam_FlagBusq = "//FLAGBUSQ";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarNivelAs = "";
    String wvarClienSecAs = "";
    String wvarPolizas = "";
    String wvarFlagBusq = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    String wvarCotiz = "";
    int wvariCounter = 0;
    String wvarCont = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //Deberá venir desde la página
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarNivelAs = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NivelAs ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSecAs = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSecAs ) */ ), 9 );
      //
      // INICIO POLIZAS
      wvarPolizas = "";
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Polizas ) */;

      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarPolizas = wvarPolizas + Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_PolProd ) */ ) + Strings.space( 4 ), 4 );
        wvarPolizas = wvarPolizas + Strings.right( Strings.fill( 8, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_PolPol ) */ ), 8 );
        wvarPolizas = wvarPolizas + Strings.right( Strings.fill( 14, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_PolCerti ) */ ), 14 );
        wvarPolizas = wvarPolizas + "  ";
      }

      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 39; wvariCounter++ )
      {
        wvarPolizas = wvarPolizas + Strings.fill( 4, " " ) + Strings.fill( 22, "0" ) + Strings.fill( 2, " " );
      }
      //para saber cuantos son
      wvarCont = Strings.right( Strings.fill( 3, "0" ) + wobjXMLList.getLength(), 3 );
      // FIN POLIZAS
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_FlagBusq ) */.getLength() != 0 )
      {
        wvarFlagBusq = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FlagBusq ) */ ) + "T", 1 );
      }
      else
      {
        wvarFlagBusq = "T";
      }
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los Parametros de la cola de MQ del archivo de configuración
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + "  000000000  000000000  000000000" + wvarFlagBusq + wvarCont + wvarPolizas;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 80 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup GoTo ClearObjects
      }
      //
      wvarStep = 200;
      wvarResult = "";
      //cantidad de caracteres ocupados por parámetros de entrada
      wvarPos = 1191;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 210;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 220;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 230;
        wvarResult = "";
        //
        wvarCotiz = "<COTIDOLAR>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, wvarPos, 15 ) ) / 10000000) ), "0.0000000" ) + " </COTIDOLAR>";
        wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(new Variant( wvarPos + 15 ))/*warning: ByRef value change will be lost.*/, new Variant(strParseString) } );
        //
        wvarStep = 270;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 280;
        //unsup wobjXMLResponse.async = false;
        wobjXMLResponse.loadXML( wvarResult );
        //
        wvarStep = 290;
        if( null /*unsup wobjXMLResponse.selectNodes( "//R" ) */.getLength() != 0 )
        {
          //unsup wobjXSLResponse.async = false;
          wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
          //
          wvarStep = 300;
          wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
          //
          wvarStep = 310;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          wobjXSLResponse = (diamondedge.util.XmlDom) null;
          //
          wvarStep = 320;
          Response.set( "<Response><Estado resultado='true' mensaje='' />" + "<MSGEST>" + wvarEstado + "</MSGEST>" + wvarCotiz + wvarResult + "</Response>" );
          //
        }
        else
        {
          //
          wvarStep = 350;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          //
        }
      }
      //
      wvarStep = 330;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarClienSecAs + wvarNivelAs + wvariCounter + wvarPolizas + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCantLin = 0;
    int wvarCounter = 0;
    //
    wvarResult = wvarResult + "<RS>";
    //
    if( !Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 3 ) ).equals( "" ) )
    {
      wvarCantLin = Obj.toInt( Strings.mid( strParseString, wvarPos.toInt(), 3 ) );
      wvarPos.set( wvarPos.add( new Variant( 3 ) ) );
      //
      for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )
      {
        wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 116 ) + "]]></R>";
        wvarPos.set( wvarPos.add( new Variant( 116 ) ) );
      }
      //
    }
    wvarResult = wvarResult + "</RS>";
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"eeuu\" decimal-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REGS'>";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REG'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RAMO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,1,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CLIDES'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,2,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CLIENSEC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='number(substring(.,32,9))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='PROD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,41,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,45,8)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERPOL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,53,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERANN'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,57,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERSEC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,61,6)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='OPERAPOL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='number(substring(.,67,6))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RECNUM'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,73,9)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='MON'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,82,3))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SIG'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"substring(.,85,1) ='-'\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:value-of select='normalize-space(substring(.,85,1))' />";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMP'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,86,14)) div 100), \"###.###.##0,00\", \"european\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='IMPCALC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='format-number((number(substring(.,86,14)) div 100), \"########0.00\", \"eeuu\")' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RENDIDO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,100,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='FECVTO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,101,10)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='AGE'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,111,2)' />-<xsl:value-of select='substring(.,113,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='ESTADO'>X</xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='MON'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='FECVTO'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='AGE'/>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    //
    return p_GetXSL;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
