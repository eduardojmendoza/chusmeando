import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVClientesConsulta implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVClientesConsulta";
  static final String mcteOpID = "1010";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_NivelAs = "//NIVELAS";
  static final String mcteParam_ClienSecAs = "//CLIENSECAS";
  static final String mcteParam_DocumTip = "//DOCUMTIP";
  static final String mcteParam_DocumNro = "//DOCUMNRO";
  static final String mcteParam_Cliendes = "//CLIENDES";
  static final String mcteParam_Producto = "//PRODUCTO";
  static final String mcteParam_Poliza = "//POLIZA";
  static final String mcteParam_Certi = "//CERTI";
  static final String mcteParam_Patente = "//PATENTE";
  static final String mcteParam_Motor = "//MOTOR";
  static final String mcteParam_EstPol = "//ESTPOL";
  /**
   * Parametros para la paginaci�n
   */
  static final String mcteParam_QryCont = "//QRYCONT";
  static final String mcteParam_CliCont = "//CLICONT";
  static final String mcteParam_ProduCont = "//PRODUCONT";
  static final String mcteParam_PolizaCont = "//POLIZACONT";
  /**
   * jc 09/2009
   */
  static final String mcteParam_Suplenums = "//SUPLENUMS";
  static final String mcteParam_Cliensecs = "//CLIENSECS";
  static final String mcteParam_Agentclas = "//AGENTCLAS";
  static final String mcteParam_Agentcods = "//AGENTCODS";
  static final String mcteParam_Nroitems = "//NROITEMS";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * fin jc
   */
  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarResultTR = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarNivelAs = "";
    String wvarClienSecAs = "";
    String wvarProducto = "";
    String wvarPoliza = "";
    String wvarCerti = "";
    String wvarDocumTip = "";
    String wvarDocumNro = "";
    String wvarCliendes = "";
    String wvarPatente = "";
    String wvarMotor = "";
    String wvarEstPol = "";
    Variant wvarPos = new Variant();
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    String wvarMensaje = "";
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    String wvarQryCont = "";
    String wvarCliCont = "";
    String wvarProduCont = "";
    String wvarPolizaCont = "";
    String wvarSuplenums = "";
    String wvarCliensecs = "";
    String wvarAgentclas = "";
    String wvarAgentcods = "";
    String wvarNroitems = "";
    //
    //
    //
    //
    //Para la paginaci�n
    //JC 09/2009
    //Fin JC
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarNivelAs = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NivelAs ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSecAs = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSecAs ) */ ), 9 );
      wvarProducto = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Producto ) */ ) + Strings.space( 4 ), 4 );
      wvarPoliza = Strings.right( Strings.fill( 8, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Poliza ) */ ), 8 );
      wvarCerti = Strings.right( Strings.fill( 14, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Certi ) */ ), 14 );
      wvarDocumTip = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DocumTip ) */ ), 2 );
      wvarDocumNro = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DocumNro ) */ ) + Strings.space( 11 ), 11 );
      wvarCliendes = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Cliendes ) */ ) + Strings.space( 20 ), 20 );
      wvarPatente = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Patente ) */ ) + Strings.space( 10 ), 10 );
      wvarMotor = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Motor ) */ ) + Strings.space( 20 ), 20 );
      wvarEstPol = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EstPol ) */ ) + Strings.space( 30 ), 30 );

      wvarQryCont = Strings.right( Strings.fill( 8, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_QryCont ) */ ), 8 );
      wvarCliCont = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CliCont ) */ ) + Strings.space( 30 ), 30 );
      wvarProduCont = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ProduCont ) */ ) + Strings.space( 4 ), 4 );
      wvarPolizaCont = Strings.right( Strings.fill( 22, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PolizaCont ) */ ), 22 );
      //jc 09/2009
      wvarSuplenums = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Suplenums ) */ ), 4 );
      wvarCliensecs = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Cliensecs ) */ ), 9 );
      wvarAgentclas = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Agentclas ) */ ) + Strings.space( 2 ), 2 );
      wvarAgentcods = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Agentcods ) */ ), 4 );
      wvarNroitems = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Nroitems ) */ ), 4 );
      //jc fin
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los Parametros de la cola de MQ del archivo de configuraci�n
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 80;
      //FJO - Se modifica porque no se informaba el nivel asumido correctamente - 2008-04-18
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + "  000000000" + "  000000000  000000000" + wvarDocumTip + wvarDocumNro + wvarCliendes + wvarProducto + wvarPoliza + wvarCerti + wvarPatente + wvarMotor + wvarEstPol;
      //jc 09/2009
      //wvarArea = wvarArea & wvarQryCont & wvarCliCont & wvarProduCont & wvarPolizaCont
      wvarArea = wvarArea + wvarQryCont + wvarCliCont + wvarProduCont + wvarPolizaCont + wvarSuplenums + wvarCliensecs + wvarAgentclas + wvarAgentcods + wvarNroitems;
      //fin jc
      wvarStep = 190;
      wvarResult = "";

      //jc 09/2009
      //wvarPos = 250 'cantidad de caracteres ocupados por par�metros de entrada
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos.set( 273 );
      //fin jc
      wvarStep = 100;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 225 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup GoTo ClearObjects
      }

      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 200;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 210;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        if( wvarEstado.equals( "TR" ) )
        {
          //
          wvarStep = 230;
          //Cargo las variables para llamar a un segundo mensaje de ser necesario
          wvarResultTR = wvarResultTR + "<QRYCONT>" + Strings.mid( strParseString, 186, 8 ) + "</QRYCONT>";
          wvarResultTR = wvarResultTR + "<CLICONT>" + Strings.mid( strParseString, 194, 30 ) + "</CLICONT>";
          wvarResultTR = wvarResultTR + "<PRODUCONT>" + Strings.mid( strParseString, 224, 4 ) + "</PRODUCONT>";
          wvarResultTR = wvarResultTR + "<POLIZACONT>" + Strings.mid( strParseString, 228, 22 ) + "</POLIZACONT>";
          //jc 09/2009 se deben agregar mas variables
          wvarResultTR = wvarResultTR + "<CLIENDES>" + Strings.mid( strParseString, 80, 20 ) + "</CLIENDES>";
          wvarResultTR = wvarResultTR + "<ESTPOL>" + Strings.mid( strParseString, 156, 30 ) + "</ESTPOL>";
          //jc 09/2009
          wvarResultTR = wvarResultTR + "<SUPLENUMS>" + Strings.mid( strParseString, 250, 4 ) + "</SUPLENUMS>";
          wvarResultTR = wvarResultTR + "<CLIENSECS>" + Strings.mid( strParseString, 254, 9 ) + "</CLIENSECS>";
          wvarResultTR = wvarResultTR + "<AGENTCLAS>" + Strings.mid( strParseString, 263, 2 ) + "</AGENTCLAS>";
          wvarResultTR = wvarResultTR + "<AGENTCODS>" + Strings.mid( strParseString, 265, 4 ) + "</AGENTCODS>";
          wvarResultTR = wvarResultTR + "<NROITEMS>" + Strings.mid( strParseString, 269, 4 ) + "</NROITEMS>";
          //fin jc
        }
        //
        wvarStep = 240;
        wvarResult = "";
        wvarResultTR = wvarResultTR + "<MSGEST>" + wvarEstado + "</MSGEST>";
        wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseString), new Variant(wvarstrLen) } );
        //
        wvarStep = 250;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 260;
        //unsup wobjXMLResponse.async = false;
        wobjXMLResponse.loadXML( wvarResult );
        //
        if( null /*unsup wobjXMLResponse.selectNodes( "//R" ) */.getLength() != 0 )
        {
          wvarStep = 270;
          //unsup wobjXSLResponse.async = false;
          wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
          //
          wvarStep = 280;
          wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
          //
          wvarStep = 290;
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
          wobjXSLResponse = (diamondedge.util.XmlDom) null;
          //
          wvarStep = 300;

          //If wvarEstado = "TR" And Mid(strParseString, 21, 2) = "52" Then
          //    wvarMensaje = "<MSGEST><![CDATA[La b�squeda realizada puede estar incompleta. Si usted no encontr� el cliente deseado, ingrese un dato mas espec�fico y realice la b�squeda nuevamente.]]></MSGEST>"
          //    Response = "<Response><Estado resultado='true' mensaje='' />" & wvarMensaje & wvarResult & "</Response>"
          //Else
          //    wvarMensaje = "<MSGEST></MSGEST>"
          //    Response = "<Response><Estado resultado='true' mensaje='' />" & wvarMensaje & wvarResult & "</Response>"
          //End If
          //
          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResultTR + wvarResult + "</Response>" );
        }
        else
        {
          //
          if( (wvarEstado.equals( "TR" )) && (Strings.mid( strParseString, 21, 2 ).equals( "52" )) )
          {
            Response.set( "<Response><Estado resultado='false' mensaje='La b�squeda realizada es muy extensa, por favor ingrese un dato mas espec�fico y real�cela nuevamente.' /></Response>" );
          }
          else
          {
            wvarStep = 310;
            Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
          }
          //
          wobjXMLResponse = (diamondedge.util.XmlDom) null;
        }
      }
      //
      wvarStep = 320;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS MQ
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarDocumTip + wvarDocumNro + wvarCliendes + wvarProducto + wvarPoliza + wvarCerti + wvarPatente + wvarMotor + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    //
    //& Chr(13)
    wvarResult = wvarResult + "<RS>";
    //
    while( (wvarPos.toInt() < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ).equals( "" )) )
    {
      System.out.println( String.valueOf( wvarPos.toDouble() ) + "|" + Strings.mid( strParseString, wvarPos.toInt(), 4 ) + "|" + Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ) + "|" + Strings.asc( Strings.mid( strParseString, wvarPos.toInt(), 1 ) ) + "-" + Strings.asc( Strings.mid( strParseString, wvarPos.add( new Variant( 1 ) ).toInt(), 1 ) ) + "-" + Strings.asc( Strings.mid( strParseString, wvarPos.add( new Variant( 2 ) ).toInt(), 1 ) ) + "-" + Strings.asc( Strings.mid( strParseString, wvarPos.add( new Variant( 3 ) ).toInt(), 1 ) ) );
      //'MHC: Soluci�n t�ctica - Fase II - Agregado de marca de Endosable
      //wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 139) & "]]></R>" '& Chr(13)
      //& Chr(13)
      wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 140 ) + "]]></R>";
      //wvarPos = wvarPos + 138
      //wvarPos = wvarPos + 139
      //MHC: Soluci�n t�ctica - Fase II - Agregado de marca de Endosable
      wvarPos.set( wvarPos.add( new Variant( 140 ) ) );
    }
    //
    wvarResult = wvarResult + "</RS>";
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/'> ";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REGS'>";
    wvarStrXSL = wvarStrXSL + "              <xsl:apply-templates select='/RS/R'/>";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='REG'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='PROD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,1,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='POL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,5,8)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERPOL'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,13,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERANN'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,17,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CERSEC'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,21,6)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CLIDES'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,40,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='TOMA'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,70,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='EST'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,100,30))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SINI'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,130,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='ALERTEXI'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,131,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='AGE'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,132,2)' />-<xsl:value-of select='substring(.,134,4)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='RAMO'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,138,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='MARCAREIMPRESION'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,139,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //
    //MHC: Soluci�n t�ctica - Fase II - Agregado de marca de Endosable
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='MARCAENDOSABLE'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,140,1)' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIDES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='TOMA'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='EST'/>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    //
    return p_GetXSL;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
