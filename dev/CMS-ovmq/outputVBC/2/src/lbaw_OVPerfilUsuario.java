import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVPerfilUsuario implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVPerfilUsuario";
  static final String mcteOpID = "1000";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarResultUnic = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //Deberá venir desde la página
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      //
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los Parametros de la cola de MQ del archivo de configuración
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarUsuario;
      //
      wvarStep = 100;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup GoTo ClearObjects
      }
      //
      wvarStep = 180;
      wvarResult = "";
      //cantidad de caracteres ocupados por parámetros de entrada
      wvarPos = 77;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 190;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //
      if( wvarEstado.equals( "ER" ) )
      {
        //
        wvarStep = 200;
        Response.set( "<Response><Estado resultado='false' mensaje='El usuario no se encuentra habilitado en el sistema.' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 230;
        wvarResultUnic = "<USER_NIVEL>" + Strings.trim( Strings.mid( strParseString, wvarPos, 1 ) ) + "</USER_NIVEL>";
        wvarResultUnic = wvarResultUnic + "<USER_NOM><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 1), 60 ) ) + "]]></USER_NOM>";
        //
        wvarResult = "";
        wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(new Variant( wvarPos + 61 ))/*warning: ByRef value change will be lost.*/, new Variant(strParseString), new Variant(wvarstrLen) } );
        //
        if( !Strings.trim( wvarResult ).equals( "" ) )
        {
          wvarStep = 290;
          Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResultUnic + wvarResult + "</Response>" );
        }
        else
        {
          Response.set( "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>" );
        }
        //
      }
      //
      wvarStep = 300;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarCount = 0;
    int wvarCantEmisiones = 0;
    //
    wvarResult = wvarResult + "<ACCESO>";
    wvarCount = 1;
    //
    while( (wvarPos.toInt() < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 6 ) ).equals( "" )) && (wvarCount <= 200) )
    {
      wvarResult = wvarResult + "<MMENU><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 6 ) + "]]></MMENU>";
      wvarPos.set( wvarPos.add( new Variant( 66 ) ) );
      wvarCount = wvarCount + 1;
    }
    //
    wvarResult = wvarResult + "</ACCESO>";

    //Cargo los Ramos que puede emitir Online
    wvarPos.set( 13338 );
    if( Strings.len( strParseString ) > wvarPos.add( new Variant( 3 ) ).toInt() )
    {
      //El area incluye valores para emisiones Onlines permitidas
      if( new Variant( Strings.mid( strParseString, wvarPos.toInt(), 3 ) ).isNumeric() )
      {
        wvarResult = wvarResult + "<EMISIONESONLINE>";
        wvarCantEmisiones = Obj.toInt( Strings.mid( strParseString, wvarPos.toInt(), 3 ) );
        wvarPos.set( wvarPos.add( new Variant( 3 ) ) );
        for( wvarCount = 1; wvarCount <= wvarCantEmisiones; wvarCount++ )
        {
          if( wvarPos.toInt() > wvarstrLen )
          {
            break;
          }
          if( Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 4 ) ).equals( "" ) )
          {
            break;
          }
          wvarResult = wvarResult + "<EMISION>" + Strings.mid( strParseString, wvarPos.toInt(), 4 ) + "</EMISION>";
          wvarPos.set( wvarPos.add( new Variant( 4 ) ) );
        }
        wvarResult = wvarResult + "</EMISIONESONLINE>";
      }
    }
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
