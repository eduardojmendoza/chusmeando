import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVSiniDetalle implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQ.lbaw_OVSiniDetalle";
  static final String mcteOpID = "1304";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_NivelAs = "//NIVELAS";
  static final String mcteParam_ClienSecAs = "//CLIENSECAS";
  static final String mcteParam_Nivel1 = "//NIVEL1";
  static final String mcteParam_ClienSec1 = "//CLIENSEC1";
  static final String mcteParam_Nivel2 = "//NIVEL2";
  static final String mcteParam_ClienSec2 = "//CLIENSEC2";
  static final String mcteParam_Nivel3 = "//NIVEL3";
  static final String mcteParam_ClienSec3 = "//CLIENSEC3";
  static final String mcteParam_Producto = "//PRODUCTO";
  static final String mcteParam_SiniAn = "//SINIAN";
  static final String mcteParam_SiniNum = "//SININUM";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarNivelAs = "";
    String wvarClienSecAs = "";
    String wvarNivel1 = "";
    String wvarClienSec1 = "";
    String wvarNivel2 = "";
    String wvarClienSec2 = "";
    String wvarNivel3 = "";
    String wvarClienSec3 = "";
    String wvarProducto = "";
    String wvarPoliza = "";
    String wvarSiniAn = "";
    String wvarSiniNum = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarEstado = "";
    String wvarError = "";
    String wvarMensaje = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los par�metros que llegan desde la p�gina
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //Deber� venir desde la p�gina
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarNivelAs = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NivelAs ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSecAs = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSecAs ) */ ), 9 );
      wvarNivel1 = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Nivel1 ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSec1 = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSec1 ) */ ), 9 );
      wvarNivel2 = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Nivel2 ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSec2 = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSec2 ) */ ), 9 );
      wvarNivel3 = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Nivel3 ) */ ) + Strings.space( 2 ), 2 );
      wvarClienSec3 = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienSec3 ) */ ), 9 );

      wvarProducto = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Producto ) */ ) + Strings.space( 4 ), 4 );
      wvarSiniAn = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SiniAn ) */ ), 2 );
      wvarSiniNum = Strings.right( Strings.fill( 6, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SiniNum ) */ ), 6 );
      //
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los Parametros de la cola de MQ del archivo de configuraci�n
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + Strings.space( 4 ) + wvarClienSecAs + wvarNivelAs + wvarNivel1 + wvarClienSec1 + wvarNivel2 + wvarClienSec2 + wvarNivel3 + wvarClienSec3 + wvarProducto + wvarSiniAn + wvarSiniNum;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 36 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup GoTo ClearObjects
      }
      //
      wvarStep = 180;
      wvarResult = "";
      //cantidad de caracteres ocupados por par�metros de entrada
      wvarPos = 79;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 190;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //Corto el error
      wvarError = Strings.mid( strParseString, 21, 2 );
      //
      if( wvarEstado.equals( "OK" ) )
      {
        //
        wvarStep = 200;
        wvarResult = wvarResult + "<DATOS>";
        wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseString), new Variant(wvarstrLen) } );
        //
        wvarStep = 210;
        wvarResult = wvarResult + "</DATOS>";
        //
        wvarStep = 220;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
      }
      else
      {
        //
        wvarStep = 230;
        
        if( wvarError.equals( "32" ) )
        {
          wvarMensaje = "No es correcta la clave de b�squeda.";
        }
        else if( wvarError.equals( "33" ) )
        {
          wvarMensaje = "El siniestro no pertenece a el productor.";
        }
        else
        {
          wvarMensaje = "Se ha producido un error en la ejecuci�n de la consulta.";
        }
        Response.set( "<Response><Estado resultado='false' mensaje='" + wvarMensaje + "' /></Response>" );
        //
      }
      //
      wvarStep = 240;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarProducto + wvarSiniAn + wvarSiniNum + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";

    //Es un solo dato.
    wvarResult = wvarResult + "<CLIDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos, 70 ) ) + "]]></CLIDES>";
    wvarResult = wvarResult + "<RAMO>" + Strings.mid( strParseString, (wvarPos + 70), 1 ) + "</RAMO>";
    wvarResult = wvarResult + "<PROD>" + Strings.mid( strParseString, (wvarPos - 12), 4 ) + "</PROD>";
    wvarResult = wvarResult + "<POL>" + Strings.mid( strParseString, (wvarPos + 71), 8 ) + "</POL>";
    wvarResult = wvarResult + "<CERPOL>" + Strings.mid( strParseString, (wvarPos + 79), 4 ) + "</CERPOL>";
    wvarResult = wvarResult + "<CERANN>" + Strings.mid( strParseString, (wvarPos + 83), 4 ) + "</CERANN>";
    wvarResult = wvarResult + "<CERSEC>" + Strings.mid( strParseString, (wvarPos + 87), 6 ) + "</CERSEC>";
    wvarResult = wvarResult + "<EST>" + Strings.mid( strParseString, (wvarPos + 93), 1 ) + "</EST>";
    wvarResult = wvarResult + "<FECSINI><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 94), 10 ) ) + "]]></FECSINI>";
    wvarResult = wvarResult + "<DATOSADI>" + Strings.mid( strParseString, (wvarPos + 104), 1 ) + "</DATOSADI>";
    wvarResult = wvarResult + "<D_DOCU>" + Strings.mid( strParseString, (wvarPos + 105), 11 ) + "</D_DOCU>";
    wvarResult = wvarResult + "<D_CLIAPE><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 116), 40 ) ) + "]]></D_CLIAPE>";
    wvarResult = wvarResult + "<D_CLINOM><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 156), 30 ) ) + "]]></D_CLINOM>";
    wvarResult = wvarResult + "<D_CAUSA><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 186), 60 ) ) + "]]></D_CAUSA>";
    wvarResult = wvarResult + "<D_CALLE><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 246), 40 ) ) + "]]></D_CALLE>";
    wvarResult = wvarResult + "<D_NRO><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 286), 30 ) ) + "]]></D_NRO>";
    wvarResult = wvarResult + "<D_LOCAL><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 316), 30 ) ) + "]]></D_LOCAL>";
    wvarResult = wvarResult + "<D_CP><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 346), 30 ) ) + "]]></D_CP>";
    wvarResult = wvarResult + "<D_PROV><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 376), 20 ) ) + "]]></D_PROV>";
    wvarResult = wvarResult + "<D_PAIS><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 396), 40 ) ) + "]]></D_PAIS>";
    wvarResult = wvarResult + "<D_COMI><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 436), 30 ) ) + "]]></D_COMI>";
    wvarResult = wvarResult + "<D_HORA><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 466), 10 ) ) + "]]></D_HORA>";
    //
    ParseoMensaje = wvarResult;

    return ParseoMensaje;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
