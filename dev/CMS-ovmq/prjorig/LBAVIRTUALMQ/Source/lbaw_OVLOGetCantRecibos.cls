VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVLOGetCantRecibos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQ.lbaw_OVLOGetCantRecibos"
Const mcteOpID              As String = "1415"

'Parametros XML de Entrada
Const mcteParam_Usuario    As String = "//USUARIO"
Const mcteParam_NivelAs    As String = "//NIVELAS"
Const mcteParam_ClienSecAs As String = "//CLIENSECAS"
Const mcteParam_FlagBusq   As String = "//FLAGBUSQ"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarNivelAs         As String
    Dim wvarClienSecAs      As String
    Dim wvarFlagBusq        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    Dim wvarCantLin         As Integer
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarNivelAs = Left(.selectSingleNode(mcteParam_NivelAs).Text & Space(2), 2)
        wvarClienSecAs = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSecAs).Text, 9)
        If .selectNodes(mcteParam_FlagBusq).length <> 0 Then
            wvarFlagBusq = Left(.selectSingleNode(mcteParam_FlagBusq).Text & "T", 1)
        Else
            wvarFlagBusq = "T"
        End If
        '
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los Parametros de la cola de MQ del archivo de configuración
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.Load (App.Path & "\" & gcteParamFileName)
    wvarCiaAsCod = wobjXMLParametros.selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & Space(4) & wvarClienSecAs & wvarNivelAs & "  000000000  000000000  000000000" & wvarFlagBusq
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 36
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        IAction_Execute = 1
        mobjCOM_Context.SetAbort
        GoTo ClearObjects:
    End If
    '
    wvarStep = 200
    wvarResult = ""
    wvarPos = 68  'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 210
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 220
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 230
        wvarCantLin = Mid(strParseString, wvarPos + 15, 4)
        wvarResult = ""
        '
        If wvarCantLin > 100 Then
            '
            wvarStep = 240
            wvarEstado = "<MSGEST>ING</MSGEST>" 'pantalla de ingreso de datos
            wvarEstado = wvarEstado & "<COTIDOLAR>" & Format(CStr(CDbl(Mid(strParseString, wvarPos, 15)) / 10000000), "0.0000000") & " </COTIDOLAR>"
            'wvarEstado = wvarEstado & CargarVectorMasivos(strParseString, wvarstrLen, wvarPos + 23219)
            wvarEstado = wvarEstado & CargarVectorMasivos(strParseString, wvarstrLen, wvarPos + 11619)
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarEstado & "</Response>"
            '
        ElseIf wvarCantLin = 0 Then
            '
            wvarStep = 250
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos' /><MSGEST>VAC</MSGEST></Response>"
            '
        Else
            '
            wvarStep = 260
            wvarEstado = "<MSGEST>LIS</MSGEST>" 'listar polizas
            wvarEstado = wvarEstado & "<COTIDOLAR>" & Format(CStr(CDbl(Mid(strParseString, wvarPos, 15)) / 10000000), "0.0000000") & " </COTIDOLAR>"
            wvarResult = wvarResult & ParseoMensaje(wvarPos + 19, strParseString, wvarCantLin)
            'wvarEstado = wvarEstado & CargarVectorMasivos(strParseString, wvarstrLen, wvarPos + 23219)
            wvarEstado = wvarEstado & CargarVectorMasivos(strParseString, wvarstrLen, wvarPos + 11619)
            '
            wvarStep = 270
            Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
            Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
            '
            wvarStep = 280
            wobjXMLResponse.async = False
            wobjXMLResponse.loadXML (wvarResult)
            '
            wvarStep = 290
            If wobjXMLResponse.selectNodes("//R").length <> 0 Then
                wobjXSLResponse.async = False
                Call wobjXSLResponse.loadXML(p_GetXSL())
                '
                wvarStep = 300
                wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
                '
                wvarStep = 310
                Set wobjXMLResponse = Nothing
                Set wobjXSLResponse = Nothing
                '
                wvarStep = 320
                Response = "<Response><Estado resultado='true' mensaje='' />" & wvarEstado & wvarResult & "</Response>"
                '
            End If
        End If
    End If
    '
    wvarStep = 330
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing

Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarNivelAs & wvarClienSecAs & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarCantLin As Integer) As String
Dim wvarResult As String
Dim wvarCounter As Long
    '
    wvarResult = wvarResult & "<RS>"
    '
        For wvarCounter = 0 To wvarCantLin - 1
            wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 116) & "]]></R>"
            wvarPos = wvarPos + 116
        Next
    '
    wvarResult = wvarResult & "</RS>"
    '
    ParseoMensaje = wvarResult
    '
End Function
Private Function CargarVectorMasivos(strParseString As String, wvarstrLen As Long, wvarPos As Long) As String
Dim wvarResult As String
Dim wvarCantLin As Integer
Dim wvarCounter As Integer
    '
    wvarCantLin = Mid(strParseString, wvarPos, 4)
    wvarResult = wvarResult & "<PRODS>"
    '
    For wvarCounter = 0 To wvarCantLin - 1
        If Mid(strParseString, wvarPos + 8, 1) = "1" Then
            wvarResult = wvarResult & "<PROD>"
            wvarResult = wvarResult & "<![CDATA[" & Mid(strParseString, wvarPos + 4, 4) & "]]>"
            wvarResult = wvarResult & "</PROD>"
        End If
        wvarPos = wvarPos + 5
    Next
    '
    wvarResult = wvarResult & "</PRODS>"
    '
    CargarVectorMasivos = wvarResult
    '
End Function
Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""eeuu"" decimal-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/'> "
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REGS'>"
    wvarStrXSL = wvarStrXSL & "              <xsl:apply-templates select='/RS/R'/>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RS/R'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REG'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CLIDES'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,1,30))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CLIENSEC'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='number(substring(.,31,9))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='PROD'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,40,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='POL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,44,8)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERPOL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,52,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERANN'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,56,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERSEC'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,60,6)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='OPERAPOL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='number(substring(.,66,6))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='RECNUM'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,72,9)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='MON'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,81,3))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='SIG'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,84,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='normalize-space(substring(.,84,1))' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='IMP'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,85,14)) div 100), ""###.###.##0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='IMPCALC'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,85,14)) div 100), ""########0.00"", ""eeuu"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='RENDIDO'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,99,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='RAMO'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,100,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='FECVTO'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,101,10)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='AGE'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,111,2)' />-<xsl:value-of select='substring(.,113,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='ESTADO'>X</xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIDES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='MON'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='FECVTO'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AGE'/>"
    '
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
    '
End Function




