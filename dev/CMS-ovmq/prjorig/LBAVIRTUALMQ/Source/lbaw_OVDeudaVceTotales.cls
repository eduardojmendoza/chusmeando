VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVDeudaVceTotales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQ.lbaw_OVDeudaVceTotales"
Const mcteOpID              As String = "1403"

'Parametros XML de Entrada
Const mcteParam_Usuario     As String = "//USUARIO"
Const mcteParam_NivelAs      As String = "//NIVELAS"
Const mcteParam_ClienSecAs   As String = "//CLIENSECAS"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarClienSecAs      As String
    Dim wvarNivelAs         As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarClienSecAs = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSecAs).Text, 9)
        wvarNivelAs = Left(.selectSingleNode(mcteParam_NivelAs).Text & Space(2), 2)
        '
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los Parametros de la cola de MQ del archivo de configuración
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.Load (App.Path & "\" & gcteParamFileName)
    wvarCiaAsCod = wobjXMLParametros.selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    '
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & Space(4) & wvarClienSecAs & wvarNivelAs & "  000000000  000000000  000000000"
    '
    wvarStep = 100
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 50
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        IAction_Execute = 1
        mobjCOM_Context.SetAbort
        GoTo ClearObjects:
    End If
    
    wvarStep = 180
    wvarResult = ""
    wvarPos = 67  'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 190
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    If wvarEstado = "ER" Then
        '
        wvarStep = 200
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 210
        wvarResult = ""
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        '
        wvarStep = 220
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 230
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML (wvarResult)
        '
        If wobjXMLResponse.selectNodes("//R").length <> 0 Then
            wvarStep = 240
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSL())
            '
            wvarStep = 250
            wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
            '
            wvarStep = 260
            wobjXMLResponse.async = False
            wobjXMLResponse.loadXML (wvarResult)
            '
            wvarStep = 270
            wobjXSLResponse.async = False
            Call wobjXSLResponse.Load(App.Path & "\" & "DeudaVceTotales.xsl")
            '
            wvarStep = 280
            wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
            '
            wvarStep = 290
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
            '
            wvarStep = 300
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
            '
        Else
            wvarStep = 310
            Set wobjXMLResponse = Nothing
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>"
        End If
    End If
    '
    wvarStep = 320
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarClienSecAs & wvarNivelAs & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarstrLen As Long) As String
Dim wvarResult As String
Dim wvarCantLin As Long
Dim wvarCounter As Long
    '
    wvarResult = wvarResult & "<RS>"
    '
    If Trim(Mid(strParseString, wvarPos, 4)) <> "" Then
       '
        wvarCantLin = Mid(strParseString, wvarPos, 4)
        wvarPos = wvarPos + 4
        '
        For wvarCounter = 0 To wvarCantLin - 1
            wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 130) & "]]></R>"
            wvarPos = wvarPos + 130
        Next
        '
    End If
    wvarResult = wvarResult & "</RS>"
    '
    ParseoMensaje = wvarResult
    '
End Function
Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/'> "
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REGS'>"
    wvarStrXSL = wvarStrXSL & "              <xsl:apply-templates select='/RS/R'/>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RS/R'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REG'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='NI1'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,1,2))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CL1'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,3,9)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='NI2'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,12,2))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CL2'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,14,9)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='NI3'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,23,2))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CL3'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,25,9)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='NOM'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,34,60))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='ESTSTR'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,94,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='MON1'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,95,3))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='SIG1'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,98,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,98,1)' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='TOTIMP1'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number(number(substring(.,99,14) div 100), ""###.###.##0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='MON2'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,113,3))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='SIG2'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,116,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='substring(.,116,1)' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='TOTIMP2'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number(number(substring(.,117,14) div 100), ""###.###.##0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NOMBRE'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='MON1'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='MON2'/>"
        
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
    '
End Function

