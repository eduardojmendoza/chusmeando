VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVDetRecibosExigible"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQ.lbaw_OVDetRecibosExigible"
Const mcteOpID              As String = "1408"

'Parametros XML de Entrada
Const mcteParam_Usuario    As String = "//USUARIO"
Const mcteParam_Poliza     As String = "//POLIZA"
Const mcteParam_Producto   As String = "//PRODUCTO"
Const mcteParam_Suplenum   As String = "//SUPLENUM"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarQueueManager    As String
    Dim wvarPutQueue        As String
    Dim wvarGetQueue        As String
    Dim wvarGMOWaitInterval As Long
    '
    Dim wobjQueueManager    As MQAX200.MQQueueManager
    Dim wobjPutQueue        As MQAX200.MQQueue
    Dim wobjGetQueue        As MQAX200.MQQueue
    Dim wobjMessage         As MQAX200.MQMessage
    Dim wobjPMO             As MQAX200.MQPutMessageOptions
    Dim wobjGMO             As MQAX200.MQGetMessageOptions
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarProducto        As String
    Dim wvarPoliza          As String
    Dim wvarSuplenum        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarStrLen          As Long
    Dim wvarEstado          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarProducto = Right(Space(4) & .selectSingleNode(mcteParam_Producto).Text, 4)
        wvarPoliza = Right(String(22, "0") & .selectSingleNode(mcteParam_Poliza).Text, 22)
        If .selectNodes(mcteParam_Suplenum).length <> 0 Then
            wvarSuplenum = Right(String(4, "0") & .selectSingleNode(mcteParam_Suplenum).Text, 4)
        Else
            wvarSuplenum = String(4, "0")
        End If
        '
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    With wobjXMLConfig
        .async = False
        Call .Load(App.Path & "\" & gcteConfFileName)
        wvarStep = 40
        wvarQueueManager = .selectSingleNode(gcteQueueManager).Text
        wvarPutQueue = .selectSingleNode(gctePutQueue).Text
        wvarGetQueue = .selectSingleNode(gcteGetQueue).Text
        wvarGMOWaitInterval = CLng(.selectSingleNode(gcteGMOWaitInterval).Text)
    End With
    '
    wvarStep = 50
    Set wobjXMLConfig = Nothing
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    'Genera y conecta el manager MQ
    Set wobjQueueManager = New MQAX200.MQQueueManager
    wobjQueueManager.Name = wvarQueueManager
    '
    wobjQueueManager.Connect
    '
    wvarStep = 90
    If Not wobjQueueManager.IsConnected Then
        Err.Raise -1, mcteClassName & "." & wcteFnName, "No se pudo realizar la conexion a " & wvarQueueManager
    Else
        Set wobjPutQueue = New MQAX200.MQQueue
        Set wobjGetQueue = New MQAX200.MQQueue
        '
        wvarStep = 100
        'Si se pudo crear el manager, crea las colas de put y get
        On Error Resume Next
        With wobjPutQueue
            Set .ConnectionReference = wobjQueueManager
            .Name = wvarPutQueue
            .OpenOptions = MQOO_OUTPUT + MQOO_FAIL_IF_QUIESCING
            .Open
        End With
        '
        wvarStep = 110
        With wobjGetQueue
            Set .ConnectionReference = wobjQueueManager
            .Name = wvarGetQueue
            .OpenOptions = MQOO_INPUT_SHARED + MQOO_FAIL_IF_QUIESCING
            .Open
        End With
        '
        wvarStep = 120
        On Error GoTo ErrorHandler
        If wobjPutQueue.ReasonCode <> MQCC_OK Then
            Err.Raise -1, mcteClassName & "." & wcteFnName, "Error al abrir la cola " & wvarPutQueue & ". Motivo: " & wobjPutQueue.ReasonName
        End If
        '
        If wobjGetQueue.ReasonCode <> MQCC_OK Then
            Err.Raise -1, mcteClassName & "." & wcteFnName, "Error al abrir la cola " & wvarGetQueue & ". Motivo: " & wobjGetQueue.ReasonName
        End If
        '
        wvarStep = 130
        Set wobjMessage = New MQAX200.MQMessage
        '
        'Genero el mensaje con sus propiedades y los parámetros de entrada
        With wobjMessage
            .Format = "MQSTR"
            .MessageData = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarProducto & wvarPoliza & wvarSuplenum
            .Expiry = 200
            .Report = MQRO_EXPIRATION_WITH_DATA + MQRO_COPY_MSG_ID_TO_CORREL_ID
            .ReplyToQueueManagerName = wvarQueueManager
            .ReplyToQueueName = wvarGetQueue
        End With
        '
        Set wobjPMO = New MQAX200.MQPutMessageOptions
        wobjPMO.Options = MQPMO_NEW_MSG_ID
        '
        wvarStep = 140
        'Coloca el mensaje en la cola
        wobjPutQueue.Put wobjMessage, wobjPMO
        '
        Set wobjGMO = New MQAX200.MQGetMessageOptions
        wobjGMO.MatchOptions = MQMO_MATCH_MSG_ID
        wobjGMO.Options = MQGMO_WAIT
        wobjGMO.WaitInterval = wvarGMOWaitInterval * 2
        '
        wvarStep = 150
        On Error Resume Next
        wobjGetQueue.Get wobjMessage, wobjGMO
        '
        If wobjGetQueue.CompletionCode = MQCC_FAILED Then
            wobjGMO.WaitInterval = wvarGMOWaitInterval * 4
            wobjGetQueue.Get wobjMessage, wobjGMO
        End If
        '
        wvarStep = 160
        If wobjGetQueue.CompletionCode = MQCC_FAILED Then
            ' NO HAY SERVICIO DE MQ
            Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & wvarResult & "</Response>"
            wvarStep = 170
            mobjCOM_Context.SetComplete
            IAction_Execute = 0
            
            mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [-1] - EL SERVICIO DE CONSULTA NO SE ENCUENTRA DISPONIBLE - Timeout Expired", _
                     vbLogEventTypeError
                     
            GoTo ClearObjects
        End If
        '
        On Error GoTo ErrorHandler
        '
        wvarStep = 180
        wvarResult = ""
        wvarPos = 97  'cantidad de caracteres ocupados por parámetros de entrada
        '
        strParseString = wobjMessage.MessageData
        wvarStrLen = Len(strParseString)
        '
        wvarStep = 190
        wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
        If wvarEstado = "OK" Then
            '
            wvarStep = 200
            wvarResult = wvarResult & "<DATOS_POLIZA>"
            wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarStrLen)
            '
            wvarStep = 210
            wvarResult = wvarResult & "</DATOS_POLIZA>"
            '
            wvarStep = 220
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
        Else
            wvarStep = 230
            Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        End If
    End If
    '
    wvarStep = 240
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS MQ
    Set wobjPMO = Nothing
    Set wobjGMO = Nothing
    Set wobjMessage = Nothing
        
    If Not wobjGetQueue Is Nothing Then
        If wobjGetQueue.IsOpen Then wobjGetQueue.Close
    End If
    
    Set wobjGetQueue = Nothing
    
    If Not wobjPutQueue Is Nothing Then
        If wobjPutQueue.IsOpen Then wobjPutQueue.Close
    End If
    
    Set wobjPutQueue = Nothing
    
    If Not wobjQueueManager Is Nothing Then
        If wobjQueueManager.IsConnected Then wobjQueueManager.Disconnect
    End If
    
    Set wobjQueueManager = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarProducto & wvarPoliza & wvarSuplenum & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarStrLen As Long) As String
Dim wvarResult As String
        
    'Es un solo dato.
    wvarResult = wvarResult & "<ALERT_OP>" & Mid(strParseString, wvarPos, 1) & "</ALERT_OP>"
    wvarResult = wvarResult & "<CLIENSEC>" & Mid(strParseString, wvarPos + 1, 9) & "</CLIENSEC>"
    wvarResult = wvarResult & "<CLIENDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 10, 30)) & "]]></CLIENDES>"
    wvarResult = wvarResult & "<ESTADO><![CDATA[" & Trim(Mid(strParseString, wvarPos + 46, 30)) & "]]></ESTADO>"
    wvarResult = wvarResult & "<ACREEDOR>" & Mid(strParseString, wvarPos + 76, 2) & "</ACREEDOR>"
    wvarResult = wvarResult & "<FEC_INI><![CDATA[" & Mid(strParseString, wvarPos + 78, 10) & "]]></FEC_INI>"
    wvarResult = wvarResult & "<FEC_RENOV><![CDATA[" & Mid(strParseString, wvarPos + 88, 23) & "]]></FEC_RENOV>"
    wvarResult = wvarResult & "<CANAL_COB><![CDATA[" & Trim(Mid(strParseString, wvarPos + 115, 20)) & "]]></CANAL_COB>"
    wvarResult = wvarResult & "<TIPO_CUE><![CDATA[" & Mid(strParseString, wvarPos + 137, 20) & "]]></TIPO_CUE>"
    wvarResult = wvarResult & "<NRO_CUE>" & Mid(strParseString, wvarPos + 157, 20) & "</NRO_CUE>"
    wvarResult = wvarResult & "<CAMPA_COD>" & Mid(strParseString, wvarPos + 177, 4) & "</CAMPA_COD>"
    wvarResult = wvarResult & "<CAMPA_DES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 181, 60)) & "]]></CAMPA_DES>"
    wvarResult = wvarResult & "<AFINIDAD><![CDATA[" & Trim(Mid(strParseString, wvarPos + 241, 60)) & "]]></AFINIDAD>"

    ParseoMensaje = wvarResult

End Function

















