VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVAnulxPagoDetalles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQ.lbaw_OVAnulxPagoDetalles"
Const mcteOpID              As String = "1410"

'Parametros XML de Entrada
Const mcteParam_Usuario    As String = "//USUARIO"
Const mcteParam_NivelAs    As String = "//NIVELAS"
Const mcteParam_ClienSecAs As String = "//CLIENSECAS"
Const mcteParam_Nivel1     As String = "//NIVEL1"
Const mcteParam_ClienSec1  As String = "//CLIENSEC1"
Const mcteParam_Nivel2     As String = "//NIVEL2"
Const mcteParam_ClienSec2  As String = "//CLIENSEC2"
Const mcteParam_Nivel3     As String = "//NIVEL3"
Const mcteParam_ClienSec3  As String = "//CLIENSEC3"
Const mcteParam_Continuar  As String = "//CONTINUAR"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarResultTR        As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarNivelAs         As String
    Dim wvarClienSecAs      As String
    Dim wvarNivel1          As String
    Dim wvarClienSec1       As String
    Dim wvarNivel2          As String
    Dim wvarClienSec2       As String
    Dim wvarNivel3          As String
    Dim wvarClienSec3       As String
    Dim wvarContinuar       As String
    Dim wvarEstado          As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstadoAreaIN    As String
    
    Dim wvarMQError                 As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los par�metros que llegan desde la p�gina
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deber� venir desde la p�gina
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarNivelAs = Left(.selectSingleNode(mcteParam_NivelAs).Text & Space(2), 2)
        wvarClienSecAs = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSecAs).Text, 9)
        wvarNivel1 = Left(.selectSingleNode(mcteParam_Nivel1).Text & Space(2), 2)
        wvarClienSec1 = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSec1).Text, 9)
        wvarNivel2 = Left(.selectSingleNode(mcteParam_Nivel2).Text & Space(2), 2)
        wvarClienSec2 = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSec2).Text, 9)
        wvarNivel3 = Left(.selectSingleNode(mcteParam_Nivel3).Text & Space(2), 2)
        wvarClienSec3 = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSec3).Text, 9)
        wvarContinuar = .selectSingleNode(mcteParam_Continuar).Text
        '
    End With
    '
    wvarStep = 15
    If Trim(wvarContinuar) = "" Then
        wvarContinuar = "00000000000000    0000000000000000000000"
        wvarEstadoAreaIN = Space(4)
    Else
        wvarEstadoAreaIN = "TR" & Space(2)
    End If
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los Parametros de la cola de MQ del archivo de configuraci�n
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.Load (App.Path & "\" & gcteParamFileName)
    wvarCiaAsCod = wobjXMLParametros.selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    '
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & wvarEstadoAreaIN & wvarClienSecAs & wvarNivelAs & wvarNivel1 & wvarClienSec1 & wvarNivel2 & wvarClienSec2 & wvarNivel3 & wvarClienSec3 & wvarContinuar
    wvarResult = ""
    wvarPos = 107  'cantidad de caracteres ocupados por par�metros de entrada
    '
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 36
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        IAction_Execute = 1
        mobjCOM_Context.SetAbort
        GoTo ClearObjects:
    End If
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 210
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 220
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        If wvarEstado = "TR" Then
            '
            wvarStep = 230
            'Cargo las variables para llamar a un segundo mensaje de ser necesario
            wvarResultTR = wvarResultTR & "<CONTINUAR>" & Mid(strParseString, 67, 40) & "</CONTINUAR>"
            '
        End If
        '
        wvarStep = 240
        wvarResult = ""
        wvarResultTR = wvarResultTR & "<MSGEST>" & wvarEstado & "</MSGEST>"
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        '
        wvarStep = 250
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 260
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML (wvarResult)
        '
        wvarStep = 270
        If wobjXMLResponse.selectNodes("//R").length <> 0 Then
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSL())
            '
            wvarStep = 280
            wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
            '
            wvarStep = 290
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
            '
            wvarStep = 300
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResultTR & wvarResult & "</Response>"
            '
        Else
            '
            wvarStep = 310
            Set wobjXMLResponse = Nothing
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>"
            '
        End If
    End If
    '
    wvarStep = 320
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing

Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarNivelAs & wvarClienSecAs & wvarNivel1 & wvarClienSec1 & wvarNivel2 & wvarClienSec2 & wvarNivel3 & wvarClienSec3 & wvarContinuar & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarstrLen As Long) As String

    Dim wvarResult As String
    Const wcteCantLines As Integer = 200 '2008-09-01 - FJO
    Dim wvarLineNbr As Integer '2008-09-01 - FJO
    '
    wvarLineNbr = 0 '2008-09-01 - FJO
    wvarResult = wvarResult & "<RS>"
    '
    '2008-09-01 - FJO
    ' Se modifica la l�gica para incluir la cantidad m�xima de registros
    ' que devuelve el componente de acuerdo a la definici�n en la LINKAGE.
    'While wvarPos < wvarstrLen And Trim(Mid(strParseString, wvarPos + 1, 4)) <> "" '2008-09-01 - FJO
    While wvarPos < wvarstrLen And Trim(Mid(strParseString, wvarPos + 1, 4)) <> "" And wvarLineNbr < wcteCantLines '2008-09-01 - FJO
        wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 92) & "]]></R>"
        wvarPos = wvarPos + 92
        wvarLineNbr = wvarLineNbr + 1 '2008-09-01 - FJO
    Wend
    '
    wvarResult = wvarResult & "</RS>"
    '
    ParseoMensaje = wvarResult
    '
End Function
Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/'> "
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REGS'>"
    wvarStrXSL = wvarStrXSL & "              <xsl:apply-templates select='/RS/R'/>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RS/R'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='REG'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='RAMO'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,1,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='PROD'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,2,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='POL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,6,8)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERPOL'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,14,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERANN'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,18,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CERSEC'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,22,6)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='FECANU'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,28,10)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='MON'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,38,3))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='SIG'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""substring(.,41,1) ='-'"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:value-of select='normalize-space(substring(.,41,1))' />"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='IMPANU'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='format-number((number(substring(.,42,14)) div 100), ""###.###.##0,00"", ""european"")' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CLIDES'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,56,30))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='COD'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,86,2)' />-<xsl:value-of select='substring(.,88,4)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='SINI'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='substring(.,92,1)' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='FECENV'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='MON'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='SIG'/>"
        
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
    '
End Function


