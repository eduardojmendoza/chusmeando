VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVSiniDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQ.lbaw_OVSiniDetalle"
Const mcteOpID              As String = "1304"

'Parametros XML de Entrada
Const mcteParam_Usuario    As String = "//USUARIO"
Const mcteParam_NivelAs      As String = "//NIVELAS"
Const mcteParam_ClienSecAs   As String = "//CLIENSECAS"
Const mcteParam_Nivel1       As String = "//NIVEL1"
Const mcteParam_ClienSec1    As String = "//CLIENSEC1"
Const mcteParam_Nivel2       As String = "//NIVEL2"
Const mcteParam_ClienSec2    As String = "//CLIENSEC2"
Const mcteParam_Nivel3       As String = "//NIVEL3"
Const mcteParam_ClienSec3    As String = "//CLIENSEC3"
Const mcteParam_Producto   As String = "//PRODUCTO"
Const mcteParam_SiniAn     As String = "//SINIAN"
Const mcteParam_SiniNum    As String = "//SININUM"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarNivelAs         As String
    Dim wvarClienSecAs      As String
    Dim wvarNivel1          As String
    Dim wvarClienSec1       As String
    Dim wvarNivel2          As String
    Dim wvarClienSec2       As String
    Dim wvarNivel3          As String
    Dim wvarClienSec3       As String
    
    Dim wvarProducto        As String
    Dim wvarPoliza          As String
    Dim wvarSiniAn          As String
    Dim wvarSiniNum         As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    Dim wvarError           As String
    Dim wvarMensaje         As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los par�metros que llegan desde la p�gina
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deber� venir desde la p�gina
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarNivelAs = Left(.selectSingleNode(mcteParam_NivelAs).Text & Space(2), 2)
        wvarClienSecAs = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSecAs).Text, 9)
        wvarNivel1 = Left(.selectSingleNode(mcteParam_Nivel1).Text & Space(2), 2)
        wvarClienSec1 = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSec1).Text, 9)
        wvarNivel2 = Left(.selectSingleNode(mcteParam_Nivel2).Text & Space(2), 2)
        wvarClienSec2 = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSec2).Text, 9)
        wvarNivel3 = Left(.selectSingleNode(mcteParam_Nivel3).Text & Space(2), 2)
        wvarClienSec3 = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSec3).Text, 9)
        
        wvarProducto = Left(.selectSingleNode(mcteParam_Producto).Text & Space(4), 4)
        wvarSiniAn = Right(String(2, "0") & .selectSingleNode(mcteParam_SiniAn).Text, 2)
        wvarSiniNum = Right(String(6, "0") & .selectSingleNode(mcteParam_SiniNum).Text, 6)
        '
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los Parametros de la cola de MQ del archivo de configuraci�n
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.Load (App.Path & "\" & gcteParamFileName)
    wvarCiaAsCod = wobjXMLParametros.selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & Space(4) & wvarClienSecAs & wvarNivelAs & wvarNivel1 & wvarClienSec1 & wvarNivel2 & wvarClienSec2 & wvarNivel3 & wvarClienSec3 & wvarProducto & wvarSiniAn & wvarSiniNum
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 36
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        IAction_Execute = 1
        mobjCOM_Context.SetAbort
        GoTo ClearObjects:
    End If
    '
    wvarStep = 180
    wvarResult = ""
    wvarPos = 79  'cantidad de caracteres ocupados por par�metros de entrada
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 190
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    wvarError = Mid(strParseString, 21, 2) 'Corto el error
    '
    If wvarEstado = "OK" Then
        '
        wvarStep = 200
        wvarResult = wvarResult & "<DATOS>"
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        '
        wvarStep = 210
        wvarResult = wvarResult & "</DATOS>"
        '
        wvarStep = 220
        Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
    Else
        '
        wvarStep = 230
        Select Case wvarError
            Case "32"
                wvarMensaje = "No es correcta la clave de b�squeda."
            Case "33"
                wvarMensaje = "El siniestro no pertenece a el productor."
            Case Else
                wvarMensaje = "Se ha producido un error en la ejecuci�n de la consulta."
        End Select
        Response = "<Response><Estado resultado='false' mensaje='" & wvarMensaje & "' /></Response>"
        '
    End If
    '
    wvarStep = 240
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing

Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarProducto & wvarSiniAn & wvarSiniNum & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarstrLen As Long) As String
Dim wvarResult As String
        
    'Es un solo dato.
    wvarResult = wvarResult & "<CLIDES><![CDATA[" & Trim(Mid(strParseString, wvarPos, 70)) & "]]></CLIDES>"
    wvarResult = wvarResult & "<RAMO>" & Mid(strParseString, wvarPos + 70, 1) & "</RAMO>"
    wvarResult = wvarResult & "<PROD>" & Mid(strParseString, wvarPos - 12, 4) & "</PROD>"
    wvarResult = wvarResult & "<POL>" & Mid(strParseString, wvarPos + 71, 8) & "</POL>"
    wvarResult = wvarResult & "<CERPOL>" & Mid(strParseString, wvarPos + 79, 4) & "</CERPOL>"
    wvarResult = wvarResult & "<CERANN>" & Mid(strParseString, wvarPos + 83, 4) & "</CERANN>"
    wvarResult = wvarResult & "<CERSEC>" & Mid(strParseString, wvarPos + 87, 6) & "</CERSEC>"
    wvarResult = wvarResult & "<EST>" & Mid(strParseString, wvarPos + 93, 1) & "</EST>"
    wvarResult = wvarResult & "<FECSINI><![CDATA[" & Trim(Mid(strParseString, wvarPos + 94, 10)) & "]]></FECSINI>"
    wvarResult = wvarResult & "<DATOSADI>" & Mid(strParseString, wvarPos + 104, 1) & "</DATOSADI>"
    wvarResult = wvarResult & "<D_DOCU>" & Mid(strParseString, wvarPos + 105, 11) & "</D_DOCU>"
    wvarResult = wvarResult & "<D_CLIAPE><![CDATA[" & Trim(Mid(strParseString, wvarPos + 116, 40)) & "]]></D_CLIAPE>"
    wvarResult = wvarResult & "<D_CLINOM><![CDATA[" & Trim(Mid(strParseString, wvarPos + 156, 30)) & "]]></D_CLINOM>"
    wvarResult = wvarResult & "<D_CAUSA><![CDATA[" & Trim(Mid(strParseString, wvarPos + 186, 60)) & "]]></D_CAUSA>"
    wvarResult = wvarResult & "<D_CALLE><![CDATA[" & Trim(Mid(strParseString, wvarPos + 246, 40)) & "]]></D_CALLE>"
    wvarResult = wvarResult & "<D_NRO><![CDATA[" & Trim(Mid(strParseString, wvarPos + 286, 30)) & "]]></D_NRO>"
    wvarResult = wvarResult & "<D_LOCAL><![CDATA[" & Trim(Mid(strParseString, wvarPos + 316, 30)) & "]]></D_LOCAL>"
    wvarResult = wvarResult & "<D_CP><![CDATA[" & Trim(Mid(strParseString, wvarPos + 346, 30)) & "]]></D_CP>"
    wvarResult = wvarResult & "<D_PROV><![CDATA[" & Trim(Mid(strParseString, wvarPos + 376, 20)) & "]]></D_PROV>"
    wvarResult = wvarResult & "<D_PAIS><![CDATA[" & Trim(Mid(strParseString, wvarPos + 396, 40)) & "]]></D_PAIS>"
    wvarResult = wvarResult & "<D_COMI><![CDATA[" & Trim(Mid(strParseString, wvarPos + 436, 30)) & "]]></D_COMI>"
    wvarResult = wvarResult & "<D_HORA><![CDATA[" & Trim(Mid(strParseString, wvarPos + 466, 10)) & "]]></D_HORA>"
    '
    ParseoMensaje = wvarResult

End Function



















