VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVExigibleaPC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQ.lbaw_OVExigibleaPC"
Const mcteOpID              As String = "1419"

'Parametros XML de Entrada
Const mcteParam_Usuario      As String = "//USUARIO"
Const mcteParam_NivelAs      As String = "//NIVELAS"
Const mcteParam_ClienSecAs   As String = "//CLIENSECAS"
Const mcteParam_Nivel1       As String = "//NIVEL1"
Const mcteParam_ClienSec1    As String = "//CLIENSEC1"
Const mcteParam_Nivel2       As String = "//NIVEL2"
Const mcteParam_ClienSec2    As String = "//CLIENSEC2"
Const mcteParam_Nivel3       As String = "//NIVEL3"
Const mcteParam_ClienSec3    As String = "//CLIENSEC3"
Const mcteParam_TIPO_DISPLAY As String = "//TIPO_DISPLAY"
Const mcteParam_Origen       As String = "//ORIGEN"

' TIPOS DE DISPLAY
Const mcteDownload_TXT          As String = "1"
Const mcteDownload_CSV          As String = "2"

Private strParseString      As String
Private mobjXMLConfig       As MSXML2.DOMDocument

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarResultParcial   As String
    Dim wvarStringTitulo    As String
    Dim wvarUsuario         As String
    Dim wvarNivelAs         As String
    Dim wvarClienSecAs      As String
    Dim wvarNivel1          As String
    Dim wvarClienSec1       As String
    Dim wvarNivel2          As String
    Dim wvarClienSec2       As String
    Dim wvarNivel3          As String
    Dim wvarClienSec3       As String
    Dim wvarCiaAsCod        As String
    Dim wvarTipoDisplay     As String
    Dim wvarContinuar       As String
    Dim wvarOrigen          As String
    '
    Dim wvarPos             As Long
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    Dim wvarParametros      As String
    Dim wvarStatus          As Boolean
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarNivelAs = Left(.selectSingleNode(mcteParam_NivelAs).Text & Space(2), 2)
        wvarClienSecAs = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSecAs).Text, 9)
        wvarNivel1 = Left(.selectSingleNode(mcteParam_Nivel1).Text & Space(2), 2)
        wvarClienSec1 = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSec1).Text, 9)
        wvarNivel2 = Left(.selectSingleNode(mcteParam_Nivel2).Text & Space(2), 2)
        wvarClienSec2 = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSec2).Text, 9)
        wvarNivel3 = Left(.selectSingleNode(mcteParam_Nivel3).Text & Space(2), 2)
        wvarClienSec3 = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSec3).Text, 9)
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarTipoDisplay = .selectSingleNode(mcteParam_TIPO_DISPLAY).Text
        wvarOrigen = .selectSingleNode(mcteParam_Origen).Text
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set mobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    mobjXMLConfig.async = False
    mobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los Parametros de la cola de MQ del archivo de configuración
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.Load (App.Path & "\" & gcteParamFileName)
    wvarCiaAsCod = wobjXMLParametros.selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarParametros = mcteOpID & wvarCiaAsCod & wvarUsuario & Space(4) & wvarClienSecAs & wvarNivelAs & wvarNivel1 & wvarClienSec1 & wvarNivel2 & wvarClienSec2 & wvarNivel3 & wvarClienSec3 & wvarOrigen
    '
    wvarStep = 140
    wvarContinuar = Space(57)
    wvarStatus = CorrerMensaje(wvarParametros, wvarContinuar)
    '
    If wvarStatus = False Then
        wvarStep = 150
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & wvarResult & "</Response>"
        IAction_Execute = 0
        '
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                 mcteClassName, _
                 wcteFnName, _
                 wvarStep, _
                 Err.Number, _
                 "Error= [-1] - EL SERVICIO DE CONSULTA NO SE ENCUENTRA DISPONIBLE - Timeout Expired", _
                 vbLogEventTypeError
        '
        GoTo ClearObjects
        '
    End If
    
    On Error GoTo ErrorHandler
    '
    wvarStep = 160
    wvarResult = ""
    wvarPos = 125  'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarStep = 170
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 180
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado = "ER" Then
        '
        wvarStep = 190
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 200
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        '
        While wvarEstado = "TR"
            '
            wvarStep = 210
            wvarContinuar = Mid(strParseString, 68, 57)
            '
            wvarStep = 220
            wvarStatus = CorrerMensaje(wvarParametros, wvarContinuar)
            '
            If wvarStatus = False Then
                '
                wvarStep = 230
                Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & wvarResult & "</Response>"
                IAction_Execute = 0
                '
                mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [-1] - EL SERVICIO DE CONSULTA NO SE ENCUENTRA DISPONIBLE - Timeout Expired", _
                         vbLogEventTypeError
                '
                GoTo ClearObjects
            End If
            '
            On Error GoTo ErrorHandler
            '
            wvarStep = 240
            wvarPos = 125  'cantidad de caracteres ocupados por parámetros de entrada
            '
            wvarstrLen = Len(strParseString)
            '
            wvarStep = 250
            wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
            '
            If wvarEstado = "ER" Then
                '
                wvarStep = 260
                Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
                '
            End If
            '
            wvarStep = 270
            wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        '
        Wend
        '
        wvarStep = 280
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 290
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML ("<RS>" & wvarResult & "</RS>")
        '
        wvarStep = 300
        If wobjXMLResponse.selectNodes("//R").length <> 0 Then
            '
            wvarStep = 310
            wobjXSLResponse.async = False
            If wvarTipoDisplay = mcteDownload_CSV Then
                '
                wvarStep = 320
                Call wobjXSLResponse.loadXML(p_GetXSL_CSV())
                wvarStringTitulo = "CODIGO,NOMBRE,PROD,POLIZA,CERTIF,ESTADO,ENDOSO,RECIBO,MONEDA,IMPORTE,CANAL,FEC.VTO.,DIAS,MOTIVO" & vbCrLf
                '
            ElseIf wvarTipoDisplay = mcteDownload_TXT Then
                '
                wvarStep = 330
                Call wobjXSLResponse.loadXML(p_GetXSL_TXT())
                wvarStringTitulo = ""
                '
            End If
            '
            wvarStep = 340
            
            wvarResultParcial = wvarResultParcial & Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
            wvarResultParcial = wvarStringTitulo & wvarResultParcial
            '
            wvarStep = 350
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
            '
        Else
            '
            wvarStep = 360
            Set wobjXMLResponse = Nothing
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>"
            '
        End If
            
        wvarStep = 370
        Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResultParcial & "</Response>"
        '
    End If
    '
    wvarStep = 380
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set mobjXMLConfig = Nothing

Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & wvarParametros & wvarContinuar & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarstrLen As Long) As String
Dim wvarResult As String
Dim wvarCantLin As Long
Dim wvarCounter As Long
    '
    If Trim(Mid(strParseString, wvarPos, 4)) <> "" Then
        wvarCantLin = Mid(strParseString, wvarPos, 4)
        wvarPos = wvarPos + 4
        '
        For wvarCounter = 0 To wvarCantLin - 1
            wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 138) & "]]></R>"
            wvarPos = wvarPos + 138
        Next
    '
    End If
    '
    ParseoMensaje = wvarResult
    '
End Function
Private Function p_GetXSL_CSV() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/'> "
    wvarStrXSL = wvarStrXSL & "              <xsl:apply-templates select='/RS/R'/>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"

    wvarStrXSL = wvarStrXSL & "<xsl:template match='/RS/R'>"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,1,6)' />,"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,7,30)' />,"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,37,4)' />,"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,41,8)' />,"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,49,14)' />,"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,63,3)' />,"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,67,6)' />,"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,73,9)' />,"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,82,3)' />,"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,85,1)' /> "
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='format-number((number(substring(.,86,14)) div 100), ""########0.00"", ""european"")' />,"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,100,4)' />,"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,104,10)' />,"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,114,1)' />"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='number(substring(.,115,4))' />,"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='normalize-space(substring(.,119,20))' />&#13;&#10;"
    wvarStrXSL = wvarStrXSL & "</xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL_CSV = wvarStrXSL
    '
End Function

Function CorrerMensaje(pvarParametros As String, pvarContinuar As String) As Boolean
Dim wvarMQError         As Long
Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(pvarParametros & pvarContinuar, strParseString, mobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    If wvarMQError = 0 Then
        CorrerMensaje = True
    Else
        mobjCOM_Context.SetComplete
        CorrerMensaje = False
    End If
    '
End Function

Private Function p_GetXSL_TXT() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/'> "
    wvarStrXSL = wvarStrXSL & "              <xsl:apply-templates select='/RS/R'/>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"

    wvarStrXSL = wvarStrXSL & "<xsl:template match='/RS/R'>"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,1,6)' />&#009;"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,7,30)' />&#009;"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,37,4)' />&#009;"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,41,8)' />&#009;"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,49,14)' />&#009;"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,63,3)' />&#009;"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,67,6)' />&#009;"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,73,9)' />&#009;"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,82,3)' />&#009;"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,85,1)' />"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='format-number((number(substring(.,86,14)) div 100), ""########0.00"", ""european"")' />&#009;"
    wvarStrXSL = wvarStrXSL & "     <xsl:if test='string-length(format-number((number(substring(.,86,14)) div 100), ""########0.00"", ""european"")) &lt; 7'>"
    wvarStrXSL = wvarStrXSL & "&#009;"
    wvarStrXSL = wvarStrXSL & "     </xsl:if>"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,100,4)' />&#009;"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,104,10)' />&#009;"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,114,1)' />"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='number(substring(.,115,4))' />&#009;"
    wvarStrXSL = wvarStrXSL & "<xsl:value-of select='substring(.,119,20)' />&#13;&#10;"
    wvarStrXSL = wvarStrXSL & "</xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL_TXT = wvarStrXSL
    '
End Function






