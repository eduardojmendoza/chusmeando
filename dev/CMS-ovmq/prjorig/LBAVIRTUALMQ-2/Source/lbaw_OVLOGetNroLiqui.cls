VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVLOGetNroLiqui"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQ.lbaw_OVLOGetNroLiqui"
Const mcteOpID              As String = "1417"

'Parametros XML de Entrada
Const mcteParam_Usuario    As String = "//USUARIO"
Const mcteParam_NivelAs    As String = "//NIVELAS"
Const mcteParam_ClienSecAs As String = "//CLIENSECAS"
'Const mcteParam_SWCheDif   As String = "//CHEQUEDIF"
Const mcteNodos_Pagos      As String = "//Request/PAGOS/PAGO"
Const mcteParam_PagForma   As String = "FORMA"
Const mcteParam_PagImp     As String = "IMPO"
Const mcteParam_PagCant    As String = "CANT"
Const mcteNodos_Recibos    As String = "//Request/RECIBOS/RECIBO"
Const mcteParam_RecRecib   As String = "RECIB"
Const mcteParam_RecImp     As String = "IMP"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjXMLListPag      As MSXML2.IXMLDOMNodeList
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarClienSecAs      As String
    Dim wvarNivelAs         As String
    Dim wvarPagos           As String
    Dim wvarPagForma        As String
    Dim wvarPagImp          As String
    Dim wvarPagCant         As String
    Dim wvarRecibos         As String
    Dim wvarRecRecib        As String
    Dim wvarRecImp          As String
    'Dim wvarSWCheDif        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarEstado          As String
    Dim wvariCounter        As Integer
    Dim wvarCont            As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los par�metros que llegan desde la p�gina
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deber� venir desde la p�gina
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarNivelAs = Left(.selectSingleNode(mcteParam_NivelAs).Text & Space(2), 2)
        wvarClienSecAs = Right(String(9, "0") & .selectSingleNode(mcteParam_ClienSecAs).Text, 9)
        'wvarSWCheDif = .selectSingleNode(mcteParam_SWCheDif).Text
        '
        ' INICIO PAGOS
        wvarPagos = ""
        Set wobjXMLListPag = .selectNodes(mcteNodos_Pagos)

        For wvariCounter = 0 To wobjXMLListPag.length - 1
            wvarPagos = wvarPagos & Right(String(2, "0") & wobjXMLListPag.Item(wvariCounter).selectSingleNode(mcteParam_PagForma).Text, 2)
            wvarPagos = wvarPagos & Right(String(14, "0") & Replace(Replace(wobjXMLListPag.Item(wvariCounter).selectSingleNode(mcteParam_PagImp).Text, ".", ""), ",", ""), 14)
            wvarPagos = wvarPagos & Right(String(3, "0") & wobjXMLListPag.Item(wvariCounter).selectSingleNode(mcteParam_PagCant).Text, 3)
        Next
        
        For wvariCounter = wobjXMLListPag.length To 5
            wvarPagos = wvarPagos & String(19, "0")
        Next
        ' FIN PAGOS
        
        ' INICIO CERTIFICADOS
        wvarRecibos = ""
        Set wobjXMLList = .selectNodes(mcteNodos_Recibos)

        For wvariCounter = 0 To wobjXMLList.length - 1
            wvarRecibos = wvarRecibos & Right(String(9, "0") & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_RecRecib).Text, 9)
            wvarRecibos = wvarRecibos & Right(String(14, "0") & Replace(Replace(wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_RecImp).Text, ".", ""), ",", ""), 14)
        Next
        
        For wvariCounter = wobjXMLList.length To 199
            wvarRecibos = wvarRecibos & String(23, "0")
        Next
        wvarCont = Right(String(3, "0") & wobjXMLList.length, 3) 'para saber cuantos son
        ' FIN CERTIFICADOS
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los Parametros de la cola de MQ del archivo de configuraci�n
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.Load (App.Path & "\" & gcteParamFileName)
    wvarCiaAsCod = wobjXMLParametros.selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & Space(4) & wvarClienSecAs & wvarNivelAs & "  000000000  000000000  000000000" & wvarPagos & wvarCont & wvarRecibos
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 36
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    
    Set wobjFrame2MQ = Nothing
    '
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        IAction_Execute = 1
        mobjCOM_Context.SetAbort
        GoTo ClearObjects:
    End If
    '
    'DA - 29/09/2008: para debbugear
'    Dim Fnum As Long
'        Fnum = FreeFile
'    Open App.Path & "\log.txt" For Append As #Fnum
'    Print #Fnum, "<AreaIN>" & wvarArea & "</AreaIN><AreaOUT>" & strParseString & "</AreaOUT>"
'    Close #Fnum
    '
    wvarStep = 200
    wvarResult = ""
    wvarPos = 4784 'cantidad de caracteres ocupados por par�metros de entrada
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 210
    wvarEstado = Mid(strParseString, 19, 2) 'Corto el estado
    '
    If wvarEstado <> "OK" Then
        '
        If Mid(strParseString, 21, 2) = "01" Then
            wvarStep = 220
            Response = "<Response><Estado resultado='false' mensaje='ESTA LIQUIDACION YA HA SIDO RENDIDA CON ANTERIORIDAD' /></Response>"
        ElseIf Mid(strParseString, 21, 2) = "02" Or Mid(strParseString, 21, 2) = "03" Then
            wvarStep = 220
            Response = "<Response><Estado resultado='false' mensaje='Su liquidaci�n no se ha podido generar, comun�quese con nuestra Mesa de Ayuda al 4323 - 4614' /></Response>"
        Else
            wvarStep = 220
            Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        End If
        '
    Else
        '
        wvarStep = 230
        wvarResult = ""
        '
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString)
        '
        wvarStep = 320
        Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
        '
    End If
    '
    wvarStep = 330
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing

Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarNivelAs & wvarClienSecAs & wvariCounter & wvarRecibos & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(wvarPos As Long, strParseString As String) As String
Dim wvarResult As String
Dim wvarCantLin As Long
Dim wvarCounter As Long
    '
    wvarResult = wvarResult & "<DATOS>"
    '
    wvarResult = wvarResult & "<NROLIQ>" & Mid(strParseString, wvarPos, 9) & "</NROLIQ>"
    'DA - 26/09/2008: se agregan 4 codigos de barras mas de 32 char y se expande el antiguo de 29 a 32
    'wvarResult = wvarResult & "<BARCODE>" & Mid(strParseString, wvarPos + 9, 29) & "</BARCODE>"
    wvarResult = wvarResult & "<BARCODE>" & Mid(strParseString, wvarPos + 9, 32) & "</BARCODE>"
    wvarResult = wvarResult & "<BARCODE_EFEC>" & Mid(strParseString, wvarPos + 41, 32) & "</BARCODE_EFEC>"
    wvarResult = wvarResult & "<BARCODE_CHEQ>" & Mid(strParseString, wvarPos + 73, 32) & "</BARCODE_CHEQ>"
    wvarResult = wvarResult & "<BARCODE_CHEQ48>" & Mid(strParseString, wvarPos + 105, 32) & "</BARCODE_CHEQ48>"
    wvarResult = wvarResult & "<BARCODE_CHEQ48DIF>" & Mid(strParseString, wvarPos + 137, 32) & "</BARCODE_CHEQ48DIF>"

    'DA - 26/09/2008: se ajusta esto ya que hay que contemplar los nuevos codigos de barra
    'wvarResult = wvarResult & "<FECLIQ>" & Mid(strParseString, wvarPos + 38, 10) & "</FECLIQ>"
    'wvarResult = wvarResult & "<FECVTO>" & Mid(strParseString, wvarPos + 48, 10) & "</FECVTO>"
    'wvarResult = wvarResult & "<HORASVENC>" & Val(Mid(strParseString, wvarPos + 58, 3)) & "</HORASVENC>"
    wvarResult = wvarResult & "<FECLIQ>" & Mid(strParseString, wvarPos + 169, 10) & "</FECLIQ>"
    wvarResult = wvarResult & "<FECVTO>" & Mid(strParseString, wvarPos + 179, 10) & "</FECVTO>"
    wvarResult = wvarResult & "<HORASVENC>" & Val(Mid(strParseString, wvarPos + 189, 3)) & "</HORASVENC>"
    wvarResult = wvarResult & "</DATOS>"
    '
    ParseoMensaje = wvarResult
    '
End Function
