package com.qbe.services.mqgeneric.impl;

import org.w3c.dom.Node;

import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Variant;

public interface IModGeneral {

	public static final String gcteQueueManager = "//QUEUEMANAGER";
	public static final String gctePutQueue = "//PUTQUEUE";
	public static final String gcteGetQueue = "//GETQUEUE";
	public static final String gcteGMOWaitInterval = "//GMO_WAITINTERVAL";
	public static final String gcteClassMQConnection = "WD.Frame2MQ";

	public String MidAsString(String pvarStringCompleto,
			Variant pvarActualCounter, int pvarLongitud) throws Exception;

	public String CompleteZero(String pvarString, int pvarLongitud)
			throws Exception;

	public String GetErrorInformacionDato(Node pobjXMLContenedor,
			String pvarPathDato, String pvarTipoDato, Node pobjLongitud,
			Node pobjDecimales, Node pobjDefault, Node pobjObligatorio)
			throws Exception;

	public Node GetRequestRetornado(XmlDomExtended pobjXMLRequest,
			Node pobjXMLRequestDef, String pvarStrRetorno) throws Exception;

}
