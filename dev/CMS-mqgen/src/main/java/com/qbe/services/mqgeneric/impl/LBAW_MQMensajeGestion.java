package com.qbe.services.mqgeneric.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.qbe.services.format.Formater;
import com.qbe.vbcompat.format.FormatMapper;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import com.qbe.vbcompat.format.VBFixesUtil;

public class LBAW_MQMensajeGestion extends AbstractMQMensaje {

	private static Logger logger = Logger.getLogger(LBAW_MQMensajeGestion.class.getName());

	/**
	 * Constructor por default
	 */
	public LBAW_MQMensajeGestion() {
		//Seteo de directorios
		mcteClassName = "lbawA_OVMQGen.lbaw_MQMensajeGestion";
		mcteSubDirName = Constants.DEF_MENSAJES_GESTION;
	}

	@Override
	public IModGeneral getModGeneral() {
		return AbstractMQMensaje.MOD_GENERAL;
	}
	
	@Override
	protected Node GetNodoParseado(String pvarstrToParse,
			Node pobjXMLDefinicion, int pvarCantidadRegistros) throws Exception {

	    org.w3c.dom.Node GetNodoParseado = null;
	    org.w3c.dom.Element wobjNodo = null;
	    org.w3c.dom.Element wobjNewNodo = null;
	    org.w3c.dom.Element wobjNewNodoCopy = null;
	    org.w3c.dom.Element wobjNodoContenedor = null;
	    XmlDomExtended wobjXMLDOM = null;
	    String wvarstrXML = "";
	    int wvarCounter = 0;
	    VbScript_RegExp wobjRegExp = null;
	    MatchCollection wobjColMatch = null;
	    Match wobjMatch = null;
	    String wvarParseEvalString = "";
	    int wvarCcurrRegistro = 0;
	    Variant wvarLastValue = new Variant();
	    String wvarNombreOcurrencia = "";
	    boolean wvarAreaConDatos = false;
	    boolean esCData = false;
	    //
	    //
	    //RegExp
	    //MatchCollection
	    //Match
	    // ************************************
	    //Conversion del Area de Salida en XML
	    // ************************************
	    //
	    wobjXMLDOM = new XmlDomExtended();
	    wobjRegExp = new VbScript_RegExp();

	    wobjXMLDOM.setPreservingWhiteSpace( true );

	    wvarParseEvalString = Strings.replace( pvarstrToParse, " ", "_" );
	    wvarstrXML = XmlDomExtended.getText( pobjXMLDefinicion.getAttributes().getNamedItem( "Nombre" ) );
	    wobjXMLDOM.loadXML( "<" + wvarstrXML + "></" + wvarstrXML + ">" );

	    if( ! (pobjXMLDefinicion.getAttributes().getNamedItem( "NombrePorOcurrencia" ) == (org.w3c.dom.Node) null) )
	    {
	      wvarNombreOcurrencia = XmlDomExtended.getText( pobjXMLDefinicion.getAttributes().getNamedItem( "NombrePorOcurrencia" ) );
	    }
	    else
	    {
	      wvarNombreOcurrencia = "";
	    }
	    //
	    // *******************************
	    //Evaluo el area segun el Pattern
	    // *******************************
	    wobjRegExp.setGlobal( true ); 
	    wobjRegExp.setPattern( XmlDomExtended.getText( pobjXMLDefinicion.getAttributes().getNamedItem( "Pattern" ) ) );
	    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
	    //
	    // *************************************
	    //Recorro el resultado de la evaluacion
	    // *************************************
	    wvarCcurrRegistro = 0;
	    wvarLastValue.set( "---" );
	    wvarAreaConDatos = true;
	    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
	    {
	    	// En el original:     For Each wobjMatch In wobjColMatch
	    	wobjMatch = wobjColMatch.next();
	    	
	      //Cada Registro Encontrado
	      if( ! ((wvarCcurrRegistro < pvarCantidadRegistros)) && (pvarCantidadRegistros != -1) )
	      {
	        break;
	      }
	      //
	      if( !wvarNombreOcurrencia.equals( "" ) )
	      {
	    	  wobjNodoContenedor = wobjXMLDOM.getDocument().createElement( wvarNombreOcurrencia );
	        wobjXMLDOM.selectSingleNode( "//" + wvarstrXML ).appendChild( wobjNodoContenedor );
	      }
	      else
	      {
	        wobjNodoContenedor = (org.w3c.dom.Element) wobjXMLDOM.selectSingleNode( "//" + wvarstrXML );
	      }
	      //
	      wvarCcurrRegistro = wvarCcurrRegistro + 1;
	      //
	      for( wvarCounter = 0; wvarCounter <= (pobjXMLDefinicion.getChildNodes().getLength() - 1); wvarCounter++ )
	      {
	        //Aca esta la definicion de cada campo
	        //
	        wobjNodo = (org.w3c.dom.Element) pobjXMLDefinicion.getChildNodes().item( wvarCounter );
	        if( wobjNodo.getChildNodes().getLength() == 0 )
	        {
	          esCData = (wobjNodo.getAttributes().getNamedItem( "CData" ) != null && wobjNodo.getAttributes().getNamedItem( "CData" ).getNodeValue().equals("SI"));
	          wvarLastValue.set(Strings.replace(wobjMatch.SubMatches(wvarCounter), "_", " ")); 
	          if( Strings.toUpperCase( XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "Visible" ) ) ).equals( "SI" ) )
	          {
	            wobjNewNodo = wobjXMLDOM.getDocument().createElement( XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "Nombre" ) ) );
	            wobjNodoContenedor.appendChild( wobjNewNodo );
	            //
	            
	            if(XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "ENTERO" ) )
	            {
	            	//La transformación del VBJ usa VB,val() y String.value(), que toman un double y por lo tanto genera para un entero x, x.0
	            	//Parece redundante pero el String puede venir como " 011" y tengo que convertirlo a "11"
	            	
	            	String newValue = "";
					if (!"".equals(wvarLastValue.toString().trim())) {
						//Esto lo hago, para que solo lo convierta a int, si viene informado, en caso contrario tendrá un 0 por default
						try {
							int parsedInt = Integer.parseInt(Strings
									.trim(wvarLastValue.toString()));
							newValue += parsedInt;
						} catch (NumberFormatException e) {
							newValue += "0";
						}
					} else {
						newValue += "0";
					}
					
	            	if (esCData) {
	            		XmlDomExtended.setCDATAText(wobjNewNodo, newValue);
	            	} else {
	            		XmlDomExtended.setText( wobjNewNodo, newValue );
	            	}
	            }
	            else if(XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "DECIMAL" ) )
	            {
	            /*  
	             * 
	               
	               wvarLastValue.set( Strings.trim( wvarLastValue.toString() ) ); //NOTA esto pasa 000000005000000 como 50000.0
	              String newValue = String.valueOf( VBFixesUtil.val( wvarLastValue.toString() ) / Math.pow( 10, VBFixesUtil.val( XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "Decimales" ) ) ) ) );
	              //Si el número no tiene decimales, VB lo formatea SIN .0 al final, mientras que en Java como String.valueOf recibe un double lo escribe con .0
	              if ( newValue.endsWith(".0")) newValue = newValue.substring(0, newValue.length()-2);
	              
	              */
	              BigDecimal bigD = convertToBigDecimal(wvarLastValue, Integer.valueOf(XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "Decimales" ))));
	              String newValue = formatBigDecimaltoQBELocale(bigD);
	              logger.fine("[LBAW_MQMensajeGestion] wvarLastValue: [" + wvarLastValue + "]");
	              logger.fine("[LBAW_MQMensajeGestion] wvarLastValue type: [" + wvarLastValue.getTypeName() + "]");
	              logger.fine("[LBAW_MQMensajeGestion] wvarLastValue decimales: [" + XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "Decimales" ) ) + "]");
	              
	              logger.fine("[LBAW_MQMensajeGestion] newValue: [" + newValue + "]");
	              // Nueva conversión
	              

	              logger.fine("[LBAW_MQMensajeGestion] nuevaConversión: newValue: [" + newValue + "]");

	              //
              	  if (esCData) {
	            	  XmlDomExtended.setCDATAText(wobjNewNodo, newValue);
	              } else {
	            	  XmlDomExtended.setText( wobjNewNodo, newValue);
	              }
	            }
	            else if(XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "FECHA" ) )
	            {
	              wvarLastValue.set( Strings.trim( wvarLastValue.toString() ) );
	              if (esCData) {
						XmlDomExtended.setCDATAText(wobjNewNodo, Strings.right( wvarLastValue.toString(), 2 ) + "/" + Strings.mid( wvarLastValue.toString(), 5, 2 ) + "/" + Strings.left( wvarLastValue.toString(), 4 ) );
					} else {
	              XmlDomExtended.setText( wobjNewNodo, Strings.right( wvarLastValue.toString(), 2 ) + "/" + Strings.mid( wvarLastValue.toString(), 5, 2 ) + "/" + Strings.left( wvarLastValue.toString(), 4 ) );
					}
					}
	            else if(XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "TEXTOORIGINAL" ) )
	            {
		            	if (esCData) {
							XmlDomExtended.setCDATAText(wobjNewNodo, wvarLastValue.toString() );
						} else {
							XmlDomExtended.setText( wobjNewNodo, wvarLastValue.toString() );
						}
	            }
	            //El siguiente If se agrega para que no aparezca el valor dos veces cuando esté el CDATA
	            else if (!esCData)
	            {
	              XmlDomExtended.setText( wobjNewNodo, Strings.trim( wvarLastValue.toString()) );
	            } else {
	            		XmlDomExtended.setCDATAText(wobjNewNodo,  Strings.trim( wvarLastValue.toString()) );
	            }
	            //
	            wobjNewNodoCopy = (org.w3c.dom.Element) null;
	            if( ! (wobjNodo.getAttributes().getNamedItem( "CopyTo" ) == (org.w3c.dom.Node) null) )
	            {
	              wobjNewNodoCopy = wobjXMLDOM.getDocument().createElement( XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "CopyTo" ) ) );
	              wobjNodoContenedor.appendChild( wobjNewNodoCopy );
	              XmlDomExtended.setText( wobjNewNodoCopy, XmlDomExtended.getText( wobjNewNodo ) );
	            }
	            //
	            //Verifico si se solicita algun Formato en especial del Origen
	            if( ! (wobjNodo.getAttributes().getNamedItem( "FormatOuput" ) == (org.w3c.dom.Node) null) )
	            {
	            	String format = XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "FormatOuput" ));
            		String toBeFormatted = XmlDomExtended.getText( wobjNewNodo );
            		String output = toBeFormatted;
	            	if (XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "TEXTO" ) ){
	            		output = FormatMapper.formatStringWithVBFormat(toBeFormatted, format);
	            	} else {
	            		//Supongo DECIMAL
		            	format = FormatMapper.mapVBNumberFormat(format);
		                DecimalFormat formatter = new DecimalFormat(format, new DecimalFormatSymbols(Locale.ENGLISH));
		                double d = Double.valueOf(toBeFormatted);
		                output = formatter.format(d);
	            	}
	            	XmlDomExtended.setText( wobjNewNodo, output );
	            }
	            //
	            //Verifico si se solicita algun Formato en especial del Destino
	            if( ! ((wobjNodo.getAttributes().getNamedItem( "FormatOuputCopy" ) == (org.w3c.dom.Node) null)) && ! ((wobjNewNodoCopy == (org.w3c.dom.Element) null)) )
	            {
	              //Antes: XmlDomExtended.setText( wobjNewNodoCopy, Strings.format( XmlDomExtended.getText( wobjNewNodo ), XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "FormatOuputCopy" ) ) ) );
	            	XmlDomExtended.setText( wobjNewNodoCopy, Formater.doubleFormat( XmlDomExtended.getText( wobjNewNodo ), XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "FormatOuputCopy" ) ) ) );
	            }
	            //
	          }
	          if( XmlDomExtended.Node_selectNodes(wobjNodo, "self::node()[./@Obligatorio='SI']").getLength() != 0 )
	          {
	            if( (XmlDomExtended.getText( wobjNewNodo ).equals( "" )) || (XmlDomExtended.getText( wobjNewNodo ).equals( "0" )) )
	            {
	              //El Dato Obligatorio no esta informado, por lo que no debe retornar ni este registro ni todos sus hijos
	              wvarAreaConDatos = false;
	            }
	          }
	        }
	        else
	        {
	        
	        //Acá empieza la diferencia con respecto a LBAW_MQMensaje
	        // agregado para el control de la longitud, para poder indicar cuantos quiero de regreso.
	        	
        	if(wobjNodo.getAttributes().getNamedItem("TipoDato") == null) {
        		//El nodo tiene hijos por lo que se debe procesar con un Pattern nuevo
        		if (!wvarLastValue.isNumeric()) {
        			wvarLastValue.set(-1);
        		}
        	} else {
        		wvarLastValue.set(wobjNodo.getAttributes().getNamedItem("TipoDato"));
        	}
        	
        	wobjNewNodo = (Element) GetNodoParseado(wobjMatch.SubMatches(wvarCounter), wobjNodo, wvarLastValue.toInt());
        	
        	if (wobjNewNodo != null) {
        		//Agregado porque si lo mando directo, salta org.w3c.dom.DOMException: WRONG_DOCUMENT_ERR: A node is used in a different document than the one that created it.
                Node importedNode = wobjXMLDOM.selectSingleNode( "//" + wvarstrXML ).getOwnerDocument().importNode(wobjNewNodo, true);
                wobjXMLDOM.selectSingleNode( "//" + wvarstrXML ).appendChild(importedNode);
        	}
	        //Acá finaliza la diferencia con respecto a LBAW_MQMensaje
        	
	        }
	      }
	    }

	    if( wvarAreaConDatos )
	    {
	      GetNodoParseado = wobjXMLDOM.getDocument().getChildNodes().item( 0 );
	    }

	    return GetNodoParseado;
	  
	}
	
	/**
	 * @param bigD
	 * @return
	 */
	private String formatBigDecimaltoQBELocale(BigDecimal bigD) {
		String newValue;
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		String pattern = "###.##";
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		// Si lo quiero usar para parsear
//		decimalFormat.setParseBigDecimal(true);
		newValue = decimalFormat.format(bigD);
		return newValue;
	}

	/**
	 * @param wvarLastValue
	 * @param decimales
	 * @return
	 * @throws Exception
	 */
	private BigDecimal convertToBigDecimal(Variant wvarLastValue, int decimales) throws Exception {
		verifyValidQBEFormat(wvarLastValue.toString());
		
		// Esta conversión espera un "." como separador de decimales
		// En nuestro caso no afecta porque los Strings que vienen desde AIS son enteros ( sin parte decimal )
		BigDecimal crudo = new BigDecimal(wvarLastValue.toString());
		BigDecimal bigD = crudo.divide(new BigDecimal(Math.pow(10, decimales)));
		return bigD;
	}

	/**
	 * @param aisNumber
	 * @throws Exception
	 */
	private void verifyValidQBEFormat(String aisNumber) throws Exception {
		Pattern aisNumberPattern = Pattern.compile("-{0,1}\\d+");
		if ( !aisNumberPattern.matcher(aisNumber).matches() ) {
			throw new Exception(aisNumber + " doesn't match");			
		}
		
		
	}
}
