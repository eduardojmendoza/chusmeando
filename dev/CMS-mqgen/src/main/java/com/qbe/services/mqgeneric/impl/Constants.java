package com.qbe.services.mqgeneric.impl;

import java.io.File;

public interface Constants {

	static final String ROOT_DEF_DIR = "DefinicionesXML";
	static final String DEF_MENSAJES_GESTION = "DefinicionesMensajesGestion";

	static final String LOG_FILES_ROOT_PATH = "/tmp" + File.separator + "Log" + File.separator;

}
