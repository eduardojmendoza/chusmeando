package com.qbe.services.mqgeneric.impl;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Node;

import com.qbe.connector.mq.MQProxy;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.vbcompat.format.VBFixesUtil;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.framework.jaxb.Response;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;

/**
 * Clase abstracta que contiene todo el comportamiento general de los mensajes.
 * 
 * @author leandrocohn
 *
 */
public abstract class AbstractMQMensaje implements VBObjectClass {

	private static Logger logger = Logger.getLogger(AbstractMQMensaje.class.getName());

	public static final IModGeneral MOD_GENERAL = new ModGeneral();

	/**
	 * Implementacion de los objetos
	 */
	protected String mcteClassName;
	protected String mcteSubDirName;
	private EventLog mobjEventLog = new EventLog();
	protected String mvarConsultaRealizada = "";
	protected boolean mvarCancelacionManual = false;
	static int wvarStep = 0;

	/**
	 * static variable for method: IAction_Execute
	 */
	private final String wcteFnName = "IAction_Execute";

	public abstract IModGeneral getModGeneral();
	
	public int IAction_Execute( String request, StringHolder responseHolder, String contextInfo ) {
		int IAction_Execute = 0;
		Variant vbLogEventTypeWarning = new Variant();
		Variant vbLogEventTypeError = new Variant();
		String wvarOpID = "";
		XmlDomExtended wobjXMLPrueba = null;
		XmlDomExtended wobjXMLDefinition = null;
		XmlDomExtended wobjXMLRequest = null;
		XmlDomExtended wobjXMLRespuesta = null;
		org.w3c.dom.NodeList wobjNodosEntrada = null;
		org.w3c.dom.Node wobjNodoEntrada = null;
		MQProxy wobjFrame2MQ = null;
		int wvarGMOWaitInterval = 0;
		String wvarResult = "";
		java.util.Date wvarFechaConsulta = DateTime.EmptyDate;
		String wvarArea = "";
		String wvarLastArea = "";
		int wvarPos = 0;
		int wvarMaxLen = 0;
		String strParseString = "";
		String strParseStringTemp = "";
		String wvarstrXMLEnvioArea = "";
		String wvarEstado = "";
		String wvarCodigoError = "";
		String wvarConfFileName = "";
		String wvarDefinitionFile = "";
		String wvarXMLAreas = "";
		String wvarError = "";
		String wvarRequestRetorno = "";
		boolean wvarAppendRequest = false;
		boolean wvarConsultarMQ = false;
		int wvarCantRellamRest = 0;
		String wvarXMLHarcodeado = "";
		String wvarAreaHarcodeada = "";
		String wvarDescripcion = "";
		int wvarMQError = 0;
		//
		try {
			//~~~~~~~~~~~~~~~~~~~~~~~~~~~
			wvarFechaConsulta = DateTime.now();
			//
			// ***************************************************************************************************
			//Levanto los XMLs de Configuracion del area MQ y del Request de la Paguna
			// ***************************************************************************************************
			//
			wvarStep = 10;
			wobjXMLRequest = new XmlDomExtended();
			wobjXMLDefinition = new XmlDomExtended();
			wobjXMLRequest.loadXML( request );
			//
			wvarStep = 20;
			wvarDefinitionFile = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DEFINICION" ) );
			
			mvarConsultaRealizada = StringUtils.left( wvarDefinitionFile, Strings.len( wvarDefinitionFile ) - 4 );

			String definitionFileName = Constants.ROOT_DEF_DIR + File.separator + mcteSubDirName + File.separator + wvarDefinitionFile;
			if ( definitionFileName == null || definitionFileName.length() == 0 ) {
				throw new Exception("definitionFileName es null o vacío");
			}
			InputStream definitionIS = Thread.currentThread().getContextClassLoader().getResourceAsStream(definitionFileName);
			if ( definitionIS == null ) {
				throw new Exception("definitionIS es null, no se encontró el archivo: " + definitionFileName);				
			}
			wobjXMLDefinition.load(definitionIS);

			wvarStep = 30;
			wvarOpID = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/MENSAJE" ) );
			//
			wvarStep = 31;
			wvarConfFileName = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//MQCONFIGFILE" ) );
			//
			wvarStep = 32;
			wvarGMOWaitInterval = Obj.toInt( XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//TIMEOUT" ) ) );
			//
			// ***************************************************************************************************
			//Recorro los campos del XML y voy armando el area MQ segun los parámetros que llegan desde la página
			// ***************************************************************************************************
			// *********************************************************************************************************************************
			//NOTA IMPORTANTISIMA: Este componente solo permite un nivel para los vectores, o sea que no funciona con un vector adentro de otro
			// *********************************************************************************************************************************
			wvarStep = 40;
			wobjNodosEntrada = wobjXMLDefinition.selectNodes( "//ENTRADA/CAMPO" );
			wvarArea = "";

			Node root = wobjXMLRequest.getDocument().getChildNodes().item( 0 );
			
			for( int nwobjNodoEntrada = 0; nwobjNodoEntrada < wobjNodosEntrada.getLength(); nwobjNodoEntrada++ ) {
				wobjNodoEntrada = wobjNodosEntrada.item( nwobjNodoEntrada );

				String nombreCampo = "//" + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre") );

				if( wobjNodoEntrada.getChildNodes().getLength() == 0 ) {
					//Es un Dato Normal
					String tipoCampo = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "TipoDato") );
					Node nodoEnteros = wobjNodoEntrada.getAttributes().getNamedItem( "Enteros");
					Node nodoDecimales = wobjNodoEntrada.getAttributes().getNamedItem( "Decimales");
					Node nodoDefault = wobjNodoEntrada.getAttributes().getNamedItem( "Default");
					Node nodoObligatorio = wobjNodoEntrada.getAttributes().getNamedItem( "Obligatorio");

					wvarArea = wvarArea + GetDatoFormateado(root, nombreCampo, tipoCampo, nodoEnteros, nodoDecimales, nodoDefault, nodoObligatorio);
				} else {
					//Es un Vector
					int cantElementos = (int)Math.rint( VBFixesUtil.val( XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Cantidad") ) ));
					wvarArea = wvarArea + GetVectorDatos(wobjXMLRequest,nombreCampo ,wobjNodoEntrada.getChildNodes(), cantElementos) ;
				}
			}
			//
			wvarStep = 50;
			//
			// ***********************************************************************
			//Se puede utilizar un Area IN / OUT preestablecidas para hacer pruebas
			// ***********************************************************************
			//
			wvarStep = 55;
			if( ! ((wobjXMLRequest.selectSingleNode( "//AREAOUTMQ" ) == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//AREAINMQ" ) == (org.w3c.dom.Node) null)) )
			{
				if( ! (wobjXMLRequest.selectSingleNode( "//AREAOUTMQ" ) == (org.w3c.dom.Node) null) )
				{
					//Desestimo el Nro de Mensaje porque se agrega despues
					wvarArea = Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//AREAOUTMQ" ) ), 5 );
				}
				else
				{
					//Desestimo el Nro de Mensaje porque se agrega despues
					wvarArea = Strings.mid( XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//AREAOUTMQ" ) ), 5 );
				}
			}
			//
			wvarStep = 55;
			if( ! ((wobjXMLRequest.selectSingleNode( "//AREAINMQ" ) == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//AREAINMQ" ) == (org.w3c.dom.Node) null)) )
			{
				if( ! (wobjXMLRequest.selectSingleNode( "//AREAINMQ" ) == (org.w3c.dom.Node) null) )
				{
					//Desestimo el Nro de Mensaje porque se agrega despues
					wvarAreaHarcodeada = Strings.mid( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//AREAINMQ" ) ), 5 );
				}
				else
				{
					//Desestimo el Nro de Mensaje porque se agrega despues
					wvarAreaHarcodeada = Strings.mid( XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//AREAINMQ" ) ), 5 );
				}
			}
			//
			wvarPos = Strings.len( wvarOpID ) + Strings.len( wvarArea ) + 1;
			wvarXMLAreas = "<AreaIN>" + wvarOpID + wvarArea + "</AreaIN>";
			//
			// *****************************************************************
			//Si solo se solicita el Area MQ la devuelvo y salgo del componente
			// *****************************************************************
			//
			wvarStep = 60;
			if( ! ((wobjXMLRequest.selectSingleNode( "//SOLORETORNARAREA" ) == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//SOLORETORNARAREA" ) == (org.w3c.dom.Node) null)) )
			{
				responseHolder.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "No se Procesa por la existencia del TAG SOLORETORNARAREA" + String.valueOf( (char)(34) ) + " />" + wvarXMLAreas + "</Response>" );
				//unsup GoTo Salir  (RESUELTO, puse un else para todo loque está entre este goto y el label Salir:)
			} 
			else
			{
				//
				// ***********************************************************************
				//Se puede utilizar un XML de respuesta preestablecido para hacer pruebas
				// ***********************************************************************
				//
				wvarStep = 70;
				wvarConsultarMQ = true;
				if( ! ((wobjXMLRequest.selectSingleNode( "//XMLPRUEBA" ) == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//XMLPRUEBA" ) == (org.w3c.dom.Node) null)) )
				{
					wvarConsultarMQ = false;
					wobjXMLPrueba = new XmlDomExtended();
					if( ! (wobjXMLRequest.selectSingleNode( "//XMLPRUEBA" ) == (org.w3c.dom.Node) null) )
					{ 
						wobjXMLPrueba.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(Constants.ROOT_DEF_DIR + File.separator + mcteSubDirName + File.separator + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//XMLPRUEBA" )))) ;
					}
					else
					{ 
						wobjXMLPrueba.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(Constants.ROOT_DEF_DIR + File.separator + mcteSubDirName + File.separator + XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//XMLPRUEBA" ) ) ));
					}
					wvarXMLHarcodeado = wobjXMLPrueba.getDocument().getDocumentElement().toString();
					wobjXMLPrueba = (XmlDomExtended) null;
				}
				//
				wvarStep = 75;
				//
				// ***********************************************************************
				//Se puede utilizar un area preestablecida de Respuesta para hacer pruebas
				// ***********************************************************************
				//
				// Este parámetro controla la cantidad de páginas a retornas, y se usa en el if (TR) abajo: en cada rellamada resta 1
				wvarCantRellamRest = 1;
				if( ! (wobjXMLDefinition.selectSingleNode( "//PAGINASRETORNADAS" ) == (org.w3c.dom.Node) null) )
				{
					wvarCantRellamRest = Obj.toInt( XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//PAGINASRETORNADAS" ) ) );
				}
				if( ! (wobjXMLRequest.selectSingleNode( "//PAGINASRETORNADAS" ) == (org.w3c.dom.Node) null) )
				{
					wvarCantRellamRest = Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//PAGINASRETORNADAS" ) ) );
				}

				//
				//Este flag controla si el do/while se vuelve a ejecutar
				boolean continuar = false;
				boolean primeraVez = true;
				do {
					// por default cada llamada es final ( no hace una llamada de continuación ), salvo que reciba un "TR" ( ver abajo )
					continuar = false;
					if( wvarConsultarMQ )
					{
						//Este if controla que el bloque se ejecute solamente la primera vez. Como se ve, en el código original está antes del label RelizarRellamado
						if ( primeraVez) {
							//
							// ******************************************************************************************
							//Levanto los datos de la cola de MQ del archivo de configuración para actualizar el TimeOut
							// ******************************************************************************************
							//
							wvarStep = 80;
							//
							// No se usa el archivo de config
//							wobjXMLConfig = new XmlDomExtended();
//
//							wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(wvarConfFileName));
//							XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( IModGeneral.gcteGMOWaitInterval ), String.valueOf( wvarGMOWaitInterval ) );
							primeraVez = false;
						}
						wvarStep = 90;
						//
						// ***************************************************
						//Componente que realiza la conexion MQ
						// ***************************************************
						//
						RealizarRellamado: 
							//
							if( wvarAreaHarcodeada.equals( "" ) )
							{
								wobjFrame2MQ = MQProxy.getInstance(wvarConfFileName);
								StringHolder strParseStringHolder = new StringHolder( strParseString);
								wvarMQError = wobjFrame2MQ.execute(wvarOpID + wvarArea, strParseStringHolder);
								strParseString = strParseStringHolder.getValue();
							}
							else
							{
								strParseString = wvarAreaHarcodeada;
								wvarMQError = 0;
							}
						//
						wvarStep = 100;
						//
						//
						if( wvarMQError != 0 )
						{
							responseHolder.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
							mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Mensaje:" + wvarOpID + " Area:" + wvarOpID + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
							//unsup GoTo Terminar ( RESUELTO )
							IAction_Execute = terminar(request, responseHolder, IAction_Execute,
									vbLogEventTypeWarning, wvarOpID, wobjXMLDefinition, wobjXMLRequest,
									wvarFechaConsulta, wvarArea, strParseString, wvarEstado,
									wvarDefinitionFile);
							return IAction_Execute;
						}
						//
						wvarStep = 120;
						//
						wvarXMLAreas = wvarXMLAreas + "<AreaOUT>" + strParseString + "</AreaOUT>";
						//
						// ****************************************************************
						//Agregamos las Areas en el XML de respuesta si es que solicitaron
						// ****************************************************************
						//
						if( ! ((wobjXMLRequest.selectSingleNode( "//RETORNARAREAS" ) == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//RETORNARAREAS" ) == (org.w3c.dom.Node) null)) )
						{
							wvarstrXMLEnvioArea = "<AreasMQ>" + wvarXMLAreas + "</AreasMQ>";
						}
						else
						{
							wvarstrXMLEnvioArea = "";
						}
						//
						wvarStep = 245;
						//
						// ****************************************************************
						//Agregamos en el XML de respuesta el Request si es que lo Solicitaron
						// ****************************************************************
						//
						if( ! ((wobjXMLRequest.selectSingleNode( "//RETORNARREQUEST" ) == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//RETORNARREQUEST" ) == (org.w3c.dom.Node) null)) )
						{
							wvarRequestRetorno = request;
							wvarAppendRequest = true;
						}
						else
						{
							wvarRequestRetorno = "";
							wvarAppendRequest = false;
						}
					}
					//
					wvarStep = 250;
					//Corto el estado
					wvarEstado = Strings.mid( strParseString, 19, 2 );
					//Corto el Codigo de Error
					wvarCodigoError = Strings.mid( strParseString, 21, 2 );
					//
					// ************************************************************
					//Cortamos el Area en caso que se determine el maximo del area
					// ************************************************************
					if( ! (wobjXMLDefinition.selectSingleNode( "//SALIDA" ).getAttributes().getNamedItem( "MaxLen" ) == (org.w3c.dom.Node) null) )
					{
						wvarMaxLen = Obj.toInt( XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//SALIDA" ).getAttributes().getNamedItem( "MaxLen" ) ) );
						strParseString = Strings.mid( strParseString, 1, (wvarMaxLen + wvarPos) - 1 );
					}
					//
					// ***********************************************************************
					//Vamos Armando el Area en el String Temporal para agregar los rellamados
					// ***********************************************************************
					if( strParseStringTemp.equals( "" ) )
					{
						strParseStringTemp = strParseString;
					}
					else
					{
						strParseStringTemp = strParseStringTemp + Strings.mid( strParseString, wvarPos );
					}
					//
					if( (wvarEstado.equals( "ER" )) || ! ((wobjXMLDefinition.selectSingleNode( ("//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "']") ) == (org.w3c.dom.Node) null)) )
					{
						//
						// **********************************************************************************************
						//En el XML De Definicion se pueden establecer Estados Errones con descripciones predeterminadas
						// **********************************************************************************************
						wvarStep = 260;
						if( wobjXMLDefinition.selectSingleNode( ("//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "']") ) == (org.w3c.dom.Node) null )
						{
							wvarDescripcion = "SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA";
						}
						else
						{
							if( (!Strings.trim( wvarCodigoError ).equals( "" )) && ! ((wobjXMLDefinition.selectSingleNode( ("//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "' and @CodigoError='" + wvarCodigoError + "']") ) == (org.w3c.dom.Node) null)) )
							{
								wvarDescripcion = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "' and @CodigoError='" + wvarCodigoError + "']" ).getAttributes().getNamedItem( "Descripcion" ) );
							}
							else
							{
								wvarDescripcion = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "']" ).getAttributes().getNamedItem( "Descripcion" ) );
							}
						}
						responseHolder.set( "<Response><Estado resultado='false' mensaje='" + wvarDescripcion + "' />" + wvarRequestRetorno + wvarstrXMLEnvioArea + "</Response>" );
						//
					}
					else if( (wvarEstado.equals( "TR" )) && ! ((wvarCantRellamRest == 1)) )
					{
						//Area Para el Rellamado
						wvarArea = Strings.mid( strParseString, 5, wvarPos - 1 ); //Empieza en 5, se saltea el código del mensaje porque en el execute se lo agrega ( wvarOpID + wvarArea )
						wvarCantRellamRest = wvarCantRellamRest - 1;
						//unsup GoTo RealizarRellamado
						// Así fuerzo el GoTo
						continuar = true;
					} else {
						//
						wvarStep = 280;
						//
						//Area de la ultima llamada
						wvarLastArea = Strings.mid( strParseString, 1, wvarPos - 1 );
						strParseStringTemp = wvarLastArea + Strings.mid( strParseStringTemp, wvarPos );
						//
						wvarResult = ParseoMensaje(wvarPos,strParseStringTemp,wobjXMLDefinition.selectSingleNode( "//SALIDA") ,wobjXMLRequest,wvarAppendRequest,wvarXMLHarcodeado)  ;
						//
						wvarStep = 340;
						responseHolder.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + wvarstrXMLEnvioArea + "</Response>" );
						//
						if( ! (wobjXMLDefinition.selectSingleNode( "//SALIDA" ).getAttributes().getNamedItem( "xPathParaVerificarExistenciaDeDatos" ) == (org.w3c.dom.Node) null) )
						{
							// ********************************************************************************************************
							//Se verifica la existencia de datos en la Respuesta segun el atributo xPathParaVerificarExistenciaDeDatos
							// ********************************************************************************************************
							wobjXMLRespuesta = new XmlDomExtended();
							if( wobjXMLRespuesta.loadXML( wvarResult ) )
							{
								if( wobjXMLRespuesta.selectNodes( XmlDomExtended.getText(  wobjXMLDefinition.selectSingleNode( "//SALIDA" ).getAttributes().getNamedItem( "xPathParaVerificarExistenciaDeDatos" ) ) ).getLength() == 0 )
								{
									responseHolder.set( "<Response><Estado resultado='false' mensaje='NO SE ENCONTRARON DATOS' />" + wvarResult + wvarstrXMLEnvioArea + "</Response>" );
								}
							}
						}
					}
				} while ( continuar);	
				//
			} //Del else agregado para saltear el GoTo Salir

			Salir: 
				wvarStep = 350;
			/*TBD mobjCOM_Context.SetComplete() ;*/
			IAction_Execute = 0;

			Terminar: 

				IAction_Execute = terminar(request, responseHolder, IAction_Execute,
						vbLogEventTypeWarning, wvarOpID, wobjXMLDefinition, wobjXMLRequest,
						wvarFechaConsulta, wvarArea, strParseString, wvarEstado,
						wvarDefinitionFile);
		}
		catch (MQProxyTimeoutException toe) {
			responseHolder.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
			return 3;
		}
		catch( Exception _e_ )
		{
			Err.set( _e_ );
			logger.log(Level.SEVERE, "Exception al ejecutar el request", _e_);

			wvarError = Err.getError().getDescription();
			mobjEventLog.Log( ErrorConstants.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + wvarError + " Mensaje:" + wvarOpID + " Area:" + wvarOpID + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );

			if( mvarCancelacionManual )
			{
				responseHolder.set( "<Response><Estado resultado='false' mensaje='" + wvarError + "' /></Response>" );
				/*TBD mobjCOM_Context.SetComplete() ;*/
				IAction_Execute = 0;
			}
			else
			{
				/*TBD mobjCOM_Context.SetAbort() ;*/
				responseHolder.set(Response.marshalWithException("Exception al ejecutar el request", _e_));
				IAction_Execute = 1;
			}
			Err.clear();
			//unsup Resume ClearObjects
			// No hace nada en ClearObjects así que es como:
			return IAction_Execute;
		}
		return IAction_Execute;
	}

	/**
	 * @param request
	 * @param responseHolder
	 * @param IAction_Execute
	 * @param vbLogEventTypeWarning
	 * @param wvarOpID
	 * @param wobjXMLDefinition
	 * @param wobjXMLRequest
	 * @param wvarFechaConsulta
	 * @param wvarArea
	 * @param strParseString
	 * @param wvarEstado
	 * @param wvarDefinitionFile
	 * @return
	 */
	private int terminar(String request, StringHolder responseHolder, int IAction_Execute,
			Variant vbLogEventTypeWarning, String wvarOpID,
			XmlDomExtended wobjXMLDefinition, XmlDomExtended wobjXMLRequest,
			java.util.Date wvarFechaConsulta, String wvarArea,
			String strParseString, String wvarEstado, String wvarDefinitionFile) {
		XmlDomExtended wobjXMLLog;
		XmlDomExtended wobjXMLOldLog;
		org.w3c.dom.Element wobjNewNodo;
		String wvarError;
		String wvarPathLog;
		int wvarCantLlamadasALoguear;
		int wvarCounter;
		int wvarCantLlamadasLogueadas;
		String wvarFiltroEstado;
		try 
		{

			wvarStep = 360;
			wvarStep = 361;
			//
			// ****************************************************************
			//Generamos un Log en caso que se solicite
			// ****************************************************************
			//
			wvarCantLlamadasALoguear = 1;
			wvarFiltroEstado = "SIN_DETERMINAR";
			if( ! ((wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) == (org.w3c.dom.Node) null)) )
			{
				//DA
				wvarStep = 362;
				wvarPathLog = Constants.LOG_FILES_ROOT_PATH + wvarDefinitionFile;
				//
				if( ! (wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) == (org.w3c.dom.Node) null) )
				{
					if( ! (wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ).getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
					{
						wvarPathLog = Constants.LOG_FILES_ROOT_PATH + XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ).getAttributes().getNamedItem( "LogFile" ) );
					}
					if( ! (wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ).getAttributes().getNamedItem( "CantLlamadasALoguear" ) == (org.w3c.dom.Node) null) )
					{
						wvarCantLlamadasALoguear = Obj.toInt( XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ).getAttributes().getNamedItem( "CantLlamadasALoguear" ) ) );
					}
					if( ! (wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ).getAttributes().getNamedItem( "EstadosNoLogueables" ) == (org.w3c.dom.Node) null) )
					{
						wvarFiltroEstado = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ).getAttributes().getNamedItem( "EstadosNoLogueables" ) );
					}
				}
				else
				{
					if( ! (wobjXMLRequest.selectSingleNode( "//GENERARLOG" ).getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
					{
						wvarPathLog = Constants.LOG_FILES_ROOT_PATH + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//GENERARLOG" ).getAttributes().getNamedItem( "LogFile" ) );
					}
					if( ! (wobjXMLRequest.selectSingleNode( "//GENERARLOG" ).getAttributes().getNamedItem( "CantLlamadasALoguear" ) == (org.w3c.dom.Node) null) )
					{
						wvarCantLlamadasALoguear = Obj.toInt( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//GENERARLOG" ).getAttributes().getNamedItem( "CantLlamadasALoguear" ) ) );
					}
					if( ! (wobjXMLRequest.selectSingleNode( "//GENERARLOG" ).getAttributes().getNamedItem( "EstadosNoLogueables" ) == (org.w3c.dom.Node) null) )
					{
						wvarFiltroEstado = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//GENERARLOG" ).getAttributes().getNamedItem( "EstadosNoLogueables" ) );
					}
				}
				//
				//DA
				wvarStep = 363;
				// Tuve que corregir la conversión porque pinchaba con:
				// java.util.regex.PatternSyntaxException: Dangling meta character '*' near index 0
				//*\
				//^
				// El original es:
				//         If Not wvarPathLog Like "*\" And (Not wvarEstado Like "*" & wvarFiltroEstado & "*") Then
				// En la sintaxis VB:
				// http://msdn.microsoft.com/en-us/library/swf8kaxw.aspx
				// *  matches: Zero or more characters
				//
				// Usando regexp sería .* entonces
				// Y pongo el File.separator() en vez de la \ que viene en el original
				// Y quoteado porque en Windows pone una \ que tenemos que escapear en regexp

				if ( ! wvarPathLog.matches(Pattern.quote(".*" + File.separator)) && ! wvarEstado.matches( ".*" + wvarFiltroEstado + ".*")  ) {

					//Genero el Log solo si en el nodo se especifico un nombre de archivo
					wobjXMLLog = new XmlDomExtended();
					//
					//DA
					wvarStep = 364;
					wobjXMLLog.loadXML( "<LOGs><LOG MensajeMQ='" + wvarOpID + "' InicioConsulta='" + DateTime.format( wvarFechaConsulta, "dd/MM/yyyy HH:mm:ss" ) + "' TiempoIncurrido = '" + (int)Math.rint( (DateTime.diff( wvarFechaConsulta, DateTime.now() ) * 86400) ) + " Seg' >" + request + responseHolder + "</LOG></LOGs>" );
					//
					//DA
					wvarStep = 365;
					wobjNewNodo = wobjXMLLog.getDocument().createElement( "AreasMQ" );
					wobjXMLLog.selectSingleNode( "//LOG" ).appendChild( wobjNewNodo );
					//
					//DA
					wvarStep = 366;
					//Agrego las areas a mano porque el LoadXML Falla con strings tan grandes como el Area Out
					wobjNewNodo = wobjXMLLog.getDocument().createElement( "AreaIN" );
					wobjXMLLog.selectSingleNode( "//LOG/AreasMQ" ).appendChild( wobjNewNodo );
					XmlDomExtended.setText( wobjNewNodo, wvarOpID + wvarArea );
					//
					//DA
					wvarStep = 368;
					wobjNewNodo = wobjXMLLog.getDocument().createElement( "AreaOUT" );
					wobjXMLLog.selectSingleNode( "//LOG/AreasMQ" ).appendChild( wobjNewNodo );
					XmlDomExtended.setText( wobjNewNodo, strParseString );
					//
					//DA
					wvarStep = 369;
					URL wvarPathLogURL = Thread.currentThread().getContextClassLoader().getResource(wvarPathLog);
					File logFile;
					String path;
					if  ( wvarPathLogURL != null &&
						( path = wvarPathLogURL.getPath() ).length() > 0 &&
						( logFile = new File(path)) != null && 	
						logFile != null && logFile.isFile())
					{
						//DA
						wvarStep = 370;
						//Ya existe el Log, entonces agrego la nueva consulta al LOG
						wobjXMLOldLog = new XmlDomExtended();
						wobjXMLOldLog.load( wvarPathLog );
						//DA
						wvarStep = 371;
						if( wobjXMLOldLog.selectSingleNode( "//LOGs" ) == (org.w3c.dom.Node) null )
						{
							// *********LOG NO VALIDO***********
							wobjXMLLog.save( wvarPathLog );
						}
						else
						{
							//DA
							wvarStep = 372;
							wvarCantLlamadasLogueadas = wobjXMLOldLog.selectSingleNode( "//LOGs" ).getChildNodes().getLength();
							for( wvarCounter = 1; wvarCounter <= ((wvarCantLlamadasLogueadas - wvarCantLlamadasALoguear) + 1); wvarCounter++ )
							{
								wobjXMLOldLog.selectSingleNode( "//LOGs" ).removeChild( wobjXMLOldLog.selectSingleNode( "//LOGs" ).getChildNodes().item( 0 ) );
							}
							//DA
							wvarStep = 373;
							wobjXMLOldLog.selectSingleNode( "//LOGs" ).appendChild( wobjXMLLog.selectSingleNode( "//LOGs" ).getChildNodes().item( 0 ) );
							//DA
							wvarStep = 374;
							wobjXMLOldLog.save( wvarPathLog );
						}
					}
					else
					{
						//DA
						wvarStep = 375;
						wobjXMLLog.save( wvarPathLog );
					}
				}
			}
		}
		catch( Exception _e_ )
		{
			Err.set( _e_ );
			logger.log(Level.SEVERE, "Exception al ejecutar el request", _e_);
				//~~~~~~~~~~~~~~~
				//
				wvarError = Err.getError().getDescription();
				mobjEventLog.Log( ErrorConstants.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Warning= [" + Err.getError().getNumber() + "] - " + wvarError + " Mensaje:" + wvarOpID + " Area:" + wvarOpID + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeWarning );

				if( mvarCancelacionManual )
				{
					responseHolder.set(Response.marshaledResultadoFalse(wvarError,""));
				}
				else
				{
					responseHolder.set(Response.marshalWithException("Exception al ejecutar el request", _e_));
				}

				/*TBD mobjCOM_Context.SetComplete() ;*/
				IAction_Execute = 0;

				//unsup Resume ClearObjects
				Err.clear();
		}
		return IAction_Execute;
	}

	private String ParseoMensaje( int pvarPos, String pstrParseString, Node pobjNodoDefiniciones, XmlDomExtended pobjXMLRequest, boolean pvarAppendRequest, String pvarXMLHarcodeado ) throws Exception
	{
		String ParseoMensaje = "";
		String wvarEvalString = "";
		XmlDomExtended wobjXslDOMResp = null;
		XmlDomExtended wobjXmlDOMResp = null;
		XmlDomExtended wobjXMLRequestTemp = null;
		String wvarXSLFile = "";
		org.w3c.dom.Node wobjNodoXSL = null;
		org.w3c.dom.Node wobjNodoParseo = null;
		String wvarNuevoXML = "";

		wobjXmlDOMResp = new XmlDomExtended();
		wobjXmlDOMResp.setPreservingWhiteSpace( true );
		if( pvarXMLHarcodeado.equals( "" ) )
		{
			// *****************************************************************************
			//Envio el Area Recibida junto con el primer Pattern para que se genere el XML
			// *****************************************************************************
			wvarEvalString = Strings.mid( pstrParseString, pvarPos );
			//En la primera llamada determino si es un unico registro o un vector
			if( pobjNodoDefiniciones.getChildNodes().item( 0 ).getChildNodes().getLength() > 1 )
			{
				//Es un campo compuesto, se entiende que no es un vector
				wobjNodoParseo = GetNodoParseado(wvarEvalString,pobjNodoDefiniciones.getChildNodes().item( 0 ),1)  ;
			}
			else
			{
				//Tiene definido el largo de un solo campo, se entiene que es un vector
				wobjNodoParseo = GetNodoParseado(wvarEvalString,pobjNodoDefiniciones.getChildNodes().item( 0 ),-1)  ;
			}
			if( ! (wobjNodoParseo == (org.w3c.dom.Node) null) )
			{
				wobjXmlDOMResp.loadXML( XmlDomExtended.marshal(wobjNodoParseo));
			}
		}
		else
		{
			// *****************************************************************************
			//Genero el XML con lo harcodeado
			// *****************************************************************************
			wobjXmlDOMResp.loadXML( pvarXMLHarcodeado );
		}
		//
		//Quiere decir que levanto un XML Valido

		//FALTA revisar esta conversión. Como a veces es null, el toString da NPE. Revisar si es lo mismo el chequeo nuevo
		//    if( !wobjXmlDOMResp.getDocument().getDocumentElement().toString().equals( "" ) )
		// Otra posibilidad:    if( !(wobjXmlDOMResp.getDocument().getDocumentElement() == null) )
//		logger.log(Level.FINEST, "en ParseoMensaje. wobjXmlDOMResp.toString(): " + StringUtils.abbreviate(wobjXmlDOMResp.toString(), 1000));
//		logger.log(Level.FINEST, "en ParseoMensaje. wobjXmlDOMResp.marshal(): " + StringUtils.abbreviate(wobjXmlDOMResp.marshal(), 1000));

		if( ! "".equals(wobjXmlDOMResp.marshal()) ) {
			//
			// *************************************************************************************
			//Se agrega el Request en el area de respuesta en caso que haya sido solicitado
			//Esto sirve para utilizarlo si se aplica un XSL que necesite algun dato de la llamada
			// *************************************************************************************
			if( pvarAppendRequest )
			{
				logger.log(Level.FINEST, "en ParseoMensaje, por agregar request");
				//AGREGADO PARA ARREGLAR ERROR EN MSG 1428
				
				//Agregado porque si lo mando directo, salta org.w3c.dom.DOMException: WRONG_DOCUMENT_ERR: A node is used in a different document than the one that created it.
				wobjXMLRequestTemp = new XmlDomExtended();
				wobjXMLRequestTemp.loadXML( pobjXMLRequest.toString() );
				logger.log(Level.FINEST, "pobjXMLRequest.toString(): " + pobjXMLRequest.toString());
				logger.log(Level.FINEST, "pobjXMLRequest.marshal(): " + pobjXMLRequest.marshal());
				Node nodeToImport = getModGeneral().GetRequestRetornado( wobjXMLRequestTemp, XmlDomExtended.Node_selectSingleNode( pobjNodoDefiniciones, "//ENTRADA" ), Strings.mid( pstrParseString, 1, pvarPos ) );
				logger.log(Level.FINEST, "nodeToImport: " + XmlDomExtended.marshal(nodeToImport));

				Node importedNode = wobjXmlDOMResp.getDocument().getChildNodes().item( 0 ).getOwnerDocument().importNode( nodeToImport , true);
				logger.log(Level.FINEST, "importedNode: " + XmlDomExtended.marshal(importedNode));
				wobjXmlDOMResp.getDocument().getChildNodes().item( 0 ).appendChild( importedNode );				

				//FIN AGREGADO PARA ARREGLAR ERROR EN MSG 1428
				
				/* COMENTADO POR ERROR DETECTADO EN MSG 1428
				
				wobjXMLRequestTemp = new XmlDomExtended();
				wobjXMLRequestTemp.loadXML( pobjXMLRequest.toString() ); //Saqué getDocument().getDocumentElement().
				wobjXmlDOMResp.getDocument().getChildNodes().item( 0 ).appendChild( ModGeneral.GetRequestRetornado( wobjXMLRequestTemp, XmlDomExtended.Node_selectSingleNode( pobjNodoDefiniciones, "//ENTRADA" ), Strings.mid( pstrParseString, 1, pvarPos ) ) );
				wobjXMLRequestTemp = (XmlDomExtended) null;
				
				FIN COMENTARIO */
			}
			//
			// ***********************************************************************************
			//Aplico el o los XSL en caso que se solicite y efectivamente existan para aplicar
			// ***********************************************************************************
			wobjXslDOMResp = new XmlDomExtended();
			for( int nwobjNodoXSL = 0; nwobjNodoXSL < pobjXMLRequest.selectNodes( "//AplicarXSL" ).getLength(); nwobjNodoXSL++ ) {
				wobjNodoXSL = pobjXMLRequest.selectNodes( "//AplicarXSL" ).item( nwobjNodoXSL );
				wvarXSLFile = XmlDomExtended.getText( wobjNodoXSL );
				String xslFilename = Constants.ROOT_DEF_DIR + File.separator + mcteSubDirName + File.separator + wvarXSLFile;
				if (Thread.currentThread().getContextClassLoader().getResourceAsStream(xslFilename) != null) { 
					wobjXslDOMResp.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(Constants.ROOT_DEF_DIR + File.separator + mcteSubDirName + File.separator + wvarXSLFile));
					wvarNuevoXML = wobjXmlDOMResp.transformNode( wobjXslDOMResp ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" ); 
					if( ! (wobjXmlDOMResp.loadXML( wvarNuevoXML )) )
					{
						//Si no pudo levantar el XML luego de transformado, retorno el resultado de esa transformacion sin importar las que sigan despues
						ParseoMensaje = wvarNuevoXML;
						//unsup GoTo ClearObjects
						return ParseoMensaje;
					}
				} else {
					throw new Exception("No se pudo cargar el XSL:" + xslFilename);
				}
			}
		}
		ParseoMensaje = XmlDomExtended.marshal(wobjXmlDOMResp.getDocument().getDocumentElement());
		return ParseoMensaje;
	}

	/**
	 * Método que necesita ser implementado por las clases que lo heredan.
	 * @param pvarstrToParse
	 * @param pobjXMLDefinicion
	 * @param pvarCantidadRegistros
	 * @return
	 * @throws Exception
	 */
	protected abstract org.w3c.dom.Node GetNodoParseado( String pvarstrToParse, org.w3c.dom.Node pobjXMLDefinicion, int pvarCantidadRegistros ) throws Exception;

	private String GetVectorDatos( XmlDomExtended pobjNodo, String pVarPathVector, org.w3c.dom.NodeList pobjSubNodosSolicitados, int pVarCantElementos ) throws Exception
	{
		String GetVectorDatos = "";
		int wvarCounterRelleno = 0;
		int wvarActualCounter = 0;
		int wvarActualNodoSolicitado = 0;
		String wvarNodoNombre = "";
		int wvarNodoTipoDato = 0;
		int wvarNodoLargoDato = 0;
		int wvarNodoCantDecimales = 0;
		org.w3c.dom.NodeList wobjSelectedNodos = null;
		org.w3c.dom.Element Nodo = null;

		wvarActualCounter = 1;
		if( ! (pobjNodo == (org.w3c.dom.Node) null) )
		{
			wobjSelectedNodos = pobjNodo.selectNodes( pVarPathVector );

			for( wvarActualCounter = 0; wvarActualCounter <= (wobjSelectedNodos.getLength() - 1); wvarActualCounter++ )
			{
				//Recorro todos los nodos que formaran el vector
				for( wvarActualNodoSolicitado = 1; wvarActualNodoSolicitado <= pobjSubNodosSolicitados.getLength(); wvarActualNodoSolicitado++ )
				{
					//Recorro todos los nodos que se solicitan para cada uno
					Nodo = (org.w3c.dom.Element) pobjSubNodosSolicitados.item( wvarActualNodoSolicitado - 1 );
					if( Nodo.getChildNodes().getLength() == 0 )
					{
						//Es un Dato simple
						GetVectorDatos = GetVectorDatos + GetDatoFormateado(wobjSelectedNodos.item( wvarActualCounter ),"./" + XmlDomExtended.getText( Nodo.getAttributes().getNamedItem( "Nombre") ) ,XmlDomExtended.getText( Nodo.getAttributes().getNamedItem( "TipoDato") ) ,Nodo.getAttributes().getNamedItem( "Enteros") ,Nodo.getAttributes().getNamedItem( "Decimales") ,Nodo.getAttributes().getNamedItem( "Obligatorio") ,null)  ;
					}
				}
				if( wvarActualCounter == (pVarCantElementos - 1) )
				{
					//unsup GoTo ClearObjects (RESUELTO)
					return GetVectorDatos;
				}
			}
		}
		for( wvarCounterRelleno = wvarActualCounter; wvarCounterRelleno <= (pVarCantElementos - 1); wvarCounterRelleno++ )
		{
			for( wvarActualNodoSolicitado = 1; wvarActualNodoSolicitado <= pobjSubNodosSolicitados.getLength(); wvarActualNodoSolicitado++ )
			{
				Nodo = (org.w3c.dom.Element) pobjSubNodosSolicitados.item( wvarActualNodoSolicitado - 1 );
				if( Nodo.getChildNodes().getLength() == 0 )
				{
					//Es un Dato simple
					GetVectorDatos = GetVectorDatos + GetDatoFormateado(null,"//" + XmlDomExtended.getText( Nodo.getAttributes().getNamedItem( "Nombre") ) ,XmlDomExtended.getText( Nodo.getAttributes().getNamedItem( "TipoDato") ) ,Nodo.getAttributes().getNamedItem( "Enteros") ,Nodo.getAttributes().getNamedItem( "Decimales") ,null,null)  ;
				}
			}
		}
		return GetVectorDatos;
	}

	protected String GetDatoFormateado( org.w3c.dom.Node pobjXMLContenedor, String pvarPathDato, String pvarTipoDato, org.w3c.dom.Node pobjLongitud, org.w3c.dom.Node pobjDecimales, org.w3c.dom.Node pobjDefault, org.w3c.dom.Node pobjObligatorio ) throws Exception
	{
		String GetDatoFormateado = "";
		org.w3c.dom.Node wobjNodoValor = null;
		String wvarDatoValue = "";
		int wvarCounter = 0;
		double wvarCampoNumerico = 0.0;
		String wvarErrorValidacion = "";
		String[] Arr = null;

		if( ! (pobjXMLContenedor == (org.w3c.dom.Node) null) )
		{
			//
			wvarErrorValidacion = getModGeneral().GetErrorInformacionDato( pobjXMLContenedor, pvarPathDato, pvarTipoDato, pobjLongitud, pobjDecimales, pobjDefault, pobjObligatorio );
			if( ! (wvarErrorValidacion.equals( "" )) )
			{
				mvarCancelacionManual = true;
				//FIXME formato del mensaje
				throw new ComponentExecutionException("-1 -- " + mcteClassName + mvarConsultaRealizada +  wvarErrorValidacion );
			}
			wobjNodoValor = XmlDomExtended.Node_selectSingleNode(pobjXMLContenedor, "./" + Strings.mid( pvarPathDato, 3 ));
			if( wobjNodoValor == (org.w3c.dom.Node) null )
			{
				if( ! (pobjDefault == (org.w3c.dom.Node) null) )
				{
					wvarDatoValue = XmlDomExtended.getText( pobjDefault );
				}
			}
			else
			{
				wvarDatoValue = XmlDomExtended.getText( wobjNodoValor );
			}
		}
		//

		if( pvarTipoDato.equals( "TEXTOORIGINAL" ) )
		{
			//Dato del Tipo String que mantiene su valor sin TRIM en origen.
			GetDatoFormateado = StringUtils.left( wvarDatoValue + Strings.space( (int)Math.rint( VBFixesUtil.val( XmlDomExtended.getText( pobjLongitud ) ) ) ), (int)Math.rint( VBFixesUtil.val( XmlDomExtended.getText( pobjLongitud ) ) ) );
		}
		else if( pvarTipoDato.equals( "TEXTO" ) )
		{
			//Dato del Tipo String
			
			//pObjLongitud es de la forma Enteros="10"
			// pLongitudStr: 10
			String pLongitudStr = XmlDomExtended.getText( pobjLongitud );
			
			// pLongitudDouble:2.0 y a veces pLongitudDouble:10.0
			double pLongitudDouble = VBFixesUtil.val( pLongitudStr );
			
			int pLongitudInt = (int)Math.rint( pLongitudDouble );
			String fill = Strings.space( pLongitudInt );
			
			String lengthString = XmlDomExtended.getText( pobjLongitud );
			double lengthDouble = VBFixesUtil.val( lengthString );
			int length = (int)Math.rint( lengthDouble );
			
			GetDatoFormateado = StringUtils.left( wvarDatoValue + fill, length);
//			if (pvarPathDato.contains("USUARCOD") ) {
//				logger.log(Level.INFO, "USUARCOD: GetDatoFormateado=[" + GetDatoFormateado + "], pobjLongitud: [" + pobjLongitud + "], pLongitudStr: ["  + pLongitudStr + 
//						"], pLongitudDouble:" + pLongitudDouble + ", pLongitudInt: " + pLongitudInt +
//						", fill: " + fill + ", lengthString: [" + lengthString + "], lengthDouble: " + lengthDouble + ", length:" + length);
//			}
		}
		else if( pvarTipoDato.equals( "ENTERO" ) )
		{
			//Dato del Tipo Entero
			GetDatoFormateado = getModGeneral().CompleteZero( wvarDatoValue, (int)Math.rint( VBFixesUtil.val( XmlDomExtended.getText( pobjLongitud ) ) ) );
		}
		else if( pvarTipoDato.equals( "DECIMAL" ) )
		{
			//Dato del Tipo "Con Decimales"
			//isNumeric ver http://msdn.microsoft.com/en-us/library/6cd3f6w1(v=vs.80).aspx
			boolean isNumeric = false;
			try {
				Double.parseDouble(wvarDatoValue);
				isNumeric = true;
			} catch (NumberFormatException e) {
			}
			if(  isNumeric)
			{
				wvarCampoNumerico = Obj.toDouble( Strings.replace( wvarDatoValue, ",", "." ) );
			}
			for( wvarCounter = 1; wvarCounter <= Obj.toInt( XmlDomExtended.getText( pobjDecimales ) ); wvarCounter++ )
			{
				wvarCampoNumerico = wvarCampoNumerico * 10;
			}
			//El numero se formatea SIN separador decimal.
			DecimalFormat formatter = new DecimalFormat("#");
			String output = formatter.format(wvarCampoNumerico);
			// TODO Lo de abajo se podría reemplazar por una version más nativa de Java, usando un DecimalFormat con patron #### con length(patron)=longitud+decimales
			GetDatoFormateado = getModGeneral().CompleteZero( String.valueOf( output ), (int)Math.rint( VBFixesUtil.val( XmlDomExtended.getText( pobjLongitud ) ) + VBFixesUtil.val( XmlDomExtended.getText( pobjDecimales ) ) ) );
		}
		else if( pvarTipoDato.equals( "FECHA" ) )
		{
			//Dato del Tipo "Fecha" Debe venir del tipo dd/mm/yyyy devuelve YYYYMMDD
			if( wvarDatoValue.matches( "\\d+/\\d+/\\d\\d\\d\\d" ) )
			{
				Arr = Strings.split( wvarDatoValue, "/", -1 );
				GetDatoFormateado = Arr[2] + getModGeneral().CompleteZero( Arr[1], 2 ) + getModGeneral().CompleteZero( Arr[0], 2 );
			}
			else
			{
				GetDatoFormateado = "00000000";
			}
		}
		else if( pvarTipoDato.equals( "TEXTOIZQUIERDA" ) )
		{
			//Dato del Tipo String alineado a la izquierda
			GetDatoFormateado = Strings.right( Strings.space( (int)Math.rint( VBFixesUtil.val( XmlDomExtended.getText( pobjLongitud ) ) ) ) + wvarDatoValue, (int)Math.rint( VBFixesUtil.val( XmlDomExtended.getText( pobjLongitud ) ) ) );
		}
		//
		return GetDatoFormateado;
	}

}
