package com.qbe.connector.mq;

public class MQProxyTimeoutException extends MQProxyException {

	public MQProxyTimeoutException() {
	}

	public MQProxyTimeoutException(String message) {
		super(message);
	}

	public MQProxyTimeoutException(Throwable cause) {
		super(cause);
	}

	public MQProxyTimeoutException(String message, Throwable cause) {
		super(message, cause);
	}

}
