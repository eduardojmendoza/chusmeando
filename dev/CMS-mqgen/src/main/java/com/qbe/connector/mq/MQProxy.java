package com.qbe.connector.mq;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.qbe.vbcompat.string.StringHolder;

/**
 * Un MQProxy permite enviar mensajes a distintos destinos via MQ usando un MQConnector, y respeta la interface esperada por los componentes migrados en cuanto
 * a parámetros y códigos de respuesta.
 * 
 * Via Spring se configuran distintas instancias que son accedidas por los clientes usando el método MQProxy getInstance(String).
 * Un bean se puede identificar por distintos nombres, por ejemplo usando los nombres de los viejos archivos de configuración de COM+
 * 
 * Cada bean se instancia con un MQConnector. Las propiedades de la conexión como timeouts, colas de destino y demás se configuran en el MQConnector,
 * esta clase es un proxy con una interface específica
 * 
 * @author ramiro
 *
 */
public class MQProxy {

	protected static Logger logger = Logger.getLogger(MQProxy.class.getName());

	/**
	 * Bean ID del que conecta a AIS
	 */
	public static final String AISPROXY = "aisproxy";
	
	/**
	 * Bean ID del que conecta a MW
	 */
	public static final String MWPROXY = "mwproxy";

	/**
	 * Bean ID del que conecta a ecoupons ( son colas distintas a las de AIS MQ )
	 */
	public static final String ECOUPONS_PROXY = "cuponesproxy";


	/**
	 * Context de Spring usado para instanciar el conector mq
	 */
	private static ApplicationContext mqcontext;
	
	/**
	 * Conector por el cual se envian los mensajes
	 */
	private MQConnector connector;
	
	/**
	 * Nombre del archivo de beans que especifica el conector que va a MQ
	 */
	public static String MQBEANS_DEFINITION_FILENAME = "beansmq.xml";


	/**
	 * Retorna la instancia identificada por un nombre.
	 * El nombre tiene que haber sido configurado en el archivo de config de Spring
	 * 
	 * @param name
	 * @return
	 */
	public static MQProxy getInstance(String name) {
		return (MQProxy)getMQContext().getBean(name);
	}

	/**
	 * Solamente se crean via Spring
	 */
	private MQProxy() {
	}
	
	private static ApplicationContext getMQContext() {
		if ( mqcontext == null) {
			mqcontext = new ClassPathXmlApplicationContext(MQBEANS_DEFINITION_FILENAME);
		}
		return mqcontext;
	}

	/**
	 * Ejecuta el request especificado vía el conector MQ.
	 * No especifica ningún timeout, así que usa el que esté configurado en el conector.
	 * Si ocurre alguna Exception la loguea y responde con un código de error.
	 * Por esto, es apropiado para ser usado desde el código migrado que no maneja Exceptions, para otros casos
	 * ver como alternativa los executePrim
	 * 
	 * @param wvarRequest	mensaje a enviar
	 * @param wvarResponse	Almacena la respuesta recibida
	 * @return 0 si se ejecuta correctamente, 1 si hay algún error "de sistema", 3 si hay un timeout
	 * 
	 */
	public int execute(String wvarRequest, StringHolder wvarResponse) {
		try {
			String executeResult = connector.sendMessage(wvarRequest);
			wvarResponse.set(executeResult);
			return 0;

		} catch ( MQConnectorTimeoutException te) {
			logger.log(Level.SEVERE, "Timeout al enviar mensaje MQ",te);
			return 3;
		} catch (MQConnectorException e) {
			String msg = "Exception [ Msg:"+ e.getMessage() + ", causa:" + e.getCause() + "] al enviar mensaje: [" + wvarRequest + "]";
			logger.log(Level.SEVERE, msg,e);
			return 1;
		}
	}
	
	/**
	 * Ejecuta el request especificado vía el conector MQ; a diferencia del execute si hay una Exception
	 * este no devuelve códigos de error si no que lanza Exceptions 
	 * 
	 * @param wvarRequest	mensaje a enviar
	 * @param wvarResponse	Almacena la respuesta recibida
	 * @throws MQProxyException que encapsula las exceptions del conector
	 * @return 0 si se ejecuta correctamente, una exception en otro caso
	 * 
	 */
	public int executePrim(String wvarRequest, StringHolder wvarResponse) throws MQProxyException {
			String executeResult;
			try {
				executeResult = connector.sendMessage(wvarRequest);
				wvarResponse.set(executeResult);
				return 0;
			} catch (MQConnectorTimeoutException e) {
				throw new MQProxyTimeoutException(e);
			} catch (MQConnectorException e) {
				throw new MQProxyException(e);
			}
	}
	
	/**
	 * Ejecuta el request esperando hasta un máximo de waitInterval para obtener la respuesta
	 * 
	 * @param wvarRequest
	 * @param wvarResponse
	 * @param waitInterval
	 * @return 0 si se ejecuta correctamente, una exception en otro caso
	 * @throws MQProxyException
	 */
	public int executePrim(String wvarRequest, StringHolder wvarResponse, int waitInterval) throws MQProxyException {
		String executeResult;
		try {
			executeResult = connector.sendMessage(wvarRequest, waitInterval);
			wvarResponse.set(executeResult);
			return 0;
		} catch (MQConnectorTimeoutException e) {
			throw new MQProxyTimeoutException(e);
		} catch (MQConnectorException e) {
			throw new MQProxyException(e);
		}
	}
	
	public MQConnector getConnector() {
		return connector;
	}

	public void setConnector(MQConnector connector) {
		this.connector = connector;
	}

}
