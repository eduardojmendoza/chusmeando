package com.qbe.connector.mq;

public class MQProxyException extends Exception {

	public MQProxyException() {
	}

	public MQProxyException(String message) {
		super(message);
	}

	public MQProxyException(Throwable cause) {
		super(cause);
	}

	public MQProxyException(String message, Throwable cause) {
		super(message, cause);
	}

}
