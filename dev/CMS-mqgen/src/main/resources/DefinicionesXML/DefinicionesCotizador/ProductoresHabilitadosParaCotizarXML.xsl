<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<ROOT>
			<xsl:for-each select="//PRODUCTOR">
				<xsl:sort select="./NOMBRE"/>
				<AGENTE>
					<xsl:attribute name="AGENTCLA"><xsl:value-of select="AGENTCLA"/></xsl:attribute>
					<xsl:attribute name="AGENTCOD"><xsl:value-of select="AGENTCOD"/></xsl:attribute>					
				</AGENTE>
			</xsl:for-each>
		</ROOT>			
	</xsl:template>
</xsl:stylesheet>
