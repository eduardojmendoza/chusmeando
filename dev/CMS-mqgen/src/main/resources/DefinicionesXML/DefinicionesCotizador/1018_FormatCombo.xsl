<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />
	<xsl:template match="/">
		<options>
			<xsl:for-each select="//SUCU">
				<option>
					<xsl:attribute name="value"><xsl:value-of select="CODISUCU" /></xsl:attribute>
					<xsl:value-of select="CODISUCU" /> - <xsl:value-of select="DESCSUCU" />
				</option>
			</xsl:for-each>
		</options>
	</xsl:template>
</xsl:stylesheet>
