<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<xsl:for-each select="//PRODUCTO">
			<xsl:sort select="./RAMOPDES"/>
			<option>
				<xsl:attribute name="value"><xsl:value-of select="RAMOPCOD"/></xsl:attribute>
				<xsl:attribute name="ramopdab"><xsl:value-of select="RAMOPDAB"/></xsl:attribute>
				<xsl:attribute name="monencod"><xsl:value-of select="substring(MONENCOD,4,1)"/></xsl:attribute>
				<xsl:value-of select="RAMOPDES"/>
			</option>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>