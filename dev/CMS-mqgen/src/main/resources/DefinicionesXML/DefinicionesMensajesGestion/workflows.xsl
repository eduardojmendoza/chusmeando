<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:output method="xml"/>

<xsl:template match="/">

	<!--Determinar el tipo de gestión-->
	<xsl:element name="workflow">
	
		<xsl:choose>
		
			<xsl:when test="//CAMPOS/TIPO='0' and //ESTADO='OK'">
				<xsl:element name="NODATO"/>
			</xsl:when>
			
			<xsl:when test="//ESTADO='JU'">
				<xsl:element name="ENJUICIO"/>
			</xsl:when>			
			
			<xsl:when test="//CAMPOS/TIPO='1'">
				<xsl:apply-templates select="//RSVS"/>
			</xsl:when>
	
			<xsl:when test="//CAMPOS/TIPO='2'">
				<xsl:apply-templates select="//ROBTOT"/>
			</xsl:when>
	
			<xsl:when test="//CAMPOS/TIPO='3'">
				<xsl:apply-templates select="//DANIOS"/>
			</xsl:when>
							
		</xsl:choose>
	
	</xsl:element>
	
</xsl:template>

<xsl:template match="//RSVS">

	<xsl:call-template name="etapa">
		<xsl:with-param name="descripcion" select="'Alta'"/>
		<xsl:with-param name="eventos">
			Fecha de siniestros:<xsl:value-of select="//FECHASIN"/>##
			Fecha de denuncia:<xsl:value-of select="//FECHADEN"/>##
			Fecha de alta:<xsl:value-of select="//FECHAALT"/>##
		</xsl:with-param>
	</xsl:call-template>

	<xsl:call-template name="etapa">
		<xsl:with-param name="descripcion" select="'Derivaciones'"/>
		<xsl:with-param name="eventos">
			Fecha de derivación:<xsl:value-of select="//RSVS//FECHADER"/>##
			Estudio:<xsl:value-of select="//RSVS//GESTINOM"/>##
			Responsable del sector:<xsl:value-of select="//RSVS//RESPONSA"/>##
		</xsl:with-param>
	</xsl:call-template>

	<xsl:call-template name="etapa">
		<xsl:with-param name="descripcion" select="'Preinformes'"/>
		<xsl:with-param name="eventos">
			<xsl:for-each select="//RSVS//PREINFO">
				Preinforme:<xsl:value-of select="."/>##
			</xsl:for-each>
		</xsl:with-param>
	</xsl:call-template>

	<xsl:call-template name="etapa">
		<xsl:with-param name="descripcion" select="'Pagos parciales'"/>
		<xsl:with-param name="eventos">
			<xsl:for-each select="//RSVS/ADELANTOS/DATOS">
				Fecha:<xsl:value-of select="./FECHAADE"/>#Importe:<xsl:value-of select="./ADELANTO"/>##
			</xsl:for-each>
		</xsl:with-param>
	</xsl:call-template>
	
	<xsl:call-template name="etapa">
		<xsl:with-param name="descripcion" select="'Informe final'"/>
		<xsl:with-param name="eventos">
			Informe:<xsl:value-of select="//RSVS//INFFINAL"/>##
			<xsl:for-each select="//RSVS/IMPFINALES/DATOS">
				Fecha de pago:<xsl:value-of select="./FECHAPAG"/>#
				Importe:<xsl:value-of select="./IMPAGADO"/>##
			</xsl:for-each>
		</xsl:with-param>		
	</xsl:call-template>

	<xsl:call-template name="etapa">
		<xsl:with-param name="descripcion" select="'Resultado'"/>
		<xsl:with-param name="eventos">
			Estado:<xsl:value-of select="//RSVS//ESTADOGE"/>##
		</xsl:with-param>
	</xsl:call-template>
			
</xsl:template>

<xsl:template match="//ROBTOT">

	<xsl:call-template name="etapa">
		<xsl:with-param name="descripcion" select="'Alta'"/>
		<xsl:with-param name="eventos">
			Fecha de siniestros:<xsl:value-of select="//FECHASIN"/>##
			Fecha de denuncia:<xsl:value-of select="//FECHADEN"/>##
			Fecha de alta:<xsl:value-of select="//FECHAALT"/>##
		</xsl:with-param>
	</xsl:call-template>

	<xsl:call-template name="etapa">
		<xsl:with-param name="descripcion" select="'Doc. Completa'"/>
		<xsl:with-param name="eventos">
		Doc. pendiente:<xsl:value-of select="//ROBTOT/DOCUPEND"/>##
		</xsl:with-param>
	</xsl:call-template>
	
	<xsl:call-template name="etapa">
			<xsl:with-param name="descripcion" select="'Análisis'"/>
			<xsl:with-param name="eventos">
			Fecha:<xsl:value-of select="//ROBTOT/FECHADOC"/>##
		</xsl:with-param>
	</xsl:call-template>

	<xsl:call-template name="etapa">
		<xsl:with-param name="descripcion" select="'Resultado'"/>
		<xsl:with-param name="eventos">
			<xsl:for-each select="//ROBTOT/ADELANTOS/DATOS">
				Fecha:<xsl:value-of select="./FECHAPAG"/>#Importe:<xsl:value-of select="./PAGOLEYE"/>##
			</xsl:for-each>
				Estado:<xsl:value-of select="//ROBTOT/ESTADOWK"/>##
		</xsl:with-param>
	</xsl:call-template>

</xsl:template>

<xsl:template match="//DANIOS">

	<xsl:call-template name="etapa">
		<xsl:with-param name="descripcion" select="'Alta'"/>
		<xsl:with-param name="eventos">
			Fecha de siniestros:<xsl:value-of select="//FECHASIN"/>##
			Fecha de denuncia:<xsl:value-of select="//FECHADEN"/>##
			Fecha de alta:<xsl:value-of select="//FECHAALT"/>##
		</xsl:with-param>
	</xsl:call-template>

	<!--xsl:call-template name="etapa">
		<xsl:with-param name="descripcion" select="'Inspecciones'"/>
		<xsl:with-param name="eventos">
		Fecha pedido Insp.:<xsl:value-of select="//DANIOS/FECHPEDINSP"/>##
		Fecha resultado Insp.:<xsl:value-of select="//DANIOS/FECHRESINSP"/>##
		</xsl:with-param>
	</xsl:call-template-->

	<xsl:call-template name="etapa">
		<xsl:with-param name="descripcion" select="'Doc. Completa'"/>
		<xsl:with-param name="eventos">
			Estado:<xsl:value-of select="//DANIOS/DOCUPEND"/>##
		</xsl:with-param>
	</xsl:call-template>
	
	<xsl:call-template name="etapa">
		<xsl:with-param name="descripcion" select="'Análisis'"/>
			<xsl:with-param name="eventos">
			Fecha:<xsl:value-of select="//DANIOS/FECHADOC"/>##
			</xsl:with-param>
	</xsl:call-template>

	<xsl:call-template name="etapa">
		<xsl:with-param name="descripcion" select="'Resultado'"/>
		<xsl:with-param name="eventos">
			<xsl:for-each select="//DANIOS/ADELANTOS/DATOS">
				Fecha:<xsl:value-of select="./FECHAPAG"/>#Importe:<xsl:value-of select="./PAGOLEYE"/>##
			</xsl:for-each>
				Estado:<xsl:value-of select="//DANIOS/ESTADOWK"/>##
		</xsl:with-param>
	</xsl:call-template>

</xsl:template>

<xsl:template name="etapa">

	<xsl:param name="descripcion"/>	
	<xsl:param name="eventos"/>
		
	<xsl:element name="etapa">

		<xsl:element name="descripcion"><xsl:value-of select="$descripcion"/></xsl:element>
		
		<xsl:if test="string-length($eventos) != 0">
				
			<xsl:element name="eventos">
							
				<xsl:call-template name="evento">
					<xsl:with-param name="cadena" select="normalize-space($eventos)"/>
				</xsl:call-template>
				
			</xsl:element>
		
		</xsl:if>
		
	</xsl:element>
	
</xsl:template>

<xsl:template name="evento">

	<xsl:param name="cadena"/>
	
	<xsl:variable name="evento" select="substring-before($cadena, '##')"/>
			
	<xsl:element name="evento">
			
		<xsl:call-template name="propiedad">
			<xsl:with-param name="cadena" select="$evento"/>
		</xsl:call-template>
			
	</xsl:element>
	
	<xsl:variable name="saldo" select="normalize-space(substring-after($cadena, '##'))"/>
		
	<xsl:if test="string-length($saldo)!=0">
	
			<xsl:call-template name="evento">
				<xsl:with-param name="cadena" select="$saldo"/>
			</xsl:call-template>
		
	</xsl:if>
	
</xsl:template>

<xsl:template name="propiedad">

	<xsl:param name="cadena"/>
	
	<xsl:variable name="subcadena">
	
		<xsl:choose>
			<xsl:when test="contains($cadena, '#')"><xsl:value-of select="substring-before($cadena, '#')"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$cadena"/></xsl:otherwise>
		</xsl:choose>
		
	</xsl:variable>
	
	<xsl:element name="propiedad">
		
		<xsl:element name="descripcion"	><xsl:value-of select="substring-before($subcadena, ':')"/></xsl:element>

		<xsl:element name="valor"><xsl:value-of select="substring-after($subcadena,  ':')"/></xsl:element>
					
	</xsl:element>

	<xsl:if test="contains($cadena, '#')">

		<xsl:call-template name="propiedad">
			<xsl:with-param name="cadena" select="substring-after($cadena, '#')"/>
		</xsl:call-template>
	
	</xsl:if>
	
</xsl:template>

</xsl:stylesheet>
