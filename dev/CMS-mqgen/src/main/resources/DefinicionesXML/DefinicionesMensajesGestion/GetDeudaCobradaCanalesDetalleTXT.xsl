<!DOCTYPE xsl:stylesheet [
    <!ENTITY nl "<xsl:text>
</xsl:text>">
	<!ENTITY tab "<xsl:text>&#x9;</xsl:text>">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format name="european" decimal-separator="."/>

<xsl:template match='/'>
Apellido / Razón Social&tab;&tab;Nro. Póliza&tab;&tab;Endoso&tab;&tab;Estado de Póliza&tab;&tab;Nro. de Recibo&tab;&tab;Moneda&tab;&tab;Importe&tab;&tab;Fecha de Cobro&tab;&tab;<!--Tipo de Cuenta&tab;&tab;-->Canal&tab;&tab;Judicial&nl;
<xsl:for-each select="//CANAL">
	<xsl:sort order="ascending" select="FECSITUE" data-type="text" />
	<xsl:value-of select="CLIENDES"/>
	&tab;&tab;
	<xsl:value-of select="RAMOPCOD"/><xsl:text>-</xsl:text><xsl:value-of select="POLIZANN"/><xsl:text>-</xsl:text><xsl:value-of select="POLIZSEC"/><xsl:if test="RAMO='M'">/<xsl:value-of select="CERTIPOL"/><xsl:value-of select="CERTIANN"/><xsl:value-of select="CERTISEC"/></xsl:if>
	&tab;&tab;
	<xsl:value-of select="format-number(SUPLENUM,'0')"/>
	&tab;&tab;
	<xsl:value-of select="SITUCPOL"/>
	&tab;&tab;
	<xsl:value-of select="RECIBANN"/><xsl:value-of select="RECIBTIP"/><xsl:value-of select="RECIBSEC"/>
	&tab;&tab;
	<xsl:value-of select="MONENDES"/>
	&tab;&tab;
	<xsl:value-of select="format-number(IMPORTE,'#.00')"/>
	&tab;&tab;
	<xsl:value-of select="FECSITUE"/>
	<!--&tab;&tab;<xsl:value-of select="COBRODESC"/>-->
	&tab;&tab;
	<xsl:value-of select="COBRODABC"/>
	&tab;&tab;
	<xsl:value-of select="ORDENJUDICIAL"/>
	&nl;
</xsl:for-each>
</xsl:template>

</xsl:stylesheet>
