<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2007. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: 1659_Mediosdepagoxcia.xsl

Fecha de Creación: 03/04/2008

Desarrollador: Fernando Osores

Descripción: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos 
			1659_Mediosdepagoxcia.xml
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html" encoding="iso-8859-1"/>
	<xsl:template match="/">
				<xsl:for-each select="//pago">
					<xsl:sort select="COBRODES"/>
					<option>
						<xsl:attribute name="value"><xsl:value-of select="COBROTIP"/></xsl:attribute>
						<xsl:value-of select="COBRODES"/>
					</option>
				</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
