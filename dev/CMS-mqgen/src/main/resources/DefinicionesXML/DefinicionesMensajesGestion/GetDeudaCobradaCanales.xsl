<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<EST>
		<xsl:for-each select="//CANAL">
			<xsl:sort select="./COBRODES"/>
			<xsl:variable name="nombreelemento">
				<xsl:value-of select="COBRODES"/>
			</xsl:variable>
			<xsl:element name="{$nombreelemento}">
				<xsl:attribute name="TOTPESO"><xsl:value-of select="IMPORTEP"/></xsl:attribute>
				<xsl:attribute name="TOTDOLAR"><xsl:value-of select="IMPORTED"/></xsl:attribute>
			</xsl:element>
		</xsl:for-each>
		</EST>
	</xsl:template>
</xsl:stylesheet>
