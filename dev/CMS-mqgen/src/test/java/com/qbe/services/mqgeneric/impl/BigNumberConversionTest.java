package com.qbe.services.mqgeneric.impl;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;

public class BigNumberConversionTest {

	private static Logger logger = Logger.getLogger(BigNumberConversionTest.class.getName());
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testBug140Original() {
	    Variant wvarLastValue = new Variant();
		wvarLastValue.set("0003547059527");
        wvarLastValue.set( Strings.trim( wvarLastValue.toString() ) ); //NOTA esto pasa 000000005000000 como 50000.0
        String newValue = String.valueOf( VB.val( wvarLastValue.toString() ) / Math.pow( 10, 2  ) );
        //Si el número no tiene decimales, VB lo formatea SIN .0 al final, mientras que en Java como String.valueOf recibe un double lo escribe con .0
        if ( newValue.endsWith(".0")) newValue = newValue.substring(0, newValue.length()-2);
        
        logger.fine("[LBAW_MQMensajeGestion] wvarLastValue: [" + wvarLastValue + "]");
        logger.fine("[LBAW_MQMensajeGestion] wvarLastValue type: [" + wvarLastValue.getTypeName() + "]");
        logger.fine("[LBAW_MQMensajeGestion] wvarLastValue decimales: [" + 2 + "]");
        logger.fine("[LBAW_MQMensajeGestion] newValue: [" + newValue + "]");
        // La conversión original lo genera con notación exponencial
        assertEquals("3.547059527E7", newValue);
	}
	
	@Test
	public void testBug140Fixed() throws Exception {
        // Nueva conversión
	    Variant wvarLastValue = new Variant();
		wvarLastValue.set("0003547059527");
		int decimales = 2;

		BigDecimal bigD = convertToBigDecimal(wvarLastValue, decimales);
		String newValue = formatBigDecimaltoQBELocale(bigD);

        logger.fine("[LBAW_MQMensajeGestion] nuevaConversión: newValue: [" + newValue + "]");
        assertEquals("35470595.27", newValue);

	}

	@Test
	public void testBug140Entero() throws Exception {
        // Nueva conversión
	    Variant wvarLastValue = new Variant();
		wvarLastValue.set("0003547059527");
		int decimales = 0;

		BigDecimal bigD = convertToBigDecimal(wvarLastValue, decimales);
		String newValue = formatBigDecimaltoQBELocale(bigD);

        logger.fine("[LBAW_MQMensajeGestion] nuevaConversión: newValue: [" + newValue + "]");
        assertEquals("3547059527", newValue);

	}

	@Test
	public void testBug140_0() throws Exception {
        // Nueva conversión
	    Variant wvarLastValue = new Variant();
		wvarLastValue.set("000000000000000000000000000000000000");
		int decimales = 0;

		BigDecimal bigD = convertToBigDecimal(wvarLastValue, decimales);
		String newValue = formatBigDecimaltoQBELocale(bigD);

        logger.fine("[LBAW_MQMensajeGestion] nuevaConversión: newValue: [" + newValue + "]");
        assertEquals("0", newValue);
	}
	
	@Test
	public void testBug140_negativo() throws Exception {
        // Nueva conversión
	    Variant wvarLastValue = new Variant();
		wvarLastValue.set("-0000000084266");
		int decimales = 0;

		BigDecimal bigD = convertToBigDecimal(wvarLastValue, decimales);
		String newValue = formatBigDecimaltoQBELocale(bigD);

        logger.fine("[LBAW_MQMensajeGestion] nuevaConversión: newValue: [" + newValue + "]");
        assertEquals("-84266", newValue);
	}

	@Test
	public void testBug140_negativoDec() throws Exception {
        // Nueva conversión
	    Variant wvarLastValue = new Variant();
		wvarLastValue.set("-0000000084266");
		int decimales = 2;

		BigDecimal bigD = convertToBigDecimal(wvarLastValue, decimales);
		String newValue = formatBigDecimaltoQBELocale(bigD);

        logger.fine("[LBAW_MQMensajeGestion] nuevaConversión: newValue: [" + newValue + "]");
        assertEquals("-842.66", newValue);
	}
	
	@Test
	public void testBug140_1Dec() throws Exception {
        // Nueva conversión
	    Variant wvarLastValue = new Variant();
		wvarLastValue.set("0000000084266");
		int decimales = 1;

		BigDecimal bigD = convertToBigDecimal(wvarLastValue, decimales);
		String newValue = formatBigDecimaltoQBELocale(bigD);

        logger.fine("[LBAW_MQMensajeGestion] nuevaConversión: newValue: [" + newValue + "]");
        assertEquals("8426.6", newValue);
	}
	
	@Test
	public void testBug140_parseo() throws Exception {
		try {
			verifyValidQBEFormat("0.11");			
		} catch (Exception e) {
			// OK, expected
		}

		verifyValidQBEFormat("-0000000092554");
		verifyValidQBEFormat("000000000057965");
		verifyValidQBEFormat("00000000069800");
}
	
	private String formatBigDecimaltoQBELocale(BigDecimal bigD) {
		String newValue;
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		String pattern = "###.##";
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		// Si lo quiero usar para parsear
//		decimalFormat.setParseBigDecimal(true);
		newValue = decimalFormat.format(bigD);
		return newValue;
	}

	private BigDecimal convertToBigDecimal(Variant wvarLastValue, int decimales) throws Exception {
		verifyValidQBEFormat(wvarLastValue.toString());
		
		// Esta conversión espera un "." como separador de decimales
		// En nuestro caso no afecta porque los Strings que vienen desde AIS son enteros ( sin parte decimal )
		BigDecimal crudo = new BigDecimal(wvarLastValue.toString());
		BigDecimal bigD = crudo.divide(new BigDecimal(Math.pow(10, decimales)));
		return bigD;
	}

	private void verifyValidQBEFormat(String aisNumber) throws Exception {
		Pattern aisNumberPattern = Pattern.compile("-{0,1}\\d+");
		if ( !aisNumberPattern.matcher(aisNumber).matches() ) {
			throw new Exception(aisNumber + " doesn't match");			
		}
		
		
	}

}
