package com.qbe.services.mqgeneric;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Assume;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;

public class CurrentProfileTest {

	private static Logger logger = Logger.getLogger(CurrentProfileTest.class.getName());
	
	@Test
	public void testEnvDev() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		logger.log(Level.FINE,"Ahora estamos en profile \"DEV\"");
	}

	@Test
	public void testEnvTest() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		logger.log(Level.FINE,"Ahora estamos en profile \"TEST\"");
	}
}
