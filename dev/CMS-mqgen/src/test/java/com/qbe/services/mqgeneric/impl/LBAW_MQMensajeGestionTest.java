package com.qbe.services.mqgeneric.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.testutils.TestUtils;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;

public class LBAW_MQMensajeGestionTest  {

	public LBAW_MQMensajeGestionTest() {
		super();
	}

	/**
	 * Testea la ejecución del mensaje, y que no vuelva un código de error. Valida contra la respuesta esperada
	 * 
	 * @param msgCode
	 * @throws Exception 
	 */
	protected void testMessageFullRequest (String msgCode, VBObjectClass comp) throws Exception {
		String responseText = this.testMessageJustExecution(msgCode, comp);
	
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		XMLUnit.setIgnoreWhitespace(true);
		String expectedResponse = TestUtils.loadResponse(msgCode);
		if ( expectedResponse == null || expectedResponse.length()==0) throw new Exception("No se pudo cargar el request");
		DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(expectedResponse, responseText));
		List allDifferences = myDiff.getAllDifferences();
		assertEquals(myDiff.toString(), 0, allDifferences.size());
		//Eliminar los &lt; y &gt;
		XMLAssert.assertXMLEqual("No volvió la respuesta esperada", expectedResponse, responseText);
	
	}
	
	/**
	 * Testea la ejecución del mensaje, y que no vuelva un código de error. No valida contra la respuesta esperada, la devuelve solamente
	 * 
	 * @param msgCode
	 * @throws Exception 
	 */
	protected String testMessageJustExecution (String msgCode, VBObjectClass comp) throws Exception {
		String request = TestUtils.loadRequest(msgCode);
		if ( request == null || request.length()==0 || request.trim().equals("")) throw new Exception("No se pudo cargar el request");
		StringHolder responseHolder = new StringHolder();
		int result = comp.IAction_Execute(request, responseHolder, null);
//		System.out.println("Response: " + responseHolder);
		if ( result == 1) Assert.fail ("Abortó la ejecución, ver el log");
		Assert.assertTrue("Result no es 1 ni 0, es un error",result == 0);
		try {
			TestUtils.parseResultado(responseHolder.getValue());
			TestUtils.parseMensaje(responseHolder.getValue());
		} catch (Exception e){
			assertTrue("No es posible parsear la respuesta", false);
		}
		String responseText = responseHolder.getValue();
		if (( responseText == null) || ( responseText.length()==0)) {
			fail("responseText vacío");
		}
		return responseText;
	}
	
	
	//------------------------------ T E S T ------------------------------------------	
	@Test
	@Ignore //broken
	public void testMSG_1116() throws Exception {
		String msgCode = "1116";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}
	@Test
	public void testMSG_1117() throws Exception {
		String msgCode = "1117";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}
	@Test
	public void testMSG_1118() throws Exception {
		String msgCode = "1118";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}
	@Test
	public void testMSG_1119() throws Exception {
		String msgCode = "1119";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}

	@Test
	public void testMSG_1120() throws Exception {
		String msgCode = "1120";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}
	@Test
	public void testMSG_1213() throws Exception {
		String msgCode = "1213";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}

	@Test
	public void testMSG_1425() throws Exception {
		String msgCode = "1425";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}

	@Test
	public void testMSG_1426() throws Exception {
		String msgCode = "1426";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}

	@Test
	public void testMSG_1427() throws Exception {
		String msgCode = "1427";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}

	@Test
	@Ignore
	//FIXME No encuentra el mensaje en el conector mock
	public void testMSG_1428() throws Exception {
		String msgCode = "1428";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}

	@Test
	public void testMSG_1428_bug128() throws Exception {
		String msgCode = "1428-bug128";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}

	@Test
	public void testMSG_1543() throws Exception {
		String msgCode = "1543";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}

	@Test
	public void testMSG_1605() throws Exception {
		String msgCode = "1605";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}
	@Test
	public void testMSG_1606() throws Exception {
		String msgCode = "1606";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}

	@Test
	public void testMSG_1607() throws Exception {
		String msgCode = "1607";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}

	@Test
	@Ignore
	//FIXME No anda
	public void testMSG_1810() throws Exception {
		String msgCode = "1810";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}
	
	@Test
	public void testMSG_1901() throws Exception {
		String msgCode = "1901";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}

	@Test
	@Ignore
	//FIXME No encuentra el mensaje en el conector mock
	public void testMSG_1425_bug431() throws Exception {
		String msgCode = "1425_bug431";
		testMessageFullRequest(msgCode,new LBAW_MQMensajeGestion());
	}
}
