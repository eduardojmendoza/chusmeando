package com.qbe.services.mqgeneric.connector;

import static org.junit.Assert.*;

import java.net.URL;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.common.CurrentProfile;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

public class MQConnectionConnectorTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Lo único que testea es que no salte una Exception no manejada
	 * @throws Exception
	 */
	@Test
	public void testExecuteDoesntThrowsUnmanagedException() throws Exception {
		if ( !CurrentProfile.getProfileName().equals("test")) {
			return;
		}
		StringHolder sh = new StringHolder();
		MQProxy c = MQProxy.getInstance("aisproxy");
		try {
			c.executePrim("<pepe>",sh, 1);
		} catch (MQProxyException e) {
			//ok, esperada
		}
	}

	@Test
	public void testAIS() throws Exception {
		MQProxy c = MQProxy.getInstance("LBAVirtualMQConfig");
	}

	@Test
	public void testMW() throws Exception {
		MQProxy c = MQProxy.getInstance("LBAVirtualMQConfigMID");
	}
	
	/**
	 * Testea que el archivo de config cargue sin null
	 * 
	 * @throws Exception
	 */
	@Test
	public void testLoad() throws Exception {
		XmlDomExtended xml = getConfig();
		assertNotNull(xml);
	}

	private XmlDomExtended getConfig() throws Exception {
		XmlDomExtended xml = new XmlDomExtended();
		URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("LBAVirtualMQConfig.xml");
		String fileName = fileUrl.getFile();
		xml.load(fileName);
		return xml;
		
	}

}
