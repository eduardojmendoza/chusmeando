<?xml version="1.0" encoding="UTF-8"?>
<!--
	COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
	LIMITED 2009. ALL RIGHTS RESERVED
	
	This software is only to be used for the purpose for which it has been provided.
	No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
	system or translated in any human or computer language in any way or for any other
	purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
	Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
	offence, which can result in heavy fines and payment of substantial damages.
	
	Nombre del Fuente: P_NBWS_ObtenerPreguntas.xsl
	
	Fecha de Creación: 31/07/2009
	
	PPcR: 50055/6010662
	
	Desarrollador: Leonardo Ruiz
	
	Descripción: XSL para obtener preguntas del usr.-
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html" encoding="iso-8859-1"/>
	<xsl:template match="/">
		<xsl:variable name="elegida" select="//ELEGIDA"/>
		<xsl:for-each select="//option">
			<option>
				<xsl:attribute name="value"><xsl:value-of select="ID"/></xsl:attribute>
				<xsl:if test="ID = $elegida">
					<xsl:attribute name="selected">true</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="PREGUNTA"/>
			</option>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
