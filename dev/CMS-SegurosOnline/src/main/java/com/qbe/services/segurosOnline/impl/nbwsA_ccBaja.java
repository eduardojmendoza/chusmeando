package com.qbe.services.segurosOnline.impl;
import java.util.logging.Logger;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Variant;

public class nbwsA_ccBaja  extends BaseOSBClient implements VBObjectClass
{
	
	protected static Logger logger = Logger.getLogger(nbwsA_ccBaja.class.getName());

	static final String mcteClassName = "nbwsA_Transacciones.nbwsA_ccBaja";
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_DOCUMDAT = "//DOCUMDAT";
  static final String mcteParam_MAIL = "//MAIL";
  /**
   * Constantes de XML de definiciones para SQL Generico
   */
  static final String mcteParam_XMLSQLGENBAJA = "P_NBWS_SetEstadoBaja.xml";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_ERROR = 1;

  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error (los números matchean los de la tabla SQL tipoAccesos)
   */
  private String[] mcteMsg_DESCRIPTION = new String[13];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXMLRespuesta1184 = null;
    XmlDomExtended wobjXMLRespuesta1185 = null;
    org.w3c.dom.NodeList wobjXMLProductosAIS_List = null;
    org.w3c.dom.Node wobjXMLProductosAIS_Node = null;
    String wvarMail = "";
    String wvarError = "";
    String wvarRequest = "";
    String wvarResponse;
    String wvarDOCUMTIP = "";
    String wvarDOCUMDAT = "";
    String wvarRAMOPCOD = "";
    String wvarPOLIZANN = "";
    String wvarPOLIZSEC = "";
    String wvarCERTIPOL = "";
    String wvarCERTIANN = "";
    String wvarCERTISEC = "";
    String wvarSWSUSCRI = "";
    String wvarSWCONFIR = "";
    String wvarCLAVE = "";
    String wvarSWCLAVE = "";
    String wvarSWTIPOSUS = "";
    //
    //XML con el request
    //
    //PARAMETROS LLAMADA 1185
    // Dim wvarMail            As String
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializacion de variables
      wvarStep = 10;
      wvarError = String.valueOf( mcteMsg_OK );
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_ERROR] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_ERROR" );
      //
      //Carga del REQUEST
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //Obtiene parametros
      wvarStep = 30;
      wvarStep = 40;
      wvarMail = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MAIL )  );
      wvarDOCUMTIP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  );
      wvarDOCUMDAT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMDAT )  );

      //
      // ***********************************************************************
      // GED 30/09/2011 - ANEXO I: SE AGREGA ENVIO DE SUSCRIPCION AL AIS
      // ***********************************************************************
      wvarRequest = "<Request>" + "<DOCUMTIP>" + wvarDOCUMTIP + "</DOCUMTIP>" + "<DOCUMDAT>" + wvarDOCUMDAT + "</DOCUMDAT>" + "<MAIL>" + wvarMail + "</MAIL>" + "</Request>";
      wvarStep = 50;

      wvarResponse = getOsbConnector().executeRequest(nbwsA_sInicio.ACTION_CODE, wvarRequest);

      wvarStep = 60;


      //Carga las dos respuestas en un XML.
      wobjXMLRespuesta1184 = new XmlDomExtended();
      wobjXMLRespuesta1184.loadXML( wvarResponse.toString() );
      //
      wvarStep = 70;
      //Verifica que no haya pinchado el COM+
      if( wobjXMLRespuesta1184.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null )
      {
        //
        wvarStep = 80;
        //Falló Response 1
        wvarError = String.valueOf( mcteMsg_ERROR );
      }

      wvarStep = 90;


      wvarStep = 110;
      //Levanta en un listado cada producto del cliente
      // SE DAN DE BAJA TODAS LAS POLIZAS QUE ESTAN SUSCRIPTAS A EPOLIZA Y NBWS
      wobjXMLProductosAIS_List = wobjXMLRespuesta1184.selectNodes( "//Response/PRODUCTO[CIAASCOD='0001' and HABILITADO_EPOLIZA='S' and MAS_DE_CINCO_CERT='N']" ) ;

      if( wobjXMLProductosAIS_List.getLength() > 0 )
      {

        wvarStep = 120;
        //Recorre pólizas de clientes.
        wvarRequest = "<Request id=\"1\"  actionCode=\"nbwsA_MQGenericoAIS\" >" + "<DEFINICION>LBA_1185_SusyDesus_epoliza.xml</DEFINICION>" + "<EPOLIZAS>";

        wvarStep = 130;

        for( int nwobjXMLProductosAIS_Node = 0; nwobjXMLProductosAIS_Node < wobjXMLProductosAIS_List.getLength(); nwobjXMLProductosAIS_Node++ )
        {
          wobjXMLProductosAIS_Node = wobjXMLProductosAIS_List.item( nwobjXMLProductosAIS_Node );
          //
          wvarStep = 140;
          wvarRAMOPCOD = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "RAMOPCOD" )  );
          wvarPOLIZANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "POLIZANN" )  );
          wvarPOLIZSEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "POLIZSEC" )  );
          wvarCERTIPOL = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "CERTIPOL" )  );
          wvarCERTIANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "CERTIANN" )  );
          wvarCERTISEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "CERTISEC" )  );
          // "N" PARA LAS BAJAS
          wvarSWSUSCRI = "N";
          wvarCLAVE = "";
          wvarSWCLAVE = "";
          // ????
          wvarSWTIPOSUS = "W";


          wvarRequest = wvarRequest + "<EPOLIZA><CIAASCOD>0001</CIAASCOD>" + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + wvarPOLIZANN + "</POLIZANN>" + "<POLIZSEC>" + wvarPOLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + wvarCERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + wvarCERTIANN + "</CERTIANN>" + "<CERTISEC>" + wvarCERTISEC + "</CERTISEC>" + "<SWSUSCRI>" + wvarSWSUSCRI + "</SWSUSCRI>" + "<MAIL>" + wvarMail + "</MAIL>" + "<CLAVE>" + wvarCLAVE + "</CLAVE>" + "<SW-CLAVE>" + wvarSWCLAVE + "</SW-CLAVE>" + "<SWTIPOSUS>" + wvarSWTIPOSUS + "</SWTIPOSUS>" + "</EPOLIZA>";
        }

        wvarRequest = wvarRequest + "</EPOLIZAS></Request>";

        //No multi
//        wvarRequest = "<Request>" + wvarRequest + "</Request>";

        wvarStep = 150;

//        ModGeneral.cmdp_ExecuteTrnMulti( "nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse );
        wvarResponse = getOsbConnector().executeRequest("nbwsA_MQGenericoAIS", wvarRequest);

        //
        wvarStep = 160;

        wobjXMLRespuesta1185 = new XmlDomExtended();
        wobjXMLRespuesta1185.loadXML( wvarResponse.toString() );

        wvarStep = 170;

        if( wobjXMLRespuesta1185.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null )
        {
          //
          wvarStep = 180;

          wvarError = String.valueOf( mcteMsg_ERROR );
        }
      }

      // ***********************************************************************
      // FIN ANEXO I
      // ***********************************************************************
      if( Obj.toInt( wvarError ) == mcteMsg_OK )
      {

        wvarStep = 190;
        wvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENBAJA + "</DEFINICION>" + "<MAIL>" + wvarMail + "</MAIL>" + "</Request>";
        //
        wvarStep = 200;

      nbwsA_SQLGenerico mobjClass = new nbwsA_SQLGenerico();
      StringHolder wvarResponseSH = new StringHolder();
      mobjClass.IAction_Execute(wvarRequest, wvarResponseSH, "" );
      wvarResponse = wvarResponseSH.getValue();

      }


      wvarStep = 290;
      pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>OK</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "</Response>" );
      //
      //Finaliza y libera objetos
      wvarStep = 700;
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        IAction_Execute = 1;
        throw new ComponentExecutionException(_e_);
    }
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
