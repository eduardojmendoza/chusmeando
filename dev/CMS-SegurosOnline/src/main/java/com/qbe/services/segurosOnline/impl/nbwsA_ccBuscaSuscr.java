package com.qbe.services.segurosOnline.impl;
import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class nbwsA_ccBuscaSuscr extends BaseOSBClient implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_ccBuscaSuscr";
  static final String mcteParam_ACCION = "//ACCION";
  static final String mcteParam_DOCUMDAT = "//DOCUMDAT";
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_VIAINSCRIPCION = "//VIAINSCRIPCION";
  static final String mcteParam_DEFINICION = "DEFINICION";
  static final String mcteParam_XMLSQLGENERICO = "P_NBWS_ClienteSuscripto.xml";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_EXISTEUSR = 1;
  static final int mcteMsg_EXISTEDOC = 2;
  static final int mcteMsg_EXISTEMAIL = 3;
  static final int mcteMsg_NOPRODNBWS = 4;
  static final int mcteMsg_NOPRODNAVEXVEND = 5;
  static final int mcteMsg_NOPRODNAVEXCERT = 6;
  static final int mcteMsg_NOEXISTEUSR = 7;
  static final int mcteMsg_NOEXISTEUSRSQL = 8;
  static final int mcteMsg_NOEXISTEUSRAIS = 9;
  static final int mcteMsg_ERRCONSULTA = 10;
  static final int mcteMsg_ERRCONSULTASQL = 11;
  static final int mcteMsg_ERRCONSULTAAIS = 12;
  static final int mcteMsg_NOPRODNBWS_HAB = 13;
  static final int mcteMsg_NOPRODNBWS_POSITIVEID = 14;
  static final int mcteMsg_POLIZAEXCLUIDA = 15;
  static final int mcteMsg_NO_RESULT_BAJA = 16;
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error
   */
  private String[] mcteMsg_DESCRIPTION = new String[17];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponseSQL = null;
    XmlDomExtended wobjXMLResponseAIS = null;
    org.w3c.dom.Element wobjNodo = null;
    org.w3c.dom.Node wobjNodePoliza = null;
    org.w3c.dom.NodeList wobjNodeList_ProdHab = null;
    org.w3c.dom.NodeList wobjNodeList_ProdNav = null;
    String wobjRequestSQL = "";
    String wobjResponseSQL = "";
    String wobjRequestAIS = "";
    String wobjResponseAIS = "";
    String wvarACCION = "";
    String wvarDocumtip = "";
    String wvarDocumdat = "";
    String wvarVIAINSCRIPCION = "";
    String wvarCLIENNOM = "";
    String wvarCLIENAP1 = "";
    String wvarCLIENAP2 = "";
    String wvarReturnMsg = "";
    String wvarError = "";
    String wvarRespPolizas = "";
    String wvarDescNavegable = "";
    String wvarPERMITE_ALTA = "";
    String wvarFiltroBusq = "";
    String wvarRespuesta = "";
    boolean wvarExisteUsrSQL = false;
    boolean wvarFalla = false;
    boolean wvarAgregar = false;
    //
    //XML con el request
    //XML con el response del SQL
    //XML con el response del AIS
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializacion de variables
      wvarStep = 10;
      wvarCLIENNOM = "";
      wvarCLIENAP1 = "";
      wvarCLIENAP2 = "";
      wvarError = String.valueOf( mcteMsg_OK );
      wvarExisteUsrSQL = false;
      wvarReturnMsg = "";
      wvarFalla = false;
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_EXISTEUSR] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_EXISTEUSR" );
      mcteMsg_DESCRIPTION[mcteMsg_EXISTEDOC] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_EXISTEDOC" );
      mcteMsg_DESCRIPTION[mcteMsg_EXISTEMAIL] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_EXISTEMAIL" );
      mcteMsg_DESCRIPTION[mcteMsg_NOPRODNBWS] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOPRODNBWS" );
      mcteMsg_DESCRIPTION[mcteMsg_NOPRODNAVEXVEND] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOPRODNAVEXVEND" );
      mcteMsg_DESCRIPTION[mcteMsg_NOPRODNAVEXCERT] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOPRODNAVEXCERT" );
      mcteMsg_DESCRIPTION[mcteMsg_NOEXISTEUSR] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOEXISTEUSR" );
      mcteMsg_DESCRIPTION[mcteMsg_NOEXISTEUSRSQL] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOEXISTEUSRSQL" );
      mcteMsg_DESCRIPTION[mcteMsg_NOEXISTEUSRAIS] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOEXISTEUSRAIS" );
      mcteMsg_DESCRIPTION[mcteMsg_ERRCONSULTA] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_ERRCONSULTA" );
      mcteMsg_DESCRIPTION[mcteMsg_ERRCONSULTASQL] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_ERRCONSULTASQL" );
      mcteMsg_DESCRIPTION[mcteMsg_ERRCONSULTAAIS] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_ERRCONSULTAAIS" );
      mcteMsg_DESCRIPTION[mcteMsg_NOPRODNBWS_HAB] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOPRODNBWS_HAB" );
      mcteMsg_DESCRIPTION[mcteMsg_NOPRODNBWS_POSITIVEID] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NOPRODNBWS_POSITIVEID" );
      mcteMsg_DESCRIPTION[mcteMsg_POLIZAEXCLUIDA] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_POLIZAEXCLUIDA" );
      mcteMsg_DESCRIPTION[mcteMsg_NO_RESULT_BAJA] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_NO_RESULT_BAJA" );
      //
      //Carga del REQUEST
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //Obtiene parametros
      wvarStep = 30;
      wvarACCION = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ACCION )  );
      wvarDocumtip = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  );
      wvarDocumdat = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMDAT )  );
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_VIAINSCRIPCION )  == (org.w3c.dom.Node) null) )
      {
        wvarVIAINSCRIPCION = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_VIAINSCRIPCION )  );
      }
      else
      {
        wvarVIAINSCRIPCION = "";
      }
      //
      //-----------------------------------------------------------------------------------
      //Paso 1 - SQL NBWS
      //-----------------------------------------------------------------------------------
      //Consulta SP de consulta de suscriptos mediante el SQL Generico de NBWS
      wvarStep = 40;
      wobjNodo = wobjXMLRequest.getDocument().createElement( mcteParam_DEFINICION );
      XmlDomExtended.setText( wobjNodo, mcteParam_XMLSQLGENERICO );
      wobjXMLRequest.selectSingleNode( "//Request" ).appendChild( wobjNodo.cloneNode( true ) );
      wvarStep = 45;
      wobjRequestSQL = XmlDomExtended.marshal(wobjXMLRequest.getDocument().getDocumentElement());
      //
      wvarStep = 50;
      StringHolder sh = new StringHolder();
      nbwsA_SQLGenerico sqlgen = new nbwsA_SQLGenerico(); //Las llamadas al SQLGenerico son locales ( in process ) porque siempre se procesan en CMS
      sqlgen.IAction_Execute(wobjRequestSQL, sh, null);
      wobjResponseSQL = sh.getValue();
      
      wvarStep = 60;
      wobjXMLResponseSQL = new XmlDomExtended();
      wobjXMLResponseSQL.loadXML( wobjResponseSQL );
      //
      //Analiza resultado del SP
      wvarStep = 80;
      if( ! (wobjXMLResponseSQL.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended.getText( wobjXMLResponseSQL.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          //
          wvarStep = 90;
          if( XmlDomExtended.getText( wobjXMLResponseSQL.selectSingleNode( "//CORRECTO" )  ).equals( "S" ) )
          {
            //Existe usuario en SQL de NBWS (COINCIDE: mail / tipo doc / nro doc)
            wvarExisteUsrSQL = true;
          }
          else
          {
            //No existe usuario con ese mail y dni, se analiza si existe usuario con alguno de
            //estos datos por separado
            wvarStep = 100;
            if( !Strings.trim( XmlDomExtended.getText( wobjXMLResponseSQL.selectSingleNode( "//MAIL" )  ) ).equals( "" ) )
            {
              //Existe un usuario con el mismo numero y tipo de documento, se informa mail
              wvarStep = 110;
              wvarError = String.valueOf( mcteMsg_EXISTEDOC );
              wvarReturnMsg = Strings.trim( XmlDomExtended.getText( wobjXMLResponseSQL.selectSingleNode( "//MAIL" )  ) );
            }
            else if( (!Strings.trim( XmlDomExtended.getText( wobjXMLResponseSQL.selectSingleNode( "//DOCUMTIP" )  ) ).equals( "0" )) && (!Strings.trim( XmlDomExtended.getText( wobjXMLResponseSQL.selectSingleNode( "//DOCUMDAT" )  ) ).equals( "0" )) )
            {
              //Existe un usuario con el mismo mail, se informa numero y tipo de documento
              wvarStep = 120;
              wvarError = String.valueOf( mcteMsg_EXISTEMAIL );
              wvarReturnMsg = Strings.trim( XmlDomExtended.getText( wobjXMLResponseSQL.selectSingleNode( "//DOCUMTIP" )  ) ) + "|" + Strings.trim( XmlDomExtended.getText( wobjXMLResponseSQL.selectSingleNode( "//DOCUMDAT" )  ) );
            }
            else
            {
              wvarStep = 125;
              wvarError = String.valueOf( mcteMsg_NOEXISTEUSRSQL );
            }
          }
        }
        else
        {
          wvarError = String.valueOf( mcteMsg_ERRCONSULTASQL );
        }
      }
      else
      {
        wvarError = String.valueOf( mcteMsg_ERRCONSULTASQL );
      }
      //-----------------------------------------------------------------------------------
      //Paso 2 - AIS
      //-----------------------------------------------------------------------------------
      if( (wvarACCION.equals( "A" )) || ((Obj.toInt( wvarError ) != mcteMsg_NOEXISTEUSRSQL) && (Obj.toInt( wvarError ) != mcteMsg_ERRCONSULTASQL)) )
      {
        //Se recuperan de AIS los datos del tipo y numero de documento informados. snoop: agregamos llamada via OSB para traer datos de INSIS
        wvarStep = 130;
        wobjResponseAIS = getOsbConnector().executeRequest("nbwsA_sInicio", pvarRequest);
//        wobjClass = new nbwsA_Transacciones.nbwsA_sInicio();
//        wobjClass.Execute( pvarRequest, wobjResponseAIS, "" );
//        wobjClass = (nbwsA_Transacciones.nbwsA_sInicio) null;
        //
        wvarStep = 140;
        wobjXMLResponseAIS = new XmlDomExtended();
        wobjXMLResponseAIS.loadXML( wobjResponseAIS );
        //
        wvarStep = 150;
        if( ! (wobjXMLResponseAIS == (XmlDomExtended) null) )
        {
          if( ! (wobjXMLResponseAIS.selectSingleNode( "//Response/Estado" )  == (org.w3c.dom.Node) null) )
          {
            if( ! (wobjXMLResponseAIS.selectSingleNode( "//Response/Estado[@resultado='true']" )  == (org.w3c.dom.Node) null) )
            {
              //
              //Recupera Nombre
              wvarStep = 160;
              if( ! (wobjXMLResponseAIS.selectSingleNode( "//CLIENNOM" )  == (org.w3c.dom.Node) null) )
              {
                wvarCLIENNOM = XmlDomExtended.getText( wobjXMLResponseAIS.selectSingleNode( "//CLIENNOM" )  );
              }
              //Recupera Apellido 1
              wvarStep = 170;
              if( ! (wobjXMLResponseAIS.selectSingleNode( "//CLIENAP1" )  == (org.w3c.dom.Node) null) )
              {
                wvarCLIENAP1 = XmlDomExtended.getText( wobjXMLResponseAIS.selectSingleNode( "//CLIENAP1" )  );
              }
              //Recupera Apellido 2
              wvarStep = 180;
              if( ! (wobjXMLResponseAIS.selectSingleNode( "//CLIENAP2" )  == (org.w3c.dom.Node) null) )
              {
                wvarCLIENAP2 = XmlDomExtended.getText( wobjXMLResponseAIS.selectSingleNode( "//CLIENAP2" )  );
              }
            }
            else
            {
              //Error: No se encuentra cliente en AIS o se encuentra y no tiene pólizas en estado vigente
              wvarError = String.valueOf( mcteMsg_NOEXISTEUSRAIS );
            }
          }
          else
          {
            //Error: Error en la ejecución del componente de AIS
            wvarError = String.valueOf( mcteMsg_ERRCONSULTAAIS );
          }
        }
        else
        {
          //Error: Error en la ejecución del componente de AIS
          wvarError = String.valueOf( mcteMsg_ERRCONSULTAAIS );
        }
      }
      //-----------------------------------------------------------------------------------
      //Paso 3 - Generacion de Response
      //-----------------------------------------------------------------------------------
      wvarStep = 200;
      //Si se ocurrió error en la consulta devuelve false
      if( (Obj.toInt( wvarError ) == mcteMsg_ERRCONSULTA) || (Obj.toInt( wvarError ) == mcteMsg_ERRCONSULTASQL) || (Obj.toInt( wvarError ) == mcteMsg_ERRCONSULTAAIS) )
      {
        wvarStep = 210;
        wvarFalla = true;
      }
      else
      {
        //Dependiendo de la accion, analiza y arma la salida
        if( wvarACCION.equals( "A" ) )
        {
          //Si es Alta analiza los productos
          wvarStep = 220;
          if( wvarExisteUsrSQL || (Obj.toInt( wvarError ) == mcteMsg_EXISTEMAIL) || (Obj.toInt( wvarError ) == mcteMsg_EXISTEDOC) )
          {
            //Si el usuario existe en NBWS, devuelve error
            wvarStep = 230;
            if( wvarExisteUsrSQL )
            {
              wvarError = String.valueOf( mcteMsg_EXISTEUSR );
            }
          }
          else
          {
            //Obtiene los productos habilitados del listado de productos del AIS
            wobjNodeList_ProdHab = wobjXMLResponseAIS.selectNodes( "//PRODUCTO[HABILITADO_NBWS='S']" ) ;
            //
            //Si no tiene productos habilitados por tabla de Productos Habilitados
            if( wobjNodeList_ProdHab.getLength() == 0 )
            {
              wvarStep = 240;
              if( wobjXMLResponseAIS.selectNodes( "//PRODUCTO[HABILITADO_NBWS='N']" ) .getLength() != 0 )
              {
                //DA - 24/08/2009: hay productos vigentes en AIS pero están NO HABILITADOS por tabla de Productos Habilitados
                wvarStep = 241;
                wvarError = String.valueOf( mcteMsg_NOPRODNBWS_HAB );
              }
              else
              {
                //DA - 24/08/2009: NO hay productos en AIS
                wvarStep = 242;
                wvarError = String.valueOf( mcteMsg_NOPRODNBWS );
              }

            }
            else
            {
              //Recorre cada producto, asociandoles una descripcion de si son o no navegables y porque
              wvarStep = 250;
              for( int nwobjNodePoliza = 0; nwobjNodePoliza < wobjNodeList_ProdHab.getLength(); nwobjNodePoliza++ )
              {
                wobjNodePoliza = wobjNodeList_ProdHab.item( nwobjNodePoliza );

                wvarDescNavegable = "";
                //Si la poliza esta marcada con X significa que la misma tiene mas de 1 certificado y ya
                //fue procesada, por lo tanto se saltea
                wvarStep = 260;
                if( Strings.find(XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "HABILITADO_NAVEGACION" )  ), "X" ) == 0 )
                {
                  //
                  //DA - 31/08/2009: pregunta si está excluída
                  wvarStep = 261;
                  if( XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "POLIZA_EXCLUIDA" )  ).equals( "S" ) )
                  {
                    wvarDescNavegable = "El cliente posee únicamente pólizas que no estarán incluídas en el servicio por razones comerciales por lo que no es posible continuar con la solicitud de alta.";
                  }
                  else
                  {
                    wvarStep = 262;
                    if( XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "HABILITADO_NAVEGACION" )  ).equals( "N" ) )
                    {
                      //
                      //DA - 05/11/2009: para evitar mostrar TODOS los certificados
                      if( XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "TIPOPROD")  ).equals( "G" ) )
                      {
                        wvarFiltroBusq = "//PRODUCTO[" + "./RAMOPCOD='" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "RAMOPCOD" )  ) + "'" + "and ./POLIZANN='" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode( wobjNodePoliza, "POLIZANN" )  ) + "'" + "and ./POLIZSEC='" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "POLIZSEC" )  ) + "'" + "and ./TIPOPROD='G']";
                        wvarStep = 263;
                        wobjNodeList_ProdNav = wobjXMLResponseAIS.selectNodes( wvarFiltroBusq ) ;
                        //
                        wvarStep = 264;
                        if( wobjNodeList_ProdNav.getLength() > 1 )
                        {
                          wvarDescNavegable = "El cliente posee pólizas incluídas en el servicio pero han sido adquiridas a través de un productor o broker por lo que no es posible continuar con la solicitud de alta.";
                        }
                        wvarStep = 265;
                        //Marca dentro de wobjNodeList_ProdHab todos los certificados con X para evitar que se reprocesen y así salga agrupado en una sola línea cuando se trate de una póliza con mas de un certificado.
                        fncMarcarPolizas(wobjNodeList_ProdHab, wobjNodePoliza);
                      }
                      else
                      {
                        wvarDescNavegable = "El cliente posee pólizas incluídas en el servicio pero han sido adquiridas a través de un productor o broker por lo que no es posible continuar con la solicitud de alta.";
                      }
                      //
                    }
                    else
                    {
                      wvarStep = 266;
                      if( XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "TIPOPROD" )).equals( "G" ) )
                      {
                        //Verifica si la poliza tiene mas de 5 certificados
                        wvarStep = 270;
                        wvarFiltroBusq = "//PRODUCTO[" + "./RAMOPCOD='" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "RAMOPCOD" )  ) + "'" + "and ./POLIZANN='" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "POLIZANN" )  ) + "'" + "and ./POLIZSEC='" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "POLIZSEC" )  ) + "'" + "and ./TIPOPROD='G']";
                        wobjNodeList_ProdNav = wobjXMLResponseAIS.selectNodes( wvarFiltroBusq ) ;
                        //
                        wvarStep = 271;
                        if( wobjNodeList_ProdNav.getLength() > 5 )
                        {
                          wvarDescNavegable = "El cliente posee únicamente pólizas que no estarán incluídas en el servicio por razones comerciales por lo que no es posible continuar con la solicitud de alta.";
                        }
                        //Marca dentro de wobjNodeList_ProdHab todos los certificados con X para evitar que se reprocesen.
                        fncMarcarPolizas(wobjNodeList_ProdHab, wobjNodePoliza);
                      }
                      //
                    }
                    //
                  }
                  //
                  wvarPERMITE_ALTA = "N";
                  if( wvarDescNavegable.equals( "" ) )
                  {
                    wvarDescNavegable = "La póliza es navegable";
                    wvarPERMITE_ALTA = "S";
                  }
                  //
                  wvarStep = 280;

                  wvarRespPolizas = wvarRespPolizas + "<POLIZA>" + "<RAMOPCOD>" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode( wobjNodePoliza, "RAMOPCOD" )  ) + "</RAMOPCOD>" + "<POLIZANN>" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "POLIZANN" )  ) + "</POLIZANN>" + "<POLIZSEC>" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "POLIZSEC" )  ) + "</POLIZSEC>" + "<CERTIPOL>" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "CERTIPOL" )  ) + "</CERTIPOL>" + "<CERTIANN>" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "CERTIANN" )  ) + "</CERTIANN>" + "<CERTISEC>" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "CERTISEC" )  ) + "</CERTISEC>" + "<POSITIVEID>" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "POSITIVEID" )  ) + "</POSITIVEID>" + "<TIPOPROD>" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "TIPOPROD" )  ) + "</TIPOPROD>" + "<HABILITADO_NBWS>" + XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjNodePoliza, "HABILITADO_NBWS" )  ) + "</HABILITADO_NBWS>" + "<RESULTADO_VALIDACION>" + wvarDescNavegable + "</RESULTADO_VALIDACION>" + "<PERMITE_ALTA>" + wvarPERMITE_ALTA + "</PERMITE_ALTA>" + "</POLIZA>";
                }
              }
            }
          }
          wvarStep = 290;
          if( (Obj.toInt( wvarError ) == mcteMsg_OK) || (Obj.toInt( wvarError ) == mcteMsg_NOEXISTEUSRSQL) )
          {
            wvarStep = 300;
            wvarRespuesta = "<CODRESULTADO>OK</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "<DESCRIPCION>" + wvarReturnMsg + "</DESCRIPCION>" + "<POLIZAS>" + wvarRespPolizas + "</POLIZAS>" + "<CLIENNOM>" + wvarCLIENNOM + "</CLIENNOM>" + "<CLIENAP1>" + wvarCLIENAP1 + "</CLIENAP1>" + "<CLIENAP2>" + wvarCLIENAP2 + "</CLIENAP2>" + XmlDomExtended.marshal(wobjXMLResponseAIS.selectSingleNode( "//Response_XML" ));
          }
          else
          {
            wvarStep = 310;
            wvarRespuesta = "<CODRESULTADO>ERROR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "<DESCRIPCION>" + wvarReturnMsg + "</DESCRIPCION>";
          }
        }
        else
        {
          //Si es Baja o Consulta la salida es igual
          wvarStep = 330;
          if( wvarExisteUsrSQL )
          {
            wvarStep = 340;
            wvarRespuesta = "<CODRESULTADO>OK</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "<DESCRIPCION>" + wvarReturnMsg + "</DESCRIPCION>" + "<CLIENNOM>" + wvarCLIENNOM + "</CLIENNOM>" + "<CLIENAP1>" + wvarCLIENAP1 + "</CLIENAP1>" + "<CLIENAP2>" + wvarCLIENAP2 + "</CLIENAP2>";
          }
          else
          {
            wvarStep = 350;
            //DA - 22/09/2009: se agrega esto para mejorar el mensaje al usuario de call centre.
            if( wvarVIAINSCRIPCION.equals( "CC" ) )
            {
              wvarError = String.valueOf( mcteMsg_NO_RESULT_BAJA );
            }
            //
            wvarRespuesta = "<CODRESULTADO>ERROR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "<DESCRIPCION>" + wvarReturnMsg + "</DESCRIPCION>";
          }
        }
      }
      //
      wvarStep = 360;
      if( wvarFalla )
      {
        wvarStep = 370;
        pvarResponse.set( "<Response>" + "<Estado resultado=\"false\" estado=\"" +  wvarError + "\" mensaje=\"" +  mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + String.valueOf( (char)(34) ) + "/>" + "</Response>" );
      }
      else
      {
        wvarStep = 380;
        pvarResponse.set( "<Response>" + "<Estado resultado=\"true\" estado=\"\" mensaje=\"\"/>" + wvarRespuesta + "</Response>" );
      }
      //
      //Finaliza y libera objetos
      wvarStep = 390;
      return IAction_Execute;
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        Err.clear();
        throw new ComponentExecutionException(_e_);
    }
  }

  private void fncMarcarPolizas( org.w3c.dom.NodeList pobjNodeList, org.w3c.dom.Node pobjNodePoliza ) throws Exception
  {
    org.w3c.dom.Node mobjNode = null;
    //
    //
    for( int nmobjNode = 0; nmobjNode < pobjNodeList.getLength(); nmobjNode++ )
    {
      mobjNode = pobjNodeList.item( nmobjNode );
      if( (XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(mobjNode, "RAMOPCOD" )  ).equals( XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(pobjNodePoliza, "RAMOPCOD" )  ) )) && (XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(mobjNode, "POLIZANN" )  ).equals( XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(pobjNodePoliza, "POLIZANN" )  ) )) && (XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(mobjNode, "POLIZSEC" )  ).equals( XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(pobjNodePoliza, "POLIZSEC" )  ) )) && (XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(mobjNode, "TIPOPROD" )  ).equals( XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(pobjNodePoliza, "TIPOPROD" )  ) )) )
      {
        //DA - 24/11/2009: Defect 62 de Mercury
        XmlDomExtended.setText(XmlDomExtended.Node_selectSingleNode(mobjNode, "HABILITADO_NAVEGACION" ) , XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(mobjNode, "HABILITADO_NAVEGACION" )  ) + "X" );
      }
    }
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
