package com.qbe.services.segurosOnline.impl;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class nbwsA_Ingresar extends BaseOSBClient implements VBObjectClass
{

	protected static Logger logger = Logger.getLogger(nbwsA_Ingresar.class.getName());

  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_Ingresar";
  static final String mcteParam_USUARIO = "//USUARIO";
  static final String mcteParam_PASSWORD = "//PASSWORD";
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_RCC = "//RCCs";
  static final String mcteParam_IPORIGEN = "//IPORIGEN";
  /**
   * Constantes de XML de definiciones para SQL Generico
   */
  static final String mcteParam_XMLSQLGENCLISUS = "P_NBWS_ClienteSuscripto.xml";
  static final String mcteParam_XMLSQLGENVALUSR = "P_NBWS_ValidaUsuario.xml";
  static final String mcteParam_XMLSQLGENOBTIDN = "P_NBWS_ObtenerIdentificador.xml";
  static final String mcteParam_XMLSQLGENGUARCC = "P_NBWS_GuardarUltimoRCC.xml";
  static final String mcteParam_XMLSQLGENRCCVAL = "P_NBWS_RegistrarRCCValido.xml";
  static final String mcteParam_XMLSQLGENRCCFAL = "P_NBWS_RegistrarRCCFallido.xml";
  static final String mcteParam_XMLSQLGENOBTPAS = "P_NBWS_ObtenerPassword.xml";
  static final String mcteParam_XMLSQLGENPASVAL = "P_NBWS_RegistrarPasswordValido.xml";
  static final String mcteParam_XMLSQLGENPASFAL = "P_NBWS_RegistrarPasswordFallido.xml";
  static final String mcteParam_XMLSQLGENLASLOG = "P_NBWS_LastLogon.xml";
  static final String mcteParam_XMLSQLGENREGACC = "P_NBWS_RegistrarAcceso.xml";
  /**
   * Constante
   */
  static final String mcteParam_RCCs = "//rcc";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_USRNOEXISTE = 1;
  static final int mcteMsg_USRNOACTIVO = 2;
  static final int mcteMsg_IDNBLOQUEADO = 3;
  static final int mcteMsg_RCCINVALIDO = 4;
  static final int mcteMsg_PASSEXPIRADA = 5;
  static final int mcteMsg_PASSBLOQUEADA = 6;
  static final int mcteMsg_PASSINVALIDA = 7;
  static final int mcteMsg_POLIZANOEMITIDA_OV = 8;
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error (los números matchean los de la tabla SQL tipoAccesos)
   */
  private String[] mcteMsg_DESCRIPTION = new String[14];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String ContextInfo )
  {
    int IAction_Execute = 0;
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    String wobjRequestSQL = "";
    String wobjResponseSQL = "";
    String wobjRequestAIS = "";
    String wobjResponseAIS = "";
    String wvarUsuario = "";
    String wvarPASSWORD = "";
    boolean wvarPassValida = false;
    String wvarDocumtip = "";
    String wvarDocumdat = "";
    String wvarCLIENNOM = "";
    String wvarCLIENAPE = "";
    String wvarRCCs = "";
    String wvarRCC = "";
    boolean wvarRCCValido = false;
    String wvarIPOrigen = "";
    String wvarError = "";
    boolean wvarUsrValido = false;
    String wvarEstadoUsr = "";
    String wvarIdentificador = "";
    String wvarEstadoIdentificador = "";
    String wvarRCCPrevio = "";
    String wvarEstadoPassword = "";
    boolean wvarLoginSinRCC = false;
    String wvarLastLogon = "";
    String wvarBodyResp = "";
    String wvarComboPID = "";
    String wvarProximoPaso = "";
    String wvarVIA_INSCRIPCION = "";
    //
    //XML con el request
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializacion de variables
      wvarStep = 10;
      wvarError = String.valueOf( mcteMsg_OK );
      wvarLoginSinRCC = true;
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_USRNOEXISTE] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_USRNOEXISTE" );
      mcteMsg_DESCRIPTION[mcteMsg_USRNOACTIVO] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_USRNOACTIVO" );
      mcteMsg_DESCRIPTION[mcteMsg_IDNBLOQUEADO] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_IDNBLOQUEADO" );
      mcteMsg_DESCRIPTION[mcteMsg_RCCINVALIDO] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_RCCINVALIDO" );
      mcteMsg_DESCRIPTION[mcteMsg_PASSEXPIRADA] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_PASSEXPIRADA" );
      mcteMsg_DESCRIPTION[mcteMsg_PASSBLOQUEADA] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_PASSBLOQUEADA" );
      mcteMsg_DESCRIPTION[mcteMsg_PASSINVALIDA] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_PASSINVALIDA" );
      mcteMsg_DESCRIPTION[mcteMsg_POLIZANOEMITIDA_OV] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_POLIZANOEMITIDA_OV" );
      //
      //Carga del REQUEST
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //Obtiene parametros
      wvarStep = 40;
      wvarStep = 50;
      wvarUsuario = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARIO )  );
      wvarStep = 60;
      wvarPASSWORD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PASSWORD )  );
      wvarStep = 70;
      wvarIPOrigen = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_IPORIGEN )  );
      wvarStep = 80;
      //Si mandan el DOCUMTIP, es el "paso 2"
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  == (org.w3c.dom.Node) null) )
      {
        wvarStep = 90;
        wvarDocumtip = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  );
        wvarStep = 100;
        wvarRCCs = XmlDomExtended.marshal(wobjXMLRequest.selectSingleNode( mcteParam_RCC ));
        wvarStep = 130;
        wvarLoginSinRCC = false;
      }
      //
      //1) Validar existencia de usuario
      wvarStep = 150;
      StringHolder wvarEstadoIdentificadorSH = new StringHolder();
      StringHolder wvarEstadoPasswordSH = new StringHolder();
      wvarEstadoUsr = CommonFunctions.fncValidarUsuario(wvarUsuario, wvarEstadoIdentificadorSH, wvarEstadoPasswordSH);
      wvarEstadoIdentificador  = wvarEstadoIdentificadorSH.getValue();
      wvarEstadoPassword = wvarEstadoPasswordSH.getValue();
      
      if( wvarEstadoUsr.equals( "ERR" ) )
      {
        wvarError = String.valueOf( mcteMsg_USRNOEXISTE );
      }
      else
      {
        wvarStep = 160;
        //2) Verificar estado del usuario
        if( !wvarEstadoUsr.equals( "A" ) )
        {
          wvarError = String.valueOf( mcteMsg_USRNOACTIVO );
        }
        else
        {
          //3) Verificar estado diferente a bloqueado
          wvarStep = 180;
          if( wvarEstadoIdentificador.equals("2"))
          {
            wvarError = String.valueOf( mcteMsg_IDNBLOQUEADO );
          }
        }
      }
      //
      // Si el login es previo a la verificacion del RCC
      // O sea, paso 1
      wvarStep = 190;
      if( wvarLoginSinRCC )
      {
        if( Obj.toInt( wvarError ) == mcteMsg_OK )
        {
          //4) Verificar RCC preexistente
          //Obtener identificador
          wvarStep = 170;

          StringHolder wvarDocumtipSH = new StringHolder();
          StringHolder wvarIdentificadorSH = new StringHolder();
          StringHolder wvarRCCPrevioSH = new StringHolder();
          CommonFunctions.fncObtenerIdentificador(wvarUsuario, wvarDocumtipSH, wvarIdentificadorSH,wvarRCCPrevioSH);    
          wvarDocumtip = wvarDocumtipSH.getValue();
          wvarIdentificador = wvarIdentificadorSH.getValue();
          wvarRCCPrevio = wvarRCCPrevioSH.getValue();
          
          //
          if( wvarRCCPrevio.length() == 0 )
          {
            //Generar RCC y guardarlo
            wvarStep = 210;
            wvarRCCs = fncGenerarRCC(wvarUsuario, wvarIdentificador);
            wvarStep = 220;
            fncGuardarRCC(wvarUsuario,wvarRCCs);
          }
          else
          {
            wvarStep = 230;
            wvarRCCs = wvarRCCPrevio;
          }
          wvarStep = 240;
          pvarResponse.set( "<Response>" + "<Estado resultado=\"true\" estado=\"\" mensaje=\"\"/>" + "<CODRESULTADO>OK</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + wvarRCCs + "</Response>" );
        }
        else
        {
          wvarStep = 250;
          pvarResponse.set( "<Response>" + "<Estado resultado=\"true\" estado=\"\" mensaje=\"\"/>" + "<CODRESULTADO>ERR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "</Response>" );
        }
      }
      
      else
      {
        //LOGIN CON RCC ESPECIFICADO
    	  // O sea, paso 2
        //4) valida el identificador en base al RCC ingresado
        wvarStep = 300;
        wvarRCCValido = fncValidarRCC(wvarUsuario,wvarDocumtip,wvarRCCs);
        if( ! (wvarRCCValido) )
        {
          wvarStep = 310;
          wvarError = String.valueOf( mcteMsg_RCCINVALIDO );
          wvarStep = 320;
          fncRegistrarRCCFallido(wvarUsuario);
        }
        else
        {
        	// <------------      

        	//5) Registra IDN Valido
          wvarStep = 330;
          fncRegistrarRCCValido(wvarUsuario);
          //Obtiene Estado Password y verifica que no este bloqueado o expirado
          if( wvarEstadoPassword.equals("1") )
          {
            wvarStep = 340;
            wvarError = String.valueOf( mcteMsg_PASSEXPIRADA );
            //DA - 18/11/2009: en caso que la PWD esté bloqueada con CAI pendiente
          }

          else if (wvarEstadoPassword.equals("2") || wvarEstadoPassword.equals("5"))
          {
            wvarStep = 350;
            wvarError = String.valueOf( mcteMsg_PASSBLOQUEADA );
          }
          else
          {
            //6) Valida la password
            wvarStep = 360;
            wvarPassValida = fncValidarPass(wvarUsuario,wvarPASSWORD);
            if( ! (wvarPassValida) )
            {
              wvarStep = 370;
              wvarError = String.valueOf( mcteMsg_PASSINVALIDA );
              fncRegistrarPassFallida(wvarUsuario);
            }
            else
            {
              wvarStep = 380;
              fncRegistrarPassValida(wvarUsuario);
            }
          }
        }
        //
        //7) Verifica si es CAI o CAP o OK
        //Genera respuesta dependiendo del caso
        wvarStep = 400;


        if( Obj.toInt( wvarError ) == mcteMsg_OK )
        {
          if( wvarEstadoPassword.equals("0"))
          {
            //Ingreso OK
            wvarStep = 410;

            wvarLastLogon = fncObtenerLastLogon(wvarUsuario);
            wvarStep = 420;
            StringHolder wvarCLIENNOMSH = new StringHolder();
            StringHolder wvarCLIENAPESH = new StringHolder();
            fncObtenerNomApeCliente(wvarUsuario,wvarCLIENNOMSH,wvarCLIENAPESH);
            wvarCLIENNOM = wvarCLIENNOMSH.getValue();
            wvarCLIENAPE = wvarCLIENAPESH.getValue();
            
            wvarStep = 425;
            StringHolder wvarDocumtipSH = new StringHolder();
            StringHolder wvarDocumdatSH = new StringHolder();
            StringHolder wvarRCCPrevioSH = new StringHolder();
            CommonFunctions.fncObtenerIdentificador(wvarUsuario,wvarDocumtipSH,wvarDocumdatSH,wvarRCCPrevioSH);
            wvarDocumtip = wvarDocumtipSH.getValue();
            wvarDocumdat = wvarDocumdatSH.getValue();
            wvarRCCPrevio = wvarRCCPrevioSH.getValue();
            
            wvarStep = 430;
            wvarBodyResp = "<INGRESO>OK</INGRESO>" + "<CLIENNOM>" + wvarCLIENNOM + "</CLIENNOM>" + "<CLIENAPE>" + wvarCLIENAPE + "</CLIENAPE>" + "<LASTLOGON>" + wvarLastLogon + "</LASTLOGON>" + "<DOCUMTIP>" + wvarDocumtip + "</DOCUMTIP>" + "<DOCUMDAT>" + wvarDocumdat + "</DOCUMDAT>";

            //DA - 06/11/2009: borra temporales de sINICIO para evitar sobrecagar al file server.
            wvarStep = 431;
          }
          else if( wvarEstadoPassword.equals("3"))
          {
            //Ingreso CAI
            wvarStep = 440;

            fncBorrarTemporales();

            StringHolder wvarComboPIDSH = new StringHolder();
            StringHolder wvarVIA_INSCRIPCIONSH = new StringHolder();
            
            wvarProximoPaso = fncAnalizarCAI(wvarUsuario,wvarComboPIDSH,wvarVIA_INSCRIPCIONSH);
            wvarComboPID = wvarComboPIDSH.getValue();
            wvarVIA_INSCRIPCION = wvarVIA_INSCRIPCIONSH.getValue();
            
            
            wvarStep = 460;
            if( wvarProximoPaso.equals( "ERR_OV" ) )
            {
              //DA - 02/10/2009: se agrega la via de inscripción.
              wvarError = String.valueOf( mcteMsg_POLIZANOEMITIDA_OV );
              wvarBodyResp = "<INGRESO>CAI</INGRESO>" + "<CODRESULTADO>ERR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "<ACCION>" + wvarProximoPaso + "</ACCION>" + "<COMBOPID>" + wvarComboPID + "</COMBOPID>" + "<VIA_INSCRIPCION>" + wvarVIA_INSCRIPCION + "</VIA_INSCRIPCION>";
            }
            else
            {
              wvarBodyResp = "<INGRESO>CAI</INGRESO>" + "<ACCION>" + wvarProximoPaso + "</ACCION>" + "<COMBOPID>" + wvarComboPID + "</COMBOPID>" + "<VIA_INSCRIPCION>" + wvarVIA_INSCRIPCION + "</VIA_INSCRIPCION>";
            }

          }
          else if( wvarEstadoPassword.equals("4"))
          {
            wvarStep = 470;
            wvarBodyResp = "<INGRESO>CAP</INGRESO>";
          }
          wvarStep = 480;
          pvarResponse.set( "<Response>" + "<Estado resultado=\"true\" estado=\"\" mensaje=\"\"/>" + wvarBodyResp + "</Response>" );
        }
        else
        {
          wvarStep = 490;
          pvarResponse.set( "<Response>" + "<Estado resultado=\"true\" estado=\"\" mensaje=\"\"/>" + "<CODRESULTADO>ERR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "</Response>" );
        }
        //Registra el acceso en log
        wvarStep = 500;
        fncRegistrarAcceso(wvarUsuario, wvarError,wvarIPOrigen);
      }
      //
      //Finaliza y libera objetos
      wvarStep = 700;
      wobjXMLRequest = null;
      //
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
        Err.set( _e_ );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), null );
        throw new ComponentExecutionException(_e_);
    }
  }

  private String fncGenerarRCC( String pvarUsuario, String pvarIdentificador ) throws Exception
  {
    String fncGenerarRCC = "";
    int wvarRCC1 = 0;
    int wvarRCC2 = 0;
    int wvarRCC3 = 0;
    String wvarIDN = "";
    String wvarResult = "";
    //
    //
    VB.randomize();
    //
    wvarRCC1 = (int)Math.rint( Math.floor( ((Strings.len( pvarIdentificador ) - 3) * VB.rnd()) + 1 ) + 2 );
    //
    wvarRCC2 = (int)Math.rint( Math.floor( ((Strings.len( pvarIdentificador ) - 3) * VB.rnd()) + 1 ) + 2 );
    while( wvarRCC2 == wvarRCC1 )
    {
      wvarRCC2 = (int)Math.rint( Math.floor( ((Strings.len( pvarIdentificador ) - 3) * VB.rnd()) + 1 ) + 2 );
    }
    //
    wvarRCC3 = (int)Math.rint( Math.floor( ((Strings.len( pvarIdentificador ) - 3) * VB.rnd()) + 1 ) + 2 );
    while( (wvarRCC3 == wvarRCC1) || (wvarRCC3 == wvarRCC2) )
    {
      wvarRCC3 = (int)Math.rint( Math.floor( ((Strings.len( pvarIdentificador ) - 3) * VB.rnd()) + 1 ) + 2 );
    }
    //
    wvarIDN = pvarIdentificador;
    wvarIDN = Strings.mid( wvarIDN, 1, (wvarRCC1 - 1) ) + "X" + Strings.mid( wvarIDN, (wvarRCC1 + 1), Strings.len( wvarIDN ) );
    wvarIDN = Strings.mid( wvarIDN, 1, (wvarRCC2 - 1) ) + "X" + Strings.mid( wvarIDN, (wvarRCC2 + 1), Strings.len( wvarIDN ) );
    wvarIDN = Strings.mid( wvarIDN, 1, (wvarRCC3 - 1) ) + "X" + Strings.mid( wvarIDN, (wvarRCC3 + 1), Strings.len( wvarIDN ) );
    //
    wvarResult = "<RCCs longitud=\"" + Strings.len( pvarIdentificador ) + "\" idn=\"" + wvarIDN + "\">" + "<rcc id=\"1\">" + wvarRCC1 + "</rcc>" + "<rcc id=\"2\">" + wvarRCC2 + "</rcc>" + "<rcc id=\"3\">" + wvarRCC3 + "</rcc>" + "</RCCs>";
    //
    fncGenerarRCC = wvarResult;
    //
    return fncGenerarRCC;
  }

  private boolean fncGuardarRCC( String pvarUsuario, String pvarRCC ) throws Exception
  {
    boolean fncGuardarRCC = false;
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarRetVal = "";
    XmlDomExtended mobjXMLResponse = null;
    //
    //
    mvarRetVal = String.valueOf( true );
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENGUARCC + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "<ULTIMORCC><![CDATA[" + pvarRCC + "]]></ULTIMORCC>" + "</Request>";
    //

    nbwsA_SQLGenerico mobjClass = new nbwsA_SQLGenerico();
    StringHolder wvarResponseSH = new StringHolder();
    mobjClass.IAction_Execute(mvarRequest, wvarResponseSH, "" );
    mvarResponse = wvarResponseSH.getValue();
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    fncGuardarRCC = Obj.toBoolean( mvarRetVal );
    mobjClass = null;
    mobjXMLResponse = null;
    //
    return fncGuardarRCC;
  }

  private boolean fncValidarRCC( String pvarUsuario, String pvarDocumtip, String pvarRCC ) throws Exception
  {
    boolean fncValidarRCC = false;
    String mvarRequest = "";
    String mvarResponse = "";
    boolean mvarRetVal = false;
    boolean mvarErrorRCC = false;
    XmlDomExtended mobjXMLResponse = null;
    XmlDomExtended mobjXMLRCC = null;
    XmlDomExtended mobjXMLUltimoRCC = null;
    org.w3c.dom.Node mobjXMLRequestNode = null;

    String mvarDOCUMDAT = "";
    String mvarDOCUMTIP = "";
    String mvarRCCULTIMO = "";
    String mvarRCC = "";
    //
    //
    mvarRetVal = false;
    mvarErrorRCC = false;
    mobjXMLRCC = new XmlDomExtended();
    mobjXMLRCC.loadXML( pvarRCC );
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENOBTIDN + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    nbwsA_SQLGenerico mobjClass = new nbwsA_SQLGenerico();
    StringHolder wvarResponseSH = new StringHolder();
    mobjClass.IAction_Execute(mvarRequest, wvarResponseSH, "" );
    mvarResponse = wvarResponseSH.getValue();
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        mvarDOCUMDAT = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//DOCUMDAT" )  );
        mvarDOCUMTIP = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//DOCUMTIP" )  );
        mvarRCCULTIMO = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//RCCULTIMO" )  );
      }
    }
    //
    if( Strings.trim( mvarRCCULTIMO ).equals( "" ) )
    {
      mvarRetVal = false;
    }
    else
    {
      //
      mobjXMLUltimoRCC = new XmlDomExtended();
      mobjXMLUltimoRCC.loadXML( mvarRCCULTIMO );
      //
      //Primero verifica que los documtip sean iguales
      if( Integer.parseInt(pvarDocumtip) == Integer.parseInt(mvarDOCUMTIP ) )
      {
        //Luego verifica cada uno de los caracteres del rcc
        for( int nmobjXMLRequestNode = 0; nmobjXMLRequestNode < mobjXMLRCC.selectNodes( mcteParam_RCCs ) .getLength(); nmobjXMLRequestNode++ )
        {
          mobjXMLRequestNode = mobjXMLRCC.selectNodes( mcteParam_RCCs ) .item( nmobjXMLRequestNode );
          mvarRCC = XmlDomExtended.getText( mobjXMLUltimoRCC.selectSingleNode( "//rcc[@id=" + XmlDomExtended.getText( mobjXMLRequestNode.getAttributes().item( 0 ) ) + "]" )  );
          //comparacion de lo ingresado con el caracter correspondiente
          if( !Strings.mid( mvarDOCUMDAT, Obj.toInt( mvarRCC ), 1 ).equals( XmlDomExtended .getText( mobjXMLRequestNode ) ) )
          {
            mvarErrorRCC = true;
            break;
          }
        }
        if( ! (mvarErrorRCC) )
        {
          mvarRetVal = true;
        }
      }
    }
    //
    fncValidarRCC = mvarRetVal;
    return fncValidarRCC;
  }

  private Variant fncRegistrarRCCFallido( String pvarUsuario ) throws Exception
  {
    Variant fncRegistrarRCCFallido = new Variant();
    String mvarRequest = "";
    String mvarResponse = "";
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENRCCFAL + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    nbwsA_SQLGenerico mobjClass = new nbwsA_SQLGenerico();
    StringHolder wvarResponseSH = new StringHolder();
    mobjClass.IAction_Execute(mvarRequest, wvarResponseSH, "" );
    mvarResponse = wvarResponseSH.getValue();
    //
    return fncRegistrarRCCFallido;
  }

  private Variant fncRegistrarRCCValido( String pvarUsuario ) throws Exception
  {
    Variant fncRegistrarRCCValido = new Variant();
    String mvarRequest = "";
    String mvarResponse = "";
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENRCCVAL + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    nbwsA_SQLGenerico mobjClass = new nbwsA_SQLGenerico();
    StringHolder wvarResponseSH = new StringHolder();
    mobjClass.IAction_Execute(mvarRequest, wvarResponseSH, "" );
    mvarResponse = wvarResponseSH.getValue();
    //
    return fncRegistrarRCCValido;
  }

  private boolean fncValidarPass( String pvarUsuario, String pvarPassword ) throws Exception
  {
    boolean fncValidarPass = false;
    String mvarRequest = "";
    String mvarResponse = "";
    boolean mvarRetVal = false;
    boolean mvarErrorRCC = false;
    XmlDomExtended mobjXMLResponse = null;
    String mvarPASSENC = "";
    String mvarPASSWORD = "";
    //
    //
    mvarRetVal = false;
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENOBTPAS + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    nbwsA_SQLGenerico mobjClass = new nbwsA_SQLGenerico();
    StringHolder wvarResponseSH = new StringHolder();
    mobjClass.IAction_Execute(mvarRequest, wvarResponseSH, "" );
    mvarResponse = wvarResponseSH.getValue();
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        mvarPASSENC = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//PASSWORD" )  ).trim();
      }
    }
    //
    mvarPASSWORD = ModEncryptDecrypt.CapicomDecrypt( mvarPASSENC );
    
    //
    //Primero verifica que las passwords sean iguales
    if( Strings.toUpperCase( pvarPassword ).equals( Strings.toUpperCase( mvarPASSWORD ) ) )
    {
      mvarRetVal = true;
    }
    //
    fncValidarPass = mvarRetVal;

    return fncValidarPass;
  }

  private Variant fncRegistrarPassFallida( String pvarUsuario ) throws Exception
  {
    Variant fncRegistrarPassFallida = new Variant();
    String mvarRequest = "";
    String mvarResponse = "";
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENPASFAL + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    nbwsA_SQLGenerico mobjClass = new nbwsA_SQLGenerico();
    StringHolder wvarResponseSH = new StringHolder();
    mobjClass.IAction_Execute(mvarRequest, wvarResponseSH, "" );
    mvarResponse = wvarResponseSH.getValue();
    //
    return fncRegistrarPassFallida;
  }

  private Variant fncRegistrarPassValida( String pvarUsuario ) throws Exception
  {
    Variant fncRegistrarPassValida = new Variant();
    String mvarRequest = "";
    String mvarResponse = "";
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENPASVAL + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    nbwsA_SQLGenerico mobjClass = new nbwsA_SQLGenerico();
    StringHolder wvarResponseSH = new StringHolder();
    mobjClass.IAction_Execute(mvarRequest, wvarResponseSH, "" );
    mvarResponse = wvarResponseSH.getValue();
    //
    return fncRegistrarPassValida;
  }

  private String fncObtenerLastLogon( String pvarUsuario ) throws Exception
  {
    String fncObtenerLastLogon = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarRetVal = "";
    XmlDomExtended mobjXMLResponse = null;
    String mvarFECHA = "";
    String mvarTIPOACCESO = "";
    String mvarDESCRIPCION = "";
    String mvarDIRECCION = "";
    //
    //
    mvarRetVal = "";
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENLASLOG + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    nbwsA_SQLGenerico mobjClass = new nbwsA_SQLGenerico();
    StringHolder wvarResponseSH = new StringHolder();
    mobjClass.IAction_Execute(mvarRequest, wvarResponseSH, "" );
    mvarResponse = wvarResponseSH.getValue();
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        mvarFECHA = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//FECHA" )  );
        //Ejemplo mvarFECHA = 2013-12-09 18:19:24.25
        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
        Date d = dateParser.parse(mvarFECHA);      

        //Estos campos se ignoran, no los usa en la respuesta - rgm
        mvarTIPOACCESO = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//TIPOACCESO" )  );
        mvarDESCRIPCION = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//DESCRIPCION" )  );
        mvarDIRECCION = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//DIRECCION" )  );

        //DA - 06/10/2009: a pedido de LG "Estetica solapas.doc" se le dá este formato.

        //original:
        // mvarRetVal = Strings.format( mvarFECHA, "dd/mm/yyyy\\, \\a \\la\\s h:mm" );

        SimpleDateFormat datePrinter = new SimpleDateFormat("dd/MM/yyyy', a las 'HH:mm");
        mvarRetVal = datePrinter.format(d);

      }
    }
    //
    fncObtenerLastLogon = mvarRetVal;
    mobjClass = null;
    mobjXMLResponse = null;
    //
    return fncObtenerLastLogon;
  }

  private void fncRegistrarAcceso( String pvarUsuario, String pvarError, String pvarIPOrigen ) throws Exception
  {
    String mvarRequest = "";
    String mvarResponse = "";
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENREGACC + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "<TIPOACCESO>" + pvarError + "</TIPOACCESO>" + "<IPADDRESS>" + pvarIPOrigen + "</IPADDRESS>" + "</Request>";
    //
    
    nbwsA_SQLGenerico mobjClass = new nbwsA_SQLGenerico();
    StringHolder wvarResponseSH = new StringHolder();
    mobjClass.IAction_Execute(mvarRequest, wvarResponseSH, "" );
    mvarResponse = wvarResponseSH.getValue();
  }

  private Variant fncObtenerNomApeCliente( String pvarUsuario, StringHolder pvarCLIENNOM, StringHolder pvarCLIENAPE ) throws Exception
  {
    Variant fncObtenerNomApeCliente = new Variant();
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarRetVal = "";
    StringHolder mvarDOCUMTIP = new StringHolder();
    StringHolder mvarDOCUMDAT = new StringHolder();
    StringHolder mvarRCCPrevio = new StringHolder();
    XmlDomExtended mobjXMLResponse = null;
    XmlDomExtended wobjXMLResponseAIS = null;
    String wobjResponseAIS = "";
    //
    //
    pvarCLIENNOM.set( "" );
    pvarCLIENAPE.set( "" );
    //
    CommonFunctions.fncObtenerIdentificador(pvarUsuario,mvarDOCUMTIP,mvarDOCUMDAT,mvarRCCPrevio);
    //
    mvarRequest = "<Request>" + "<DOCUMTIP>" + mvarDOCUMTIP + "</DOCUMTIP>" + "<DOCUMDAT>" + mvarDOCUMDAT + "</DOCUMDAT>" + "</Request>";
    wobjResponseAIS = getOsbConnector().executeRequest("nbwsA_sInicio", mvarRequest);
    
    wobjXMLResponseAIS = new XmlDomExtended();
    wobjXMLResponseAIS.loadXML( wobjResponseAIS );
    //
    if( ! (wobjXMLResponseAIS == ( XmlDomExtended ) null) )
    {
      if( ! (wobjXMLResponseAIS.selectSingleNode( "//Response/Estado" )  == (org.w3c.dom.Node) null) )
      {
        if( ! (wobjXMLResponseAIS.selectSingleNode( "//Response/Estado[@resultado='true']" )  == (org.w3c.dom.Node) null) )
        {
          //Recupera Nombre
          if( ! (wobjXMLResponseAIS.selectSingleNode( "//CLIENNOM" )  == (org.w3c.dom.Node) null) )
          {
            pvarCLIENNOM.set( XmlDomExtended .getText( wobjXMLResponseAIS.selectSingleNode( "//CLIENNOM" )  ) );
          }
          //Recupera Apellido 1
          if( ! (wobjXMLResponseAIS.selectSingleNode( "//CLIENAP1" )  == (org.w3c.dom.Node) null) )
          {
            pvarCLIENAPE.set( XmlDomExtended .getText( wobjXMLResponseAIS.selectSingleNode( "//CLIENAP1" )  ) );
          }
        }
      }
    }
    //
    mobjXMLResponse = null;
    wobjXMLResponseAIS = null;
    return fncObtenerNomApeCliente;
  }

  private String fncAnalizarCAI( String pvarUsuario, StringHolder pPositiveId, StringHolder pVIA_INSCRIPCION ) throws Exception
  {
    String fncAnalizarCAI = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarRetVal = "";
    String mvarVIAINSCR = "";
    String mvarXMLPositiveID = "";
    boolean mvarPolizasValidas = false;
    XmlDomExtended mobjXMLResponse = null;
    XmlDomExtended wobjXMLResponseAIS = null;
    org.w3c.dom.NodeList wobjNodeList = null;
    org.w3c.dom.Node wobjNode = null;
    org.w3c.dom.Node mobjNodo = null;
    String mvarPasoSiguiente = "";
    String mvarDOCUMTIP = "";
    String mvarDOCUMDAT = "";
    String mvarRCCPrevio = "";
    int mvarI = 0;
    String mvarResp = "";
    String mvarOption = "";
    String mvarTIPOPROD = "";
    String mvarRAMOPCOD = "";
    String mvarPOLIZANN = "";
    String mvarPOLIZSEC = "";
    String mvarCERTIPOL = "";
    String mvarCERTIANN = "";
    String mvarCERTISEC = "";
    String mvarPOSITIVEID = "";
    String mvarDato2 = "";
    String mvarDato3 = "";
    String mvarCOBRODES = "";
    //
    //
    mvarPolizasValidas = false;
    mvarXMLPositiveID = "";
    //
    
    StringHolder wvarDocumtipSH = new StringHolder();
    StringHolder wvarIdentificadorSH = new StringHolder();
    StringHolder wvarRCCPrevioSH = new StringHolder();
    CommonFunctions.fncObtenerIdentificador(pvarUsuario, wvarDocumtipSH, wvarIdentificadorSH,wvarRCCPrevioSH);    
    mvarDOCUMTIP = wvarDocumtipSH.getValue();
    mvarDOCUMDAT = wvarIdentificadorSH.getValue();
    mvarRCCPrevio = wvarRCCPrevioSH.getValue();
    //
    //Obtiene la via de inscripcion del cliente (CC, OV, NBWS)
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENCLISUS + "</DEFINICION>" + "<DOCUMTIP>" + mvarDOCUMTIP + "</DOCUMTIP>" + "<DOCUMDAT>" + mvarDOCUMDAT + "</DOCUMDAT>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    nbwsA_SQLGenerico mobjClass = new nbwsA_SQLGenerico();
    StringHolder wvarResponseSH = new StringHolder();
    mobjClass.IAction_Execute(mvarRequest, wvarResponseSH, "" );
    mvarResponse = wvarResponseSH.getValue();

    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        mvarVIAINSCR = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//VIAINSCRIPCION" )).trim() ; //NBWS vuelve con espacios al final...
      }
    }
    //
    //DA - 02/10/2009: devuelve la vía de inscripción
    pVIA_INSCRIPCION.set( mvarVIAINSCR); 
    //
    //Dependiendo de la via de inscripcion, analiza cual es el paso siguiente
    
    if( mvarVIAINSCR.equals( "CC" ) )
    {
      mvarPasoSiguiente = "2";
    }
    else if( mvarVIAINSCR.equals( "OV" ) || mvarVIAINSCR.equals( "NBWS" ) )
    {
      mvarRequest = "<Request>" + "<DOCUMTIP>" + mvarDOCUMTIP + "</DOCUMTIP>" + "<DOCUMDAT>" + mvarDOCUMDAT + "</DOCUMDAT>" + "</Request>";
      mvarResponse = getOsbConnector().executeRequest("nbwsA_sInicio", mvarRequest);

      wobjXMLResponseAIS = new XmlDomExtended();
      wobjXMLResponseAIS.loadXML( mvarResponse );
      //
      mvarResp = "";
      if( ! (wobjXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended .getText( wobjXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          //DA - 01/09/2009: se agrega la validación de pólizas excluídas
          //Se muestran todos los productos del cliente, tenga o no Positive ID.
          //Set wobjNodeList = wobjXMLResponseAIS.selectNodes("//PRODUCTO[HABILITADO_NBWS='S' and POSITIVEID!=' ']")
          wobjNodeList = wobjXMLResponseAIS.selectNodes( "//PRODUCTO[HABILITADO_NBWS='S' and POLIZA_EXCLUIDA ='N' and HABILITADO_NAVEGACION='S' and MAS_DE_CINCO_CERT='N']" ) ;

          if( wobjNodeList.getLength() == 0 )
          {
            //
            mvarResp = "<OPTION VALUE=\"0\">Usted no posee productos habilitados para NBWS</OPTION>";
          }
          else
          {
            //Si hay polizas navegables, arma el XML para el combo y validacion de PositiveID
            //DA - 18/09/2009: en una call con LG pidió que se agrupen los productos. Es decir, si tiene mas de un AUS1 mostrar solo uno.
            mvarPolizasValidas = true;
            for( int nmobjNodo = 0; nmobjNodo < wobjNodeList.getLength(); nmobjNodo++ )
            {
              mobjNodo = wobjNodeList.item( nmobjNodo );
              mvarTIPOPROD = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(mobjNodo, "TIPOPROD" )  );
              mvarRAMOPCOD = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(mobjNodo, "RAMOPCOD" )  );
              mvarPOLIZANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(mobjNodo, "POLIZANN" )  );
              mvarPOLIZSEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(mobjNodo, "POLIZSEC" )  );
              mvarCERTIPOL = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(mobjNodo, "CERTIPOL" )  );
              mvarCERTIANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(mobjNodo, "CERTIANN" )  );
              mvarCERTISEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(mobjNodo, "CERTISEC" )  );
              mvarPOSITIVEID = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(mobjNodo, "POSITIVEID" )  );
              mvarDato2 = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(mobjNodo, "DATO2" )  );
              mvarDato3 = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(mobjNodo, "DATO3" )  );
              mvarCOBRODES = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(mobjNodo, "COBRODES" )  );
              //
              if( (mvarTIPOPROD.equals( "G" )) && (mvarCERTISEC.equals( "0" )) )
              {
                //DA - 11/11/2009: LG en un mail del 11/11/2009 pidió que para los productos GENERALES no se muestre la póliza colectiva con CERTISEC = 0
              }
              else
              {
                mvarOption = "<OPTION PID=\"" + mvarPOSITIVEID + "\" value=\"" + mvarRAMOPCOD + "|" + mvarPOLIZANN + "|" + mvarPOLIZSEC + "|" + mvarCERTIPOL + "|" + mvarCERTIANN + "|" + mvarCERTISEC + "\" DATO2=\"" + mvarDato2 + "\" DATO3=\"" + mvarDato3 + "\" COBRODES=\"" + mvarCOBRODES + "\">";
                //
                if( (mvarTIPOPROD.equals( "M" )) || (mvarTIPOPROD.equals( "G" )) )
                {
                  //DA - 18/09/2009: LG en una call dijo que se quite el nro de póliza
                  //Esto rompe el esquema de los combos del PositiveID de NYL. Hablar con AG cuando vuelva de vacaciones.
                  //En la reunión del 20/10/2009 se acordó dejar el combo como está.
                  mvarOption = mvarOption + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(mobjNodo, "RAMOPDES" )  ) + " (" + mvarRAMOPCOD + "-" + Strings.right( ("00" + mvarPOLIZANN), 2 ) + "-" + Strings.right( ("000000" + mvarPOLIZSEC), 6 ) + "/" + Strings.right( ("0000" + mvarCERTIPOL), 4 ) + "-" + Strings.right( ("0000" + mvarCERTIANN), 4 ) + "-" + Strings.right( ("000000" + mvarCERTISEC), 6 ) + ")";
                }
                else
                {
                  //DA - 18/09/2009: LG en una call dijo que se quite el nro de póliza
                  //Esto rompe el esquema de los combos del PositiveID de NYL. Hablar con AG cuando vuelva de vacaciones.
                  //En la reunión del 20/10/2009 se acordó dejar el combo como está.
                  mvarOption = mvarOption + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(mobjNodo, "RAMOPDES" )  ) + " (" + mvarRAMOPCOD + "-" + Strings.right( ("00" + mvarPOLIZANN), 2 ) + "-" + Strings.right( ("000000" + mvarPOLIZSEC), 6 ) + ")";
                }
                mvarOption = mvarOption + "</OPTION>";
              }
              //
              //Verifica que la poliza no este cargada ya en el combo
              //Caso Productos de Cartera general con mas de un certificado
              if( Strings.find( mvarResp, (mvarRAMOPCOD + " " + mvarPOLIZANN + "-" + mvarPOLIZSEC) ) == 0 )
              {
                mvarResp = mvarResp + mvarOption;
              }
              //
              mvarOption = "";
              //
            }
          }
        }
      }
      //
      pPositiveId.set( mvarResp );
      //
      if( mvarPolizasValidas )
      {
        mvarPasoSiguiente = "1";
      }
      else
      {
        //
        if( mvarVIAINSCR.equals( "OV" ) )
        {
          mvarPasoSiguiente = "ERR_OV";
        }
        else
        {
          mvarPasoSiguiente = "E";
        }
        //
      }
      //
      wobjXMLResponseAIS = null;
    }
    //
    fncAnalizarCAI = mvarPasoSiguiente;
    mobjXMLResponse = null;
    //
    return fncAnalizarCAI;
  }

  /**
   * Borra los archivos temporales de más de más de un día de antigüedad.
   * ( No sé donde se crean! )
   * 
   * @return
 * @throws XmlDomExtendedException 
 * @throws IOException 
   * @throws Exception
   */
  private void fncBorrarTemporales() throws IOException, XmlDomExtendedException 
  {
    XmlDomExtended wobjXMLNBWS_TempFilesServer = null;

    File tempsFolder = null;
    File[] tempFiles = null;

    //Levanta XML de configuración donde se indica la ruta de grabación de archivos temporales.
    wobjXMLNBWS_TempFilesServer = new XmlDomExtended();
    wobjXMLNBWS_TempFilesServer.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.wcteNBWS_TempFilesServer));
    String path = XmlDomExtended .getText( wobjXMLNBWS_TempFilesServer.selectSingleNode( "//PATH" ));
    if ( path == null ) {
    	throw new ComponentExecutionException("Error de configuración: no se pudo recuperar el path de archivos temporales del archivo " + ModGeneral.wcteNBWS_TempFilesServer);
    }
    tempsFolder = new File(path);
    tempFiles = tempsFolder.listFiles();
    if ( tempFiles == null ) {
    	return;
    }
    for (File file : tempFiles) {
	    if ( file.isFile()) {
	      	int daysBack = 1;
	    	long purgeTime = System.currentTimeMillis() - (daysBack * 24L * 60L * 60L * 1000L);
	    	long fileModTime = file.lastModified();
	    	if ( 0L != fileModTime && fileModTime <= purgeTime) {
	    	  if ( !file.delete() ) {
	    		  logger.log(Level.WARNING, String.format("No pude borrar el archivo %s",file));
	    	  }
	    	}
	     }
	}
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
