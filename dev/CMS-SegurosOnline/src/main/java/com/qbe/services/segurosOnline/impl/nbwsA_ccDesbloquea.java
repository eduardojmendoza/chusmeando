package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class nbwsA_ccDesbloquea implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_ccDesbloquea";
  static final String mcteParam_MAIL = "//MAIL";
  /**
   * Archivo Configuracion CAI / CAP
   */
  static final String mcteEMailTemplateConfig = "NBWS_EnvioMailConfig.xml";
  /**
   * Constantes de XML de definiciones para SQL Generico
   */
  static final String mcteParam_XMLSQLGENDESBL = "P_NBWS_Desbloqueo.xml";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_ERROR = 1;
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
   */
  private String[] mcteMsg_DESCRIPTION = new String[13];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    Object wobjClass = null;
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    String wobjRequestSQL = "";
    String wobjResponseSQL = "";
    String wobjRequestAIS = "";
    String wobjResponseAIS = "";
    String wvarMail = "";
    String wvarCAP = "";
    String wvarError = "";
    String wvarCAPEnc = "";
    String wvarRetVal = "";
    String wvarRequest = "";
    String wvarResponse = "";
    //
    //XML con el request
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializacion de variables
      wvarStep = 10;
      wvarError = String.valueOf( mcteMsg_OK );
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_ERROR] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_ERROR" );
      //
      //Carga del REQUEST
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //Obtiene parametros
      wvarStep = 40;
      wvarStep = 50;
      wvarMail = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MAIL )  );
      //
      wvarStep = 100;
      wvarCAP = Strings.toUpperCase( ModGeneral.generar_RNDSTR( new Variant( 8 )/*warning: ByRef value change will be lost.*/ ) );
      wvarStep = 130;
      wvarCAPEnc = ModEncryptDecrypt.CapicomEncrypt( wvarCAP );
      //
      wvarStep = 150;
      wvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENDESBL + "</DEFINICION>" + "<MAIL>" + wvarMail + "</MAIL>" + "<CAP><![CDATA[" + wvarCAPEnc + "]]></CAP>" + "</Request>";
      //
      wvarStep = 200;
      wobjClass = new nbwsA_SQLGenerico();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
      wobjClass = null;
      //
      //invoke( "fncEnviarMail", new Variant[] { new Variant(wvarMail), new Variant(wvarCAP) } ); //SE COMENTA, SE DESCONOCE EL USO DEL INVOKE.
      //
      wvarStep = 290;
      pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>OK</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "</Response>" );
      //
      //Finaliza y libera objetos
      wvarStep = 700;
      wobjXMLRequest = null;
      //
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        wobjXMLRequest = null;
        //
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  /**
   * GD: 17/11/2009 - Envio de mail via SOAPMW
   */
  private boolean fncEnviarMail( String pvarMail, String pvarCAP ) throws Exception
  {
    boolean fncEnviarMail = false;
    Object wobjClass = null;
    XmlDomExtended wvarXMLResponseMdw = null;
    XmlDomExtended wobjXMLConfig = null;
    String wvarResponseMdw = "";
    String mvarRequestMDW = "";
    String mvarTemplateCAP = "";
    String mvarMailPrueba = "";
    String mvarCopiaOculta = "";
    //
    //Se obtienen los par�metros desde XML
    wobjXMLConfig = new XmlDomExtended();
    wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteEMailTemplateConfig));


    mvarTemplateCAP = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/ENVIOCAP" )  );

    //GED: para que no envie al mail que viene si no al que se define para pruebas, asi se evita mandar al exterior
    // correos con info no deseada accidentalmente
    if( ! (wobjXMLConfig.selectSingleNode( "//MAILPRUEBA" )  == (org.w3c.dom.Node) null) )
    {
      mvarMailPrueba = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MAILPRUEBA" )  );
    }
    else
    {
      mvarMailPrueba = "";
    }

    if( ! (wobjXMLConfig.selectSingleNode( "//COPIAOCULTA" )  == (org.w3c.dom.Node) null) )
    {
      mvarCopiaOculta = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//COPIAOCULTA" )  );
    }
    else
    {
      mvarCopiaOculta = "";
    }

    //Se cambia envio de mail por medio de MQ.
    mvarRequestMDW = "<Request>" + "<DEFINICION>ismSendPdfReportLBAVO.xml</DEFINICION>" + "<Raiz>share:sendPdfReport</Raiz>" + "<files/>" + "<applicationId>NBWS</applicationId>" + "<reportId/>" + "<recipientTO>" + pvarMail + "</recipientTO>" + "<recipientsCC/>" + "<recipientsBCC>" + mvarCopiaOculta + "</recipientsBCC>" + "<templateFileName>" + mvarTemplateCAP + "</templateFileName>" + "<parametersTemplate>CLAVE=" + pvarCAP + "</parametersTemplate>" + "<from/>" + "<replyTO/>" + "<bodyText/>" + "<subject/>" + "<importance/>" + "<imagePathFile/>" + "<attachPassword/>" + "<attachFileName/>" + "</Request>";

    //
    //wobjClass = new LBAA_MWGenerico.lbaw_MQMW();//SE COMENTA, ESTA VARIABLE SE INICIALIZA EN NULL LUEGO.
    //error: function 'Execute' was not found.
    //unsup: Call wobjClass.Execute(mvarRequestMDW, wvarResponseMdw, "")
    //
    wobjClass = null;
    //
    wvarXMLResponseMdw = new XmlDomExtended();
    wvarXMLResponseMdw.loadXML( wvarResponseMdw );
    //
    if( ! (wvarXMLResponseMdw.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( wvarXMLResponseMdw.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        if( wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" )  == (org.w3c.dom.Node) null )
        {
          fncEnviarMail = true;
        }
        else
        {
          fncEnviarMail = false;
        }
      }
      else
      {
        if( ! (wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" )  == (org.w3c.dom.Node) null) )
        {
          fncEnviarMail = false;
        }
        else
        {
          fncEnviarMail = false;
        }
        fncEnviarMail = false;
      }
    }
    else
    {
      fncEnviarMail = false;
    }
    //
    wobjClass = null;
    wvarXMLResponseMdw = null;
    wobjXMLConfig = null;

    return fncEnviarMail;
  }

  private boolean fncEnviarMail_bak( String pvarMail, String pvarCAP ) throws Exception
  {
    boolean fncEnviarMail_bak = false;
    String wvarRequest = "";
    String wvarResponse = "";
    Object wobjClass = null;
    XmlDomExtended wvarXMLRequest = null;
    XmlDomExtended wvarXMLResponse = null;
    XmlDomExtended wobjXMLParametro = null;
    XmlDomExtended wobjXMLCODOPParam = null;
    boolean wvarRellamar = false;
    String wvarResponseAIS = "";
    String wvarCODOPDesc = "";
    //
    //
    wobjXMLParametro = new XmlDomExtended();
    wobjXMLParametro.load( System.getProperty("user.dir") + "\\XMLs\\NBWSEnvioCAP.xml" );
    //
    //invoke( "agregarParametro", new Variant[] { new Variant(wobjXMLParametro), new Variant("%%CAP%%"), new Variant(pvarCAP) } );// SE COMENTA, SE DESCONOCE EL USO PERO NO ASIGNA EL VALOR A NINGUNA VARIABLE
    //
    XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//TO" ) , pvarMail );
    //
    wvarRequest = XmlDomExtended.marshal(wobjXMLParametro.selectSingleNode( "//Request" ));
    //
    //wobjClass = new cam_OficinaVirtual.camA_EnviarMail();//SE COMENTA, ESTA VARIABLE SE INICIALIZA EN NULL LUEGO
    //error: function 'Execute' was not found.
    //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    wobjClass = null;
    //
    wvarXMLResponse = new XmlDomExtended();
    wvarXMLResponse.loadXML( wvarResponse );
    //
    if( ! (wvarXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( wvarXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        fncEnviarMail_bak = true;
      }
      else
      {
        fncEnviarMail_bak = false;
      }
    }
    else
    {
      fncEnviarMail_bak = false;
    }
    //
    wvarXMLResponse = null;
    wobjClass = null;
    wobjXMLParametro = null;
    return fncEnviarMail_bak;
  }

  private void agregarParametro( XmlDomExtended pobjXMLParametro, String pvarParam, String pvarValor ) throws Exception
  {
    org.w3c.dom.Element wobjXMLElement = null;
    org.w3c.dom.Element wobjXMLElementAux = null;
    org.w3c.dom.CDATASection wobjXMLElementCda = null;
    XmlDomExtended wobjXMLParametro = null;

    wobjXMLElement = pobjXMLParametro.getDocument().createElement( "PARAMETRO" );
    //
    wobjXMLElementAux = pobjXMLParametro.getDocument().createElement( "PARAM_NOMBRE" );
    wobjXMLElementCda = pobjXMLParametro.getDocument().createCDATASection( pvarParam );
    wobjXMLElementAux.appendChild( wobjXMLElementCda );
    wobjXMLElement.appendChild( wobjXMLElementAux );
    wobjXMLElementAux = pobjXMLParametro.getDocument().createElement( "PARAM_VALOR" );
    wobjXMLElementCda = pobjXMLParametro.getDocument().createCDATASection( pvarValor );
    wobjXMLElementAux.appendChild( wobjXMLElementCda );
    //
    wobjXMLElement.appendChild( wobjXMLElementAux );
    //
    /*unsup pobjXMLParametro.selectSingleNode( "//PARAMETROS" ) .appendChild( wobjXMLElement );*/ //SE COMENTA, ESTA LINEA CERRABA EL COMENTARIO JUSTO ANTES DEL .appendChild
    //
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
