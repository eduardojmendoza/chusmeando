package com.qbe.services.segurosOnline.impl;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.StreamUtils;

import com.qbe.vbcompat.framework.ComponentExecutionException;

public class MailSender {

	private static final String HEADER_CLIENTES_GIF = "LocalMailTemplates/header-clientes.gif";

	private static final String TEMPLATE_EMAIL_CAI = "LocalMailTemplates/Local_NBWS_TemplateEmail_CAI.html";

	private static final String TEMPLATE_EMAIL_CAI_OV_ = "LocalMailTemplates/Local_NBWS_TemplateEmail_CAI_OV.html";

	private static final String GIF_MIME_TYPE = "image/gif";

	private static Logger logger = Logger.getLogger(MailSender.class.getName());

	protected static ApplicationContext context; 

	protected synchronized static ApplicationContext getContext() {

		if ( context == null) {
			context = new ClassPathXmlApplicationContext("classpath*:spring-beans-seguros-online.xml");
		}
		return context;
	}


	/**
	 * Envía mail de CAIs
	 * FIXME parametrizarlo para que pueda usar varios templates
	 * 
	 * @param email
	 * @param viaInscripcion
	 * @param claveTemporal
	 * @return
	 */
	public boolean sendCAIMail(String email, String viaInscripcion, String claveTemporal) {
		String subject = "Suscripción Zurich Seguros OnLine";
		String fromAddress = "Mis Seguros <misseguros@zurich.com>";
		String replyToAddress = "Contacto Zurich <contactenos.patrimoniales@zurich.com>";

		try {
			JavaMailSender mailSender = (JavaMailSender) getContext().getBean("mailSender");
			if (mailSender == null) {
				throw new ComponentExecutionException(
						"No está configurado el mailSender en Seguros Online. Revisar la configuración de Spring");
			}

			MimeMessage message = mailSender.createMimeMessage();

			// use the true flag to indicate you need a multipart message
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(fromAddress);
			helper.setTo(email);
			helper.setReplyTo(replyToAddress);
			helper.setSubject(subject);
			// http://docs.spring.io/spring/docs/2.0.x/api/org/springframework/mail/javamail/MimeMessageHelper.html#setPriority(int)
			// priority - the priority value; typically between 1 (highest) and
			// 5 (lowest)
			helper.setPriority(3);
			boolean htmlFormat = true;
			// use the true flag to indicate the text included is HTML
			String body = null;
			try {
				if ( "OV".equalsIgnoreCase(viaInscripcion)) {
					body = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream(TEMPLATE_EMAIL_CAI_OV_), Charset.forName("UTF-8"));
				} else {
					body = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream(TEMPLATE_EMAIL_CAI), Charset.forName("UTF-8"));
				}
			} catch (IOException e) {
				body = "<p><strong>Estimado Cliente:</strong></p><p>Tu Contraseña de Acceso Inicial a Zurich Seguros Online es <strong>%%CLAVE%%</strong>.</p>" +
						"<p>Ingresá a <strong>www.segurosonline.qbe.com.ar</strong> con la misma para completar tu registración.</p>" +
						"<p>Deberás seleccionar una contraseña que sea de tu agrado y te servirá en los próximos ingresos.</p>" +
						"<p>También te serán requeridos ciertos datos de carácter personal para poder identificarte como cliente de la compañía en forma fehaciente.</p>" +
						"<p>Todo este proceso deberá ser realizado por ÚNICA VEZ.</p>" +
						"<p>Muchas gracias.</p>";
			}
			body = StringUtils.replace(body, "%%CLAVE%%", claveTemporal);
			helper.setText(body, htmlFormat);
			
			try {
				helper.addInline("header-clientes", new ByteArrayDataSource(Thread.currentThread().getContextClassLoader().getResourceAsStream(HEADER_CLIENTES_GIF), GIF_MIME_TYPE));
			} catch (IOException e) {
				logger.log(Level.SEVERE, "No puedo incrustar el gif en el mail", e);
			}
			mailSender.send(message);
			return true;
		} catch (MailException e) {
			logger.log(Level.SEVERE, "Al enviar mail", e);
			throw new ComponentExecutionException("Al enviar mail", e);
		} catch (MessagingException e) {
			logger.log(Level.SEVERE, "Al enviar mail", e);
			throw new ComponentExecutionException("Al enviar mail", e);
		}
	}

}
