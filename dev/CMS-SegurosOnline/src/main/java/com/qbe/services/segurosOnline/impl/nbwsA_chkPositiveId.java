package com.qbe.services.segurosOnline.impl;
import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import java.util.logging.Logger;

import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class nbwsA_chkPositiveId extends BaseOSBClient implements VBObjectClass
{

	protected static Logger logger = Logger.getLogger(nbwsA_chkPositiveId.class.getName());
	
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_chkPositiveId";
  static final String mcteParam_USUARIO = "//USUARIO";
  static final String mcteParam_PRODUCTO = "//PRODUCTO";
  static final String mcteParam_DATO1 = "//DATO1";
  static final String mcteParam_DATO2 = "//DATO2";
  static final String mcteParam_DATO3 = "//DATO3";
  static final String mcteParam_DATO4 = "//DATO4";
  /**
   * Constantes de XML de definiciones para SQL Generico
   */
  static final String mcteParam_XMLSQLGENVALUSR = "P_NBWS_ValidaUsuario.xml";
  static final String mcteParam_XMLSQLGENOBTIDN = "P_NBWS_ObtenerIdentificador.xml";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_USRNOEXISTE = 1;
  static final int mcteMsg_DATOSINVALIDOS = 2;
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error (los números matchean los de la tabla SQL tipoAccesos)
   */
  private String[] mcteMsg_DESCRIPTION = new String[13];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String ContextInfo )
  {
    int IAction_Execute = 0;
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    String wobjRequestSQL = "";
    String wobjResponseSQL = "";
    String wobjRequestAIS = "";
    String wobjResponseAIS = "";
    String wvarUsuario = "";
    String wvarProducto = "";
    String wvarDato1 = "";
    String wvarDato2 = "";
    String wvarDato3 = "";
    String wvarDato4 = "";
    int wvarError = 0;
    String wvarEstadoIdentificador = "";
    String wvarEstadoPassword = "";
    String wvarEstadoUsr = "";
    boolean wvarResChk = false;
    //
    //XML con el request
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializacion de variables
      wvarStep = 10;
      wvarError = mcteMsg_OK;
      wvarResChk = false;
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_USRNOEXISTE] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_USRNOEXISTE" );
      mcteMsg_DESCRIPTION[mcteMsg_DATOSINVALIDOS] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_DATOSINVALIDOS" );
      //
      //Carga del REQUEST
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //Obtiene parametros
      wvarStep = 40;
      wvarStep = 50;
      wvarUsuario = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARIO )  );
      wvarStep = 60;
      wvarProducto = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PRODUCTO )  );
      wvarStep = 70;
      wvarDato1 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DATO1 )  );
      wvarStep = 80;
      wvarDato2 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DATO2 )  );
      wvarStep = 90;
      wvarDato3 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DATO3 )  );
      wvarStep = 100;
      wvarDato4 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DATO4 )  );
      //
      //1) Validar existencia de usuario
      wvarStep = 150;
      StringHolder wvarEstadoIdentificadorSH = new StringHolder();
      StringHolder wvarEstadoPasswordSH = new StringHolder();
      wvarEstadoUsr = CommonFunctions.fncValidarUsuario(wvarUsuario, wvarEstadoIdentificadorSH, wvarEstadoPasswordSH);
      wvarEstadoIdentificador  = wvarEstadoIdentificadorSH.getValue();
      wvarEstadoPassword = wvarEstadoPasswordSH.getValue();
      
      if( wvarEstadoUsr.equals( "ERR" ) )
      {
        wvarError = mcteMsg_USRNOEXISTE;
      }
      else
      {
        wvarError = mcteMsg_OK;
      }
      //
      wvarStep = 190;
      if( wvarError == mcteMsg_OK )
      {
        //
        wvarResChk = fncChkPositiveId(wvarUsuario,wvarProducto,wvarDato1,wvarDato2,wvarDato3,wvarDato4);
        //
        if( wvarResChk )
        {
          wvarStep = 200;
          pvarResponse.set( "<Response>" + "<Estado resultado=\"true\" estado=\"\" mensaje=\"\"/>" + "<CODRESULTADO>OK</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[wvarError] + "</MSGRESULTADO>" + "</Response>" );
        }
        else
        {
          wvarError = mcteMsg_DATOSINVALIDOS;
          wvarStep = 210;
          pvarResponse.set( "<Response>" + "<Estado resultado=\"true\" estado=\"\" mensaje=\"\"/>" + "<CODRESULTADO>ERR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[wvarError] + "</MSGRESULTADO>" + "</Response>" );
        }
      }
      else
      {
        wvarStep = 220;
        pvarResponse.set( "<Response>" + "<Estado resultado=\"true\" estado=\"\" mensaje=\"\"/>" + "<CODRESULTADO>ERR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[wvarError] + "</MSGRESULTADO>" + "</Response>" );
      }
      //
      //Finaliza y libera objetos
      wvarStep = 500;
      //
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
        Err.set( _e_ );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), null );
        throw new ComponentExecutionException(_e_);
    }
  }


  private boolean fncChkPositiveId( String pvarUsuario, String pvarProducto, String pvarDato1, String pvarDato2, String pvarDato3, String pvarDato4 ) throws Exception
  {
    boolean fncChkPositiveId = false;
    String mvarRequest = "";
    String mvarResponse = "";
    XmlDomExtended wobjXMLResponse = null;
    org.w3c.dom.NodeList wobjNodeList = null;
    boolean mvarRetVal = false;
    String mvarDOCUMTIP = "";
    String mvarDOCUMDAT = "";
    String mvarRCCPrevio = "";
    String[] wArrProducto = null;
    String mvarRAMOPCOD = "";
    String mvarPOLIZANN = "";
    String mvarPOLIZSEC = "";
    String mvarCERTIPOL = "";
    String mvarCERTIANN = "";
    String mvarCERTISEC = "";
    String wvarFiltro = "";
    String mvarDato1 = "";
    String mvarDato2 = "";
    String mvarDato3 = "";
    String mvarDato4 = "";
    //
    //
    mvarRetVal = false;

    StringHolder wvarDocumtipSH = new StringHolder();
    StringHolder wvarDocumdatSH = new StringHolder();
    StringHolder wvarRCCPrevioSH = new StringHolder();
    CommonFunctions.fncObtenerIdentificador(pvarUsuario,wvarDocumtipSH,wvarDocumdatSH,wvarRCCPrevioSH);
    mvarDOCUMTIP = wvarDocumtipSH.getValue();
    mvarDOCUMDAT = wvarDocumdatSH.getValue();
    mvarRCCPrevio = wvarRCCPrevioSH.getValue();

    mvarRequest = "<Request>" + "<DOCUMTIP>" + mvarDOCUMTIP + "</DOCUMTIP>" + "<DOCUMDAT>" + mvarDOCUMDAT + "</DOCUMDAT>" + "</Request>";
    //
    mvarResponse = getOsbConnector().executeRequest("nbwsA_sInicio", mvarRequest);

    wobjXMLResponse = new XmlDomExtended();
    wobjXMLResponse.loadXML( mvarResponse );
    //
    //Replica los pasos inversos de nbwsA_Ingresar
    //
    wArrProducto = Strings.split( pvarProducto, "|", -1 );
    mvarRAMOPCOD = wArrProducto[0];
    mvarPOLIZANN = wArrProducto[1];
    mvarPOLIZSEC = wArrProducto[2];
    mvarCERTIPOL = wArrProducto[3];
    mvarCERTIANN = wArrProducto[4];
    mvarCERTISEC = wArrProducto[5];
    //
    //Arma el query XPATH
    //
    if( ! (new StringHolder( pvarDato1 ).isNumeric()) )
    {
      mvarDato1 = "'" + Strings.toUpperCase( pvarDato1 ) + "'";
    }
    else
    {
      mvarDato1 = pvarDato1;
    }
    if( ! (new StringHolder( pvarDato2 ).isNumeric()) )
    {
      mvarDato2 = "'" + Strings.toUpperCase( pvarDato2 ) + "'";
    }
    else
    {
      mvarDato2 = pvarDato2;
    }
    if( ! (new StringHolder( pvarDato3 ).isNumeric()) )
    {
      mvarDato3 = "'" + Strings.toUpperCase( pvarDato3 ) + "'";
    }
    else
    {
      mvarDato3 = pvarDato3;
    }
    if( ! (new StringHolder( pvarDato4 ).isNumeric()) )
    {
      mvarDato4 = "'" + Strings.toUpperCase( pvarDato4 ) + "'";
    }
    else
    {
      mvarDato4 = pvarDato4;
    }
    //
    wvarFiltro = "//PRODUCTO[RAMOPCOD='" + mvarRAMOPCOD + "'" + " and POLIZANN='" + mvarPOLIZANN + "'" + " and POLIZSEC='" + mvarPOLIZSEC + "'" + " and CERTIPOL='" + mvarCERTIPOL + "'" + " and CERTIANN='" + mvarCERTIANN + "'" + " and CERTISEC='" + mvarCERTISEC + "'" + " and DATO1=" + mvarDato1 + " and DATO2=" + mvarDato2 + " and DATO3=" + mvarDato3 + " and DATO4=" + mvarDato4 + "]";
    //
    if( ! (wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        wobjNodeList = wobjXMLResponse.selectNodes( wvarFiltro ) ;
        if( wobjNodeList.getLength() > 0 )
        {
          mvarRetVal = true;
        }
      }
    }
    //
    fncChkPositiveId = mvarRetVal;
    //
    return fncChkPositiveId;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
