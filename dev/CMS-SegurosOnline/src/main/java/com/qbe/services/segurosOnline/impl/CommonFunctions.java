package com.qbe.services.segurosOnline.impl;

import org.w3c.dom.Node;

import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

public class CommonFunctions {

	public CommonFunctions() {
		// TODO Auto-generated constructor stub
	}

	public static String fncValidarUsuario( String pvarUsuario, StringHolder pvarEstadoIdentificador, StringHolder pvarEstadoPassword ) throws Exception
	  {
	    String fncValidarUsuario = "";
	    String mvarRequest = "";
	    String mvarResponse = "";
	    String mvarRetVal = "";
	    XmlDomExtended mobjXMLResponse = null;
	    //
	    //
	    mvarRequest = "<Request>" + "<DEFINICION>" + nbwsA_Ingresar.mcteParam_XMLSQLGENVALUSR + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
	    //
	
	    nbwsA_SQLGenerico mobjClass = new nbwsA_SQLGenerico();
	    StringHolder wvarResponseSH = new StringHolder();
	    mobjClass.IAction_Execute(mvarRequest, wvarResponseSH, "" );
	    mvarResponse = wvarResponseSH.getValue();
	
	    //
	    mobjXMLResponse = new XmlDomExtended();
	    mobjXMLResponse.loadXML( mvarResponse );
	    //
	    //Analiza resultado del SP
	    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
	    {
	      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
	      {
	        //
	        if( ( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//EXISTE" )  ).equals( "S" )) || ( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//EXISTE" )  ).equals( "N" )) )
	        {
	          mvarRetVal = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//ESTADO" )  );
	          pvarEstadoIdentificador.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//ESTADOIDENTIFICADOR" )  ) );
	          pvarEstadoPassword.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//ESTADOPASSWORD" )  ) );
	        }
	      }
	    }
	    //
	    if( mvarRetVal.equals( "" ) )
	    {
	      mvarRetVal = "ERR";
	    }
	    //
	    fncValidarUsuario = mvarRetVal;
	    mobjClass = null;
	    mobjXMLResponse = null;
	    //
	    return fncValidarUsuario;
	  }

	public static String fncObtenerIdentificador( String pvarUsuario, StringHolder pvarDocumtip, StringHolder pvarDocumdat, StringHolder pvarRCCPrevio ) throws Exception
	  {
	    String fncObtenerIdentificador = "";
	    String mvarRequest = "";
	    String mvarResponse = "";
	    XmlDomExtended mobjXMLResponse = null;
	    //
	    //
	    mvarRequest = "<Request>" + "<DEFINICION>" + nbwsA_Ingresar.mcteParam_XMLSQLGENOBTIDN + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
	    //
	    nbwsA_SQLGenerico mobjClass = new nbwsA_SQLGenerico();
	    StringHolder wvarResponseSH = new StringHolder();
	    mobjClass.IAction_Execute(mvarRequest, wvarResponseSH, "" );
	    mvarResponse = wvarResponseSH.getValue();
	
	    //
	    mobjXMLResponse = new XmlDomExtended();
	    mobjXMLResponse.loadXML( mvarResponse );
	    //
	    //Analiza resultado del SP
	    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
	    {
	      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
	      {
	        pvarDocumtip.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//DOCUMTIP" )  ) );
	        pvarDocumdat.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//DOCUMDAT" )  ) );
	        pvarRCCPrevio.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//RCCULTIMO" )  ) );
	      }
	    }
	    //
	    if( pvarDocumdat.toString().equals( "" ) )
	    {
	      pvarDocumdat.set( "ERR" );
	    }
	    //
	    mobjClass = null;
	    mobjXMLResponse = null;
	    //
	    return fncObtenerIdentificador;
	  }

}
