package com.qbe.services.segurosOnline.impl;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.logging.Logger;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.qbe.services.format.Formater;
import com.qbe.services.mqgeneric.impl.AbstractMQMensaje;
import com.qbe.services.mqgeneric.impl.IModGeneral;
import com.qbe.vbcompat.format.FormatMapper;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import com.qbe.vbcompat.format.VBFixesUtil;
/**
 * Objetos del FrameWork
 */

public class NBWSA_MQGenericoAIS extends AbstractMQMensaje
{
	private static Logger logger = Logger.getLogger(NBWSA_MQGenericoAIS.class.getName());

	private static final ModGeneral MOD_GENERAL = new ModGeneral();

	/**
	 * Constructor por default
	 */
	public NBWSA_MQGenericoAIS() {
		// Seteo de directorios
		mcteClassName = "nbwsA_Transacciones.nbwsA_MQGenericoAIS";
		mcteSubDirName = "DefinicionesAIS";
	}
 
	@Override
	protected org.w3c.dom.Node GetNodoParseado( String pvarstrToParse, org.w3c.dom.Node pobjXMLDefinicion, int pvarCantidadRegistros ) throws Exception
	{
		org.w3c.dom.Node GetNodoParseado = null;
		org.w3c.dom.Element wobjNodo = null;
		org.w3c.dom.Element wobjNewNodo = null;
		org.w3c.dom.Element wobjNewNodoCopy = null;
		org.w3c.dom.Element wobjNodoContenedor = null;
		XmlDomExtended wobjXMLDOM = null;
		String wvarstrXML = "";
		int wvarCounter = 0;
		VbScript_RegExp wobjRegExp = null;
		MatchCollection wobjColMatch = null;
		Match wobjMatch = null;
		String wvarParseEvalString = "";
		int wvarCcurrRegistro = 0;
		Variant wvarLastValue = new Variant();
		String wvarNombreOcurrencia = "";
		boolean wvarAreaConDatos = false;
		boolean esCData = false;
		//
		//
		//RegExp
		//MatchCollection
		//Match
		// ************************************
		//Conversion del Area de Salida en XML
		// ************************************
		//
		wobjXMLDOM = new XmlDomExtended();
		wobjRegExp = new VbScript_RegExp();

		wvarParseEvalString = Strings.replace( pvarstrToParse, " ", "_" );
		wvarstrXML = XmlDomExtended.getText( pobjXMLDefinicion.getAttributes().getNamedItem( "Nombre" ) );
		wobjXMLDOM.loadXML( "<" + wvarstrXML + "></" + wvarstrXML + ">" );

		if( ! (pobjXMLDefinicion.getAttributes().getNamedItem( "NombrePorOcurrencia" ) == (org.w3c.dom.Node) null) )
		{
			wvarNombreOcurrencia = XmlDomExtended.getText( pobjXMLDefinicion.getAttributes().getNamedItem( "NombrePorOcurrencia" ) );
		}
		else
		{
			wvarNombreOcurrencia = "";
		}
		//
		// *******************************
		//Evaluo el area segun el Pattern
		// *******************************
		wobjRegExp.setGlobal(true); 
		wobjRegExp.setPattern( XmlDomExtended.getText( pobjXMLDefinicion.getAttributes().getNamedItem( "Pattern" ) ) );
		wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
		//
		// *************************************
		//Recorro el resultado de la evaluacion
		// *************************************
		wvarCcurrRegistro = 0;
		wvarLastValue.set( "---" );
		wvarAreaConDatos = true;
		for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
		{
			// En el original:     For Each wobjMatch In wobjColMatch
			wobjMatch = wobjColMatch.next();

			//Cada Registro Encontrado
			if( ! ((wvarCcurrRegistro < pvarCantidadRegistros)) && (pvarCantidadRegistros != -1) )
			{
				break;
			}
			//
			if( !wvarNombreOcurrencia.equals( "" ) )
			{
				wobjNodoContenedor = wobjXMLDOM.getDocument().createElement( wvarNombreOcurrencia );
				wobjXMLDOM.selectSingleNode( "//" + wvarstrXML ).appendChild( wobjNodoContenedor );
			}
			else
			{
				wobjNodoContenedor = (org.w3c.dom.Element) wobjXMLDOM.selectSingleNode( "//" + wvarstrXML );
			}
			//
			wvarCcurrRegistro = wvarCcurrRegistro + 1;
			//
			for( wvarCounter = 0; wvarCounter <= (pobjXMLDefinicion.getChildNodes().getLength() - 1); wvarCounter++ )
			{
				//Aca esta la definicion de cada campo
				//
				wobjNodo = (org.w3c.dom.Element) pobjXMLDefinicion.getChildNodes().item( wvarCounter );
				if( wobjNodo.getChildNodes().getLength() == 0 )
				{
					esCData = (wobjNodo.getAttributes().getNamedItem( "CData" ) != null && wobjNodo.getAttributes().getNamedItem( "CData" ).getNodeValue().equals("SI"));
					wvarLastValue.set(Strings.replace(wobjMatch.SubMatches(wvarCounter), "_", " ").trim()); 
					if( Strings.toUpperCase( XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "Visible" ) ) ).equals( "SI" ) )
					{
						wobjNewNodo = wobjXMLDOM.getDocument().createElement( XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "Nombre" ) ) );
						wobjNodoContenedor.appendChild( wobjNewNodo );
						//

						if(XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "ENTERO" ) )
						{
							//La transformación del VBJ usa VB,val() y String.value(), que toman un double y por lo tanto genera para un entero x, x.0
							//Parece redundante pero el String puede venir como " 011" y tengo que convertirlo a "11"
							String newValue = "";
							if (!"".equals(wvarLastValue.toString().trim())) {
								try {
									//Esto lo hago, para que solo lo convierta a int, si viene informado, en caso contrario tendrá un 0 por default
									int parsedInt = Integer.parseInt(Strings
											.trim(wvarLastValue.toString()));
									newValue += parsedInt;
								} catch (NumberFormatException e) {
									newValue += "0";
								}
							} else {
								newValue += "0";
							}
							if (esCData) {
								XmlDomExtended.setCDATAText(wobjNewNodo, newValue);
							} else {
								XmlDomExtended.setText( wobjNewNodo, newValue );
							}
						}
						else if(XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "DECIMAL" ) )
						{
							String newValue = String.valueOf( VBFixesUtil.val( wvarLastValue.toString() ) / Math.pow( 10, VBFixesUtil.val( XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "Decimales" ) ) ) ) );
							//Si el número no tiene decimales, VB lo formatea SIN .0 al final, mientras que en Java como String.valueOf recibe un double lo escribe con .0
							if ( newValue.endsWith(".0")) newValue = newValue.substring(0, newValue.length()-2);
							
							if (esCData) {
								XmlDomExtended.setCDATAText(wobjNewNodo, newValue);
							} else {
								XmlDomExtended.setText( wobjNewNodo, newValue);
							}
						}
						else if(XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "FECHA" ) )
						{
							if (esCData) {
								XmlDomExtended.setCDATAText(wobjNewNodo, Strings.right( wvarLastValue.toString(), 2 ) + "/" + Strings.mid( wvarLastValue.toString(), 5, 2 ) + "/" + Strings.left( wvarLastValue.toString(), 4 ) );
							} else {
								XmlDomExtended.setText( wobjNewNodo, Strings.right( wvarLastValue.toString(), 2 ) + "/" + Strings.mid( wvarLastValue.toString(), 5, 2 ) + "/" + Strings.left( wvarLastValue.toString(), 4 ) );
							}
						}
						 // El siguiente If se agrega para que no aparezca el valor dos veces cuando esté el CDATA
			            else if (!esCData)
						{
							XmlDomExtended.setText( wobjNewNodo, Strings.trim( wvarLastValue.toString() ) );
						} else {
							XmlDomExtended.setCDATAText(wobjNewNodo, wvarLastValue.toString().trim() );
						}
						//
						wobjNewNodoCopy = (org.w3c.dom.Element) null;
						if( ! (wobjNodo.getAttributes().getNamedItem( "CopyTo" ) == (org.w3c.dom.Node) null) )
						{
							wobjNewNodoCopy = wobjXMLDOM.getDocument().createElement( XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "CopyTo" ) ) );
							wobjNodoContenedor.appendChild( wobjNewNodoCopy );
							XmlDomExtended.setText( wobjNewNodoCopy, XmlDomExtended.getText( wobjNewNodo ) );
						}
						//
						//Verifico si se solicita algun Formato en especial del Origen
						if( ! (wobjNodo.getAttributes().getNamedItem( "FormatOuput" ) == (org.w3c.dom.Node) null) )
						{
							String format = XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "FormatOuput" ));
							String toBeFormatted = XmlDomExtended.getText( wobjNewNodo );
		            		String output = toBeFormatted;
			            	if (XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "TEXTO" ) ){
			            		output = FormatMapper.formatStringWithVBFormat(toBeFormatted, format);
			            	} else {
			            		//Supongo DECIMAL
				            	format = FormatMapper.mapVBNumberFormat(format);
				                DecimalFormat formatter = new DecimalFormat(format, new DecimalFormatSymbols(Locale.ENGLISH));
				                double d = Double.valueOf(toBeFormatted);
				                output = formatter.format(d);
			            	}
							XmlDomExtended.setText( wobjNewNodo, output );
						}
						//
						//Verifico si se solicita algun Formato en especial del Destino
						if( ! ((wobjNodo.getAttributes().getNamedItem( "FormatOuputCopy" ) == (org.w3c.dom.Node) null)) && ! ((wobjNewNodoCopy == (org.w3c.dom.Element) null)) )
						{
							XmlDomExtended.setText( wobjNewNodoCopy, Formater.doubleFormat( XmlDomExtended.getText( wobjNewNodo ), XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "FormatOuputCopy" ) ) ) );
						}
						//
					}
					if( XmlDomExtended.Node_selectNodes(wobjNodo, "self::node()[./@Obligatorio='SI']").getLength() != 0 )
					{
						if( (XmlDomExtended.getText( wobjNewNodo ).equals( "" )) || (XmlDomExtended.getText( wobjNewNodo ).equals( "0" )) )
						{
							//El Dato Obligatorio no esta informado, por lo que no debe retornar ni este registro ni todos sus hijos
							wvarAreaConDatos = false;
						}
					}
				}
				else
				{
					//El nodo tiene hijos por lo que se debe procesar con un Pattern nuevo
					//MC - 30/08/2007 Si el valor anterior es muy grande lo reemplaza por -1
					if( Strings.len( wvarLastValue.toString() ) > 10 )
		          {
		            wvarLastValue.set( -1 );
		          }
					if( ! (wvarLastValue.isNumeric()) ) //FALTA Revisar si esto OK
					{
						wvarLastValue.set( -1 );
					}
					//error: function 'SubMatches' was not found.
					//Asi estaba antes: unsup: Set wobjNewNodo = GetNodoParseado(wobjMatch.SubMatches(wvarCounter), wobjNodo, Val(wvarLastValue))
					wobjNewNodo = (Element) GetNodoParseado(wobjMatch.SubMatches(wvarCounter), wobjNodo, wvarLastValue.toInt());
					if( ! (wobjNewNodo == (org.w3c.dom.Element) null) )
					{
							//Agregado porque si lo mando directo, salta org.w3c.dom.DOMException: WRONG_DOCUMENT_ERR: A node is used in a different document than the one that created it.
							Node importedNode = wobjXMLDOM.selectSingleNode( "//" + wvarstrXML ).getOwnerDocument().importNode(wobjNewNodo, true);
							wobjXMLDOM.selectSingleNode( "//" + wvarstrXML ).appendChild( importedNode );
					}
				}
			}
		}

		if( wvarAreaConDatos )
		{
			GetNodoParseado = wobjXMLDOM.getDocument().getChildNodes().item( 0 );
		}

		return GetNodoParseado;
	}
	
	@Override
	protected String GetDatoFormateado( org.w3c.dom.Node pobjXMLContenedor, String pvarPathDato, String pvarTipoDato, org.w3c.dom.Node pobjLongitud, org.w3c.dom.Node pobjDecimales, org.w3c.dom.Node pobjDefault, org.w3c.dom.Node pobjObligatorio ) throws Exception
	{
		String GetDatoFormateado = "";
		org.w3c.dom.Node wobjNodoValor = null;
		String wvarDatoValue = "";
		int wvarCounter = 0;
		double wvarCampoNumerico = 0.0;
		String wvarErrorValidacion = "";
		String[] Arr = null;

		if( ! (pobjXMLContenedor == (org.w3c.dom.Node) null) )
		{
			//
			wvarErrorValidacion = getModGeneral().GetErrorInformacionDato( pobjXMLContenedor, pvarPathDato, pvarTipoDato, pobjLongitud, pobjDecimales, pobjDefault, pobjObligatorio );
			if( ! (wvarErrorValidacion.equals( "" )) )
			{
				mvarCancelacionManual = true;
				//FIXME formato del mensaje
				throw new ComponentExecutionException("-1 -- " + mvarConsultaRealizada +  wvarErrorValidacion );
			}
			wobjNodoValor = XmlDomExtended.Node_selectSingleNode(pobjXMLContenedor, "./" + Strings.mid( pvarPathDato, 3 ));
			if( wobjNodoValor == (org.w3c.dom.Node) null )
			{
				if( ! (pobjDefault == (org.w3c.dom.Node) null) )
				{
					wvarDatoValue = XmlDomExtended.getText( pobjDefault );
				}
			}
			else
			{
				wvarDatoValue = XmlDomExtended.getText( wobjNodoValor );
			}
		}
		//

		if( pvarTipoDato.equals( "TEXTO" ) )
		{
			//Dato del Tipo String
			GetDatoFormateado = Strings.left( wvarDatoValue + Strings.space( (int)Math.rint( VBFixesUtil.val( XmlDomExtended.getText( pobjLongitud ) ) ) ), (int)Math.rint( VBFixesUtil.val( XmlDomExtended.getText( pobjLongitud ) ) ) );
		}
		else if( pvarTipoDato.equals( "ENTERO" ) )
		{
			//Dato del Tipo Entero
			GetDatoFormateado = getModGeneral().CompleteZero( wvarDatoValue, (int)Math.rint( VBFixesUtil.val( XmlDomExtended.getText( pobjLongitud ) ) ) );
		}
		else if( pvarTipoDato.equals( "DECIMAL" ) )
		{
			//Dato del Tipo "Con Decimales"
			//isNumeric ver http://msdn.microsoft.com/en-us/library/6cd3f6w1(v=vs.80).aspx
			boolean isNumeric = false;
			try {
				Double.parseDouble(wvarDatoValue);
				isNumeric = true;
			} catch (NumberFormatException e) {
			}
			if(  isNumeric)
			{
				wvarCampoNumerico = Obj.toDouble( Strings.replace( wvarDatoValue, ",", "." ) );
				 String parteEnteraConvertida = String.valueOf(Obj.toDouble( Strings.replace( wvarDatoValue, ",", "." )) );
					if ( parteEnteraConvertida.endsWith(".0")) parteEnteraConvertida = parteEnteraConvertida.substring(0, parteEnteraConvertida.length()-2);
					wvarCampoNumerico = Double.valueOf(parteEnteraConvertida);
			}
			for( wvarCounter = 1; wvarCounter <= Obj.toInt( XmlDomExtended.getText( pobjDecimales ) ); wvarCounter++ )
			{
				wvarCampoNumerico = wvarCampoNumerico * 10;
			}
			//El numero se formatea SIN separador decimal.
			DecimalFormat formatter = new DecimalFormat("#");
			String output = formatter.format(wvarCampoNumerico);
			// TODO Lo de abajo se podría reemplazar por una version más nativa de Java, usando un DecimalFormat con patron #### con length(patron)=longitud+decimales
			GetDatoFormateado = getModGeneral().CompleteZero( String.valueOf( output ), (int)Math.rint( VBFixesUtil.val( XmlDomExtended.getText( pobjLongitud ) ) + VBFixesUtil.val( XmlDomExtended.getText( pobjDecimales ) ) ) );
		}
		else if( pvarTipoDato.equals( "FECHA" ) )
		{
			//Dato del Tipo "Fecha" Debe venir del tipo dd/mm/yyyy devuelve YYYYMMDD
			if( wvarDatoValue.matches( "\\d+/\\d+/\\d\\d\\d\\d" ) )
			{
				Arr = Strings.split( wvarDatoValue, "/", -1 );
				GetDatoFormateado = Arr[2] + getModGeneral().CompleteZero( Arr[1], 2 ) + getModGeneral().CompleteZero( Arr[0], 2 );
			}
			else
			{
				GetDatoFormateado = "00000000";
			}
		}
		else if( pvarTipoDato.equals( "TEXTOIZQUIERDA" ) )
		{
			//Dato del Tipo String alineado a la izquierda
			GetDatoFormateado = Strings.right( Strings.space( (int)Math.rint( VBFixesUtil.val( XmlDomExtended.getText( pobjLongitud ) ) ) ) + wvarDatoValue, (int)Math.rint( VBFixesUtil.val( XmlDomExtended.getText( pobjLongitud ) ) ) );
		}
		//
		return GetDatoFormateado;
	}

	@Override
	public IModGeneral getModGeneral() {
		return MOD_GENERAL;
	}
  
}
