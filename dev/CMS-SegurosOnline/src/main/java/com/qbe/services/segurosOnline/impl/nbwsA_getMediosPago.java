package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 * TODO: poner COPYRIGHT
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_getMediosPago implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_getMediosPago";
  static final String mcteParam_COBROTIP = "//COBROTIP";
  static final String mcteParam_COBROTIPDES = "//COBROTIPDES";
  static final String mcteNBWSMediosDePago = "XMLs\\NBWSMediosDePago.xml";
  static final String mcteNBWSMediosDePago_XSL = "XSLs\\NBWSMediosDePago.xsl";
  static final String mcteNBWSMediosDePago_Equivalencias = "XMLs\\NBWSMediosDePago_Equivalencias.xml";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLMediosDePago = null;
    XmlDomExtended wobjNBWSMediosDePago = null;
    XmlDomExtended wobjNBWSMediosDePago_Equivalencias = null;
    org.w3c.dom.Element oNodo = null;
    org.w3c.dom.Element oOPTION = null;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    HSBCInterfaces.IAction wobjClass = null;
    String wvarCOBROTIP = "";
    String wvarCOBROTIPDES = "";
    String wvarPREGUNTA = "";
    String wvarRespuesta = "";
    int wvarMEDIO_PAGO_COD = 0;
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Carga XML de entrada
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      wvarCOBROTIP = Strings.trim( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_COBROTIP )  ) );
      wvarCOBROTIPDES = Strings.trim( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_COBROTIPDES )  ) );
      //
      //Levanta el XML de medios de pago
      wvarStep = 30;
      wobjNBWSMediosDePago = new XmlDomExtended();
      wobjNBWSMediosDePago.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteNBWSMediosDePago));
      //
      //Levanta el XML de equivalencias
      wvarStep = 40;
      wobjNBWSMediosDePago_Equivalencias = new XmlDomExtended();
      wobjNBWSMediosDePago_Equivalencias.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteNBWSMediosDePago_Equivalencias));

      //Levanta XSL para armar el combo de salida
      wvarStep = 50;
      wobjXSLMediosDePago = new XmlDomExtended();
      wobjXSLMediosDePago.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteNBWSMediosDePago_XSL));
      //
      //Transforma el XML en el combo de opciones
      wvarStep = 60;
      wobjNBWSMediosDePago.loadXML( /*unsup wobjNBWSMediosDePago.transformNode( wobjXSLMediosDePago ) */.toString() );
      //
      //Manda el COBROTIP que vino en el GRAN MENSAJE como valor para la verificaci�n del PID dentro del combo
      wvarStep = 70;
      if( ! (wobjNBWSMediosDePago_Equivalencias.selectSingleNode( ("EQUIVALENCIAS/EQUIVALENCIA[COBROTIP='" + wvarCOBROTIP + "']/MEDIO_PAGO_COD") )  == (org.w3c.dom.Node) null) )
      {
        //Si existe en el xml de equivalencias, levanta el valor de ah�.
        wvarMEDIO_PAGO_COD = Obj.toInt( XmlDomExtended .getText( wobjNBWSMediosDePago_Equivalencias.selectSingleNode( "EQUIVALENCIAS/EQUIVALENCIA[COBROTIP='" + wvarCOBROTIP + "']/MEDIO_PAGO_COD" )  ) );
        XmlDomExtended.setText( wobjNBWSMediosDePago.selectSingleNode( "//OPTION[@value = '" + wvarMEDIO_PAGO_COD + "']" ) .getAttributes().item( 0 ), wvarCOBROTIP );
      }
      else
      {
        //Si NO existe en el xml de equivalencias, arma el combo con el COBROTIP y COBROTIPDES que vino del AIS
        //Esto es porque en el AIS pueden dar de alta COBROTIPs y los mismos pueden no existir en la tabla de equivalencias del front, ya que el XML del front nunca se entera si en el AIS se abri� un nuevo COBROTIP.
        oOPTION = wobjNBWSMediosDePago.getDocument().createElement( "OPTION" );
        XmlDomExtended.setText( oOPTION, wvarCOBROTIPDES );
        oOPTION.setAttribute( "value", wvarCOBROTIP );
        /*unsup wobjNBWSMediosDePago.selectSingleNode( "//OPTIONS" ) */.appendChild( oOPTION );
      }

      //
      wvarStep = 80;
      //
      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"></Estado>" + XmlDomExtended.marshal(wobjNBWSMediosDePago.selectSingleNode( "//OPTIONS" )) + "</Response>" );
      //
      wvarStep = 90;
      //
      wobjXMLRequest = null;
      wobjXMLResponse = null;
      wobjNBWSMediosDePago = null;
      wobjNBWSMediosDePago_Equivalencias = null;
      wobjXSLMediosDePago = null;
      oOPTION = (org.w3c.dom.Element) null;
      //
      wvarStep = 100;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = null;
        wobjXMLResponse = null;
        wobjNBWSMediosDePago = null;
        wobjNBWSMediosDePago_Equivalencias = null;
        wobjXSLMediosDePago = null;
        oOPTION = (org.w3c.dom.Element) null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        /*TBD mobjCOM_Context.SetAbort() ;*/
        IAction_Execute = 1;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
