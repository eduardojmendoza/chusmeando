package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 * TODO: poner COPYRIGHT
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_getCupItem implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_getCupItem";
  static final String mcteParam_NROREC = "//NROREC";
  static final String mcteParam_CIAASCOD = "//CIAASCOD";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    Variant vbLogEventTypeWarning = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    //HSBC_ASP.CmdProcessor wobjCmdProcessor = new HSBC_ASP.CmdProcessor();//SE COMENTA, INSTRUCCION COMENTADA EN OT
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarNROREC = "";
    String wvarCIAASCOD = "";
    String wvarFecCatalogo = "";
    String wvarExecReturn = "";
    String wvarDocKey = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Carga XML de entrada
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      wvarNROREC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NROREC )  );
      wvarCIAASCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CIAASCOD )  );
      //
      wvarStep = 30;
      if( wvarCIAASCOD.equals( "0001" ) )
      {
        //Cat�logo de LBA (MA)
        //DA - 25/08/2009: LG dijo en la reuni�n que el cat�logo es el de OVs, no el de MA.
        wvarRequest = "<Request><VALOR_CLAVE>" + wvarNROREC + "</VALOR_CLAVE></Request>";
        wvarRequest = ModGeneral.cmdp_FormatRequest( "lbaw_OVSQLGETCatalogCup", "lbaw_OVSQLGETCatalogCup.biz", wvarRequest );
      }
      else
      {
        //Cat�logo de NYL (MA)
        //DA - 25/08/2009: LG dijo en la reuni�n que el cat�logo es el de OVs, no el de MA.
        wvarRequest = "<Request><DEFINICION>Consulta_CatalogoCup.xml</DEFINICION><VALOR_CLAVE>" + wvarNROREC + "</VALOR_CLAVE></Request>";
        wvarRequest = ModGeneral.cmdp_FormatRequest( "nylw_OVSQLGen", "nylw_OVSQLGen.biz", wvarRequest );
      }
      //
      wvarStep = 40;
      //DOING:
      //wobjCmdProcessor = new HSBC_ASP.CmdProcessor();//SE COMENTA, INSTRUCCION COMENTADA EN OT
      //wvarExecReturn = String.valueOf( wobjCmdProcessor.Execute( wvarRequest, wvarResponse ) );//SE COMENTA, INSTRUCCION COMENTADA EN OT
      //
      wvarStep = 50;
      wobjXMLResponse = new XmlDomExtended();
      wobjXMLResponse.loadXML( wvarResponse );
      //
      wvarStep = 60;
      if( wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null )
      {
        //
        wvarStep = 70;
        Err.getError().setDescription( "Error en COM+ de cat�logo de ColdView" );
        //unsup GoTo ErrorHandler
        //
      }
      else
      {
        if( !XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          //
          wvarStep = 80;
          Err.getError().setDescription( "Error en COM+ de cat�logo de ColdView" );
          //unsup GoTo ErrorHandler
          //
        }
        else
        {
          //
          wvarStep = 90;
          wvarFecCatalogo = XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//FEC_CATALOGO" )  );
          //wobjCmdProcessor = (HSBC_ASP.CmdProcessor) null;//SE COMENTA, INSTRUCCION COMENTADA EN OT
          //
          //DA - 18/09/2009: el DocKey es distinto para LBA y NYL
          wvarStep = 100;
          if( wvarCIAASCOD.equals( "0001" ) )
          {
            wvarDocKey = "LBA_CHOV";
          }
          else
          {
            wvarDocKey = "NYL_CHOV";
          }
          //
          wvarStep = 105;
          wvarRequest = "<Request><CATALOG>LBA - AIS COBRANZAS</CATALOG><PAGENBR></PAGENBR><ISSUEITEMNBR>1</ISSUEITEMNBR><DATEISSUE>" + wvarFecCatalogo + "</DATEISSUE><IDX_BUSQUEDA>" + wvarNROREC + "</IDX_BUSQUEDA><IssueDateFrom>" + wvarFecCatalogo + "</IssueDateFrom><IssueDateTo>" + wvarFecCatalogo + "</IssueDateTo><QueryClause><![CDATA[(factura = '" + wvarNROREC + "')]]></QueryClause><DocKey>" + wvarDocKey + "</DocKey><PDFNoprint>0</PDFNoprint><PDFNomodify>-1</PDFNomodify><PDFNocopy>0</PDFNocopy><PDFNoannots>-1</PDFNoannots><ACCESSWAY>NBWS</ACCESSWAY></Request>";
          wvarRequest = ModGeneral.cmdp_FormatRequest( "GetXMLPDF_LBA", "GetXMLPDF_LBA.biz", wvarRequest );
          //
          wvarStep = 110;
          //wobjCmdProcessor = new HSBC_ASP.CmdProcessor();//SE COMENTA, INSTRUCCION COMENTADA EN OT
          //wvarExecReturn = String.valueOf( wobjCmdProcessor.Execute( wvarRequest, wvarResponse ) );//SE COMENTA, INSTRUCCION COMENTADA EN OT

          wvarStep = 120;
          wobjXMLResponse.loadXML( wvarResponse );

          if( wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null )
          {
            //
            wvarStep = 130;
            Err.getError().setDescription( "Error en el COM+ de recupero de PDF de ColdView" );
            //unsup GoTo ErrorHandler
            //
          }
          else
          {

            if( !Strings.toUpperCase( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ) ).equals( "TRUE" ) )
            {
              //
              wvarStep = 140;
              Err.getError().setDescription( "No se pudo recuperar el PDF de ColdView" );
              //unsup GoTo ErrorHandler
              //
            }
            else
            {
              //
              wvarStep = 150;
              Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"></Estado><PDFSTREAM>" + XmlDomExtended.getText( null /*unsup wobjXMLResponse.getDocument().getDocumentElement().selectSingleNode( "//PDFSTREAM" ) */ ) + "</PDFSTREAM></Response>" );
              //
            }
          }
        }
      }
      //
      wobjXMLRequest = null;
      wobjXMLResponse = null;
      //wobjCmdProcessor = (HSBC_ASP.CmdProcessor) null;//SE COMENTA, INSTRUCCION COMENTADA EN OT
      //
      wvarStep = 160;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = null;
        wobjXMLResponse = null;
        //wobjCmdProcessor = (HSBC_ASP.CmdProcessor) null;//SE COMENTA, INSTRUCCION COMENTADA EN OT
        //
        if( wvarStep == 140 )
        {
          mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeWarning );
        }
        else
        {
          mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        }
        //
        /*TBD mobjCOM_Context.SetAbort() ;*/
        IAction_Execute = 1;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
