package com.qbe.services.segurosOnline.impl;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class nbwsA_setPassword implements VBObjectClass
{
	protected static Logger logger = Logger.getLogger(nbwsA_setPassword.class.getName());
	
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_setPassword";
  static final String mcteParam_USUARIO = "//USUARIO";
  static final String mcteParam_PASSWORD = "//PASSWORD";
  /**
   * Constantes de XML de definiciones para SQL Generico
   */
  static final String mcteParam_XMLSQLGENUPDPAS = "P_NBWS_UpdatePassword.xml";
  static final String mcteParam_XMLSQLGENVALUSR = "P_NBWS_ValidaUsuario.xml";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_USRNOEXISTE = 1;
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error (los números matchean los de la tabla SQL tipoAccesos)
   */
  private String[] mcteMsg_DESCRIPTION = new String[13];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    String wobjRequestSQL = "";
    String wobjResponseSQL = "";
    String wobjRequestAIS = "";
    String wobjResponseAIS = "";
    String wvarUsuario = "";
    String wvarPASSWORD = "";
    int wvarError = 0;
    String wvarEstadoIdentificador = "";
    String wvarEstadoPassword = "";
    String wvarEstadoUsr = "";
    //
    //XML con el request
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializacion de variables
      wvarStep = 10;
      wvarError = mcteMsg_OK;
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_USRNOEXISTE] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_USRNOEXISTE" );
      //
      //Carga del REQUEST
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //Obtiene parametros
      wvarStep = 40;
      wvarStep = 50;
      wvarUsuario = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARIO )  );
      wvarStep = 60;
      wvarPASSWORD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PASSWORD )  );
      //
      //1) Validar existencia de usuario
      wvarStep = 150;
      StringHolder wvarEstadoIdentificadorSH = new StringHolder();
      StringHolder wvarEstadoPasswordSH = new StringHolder();
      wvarEstadoUsr = CommonFunctions.fncValidarUsuario(wvarUsuario, wvarEstadoIdentificadorSH, wvarEstadoPasswordSH);
      wvarEstadoIdentificador  = wvarEstadoIdentificadorSH.getValue();
      wvarEstadoPassword = wvarEstadoPasswordSH.getValue();

      if( wvarEstadoUsr.equals( "ERR" ) )
      {
        wvarError = mcteMsg_USRNOEXISTE;
      }
      //
      //Si el login es previo a la verificacion del RCC
      wvarStep = 190;
      if( wvarError == mcteMsg_OK )
      {
        //
        fncSetPassword(wvarUsuario, wvarPASSWORD);
        //
        wvarStep = 200;
        pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>OK</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[wvarError] + "</MSGRESULTADO>" + "</Response>" );
      }
      else
      {
        wvarStep = 220;
        pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>ERR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[wvarError] + "</MSGRESULTADO>" + "</Response>" );
      }
      //
      //Finaliza y libera objetos
      wvarStep = 500;
      wobjXMLRequest = null;
      //
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        wobjXMLRequest = null;
        //
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }


  private void fncSetPassword( String pvarUsuario, String pvarPassword ) throws Exception
  {
    String mvarRequest = "";
    String mvarResponse = "";
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENUPDPAS + "</DEFINICION>" + "<PASSWORD>" + ModEncryptDecrypt.CapicomEncrypt( Strings.toUpperCase( pvarPassword ) ) + "</PASSWORD>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";

    nbwsA_SQLGenerico sql2 = new nbwsA_SQLGenerico();
    StringHolder mvarResponseSH = new StringHolder();
    sql2.IAction_Execute(mvarRequest, mvarResponseSH, "" );
    mvarResponse = mvarResponseSH.getValue();
    
    logger.log(Level.FINE, mvarResponse);
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
