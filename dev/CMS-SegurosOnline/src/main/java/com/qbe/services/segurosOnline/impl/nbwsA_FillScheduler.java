package com.qbe.services.segurosOnline.impl;
import java.sql.CallableStatement;
import java.sql.Types;
import java.util.Calendar;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.framework.jaxb.Response;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.ado.Command;
import diamondedge.ado.Connection;
import diamondedge.ado.Parameter;
import diamondedge.ado.Recordset;
import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.FileSystem;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
/**
 * Request para probar <Request><INSCTRL>N</INSCTRL></Request>
 * Objetos del FrameWork
 */

public class nbwsA_FillScheduler  extends BaseOSBClient implements VBObjectClass
{
	
	protected static Logger logger = Logger.getLogger(nbwsA_FillScheduler.class.getName());

	/**
	 * Código de usuario usado para distinguir los registros de este scheduler del anterior a la migración.
	 * Usualmente coincide con el que usa el Scheduler, en SchedulerConstants
	 */
	public static final String CODIGO_USUARIO = "SCHEDULER_INT";

	
  /**
   * Implementacion de los objetos
   * Archivo de Configuracion
   */
  static final String mcteEPolizaConfig = "NBWS_ePolizaConfig.xml";
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_FillScheduler";
  /**
   * Store procedure que inserta en las tablas del scheduler
   */
  static final String mcteStoreProcInsBandejaSCH = "P_OSB_SA_BANDEJA_INSERT";
  /**
   *  TBD
   */
  static final String mcteStoreProcInsControl = "P_ALERTAS_INSERT_CONTROL";
  /**
   *  Store procedure que inserta en la tabla de log el envio de ePolizas
   */
  static final String mcteStoreProcInsLog = "P_NBWS_INSERT_ENVIOEPOLIZAS_LOG";
  /**
   * 
   */
  static final String mcteParam_INSCTRL = "//INSCTRL";
  /**
   * 
   */
  static final String mcteOperacion = "FILLSCH";
  
  

  private EventLog mobjEventLog = new EventLog();
  private NODOINSERTASCH mvarNodoInsertaSch = new NODOINSERTASCH();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();

    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse;
    String wvarResponseNYL = "";
    String wvarResponseLBA = "";
    String wvarResult = "";
    XmlDomExtended wobjXMLRespuestas = null;
    org.w3c.dom.Node wobjXMLConfigNode = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLRequest = null;
    int mvarIDCONTROL = 0;
    String mvarINSCTRL = "";
    String mvarResponseAIS = "";
    boolean mvarEnvioMDW = false;
    boolean mvarProcConErrores = false;
    boolean mvarProcesar = false;
    int mvarPendientes = 0;
    String mvarMSGError = "";
    String mvarCODOP = "";
    int wvarCount = 0;
    String wvarFalseAIS = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicialización de variables
      wvarStep = 10;
      mvarINSCTRL = "N";
      //
      //Lectura de parámetros
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 30;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_INSCTRL )  == (org.w3c.dom.Node) null) )
      {
        mvarINSCTRL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSCTRL )  );
      }
      //
      //Registración en Log de inicio de ejecución
      wvarStep = 40;
      //insertLog mcteOperacion, "I-NBWS_FILLSCH - Nueva ejecución"
      wvarRequest = "";


      wvarStep = 50;
      //Póliza de LBA: Arma XML de entrada a la función Multithreading (muchos request)
      wvarRequest = wvarRequest + "<Request id=\"1\" actionCode=\"nbwsA_MQGenericoAIS\" ciaascod=\"0001\">" + "   <DEFINICION>LBA_1187_ListadoePolizas.xml</DEFINICION>" + "   <LNK-CIAASCOD>0001</LNK-CIAASCOD>" + "</Request>";
      wvarStep = 60;
      //Ejecuta la función Multithreading
      //Comentado porque no usamos el multi
//      wvarRequest = "<Request>" + wvarRequest + "</Request>";
//      ModGeneral.cmdp_ExecuteTrnMulti( "nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse );
      logger.log(Level.FINEST, "paso:" + wvarStep);
      wvarResponse = getOsbConnector().executeRequest("nbwsA_MQGenericoAIS", wvarRequest);
      wvarStep = 62;
      logger.log(Level.FINEST, "paso:" + wvarStep);
      wobjXMLRespuestas = new XmlDomExtended();

      wobjXMLRespuestas.loadXML( wvarResponse.toString() );

      //Carga las dos respuestas en un XML.
      wvarStep = 70;
      logger.log(Level.FINEST, "paso:" + wvarStep);
      if( wobjXMLRespuestas.selectSingleNode( "Response/Estado" )  == (org.w3c.dom.Node) null )
      {
        //
        //Falló Response 1
        final String description = "Response 1: falló COM+ de Request 1";
		Err.getError().setDescription( description );
        //FIXEDunsup GoTo ErrorHandler
        //
        throw new Exception(description);
      }
      //
      wvarStep = 80;

      wvarResponseNYL = "";

      wvarStep = 90;

      if( ! (wobjXMLRespuestas.selectSingleNode( "Response/CAMPOS/IMPRESOS" )  == (org.w3c.dom.Node) null) )
      {
        wvarResponseLBA = XmlDomExtended.marshal(wobjXMLRespuestas.selectSingleNode( "Response/CAMPOS" ));
        wvarResponseLBA = Strings.replace( Strings.replace( wvarResponseLBA, "<CAMPOS>", "" ), "</CAMPOS>", "" );

        wvarResponseLBA = "<Response>" + Strings.replace( Strings.replace( wvarResponseLBA, "<IMPRESOS>", "" ), "</IMPRESOS>", "" ) + "</Response>";

      }
      else
      {
        wvarResponseLBA = "";
        wvarFalseAIS = wvarFalseAIS + String.valueOf( (char)(13) ) + XmlDomExtended.getText( wobjXMLRespuestas.selectSingleNode( "Response/Estado/@mensaje" )  );
      }
      wvarStep = 100;

      if( !wvarResponseLBA.equals( "" ) )
      {
        if( ! (insertaImpresoSCH(wvarResponseLBA,"0001") ))
        {
            //FIXEDunsup GoTo ErrorHandler
            throw new Exception("Exception en insertaImpresoSCH");
        }
      }

      if( wvarResponseLBA.equals( "" ) )
      {
        //FIXEDunsup GoTo FalseAIS, agregué el else
      } else {
          wvarStep = 110;
          logger.log(Level.FINEST, "paso:" + wvarStep);

          //Registración en Log de fin de proceso
          if( mvarProcConErrores )
          {
            //insertLog mcteOperacion, "E- Fin NBWS_FILLSCH. Estado: ERROR"
        	  pvarResponse.set(Response.marshaledResultadoFalse("E- Fin NBWS_FILLSCH. Estado: ERROR", mvarMSGError));          }
          else
          {
        	  pvarResponse.set(Response.marshaledResultadoTrue("OK", "0-Fin NBWS_EXESCH.  Estado: OK"));
          }

          wvarStep = 120;
    	  
      }
      FalseAIS: 
      if( wvarResponse.toString().equals( "" ) )
      {
    	 pvarResponse.set(Response.marshaledResultadoFalse(wvarFalseAIS, "<Response_XML></Response_XML><Response_HTML>" + wvarFalseAIS + "</Response_HTML>"));
      }
      //
      //FIN
      //
      wvarStep = 130;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      wvarStep = 140;
      logger.log(Level.FINEST, "paso:" + wvarStep);

      return IAction_Execute;



      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        //Inserta error en Log
        //insertLog mcteOperacion, mcteClassName & " - " & _
        //                      wcteFnName & " - " & _
        //error: syntax error: near " & ":
        //unsup: wvarStep & " - " & _
        //insertLog mcteOperacion, "99- Fin NBWS_FILLSCH. Estado: ABORTADO POR ERROR"
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        pvarResponse.set(Response.marshalWithException("99- Fin NBWS_FILLSCH. Estado: ABORTADO POR ERROR",_e_));

    }
    return IAction_Execute;
  }

  public boolean insertaImpresoSCH( String pvarResponse, String pvarCiaascod ) throws Exception
  {
    boolean insertaImpresoSCH = false;
    int mvarx = 0;
    String wvarNombreArchivos = "";
    XmlDomExtended wobjXMLImpresos = null;
    XmlDomExtended wobjXMLDOMArchivos = null;
    XmlDomExtended wobjXSLResponse = null;
    org.w3c.dom.Node wobjXMLNode = null;
    org.w3c.dom.NodeList wobjXMLImpresosList = null;
    org.w3c.dom.NodeList wobjXMLNombreArchivos = null;
    org.w3c.dom.NodeList wobjXMLFormudes = null;
    org.w3c.dom.Node wobjXMLArchivos = null;
    String wvarResult = "";



    wobjXMLImpresos = new XmlDomExtended();
    /*FIXEDunsup wobjXMLImpresos.setProperty( "SelectionLanguage", "XPath" ) */; //lo único que soportamos es XPath
    wobjXMLImpresos.loadXML( pvarResponse );

    wobjXSLResponse = new XmlDomExtended();
    wobjXSLResponse.loadXML( p_GetXSL());


    wvarResult = wobjXMLImpresos.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );

    wobjXMLDOMArchivos = new XmlDomExtended();
    /*FIXEDunsup wobjXMLDOMArchivos.setProperty( "SelectionLanguage", "XPath" ) */;
    wobjXMLDOMArchivos.loadXML( wvarResult );

    //Está comentado en el original VB
    //wvarResult = wobjXMLImpresos.transformNode(wobjXSLResponse)
    mvarNodoInsertaSch.NOMARCH = "";

    wobjXMLImpresosList = wobjXMLDOMArchivos.selectNodes( "//IMPRESO[not(AGRUPAR=preceding-sibling::IMPRESO/AGRUPAR)]" ) ;

    logger.log(Level.FINEST, "wobjXMLImpresosList.getLength() == " + wobjXMLImpresosList.getLength());
    for( int nwobjXMLNode = 0; nwobjXMLNode < wobjXMLImpresosList.getLength(); nwobjXMLNode++ )
    {
      logger.log(Level.FINEST, "nwobjXMLNode == " + nwobjXMLNode);
      wobjXMLNode = wobjXMLImpresosList.item( nwobjXMLNode );

      mvarNodoInsertaSch.NOMARCH = "";
      mvarNodoInsertaSch.CIAASCOD = pvarCiaascod;
      mvarNodoInsertaSch.RAMOPCOD = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"RAMOPCOD" )  );
      mvarNodoInsertaSch.POLIZANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"POLIZANN" )  );
      mvarNodoInsertaSch.POLIZSEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"POLIZSEC" )  );
      mvarNodoInsertaSch.CERTIPOL = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"CERTIPOL" )  );
      mvarNodoInsertaSch.CERTIANN = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"CERTIANN" )  );
      mvarNodoInsertaSch.CERTISEC = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"CERTISEC" )  );
      mvarNodoInsertaSch.OPERAPOL = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"OPERAPOL" )  );
      mvarNodoInsertaSch.DOCUMTIP = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"DOCUMTIP" )  );
      mvarNodoInsertaSch.DOCUMDAT = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"DOCUMDAT" )  );
      mvarNodoInsertaSch.CLIENAP1 = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"CLIENAP1" )  );
      mvarNodoInsertaSch.CLIENAP2 = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"CLIENAP2" )  );
      mvarNodoInsertaSch.CLIENNOM = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"CLIENNOM" )  );


      wobjXMLNombreArchivos = wobjXMLDOMArchivos.selectNodes( "//IMPRESO[CIAASCOD='" + mvarNodoInsertaSch.CIAASCOD + "' and RAMOPCOD='" + mvarNodoInsertaSch.RAMOPCOD + "' and POLIZANN='" + mvarNodoInsertaSch.POLIZANN + "' and POLIZSEC='" + mvarNodoInsertaSch.POLIZSEC + "' and CERTIPOL='" + mvarNodoInsertaSch.CERTIPOL + "' and CERTIANN='" + mvarNodoInsertaSch.CERTIANN + "' and CERTISEC='" + mvarNodoInsertaSch.CERTISEC + "' and OPERAPOL='" + mvarNodoInsertaSch.OPERAPOL + "' ]/NOMARCH" ) ;

      wobjXMLFormudes = wobjXMLDOMArchivos.selectNodes( "//IMPRESO[CIAASCOD='" + mvarNodoInsertaSch.CIAASCOD + "' and RAMOPCOD='" + mvarNodoInsertaSch.RAMOPCOD + "' and POLIZANN='" + mvarNodoInsertaSch.POLIZANN + "' and POLIZSEC='" + mvarNodoInsertaSch.POLIZSEC + "' and CERTIPOL='" + mvarNodoInsertaSch.CERTIPOL + "' and CERTIANN='" + mvarNodoInsertaSch.CERTIANN + "' and CERTISEC='" + mvarNodoInsertaSch.CERTISEC + "' and OPERAPOL='" + mvarNodoInsertaSch.OPERAPOL + "' ]/FORMUDES" ) ;



      for( mvarx = 0; mvarx <= (wobjXMLNombreArchivos.getLength() - 1); mvarx++ )
      {

        mvarNodoInsertaSch.NOMARCH = mvarNodoInsertaSch.NOMARCH + "<ARCHIVO>" + Strings.replace( XmlDomExtended.marshal(wobjXMLNombreArchivos.item( mvarx )), String.valueOf( (char)(32) ), "_" ) + XmlDomExtended.marshal(wobjXMLFormudes.item( mvarx )) + "</ARCHIVO>";

      }


      mvarNodoInsertaSch.SWSUSCRI = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"SWSUSCRI" )  );
      mvarNodoInsertaSch.MAIL = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"MAIL" )  );
      mvarNodoInsertaSch.CLAVE = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"CLAVE" )  );
      mvarNodoInsertaSch.SWCLAVE = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"SW-CLAVE" )  );
      mvarNodoInsertaSch.SWENDOSO = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"SWENDOSO" )  );
      mvarNodoInsertaSch.OPERATIP = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"OPERATIP" )  );
      mvarNodoInsertaSch.ESTADO = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"ESTADO" )  );
      mvarNodoInsertaSch.CODESTAD = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"CODESTAD" )  );

      if( !mvarNodoInsertaSch.RAMOPCOD.equals( "" ) )
      {
          logger.log(Level.FINEST,"Inserta c/u de los registros en la Bandeja de Jobs");
        if( ! (insertScheduler(mvarNodoInsertaSch)) )
        {
          insertaImpresoSCH = false;
          return insertaImpresoSCH;
        }
      }
    }

    wobjXMLNode = (org.w3c.dom.Node) null;
    wobjXMLImpresos = null;
    insertaImpresoSCH = true;
    return insertaImpresoSCH;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "<xsl:template match='/'>";
    wvarStrXSL = wvarStrXSL + " <Request>";
    wvarStrXSL = wvarStrXSL + "  <xsl:for-each select='//IMPRESO'>";
    wvarStrXSL = wvarStrXSL + "     <xsl:sort select='concat(CIAASCOD,RAMOPCOD,POLIZANN,POLIZSEC,CERTIPOL,CERTIANN,CERTISEC,OPERAPOL)' data-type='text' order='ascending'/>";
    wvarStrXSL = wvarStrXSL + " <xsl:if test='RAMOPCOD != \"\"' >";

    wvarStrXSL = wvarStrXSL + " <IMPRESO>";

    wvarStrXSL = wvarStrXSL + "<AGRUPAR><xsl:value-of select='concat(CIAASCOD,RAMOPCOD,POLIZANN,POLIZSEC,CERTIPOL,CERTIANN,CERTISEC,OPERAPOL)'/></AGRUPAR>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CIAASCOD'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='RAMOPCOD'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='POLIZANN'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='POLIZSEC'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CERTIPOL'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CERTIANN'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CERTISEC'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='OPERAPOL'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='FORMUDES'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='DOCUMTIP'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='DOCUMDAT'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CLIENAP1'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CLIENAP2'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CLIENNOM'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='NOMARCH'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='SWSUSCRI'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='MAIL'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CLAVE'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='SW-CLAVE'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='SWENDOSO'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='OPERATIP'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='ESTADO'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CODESTAD'/>";
    wvarStrXSL = wvarStrXSL + " </IMPRESO>";
    wvarStrXSL = wvarStrXSL + " </xsl:if >";

    wvarStrXSL = wvarStrXSL + "   </xsl:for-each>";
    wvarStrXSL = wvarStrXSL + " </Request>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private boolean insertScheduler( NODOINSERTASCH pvarNodo) throws Exception
  {
    boolean insertScheduler = false;
    String wvarCodusu = "";
    String wvarAplicacion = "";
    String wvarDataIn = "";
    //
    //
    wvarAplicacion = "NBWS_ePoliza_Exe";

//    wvarCodusu = "NBWSFILLSCH";
    wvarCodusu = CODIGO_USUARIO;
    wvarDataIn = "<CIAASCOD>" + pvarNodo.CIAASCOD + "</CIAASCOD>" + "<RAMOPCOD>" + pvarNodo.RAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + pvarNodo.POLIZANN + "</POLIZANN>" + "<POLIZSEC>" + pvarNodo.POLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + pvarNodo.CERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + pvarNodo.CERTIANN + "</CERTIANN>" + "<CERTISEC>" + pvarNodo.CERTISEC + "</CERTISEC>" + "<OPERAPOL>" + pvarNodo.OPERAPOL + "</OPERAPOL>" + "<DOCUMTIP>" + pvarNodo.DOCUMTIP + "</DOCUMTIP>" + "<DOCUMDAT>" + pvarNodo.DOCUMDAT + "</DOCUMDAT>" + "<CLIENAP1>" + pvarNodo.CLIENAP1 + "</CLIENAP1>" + "<CLIENAP2>" + pvarNodo.CLIENAP2 + "</CLIENAP2>" + "<CLIENNOM>" + pvarNodo.CLIENNOM + "</CLIENNOM>" + "<ARCHIVOS>" + pvarNodo.NOMARCH + "</ARCHIVOS>" + "<FORMUDES>" + pvarNodo.FORMUDES + "</FORMUDES>" + "<SWSUSCRI>" + pvarNodo.SWSUSCRI + "</SWSUSCRI>" + "<MAIL>" + pvarNodo.MAIL + "</MAIL>" + "<CLAVE>" + pvarNodo.CLAVE + "</CLAVE>" + "<SW-CLAVE>" + pvarNodo.SWCLAVE + "</SW-CLAVE>" + "<SWENDOSO>" + pvarNodo.SWENDOSO + "</SWENDOSO>" + "<OPERATIP>" + pvarNodo.OPERATIP + "</OPERATIP>" + "<ESTADO>" + pvarNodo.ESTADO + "</ESTADO>" + "<CODESTAD>" + pvarNodo.CODESTAD + "</CODESTAD>";

    wvarDataIn = Strings.replace( wvarDataIn, "&", "" );
    insertScheduler = insertBandejaSCH(wvarAplicacion,wvarCodusu,wvarDataIn);

    return insertScheduler;
  }

  private boolean insertBandejaSCH( String pvarAplicacion, String pvarCODUSU, String pvarDataIn ) throws Exception
  {
    boolean insertBandejaSCH = false;

    java.sql.Connection jdbcConn = null;
    try {
        JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
        jdbcConn = connFactory.getJDBCConnection(ModGeneral.gcteDBACTIONS);
    
	String sp = String.format("{ ? = call %s(?,?,?,?,?,?)}", mcteStoreProcInsBandejaSCH);
	CallableStatement cs = jdbcConn.prepareCall(sp);
	
	int paramIndex = 1;
	cs.registerOutParameter(paramIndex++, Types.INTEGER);
	
//	"(APLIC   VARCHAR(20),     	-- Identificador de aplicación  \n" +
	cs.setString(paramIndex++, pvarAplicacion);
	
//	"SOLICITUD  INT,       	-- Numero de Solicitud  \n" +
	cs.setInt(paramIndex++, 0);
	
//	"PASO   VARCHAR(20),    	-- Paso que identifica la Tarea a ejecutar  \n" +
	cs.setString(paramIndex++, "100");
	
//	"FECHAEXEC DATETIME,		-- Fecha de ejecución del job\n" +
	cs.setDate(paramIndex++, new java.sql.Date(Calendar.getInstance().getTime().getTime())); // Sucks
	
//	"DATAIN   VARCHAR(5000),    	-- Data para el xml de entrada al IAction  \n" +
	//Limitado a 4000 porque el driver JDBC no soporta VARCHAR > 4000.
	//Ver release.txt del driver
	cs.setString(paramIndex++, Strings.left( pvarDataIn, 4000 ));
	
//	"COD_USU  VARCHAR(20)     	-- Usuario que inserto la tarea  \n" +
	cs.setString(paramIndex++, pvarCODUSU);
	
	cs.execute();

	int returnValue = cs.getInt(1);

	//Controlamos la respuesta del SQL
    if ( returnValue >= 0 )
    {
      insertBandejaSCH = true;
    }
    else
    {
      insertBandejaSCH = false;
    }
    //
    cs.close();
    } finally {
        try {
            if (jdbcConn != null)
                jdbcConn.close();
        } catch (Exception e) {
            logger.log(Level.WARNING, "Exception al hacer un close", e);
        }
    }
    return insertBandejaSCH;
  }

  private Variant insertLog( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    Variant insertLog = new Variant();
    XmlDomExtended wobjXMLConfig = null;
    boolean wvarReqRespInTxt = false;
    int mvarDebugCode = 0;
    //
    //
    wvarReqRespInTxt = false;
    //
    wobjXMLConfig = new XmlDomExtended();
    wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteEPolizaConfig ));
    //1=Debug to BD - 2=Debug to BD and File sin request/Responses - 3=Debug to BD andFile completo
    mvarDebugCode = Obj.toInt( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//DEBUG" )  ) );
    //
    if( (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<REQUEST>", true ) > 0) || (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<RESPONSE>", true ) > 0) )
    {
      wvarReqRespInTxt = true;
    }
    //
    if( (mvarDebugCode >= 1) && ! (wvarReqRespInTxt) )
    {
      debugToBD(pvarCODOP,pvarDescripcion);
    }
    if( (mvarDebugCode == 3) || ((mvarDebugCode == 2) && ! (wvarReqRespInTxt)) )
    {
      debugToFile(pvarCODOP,pvarDescripcion);
    }
    //
    return insertLog;
  }

  /**
   * FIXME El store procedure que está en la base tiene más parámetros, si esto da error ajustarlo a:
   * P_NBWS_INSERT_ENVIOEPOLIZAS_LOG

	@CODOP       VARCHAR(6),
	@DESCRIPCION VARCHAR(100),
	@CLIENTE     VARCHAR(70),
	@EMAIL       VARCHAR(50),
	@ESTADO      VARCHAR(3),
	@DOCUMTIP    NUMERIC(2) = 0,
	@DOCUMDAT    NUMERIC(11) = 0
   * @param pvarCODOP
   * @param pvarDescripcion
   * @return
   * @throws Exception
   */
  private boolean debugToBD( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    boolean debugToBD = false;
    //
    //
    SimpleJdbcCall call = new SimpleJdbcCall(JDBCConnectionFactory.getDataSourceForUDLName(ModGeneral.gcteDBLOG))
//    wobjDBCmd.setCommandText( mcteStoreProcInsLog );
//    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    .withProcedureName(mcteStoreProcInsLog)
//    .withoutProcedureColumnMetaDataAccess() //No sé si es necesario ponerlo, probar
//    wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( "0" ) );
//    wobjDBCmd.getParameters().append( wobjDBParm );
//    wobjDBParm = (Parameter) null;
    .declareParameters(new SqlOutParameter("RETURN_VALUE", Types.INTEGER))

//    wobjDBParm = new Parameter( "@CODOP", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( Strings.left( pvarCODOP, 6 ) ) );
//    wobjDBCmd.getParameters().append( wobjDBParm );
//    wobjDBParm = (Parameter) null;
    .declareParameters(new SqlInOutParameter("CODOP", Types.CHAR))

//    wobjDBParm = new Parameter( "@DESCRIPCION", AdoConst.adChar, AdoConst.adParamInput, 100, new Variant( Strings.left( pvarDescripcion, 100 ) ) );
//    wobjDBCmd.getParameters().append( wobjDBParm );
//    wobjDBParm = (Parameter) null;
    .declareParameters(new SqlInOutParameter("DESCRIPCION", Types.CHAR));
    
//    wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
    Map<String, Object> out = call.execute(Strings.left( pvarCODOP, 6 ), Strings.left( pvarDescripcion, 100 ));

    Object returnValue = out.get("RETURN_VALUE");
    
    //Controlamos la respuesta del SQL
    debugToBD = Integer.parseInt(returnValue.toString()) >= 0;

    return debugToBD;
  }

  private void debugToFile( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    XmlDomExtended wobjXMLConfig = null;
    String wvarText = "";
    String wvarFileName = "";
    int wvarNroArch = 0;
    //
    //
    wvarText = "(" + DateTime.now() + "-" + DateTime.now() + "):" + pvarDescripcion;

    wvarFileName = "debug-" + pvarCODOP + "-" + DateTime.year( DateTime.now() ) + Strings.right( ("00" + DateTime.month( DateTime.now() )), 2 ) + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) + ".log";

    logger.log(Level.FINE, wvarText);
    wvarNroArch = FileSystem.getFreeFile();
    FileSystem.openAppend("/tmp/DEBUG" + wvarFileName,1);
    FileSystem.out(1).writeValue( wvarText );
    FileSystem.out(1).println();
    FileSystem.close( 1 );
  }

  private void ObjectControl_Activate() throws Exception
  {
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {

  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
