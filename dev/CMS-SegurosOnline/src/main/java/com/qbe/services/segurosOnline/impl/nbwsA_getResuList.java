package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 * TODO: poner COPYRIGHT
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_getResuList implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_getResuList";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_SUPLENUM = "//SUPLENUM";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLRespuestas = null;
    int wvarCount = 0;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarRAMOPCOD = "";
    String wvarPOLIZANN = "";
    String wvarPOLIZSEC = "";
    String wvarCERTIPOL = "";
    String wvarCERTIANN = "";
    String wvarCERTISEC = "";
    String wvarSUPLENUM = "";
    String wvarMesHoy = "";
    String wvarDiaHoy = "";
    Variant wvarPeriodo1 = new Variant();
    Variant wvarPeriodo2 = new Variant();
    Variant wvarPeriodo3 = new Variant();
    String wvarIMPRESO1 = "";
    String wvarIMPRESO2 = "";
    String wvarIMPRESO3 = "";
    String wvarUltimo = "";
    String wvarIMPRESO = "";
    //
    //
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Carga XML de entrada
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      wvarRAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  );
      wvarPOLIZANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN )  );
      wvarPOLIZSEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC )  );
      wvarCERTIPOL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL )  );
      wvarCERTIANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN )  );
      wvarCERTISEC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC )  );
      wvarSUPLENUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SUPLENUM )  );
      //
      //
      wvarStep = 30;
      //Calcula los per�odos
      wvarMesHoy = Strings.right( "00" + DateTime.month( DateTime.now() ), 2 );
      wvarDiaHoy = Strings.right( "00" + DateTime.day( DateTime.now() ), 2 );
      //
      if( Obj.toInt( wvarMesHoy ) < 6 )
      {
        ModGeneral.fncCalculaPeriodos1( wvarPeriodo1, wvarPeriodo2, wvarPeriodo3 );
      }
      else
      {
        if( Obj.toInt( wvarMesHoy ) == 6 )
        {
          if( Obj.toInt( wvarDiaHoy ) == 30 )
          {
            ModGeneral.fncCalculaPeriodos2( wvarPeriodo1, wvarPeriodo2, wvarPeriodo3 );
          }
          else
          {
            ModGeneral.fncCalculaPeriodos1( wvarPeriodo1, wvarPeriodo2, wvarPeriodo3 );
          }
        }
        else
        {
          ModGeneral.fncCalculaPeriodos2( wvarPeriodo1, wvarPeriodo2, wvarPeriodo3 );
        }
      }
      //
      //DA - 14/07/2009: se decidi� entre AG y DA y MC que se mostrar�n las impresoritas hardcodeadas x un tema de performance
      //''''''''''''''''    wvarStep = 40
      //''''''''''''''''    wvarRequest = wvarRequest & "<Request id=""1"" actionCode=""lbaw_OVMWGen"" ciaascod=""0020"">" & _
      // '''''''''''''''''                                "    <DEFINICION>ismRetrieveReportList.xml</DEFINICION>" & _
      //''''''''''''''''                                "    <coldViewPubNode>CVUN_RepositoryEstructure/CVUns_Pub_Root/NYLPrinting/HSBCNYLVida/SegurosComplejos/Semestrales</coldViewPubNode>" & _
      // '''''''''''''''''                                "    <coldViewMetadata>poli=" & wvarRAMOPCOD & "-" & wvarPOLIZANN & "-" & wvarPOLIZSEC & "-" & wvarCERTIPOL & "-" & wvarCERTIANN & "-" & wvarCERTISEC & "-" & wvarSUPLENUM & ";fhas=" & wvarPeriodo1 & "</coldViewMetadata>" & _
      //''''''''''''''''                                "</Request>"
      //''''''''''''''''    wvarStep = 50
      //''''''''''''''''    wvarRequest = wvarRequest & "<Request id=""2"" actionCode=""lbaw_OVMWGen"" ciaascod=""0020"">" & _
      // '''''''''''''''''                                "    <DEFINICION>ismRetrieveReportList.xml</DEFINICION>" & _
      //''''''''''''''''                                "    <coldViewPubNode>CVUN_RepositoryEstructure/CVUns_Pub_Root/NYLPrinting/HSBCNYLVida/SegurosComplejos/Semestrales</coldViewPubNode>" & _
      // '''''''''''''''''                                "    <coldViewMetadata>poli=" & wvarRAMOPCOD & "-" & wvarPOLIZANN & "-" & wvarPOLIZSEC & "-" & wvarCERTIPOL & "-" & wvarCERTIANN & "-" & wvarCERTISEC & "-" & wvarSUPLENUM & ";fhas=" & wvarPeriodo2 & "</coldViewMetadata>" & _
      //''''''''''''''''                                "</Request>"
      //''''''''''''''''    wvarStep = 60
      //''''''''''''''''    wvarRequest = wvarRequest & "<Request id=""3"" actionCode=""lbaw_OVMWGen"" ciaascod=""0020"">" & _
      // '''''''''''''''''                                "    <DEFINICION>ismRetrieveReportList.xml</DEFINICION>" & _
      //''''''''''''''''                                "    <coldViewPubNode>CVUN_RepositoryEstructure/CVUns_Pub_Root/NYLPrinting/HSBCNYLVida/SegurosComplejos/Semestrales</coldViewPubNode>" & _
      // '''''''''''''''''                                "    <coldViewMetadata>poli=" & wvarRAMOPCOD & "-" & wvarPOLIZANN & "-" & wvarPOLIZSEC & "-" & wvarCERTIPOL & "-" & wvarCERTIANN & "-" & wvarCERTISEC & "-" & wvarSUPLENUM & ";fhas=" & wvarPeriodo3 & "</coldViewMetadata>" & _
      //''''''''''''''''                                "</Request>"
      //''''''''''''''''    '
      //''''''''''''''''    wvarStep = 70
      //''''''''''''''''    'Ejecuta la funci�n Multithreading
      //''''''''''''''''    wvarRequest = "<Request>" & wvarRequest & "</Request>"
      //''''''''''''''''    Call cmdp_ExecuteTrnMulti("lbaw_OVMWGen", "lbaw_OVMWGen.biz", wvarRequest, wvarResponse)
      //''''''''''''''''    '
      //''''''''''''''''    wvarStep = 80
      //''''''''''''''''    'Carga las respuestas
      //''''''''''''''''    Set wobjXMLRespuestas = CreateObject("MSXML2.DOMDocument")
      //''''''''''''''''        wobjXMLRespuestas.async = False
      //''''''''''''''''    Call wobjXMLRespuestas.loadXML(wvarResponse)
      //''''''''''''''''    '
      //''''''''''''''''    wvarStep = 90
      //''''''''''''''''    'Verifica si pinch� el COM+
      //''''''''''''''''    If wobjXMLRespuestas.selectSingleNode("//Response") Is Nothing Then
      //''''''''''''''''        '
      //''''''''''''''''        'Pinch� el COM+
      //''''''''''''''''        Err.Description = "Fall� la ejecuci�n de la funci�n Multithreading."
      //''''''''''''''''        GoTo ErrorHandler
      //''''''''''''''''    Else
      //''''''''''''''''        '
      //''''''''''''''''        wvarStep = 100
      //''''''''''''''''        If wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']") Is Nothing Then
      //''''''''''''''''            '
      //''''''''''''''''            'TODO: Pinch� el COM+ para el per�odo 1. Ver qu� hacer.
      //''''''''''''''''            '
      //''''''''''''''''        Else
      //''''''''''''''''            wvarStep = 110
      //''''''''''''''''            If wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']") Is Nothing Then
      //''''''''''''''''                '
      //''''''''''''''''                'TODO: Pinch� el COM+ para el per�odo 2. Ver qu� hacer.
      //''''''''''''''''                '
      //''''''''''''''''            Else
      //''''''''''''''''                wvarStep = 120
      //''''''''''''''''                If wobjXMLRespuestas.selectSingleNode("Response/Response[@id='3']") Is Nothing Then
      //''''''''''''''''                    '
      //''''''''''''''''                    'TODO: Pinch� el COM+ para el per�odo 3. Ver qu� hacer.
      //''''''''''''''''                    '
      //''''''''''''''''                Else
      //''''''''''''''''                    '
      //''''''''''''''''                    'Anduvieron los 3 COM+
      //''''''''''''''''                    wvarStep = 130
      //''''''''''''''''                    wvarIMPRESO1 = ""
      //''''''''''''''''                    If Not wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/retrieveReportListReturn//item") Is Nothing Then
      //''''''''''''''''                        'Recupera el ITEM
      //''''''''''''''''                        wvarUltimo = wobjXMLRespuestas.selectNodes("Response/Response[@id='1']/retrieveReportListReturn//item").length
      //''''''''''''''''                        wvarIMPRESO1 = "<IMPRESO1><PERIODO>" & wvarPeriodo1 & "</PERIODO><ITEM>" & wobjXMLRespuestas.selectNodes("Response/Response[@id='1']/retrieveReportListReturn//item").Item(wvarUltimo).Text & "</ITEM></IMPRESO1>"
      //''''''''''''''''                        '
      //''''''''''''''''                    Else
      //''''''''''''''''                        'no hay item
      //''''''''''''''''                        wvarIMPRESO1 = "<IMPRESO1><PERIODO>" & wvarPeriodo1 & "</PERIODO><ITEM></ITEM></IMPRESO1>"
      //''''''''''''''''                    End If
      //''''''''''''''''                    '
      //''''''''''''''''                    wvarStep = 140
      //''''''''''''''''                    wvarIMPRESO2 = ""
      //''''''''''''''''                    If Not wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']/retrieveReportListReturn//item") Is Nothing Then
      //''''''''''''''''                        'Recupera el ITEM
      //''''''''''''''''                        wvarUltimo = wobjXMLRespuestas.selectNodes("Response/Response[@id='2']/retrieveReportListReturn//item").length
      //''''''''''''''''                        wvarIMPRESO2 = "<IMPRESO2><PERIODO>" & wvarPeriodo2 & "</PERIODO><ITEM>" & wobjXMLRespuestas.selectNodes("Response/Response[@id='2']/retrieveReportListReturn//item").Item(wvarUltimo).Text & "</ITEM></IMPRESO2>"
      //''''''''''''''''                    Else
      //''''''''''''''''                        'no hay item
      //''''''''''''''''                        wvarIMPRESO2 = "<IMPRESO2><PERIODO>" & wvarPeriodo2 & "</PERIODO><ITEM></ITEM></IMPRESO2>"
      //''''''''''''''''                    End If
      //''''''''''''''''                    '
      //''''''''''''''''                    wvarStep = 150
      //''''''''''''''''                    wvarIMPRESO3 = ""
      //''''''''''''''''                    If Not wobjXMLRespuestas.selectSingleNode("Response/Response[@id='3']/retrieveReportListReturn//item") Is Nothing Then
      //''''''''''''''''                        'Recupera el ITEM
      //''''''''''''''''                        wvarUltimo = wobjXMLRespuestas.selectNodes("Response/Response[@id='3']/retrieveReportListReturn//item").length
      //''''''''''''''''                        wvarIMPRESO3 = "<IMPRESO3><PERIODO>" & wvarPeriodo3 & "</PERIODO><ITEM>" & wobjXMLRespuestas.selectNodes("Response/Response[@id='3']/retrieveReportListReturn//item").Item(wvarUltimo).Text & "</ITEM></IMPRESO3>"
      //''''''''''''''''                    Else
      //''''''''''''''''                        'no hay item
      //''''''''''''''''                        wvarIMPRESO3 = "<IMPRESO3><PERIODO>" & wvarPeriodo3 & "</PERIODO><ITEM></ITEM></IMPRESO3>"
      //''''''''''''''''                    End If
      //''''''''''''''''                    '
      //''''''''''''''''                End If
      //''''''''''''''''            End If
      //''''''''''''''''        End If
      //''''''''''''''''    End If
      //
      //
      wvarIMPRESO1 = "<IMPRESO1><PERIODO>" + wvarPeriodo1 + "</PERIODO></IMPRESO1>";
      wvarIMPRESO2 = "<IMPRESO2><PERIODO>" + wvarPeriodo2 + "</PERIODO></IMPRESO2>";
      wvarIMPRESO3 = "<IMPRESO3><PERIODO>" + wvarPeriodo3 + "</PERIODO></IMPRESO3>";
      //
      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"></Estado>" + wvarIMPRESO1 + wvarIMPRESO2 + wvarIMPRESO3 + "</Response>" );
      //
      wobjXMLRequest = null;
      wobjXMLRespuestas = null;
      //
      wvarStep = 190;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = null;
        wobjXMLRespuestas = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        /*TBD mobjCOM_Context.SetAbort() ;*/
        IAction_Execute = 1;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
