package com.qbe.services.segurosOnline.impl;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;


public class nbwsA_sInicio extends BaseOSBClient implements VBObjectClass
{
	protected static Logger logger = Logger.getLogger(nbwsA_sInicio.class.getName());

  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_sInicio";

  public static final String ACTION_CODE = "nbwsA_sInicio";

  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_DOCUMDAT = "//DOCUMDAT";
  static final String mcteParam_SESSION_ID = "//SESSION_ID";
  static final String wcteXSL_sInicio = "XSLs" + File.separator + "sInicio.xsl";
  
  private EventLog mobjEventLog = new EventLog();

  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLRespuestas = null;
    XmlDomExtended wobjXSLSalida = null;
    XmlDomExtended wobjXMLProductosAIS = null;
    XmlDomExtended wobjXMLProductosHabilitados = null;
    XmlDomExtended wobjXMLVendedoresHabilitados = null;
    XmlDomExtended wobjXMLPolizasExcluidas = null;
    XmlDomExtended wobjXMLNBWS_TempFilesServer = null;
    XmlDomExtended wobjXML_AUX = null;
    org.w3c.dom.NodeList wobjXMLProductosAIS_List = null;
    org.w3c.dom.Node wobjXMLProductosAIS_Node = null;
    org.w3c.dom.Element woNodoProductoHabilitadoNBWS = null;
    org.w3c.dom.Element woNodoProductoHabilitadoNavegacion = null;
    org.w3c.dom.Element woNodoPermiteEPoliza = null;
    org.w3c.dom.Element woNodoPolizaExcluida = null;
    org.w3c.dom.Element woNodoPositiveID = null;
    org.w3c.dom.Element woNodoMasDeCincoCert = null;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarResponse_XML = "";
    String wvarResponse_HTML = "";
    String wvarFalseAIS = "";
    String wvarDocumtip = "";
    String wvarDocumdat = "";
    String wvarSESSION_ID = "";

    try 
    {

      wvarStep = 10;
      //Carga XML de entrada
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      wvarDocumtip = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  );
      wvarDocumdat = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMDAT )  );
      //
      wvarStep = 21;
      //DA - 16/10/2009: se agrega esto para guardar en temporal el XML de inicio y evitar XSS en los ASPs.
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_SESSION_ID )  == (org.w3c.dom.Node) null) )
      {
        wvarSESSION_ID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SESSION_ID )  );
      }
      else
      {
        wvarSESSION_ID = "";
      }
      //
      //
      wvarStep = 30;

      wvarRequest = "<Request>" + "   <DEFINICION>LBA_1184_ProductosClientes.xml</DEFINICION>" + "   <AplicarXSL>LBA_1184_ProductosClientes.xsl</AplicarXSL>" + "   <TIPODOCU>" + wvarDocumtip + "</TIPODOCU>" + "   <NUMEDOCU>" + wvarDocumdat + "</NUMEDOCU>" + "</Request>";

      wvarStep = 40;
      wvarResponse = getOsbConnector().executeRequest("nbwsA_MQGenericoAIS", wvarRequest);
      logger.fine(String.format("OSB devuelve wvarResponse = [%s]", wvarResponse));

      //
      wvarStep = 50;
      //Carga las dos respuestas en un XML.
      wobjXMLRespuestas = new XmlDomExtended();
      wobjXMLRespuestas.loadXML( wvarResponse );
      //
      wvarStep = 55;
      //Verifica que no haya pinchado el COM+
      if( wobjXMLRespuestas.selectSingleNode( "Response/Estado" )  == (org.w3c.dom.Node) null )
      {
        final String description = "Error al ejecutar la llamada al 1184, no volvió el Response/Estado";
		logger.severe(description);
        throw new ComponentExecutionException(description);
      }
      String resultado = XmlDomExtended.getText( wobjXMLRespuestas.selectSingleNode( "Response/Estado/@resultado" ));
      if ( "false".equalsIgnoreCase(resultado)) {
    	  //Propago la respuesta con error
    	  Response.set(wvarResponse);
          return 1;    	  
      }
      // Rearma la respuesta
      wvarResponse = "";
      wvarStep = 57;

      if ( ! (wobjXMLRespuestas.selectSingleNode( "Response/CAMPOS/PRODUCTOS" )  == (org.w3c.dom.Node) null) ) {
        wvarResponse = wvarResponse + XmlDomExtended.marshal(wobjXMLRespuestas.selectSingleNode( "Response/CAMPOS/PRODUCTOS" ));
      } else { 
        String mensaje = "La invocación a 1184 retornó una respuesta con resultado=true pero formato inválido, no contiene Response/CAMPOS/PRODUCTOS";
        logger.fine(mensaje);
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"" + mensaje + "\"></Estado><Response_XML>" + wvarResponse + "</Response_XML><Response_HTML>" + wvarFalseAIS + "</Response_HTML></Response>" );
        return 0;
      }
      wvarStep = 59;
      logger.fine(String.format("wvarResponse = [%s]", wvarResponse));
      //Esto pincha si por algún motivo, el PRODUCTOS viene con NS, como pasa cuando vuelve una respuesta de INSIS, porque reemplaza solamente el tag de cierre, dejando un XML inválido
//      wvarResponse = "<Response>" + Strings.replace( Strings.replace( wvarResponse, "<PRODUCTOS>", "" ), "</PRODUCTOS>", "" ) + "</Response>";
      //Reemplazado por manejode XML en vez de Strings:
      wvarResponse = "<Response>" + wvarResponse + "</Response>";
      wobjXMLProductosAIS = new XmlDomExtended();
      wobjXMLProductosAIS.loadXML( wvarResponse );
      Node responseNode = wobjXMLProductosAIS.selectSingleNode( "//Response" );
      NodeList productosChildren = wobjXMLProductosAIS.selectNodes("//Response/PRODUCTOS/*");
      XmlDomExtended.addChildrenToParent(responseNode,productosChildren);
      responseNode.removeChild(wobjXMLProductosAIS.selectSingleNode( "//PRODUCTOS" ));
      
      wvarStep = 61;
      //TODO CONVERT Difícil!
//      wobjXMLProductosAIS.selectSingleNode( "//Response" ).appendChild( wobjXMLRespuestas.selectSingleNode( "//TIPODOCU" ) .cloneNode( true ) );
      Node original;
      
      original = wobjXMLRespuestas.selectSingleNode( "//CLIENSEC" );
      if ( original != null) {
          XmlDomExtended.nodeImportAsChildNode(wobjXMLProductosAIS.selectSingleNode( "//Response" ),original);
      }
      
      original = wobjXMLRespuestas.selectSingleNode( "//TIPODOCU" );
      if ( original != null) {
          XmlDomExtended.nodeImportAsChildNode(wobjXMLProductosAIS.selectSingleNode( "//Response" ),original);
      }
      
      original = wobjXMLRespuestas.selectSingleNode( "//NUMEDOCU" );
      if ( original != null) {
          XmlDomExtended.nodeImportAsChildNode(wobjXMLProductosAIS.selectSingleNode( "//Response" ),original);
      }
      
      original = wobjXMLRespuestas.selectSingleNode( "//CLIENAP1" );
      if ( original != null) {
          XmlDomExtended.nodeImportAsChildNode(wobjXMLProductosAIS.selectSingleNode( "//Response" ),original);
      }
      
      original = wobjXMLRespuestas.selectSingleNode( "//CLIENAP2" );
      if ( original != null) {
          XmlDomExtended.nodeImportAsChildNode(wobjXMLProductosAIS.selectSingleNode( "//Response" ),original);
      }
      
      original = wobjXMLRespuestas.selectSingleNode( "//CLIENNOM" );
      if ( original != null) {
          XmlDomExtended.nodeImportAsChildNode(wobjXMLProductosAIS.selectSingleNode( "//Response" ),original);
      }
      //
      wvarStep = 62;
      //Levanta XML de productos habilitados para NBWS.
      wobjXMLProductosHabilitados = new XmlDomExtended();
      wobjXMLProductosHabilitados.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.wcteProductosHabilitados));
      //
      wvarStep = 70;
      //Levanta en un listado cada producto del cliente
      wobjXMLProductosAIS_List = wobjXMLProductosAIS.selectNodes( "//PRODUCTO" ) ;
      //
      wvarStep = 80;
      //Pide al SQL listado de vendedores de todos los AGENTCLA y AGENTCOD de las pólizas del cliente.
      wobjXMLVendedoresHabilitados = new XmlDomExtended();
      wobjXMLVendedoresHabilitados.loadXML( fncVendedoresHabilitados(wobjXMLProductosAIS_List) );
      //
      wvarStep = 85;

      //DA - 31/08/2009: Pregunta al SQL si las pólizas del cliente están excluídas del NBWS (pólizas encuadradas)
      wobjXMLPolizasExcluidas = new XmlDomExtended();
      wobjXMLPolizasExcluidas.loadXML( fncExclusionDePolizas(wobjXMLProductosAIS_List) );
      //
      wvarStep = 90;
      //Recorre pólizas de clientes.
      for( int nwobjXMLProductosAIS_Node = 0; nwobjXMLProductosAIS_Node < wobjXMLProductosAIS_List.getLength(); nwobjXMLProductosAIS_Node++ )
      {
        wobjXMLProductosAIS_Node = wobjXMLProductosAIS_List.item( nwobjXMLProductosAIS_Node );
        String certisec = XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "CERTISEC" ));
        
        //
        wvarStep = 100;
        //
        woNodoProductoHabilitadoNBWS = wobjXMLProductosAIS.getDocument().createElement( "HABILITADO_NBWS" );
        woNodoProductoHabilitadoNavegacion = wobjXMLProductosAIS.getDocument().createElement( "HABILITADO_NAVEGACION" );
        woNodoPermiteEPoliza = wobjXMLProductosAIS.getDocument().createElement( "HABILITADO_EPOLIZA" );
        woNodoPolizaExcluida = wobjXMLProductosAIS.getDocument().createElement( "POLIZA_EXCLUIDA" );
        woNodoPositiveID = wobjXMLProductosAIS.getDocument().createElement( "POSITIVEID" );
        woNodoMasDeCincoCert = wobjXMLProductosAIS.getDocument().createElement( "MAS_DE_CINCO_CERT" );
        //
        //Verifica si el producto está habilitado en NBWS (xPath al XML de Productos Habilitados)
        if( ! ( wobjXMLProductosHabilitados.selectSingleNode( ("//PRODUCTO[RAMOPCOD='" + 
        		XmlDomExtended.getText(XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "RAMOPCOD" ))  + "']") )  == (org.w3c.dom.Node) null)) 
        {
          //
          wvarStep = 105;
          //Manda el RAMOPDES de la tabla de productos habilitados
          XmlDomExtended.setText(  XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "RAMOPDES" ) , 
        		  XmlDomExtended.getText( 
        				  wobjXMLProductosHabilitados.selectSingleNode( 
        						  "//PRODUCTO[RAMOPCOD='" + 
        				  XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"RAMOPCOD" ) ) + "']/RAMOPDES" ) ) );
          XmlDomExtended.setText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node, "TIPOPROD" ) , 
        		  XmlDomExtended.getText( 
        				  wobjXMLProductosHabilitados.selectSingleNode( 
        						  "//PRODUCTO[RAMOPCOD='" + 
        				  XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"RAMOPCOD" ) ) + "']/TIPOPRODU" ) ) );

          wvarStep = 110;
          //Marca producto habilitado para el NBWS
          XmlDomExtended.setText( woNodoProductoHabilitadoNBWS, "S" );
          wobjXMLProductosAIS_Node.appendChild( woNodoProductoHabilitadoNBWS );

          wvarStep = 115;
          //Indica a qué Positive ID debe redirigir en caso de corresponder
          XmlDomExtended.setText( woNodoPositiveID, XmlDomExtended.getText( wobjXMLProductosHabilitados.selectSingleNode( 
        		  "//PRODUCTO[RAMOPCOD='" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"RAMOPCOD" ) ) + "']/POSITIVEID" ) ) );
          wobjXMLProductosAIS_Node.appendChild( woNodoPositiveID );
          //
          wvarStep = 120;
          //DA - 31/08/2009: Verifica en SQL si la póliza no esté excluída (pólizas encuadradas)
          if( ! (wobjXMLPolizasExcluidas.selectSingleNode( 
        		  ("//EXCLUSION[CIAASCOD='" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"CIAASCOD" ) )  +
        		  "' and RAMOPCOD='" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"RAMOPCOD" ) )  + 
        		  "' and POLIZANN='" + Obj.toInt( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"POLIZANN" ) ) ) + 
        		  "' and POLIZSEC='" + Obj.toInt( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"POLIZSEC" ) ) ) + 
        		  "']") ) == (org.w3c.dom.Node) null) )
          {
            //
            wvarStep = 130;
            //Marca la póliza como excluída del NBWS
            XmlDomExtended.setText( woNodoPolizaExcluida, "S" );
            //
          }
          else
          {
            //
            wvarStep = 140;
            //Marca la póliza como NO excluida del NBWS (es decir, incluída)
            XmlDomExtended.setText( woNodoPolizaExcluida, "N" );
            //
          }
          //
          wvarStep = 145;
          wobjXMLProductosAIS_Node.appendChild( woNodoPolizaExcluida );
          //
          wvarStep = 150;
          //Verifica en SQL si el producto es navegable según quién haya vendido la póliza
          logger.log(Level.FINE, "wobjXMLVendedoresHabilitados es: " +  wobjXMLVendedoresHabilitados.marshal());
          String xpathVendedores = "//VENDEDOR[AGENTCLA='" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"AGENTCLA" ) )  + 
        		  "' and AGENTCOD='" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"AGENTCOD" ) )  + 
        		  "']"; 
          if( ! (wobjXMLVendedoresHabilitados.selectSingleNode( xpathVendedores )  == (org.w3c.dom.Node) null) )
          {
            //
            //Marca producto como habilitado para la navegación
            wvarStep = 151;
            XmlDomExtended.setText( woNodoProductoHabilitadoNavegacion, "S" );
            wobjXMLProductosAIS_Node.appendChild( woNodoProductoHabilitadoNavegacion );
            //
            //Verifica que la póliza en cuestión no tenga mas de 5 certificados
            if( wobjXMLProductosAIS.selectNodes( ("//PRODUCTO[RAMOPCOD='" + 
            XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"RAMOPCOD" ) ) ) + 
            "' and POLIZANN='" + Obj.toInt( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"POLIZANN" ) ) ) + 
  		    "' and POLIZSEC='" + Obj.toInt( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"POLIZSEC" ) ) ) + 
            "']").getLength() > 5 )
            {
              //
              wvarStep = 152;
              //Indica que el producto tiene mas de 5 certificados
              XmlDomExtended.setText( woNodoMasDeCincoCert, "S" );
            }
            else
            {
              wvarStep = 153;
              //Indica que el producto NO tiene mas de 5 certificados
              XmlDomExtended.setText( woNodoMasDeCincoCert, "N" );
            }
            //
            wvarStep = 154;
            wobjXMLProductosAIS_Node.appendChild( woNodoMasDeCincoCert );
            //
          }
          else
          {
            //
            wvarStep = 160;
            //Marca producto NO habilitado para la navegación
            XmlDomExtended.setText( woNodoProductoHabilitadoNavegacion, "S" ); //Parchado según bug 512, le cableamos "S" en ambos casos
            wobjXMLProductosAIS_Node.appendChild( woNodoProductoHabilitadoNavegacion );
            wvarStep = 161;
            //Verifica que la póliza en cuestión no tenga mas de 5 certificados
            if( wobjXMLProductosAIS.selectNodes( ("//PRODUCTO[RAMOPCOD='" + 
                    XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"RAMOPCOD" ) ) ) + 
                    "' and POLIZANN='" + Obj.toInt( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"POLIZANN" ) ) ) + 
          		    "' and POLIZSEC='" + Obj.toInt( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"POLIZSEC" ) ) ) + 
                    "']").getLength() > 5 )
            {
              //
              wvarStep = 162;
              //Indica que el producto tiene mas de 5 certificados
              XmlDomExtended.setText( woNodoMasDeCincoCert, "S" );
            }
            else
            {
              wvarStep = 163;
              //Indica que el producto NO tiene mas de 5 certificados
              XmlDomExtended.setText( woNodoMasDeCincoCert, "N" );
            }
            //
            wvarStep = 164;
            wobjXMLProductosAIS_Node.appendChild( woNodoMasDeCincoCert );
            //
          }
          //
          //Verifica si el producto está habilitado para ePoliza (xPath al XML de Productos Habilitados)
          if( ! (wobjXMLProductosHabilitados.selectSingleNode( ("//PRODUCTO[RAMOPCOD='" + 
          XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"RAMOPCOD" ) ) ) + 
          "' and EPOLIZA = 'S']") == (org.w3c.dom.Node) null) )
          {
            wvarStep = 170;
            //Marca producto habilitado para la ePoliza
            XmlDomExtended.setText( woNodoPermiteEPoliza, "S" );
            //
          }
          else
          {
            wvarStep = 180;
            //Marca producto NO habilitado para la ePoliza
            XmlDomExtended.setText( woNodoPermiteEPoliza, "N" );
            //
          }
          //
          wvarStep = 181;
          wobjXMLProductosAIS_Node.appendChild( woNodoPermiteEPoliza );
          //
        }
        else
        {
          //
          wvarStep = 190;
          //Marca producto NO habilitado para el NBWS (no está en la tabla de Productos Habilitados para NBWS)
          XmlDomExtended.setText( woNodoProductoHabilitadoNBWS, "N" );
          wobjXMLProductosAIS_Node.appendChild( woNodoProductoHabilitadoNBWS );
          //
          wvarStep = 200;
          //Marca producto NO habilitado para la navegación (sin pasar por la validación)
          XmlDomExtended.setText( woNodoProductoHabilitadoNavegacion, "N" );
          wobjXMLProductosAIS_Node.appendChild( woNodoProductoHabilitadoNavegacion );
          //
          wvarStep = 201;
          //Verifica si el producto está habilitado para ePoliza (xPath al XML de Productos Habilitados)
          if( ! (wobjXMLProductosHabilitados.selectSingleNode( ("//PRODUCTO[RAMOPCOD='" + 
          XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"RAMOPCOD" ) ) + 
          "' and EPOLIZA = 'S']") ) == (org.w3c.dom.Node) null) )
          {
            wvarStep = 202;
            //Marca producto habilitado para la ePoliza
            XmlDomExtended.setText( woNodoPermiteEPoliza, "S" );
            //
          }
          else
          {
            wvarStep = 203;
            //Marca producto NO habilitado para la ePoliza
            XmlDomExtended.setText( woNodoPermiteEPoliza, "N" );
            //
          }
          //
          wvarStep = 204;
          wobjXMLProductosAIS_Node.appendChild( woNodoPermiteEPoliza );
          //
          wvarStep = 205;
          //DA - 31/08/2009: Verifica en SQL si la póliza no está excluída (pólizas encuadradas)
          if( ! (wobjXMLPolizasExcluidas.selectSingleNode( ("//EXCLUSION[CIAASCOD='" + 
          XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"CIAASCOD" ) ) + 
          "' and RAMOPCOD='" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"RAMOPCOD" ) ) + 
          "' and POLIZANN='" + Obj.toInt( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"POLIZANN" ) ) ) + 
          "' and POLIZSEC='" + Obj.toInt( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLProductosAIS_Node,"POLIZSEC" ) ) ) + 
          "']") ) == (org.w3c.dom.Node) null) )
          {
            //
            wvarStep = 206;
            //Marca la póliza como excluída del NBWS
            XmlDomExtended.setText( woNodoPolizaExcluida, "S" );
            //
          }
          else
          {
            //
            wvarStep = 207;
            //Marca la póliza como NO excluída del NBWS (es decir, incluída) (!)
            XmlDomExtended.setText( woNodoPolizaExcluida, "N" );
            //
          }
          //
          wvarStep = 208;
          wobjXMLProductosAIS_Node.appendChild( woNodoPolizaExcluida );
          //
        }
        //
        woNodoProductoHabilitadoNBWS = (org.w3c.dom.Element) null;
        woNodoProductoHabilitadoNavegacion = (org.w3c.dom.Element) null;
        woNodoPermiteEPoliza = (org.w3c.dom.Element) null;
        woNodoPositiveID = (org.w3c.dom.Element) null;
        woNodoPolizaExcluida = (org.w3c.dom.Element) null;
        //
        wvarStep = 210;
        //OK que no lo convierta porque itera por índice
        /*unsup wobjXMLProductosAIS_List.nextNode() */;
        //
      }
      //
      wvarStep = 220;
      //Levanta XSL para armar XML de salida
      wobjXSLSalida = new XmlDomExtended();
      wobjXSLSalida.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(wcteXSL_sInicio));
      //
      wvarStep = 240;
      //Devuelve respuesta en formato XML y HTML
      wvarResponse_XML = XmlDomExtended.marshal(wobjXMLProductosAIS.getDocument().getDocumentElement());
      //
      wvarStep = 250;
      wvarResponse_HTML = wobjXMLProductosAIS.transformNode( wobjXSLSalida ).toString();
      //
      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"></Estado><Response_XML>" + wvarResponse_XML + "</Response_XML><Response_HTML><![CDATA[" + wvarResponse_HTML + "]]></Response_HTML></Response>" );
      //
      ClienteNoExiste: 
      wvarStep = 260;
      if( wvarResponse.equals( "" ) )
      {
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarFalseAIS + "\"></Estado><Response_XML></Response_XML><Response_HTML>" + wvarFalseAIS + "</Response_HTML></Response>" );
      }
      //
      wvarStep = 270;
      //DA - 16/10/2009: se agrega esto para guardar en temporal el XML de inicio y evitar XSS en los ASPs.
      //Eliminado rgm. Martín cabrera confirmó que no lo usan
      /*
      if( !wvarSESSION_ID.equals( "" ) )
      {
        //
        wvarStep = 271;
        //Levanta XML de configuración donde se indica la ruta de grabación de archivos temporales.
        wobjXMLNBWS_TempFilesServer = new XmlDomExtended();
        wobjXMLNBWS_TempFilesServer.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.wcteNBWS_TempFilesServer));
        //
        wvarStep = 272;
        //Guarda en temporal el XML de INICIO
        if( ! (wobjXMLProductosAIS == (diamondedge.util.XmlDom) null) )
        {
          wobjXMLProductosAIS.save( XmlDomExtended.getText( wobjXMLNBWS_TempFilesServer.selectSingleNode( "//PATH" ) ) + 
        		  File.separator + "sINICIO_" + wvarSESSION_ID + "" );
        }
        else
        	//Esto es dead code, porque wobjXMLProductosAIS nunca es null. Vino así
        { 
          wvarStep = 273;
          //DA - 30/11/2009: cuando no hay pólizas en NYL ni LBA
          wobjXML_AUX = new XmlDomExtended();
          wobjXML_AUX.loadXML( Response.toString() );
          wobjXML_AUX.save( XmlDomExtended.getText( wobjXMLNBWS_TempFilesServer.selectSingleNode( "//PATH" ) ) + "\\sINICIO_" + wvarSESSION_ID + "" );
          wobjXML_AUX = null;
          //
        }
        //
        wobjXMLNBWS_TempFilesServer = null;
        //
      }
      */
      //
      wvarStep = 280;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      logger.log(Level.SEVERE,"Exception", _e_);
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        Err.clear();
        throw new ComponentExecutionException(_e_);
    }
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * ***************************************************************
   * *** Devuelve un XML con los vendedores habilitados
   * ***************************************************************
   */
  public String fncVendedoresHabilitados( org.w3c.dom.NodeList pobjXMLProductosAIS_List ) throws Exception
  {
	  
	final String mcteParam_DEFINICION_SQL = "P_NBWS_ObtenerVendedores.xml";
	  
    String fncVendedoresHabilitados = "";
    org.w3c.dom.Node oNodoProducto = null;
    String mvarRequest = "";
    String mvarResponse = "";
    XmlDomExtended mobjXMLVendedoresHabilitados = null;
    //
    //
    mvarRequest = "";
    //Arma el XML de entrada
    for( int noNodoProducto = 0; noNodoProducto < pobjXMLProductosAIS_List.getLength(); noNodoProducto++ )
    {
      oNodoProducto = pobjXMLProductosAIS_List.item( noNodoProducto );
      //
      mvarRequest = mvarRequest + "<AGENTE AGENTCLA=\"" + 
    		  XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(oNodoProducto, "AGENTCLA" )  ) + 
    		  "\" AGENTCOD=\"" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(oNodoProducto, "AGENTCOD" )  ) + "\"></AGENTE>";
      /*unsup pobjXMLProductosAIS_List.nextNode() */;
      //
    }
    //
    //Llama al COM+ genérico de SQL con un listado de AGENTCLA y AGENTCOD traidos de los productos del cliente
    mvarRequest = "<Request><DEFINICION>" + mcteParam_DEFINICION_SQL + "</DEFINICION><ROOT>" + mvarRequest + "</ROOT></Request>";
    //
    nbwsA_SQLGenerico sql2 =  new nbwsA_SQLGenerico();
    StringHolder mvarResponseSH = new StringHolder();
    logger.fine(String.format("SQL a ejecutar: [%s] [%s]", mcteParam_DEFINICION_SQL, mvarRequest));
    sql2.IAction_Execute(mvarRequest, mvarResponseSH, "" );
    mvarResponse = mvarResponseSH.getValue();
    //
    //Levanta el resultado de la ejecución para analizar la respuesta
    mobjXMLVendedoresHabilitados = new XmlDomExtended();
    mobjXMLVendedoresHabilitados.loadXML( mvarResponse );
    //
    //Verifica respuesta del COM+
    if( ! (mobjXMLVendedoresHabilitados.selectSingleNode( "//Response" )  == (org.w3c.dom.Node) null) )
    {
      fncVendedoresHabilitados = XmlDomExtended.marshal(mobjXMLVendedoresHabilitados.selectSingleNode( "//Response" ));
    }
    else
    {
      fncVendedoresHabilitados = "<Response></Response>";
    }
    //
    mobjXMLVendedoresHabilitados = null;
    //
    return fncVendedoresHabilitados;
  }

  /**
   * ***************************************************************
   * *** En base a un listado de pólizas, determina si alguna de ellas está excluída para el NBWS
   * ***************************************************************
   */
  public String fncExclusionDePolizas( org.w3c.dom.NodeList pobjXMLProductosAIS_List ) throws Exception
  {
	  
	final String mcteParam_DEFINICION_SQL = "P_NBWS_ObtenerExclusiones.xml";
	  
    String fncExclusionDePolizas = "";
    org.w3c.dom.Node oNodoProducto = null;
    String mvarRequest = "";
    String mvarResponse = "";
    XmlDomExtended mobjXMLPolizasExcluidas = null;
    //
    //
    mvarRequest = "";
    //Arma el XML de entrada
    for( int noNodoProducto = 0; noNodoProducto < pobjXMLProductosAIS_List.getLength(); noNodoProducto++ )
    {
      oNodoProducto = pobjXMLProductosAIS_List.item( noNodoProducto );
      //
      mvarRequest = mvarRequest + 
    		  "<POLIZA CIAASCOD=\"" + 
    		  XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(oNodoProducto, "CIAASCOD" )  ) + 
    		  "\" RAMOPCOD=\"" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(oNodoProducto, "RAMOPCOD" )  ) + "\" POLIZANN=\"" + 
    		  XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(oNodoProducto, "POLIZANN" )  ) + 
    		  "\" POLIZSEC=\"" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(oNodoProducto, "POLIZSEC" )  ) + "\"></POLIZA>";
      /*unsup pobjXMLProductosAIS_List.nextNode() */;
      //
    }
    //
    //Llama al COM+ genérico de SQL con un listado de AGENTCLA y AGENTCOD traidos de los productos del cliente
    mvarRequest = "<Request><DEFINICION>" + mcteParam_DEFINICION_SQL + "</DEFINICION><ROOT>" + mvarRequest + "</ROOT></Request>";
    //
    nbwsA_SQLGenerico sql2 = new nbwsA_SQLGenerico();
    StringHolder mvarResponseSH = new StringHolder();
    sql2.IAction_Execute(mvarRequest, mvarResponseSH, "" );
    mvarResponse = mvarResponseSH.getValue();
    //
    //Levanta el resultado de la ejecución para analizar la respuesta
    mobjXMLPolizasExcluidas = new XmlDomExtended();
    mobjXMLPolizasExcluidas.loadXML( mvarResponse );
    //
    //Verifica respuesta del COM+
    if( ! (mobjXMLPolizasExcluidas.selectSingleNode( "//Response" )  == (org.w3c.dom.Node) null) )
    {
      fncExclusionDePolizas = XmlDomExtended.marshal(mobjXMLPolizasExcluidas.selectSingleNode( "//Response" ));
    }
    else
    {
      fncExclusionDePolizas = "<Response></Response>";
    }
    //
    mobjXMLPolizasExcluidas = null;
    //
    return fncExclusionDePolizas;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
