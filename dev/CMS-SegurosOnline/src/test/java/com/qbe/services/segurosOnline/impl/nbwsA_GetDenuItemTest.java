package com.qbe.services.segurosOnline.impl;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Node;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.vbcompat.base64.Base64Encoding;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

public class nbwsA_GetDenuItemTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Ignore //Este test no lo podemos correr siempre xq coldview de desarrollo se cuelga habitualmente
	public void testIAction_Execute() throws IOException, XmlDomExtendedException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		nbwsA_getDenuItem comp = new nbwsA_getDenuItem();
		String request = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("Request_GetDenuItem_1.xml"));
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(request, sh, "");
		//Chequeo que haya vuelto un PDF
		
		XmlDomExtended responseXml = new XmlDomExtended();
		responseXml.loadXML(sh.getValue());
		assertNotNull(responseXml.selectSingleNode("Response/Estado/@resultado"));
		assertEquals("responseXML con false: " + responseXml, XmlDomExtended.getText(responseXml.selectSingleNode("Response/Estado/@resultado")), "true");
		assertNotNull(responseXml.selectSingleNode("Response/PDFSTREAM"));
		assertTrue("PDFSTREAM no es un PDF", XmlDomExtended.getText(responseXml.selectSingleNode("Response/PDFSTREAM")).startsWith("JVB"));
		
		// Para poder ver el PDF obtenido
		byte[] pdf = Base64Encoding.decode(XmlDomExtended.getText(responseXml.selectSingleNode("Response/PDFSTREAM")));
		FileUtils.writeByteArrayToFile(new File("/tmp/nbwsA_getDenuItem.pdf"), pdf);
		
	}

}
