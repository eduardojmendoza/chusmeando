package com.qbe.services.segurosOnline.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.vbcompat.string.StringHolder;

public class nbwsA_sInicioTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
//	@Ignore //porque en el 844 no está definido el nombre que usa el WSDL en el endpoint ( el hostname del 98 )
	public void testCasoSinVigencia() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		nbwsA_sInicio comp = new nbwsA_sInicio();
		OSBConnector conn = new OSBConnector();
		conn.setServiceURL(TestConstants.ROUTER_OV);
		comp.setOsbConnector(conn);
		String pvarRequest = "<Request><DOCUMTIP>01</DOCUMTIP><DOCUMDAT>31467581</DOCUMDAT></Request>";
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
		String response = sh.getValue();
//		System.out.println(response);
		//TODO Controlar el resultado!
	}

	@Test
//	@Ignore //porque en el 844 no está definido el nombre que usa el WSDL en el endpoint ( el hostname del 98 )
	public void testCasoConPolizas() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		String endpoint = TestConstants.TEST_SERVICE;
		nbwsA_sInicio comp = new nbwsA_sInicio();
		OSBConnector conn = new OSBConnector();
		conn.setServiceURL(endpoint);
		comp.setOsbConnector(conn);
		String pvarRequest = "<Request><DOCUMTIP>01</DOCUMTIP><DOCUMDAT>18069158</DOCUMDAT><MAIL>martin.cabrera@zurich.com.ar</MAIL></Request>";
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
		String response = sh.getValue();
//		System.out.println(response);
		//TODO Controlar el resultado!
	}

	@Test
	public void testCasoBug112() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
//		String endpoint = TestConstants.TEST_SERVICE;
//		String endpoint = "http://localhost:7001/CMS-fewebservices/MigratedComponentService";
		String endpoint = "http://10.1.10.98:8011/OV/Proxy/CMSSvc?wsdl";
		nbwsA_sInicio comp = new nbwsA_sInicio();
		OSBConnector conn = new OSBConnector();
		conn.setServiceURL(endpoint);
		comp.setOsbConnector(conn);
		String pvarRequest = "<Request><DOCUMTIP>01</DOCUMTIP><DOCUMDAT>27767155</DOCUMDAT><SESSION_ID>8667460</SESSION_ID></Request>";
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
		String response = sh.getValue();
		assertTrue("No aparece como habilitado para navegacion", response.contains("<HABILITADO_NAVEGACION>S</HABILITADO_NAVEGACION>"));
		assertTrue("No aparece como habilitado para navegacion", !response.contains("<HABILITADO_NAVEGACION>N</HABILITADO_NAVEGACION>"));
	}

	@Test
	public void testCasoBug112_2() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
//		String endpoint = TestConstants.TEST_SERVICE;
//		String endpoint = "http://localhost:7001/CMS-fewebservices/MigratedComponentService";
		String endpoint = "http://10.1.10.98:8011/OV/Proxy/QBESvc?wsdl";
		nbwsA_sInicio comp = new nbwsA_sInicio();
		OSBConnector conn = new OSBConnector();
		conn.setServiceURL(endpoint);
		comp.setOsbConnector(conn);
		String pvarRequest = "<Request><DOCUMTIP>01</DOCUMTIP><DOCUMDAT>27770101</DOCUMDAT><SESSION_ID>8667607</SESSION_ID></Request>";
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
		String response = sh.getValue();
//		assertTrue("No aparece como habilitado para navegacion", response.contains("<HABILITADO_NAVEGACION>S</HABILITADO_NAVEGACION>"));
//		assertTrue("No aparece como habilitado para navegacion", !response.contains("<HABILITADO_NAVEGACION>N</HABILITADO_NAVEGACION>"));
	}

	@Test
	public void testCasoBugXXX() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
//		String endpoint = TestConstants.TEST_SERVICE;
//		String endpoint = "http://localhost:7001/CMS-fewebservices/MigratedComponentService";
		String endpoint = "http://10.1.10.98:8011/OV/Proxy/QBESvc?wsdl";
		nbwsA_sInicio comp = new nbwsA_sInicio();
		OSBConnector conn = new OSBConnector();
		conn.setServiceURL(endpoint);
		comp.setOsbConnector(conn);
		String pvarRequest = "<Request><DOCUMTIP>01</DOCUMTIP><DOCUMDAT>13336480</DOCUMDAT><SESSION_ID>236633439</SESSION_ID></Request>";
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
		String response = sh.getValue();
//		System.out.println(response);
//		assertTrue("No aparece como habilitado para navegacion", response.contains("<HABILITADO_NAVEGACION>S</HABILITADO_NAVEGACION>"));
//		assertTrue("No aparece como habilitado para navegacion", !response.contains("<HABILITADO_NAVEGACION>N</HABILITADO_NAVEGACION>"));
	}

	@Test
	@Ignore //Ignorado porque ahora sale que la póliza no está vigente, quedó viejo este caso
	public void testCasoBug436() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		String endpoint = "http://10.1.10.98:8011/OV/Proxy/CMSSvc?wsdl";
		nbwsA_sInicio comp = new nbwsA_sInicio();
		OSBConnector conn = new OSBConnector();
		conn.setServiceURL(endpoint);
		comp.setOsbConnector(conn);
		String pvarRequest = "<Request><DOCUMTIP>01</DOCUMTIP><DOCUMDAT>20336029</DOCUMDAT><SESSION_ID>236634459</SESSION_ID></Request>";
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
		String response = sh.getValue();
		assertTrue("No aparece como habilitado para navegacion", response.contains("<HABILITADO_NAVEGACION>S</HABILITADO_NAVEGACION>"));
		assertTrue("No aparece como habilitado para navegacion", !response.contains("<HABILITADO_NAVEGACION>N</HABILITADO_NAVEGACION>"));
	}

	@Test
	public void testCasoBug512() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		nbwsA_sInicio comp = new nbwsA_sInicio();
		OSBConnector conn = new OSBConnector();
		String serviceURL = CurrentProfile.getProfileName().equals("test") ? TestConstants.TEST_SERVICE : TestConstants.UAT_SERVICE;
		conn.setServiceURL(serviceURL);
		comp.setOsbConnector(conn);
		String pvarRequest = "<Request><DOCUMTIP>01</DOCUMTIP><DOCUMDAT>18069158</DOCUMDAT><SESSION_ID>236634459</SESSION_ID></Request>";
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
		String response = sh.getValue();
		assertTrue("No aparece como habilitado para navegacion", response.contains("<HABILITADO_NAVEGACION>S</HABILITADO_NAVEGACION>"));
		assertTrue("No aparece como habilitado para navegacion", !response.contains("<HABILITADO_NAVEGACION>N</HABILITADO_NAVEGACION>"));
	}
}
