package com.qbe.services.segurosOnline.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;

public class MailSenderTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Ignore //A partir de 4/8/2014 da java.net.ConnectException: Connection refused
	public void testSendCAIMail() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));

		MailSender sender = new MailSender();
		boolean result = sender.sendCAIMail("ramiro@snoopconsulting.com", "NBWS", "HiI'mJenkins");
		assertTrue(result);
	}

}
