package com.qbe.services.segurosOnline.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.qbe.scheduler.InboxItem;
import com.qbe.scheduler.InstallSchedulerDB;
import com.qbe.scheduler.SchedulerConstants;
import com.qbe.scheduler.SchedulerDAO;
import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.vbcompat.string.StringHolder;

@Ignore
public class nbwsA_FillSchedulerTest {

	private static final String SCHEDULER_DAO = "schedulerDAO";
	private ApplicationContext context;
	private SchedulerDAO dao;
	
	@BeforeClass
	public static void load() throws Exception {
		if ( CurrentProfile.getProfileName().equals("dev")) {
//			System.out.println("Profile es: " + CurrentProfile.getProfileName());
			SchedulerDAO dao = (SchedulerDAO) new ClassPathXmlApplicationContext(
					"classpath*:scheduler-beans.xml").getBean(SCHEDULER_DAO);
			InstallSchedulerDB installer = new InstallSchedulerDB();
			installer.setDao(dao);
			installer.loadSchema();
		}
	}

	public nbwsA_FillSchedulerTest() {
		context = new ClassPathXmlApplicationContext(
				"classpath*:scheduler-beans.xml");
		dao = (SchedulerDAO) context.getBean(SCHEDULER_DAO);
	}

	@AfterClass
	public static void tearDown() throws Exception {
		if ( CurrentProfile.getProfileName().equals("dev")) {
			SchedulerDAO dao = (SchedulerDAO) new ClassPathXmlApplicationContext(
					"classpath*:scheduler-beans.xml").getBean(SCHEDULER_DAO);
			InstallSchedulerDB installer = new InstallSchedulerDB();
			installer.setDao(dao);
			installer.shutdownData();
		}
	}

	@Test 
	public void testIAction_Execute() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		nbwsA_FillScheduler fillS = new nbwsA_FillScheduler();
		OSBConnector osbConnector = new OSBConnector();
		osbConnector.setServiceURL(TestConstants.ROUTER_OV);
		fillS.setOsbConnector(osbConnector);

		String pvarRequest = "<INSCTRL>S</INSCTRL>"; //probar con N
		String pvarContextInfo = "";
		StringHolder pvarResponseSH = new StringHolder();
		int code = fillS.IAction_Execute(pvarRequest, pvarResponseSH, pvarContextInfo);
		assertTrue("Falló. code = " + code + " Response = " + pvarResponseSH.getValue(), code == 0);

//		System.out.println("Begin tests resultados en la base");
		//Valido que haya cargado algo en la base
		List<InboxItem> items = dao.findUnlockedItemsByAplicTerminadoUsu(SchedulerConstants.NBWS_E_POLIZA_EXE_APP, SchedulerConstants.NO_TERMINADO, SchedulerConstants.USUARIO);
		
		assertNotNull(items);
		//Con los datos que tengo ahora en el mock ( y en AIS ), genera unos 30 registros
		//FIXME Corregir este test, no está andando en desa98
//		assertTrue("No encontró más de 30 registros, revisar si el servicio ( mock, ais, insis ) devuelve esta cantidad",items.size() > 30);
	}

}
