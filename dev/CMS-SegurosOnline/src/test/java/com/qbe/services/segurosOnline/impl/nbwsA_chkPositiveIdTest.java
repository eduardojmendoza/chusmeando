package com.qbe.services.segurosOnline.impl;

import static org.junit.Assert.*;

import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.vbcompat.string.StringHolder;

public class nbwsA_chkPositiveIdTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Ignore //FIXME Se perdieron los datos de la base de test. Cargar algunos para probarlo
	public void testIAction_Execute() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		nbwsA_chkPositiveId comp = new nbwsA_chkPositiveId();
		OSBConnector conn = new OSBConnector();
		conn.setServiceURL(TestConstants.TEST_SERVICE);
		comp.setOsbConnector(conn);
		String pvarRequest = "<Request><USUARIO>martin.cabrera@zurich.com.ar</USUARIO><PRODUCTO>AUS1|0|1|0|1|411665</PRODUCTO><DATO1>00010</DATO1><DATO2>10DBS40005787</DATO2><DATO3>JCB046</DATO3><DATO4>2010</DATO4></Request>";
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(pvarRequest, sh, "");
		//String expectedFull = "<Response><Estado resultado=\"true\" estado=\"\" mensaje=\"\"/><CODRESULTADO>OK</CODRESULTADO><CODERROR>0</CODERROR><MSGRESULTADO>OK</MSGRESULTADO></Response>";
		String flatResult = StringUtils.deleteWhitespace(sh.getValue());
		assertTrue(flatResult.contains("<CODRESULTADO>OK</CODRESULTADO><CODERROR>0</CODERROR><MSGRESULTADO>OK</MSGRESULTADO>"));
	}

}
