package com.qbe.services.segurosOnline.impl;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Node;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

public class nbwsA_IngresarTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIAction_Execute() throws CurrentProfileException, XmlDomExtendedException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		nbwsA_Ingresar comp = new nbwsA_Ingresar();
		OSBConnector conn = new OSBConnector();
		conn.setServiceURL(TestConstants.TEST_SERVICE);
		comp.setOsbConnector(conn);
		String paso1 = "	<Request> " +
"<IPORIGEN>9.9.9.9</IPORIGEN>" +
"<PASSWORD>SNOOP123</PASSWORD>" +
"<USUARIO>ramiro@snoopconsulting.com</USUARIO>" +
"</Request>";
		StringHolder sh = new StringHolder();
		comp.IAction_Execute(paso1, sh, "");
		System.out.println(sh.getValue());

		// acá tengo q averiguar los digitos
		// DNI 23626965
		
		String dni = "23626965";
		
		XmlDomExtended responseXML = new XmlDomExtended();
		responseXML.loadXML(sh.getValue());
		
		Node resultadoNode = responseXML.selectSingleNode("//Response/Estado/@resultado");
		String resultadoValue = XmlDomExtended.getText(resultadoNode);
		assertTrue("resultado=false en el paso1", "true".equals(resultadoValue));
		
		Node rcc1Node = responseXML.selectSingleNode("//Response/RCCs/rcc[@id='1']");
		String rcc1 = XmlDomExtended.getText(rcc1Node);
		char rcc1Digit = dni.charAt(Integer.parseInt(rcc1) - 1); 

		Node rcc2Node = responseXML.selectSingleNode("//Response/RCCs/rcc[@id='2']");
		String rcc2 = XmlDomExtended.getText(rcc2Node);
		char rcc2Digit = dni.charAt(Integer.parseInt(rcc2) - 1); 

		Node rcc3Node = responseXML.selectSingleNode("//Response/RCCs/rcc[@id='3']");
		String rcc3 = XmlDomExtended.getText(rcc3Node);
		char rcc3Digit = dni.charAt(Integer.parseInt(rcc3) - 1); 

		
		String paso2Template = "	<Request> " +
"<IPORIGEN>9.9.9.9</IPORIGEN>" +
"<PASSWORD>SNOOP123</PASSWORD>" +
"<USUARIO>ramiro@snoopconsulting.com</USUARIO>" +
"<DOCUMTIP>1</DOCUMTIP>" +
"<RCCs>" +
"	<rcc id=\"1\">{0}</rcc>" +
"   <rcc id=\"2\">{1}</rcc>" +
"   <rcc id=\"3\">{2}</rcc>" +
"</RCCs>" +
"</Request>";

		String paso2 = MessageFormat.format(paso2Template, rcc1Digit, rcc2Digit, rcc3Digit);
		System.out.println("Paso2 = " + paso2);
		StringHolder sh2 = new StringHolder();
		comp.IAction_Execute(paso2, sh2, "");
		System.out.println(sh2.getValue());
	
	}

	
	@Test
	public void listarUsuarios() throws SQLException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));

        java.sql.Connection jdbcConn = null;
        try {
            JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
            jdbcConn = connFactory.getJDBCConnection("camA_OficinaVirtual.udl");

      Statement stmt = jdbcConn.createStatement();

      String sql = "SELECT DOCUMTIP, DOCUMDAT, Mail, Password, EstadoPassword, " +
      		"PasswordIntentosFallidos, ViaInscripcion, ResponsableAlta, EstadoIdentificador, " +
      		"RCCIntentosFallidos, FechaSuscripcion, FechaUltAct, Estado, Conformidad " +
      		"FROM NBWS_Usuarios";
      ResultSet rs = stmt.executeQuery(sql);

      while(rs.next()){
         String mail = rs.getString("Mail");
         String password = rs.getString("Password");
         BigDecimal nroDoc = rs.getBigDecimal("DOCUMDAT");
         
         System.out.print("mail: " + mail);
         System.out.print(", nroDoc: " + nroDoc);
         System.out.println(", password: " + password);
      }
      rs.close();
        } finally {
                if (jdbcConn != null)
                    jdbcConn.close();
        }
		
	}
	
}
