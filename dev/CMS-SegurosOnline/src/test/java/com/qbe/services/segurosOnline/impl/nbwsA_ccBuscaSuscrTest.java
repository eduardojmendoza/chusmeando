package com.qbe.services.segurosOnline.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.testutils.TestUtils;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;

public class nbwsA_ccBuscaSuscrTest  {
		
		public nbwsA_ccBuscaSuscrTest() {
			super();
		}
		
		/**
		 * Testea la ejecución del mensaje, y que no vuelva un código de error. Valida contra la respuesta esperada
		 * 
		 * @param msgCode
		 * @throws Exception 
		 */
		protected void testMessageFullRequest (String msgCode, VBObjectClass comp) throws Exception {
			String responseText = this.testMessageJustExecution(msgCode, comp);
		
			Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
			XMLUnit.setIgnoreWhitespace(true);
			String expectedResponse = TestUtils.loadResponse(msgCode);
			if ( expectedResponse == null || expectedResponse.length()==0) throw new Exception("No se pudo cargar el request");
			DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(expectedResponse, responseText));
			List allDifferences = myDiff.getAllDifferences();
			assertEquals(myDiff.toString(), 0, allDifferences.size());
			//Eliminar los &lt; y &gt;
			XMLAssert.assertXMLEqual("No volvió la respuesta esperada", expectedResponse, responseText);
		
		}
		
		/**
		 * Testea la ejecución del mensaje, y que no vuelva un código de error. No valida contra la respuesta esperada, la devuelve solamente
		 * 
		 * @param msgCode
		 * @throws Exception 
		 */
		protected String testMessageJustExecution (String msgCode, VBObjectClass comp) throws Exception {
			String request = TestUtils.loadRequest(msgCode);
			if ( request == null || request.length()==0 || request.trim().equals("")) throw new Exception("No se pudo cargar el request");
			StringHolder responseHolder = new StringHolder();
			int result = comp.IAction_Execute(request, responseHolder, null);
//			System.out.println("Response: " + responseHolder);
			if ( result == 1) Assert.fail ("Abortó la ejecución, ver el log");
			Assert.assertTrue("Result no es 1 ni 0, es un error",result == 0);
			try {
				TestUtils.parseResultado(responseHolder.getValue());
				TestUtils.parseMensaje(responseHolder.getValue());
			} catch (Exception e){
				assertTrue("No es posible parsear la respuesta", false);
			}
			String responseText = responseHolder.getValue();
			if (( responseText == null) || ( responseText.length()==0)) {
				fail("responseText vacío");
			}
			return responseText;
		}
	
		//------------------------------ T E S T ------------------------------------------	
		
		@Test
		public void testccBuscaSuscr() throws Exception {
			String msgCode = "_nbwsA_ccBuscaSuscr";
			Assume.assumeTrue(CurrentProfile.getProfileName().equals("test") || CurrentProfile.getProfileName().equals("uat"));
			nbwsA_ccBuscaSuscr comp = new nbwsA_ccBuscaSuscr();
			OSBConnector conn = new OSBConnector();
			String serviceURL = CurrentProfile.getProfileName().equals("test") ? TestConstants.TEST_SERVICE : TestConstants.UAT_SERVICE;
			conn.setServiceURL(serviceURL);
			comp.setOsbConnector(conn);
			testMessageFullRequest(msgCode, comp);
		}
		
		@Test
		public void testccBuscaSuscr_bug512() throws Exception {
			String msgCode = "_nbwsA_ccBuscaSuscr_bug512";
			Assume.assumeTrue(CurrentProfile.getProfileName().equals("test") || CurrentProfile.getProfileName().equals("uat"));
			nbwsA_ccBuscaSuscr comp = new nbwsA_ccBuscaSuscr();
			OSBConnector conn = new OSBConnector();
			String serviceURL = CurrentProfile.getProfileName().equals("test") ? TestConstants.TEST_SERVICE : TestConstants.UAT_SERVICE;
			conn.setServiceURL(serviceURL);
			comp.setOsbConnector(conn);
			testMessageFullRequest(msgCode, comp);
		}
		
		@Test
		public void testccBuscaSuscr_bug512_2() throws Exception {
			String msgCode = "_nbwsA_ccBuscaSuscr_bug512_2";
			Assume.assumeTrue(CurrentProfile.getProfileName().equals("test") || CurrentProfile.getProfileName().equals("uat"));
			nbwsA_ccBuscaSuscr comp = new nbwsA_ccBuscaSuscr();
			OSBConnector conn = new OSBConnector();
			String serviceURL = CurrentProfile.getProfileName().equals("test") ? TestConstants.TEST_SERVICE : TestConstants.UAT_SERVICE;
			conn.setServiceURL(serviceURL);
			comp.setOsbConnector(conn);
			testMessageFullRequest(msgCode, comp);
		}
		
		
		
}