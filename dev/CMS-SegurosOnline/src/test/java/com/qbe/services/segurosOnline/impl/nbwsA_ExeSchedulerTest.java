package com.qbe.services.segurosOnline.impl;

import static org.junit.Assert.assertTrue;

import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.junit.AfterClass;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;
import org.springframework.context.ApplicationContext;

import com.qbe.scheduler.InstallSchedulerDB;
import com.qbe.scheduler.SchedulerDAO;
import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.vbcompat.string.StringHolder;

@Ignore
public class nbwsA_ExeSchedulerTest {

	private static final String SCHEDULER_DAO = "schedulerDAO";
	private ApplicationContext context;
	private SchedulerDAO dao;
	
	@BeforeClass
	public static void load() throws Exception {
		if ( CurrentProfile.getProfileName().equals("dev")) {
//			System.out.println("Profile es: " + CurrentProfile.getProfileName());
			SchedulerDAO dao = (SchedulerDAO) new ClassPathXmlApplicationContext(
					"classpath*:scheduler-beans.xml").getBean(SCHEDULER_DAO);
			InstallSchedulerDB installer = new InstallSchedulerDB();
			installer.setDao(dao);
			installer.loadSchema();
		}
	}

	public nbwsA_ExeSchedulerTest() {
		context = new ClassPathXmlApplicationContext(
				"classpath*:scheduler-beans.xml");
		dao = (SchedulerDAO) context.getBean(SCHEDULER_DAO);
	}

	@AfterClass
	public static void tearDown() throws Exception {
		if ( CurrentProfile.getProfileName().equals("dev")) {
			SchedulerDAO dao = (SchedulerDAO) new ClassPathXmlApplicationContext(
					"classpath*:scheduler-beans.xml").getBean(SCHEDULER_DAO);
			InstallSchedulerDB installer = new InstallSchedulerDB();
			installer.setDao(dao);
			installer.shutdownData();
		}
	}

	/**
	 * Manda un request cableado 
	 * @throws CurrentProfileException 
	 */
	@Test
//	@Ignore
	public void testIAction_Execute() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		String request = "<Request>" +
				"<CIAASCOD>0001</CIAASCOD>" +
				"<RAMOPCOD>AUS1</RAMOPCOD>" +
				"<POLIZANN>0</POLIZANN>" +
				"<POLIZSEC>1</POLIZSEC>" +
				"<CERTIPOL>0</CERTIPOL>" +
				"<CERTIANN>1</CERTIANN>" +
				"<CERTISEC>265545</CERTISEC>" +
				"<OPERAPOL>13</OPERAPOL>" +
				"<DOCUMTIP>01</DOCUMTIP>" +
				"<DOCUMDAT>14822056</DOCUMDAT>" +
				"<CLIENAP1>MONTAÑA</CLIENAP1>" +
				"<CLIENAP2></CLIENAP2>" +
				"<CLIENNOM>SANTIAGO CARLOS</CLIENNOM>" +
				"<ARCHIVOS>" +
				"<ARCHIVO>" +
				"</ARCHIVO>" +  //"<NOMARCH>GR17200A_455067_000005.pdf</NOMARCH>" + "<FORMUDES></FORMUDES>" +
				"</ARCHIVOS>" +
				"<FORMUDES></FORMUDES>" +
				"<SWSUSCRI>S</SWSUSCRI>" +
				"<MAIL>smontanasegura@yahoo.com.ar</MAIL>" +
				"<CLAVE></CLAVE>" +
				"<SW-CLAVE></SW-CLAVE>" +
				"<SWENDOSO>S</SWENDOSO>" +
				"<OPERATIP>9</OPERATIP>" +
				"<ESTADO>P</ESTADO>" +
				"<CODESTAD></CODESTAD>" +
				"</Request>";
		StringHolder respHS = new StringHolder();
		nbwsA_ExeScheduler exe = new nbwsA_ExeScheduler();
		OSBConnector osbConnector = new OSBConnector();
		osbConnector.setServiceURL(TestConstants.ROUTER_OV);
		exe.setOsbConnector(osbConnector);
		int code = exe.IAction_Execute(request, respHS, "");
		assertTrue("Falló. code = " + code + " Response = " + respHS.getValue(), code == 0);
	}

	
	/**
	 * Manda un request cableado 
	 * @throws CurrentProfileException 
	 */
	@Test
//	@Ignore
	public void testBug599() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		String request = "<Request>" +
				"<CIAASCOD>0001</CIAASCOD>" +
				"<RAMOPCOD>RCP1</RAMOPCOD>" +
				"<POLIZANN>0</POLIZANN>" +
				"<POLIZSEC>136</POLIZSEC>" +
				"<CERTIPOL>136</CERTIPOL>" +
				"<CERTIANN>12</CERTIANN>" +
				"<CERTISEC>2</CERTISEC>" +
				"<OPERAPOL>3</OPERAPOL>" +
				"<DOCUMTIP>04</DOCUMTIP>" +
				"<DOCUMDAT>30708960612</DOCUMDAT>" +
				"<CLIENAP1>SIL SA SISTEMA INTEG</CLIENAP1>" +
				"<CLIENAP2>RAL DE LIMPIEZA</CLIENAP2>" +
				"<CLIENNOM/>" +
				"<ARCHIVOS>" +
				"   <ARCHIVO>      <NOMARCH>GR17150Q_487779_000001_000000000143157001.PDF</NOMARCH>      <FORMUDES>Póliza de RC Patronal</FORMUDES>   </ARCHIVO>" +
				"</ARCHIVOS>" +
				"<FORMUDES>null</FORMUDES>" +
				"<SWSUSCRI>S</SWSUSCRI>" +
				"<MAIL>ramiro@snoopconsulting.com</MAIL>" +
				"<CLAVE/>" +
				"<SW-CLAVE/>" +
				"<SWENDOSO/>" +
				"<OPERATIP>8</OPERATIP><ESTADO>P</ESTADO><CODESTAD/></Request>";
		StringHolder respHS = new StringHolder();
		nbwsA_ExeScheduler exe = new nbwsA_ExeScheduler();
		OSBConnector osbConnector = new OSBConnector();
		osbConnector.setServiceURL(TestConstants.TEST_SERVICE);
		exe.setOsbConnector(osbConnector);
		int code = exe.IAction_Execute(request, respHS, "");
		if ( code == 1 && respHS.getValue().contains("java.net.UnknownHostException: hostplanetpress")) {
			//problema de config
			return;
		}
		assertTrue("Falló. code = " + code + " Response = " + respHS.getValue(), code == 0);
	}

}
