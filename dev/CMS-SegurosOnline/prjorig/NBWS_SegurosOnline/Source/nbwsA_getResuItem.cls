VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_getResuItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

'-----------------------------------------------------------------------------------------------------------------------------------
'TODO: poner COPYRIGHT
' *****************************************************************
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName              As String = "nbwsA_Transacciones.nbwsA_getResuItem"
Const mcteParam_NROPOLIZA        As String = "//NROPOLIZA"
Const mcteParam_PERIODO          As String = "//PERIODO"

'
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjCmdProcessor    As Object
    Dim wvarExecReturn      As String
    '
    Dim wvarStep            As Long
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wvarITEM            As Long
    Dim wvarUltimo          As Long
    Dim wvarBASE64          As String
    '
    Dim wvarNROPOLIZA        As String
    Dim wvarPERIODO          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Carga XML de entrada
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
        wobjXMLRequest.async = False
        wobjXMLRequest.loadXML (Request)
    '
    wvarStep = 20
    wvarNROPOLIZA = wobjXMLRequest.selectSingleNode(mcteParam_NROPOLIZA).Text
    wvarPERIODO = wobjXMLRequest.selectSingleNode(mcteParam_PERIODO).Text
    '
    wvarStep = 30
    wvarRequest = wvarRequest & "<Request>" & _
                                "    <DEFINICION>ismRetrieveReportList.xml</DEFINICION>" & _
                                "    <coldViewPubNode>CVUN_RepositoryEstructure/CVUns_Pub_Root/NYLPrinting/HSBCNYLVida/SegurosComplejos/Semestrales</coldViewPubNode>" & _
                                "    <coldViewMetadata>poli=" & wvarNROPOLIZA & ";fhas=" & wvarPERIODO & "</coldViewMetadata>" & _
                                "</Request>"
    
    wvarRequest = cmdp_FormatRequest("lbaw_OVMWGen", "lbaw_OVMWGen.biz", wvarRequest)
    '
    wvarStep = 40
    Set wobjCmdProcessor = CreateObject("HSBC_ASP.CmdProcessor")
    wvarExecReturn = wobjCmdProcessor.Execute(wvarRequest, wvarResponse)
    '
    wvarStep = 50
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        wobjXMLResponse.async = False
    Call wobjXMLResponse.loadXML(wvarResponse)

    If Not wobjXMLResponse.selectSingleNode("Response/Estado/@resultado") Is Nothing Then
        If Not wobjXMLResponse.selectSingleNode("Response/retrieveReportListReturn//item") Is Nothing Then
            '
            wvarStep = 60
            'Recupera el �ltimo ITEM (se asume que ese es el PDF a mostrar)
            wvarUltimo = wobjXMLResponse.selectNodes("Response/retrieveReportListReturn//item").length
            wvarITEM = wobjXMLResponse.selectNodes("Response/retrieveReportListReturn//item").Item(wvarUltimo - 1).Text
            '
        Else
            '
            wvarStep = 70
            'No hay item
            Err.Description = "No se encontraron items en coldview."
            GoTo ErrorHandler
            '
        End If
    Else
        wvarStep = 80
        Err.Description = "Pinch� el COM+ de recupero de ITEMS"
        GoTo ErrorHandler
    End If
    '
    Set wobjCmdProcessor = Nothing
    '
    wvarStep = 90
    wvarRequest = ""
    'DA - 10/06/2011: se pasa el par�metro fhas por ticket 619330
    wvarRequest = wvarRequest & "<Request>" & _
                                "    <DEFINICION>ismRetrieveReport.xml</DEFINICION>" & _
                                "    <coldViewPubNode>CVUN_RepositoryEstructure/CVUns_Pub_Root/NYLPrinting/HSBCNYLVida/SegurosComplejos/Semestrales</coldViewPubNode>" & _
                                "    <coldViewMetadata>poli=" & wvarNROPOLIZA & ";fhas=" & wvarPERIODO & "</coldViewMetadata>" & _
                                "    <item>" & wvarITEM & "</item>" & _
                                "</Request>"
    
    wvarRequest = cmdp_FormatRequest("lbaw_OVMWGen", "lbaw_OVMWGen.biz", wvarRequest)
    '
    wvarStep = 100
    Set wobjCmdProcessor = CreateObject("HSBC_ASP.CmdProcessor")
    wvarResponse = ""
    wvarExecReturn = wobjCmdProcessor.Execute(wvarRequest, wvarResponse)
    '
    wvarStep = 110
    Call wobjXMLResponse.loadXML(wvarResponse)
    
    wvarStep = 120
    If Not wobjXMLResponse.selectSingleNode("Response/Estado/@resultado") Is Nothing Then
        If Not wobjXMLResponse.selectSingleNode("//report") Is Nothing Then
            '
            wvarStep = 130
            'Recupera el base 64
            wvarBASE64 = "<PDFSTREAM>" & wobjXMLResponse.selectSingleNode("//report").Text & "</PDFSTREAM>"
            '
        Else
            '
            wvarStep = 140
            'No hay PDF
            Err.Description = "No se encontr� el PDF en coldview."
            GoTo ErrorHandler
            '
        End If
    Else
        wvarStep = 150
        Err.Description = "Pinch� el COM+ de recupero de PDF"
        GoTo ErrorHandler
    End If
    '
    Response = "<Response><Estado resultado=""true"" mensaje=""""></Estado>" & wvarBASE64 & "</Response>"
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    '
    wvarStep = 190
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    '
    If wvarStep = 70 Then
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeWarning
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    End If
    '
    mobjCOM_Context.SetAbort
    IAction_Execute = 1
    '
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub







